﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Windows.Forms;
using System.IO;

namespace Dit.Framework.Ini
{
    public static class IniFileManager
    {
        public static void SaveIni(string iniFilePath, BaseSetting data)
        {
            IniReader ini = new IniReader(iniFilePath);

            foreach (PropertyInfo info in data.GetType().GetProperties())
            {
                if (info.PropertyType.IsPublic)
                {
                    foreach (object obj in info.GetCustomAttributes(false))
                    {
                        IniAttribute iniAttri = obj as IniAttribute;
                        if (iniAttri != null)
                        {
                            object value = info.GetValue(data, null);
                            value = (value == null) ? iniAttri.DefValue : value;

                            if (info.GetGetMethod().ReturnType == typeof(int))
                            {
                                ini.SetInteger(iniAttri.Section, iniAttri.Key, (int)value);
                            }
                            else if (info.GetGetMethod().ReturnType == typeof(long))
                            {
                                ini.SetLong(iniAttri.Section, iniAttri.Key, (long)value);
                            }
                            else if (info.GetGetMethod().ReturnType == typeof(short))
                            {
                                ini.SetInteger(iniAttri.Section, iniAttri.Key, (int)(short)value);
                            }
                            else if (info.GetGetMethod().ReturnType == typeof(short[]))
                            {
                                string str = string.Join(",", (short[])value).ToString();
                                ini.SetString(iniAttri.Section, iniAttri.Key, str);
                            }
                            else if (info.GetGetMethod().ReturnType == typeof(float))
                            {
                                ini.SetFloat(iniAttri.Section, iniAttri.Key, (float)Convert.ToDouble(value));
                            }
                            else if (info.GetGetMethod().ReturnType == typeof(double))
                            {
                                ini.SetDouble(iniAttri.Section, iniAttri.Key, Convert.ToDouble(value));
                            }
                            else if (info.GetGetMethod().ReturnType == typeof(string))
                            {
                                ini.SetString(iniAttri.Section, iniAttri.Key, (string)value);
                            }
                            else if (info.GetGetMethod().ReturnType == typeof(bool))
                            {
                                ini.SetBoolean(iniAttri.Section, iniAttri.Key, (bool)value);
                            }
                            else if (info.GetGetMethod().ReturnType == typeof(DateTime))
                            {
                                ini.SetString(iniAttri.Section, iniAttri.Key, ((DateTime)value).ToString("yyyyMMddHHmmss"));
                            }

                            break;
                        }
                    }
                }
            }
        }
        public static void LoadIni(string iniFilePath, BaseSetting data)
        {
            Func<string, short> convertShort = delegate(string value)
            {
                return value != "" ? short.Parse(value) : (short)0;
            };
            Func<string, int, short[]> convertShorts = delegate(string value, int arrayCount)
            {
                short[] values = new short[arrayCount];
                int iPos = 0;
                foreach (short vv in value.Split(',').ToList().Select(f => convertShort(f)))
                    values[iPos++] = vv;

                return values;
            };

            if (!File.Exists(iniFilePath))
                throw new Exception("파일존재하지않음");
            IniReader ini = new IniReader(iniFilePath);
            foreach (PropertyInfo info in data.GetType().GetProperties())
            {
                if (info.PropertyType.IsPublic)
                {
                    foreach (object obj in info.GetCustomAttributes(false))
                    {
                        try
                        {
                            IniAttribute iniAttri = obj as IniAttribute;
                            if (iniAttri != null)
                            {
                                if (info.GetGetMethod().ReturnType == typeof(int))
                                {
                                    int value = ini.GetInteger(iniAttri.Section, iniAttri.Key, (int)iniAttri.DefValue);
                                    info.SetValue(data, value, null);
                                }
                                else if (info.GetGetMethod().ReturnType == typeof(short))
                                {
                                    short value = (short)ini.GetInteger(iniAttri.Section, iniAttri.Key, (int)iniAttri.DefValue);
                                    info.SetValue(data, value, null);
                                }
                                else if (info.GetGetMethod().ReturnType == typeof(short[]))
                                {
                                    string str = ini.GetString(iniAttri.Section, iniAttri.Key);
                                    int length = (int)Math.Truncate((double)(str.Length / 2)) + 1;
                                    short[] vv = convertShorts(str, length);
                                    info.SetValue(data, vv, null);
                                }
                                else if (info.GetGetMethod().ReturnType == typeof(long))
                                {
                                    long value = (long)ini.GetInteger(iniAttri.Section, iniAttri.Key, (int)iniAttri.DefValue);
                                    info.SetValue(data, value, null);
                                }
                                else if (info.GetGetMethod().ReturnType == typeof(float))
                                {
                                    float value = ini.GetFloat(iniAttri.Section, iniAttri.Key, (float)Convert.ToDouble(iniAttri.DefValue));
                                    info.SetValue(data, value, null);
                                }
                                else if (info.GetGetMethod().ReturnType == typeof(double))
                                {
                                    double value = ini.GetDouble(iniAttri.Section, iniAttri.Key, Convert.ToDouble(iniAttri.DefValue));
                                    info.SetValue(data, value, null);
                                }
                                else if (info.GetGetMethod().ReturnType == typeof(string))
                                {
                                    string value = ini.GetString(iniAttri.Section, iniAttri.Key);
                                    info.SetValue(data, value, null);
                                }
                                else if (info.GetGetMethod().ReturnType == typeof(bool))
                                {
                                    int vv;
                                    if (int.TryParse(iniAttri.DefValue.ToString(), out vv) == false)
                                    {
                                        bool value = ini.GetBoolean(iniAttri.Section, iniAttri.Key, (bool)iniAttri.DefValue);
                                        info.SetValue(data, value, null);
                                    }
                                    else
                                    {
                                        bool value = ini.GetBoolean(iniAttri.Section, iniAttri.Key, (int)iniAttri.DefValue == 1 ? true : false);
                                        info.SetValue(data, value, null);
                                    }
                                }
                                else if (info.GetGetMethod().ReturnType == typeof(DateTime))
                                {
                                    string date = ini.GetString(iniAttri.Section, iniAttri.Key);

                                    DateTime datetime;
                                    DateTime.TryParseExact(date, "yyyyMMddHHmmss", null, System.Globalization.DateTimeStyles.None, out datetime);
                                    info.SetValue(data, datetime, null);
                                }
                                break;
                            }

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                            MessageBox.Show(ex.StackTrace);
                        }
                    }
                }
            }
        }
    }
}
