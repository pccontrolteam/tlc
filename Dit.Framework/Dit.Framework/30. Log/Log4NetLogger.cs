﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using log4net;
using log4net.Appender;
using log4net.Layout;
using log4net.Repository.Hierarchy;
using log4net.Core;
using System.IO;
using System.Timers;
using Dit.Framework.Log;

namespace Dit.Framework.Log
{
    public enum LogPeriod
    {
        Hour,
        Day,
    }

    /// <summary>
    /// brief log4net 사용 logger
    /// since 180221
    /// </summary>
    public class Log4NetLogger
    {
        private string _name;
        private static List<Log4NetLogger> _loggers = new List<Log4NetLogger>();
        private ILog Log;

        //UI output
        private static System.Timers.Timer _tmrUiOut = null;
        private static List<LoggingEventArgs> _logReporter;
        private static ImageList _imgList = new ImageList();
        private static List<ListView> _logViewList = null;

        private string _rootPath;
        private string _maxFileSize;
        private int _maxFileCount;
        private string _logPattern;
        private int _storageDay = 1;
        private static System.Timers.Timer _tmrDeleteOldLog;

        private static Dictionary<LogPeriod, string> LogPeriodPattern = new Dictionary<LogPeriod, string>()
        {
            {LogPeriod.Hour, "yyyy-MM-dd-HH"},
            {LogPeriod.Day, "yyyy-MM-dd"},
        };

        /// <summary>
        /// 
        /// </summary>
        /// <param name="loggerName">ex) "Control"</param>
        /// <param name="rootPath">ex) "Log\\"</param>
        /// <param name="filenamePrefix">ex) "Ctrl"</param>
        /// <param name="maxFileSize">default) "15MB"</param>
        /// <param name="logPeriod">
        /// <para>파일 저장 주기 설정</para>
        /// <para>default) LogPeriod.Day</para>
        /// </param>
        /// <param name="logMsgPattern">
        /// <para>string.Empty 전달 시 Default 패턴을 따릅니다</para>
        /// <para>참고링크 - "https://logging.apache.org/log4net/log4net-1.2.13/release/sdk/log4net.Layout.PatternLayout.html"</para>
        /// <para>default) "%date{yyMMdd}\t%date{HH:mm:ss.fff}\t%level\t%message%newline"</para>
        /// </param>
        /// <param name="maxFileCount">default) 1000</param>
        /// <param name="storageDay">default) 30</param>
        /// <param name="datePattern">
        /// <para>임의로 폴더 및 파일명을 설정할 때 사용,</para>
        /// <para>string.Empty 전달 시 "yyyy-MM-dd\\\\'{filenamePrefix}'{logPeriod}'.log'" 패턴을 따릅니다</para>
        /// <para>ex) yyyy-MM-dd\'Ctrl-'yyyy-MM-dd-hh'.log'</para>
        /// </param>     
        public Log4NetLogger(
            string loggerName, string rootPath, string filenamePrefix, LogPeriod logPeriod, string datePattern,
            int storageDay, string maxFileSize, string logMsgPattern, int maxFileCount)
        {
            _name = loggerName;

            Initialize(
                loggerName, rootPath, filenamePrefix, logPeriod, datePattern,
                storageDay, maxFileSize, logMsgPattern, maxFileCount);

            _loggers.Add(this);
        }

        private static void _tmrDeleteOldLog_Elapsed(object sender, ElapsedEventArgs e)
        {
            foreach (Log4NetLogger logger in _loggers)
            {
                DirectoryInfo root = new DirectoryInfo(logger._rootPath);
                DeleteFilesAndDirectories(root, root, DateTime.Now.Date.AddDays(-logger._storageDay));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="loggerName">ex) "Control"</param>
        /// <param name="rootPath">ex) "Log\\"</param>
        /// <param name="filenamePrefix">ex) "Ctrl"</param>
        /// <param name="maxFileSize">ex) "15MB"</param>
        /// <param name="logPeriod">파일 저장 주기 설정 ex) LogPeriod.Day</param>
        /// <param name="logMsgPattern">
        /// <para>string.Empty 전달 시 Default 패턴을 따릅니다</para>
        /// <para>"https://logging.apache.org/log4net/log4net-1.2.13/release/sdk/log4net.Layout.PatternLayout.html"</para>
        /// <para>default) "%date{yyMMdd}\t%date{HH:mm:ss.fff}\t%level\t%message%newline"</para>
        /// </param>
        /// <param name="maxFileCount">ex) 1000</param>
        /// <param name="storageDay">ex) 30</param>
        /// <param name="datePattern">
        /// <para>임의로 폴더 및 파일명을 설정할 때 사용,</para>
        /// <para>string.Empty 전달 시 "yyyy-MM-dd\\\\'{filenamePrefix}'{logPeriod}'.log'" 패턴을 따릅니다</para>
        /// <para>ex) yyyy-MM-dd\'Ctrl-'yyyy-MM-dd-hh'.log'</para>
        /// </param>         
        private void Initialize(
            string loggerName, string rootPath, string filenamePrefix, LogPeriod logPeriod, string datePattern,
            int storageDay, string maxFileSize, string logMsgPattern, int maxFileCount)
        {
            Log = log4net.LogManager.GetLogger(loggerName);

            StaticInitialize();

            _rootPath = rootPath;
            _storageDay = storageDay;
            _maxFileSize = maxFileSize;
            _logPattern = logMsgPattern == string.Empty ? "%date{yyMMdd}\t%date{HH:mm:ss.fff}\t%level\t%message%newline" : logMsgPattern;
            _maxFileCount = maxFileCount;

            Hierarchy hierarchy = (Hierarchy)LogManager.GetRepository();

            PatternLayout patternLayout = new PatternLayout();
            patternLayout.ConversionPattern = _logPattern;
            patternLayout.ActivateOptions();

            RollingFileAppender roller = new RollingFileAppender();
            //jys:: _logPath\yyyy-MM-dd\yyyy-MM-dd-hh.log 형태로 생성됨
            //jys:: 문자열을 ''로 감싸면 형식지정되지않음
            roller.DatePattern = datePattern == string.Empty ? string.Format("yyyy-MM-dd\\\\'{0}'{1}'.log'", filenamePrefix, LogPeriodPattern[logPeriod]) : datePattern;
            roller.AppendToFile = true;
            roller.File = _rootPath;
            roller.Layout = patternLayout;
            roller.MaxSizeRollBackups = _maxFileCount;
            roller.MaximumFileSize = _maxFileSize;
            roller.RollingStyle = RollingFileAppender.RollingMode.Composite;
            roller.StaticLogFileName = false;
            roller.PreserveLogFileNameExtension = true;
            roller.ActivateOptions();

            hierarchy.Root.AddAppender(roller);

            hierarchy.Root.Level = Level.All;
            hierarchy.Configured = true;
        }

        private static void StaticInitialize()
        {
            if (_tmrDeleteOldLog == null)
            {
                _tmrDeleteOldLog = new System.Timers.Timer();
                _tmrDeleteOldLog.Elapsed += new ElapsedEventHandler(_tmrDeleteOldLog_Elapsed);
                _tmrDeleteOldLog.Interval = (int)((new TimeSpan(0, 0, 1).TotalSeconds) * 1000);
                _tmrDeleteOldLog.Start();
            }
        }
        /// <summary>
        /// Default - Initialize("Control","Log\\","Ctrl",LogPeriod.Day,30,"15MB","%date{yyMMdd}\t%date{HH:mm:ss.fff}\t%level\t%message%newline",1000);
        /// </summary>
        public void InitializeDefault()
        {
            Initialize(
                "Control",
                "Log\\",
                "Ctrl",
                LogPeriod.Day,
                string.Empty,
                30,
                "15MB",
                "%date{yyMMdd}\t%date{HH:mm:ss.fff}\t%level\t%message%newline",
                1000);
        }

        public static void InitializeUI(int listviewUpdateInterval, ListView logView)
        {
            if (_logReporter == null)
                _logReporter = new List<LoggingEventArgs>();

            AddListView(logView);

            if (_tmrUiOut == null)
            {
                _tmrUiOut = new System.Timers.Timer(listviewUpdateInterval);
                _tmrUiOut.Elapsed += new ElapsedEventHandler(_tmrUiOut_Elapsed);
                _tmrUiOut.Start();
            }
        }

        public static void AddListView(ListView logView)
        {
            if (_logViewList == null)
                _logViewList = new List<ListView>();

            if (logView != null)
            {
                _logViewList.Add(logView);
                InitializeLogView(logView);
            }
        }

        private static void InitializeLogView(ListView logView)
        {
            _imgList.Images.Add(System.Drawing.SystemIcons.Warning);
            _imgList.Images.Add(System.Drawing.SystemIcons.Error);
            _imgList.Images.Add(System.Drawing.SystemIcons.Information);

            logView.View = View.Details;
            logView.FullRowSelect = true;
            logView.SmallImageList = _imgList;
            logView.LargeImageList = _imgList;

            int width = logView.Width - 160 > 0 ? logView.Width - 160 : 300;
            logView.Columns.Add(new ColumnHeader() { Width = 30, Name = "chNo", Text = "-" });
            logView.Columns.Add(new ColumnHeader() { Width = 100, Name = "chTime", Text = "TIME", TextAlign = HorizontalAlignment.Center });
            logView.Columns.Add(new ColumnHeader() { Width = width + 50, Name = "chLog", Text = "LOG", TextAlign = HorizontalAlignment.Left });
            logView.Resize += delegate(object sender, EventArgs e)
            {
                logView.Columns[2].Width = 1000;
            };
        }

        private static void _tmrUiOut_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (System.Threading.Monitor.TryEnter(_tmrUiOut))
            {
                foreach (Log4NetLogger logger in _loggers)
                {
                    try
                    {
                        int a = 0;
                        if (Log4NetLogger._logReporter.Count > 0)
                            a = 0;
                        LoggingEventArgs[] logReporter = new LoggingEventArgs[Log4NetLogger._logReporter.Count];
                        lock (Log4NetLogger._logReporter)
                        {
                            Log4NetLogger._logReporter.CopyTo(logReporter);
                            Log4NetLogger._logReporter.Clear();
                        }

                        foreach (LoggingEventArgs log in logReporter)
                        {
                            int logIcon = 0;
                            switch (log.Level)
                            {
                                case LogLevel.AllLog: logIcon = 2; break;
                                case LogLevel.NoLog: logIcon = 3; break;
                                case LogLevel.Info: logIcon = 2; break;
                                case LogLevel.Warning: logIcon = 0; break;
                                case LogLevel.Error: logIcon = 1; break;
                                case LogLevel.Fault: logIcon = 1; break;
                                case LogLevel.Unuse: logIcon = 2; break;
                                case LogLevel.End: logIcon = 2; break;
                                case LogLevel.Stop: logIcon = 2; break;
                                case LogLevel.Exception: logIcon = 1; break;
                                case LogLevel.Closed: logIcon = 2; break;
                                case LogLevel.Connected: logIcon = 2; break;
                                default: logIcon = 3; break;
                            }

                            if (logIcon == 3) continue;
                            if (Log4NetLogger._logViewList != null)
                            {
                                foreach (ListView logView in Log4NetLogger._logViewList)
                                {
                                    if (logView.InvokeRequired)
                                    {
                                        logView.Invoke(new InsertToLogViewHandler(InsertToLogView), logView, log, logIcon);
                                    }
                                    else
                                        InsertToLogView(logView, log, logIcon);
                                }
                            }
                        }
                    }
                    catch (System.Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    System.Threading.Monitor.Exit(_tmrUiOut);
                }
            }
        }
        private delegate void InsertToLogViewHandler(ListView owner, LoggingEventArgs log, int logIcon);
        private static void InsertToLogView(ListView owner, LoggingEventArgs log, int logIcon)
        {
            owner.Items.Insert(0, new ListViewItem(new string[] { "", log.SignalTime.ToString("HH:mm:ss.fff"), log.LogMessage }, logIcon));
        }

        public void AppendLine(LogLevel lvl, string format, params object[] args)
        {
            string log = string.Format(format, args);
            AppendLine(lvl, log);
        }
        public void AppendLine(LogLevel lvl, string value)
        {
            //lock (Logger)
            {
                if (_logReporter != null)
                {
                    if (_logReporter.Count > 100)
                    {
                        _logReporter.Clear();
                        _logReporter.Add(new LoggingEventArgs() { Level = lvl, SignalTime = DateTime.Now, LogMessage = "SKIP...." });
                    }
                    _logReporter.Add(new LoggingEventArgs() { Level = lvl, SignalTime = DateTime.Now, LogMessage = value });
                }
                switch (lvl)
                {
                    case LogLevel.AllLog:       Log.Info(value); break;
                    case LogLevel.NoLog:        Log.Info(value); break;
                    case LogLevel.Info:         Log.Info(value); break;
                    case LogLevel.Warning:      Log.Warn(value); break;
                    case LogLevel.Error:        Log.Error(value); break;
                    case LogLevel.Fault:        Log.Fatal(value); break;
                    case LogLevel.Unuse:       Log.Info(value); break;
                    case LogLevel.End:          Log.Info(value); break;
                    case LogLevel.Stop:         Log.Info(value); break;
                    case LogLevel.Exception:    Log.Error(value); break;
                    case LogLevel.Closed:       Log.Info(value); break;
                    case LogLevel.Connected:    Log.Info(value); break;
                    default: Log.Warn(value); break;
                }
            }
        }

        private static void DeleteFilesAndDirectories(DirectoryInfo root, DirectoryInfo curPath, DateTime deadline)
        {
            foreach (FileInfo curFile in curPath.GetFiles())
            {
                if (curFile.LastWriteTime < deadline)
                {
                    try
                    {
                        curFile.Delete();
                    }
                    catch (IOException e)
                    {
                        Console.WriteLine(e);
                    }
                    catch (System.Security.SecurityException e)
                    {
                        Console.WriteLine(e);
                    }
                    catch (UnauthorizedAccessException e)
                    {
                        Console.WriteLine(e);
                    }
                }
            }

            if (curPath.GetDirectories().Length > 0)
            {
                foreach (DirectoryInfo subDir in curPath.GetDirectories())
                {
                    DeleteFilesAndDirectories(root, subDir, deadline);
                }
            }

            if (curPath.GetFiles().Length == 0 && curPath.GetDirectories().Length == 0)
            {
                if (curPath != root)
                {
                    try
                    {
                        curPath.Delete();
                    }
                    catch (IOException e)
                    {
                        Console.WriteLine(e);
                    }
                    catch (System.Security.SecurityException e)
                    {
                        Console.WriteLine(e);
                    }
                }
            }
        }
    }
}