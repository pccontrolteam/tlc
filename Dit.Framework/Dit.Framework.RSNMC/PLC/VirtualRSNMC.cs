﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.PLC;
using Dit.Framework.Comm;

namespace Dit.Framework.RSNMC.PLC
{
    /// <summary>
    /// RS Network Motor Contoller Memory처리
    /// date 180710
    /// since 180710
    /// </summary>
    public class VirtualRSNMC : IVirtualMem
    {
        private int ONE_BYTE_SIZE = 4;
        private const int MEM_SIZE = 102400;
        
        public int[] MC_INT = new int[MEM_SIZE];
        public double[] MC_REAL = new double[MEM_SIZE];
        public int[] CTRL_INT = new int[MEM_SIZE];
        public double[] CTRL_REAL = new double[MEM_SIZE];

        private RSNMCWorker _worker;

        public VirtualRSNMC(string name, RSNMCWorker worker)
        {
            this.Name = name;
            _worker = worker;
        }
        //메소드 연결
        public override int Open()
        {
            return _worker.Open();
        }
        public override int Close()
        {
            return _worker.Close();
        }

        //메소드 동기화
        public override int ReadFromPLC(PlcAddr addr, int wordSize)
        {
            try
            {
                if (addr.Type < PlcMemType.RS_CI || PlcMemType.RS_MR < addr.Type) return FALSE;

                object[] dest;
                _worker.ReadMemory(addr, out dest);

                Array arrValue = dest as Array;

                if (addr.Type == PlcMemType.RS_MI)
                    Array.Copy(arrValue, 0, MC_INT, addr.Addr, addr.Length);
                else if (addr.Type == PlcMemType.RS_MR)
                    Array.Copy(arrValue, 0, MC_REAL, addr.Addr, addr.Length);
                else if (addr.Type == PlcMemType.RS_CI)
                    Array.Copy(arrValue, 0, CTRL_INT, addr.Addr, addr.Length);
                else if (addr.Type == PlcMemType.RS_CR)
                    Array.Copy(arrValue, 0, CTRL_REAL, addr.Addr, addr.Length);
                else
                    return FALSE;

                return TRUE;
            }
            catch
            {
                return FALSE;
            }
        }
        public override int WriteToPLC(PlcAddr addr, int wordSize)
        {
            try
            {
                if (addr.Type == PlcMemType.RS_MI)
                {
                    int[] value = new int[addr.Length];
                    Array.Copy(MC_INT, addr.Addr, value, 0, addr.Length);
                    _worker.WriteMemory(addr, value);   
                    return TRUE;
                }
                else if (addr.Type == PlcMemType.RS_MR)
                {
                    double[] value = new double[addr.Length];
                    Array.Copy(MC_REAL, addr.Addr, value, 0, addr.Length);
                    _worker.WriteMemory(addr, value);
                    return TRUE;
                }
                else if (addr.Type == PlcMemType.RS_CI)
                {
                    int[] value = new int[addr.Length];
                    Array.Copy(CTRL_INT, addr.Addr, value, 0, addr.Length);
                    _worker.WriteMemory(addr, value);
                    return TRUE;
                }
                else if (addr.Type == PlcMemType.RS_CR)
                {
                    double[] value = new double[addr.Length];
                    Array.Copy(CTRL_REAL, addr.Addr, value, 0, addr.Length);
                    _worker.WriteMemory(addr, value);
                    return TRUE;
                }
                else
                {
                    return FALSE;
                }
            }
            catch
            {
                return FALSE;
            }

        }
        #region Access Memory        
        //메소드 비트
        public override bool GetBit(PlcAddr addr)
        {
            throw new Exception("미 구현");
        }
        public override void SetBit(PlcAddr addr)
        {
            throw new Exception("미 구현");
        }
        public override void ClearBit(PlcAddr addr)
        {
            throw new Exception("미 구현");
        }
        public override void SetBit(PlcAddr addr, bool value)
        {
            throw new Exception("미 구현");
        }
        public override void Toggle(PlcAddr addr)
        {
            throw new Exception("미 구현");
        }
        public bool[] GetBists(PlcAddr addr, int wordSize, out int result)
        {
            throw new Exception("미 구현");
            //return new bool[0];
        }
        //메소드 - STRING
        public override int SetAscii(PlcAddr addr, string text)
        {
            throw new Exception("미 구현");
            //return 0;
        }
        //메소드 - SHORT
        public override short GetShort(PlcAddr addr)
        {
            throw new Exception("미 구현");
            //return 0;
        }
        public override void SetShort(PlcAddr addr, short value)
        {
            throw new Exception("미 구현");
        }
        //메소드 - SHORT        
        public override short[] GetShorts(PlcAddr addr, int wordSize, out int result)
        {
            throw new Exception("미 구현");
        }
        public override void SetShorts(PlcAddr addr, short[] values, out int result)
        {
            throw new Exception("미 구현");
        }

        //메소드 - INT32
        public override int GetInt32(PlcAddr addr)
        {
            throw new Exception("미 구현");
        }
        public override void SetInt32(PlcAddr addr, int value)
        {
            throw new Exception("미 구현");
        }

        //읽어온 메모리에서 읽어오는 함수.
        public override bool VirGetBit(PlcAddr addr)
        {           
            return VirGetInt32(addr).GetBit(addr.Bit);
        }
        public override void VirSetBit(PlcAddr addr, bool value)
        {
            int vv = VirGetInt32(addr).SetBit(addr.Bit, value);
            VirSetInt32(addr, vv);
        }
        public override short VirGetShort(PlcAddr addr)
        {
            throw new Exception("미구현 메모리");
        }
        public override void VirSetShort(PlcAddr addr, short value)
        {
            throw new Exception("미구현 메모리");
        }

        public override string VirGetAscii(PlcAddr addr)
        {
            throw new Exception("미구현 메모리");
        }
        public override void VirSetAscii(PlcAddr addr, string value)
        {
            throw new Exception("미구현 메모리");
        }
        public override int VirGetInt32(PlcAddr addr)
        {
            if (addr.Type == PlcMemType.RS_MI)
            {
                return MC_INT[addr.Addr];
            }
            else if (addr.Type == PlcMemType.RS_CI)
            {
                return CTRL_INT[addr.Addr];
            }
            else
                throw new Exception("ADDR TYPE ERROR");
        }
        public override void VirSetInt32(PlcAddr addr, int value)
        {
            if (addr.Type == PlcMemType.RS_MI)
            {
                MC_INT[addr.Addr] = value;
            }
            else if (addr.Type == PlcMemType.RS_CI)
            {
                CTRL_INT[addr.Addr] = value;
            }
            else
                throw new Exception("ADDR TYPE ERROR");
        }

        public override bool[] VirGetBits(PlcAddr addr, int wordSize)
        {
            throw new Exception("미구현 메모리");
        }
        public override short[] VirGetShorts(PlcAddr addr)
        {
            throw new Exception("미구현 메모리");
        }
        public override float VirGetFloat(PlcAddr addr)
        {
            if (addr.Type == PlcMemType.RS_MR)
            {
                return (float)MC_REAL[addr.Addr];
            }
            else if (addr.Type == PlcMemType.RS_CR)
            {
                return (float)CTRL_REAL[addr.Addr];
            }
            else
                throw new Exception("ADDR TYPE ERROR");
        }
        public override void VirSetFloat(PlcAddr addr, float value)
        {
            if (addr.Type == PlcMemType.RS_MR)
            {
                MC_REAL[addr.Addr] = value;
            }
            else if (addr.Type == PlcMemType.RS_CR)
            {
                CTRL_REAL[addr.Addr] = value;
            }
            else
                throw new Exception("ADDR TYPE ERROR");
        }
        #endregion
    }
}
