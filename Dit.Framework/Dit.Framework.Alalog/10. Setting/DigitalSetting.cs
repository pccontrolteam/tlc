﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Dit.Framework.Ini;

namespace Dit.Framework.Alalog
{
    public class DigitalSetting : BaseSetting
    {
        public string PATH_SETTING { get; set; }
        public static string PATH_SETTING_T1 = Path.Combine(Application.StartupPath, "Setting", "PcDASetting_T1.ini");
        public static string PATH_SETTING_T2 = Path.Combine(Application.StartupPath, "Setting", "PcDASetting_T2.ini");
        public static string PATH_SETTING_T3 = Path.Combine(Application.StartupPath, "Setting", "PcDASetting_T3.ini");
        public static string PATH_SETTING_T4 = Path.Combine(Application.StartupPath, "Setting", "PcDASetting_T4.ini");
        public static string PATH_SETTING_T5 = Path.Combine(Application.StartupPath, "Setting", "PcDASetting_T5.ini");
        public static string PATH_SETTING_T6 = Path.Combine(Application.StartupPath, "Setting", "PcDASetting_T6.ini");
        public static string PATH_SETTING_T7 = Path.Combine(Application.StartupPath, "Setting", "PcDASetting_T7.ini");
        public static string PATH_SETTING_T8 = Path.Combine(Application.StartupPath, "Setting", "PcDASetting_T8.ini");
        public static string PATH_SETTING_T9 = Path.Combine(Application.StartupPath, "Setting", "PcDASetting_T9.ini");
        public static string PATH_SETTING_T10 = Path.Combine(Application.StartupPath, "Setting", "PcDASetting_T10.ini");
        public static string PATH_SETTING_T11 = Path.Combine(Application.StartupPath, "Setting", "PcDASetting_T11.ini");
        public static string PATH_SETTING_T12 = Path.Combine(Application.StartupPath, "Setting", "PcDASetting_T12.ini");
        public static string PATH_SETTING_T13 = Path.Combine(Application.StartupPath, "Setting", "PcDASetting_T13.ini");
        public static string PATH_SETTING_T14 = Path.Combine(Application.StartupPath, "Setting", "PcDASetting_T14.ini");
        public static string PATH_SETTING_T15 = Path.Combine(Application.StartupPath, "Setting", "PcDASetting_T15.ini");

        public const float Soft_Maximum_Limit_DA = 0.9f;
        public const float Soft_Minimum_Limit_DA = 0.000f;

        [IniAttribute("Setting", "DA1_CH1_WRITE_DATA", 0.0f)]
        public float DA1_CH1_WriteData { get; set; }
        [IniAttribute("Setting", "DA1_CH2_WRITE_DATA", 0.0f)]
        public float DA1_CH2_WriteData { get; set; }
        [IniAttribute("Setting", "DA1_CH3_WRITE_DATA", 0.0f)]
        public float DA1_CH3_WriteData { get; set; }
        [IniAttribute("Setting", "DA1_CH4_WRITE_DATA", 0.0f)]
        public float DA1_CH4_WriteData { get; set; }
        [IniAttribute("Setting", "DA1_CH5_WRITE_DATA", 0.0f)]
        public float DA1_CH5_WriteData { get; set; }
        [IniAttribute("Setting", "DA1_CH6_WRITE_DATA", 0.0f)]
        public float DA1_CH6_WriteData { get; set; }
        [IniAttribute("Setting", "DA1_CH7_WRITE_DATA", 0.0f)]
        public float DA1_CH7_WriteData { get; set; }
        [IniAttribute("Setting", "DA1_CH8_WRITE_DATA", 0.0f)]
        public float DA1_CH8_WriteData { get; set; }


        [IniAttribute("Setting", "DA1_CH1_Offset", 0.0f)]
        public float DA1_CH1_Offset { get; set; }
        [IniAttribute("Setting", "DA1_CH2_Offset", 0.0f)]
        public float DA1_CH2_Offset { get; set; }
        [IniAttribute("Setting", "DA1_CH3_Offset", 0.0f)]
        public float DA1_CH3_Offset { get; set; }
        [IniAttribute("Setting", "DA1_CH4_Offset", 0.0f)]
        public float DA1_CH4_Offset { get; set; }
        [IniAttribute("Setting", "DA1_CH5_Offset", 0.0f)]
        public float DA1_CH5_Offset { get; set; }
        [IniAttribute("Setting", "DA1_CH6_Offset", 0.0f)]
        public float DA1_CH6_Offset { get; set; }
        [IniAttribute("Setting", "DA1_CH7_Offset", 0.0f)]
        public float DA1_CH7_Offset { get; set; }
        [IniAttribute("Setting", "DA1_CH8_Offset", 0.0f)]
        public float DA1_CH8_Offset { get; set; }

        //Value -> 전압/전류 변환 비율
        [IniAttribute("Setting", "CH1_Ratio", 0.0f)]
        public float CH1_Ratio { get; set; }
        [IniAttribute("Setting", "CH2_Ratio", 0.0f)]
        public float CH2_Ratio { get; set; }
        [IniAttribute("Setting", "CH3_Ratio", 0.0f)]
        public float CH3_Ratio { get; set; }
        [IniAttribute("Setting", "CH4_Ratio", 0.0f)]
        public float CH4_Ratio { get; set; }
        [IniAttribute("Setting", "CH5_Ratio", 0.0f)]
        public float CH5_Ratio { get; set; }
        [IniAttribute("Setting", "CH6_Ratio1", 0.0f)]
        public float CH6_Ratio { get; set; }
        [IniAttribute("Setting", "CH7_Ratio", 0.0f)]
        public float CH7_Ratio { get; set; }
        [IniAttribute("Setting", "CH8_Ratio", 0.0f)]
        public float CH8_Ratio { get; set; }

        public DigitalSetting()
        {

        }

        /**
        *@param : path - null : 기본 경로
        *            not null : 경로 변경
        *               */
        public override bool Save(string path = null)
        {
            if (null != path)
                PATH_SETTING = path;
            if (!Directory.Exists(PATH_SETTING))
                Directory.CreateDirectory(PATH_SETTING.Remove(PATH_SETTING.LastIndexOf('\\')));

            return base.Save(PATH_SETTING);
        }
        /**
         *@param : path - null : 기본 경로
         *            not null : 경로 변경
         *               */
        public override bool Load(string path = null)
        {
            if (null != path)
                PATH_SETTING = path;
            return base.Load(PATH_SETTING);
        }
        public void SetOffset(int nIdx, float offset)
        {
            switch (nIdx)
            {
                case 0:
                    DA1_CH1_Offset = offset;
                    break;
                case 1:
                    DA1_CH2_Offset = offset;
                    break;
                case 2:
                    DA1_CH3_Offset = offset;
                    break;
                case 3:
                    DA1_CH4_Offset = offset;
                    break;
                case 4:
                    DA1_CH5_Offset = offset;
                    break;
                case 5:
                    DA1_CH6_Offset = offset;
                    break;
                case 6:
                    DA1_CH7_Offset = offset;
                    break;
                case 7:
                    DA1_CH8_Offset = offset;
                    break;
                default:
                    throw new Exception("미저정 코드");
            }
        }
        public float GetOffset(int nIdx)
        {
            switch (nIdx)
            {
                case 0:
                    return DA1_CH1_Offset;
                case 1:
                    return DA1_CH2_Offset;
                case 2:
                    return DA1_CH3_Offset;
                case 3:
                    return DA1_CH4_Offset;
                case 4:
                    return DA1_CH5_Offset;
                case 5:
                    return DA1_CH6_Offset;
                case 6:
                    return DA1_CH7_Offset;
                case 7:
                    return DA1_CH8_Offset;
                default:
                    throw new Exception("Unspecified code");
            }
        }
        public void SetRatio(int nIdx, float ratio)
        {
            switch (nIdx)
            {
                case 0:
                    CH1_Ratio = ratio;
                    break;
                case 1:
                    CH2_Ratio = ratio;
                    break;
                case 2:
                    CH3_Ratio = ratio;
                    break;
                case 3:
                    CH4_Ratio = ratio;
                    break;
                case 4:
                    CH5_Ratio = ratio;
                    break;
                case 5:
                    CH6_Ratio = ratio;
                    break;
                case 6:
                    CH7_Ratio = ratio;
                    break;
                case 7:
                    CH8_Ratio = ratio;
                    break;
                default:
                    throw new Exception("Unspecified code");
            }
        }
        public float GetRatio(int nIdx)
        {
            switch (nIdx)
            {
                case 0:
                    return CH1_Ratio;
                case 1:
                    return CH2_Ratio;
                case 2:
                    return CH3_Ratio;
                case 3:
                    return CH4_Ratio;
                case 4:
                    return CH5_Ratio;
                case 5:
                    return CH6_Ratio;
                case 6:
                    return CH7_Ratio;
                case 7:
                    return CH8_Ratio;
                default:
                    throw new Exception("Unspecified code");
            }
        }
        public void SetDAValue(int index, float writeData)
        {
            float data = CheckDALimitValue(writeData);

            switch (index)
            {
                case 0:
                    DA1_CH1_WriteData = data;
                    break;
                case 1:
                    DA1_CH2_WriteData = data;
                    break;
                case 2:
                    DA1_CH3_WriteData = data;
                    break;
                case 3:
                    DA1_CH4_WriteData = data;
                    break;
                case 4:
                    DA1_CH5_WriteData = data;
                    break;
                case 5:
                    DA1_CH6_WriteData = data;
                    break;
                case 6:
                    DA1_CH7_WriteData = data;
                    break;
                case 7:
                    DA1_CH8_WriteData = data;
                    break;
                default:
                    throw new Exception("Unspecified code");
            }

            Save();
        }
        public float CheckDALimitValue(float writeData)
        {
            writeData = (writeData < Soft_Minimum_Limit_DA) ? Soft_Minimum_Limit_DA : writeData;
            writeData = (writeData > Soft_Maximum_Limit_DA) ? Soft_Maximum_Limit_DA : writeData;
            return writeData;
        }
    }
}