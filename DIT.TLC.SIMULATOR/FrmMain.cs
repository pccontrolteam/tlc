﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Dit.Framework.PLC;
using Dit.Framework.Comm;
using System.Threading;
using Dit.Framework.Log;
using System.IO;

namespace DIT.TLC.SIMULATOR
{
    public partial class FrmMain : Form
    {
        private bool _isRunning = false;
        private EquipSimul _equip = new EquipSimul();
        private BackgroundWorker _woker = new BackgroundWorker();

        public FrmMain()
        {
            InitializeComponent();

            _woker.DoWork += Woker_DoWork;
            _woker.ProgressChanged += Woker_ProgressChanged;

        }
        private void Woker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
        }
        private void Woker_DoWork(object sender, DoWorkEventArgs e)
        {
            while (_isRunning)
            {

                UpdateUI();

                _equip.Working();

                Thread.Sleep(100);
            }
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            Logger.Log = new SimpleFileLoggerMark5(Path.Combine(Application.StartupPath, "Log"), "CLIENT", 500, 1024 * 1024 * 20, lstLogger);


            GG.CCLINK = (IVirtualMem)new VirtualMem("CCLINK");
            GG.MEM_INSP = (IVirtualMem)new VirtualMem("PC_CTRL_MEM") { PLC_MEM_SIZE = 2000 };
            GG.PMAC = (IVirtualMem)new VirtualMem("PMAC");
            GG.HSMS = (IVirtualMem)new VirtualMem("HSMS_MEM.S");

            GG.CCLINK.Open();
            GG.MEM_INSP.Open();
            GG.PMAC.Open();
            GG.HSMS.Open();

            AddressMgr.Load(GG.ADDRESS_PMAC, GG.PMAC);
            _equip.SetAddress();

            _isRunning = true;
            _woker.RunWorkerAsync();

            ServoSimulUmac[] motors = _equip.GetServoMotors();
            for (int jPos = 0; jPos < motors.Length; jPos++)
            {
                UcrlServo ucrlServo = new UcrlServo();
                ucrlServo.Servo = motors[jPos];
                flpnlServo.Controls.Add(ucrlServo);
            }
        }


        private void UpdateUI()
        {


            //this.Loader.X1 = (double)_equip.LoadUnit.X1Axis.XF_CurrMotorPosition.vFloat;
            //this.Loader.X2 = (double)_equip.LoadUnit.X2Axis.XF_CurrMotorPosition.vFloat;
            //this.Loader.Y1 = (double)_equip.LoadUnit.Y1Axis.XF_CurrMotorPosition.vFloat;
            //this.Loader.Y2 = (double)_equip.LoadUnit.Y2Axis.XF_CurrMotorPosition.vFloat;

            //this.TopLoaderTransfer.Y = (double)_equip.TopLoaderTransferUnit.YAxis.XF_CurrMotorPosition.vFloat;
            //this.TopLoaderTransfer.X = (double)_equip.TopLoaderTransferUnit.XAxis.XF_CurrMotorPosition.vFloat;
            //this.TopLoaderTransfer.Z = (double)_equip.TopLoaderTransferUnit.ZAxis.Position;

            //this.BotLoaderTransfer.Y = (double)_equip.BotLoaderTransferUnit.YAxis.XF_CurrMotorPosition.vFloat;
            //this.BotLoaderTransfer.X = (double)_equip.BotLoaderTransferUnit.XAxis.XF_CurrMotorPosition.vFloat;
            //this.BotLoaderTransfer.Z = (double)_equip.BotLoaderTransferUnit.ZAxis.Position;

            //this.TopProcessStage.Y = (double)_equip.TopProcessStageUnit.YAxis.XF_CurrMotorPosition.vFloat;
            //this.BotProcessStage.Y = (double)_equip.BotProcessStageUnit.YAxis.XF_CurrMotorPosition.vFloat;

            //this.TopBreakTransfer.Y = (double)_equip.TopBreakTransferUnit.YAxis.XF_CurrMotorPosition.vFloat;
            //this.TopBreakTransfer.X = (double)_equip.TopBreakTransferUnit.XAxis.XF_CurrMotorPosition.vFloat;
            //this.TopBreakTransfer.Z = (double)_equip.TopBreakTransferUnit.ZAxis.CurrPosition;

            //this.BotBreakTransfer.Y = (double)_equip.BotBreakTransferUnit.YAxis.XF_CurrMotorPosition.vFloat;
            //this.BotBreakTransfer.X = (double)_equip.BotBreakTransferUnit.XAxis.XF_CurrMotorPosition.vFloat;
            //this.BotBreakTransfer.Z = (double)_equip.BotBreakTransferUnit.ZAxis.CurrPosition;

            //this.TopBreakState.Y = (double)_equip.TopBreakStageUnit.YAxis.XF_CurrMotorPosition.vFloat;
            //this.TopBreakState.X = (double)_equip.TopBreakStageUnit.XAxisHeader.XF_CurrMotorPosition.vFloat;
            //this.TopBreakState.Z = (double)_equip.TopBreakStageUnit.ZAxisHeader.XF_CurrMotorPosition.vFloat;

            //this.BotBreakState.Y = (double)_equip.BotBreakStageUnit.YAxis.XF_CurrMotorPosition.vFloat;
            //this.BotBreakState.X = (double)_equip.BotBreakStageUnit.XAxisHeader.XF_CurrMotorPosition.vFloat;
            //this.BotBreakState.Z = (double)_equip.BotBreakStageUnit.ZAxisHeader.XF_CurrMotorPosition.vFloat;

            //this.TopUnloaderTransfer.Y = (double)_equip.TopUnLoaderTransferUnit.YAxis.XF_CurrMotorPosition.vFloat;
            //this.TopUnloaderTransfer.X = (double)_equip.TopUnLoaderTransferUnit.XAxis.XF_CurrMotorPosition.vFloat;
            //this.TopUnloaderTransfer.Z = (double)_equip.TopUnLoaderTransferUnit.ZAxis.CurrPosition;

            //this.BotUnloaderTransfer.Y = (double)_equip.BotUnLoaderTransferUnit.YAxis.XF_CurrMotorPosition.vFloat;
            //this.BotUnloaderTransfer.X = (double)_equip.BotUnLoaderTransferUnit.XAxis.XF_CurrMotorPosition.vFloat;
            //this.BotUnloaderTransfer.Z = (double)_equip.BotUnLoaderTransferUnit.ZAxis.CurrPosition;


            //this.Unloader.X1 = (double)_equip.UnloadUnit.X1Axis.XF_CurrMotorPosition.vFloat;
            //this.Unloader.X2 = (double)_equip.UnloadUnit.X2Axis.XF_CurrMotorPosition.vFloat;
            //this.Unloader.Y1 = (double)_equip.UnloadUnit.Y1Axis.XF_CurrMotorPosition.vFloat;
            //this.Unloader.Y2 = (double)_equip.UnloadUnit.Y2Axis.XF_CurrMotorPosition.vFloat;
        }

        private void tmrWorker_Tick(object sender, EventArgs e)
        {
            foreach (var ctrl in flpnlServo.Controls)
            {
                UcrlServo ucrlServo = ctrl as UcrlServo;
                if (ucrlServo != null)
                    ucrlServo.Update();
            }
        }
    }
}
