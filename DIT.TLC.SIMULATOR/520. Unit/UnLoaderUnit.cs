﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.SIMULATOR
{
    public class UnLoaderUnit : BaseUnit
    {
        public ServoSimulUmac X1Axis = null;
        public ServoSimulUmac X2Axis = null;
        public ServoSimulUmac Y1Axis= null;
        public ServoSimulUmac Y2Axis = null;        

        public override void LogicWorking(EquipSimul equip)
        {
            X1Axis.LogicWorking(equip);
            X2Axis.LogicWorking(equip);
            Y1Axis.LogicWorking(equip);
            Y2Axis.LogicWorking(equip);            
        }
    }
}
