﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.SIMULATOR
{
    public class CstLoaderUint : BaseUnit
    {
        public CassetteInfo InCst { get; set; }
        public CassetteInfo OutEmptyCst { get; set; }

        public CylinderSimul TiltCylinder { get; set; }
        //MUTING
        public SwitchSimul Muting { get; set; }

        //LIFT
        //TILT 센서
        public SwitchSimul Tilt { get; set; }


        public override void LogicWorking(EquipSimul equip)
        {


        }
         
    }
}
