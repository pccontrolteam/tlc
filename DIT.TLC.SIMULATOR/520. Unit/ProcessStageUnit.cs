﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.SIMULATOR
{
    public class ProcessStageUnit : BaseUnit
    {
        public ServoSimulUmac YAxis { get; set; }

        public ProcessStageUnit()
        {            
        }

        public override void LogicWorking(EquipSimul equip)
        {
            YAxis.LogicWorking(equip);
        }
    }
}
