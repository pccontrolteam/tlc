﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.SIMULATOR
{
    public class LoaderUnit : BaseUnit
    {
        //액추에이터
        //센서
        public ServoSimulUmac Y1Axis { get; set; }
        public ServoSimulUmac Y2Axis { get; set; }
        public ServoSimulUmac X1Axis { get; set; }
        public ServoSimulUmac X2Axis { get; set; }
        

        public SwitchSimul Vaccum { get; set; }
        public SwitchSimul Blower { get; set; }


        public LoaderUnit()
        {
            

        }        
        public override void LogicWorking(EquipSimul equip)
        {
            Y1Axis.LogicWorking(equip);
            Y2Axis.LogicWorking(equip);
            X1Axis.LogicWorking(equip);
            X2Axis.LogicWorking(equip);
        }       
    }
}
