﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.PLC;

namespace DIT.TLC.SIMULATOR
{
    public enum EmRecvPio
    {
        UP_000_WAIT,
        UP_010_RECV_START,
        UP_020,
        UP_030,
        UP_040,
        UP_050,
        UP_060,
    }
    public enum EmSendPio
    {
        DW_000_WAIT,
        DW_010_SEND_START,
        DW_020,
        DW_030,
        DW_040,
        DW_050,
    }


    public class BaseUnit
    {
        
        public virtual void LogicWorking(EquipSimul equip)
        {
        }
        
    }
}
