﻿using Dit.Framework.Comm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace DIT.TLC.SIMULATOR
{
    public class EquipSimul
    {
        public PMacSimul PMac = new PMacSimul();

        public CstLoaderUint TopCstLoaderUint = new CstLoaderUint();
        public CstLoaderUint BotCstLoaderUint = new CstLoaderUint();

        public LoaderUnit LoadUnit = new LoaderUnit();

        public LoaderTransferUnit TopLoaderTransferUnit = new LoaderTransferUnit();
        public LoaderTransferUnit BotLoaderTransferUnit = new LoaderTransferUnit();

        public ProcessStageUnit TopProcessStageUnit = new ProcessStageUnit();
        public ProcessStageUnit BotProcessStageUnit = new ProcessStageUnit();

        public BreakTransferUnit TopBreakTransferUnit = new BreakTransferUnit();
        public BreakTransferUnit BotBreakTransferUnit = new BreakTransferUnit();

        public BreakStageUnit TopBreakStageUnit = new BreakStageUnit();
        public BreakStageUnit BotBreakStageUnit = new BreakStageUnit();

        public UnLoaderTransferUnit TopUnLoaderTransferUnit = new UnLoaderTransferUnit();
        public UnLoaderTransferUnit BotUnLoaderTransferUnit = new UnLoaderTransferUnit();

        public UnLoaderUnit UnloadUnit = new UnLoaderUnit();

        public EquipSimul()
        {
        }



        public void Working()
        {

            try
            {
                TopCstLoaderUint.LogicWorking(this);
                BotCstLoaderUint.LogicWorking(this);

                LoadUnit.LogicWorking(this);

                TopLoaderTransferUnit.LogicWorking(this);
                BotLoaderTransferUnit.LogicWorking(this);

                TopProcessStageUnit.LogicWorking(this);
                BotProcessStageUnit.LogicWorking(this);

                TopBreakTransferUnit.LogicWorking(this);
                BotBreakTransferUnit.LogicWorking(this);

                TopBreakStageUnit.LogicWorking(this);
                BotBreakStageUnit.LogicWorking(this);

                TopUnLoaderTransferUnit.LogicWorking(this);
                BotUnLoaderTransferUnit.LogicWorking(this);

                UnloadUnit.LogicWorking(this);
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        public ServoSimulUmac[] GetServoMotors()
        {
            ServoSimulUmac[] motors = new ServoSimulUmac[]
            {
                LoadUnit.X1Axis, LoadUnit.X2Axis, LoadUnit.Y1Axis, LoadUnit.Y2Axis,

                TopLoaderTransferUnit.XAxis,TopLoaderTransferUnit.YAxis,
                BotLoaderTransferUnit.XAxis,BotLoaderTransferUnit.YAxis,

                TopProcessStageUnit.YAxis,
                BotProcessStageUnit.YAxis,

                TopBreakTransferUnit.XAxis,TopBreakTransferUnit.YAxis,
                BotBreakTransferUnit.XAxis,BotBreakTransferUnit.YAxis,

                TopBreakStageUnit.YAxis,TopBreakStageUnit.XAxisHeader, TopBreakStageUnit.ZAxisHeader,
                BotBreakStageUnit.YAxis,BotBreakStageUnit.XAxisHeader, BotBreakStageUnit.ZAxisHeader,

                TopUnLoaderTransferUnit.XAxis,TopUnLoaderTransferUnit.YAxis,
                BotUnLoaderTransferUnit.XAxis,BotUnLoaderTransferUnit.YAxis,

                UnloadUnit.X1Axis,UnloadUnit.X2Axis, UnloadUnit.Y1Axis, UnloadUnit.Y2Axis
            };
            return motors;
        }

        public void SetAddress()
        {

            LoadUnit.X1Axis                 /**/= new ServoSimulUmac(01, "LoadUnit.X1Axis              ", 1000, 100, 15);
            LoadUnit.X2Axis                 /**/= new ServoSimulUmac(02, "LoadUnit.X2Axis              ", 1000, 100, 15);
            LoadUnit.Y1Axis                 /**/= new ServoSimulUmac(03, "LoadUnit.Y1Axis             ", 1000, 100, 15);
            LoadUnit.Y2Axis                 /**/= new ServoSimulUmac(04, "LoadUnit.Y1Axis             ", 1000, 100, 15);

            TopLoaderTransferUnit.XAxis     /**/= new ServoSimulUmac(05, "TopLoaderTransferUnit.XAxis  ", 1000, 100, 15);
            TopLoaderTransferUnit.YAxis     /**/= new ServoSimulUmac(06, "TopLoaderTransferUnit.YAxis  ", 1000, 100, 15);
            BotLoaderTransferUnit.XAxis     /**/= new ServoSimulUmac(07, "BotLoaderTransferUnit.XAxis  ", 1000, 100, 15);
            BotLoaderTransferUnit.YAxis     /**/= new ServoSimulUmac(08, "BotLoaderTransferUnit.YAxis  ", 1000, 100, 15);
            TopProcessStageUnit.YAxis       /**/= new ServoSimulUmac(09, "TopProcessStageUnit.YAxis    ", 1000, 100, 15);
            BotProcessStageUnit.YAxis       /**/= new ServoSimulUmac(10, "BotProcessStageUnit.YAxis    ", 1000, 100, 15);
            TopBreakTransferUnit.XAxis      /**/= new ServoSimulUmac(11, "TopBreakTransferUnit.XAxis   ", 1000, 100, 15);
            TopBreakTransferUnit.YAxis      /**/= new ServoSimulUmac(12, "TopBreakTransferUnit.YAxis   ", 1000, 100, 15);
            BotBreakTransferUnit.XAxis      /**/= new ServoSimulUmac(13, "BotBreakTransferUnit.XAxis   ", 1000, 100, 15);
            BotBreakTransferUnit.YAxis      /**/= new ServoSimulUmac(14, "BotBreakTransferUnit.YAxis   ", 1000, 100, 15);
            TopBreakStageUnit.YAxis         /**/= new ServoSimulUmac(15, "TopBreakStageUnit.YAxis      ", 1000, 100, 15);
            TopBreakStageUnit.XAxisHeader   /**/= new ServoSimulUmac(16, "TopBreakStageUnit.XAxisHeader", 1000, 100, 15);
            TopBreakStageUnit.ZAxisHeader   /**/= new ServoSimulUmac(17, "TopBreakStageUnit.ZAxisHeader", 1000, 100, 15);
            BotBreakStageUnit.YAxis         /**/= new ServoSimulUmac(18, "BotBreakStageUnit.YAxis      ", 1000, 100, 15);
            BotBreakStageUnit.XAxisHeader   /**/= new ServoSimulUmac(19, "BotBreakStageUnit.XAxisHeader", 1000, 100, 15);
            BotBreakStageUnit.ZAxisHeader   /**/= new ServoSimulUmac(20, "BotBreakStageUnit.ZAxisHeader", 1000, 100, 15);
            TopUnLoaderTransferUnit.XAxis   /**/= new ServoSimulUmac(21, "TopUnLoaderTransferUnit.XAxis", 1000, 100, 15);
            TopUnLoaderTransferUnit.YAxis   /**/= new ServoSimulUmac(22, "TopUnLoaderTransferUnit.YAxis", 1000, 100, 15);
            BotUnLoaderTransferUnit.XAxis   /**/= new ServoSimulUmac(23, "BotUnLoaderTransferUnit.XAxis", 1000, 100, 15);
            BotUnLoaderTransferUnit.YAxis   /**/= new ServoSimulUmac(24, "BotUnLoaderTransferUnit.YAxis", 1000, 100, 15);
            UnloadUnit.X1Axis               /**/= new ServoSimulUmac(25, "UnloadUnit.X1Axis            ", 1000, 100, 15);
            UnloadUnit.X2Axis               /**/= new ServoSimulUmac(26, "UnloadUnit.X2Axis            ", 1000, 100, 15);
            UnloadUnit.Y1Axis               /**/= new ServoSimulUmac(27, "UnloadUnit.YAxis             ", 1000, 100, 15);
            UnloadUnit.Y2Axis               /**/= new ServoSimulUmac(28, "UnloadUnit.YAxis             ", 1000, 100, 15);



            PMac.YB_EquipMode                       /**/ = AddressMgr.GetAddress("PMAC_YB_CheckMode", 0);
            PMac.YB_CheckAlarmStatus                /**/ = AddressMgr.GetAddress("PMAC_YB_CheckAlarmStatus", 0);
            PMac.YB_UpperInterfaceWorking           /**/ = AddressMgr.GetAddress("PMAC_YB_UpperInterfaceWorking", 0);
            PMac.YB_LowerInterfaceWorking           /**/ = AddressMgr.GetAddress("PMAC_YB_LowerInterfaceWorking", 0);
            PMac.XB_PmacReady                       /**/ = AddressMgr.GetAddress("PMAC_XB_PmacReady", 0);
            PMac.XB_PmacAlive                       /**/ = AddressMgr.GetAddress("PMAC_XB_PmacAlive", 0);
            PMac.XB_PmacHeavyAlarm                  /**/ = AddressMgr.GetAddress("PMAC_XB_PmacMidAlarm", 0);
            PMac.YB_PinUpMotorInterlockDisable      /**/ = AddressMgr.GetAddress("PMAC_YB_PinUpMotorInterlock", 0);
            PMac.XB_PinUpMotorInterlockDiableAck    /**/ = AddressMgr.GetAddress("PMAC_XB_PinUpMotorInterlockAck", 0);
            PMac.YB_ReviewTimerOverCmd              /**/ = AddressMgr.GetAddress("PMAC_YB_ReviewTimerOverCmd", 0);
            PMac.XB_ReviewTimerOverCmdAck           /**/ = AddressMgr.GetAddress("PMAC_XB_ReviewTimerOverCmdAck", 0);
            PMac.YB_PmacResetCmd                    /**/ = AddressMgr.GetAddress("PMAC_YB_PmacResetCmd", 0);
            PMac.XB_PmacResetCmdAck                 /**/ = AddressMgr.GetAddress("PMAC_XB_PmacResetCmdAck", 0);

            #region PMAC
            ServoSimulUmac[] motors = GetServoMotors();


            PMac.YB_EquipMode                       /**/ = AddressMgr.GetAddress("PMAC_YB_EquipState", 0, 0);
            PMac.YB_CheckAlarmStatus                /**/ = AddressMgr.GetAddress("PMAC_YB_EquipState", 0, 1);
            PMac.YB_UpperInterfaceWorking           /**/ = AddressMgr.GetAddress("PMAC_YB_EquipState", 0, 2);
            PMac.YB_LowerInterfaceWorking           /**/ = AddressMgr.GetAddress("PMAC_YB_EquipState", 0, 3);

            PMac.XB_PmacReady                       /**/ = AddressMgr.GetAddress("PMAC_XB_PmacState", 0, 0);
            PMac.XB_PmacAlive                       /**/ = AddressMgr.GetAddress("PMAC_XB_PmacState", 0, 1);
            //PMac.XB_ReviewUsingPmac               /**/ = AddressMgr.GetAddress("PMAC_XB_PmacState", 0, 2);

            //PMac.XB_PmacHeavyAlarm                /**/ = AddressMgr.GetAddress("PMAC_XB_PmacMidAlarm", 0);
            //PMac.YB_PinUpMotorInterlockOffCmd     /**/ = AddressMgr.GetAddress("PMAC_YB_CommonCmd", 0, 0);
            //PMac.XB_PinUpMotorInterlockOffCmdAck  /**/ = AddressMgr.GetAddress("PMAC_XB_CommonCmdAck", 0, 0);
            PMac.YB_PmacResetCmd                    /**/ = AddressMgr.GetAddress("PMAC_YB_CommonCmd", 0, 1);
            PMac.XB_PmacResetCmdAck                 /**/ = AddressMgr.GetAddress("PMAC_XB_CommonCmdAck", 0, 1);
            //PMac.YB_ImmediateStopCmd              /**/ = AddressMgr.GetAddress("PMAC_YB_CommonCmd", 0, 2);
            //PMac.XB_ImmediateStopCmdAck           /**/ = AddressMgr.GetAddress("PMAC_XB_CommonCmdAck", 0, 2);
            PMac.YB_PmacValueSave                   /**/ = AddressMgr.GetAddress("PMAC_YB_CommonCmd", 0, 3);
            PMac.XB_PmacValueSaveAck                /**/ = AddressMgr.GetAddress("PMAC_XB_CommonCmdAck", 0, 3);
            PMac.YB_ReviewTimerOverCmd              /**/ = AddressMgr.GetAddress("PMAC_YB_CommonCmd", 0, 4);
            PMac.XB_ReviewTimerOverCmdAck           /**/ = AddressMgr.GetAddress("PMAC_XB_CommonCmdAck", 0, 4);

            //ScanX1.YF_TriggerStartPosi            /**/ = AddressMgr.GetAddress("TriggerStartPosCmd", 0);
            //ScanX1.XF_TriggerStartPosiAck         /**/ = AddressMgr.GetAddress("TriggerStartPosCmdAck", 0);
            //
            //ScanX1.YF_TriggerEndPosi              /**/ = AddressMgr.GetAddress("TriggerEndPosCmd", 0);
            //ScanX1.XF_TriggerEndPosiAck           /**/ = AddressMgr.GetAddress("TriggerEndPosCmdAck", 0);

            for (int jPos = 0; jPos < motors.Length; jPos++)
            {
                //string motor = motors[jPos].Name;
                //string slayerMotor = motors[jPos].SlayerName;

                int axis = motors[jPos].Axis - 1;
                string axisStr = (axis + 1).ToString("D2");
                int slayerAxis = motors[jPos].Axis - 1;
                string slayerAxisStr = (slayerAxis + 1).ToString("D2");

                motors[jPos].XB_StatusHomeCompleteBit                       /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_StatusHomeCompleteBit"), 0, axis);
                motors[jPos].XB_StatusHomeInPosition                        /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_StatusHomeInPosition"), 0, axis);
                motors[jPos].XB_StatusMotorMoving                           /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_StatusMotorMoving"), 0, axis);
                motors[jPos].XB_StatusMotorInPosition                       /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_StatusMotorInPosition"), 0, axis);
                motors[jPos].XB_StatusNegativeLimitSet                      /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_StatusNegativeLimitSet"), 0, axis);
                motors[jPos].XB_StatusPositiveLimitSet                      /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_StatusPositiveLimitSet"), 0, axis);
                motors[jPos].XB_ErrMotorServoOn                             /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_ErrMotorServoOn"), 0, axis);
                motors[jPos].XB_ErrFatalFollowingError                      /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_ErrFatalFollowingError"), 0, axis);
                motors[jPos].XB_ErrAmpFaultError                            /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_ErrAmpFaultError"), 0, axis);
                motors[jPos].XB_ErrI2TAmpFaultError                         /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_ErrI2TAmpFaultError"), 0, axis);

                motors[jPos].XF_CurrMotorPosition                           /**/  = AddressMgr.GetAddress(string.Format("Axis{0}_XF_CurrentMotorPosition", axisStr), 0);
                motors[jPos].XF_CurrMotorSpeed                              /**/  = AddressMgr.GetAddress(string.Format("Axis{0}_XF_CurrentMotorSpeed", axisStr), 0);
                //motors[jPos].XI_CurrMotorStress                             /**/  = AddressMgr.GetAddress(string.Format("Axis{0}_XI_CurrentMotorStress", axisStr), 0);

                //motors[jPos].XB_StatusHomeCompleteBitSlayer                 /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_StatusHomeCompleteBit"), 0, slayerAxis);
                //motors[jPos].XB_StatusHomeInPositionSlayer                  /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_StatusHomeInPosition"), 0, slayerAxis);
                //motors[jPos].XB_StatusMotorMovingSlayer                     /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_StatusMotorMoving"), 0, slayerAxis);
                //motors[jPos].XB_StatusMotorInPositionSlayer                 /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_StatusMotorInPosition"), 0, slayerAxis);
                //motors[jPos].XB_StatusNegativeLimitSetSlayer                /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_StatusNegativeLimitSet"), 0, slayerAxis);
                //motors[jPos].XB_StatusPositiveLimitSetSlayer                /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_StatusPositiveLimitSet"), 0, slayerAxis);
                //motors[jPos].XB_StatusMotorServoOnSlayer                    /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_ErrMotorServoOn"), 0, slayerAxis);
                //motors[jPos].XB_ErrFatalFollowingErrorSlayer                /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_ErrFatalFollowingError"), 0, slayerAxis);
                //motors[jPos].XB_ErrAmpFaultErrorSlayer                      /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_ErrAmpFaultError"), 0, slayerAxis);
                //motors[jPos].XB_ErrI2TAmpFaultErrorSlayer                   /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_ErrI2TAmpFaultError"), 0, slayerAxis);
                //
                //motors[jPos].XF_CurrMotorPositionSlayer                     /**/  = AddressMgr.GetAddress(string.Format("Axis{0}_XF_CurrentMotorPosition", slayerAxisStr), 0);
                //motors[jPos].XF_CurrMotorSpeedSlayer                        /**/  = AddressMgr.GetAddress(string.Format("Axis{0}_XF_CurrentMotorSpeed", slayerAxisStr), 0);
                //motors[jPos].XI_CurrMotorStressSlayer                       /**/  = AddressMgr.GetAddress(string.Format("Axis{0}_XI_CurrentMotorStress", slayerAxisStr), 0);

                motors[jPos].YB_HomeCmd                                     /**/  = AddressMgr.GetAddress(string.Format("PMAC_YB_HomeCmd"), 0, axis);
                motors[jPos].XB_HomeCmdAck                                  /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_HomeCmdAck"), 0, axis);
                motors[jPos].YB_MotorJogMinus                               /**/  = AddressMgr.GetAddress(string.Format("PMAC_YB_MinusJogCmd"), 0, axis);
                motors[jPos].YB_MotorJogPlus                                /**/  = AddressMgr.GetAddress(string.Format("PMAC_YB_PlusJogCmd"), 0, axis);
                motors[jPos].YF_MotorJogSpeedCmd                            /**/  = AddressMgr.GetAddress(string.Format("Axis{0}_YB_JogSpeed", axisStr), 0);
                //motors[jPos].XI_CmdAckLogMsg                                /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_CmdAckLogMsg"), 0, axis);

                for (int iPos = 1; iPos <= motors[jPos].PositionCount; iPos++)
                {
                    motors[jPos].YB_Position0MoveCmd[iPos - 1]        /**/= AddressMgr.GetAddress(string.Format("Axis{0}_YB_PositionMoveCmd", axisStr), 0, iPos - 1);
                    motors[jPos].XB_Position0MoveCmdAck[iPos - 1]     /**/= AddressMgr.GetAddress(string.Format("Axis{0}_XB_PositionMoveCmdAck", axisStr), 0, iPos - 1);

                    motors[jPos].XB_Position0Complete[iPos - 1]       /**/= AddressMgr.GetAddress(string.Format("Axis{0}_XB_PositionMoveComplete", axisStr), 0, iPos - 1);

                    motors[jPos].YF_Position0Posi[iPos - 1]          /**/= AddressMgr.GetAddress(string.Format("Axis{0}_YF_Position{1}Point", axisStr, iPos.ToString("D2")), 0);
                    motors[jPos].XF_Position0PosiAck[iPos - 1]       /**/= AddressMgr.GetAddress(string.Format("Axis{0}_XF_Position{1}PointAck", axisStr, iPos.ToString("D2")), 0);

                    //motors[jPos].YF_Position0Speed[iPos - 1]          /**/= AddressMgr.GetAddress(string.Format("Axis{0}_YF_Position{1}Speed", axisStr, iPos.ToString("D2")), 0);
                    //motors[jPos].XF_Position0SpeedAck[iPos - 1]       /**/= AddressMgr.GetAddress(string.Format("Axis{0}_XF_Position{1}SpeedAck", axisStr, iPos.ToString("D2")), 0);
                    //
                    //motors[jPos].YF_Position0Accel[iPos - 1]          /**/= AddressMgr.GetAddress(string.Format("Axis{0}_Yf_Position{1}Accel", axisStr, iPos.ToString("D2")), 0);
                    //motors[jPos].XF_Position0AccelAck[iPos - 1]       /**/= AddressMgr.GetAddress(string.Format("Axis{0}_Xf_Position{1}AccelAck", axisStr, iPos.ToString("D2")), 0);
                }
            }
            #endregion

        }
    }
}
