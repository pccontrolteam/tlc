﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Timers;
using Dit.Framework.Log;
using Dit.Framework.PLC;

namespace DIT.TLC.SIMULATOR
{
    public class ServoSimulUmac
    {
        public VirtualMem PLC;
        public string Name;
        public int Axis;

        public PlcAddr XB_StatusHomeCompleteBit { get; set; }
        public PlcAddr XB_StatusHomeInPosition { get; set; }
        public PlcAddr XB_StatusMotorMoving { get; set; }
        public PlcAddr XB_StatusMotorInPosition { get; set; }
        public PlcAddr XB_StatusNegativeLimitSet { get; set; }
        public PlcAddr XB_StatusPositiveLimitSet { get; set; }
        public PlcAddr XB_ErrMotorServoOn { get; set; }
        public PlcAddr XB_ErrFatalFollowingError { get; set; }
        public PlcAddr XB_ErrAmpFaultError { get; set; }
        public PlcAddr XB_ErrI2TAmpFaultError { get; set; }

        public PlcAddr XF_CurrMotorPosition { get; set; }
        public PlcAddr XF_CurrMotorSpeed { get; set; }
        public PlcAddr XF_CurrMotorStress { get; set; }

        public PlcAddr YB_HomeCmd { get; set; }
        public PlcAddr XB_HomeCmdAck { get; set; }

        public PlcAddr YB_MotorJogMinus { get; set; }
        public PlcAddr YB_MotorJogPlus { get; set; }
        public PlcAddr YF_MotorJogSpeedCmd { get; set; }
        
        public PlcAddr YB_PmacValueSave { get; set; }
        public PlcAddr YB_PmacValueSaveAck { get; set; }

        public PlcAddr[] YB_Position0MoveCmd { get; set; }
        public PlcAddr[] XB_Position0MoveCmdAck { get; set; }
        public PlcAddr[] YF_Position0Posi { get; set; }
        public PlcAddr[] XF_Position0PosiAck { get; set; }
        public PlcAddr[] XB_Position0Complete { get; set; }
        public PlcTimer[] _moveDelay = new PlcTimer[] { new PlcTimer() };
        public PlcTimer _homeDelay = new PlcTimer();
        public PlcTimer _startPosiDelay = new PlcTimer();
        public Func<bool> InterLockFunc { get; set; }

        public int[] _moveStep = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        public int _homeStep = 0;
        public int _startPosiStep = 0;
        public int TotalLength { get; set; }

        protected bool _moving = false;
        protected bool _moveComplete { get; set; }
        protected bool _isMinuseGo = false;
        protected int _jogGo { get; set; }
        protected int _stepSetting = 0;
        protected double _inTargetPosi { get; set; }
        protected double _inSpeed { get; set; }

        public bool OutHomeAck { get; set; }

        public bool _isHomeCompleteBit = false;
        public bool NotReply { get; set; }

        public int PositionCount { get; set; }

        

        public ServoSimulUmac(int axis, string name, int length, int speed, int posiCount = 7)
        {
            Axis = axis;
            PositionCount = posiCount;

            _moveStep = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 , 0};
            YB_Position0MoveCmd = new PlcAddr[PositionCount];
            XB_Position0MoveCmdAck = new PlcAddr[PositionCount];
            YF_Position0Posi = new PlcAddr[PositionCount];
            XF_Position0PosiAck = new PlcAddr[PositionCount];
            XB_Position0Complete = new PlcAddr[PositionCount];

            Name = name;
            TotalLength = length;

            _moveDelay = new PlcTimer[PositionCount];
            for (int iPos = 0; iPos < PositionCount; iPos++)
                _moveDelay[iPos] = new PlcTimer();

            _inSpeed = speed;
        }
        public void Initialize()
        {
            //XF_CurrMotorPosition.vFloat = 0;
            //XB_ErrMotorServoOn.vBit = false;

            for (int iPos = 0; iPos < PositionCount; iPos++)
                YF_Position0Posi[iPos].vFloat = 100 * iPos;
        }
        public void HomeOn()
        {
            XB_ErrMotorServoOn.vBit = true;
            XB_StatusHomeCompleteBit.vBit = true;
        }
        public virtual void LogicWorking(EquipSimul equip)
        {
            MoveWorking();
            Working();
            SettingWorking(equip);
            //UpdatePositionState();
        }
        private void UpdatePositionState()
        {
            for (int iPos = 0; iPos < PositionCount; iPos++)
            {
                if (YF_Position0Posi[iPos].vFloat == XF_CurrMotorPosition.vFloat && XB_StatusHomeCompleteBit.vBit == true)
                {
                    XB_Position0Complete[iPos].vBit = true;
                }
                else
                    XB_Position0Complete[iPos].vBit = false;

            }
        }
        public virtual void SettingWorking(EquipSimul equip)
        {
            if (_stepSetting == 0)
            {
                if (equip.PMac.YB_PmacValueSave.vBit == true)
                    _stepSetting = 10;
            }
            else if (_stepSetting == 10)
            {
                for (int iPos = 0; iPos < PositionCount; iPos++)
                    XF_Position0PosiAck[iPos].vFloat = YF_Position0Posi[iPos].vFloat;

                equip.PMac.XB_PmacValueSaveAck.vBit = true;
                _stepSetting = 20;
            }
            else if (_stepSetting == 20)
            {
                if (equip.PMac.YB_PmacValueSave.vBit == false)
                {
                    equip.PMac.XB_PmacValueSaveAck.vBit = false;
                    _stepSetting = 0;
                }
            }
        }
        private void MoveWorking()
        {
            //Default 
            XB_ErrMotorServoOn.vBit = true;

            PositionWorking(20, ref _homeStep, _homeDelay, YB_HomeCmd, XB_HomeCmdAck, XB_StatusHomeCompleteBit, 10, _inSpeed, "HOME");

            for (int iPos = 0; iPos < PositionCount; iPos++)
            {
                PositionWorking(iPos, ref _moveStep[iPos], _moveDelay[iPos], YB_Position0MoveCmd[iPos], XB_Position0MoveCmdAck[iPos], XB_Position0Complete[iPos], YF_Position0Posi[iPos].vFloat, _inSpeed, string.Format("POSI NO = {0}", iPos));
                if (_moveStep[iPos] != 0)
                    return;
            }
        }
        public virtual void PositionWorking(int p, ref int stepMove, PlcTimer moveDelay, PlcAddr yb_Position0MoveCmd, PlcAddr xb_Position0MoveCmdAck, PlcAddr xb_Position0Complete, double posi, double speed, string desc)
        {
            if (stepMove == 0)
            {
                if (yb_Position0MoveCmd.vBit == true)
                {

                    //XB_StatusHomeInPosition.vBit = false;
                    xb_Position0MoveCmdAck.vBit = true;
                    XB_StatusMotorMoving.vBit = false;
                    XB_StatusMotorInPosition.vBit = false;
                    xb_Position0Complete.vBit = false;
                    stepMove = 10;
                }
            }
            else if (stepMove == 10)
            {
                if (yb_Position0MoveCmd.vBit == false)
                {
                    for (int iPos = 0; iPos < PositionCount; iPos++)
                        XB_Position0Complete[iPos].vBit = false;

                    xb_Position0MoveCmdAck.vBit = false;

                    _inTargetPosi = posi;
                    _inSpeed = speed;

                    Logger.Log.AppendLine(LogLevel.Info, "{0} 모터 {1} 이동 시퀀스 시작 {2} => {3}", Name, desc, XF_CurrMotorSpeed.vFloat, posi);

                    stepMove = 20;
                }
            }
            else if (stepMove == 20)
            {
                //기본. 
                XB_StatusMotorMoving.vBit = true;

                Logger.Log.AppendLine(LogLevel.Info, "{0} 모터 {1}  이동 시작", Name, desc);

                //속도 및 셋팅
                _isMinuseGo = _inTargetPosi < XF_CurrMotorPosition.vFloat;
                _moveComplete = false;
                _moving = true;
                stepMove = 30;
            }
            else if (stepMove == 30)
            {
                moveDelay.Start(1);
                stepMove = 40;
            }
            else if (stepMove == 40)
            {
                if (moveDelay)
                {
                    moveDelay.Stop();
                    stepMove = 50;
                }
            }
            else if (stepMove == 50)
            {
                if (_moveComplete && NotReply == false)
                {
                    if (p >= 0 && p < PositionCount)
                    {
                        for (int iPos = 0; iPos < PositionCount; iPos++)
                            if (YF_Position0Posi[iPos].vFloat == YF_Position0Posi[p].vFloat) XB_Position0Complete[iPos].vBit = XB_Position0Complete[p].vBit;

                        XF_CurrMotorPosition.vFloat = YF_Position0Posi[p].vFloat;
                    }
                    else
                        XF_CurrMotorPosition.vFloat = 0.0f;

                    stepMove = 60;
                }

                Logger.Log.AppendLine(LogLevel.Info, "{0} 모터 {1} 이동 시퀀스 종료", Name, desc);
            }
            else if (stepMove == 60)
            {
                xb_Position0Complete.vBit = true;
                XB_StatusMotorInPosition.vBit = true;
                XB_StatusMotorMoving.vBit = false;

                if (p == 20)
                {
                    XB_StatusHomeCompleteBit.vBit = true;
                    XB_StatusHomeInPosition.vBit = true;
                    XB_Position0Complete[0].vBit = true;
                }
                else if (YF_Position0Posi[p].vFloat == 0)
                {
                    XB_StatusHomeInPosition.vBit = true;
                }
                UpdatePositionState();
                Logger.Log.AppendLine(LogLevel.Info, "{0} 모터 {1} 이동 완료", Name, desc);
                stepMove = 0;
            }
        }
        public virtual void ConvWorking(PlcAddr yb_MoveCmd, PlcAddr xb_MoveCmdAck, bool isForward)
        {
            if (yb_MoveCmd.vBit == true)
            {
                XB_StatusHomeInPosition.vBit = false;
                xb_MoveCmdAck.vBit = true;
                XB_StatusMotorMoving.vBit = true;

                int position = 0;
                position = Convert.ToInt32(isForward == false ? XF_CurrMotorPosition.vFloat - _inSpeed :
                                                               XF_CurrMotorPosition.vFloat + _inSpeed);


                //if (position < 0)
                //    position = 0;

                if (position > TotalLength)
                    position = 0;

                XF_CurrMotorPosition.vFloat = position;
            }
            else
            {
                xb_MoveCmdAck.vBit = false;
                
                for (int iPos = 0; iPos < PositionCount; iPos++)
                    if (YB_Position0MoveCmd[iPos].vBit == true) return;
                
                XB_StatusMotorMoving.vBit = false;
            }
        }
        public virtual void Working()
        {
            if (YB_MotorJogMinus.vBit == true)
            {
                //if (InJogPlus)
                //    Logger.Log.AppendLine(LogLevel.Info, "{0} 모터 PLUS JOG 이동 시작", Name);
                //else
                //    Logger.Log.AppendLine(LogLevel.Info, "{0} 모터 PLUS JOG 이동 종료", Name);

                _inSpeed = YF_MotorJogSpeedCmd.vFloat;
            }
            if (YB_MotorJogPlus.vBit == true)
            {
                //if (InJogMinus)
                //    Logger.Log.AppendLine(LogLevel.Info, "{0} 모터 MINUS JOG 이동 시작", Name);
                //else
                //    Logger.Log.AppendLine(LogLevel.Info, "{0} 모터 MINUS JOG 이동 종료", Name);

                _inSpeed = YF_MotorJogSpeedCmd.vFloat;
            }
            if (_moving || YB_MotorJogMinus.vBit == true || YB_MotorJogPlus.vBit == true)
            {
                XB_StatusHomeInPosition.vBit = false;
                if ((XF_CurrMotorPosition.vFloat > TotalLength && YB_MotorJogPlus.vBit == true) || (XF_CurrMotorPosition.vFloat < 0 && YB_MotorJogMinus.vBit == true))
                    return;


                int position = 0;
                if (YB_MotorJogMinus.vBit == true || YB_MotorJogPlus.vBit == true)
                {
                    position = Convert.ToInt32(YB_MotorJogMinus.vBit == true ? XF_CurrMotorPosition.vFloat - _inSpeed :
                                                  YB_MotorJogPlus.vBit == true ? XF_CurrMotorPosition.vFloat + _inSpeed : XF_CurrMotorPosition.vFloat);
                }
                else
                {
                    position = Convert.ToInt32(_isMinuseGo ? XF_CurrMotorPosition.vFloat - _inSpeed : XF_CurrMotorPosition.vFloat + _inSpeed);
                }

                //if (position < 0)
                //    position = 0;

                if (position > TotalLength)
                    position = TotalLength;

                XF_CurrMotorPosition.vFloat = position;

                if (XF_CurrMotorPosition.vFloat <= _inTargetPosi && _isMinuseGo && YB_MotorJogMinus.vBit == false && YB_MotorJogPlus == false)
                {
                    _moveComplete = true;
                    _moving = false;

                }
                else if (XF_CurrMotorPosition.vFloat >= _inTargetPosi && _isMinuseGo == false && YB_MotorJogMinus.vBit == false && YB_MotorJogPlus == false)
                {
                    _moveComplete = true;
                    _moving = false;

                }

            }
        }
    }
}
