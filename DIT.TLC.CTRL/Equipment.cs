﻿using Dit.Framework.Comm;
using Dit.Framework.PLC;
using DIT.TLC.CTRL._050._Struct._520._Unit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace DIT.TLC.CTRL
{
    public class LoaderModule
    {
        public CstLoaderUint TopCstLoader { get; set; }
        public CstLoaderUint BotCstLoader { get; set; }

        public LoaderUnit Loader { get; set; }

        public LoaderTransferUnit TopLoaderTransfer { get; set; }
        public LoaderTransferUnit BotLoaderTransfer { get; set; }
        public PriAlignProxy PreAlign { get; set; }
        public PlcAddr MainCDAPressurValuie1 { get; internal set; }
        public PlcAddr MainCDAPressurValuie2 { get; internal set; }
        public PlcAddr MainCDAPressurValuie3 { get; internal set; }
        public PlcAddr PanelInnerTempValue { get; internal set; }


        #region LD AJIN

        public Switch CpBoxResetSwitch                                                    /**/    = new Switch();
        public Switch CpBoxTempResetSwitch                                                /**/    = new Switch();

        public Sensor LDLiferMuting1_1                                                    /**/    = new Sensor();
        public Sensor LDLiferMuting1_2                                                    /**/    = new Sensor();
        public Sensor LDLiferMuting2_1                                                    /**/    = new Sensor();
        public Sensor LDLiferMuting2_2                                                    /**/    = new Sensor();

        public Sensor LDEmergencyStop1                                                    /**/    = new Sensor();
        public Sensor LDEmergencyStop2                                                    /**/    = new Sensor();
        public Sensor LDEmergencyStop3                                                    /**/    = new Sensor();
        public Switch EmergencyStopEnableGripSwitch1                                      /**/    = new Switch();
        public Switch SafteyEnableGripSwitchOn1                                           /**/    = new Switch();
        public OffCheckSwitch Door01                                                      /**/    = new OffCheckSwitch();
        public OffCheckSwitch Door02                                                      /**/    = new OffCheckSwitch();
        public OffCheckSwitch Door03                                                      /**/    = new OffCheckSwitch();
        public Sensor LDMcOff1                                                            /**/    = new Sensor();
        public Sensor LDMcOff2                                                            /**/    = new Sensor();
        public Switch ModeSelectSwitchAuto                                                /**/    = new Switch();
        public Switch ModeSelectSwitchTeach                                               /**/    = new Switch();
        public Switch ControlSelectLD                                                     /**/    = new Switch();
        public Sensor Cell취출Hand출동방지                                                /**/    = new Sensor();
        public Switch LDResetSwitchUserA                                                  /**/    = new Switch();
        public Switch LDResetSwitchUserB                                                  /**/    = new Switch();



        public Switch MainCDAPressurSwitch1                                               /**/    = new Switch();
        public Switch MainCDAPressurSwitch2                                               /**/    = new Switch();
        public Switch MainCDAPressurSwitch3                                               /**/    = new Switch();
        public Switch MainVacuumPressurSwitch4                                            /**/    = new Switch();


        public Sensor LDLightCurtain감지                                                  /**/    = new Sensor();
        public Sensor OffDelayTimer감지                                                   /**/    = new Sensor();
        public Sensor LDFanStopAlarm1                                                     /**/    = new Sensor();
        public Sensor LDFanStopAlarm2                                                     /**/    = new Sensor();
        public Sensor LDFanStopAlarm3                                                     /**/    = new Sensor();
        public Sensor LDFanStopAlarm4                                                     /**/    = new Sensor();
        public Sensor LDFanStopAlarm5                                                     /**/    = new Sensor();
        public Sensor LDFanStopAlarm6                                                     /**/    = new Sensor();
        public Sensor EFUAlarm                                                            /**/    = new Sensor();
        public Sensor LD판넬화재알람                                                      /**/    = new Sensor();
        public Sensor LD판넬온도알람                                                      /**/    = new Sensor();

        public SwitchOneWay LDMotorControlOn                    /**/ = new SwitchOneWay();

        public SwitchOneWay LDAResetSwitchLamp                  /**/ = new SwitchOneWay();
        public SwitchOneWay LDBResetSwitchLamp                  /**/ = new SwitchOneWay();
        public SwitchOneWay LDACstMutingSwitchLamp              /**/ = new SwitchOneWay();
        public SwitchOneWay LDBCstMutingSwitchLamp              /**/ = new SwitchOneWay();

        public SwitchOneWay SafteyReset                 /**/ = new SwitchOneWay();
        public SwitchOneWay TowerLampR                  /**/ = new SwitchOneWay();
        public SwitchOneWay TowerLampY                  /**/ = new SwitchOneWay();
        public SwitchOneWay TowerLampG                  /**/ = new SwitchOneWay();
        public SwitchOneWay Buzzer                      /**/ = new SwitchOneWay();
        public SwitchOneWay ControlSelectLDCMD          /**/ = new SwitchOneWay();
        public SwitchOneWay ModelSelectSwAutoTeachLockingSol    /**/ = new SwitchOneWay();
        public SwitchOneWay CpBoxTempResetSWLamp            /**/ = new SwitchOneWay();
        public SwitchOneWay InnerLamp                   /**/ = new SwitchOneWay();


        #endregion

        public LoaderModule()
        {
            TopCstLoader = new CstLoaderUint();
            BotCstLoader = new CstLoaderUint();

            Loader = new LoaderUnit();

            TopLoaderTransfer = new LoaderTransferUnit();
            BotLoaderTransfer = new LoaderTransferUnit();
        }


        public void LogicWorking(Equipment equip)
        {
            TopCstLoader.LogicWorking(equip);
            BotCstLoader.LogicWorking(equip);

            Loader.LogicWorking(equip);

            TopLoaderTransfer.LogicWorking(equip);
            BotLoaderTransfer.LogicWorking(equip);

        }
    }


    public class ProceeModule
    {
        public ProcessStageUnit TopProcessStage { get; set; }
        public ProcessStageUnit BotProcessStage { get; set; }
        public BreakAlignProxy BreakAlign { get; set; }
        public LaserHeadUnit LaserHead { get; set; }
        public BreakingHeadUnit BreakingHead { get; set; }

        public MasureUnit Masure { get; set; }
        public BreakTransferUnit TopBreakTransfer { get; set; }
        public BreakTransferUnit BotBreakTransfer { get; set; }

        public BreakAlignProxy TopBreakAlign { get; set; }
        public BreakAlignProxy BotBreakAlign { get; set; }

        public BreakStageUnit TopBreakStage { get; set; }
        public BreakStageUnit BotBreakStage { get; set; }
        public PlcAddr DLMBlowerPressureValue { get; internal set; }
        public PlcAddr CpBoxTempValue { get; internal set; }

        #region CUTING UMAC
        public Sensor MCOff1                                                       /**/    = new Sensor();
        public Sensor MCOff2                                                       /**/    = new Sensor();
        public Sensor EmergencyStopEnableGripSwitch                                      /**/    = new Sensor();
        public Sensor SafetyEnableGripSwitchOn                                           /**/    = new Sensor();
        public OffCheckSwitch Door4                                                       /**/    = new OffCheckSwitch();
        public OffCheckSwitch Door5                                                       /**/    = new OffCheckSwitch();
        public OffCheckSwitch Door6                                                       /**/    = new OffCheckSwitch();
        public OffCheckSwitch Door7                                                       /**/    = new OffCheckSwitch();
        public OffCheckSwitch Door8                                                       /**/    = new OffCheckSwitch();
        public Sensor LaserAlarm                                                          /**/    = new Sensor();
        public Sensor LaserOn                                                             /**/    = new Sensor();
        public Switch CpBoxTempResetSwitch                                           /**/    = new Switch();

        //public Sensor LaserHeadUpCylinder1                                                /**/    = new Sensor();
        //public Sensor LaserHeadDownCylinder1                                              /**/    = new Sensor();
        //public Sensor LaserHeadUpCylinder2                                                /**/    = new Sensor();
        //public Sensor LaserHeadDownCylinder2                                              /**/    = new Sensor();
        //public Sensor BreakIngHeadX1X2충돌방지                                             /**/    = new Sensor();
        public Switch DLMBlowerPressureSwitch                                             /**/    = new Switch();
        public Sensor Process판넬화재Alarm                                                 /**/    = new Sensor();
        public Sensor Process판넬온도Alarm                                                 /**/    = new Sensor();
        public Sensor Leak원형PMC                                                          /**/    = new Sensor();
        public Sensor DummyTankDetect                                                      /**/    = new Sensor();

        //public Sensor LaserShutter        /*Open Close*/                                   /**/    = new Sensor();
        //public Sensor ProcessStageBrush   /*Up Down  12  */                                /**/    = new Sensor();
        //public Sensor AferIRCut이재기      /*Up Down  12  */                               /**/    = new Sensor();
        //public Sensor BreakingStageBrush   /*Up Down 12  */                                /**/    = new Sensor();
        //public Sensor DummyShutter         /*Open Close  12 */                             /**/    = new Sensor();
        //public Sensor DummyTank투입                                                        /**/    = new Sensor();
        //public Sensor DummyTank배출                                                        /**/    = new Sensor();
        //public Switch CuttingStage1chPressureSwitch1                                       /**/    = new Switch();
        //public Switch CuttingStage2chPressureSwitch1                                       /**/    = new Switch();
        //public Switch CuttingStage1chPressureSwitch2                                       /**/    = new Switch();
        //public Switch CuttingStage2chPressureSwitch2                                       /**/    = new Switch();
        //public Switch CuttingStage1chPressureSwitch3                                       /**/    = new Switch();
        //public Switch CuttingStage2chPressureSwitch3                                       /**/    = new Switch();
        //public Switch CuttingStage1chPressureSwitch4                                       /**/    = new Switch();
        //public Switch CuttingStage2chPressureSwitch4                                       /**/    = new Switch();
        //public Sensor BreakingStagePressure1                                               /**/    = new Sensor();
        //public Sensor BreakingStagePressure2                                               /**/    = new Sensor();
        //public Sensor BreakingStagePressure3                                               /**/    = new Sensor();
        //public Sensor BreakingStagePressure4                                               /**/    = new Sensor();
        //public Switch AfterIRCut이재기PressureSwitch1                                       /**/    = new Switch();
        //public Switch AfterIRCut이재기PressureSwitch2                                       /**/    = new Switch();
        //public Switch AfterIRCut이재기PressureSwitch3                                       /**/    = new Switch();
        //public Switch AfterIRCut이재기PressureSwitch4                                       /**/    = new Switch();
        public Sensor ProcessFanStopAlarm01                                                 /**/    = new Sensor();
        public Sensor ProcessFanStopAlarm02                                                 /**/    = new Sensor();
        public Sensor ProcessFanStopAlarm03                                                 /**/    = new Sensor();
        public Sensor ProcessFanStopAlarm04                                                 /**/    = new Sensor();
        public Sensor ProcessFanStopAlarm05                                                 /**/    = new Sensor();
        public Sensor ProcessFanStopAlarm06                                                 /**/    = new Sensor();
        public Sensor ProcessFanStopAlarm07                                                 /**/    = new Sensor();
        public Sensor ProcessFanStopAlarm08                                                 /**/    = new Sensor();
        public Sensor ProcessFanStopAlarm09                                                 /**/    = new Sensor();
        public Sensor ProcessFanStopAlarm10                                                /**/    = new Sensor();
        public Sensor ProcessFanStopAlarm11                                                /**/    = new Sensor();
        public Sensor ProcessFanStopAlarm12                                                /**/    = new Sensor();


        public Sensor IonizerRun1                                                          /**/    = new Sensor();
        public Sensor IonizerAlarm1_1                                                      /**/    = new Sensor();
        public Sensor IonizerAlarm1_2                                                      /**/    = new Sensor();
        public Sensor IonizerRun2                                                          /**/    = new Sensor();
        public Sensor IonizerAlarm2_1                                                      /**/    = new Sensor();
        public Sensor IonizerAlarm2_2                                                      /**/    = new Sensor();
        public Sensor IonizerRun3                                                          /**/    = new Sensor();
        public Sensor IonizerAlarm3_1                                                      /**/    = new Sensor();
        public Sensor IonizerAlarm3_2                                                      /**/    = new Sensor();
        public Sensor IonizerRun4                                                          /**/    = new Sensor();
        public Sensor IonizerAlarm4_1                                                      /**/    = new Sensor();
        public Sensor IonizerAlarm4_2                                                      /**/    = new Sensor();

        public SwitchOneWay MotorControl                                                   /**/    = new SwitchOneWay();
        public SwitchOneWay LaserStopInternal                                              /**/    = new SwitchOneWay();
        public SwitchOneWay LaserStopExternal                                              /**/    = new SwitchOneWay();
        public SwitchOneWay LaserEmergencyStop                                              /**/    = new SwitchOneWay();
        public SwitchOneWay InnerLamp                                /**/    = new SwitchOneWay();
        public SwitchOneWay LaserOnDisplayBoard                      /**/    = new SwitchOneWay();


        public SwitchOneWay IonizerRunCmd1                           /**/    = new SwitchOneWay();
        public SwitchOneWay IonizerAirRunCmd1                        /**/    = new SwitchOneWay();
        public SwitchOneWay IonizerAirStopCmd1                       /**/    = new SwitchOneWay();
        public SwitchOneWay IonizerRunCmd2                           /**/    = new SwitchOneWay();
        public SwitchOneWay IonizerAirRunCmd2                        /**/    = new SwitchOneWay();
        public SwitchOneWay IonizerAirStopCmd2                       /**/    = new SwitchOneWay();
        public SwitchOneWay IonizerRunCmd3                           /**/    = new SwitchOneWay();
        public SwitchOneWay IonizerAirRunCmd3                        /**/    = new SwitchOneWay();
        public SwitchOneWay IonizerAirStopCmd3                       /**/    = new SwitchOneWay();
        public SwitchOneWay IonizerRunCmd4                           /**/    = new SwitchOneWay();
        public SwitchOneWay IonizerAirRunCmd4                        /**/    = new SwitchOneWay();
        public SwitchOneWay IonizerAirStopCmd4                       /**/    = new SwitchOneWay();
        #endregion

        public ProceeModule()
        {
             

            TopProcessStage = new ProcessStageUnit();
            BotProcessStage = new ProcessStageUnit();
            BreakAlign = new BreakAlignProxy();
            LaserHead = new LaserHeadUnit();
            BreakingHead = new BreakingHeadUnit();

            Masure = new MasureUnit();
            TopBreakTransfer = new BreakTransferUnit();
            BotBreakTransfer = new BreakTransferUnit();

            TopBreakAlign = new BreakAlignProxy();
            BotBreakAlign = new BreakAlignProxy();

            TopBreakStage = new BreakStageUnit();
            BotBreakStage = new BreakStageUnit();
        }

        public void LogicWorking(Equipment equip)
        {
            TopProcessStage.LogicWorking(equip);
            BotProcessStage.LogicWorking(equip);

            TopBreakTransfer.LogicWorking(equip);
            BotBreakTransfer.LogicWorking(equip);

            TopBreakStage.LogicWorking(equip);
            BotBreakStage.LogicWorking(equip);
        }
    }

    public class UnloaderMoudle
    {


        public UnLoaderTransferUnit TopUnloaderTransfer { get; set; }
        public UnLoaderTransferUnit BotUnloaderTransfer { get; set; }

        public InspectionStageUnit InspectionStage { get; set; }

        public BufferUnit Buffer_1ch { get; set; }



        public CameraUnit Camera { get; set; }


        public UnloaderUnit Unloader { get; set; }
        public CstUnloaderUint TopCstUnloader { get; set; }
        public CstUnloaderUint BotCstUnloader { get; set; }

        #region ULD AJIN

        public Switch CpBoxTempResetSwitch                                                  /**/    = new Switch();

        public Sensor ULDLiferMuting1_1                                                    /**/    = new Sensor();
        public Sensor ULDLiferMuting1_2                                                    /**/    = new Sensor();
        public Sensor ULDLiferMuting2_1                                                    /**/    = new Sensor();
        public Sensor ULDLiferMuting2_2                                                    /**/    = new Sensor();

        public Sensor ULDEmergencyStop4                                                    /**/    = new Sensor();
        public Sensor ULDEmergencyStop5                                                    /**/    = new Sensor();
        public Sensor ULDEmergencyStop6                                                    /**/    = new Sensor();
        public Switch EmergencyStopEnalbeGripSwitch3                                       /**/    = new Switch();
        public Switch SafteyEnableGripSwitchOn3                                            /**/    = new Switch();
        public OffCheckSwitch Door09                                                       /**/    = new OffCheckSwitch();
        public OffCheckSwitch Door10                                                       /**/    = new OffCheckSwitch();
        public OffCheckSwitch Door11                                                       /**/    = new OffCheckSwitch();
        public Sensor ULDMcOff1                                                            /**/    = new Sensor();
        public Sensor ULDMcOff2                                                            /**/    = new Sensor();
        public Sensor ControlSelectULD                                                     /**/    = new Sensor();
        public Sensor Cell투입Hand출동방지                                                  /**/    = new Sensor();
        public Switch ULDResetSwitchUserA                                                  /**/    = new Switch();
        public Switch ULDResetSwitchUserB                                                  /**/    = new Switch();
        public Sensor ULDIRCUT충돌방지Y1_Y3                                                 /**/    = new Sensor();
        public Sensor ULDIRCUT충돌방지Y2_Y4                                                 /**/    = new Sensor();
        public Sensor BufferVacuumPressureSwitch                                           /**/    = new Sensor();
        public Sensor BufferUpCylinder                                                     /**/    = new Sensor();
        public Sensor BufferDownCylinder                                                   /**/    = new Sensor();

        
        public Switch Cell투입HandPressureSwitch1                                          /**/    = new Switch();
        public Switch Cell투입HandPressureSwitch2                                          /**/    = new Switch();
        public Switch ULDTransferVaccumPressureSwith1                                      /**/    = new Switch();
        public Switch ULDTransferVaccumPressureSwith2                                      /**/    = new Switch();
        public Switch ULDTransferVaccumPressureSwith3                                      /**/    = new Switch();
        public Switch ULDTransferVaccumPressureSwith4                                      /**/    = new Switch();
        public Switch ULDTransferVaccumPressureSwith5                                      /**/    = new Switch();
        public Switch ULDTransferVaccumPressureSwith6                                      /**/    = new Switch();
        public Switch ULDTransferVaccumPressureSwith7                                      /**/    = new Switch();
        public Switch ULDTransferVaccumPressureSwith8                                      /**/    = new Switch();
        public Switch InspectionStagePressureSwitch1                                       /**/    = new Switch();
        public Switch InspectionStagePressureSwitch2                                       /**/    = new Switch();
        public Switch InspectionStagePressureSwitch3                                       /**/    = new Switch();
        public Switch InspectionStagePressureSwitch4                                       /**/    = new Switch();
        public Sensor ULDLightCurtain감지                                                  /**/    = new Sensor();
        public Sensor ULDFanStopAlarm1                                                     /**/    = new Sensor();
        public Sensor ULDFanStopAlarm2                                                     /**/    = new Sensor();
        public Sensor ULDFanStopAlarm3                                                     /**/    = new Sensor();
        public Sensor ULDFanStopAlarm4                                                     /**/    = new Sensor();
        public Sensor ULDFanStopAlarm5                                                     /**/    = new Sensor();
        public Sensor ULDFanStopAlarm6                                                     /**/    = new Sensor();
        public Sensor ULD판넬화재알람                                                       /**/    = new Sensor();
        public Sensor ULD판넬온도알람                                                       /**/    = new Sensor();
        public Sensor PCRack화재알람                                                        /**/    = new Sensor();

        public SwitchOneWay ULDMotorControlOn                                               /**/ = new SwitchOneWay();

        public SwitchOneWay ULDAResetSwitchLamp                  /**/ = new SwitchOneWay();
        public SwitchOneWay ULDBResetSwitchLamp                  /**/ = new SwitchOneWay();
        public SwitchOneWay ULDACstMutingSwitchLamp              /**/ = new SwitchOneWay();
        public SwitchOneWay ULDBCstMutingSwitchLamp              /**/ = new SwitchOneWay();
        public SwitchOneWay TowerLampR                  /**/ = new SwitchOneWay();
        public SwitchOneWay TowerLampY                  /**/ = new SwitchOneWay();
        public SwitchOneWay TowerLampG                  /**/ = new SwitchOneWay();
        public SwitchOneWay ControlSelectULDCMD          /**/ = new SwitchOneWay();
        public SwitchOneWay CpBoxTempResetSWLamp            /**/ = new SwitchOneWay();
        public SwitchOneWay InnerLamp                   /**/ = new SwitchOneWay();
        #endregion

        public UnloaderMoudle()
        {

            TopUnloaderTransfer = new UnLoaderTransferUnit();
            BotUnloaderTransfer = new UnLoaderTransferUnit();
            
            Camera = new CameraUnit();
            Unloader = new UnloaderUnit();

            TopCstUnloader = new CstUnloaderUint();
            BotCstUnloader = new CstUnloaderUint(); 
         

    }

        public void LogicWorking(Equipment equip)
        {


            TopUnloaderTransfer.LogicWorking(equip);
            BotUnloaderTransfer.LogicWorking(equip);

            Unloader.LogicWorking(equip);

            TopCstUnloader.LogicWorking(equip);
            BotCstUnloader.LogicWorking(equip);
        }
    }

    public class Equipment
    {
        public LoaderModule LD = new LoaderModule();
        public ProceeModule PROC = new ProceeModule();
        public UnloaderMoudle UD = new UnloaderMoudle();

        public PMacProxy PMac                                                           /**/ = new PMacProxy();

        public EmEquipRunMode RunMode { get; set; }

        public bool IsHeavyAlarm { get; set; }
        public bool IsLightAlarm { get; set; }
        public bool IsUnusedAlarm { get; set; }
        public bool IsUseInterLockOff { get; set; }
        public bool IsUseDoorInterLockOff { get; set; }
        public bool IsDoorOpen { get; set; }
        public bool IsPause { get; set; }
        public bool IsImmediatStop { get; set; }

        public Thread _worker = null;

        public bool _running = false;

        public EqpRecipeManager EqpRecipeMgr { get; set; }

        public BreakRecipeManager BreakRecipeMgr { get; set; }
        public ProcessRecipeManager ProcessRecipeMgr { get; set; }

        public Equipment()
        {
            LD = new LoaderModule();
            PROC = new ProceeModule();
            UD = new UnloaderMoudle();

            EqpRecipeMgr = new EqpRecipeManager();
            ProcessRecipeMgr = new ProcessRecipeManager();
        }

        public void StartLogic()
        {
            _running = true;
            _worker = new Thread(new ThreadStart(Working));
            _worker.Start();
        }

        public void StopLogic()
        {
            _running = false;
            _worker.Join();
        }

        public void Working()
        {
            while (_running)
            {
                LogicWorking();
                Thread.Sleep(100);
            }
        }

        public ServoMotorControl[] GetServoMotors()
        {
            ServoMotorControl[] motors = new ServoMotorControl[]
            {
                LD.Loader.X1Axis, LD.Loader.X2Axis, LD.Loader.Y1Axis,LD.Loader.Y2Axis,

                LD.TopLoaderTransfer.XAxis,LD.TopLoaderTransfer.YAxis,
                LD.BotLoaderTransfer.XAxis,LD.BotLoaderTransfer.YAxis,

                PROC.TopProcessStage.YAxis,
                PROC.BotProcessStage.YAxis,

                PROC.TopBreakTransfer.XAxis,PROC.TopBreakTransfer.YAxis,
                PROC.BotBreakTransfer.XAxis,PROC.BotBreakTransfer.YAxis,

                PROC.TopBreakStage.YAxis,PROC.TopBreakStage.X1AxisHeader, PROC.TopBreakStage.X2AxisHeader, PROC.TopBreakStage.Z1AxisHeader, PROC.TopBreakStage.Z2AxisHeader, PROC.TopBreakStage.T1AxisHeader,  PROC.TopBreakStage.T2AxisHeader,
                PROC.BotBreakStage.YAxis,PROC.BotBreakStage.X1AxisHeader, PROC.BotBreakStage.X2AxisHeader, PROC.BotBreakStage.Z1AxisHeader, PROC.BotBreakStage.Z2AxisHeader, PROC.BotBreakStage.T1AxisHeader, PROC.BotBreakStage.T2AxisHeader,

                UD.TopUnloaderTransfer.YAxis,UD.TopUnloaderTransfer.T1Axis, UD.TopUnloaderTransfer.T2Axis,
                UD.BotUnloaderTransfer.YAxis,UD.BotUnloaderTransfer.T1Axis, UD.BotUnloaderTransfer.T2Axis,

                UD.Unloader .X1Axis,UD.Unloader .X2Axis, UD.Unloader.Y1Axis,UD.Unloader.Y2Axis
            };

            return motors;
        }
        public void SetAddress()
        {
            LD.Loader.X1Axis                        /**/= new LoaderXAxisServo(01, "LoadUnit.X1Axis", 15, 0);
            LD.Loader.X2Axis                        /**/= new LoaderXAxisServo(02, "LoadUnit.X2Axis", 15, 0);
            LD.Loader.Y1Axis                        /**/= new LoaderYAxisServo(03, "LoadUnit.YAxis", 15, 0);
            LD.Loader.Y2Axis                        /**/= new LoaderYAxisServo(04, "LoadUnit.YAxis", 15, 0);

            LD.TopLoaderTransfer.XAxis              /**/= new LoaderTransferXAxisServo(05, "TopLoaderTransferUnit.XAxis", 15, 0);
            LD.TopLoaderTransfer.YAxis              /**/= new LoaderTransferYAxisServo(06, "TopLoaderTransferUnit.YAxis", 15, 0);

            LD.BotLoaderTransfer.XAxis              /**/= new LoaderTransferXAxisServo(07, "BotLoaderTransferUnit.XAxis", 15, 0);
            LD.BotLoaderTransfer.YAxis              /**/= new LoaderTransferYAxisServo(08, "BotLoaderTransferUnit.YAxis", 15, 0);

            PROC.TopProcessStage.YAxis              /**/= new ProcessStateXAxisServo(09, "TopProcessStageUnit.YAxis", 15, 0);
            PROC.BotProcessStage.YAxis              /**/= new ProcessStateXAxisServo(10, "BotProcessStageUnit.YAxis", 15, 0);

            PROC.TopBreakTransfer.XAxis               /**/= new BreakTransferXAxisServo(11, "TopBreakTransferUnit.XAxis", 15, 0);
            PROC.TopBreakTransfer.YAxis               /**/= new BreakTransferYAxisServo(12, "TopBreakTransferUnit.YAxis", 15, 0);
            PROC.BotBreakTransfer.XAxis               /**/= new BreakTransferXAxisServo(13, "BotBreakTransferUnit.XAxis", 15, 0);
            PROC.BotBreakTransfer.YAxis               /**/= new BreakTransferYAxisServo(14, "BotBreakTransferUnit.YAxis", 15, 0);

            PROC.TopBreakStage.YAxis                  /**/= new BreakUnitYAxisServo(15, "TopBreakStageUnit.YAxis", 15, 0);
            PROC.TopBreakStage.X1AxisHeader           /**/= new BreakUnitXAxisServo(15, "TopBreakStageUnit.YAxis", 15, 0);
            PROC.TopBreakStage.X2AxisHeader           /**/= new BreakUnitXAxisServo(15, "TopBreakStageUnit.YAxis", 15, 0);
            PROC.TopBreakStage.Z1AxisHeader           /**/= new BreakUnitZAxisServo(16, "TopBreakStageUnit.XAxisHeader", 15, 0);
            PROC.TopBreakStage.Z2AxisHeader           /**/= new BreakUnitZAxisServo(17, "TopBreakStageUnit.ZAxisHeader", 15, 0);
            PROC.TopBreakStage.T1AxisHeader           /**/= new BreakUnitTAxisServo(20, "BotBreakStageUnit.ZAxisHeader", 15, 0);
            PROC.TopBreakStage.T2AxisHeader           /**/= new BreakUnitTAxisServo(20, "BotBreakStageUnit.ZAxisHeader", 15, 0);

            PROC.BotBreakStage.YAxis                  /**/= new BreakUnitYAxisServo(18, "BotBreakStageUnit.YAxis", 15, 0);
            PROC.BotBreakStage.X1AxisHeader           /**/= new BreakUnitXAxisServo(15, "TopBreakStageUnit.YAxis", 15, 0);
            PROC.BotBreakStage.X2AxisHeader           /**/= new BreakUnitXAxisServo(15, "TopBreakStageUnit.YAxis", 15, 0);
            PROC.BotBreakStage.Z1AxisHeader           /**/= new BreakUnitZAxisServo(19, "BotBreakStageUnit.XAxisHeader", 15, 0);
            PROC.BotBreakStage.Z2AxisHeader           /**/= new BreakUnitZAxisServo(20, "BotBreakStageUnit.ZAxisHeader", 15, 0);
            PROC.BotBreakStage.T1AxisHeader           /**/= new BreakUnitTAxisServo(20, "BotBreakStageUnit.ZAxisHeader", 15, 0);
            PROC.BotBreakStage.T2AxisHeader           /**/= new BreakUnitTAxisServo(20, "BotBreakStageUnit.ZAxisHeader", 15, 0);


            UD.TopUnloaderTransfer.YAxis            /**/= new UnloaderTransferYAxisServo(21, "TopUnLoaderTransferUnit.XAxis", 15, 0);
            UD.TopUnloaderTransfer.T1Axis           /**/= new UnloaderTransferTAxisServo(22, "TopUnLoaderTransferUnit.YAxis", 15, 0);
            UD.TopUnloaderTransfer.T2Axis           /**/= new UnloaderTransferTAxisServo(22, "TopUnLoaderTransferUnit.YAxis", 15, 0);


            UD.BotUnloaderTransfer.YAxis            /**/= new UnloaderTransferYAxisServo(23, "BotUnLoaderTransferUnit.XAxis", 15, 0);
            UD.BotUnloaderTransfer.T1Axis           /**/= new UnloaderTransferTAxisServo(24, "BotUnLoaderTransferUnit.YAxis", 15, 0);
            UD.BotUnloaderTransfer.T2Axis           /**/= new UnloaderTransferTAxisServo(24, "BotUnLoaderTransferUnit.YAxis", 15, 0);

            UD.Unloader.X1Axis                      /**/= new UnLoaderXAxisServo(25, "UnloadUnit.X1Axis", 15, 0);
            UD.Unloader.X2Axis                      /**/= new UnLoaderXAxisServo(26, "UnloadUnit.X2Axis", 15, 0);
            UD.Unloader.Y1Axis                      /**/= new UnLoaderYAxisServo(27, "UnloadUnit.YAxis", 15, 0);
            UD.Unloader.Y2Axis                      /**/= new UnLoaderYAxisServo(28, "UnloadUnit.YAxis", 15, 0);


            PMac.YB_EquipMode                       /**/ = AddressMgr.GetAddress("PMAC_YB_CheckMode", 0);
            PMac.YB_CheckAlarmStatus                /**/ = AddressMgr.GetAddress("PMAC_YB_CheckAlarmStatus", 0);
            PMac.YB_UpperInterfaceWorking           /**/ = AddressMgr.GetAddress("PMAC_YB_UpperInterfaceWorking", 0);
            PMac.YB_LowerInterfaceWorking           /**/ = AddressMgr.GetAddress("PMAC_YB_LowerInterfaceWorking", 0);
            PMac.XB_PmacReady                       /**/ = AddressMgr.GetAddress("PMAC_XB_PmacReady", 0);
            PMac.XB_PmacAlive                       /**/ = AddressMgr.GetAddress("PMAC_XB_PmacAlive", 0);
            PMac.XB_PmacHeavyAlarm                  /**/ = AddressMgr.GetAddress("PMAC_XB_PmacMidAlarm", 0);
            PMac.YB_PinUpMotorInterlockDisable      /**/ = AddressMgr.GetAddress("PMAC_YB_PinUpMotorInterlock", 0);
            PMac.XB_PinUpMotorInterlockDisableAck   /**/ = AddressMgr.GetAddress("PMAC_XB_PinUpMotorInterlockAck", 0);
            PMac.YB_ReviewTimerOverCmd              /**/ = AddressMgr.GetAddress("PMAC_YB_ReviewTimerOverCmd", 0);
            PMac.XB_ReviewTimerOverCmdAck           /**/ = AddressMgr.GetAddress("PMAC_XB_ReviewTimerOverCmdAck", 0);
            PMac.YB_PmacResetCmd                    /**/ = AddressMgr.GetAddress("PMAC_YB_PmacResetCmd", 0);
            PMac.XB_PmacResetCmdAck                 /**/ = AddressMgr.GetAddress("PMAC_XB_PmacResetCmdAck", 0);

            #region PMAC
            ServoMotorControl[] motors = GetServoMotors();


            PMac.YB_EquipMode                       /**/ = AddressMgr.GetAddress("PMAC_YB_EquipState", 0, 0);
            PMac.YB_CheckAlarmStatus                /**/ = AddressMgr.GetAddress("PMAC_YB_EquipState", 0, 1);
            PMac.YB_UpperInterfaceWorking           /**/ = AddressMgr.GetAddress("PMAC_YB_EquipState", 0, 2);
            PMac.YB_LowerInterfaceWorking           /**/ = AddressMgr.GetAddress("PMAC_YB_EquipState", 0, 3);

            PMac.XB_PmacReady                       /**/ = AddressMgr.GetAddress("PMAC_XB_PmacState", 0, 0);
            PMac.XB_PmacAlive                       /**/ = AddressMgr.GetAddress("PMAC_XB_PmacState", 0, 1);
            //PMac.XB_ReviewUsingPmac               /**/ = AddressMgr.GetAddress("PMAC_XB_PmacState", 0, 2);

            //PMac.XB_PmacHeavyAlarm                /**/ = AddressMgr.GetAddress("PMAC_XB_PmacMidAlarm", 0);
            //PMac.YB_PinUpMotorInterlockOffCmd     /**/ = AddressMgr.GetAddress("PMAC_YB_CommonCmd", 0, 0);
            //PMac.XB_PinUpMotorInterlockOffCmdAck  /**/ = AddressMgr.GetAddress("PMAC_XB_CommonCmdAck", 0, 0);
            PMac.YB_PmacResetCmd                    /**/ = AddressMgr.GetAddress("PMAC_YB_CommonCmd", 0, 1);
            PMac.XB_PmacResetCmdAck                 /**/ = AddressMgr.GetAddress("PMAC_XB_CommonCmdAck", 0, 1);
            //PMac.YB_ImmediateStopCmd              /**/ = AddressMgr.GetAddress("PMAC_YB_CommonCmd", 0, 2);
            //PMac.XB_ImmediateStopCmdAck           /**/ = AddressMgr.GetAddress("PMAC_XB_CommonCmdAck", 0, 2);
            PMac.YB_PmacValueSave                   /**/ = AddressMgr.GetAddress("PMAC_YB_CommonCmd", 0, 3);
            PMac.XB_PmacValueSaveAck                /**/ = AddressMgr.GetAddress("PMAC_XB_CommonCmdAck", 0, 3);
            PMac.YB_ReviewTimerOverCmd              /**/ = AddressMgr.GetAddress("PMAC_YB_CommonCmd", 0, 4);
            PMac.XB_ReviewTimerOverCmdAck           /**/ = AddressMgr.GetAddress("PMAC_XB_CommonCmdAck", 0, 4);

            //ScanX1.YF_TriggerStartPosi            /**/ = AddressMgr.GetAddress("TriggerStartPosCmd", 0);
            //ScanX1.XF_TriggerStartPosiAck         /**/ = AddressMgr.GetAddress("TriggerStartPosCmdAck", 0);
            //
            //ScanX1.YF_TriggerEndPosi              /**/ = AddressMgr.GetAddress("TriggerEndPosCmd", 0);
            //ScanX1.XF_TriggerEndPosiAck           /**/ = AddressMgr.GetAddress("TriggerEndPosCmdAck", 0);

            for (int jPos = 0; jPos < motors.Length; jPos++)
            {
                //string motor = motors[jPos].Name;
                //string slayerMotor = motors[jPos].SlayerName;

                int axis = motors[jPos].Axis - 1;
                string axisStr = (axis + 1).ToString("D2");
                int slayerAxis = motors[jPos].Axis - 1;
                string slayerAxisStr = (slayerAxis + 1).ToString("D2");

                motors[jPos].XB_StatusHomeCompleteBit                       /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_StatusHomeCompleteBit"), 0, axis);
                motors[jPos].XB_StatusHomeInPosition                        /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_StatusHomeInPosition"), 0, axis);
                motors[jPos].XB_StatusMotorMoving                           /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_StatusMotorMoving"), 0, axis);
                motors[jPos].XB_StatusMotorInPosition                       /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_StatusMotorInPosition"), 0, axis);
                motors[jPos].XB_StatusNegativeLimitSet                      /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_StatusNegativeLimitSet"), 0, axis);
                motors[jPos].XB_StatusPositiveLimitSet                      /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_StatusPositiveLimitSet"), 0, axis);
                //motors[jPos].XB_ErrMotorServoOn                             /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_ErrMotorServoOn"), 0, axis);
                //motors[jPos].XB_ErrFatalFollowingError                      /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_ErrFatalFollowingError"), 0, axis);
                //motors[jPos].XB_ErrAmpFaultError                            /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_ErrAmpFaultError"), 0, axis);
                //motors[jPos].XB_ErrI2TAmpFaultError                         /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_ErrI2TAmpFaultError"), 0, axis);

                motors[jPos].XF_CurrMotorPosition                           /**/  = AddressMgr.GetAddress(string.Format("Axis{0}_XF_CurrentMotorPosition", axisStr), 0);
                motors[jPos].XF_CurrMotorSpeed                              /**/  = AddressMgr.GetAddress(string.Format("Axis{0}_XF_CurrentMotorSpeed", axisStr), 0);
                //motors[jPos].XI_CurrMotorStress                             /**/  = AddressMgr.GetAddress(string.Format("Axis{0}_XI_CurrentMotorStress", axisStr), 0);

                //motors[jPos].XB_StatusHomeCompleteBitSlayer                 /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_StatusHomeCompleteBit"), 0, slayerAxis);
                //motors[jPos].XB_StatusHomeInPositionSlayer                  /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_StatusHomeInPosition"), 0, slayerAxis);
                //motors[jPos].XB_StatusMotorMovingSlayer                     /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_StatusMotorMoving"), 0, slayerAxis);
                //motors[jPos].XB_StatusMotorInPositionSlayer                 /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_StatusMotorInPosition"), 0, slayerAxis);
                //motors[jPos].XB_StatusNegativeLimitSetSlayer                /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_StatusNegativeLimitSet"), 0, slayerAxis);
                //motors[jPos].XB_StatusPositiveLimitSetSlayer                /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_StatusPositiveLimitSet"), 0, slayerAxis);
                //motors[jPos].XB_StatusMotorServoOnSlayer                    /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_ErrMotorServoOn"), 0, slayerAxis);
                //motors[jPos].XB_ErrFatalFollowingErrorSlayer                /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_ErrFatalFollowingError"), 0, slayerAxis);
                //motors[jPos].XB_ErrAmpFaultErrorSlayer                      /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_ErrAmpFaultError"), 0, slayerAxis);
                //motors[jPos].XB_ErrI2TAmpFaultErrorSlayer                   /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_ErrI2TAmpFaultError"), 0, slayerAxis);
                //
                //motors[jPos].XF_CurrMotorPositionSlayer                     /**/  = AddressMgr.GetAddress(string.Format("Axis{0}_XF_CurrentMotorPosition", slayerAxisStr), 0);
                //motors[jPos].XF_CurrMotorSpeedSlayer                        /**/  = AddressMgr.GetAddress(string.Format("Axis{0}_XF_CurrentMotorSpeed", slayerAxisStr), 0);
                //motors[jPos].XI_CurrMotorStressSlayer                       /**/  = AddressMgr.GetAddress(string.Format("Axis{0}_XI_CurrentMotorStress", slayerAxisStr), 0);

                motors[jPos].YB_HomeCmd                                     /**/  = AddressMgr.GetAddress(string.Format("PMAC_YB_HomeCmd"), 0, axis);
                motors[jPos].XB_HomeCmdAck                                  /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_HomeCmdAck"), 0, axis);
                motors[jPos].YB_MotorJogMinusMove                            /**/  = AddressMgr.GetAddress(string.Format("PMAC_YB_MinusJogCmd"), 0, axis);
                motors[jPos].YB_MotorJogPlusMove                            /**/  = AddressMgr.GetAddress(string.Format("PMAC_YB_PlusJogCmd"), 0, axis);
                motors[jPos].YF_MotorJogSpeedCmd                            /**/  = AddressMgr.GetAddress(string.Format("Axis{0}_YB_JogSpeed", axisStr), 0);
                //motors[jPos].XI_CmdAckLogMsg                               /**/  = AddressMgr.GetAddress(string.Format("PMAC_XB_CmdAckLogMsg"), 0, axis);

                for (int iPos = 1; iPos <= motors[jPos].PositionCount; iPos++)
                {
                    motors[jPos].YB_Position0MoveCmd[iPos - 1]        /**/= AddressMgr.GetAddress(string.Format("Axis{0}_YB_PositionMoveCmd", axisStr), 0, iPos - 1);
                    motors[jPos].XB_Position0MoveCmdAck[iPos - 1]     /**/= AddressMgr.GetAddress(string.Format("Axis{0}_XB_PositionMoveCmdAck", axisStr), 0, iPos - 1);

                    motors[jPos].XB_PositionComplete[iPos - 1]       /**/= AddressMgr.GetAddress(string.Format("Axis{0}_XB_PositionMoveComplete", axisStr), 0, iPos - 1);

                    motors[jPos].YF_PositionPoint[iPos - 1]          /**/= AddressMgr.GetAddress(string.Format("Axis{0}_YF_Position{1}Point", axisStr, iPos.ToString("D2")), 0);
                    motors[jPos].XF_PositionPointAck[iPos - 1]       /**/= AddressMgr.GetAddress(string.Format("Axis{0}_XF_Position{1}PointAck", axisStr, iPos.ToString("D2")), 0);

                    //motors[jPos].YF_Position0Speed[iPos - 1]          /**/= AddressMgr.GetAddress(string.Format("Axis{0}_YF_Position{1}Speed", axisStr, iPos.ToString("D2")), 0);
                    //motors[jPos].XF_Position0SpeedAck[iPos - 1]       /**/= AddressMgr.GetAddress(string.Format("Axis{0}_XF_Position{1}SpeedAck", axisStr, iPos.ToString("D2")), 0);
                    //
                    //motors[jPos].YF_Position0Accel[iPos - 1]          /**/= AddressMgr.GetAddress(string.Format("Axis{0}_Yf_Position{1}Accel", axisStr, iPos.ToString("D2")), 0);
                    //motors[jPos].XF_Position0AccelAck[iPos - 1]       /**/= AddressMgr.GetAddress(string.Format("Axis{0}_Xf_Position{1}AccelAck", axisStr, iPos.ToString("D2")), 0);
                }
            }
            #endregion


            // LOADER UNIT START            
            LD.TopCstLoader.MutingSwitch.XB_OnOff                           /**/= AddressMgr.GetAddress("LD_A열_투입_Muting_Switch_On_Off_Use", 0);
            LD.BotCstLoader.MutingSwitch.XB_OnOff                           /**/= AddressMgr.GetAddress("LD_B열_투입_Muting_Switch_On_Off_Use", 0);

            LD.CpBoxResetSwitch.XB_OnOff                                    /**/= AddressMgr.GetAddress("CP_BOX_Reset_Switch_User", 0);
            LD.CpBoxTempResetSwitch.XB_OnOff                                /**/= AddressMgr.GetAddress("LD_CPBOX_TEMP_Reset_Switch_User", 0);


            LD.TopCstLoader.LiftMutingSwitch1.XB_OnOff                      /**/= AddressMgr.GetAddress("1_1_LD_Lifter_Muting_On", 0);
            LD.TopCstLoader.LiftMutingSwitch2.XB_OnOff                      /**/= AddressMgr.GetAddress("1_2_LD_Lifter_Muting_On", 0);
            LD.BotCstLoader.LiftMutingSwitch1.XB_OnOff                      /**/= AddressMgr.GetAddress("2_1_LD_Lifter_Muting_On", 0);
            LD.BotCstLoader.LiftMutingSwitch2.XB_OnOff                      /**/= AddressMgr.GetAddress("2_2_LD_Lifter_Muting_On", 0);


            LD.LDEmergencyStop1.XB_OnOff                                    /**/= AddressMgr.GetAddress("1_LD_Emergency_Stop_Feedback", 0);
            LD.LDEmergencyStop2.XB_OnOff                                    /**/= AddressMgr.GetAddress("2_LD_Emergency_Stop_Feedback", 0);
            LD.LDEmergencyStop3.XB_OnOff                                    /**/= AddressMgr.GetAddress("3_LD_Emergency_Stop_Feedback", 0);
            LD.EmergencyStopEnableGripSwitch1.XB_OnOff                      /**/= AddressMgr.GetAddress("1_Emergency_Stop_Enable_Grip_SW", 0);
            LD.SafteyEnableGripSwitchOn1.XB_OnOff                           /**/= AddressMgr.GetAddress("1_Safety_Enable_Grip_SW_On_Feedback", 0);

            LD.Door01.XB_OnOff                                              /**/= AddressMgr.GetAddress("1_Safety_Door_SW_Open", 0);
            LD.Door02.XB_OnOff                                              /**/= AddressMgr.GetAddress("2_Safety_Door_SW_Open", 0);
            LD.Door03.XB_OnOff                                              /**/= AddressMgr.GetAddress("3_Safety_Door_SW_Open", 0);

            LD.LDMcOff1.XB_OnOff                                            /**/= AddressMgr.GetAddress("LD_MC_1_OFF", 0);
            LD.LDMcOff2.XB_OnOff                                            /**/= AddressMgr.GetAddress("LD_MC_2_OFF", 0);

            LD.ModeSelectSwitchAuto.XB_OnOff                                /**/= AddressMgr.GetAddress("Mode_Select_SW_Auto", 0);
            LD.ModeSelectSwitchTeach.XB_OnOff                               /**/= AddressMgr.GetAddress("Mode_Select_SW_Teach", 0);

            LD.ControlSelectLD.XB_OnOff                                     /**/= AddressMgr.GetAddress("Control_Select_LD", 0);

            LD.Cell취출Hand출동방지.XB_OnOff                                /**/= AddressMgr.GetAddress("Cell_취출_Hand_충돌방지_센서", 0);
            LD.TopCstLoader.ResetSwitch.XB_OnOff                            /**/= AddressMgr.GetAddress("LD_A_Reset_Switch_User", 0);
            LD.BotCstLoader.ResetSwitch.XB_OnOff                            /**/= AddressMgr.GetAddress("LD_B_Reset_Switch_User", 0);

            LD.TopCstLoader.MutingSwitch.XB_OnOff                           /**/= AddressMgr.GetAddress("LD_A_IN_OUT_Muting_On", 0);
            LD.BotCstLoader.MutingSwitch.XB_OnOff                           /**/= AddressMgr.GetAddress("LD_B_IN_OUT_Muting_On", 0);

            LD.TopCstLoader.LDCasstteDetect_1.XB_OnOff                      /**/= AddressMgr.GetAddress("1_1_LD_Cassette_Detect", 0);
            LD.TopCstLoader.LDCasstteDetect_2.XB_OnOff                      /**/= AddressMgr.GetAddress("1_2_LD_Cassette_Detect", 0);
            LD.TopCstLoader.LDCasstteDetect_3.XB_OnOff                      /**/= AddressMgr.GetAddress("1_3_LD_Cassette_Detect", 0);
            LD.TopCstLoader.LDCasstteDetect_4.XB_OnOff                      /**/= AddressMgr.GetAddress("1_4_LD_Cassette_Detect", 0);
            LD.TopCstLoader.LDCasstteDetect_5.XB_OnOff                      /**/= AddressMgr.GetAddress("1_5_LD_Cassette_Detect", 0);
            LD.TopCstLoader.LDCasstteDetect_6.XB_OnOff                      /**/= AddressMgr.GetAddress("1_6_LD_Cassette_Detect", 0);

            LD.BotCstLoader.LDCasstteDetect_1.XB_OnOff                      /**/= AddressMgr.GetAddress("2_1_LD_Cassette_Detect", 0);
            LD.BotCstLoader.LDCasstteDetect_2.XB_OnOff                      /**/= AddressMgr.GetAddress("2_2_LD_Cassette_Detect", 0);
            LD.BotCstLoader.LDCasstteDetect_3.XB_OnOff                      /**/= AddressMgr.GetAddress("2_3_LD_Cassette_Detect", 0);
            LD.BotCstLoader.LDCasstteDetect_4.XB_OnOff                      /**/= AddressMgr.GetAddress("2_4_LD_Cassette_Detect", 0);
            LD.BotCstLoader.LDCasstteDetect_5.XB_OnOff                      /**/= AddressMgr.GetAddress("2_5_LD_Cassette_Detect", 0);
            LD.BotCstLoader.LDCasstteDetect_6.XB_OnOff                      /**/= AddressMgr.GetAddress("2_6_LD_Cassette_Detect", 0);

            LD.TopCstLoader.CasstteGripCylinder.XB_ForwardCompleteP1        /**/= AddressMgr.GetAddress("1_1_LD_Cassette_좌우_Grip_POS", 0);
            LD.TopCstLoader.CasstteGripCylinder.XB_BackwardCompleteP1       /**/= AddressMgr.GetAddress("1_1_LD_Cassette_좌우_Ungrip_POS", 0);
            LD.TopCstLoader.CasstteGripCylinder.XB_ForwardCompleteP2        /**/= AddressMgr.GetAddress("1_2_LD_Cassette_좌우_Grip_POS", 0);
            LD.TopCstLoader.CasstteGripCylinder.XB_BackwardCompleteP2       /**/= AddressMgr.GetAddress("1_2_LD_Cassette_좌우_Ungrip_POS", 0);
            LD.BotCstLoader.CasstteGripCylinder.XB_ForwardCompleteP1        /**/= AddressMgr.GetAddress("2_1_LD_Cassette_좌우_Grip_POS", 0);
            LD.BotCstLoader.CasstteGripCylinder.XB_BackwardCompleteP1       /**/= AddressMgr.GetAddress("2_1_LD_Cassette_좌우_Ungrip_POS", 0);
            LD.BotCstLoader.CasstteGripCylinder.XB_ForwardCompleteP2        /**/= AddressMgr.GetAddress("2_2_LD_Cassette_좌우_Grip_POS", 0);
            LD.BotCstLoader.CasstteGripCylinder.XB_BackwardCompleteP2       /**/= AddressMgr.GetAddress("2_2_LD_Cassette_좌우_Ungrip_POS", 0);

            LD.TopLoaderTransfer.UpDonw1Cylinder.XB_BackwardComplete        /**/= AddressMgr.GetAddress("1_LD_Transfer_Up_Cylinder", 0);
            LD.TopLoaderTransfer.UpDonw1Cylinder.XB_ForwardComplete         /**/= AddressMgr.GetAddress("1_LD_Transfer_Down_Cylinder", 0);
            LD.TopLoaderTransfer.UpDonw2Cylinder.XB_BackwardComplete        /**/= AddressMgr.GetAddress("2_LD_Transfer_Up_Cylinder", 0);
            LD.TopLoaderTransfer.UpDonw2Cylinder.XB_ForwardComplete         /**/= AddressMgr.GetAddress("2_LD_Transfer_Down_Cylinder", 0);

            LD.BotLoaderTransfer.UpDonw1Cylinder.XB_BackwardComplete        /**/= AddressMgr.GetAddress("3_LD_Transfer_Up_Cylinder", 0);
            LD.BotLoaderTransfer.UpDonw1Cylinder.XB_ForwardComplete         /**/= AddressMgr.GetAddress("3_LD_Transfer_Down_Cylinder", 0);
            LD.BotLoaderTransfer.UpDonw2Cylinder.XB_BackwardComplete        /**/= AddressMgr.GetAddress("4_LD_Transfer_Up_Cylinder", 0);
            LD.BotLoaderTransfer.UpDonw2Cylinder.XB_ForwardComplete         /**/= AddressMgr.GetAddress("4_LD_Transfer_Down_Cylinder", 0);

            LD.TopCstLoader.TiltCylinder.XB_BackwardComplete                /**/= AddressMgr.GetAddress("1_LD_Cassette_Lift_Tilting_Sensor_UP", 0);
            LD.TopCstLoader.TiltCylinder.XB_ForwardComplete                 /**/= AddressMgr.GetAddress("1_LD_Cassette_Lift_Tilting_Sensor_DOWN", 0);
            LD.BotCstLoader.TiltCylinder.XB_BackwardComplete                /**/= AddressMgr.GetAddress("2_LD_Cassette_Lift_Tilting_Sensor_UP", 0);
            LD.BotCstLoader.TiltCylinder.XB_BackwardComplete                /**/= AddressMgr.GetAddress("2_LD_Cassette_Lift_Tilting_Sensor_DOWN", 0);

            LD.MainCDAPressurSwitch1.XB_OnOff                               /**/= AddressMgr.GetAddress("1_Main_CDA_Pressure_SW", 0);
            LD.MainCDAPressurSwitch2.XB_OnOff                               /**/= AddressMgr.GetAddress("2_Main_CDA_Pressure_SW", 0);
            LD.MainCDAPressurSwitch3.XB_OnOff                               /**/= AddressMgr.GetAddress("3_Main_CDA_Pressure_SW", 0);
            LD.MainVacuumPressurSwitch4.XB_OnOff                            /**/= AddressMgr.GetAddress("4_Main_Vacuum_Pressure_SW", 0);

            LD.Loader.Vaccum1.XB_OnOff                                      /**/= AddressMgr.GetAddress("1_Cell_취출_Hand_Pressure_SW", 0);
            LD.Loader.Vaccum2.XB_OnOff                                      /**/= AddressMgr.GetAddress("2_Cell_취출_Hand_Pressure_SW", 0);

            LD.TopLoaderTransfer.Vaccum1.XB_OnOff                           /**/= AddressMgr.GetAddress("1_LD_Transfer_Vaccum_Pressure_SW", 0);
            LD.TopLoaderTransfer.Vaccum2.XB_OnOff                           /**/= AddressMgr.GetAddress("2_LD_Transfer_Vaccum_Pressure_SW", 0);
            LD.BotLoaderTransfer.Vaccum1.XB_OnOff                           /**/= AddressMgr.GetAddress("3_LD_Transfer_Vaccum_Pressure_SW", 0);
            LD.BotLoaderTransfer.Vaccum1.XB_OnOff                           /**/= AddressMgr.GetAddress("4_LD_Transfer_Vaccum_Pressure_SW", 0);

            LD.LDLightCurtain감지.XB_OnOff                                  /**/= AddressMgr.GetAddress("LD_Light_Curtain_감지", 0);
            LD.OffDelayTimer감지.XB_OnOff                                   /**/= AddressMgr.GetAddress("Off_Delay_Timer_감지", 0);
            LD.LDFanStopAlarm1.XB_OnOff                                     /**/= AddressMgr.GetAddress("1_LD_FAN_Stop_Alarm", 0);
            LD.LDFanStopAlarm2.XB_OnOff                                     /**/= AddressMgr.GetAddress("2_LD_FAN_Stop_Alarm", 0);
            LD.LDFanStopAlarm3.XB_OnOff                                     /**/= AddressMgr.GetAddress("3_LD_FAN_Stop_Alarm", 0);
            LD.LDFanStopAlarm4.XB_OnOff                                     /**/= AddressMgr.GetAddress("4_LD_FAN_Stop_Alarm", 0);
            LD.LDFanStopAlarm5.XB_OnOff                                     /**/= AddressMgr.GetAddress("5_LD_FAN_Stop_Alarm", 0);
            LD.LDFanStopAlarm6.XB_OnOff                                     /**/= AddressMgr.GetAddress("6_LD_FAN_Stop_Alarm", 0);
            LD.EFUAlarm.XB_OnOff                                            /**/= AddressMgr.GetAddress("EFU_Alarm", 0);
            LD.LD판넬화재알람.XB_OnOff                                      /**/= AddressMgr.GetAddress("LD부_판넬_화재_알람", 0);
            LD.LD판넬온도알람.XB_OnOff                                      /**/= AddressMgr.GetAddress("LD부_판넬_온도_알람", 0);

            //OUT 
            LD.LDMotorControlOn.YB_OnOff                                   /**/= AddressMgr.GetAddress("LD_Motor_Control_On_CMD", 0);

            LD.TopCstLoader.ResetSwitch.YB_OnOff                           /**/= AddressMgr.GetAddress("LD_A열_Reset_Switch_Lamp", 0);
            LD.BotCstLoader.ResetSwitch.YB_OnOff                           /**/= AddressMgr.GetAddress("LD_B열_Reset_Switch_Lamp", 0);

            LD.TopCstLoader.MutingLamp.YB_OnOff                            /**/= AddressMgr.GetAddress("LD_A열_Cassette_Muting_Switch_Lamp", 0);
            LD.BotCstLoader.MutingLamp.YB_OnOff                            /**/= AddressMgr.GetAddress("LD_B열_Cassette_Muting_Switch_Lamp", 0);
            LD.SafteyReset.YB_OnOff                                        /**/= AddressMgr.GetAddress("안전회로_Reset", 0);

            LD.TowerLampR.YB_OnOff                                         /**/= AddressMgr.GetAddress("1_Tower_Lamp_R", 0);
            LD.TowerLampY.YB_OnOff                                         /**/= AddressMgr.GetAddress("1_Tower_Lamp_Y", 0);
            LD.TowerLampG.YB_OnOff                                         /**/= AddressMgr.GetAddress("1_Tower_Lamp_G", 0);

            LD.ControlSelectLD.YB_OnOff                                    /**/= AddressMgr.GetAddress("Control_Select_LD_Feedback", 0);

            LD.TopCstLoader.MutingSwitch.YB_OnOff                          /**/= AddressMgr.GetAddress("LD_A열_Cassette_투입_배출_Muting_Lamp", 0);
            LD.TopCstLoader.MutingSwitch.YB_OnOff                          /**/= AddressMgr.GetAddress("LD_B열_Cassette_투입_배출_Muting_Lamp", 0);

            LD.ModeSelectSwitchTeach.YB_OnOff                              /**/= AddressMgr.GetAddress("Mode_Select_SW_Auto_TEACH", 0);
            LD.CpBoxTempResetSwitch.YB_OnOff                               /**/= AddressMgr.GetAddress("LD_CPBOX_TEMP_RESET_SW_LAMP", 0);


            LD.TopCstLoader.CasstteGripCylinder.YB_ForwardCmd              /**/= AddressMgr.GetAddress("1_LD_Casstte_Grip", 0);
            LD.TopCstLoader.CasstteGripCylinder.YB_BackwardCmd             /**/= AddressMgr.GetAddress("1_LD_Casstte_UnGrip", 0);

            LD.BotCstLoader.CasstteGripCylinder.YB_ForwardCmd              /**/= AddressMgr.GetAddress("2_LD_Casstte_Grip", 0);
            LD.BotCstLoader.CasstteGripCylinder.YB_BackwardCmd             /**/= AddressMgr.GetAddress("2_LD_Casstte_UnGrip", 0);


            LD.TopLoaderTransfer.UpDonw1Cylinder.YB_BackwardCmd             /**/= AddressMgr.GetAddress("1_LD_Transfer_Up_Cylinder", 0);
            LD.TopLoaderTransfer.UpDonw1Cylinder.YB_ForwardCmd              /**/= AddressMgr.GetAddress("1_LD_Transfer_Down_Cylinder", 0);
            LD.TopLoaderTransfer.UpDonw2Cylinder.YB_BackwardCmd             /**/= AddressMgr.GetAddress("2_LD_Transfer_Up_Cylinder", 0);
            LD.TopLoaderTransfer.UpDonw2Cylinder.YB_ForwardCmd              /**/= AddressMgr.GetAddress("2_LD_Transfer_Down_Cylinder", 0);

            LD.BotLoaderTransfer.UpDonw1Cylinder.YB_BackwardCmd             /**/= AddressMgr.GetAddress("3_LD_Transfer_Up_Cylinder", 0);
            LD.BotLoaderTransfer.UpDonw1Cylinder.YB_ForwardCmd              /**/= AddressMgr.GetAddress("3_LD_Transfer_Down_Cylinder", 0);
            LD.BotLoaderTransfer.UpDonw2Cylinder.YB_BackwardCmd             /**/= AddressMgr.GetAddress("4_LD_Transfer_Up_Cylinder", 0);
            LD.BotLoaderTransfer.UpDonw2Cylinder.YB_ForwardCmd              /**/= AddressMgr.GetAddress("4_LD_Transfer_Down_Cylinder", 0);

            LD.TopCstLoader.TiltCylinder.YB_ForwardCmd                      /**/= AddressMgr.GetAddress("1_LD_Cassette_Lift_Tilting_Sensor_UP", 0);
            LD.TopCstLoader.TiltCylinder.YB_BackwardCmd                     /**/= AddressMgr.GetAddress("1_LD_Cassette_Lift_Tilting_Sensor_DOWN", 0);

            LD.BotCstLoader.TiltCylinder.YB_ForwardCmd                      /**/= AddressMgr.GetAddress("2_LD_Cassette_Lift_Tilting_Sensor_UP", 0);
            LD.BotCstLoader.TiltCylinder.YB_BackwardCmd                     /**/= AddressMgr.GetAddress("2_LD_Cassette_Lift_Tilting_Sensor_DOWN", 0);

            LD.Loader.Vaccum1.YB_On                                         /**/= AddressMgr.GetAddress("1_Cell_취출_HAND_Vacuum_ON", 0);
            LD.Loader.Vaccum1.YB_Off                                        /**/= AddressMgr.GetAddress("1_Cell_취출_HAND_Vacuum_OFF", 0);
            LD.Loader.Blower1.YB_OnOff                                      /**/= AddressMgr.GetAddress("1_Cell_취출_HAND_파기_AIR_ON", 0);

            LD.Loader.Vaccum2.YB_On                                         /**/= AddressMgr.GetAddress("2_Cell_취출_HAND_Vacuum_ON", 0);
            LD.Loader.Vaccum2.YB_On                                         /**/= AddressMgr.GetAddress("2_Cell_취출_HAND_Vacuum_ON", 0);
            LD.Loader.Blower2.YB_OnOff                                      /**/= AddressMgr.GetAddress("2_Cell_취출_HAND_파기_AIR_ON", 0);

            LD.TopLoaderTransfer.Vaccum1.YB_On                              /**/= AddressMgr.GetAddress("1_LD_Transfer_Vaccum_ON", 0);
            LD.TopLoaderTransfer.Vaccum1.YB_Off                             /**/= AddressMgr.GetAddress("1_LD_Transfer_Vaccum_OFF", 0);
            LD.TopLoaderTransfer.Blower1.YB_OnOff                           /**/= AddressMgr.GetAddress("1_LD_Transfer_파기_AIR_ON", 0);

            LD.TopLoaderTransfer.Vaccum2.YB_On                              /**/= AddressMgr.GetAddress("2_LD_Transfer_Vaccum_ON", 0);
            LD.TopLoaderTransfer.Vaccum2.YB_Off                             /**/= AddressMgr.GetAddress("2_LD_Transfer_Vaccum_OFF", 0);
            LD.TopLoaderTransfer.Blower2.YB_OnOff                           /**/= AddressMgr.GetAddress("2_LD_Transfer_파기_AIR_ON", 0);


            LD.BotLoaderTransfer.Vaccum1.YB_On                              /**/= AddressMgr.GetAddress("3_LD_Transfer_Vaccum_ON", 0);
            LD.BotLoaderTransfer.Vaccum1.YB_Off                             /**/= AddressMgr.GetAddress("3_LD_Transfer_Vaccum_OFF", 0);
            LD.BotLoaderTransfer.Blower1.YB_OnOff                           /**/= AddressMgr.GetAddress("3_LD_Transfer_파기_AIR_ON", 0);

            LD.BotLoaderTransfer.Vaccum2.YB_On                              /**/= AddressMgr.GetAddress("4_LD_Transfer_Vaccum_ON", 0);
            LD.BotLoaderTransfer.Vaccum2.YB_Off                             /**/= AddressMgr.GetAddress("4_LD_Transfer_Vaccum_OFF", 0);
            LD.BotLoaderTransfer.Blower2.YB_OnOff                           /**/= AddressMgr.GetAddress("4_LD_Transfer_파기_AIR_ON", 0);


            LD.Door01.YB_OnOff                                              /**/= AddressMgr.GetAddress("1_Safety_Door_SW_Open", 0);
            LD.Door02.YB_OnOff                                              /**/= AddressMgr.GetAddress("2_Safety_Door_SW_Open", 0);
            LD.Door03.YB_OnOff                                              /**/= AddressMgr.GetAddress("3_Safety_Door_SW_Open", 0);

            LD.TopCstLoader.MutingSwitch.YB_OnOff                           /**/= AddressMgr.GetAddress("1_1_LD_투입_배출_Safety_Muting_On_Off", 0);
            LD.BotCstLoader.MutingSwitch.YB_OnOff                           /**/= AddressMgr.GetAddress("1_2_LD_투입_배출_Safety_Muting_On_Off", 0);


            LD.TopCstLoader.LiftMutingSwitch1.YB_OnOff                      /**/= AddressMgr.GetAddress("1_1_LD_Lifter_SAFETY_Muting_On_Off", 0);
            LD.TopCstLoader.LiftMutingSwitch2.YB_OnOff                      /**/= AddressMgr.GetAddress("1_2_LD_Lifter_SAFETY_Muting_On_Off", 0);
            LD.BotCstLoader.LiftMutingSwitch1.YB_OnOff                      /**/= AddressMgr.GetAddress("2_1_LD_Lifter_SAFETY_Muting_On_Off", 0);
            LD.BotCstLoader.LiftMutingSwitch2.YB_OnOff                      /**/= AddressMgr.GetAddress("2_2_LD_Lifter_SAFETY_Muting_On_Off", 0);

            LD.InnerLamp.YB_OnOff                                           /**/= AddressMgr.GetAddress("설비_내부_형광등_1_ON_OFF", 0);

            LD.Loader.HandPressureValue1                                    /**/= AddressMgr.GetAddress("1_Cell_취출_HAND_Pressure_Data", 0);
            LD.Loader.HandPressureValue2                                    /**/= AddressMgr.GetAddress("2_Cell_취출_HAND_Pressure_Data", 0);


            LD.TopLoaderTransfer.HandPressureValue1                         /**/= AddressMgr.GetAddress("1_LD_Transfer_Pressure_Data", 0);
            LD.TopLoaderTransfer.HandPressureValue2                         /**/= AddressMgr.GetAddress("2_LD_Transfer_Pressure_Data", 0);

            LD.BotLoaderTransfer.HandPressureValue1                         /**/= AddressMgr.GetAddress("3_LD_Transfer_Pressure_Data", 0);
            LD.BotLoaderTransfer.HandPressureValue2                         /**/= AddressMgr.GetAddress("4_LD_Transfer_Pressure_Data", 0);

            LD.MainCDAPressurValuie1                                        /**/= AddressMgr.GetAddress("1_Main_AIR_Flowmeter_Data", 0);
            LD.MainCDAPressurValuie2                                        /**/= AddressMgr.GetAddress("2_Main_AIR_Flowmeter_Data", 0);
            LD.MainCDAPressurValuie3                                        /**/= AddressMgr.GetAddress("3_Main_AIR_Flowmeter_Data", 0);
            LD.PanelInnerTempValue                                          /**/= AddressMgr.GetAddress("LD부_판넬_온도", 0);

            // LOADER UNIT END




            // PROCESS UNIT START
            PROC.MCOff1.XB_OnOff                                        /**/= AddressMgr.GetAddress("가공부_MC_1_Off", 0);
            PROC.MCOff2.XB_OnOff                                        /**/= AddressMgr.GetAddress("가공부_MC_2_Off", 0);

            PROC.EmergencyStopEnableGripSwitch.XB_OnOff                 /**/= AddressMgr.GetAddress("2_Emergency_Stop_Enable_Grip_SW", 0);
            PROC.SafetyEnableGripSwitchOn.XB_OnOff                      /**/= AddressMgr.GetAddress("2_Safety_Enable_Grip_SW_On", 0);
            PROC.Door4.XB_OnOff                                         /**/= AddressMgr.GetAddress("4_Safety_Door_SW_Open", 0);
            PROC.Door5.XB_OnOff                                         /**/= AddressMgr.GetAddress("5_Safety_Door_SW_Open", 0);
            PROC.Door6.XB_OnOff                                         /**/= AddressMgr.GetAddress("6_Safety_Door_SW_Open", 0);
            PROC.Door7.XB_OnOff                                         /**/= AddressMgr.GetAddress("7_Safety_Door_SW_Open", 0);
            PROC.Door8.XB_OnOff                                         /**/= AddressMgr.GetAddress("8_Safety_Door_SW_Open", 0);
            PROC.LaserAlarm.XB_OnOff                                    /**/= AddressMgr.GetAddress("Laser_Alarm", 0);
            PROC.LaserOn.XB_OnOff                                       /**/= AddressMgr.GetAddress("Laser_On", 0);


            PROC.CpBoxTempResetSwitch.XB_OnOff                          /**/= AddressMgr.GetAddress("가공부_CPBOX_TEMP_RESET_SW", 0);

            PROC.LaserHead.UnDownCylinder1.XB_BackwardComplete          /**/= AddressMgr.GetAddress("1_Laser_HEAD_UP_Cylinder_Sensor", 0);
            PROC.LaserHead.UnDownCylinder1.XB_ForwardComplete           /**/= AddressMgr.GetAddress("1_Laser_HEAD_DOWN_Cylinder_Sensor", 0);
            PROC.LaserHead.UnDownCylinder2.XB_BackwardComplete          /**/= AddressMgr.GetAddress("2_Laser_HEAD_UP_Cylinder_Sensor", 0);
            PROC.LaserHead.UnDownCylinder2.XB_ForwardComplete           /**/= AddressMgr.GetAddress("2_Laser_HEAD_DOWN_Cylinder_Sensor", 0);

            PROC.BreakingHead.HeaderX1X2충돌방지.XB_OnOff               /**/= AddressMgr.GetAddress("Breaking_Head_X1_X2_충돌방지_센서", 0);
            PROC.DLMBlowerPressureSwitch.XB_OnOff                       /**/= AddressMgr.GetAddress("DLM_Blower_Pressure_SW_Feedback", 0);
            PROC.BreakingHead.HeaderX1X2충돌방지.XB_OnOff               /**/= AddressMgr.GetAddress("Breaking_Head_X1_X2_충돌방지_센서", 0);
            PROC.Process판넬화재Alarm.XB_OnOff                          /**/= AddressMgr.GetAddress("가공부_판넬_화재_알람", 0);
            PROC.Process판넬온도Alarm.XB_OnOff                          /**/= AddressMgr.GetAddress("가공부_판넬_온도_알람", 0);

            PROC.Leak원형PMC.XB_OnOff                                   /**/= AddressMgr.GetAddress("Leak_Sensor_원형_PMC", 0);
            PROC.DummyTankDetect.XB_OnOff                               /**/= AddressMgr.GetAddress("Dummy_TANK_Detect", 0);


            PROC.LaserHead.LaserShutterCylinder.XB_BackwardComplete     /**/= AddressMgr.GetAddress("Laser_Shutter_OPEN", 0);
            PROC.LaserHead.LaserShutterCylinder.XB_ForwardComplete      /**/= AddressMgr.GetAddress("Laser_Shutter_CLOSE", 0);

            PROC.TopProcessStage.BrushUpDownCylinder.XB_ForwardComplete     /**/= AddressMgr.GetAddress("1_가공_Stage_Brush_UP", 0);
            PROC.TopProcessStage.BrushUpDownCylinder.XB_BackwardComplete    /**/= AddressMgr.GetAddress("1_가공_Stage_Brush_DOWN", 0);
            PROC.BotProcessStage.BrushUpDownCylinder.XB_ForwardComplete     /**/= AddressMgr.GetAddress("2_가공_Stage_Brush_UP", 0);
            PROC.BotProcessStage.BrushUpDownCylinder.XB_BackwardComplete    /**/= AddressMgr.GetAddress("2_가공_Stage_Brush_DOWN", 0);

            PROC.TopBreakTransfer.UnDownCylinder1.XB_BackwardComplete           /**/= AddressMgr.GetAddress("1_After_IR_Cut_이재기_UP", 0);
            PROC.TopBreakTransfer.UnDownCylinder1.XB_ForwardComplete            /**/= AddressMgr.GetAddress("1_After_IR_Cut_이재기_DOWN", 0);
            PROC.TopBreakTransfer.UnDownCylinder2.XB_BackwardComplete           /**/= AddressMgr.GetAddress("2_After_IR_Cut_이재기_UP", 0);
            PROC.TopBreakTransfer.UnDownCylinder2.XB_ForwardComplete            /**/= AddressMgr.GetAddress("2_After_IR_Cut_이재기_DOWN", 0);


            PROC.BotBreakTransfer.UnDownCylinder1.XB_BackwardComplete           /**/= AddressMgr.GetAddress("3_After_IR_Cut_이재기_UP", 0);
            PROC.BotBreakTransfer.UnDownCylinder1.XB_ForwardComplete            /**/= AddressMgr.GetAddress("3_After_IR_Cut_이재기_DOWN", 0);
            PROC.BotBreakTransfer.UnDownCylinder2.XB_BackwardComplete           /**/= AddressMgr.GetAddress("4_After_IR_Cut_이재기_UP", 0);
            PROC.BotBreakTransfer.UnDownCylinder2.XB_ForwardComplete            /**/= AddressMgr.GetAddress("4_After_IR_Cut_이재기_DOWN", 0);


            PROC.TopBreakStage.BrushUnDownCylinder.XB_BackwardComplete           /**/= AddressMgr.GetAddress("1_Breaking_Stage_Brush_UP", 0);
            PROC.TopBreakStage.BrushUnDownCylinder.XB_ForwardComplete            /**/= AddressMgr.GetAddress("1_Breaking_Stage_Brush_DOWN", 0);
            PROC.BotBreakStage.BrushUnDownCylinder.XB_BackwardComplete           /**/= AddressMgr.GetAddress("2_Breaking_Stage_Brush_UP", 0);
            PROC.BotBreakStage.BrushUnDownCylinder.XB_ForwardComplete            /**/= AddressMgr.GetAddress("2_Breaking_Stage_Brush_DOWN", 0);



            PROC.LaserHead.DummyShutterCylinder1.XB_BackwardComplete        /**/= AddressMgr.GetAddress("1_Dummy_Shutter_OPEN", 0);
            PROC.LaserHead.DummyShutterCylinder1.XB_ForwardComplete         /**/= AddressMgr.GetAddress("1_Dummy_Shutter_CLOSE", 0);
            PROC.LaserHead.DummyShutterCylinder2.XB_BackwardComplete        /**/= AddressMgr.GetAddress("2_Dummy_Shutter_OPEN", 0);
            PROC.LaserHead.DummyShutterCylinder2.XB_ForwardComplete         /**/= AddressMgr.GetAddress("2_Dummy_Shutter_CLOSE", 0);

            PROC.LaserHead.DummyTankCylinder.XB_BackwardComplete            /**/= AddressMgr.GetAddress("Dummy_TANK_투입", 0);
            PROC.LaserHead.DummyTankCylinder.XB_ForwardComplete             /**/= AddressMgr.GetAddress("Dummy_TANK_배출", 0);

            PROC.TopProcessStage.Vaccum1Ch1.XB_OnOff             /**/= AddressMgr.GetAddress("1_Cutting_Stage_1ch_Pressure_SW", 0);
            PROC.TopProcessStage.Vaccum1Ch2.XB_OnOff             /**/= AddressMgr.GetAddress("1_Cutting_Stage_2ch_Pressure_SW", 0);
            PROC.TopProcessStage.Vaccum2Ch1.XB_OnOff             /**/= AddressMgr.GetAddress("2_Cutting_Stage_1ch_Pressure_SW", 0);
            PROC.TopProcessStage.Vaccum2Ch2.XB_OnOff             /**/= AddressMgr.GetAddress("2_Cutting_Stage_2ch_Pressure_SW", 0);

            PROC.BotProcessStage.Vaccum1Ch1.XB_OnOff             /**/= AddressMgr.GetAddress("3_Cutting_Stage_1ch_Pressure_SW", 0);
            PROC.BotProcessStage.Vaccum1Ch2.XB_OnOff             /**/= AddressMgr.GetAddress("3_Cutting_Stage_2ch_Pressure_SW", 0);
            PROC.BotProcessStage.Vaccum2Ch1.XB_OnOff             /**/= AddressMgr.GetAddress("4_Cutting_Stage_1ch_Pressure_SW", 0);
            PROC.BotProcessStage.Vaccum2Ch2.XB_OnOff             /**/= AddressMgr.GetAddress("4_Cutting_Stage_2ch_Pressure_SW", 0);


            PROC.TopBreakStage.Vaccum1.XB_OnOff                 /**/= AddressMgr.GetAddress("1_Breaking_Stage_Pressure_SW", 0);
            PROC.TopBreakStage.Vaccum2.XB_OnOff                 /**/= AddressMgr.GetAddress("2_Breaking_Stage_Pressure_SW", 0);
            PROC.BotBreakStage.Vaccum1.XB_OnOff                 /**/= AddressMgr.GetAddress("3_Breaking_Stage_Pressure_SW", 0);
            PROC.BotBreakStage.Vaccum2.XB_OnOff                 /**/= AddressMgr.GetAddress("4_Breaking_Stage_Pressure_SW", 0);


            PROC.TopBreakTransfer.Vaccum1.XB_OnOff             /**/= AddressMgr.GetAddress("1_After_IRCut_Transfer_Pressure_SW", 0);
            PROC.TopBreakTransfer.Vaccum2.XB_OnOff             /**/= AddressMgr.GetAddress("2_After_IRCut_Transfer_Pressure_SW", 0);
            PROC.BotBreakTransfer.Vaccum1.XB_OnOff             /**/= AddressMgr.GetAddress("3_After_IRCut_Transfer_Pressure_SW", 0);
            PROC.BotBreakTransfer.Vaccum2.XB_OnOff             /**/= AddressMgr.GetAddress("4_After_IRCut_Transfer_Pressure_SW", 0);


            PROC.ProcessFanStopAlarm01.XB_OnOff              /**/= AddressMgr.GetAddress("1_가공부_FAN_Stop_Alarm", 0);
            PROC.ProcessFanStopAlarm02.XB_OnOff              /**/= AddressMgr.GetAddress("2_가공부_FAN_Stop_Alarm", 0);
            PROC.ProcessFanStopAlarm03.XB_OnOff              /**/= AddressMgr.GetAddress("3_가공부_FAN_Stop_Alarm", 0);
            PROC.ProcessFanStopAlarm04.XB_OnOff              /**/= AddressMgr.GetAddress("4_가공부_FAN_Stop_Alarm", 0);
            PROC.ProcessFanStopAlarm05.XB_OnOff              /**/= AddressMgr.GetAddress("5_가공부_FAN_Stop_Alarm", 0);
            PROC.ProcessFanStopAlarm06.XB_OnOff              /**/= AddressMgr.GetAddress("6_가공부_FAN_Stop_Alarm", 0);
            PROC.ProcessFanStopAlarm07.XB_OnOff              /**/= AddressMgr.GetAddress("7_가공부_FAN_Stop_Alarm", 0);
            PROC.ProcessFanStopAlarm08.XB_OnOff              /**/= AddressMgr.GetAddress("8_가공부_FAN_Stop_Alarm", 0);
            PROC.ProcessFanStopAlarm09.XB_OnOff              /**/= AddressMgr.GetAddress("9_가공부_FAN_Stop_Alarm", 0);
            PROC.ProcessFanStopAlarm10.XB_OnOff              /**/= AddressMgr.GetAddress("10_가공부_FAN_Stop_Alarm", 0);
            PROC.ProcessFanStopAlarm11.XB_OnOff              /**/= AddressMgr.GetAddress("11_가공부_FAN_Stop_Alarm", 0);
            PROC.ProcessFanStopAlarm12.XB_OnOff              /**/= AddressMgr.GetAddress("12_가공부_FAN_Stop_Alarm", 0);

            PROC.IonizerRun1.XB_OnOff                   /**/= AddressMgr.GetAddress("1_Ionizer_RUN", 0);
            PROC.IonizerAlarm1_1.XB_OnOff               /**/= AddressMgr.GetAddress("1_1_Ionizer_Alarm", 0);
            PROC.IonizerAlarm1_2.XB_OnOff               /**/= AddressMgr.GetAddress("1_2_Ionizer_Alarm", 0);
            PROC.IonizerRun2.XB_OnOff                   /**/= AddressMgr.GetAddress("2_Ionizer_RUN", 0);
            PROC.IonizerAlarm2_1.XB_OnOff               /**/= AddressMgr.GetAddress("2_1_Ionizer_Alarm", 0);
            PROC.IonizerAlarm2_2.XB_OnOff               /**/= AddressMgr.GetAddress("2_2_Ionizer_Alarm", 0);
            PROC.IonizerRun3.XB_OnOff                   /**/= AddressMgr.GetAddress("3_Ionizer_RUN", 0);
            PROC.IonizerAlarm3_1.XB_OnOff               /**/= AddressMgr.GetAddress("3_1_Ionizer_Alarm", 0);
            PROC.IonizerAlarm3_2.XB_OnOff               /**/= AddressMgr.GetAddress("3_2_Ionizer_Alarm", 0);
            PROC.IonizerRun4.XB_OnOff                   /**/= AddressMgr.GetAddress("4_Ionizer_RUN", 0);
            PROC.IonizerAlarm4_1.XB_OnOff               /**/= AddressMgr.GetAddress("4_1_Ionizer_Alarm", 0);
            PROC.IonizerAlarm4_2.XB_OnOff               /**/= AddressMgr.GetAddress("4_2_Ionizer_Alarm", 0);


            PROC.MotorControl.YB_OnOff                  /**/= AddressMgr.GetAddress("가공부_Motor_Control_On", 0);
            PROC.CpBoxTempResetSwitch.YB_OnOff          /**/= AddressMgr.GetAddress("가공부_CPBOX_TEMP_RESET_SW_LAMP", 0);

            PROC.LaserStopInternal.YB_OnOff             /**/= AddressMgr.GetAddress("Laser_Stop_Internal_Interlock", 0);
            PROC.LaserStopExternal.YB_OnOff             /**/= AddressMgr.GetAddress("Laser_Stop_External_Interlock", 0);
            PROC.LaserEmergencyStop.YB_OnOff            /**/= AddressMgr.GetAddress("Laser_Emergency_Stop", 0);



            PROC.LaserHead.LaserShutterCylinder.YB_BackwardCmd      /**/= AddressMgr.GetAddress("Laser_Shutter_OPEN", 0);
            PROC.LaserHead.LaserShutterCylinder.YB_ForwardCmd       /**/= AddressMgr.GetAddress("Laser_Shutter_CLOSE", 0);

            PROC.TopProcessStage.BrushUpDownCylinder.YB_ForwardCmd  /**/= AddressMgr.GetAddress("1_가공_Stage_Brush_UP", 0);
            PROC.TopProcessStage.BrushUpDownCylinder.YB_BackwardCmd /**/= AddressMgr.GetAddress("1_가공_Stage_Brush_DOWN", 0);
            PROC.BotProcessStage.BrushUpDownCylinder.YB_ForwardCmd  /**/= AddressMgr.GetAddress("2_가공_Stage_Brush_UP", 0);
            PROC.BotProcessStage.BrushUpDownCylinder.YB_BackwardCmd /**/= AddressMgr.GetAddress("2_가공_Stage_Brush_DOWN", 0);

            PROC.TopBreakTransfer.UnDownCylinder1.YB_BackwardCmd        /**/= AddressMgr.GetAddress("1_After_IR_Cut_이재기_UP", 0);
            PROC.TopBreakTransfer.UnDownCylinder1.YB_ForwardCmd         /**/= AddressMgr.GetAddress("1_After_IR_Cut_이재기_DOWN", 0);
            PROC.TopBreakTransfer.UnDownCylinder2.YB_BackwardCmd        /**/= AddressMgr.GetAddress("2_After_IR_Cut_이재기_UP", 0);
            PROC.TopBreakTransfer.UnDownCylinder2.YB_ForwardCmd         /**/= AddressMgr.GetAddress("2_After_IR_Cut_이재기_DOWN", 0);


            PROC.BotBreakTransfer.UnDownCylinder1.YB_BackwardCmd        /**/= AddressMgr.GetAddress("3_After_IR_Cut_이재기_UP", 0);
            PROC.BotBreakTransfer.UnDownCylinder1.YB_ForwardCmd         /**/= AddressMgr.GetAddress("3_After_IR_Cut_이재기_DOWN", 0);
            PROC.BotBreakTransfer.UnDownCylinder2.YB_BackwardCmd        /**/= AddressMgr.GetAddress("4_After_IR_Cut_이재기_UP", 0);
            PROC.BotBreakTransfer.UnDownCylinder2.YB_ForwardCmd         /**/= AddressMgr.GetAddress("4_After_IR_Cut_이재기_DOWN", 0);


            PROC.TopBreakStage.BrushUnDownCylinder.YB_BackwardCmd            /**/= AddressMgr.GetAddress("1_Breaking_Stage_Brush_UP", 0);
            PROC.TopBreakStage.BrushUnDownCylinder.YB_ForwardCmd             /**/= AddressMgr.GetAddress("1_Breaking_Stage_Brush_DOWN", 0);
            PROC.BotBreakStage.BrushUnDownCylinder.YB_BackwardCmd            /**/= AddressMgr.GetAddress("2_Breaking_Stage_Brush_UP", 0);
            PROC.BotBreakStage.BrushUnDownCylinder.YB_ForwardCmd             /**/= AddressMgr.GetAddress("2_Breaking_Stage_Brush_DOWN", 0);


            PROC.LaserHead.DummyShutterCylinder1.YB_BackwardCmd             /**/= AddressMgr.GetAddress("1_Dummy_Shutter_OPEN", 0);
            PROC.LaserHead.DummyShutterCylinder1.YB_ForwardCmd              /**/= AddressMgr.GetAddress("1_Dummy_Shutter_CLOSE", 0);

            PROC.LaserHead.DummyShutterCylinder2.YB_BackwardCmd             /**/= AddressMgr.GetAddress("2_Dummy_Shutter_OPEN", 0);
            PROC.LaserHead.DummyShutterCylinder2.YB_ForwardCmd              /**/= AddressMgr.GetAddress("2_Dummy_Shutter_CLOSE", 0);

            PROC.LaserHead.DummyTankCylinder.YB_BackwardCmd                 /**/= AddressMgr.GetAddress("Dummy_TANK_투입", 0);
            PROC.LaserHead.DummyTankCylinder.YB_ForwardCmd                  /**/= AddressMgr.GetAddress("Dummy_TANK_배출", 0);

            PROC.LaserHead.UnDownCylinder1.YB_BackwardCmd                   /**/= AddressMgr.GetAddress("Laser_HEAD_UP_Cylinder", 0);
            PROC.LaserHead.UnDownCylinder1.YB_ForwardCmd                    /**/= AddressMgr.GetAddress("Laser_HEAD_DOWN_Cylinder", 0);


            PROC.TopProcessStage.Vaccum1Ch1.YB_OnOff                        /**/= AddressMgr.GetAddress("1_컷팅_Stage_1ch_Vacuum_ON", 0);
            PROC.TopProcessStage.Blower1Ch1.YB_OnOff                        /**/= AddressMgr.GetAddress("1_컷팅_Stage_1ch_파기_ON", 0);
            PROC.TopProcessStage.Vaccum1Ch2.YB_OnOff                        /**/= AddressMgr.GetAddress("1_컷팅_Stage_2ch_Vacuum_ON", 0);
            PROC.TopProcessStage.Blower1Ch2.YB_OnOff                        /**/= AddressMgr.GetAddress("1_컷팅_Stage_2ch_파기_ON", 0);

            PROC.TopProcessStage.Vaccum2Ch1.YB_OnOff                        /**/= AddressMgr.GetAddress("2_컷팅_Stage_1ch_Vacuum_ON", 0);
            PROC.TopProcessStage.Blower2Ch1.YB_OnOff                        /**/= AddressMgr.GetAddress("2_컷팅_Stage_1ch_파기_ON", 0);
            PROC.TopProcessStage.Vaccum2Ch2.YB_OnOff                        /**/= AddressMgr.GetAddress("2_컷팅_Stage_2ch_Vacuum_ON", 0);
            PROC.TopProcessStage.Blower2Ch2.YB_OnOff                        /**/= AddressMgr.GetAddress("2_컷팅_Stage_2ch_파기_ON", 0);


            PROC.BotProcessStage.Vaccum1Ch1.YB_OnOff                        /**/= AddressMgr.GetAddress("3_컷팅_Stage_1ch_Vacuum_ON", 0);
            PROC.BotProcessStage.Blower1Ch1.YB_OnOff                        /**/= AddressMgr.GetAddress("3_컷팅_Stage_1ch_파기_ON", 0);
            PROC.BotProcessStage.Vaccum1Ch2.YB_OnOff                        /**/= AddressMgr.GetAddress("3_컷팅_Stage_2ch_Vacuum_ON", 0);
            PROC.BotProcessStage.Blower1Ch2.YB_OnOff                        /**/= AddressMgr.GetAddress("3_컷팅_Stage_2ch_파기_ON", 0);

            PROC.BotProcessStage.Vaccum2Ch1.YB_OnOff                        /**/= AddressMgr.GetAddress("4_컷팅_Stage_1ch_Vacuum_ON", 0);
            PROC.BotProcessStage.Blower2Ch1.YB_OnOff                        /**/= AddressMgr.GetAddress("4_컷팅_Stage_1ch_파기_ON", 0);
            PROC.BotProcessStage.Vaccum2Ch2.YB_OnOff                        /**/= AddressMgr.GetAddress("4_컷팅_Stage_2ch_Vacuum_ON", 0);
            PROC.BotProcessStage.Blower2Ch2.YB_OnOff                        /**/= AddressMgr.GetAddress("4_컷팅_Stage_2ch_파기_ON", 0);

            PROC.TopBreakTransfer.Vaccum1.YB_On                             /**/= AddressMgr.GetAddress("1_After_IR컷_이재기_1ch_Vac_ON", 0);
            PROC.TopBreakTransfer.Vaccum1.YB_Off                            /**/= AddressMgr.GetAddress("1_After_IR컷_이재기_1ch_Vac_OFF", 0);
            PROC.TopBreakTransfer.Blower1.YB_OnOff                          /**/= AddressMgr.GetAddress("1_After_IR컷_이재기_1ch_Vac_OFF", 0);

            PROC.TopBreakTransfer.Vaccum2.YB_On                             /**/= AddressMgr.GetAddress("2_After_IR컷_이재기_1ch_Vac_ON", 0);
            PROC.TopBreakTransfer.Vaccum2.YB_Off                            /**/= AddressMgr.GetAddress("2_After_IR컷_이재기_1ch_Vac_OFF", 0);
            PROC.TopBreakTransfer.Blower2.YB_OnOff                          /**/= AddressMgr.GetAddress("2_After_IR컷_이재기_1ch_Vac_OFF", 0);

            PROC.BotBreakTransfer.Vaccum1.YB_On                             /**/= AddressMgr.GetAddress("3_After_IR컷_이재기_1ch_Vac_ON", 0);
            PROC.BotBreakTransfer.Vaccum1.YB_Off                            /**/= AddressMgr.GetAddress("3_After_IR컷_이재기_1ch_Vac_OFF", 0);
            PROC.BotBreakTransfer.Blower1.YB_OnOff                          /**/= AddressMgr.GetAddress("3_After_IR컷_이재기_1ch_Vac_OFF", 0);

            PROC.BotBreakTransfer.Vaccum2.YB_On                             /**/= AddressMgr.GetAddress("4_After_IR컷_이재기_1ch_Vac_ON", 0);
            PROC.BotBreakTransfer.Vaccum2.YB_Off                            /**/= AddressMgr.GetAddress("4_After_IR컷_이재기_1ch_Vac_OFF", 0);
            PROC.BotBreakTransfer.Blower2.YB_OnOff                          /**/= AddressMgr.GetAddress("4_After_IR컷_이재기_1ch_Vac_OFF", 0);

            PROC.DLMBlowerPressureSwitch.YB_OnOff                           /**/= AddressMgr.GetAddress("DLM_Blower_On/Off", 0);

            //PROC.TopBreakStage.Blower.YB_OnOff                              /**/= AddressMgr.GetAddress("1_Breaking_Blower_ON", 0);
            //PROC.BotBreakStage.Blower.YB_OnOff                              /**/= AddressMgr.GetAddress("2_Breaking_Blower_ON", 0);


            PROC.TopBreakStage.Vaccum1.YB_OnOff     /**/= AddressMgr.GetAddress("1_Breaking_Stage_Vacuum_ON", 0);
            PROC.TopBreakStage.Blower1.YB_OnOff     /**/= AddressMgr.GetAddress("1_Breaking_Stage_파기_ON", 0);
            PROC.TopBreakStage.Vaccum2.YB_OnOff     /**/= AddressMgr.GetAddress("2_Breaking_Stage_Vacuum_ON", 0);
            PROC.TopBreakStage.Blower2.YB_OnOff     /**/= AddressMgr.GetAddress("2_Breaking_Stage_파기_ON", 0);

            PROC.BotBreakStage.Vaccum1.YB_OnOff     /**/= AddressMgr.GetAddress("3_Breaking_Stage_Vacuum_ON", 0);
            PROC.BotBreakStage.Blower1.YB_OnOff     /**/= AddressMgr.GetAddress("3_Breaking_Stage_파기_ON", 0);
            PROC.BotBreakStage.Vaccum2.YB_OnOff     /**/= AddressMgr.GetAddress("4_Breaking_Stage_Vacuum_ON", 0);
            PROC.BotBreakStage.Blower2.YB_OnOff     /**/= AddressMgr.GetAddress("4_Breaking_Stage_파기_ON", 0);


            PROC.Door4.YB_OnOff                     /**/= AddressMgr.GetAddress("4_Safety_Door_SW_Open", 0);
            PROC.Door5.YB_OnOff                     /**/= AddressMgr.GetAddress("5_Safety_Door_SW_Open", 0);
            PROC.Door6.YB_OnOff                     /**/= AddressMgr.GetAddress("6_Safety_Door_SW_Open", 0);
            PROC.Door7.YB_OnOff                     /**/= AddressMgr.GetAddress("7_Safety_Door_SW_Open", 0);
            PROC.Door8.YB_OnOff                     /**/= AddressMgr.GetAddress("7,_8_Safety_Door_SW_Open", 0);
            PROC.InnerLamp.YB_OnOff                 /**/= AddressMgr.GetAddress("설비_내부_형광등_2_On_Off", 0);
            PROC.LaserOnDisplayBoard.YB_OnOff       /**/= AddressMgr.GetAddress("Laser_Head_Blow_On", 0);

            PROC.LaserHead.LaserInterferometerReset1.YB_OnOff  /**/= AddressMgr.GetAddress("Laser_Interferometer_변위센서_Reset1", 0);
            PROC.LaserHead.LaserInterferometerReset2.YB_OnOff  /**/= AddressMgr.GetAddress("Laser_Interferometer_변위센서_Reset2", 0);

            PROC.IonizerRunCmd1.YB_OnOff        /**/= AddressMgr.GetAddress("1_Ionizer_RUN_CMD", 0);
            PROC.IonizerAirRunCmd1.YB_OnOff     /**/= AddressMgr.GetAddress("1_Ionizer_AIR_RUN_CMD_SOL", 0);
            PROC.IonizerAirStopCmd1.YB_OnOff    /**/= AddressMgr.GetAddress("1_Ionizer_AIR_STOP_CMD_SOL", 0);

            PROC.IonizerRunCmd2.YB_OnOff        /**/= AddressMgr.GetAddress("2_Ionizer_RUN_CMD", 0);
            PROC.IonizerAirRunCmd2.YB_OnOff     /**/= AddressMgr.GetAddress("2_Ionizer_AIR_RUN_CMD_SOL", 0);
            PROC.IonizerAirStopCmd2.YB_OnOff    /**/= AddressMgr.GetAddress("2_Ionizer_AIR_STOP_CMD_SOL", 0);
            
            PROC.IonizerRunCmd3.YB_OnOff        /**/= AddressMgr.GetAddress("3_Ionizer_RUN_CMD", 0);
            PROC.IonizerAirRunCmd3.YB_OnOff     /**/= AddressMgr.GetAddress("3_Ionizer_AIR_RUN_CMD_SOL", 0);
            PROC.IonizerAirStopCmd3.YB_OnOff    /**/= AddressMgr.GetAddress("3_Ionizer_AIR_STOP_CMD_SOL", 0);

            PROC.IonizerRunCmd4.YB_OnOff        /**/= AddressMgr.GetAddress("4_Ionizer_RUN_CMD", 0);
            PROC.IonizerAirRunCmd4.YB_OnOff     /**/= AddressMgr.GetAddress("4_Ionizer_AIR_RUN_CMD_SOL", 0);
            PROC.IonizerAirStopCmd4.YB_OnOff    /**/= AddressMgr.GetAddress("4_Ionizer_AIR_STOP_CMD_SOL", 0);

            PROC.TopProcessStage.Vaccum1Ch1Value    /**/= AddressMgr.GetAddress("1_Cutting_Stage_1ch_압력", 0);
            PROC.TopProcessStage.Vaccum1Ch2Value    /**/= AddressMgr.GetAddress("1_Cutting_Stage_2ch_압력", 0);
            PROC.TopProcessStage.Vaccum2Ch1Value    /**/= AddressMgr.GetAddress("2_Cutting_Stage_1ch_압력", 0);
            PROC.TopProcessStage.Vaccum2Ch2Value    /**/= AddressMgr.GetAddress("2_Cutting_Stage_1ch_압력", 0);

            PROC.BotProcessStage.Vaccum1Ch1Value    /**/= AddressMgr.GetAddress("3_Cutting_Stage_1ch_압력", 0);
            PROC.BotProcessStage.Vaccum1Ch2Value    /**/= AddressMgr.GetAddress("3_Cutting_Stage_2ch_압력", 0);
            PROC.BotProcessStage.Vaccum2Ch1Value    /**/= AddressMgr.GetAddress("4_Cutting_Stage_1ch_압력", 0);
            PROC.BotProcessStage.Vaccum2Ch2Value    /**/= AddressMgr.GetAddress("4_Cutting_Stage_1ch_압력", 0);

            PROC.TopBreakStage.Vaccum1Value         /**/= AddressMgr.GetAddress("1_Breaking_Stage_압력", 0);
            PROC.TopBreakStage.Vaccum2Value         /**/= AddressMgr.GetAddress("2_Breaking_Stage_압력", 0);
            PROC.BotBreakStage.Vaccum1Value         /**/= AddressMgr.GetAddress("3_Breaking_Stage_압력", 0);
            PROC.BotBreakStage.Vaccum2Value         /**/= AddressMgr.GetAddress("4_Breaking_Stage_압력", 0);

            PROC.TopBreakTransfer.Vaccum1Value      /**/= AddressMgr.GetAddress("1_LD_이재기_1ch_압력", 0);
            PROC.TopBreakTransfer.Vaccum2Value      /**/= AddressMgr.GetAddress("2_LD_이재기_1ch_압력", 0);
            PROC.BotBreakTransfer.Vaccum1Value      /**/= AddressMgr.GetAddress("3_LD_이재기_1ch_압력", 0);
            PROC.BotBreakTransfer.Vaccum2Value      /**/= AddressMgr.GetAddress("4_LD_이재기_1ch_압력", 0);

            PROC.LaserHead.LaserInterferometerIRValue   /**/= AddressMgr.GetAddress("Laser_Interferometer_변위센서_IR", 0);
            PROC.LaserHead.LaserInterferometerBKValue   /**/= AddressMgr.GetAddress("Laser_Interferometer_변위센서_BK", 0);

            PROC.DLMBlowerPressureValue                 /**/= AddressMgr.GetAddress("DLM_Blower_Pressure_Data", 0);
            PROC.CpBoxTempValue                         /**/= AddressMgr.GetAddress("가공부_판넬_온도", 0);



            // PROCESS UNIT END




            // UNLOAD UNIT START
            UD.TopCstUnloader.MutingSwitch.XB_OnOff                           /**/= AddressMgr.GetAddress("ULD_A열_투입_Muting_Switch_On_Off_Use", 0);
            UD.BotCstUnloader.MutingSwitch.XB_OnOff                           /**/= AddressMgr.GetAddress("ULD_B열_투입_Muting_Switch_On_Off_Use", 0);

            UD.CpBoxTempResetSwitch.XB_OnOff                                /**/= AddressMgr.GetAddress("ULD_CPBOX_TEMP_Reset_Switch_User", 0);
            

            UD.TopCstUnloader.LiftMutingSwitch1.XB_OnOff                      /**/= AddressMgr.GetAddress("1_1_ULD_Lifter_Muting_On", 0);
            UD.TopCstUnloader.LiftMutingSwitch2.XB_OnOff                      /**/= AddressMgr.GetAddress("1_2_ULD_Lifter_Muting_On", 0);
            UD.BotCstUnloader.LiftMutingSwitch1.XB_OnOff                      /**/= AddressMgr.GetAddress("2_1_ULD_Lifter_Muting_On", 0);
            UD.BotCstUnloader.LiftMutingSwitch2.XB_OnOff                      /**/= AddressMgr.GetAddress("2_2_ULD_Lifter_Muting_On", 0);


            UD.ULDEmergencyStop4.XB_OnOff                                    /**/= AddressMgr.GetAddress("4_ULD_Emergency_Stop_Feedback", 0);
            UD.ULDEmergencyStop5.XB_OnOff                                    /**/= AddressMgr.GetAddress("5_ULD_Emergency_Stop_Feedback", 0);
            UD.ULDEmergencyStop6.XB_OnOff                                    /**/= AddressMgr.GetAddress("6_ULD_Emergency_Stop_Feedback", 0);
            UD.EmergencyStopEnalbeGripSwitch3.XB_OnOff                      /**/= AddressMgr.GetAddress("3_Emergency_Stop_Enable_Grip_SW", 0);
            UD.SafteyEnableGripSwitchOn3.XB_OnOff                           /**/= AddressMgr.GetAddress("3_Safety_Enable_Grip_SW_On_Feedback", 0);
            
            UD.Door09.XB_OnOff                                              /**/= AddressMgr.GetAddress("9_Safety_Door_SW_Open", 0);
            UD.Door10.XB_OnOff                                              /**/= AddressMgr.GetAddress("10_Safety_Door_SW_Open", 0);
            UD.Door11.XB_OnOff                                              /**/= AddressMgr.GetAddress("11_Safety_Door_SW_Open", 0);
            
            UD.ULDMcOff1.XB_OnOff                                            /**/= AddressMgr.GetAddress("ULD_MC_1_OFF", 0);
            UD.ULDMcOff2.XB_OnOff                                            /**/= AddressMgr.GetAddress("ULD_MC_2_OFF", 0);
            
            UD.ControlSelectULD.XB_OnOff                                     /**/= AddressMgr.GetAddress("Control_Select_ULD", 0);
            
            UD.Cell투입Hand출동방지.XB_OnOff                                /**/= AddressMgr.GetAddress("Cell_취출_Hand_충돌방지_센서", 0);

            UD.TopCstUnloader.ResetSwitch.XB_OnOff                            /**/= AddressMgr.GetAddress("ULD_A_Reset_Switch_User", 0);
            UD.BotCstUnloader.ResetSwitch.XB_OnOff                            /**/= AddressMgr.GetAddress("ULD_B_Reset_Switch_User", 0);
            
            UD.ULDIRCUT충돌방지Y1_Y3.XB_OnOff                                   /**/= AddressMgr.GetAddress("ULD_A열_IR_CUT_TR_충돌방지_센서_Y1_Y3", 0);
            UD.ULDIRCUT충돌방지Y2_Y4.XB_OnOff                                   /**/= AddressMgr.GetAddress("ULD_B열_IR_CUT_TR_충돌방지_센서_Y2_Y4", 0);
            UD.BufferVacuumPressureSwitch.XB_OnOff                             /**/= AddressMgr.GetAddress("Buffer_Vacuum_Pressure_SW", 0);
            UD.BufferUpCylinder.XB_OnOff                                       /**/= AddressMgr.GetAddress("Buffer_Up_Cylinder_Sensor", 0);
            UD.BufferDownCylinder.XB_OnOff                                     /**/= AddressMgr.GetAddress("Buffer_Down_Cylinder_Sensor", 0);

            UD.TopCstUnloader.ULDCasstteDetect_1.XB_OnOff                      /**/= AddressMgr.GetAddress("1_1_ULD_Cassette_Detect", 0);
            UD.TopCstUnloader.ULDCasstteDetect_2.XB_OnOff                      /**/= AddressMgr.GetAddress("1_2_ULD_Cassette_Detect", 0);
            UD.TopCstUnloader.ULDCasstteDetect_3.XB_OnOff                      /**/= AddressMgr.GetAddress("1_3_ULD_Cassette_Detect", 0);
            UD.TopCstUnloader.ULDCasstteDetect_4.XB_OnOff                      /**/= AddressMgr.GetAddress("1_4_ULD_Cassette_Detect", 0);
            UD.TopCstUnloader.ULDCasstteDetect_5.XB_OnOff                      /**/= AddressMgr.GetAddress("1_5_ULD_Cassette_Detect", 0);
            UD.TopCstUnloader.ULDCasstteDetect_6.XB_OnOff                      /**/= AddressMgr.GetAddress("1_6_ULD_Cassette_Detect", 0);
            
            UD.BotCstUnloader.ULDCasstteDetect_1.XB_OnOff                      /**/= AddressMgr.GetAddress("2_1_ULD_Cassette_Detect", 0);
            UD.BotCstUnloader.ULDCasstteDetect_2.XB_OnOff                      /**/= AddressMgr.GetAddress("2_2_ULD_Cassette_Detect", 0);
            UD.BotCstUnloader.ULDCasstteDetect_3.XB_OnOff                      /**/= AddressMgr.GetAddress("2_3_ULD_Cassette_Detect", 0);
            UD.BotCstUnloader.ULDCasstteDetect_4.XB_OnOff                      /**/= AddressMgr.GetAddress("2_4_ULD_Cassette_Detect", 0);
            UD.BotCstUnloader.ULDCasstteDetect_5.XB_OnOff                      /**/= AddressMgr.GetAddress("2_5_ULD_Cassette_Detect", 0);
            UD.BotCstUnloader.ULDCasstteDetect_6.XB_OnOff                      /**/= AddressMgr.GetAddress("2_6_ULD_Cassette_Detect", 0);
            
            UD.TopCstUnloader.CasstteGripCylinder.XB_ForwardCompleteP1        /**/= AddressMgr.GetAddress("1_1_ULD_Cassette_좌우_Grip_POS", 0);
            UD.TopCstUnloader.CasstteGripCylinder.XB_BackwardCompleteP1       /**/= AddressMgr.GetAddress("1_1_ULD_Cassette_좌우_Ungrip_POS", 0);
            UD.TopCstUnloader.CasstteGripCylinder.XB_ForwardCompleteP2        /**/= AddressMgr.GetAddress("1_2_ULD_Cassette_좌우_Grip_POS", 0);
            UD.TopCstUnloader.CasstteGripCylinder.XB_BackwardCompleteP2       /**/= AddressMgr.GetAddress("1_2_ULD_Cassette_좌우_Ungrip_POS", 0);
            UD.BotCstUnloader.CasstteGripCylinder.XB_ForwardCompleteP1        /**/= AddressMgr.GetAddress("2_1_ULD_Cassette_좌우_Grip_POS", 0);
            UD.BotCstUnloader.CasstteGripCylinder.XB_BackwardCompleteP1       /**/= AddressMgr.GetAddress("2_1_ULD_Cassette_좌우_Ungrip_POS", 0);
            UD.BotCstUnloader.CasstteGripCylinder.XB_ForwardCompleteP2        /**/= AddressMgr.GetAddress("2_2_ULD_Cassette_좌우_Grip_POS", 0);
            UD.BotCstUnloader.CasstteGripCylinder.XB_BackwardCompleteP2       /**/= AddressMgr.GetAddress("2_2_ULD_Cassette_좌우_Ungrip_POS", 0);
            
            UD.TopUnloaderTransfer.UpDonw1Cylinder.XB_BackwardComplete        /**/= AddressMgr.GetAddress("1_ULD_Transfer_Up_Cylinder", 0);
            UD.TopUnloaderTransfer.UpDonw1Cylinder.XB_ForwardComplete         /**/= AddressMgr.GetAddress("1_ULD_Transfer_Down_Cylinder", 0);
            UD.TopUnloaderTransfer.UpDonw2Cylinder.XB_BackwardComplete        /**/= AddressMgr.GetAddress("2_ULD_Transfer_Up_Cylinder", 0);
            UD.TopUnloaderTransfer.UpDonw2Cylinder.XB_ForwardComplete         /**/= AddressMgr.GetAddress("2_ULD_Transfer_Down_Cylinder", 0);
            
            UD.BotUnloaderTransfer.UpDonw1Cylinder.XB_BackwardComplete        /**/= AddressMgr.GetAddress("3_ULD_Transfer_Up_Cylinder", 0);
            UD.BotUnloaderTransfer.UpDonw1Cylinder.XB_ForwardComplete         /**/= AddressMgr.GetAddress("3_ULD_Transfer_Down_Cylinder", 0);
            UD.BotUnloaderTransfer.UpDonw2Cylinder.XB_BackwardComplete        /**/= AddressMgr.GetAddress("4_ULD_Transfer_Up_Cylinder", 0);
            UD.BotUnloaderTransfer.UpDonw2Cylinder.XB_ForwardComplete         /**/= AddressMgr.GetAddress("4_ULD_Transfer_Down_Cylinder", 0);
            
            UD.TopCstUnloader.TiltCylinder.XB_BackwardComplete                /**/= AddressMgr.GetAddress("1_ULD_Cassette_Lift_Tilting_Sensor_UP", 0);
            UD.TopCstUnloader.TiltCylinder.XB_ForwardComplete                 /**/= AddressMgr.GetAddress("1_ULD_Cassette_Lift_Tilting_Sensor_DOWN", 0);
            UD.BotCstUnloader.TiltCylinder.XB_BackwardComplete                /**/= AddressMgr.GetAddress("2_ULD_Cassette_Lift_Tilting_Sensor_UP", 0);
            UD.BotCstUnloader.TiltCylinder.XB_BackwardComplete                /**/= AddressMgr.GetAddress("2_ULD_Cassette_Lift_Tilting_Sensor_DOWN", 0);

            UD.TopUnloaderTransfer.UpDonw1Cylinder.XB_BackwardComplete        /**/= AddressMgr.GetAddress("5_ULD_Transfer_Up_Cylinder", 0);
            UD.TopUnloaderTransfer.UpDonw1Cylinder.XB_ForwardComplete         /**/= AddressMgr.GetAddress("5_ULD_Transfer_Down_Cylinder", 0);
            UD.TopUnloaderTransfer.UpDonw2Cylinder.XB_BackwardComplete        /**/= AddressMgr.GetAddress("6_ULD_Transfer_Up_Cylinder", 0);
            UD.TopUnloaderTransfer.UpDonw2Cylinder.XB_ForwardComplete         /**/= AddressMgr.GetAddress("6_ULD_Transfer_Down_Cylinder", 0);

            UD.BotUnloaderTransfer.UpDonw1Cylinder.XB_BackwardComplete        /**/= AddressMgr.GetAddress("7_ULD_Transfer_Up_Cylinder", 0);
            UD.BotUnloaderTransfer.UpDonw1Cylinder.XB_ForwardComplete         /**/= AddressMgr.GetAddress("7_ULD_Transfer_Down_Cylinder", 0);
            UD.BotUnloaderTransfer.UpDonw2Cylinder.XB_BackwardComplete        /**/= AddressMgr.GetAddress("8_ULD_Transfer_Up_Cylinder", 0);
            UD.BotUnloaderTransfer.UpDonw2Cylinder.XB_ForwardComplete         /**/= AddressMgr.GetAddress("8_ULD_Transfer_Down_Cylinder", 0);

            UD.Unloader.Vaccum1.XB_OnOff                                      /**/= AddressMgr.GetAddress("1_Cell_투입_Hand_Pressure_SW", 0);
            UD.Unloader.Vaccum2.XB_OnOff                                      /**/= AddressMgr.GetAddress("2_Cell_투입_Hand_Pressure_SW", 0);
            
            UD.TopUnloaderTransfer.Vaccum1.XB_OnOff                           /**/= AddressMgr.GetAddress("1_ULD_Transfer_Vaccum_Pressure_SW", 0);
            UD.TopUnloaderTransfer.Vaccum2.XB_OnOff                           /**/= AddressMgr.GetAddress("2_ULD_Transfer_Vaccum_Pressure_SW", 0);
            UD.BotUnloaderTransfer.Vaccum1.XB_OnOff                           /**/= AddressMgr.GetAddress("3_ULD_Transfer_Vaccum_Pressure_SW", 0);
            UD.BotUnloaderTransfer.Vaccum2.XB_OnOff                           /**/= AddressMgr.GetAddress("4_ULD_Transfer_Vaccum_Pressure_SW", 0);
            UD.TopUnloaderTransfer.Vaccum1.XB_OnOff                           /**/= AddressMgr.GetAddress("5_ULD_Transfer_Vaccum_Pressure_SW", 0);
            UD.TopUnloaderTransfer.Vaccum2.XB_OnOff                           /**/= AddressMgr.GetAddress("6_ULD_Transfer_Vaccum_Pressure_SW", 0);
            UD.BotUnloaderTransfer.Vaccum1.XB_OnOff                           /**/= AddressMgr.GetAddress("7_ULD_Transfer_Vaccum_Pressure_SW", 0);
            UD.BotUnloaderTransfer.Vaccum2.XB_OnOff                           /**/= AddressMgr.GetAddress("8_ULD_Transfer_Vaccum_Pressure_SW", 0);

            UD.InspectionStagePressureSwitch1.XB_OnOff                           /**/= AddressMgr.GetAddress("1_Inspection_Stage_Pressure_SW", 0);
            UD.InspectionStagePressureSwitch2.XB_OnOff                           /**/= AddressMgr.GetAddress("2_Inspection_Stage_Pressure_SW", 0);
            UD.InspectionStagePressureSwitch3.XB_OnOff                           /**/= AddressMgr.GetAddress("3_Inspection_Stage_Pressure_SW", 0);
            UD.InspectionStagePressureSwitch4.XB_OnOff                           /**/= AddressMgr.GetAddress("4_Inspection_Stage_Pressure_SW", 0);


            UD.ULDLightCurtain감지.XB_OnOff                                  /**/= AddressMgr.GetAddress("ULD_Light_Curtain_감지", 0);
            UD.ULDFanStopAlarm1.XB_OnOff                                     /**/= AddressMgr.GetAddress("1_ULD_FAN_Stop_Alarm", 0);
            UD.ULDFanStopAlarm2.XB_OnOff                                     /**/= AddressMgr.GetAddress("2_ULD_FAN_Stop_Alarm", 0);
            UD.ULDFanStopAlarm3.XB_OnOff                                     /**/= AddressMgr.GetAddress("3_ULD_FAN_Stop_Alarm", 0);
            UD.ULDFanStopAlarm4.XB_OnOff                                     /**/= AddressMgr.GetAddress("4_ULD_FAN_Stop_Alarm", 0);
            UD.ULDFanStopAlarm5.XB_OnOff                                     /**/= AddressMgr.GetAddress("5_ULD_FAN_Stop_Alarm", 0);
            UD.ULDFanStopAlarm6.XB_OnOff                                     /**/= AddressMgr.GetAddress("6_ULD_FAN_Stop_Alarm", 0);
            UD.ULD판넬화재알람.XB_OnOff                                      /**/= AddressMgr.GetAddress("ULD부_판넬_화재_알람", 0);
            UD.ULD판넬온도알람.XB_OnOff                                      /**/= AddressMgr.GetAddress("ULD부_판넬_온도_알람", 0);
            UD.PCRack화재알람.XB_OnOff                                       /**/= AddressMgr.GetAddress("PC_Rack_화재_알람", 0);

            //OUT 
            UD.ULDMotorControlOn.YB_OnOff                                   /**/= AddressMgr.GetAddress("ULD_Motor_Control_On_CMD", 0);

            UD.TowerLampR.YB_OnOff                                         /**/= AddressMgr.GetAddress("2_Tower_Lamp_R", 0);
            UD.TowerLampY.YB_OnOff                                         /**/= AddressMgr.GetAddress("2_Tower_Lamp_Y", 0);
            UD.TowerLampG.YB_OnOff                                         /**/= AddressMgr.GetAddress("2_Tower_Lamp_G", 0);
            
            UD.ControlSelectULDCMD.YB_OnOff                                /**/= AddressMgr.GetAddress("Control_Select_ULD", 0);

            UD.TopCstUnloader.Buzzer.YB_OnOff                               /**/= AddressMgr.GetAddress("ULD_A열_Cassette_투입_배출_Buzzer", 0);
            UD.TopCstUnloader.Buzzer.YB_OnOff                               /**/= AddressMgr.GetAddress("ULD_B열_Cassette_투입_배출_Buzzer", 0);

            UD.TopCstUnloader.ResetSwitch.YB_OnOff                           /**/= AddressMgr.GetAddress("ULD_A열_Reset_Switch_Lamp", 0);
            UD.BotCstUnloader.ResetSwitch.YB_OnOff                           /**/= AddressMgr.GetAddress("ULD_B열_Reset_Switch_Lamp", 0);

            UD.TopCstUnloader.MutingLamp.YB_OnOff                            /**/= AddressMgr.GetAddress("ULD_A열_Cassette_Muting_Switch_Lamp", 0);
            UD.BotCstUnloader.MutingLamp.YB_OnOff                            /**/= AddressMgr.GetAddress("ULD_B열_Cassette_Muting_Switch_Lamp", 0);
            UD.CpBoxTempResetSwitch.YB_OnOff                                 /**/= AddressMgr.GetAddress("ULD_CPBOX_TEMP_RESET_SW_LAMP", 0);

            UD.BufferUpCylinder.XB_OnOff                                       /**/= AddressMgr.GetAddress("Buffer_Up_CMD", 0);
            UD.BufferDownCylinder.XB_OnOff                                     /**/= AddressMgr.GetAddress("Buffer_Down_CMD", 0);

            UD.TopCstUnloader.CasstteGripCylinder.YB_ForwardCmd              /**/= AddressMgr.GetAddress("1_ULD_Casstte_Grip", 0);
            UD.TopCstUnloader.CasstteGripCylinder.YB_BackwardCmd             /**/= AddressMgr.GetAddress("1_ULD_Casstte_UnGrip", 0);
            
            UD.BotCstUnloader.CasstteGripCylinder.YB_ForwardCmd              /**/= AddressMgr.GetAddress("2_ULD_Casstte_Grip", 0);
            UD.BotCstUnloader.CasstteGripCylinder.YB_BackwardCmd             /**/= AddressMgr.GetAddress("2_ULD_Casstte_UnGrip", 0);
            

            UD.TopUnloaderTransfer.UpDonw1Cylinder.YB_BackwardCmd             /**/= AddressMgr.GetAddress("1_ULD_Transfer_Up_Cylinder", 0);
            UD.TopUnloaderTransfer.UpDonw1Cylinder.YB_ForwardCmd              /**/= AddressMgr.GetAddress("1_ULD_Transfer_Down_Cylinder", 0);
            UD.TopUnloaderTransfer.UpDonw2Cylinder.YB_BackwardCmd             /**/= AddressMgr.GetAddress("2_ULD_Transfer_Up_Cylinder", 0);
            UD.TopUnloaderTransfer.UpDonw2Cylinder.YB_ForwardCmd              /**/= AddressMgr.GetAddress("2_ULD_Transfer_Down_Cylinder", 0);
                                                                                                             
            UD.BotUnloaderTransfer.UpDonw1Cylinder.YB_BackwardCmd             /**/= AddressMgr.GetAddress("3_ULD_Transfer_Up_Cylinder", 0);
            UD.BotUnloaderTransfer.UpDonw1Cylinder.YB_ForwardCmd              /**/= AddressMgr.GetAddress("3_ULD_Transfer_Down_Cylinder", 0);
            UD.BotUnloaderTransfer.UpDonw2Cylinder.YB_BackwardCmd             /**/= AddressMgr.GetAddress("4_ULD_Transfer_Up_Cylinder", 0);
            UD.BotUnloaderTransfer.UpDonw2Cylinder.YB_ForwardCmd              /**/= AddressMgr.GetAddress("4_ULD_Transfer_Down_Cylinder", 0);
                                                                                                             
            UD.TopUnloaderTransfer.UpDonw1Cylinder.YB_BackwardCmd             /**/= AddressMgr.GetAddress("5_ULD_Transfer_Up_Cylinder", 0);
            UD.TopUnloaderTransfer.UpDonw1Cylinder.YB_ForwardCmd              /**/= AddressMgr.GetAddress("5_ULD_Transfer_Down_Cylinder", 0);
            UD.TopUnloaderTransfer.UpDonw2Cylinder.YB_BackwardCmd             /**/= AddressMgr.GetAddress("6_ULD_Transfer_Up_Cylinder", 0);
            UD.TopUnloaderTransfer.UpDonw2Cylinder.YB_ForwardCmd              /**/= AddressMgr.GetAddress("6_ULD_Transfer_Down_Cylinder", 0);

            UD.BotUnloaderTransfer.UpDonw1Cylinder.YB_BackwardCmd             /**/= AddressMgr.GetAddress("7_ULD_Transfer_Up_Cylinder", 0);
            UD.BotUnloaderTransfer.UpDonw1Cylinder.YB_ForwardCmd              /**/= AddressMgr.GetAddress("7_ULD_Transfer_Down_Cylinder", 0);
            UD.BotUnloaderTransfer.UpDonw2Cylinder.YB_BackwardCmd             /**/= AddressMgr.GetAddress("8_ULD_Transfer_Up_Cylinder", 0);
            UD.BotUnloaderTransfer.UpDonw2Cylinder.YB_ForwardCmd              /**/= AddressMgr.GetAddress("8_ULD_Transfer_Down_Cylinder", 0);

            UD.TopCstUnloader.TiltCylinder.YB_ForwardCmd                      /**/= AddressMgr.GetAddress("1_ULD_Cassette_Lift_Tilting_Sensor_UP", 0);
            UD.TopCstUnloader.TiltCylinder.YB_BackwardCmd                     /**/= AddressMgr.GetAddress("1_ULD_Cassette_Lift_Tilting_Sensor_DOWN", 0);
                                                                                                             
            UD.BotCstUnloader.TiltCylinder.YB_ForwardCmd                      /**/= AddressMgr.GetAddress("2_ULD_Cassette_Lift_Tilting_Sensor_UP", 0);
            UD.BotCstUnloader.TiltCylinder.YB_BackwardCmd                     /**/= AddressMgr.GetAddress("2_ULD_Cassette_Lift_Tilting_Sensor_DOWN", 0);
            
            UD.Unloader.Vaccum1.YB_On                                         /**/= AddressMgr.GetAddress("1_Cassette_투입_이재기_Vacuum_ON", 0);
            UD.Unloader.Vaccum1.YB_Off                                        /**/= AddressMgr.GetAddress("1_Cassette_투입_이재기_Vacuum_OFF", 0);
            UD.Unloader.Blower1.YB_OnOff                                      /**/= AddressMgr.GetAddress("1_Cassette_투입_이재기_파기_AIR_ON", 0);
            
            UD.Unloader.Vaccum2.YB_On                                         /**/= AddressMgr.GetAddress("2_Cassette_투입_이재기_Vacuum_ON", 0);
            UD.Unloader.Vaccum2.YB_On                                         /**/= AddressMgr.GetAddress("2_Cassette_투입_이재기_Vacuum_ON", 0);
            UD.Unloader.Blower2.YB_OnOff                                      /**/= AddressMgr.GetAddress("2_Cassette_투입_이재기_파기_AIR_ON", 0);
            
            UD.TopUnloaderTransfer.Vaccum1.YB_On                              /**/= AddressMgr.GetAddress("1_ULD_Transfer_Vaccum_ON", 0);
            UD.TopUnloaderTransfer.Vaccum1.YB_Off                             /**/= AddressMgr.GetAddress("1_ULD_Transfer_Vaccum_OFF", 0);
            UD.TopUnloaderTransfer.Blower1.YB_OnOff                           /**/= AddressMgr.GetAddress("1_ULD_Transfer_파기_AIR_ON", 0);
            
            UD.TopUnloaderTransfer.Vaccum2.YB_On                              /**/= AddressMgr.GetAddress("2_ULD_Transfer_Vaccum_ON", 0);
            UD.TopUnloaderTransfer.Vaccum2.YB_Off                             /**/= AddressMgr.GetAddress("2_ULD_Transfer_Vaccum_OFF", 0);
            UD.TopUnloaderTransfer.Blower2.YB_OnOff                           /**/= AddressMgr.GetAddress("2_ULD_Transfer_파기_AIR_ON", 0);
            

            UD.BotUnloaderTransfer.Vaccum1.YB_On                              /**/= AddressMgr.GetAddress("3_ULD_Transfer_Vaccum_ON", 0);
            UD.BotUnloaderTransfer.Vaccum1.YB_Off                             /**/= AddressMgr.GetAddress("3_ULD_Transfer_Vaccum_OFF", 0);
            UD.BotUnloaderTransfer.Blower1.YB_OnOff                           /**/= AddressMgr.GetAddress("3_ULD_Transfer_파기_AIR_ON", 0);
            
            UD.BotUnloaderTransfer.Vaccum2.YB_On                              /**/= AddressMgr.GetAddress("4_ULD_Transfer_Vaccum_ON", 0);
            UD.BotUnloaderTransfer.Vaccum2.YB_Off                             /**/= AddressMgr.GetAddress("4_ULD_Transfer_Vaccum_OFF", 0);
            UD.BotUnloaderTransfer.Blower2.YB_OnOff                           /**/= AddressMgr.GetAddress("4_ULD_Transfer_파기_AIR_ON", 0);

            UD.TopUnloaderTransfer.Vaccum1.YB_On                              /**/= AddressMgr.GetAddress("5_ULD_Transfer_Vaccum_ON", 0);
            UD.TopUnloaderTransfer.Vaccum1.YB_Off                             /**/= AddressMgr.GetAddress("5_ULD_Transfer_Vaccum_OFF", 0);
            UD.TopUnloaderTransfer.Blower1.YB_OnOff                           /**/= AddressMgr.GetAddress("5_ULD_Transfer_파기_AIR_ON", 0);

            UD.TopUnloaderTransfer.Vaccum2.YB_On                              /**/= AddressMgr.GetAddress("6_ULD_Transfer_Vaccum_ON", 0);
            UD.TopUnloaderTransfer.Vaccum2.YB_Off                             /**/= AddressMgr.GetAddress("6_ULD_Transfer_Vaccum_OFF", 0);
            UD.TopUnloaderTransfer.Blower2.YB_OnOff                           /**/= AddressMgr.GetAddress("6_ULD_Transfer_파기_AIR_ON", 0);


            UD.BotUnloaderTransfer.Vaccum1.YB_On                              /**/= AddressMgr.GetAddress("7_ULD_Transfer_Vaccum_ON", 0);
            UD.BotUnloaderTransfer.Vaccum1.YB_Off                             /**/= AddressMgr.GetAddress("7_ULD_Transfer_Vaccum_OFF", 0);
            UD.BotUnloaderTransfer.Blower1.YB_OnOff                           /**/= AddressMgr.GetAddress("7_ULD_Transfer_파기_AIR_ON", 0);

            UD.BotUnloaderTransfer.Vaccum2.YB_On                              /**/= AddressMgr.GetAddress("8_ULD_Transfer_Vaccum_ON", 0);
            UD.BotUnloaderTransfer.Vaccum2.YB_Off                             /**/= AddressMgr.GetAddress("8_ULD_Transfer_Vaccum_OFF", 0);
            UD.BotUnloaderTransfer.Blower2.YB_OnOff                           /**/= AddressMgr.GetAddress("8_ULD_Transfer_파기_AIR_ON", 0);

            //UD.InspectionStage.Vaccum1.YB_On                           /**/= AddressMgr.GetAddress("1_Inspection_Stage_Vacuum_ON", 0);
            //UD.InspectionStage.Vaccum1.YB_Off                          /**/= AddressMgr.GetAddress("1_Inspection_Stage_Vacuum_OFF", 0);
            //UD.InspectionStage.Blower1.YB_OnOff                        /**/= AddressMgr.GetAddress("1_Inspection_Stage_파기_AIR_ON", 0);
            //
            //UD.InspectionStage.Vaccum2.YB_On                           /**/= AddressMgr.GetAddress("2_Inspection_Stage_Vacuum_ON", 0);
            //UD.InspectionStage.Vaccum2.YB_Off                          /**/= AddressMgr.GetAddress("2_Inspection_Stage_Vacuum_OFF", 0);
            //UD.InspectionStage.Blower2.YB_OnOff                        /**/= AddressMgr.GetAddress("2_Inspection_Stage_파기_AIR_ON", 0);
            //
            //UD.InspectionStage.Vaccum3.YB_On                           /**/= AddressMgr.GetAddress("3_Inspection_Stage_Vacuum_ON", 0);
            //UD.InspectionStage.Vaccum3.YB_Off                          /**/= AddressMgr.GetAddress("3_Inspection_Stage_Vacuum_OFF", 0);
            //UD.InspectionStage.Blower3.YB_OnOff                        /**/= AddressMgr.GetAddress("3_Inspection_Stage_파기_AIR_ON", 0);
            //
            //UD.InspectionStage.Vaccum4.YB_On                           /**/= AddressMgr.GetAddress("4_Inspection_Stage_Vacuum_ON", 0);
            //UD.InspectionStage.Vaccum4.YB_Off                          /**/= AddressMgr.GetAddress("4_Inspection_Stage_Vacuum_OFF", 0);
            //UD.InspectionStage.Blower4.YB_OnOff                        /**/= AddressMgr.GetAddress("4_Inspection_Stage_파기_AIR_ON", 0);
            //
            //UD.Buffer_1ch.Vaccum1.YB_On                                 /**/= AddressMgr.GetAddress("Buffer_1CH_Vacuum_ON_CMD", 0);
            //UD.Buffer_1ch.Vaccum1.YB_Off                                /**/= AddressMgr.GetAddress("Buffer_1CH_Vacuum_OFF_CMD", 0);
            //UD.Buffer_1ch.Blower1.YB_OnOff                              /**/= AddressMgr.GetAddress("Buffer_1CH_파기_AIR_ON_CMD", 0);

            UD.Door09.YB_OnOff                                              /**/= AddressMgr.GetAddress("9_Safety_Door_SW_Open", 0);
            UD.Door10.YB_OnOff                                              /**/= AddressMgr.GetAddress("10_Safety_Door_SW_Open", 0);
            UD.Door11.YB_OnOff                                              /**/= AddressMgr.GetAddress("11_Safety_Door_SW_Open", 0);
            
            UD.TopCstUnloader.MutingSwitch.YB_OnOff                           /**/= AddressMgr.GetAddress("1_1_ULD_투입_배출_Safety_Muting_On_Off", 0);
            UD.BotCstUnloader.MutingSwitch.YB_OnOff                           /**/= AddressMgr.GetAddress("1_2_ULD_투입_배출_Safety_Muting_On_Off", 0);
          

            UD.TopCstUnloader.LiftMutingSwitch1.YB_OnOff                      /**/= AddressMgr.GetAddress("1_1_ULD_Lifter_SAFETY_Muting_On_Off", 0);
            UD.TopCstUnloader.LiftMutingSwitch2.YB_OnOff                      /**/= AddressMgr.GetAddress("1_2_ULD_Lifter_SAFETY_Muting_On_Off", 0);
            UD.BotCstUnloader.LiftMutingSwitch1.YB_OnOff                      /**/= AddressMgr.GetAddress("2_1_ULD_Lifter_SAFETY_Muting_On_Off", 0);
            UD.BotCstUnloader.LiftMutingSwitch2.YB_OnOff                      /**/= AddressMgr.GetAddress("2_2_ULD_Lifter_SAFETY_Muting_On_Off", 0);
            
            UD.InnerLamp.YB_OnOff                                           /**/= AddressMgr.GetAddress("설비_내부_형광등_3_ON_OFF", 0);
            


            // UNLOAD UNIT END
        }


        public void LogicWorking()
        {
            LD.LogicWorking(this);
            PROC.LogicWorking(this);
            UD.LogicWorking(this);
        }
    }
}





