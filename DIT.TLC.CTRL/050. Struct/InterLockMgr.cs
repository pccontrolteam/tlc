﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public struct InterlockMsg
    {
        public string Interlock { get; set; }
        public string Detail { get; set; }
    }

    public class InterLockMgr
    {
        public static Queue<InterlockMsg> LstInterLock = new Queue<InterlockMsg>();
        public static void AddInterLock(string interLock, string detail = "")
        {
            lock (LstInterLock)
            {
                if (LstInterLock.Count < 1)
                    LstInterLock.Enqueue(new InterlockMsg {Interlock = interLock, Detail = detail});
            }
        }
    }
}
