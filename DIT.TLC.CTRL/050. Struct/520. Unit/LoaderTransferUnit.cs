﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.PLC;

namespace DIT.TLC.CTRL
{

    public enum EmLoaderTrnasferUnitCmd
    {
        S0000_WAIT,
        S1000_RECV_START,
        S2000_RECV_ING,
        S3000_RECV_ING,
        S4000,
        S5000,
        S6000,
        S7000,
        S8000,
        S9000,
        S0900,
        S0100
    }
    public class LoaderTransferUnit : BaseUnit
    {
        public LoaderTransferXAxisServo XAxis { get; set; }
        public LoaderTransferYAxisServo YAxis { get; set; }
        public LoaderTransferTAxisServo TAxis { get; set; }


        public Cylinder UpDonw1Cylinder = new Cylinder();
        public Cylinder UpDonw2Cylinder = new Cylinder();


        public Switch2Cmd1Sensor Vaccum1 = new Switch2Cmd1Sensor();
        public Switch2Cmd1Sensor Vaccum2 = new Switch2Cmd1Sensor();
        public SwitchOneWay Blower1 = new SwitchOneWay();
        public SwitchOneWay Blower2 = new SwitchOneWay();

        public EmLoaderTrnasferUnitCmd StepCmd { get; set; }

        public PioSendStep TPioSend { get; set; }
        public PioSendStep BPioSend { get; set; }
        public PioRecvStep PioRecv { get; set; }
        public PlcAddr HandPressureValue1 { get; internal set; }
        public PlcAddr HandPressureValue2 { get; internal set; }

        public override void LogicWorking(Equipment equip)
        {
            XAxis.LogicWorking(equip);
            YAxis.LogicWorking(equip);             
        }
    }
}
