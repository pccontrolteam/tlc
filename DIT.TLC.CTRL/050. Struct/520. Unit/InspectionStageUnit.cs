﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL._050._Struct._520._Unit
{
    public class InspectionStageUnit
    {

        public Switch2Cmd1Sensor Vaccum1 = new Switch2Cmd1Sensor();
        public Switch2Cmd1Sensor Vaccum2 = new Switch2Cmd1Sensor();
        public Switch2Cmd1Sensor Vaccum3 = new Switch2Cmd1Sensor();
        public Switch2Cmd1Sensor Vaccum4 = new Switch2Cmd1Sensor();
        public SwitchOneWay Blower1 = new SwitchOneWay();
        public SwitchOneWay Blower2 = new SwitchOneWay();
        public SwitchOneWay Blower3 = new SwitchOneWay();
        public SwitchOneWay Blower4 = new SwitchOneWay();
    }
}
