﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.PLC;

namespace DIT.TLC.CTRL
{
    public enum EmLoaderUnitCmd
    {
        S000_WAIT,
        CST_READY,
        CST_DOWN,
        S1010_RECV_WAIT,
        S1000_RECV_START,
        S1100_RECV_T_CST,
        RECV_B_CST,
        S1110_RECV_T_LOADER_ARM_FORWARD,
        S1120_RECV_T_CST_SLOT_DOWN_START,
        S1130_RECV_T_CST_SLOT_DOWN_WAIT,
        S1140_PNL_MOVE_SEND_POSI,
        S1150,
        S1160,
        S5100_RECV_B_CST,
        S5110_RECV_B_LOADER_ARM_FORWARD,
        S5120_RECV_B_CST_SLOT_DOWN_START,
        S5130_RECV_B_CST_SLOT_DOWN_WAIT,
        S5140_PNL_MOVE_SEND_POSI,
        S6000

    }

    public enum EmLoaderStep
    {
        LD_0000_WAIT,

    }

    public class LoaderUnit : BaseUnit
    {
        //액추에이터
        //센서
        public LoaderYAxisServo Y1Axis { get; set; }
        public LoaderYAxisServo Y2Axis { get; set; }
        public LoaderXAxisServo X1Axis { get; set; }
        public LoaderXAxisServo X2Axis { get; set; }

        public EmLoaderUnitCmd StepCmd { get; set; }

        public Switch2Cmd1Sensor Vaccum1    /**/= new Switch2Cmd1Sensor();
        public SwitchOneWay Blower1         /**/= new SwitchOneWay();

        public Switch2Cmd1Sensor Vaccum2    /**/= new Switch2Cmd1Sensor();
        public SwitchOneWay Blower2         /**/= new SwitchOneWay();


        public PioSendStep PioSendT { get; set; }
        public PioRecvStep PioRecvT { get; set; }

        public PioSendStep PioSendB { get; set; }
        public PioRecvStep PioRecvB { get; set; }

        public PlcAddr HandPressureValue1 { get; set; }
        public PlcAddr HandPressureValue2 { get; set; }

        public LoaderUnit()
        {
            StepCmd = EmLoaderUnitCmd.S000_WAIT;
        }
        public void StartActionCmd(EmLoaderUnitCmd cmd)
        {
        }
        public override void LogicWorking(Equipment equip)
        {
            base.LogicWorking(equip);
            
            Y1Axis.LogicWorking(equip);
            Y2Axis.LogicWorking(equip);
            X1Axis.LogicWorking(equip);
            X2Axis.LogicWorking(equip);
            
            LoaderCmdLogicWorking(equip);
            LoaderAutoLogicWorking();
        }
        public void LoaderCmdLogicWorking(Equipment equip)
        {
            if (equip.RunMode == EmEquipRunMode.Manual)
                return;

            //if (StepCmd == EmLoaderUnitCmd.S000_WAIT)
            //{
            //}
            //else if (StepCmd == EmLoaderUnitCmd.S1000_RECV_START)
            //{
            //    this.PioRecvT.StartPioRecv(equip, this);
            //    this.PioRecvB.StartPioRecv(equip, this);
            //}
            //else if (StepCmd == EmLoaderUnitCmd.S1010_RECV_WAIT)
            //{
            //    /***/
            //    if (this.PioRecvT.IsRecvStarted())
            //    {
            //        StepCmd = EmLoaderUnitCmd.S1100_RECV_T_CST;
            //    }
            //    else if (this.PioRecvB.IsRecvStarted())
            //    {
            //        StepCmd = EmLoaderUnitCmd.RECV_B_CST;
            //    }
            //}
            //else if (StepCmd == EmLoaderUnitCmd.S1100_RECV_T_CST)
            //{
            //    if (YAxis.IsMoveOnPosition(LoaderYAxisServo.TCstLoadingPosiNo) == false)
            //        YAxis.MovePosition(equip, LoaderYAxisServo.TCstLoadingPosiNo);

            //    StepCmd = EmLoaderUnitCmd.S1110_RECV_T_LOADER_ARM_FORWARD;
            //}
            //else if (StepCmd == EmLoaderUnitCmd.S1110_RECV_T_LOADER_ARM_FORWARD)
            //{
            //    if (YAxis.IsMoveOnPosition(LoaderYAxisServo.TCstLoadingPosiNo) == false)
            //    {
            //        XAxis.MovePosition(equip, LoaderXAxisServo.TCstLoadingPosiNo);
            //        StepCmd = EmLoaderUnitCmd.S1120_RECV_T_CST_SLOT_DOWN_START;
            //    }
            //}
            //else if (StepCmd == EmLoaderUnitCmd.S1120_RECV_T_CST_SLOT_DOWN_START)
            //{
            //    //RECV TOP CST DOWN 명령 처리 고민중.
            //    //Trandsfer LogincWoring에서 Stage를 조정한다. 
            //    if (equip.CstLoaderT.CstSlotDown(equip))
            //    {
            //        StepCmd = EmLoaderUnitCmd.S1130_RECV_T_CST_SLOT_DOWN_WAIT;
            //    }
            //}
            //else if (StepCmd == EmLoaderUnitCmd.S1130_RECV_T_CST_SLOT_DOWN_WAIT)
            //{
            //    if (equip.CstLoaderT.IsSlotPosiont(equip))
            //    {
            //        XAxis.MovePosition(equip, LoaderXAxisServo.WaitPosiNo);
            //        StepCmd = EmLoaderUnitCmd.S1140_PNL_MOVE_SEND_POSI;
            //    }
            //}
            //else if (StepCmd == EmLoaderUnitCmd.S1140_PNL_MOVE_SEND_POSI)
            //{
            //    if (XAxis.IsMoveOnPosition(LoaderXAxisServo.WaitPosiNo))
            //    {
            //        //좌우 분산 방법이 있을 경우. 분류함.  없을 경우. 같은 라인으로 이동.                    
            //        YAxis.MovePosition(equip, LoaderYAxisServo.TUnloadingPosiNo);
            //    }
            //}
            //else if (StepCmd == EmLoaderUnitCmd.S1160)
            //{
            //    if (XAxis.IsMoveOnPosition(LoaderXAxisServo.WaitPosiNo) && YAxis.IsMoveOnPosition(LoaderYAxisServo.TUnloadingPosiNo))
            //    {
            //        this.PioSendT.StartPioSend(equip, this);
            //    }
            //}
            //else if (StepCmd == EmLoaderUnitCmd.S1160)
            //{
            //    if (this.PioSendT.IsSendComplete())
            //    {
            //        //PNL 없는지 확인하고, 다음 포지션 확인 후. 다음 동작 처리
            //        if (true)
            //        {
            //            StepCmd = EmLoaderUnitCmd.S1000_RECV_START;
            //        }
            //    }
            //}
        }
        public void LoaderAutoLogicWorking()
        {
        }
    }
}
