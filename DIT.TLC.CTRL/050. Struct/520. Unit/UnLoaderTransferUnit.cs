﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class UnLoaderTransferUnit : BaseUnit
    {
        public UnloaderTransferYAxisServo YAxis { get; set; }        
        public UnloaderTransferTAxisServo T1Axis { get; set; }
        public UnloaderTransferTAxisServo T2Axis { get; set; }

        public Cylinder UpDonw1Cylinder = new Cylinder();
        public Cylinder UpDonw2Cylinder = new Cylinder();

        public Cylinder ZAxis { get; set; } //PIKER

        public Switch2Cmd1Sensor Vaccum1 = new Switch2Cmd1Sensor();
        public Switch2Cmd1Sensor Vaccum2 = new Switch2Cmd1Sensor();
        public SwitchOneWay Blower1 = new SwitchOneWay();
        public SwitchOneWay Blower2 = new SwitchOneWay();

        public SwitchOneWay Blower { get; set; }

        public override void LogicWorking(Equipment equip)
        {
            YAxis.LogicWorking(equip);
            T1Axis.LogicWorking(equip);
            T2Axis.LogicWorking(equip);
        }
    }
}

