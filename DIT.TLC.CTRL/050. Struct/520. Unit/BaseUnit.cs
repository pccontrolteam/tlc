﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.PLC;

namespace DIT.TLC.CTRL
{
    public enum EmRecvPio
    {
        UP_000_WAIT,
        UP_010_RECV_START,
        UP_020,
        UP_030,
        UP_040,
        UP_050,
        UP_060,
    }
    public enum EmSendPio
    {
        DW_000_WAIT,
        DW_010_SEND_START,
        DW_020,
        DW_030,
        DW_040,
        DW_050,
    }


    public class BaseUnit
    {
        public PlcAddr StartAddr { get; set; }
        public VirtualShare ShareMem { get; set; }

        public PanelInfo PanelInfo { get; set; }

        public BaseUnit SendUnit { get; set; } //상류 Uint 
        public BaseUnit RecvUnit { get; set; } //하류 Uint 

        public bool IsExitsPanel { get; set; }
        public Equipment EquipMain { get; set; }

        public Queue<double> QueTactTime = new Queue<double>(20);
        public List<double> LstTactTime = new List<double>(20);
        public DateTime LastTactWriteTime = DateTime.Now;
        public void AddTactTime(double tact)
        {
            LastTactWriteTime = DateTime.Now;
            QueTactTime.Enqueue(tact);      
        }


        public void Initizie(BaseUnit sender01, BaseUnit sender02, BaseUnit recver01, BaseUnit recver02, PlcAddr startAddr, IVirtualMem shareMem)
        {
        }

        public virtual void LogicWorking(Equipment equip)
        {
        }
    }
}
