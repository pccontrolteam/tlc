﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class CstLoaderUint : BaseUnit
    {
        public CassetteInfo InCst { get; set; }
        public CassetteInfo OutEmptyCst { get; set; }

        public Cylinder TiltCylinder = new Cylinder();


        public Sensor LDCasstteDetect_1                                                  /**/    = new Sensor();
        public Sensor LDCasstteDetect_2                                                  /**/    = new Sensor();
        public Sensor LDCasstteDetect_3                                                  /**/    = new Sensor();
        public Sensor LDCasstteDetect_4                                                  /**/    = new Sensor();
        public Sensor LDCasstteDetect_5                                                  /**/    = new Sensor();
        public Sensor LDCasstteDetect_6                                                  /**/    = new Sensor();

        public CylinderTwo CasstteGripCylinder                                          /**/    = new CylinderTwo();
        


        public Sensor LDCassetteLiftTiltinUp1                                            /**/    = new Sensor();
        public Sensor LDCassetteLiftTiltinDown1                                          /**/    = new Sensor();

        //MUTING
        public Switch MutingSwitch                                                      /**/    = new Switch();
        public Switch LiftMutingSwitch1                                                 /**/    = new Switch();
        public Switch LiftMutingSwitch2                                                 /**/    = new Switch();

        public Switch ResetSwitch                                                       /**/    = new Switch();

        public SwitchOneWay MutingLamp                                                  /**/   = new SwitchOneWay();

        //LIFT
        //TILT 센서
        public Switch Tilt { get; set; }

        public CstLoaderRotationUpAxisServo RotationAxis { get; set; }
        public CstLoaderRotationDownAxisServo CstUpDownAxis { get; set; }


        public override void LogicWorking(Equipment equip)
        {

        }

        internal bool IsSlotPosiont(Equipment equip)
        {
            throw new NotImplementedException();
        }

        internal bool CstSlotDown(Equipment equip)
        {
            throw new NotImplementedException();
        }
    }
}
