﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class CstUnloaderUint : BaseUnit
    {
        public CassetteInfo InCst { get; set; }
        public CassetteInfo OutEmptyCst { get; set; }

        public Cylinder TiltCylinder = new Cylinder();

        public Sensor ULDCasstteDetect_1                                                  /**/    = new Sensor();
        public Sensor ULDCasstteDetect_2                                                  /**/    = new Sensor();
        public Sensor ULDCasstteDetect_3                                                  /**/    = new Sensor();
        public Sensor ULDCasstteDetect_4                                                  /**/    = new Sensor();
        public Sensor ULDCasstteDetect_5                                                  /**/    = new Sensor();
        public Sensor ULDCasstteDetect_6                                                  /**/    = new Sensor();

        public CylinderTwo CasstteGripCylinder                                          /**/    = new CylinderTwo();

        //MUTING
        public Switch Muting = new Switch();


        public CstLoaderRotationUpAxisServo RotationUpAxis { get; set; }
        public CstLoaderRotationDownAxisServo RotationDownAxis { get; set; }
        public CstLoaderElectrictyZAxisServo ZAxis { get; set; }

        //MUTING
        public Switch MutingSwitch                                                      /**/    = new Switch();
        public Switch LiftMutingSwitch1                                                 /**/    = new Switch();
        public Switch LiftMutingSwitch2                                                 /**/    = new Switch();

        public Switch ResetSwitch                                                       /**/    = new Switch();

        public Switch Buzzer                                                       /**/    = new Switch();

        public SwitchOneWay MutingLamp                                                  /**/   = new SwitchOneWay();
        //LIFT
        //TILT 센서
        public Switch Tilt { get; set; }
        public override void LogicWorking(Equipment equip)
        {
        }

        internal bool IsSlotPosiont(Equipment equip)
        {
            throw new NotImplementedException();
        }

        internal bool CstSlotDown(Equipment equip)
        {
            throw new NotImplementedException();
        }
    }
}
