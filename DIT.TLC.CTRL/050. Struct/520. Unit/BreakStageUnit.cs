﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.PLC;

namespace DIT.TLC.CTRL
{
    public class BreakStageUnit : BaseUnit
    {
        public BreakUnitYAxisServo YAxis { get; set; }

        public BreakUnitXAxisServo X1AxisHeader { get; set; }
        public BreakUnitXAxisServo X2AxisHeader { get; set; }

        public BreakUnitZAxisServo Z1AxisHeader { get; set; }
        public BreakUnitZAxisServo Z2AxisHeader { get; set; }

        public BreakUnitTAxisServo T1AxisHeader { get; set; }
        public BreakUnitTAxisServo T2AxisHeader { get; set; }
        public PlcAddr Vaccum1Value { get; set; }
        public PlcAddr Vaccum2Value { get; set; }

        public Switch Vaccum1                              /**/= new Switch();
        public Switch Vaccum2                              /**/= new Switch();
        public SwitchOneWay Blower1                        /**/ = new SwitchOneWay();
        public SwitchOneWay Blower2                        /**/ = new SwitchOneWay();
        public Cylinder BrushUnDownCylinder                /**/= new Cylinder();
        


        public override void LogicWorking(Equipment equip)
        {
            YAxis.LogicWorking(equip);
            X1AxisHeader.LogicWorking(equip);
            X2AxisHeader.LogicWorking(equip);
            Z1AxisHeader.LogicWorking(equip);
            T1AxisHeader.LogicWorking(equip);
            T2AxisHeader.LogicWorking(equip);
        }
    }
}

