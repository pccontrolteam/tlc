﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.Ini;
using System.IO;
using System.Windows.Forms;


namespace DIT.TLC.CTRL
{
    public class LoaderYAxisServo : ServoMotorControl
    {
        public static string PATH_SETTING = Path.Combine(Application.StartupPath, "Setting", "ScanXServo.ini");

        public static int Purege_A_Cst_Inner_Y1 { get; set; }
        public static int Purege_A_Cst_Inner_Y2 { get; set; }
        public static int Purege_B_Cst_Inner_Y1 { get; set; }
        public static int Purege_B_Cst_Inner_Y2 { get; set; }
        public static int Purege_LoaderMid_Y1 { get; set; }
        public static int Purege_LoaderMid_Y2 { get; set; }
        public static int Purege_Unloading_Y1 { get; set; }
        public static int Purege_Unloading_Y2 { get; set; }
        public static int Purege_2mm_Y1 { get; set; }



        private bool _isLoadingOnPosition = false;

        public LoaderYAxisServo(int axis, string name, int posiCount, int acsBuffer) :
            base(axis, name, axis, name, posiCount, acsBuffer, acsBuffer)
        {
            if (posiCount == 0) return;

            SoftMinusLimit = 0;
            SoftPlusLimit = 2585;
            SoftSpeedLimit = 701;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 5000;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;

            Purege_A_Cst_Inner_Y1           = 1;   
            Purege_A_Cst_Inner_Y2           = 2;   
            Purege_B_Cst_Inner_Y1           = 3;   
            Purege_B_Cst_Inner_Y2           = 4;        
            Purege_LoaderMid_Y1             = 5;   
            Purege_LoaderMid_Y2             = 6;   
            Purege_Unloading_Y1             = 7;   
            Purege_Unloading_Y2             = 8;   
            Purege_2mm_Y1                   = 9;   
 


            base.MoveActionName[0]  = "Y1_CST_L";                 //  취출 이재기(Y1) : A 카세트 안쪽
            base.MoveActionName[1]  = "Y2_CST_L";                 //  취출 이재기(Y2) : A 카세트 안쪽
            base.MoveActionName[2]  = "Y1_CST_R";                 //  취출 이재기(Y1) : B 카세트 안쪽
            base.MoveActionName[3]  = "Y2_CST_R";                 //  취출 이재기(Y2) : B 카세트 안쪽     
            base.MoveActionName[4]  = "Y1_LoaderMid";             //  취출 이재기(Y1) : 카세트 중간
            base.MoveActionName[5]  = "Y2_LoaderMid";             //  취출 이재기(Y2) : 카세트 중간
            base.MoveActionName[6]  = "Y1_Unloading";             //  취출 이재기(Y1) : 언로딩 이재기
            base.MoveActionName[7]  = "Y2_Unloading";             //  취출 이재기(Y2) : 언로딩 이재기
            base.MoveActionName[8]  = "Y1_2mm";                   //  취출 이재기(Y1, Y2) : 2mm 후퇴 속도

        }       
    }
}


















