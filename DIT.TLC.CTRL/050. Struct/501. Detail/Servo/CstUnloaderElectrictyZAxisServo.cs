﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class CstUnloaderElectrictyZAxisServo : ServoMotorControl
    {
        public static int A_Cst_Insert_Stand { get; set; }

        public static int A_Cst_Insert { get; set; }
        public static int A_Cst_Tilt_Measure { get; set; }
        public static int A_Cell_Emission_Start { get; set; }
        public static int A_Cst_Emission { get; set; }
        public static int B_Cst_Insert_Stand { get; set; }
        public static int B_Lift_Move { get; set; }
        public static int B_Cst_Insert { get; set; }
        public static int B_Cst_Tilt_Measure { get; set; }
        public static int B_Cell_Emission_Start { get; set; }

        public CstUnloaderElectrictyZAxisServo(int axis, string name, int posiCount, int acsBuffer) :
            base(axis, name, axis, name, posiCount, acsBuffer, acsBuffer)
        {
            if (posiCount == 0) return;

            SoftMinusLimit = 0;
            SoftPlusLimit = 2585;
            SoftSpeedLimit = 701;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 5000;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;


            A_Cst_Insert_Stand      =   1;
            A_Cst_Insert            =   2;
            A_Cst_Tilt_Measure      =   3;
            A_Cell_Emission_Start   =   4;
            A_Cst_Emission          =   5;
            B_Cst_Insert_Stand      =   6;
            B_Lift_Move             =   7;
            B_Cst_Insert            =   8;
            B_Cst_Tilt_Measure      =   9;
            B_Cell_Emission_Start   =   10;


            base.MoveActionName[0] = "StandByTopL_Z";           //  A 리프트(Z1) : 카세트 투입 대기
            base.MoveActionName[1] = "LoadingTopL_Z";           //  A 리프트(Z1) : 카세트 투입
            base.MoveActionName[2] = "MeasureL_Z";              //  A 리프트(Z1) : 카세트 틸트 측정
            base.MoveActionName[3] = "CellContactL_Z";          //  A 리프트(Z1) : 셀 배출 시작
            base.MoveActionName[4] = "UnloadingBottomL_Z";      //  A 리프트(Z1) : 카세트 배출
            base.MoveActionName[5] = "StandByTopR_Z";           //  B 리프트(Z2) : 카세트 투입 대기
            base.MoveActionName[6] = "LoadingTopR_Z";           //  B 리프트(Z2) : 카세트 투입
            base.MoveActionName[7] = "MeasureR_Z";              //  B 리프트(Z2) : 카세트 틸트 측정
            base.MoveActionName[8] = "CellContactR_Z";          //  B 리프트(Z2) : 셀 배출 시작
            base.MoveActionName[9] = "UnloadingBottomR_Z";      //  B 리프트(Z2) : 카세트 배출
        }
    }
}



