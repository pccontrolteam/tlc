﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.Ini;
using System.IO;
using System.Windows.Forms;


namespace DIT.TLC.CTRL
{
    public class LoaderXAxisServo : ServoMotorControl
    {
        public static string PATH_SETTING = Path.Combine(Application.StartupPath, "Setting", "ScanXServo.ini");


        public static int Purege_A_Cst_Mid_X1 { get; set; }
        public static int Purege_A_Cst_Mid_X2 { get; set; }

        public static int Purege_A_Cst_StandBy_X1 { get; set; }
        public static int Purege_A_Cst_StandBy_X2 { get; set; }

        public static int Purege_B_Cst_Mid_X1 { get; set; }
        public static int Purege_B_Cst_Mid_X2 { get; set; }

        public static int Purege_B_Cst_StandBy_X1 { get; set; }
        public static int Purege_B_Cst_StandBy_X2 { get; set; }

        public static int Purege_Unloading_X1 { get; set; }
        public static int Purege_Unloading_X2 { get; set; }



        private bool _isLoadingOnPosition = false;

        public LoaderXAxisServo(int axis, string name, int posiCount, int acsBuffer) :
            base(axis, name, axis, name, posiCount, acsBuffer, acsBuffer)
        {
            if (posiCount == 0) return;

            SoftMinusLimit = 0;
            SoftPlusLimit = 2585;
            SoftSpeedLimit = 701;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 5000;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;


            Purege_A_Cst_Mid_X1            =   1;
            Purege_A_Cst_Mid_X2            =   2;
            Purege_A_Cst_StandBy_X1        =   3;
            Purege_A_Cst_StandBy_X2        =   4;
            Purege_B_Cst_Mid_X1            =   5;
            Purege_B_Cst_Mid_X2            =   6;
            Purege_B_Cst_StandBy_X1        =   7;
            Purege_B_Cst_StandBy_X2        =   8;
            Purege_Unloading_X1            =   9;
            Purege_Unloading_X2            =  10;



            base.MoveActionName[0]  = "X1_CST_L";                //  취출 이재기(X1) : A 카세트 중간
            base.MoveActionName[1]  = "X2_CST_L";                //  취출 이재기(X2) : A 카세트 중간
            base.MoveActionName[2]  = "X1_CST_STAND_BY_L";       //  취출 이재기(X1) : A 카세트 대기
            base.MoveActionName[3]  = "X2_CST_STAND_BY_L";       //  취출 이재기(X2) : A 카세트 대기
            base.MoveActionName[4]  = "X1_CST_R";                //  취출 이재기(X1) : B 카세트 중간
            base.MoveActionName[5]  = "X2_CST_R";                //  취출 이재기(X2) : B 카세트 중간
            base.MoveActionName[6]  = "X1_CST_STAND_BY_R";       //  취출 이재기(X1) : B 카세트 대기
            base.MoveActionName[7]  = "X2_CST_STAND_BY_R";       //  취출 이재기(X2) : B 카세트 대기
            base.MoveActionName[8]  = "X1_Unloading";            //  취출 이재기(X1) : 언로딩 이재기
            base.MoveActionName[9]  = "X2_Unloading";            //  취출 이재기(X2) : 언로딩 이재기

        }                                                                       
                                                                                
    }                                                                           
}      







