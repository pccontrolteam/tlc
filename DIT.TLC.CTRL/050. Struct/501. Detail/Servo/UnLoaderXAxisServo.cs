﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class UnLoaderXAxisServo : ServoMotorControl
    {
        public static int ULD_A_UnLodering_X1 { get; set; }
        public static int ULD_A_UnLodering_X2 { get; set; }
        public static int ULD_A_Cst_Mid_X1 { get; set; }
        public static int ULD_A_Cst_Mid_X2 { get; set; }
        public static int ULD_A_Cst_StandBy_X1 { get; set; }
        public static int ULD_A_Cst_StandBy_X2 { get; set; }
        public static int ULD_B_UnLodering_X1 { get; set; }
        public static int ULD_B_UnLodering_X2 { get; set; }
        public static int ULD_B_Cst_Mid_X1 { get; set; }
        public static int ULD_B_Cst_Mid_X2 { get; set; }
        public static int ULD_B_Cst_StandBy_X1 { get; set; }
        public static int ULD_B_Cst_StandBy_X2 { get; set; }
        public static int ULD_Buffer_X1 { get; set; }
        public static int ULD_Buffer_X2 { get; set; }

        public UnLoaderXAxisServo(int axis, string name, int posiCount, int acsBuffer) :
            base(axis, name, axis, name, posiCount, acsBuffer, acsBuffer)
        {
            if (posiCount == 0) return;

            SoftMinusLimit = 0;
            SoftPlusLimit = 2585;
            SoftSpeedLimit = 701;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 5000;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;

            ULD_A_UnLodering_X1 = 1;
            ULD_A_UnLodering_X2 = 2;
            ULD_A_Cst_Mid_X1 = 3;
            ULD_A_Cst_Mid_X2 = 4;
            ULD_A_Cst_StandBy_X1 = 5;
            ULD_A_Cst_StandBy_X2 = 6;
            ULD_B_UnLodering_X1 = 7;
            ULD_B_UnLodering_X2 = 8;
            ULD_B_Cst_Mid_X1 = 9;
            ULD_B_Cst_Mid_X2 = 10;
            ULD_B_Cst_StandBy_X1 = 11;
            ULD_B_Cst_StandBy_X2 = 12;
            ULD_Buffer_X1 = 13;
            ULD_Buffer_X2 = 14;

            base.MoveActionName[0] = "X1_TR_L";                         // 언로더(X1) : A 언로딩 이재기
            base.MoveActionName[1] = "X2_TR_L";                         // 언로더(X2) : A 언로딩 이재기
            base.MoveActionName[2] = "X1_CST_L";                        // 언로더(X1) : A 카세트 중간  
            base.MoveActionName[3] = "X2_CST_L";                        // 언로더(X2) : A 카세트 중간  
            base.MoveActionName[4] = "X1_CST_STAND_BY_L";               // 언로더(X1) : A 카세트 대기  
            base.MoveActionName[5] = "X2_CST_STAND_BY_L";               // 언로더(X2) : A 카세트 대기  
            base.MoveActionName[6] = "X1_TR_R";                         // 언로더(X1) : B 언로딩 이재기
            base.MoveActionName[7] = "X2_TR_R";                         // 언로더(X2) : B 언로딩 이재기
            base.MoveActionName[8] = "X1_CST_R";                        // 언로더(X1) : B 카세트 중간  
            base.MoveActionName[9] = "X2_CST_R";                        // 언로더(X2) : B 카세트 중간  
            base.MoveActionName[10] = "X1_CST_STAND_BY_R";              // 언로더(X1) : B 카세트 대기  
            base.MoveActionName[11] = "X2_CST_STAND_BY_R";              // 언로더(X2) : B 카세트 대기  
            base.MoveActionName[12] = "X1_BUFFER";                      // 언로더(X1) : 버퍼          
            base.MoveActionName[13] = "X2_BUFFER";                      // 언로더(X2) : 버퍼          

        }
    }
}





























