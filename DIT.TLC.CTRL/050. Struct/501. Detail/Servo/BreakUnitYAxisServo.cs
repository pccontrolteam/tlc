﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class BreakUnitYAxisServo : ServoMotorControl
    {

        public static int A_Table_Cell_Load     {get; set;}
        public static int A_Table_Cell_UnLoad   {get; set;}
        public static int B_Table_Cell_Load     {get; set;}
        public static int B_Table_Cell_UnLoad   {get; set;}
        public static int A_Vision_Align_Y1     {get; set;}
        public static int A_Vision_Align_Y2     {get; set;}  
        public static int LDS_Break_Measure_Y   {get; set;}


        public BreakUnitYAxisServo(int axis, string name, int posiCount, int acsBuffer) :
            base(axis, name, axis, name, posiCount, acsBuffer, acsBuffer)
        {
            if (posiCount == 0) return;

            SoftMinusLimit = 0;
            SoftPlusLimit = 2585;
            SoftSpeedLimit = 701;
            SoftJogSpeedLimit = 100;
            SoftAccelPlusLimit = 5000;
            SoftAccelMinusLimit = 100;
            EnableGripJogSpeedLimit = 30;

            A_Table_Cell_Load              = 1;
            A_Table_Cell_UnLoad            = 2;
            B_Table_Cell_Load              = 3;
            B_Table_Cell_UnLoad            = 4;
            A_Vision_Align_Y1              = 5;
            A_Vision_Align_Y2              = 6;
            LDS_Break_Measure_Y            = 7;


            base.MoveActionName[0] = "Load_Y1";                             // A 테이블 : 셀 로드 위치(Y1)       
            base.MoveActionName[1] = "UnLoad_Y1";                           // A 테이블 : 셀 언로드 위치(Y1)     
            base.MoveActionName[2] = "Load_Y2";                             // B 테이블 : 셀 로드 위치(Y2)       
            base.MoveActionName[3] = "UnLoad_Y2";                           // B 테이블 : 셀 언로드 위치(Y2)     
            base.MoveActionName[4] = "VISION_ALIGN_Y1";                     // A Align Cam 기준 위치(Y1)        
            base.MoveActionName[5] = "VISION_ALIGN_Y2";                     // B Align Cam 기준 위치(Y2)               
            base.MoveActionName[7] = "LDS_Break_Y";                         // LDS 측정 Y : LDS 기준 측정위치(Y1)
        }
    }
}
