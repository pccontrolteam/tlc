﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Data;
using System.Windows.Forms;

namespace DIT.TLC.CTRL
{
    public class SerialComm
    {
        private SerialPort _sp; //_SP라는 객체 선언, SP라는 속성 구현.
        public SerialPort Sp 
        {
            set
            {
                _sp = value;
            }
            get
            {
                return _sp;
            }
        }

        public SerialComm()
        {

        }
        public SerialComm(string comPort, int baudRate)
        {
            _sp = new SerialPort();
            _sp.PortName = comPort;
            _sp.BaudRate = (int)baudRate;
            _sp.DataBits = (int)8;
            _sp.Parity = Parity.None;
            _sp.StopBits = StopBits.One;
            _sp.ReadTimeout = (int)800;
            _sp.WriteTimeout = (int)500;
            _sp.Handshake = Handshake.None;
            _sp.Encoding = Encoding.Default;
            _sp.DataReceived += new SerialDataReceivedEventHandler(SP_DataReveived);
        }

        public bool Open()
        {
            return false;
            if (GG.TestMode == true) return false;

            _sp.Open();

            return IsOpen;
        }
        public Boolean IsOpen
        {
            get { return _sp.IsOpen; }
            set
            {
                if (value)
                {
                    MessageBox.Show("Success Connection");
                }
                else
                {
                    MessageBox.Show("Fail Connection");
                }
            }
        }

        public void SP_DataReveived(object sender, SerialDataReceivedEventArgs e)
        {
            if (_sp.IsOpen)
            {
                String msg = _sp.ReadExisting();
            }
        }
    }
}
