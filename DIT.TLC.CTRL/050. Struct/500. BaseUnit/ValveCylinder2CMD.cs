﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.PLC;
using Dit.Framework.Log;

namespace DIT.TLC.CTRL
{
    public class ValveCylinder2CMD : UnitBase
    {
        public float OPEN_OVERTIME = 5000;
        public float CLOSE_OVERTIME = 5000;
        private float _currentDirectionOverTime = 5000;

        public PlcAddr YB_OpenCmd { get; set; }
        public PlcAddr YB_CloseCmd { get; set; }

        public PlcAddr XB_OpenComplete { get; set; }
        public PlcAddr XB_CloseComplete { get; set; }

        public DateTime OpenStartTime { get; set; }
        public DateTime CloseStartTime { get; set; }

        public EM_AL_LST AlcdOpenTimeOut { get; set; }
        public EM_AL_LST AlcdCloseTimeOut { get; set; }

        public string OpenTime { get; set; }
        public string CloseTime { get; set; }

        private DateTime _errorStartTime = DateTime.Now;

        private bool _isFirstChk = false;

        public ValveCylinder2CMD()
        {
            AlcdOpenTimeOut = EM_AL_LST.AL_NONE;
            AlcdCloseTimeOut = EM_AL_LST.AL_NONE;
        }
        public virtual bool Open(Equipment equip)
        {
            OpenStartTime = DateTime.Now;
            YB_OpenCmd.vBit = true;
            YB_CloseCmd.vBit = false;

            return true;
        }
        public virtual bool Close(Equipment equip)
        {
            CloseStartTime = DateTime.Now;
            YB_CloseCmd.vBit = true;
            YB_OpenCmd.vBit = false;

            return true;
        }
        public bool IsOpen
        {
            get
            {
                return XB_OpenComplete.vBit == true &&
                       XB_CloseComplete.vBit == false;
            }
        }
        public bool IsOpening
        {
            get
            {
                return IsOpen == false && YB_OpenCmd == true;
            }
        }
        public bool IsClose
        {
            get
            {
                return XB_OpenComplete.vBit == false &&
                       XB_CloseComplete.vBit == true;
            }
        }
        public bool IsCloseing
        {
            get
            {
                return IsClose == false && YB_CloseCmd == true;
            }
        }
        public override void LogicWorking(Equipment equip)
        {
            UpdateTime();

            if (XB_OpenComplete.vBit == false && XB_CloseComplete.vBit == false && _isFirstChk == true)
            {
                _errorStartTime = DateTime.Now;
                _isFirstChk = false;
            }
            else if (XB_OpenComplete.vBit != XB_CloseComplete.vBit)
            {
                _errorStartTime = DateTime.Now;
                _isFirstChk = true;
            }

            _currentDirectionOverTime = YB_OpenCmd.vBit == false ? OPEN_OVERTIME : CLOSE_OVERTIME;
            if ((DateTime.Now - _errorStartTime).TotalMilliseconds > _currentDirectionOverTime)
            {
                if (YB_OpenCmd.vBit == false)
                {
                    AlarmMgr.Instance.Happen(equip, AlcdOpenTimeOut);
                }
                else if (YB_CloseCmd.vBit == true)
                {
                    AlarmMgr.Instance.Happen(equip, AlcdCloseTimeOut);
                }
                else
                {
                    AlarmMgr.Instance.Happen(equip, AlcdOpenTimeOut);
                    AlarmMgr.Instance.Happen(equip, AlcdCloseTimeOut);
                }
            }

            if (XB_OpenComplete.vBit && XB_CloseComplete.vBit)
            {
                Logger.Log.AppendLine(LogLevel.Error, "Error occurred by valve concurrent completing.");
                //Console.WriteLine("실린더 완료 동시 ON Error");
            }

            if (YB_OpenCmd.vBit == true && XB_OpenComplete.vBit && XB_CloseComplete.vBit == false)
            {
                YB_OpenCmd.vBit = false;
            }
            else if (YB_CloseCmd.vBit == true && XB_CloseComplete.vBit && XB_OpenComplete.vBit == false)
            {
                YB_CloseCmd.vBit = false;
            }
            else
            {
                if (YB_OpenCmd.vBit == true && XB_OpenComplete.vBit == false)
                {
                    if ((DateTime.Now - OpenStartTime).TotalMilliseconds > OPEN_OVERTIME)
                    {
                        if (XB_OpenComplete.vBit == false)
                            AlarmMgr.Instance.Happen(equip, AlcdOpenTimeOut);
                    }
                }
                if (YB_CloseCmd.vBit == true && XB_CloseComplete.vBit == false)
                {
                    if ((DateTime.Now - CloseStartTime).TotalMilliseconds > CLOSE_OVERTIME)
                    {
                        if (XB_CloseComplete.vBit == false)
                            AlarmMgr.Instance.Happen(equip, AlcdCloseTimeOut);
                    }
                }
            }
        }
        private void UpdateTime()
        {
            if (YB_OpenCmd.vBit == false && XB_OpenComplete.vBit == true)
                OpenTime = string.Format("{0:0.#}", (DateTime.Now - OpenStartTime).TotalMilliseconds / 1000);
            if (YB_CloseCmd.vBit == true && XB_CloseComplete.vBit == true)
                CloseTime = string.Format("{0:0.#}", (DateTime.Now - CloseStartTime).TotalMilliseconds / 1000);
        }

        //메소드 - 공통 인터락 확인 사항 
        public virtual bool IsInterlock(Equipment equip, bool isClose)
        {
            if (equip.IsUseInterLockOff) return false;

            //if (equip.IsHeavyAlarm == true && GG.TestMode == false)
            //{
            //    InterLockMgr.AddInterLock("Interlock <HEAVY ALARM> \n (HEAVY ALARM. Centering operation disabled)");
            //    return true;
            //}

            return false;
        }
    }
}
