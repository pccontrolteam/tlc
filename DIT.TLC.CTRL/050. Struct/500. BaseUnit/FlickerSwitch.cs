﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Timers;

namespace DIT.TLC.CTRL
{
    public class FlickerSwitch : Switch
    {
        private Timer _tmrFlicker = new Timer();
        bool _isFlicker = false;
        int _interval = 1000;
        private PlcTimerEx _flickerDlay = new PlcTimerEx("flicker delay");

        public FlickerSwitch()
        {
            ON_OVERTIME = 3000;
            OFF_OVERTIME = 3000;
            ON_TIME_OUT_ERROR = EM_AL_LST.AL_NONE;
            OFF_TIME_OUT_ERROR = EM_AL_LST.AL_NONE;

            _tmrFlicker.Interval = 1000;
            _tmrFlicker.Elapsed += new ElapsedEventHandler(_tmrFlicker_Tick);
        }        
        private void _tmrFlicker_Tick(object sender, EventArgs e)
        {
            YB_OnOff.vBit = !YB_OnOff.vBit;
        }
        public bool Flicker
        {
            get { return _isFlicker; }
            set
            {
                _isFlicker = value;
                if(value == false)
                    YB_OnOff.vBit = false;

                OnOffStartTime.Restart();
            }
        }
        public override void LogicWorking(Equipment equip)
        {
                if (Flicker)
                {
                    _tmrFlicker.Enabled = true;
                    
                    //if ((DateTime.Now - OnOffStartTime).TotalMilliseconds >= 0)
                    //{
                    //    YB_OnOff.vBit = (YB_OnOff.vBit == true) ? false : true;
                    //
                    //    OnOffStartTime = DateTime.Now;
                    //}
                }
        }
            

            /*
            UpdateTime();

            if (YB_OnOff.vBit == true && XB_OnOff.vBit == false)
            {
                if ((DateTime.Now - OnOffStartTime).TotalSeconds > TimeOutInterval)
                {
                    AlarmMgr.Instatnce.Happen(equip, ON_TIME_OUT_ERROR);
                }
            }

            if (YB_OnOff.vBit == false && XB_OnOff.vBit == true)
            {
                if ((DateTime.Now - OnOffStartTime).TotalSeconds > TimeOutInterval)
                {
                    AlarmMgr.Instatnce.Happen(equip, OFF_TIME_OUT_ERROR);
                }
            }*/
        }
    }
