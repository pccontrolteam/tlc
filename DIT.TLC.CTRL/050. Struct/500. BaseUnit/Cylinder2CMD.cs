﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Dit.Framework.PLC;

namespace DIT.TLC.CTRL
{
    public class Cylinder2CMD : Cylinder
    {
        public PlcAddr YB_ForwardCmd2p { get; set; }
        public PlcAddr YB_BackwardCmd2p { get; set; }



        public Cylinder2CMD()
        {

        }

        public override bool Forward(Equipment equip)
        {
            ForwardStartTime = DateTime.Now;
            YB_ForwardCmd.vBit = true;
            YB_ForwardCmd2p.vBit = true;
            YB_BackwardCmd.vBit = false;
            YB_BackwardCmd2p.vBit = false;

            return true;
        }
        public override bool Backward(Equipment equip)
        {
            BackwardStartTime = DateTime.Now;
            YB_ForwardCmd.vBit = false;
            YB_ForwardCmd2p.vBit = false;
            YB_BackwardCmd.vBit = true;
            YB_BackwardCmd2p.vBit = true;

            return true;
        }
    }
}
