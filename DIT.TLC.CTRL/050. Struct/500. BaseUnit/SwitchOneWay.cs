﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.PLC;


namespace DIT.TLC.CTRL
{
    public class SwitchOneWay : UnitBase
    {
        public Func<Equipment, bool, bool> InterLockFunc { get; set; }
        public PlcAddr YB_OnOff { get; set; }

        public SwitchOneWay()
        {
        }

        public virtual bool OnOff(Equipment equip, bool value)
        {
            if (InterLockFunc != null)
                if (InterLockFunc(equip, value) == true)
                {
                    return false;
                }

            YB_OnOff.vBit = value;
            return true;
        }       
        public bool IsOnOff { get { return YB_OnOff.vBit; } }
    }
}
