﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.Comm;
using System.Diagnostics;
using Dit.Framework.PLC;
using System.IO;
using System.Windows.Forms;
using Dit.Framework.Log;

namespace DIT.TLC.CTRL
{
    public enum MoveCommand
    {
        NO_1_POSI = 0,
        NO_2_POSI,
        NO_3_POSI,
        NO_4_POSI,
        NO_5_POSI,
        NO_6_POSI,
        NO_7_POSI,
        NO_8_POSI,
        NO_9_POSI,
        NO_10_POSI,
        NO_11_POSI,
        NO_12_POSI,
        NO_13_POSI,
        NO_14_POSI,
        NO_15_POSI,
        NO_16_POSI,
        NO_17_POSI,
        NO_18_POSI,
        NO_19_POSI,
        NO_20_POSI,
        NO_21_POSI,
        NO_22_POSI,
        NO_23_POSI,
        NO_24_POSI,
        NO_25_POSI,
        NO_26_POSI,
        NO_27_POSI,
        NO_28_POSI,
        NO_29_POSI,
        NO_30_POSI,
        NO_31_POSI,
        NO_32_POSI,
        HOME_POSI,
        START_POSI,
        JOG,
    }
    public enum EM_SERVO_JOG
    {
        JOG_PLUS = 1,
        JOG_STOP = 0,
        JOG_MINUS = -1,
    }
    public enum EM_LOG_MSG
    {
        CMD_ACK_DEFAULT,
        CMD_ACK_OK,
        CMD_ACK_FAIL_PMAC_INTERLOCK_ON,
        CMD_ACK_FAIL_HOME_PROGRAM_RUNNING,
        CMD_ACK_FAIL_AUTO_REVIEW_RUNNING,
        CMD_ACK_FAIL_PTP_MOVE_RUNNING,
        CMD_ACK_FAIL_OTHER_PROGRAM_RUNNING,
        CMD_ACK_FAIL_CONTROL_ACCESS
    }
    public enum EM_MOTION_COMPLETE_LOG_MSG
    {
        CMD_ACK_DEFAULT,
        CMD_ACK_HOME_SWQ_OK,
        CMD_ACK_PTP_SEQ_OK,
        CMD_ACK_MOTION_SEQ_FAIL,
        CMD_ACK_MOTION_SEQ_ESTOP,
        CMD_ACK_HOME_SEQ_FAIL,
        CMD_ACK_SEQ_OEVERLAP_FAIL,
        CMD_ACK_AUTO_REVIEW_COMPLETE,
        CMD_ACK_AUTO_REVIEW_STOP_TIME_END
    }
    public class ServoMotorControl : UnitBase
    {
        public static int HomePositionPosiNo = 0;
        public int SoftMinusLimit = 10;
        public int SoftPlusLimit = 10;
        public int SoftSpeedLimit = 10;
        public int SoftJogSpeedLimit = 10;
        public int EnableGripJogSpeedLimit = 30;
        public float InposOffset = 0.01f;
        public int SoftAccelPlusLimit = 1000;
        public int SoftAccelMinusLimit = 100;
        public float PinAvoidPos = 500.0f;

        public string Name { get; set; }
        public string SlayerName { get; set; }
        public int Axis { get; set; }
        public int SlayerAxis { get; set; }
        public ServoSetting Setting { get; set; }
        public int MasterACSBuffer { get; set; }
        public int SlaveACSBuffer { get; set; }

        public PlcAddr XB_StatusHomeCompleteBit { get; set; }
        public PlcAddr XB_StatusHomeInPosition { get; set; }
        public PlcAddr XB_StatusMotorMoving { get; set; }
        public PlcAddr XB_StatusMotorInPosition { get; set; }
        public PlcAddr XB_StatusNegativeLimitSet { get; set; }
        public PlcAddr XB_StatusPositiveLimitSet { get; set; }
        public PlcAddr XB_StatusMotorServoOn { get; set; }
        public PlcAddr XB_ErrCriticalPositionError { get; set; }
        public PlcAddr XB_ErrDriveFaultError { get; set; }
        public PlcAddr XB_ErrOverCurrentError { get; set; }
        public PlcAddr XB_ErrEcallError { get; set; }

        public PlcAddr XF_CurrMotorPosition { get; set; }
        public PlcAddr XF_CurrMotorSpeed { get; set; }
        public PlcAddr XF_CurrMotorStress { get; set; }

        //겐츄리 및 그래버일 경우만 다름 사용=================
        public PlcAddr XB_StatusHomeCompleteBitSlayer { get; set; }
        public PlcAddr XB_StatusHomeInPositionSlayer { get; set; }

        public PlcAddr XB_StatusMotorInPositionSlayer { get; set; }
        public PlcAddr XB_StatusNegativeLimitSetSlayer { get; set; }
        public PlcAddr XB_StatusPositiveLimitSetSlayer { get; set; }
        public PlcAddr XB_StatusMotorServoOnSlayer { get; set; }
        public PlcAddr XB_ErrCriticalPositionErrorSlayer { get; set; }
        public PlcAddr XB_ErrDriveFaultErrorSlayer { get; set; }
        public PlcAddr XB_ErrOverCurrentErrorSlayer { get; set; }
        public PlcAddr XB_ErrEcallErrorSlayer { get; set; }

        public PlcAddr XF_CurrMotorPositionSlayer { get; set; }
        public PlcAddr XF_CurrMotorSpeedSlayer { get; set; }
        public PlcAddr XF_CurrMotorStressSlayer { get; set; }
        //================================================

        public PlcAddr XI_CmdAckLogMsg { get; set; }
        public PlcAddr XI_CmdAckMotionLogCompleteMsg { get; set; }

        public PlcAddr YB_HomeCmd { get; set; }
        public PlcAddr XB_HomeCmdAck { get; set; }
        //public PlcAddr YB_StartPointCmd { get; set; }
        //public PlcAddr XB_StartPointCmdAck { get; set; }

        //public PlcAddr YF_StartPointSpeedCmd { get; set; }
        //public PlcAddr XF_StartPointSpeedCmdAck { get; set; }

        public PlcAddr YB_MotorJogMinusMove { get; set; }
        public PlcAddr YB_MotorJogPlusMove { get; set; }
        public PlcAddr YF_MotorJogSpeedCmd { get; set; }
        //public PlcAddr XF_MotorJogSpeedCmdAck { get; set; }


        public PlcAddr YI_InposBandWidth { get; set; }
        public PlcAddr XI_InposBandWidthAck { get; set; }


        //마크로 사용 추가=================

        public PlcAddr YF_ScanSpeed { get; set; }
        public PlcAddr XF_ScanSpeedAck { get; set; }

        public PlcAddr YF_ScanAccel { get; set; }
        public PlcAddr XF_ScanAccelAck { get; set; }

        public PlcAddr YF_Trigger1StartPosi { get; set; }
        public PlcAddr XF_Trigger1StartPosiAck { get; set; }

        public PlcAddr YF_Trigger2StartPosi { get; set; }
        public PlcAddr XF_Trigger2StartPosiAck { get; set; }

        public PlcAddr YF_Trigger3StartPosi { get; set; }
        public PlcAddr XF_Trigger3StartPosiAck { get; set; }

        public PlcAddr YF_Trigger1EndPosi { get; set; }
        public PlcAddr XF_Trigger1EndPosiAck { get; set; }

        public PlcAddr YF_Trigger2EndPosi { get; set; }
        public PlcAddr XF_Trigger2EndPosiAck { get; set; }

        public PlcAddr YF_Trigger3EndPosi { get; set; }
        public PlcAddr XF_Trigger3EndPosiAck { get; set; }

        public PlcAddr[] YB_Position0MoveCmd { get; set; }
        public PlcAddr[] XB_Position0MoveCmdAck { get; set; }



        public PlcAddr[] YF_PositionPoint { get; set; }
        public PlcAddr[] XF_PositionPointAck { get; set; }
        public PlcAddr[] YF_PositionSpeed { get; set; }
        public PlcAddr[] XF_PositionSpeedAck { get; set; }

        public PlcAddr[] XB_PositionComplete { get; set; }
        public PlcAddr[] YF_PositionAccel { get; set; }
        public PlcAddr[] XF_PositionAccelAck { get; set; }

        public int PositionCount { get; set; }
        public string[] MoveActionName { get; set; }

        //public MccActionItem MccHomeActionOnOff { get; set; }
        //public MccActionItem[] MccMoveActionOnOff { get; set; }

        public TimeSpan[] PositionMoveTime { get; set; }

        public int[] _moveStep;// = new int[] { 0, 0, 0, 0, 0, 0, 0 };
        public int[] _moveTimeout;
        public DateTime _homeStartTime = PcDateTime.Now;
        public DateTime[] _moveStartTime;//= new DateTime[] { DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now };
        public int MOVE_HOME_OVERTIME;
        public int _homeStep = 0;
        public int _startPosiStep = 0;
        private static int _stepSetting = 0;
        private static bool _IsSettingComplete = false;
        private bool _isAbsolute = false;

        public PlcTimer[] _moveDelay = null;
        public PlcTimer _homeDelay = new PlcTimer();
        public PlcTimer _startPosiDelay = new PlcTimer();

        public DateTime ServoMoveStartTime { get; set; }
        public static int ServoSettingTimeout = 30000;

        protected int DefaultServoTimeOut = 15000;
        protected int DefaultServoHomeTimeOver = 30000;
        //알람 리스트
        public static EM_AL_LST SETTING_TIMEOUT { get; set; }
        public EM_AL_LST PLUS_LIMIT_ERROR { get; set; }
        public EM_AL_LST MINUS_LIMIT_ERROR { get; set; }
        public EM_AL_LST MOTOR_SERVO_ON_ERROR { get; set; }
        public EM_AL_LST CRITICAL_POSITION_ERROR { get; set; }
        public EM_AL_LST DRIVE_FAULT_ERROR { get; set; }
        public EM_AL_LST OVER_CURRENT_ERROR { get; set; }
        public EM_AL_LST ECALL_ERROR { get; set; }
        public EM_AL_LST MOVE_OVERTIME_ERROR { get; set; }
        public EM_AL_LST BUFFER_RESET_ERROR { get; set; }
        public EM_AL_LST MOTOR_IS_NOT_MOVING_ERROR { get; set; }
        public EM_AL_LST MESSAGE_ACK_ERROR { get; set; }
        public EM_AL_LST MESSAGE_MOTION_COMPLETE_ERROR { get; set; }

        public ServoMotorControl(int axis, string name,
            int slayerAxis, string slayerName,
            int posiCount,
            int masterAcsBuffer = 0, int slaveAcsBuffer = 0, bool isAbsolute = false)
        {
            InitPositionSetting();
            Setting = new ServoSetting();
            Setting.CreateSampleData();            
            Setting.PATH_SETTING = Path.Combine(Application.StartupPath, "Setting", string.Format("Servo_{0}.ini", name));
            Setting.Save();
            Setting.Load();
            this.Axis = axis;
            this.Name = name;
            this.SlayerAxis = slayerAxis;
            this.SlayerName = slayerName;
            PositionCount = posiCount;
            this.MasterACSBuffer = masterAcsBuffer;
            this.SlaveACSBuffer = slaveAcsBuffer;

            _isAbsolute = isAbsolute;

            _moveStep = new int[PositionCount];
            _moveTimeout = new int[PositionCount];
            for (int idx = 0; idx < PositionCount; idx++)
                _moveTimeout[idx] = DefaultServoTimeOut;
            MOVE_HOME_OVERTIME = DefaultServoHomeTimeOver;
            _moveStartTime = new DateTime[PositionCount];
            MoveActionName = new string[PositionCount];
            PositionMoveTime = new TimeSpan[PositionCount];

            YB_Position0MoveCmd = new PlcAddr[PositionCount];
            XB_Position0MoveCmdAck = new PlcAddr[PositionCount];

            YF_PositionPoint = new PlcAddr[PositionCount];
            XF_PositionPointAck = new PlcAddr[PositionCount];
            YF_PositionSpeed = new PlcAddr[PositionCount];
            XF_PositionSpeedAck = new PlcAddr[PositionCount];

            XB_PositionComplete = new PlcAddr[PositionCount];

            YF_PositionAccel = new PlcAddr[PositionCount];
            XF_PositionAccelAck = new PlcAddr[PositionCount];


            SETTING_TIMEOUT = EM_AL_LST.AL_NONE;
            PLUS_LIMIT_ERROR = EM_AL_LST.AL_NONE;
            MINUS_LIMIT_ERROR = EM_AL_LST.AL_NONE;
            MOTOR_SERVO_ON_ERROR = EM_AL_LST.AL_NONE;
            CRITICAL_POSITION_ERROR = EM_AL_LST.AL_NONE;
            DRIVE_FAULT_ERROR = EM_AL_LST.AL_NONE;
            OVER_CURRENT_ERROR = EM_AL_LST.AL_NONE;
            ECALL_ERROR = EM_AL_LST.AL_NONE;
            MOVE_OVERTIME_ERROR = EM_AL_LST.AL_NONE;

            //MccHomeActionOnOff = MccActionItem.NONE;
            //
            //MccMoveActionOnOff = new MccActionItem[PositionCount];

            _moveDelay = new PlcTimer[PositionCount];

            for (int iPos = 0; iPos < PositionCount; iPos++)
            {
                /*
                MoveActionName[iPos] =
                    iPos == (PositionCount - 2) ? string.Format("HOME", iPos) :
                    iPos == (PositionCount - 1) ? string.Format("START", iPos) : string.Format("MOVE {0:0#}", iPos);
                */
                //MccMoveActionOnOff[iPos] = MccActionItem.NONE;
                _moveStartTime[iPos] = PcDateTime.Now;
                _moveDelay[iPos] = new PlcTimer();
            }
        }
        //public void Reset()
        //{
        //    _homeStartTime = DateTime.Now.AddDays(-1);
        //
        //    for (int iPos = 0; iPos < PositionCount; iPos++)
        //        _moveStartTime[iPos] = DateTime.Now.AddDays(-1);
        //}
        //public void TimeReset()
        //{
        //    _homeStartTime = DateTime.Now;
        //    _setStartTime = DateTime.Now;
        //    for(int iPos = 0; iPos < PositionCount; iPos++)
        //    {
        //        _moveStartTime[iPos] = DateTime.Now;
        //    }
        //}
        public void InitPositionSetting(
            int softMinusLimit = 10,
            int softPlusLimit = 10,
            int softSpeedLimit = 10,
            int softJogSpeedLimit = 10,
            int enableGripJogSpeedLimit = 30,
            float inposOffset = 0.1f,
            int softAccelPlusLimit = 1000,
            int softAccelMinusLimit = 100)
        {
            SoftMinusLimit = softMinusLimit;
            SoftPlusLimit = softPlusLimit;
            SoftSpeedLimit = softSpeedLimit;
            SoftJogSpeedLimit = softJogSpeedLimit;
            EnableGripJogSpeedLimit = enableGripJogSpeedLimit;
            InposOffset = inposOffset;
            SoftAccelPlusLimit = softAccelPlusLimit;
            SoftAccelMinusLimit = softAccelMinusLimit;
        }
        public virtual bool IsHomeCompleteBit
        {
            get { return XB_StatusHomeCompleteBit.vBit; }
        }
        public virtual bool IsStatusHomeInPosition
        {
            get { return XB_StatusHomeInPosition.vBit; }
        }
        public virtual bool IsStatusMotorInPosition
        {
            get { return XB_StatusMotorInPosition.vBit; }
        }
        public virtual bool IsNegativeLimit
        {
            get { return XB_StatusNegativeLimitSet.vBit; }
        }
        public virtual bool IsPositiveLimit
        {
            get { return XB_StatusPositiveLimitSet.vBit; }
        }
        public virtual bool IsServoOnBit
        {
            get { return XB_StatusMotorServoOn.vBit; }
        }
        public virtual bool IsCriticalPositionError
        {
            get { return XB_ErrCriticalPositionError.vBit; }
        }
        public virtual bool IsDriveFaultError
        {
            get { return XB_ErrDriveFaultError.vBit; }
        }
        public virtual bool IsOverCurrentError
        {
            get { return XB_ErrOverCurrentError.vBit; }
        }
        public virtual bool IsECallError
        {
            get { return XB_ErrEcallError.vBit; }
        }
        public virtual bool IsServoError
        {
            get
            {
                return IsHomeCompleteBit == false || IsNegativeLimit || IsPositiveLimit ;//|| IsServoOnBit == false || IsCriticalPositionError || IsDriveFaultError || IsOverCurrentError || IsECallError;
            }
        }
        public virtual bool IsFatalServoError
        {
            get
            {
                return IsCriticalPositionError || IsDriveFaultError || IsOverCurrentError || IsECallError;
            }
        }
        public EM_LOG_MSG CmdAckLogMsg
        {
            get
            {
                return (EM_LOG_MSG)XI_CmdAckLogMsg.vInt;
            }
        }
        public EM_MOTION_COMPLETE_LOG_MSG MotionCompleteLogMsg
        {
            get
            {
                return (EM_MOTION_COMPLETE_LOG_MSG)XI_CmdAckMotionLogCompleteMsg.vInt;
            }
        }
        public bool IsHomming { get { return _homeStep != 0; } }
        // 메소드 - 커멘드 함수들
        public virtual bool MovePosition(Equipment equip, int posiNo)
        {
            if (equip.IsPause)
            {
                InterLockMgr.AddInterLock(string.Format("Interlock <PAUSE> \n (PAUSE state, axis = {0} Do not move.)", Name));
                Logger.Log.AppendLine(LogLevel.Warning, "PAUSE state, axis = {0} Do not move.", Name);
                return false;
            }

            if (equip.IsDoorOpen && equip.IsUseInterLockOff == false && equip.IsUseDoorInterLockOff == false)
            {
                InterLockMgr.AddInterLock(string.Format("Interlock <DOOR> \n (DOOR OPEN state, axis = {0} movement prohibited.)", Name));
                Logger.Log.AppendLine(LogLevel.Warning, "DOOR OPEN state, axis = {0} movement prohibited.", Name);
                return false;
            }

            if (IsServoError == true && _isAbsolute == false)
            {
                InterLockMgr.AddInterLock(string.Format("Interlock <SERVO ERROR> \n (Axis = {0} MOTOR ERROR, motor home position required.)", Name));
                Logger.Log.AppendLine(LogLevel.Warning, "Axis = {0} MOTOR ERROR, motor home position required.", Name);
                return false;
            }

            if (equip.IsImmediatStop)
            {
                InterLockMgr.AddInterLock(string.Format("Interlock <EMERGENCY> \n (EMERGENCY state, Axis = {0} can not be moved.)", Name));
                Logger.Log.AppendLine(LogLevel.Warning, "EMERGENCY state, Axis = {0} can not be moved.", Name);
                return false;
            }

            if (IsHomeCompleteBit == false)
            {
                InterLockMgr.AddInterLock(string.Format("Interlock <HOME BIT ERROR> \n (Axis = {0} Motor HomeCompleteBit OFF, motor home position required.)", Name));
                Logger.Log.AppendLine(LogLevel.Warning, "Axis = {0} Motor HomeCompleteBit OFF, motor home position required.", Name);
                return false;
            }

            if (IsNegativeLimit || IsPositiveLimit)
            {
                InterLockMgr.AddInterLock(string.Format("Interlock <LIMIT SENSOR> \n (Axis = {0} motor limit sensor detection. \nJog and home movement only.)", Name));
                Logger.Log.AppendLine(LogLevel.Warning, "Axis = {0} motor limit sensor detection. \nJog and home movement only.", Name);
                return false;
            }

            if (CheckMotorCtrlPositionSetting(equip, (MoveCommand)posiNo, MoveActionName[posiNo]) == false) return false;
            if (IsStartedStep(equip, (MoveCommand)posiNo, MoveActionName[posiNo]) == true) return false;

            if (CheckStartMoveInterLock(equip, (MoveCommand)posiNo))
            {
                //IsInterLockError = true;
                return false;
            }
            else if (IsMoveOnPosition(posiNo) == true)
            {
                return true;
            }
            else
            {
                if (SetEquipState(equip) == false) return false;
                int minMoveTime = 5000; // ms
                //_moveTimeout[posiNo] = minMoveTime + CalculateMoveTime(posiNo, equip.MotorCtrl.Triggers != 0);
                _moveStep[posiNo] = 10;
                Console.WriteLine("Posi {1} MoveTime{0}", _moveTimeout[posiNo], posiNo);
                return true;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="posiNo"></param>
        /// <returns>예상시간(ms)</returns>
        public virtual int CalculateMoveTime(int posiNo, bool isScan = false)
        {
            double currentPosition = XF_CurrMotorPosition.vFloat;
            double cmdSpeed = XF_PositionSpeedAck[posiNo].vFloat;
            double cmdPosition = XF_PositionPointAck[posiNo].vFloat;
            if ((int)cmdSpeed < 1)
                cmdSpeed = 1;

            int moveTime = (int)(Math.Abs(cmdPosition - currentPosition) / cmdSpeed * 1000);

            return moveTime;
        }
        public virtual bool GoHomeOrPositionOne(Equipment equip)
        {
            if (IsServoError == false && GG.TestMode == false)
            {
                return MovePosition(equip, 0);
            }
            else
            {
                return GoHome(equip);
            }
        }
        public virtual bool IsHomeOnPosition()
        {
            return IsStatusHomeInPosition &&
                    IsServoError == false &&
                    IsMoving == false &&
                    _homeStep == 0;
        }
        public virtual bool GoHome(Equipment equip)
        {
            if (_isAbsolute == true)
            {
                if (MovePosition(equip, 0) == true) return true;
                else return false;
            }
            else
            {
                if (CheckMotorCtrlPositionSetting(equip, MoveCommand.HOME_POSI, "HOME") == false) return false;
                if (IsStartedStep(equip, MoveCommand.HOME_POSI, "HOME") == true) return false;

                if (CheckStartMoveInterLock(equip, MoveCommand.HOME_POSI))
                {
                    //IsInterLockError = true;
                    return false;
                }
                else
                {
                    if (SetEquipState(equip) == false) return false;
                    _homeStep = 10;
                    //if (GG.TestMode == true)
                    //    _homeStep = 10;
                    //else
                    //    _homeStep = 1;
                    //
                    _isHappenAlarm = false;
                    return true;
                }
            }
        }
        protected bool JogMoveInterlock(Equipment equip, EM_SERVO_JOG jog, float speed)
        {
            if (equip.IsPause)
            {
                InterLockMgr.AddInterLock(
                    string.Format("Interlock <PAUSE> \n (PAUSE state, axis = {0}, direction = {1}, speed = {2}), prohibit operation)", Name, jog, speed));
                Logger.Log.AppendLine(LogLevel.Warning, "PAUSE state, axis = {0}, direction = {1}, speed = {2}), prohibit operation", Name, jog, speed);
                return false;
            }

            if (equip.IsDoorOpen == true && equip.IsUseInterLockOff == false && equip.IsUseDoorInterLockOff == false)
            {
                InterLockMgr.AddInterLock(
                    string.Format("Interlock <DOOR> \n (DOOR OPEN state, axis = {0}, direction = {1}, speed = {2}, prohibit operation)", Name, jog, speed));
                Logger.Log.AppendLine(
                    LogLevel.Warning, "DOOR OPEN state, axis = {0}, direction = {1}, speed = {2}, prohibit operation", Name, jog, speed);
                return false;
            }

            if (equip.IsImmediatStop)
            {
                InterLockMgr.AddInterLock(
                    string.Format("Interlock <EMERGENCY> \n (EMERGENCY state, axis = {0}, direction = {1}, speed = {2}, prohibit operation)", Name, jog, speed));
                Logger.Log.AppendLine(
                    LogLevel.Warning, "EMERGENCY state, axis = {0}, direction = {1}, speed = {2}, prohibit operation", Name, jog, speed);
                return false;
            }

            if (IsStepEnd == false)
            {
                InterLockMgr.AddInterLock(
                    string.Format("Interlock <RUNNING> \n (axis = {0} moving, prohibit operation)", Name));
                Logger.Log.AppendLine(LogLevel.Warning, "axis = {0} moving, prohibit operation", Name);
                return false;
            }

            if (CheckStartMoveInterLock(equip, MoveCommand.JOG))
            {
                //IsInterLockError = true;
                return false;
            }
            if (SetEquipState(equip) == false) return false;
            return true;
        }
        public bool JogMove(Equipment equip, EM_SERVO_JOG jog, float speed)
        {
            if (jog == EM_SERVO_JOG.JOG_STOP)
            {
                this.YF_MotorJogSpeedCmd.vFloat = speed;
                this.YB_MotorJogMinusMove.vBit = false;
                this.YB_MotorJogPlusMove.vBit = false;
                return true;
            }

            if (JogMoveInterlock(equip, jog, (int)speed) == false)
                return false;

            Logger.Log.AppendLine(LogLevel.Info, "{0} motor {1} velocity {2} move", Name, jog, speed);
            if (jog == EM_SERVO_JOG.JOG_MINUS)
            {
                this.YF_MotorJogSpeedCmd.vFloat = speed;
                this.YB_MotorJogPlusMove.vBit = false;
                this.YB_MotorJogMinusMove.vBit = true;
            }
            else if (jog == EM_SERVO_JOG.JOG_PLUS)
            {
                this.YF_MotorJogSpeedCmd.vFloat = speed;
                this.YB_MotorJogMinusMove.vBit = false;
                this.YB_MotorJogPlusMove.vBit = true;
            }
            return true;
        }
        public virtual bool IsStepEnd
        {
            get
            {
                bool step = (_moveStep[0] == 0);
                for (int iPos = 0; iPos < PositionCount; iPos++)
                    step = step && (_moveStep[iPos] == 0);

                return step && !IsMoving && _homeStep == 0;
            }
        }
        public virtual bool IsMoving
        {
            get { return XB_StatusMotorMoving.vBit; }
        }
        public virtual bool IsMoveOnPosition(int posiNo)
        {
            if (GG.TestMode)
                return true;//jys::todo temp, XB_PositionComplete[posiNo].vBit && IsMoving == false && _moveStep[posiNo] == 0;
            else
                return Math.Abs(XF_CurrMotorPosition.vFloat - XF_PositionPointAck[posiNo].vFloat) <= InposOffset /*&& XB_PositionComplete[posiNo].vBit */&& IsMoving == false && _moveStep[posiNo] == 0 && IsHomeCompleteBit;
        }
        public virtual bool IsMovingOnPosition(int posiNo)
        {
            return _moveStep[posiNo] != 0;
        }
        public virtual bool IsMovingOnHome()
        {
            return _homeStep != 0;
        }
        protected virtual bool CheckStartMoveInterLock(Equipment equip, MoveCommand cmd)
        {
            return false;
        }
        protected virtual bool SetEquipState(Equipment equip)
        {
            return true;
        }
        protected virtual bool CheckMovingInterLock(Equipment equip, MoveCommand cmd, ref int stepMove)
        {
            if (IsMoving)
            {
            }

            return false;
        }
        public override void LogicWorking(Equipment equip)
        {
            CheckServoStatus(equip);

            if (PositionCount == 0) return;

            PositionWorking(equip, MoveCommand.HOME_POSI, ref _homeStep, MOVE_HOME_OVERTIME, _homeDelay,
                YB_HomeCmd, XB_HomeCmdAck, XB_StatusHomeInPosition,
                100, 10, "HOME", ref _homeStartTime);

            for (int iPos = 0; iPos < PositionCount; iPos++)
                PositionWorking(equip, (MoveCommand)iPos, ref _moveStep[iPos], _moveTimeout[iPos], _moveDelay[iPos],
                    YB_Position0MoveCmd[iPos], XB_Position0MoveCmdAck[iPos], XB_PositionComplete[iPos],
                    100, 10, MoveActionName[iPos], ref _moveStartTime[iPos]);

            //SettingWorking(equip);
            this.YF_MotorJogSpeedCmd.vFloat = 100;
        }
        private bool _isHappenAlarm = false;
        private bool _isLimitAlarm = false;
        public bool StartReset(Equipment equip)
        {
            return true;
        }
        private void CheckServoStatus(Equipment equip)
        {
            if (_isHappenAlarm == false)
            {
                if (this.IsNegativeLimit && IsHomming == false)
                {
                    AlarmMgr.Instance.Happen(equip, MINUS_LIMIT_ERROR);
                    _isHappenAlarm = true;
                    _isLimitAlarm = true;
                }
                if (this.IsPositiveLimit && IsHomming == false)
                {
                    AlarmMgr.Instance.Happen(equip, PLUS_LIMIT_ERROR);
                    _isHappenAlarm = true;
                    _isLimitAlarm = true;
                }
                //if (this.IsServoOnBit == false && IsHomming == false)
                //{
                //    AlarmMgr.Instance.Happen(equip, MOTOR_SERVO_ON_ERROR);
                //    _isHappenAlarm = true;
                //}
                //if (this.IsCriticalPositionError && IsHomming == false)
                //{
                //    AlarmMgr.Instance.Happen(equip, CRITICAL_POSITION_ERROR);
                //    _isHappenAlarm = true;
                //}
                //if (this.IsDriveFaultError && IsHomming == false)
                //{
                //    AlarmMgr.Instance.Happen(equip, DRIVE_FAULT_ERROR);
                //    _isHappenAlarm = true;
                //}
                //if (this.IsOverCurrentError && IsHomming == false)
                //{
                //    AlarmMgr.Instance.Happen(equip, OVER_CURRENT_ERROR);
                //    _isHappenAlarm = true;
                //}
                //if (this.IsECallError && IsHomming == false)
                //{
                //    AlarmMgr.Instance.Happen(equip, ECALL_ERROR);
                //    _isHappenAlarm = true;
                //}
            }
            else
            {
                if (_isLimitAlarm == true && this.IsNegativeLimit == false && IsPositiveLimit == false)
                {
                    _isLimitAlarm = false;
                    _isHappenAlarm = false;
                }
            }
        }

        protected virtual void OnSettingEx(Equipment equip)
        {

        }

        private static PlcTimer _setDelay = new PlcTimer();
        private static DateTime _setStartTime = DateTime.Now;
        private static PlcTimerEx _tmrSettingDelay = new PlcTimerEx("Motor Control Setting Delay");
        //protected static void SettingWorking(Equipment equip)
        //{
        //    if (_stepSetting > 10 && (DateTime.Now - _setStartTime).TotalMilliseconds > ServoSettingTimeout)
        //    {
        //        Logger.Log.AppendLine(LogLevel.Error, "Motor setting TIME OVER");
        //        AlarmMgr.Instance.Happen(equip, SETTING_TIMEOUT);
        //        _stepSetting = 0;
        //        return;
        //    }

        //    if (_stepSetting == 0)
        //    {
        //    }
        //    else if (_stepSetting == 10)
        //    {
        //        Logger.Log.AppendLine(LogLevel.Info, "Motor start writing settings.");
        //        _setStartTime = DateTime.Now;
        //        _IsSettingComplete = false;

        //        foreach (ServoMotorControl motor in equip.Motors)
        //        {
        //            for (int iPos = 0; iPos < motor.PositionCount; iPos++)
        //            {
        //                motor.YF_PositionPoint[iPos].vFloat = Convert.ToSingle(motor.Setting.GetPosition(iPos));
        //                motor.YF_PositionSpeed[iPos].vFloat = Convert.ToSingle(motor.Setting.GetSpeed(iPos));
        //                motor.YF_PositionAccel[iPos].vFloat = Convert.ToSingle(motor.Setting.GetAccel(iPos));
        //            }
        //        }

        //        GG.MOTOR.WriteToPLC(GG.CTRL_REAR_MEM, GG.CTRL_REAR_MEM.Length);
        //        _setDelay.Start(1);
        //        _stepSetting = 20;
        //    }
        //    else if (_stepSetting == 20)
        //    {
        //        if (_setDelay)
        //        {
        //            _setDelay.Stop();

        //            equip.MotorCtrl.YB_ValueSave.vBit = true;
        //            _stepSetting = 30;
        //        }
        //    }
        //    else if (_stepSetting == 30)
        //    {
        //        if (equip.MotorCtrl.XB_ValueSaveAck.vBit == true)
        //        {
        //            equip.MotorCtrl.YB_ValueSave.vBit = false;
        //            _tmrSettingDelay.Start(3);
        //            _stepSetting = 40;
        //        }
        //    }
        //    else if (_stepSetting == 40)
        //    {
        //        if (equip.MotorCtrl.XB_ValueSaveAck.vBit == false && _tmrSettingDelay)
        //        {
        //            GG.MOTOR.ReadFromPLC(GG.ACS_REAR_MEM, GG.ACS_REAR_MEM.Length);
        //            _tmrSettingDelay.Stop();
        //            _stepSetting = 50;
        //        }
        //    }
        //    else if (_stepSetting == 50)
        //    {
        //        if (equip.Motors.ToList().TrueForAll(m => m.CheckSaveResult()))
        //        {
        //            _stepSetting = 80;
        //        }
        //        else
        //        {
        //            _stepSetting = 60;
        //        }
        //    }
        //    else if (_stepSetting == 60)
        //    {
        //        Logger.Log.AppendLine(LogLevel.Info, "Motor set writing Retry.");
        //        GG.MOTOR.ReadFromPLC(GG.ACS_REAR_MEM, GG.ACS_REAR_MEM.Length);
        //        _tmrSettingDelay.Start(1);
        //        _stepSetting = 70;
        //    }
        //    else if (_stepSetting == 70)
        //    {
        //        if (_tmrSettingDelay)
        //        {
        //            if (equip.Motors.ToList().TrueForAll(m => m.CheckSaveResult()))
        //            {
        //                _tmrSettingDelay.Stop();
        //                _stepSetting = 80;
        //            }
        //            else
        //            {
        //                _stepSetting = 60;
        //                _tmrSettingDelay.Stop();
        //            }
        //        }
        //    }
        //    else if (_stepSetting == 80)
        //    {
        //        Logger.Log.AppendLine(LogLevel.Info, "Motor set writing completed.");
        //        _IsSettingComplete = true;
        //        _stepSetting = 0;
        //    }
        //}
        //private bool CheckSaveResult()
        //{
        //    if (GG.TestMode) return true;

        //    for (int iPos = 0; iPos < PositionCount; iPos++)
        //    {
        //        if (YF_PositionAccel[iPos].vFloat != XF_PositionAccelAck[iPos].vFloat ||
        //            YF_PositionPoint[iPos].vFloat != XF_PositionPointAck[iPos].vFloat ||
        //            YF_PositionSpeed[iPos].vFloat != XF_PositionSpeedAck[iPos].vFloat)
        //        {
        //            return false;
        //        }
        //    }
        //    return true;
        //}

        private PlcTimerEx _tmrCmdAckMsgWait = new PlcTimerEx("CMD ACK MSG WAIT TIMER");
        private PlcTimerEx _tmrHomeStartWait = new PlcTimerEx("HOME START WAIT DELAY");
        private DateTime _startTime = DateTime.Now;
        private TimeSpan _ackEndTime = new TimeSpan();
        private PlcTimer _runDelay = new PlcTimer();
        private void PositionWorking(Equipment equip, MoveCommand cmd, ref int stepMove, int moveTimeout, PlcTimer moveDelay,
            PlcAddr yb_Position0MoveCmd, PlcAddr xb_Position0MoveCmdAck, PlcAddr xb_Position0Complete,
            int posi, int speed, string actionName, ref DateTime startTime/*, MccActionItem mccActionOnOff*/)
        {
            //EMS 명령 수행
            if (IsMoving &&
                CheckMovingInterLock(equip, cmd, ref stepMove) == true)
            {
                return;
            }


            if (stepMove < 4 && stepMove != 0 && (PcDateTime.Now - startTime).TotalMilliseconds > 5000)
            {
                Logger.Log.AppendLine(LogLevel.Error, "{0}가 BUFFER RESET ERROR, ", Name);
                AlarmMgr.Instance.Happen(equip, BUFFER_RESET_ERROR);
                _startTime = PcDateTime.Now;
                stepMove = 0;
                return;
            }

            if (stepMove == 20 && (PcDateTime.Now - startTime).TotalMilliseconds > 5000)
            {
                Logger.Log.AppendLine(LogLevel.Error, "{0}가 ACK SIGNAL OVERTIME, ", Name);
                AlarmMgr.Instance.Happen(equip, EM_AL_LST.AL_0727_MOTOR_CMD_ACK_SIGNAL_TIMEOVER);
                _startTime = PcDateTime.Now;
                stepMove = 0;
                return;
            }

            //if (stepMove == 40 && IsMoving == false && xb_Position0Complete.vBit == false && (PcDateTime.Now - startTime).TotalMilliseconds > 5000)
            //{
            //    Logger.Log.AppendLine(LogLevel.Error, "{0}가 IS NOT MOVING ERROR, ", Name);
            //    AlarmMgr.Instance.Happen(equip, MOTOR_IS_NOT_MOVING_ERROR);
            //    equip.IsNeedSeq = EMNeedSeq.Home;
            //    _startTime = PcDateTime.Now;
            //    stepMove = 0;
            //    return;
            //}

            if (stepMove > 20 && (PcDateTime.Now - startTime).TotalMilliseconds > moveTimeout)
            {
                Logger.Log.AppendLine(LogLevel.Error, "{0}가 {1} TIME OVER, Calculated Time : {2} / Current Position : {3}", Name, actionName, moveTimeout, XF_CurrMotorPosition.vFloat);
                AlarmMgr.Instance.Happen(equip, MOVE_OVERTIME_ERROR);
                _startTime = PcDateTime.Now;
                stepMove = 0;
                return;
            }

            if (stepMove == 0)
            {
                //yb_Position0MoveCmd.vBit = false;
            }
            //MOTOR BUFFER COMPILE
            else if (stepMove == 1)
            {
                startTime = PcDateTime.Now;

                //if (Axis != SlayerAxis)
                //{
                //    GG.MOTOR.BufferCompile(MasterACSBuffer, SlaveACSBuffer);
                //}
                //else
                //{
                //    GG.MOTOR.BufferCompile(MasterACSBuffer);
                //}

                stepMove = 2;
            }
            else if (stepMove == 2)
            {
                //if (Axis != SlayerAxis)
                //{
                //    if (GG.MOTOR.IsBufferCompileComplete(MasterACSBuffer, SlaveACSBuffer))
                //    {
                //        GG.MOTOR.RunBuffer(MasterACSBuffer, SlaveACSBuffer);
                //        _runDelay.Start(1);
                //        stepMove = 4;
                //    }
                //}
                //else
                //{
                //    if (GG.MOTOR.IsBufferCompileComplete(MasterACSBuffer))
                //    {
                //        GG.MOTOR.RunBuffer(MasterACSBuffer);
                //        _runDelay.Start(1);
                //        stepMove = 4;
                //    }
                //}

                stepMove = 4;
            }
            else if (stepMove == 4)
            {
                if (_runDelay)
                {
                    _runDelay.Stop();
                    stepMove = 10;
                }
            }
            else if (stepMove == 10)
            {
                startTime = PcDateTime.Now;
                //equip.MccPc.SetMccAction(mccActionOnOff);
                _startTime = PcDateTime.Now;
                Logger.Log.AppendLine(LogLevel.Info, "{0} Current position ({1}) -> {2} Start to move", Name, XF_CurrMotorPosition.vFloat, actionName);

                yb_Position0MoveCmd.vBit = true;
                stepMove = 20;
            }
            else if (stepMove == 20)
            {
                if (xb_Position0MoveCmdAck.vBit == true)
                {
                    Logger.Log.AppendLine(LogLevel.Info, "{0} Axis : {1} CMD ACK OK", Name, actionName);
                    yb_Position0MoveCmd.vBit = false;
                    _tmrHomeStartWait.Start(0, 3000);
                    _tmrCmdAckMsgWait.Start(0, 2000);

                    stepMove = 30;
                }
            }
            else if (stepMove == 30)
            {
                if (xb_Position0MoveCmdAck.vBit == false)
                {
                    _ackEndTime = PcDateTime.Now - _startTime;
                    if (cmd == MoveCommand.HOME_POSI)
                        stepMove = 35;
                    else
                        stepMove = 40;
                }
            }
            else if (stepMove == 35)
            {
                if (_tmrHomeStartWait)
                {
                    _tmrHomeStartWait.Stop();
                    stepMove = 40;
                }
            }
            else if (stepMove == 40)
            {
                if (xb_Position0Complete.vBit == true && XB_StatusHomeCompleteBit.vBit == true
                    )
                {
                    //if (_tmrCmdAckMsgWait)
                    //{
                    //    _tmrCmdAckMsgWait.Stop();
                    //    if (CmdAckLogMsg != EM_ISPT_LOG_MSG.CMD_ACK_DEFAULT &&
                    //    CmdAckLogMsg != EM_ISPT_LOG_MSG.CMD_ACK_OK &&
                    //    GG.TestMode == false)
                    //    {
                    //        AlarmMgr.Instance.Happen(equip, MESSAGE_ACK_ERROR);
                    //        Logger.Log.AppendLine(LogLevel.Error, "{0}Axis Cmd Ack Error Log Msg : {1}", Name, CmdAckLogMsg.ToString());
                    //        stepMove = 0;
                    //        return;
                    //    }
                    //}
                    Logger.Log.AppendLine(LogLevel.Info, "{0} is {1}({2}) Move completed.", Name, actionName, XF_CurrMotorPosition.vFloat);

                    //equip.MccPc.SetMccAction(mccActionOnOff, false);
                    if (cmd == MoveCommand.HOME_POSI && GG.TestMode == false)
                    {
                        moveDelay.Start(2);
                    }

                    stepMove = 50;
                }
                //if (_tmrCmdAckMsgWait)
                //{
                //    _tmrCmdAckMsgWait.Stop();
                //    if (CmdAckLogMsg != EM_ISPT_LOG_MSG.CMD_ACK_DEFAULT &&
                //        CmdAckLogMsg != EM_ISPT_LOG_MSG.CMD_ACK_OK &&
                //        GG.TestMode == false)
                //    {
                //        AlarmMgr.Instance.Happen(equip, MESSAGE_ACK_ERROR);
                //        Logger.Log.AppendLine(LogLevel.Error, "{0}Axis Cmd Ack Error Log Msg : {1}", Name, CmdAckLogMsg.ToString());
                //        stepMove = 0;
                //        return;
                //    }
                //    Logger.Log.AppendLine(LogLevel.Error, "{0}Axis Cmd Ack OK Log Msg : {1}", Name, CmdAckLogMsg.ToString());
                //}
                //if (MotionCompleteLogMsg != EM_ISPT_MOTION_COMPLETE_LOG_MSG.CMD_ACK_DEFAULT &&
                //    MotionCompleteLogMsg != EM_ISPT_MOTION_COMPLETE_LOG_MSG.CMD_ACK_HOME_SWQ_OK &&
                //    MotionCompleteLogMsg != EM_ISPT_MOTION_COMPLETE_LOG_MSG.CMD_ACK_PTP_SEQ_OK &&
                //    GG.TestMode == false)
                //{
                //    if (MotionCompleteLogMsg == EM_ISPT_MOTION_COMPLETE_LOG_MSG.CMD_ACK_MOTION_SEQ_ESTOP)
                //        AlarmMgr.Instance.Happen(equip, IMMEDIATE_STOP_ERROR);
                //    else
                //        AlarmMgr.Instance.Happen(equip, MESSAGE_MOTION_COMPLETE_ERROR);
                //
                //    Logger.Log.AppendLine(LogLevel.Error, "{0}Axis Motion Complete Error Log Msg : {1}", Name, MotionCompleteLogMsg.ToString());
                //
                //    equip.IsInterLock = true;
                //    stepMove = 0;
                //}
            }
            else if (stepMove == 50)
            {
                if (cmd == MoveCommand.HOME_POSI && GG.TestMode == false)
                {
                    if (IsHomeCompleteBit == true &&
                        IsStatusHomeInPosition == true &&
                        IsStatusMotorInPosition == true &&
                        XB_StatusMotorServoOn.vBit == true &&
                        moveDelay)
                    {
                        stepMove = 60;
                        moveDelay.Stop();
                    }
                    //if (MotionCompleteLogMsg != EM_ISPT_MOTION_COMPLETE_LOG_MSG.CMD_ACK_DEFAULT &&
                    //    MotionCompleteLogMsg != EM_ISPT_MOTION_COMPLETE_LOG_MSG.CMD_ACK_HOME_SWQ_OK &&
                    //    MotionCompleteLogMsg != EM_ISPT_MOTION_COMPLETE_LOG_MSG.CMD_ACK_PTP_SEQ_OK &&
                    //    GG.TestMode == false)
                    //{
                    //    if (MotionCompleteLogMsg == EM_ISPT_MOTION_COMPLETE_LOG_MSG.CMD_ACK_MOTION_SEQ_ESTOP)
                    //        AlarmMgr.Instance.Happen(equip, IMMEDIATE_STOP_ERROR);
                    //    else
                    //        AlarmMgr.Instance.Happen(equip, MESSAGE_MOTION_COMPLETE_ERROR);
                    //
                    //    Logger.Log.AppendLine(LogLevel.Error, "{0}Axis Motion Complete Error Log Msg : {1}", Name, MotionCompleteLogMsg.ToString());
                    //
                    //    equip.IsInterLock = true;
                    //    stepMove = 0;
                    //}
                }
                else
                    stepMove = 60;
            }
            else if (stepMove == 60)
            {
                Logger.Log.AppendLine(LogLevel.Info, "{0} Axis Motion Moving Time Total : {1} Ack : {2}", Name, (DateTime.Now - _startTime).TotalMilliseconds.ToString(), _ackEndTime.TotalMilliseconds.ToString());
                //Logger.Log.AppendLine(LogLevel.Error, "{0}Axis Motion Complete OK Log Msg : {1}", Name, MotionCompleteLogMsg.ToString());
                //if ((int)cmd < PositionCount)
                //    PositionMoveTime[(int)cmd] = startTime.ElapsedMilliseconds;

                stepMove = 0;
            }
        }
        public bool CheckMotorCtrlPositionSetting(Equipment equip, MoveCommand posiNo, string actionName)
        {
            if (GG.TestMode) return true;

            if (MoveCommand.HOME_POSI == posiNo)
                return true;

            int iPos = (int)posiNo;
            if ((YF_PositionPoint[iPos].vFloat != (float)Setting.GetPosition(iPos)) || (XF_PositionPointAck[iPos].vFloat != (float)Setting.GetPosition(iPos)))
            {
                AlarmMgr.Instance.Happen(equip, EM_AL_LST.AL_0722_MOTOR_POSITION_SETTING_ERROR);
                //CheckMgr.AddCheckMsg(false, string.Format("{0} {1} MOTOR CONTROL POSITION SETTING ERROR", Name, actionName));
                Logger.Log.AppendLine(LogLevel.Error, string.Format("{0} {1} MOTOR CONTROL POSITION SETTING ERROR", Name, actionName));

                equip.IsPause = true;
                return false;
            }

            if ((YF_PositionSpeed[iPos].vFloat != (float)Setting.GetSpeed(iPos)) || (XF_PositionSpeedAck[iPos].vFloat != (float)Setting.GetSpeed(iPos)))
            {
                AlarmMgr.Instance.Happen(equip, EM_AL_LST.AL_0723_MOTOR_SPEED_SETTING_ERROR);
                //CheckMgr.AddCheckMsg(false, string.Format("{0} {1} PMAC SPEED SETTING ERROR", Name, actionName));
                Logger.Log.AppendLine(LogLevel.Error, string.Format("{0} {1} PMAC SPEED SETTING ERROR", Name, actionName));

                equip.IsPause = true;
                return false;
            }

            if ((YF_PositionAccel[iPos].vFloat != (float)Setting.GetAccel(iPos)) || (XF_PositionAccelAck[iPos].vFloat != (float)Setting.GetAccel(iPos)))
            {
                AlarmMgr.Instance.Happen(equip, EM_AL_LST.AL_0724_MOTOR_ACCELERATION_SETTING_ERROR);
                //CheckMgr.AddCheckMsg(false, string.Format("{0} {1} PMAC ACCELERATION SETTING ERROR", Name, actionName));
                Logger.Log.AppendLine(LogLevel.Error, string.Format("{0} {1} PMAC ACCELERATION SETTING ERROR", Name, actionName));

                equip.IsPause = true;
                return false;
            }

            return true;
        }
        protected virtual bool IsStartedStep(Equipment equip, MoveCommand posiNo, string actionName)
        {
            if (_homeStep != 0)
            {
                InterLockMgr.AddInterLock(string.Format("Interlock <RUNNING> \n ({1} move command entered while moving {0} home.)", Name, actionName));
                Logger.Log.AppendLine(LogLevel.Warning, "{1} move command entered while moving {0} home.", Name, actionName);
                return true;
            }
            if (_startPosiStep != 0)
            {
                InterLockMgr.AddInterLock(string.Format("Interlock <RUNNING> \n ({1} move command entered while moving {0} starting point.)", Name, actionName));
                Logger.Log.AppendLine(LogLevel.Warning, "{1} move command entered while moving {0} starting point.", Name, actionName);
                return true;
            }
            for (int iPos = 0; iPos < PositionCount; iPos++)
            {
                if (_moveStep[iPos] != 0)
                {
                    InterLockMgr.AddInterLock(string.Format("Interlock <RUNNING> \n ({0} {2} move command entered during {1} move.)", Name, actionName, MoveActionName[iPos]));
                    Logger.Log.AppendLine(LogLevel.Warning, "{0} {2} move command entered during {1} move.", Name, actionName, MoveActionName[iPos]);
                    return true;
                }
            }
            return false;
        }
        public void HomeStepReset()
        {
            StepReset(0);
            YB_HomeCmd.vBit = false;
            _homeStep = 0;
        }
        public void StepReset(int posiNo)
        {
            YB_Position0MoveCmd[posiNo].vBit = false;
            _moveStep[posiNo] = 0;
        }
        public int StepNum(int posiNo)
        {
            return _moveStep[posiNo];
        }
        public static bool StartSaveToMotorCtrl(Equipment equip)
        {
            _stepSetting = 10;
            return true;
        }
        public static bool IsCompleteSaveToMotorCtrl(Equipment equip)
        {
            return _IsSettingComplete && (_stepSetting == 0);
        }
        public virtual bool CheckInterferenceInterLock(Equipment equip)
        {
            return false;
        }


        private int _stepChangeInposiBandWidth = 0;
        private bool _IsChangeInposiBandWidthComplete = false;
        private DateTime _changeStartTime = DateTime.Now;
        public bool StartChangeInposiBandWidth(Equipment equip)
        {
            _stepChangeInposiBandWidth = 10;
            return true;
        }
        public bool IsCompleteChangeInposiBandWidthComplete(Equipment equip)
        {
            return _IsChangeInposiBandWidthComplete && (_stepChangeInposiBandWidth == 0);
        }

        PlcTimerEx _tmrInposiBandWidthWorkingDelay = new PlcTimerEx("MOTOR CONTROL Delay");
        //protected void ChangeInposiBandWidthWorking(Equipment equip)
        //{
        //    if (_stepChangeInposiBandWidth > 10 && (DateTime.Now - _changeStartTime).TotalMilliseconds > ServoSettingTimeout)
        //    {
        //        Logger.Log.AppendLine(LogLevel.Error, "{0} motor inposition band Width change TIME OVER", Name);
        //        AlarmMgr.Instance.Happen(equip, SETTING_TIMEOUT);
        //        _stepChangeInposiBandWidth = 0;
        //        return;
        //    }

        //    if (_stepChangeInposiBandWidth == 0)
        //    {
        //    }
        //    else if (_stepChangeInposiBandWidth == 10)
        //    {
        //        Logger.Log.AppendLine(LogLevel.Info, "{0} motor starts change inposition bandwidth.", Name);
        //        _changeStartTime = DateTime.Now;
        //        _IsChangeInposiBandWidthComplete = false;

        //        YI_InposBandWidth.vInt = Convert.ToInt32(Setting.InpositionBandWidth);
        //        GG.MOTOR.WriteToPLC(GG.CTRL_REAR_MEM, GG.CTRL_REAR_MEM.Length);

        //        _tmrInposiBandWidthWorkingDelay.Start(1);
        //        _stepChangeInposiBandWidth = 20;
        //    }
        //    else if (_stepChangeInposiBandWidth == 20)
        //    {
        //        if (_tmrInposiBandWidthWorkingDelay)
        //        {
        //            _tmrInposiBandWidthWorkingDelay.Stop();

        //            equip.MotorCtrl.YB_InposBandWidthChangeCmd.vBit = true;
        //            _stepChangeInposiBandWidth = 30;
        //        }
        //    }
        //    else if (_stepChangeInposiBandWidth == 30)
        //    {
        //        if (equip.MotorCtrl.XB_InposBandWidthChangeCmdAck.vBit == true)
        //        {
        //            equip.MotorCtrl.YB_InposBandWidthChangeCmd.vBit = false;
        //            _tmrInposiBandWidthWorkingDelay.Start(3);
        //            _stepChangeInposiBandWidth = 40;
        //        }
        //    }
        //    else if (_stepChangeInposiBandWidth == 40)
        //    {
        //        if (equip.MotorCtrl.YB_InposBandWidthChangeCmd.vBit == false && _tmrInposiBandWidthWorkingDelay)
        //        {
        //            GG.MOTOR.ReadFromPLC(GG.ACS_REAR_MEM, GG.ACS_REAR_MEM.Length);
        //            _tmrInposiBandWidthWorkingDelay.Stop();
        //            _stepChangeInposiBandWidth = 50;
        //        }
        //    }
        //    else if (_stepChangeInposiBandWidth == 50)
        //    {
        //        if (XI_InposBandWidthAck.vInt == YI_InposBandWidth.vInt)
        //        {
        //            _stepChangeInposiBandWidth = 80;
        //        }
        //        else
        //        {
        //            _stepChangeInposiBandWidth = 60;
        //        }
        //    }
        //    else if (_stepChangeInposiBandWidth == 60)
        //    {
        //        Logger.Log.AppendLine(LogLevel.Info, "{0} motor starts change inposition bandwidth Retry.", Name);
        //        GG.MOTOR.ReadFromPLC(GG.ACS_REAR_MEM, GG.ACS_REAR_MEM.Length);
        //        //왜 또 쓰는 거지 ??? 
        //        _tmrInposiBandWidthWorkingDelay.Start(1);
        //        _stepChangeInposiBandWidth = 70;
        //    }
        //    else if (_stepChangeInposiBandWidth == 70)
        //    {
        //        if (_tmrSettingDelay)
        //        {
        //            if (XI_InposBandWidthAck.vInt == YI_InposBandWidth.vInt)
        //            {
        //                _tmrInposiBandWidthWorkingDelay.Stop();
        //                _stepChangeInposiBandWidth = 80;
        //            }
        //            else
        //            {
        //                _stepChangeInposiBandWidth = 60;
        //                _tmrInposiBandWidthWorkingDelay.Stop();
        //            }
        //        }
        //    }
        //    else if (_stepChangeInposiBandWidth == 80)
        //    {
        //        Logger.Log.AppendLine(LogLevel.Info, "{0}  motor starts change inposition bandwidth completed.", Name);
        //        _IsChangeInposiBandWidthComplete = true;
        //        _stepChangeInposiBandWidth = 0;
        //    }
        //}


    }
}
