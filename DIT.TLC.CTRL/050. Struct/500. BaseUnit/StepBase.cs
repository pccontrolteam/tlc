﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL.Step
{
    public class StepBase
    {
        public string Name;

        public StepBase(string name ="")
        {
            Name = name;
        }

        public virtual void LogicWorking(Equipment equip)
        { }
    }
}
