﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class CassetteInfo
    {
        public List<PanelInfo> LstPanel { get; set; }

        public CassetteInfo()
        {
            LstPanel = new List<PanelInfo>();
        }
    }
}
