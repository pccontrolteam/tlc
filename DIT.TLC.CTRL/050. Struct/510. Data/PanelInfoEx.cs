﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DIT.TLC.CTRL
{
    public class PanelInfoEx
    {
        public PanelInfo PnlInfo = new PanelInfo();

        [Browsable(true)]
        [Description("PANEL NO")]
        public string PNL_NO { get { return PnlInfo.PanelNo; } set { PnlInfo.PanelNo = value; } }

        public PanelInfoEx(PanelInfo info)
        {
            PnlInfo = info;
        }
    }
}
