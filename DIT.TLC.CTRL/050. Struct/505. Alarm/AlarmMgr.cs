﻿using System;
using System.IO;
using System.Text;
using System.Linq;
using System.Timers;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Drawing;
using System.Collections;
using Dit.Framework.Log;


namespace DIT.TLC.CTRL
{
    public enum EM_AL_STATE
    {
        Heavy,
        Warn,
        Unused
    }
    #region ALARM ID LIST
    public enum EM_AL_LST
    {
        AL_0000_EMERGENCY_OUTSIDE_1_ERROR,
        AL_0001_EMERGENCY_OUTISDE_2_ERROR,
        AL_0002_EMERGENCY_OUTSIDE_3_ERROR,
        AL_0003_EMERGENCY_INSIDE_1_ERROR,
        AL_0004_EMERGENCY_INSIDE_2_ERROR,
        AL_0005_EMERGENCY_INSIDE_3_ERROR,
        AL_0006_,
        AL_0007_,
        AL_0008_,
        AL_0009_,
        AL_0010_EQP_TOP_DOOR_01_OPEN_ERROR,
        AL_0011_EQP_TOP_DOOR_02_OPEN_ERROR,
        AL_0012_EQP_TOP_DOOR_03_OPEN_ERROR,
        AL_0013_EQP_TOP_DOOR_04_OPEN_ERROR,
        AL_0014_,
        AL_0015_,
        AL_0016_,
        AL_0017_,
        AL_0018_,
        AL_0019_,
        AL_0020_,
        AL_0021_,
        AL_0022_,
        AL_0023_,
        AL_0024_,
        AL_0025_,
        AL_0026_,
        AL_0027_,
        AL_0028_,
        AL_0029_,
        AL_0030_IONIZER_1_CONDITION_WARNING,
        AL_0031_IONIZER_1_ION_LEVEL_WARNING,
        AL_0032_IONIZER_1_ON_TIMEOUT_ERROR,
        AL_0033_IONIZER_1_OFF_TIMEOUT_ERROR,
        AL_0034_IONIZER_2_CONDITION_WARNING,
        AL_0035_IONIZER_2_ION_LEVEL_WARNING,
        AL_0036_IONIZER_2_ON_TIMEOUT_ERROR,
        AL_0037_IONIZER_2_OFF_TIMEOUT_ERROR,
        AL_0038_IONIZER_3_CONDITION_WARNING,
        AL_0039_IONIZER_3_ION_LEVEL_WARNING,
        AL_0040_IONIZER_3_ON_TIMEOUT_ERROR,
        AL_0041_IONIZER_3_OFF_TIMEOUT_ERROR,
        AL_0042_IONIZER_4_CONDITION_WARNING,
        AL_0043_IONIZER_4_ION_LEVEL_WARNING,
        AL_0044_IONIZER_4_ON_TIMEOUT_ERROR,
        AL_0045_IONIZER_4_OFF_TIMEOUT_ERROR,
        AL_0046_,
        AL_0047_,
        AL_0048_,
        AL_0049_,
        AL_0050_,
        AL_0051_,
        AL_0052_,
        AL_0053_,
        AL_0054_,
        AL_0055_,
        AL_0056_,
        AL_0057_,
        AL_0058_,
        AL_0059_,
        AL_0060_,
        AL_0061_,
        AL_0062_,
        AL_0063_,
        AL_0064_,
        AL_0065_,
        AL_0066_,
        AL_0067_,
        AL_0068_,
        AL_0069_,
        AL_0070_ISOLATOR1_ERROR,
        AL_0071_ISOLATOR2_ERROR,
        AL_0072_ISOLATOR3_ERROR,
        AL_0073_ISOLATOR4_ERROR,
        AL_0074_,
        AL_0075_,
        AL_0076_,
        AL_0077_,
        AL_0078_,
        AL_0079_,
        AL_0080_MAIN_N2_1_ERROR,
        AL_0081_MAIN_N2_2_ERROR,
        AL_0082_MAIN_N2_3_ERROR,
        AL_0083_MAIN_AIR_1_ERROR,
        AL_0084_,
        AL_0085_MAIN_VACUUM_1_ERROR,
        AL_0086_MAIN_VACUUM_2_ERROR,
        AL_0087_MAIN_VACUUM_3_ERROR,
        AL_0088_,
        AL_0089_,
        AL_0090_,
        AL_0091_,
        AL_0092_,
        AL_0093_ALL_VACUUM_IS_NOT_OFF_STATE_ERROR,
        AL_0094_ALL_VACUUM_IS_NOT_ON_STATE_ERROR,
        AL_0095_ALL_VACUUM_SOL_IS_NOT_OFF_STATE_ERROR,
        AL_0096_VACUUM_OFF_CMD_STOP_IN_REVIEW_RUNNING,
        AL_0097_,
        AL_0098_,
        AL_0099_,
        AL_0100_VACUUM_1_SOL_ON_TIME_OUT_ERROR,
        AL_0101_VACCUM_1_SOL_OFF_TIME_OUT_ERROR,
        AL_0102_VACUUM_2_SOL_ON_TIME_OUT_ERROR,
        AL_0103_VACCUM_2_SOL_OFF_TIME_OUT_ERROR,
        AL_0104_,
        AL_0105_,
        AL_0106_,
        AL_0107_,
        AL_0108_,
        AL_0109_,
        AL_0110_,
        AL_0111_,
        AL_0112_,
        AL_0113_,
        AL_0114_,
        AL_0115_,
        AL_0116_,
        AL_0117_,
        AL_0118_,
        AL_0119_,
        AL_0120_,
        AL_0121_,
        AL_0122_,
        AL_0123_,
        AL_0124_,
        AL_0125_,
        AL_0126_,
        AL_0127_,
        AL_0128_,
        AL_0129_,
        AL_0130_,
        AL_0131_,
        AL_0132_,
        AL_0133_,
        AL_0134_,
        AL_0135_,
        AL_0136_,
        AL_0137_,
        AL_0138_,
        AL_0139_,
        AL_0140_,
        AL_0141_,
        AL_0142_,
        AL_0143_,
        AL_0144_,
        AL_0145_,
        AL_0146_,
        AL_0147_,
        AL_0148_,
        AL_0149_,
        AL_0150_,
        AL_0151_,
        AL_0152_,
        AL_0153_,
        AL_0154_,
        AL_0155_,
        AL_0156_,
        AL_0157_,
        AL_0158_,
        AL_0159_,
        AL_0160_,
        AL_0161_,
        AL_0162_,
        AL_0163_,
        AL_0164_,
        AL_0165_,
        AL_0166_,
        AL_0167_,
        AL_0168_,
        AL_0169_,
        AL_0170_,
        AL_0171_,
        AL_0172_,
        AL_0173_,
        AL_0174_,
        AL_0175_,
        AL_0176_,
        AL_0177_,
        AL_0178_,
        AL_0179_,
        AL_0180_LIFT_PIN_IS_NOT_DOWN_POSITION_STATE_ERROR,
        AL_0181_LIFT_PIN_IS_NOT_MIDDLE_POSITION_STATE_ERROR,
        AL_0182_LIFT_PIN_INTERLOCK_ERROR,
        AL_0183_LIFT_PIN_ABNORMAL_GLASS_POSITION_ERROR,
        AL_0184_LIFT_PIN_1ST_UP_OVERTIME_ERROR,
        AL_0185_LIFT_PIN_1ST_DOWN_OVERTIME_ERROR,
        AL_0186_LIFT_PIN_2ND_UP_OVERTIME_ERROR,
        AL_0187_LIFT_PIN_2ND_DOWN_OVERTIME_ERROR,
        AL_0188_LIFT_PIN_MIDDLE_OVERTIME_ERROR,
        AL_0189_,
        AL_0190_,
        AL_0191_,
        AL_0192_,
        AL_0193_,
        AL_0194_,
        AL_0195_,
        AL_0196_,
        AL_0197_,
        AL_0198_,
        AL_0199_,
        AL_0200_ALL_CENTERING_IS_NOT_FORWARD_STATE_ERROR,
        AL_0201_ALL_CENTERING_IS_NOT_BACKWARD_STATE_ERROR,
        AL_0202_,
        AL_0203_,
        AL_0204_,
        AL_0205_,
        AL_0206_,
        AL_0207_FRONT_CENTERING1_FWD_ERROR,
        AL_0208_FRONT_CENTERING2_FWD_ERROR,
        AL_0209_FRONT_CENTERING1_BWD_ERROR,
        AL_0210_FRONT_CENTERING2_BWD_ERROR,
        AL_0211_,
        AL_0212_,
        AL_0213_,
        AL_0214_,
        AL_0215_STANDARD_CENTERING1_FWD_ERROR,
        AL_0216_STANDARD_CENTERING2_FWD_ERROR,
        AL_0217_STANDARD_CENTERING1_BWD_ERROR,
        AL_0218_STANDARD_CENTERING2_BWD_ERROR,
        AL_0219_,
        AL_0220_,
        AL_0221_,
        AL_0222_,
        AL_0223_ASSISTANCE_CENTERING1_FWD_ERROR,
        AL_0224_ASSISTANCE_CENTERING2_FWD_ERROR,
        AL_0225_ASSISTANCE_CENTERING1_BWD_ERROR,
        AL_0226_ASSISTANCE_CENTERING2_BWD_ERROR,
        AL_0227_,
        AL_0228_,
        AL_0229_,
        AL_0230_,
        AL_0231_REAR_CENTERING1_FWD_ERROR,
        AL_0232_,
        AL_0233_REAR_CENTERING1_BWD_ERROR,
        AL_0234_,
        AL_0235_,
        AL_0236_,
        AL_0237_,
        AL_0238_,
        AL_0239_,
        AL_0240_,
        AL_0241_,
        AL_0242_,
        AL_0243_,
        AL_0244_,
        AL_0245_,
        AL_0246_,
        AL_0247_,
        AL_0248_,
        AL_0249_,
        AL_0250_,
        AL_0251_,
        AL_0252_,
        AL_0253_,
        AL_0254_,
        AL_0255_,
        AL_0256_,
        AL_0257_,
        AL_0258_,
        AL_0259_,
        AL_0260_LAMP1_QBS1_LIMIT_TIME_OVER,
        AL_0261_LAMP2_QBS2_LIMIT_TIME_OVER,
        AL_0262_LAMP3_QBS3_LIMIT_TIME_OVER,
        AL_0263_LAMP4_QBS4_LIMIT_TIME_OVER,
        AL_0264_LAMP5_CRY1_1_LIMIT_TIME_OVER,
        AL_0265_LAMP6_CRY1_2_LIMIT_TIME_OVER,
        AL_0266_LAMP7_CRY2_1_LIMIT_TIME_OVER,
        AL_0267_LAMP8_CRY2_2_LIMIT_TIME_OVER,
        AL_0268_,
        AL_0269_,
        AL_0270_,
        AL_0271_,
        AL_0272_,
        AL_0273_,
        AL_0274_,
        AL_0275_,
        AL_0276_,
        AL_0277_,
        AL_0278_,
        AL_0279_,
        AL_0280_,
        AL_0281_,
        AL_0282_,
        AL_0283_,
        AL_0284_,
        AL_0285_,
        AL_0286_,
        AL_0287_,
        AL_0288_,
        AL_0289_,
        AL_0290_,
        AL_0291_,
        AL_0292_,
        AL_0293_,
        AL_0294_,
        AL_0295_,
        AL_0296_,
        AL_0297_,
        AL_0298_,
        AL_0299_,
        AL_0300_KEY_IS_CHANGED_ON_AUTO_RUN,
        AL_0301_SERVO_MC_POWER_OFF_ERROR,
        AL_0302_EFU_ALARM,
        AL_0303_,
        AL_0304_GLASS_DETECT_ERROR,
        AL_0305_PANEL_TEMPERATURE_HIGH_ERROR,
        AL_0306_PC_RACK_TEMPERATURE_HIGH_ERROR,
        AL_0307_,
        AL_0308_ROBOT_ARM_DETECT_ERROR,
        AL_0309_,
        AL_0310_,
        AL_0311_,
        AL_0312_,
        AL_0313_,
        AL_0314_AD1_ROM_ERROR,
        AL_0315_AD2_ROM_ERROR,
        AL_0316_,
        AL_0317_,
        AL_0318_,
        AL_0319_,
        AL_0320_LoadStage1LockAlcdForwardTimeOut     ,
        AL_0321_LoadStage1LockAlcdForwardTimeOut     ,
        AL_0322_LoadStage1LockAlcdBackwardTimeOut    ,
        AL_0323_LoadStage1LockAlcdBackwardTimeOut    ,
        AL_0324_LoadStage2LockAlcdForwardTimeOut     ,
        AL_0325_LoadStage2LockAlcdForwardTimeOut     ,
        AL_0326_LoadStage2LockAlcdBackwardTimeOut    ,
        AL_0327_LoadStage2LockAlcdBackwardTimeOut    ,
        AL_0328_UnldStage1LockAlcdForwardTimeOut     ,
        AL_0329_UnldStage1LockAlcdForwardTimeOut     ,
        AL_0330_UnldStage1LockAlcdBackwardTimeOut    ,
        AL_0331_UnldStage1LockAlcdBackwardTimeOut    ,
        AL_0332_UnldStage2LockAlcdForwardTimeOut     ,
        AL_0333_UnldStage2LockAlcdForwardTimeOut     ,
        AL_0334_UnldStage2LockAlcdBackwardTimeOut    ,
        AL_0335_UnldStage2LockAlcdBackwardTimeOut    ,
        AL_0336_LoadStage1ArrangeAlcdForwardTimeOut  ,
        AL_0337_LoadStage1ArrangeAlcdForwardTimeOut  ,
        AL_0338_LoadStage1ArrangeAlcdBackwardTimeOut ,
        AL_0339_LoadStage1ArrangeAlcdBackwardTimeOut ,
        AL_0340_LoadStage2ArrangeAlcdForwardTimeOut  ,
        AL_0341_LoadStage2ArrangeAlcdForwardTimeOut  ,
        AL_0342_LoadStage2ArrangeAlcdBackwardTimeOut ,
        AL_0343_LoadStage2ArrangeAlcdBackwardTimeOut ,
        AL_0344_UnldStage1ArrangeAlcdForwardTimeOut  ,
        AL_0345_UnldStage1ArrangeAlcdForwardTimeOut  ,
        AL_0346_UnldStage1ArrangeAlcdBackwardTimeOut ,
        AL_0347_UnldStage1ArrangeAlcdBackwardTimeOut ,
        AL_0348_UnldStage2ArrangeAlcdForwardTimeOut  ,
        AL_0349_UnldStage2ArrangeAlcdForwardTimeOut  ,
        AL_0350_UnldStage2ArrangeAlcdBackwardTimeOut ,
        AL_0351_UnldStage2ArrangeAlcdBackwardTimeOut ,
        AL_0352_LoadStage1AlcdForwardTimeOut     ,
        AL_0353_LoadStage1AlcdForwardTimeOut     ,
        AL_0354_LoadStage1AlcdBackwardTimeOut    ,
        AL_0355_LoadStage1AlcdBackwardTimeOut    ,
        AL_0356_LoadStage2AlcdForwardTimeOut     ,
        AL_0357_LoadStage2AlcdForwardTimeOut     ,
        AL_0358_LoadStage2AlcdBackwardTimeOut    ,
        AL_0359_LoadStage2AlcdBackwardTimeOut    ,
        AL_0360_UnldStage1AlcdForwardTimeOut     ,
        AL_0361_UnldStage1AlcdForwardTimeOut     ,
        AL_0362_UnldStage1AlcdBackwardTimeOut    ,
        AL_0363_UnldStage1AlcdBackwardTimeOut    ,
        AL_0364_UnldStage2AlcdForwardTimeOut     ,
        AL_0365_UnldStage2AlcdForwardTimeOut     ,
        AL_0366_UnldStage2AlcdBackwardTimeOut    ,
        AL_0367_UnldStage2AlcdBackwardTimeOut    ,
        AL_0368_,
        AL_0369_,
        AL_0370_,
        AL_0371_,
        AL_0372_,
        AL_0373_,
        AL_0374_,
        AL_0375_,
        AL_0376_,
        AL_0377_,
        AL_0378_,
        AL_0379_,
        AL_0380_,
        AL_0381_,
        AL_0382_,
        AL_0383_,
        AL_0384_,
        AL_0385_,
        AL_0386_,
        AL_0387_,
        AL_0388_,
        AL_0389_,
        AL_0390_PURIFIER_ALIVE_ERROR,
        AL_0391_PURIFIER_COMMAND_SIGNAL_TIMEOUT_ERROR,
        AL_0392_PURIFIER_HEAVY_ALARM,
        AL_0393_PURIFIER_LIGHT_ALARM,
        AL_0394_,
        AL_0395_,
        AL_0396_,
        AL_0397_CHAMBER_PRESSURE_GUAGE_DATE_READ_ERROR,
        AL_0398_CHAMBER_INNER_TEMPERATURE_HIGH_ERROR,
        AL_0399_CHAMBER_CDA_QUICK_PURGE_PRESSURE_DIGITAL_GATE_ERROR,
        AL_0400_,
        AL_0401_,
        AL_0402_CHAMBER_CDA_OUT_QUICK_PURGING_VALVE_OPEN_ERROR,
        AL_0403_CHAMBER_CDA_OUT_QUICK_PURGING_VALVE_CLOSE_ERROR,
        AL_0404_,
        AL_0405_,
        AL_0406_,
        AL_0407_,
        AL_0408_,
        AL_0409_,
        AL_0410_,
        AL_0411_,
        AL_0412_,
        AL_0413_,
        AL_0414_,
        AL_0415_,
        AL_0416_,
        AL_0417_,
        AL_0418_,
        AL_0419_,
        AL_0420_CHAMBER_LEAK_SENSOR_1_ERROR,
        AL_0421_CHAMBER_LEAK_SENSOR_2_ERROR,
        AL_0422_,
        AL_0423_,
        AL_0424_,
        AL_0425_,
        AL_0426_,
        AL_0427_,
        AL_0428_,
        AL_0429_,
        AL_0430_CHAMBER_PRESSURE_LOW_LIGHT_ALARM,
        AL_0431_CHAMBER_PRESSURE_LOW_HEAVY_ALARM,
        AL_0432_CHAMBER_PRESSURE_HIGH_LIGHT_ALARM,
        AL_0433_CHAMBER_PRESSURE_HIGH_HEAVY_ALARM,
        AL_0434_,
        AL_0435_,
        AL_0436_,
        AL_0437_,
        AL_0438_,
        AL_0439_,
        AL_0440_INSP_PC_ALIVE_ERROR,
        AL_0441_INSPECTION_ALARM_SIGNAL_PPID_ERROR,
        AL_0442_INSPECTION_ALARM_SIGNAL_MODULE_PC,
        AL_0443_INSPECTION_ALARM_SIGNAL_SERVER_OVERFLOW,
        AL_0444_INSPECTION_ALARM_SIGNAL_INSPECTION_OVERFLOW,
        AL_0445_INSPECTION_ALARM_SIGNAL_ALIGN,
        AL_0446_INSPECTION_ALARM_SIGNAL_LIGHT_VALUE,
        AL_0447_INSPECTION_ALARM_SIGNAL_GLASS_NG,
        AL_0448_INAPECTION_ALARM_SIGNAL_PT_OVER_SIZE,
        AL_0449_INSPECTION_ALARM_SIGNAL_GLASS_NG_WARN,
        AL_0450_INSPECTION_ALARM_SIGNAL_PT_COUNT_LIMIT,
        AL_0451_INSPECTION_ALARM_SIGNAL_BL_COUNT_LIMIT,
        AL_0452_INAPECTION_ALARM_SIGNAL_SC_COUNT_LIMIT,
        AL_0453_INSPECTION_ALARM_SIGNAL_AFM_ERROR,
        AL_0454_INSPECTION_ALARM_SIGNAL_EDGE_CRACK,
        AL_0455_INSPECTION_ALARM_SIGNAL_MASK_DEFECT,
        AL_0456_INSPECTION_ALARM_SIGNAL_CONTINUE_DEFECT,
        AL_0457_INSPECTION_ALARM_SCAN_READY_TIMEOVER,
        AL_0458_INSPECTION_ALARM_RESULT_FILE_CREATE_TIMEOVER,
        AL_0459_,
        AL_0460_INSP_PC_REQ_LOADING_ACK_SIGNAL_IS_NOT_OFF_ERROR,
        AL_0461_INSP_PC_REQ_SCAN_START_ACK_IS_NOT_OFF_ERROR,
        AL_0462_INSP_PC_REQ_VCR_READ_COMPLETE_ACK_IS_NOT_OFF_ERROR,
        AL_0463_INSP_PC_REQ_VCR_KEY_IN_COMPLETE_ACK_IS_NOT_OFF_ERROR,
        AL_0464_INSP_PC_REQ_UNLOADING_ACK_IS_NOT_OFF_ERROR,
        AL_0465_INSP_PC_REQ_DATE_TIME_SET_ACK_IS_NOT_OFF_ERROR,
        AL_0466_,
        AL_0467_,
        AL_0468_,
        AL_0469_,
        AL_0470_INSP_PC_REQUEST_STEP_ERROR,
        AL_0471_INSP_PC_REQ_LOADING_ACK_TIMEOVER,
        AL_0472_INSP_PC_REQ_SCAN_START_ACK_TIMEOVER,
        AL_0473_INSP_PC_REQ_VCR_READ_COMPLETE_ACK_TIMEOVER,
        AL_0474_INSP_PC_REQ_VCR_KEY_IN_COMPLETE_ACK_TIMEOVER,
        AL_0475_INSP_PC_REQ_UNLOADING_ACK_TIMEOVER,
        AL_0476_INSP_PC_REQ_DATE_TIME_SET_ACK_TIMEOVER,
        AL_0477_,
        AL_0478_,
        AL_0479_,
        AL_0480_INSP_PC_REQ_LOADING_HS_TIMEOVER,
        AL_0481_INSP_PC_REQ_SCAN_START_HS_TIMEOVER,
        AL_0482_INSP_PC_REQ_VCR_READ_COMPLETE_HS_TIMEOVER,
        AL_0483_INSP_PC_REQ_VCR_KEY_IN_COMPLETE_HS_TIMEOVER,
        AL_0484_INSP_PC_REQ_UNLOADING_HS_TIMEOVER,
        AL_0485_INSP_PC_REQ_TIME_CHANGE_HS_TIMEOVER,
        AL_0486_,
        AL_0487_,
        AL_0488_,
        AL_0489_,
        AL_0490_INSP_PC_EVT_SCAN_READY_SIGNAL_IS_NOT_OFF_ERROR,
        AL_0491_INSP_PC_EVT_VCR_READ_COMPLETE_SIGNAL_IS_NOT_OFF_ERROR,
        AL_0492_INSP_PC_EVT_CREATE_RESULT_FILE_COMPLETE_SIGNAL_IS_NOT_OFF_ERROR,
        AL_0493_INSP_PC_EVT_ZAXIS_DOWN_COMPLETE_SIGNAL_IS_NOT_OFF_ERROR,
        AL_0494_INSP_PC_EVT_RECIPE_CHANGE_SIGNAL_IS_NOT_OFF_ERROR,
        AL_0495_,
        AL_0496_,
        AL_0497_,
        AL_0498_,
        AL_0499_,
        AL_0500_INSP_PC_EVENT_STEP_ERROR,
        AL_0501_INSP_PC_EVT_SCAN_READY_HS_TIMEOVER,
        AL_0502_INSP_PC_EVT_VCR_READ_COMPLETE_HS_TIMEOVER,
        AL_0503_INSP_PC_EVT_CREATE_RESULT_FILE_COMPLETE_HS_TIMEOVER,
        AL_0504_INSP_PC_EVT_ZAXIS_DOWN_COMPLETE_HS_TIMEOVER,
        AL_0505_INSP_PC_EVT_RECIPE_CHANGE_HS_TIMEOVER,
        AL_0506_,
        AL_0507_,
        AL_0508_,
        AL_0509_,
        AL_0510_,
        AL_0511_,
        AL_0512_,
        AL_0513_,
        AL_0514_,
        AL_0515_,
        AL_0516_,
        AL_0517_,
        AL_0518_,
        AL_0519_,
        AL_0520_,
        AL_0521_,
        AL_0522_,
        AL_0523_,
        AL_0524_,
        AL_0525_,
        AL_0526_,
        AL_0527_,
        AL_0528_,
        AL_0529_,
        AL_0530_,
        AL_0531_,
        AL_0532_,
        AL_0533_,
        AL_0534_,
        AL_0535_,
        AL_0536_,
        AL_0537_,
        AL_0538_,
        AL_0539_,
        AL_0540_,
        AL_0541_,
        AL_0542_,
        AL_0543_,
        AL_0544_,
        AL_0545_,
        AL_0546_,
        AL_0547_,
        AL_0548_,
        AL_0549_,
        AL_0550_REVIEW_ALIVE_ERROR,
        AL_0551_REVIEW_LOADING_COMPLETE_TIMEOVER,
        AL_0552_REVIEW_ALIGN_COMPLETE_TIMEOVER,
        AL_0553_REVIEW_READY_COMPLETE_TIMEOVER,
        AL_0554_REVIEW_REVIEW_COMPLETE_TIMEOVER,
        AL_0555_REVIEW_RESULT_FILE_CREATE_COMPLETE_TIMEOVER,
        AL_0556_REVIEW_LOADING_FAIL,
        AL_0557_REVIEW_ALIGN_FAIL,
        AL_0558_REVIEW_REVIEW_FAIL,
        AL_0559_,
        AL_0560_,
        AL_0561_,
        AL_0562_,
        AL_0563_,
        AL_0564_,
        AL_0565_,
        AL_0566_,
        AL_0567_,
        AL_0568_,
        AL_0569_,
        AL_0570_REVIEW_PC_REQ_LOADING_ACK_IS_NOT_OFF_ERROR,
        AL_0571_REVIEW_PC_REQ_ALIGN_START_ACK_IS_NOT_OFF_ERROR,
        AL_0572_REVIEW_PC_REQ_RESULT_FILE_READ_REQ_ACK_IS_NOT_OFF_ERROR,
        AL_0573_REVIEW_PC_REQ_REVIEW_START_ACK_IS_NOT_OFF_ERROR,
        AL_0574_REVIEW_PC_REQ_REVIEW_END_ACK_IS_NOT_OFF_ERROR,
        AL_0575_REVIEW_PC_REQ_AFM_HOME_MOVE_ACK_IS_NOT_OFF_ERROR,
        AL_0576_REVIEW_PC_REQ_WSI_HOME_MOVE_ACK_IS_NOT_OFF_ERROR,
        AL_0577_REVIEW_PC_REQ_DATE_TIME_SET_ACK_IS_NOT_OFF_ERROR,
        AL_0578_,
        AL_0579_,
        AL_0580_REVIEW_PC_REQUEST_STEP_ERROR,
        AL_0581_REVIEW_PC_REQ_LOADING_ACK_TIMEOVER,
        AL_0582_REVIEW_PC_REQ_ALIGN_START_ACK_TIMEOVER,
        AL_0583_REVIEW_PC_REQ_RESULT_FILE_READ_REQ_ACK_TIMEOVER,
        AL_0584_REVIEW_PC_REQ_REVIEW_START_ACK_TIMEOVER,
        AL_0585_REVIEW_PC_REQ_REVIEW_END_ACK_TIMEOVER,
        AL_0586_REVIEW_PC_REQ_AFM_HOME_MOVE_ACK_TIMEOVER,
        AL_0587_REVIEW_PC_REQ_WSI_HOME_MOVE_ACK_TIMEOVER,
        AL_0588_REVIEW_PC_REQ_DATE_TIME_SET_ACK_TIMEOVER,
        AL_0589_,
        AL_0590_REVIEW_PC_REQ_LOADING_HS_TIMEOVER,
        AL_0591_REVIEW_PC_REQ_ALIGN_START_HS_TIMEOVER,
        AL_0592_REVIEW_PC_REQ_RESULT_FILE_READ_REQ_HS_TIMEOVER,
        AL_0593_REVIEW_PC_REQ_REVIEW_START_HS_TIMEOVER,
        AL_0594_REVIEW_PC_REQ_REVIEW_END_HS_TIMEOVER,
        AL_0595_REVIEW_PC_REQ_AFM_HOME_MOVE_HS_TIMEOVER,
        AL_0596_REVIEW_PC_REQ_WSI_HOME_MOVE_HS_TIMEOVER,
        AL_0597_REVIEW_PC_REQ_DATE_TIME_SET_HS_TIMEOVER,
        AL_0598_,
        AL_0599_,
        AL_0600_REVIEW_PC_EVT_LOADING_COMPLETE_SIGNAL_IS_NOT_OFF_ERROR,
        AL_0601_REVIEW_PC_EVT_ALIGN_COMPELTE_SIGNAL_IS_NOT_OFF_ERROR,
        AL_0602_REVIEW_PC_EVT_REVIEW_READY_SIGNAL_IS_NOT_OFF_ERROR,
        AL_0603_REVIEW_PC_EVT_REVIEW_COMPLETE_SIGNAL_IS_NOT_OFF_ERROR,
        AL_0604_REVIEW_PC_EVT_RESULT_FILE_CREATE_COMPLETE_SIGNAL_IS_NOT_OFF_ERROR,
        AL_0605_REVIEW_PC_EVT_ALARM_OCCUR_SIGNAL_IS_NOT_OFF_ERROR,
        AL_0606_,
        AL_0607_,
        AL_0608_,
        AL_0609_,
        AL_0610_REVIEW_PC_EVENT_STEP_ERROR,
        AL_0611_REVIEW_PC_EVT_LOADING_COMPLETE_HS_TIMEOVER,
        AL_0612_REVIEW_PC_EVT_ALIGN_COMPELTE_HS_TIMEOVER,
        AL_0613_REVIEW_PC_EVT_REVIEW_READY_HS_TIMEOVER,
        AL_0614_REVIEW_PC_EVT_REVIEW_COMPLETE_HS_TIMEOVER,
        AL_0615_REVIEW_PC_EVT_RESULT_FILE_CREATE_COMPLETE_HS_TIMEOVER,
        AL_0616_REVIEW_PC_EVT_ALARM_OCCUR_HS_TIMEOVER,
        AL_0617_,
        AL_0618_,
        AL_0619_,
        AL_0620_PIO_GLASS_DETECT_ERROR,
        AL_0621_,
        AL_0622_,
        AL_0623_,
        AL_0624_,
        AL_0625_,
        AL_0626_,
        AL_0627_PIO_RECV_T2_TIME_OUT,
        AL_0628_PIO_RECV_T3_TIME_OUT,
        AL_0629_,
        AL_0630_,
        AL_0631_,
        AL_0632_,
        AL_0633_PIO_SEND_T2_TIME_OUT,
        AL_0634_PIO_SEND_T3_TIME_OUT,
        AL_0635_,
        AL_0636_,
        AL_0637_,
        AL_0638_PIO_EXCH_T2_TIME_OUT,
        AL_0639_PIO_EXCH_T3_TIME_OUT,
        AL_0640_SYNC_SERVER_RUN_ERROR,
        AL_0641_,
        AL_0642_,
        AL_0643_,
        AL_0644_,
        AL_0645_ALARM_TEST_1,
        AL_0646_ALARM_TEST_2,
        AL_0647_ALARM_TEST_3,
        AL_0648_,
        AL_0649_,
        AL_0650_T1_OVER_TIME,
        AL_0651_,
        AL_0652_,
        AL_0653_,
        AL_0654_,
        AL_0655_,
        AL_0656_,
        AL_0657_,
        AL_0658_,
        AL_0659_,
        AL_0660_RECV_GLASS_DATA_PPID_ERROR,
        AL_0661_,
        AL_0662_,
        AL_0663_,
        AL_0664_,
        AL_0665_,
        AL_0666_,
        AL_0667_,
        AL_0668_,
        AL_0669_,
        AL_0670_RECV_JOB_REPORT_T1,
        AL_0671_SEND_OUT_JOB_REPORT1,
        AL_0672_STORED_JOB_REPORT_T1,
        AL_0673_FETCHED_OUT_JOB_REPORT_T1,
        AL_0674_REMOVED_JOB_REPORT_T1,
        AL_0675_GLASS_PROCESS_START_REPORT_T1,
        AL_0676_GLASS_PROCESS_END_REPORT_T1,
        AL_0677_MACHINE_STATUS_CHANGE_REPORT_T1,
        AL_0678_ALARM_STATUS_CHANGE_REPORT_T1,
        AL_0679_JOB_DATA_REQUEST_T1,
        AL_0680_JOB_DATA_CHANGE_REQUEST_T1,
        AL_0681_PROCESS_DATE_REPORT_T1,
        AL_0682_CURRENT_RECIPEID_CHANGE_REPORT_T1,
        AL_0683_RECIPE_LIST_CHANGE_REPORT_T1,
        AL_0684_RECIPE_PARAMETER_DATA_CHANGE_REPORT_T1,
        AL_0685_CIM_MEASSAGE_CONFIRM_REPORT_T1,
        AL_0686_VCR_EVENT_REPORT_T1,
        AL_0687_VCR_MODE_CHANGE_REPORT_T1,
        AL_0688_PANEL_DATA_UPDATE_REPORT_T1,
        AL_0689_JOB_JUDGE_RESULT_REPORT_T1,
        AL_0690_MACHINE_MODE_CHANGE_REPORT_T1,
        AL_0691_LOADING_STOP_REQUEST_REPORT_T1,
        AL_0692_LOADING_STOP_RELEASE_REPORT_T1,
        AL_0693_,
        AL_0694_,
        AL_0695_,
        AL_0696_,
        AL_0697_,
        AL_0698_,
        AL_0699_,
        AL_0700_CIM_MODE_CHANGE_CMD_T2,
        AL_0701_RECIPE_REGISTER_CHECK_CMD_T2,
        AL_0702_RECIPE_PARAMETER_REQUEST_CMD_T2,
        AL_0703_CIM_MESSAGE_SET_CMD_T2,
        AL_0704_CIM_MESSAGE_CLEAR_CMD_T2,
        AL_0705_DATE_TIME_SET_CMD_T2,
        AL_0706_VCR_MODE_CHANGE_CMD_T2,
        AL_0707_MACHINE_MODE_CHANGE_CMD_T2,
        AL_0708_,
        AL_0709_,
        AL_0710_,
        AL_0711_,
        AL_0712_,
        AL_0713_,
        AL_0714_,
        AL_0715_,
        AL_0716_,
        AL_0717_,
        AL_0718_,
        AL_0719_,
        AL_0720_MOTOR_HEAVY_ERROR,
        AL_0721_MOTOR_ALIVE_ERROR,
        AL_0722_MOTOR_POSITION_SETTING_ERROR,
        AL_0723_MOTOR_SPEED_SETTING_ERROR,
        AL_0724_MOTOR_ACCELERATION_SETTING_ERROR,
        AL_0725_MOTOR_CONNECTION_ERROR,
        AL_0726_,
        AL_0727_MOTOR_CMD_ACK_SIGNAL_TIMEOVER,
        AL_0728_MOTOR_CMD_STOP_IN_REVIEW_RUNNING,
        AL_0729_,
        AL_0730_MOTOR_EMS_ROBOT_ARM_DETECT_STATE,
        AL_0731_MOTOR_EMS_VACUUM_OFF_STATE,
        AL_0732_MOTOR_EMS_GLASS_DETECT_ERROR_STATE,
        AL_0733_MOTOR_EMS_LIFT_PIN_UP_STATE,
        AL_0734_MOTOR_EMS_REVIEW_RUNNING_STATE,
        AL_0735_,
        AL_0736_,
        AL_0737_,
        AL_0738_,
        AL_0739_,
        AL_0740_SCAN_X_PLUS_LIMIT_ERROR,
        AL_0741_SCAN_X_MINUS_LIMIT_ERROR,
        AL_0742_SCAN_X_MOTOR_SERVO_ON_ERROR,
        AL_0743_SCAN_X_CRITICAL_POSITION_ERROR,
        AL_0744_SCAN_X_DRIVA_FAULT_ERROR,
        AL_0745_SCAN_X_OVER_CURRENT_ERROR,
        AL_0746_SCAN_X_ECALL_ERROR,
        AL_0747_SCAN_X_MOVE_OVERTIME_ERROR,
        AL_0748_SCAN_X_SETTING_TIMEOUT_ERROR,
        AL_0749_SCAN_X_BUFFER_RESET_ERROR,
        AL_0750_SCAN_X_MOTOR_IS_NOT_MOVING_ERROR,
        AL_0751_SCAN_X_IS_NOT_LOADING_POSITION_STATE_ERROR,
        AL_0752_,
        AL_0753_,
        AL_0754_,
        AL_0755_INSP_Y_PLUS_LIMIT_ERROR,
        AL_0756_INSP_Y_MINUS_LIMIT_ERROR,
        AL_0757_INSP_Y_MOTOR_SERVO_ON_ERROR,
        AL_0758_INSP_Y_CRITICAL_POSITION_ERROR,
        AL_0759_INSP_Y_DRIVA_FAULT_ERROR,
        AL_0760_INSP_Y_OVER_CURRENT_ERROR,
        AL_0761_INSP_Y_ECALL_ERROR,
        AL_0762_INSP_Y_MOVE_OVERTIME_ERROR,
        AL_0763_INSP_Y_SETTING_TIMEOUT_ERROR,
        AL_0764_INSP_Y_BUFFER_RESET_ERROR,
        AL_0765_INSP_Y_MOTOR_IS_NOT_MOVING_ERROR,
        AL_0766_INSP_Y_IS_NOT_LOADING_POSITION_STATE_ERROR,
        AL_0767_,
        AL_0768_,
        AL_0769_,
        AL_0770_REVI_Y_PLUS_LIMIT_ERROR,
        AL_0771_REVI_Y_MINUS_LIMIT_ERROR,
        AL_0772_REVI_Y_MOTOR_SERVO_ON_ERROR,
        AL_0773_REVI_Y_CRITICAL_POSITION_ERROR,
        AL_0774_REVI_Y_DRIVA_FAULT_ERROR,
        AL_0775_REVI_Y_OVER_CURRENT_ERROR,
        AL_0776_REVI_Y_ECALL_ERROR,
        AL_0777_REVI_Y_MOVE_OVERTIME_ERROR,
        AL_0778_REVI_Y_SETTING_TIMEOUT_ERROR,
        AL_0779_REVI_Y_BUFFER_RESET_ERROR,
        AL_0780_REVI_Y_MOTOR_IS_NOT_MOVING_ERROR,
        AL_0781_REVI_Y_IS_NOT_LOADING_POSITION_STATE_ERROR,
        AL_0782_,
        AL_0783_,
        AL_0784_,
        AL_0785_INSP_Z0_PLUS_LIMIT_ERROR,
        AL_0786_INSP_Z0_MINUS_LIMIT_ERROR,
        AL_0787_INSP_Z0_MOTOR_SERVO_ON_ERROR,
        AL_0788_INSP_Z0_CRITICAL_POSITION_ERROR,
        AL_0789_INSP_Z0_DRIVA_FAULT_ERROR,
        AL_0790_INSP_Z0_OVER_CURRENT_ERROR,
        AL_0791_INSP_Z0_ECALL_ERROR,
        AL_0792_INSP_Z0_MOVE_OVERTIME_ERROR,
        AL_0793_INSP_Z0_SETTING_TIMEOUT_ERROR,
        AL_0794_INSP_Z0_MOTOR_IS_NOT_MOVING_ERROR,
        AL_0795_,
        AL_0796_,
        AL_0797_,
        AL_0798_,
        AL_0799_,
        AL_0800_INSP_Z1_PLUS_LIMIT_ERROR,
        AL_0801_INSP_Z1_MINUS_LIMIT_ERROR,
        AL_0802_INSP_Z1_MOTOR_SERVO_ON_ERROR,
        AL_0803_INSP_Z1_CRITICAL_POSITION_ERROR,
        AL_0804_INSP_Z1_DRIVA_FAULT_ERROR,
        AL_0805_INSP_Z1_OVER_CURRENT_ERROR,
        AL_0806_INSP_Z1_ECALL_ERROR,
        AL_0807_INSP_Z1_MOVE_OVERTIME_ERROR,
        AL_0808_INSP_Z1_SETTING_TIMEOUT_ERROR,
        AL_0809_INSP_Z1_MOTOR_IS_NOT_MOVING_ERROR,
        AL_0810_,
        AL_0811_,
        AL_0812_,
        AL_0813_,
        AL_0814_,
        AL_0815_INSP_Z2_PLUS_LIMIT_ERROR,
        AL_0816_INSP_Z2_MINUS_LIMIT_ERROR,
        AL_0817_INSP_Z2_MOTOR_SERVO_ON_ERROR,
        AL_0818_INSP_Z2_CRITICAL_POSITION_ERROR,
        AL_0819_INSP_Z2_DRIVA_FAULT_ERROR,
        AL_0820_INSP_Z2_OVER_CURRENT_ERROR,
        AL_0821_INSP_Z2_ECALL_ERROR,
        AL_0822_INSP_Z2_MOVE_OVERTIME_ERROR,
        AL_0823_INSP_Z2_SETTING_TIMEOUT_ERROR,
        AL_0824_INSP_Z2_MOTOR_IS_NOT_MOVING_ERROR,
        AL_0825_,
        AL_0826_,
        AL_0827_,
        AL_0828_,
        AL_0829_,
        AL_0830_INSP_Z3_PLUS_LIMIT_ERROR,
        AL_0831_INSP_Z3_MINUS_LIMIT_ERROR,
        AL_0832_INSP_Z3_MOTOR_SERVO_ON_ERROR,
        AL_0833_INSP_Z3_CRITICAL_POSITION_ERROR,
        AL_0834_INSP_Z3_DRIVA_FAULT_ERROR,
        AL_0835_INSP_Z3_OVER_CURRENT_ERROR,
        AL_0836_INSP_Z3_ECALL_ERROR,
        AL_0837_INSP_Z3_MOVE_OVERTIME_ERROR,
        AL_0838_INSP_Z3_SETTING_TIMEOUT_ERROR,
        AL_0839_INSP_Z3_MOTOR_IS_NOT_MOVING_ERROR,
        AL_0840_,
        AL_0841_,
        AL_0842_,
        AL_0843_,
        AL_0844_,
        AL_0845_INSP_Z4_PLUS_LIMIT_ERROR,
        AL_0846_INSP_Z4_MINUS_LIMIT_ERROR,
        AL_0847_INSP_Z4_MOTOR_SERVO_ON_ERROR,
        AL_0848_INSP_Z4_CRITICAL_POSITION_ERROR,
        AL_0849_INSP_Z4_DRIVA_FAULT_ERROR,
        AL_0850_INSP_Z4_OVER_CURRENT_ERROR,
        AL_0851_INSP_Z4_ECALL_ERROR,
        AL_0852_INSP_Z4_MOVE_OVERTIME_ERROR,
        AL_0853_INSP_Z4_SETTING_TIMEOUT_ERROR,
        AL_0854_INSP_Z4_MOTOR_IS_NOT_MOVING_ERROR,
        AL_0855_,
        AL_0856_,
        AL_0857_,
        AL_0858_,
        AL_0859_,
        AL_0860_INSP_Z5_PLUS_LIMIT_ERROR,
        AL_0861_INSP_Z5_MINUS_LIMIT_ERROR,
        AL_0862_INSP_Z5_MOTOR_SERVO_ON_ERROR,
        AL_0863_INSP_Z5_CRITICAL_POSITION_ERROR,
        AL_0864_INSP_Z5_DRIVA_FAULT_ERROR,
        AL_0865_INSP_Z5_OVER_CURRENT_ERROR,
        AL_0866_INSP_Z5_ECALL_ERROR,
        AL_0867_INSP_Z5_MOVE_OVERTIME_ERROR,
        AL_0868_INSP_Z5_SETTING_TIMEOUT_ERROR,
        AL_0869_INSP_Z5_MOTOR_IS_NOT_MOVING_ERROR,
        AL_0870_,
        AL_0871_,
        AL_0872_,
        AL_0873_,
        AL_0874_,
        AL_0875_INSP_Z6_PLUS_LIMIT_ERROR,
        AL_0876_INSP_Z6_MINUS_LIMIT_ERROR,
        AL_0877_INSP_Z6_MOTOR_SERVO_ON_ERROR,
        AL_0878_INSP_Z6_CRITICAL_POSITION_ERROR,
        AL_0879_INSP_Z6_DRIVA_FAULT_ERROR,
        AL_0880_INSP_Z6_OVER_CURRENT_ERROR,
        AL_0881_INSP_Z6_ECALL_ERROR,
        AL_0882_INSP_Z6_MOVE_OVERTIME_ERROR,
        AL_0883_INSP_Z6_SETTING_TIMEOUT_ERROR,
        AL_0884_INSP_Z6_MOTOR_IS_NOT_MOVING_ERROR,
        AL_0885_,
        AL_0886_,
        AL_0887_,
        AL_0888_,
        AL_0889_,
        AL_0890_INSP_Z7_PLUS_LIMIT_ERROR,
        AL_0891_INSP_Z7_MINUS_LIMIT_ERROR,
        AL_0892_INSP_Z7_MOTOR_SERVO_ON_ERROR,
        AL_0893_INSP_Z7_CRITICAL_POSITION_ERROR,
        AL_0894_INSP_Z7_DRIVA_FAULT_ERROR,
        AL_0895_INSP_Z7_OVER_CURRENT_ERROR,
        AL_0896_INSP_Z7_ECALL_ERROR,
        AL_0897_INSP_Z7_MOVE_OVERTIME_ERROR,
        AL_0898_INSP_Z7_SETTING_TIMEOUT_ERROR,
        AL_0899_INSP_Z7_MOTOR_IS_NOT_MOVING_ERROR,
        AL_0900_,
        AL_0901_,
        AL_0902_,
        AL_0903_,
        AL_0904_,
        AL_0905_INSP_Z8_PLUS_LIMIT_ERROR,
        AL_0906_INSP_Z8_MINUS_LIMIT_ERROR,
        AL_0907_INSP_Z8_MOTOR_SERVO_ON_ERROR,
        AL_0908_INSP_Z8_CRITICAL_POSITION_ERROR,
        AL_0909_INSP_Z8_DRIVA_FAULT_ERROR,
        AL_0910_INSP_Z8_OVER_CURRENT_ERROR,
        AL_0911_INSP_Z8_ECALL_ERROR,
        AL_0912_INSP_Z8_MOVE_OVERTIME_ERROR,
        AL_0913_INSP_Z8_SETTING_TIMEOUT_ERROR,
        AL_0914_INSP_Z8_MOTOR_IS_NOT_MOVING_ERROR,
        AL_0915_,
        AL_0916_,
        AL_0917_,
        AL_0918_,
        AL_0919_,
        AL_0920_INSP_Z9_PLUS_LIMIT_ERROR,
        AL_0921_INSP_Z9_MINUS_LIMIT_ERROR,
        AL_0922_INSP_Z9_MOTOR_SERVO_ON_ERROR,
        AL_0923_INSP_Z9_CRITICAL_POSITION_ERROR,
        AL_0924_INSP_Z9_DRIVA_FAULT_ERROR,
        AL_0925_INSP_Z9_OVER_CURRENT_ERROR,
        AL_0926_INSP_Z9_ECALL_ERROR,
        AL_0927_INSP_Z9_MOVE_OVERTIME_ERROR,
        AL_0928_INSP_Z9_SETTING_TIMEOUT_ERROR,
        AL_0929_INSP_Z9_MOTOR_IS_NOT_MOVING_ERROR,
        AL_0930_,
        AL_0931_,
        AL_0932_,
        AL_0933_,
        AL_0934_,
        AL_0935_INSP_Z10_PLUS_LIMIT_ERROR,
        AL_0936_INSP_Z10_MINUS_LIMIT_ERROR,
        AL_0937_INSP_Z10_MOTOR_SERVO_ON_ERROR,
        AL_0938_INSP_Z10_CRITICAL_POSITION_ERROR,
        AL_0939_INSP_Z10_DRIVA_FAULT_ERROR,
        AL_0940_INSP_Z10_OVER_CURRENT_ERROR,
        AL_0941_INSP_Z10_ECALL_ERROR,
        AL_0942_INSP_Z10_MOVE_OVERTIME_ERROR,
        AL_0943_INSP_Z10_SETTING_TIMEOUT_ERROR,
        AL_0944_INSP_Z10_MOTOR_IS_NOT_MOVING_ERROR,
        AL_0945_,
        AL_0946_,
        AL_0947_,
        AL_0948_,
        AL_0949_,
        AL_0950_INSP_Z11_PLUS_LIMIT_ERROR,
        AL_0951_INSP_Z11_MINUS_LIMIT_ERROR,
        AL_0952_INSP_Z11_MOTOR_SERVO_ON_ERROR,
        AL_0953_INSP_Z11_CRITICAL_POSITION_ERROR,
        AL_0954_INSP_Z11_DRIVA_FAULT_ERROR,
        AL_0955_INSP_Z11_OVER_CURRENT_ERROR,
        AL_0956_INSP_Z11_ECALL_ERROR,
        AL_0957_INSP_Z11_MOVE_OVERTIME_ERROR,
        AL_0958_INSP_Z11_SETTING_TIMEOUT_ERROR,
        AL_0959_INSP_Z11_MOTOR_IS_NOT_MOVING_ERROR,
        AL_0960_,
        AL_0961_,
        AL_0962_,
        AL_0963_,
        AL_0964_,
        AL_0965_INSP_Z12_PLUS_LIMIT_ERROR,
        AL_0966_INSP_Z12_MINUS_LIMIT_ERROR,
        AL_0967_INSP_Z12_MOTOR_SERVO_ON_ERROR,
        AL_0968_INSP_Z12_CRITICAL_POSITION_ERROR,
        AL_0969_INSP_Z12_DRIVA_FAULT_ERROR,
        AL_0970_INSP_Z12_OVER_CURRENT_ERROR,
        AL_0971_INSP_Z12_ECALL_ERROR,
        AL_0972_INSP_Z12_MOVE_OVERTIME_ERROR,
        AL_0973_INSP_Z12_SETTING_TIMEOUT_ERROR,
        AL_0974_INSP_Z12_MOTOR_IS_NOT_MOVING_ERROR,
        AL_0975_,
        AL_0976_,
        AL_0977_,
        AL_0978_,
        AL_0979_,
        AL_0980_,
        AL_0981_,
        AL_0982_,
        AL_0983_,
        AL_0984_,
        AL_0985_,
        AL_0986_,
        AL_0987_,
        AL_0988_,
        AL_0989_,
        AL_0990_,
        AL_0991_,
        AL_0992_,
        AL_0993_,
        AL_0994_,
        AL_0995_,
        AL_0996_,
        AL_0997_,
        AL_0998_,
        AL_0999_,
        AL_1000_,
        AL_1001_,
        AL_1002_,
        AL_1003_,
        AL_1004_,
        AL_1005_,
        AL_1006_,
        AL_1007_,
        AL_1008_,
        AL_1009_,
        AL_1010_,
        AL_1011_,
        AL_1012_,
        AL_1013_,
        AL_1014_,
        AL_1015_,
        AL_1016_,
        AL_1017_,
        AL_1018_,
        AL_1019_,
        AL_1020_,
        AL_1021_,
        AL_1022_,
        AL_1023_,
        AL_1024_,
        AL_1025_,
        AL_1026_,
        AL_1027_,
        AL_1028_,
        AL_1029_,
        AL_1030_,
        AL_1031_,
        AL_1032_,
        AL_1033_,
        AL_1034_,
        AL_1035_,
        AL_1036_,
        AL_1037_,
        AL_1038_,
        AL_1039_,
        AL_1040_,
        AL_1041_,
        AL_1042_,
        AL_1043_,
        AL_1044_,
        AL_1045_,
        AL_1046_,
        AL_1047_,
        AL_1048_,
        AL_1049_,
        AL_1050_,
        AL_1051_,
        AL_1052_,
        AL_1053_,
        AL_1054_,
        AL_1055_,
        AL_1056_,
        AL_1057_,
        AL_1058_,
        AL_1059_,
        AL_1060_,
        AL_1061_,
        AL_1062_,
        AL_1063_,
        AL_1064_,
        AL_1065_,
        AL_1066_,
        AL_1067_,
        AL_1068_,
        AL_1069_,
        AL_1070_,
        AL_1071_,
        AL_1072_,
        AL_1073_,
        AL_1074_,
        AL_1075_,
        AL_1076_,
        AL_1077_,
        AL_1078_,
        AL_1079_,
        AL_1080_,
        AL_1081_,
        AL_1082_,
        AL_1083_,
        AL_1084_,
        AL_1085_,
        AL_1086_,
        AL_1087_,
        AL_1088_,
        AL_1089_,
        AL_1090_,
        AL_1091_,
        AL_1092_,
        AL_1093_,
        AL_1094_,
        AL_1095_,
        AL_1096_,
        AL_1097_,
        AL_1098_,
        AL_1099_,
        AL_1100_,
        AL_1101_,
        AL_1102_,
        AL_1103_,
        AL_1104_,
        AL_1105_,
        AL_1106_,
        AL_1107_,
        AL_1108_,
        AL_1109_,
        AL_1110_,
        AL_1111_,
        AL_1112_,
        AL_1113_,
        AL_1114_,
        AL_1115_,
        AL_1116_,
        AL_1117_,
        AL_1118_,
        AL_1119_,
        AL_1120_,
        AL_1121_,
        AL_1122_,
        AL_1123_,
        AL_1124_,
        AL_1125_,
        AL_1126_,
        AL_1127_,
        AL_1128_,
        AL_1129_,
        AL_1130_,
        AL_1131_,
        AL_1132_,
        AL_1133_,
        AL_1134_,
        AL_1135_,
        AL_1136_,
        AL_1137_,
        AL_1138_,
        AL_1139_,
        AL_1140_,
        AL_1141_,
        AL_1142_,
        AL_1143_,
        AL_1144_,
        AL_1145_,
        AL_1146_,
        AL_1147_,
        AL_1148_,
        AL_1149_,
        AL_1150_,
        AL_1151_,
        AL_1152_,
        AL_1153_,
        AL_1154_,
        AL_1155_,
        AL_1156_,
        AL_1157_,
        AL_1158_,
        AL_1159_,
        AL_1160_,
        AL_1161_,
        AL_1162_,
        AL_1163_,
        AL_1164_,
        AL_1165_,
        AL_1166_,
        AL_1167_,
        AL_1168_,
        AL_1169_,
        AL_1170_,
        AL_1171_,
        AL_1172_,
        AL_1173_,
        AL_1174_,
        AL_1175_,
        AL_1176_,
        AL_1177_,
        AL_1178_,
        AL_1179_,
        AL_1180_,
        AL_1181_,
        AL_1182_,
        AL_1183_,
        AL_1184_,
        AL_1185_,
        AL_1186_,
        AL_1187_,
        AL_1188_,
        AL_1189_,
        AL_1190_,
        AL_1191_,
        AL_1192_,
        AL_1193_,
        AL_1194_,
        AL_1195_,
        AL_1196_,
        AL_1197_,
        AL_1198_,
        AL_1199_,
        AL_1200_,

        AL_NONE,
        AL_0531_PMAC_EVENT_SIGNAL_TIMEOVER,
        AL_0532_PMAC_ALIVE_ERROR,
    }

    #endregion
    public class AlarmMgr
    {
        // 필드 1
        public static string PATH_SETTING = Path.Combine(Application.StartupPath, "Setting", "Alarm.ini");

        private System.Timers.Timer _logTimer;
        public List<ListView> LstlogAlarms = new List<ListView>();
        public List<ListView> LstlogAlarmHistory = new List<ListView>();
        public DataGridView LstvSettingAlarm = null;

        public SortedList<EM_AL_LST, Alarm> HappenAlarms = new SortedList<EM_AL_LST, Alarm>();
        private Queue<Alarm> _queShowAlarm = new Queue<Alarm>();
        private Queue<ListViewItem> _quePastItem = new Queue<ListViewItem>();

        private int timerInterval = 500;
        private ImageList _imgList = new ImageList();

        //싱클던 
        private static AlarmMgr _selfInstance = null;
        public static AlarmMgr Instance
        {
            get
            {
                if (_selfInstance == null)
                    _selfInstance = new AlarmMgr();
                return _selfInstance;
            }
        }

        //private System.Threading.Monitor _montor;
        private AlarmMgr()
        {
            InitializeAlarmList();

            _logTimer = new System.Timers.Timer(timerInterval);
            _logTimer.Elapsed += delegate(object sender, ElapsedEventArgs e)
            {
                while (_queShowAlarm.Count > 0)
                {
                    try
                    {
                        if (System.Threading.Monitor.TryEnter(_queShowAlarm))
                        {
                            Alarm al = _queShowAlarm.Dequeue();
                            foreach (ListView lstv in LstlogAlarms)
                            {
                                if (lstv != null)
                                {
                                    string desc = string.IsNullOrEmpty(al.Desc) ? al.ID.ToString() : al.Desc;
                                    ListViewItem item = new ListViewItem(new string[] { 
                                                        "", 
                                                        al.HappenTime.ToString("MM-dd HH:mm:ss"), 
                                                        ((int)al.ID).ToString(),                                                        
                                                        desc });

                                    if (al.State == EM_AL_STATE.Heavy)
                                        item.ForeColor = Color.Red;
                                    else if (al.State == EM_AL_STATE.Warn)
                                        item.ForeColor = Color.DarkOrange;
                                    else if (al.State == EM_AL_STATE.Unused)
                                        item.ForeColor = Color.Gray;

                                    lstv.Items.Insert(0, item);

                                    if (lstv.Items.Count > 200)
                                    {
                                        PastToHistoryListView(ref LstlogAlarms, ref LstlogAlarmHistory);
                                    }
                                }
                            }
                            System.Threading.Monitor.Exit(_queShowAlarm);
                        }
                        else
                            break;
                    }
                    catch (System.Exception ex)
                    {
                        System.Threading.Monitor.Exit(_queShowAlarm);
                        Console.WriteLine(ex.Message);
                        break;
                    }
                }
                while (_quePastItem.Count > 0)
                {
                    try
                    {
                        if (System.Threading.Monitor.TryEnter(_quePastItem))
                        {
                            foreach (ListView lstvHistory in LstlogAlarmHistory)
                            {
                                lstvHistory.Items.Insert(0, _quePastItem.Dequeue());
                                if (lstvHistory != null)
                                {
                                    if (lstvHistory.Items.Count > 200)
                                        lstvHistory.Items.RemoveAt(200);
                                }
                            }
                            System.Threading.Monitor.Exit(_quePastItem);
                        }
                        else
                            break;
                    }
                    catch (System.Exception ex)
                    {
                        System.Threading.Monitor.Exit(_quePastItem);
                        Console.WriteLine(ex.Message);
                        break;
                    }
                }
            };

            _logTimer.Start();
        }
        private void PastToHistoryListView(ref List<ListView> source, ref List<ListView> dest)
        {
            foreach (ListView lstv in source)
            {
                try
                {
                    if (System.Threading.Monitor.TryEnter(lstv))
                    {
                        DateTime st = DateTime.Now;
                        int cnt = lstv.Items.Count;
                        if (LstlogAlarms.IndexOf(lstv) == 0)
                        {
                            for (int itemIter = lstv.Items.Count - 1; itemIter >= 0; itemIter--)
                            {
                                foreach (ListView lstvHistory in dest)
                                    _quePastItem.Enqueue(lstv.Items[itemIter].Clone() as ListViewItem);
                            }
                        }
                        lstv.Items.Clear();
                        System.Threading.Monitor.Exit(lstv);
                    }
                }
                catch (System.Exception ex)
                {
                    System.Threading.Monitor.Exit(lstv);
                    Console.WriteLine(ex.Message);
                }
            }
        }
        public bool Happen(Equipment equip, EM_AL_LST id)
        {
            if (HappenAlarms.ContainsKey(id) == true)
            {
                if (HappenAlarms[id].Happen == false)
                {
                    bool happenAlarm = true;
                    HappenAlarms[id].ID = id;
                    HappenAlarms[id].Happen = happenAlarm;
                    HappenAlarms[id].HappenTime = DateTime.Now;

                    if (HappenAlarms[id].State == EM_AL_STATE.Heavy)
                    {
                        _queShowAlarm.Enqueue(new Alarm()
                        {
                            ID = id,
                            Happen = happenAlarm,
                            State = EM_AL_STATE.Heavy,
                            HappenTime = DateTime.Now,
                            Desc = HappenAlarms[id].Desc
                        });
                        //equip.IsPause = true;
                        //equip.BuzzerK1.OnOff(equip, true);
                        //equip.BuzzerK2.OnOff(equip, false);
                        //equip.IsBuzzerStopSW = false;
                        equip.IsHeavyAlarm = true;


                        //equip.HsmsPc.CimAlarms.Enqueue(new CimAlarm() { Code = 1, ID = (short)id, Type = 1, Status = 1, Text = id.ToString() });

                        //if (equip.IsSetupMode == false)
                        //    equip.HsmsPc.StartCommand(equip, Detail.HSMS.EmHsmsPcCommand.ALARM_STATUS_CHANGE_REPORT, null);

                        Logger.Log.AppendLine(LogLevel.Error, "HEAVY ALARM OCCURRED ID = {0}, ALARM CODE = {1} ", id, HappenAlarms[id].Desc);
                        Logger.AlarmLog.AppendLine(LogLevel.Error, "HEAVY ALARM OCCURRED ID = {0}, ALARM CODE = {1} ", id, HappenAlarms[id].Desc);
                        if (equip.IsHeavyAlarm == false)
                            EquipStatusDump.LogEquipState(equip, EM_LOG_TP.Alarm);
                    }
                    else if (HappenAlarms[id].State == EM_AL_STATE.Warn)
                    {
                        _queShowAlarm.Enqueue(new Alarm()
                        {
                            ID = id,
                            Happen = happenAlarm,
                            State = EM_AL_STATE.Warn,
                            HappenTime = DateTime.Now,
                            Desc = HappenAlarms[id].Desc
                        });
                        if (equip.IsHeavyAlarm == false)
                        {
                            //equip.IsBuzzerStopSW = false;
                            //equip.BuzzerK1.OnOff(equip, false);
                            //equip.BuzzerK2.OnOff(equip, true);
                        }
                        equip.IsLightAlarm = true;

                        //equip.HsmsPc.CimAlarms.Enqueue(new CimAlarm() { Code = 1, ID = (short)id, Type = 0, Status = 1, Text = id.ToString() });

                        //if (equip.IsSetupMode == false)
                        //    equip.HsmsPc.StartCommand(equip, Detail.HSMS.EmHsmsPcCommand.ALARM_STATUS_CHANGE_REPORT, null);

                        Logger.Log.AppendLine(LogLevel.Warning, "LIGHT ALARM OCCURRED ID = {0}, ALARM CODE = {1} ", id, HappenAlarms[id].Desc);
                        Logger.AlarmLog.AppendLine(LogLevel.Warning, "LIGHT ALARM OCCURRED ID = {0}, ALARM CODE = {1} ", id, HappenAlarms[id].Desc);
                        if (equip.IsLightAlarm == false)
                            EquipStatusDump.LogEquipState(equip, EM_LOG_TP.Alarm);
                    }
                    else if (HappenAlarms[id].State == EM_AL_STATE.Unused)
                    {
                        _queShowAlarm.Enqueue(new Alarm()
                        {
                            ID = id,
                            Happen = happenAlarm,
                            State = EM_AL_STATE.Unused,
                            HappenTime = DateTime.Now,
                            Desc = HappenAlarms[id].Desc
                        });
                        Logger.Log.AppendLine(LogLevel.Unuse, "UNUSED ALARM OCCURED ID = {0}, ALARM CODE = {1} ", id, HappenAlarms[id].Desc);
                        Logger.AlarmLog.AppendLine(LogLevel.Unuse, "UNUSED ALARM OCCURED ID = {0}, ALARM CODE = {1} ", id, HappenAlarms[id].Desc);
                        if (equip.IsUnusedAlarm == false)
                            EquipStatusDump.LogEquipState(equip, EM_LOG_TP.Alarm);

                        equip.IsUnusedAlarm = true;
                    }
                    else
                    {
                        _queShowAlarm.Enqueue(new Alarm()
                        {
                            ID = EM_AL_LST.AL_NONE,
                            Happen = happenAlarm,
                            State = EM_AL_STATE.Heavy,
                            HappenTime = DateTime.Now,
                            Desc = "Alarm Code Error"
                        });
                        Logger.Log.AppendLine(LogLevel.Error, "Alarm Code Error");

                        Logger.Log.AppendLine(LogLevel.Error, "Alarm Code Error ID = {0}, ALARM CODE = {1} ", EM_AL_LST.AL_NONE, HappenAlarms[id].Desc);
                    }                     
                }
            }
            else
            {
                Logger.Log.AppendLine(LogLevel.Error, "Alarm Code Error");
                _queShowAlarm.Enqueue(new Alarm() { ID = EM_AL_LST.AL_NONE, Happen = true, State = EM_AL_STATE.Heavy, HappenTime = DateTime.Now });
                //equip.IsPause = true;
                //equip.IsHeavyAlarm = true;

                Logger.Log.AppendLine(LogLevel.Error, "Alarm Code Error ID = {0}, ALARM CODE = {1} ", EM_AL_LST.AL_NONE, "Alarm Code Error");
            }

            return true;
        }
        public bool Clear(Equipment equip)
        {
            foreach (EM_AL_LST id in HappenAlarms.Keys)
            {
                //if (HappenAlarms[id].Happen == true)
                //    if (HappenAlarms[id].State == EM_AL_STATE.Heavy)
                //        equip.HsmsPc.CimAlarms.Enqueue(new CimAlarm() { Code = 1, ID = (short)id, Type = 1, Status = 0, Text = id.ToString() });
                //    else if (HappenAlarms[id].State == EM_AL_STATE.Warn)
                //        equip.HsmsPc.CimAlarms.Enqueue(new CimAlarm() { Code = 1, ID = (short)id, Type = 0, Status = 0, Text = id.ToString() });
            }

            HappenAlarms.Values.ToList().ForEach(f => f.Happen = false);
            PastToHistoryListView(ref LstlogAlarms, ref LstlogAlarmHistory);
            //foreach (ListView lstv in LstlogAlarms)
            //{
            //    for (int iPos = 0; iPos < lstv.Items.Count; iPos++)
            //    {
            //        lstv.Items[iPos].ForeColor = Color.Black;
            //    }
            //}
            equip.IsHeavyAlarm = false;
            equip.IsLightAlarm = false;
            equip.IsUnusedAlarm = false;

            //equip.HsmsPc.StartCommand(equip, Detail.HSMS.EmHsmsPcCommand.ALARM_STATUS_CHANGE_REPORT, null);
            return false;
        }
        public void InitializeAlarmView(ListView logView)
        {
            _imgList.Images.Add(System.Drawing.SystemIcons.Warning);
            _imgList.Images.Add(System.Drawing.SystemIcons.Error);
            _imgList.Images.Add(System.Drawing.SystemIcons.Information);

            logView.View = View.Details;
            logView.FullRowSelect = true;
            logView.SmallImageList = _imgList;
            logView.LargeImageList = _imgList;

            int width = logView.Width - 210 > 0 ? logView.Width - 210 : 300;
            logView.Columns.Add(new ColumnHeader() { Width = 10, Name = "chNo", Text = "-" });
            logView.Columns.Add(new ColumnHeader() { Width = 120, Name = "chTime", Text = "TIME", TextAlign = HorizontalAlignment.Center });
            logView.Columns.Add(new ColumnHeader() { Width = 50, Name = "chID", Text = "ID", TextAlign = HorizontalAlignment.Center });
            logView.Columns.Add(new ColumnHeader() { Width = width, Name = "chLog", Text = "TEXT", TextAlign = HorizontalAlignment.Left });
            /*logView.DoubleClick += delegate(object sender, EventArgs e)
            {
                ListView l = (ListView)sender;
                if (l.SelectedItems.Count == 1)
                {
                    int alarmNum = 0;
                    int.TryParse(l.SelectedItems[0].SubItems[2].Text, out alarmNum);

                    foreach (Form openForm in Application.OpenForms)
                    {
                        if (openForm.Name == "SolutionDialog")
                        {
                            openForm.Close();
                            break;
                        }
                    }

                    SolutionDialog dlg = new SolutionDialog(AlarmMgr.Instance.GetSolution(alarmNum));
                    dlg.Show();
                }
            };*/
            //logView.Resize += delegate(object sender, EventArgs e)
            //{
            //    logView.Columns[2].Width = logView.Width - 210 > 0 ? logView.Width - 210 : 300; ;
            //};
        }
        private void InitializeAlarmList()
        {
            SortedList<EM_AL_LST, Alarm> lstAlarmInfo = GetAlarmInfo();

            foreach (EM_AL_LST alid in Enum.GetValues(typeof(EM_AL_LST)))
            {
                if (lstAlarmInfo.ContainsKey(alid) == false)
                    HappenAlarms.Add(alid, new Alarm() { ID = alid });
                else
                    HappenAlarms.Add(alid, lstAlarmInfo[alid]);
            }

            if (lstAlarmInfo.Count == 0) return;
        }
        private SortedList<EM_AL_LST, Alarm> GetAlarmInfo()
        {
            SortedList<EM_AL_LST, Alarm> lstAlarmInfo = new SortedList<EM_AL_LST, Alarm>();

            if (File.Exists(PATH_SETTING) == false) return lstAlarmInfo;
            foreach (string line in File.ReadAllLines(PATH_SETTING))
            {
                if (line == "") break;
                string[] items = line.Split(',');

                if (items[1].Length == 0)
                {
                    lstAlarmInfo.Add((EM_AL_LST)(int.Parse(items[0])), new Alarm());
                }
                else
                {
                    Alarm alarmItem = new Alarm();
                    alarmItem.ID = (EM_AL_LST)(int.Parse(items[0]));

                    if (alarmItem.ID.ToString() == items[1].ToString())
                        alarmItem.Desc = items[2];

                    alarmItem.SetState(items[3]);
                    alarmItem.SetLevel(items[4]);
                    lstAlarmInfo.Add(alarmItem.ID, alarmItem);
                }
            }
            return lstAlarmInfo;
        }
        public void AlarmStateChange(DataGridView dgvAlarmSetting)
        {
            foreach (DataGridViewRow dgvRow in dgvAlarmSetting.Rows)
            {
                if (dgvRow.Cells[3].Value != null)
                {
                    EM_AL_LST id = (EM_AL_LST)(int.Parse(dgvRow.Cells[0].Value.ToString()));
                    EM_AL_STATE state = GetState(dgvRow.Cells[3].Value.ToString(), dgvRow.Cells[4].Value.ToString(), dgvRow.Cells[5].Value.ToString());
                    try
                    {
                        HappenAlarms[id].State = state;
                    }
                    catch (System.Exception ex)
                    {
                        throw new Exception("Alarm code that does not exist ");
                    }
                }
            }
        }
        public bool SaveAlarmINIFile(DataGridView dgvAlarmSetting)
        {
            string[] lines = new string[dgvAlarmSetting.Rows.Count];
            foreach (DataGridViewRow dgvRow in dgvAlarmSetting.Rows)
            {
                string stateStr = string.Empty;
                string levelStr = string.Empty;
                if (dgvRow.Cells[3].Value != null)
                {
                    stateStr = GetStateStr(dgvRow.Cells[3].Value.ToString(), dgvRow.Cells[4].Value.ToString(), dgvRow.Cells[5].Value.ToString());
                    levelStr = GetLevelStr(dgvRow.Cells[4].Style, dgvRow.Cells[4].Style);
                    lines[dgvRow.Index] = dgvRow.Cells[0].Value.ToString() + ","
                                        + dgvRow.Cells[1].Value.ToString() + ","
                                        + dgvRow.Cells[2].Value.ToString() + ","
                                        + stateStr + ","
                                        + levelStr;
                }
                else
                {
                    if (dgvRow.Cells[0].Value == null) break;
                    lines[dgvRow.Index] = dgvRow.Cells[0].Value.ToString() + ",,,";
                }
            }

            File.WriteAllLines(PATH_SETTING, lines);
            return true;
        }
        private EM_AL_STATE GetState(string isHeavy, string isWarn, string isUnused)
        {
            EM_AL_STATE state = EM_AL_STATE.Heavy;

            if (isHeavy.Equals("True"))
                state = EM_AL_STATE.Heavy;
            else if (isWarn.Equals("True"))
                state = EM_AL_STATE.Warn;
            else if (isUnused.Equals("True"))
                state = EM_AL_STATE.Unused;

            return state;
        }
        private string GetStateStr(string isHeavy, string isWarn, string isUnused)
        {
            string stateStr = string.Empty;

            if (isHeavy.Equals("True"))
                stateStr = "HEAVY";
            else if (isWarn.Equals("True"))
                stateStr = "WARN";
            else if (isUnused.Equals("True"))
                stateStr = "UNUSED";

            return stateStr;
        }
        private string GetLevelStr(DataGridViewCellStyle isWarn, DataGridViewCellStyle isUnused)
        {
            string levelStr = string.Empty;

            if (isWarn.BackColor == Color.LightGray)
                return levelStr = "HEAVY";
            else if (isUnused.BackColor == Color.LightGray)
                return levelStr = "WARN";
            else
                return levelStr = "NORMAL";
        }
        public void LoadAlarmSettingList(DataGridView dgvAlarmSetting)
        {
            dgvAlarmSetting.Rows.Clear();

            for (int iter = 0; iter < HappenAlarms.Count; iter++)
            {
                EM_AL_LST id = (EM_AL_LST)iter;
                bool[] isChecked = new bool[]{  HappenAlarms[id].State == EM_AL_STATE.Heavy,  
                                                HappenAlarms[id].State == EM_AL_STATE.Warn, 
                                                HappenAlarms[id].State == EM_AL_STATE.Unused};

                string desc = (HappenAlarms[id].Desc == null) ? null : HappenAlarms[id].Desc.ToString();
                string name = (desc == null) ? null : HappenAlarms[id].ID.ToString();

                dgvAlarmSetting.Rows.Add(new string[] 
                    {
                        iter.ToString(),
                        name,
                        desc                        
                    });

                dgvAlarmSetting.Rows[iter].Cells[3].ReadOnly = true;
                dgvAlarmSetting.Rows[iter].Cells[4].ReadOnly = true;
                dgvAlarmSetting.Rows[iter].Cells[5].ReadOnly = true;

                if (name == null) continue;
                dgvAlarmSetting.Rows[iter].Cells[3].Value = isChecked[0];
                dgvAlarmSetting.Rows[iter].Cells[4].Value = isChecked[1];
                dgvAlarmSetting.Rows[iter].Cells[5].Value = isChecked[2];

                if (HappenAlarms[id].Level == EM_AL_LV.Heavy)
                {
                    dgvAlarmSetting.Rows[iter].Cells[4].Style.BackColor = Color.LightGray;
                    dgvAlarmSetting.Rows[iter].Cells[5].Style.BackColor = Color.LightGray;

                    if ((bool)dgvAlarmSetting.Rows[iter].Cells[3].Value == false)
                    {
                        dgvAlarmSetting.Rows[iter].Cells[3].Value = true;
                        dgvAlarmSetting.Rows[iter].Cells[4].Value = false;
                        dgvAlarmSetting.Rows[iter].Cells[5].Value = false;
                    }
                }
                else if (HappenAlarms[id].Level == EM_AL_LV.Warn)
                {
                    dgvAlarmSetting.Rows[iter].Cells[5].Style.BackColor = Color.LightGray;

                    if ((bool)dgvAlarmSetting.Rows[iter].Cells[5].Value == true)
                    {
                        dgvAlarmSetting.Rows[iter].Cells[3].Value = true;
                        dgvAlarmSetting.Rows[iter].Cells[5].Value = false;
                    }
                }
            }
        }
        public byte[] GetAlarmBytes()
        {
            //BitArray array = new BitArray(HappenAlarms.Values.Select(f => f.Happen).ToArray());            
            byte[] ret = new byte[(int)Math.Floor(HappenAlarms.Count / 8f) + 1];
            foreach (EM_AL_LST key in HappenAlarms.Keys)
            {
                int iKey = (int)key;
                int iByte = (int)Math.Truncate(iKey / 8f);
                int iBit = iKey % 8;
                if (HappenAlarms[key].Happen)
                    ret[iByte] |= (byte)(1 << iBit);
            }
            return ret;
        }
        // 조치사항 관련
        public Dictionary<int, AlarmSolution> Solutions = new Dictionary<int, AlarmSolution>();
        public void InitializeSolution()
        {
            string strAlarmNum;
            string alarmName;
            foreach (string alarmItem in Enum.GetNames(typeof(EM_AL_LST)))
            {
                //AL_0248_REVIEW_Y2_LOADING_POSITION_ERROR
                string[] temp = alarmItem.Split('_');
                if (temp.Length < 3 || alarmItem.Length < 8)
                    continue;

                strAlarmNum = temp[1];
                alarmName = alarmItem.Remove(0, 8);
                if (alarmName == string.Empty)
                    alarmName = "NONE";

                int alarmNum = 0;
                int.TryParse(strAlarmNum, out alarmNum);
                AlarmSolution solution = new AlarmSolution(alarmNum);
                solution.Load();
                if (solution.Name != alarmName)
                {
                    if (alarmName == "NONE") // 삭제된 것.
                    {
                        solution = new AlarmSolution(alarmNum);
                        solution.Save();
                    }
                    else // 추가/변경된 것.
                    {
                        solution.Name = alarmName;
                        solution.Save();
                        Solutions[alarmNum] = solution;
                    }
                }
            }
        }
        private void ReloadSolution(int alarmNum)
        {
            if (Solutions.ContainsKey(alarmNum))
                Solutions[alarmNum].Load();
        }
        public AlarmSolution GetSolution(int alarmNum)
        {
            if (Solutions.ContainsKey(alarmNum))
            {
                ReloadSolution(alarmNum);
                return Solutions[alarmNum];
            }
            else
                return new AlarmSolution(-1);
        }
    }
}