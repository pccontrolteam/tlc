﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DIT.TLC.CTRL
{

    public class BreakZigRecipe : ICloneable
    {
        public PointD BreakingZigXY { get; set; }
        public double BreakingZigThickness { get; set; }
        public double BreakingZigCamToPin { get; set; }

        public double BreakingOnTime { get; set; }
        public double BreakingCassettePitchNormal { get; set; }
        public double BreakingCassettePitchReverse { get; set; }

        public double BreakingJumpSpeed { get; set; }
        public double BreakingDefaultZ { get; set; }
        public double BreakinAlignMatch { get; set; }
        public double BreakinPreAlignErrorAng { get; set; }
        public double BreakinErrorY { get; set; }
        public double BreakinAlignDistance { get; set; }
        public double BreakinPreAlignErrorX { get; set; }
        public double BreakinPreAlignAngle { get; set; }
        public double BreakinPreAlignErrorY { get; set; }

        public object Clone()
        {
            BreakZigRecipe info = new BreakZigRecipe();
            info.BreakingZigXY.X                             /**/ = this.BreakingZigXY.X;
            info.BreakingZigXY.Y                             /**/ = this.BreakingZigXY.Y;
            info.BreakingZigThickness                        /**/ = this.BreakingZigThickness;
            info.BreakingZigCamToPin                         /**/ = this.BreakingZigCamToPin;

            info.BreakingOnTime                              /**/ = this.BreakingOnTime;
            info.BreakingCassettePitchNormal                 /**/ = this.BreakingCassettePitchNormal;
            info.BreakingCassettePitchReverse                /**/ = this.BreakingCassettePitchReverse;

            info.BreakingJumpSpeed                           /**/ = this.BreakingJumpSpeed;
            info.BreakingDefaultZ                            /**/ = this.BreakingDefaultZ;
            info.BreakinAlignMatch                           /**/ = this.BreakinAlignMatch;
            info.BreakinPreAlignErrorAng                     /**/ = this.BreakinPreAlignErrorAng;
            info.BreakinErrorY                               /**/ = this.BreakinErrorY;
            info.BreakinAlignDistance                        /**/ = this.BreakinAlignDistance;
            info.BreakinPreAlignErrorX                       /**/ = this.BreakinPreAlignErrorX;
            info.BreakinPreAlignAngle                        /**/ = this.BreakinPreAlignAngle;
            info.BreakinPreAlignErrorY                        /**/ = this.BreakinPreAlignErrorY;
            return info;
        }
    }

    public class BreakZigRecipeManager
    {
        public static string PathOfXml = Path.Combine(Application.StartupPath, "Setting", "BreakZigRecipeManager.Xml");
        private BreakZigRecipe BreakZigRcp;

        public BreakZigRecipeManager()
        {

        }

        public void Save()
        {
            XmlFileManager<BreakZigRecipe>.TrySaveXml(PathOfXml, BreakZigRcp);
        }
        public void Load()
        {
            XmlFileManager<BreakZigRecipe>.TryLoadData(PathOfXml, out BreakZigRcp);
        }
    }
}
