﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DIT.TLC.CTRL
{
    public class BreakRecipeManager
    {
        public static string PathOfXml = Path.Combine(Application.StartupPath, "Setting", "BreakRecipeManager.Xml");

        public List<BreakRecipe> LstBreakRecipe;

        public BreakRecipeManager()
        {
            LstBreakRecipe = new List<BreakRecipe>();
        }

        public void Save()
        {
            XmlFileManager<List<BreakRecipe>>.TrySaveXml(PathOfXml, LstBreakRecipe);
        }
        public void Load()
        {
            XmlFileManager<List<BreakRecipe>>.TryLoadData(PathOfXml, out LstBreakRecipe);
        }
    }
}
