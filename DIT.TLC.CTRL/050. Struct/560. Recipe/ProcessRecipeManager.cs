﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DIT.TLC.CTRL
{
    public class ProcessRecipeManager
    {
        public static string PathOfXml = Path.Combine(Application.StartupPath, "Setting", "EqpRecipeManager.Xml");
        public List<ProcessRecipe> LstProcessRecipe;

        public ProcessRecipeManager()
        {
            LstProcessRecipe = new List<ProcessRecipe>();
        }
        public void Save()
        {
            XmlFileManager<List<ProcessRecipe>>.TrySaveXml(PathOfXml, LstProcessRecipe);
        }
        public void Load()
        {
            XmlFileManager<List<ProcessRecipe>>.TryLoadData(PathOfXml, out LstProcessRecipe);
        }
    }
}
