﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class PointD 
    {
        public double X {get;set;}
        public double Y {get;set;}
    
        public PointD()
        {
            X = 0;
            Y = 0;
        }
    }

    public class PointBreaking
    {
        public double Left { get; set; }
        public double Right { get; set; }
        public double Half { get; set; }

        public PointBreaking()
        {
            Left = 0;
            Right = 0;
            Half = 0;
        }
    }

    public class EqpRecipe : ICloneable
    {
        public bool Used { get; set; }
        public bool Cim { get; set; }
        public string Name { get; set; }

        public double AlignImagePath { get; set; }

        public PointD Table1AlignMark1 { get; set; }        
        public PointD Table1AlignMark2 { get; set; }
        

        public PointD Table2AlignMark1 { get; set; }
        public PointD Table2AlignMark2 { get; set; }
        
        public PointD BreakLeft1 { get; set; }        
        public PointD BreakLeft2 { get; set; }        
        public double BreakLeftY { get; set; }
        
        public PointD BreakRight1 { get; set; }        
        public PointD BreakRight2 { get; set; }        
        public double BreakRightY { get; set; }

        public PointD AMatrixSize { get; set; }


        //LayerInfo
        public string DxfPath { get; set; }
        public string AlignMentLayer { get; set; }
        public string ProcessLayer { get; set; }
        public string GuideLayer { get; set; }
        public string ProcessParam { get; set; }
        public string CellType { get; set; }
        public string TaThickness { get; set; }


        //Cassette Setting
        public double CassetteWidth { get; set; }
        public double CassetteHeight { get; set; }
        public double CassetteMaxHeightCount { get; set; }
        public double CassetteCellCount { get; set; }
                
        public emPickerType PickerType1 { get; set; }
        public emPickerType PickerType2 { get; set; }

        //LDS Process Setting        
        public PointD LDSProcess01 { get; set; }
        public PointD LDSProcess02 { get; set; }
        public PointD LDSProcess03 { get; set; }
        public PointD LDSProcess04 { get; set; }

        //LDS Break Setting        
        public PointD LDSBreak01 { get; set; }
        public PointD LDSBreak02 { get; set; }
        public PointD LDSBreak03 { get; set; }
        public PointD LDSBreak04 { get; set; }

        //Y-Offset
        public double OffsetPre { get; set; }
        public double OffsetBreak { get; set; }

        public object Clone()
        {
            EqpRecipe info = new EqpRecipe();
            //info.Used                             /**/ = this.Used;
            //info.Cim                              /**/ = this.Cim;
            info.Name                               /**/ = this.Name;
            info.Table1AlignMark1.X                 /**/ = this.Table1AlignMark1.X;
            info.Table1AlignMark1.Y                 /**/ = this.Table1AlignMark1.Y;
            info.Table1AlignMark2.X                 /**/ = this.Table1AlignMark2.X;
            info.Table1AlignMark2.Y                 /**/ = this.Table1AlignMark2.Y;
            info.Table2AlignMark1.X                 /**/ = this.Table2AlignMark1.X;
            info.Table2AlignMark1.Y                 /**/ = this.Table2AlignMark1.Y;
            info.BreakLeft1.X                       /**/ = this.BreakLeft1.X;
            info.BreakLeft1.Y                       /**/ = this.BreakLeft1.Y;
            info.BreakLeft2.X                       /**/ = this.BreakLeft2.X;
            info.BreakLeft2.Y                       /**/ = this.BreakLeft2.Y;
            info.BreakLeftY                         /**/ = this.BreakLeftY;
            info.BreakRight1.X                      /**/ = this.BreakRight1.X;
            info.BreakRight1.Y                      /**/ = this.BreakRight1.Y;
            info.BreakRight2.X                      /**/ = this.BreakRight2.X;
            info.BreakRight2.Y                      /**/ = this.BreakRight2.Y;
            info.BreakRightY                        /**/ = this.BreakRightY;
            info.AMatrixSize.X                      /**/ = this.AMatrixSize.X;
            info.AMatrixSize.Y                      /**/ = this.AMatrixSize.Y;
            //info.DxfPath                          /**/ = this.DxfPath;
            info.AlignMentLayer                     /**/ = this.AlignMentLayer;
            info.ProcessLayer                       /**/ = this.ProcessLayer;
            info.GuideLayer                         /**/ = this.GuideLayer;
            info.ProcessParam                       /**/ = this.ProcessParam;
            info.CellType                           /**/ = this.CellType;
            info.TaThickness                        /**/ = this.TaThickness;

            //Cassette
            info.CassetteWidth                      /**/ = this.CassetteWidth;
            info.CassetteHeight                     /**/ = this.CassetteHeight;
            info.CassetteMaxHeightCount             /**/ = this.CassetteMaxHeightCount;
            info.CellType                           /**/ = this.CellType;

            //Picker

            //LDS Process
            info.LDSProcess01.X                     /**/ = this.LDSProcess01.X;
            info.LDSProcess01.Y                     /**/ = this.LDSProcess01.Y;
            info.LDSProcess02.X                     /**/ = this.LDSProcess02.X;
            info.LDSProcess02.Y                     /**/ = this.LDSProcess02.Y; 
            info.LDSProcess03.X                     /**/ = this.LDSProcess03.X;
            info.LDSProcess03.Y                     /**/ = this.LDSProcess03.Y;
            info.LDSProcess04.X                     /**/ = this.LDSProcess04.X;
            info.LDSProcess04.Y                     /**/ = this.LDSProcess04.Y;

            //LDS Break
            info.LDSBreak01.X                       /**/ = this.LDSBreak01.X;
            info.LDSBreak01.Y                       /**/ = this.LDSBreak01.Y;
            info.LDSBreak02.X                       /**/ = this.LDSBreak02.X;
            info.LDSBreak02.Y                       /**/ = this.LDSBreak02.Y;
            info.LDSBreak03.X                       /**/ = this.LDSBreak03.X;
            info.LDSBreak03.Y                       /**/ = this.LDSBreak03.Y;
            info.LDSBreak04.X                       /**/ = this.LDSBreak04.X;
            info.LDSBreak04.Y                       /**/ = this.LDSBreak04.Y;

            //Y-Offset
            info.OffsetPre                          /**/ = this.OffsetPre;
            info.OffsetBreak                        /**/ = this.OffsetBreak;
            return info;


        }
    }

    public enum emPickerType
    {
        Normal,
        CW90,
        CCW90,
        CW180,
    }
}


