﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.Ini;
using System.IO;
using System.Windows.Forms;
using Dit.Framework.PLC;


namespace DIT.TLC.CTRL
{
    public class EquipInitSetting : BaseSetting
    {
        public static string PATH_SETTING = Path.Combine(Application.StartupPath, "Setting", "_EquipInitSetting.ini");

        #region Info
        [IniAttribute("Info", "EquipNo", 59)]
        public int EquipNo { get; set; }
        [IniAttribute("Info", "EquipName", "IJP")]
        public string EquipName { get; set; }
        [IniAttribute("Info", "Version", "1701011430")]
        public string SwVersion { get; set; }
        #endregion

        #region Setting
        [IniAttribute("Setting", "ControlLogPath", "D:\\DitCtrl\\Exec\\Ctrl\\Log")]
        public string ControlLogBasePath { get; set; }
        [IniAttribute("Setting", "ErrorLogPath", "D:\\DitCtrl\\Exec\\Ctrl\\Log")]
        public string ErrorLogBasePath { get; set; }
        [IniAttribute("Setting", "UseFreeLoginAcces", true)]
        public bool UseFreeLoginAccess { get; set; }
        [IniAttribute("Setting", "IsLeftSideEquip", true)]
        public bool IsLeftSideEquip { get; set; }
        #endregion

        #region Communication
        [IniAttribute("Communication", "PmacIP", "192.168.0.200")]
        public string PmacIP { get; set; }
        [IniAttribute("Communication", "PmacPort", 1025)]
        public int PmacPort { get; set; }
        [IniAttribute("Communication", "NumFFU", 9)]
        public int NumFFU { get; set; }
        [IniAttribute("Communication", "EFUPort", "COM6")]
        public string EFUPort { get; set; }
        [IniAttribute("Communication", "DisplayPort", "COM5")]
        public string DisplayPort { get; set; }
        [IniAttribute("Communication", "KM50NPSPort", "COM4")]
        public string KM50NPSPort { get; set; }
        [IniAttribute("Communication", "KM50UPSPort", "COM3")]
        public string KM50UPSPort { get; set; }
        #endregion

        public EquipInitSetting()
        {
            EquipNo = 59;
            SwVersion = "1701011430";
            ControlLogBasePath = "D:\\DitCtrl\\Exec\\Ctrl\\Log";
            ErrorLogBasePath = "D:\\DitCtrl\\Exec\\Ctrl\\Log";
            UseFreeLoginAccess = false;
            IsLeftSideEquip = true;
            PmacIP = "192.168.0.200";
            PmacPort = 1025;
            NumFFU = 9; //초기값 NumFFU = 12;
            EFUPort = "COM6";
            DisplayPort = "COM5";
            KM50NPSPort = "COM4";
            KM50UPSPort = "COM3";
        }
        /**
        *@param : path - null : 기본 경로s
        *            not null : 경로 변경
        *               */
        public override bool Save(string path = null)
        {
            if (null != path)
                PATH_SETTING = path;
            if (!Directory.Exists(PATH_SETTING))
                Directory.CreateDirectory(PATH_SETTING.Remove(PATH_SETTING.LastIndexOf('\\')));

            return base.Save(PATH_SETTING);
        }
        /**
         *@param : path - null : 기본 경로
         *            not null : 경로 변경
         *               */
        public override bool Load(string path = null)
        {
            if (null != path)
                PATH_SETTING = path;
            return base.Load(PATH_SETTING);
        }

        public bool CheckValueConsistency()
        {
            string errMsg = string.Empty;

            try
            {
                uint ipaddress = PowerPmac.ToInt(PmacIP);
            }
            catch (Exception ex)
            {
                errMsg = "Pmac IP has wrong type.";
            }
            if (NumFFU == 0)
                errMsg = "NumFFU must be over 0(zero)";

            if (string.IsNullOrEmpty(EFUPort) == true)
                errMsg = "EFU PORT Is Null";

            if (string.IsNullOrEmpty(DisplayPort) == true)
                errMsg = "Display PORT Is Null";

            if (string.IsNullOrEmpty(KM50NPSPort) == true)
                errMsg = "KM50NPS PORT Is Null";

            if (string.IsNullOrEmpty(KM50UPSPort) == true)
                errMsg = "KM50UPS PORT Is Null";
            
            if (errMsg != string.Empty)
            {
                MessageBox.Show(string.Format("{0} {1} check please", errMsg, PATH_SETTING));
                return false;
            }

            return true;
        }
    }
}