﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIT.TLC.CTRL
{
    public class UserInfo
    {
        public string UserID { get; set; }
        public string Password { get; set; }
        public int Level { get; set; }
    }
}
