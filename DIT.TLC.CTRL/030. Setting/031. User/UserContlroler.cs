﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace DIT.TLC.CTRL
{
    public class UserInfoContlroler
    {
        public static string PathOfXml = Path.Combine(Application.StartupPath, "Setting", "UserInfo.Xml");

        public List<UserInfo> LstUserInfos = new List<UserInfo>();
        public UserInfo GetUserInfo(string userID, string password)
        {
            return LstUserInfos.FirstOrDefault(f => f.UserID == userID && f.Password == password);
        }

        public void Save()
        {
            XmlFileManager<List<UserInfo>>.TrySaveXml(PathOfXml, LstUserInfos);
        }
        public void Load()
        {
            XmlFileManager<List<UserInfo>>.TryLoadData(PathOfXml, out LstUserInfos);
        }
    }
}
