﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dit.Framework.Ini;
using System.IO;
using Dit.Framework.Xml;

namespace DIT.TLC.CTRL
{
    [Serializable]
    public class ServoPosiInfo
    {
        public double Position1st { get; set; }
        public double Speed1st { get; set; }
        public double Accel1st { get; set; }

        public ServoPosiInfo()
        {
            Position1st = 0;
            Speed1st = 0;
            Accel1st = 0;
        }
    }

    [Serializable]
    public class ServoSetting
    {
        public string PATH_SETTING { get; set; }
        /**
         * [관리항목]
         * 원점 이동속도
         * 조그 속도
         * 정의된 위치/속도 * 7개
        */

        public double HOME_SPEED { get; set; }
        public double JOG_SPEED { get; set; }

        public ServoPosiInfo[] LstServoPosiInfo = new ServoPosiInfo[32];

        public ServoSetting()
        {
            for (int iPos = 0; iPos < LstServoPosiInfo.Length; iPos++)
                LstServoPosiInfo[iPos] = new ServoPosiInfo();
        }
        public static ServoSetting Load(string path)
        {
            ServoSetting setting = new ServoSetting();
            if (XmlFileManager<ServoSetting>.TryLoadData(path, out setting))
            {
                return setting;
            }
            else
            {
                return new ServoSetting();
            }
        }
        public void Load()
        {
            ServoSetting setting = new ServoSetting();
            if (XmlFileManager<ServoSetting>.TryLoadData(PATH_SETTING, out setting))
            {
                string temp = this.PATH_SETTING;
                this.CopyFrom(setting);
                this.PATH_SETTING = temp;
            }
            else
            {
                this.CopyFrom(new ServoSetting() { PATH_SETTING = this.PATH_SETTING });
            }
        }
        public void Save()
        {
            ServoSetting setting = new ServoSetting();
            this.CopyTo(ref setting);
            XmlFileManager<ServoSetting>.TrySaveXml(PATH_SETTING, setting);
        }
        public static void Save(string path, ServoSetting setting)
        {
            XmlFileManager<ServoSetting>.TrySaveXml(path, setting);
        }
        public void CopyFrom(ServoSetting source)
        {
            this.PATH_SETTING = source.PATH_SETTING;

            this.HOME_SPEED = source.HOME_SPEED;
            this.JOG_SPEED = source.JOG_SPEED;

            for (int iter = 0; iter < LstServoPosiInfo.Length; ++iter)
            {
                this.LstServoPosiInfo[iter].Position1st = source.LstServoPosiInfo[iter].Position1st;
                this.LstServoPosiInfo[iter].Speed1st = source.LstServoPosiInfo[iter].Speed1st;
                this.LstServoPosiInfo[iter].Accel1st = source.LstServoPosiInfo[iter].Accel1st;
            }
        }
        public void CopyTo(ref ServoSetting dest)
        {
            dest.PATH_SETTING = this.PATH_SETTING;
            dest.HOME_SPEED = this.HOME_SPEED;
            dest.JOG_SPEED = this.JOG_SPEED;

            for (int iter = 0; iter < LstServoPosiInfo.Length; ++iter)
            {
                dest.LstServoPosiInfo[iter].Position1st = this.LstServoPosiInfo[iter].Position1st;
                dest.LstServoPosiInfo[iter].Speed1st = this.LstServoPosiInfo[iter].Speed1st;
                dest.LstServoPosiInfo[iter].Accel1st = this.LstServoPosiInfo[iter].Accel1st;
            }
        }
        public object Clone()
        {
            ServoSetting clone = new ServoSetting();
            this.CopyTo(ref clone);
            return clone;
        }
        public double GetPosition(int posiNo)
        {
            return LstServoPosiInfo[posiNo].Position1st;
        }
        public double GetSpeed(int posiNo)
        {
            return LstServoPosiInfo[posiNo].Speed1st;
        }
        public double GetAccel(int posiNo)
        {
            return LstServoPosiInfo[posiNo].Accel1st;
        }
        public bool SetPosition(int posiNo, double value)
        {
            LstServoPosiInfo[posiNo].Position1st = value;
            return true;
        }
        public bool SetSpeed(int posiNo, double value)
        {
            LstServoPosiInfo[posiNo].Speed1st = value;
            return true;
        }
        public bool SetAccel(int posiNo, double value)
        {
            LstServoPosiInfo[posiNo].Accel1st = value;
            return true;
        }

        public void CreateSampleData()
        {
            this.HOME_SPEED = 100;
            this.JOG_SPEED = 50;

            for (int iter = 0; iter < LstServoPosiInfo.Length; ++iter)
            {
                this.LstServoPosiInfo[iter].Position1st = 10 * iter;
                this.LstServoPosiInfo[iter].Speed1st = 10;
                this.LstServoPosiInfo[iter].Accel1st = 10;
            }
        }

    }
}
