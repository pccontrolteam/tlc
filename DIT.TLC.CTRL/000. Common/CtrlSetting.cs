﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using DitCim.Lib;

namespace DIT.TLC.CTRL
{
    public class EquipSetting
    {
        private static EquipSetting instance = null;
        public static EquipSetting Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new EquipSetting();
                }

                return instance;
            }
        }
        public const string FILE_NAME = "DitCim.Setting.ini";

 

        public EquipSetting()
        {      

            
            InspXYJogSpeedLow = 3;
            InspXYJogSpeedMiddle = 7;
            InspXYJogSpeedHight = 15;

            RevYJogSpeedLow = 3;
            RevXYJogSpeedMiddle = 7;
            RevXYJogSpeedHight = 15;

                                  
        }
                
        


        public int InspXYJogSpeedLow { get; set; }
        public int InspXYJogSpeedMiddle { get; set; }
        public int InspXYJogSpeedHight { get; set; }


         
        public int RevYJogSpeedLow { get; set; }
        public int RevXYJogSpeedMiddle { get; set; }
        public int RevXYJogSpeedHight { get; set; }

        public int LiftPinUp1Speed { get; set; }
        public int LiftPinUp2Speed { get; set; }
        public int LiftPinDown2Speed { get; set; }
        public int LiftPinDown1Speed { get; set; }


        
        

    }
}


