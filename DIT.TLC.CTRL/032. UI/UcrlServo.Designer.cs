﻿namespace DIT.TLC.CTRL
{
    partial class UcrlServo
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlServo = new System.Windows.Forms.Panel();
            this.nuJobSpeed = new System.Windows.Forms.NumericUpDown();
            this.btnPos31 = new Dit.Framework.UI.ButtonDelay();
            this.btnPos25 = new Dit.Framework.UI.ButtonDelay();
            this.btnPos30 = new Dit.Framework.UI.ButtonDelay();
            this.btnPos24 = new Dit.Framework.UI.ButtonDelay();
            this.btnPos19 = new Dit.Framework.UI.ButtonDelay();
            this.btnPos18 = new Dit.Framework.UI.ButtonDelay();
            this.btnPos29 = new Dit.Framework.UI.ButtonDelay();
            this.btnPos23 = new Dit.Framework.UI.ButtonDelay();
            this.btnPos17 = new Dit.Framework.UI.ButtonDelay();
            this.btnPos28 = new Dit.Framework.UI.ButtonDelay();
            this.btnPos22 = new Dit.Framework.UI.ButtonDelay();
            this.btnPos16 = new Dit.Framework.UI.ButtonDelay();
            this.btnPos27 = new Dit.Framework.UI.ButtonDelay();
            this.btnPos21 = new Dit.Framework.UI.ButtonDelay();
            this.btnPos15 = new Dit.Framework.UI.ButtonDelay();
            this.btnPos26 = new Dit.Framework.UI.ButtonDelay();
            this.btnPos20 = new Dit.Framework.UI.ButtonDelay();
            this.btnPos14 = new Dit.Framework.UI.ButtonDelay();
            this.btnPos13 = new Dit.Framework.UI.ButtonDelay();
            this.btnPos12 = new Dit.Framework.UI.ButtonDelay();
            this.btnPos11 = new Dit.Framework.UI.ButtonDelay();
            this.btnPos10 = new Dit.Framework.UI.ButtonDelay();
            this.btnPos9 = new Dit.Framework.UI.ButtonDelay();
            this.btnPos8 = new Dit.Framework.UI.ButtonDelay();
            this.btnPos7 = new Dit.Framework.UI.ButtonDelay();
            this.btnPos6 = new Dit.Framework.UI.ButtonDelay();
            this.btnPos5 = new Dit.Framework.UI.ButtonDelay();
            this.btnPos4 = new Dit.Framework.UI.ButtonDelay();
            this.btnMoveLoading = new Dit.Framework.UI.ButtonDelay();
            this.btnPos2 = new Dit.Framework.UI.ButtonDelay();
            this.btnPos3 = new Dit.Framework.UI.ButtonDelay();
            this.btnMoveHome = new Dit.Framework.UI.ButtonDelay();
            this.btnMoveJogMinus = new Dit.Framework.UI.ButtonDelay();
            this.btnMoveJogPlus = new Dit.Framework.UI.ButtonDelay();
            this.lblErr = new Dit.Framework.UI.LabelDelay();
            this.lblServoOn = new Dit.Framework.UI.LabelDelay();
            this.lblPositiveLimit = new Dit.Framework.UI.LabelDelay();
            this.lblNegativeLimit = new Dit.Framework.UI.LabelDelay();
            this.lblMoving = new Dit.Framework.UI.LabelDelay();
            this.lblHomeCompleteBit = new Dit.Framework.UI.LabelDelay();
            this.lblJogSpeed = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblPosition = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tmrUiUpdate = new System.Windows.Forms.Timer(this.components);
            this.pnlServo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nuJobSpeed)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlServo
            // 
            this.pnlServo.BackColor = System.Drawing.SystemColors.Control;
            this.pnlServo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlServo.Controls.Add(this.lblPosition);
            this.pnlServo.Controls.Add(this.label2);
            this.pnlServo.Controls.Add(this.nuJobSpeed);
            this.pnlServo.Controls.Add(this.btnPos31);
            this.pnlServo.Controls.Add(this.btnPos25);
            this.pnlServo.Controls.Add(this.btnPos30);
            this.pnlServo.Controls.Add(this.btnPos24);
            this.pnlServo.Controls.Add(this.btnPos19);
            this.pnlServo.Controls.Add(this.btnPos18);
            this.pnlServo.Controls.Add(this.btnPos29);
            this.pnlServo.Controls.Add(this.btnPos23);
            this.pnlServo.Controls.Add(this.btnPos17);
            this.pnlServo.Controls.Add(this.btnPos28);
            this.pnlServo.Controls.Add(this.btnPos22);
            this.pnlServo.Controls.Add(this.btnPos16);
            this.pnlServo.Controls.Add(this.btnPos27);
            this.pnlServo.Controls.Add(this.btnPos21);
            this.pnlServo.Controls.Add(this.btnPos15);
            this.pnlServo.Controls.Add(this.btnPos26);
            this.pnlServo.Controls.Add(this.btnPos20);
            this.pnlServo.Controls.Add(this.btnPos14);
            this.pnlServo.Controls.Add(this.btnPos13);
            this.pnlServo.Controls.Add(this.btnPos12);
            this.pnlServo.Controls.Add(this.btnPos11);
            this.pnlServo.Controls.Add(this.btnPos10);
            this.pnlServo.Controls.Add(this.btnPos9);
            this.pnlServo.Controls.Add(this.btnPos8);
            this.pnlServo.Controls.Add(this.btnPos7);
            this.pnlServo.Controls.Add(this.btnPos6);
            this.pnlServo.Controls.Add(this.btnPos5);
            this.pnlServo.Controls.Add(this.btnPos4);
            this.pnlServo.Controls.Add(this.btnMoveLoading);
            this.pnlServo.Controls.Add(this.btnPos2);
            this.pnlServo.Controls.Add(this.btnPos3);
            this.pnlServo.Controls.Add(this.btnMoveHome);
            this.pnlServo.Controls.Add(this.btnMoveJogMinus);
            this.pnlServo.Controls.Add(this.btnMoveJogPlus);
            this.pnlServo.Controls.Add(this.lblErr);
            this.pnlServo.Controls.Add(this.lblServoOn);
            this.pnlServo.Controls.Add(this.lblPositiveLimit);
            this.pnlServo.Controls.Add(this.lblNegativeLimit);
            this.pnlServo.Controls.Add(this.lblMoving);
            this.pnlServo.Controls.Add(this.lblHomeCompleteBit);
            this.pnlServo.Controls.Add(this.lblJogSpeed);
            this.pnlServo.Controls.Add(this.label3);
            this.pnlServo.Location = new System.Drawing.Point(0, 15);
            this.pnlServo.Name = "pnlServo";
            this.pnlServo.Size = new System.Drawing.Size(470, 248);
            this.pnlServo.TabIndex = 95;
            // 
            // nuJobSpeed
            // 
            this.nuJobSpeed.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nuJobSpeed.Location = new System.Drawing.Point(342, 6);
            this.nuJobSpeed.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nuJobSpeed.Name = "nuJobSpeed";
            this.nuJobSpeed.Size = new System.Drawing.Size(82, 21);
            this.nuJobSpeed.TabIndex = 94;
            this.nuJobSpeed.ValueChanged += new System.EventHandler(this.nuJogSpeed_ValueChanged);
            this.nuJobSpeed.KeyDown += new System.Windows.Forms.KeyEventHandler(this.nuJobSpeed_KeyDown);
            // 
            // btnPos31
            // 
            this.btnPos31.BackColor = System.Drawing.Color.Transparent;
            this.btnPos31.Delay = 3000;
            this.btnPos31.Flicker = false;
            this.btnPos31.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnPos31.ForeColor = System.Drawing.Color.Black;
            this.btnPos31.IsLeftLampOn = true;
            this.btnPos31.IsRightLampOn = true;
            this.btnPos31.LampOnWiatTime = 1000;
            this.btnPos31.LampSize = 5;
            this.btnPos31.LeftDelayOff = true;
            this.btnPos31.LeftLampColor = System.Drawing.Color.Red;
            this.btnPos31.Location = new System.Drawing.Point(377, 211);
            this.btnPos31.Name = "btnPos31";
            this.btnPos31.OffColor = System.Drawing.Color.Transparent;
            this.btnPos31.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btnPos31.OnOff = false;
            this.btnPos31.RightDelayOff = true;
            this.btnPos31.RightLampColor = System.Drawing.Color.Green;
            this.btnPos31.Size = new System.Drawing.Size(60, 33);
            this.btnPos31.TabIndex = 93;
            this.btnPos31.TabStop = false;
            this.btnPos31.Text = "2 Scan";
            this.btnPos31.Text2 = "";
            this.btnPos31.UseVisualStyleBackColor = false;
            this.btnPos31.VisibleLeftLamp = true;
            this.btnPos31.VisibleRightLamp = true;
            this.btnPos31.DelayClick += new System.EventHandler(this.btnStepReset_DelayClick);
            this.btnPos31.Click += new System.EventHandler(this.btnMovePosition_Click);
            // 
            // btnPos25
            // 
            this.btnPos25.BackColor = System.Drawing.Color.Transparent;
            this.btnPos25.Delay = 3000;
            this.btnPos25.Flicker = false;
            this.btnPos25.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnPos25.ForeColor = System.Drawing.Color.Black;
            this.btnPos25.IsLeftLampOn = true;
            this.btnPos25.IsRightLampOn = true;
            this.btnPos25.LampOnWiatTime = 1000;
            this.btnPos25.LampSize = 5;
            this.btnPos25.LeftDelayOff = true;
            this.btnPos25.LeftLampColor = System.Drawing.Color.Red;
            this.btnPos25.Location = new System.Drawing.Point(377, 176);
            this.btnPos25.Name = "btnPos25";
            this.btnPos25.OffColor = System.Drawing.Color.Transparent;
            this.btnPos25.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btnPos25.OnOff = false;
            this.btnPos25.RightDelayOff = true;
            this.btnPos25.RightLampColor = System.Drawing.Color.Green;
            this.btnPos25.Size = new System.Drawing.Size(60, 33);
            this.btnPos25.TabIndex = 93;
            this.btnPos25.TabStop = false;
            this.btnPos25.Text = "2 Scan";
            this.btnPos25.Text2 = "";
            this.btnPos25.UseVisualStyleBackColor = false;
            this.btnPos25.VisibleLeftLamp = true;
            this.btnPos25.VisibleRightLamp = true;
            this.btnPos25.DelayClick += new System.EventHandler(this.btnStepReset_DelayClick);
            this.btnPos25.Click += new System.EventHandler(this.btnMovePosition_Click);
            // 
            // btnPos30
            // 
            this.btnPos30.BackColor = System.Drawing.Color.Transparent;
            this.btnPos30.Delay = 3000;
            this.btnPos30.Flicker = false;
            this.btnPos30.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnPos30.ForeColor = System.Drawing.Color.Black;
            this.btnPos30.IsLeftLampOn = true;
            this.btnPos30.IsRightLampOn = true;
            this.btnPos30.LampOnWiatTime = 1000;
            this.btnPos30.LampSize = 5;
            this.btnPos30.LeftDelayOff = true;
            this.btnPos30.LeftLampColor = System.Drawing.Color.Red;
            this.btnPos30.Location = new System.Drawing.Point(316, 211);
            this.btnPos30.Name = "btnPos30";
            this.btnPos30.OffColor = System.Drawing.Color.Transparent;
            this.btnPos30.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btnPos30.OnOff = false;
            this.btnPos30.RightDelayOff = true;
            this.btnPos30.RightLampColor = System.Drawing.Color.Green;
            this.btnPos30.Size = new System.Drawing.Size(60, 33);
            this.btnPos30.TabIndex = 93;
            this.btnPos30.TabStop = false;
            this.btnPos30.Text = "2 Scan";
            this.btnPos30.Text2 = "";
            this.btnPos30.UseVisualStyleBackColor = false;
            this.btnPos30.VisibleLeftLamp = true;
            this.btnPos30.VisibleRightLamp = true;
            this.btnPos30.DelayClick += new System.EventHandler(this.btnStepReset_DelayClick);
            this.btnPos30.Click += new System.EventHandler(this.btnMovePosition_Click);
            // 
            // btnPos24
            // 
            this.btnPos24.BackColor = System.Drawing.Color.Transparent;
            this.btnPos24.Delay = 3000;
            this.btnPos24.Flicker = false;
            this.btnPos24.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnPos24.ForeColor = System.Drawing.Color.Black;
            this.btnPos24.IsLeftLampOn = true;
            this.btnPos24.IsRightLampOn = true;
            this.btnPos24.LampOnWiatTime = 1000;
            this.btnPos24.LampSize = 5;
            this.btnPos24.LeftDelayOff = true;
            this.btnPos24.LeftLampColor = System.Drawing.Color.Red;
            this.btnPos24.Location = new System.Drawing.Point(316, 176);
            this.btnPos24.Name = "btnPos24";
            this.btnPos24.OffColor = System.Drawing.Color.Transparent;
            this.btnPos24.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btnPos24.OnOff = false;
            this.btnPos24.RightDelayOff = true;
            this.btnPos24.RightLampColor = System.Drawing.Color.Green;
            this.btnPos24.Size = new System.Drawing.Size(60, 33);
            this.btnPos24.TabIndex = 93;
            this.btnPos24.TabStop = false;
            this.btnPos24.Text = "2 Scan";
            this.btnPos24.Text2 = "";
            this.btnPos24.UseVisualStyleBackColor = false;
            this.btnPos24.VisibleLeftLamp = true;
            this.btnPos24.VisibleRightLamp = true;
            this.btnPos24.DelayClick += new System.EventHandler(this.btnStepReset_DelayClick);
            this.btnPos24.Click += new System.EventHandler(this.btnMovePosition_Click);
            // 
            // btnPos19
            // 
            this.btnPos19.BackColor = System.Drawing.Color.Transparent;
            this.btnPos19.Delay = 3000;
            this.btnPos19.Flicker = false;
            this.btnPos19.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnPos19.ForeColor = System.Drawing.Color.Black;
            this.btnPos19.IsLeftLampOn = true;
            this.btnPos19.IsRightLampOn = true;
            this.btnPos19.LampOnWiatTime = 1000;
            this.btnPos19.LampSize = 5;
            this.btnPos19.LeftDelayOff = true;
            this.btnPos19.LeftLampColor = System.Drawing.Color.Red;
            this.btnPos19.Location = new System.Drawing.Point(377, 141);
            this.btnPos19.Name = "btnPos19";
            this.btnPos19.OffColor = System.Drawing.Color.Transparent;
            this.btnPos19.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btnPos19.OnOff = false;
            this.btnPos19.RightDelayOff = true;
            this.btnPos19.RightLampColor = System.Drawing.Color.Green;
            this.btnPos19.Size = new System.Drawing.Size(60, 33);
            this.btnPos19.TabIndex = 93;
            this.btnPos19.TabStop = false;
            this.btnPos19.Text = "2 Scan";
            this.btnPos19.Text2 = "";
            this.btnPos19.UseVisualStyleBackColor = false;
            this.btnPos19.VisibleLeftLamp = true;
            this.btnPos19.VisibleRightLamp = true;
            this.btnPos19.DelayClick += new System.EventHandler(this.btnStepReset_DelayClick);
            this.btnPos19.Click += new System.EventHandler(this.btnMovePosition_Click);
            // 
            // btnPos18
            // 
            this.btnPos18.BackColor = System.Drawing.Color.Transparent;
            this.btnPos18.Delay = 3000;
            this.btnPos18.Flicker = false;
            this.btnPos18.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnPos18.ForeColor = System.Drawing.Color.Black;
            this.btnPos18.IsLeftLampOn = true;
            this.btnPos18.IsRightLampOn = true;
            this.btnPos18.LampOnWiatTime = 1000;
            this.btnPos18.LampSize = 5;
            this.btnPos18.LeftDelayOff = true;
            this.btnPos18.LeftLampColor = System.Drawing.Color.Red;
            this.btnPos18.Location = new System.Drawing.Point(316, 141);
            this.btnPos18.Name = "btnPos18";
            this.btnPos18.OffColor = System.Drawing.Color.Transparent;
            this.btnPos18.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btnPos18.OnOff = false;
            this.btnPos18.RightDelayOff = true;
            this.btnPos18.RightLampColor = System.Drawing.Color.Green;
            this.btnPos18.Size = new System.Drawing.Size(60, 33);
            this.btnPos18.TabIndex = 93;
            this.btnPos18.TabStop = false;
            this.btnPos18.Text = "2 Scan";
            this.btnPos18.Text2 = "";
            this.btnPos18.UseVisualStyleBackColor = false;
            this.btnPos18.VisibleLeftLamp = true;
            this.btnPos18.VisibleRightLamp = true;
            this.btnPos18.DelayClick += new System.EventHandler(this.btnStepReset_DelayClick);
            this.btnPos18.Click += new System.EventHandler(this.btnMovePosition_Click);
            // 
            // btnPos29
            // 
            this.btnPos29.BackColor = System.Drawing.Color.Transparent;
            this.btnPos29.Delay = 3000;
            this.btnPos29.Flicker = false;
            this.btnPos29.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnPos29.ForeColor = System.Drawing.Color.Black;
            this.btnPos29.IsLeftLampOn = true;
            this.btnPos29.IsRightLampOn = true;
            this.btnPos29.LampOnWiatTime = 1000;
            this.btnPos29.LampSize = 5;
            this.btnPos29.LeftDelayOff = true;
            this.btnPos29.LeftLampColor = System.Drawing.Color.Red;
            this.btnPos29.Location = new System.Drawing.Point(254, 211);
            this.btnPos29.Name = "btnPos29";
            this.btnPos29.OffColor = System.Drawing.Color.Transparent;
            this.btnPos29.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btnPos29.OnOff = false;
            this.btnPos29.RightDelayOff = true;
            this.btnPos29.RightLampColor = System.Drawing.Color.Green;
            this.btnPos29.Size = new System.Drawing.Size(60, 33);
            this.btnPos29.TabIndex = 93;
            this.btnPos29.TabStop = false;
            this.btnPos29.Text = "2 Scan";
            this.btnPos29.Text2 = "";
            this.btnPos29.UseVisualStyleBackColor = false;
            this.btnPos29.VisibleLeftLamp = true;
            this.btnPos29.VisibleRightLamp = true;
            this.btnPos29.DelayClick += new System.EventHandler(this.btnStepReset_DelayClick);
            this.btnPos29.Click += new System.EventHandler(this.btnMovePosition_Click);
            // 
            // btnPos23
            // 
            this.btnPos23.BackColor = System.Drawing.Color.Transparent;
            this.btnPos23.Delay = 3000;
            this.btnPos23.Flicker = false;
            this.btnPos23.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnPos23.ForeColor = System.Drawing.Color.Black;
            this.btnPos23.IsLeftLampOn = true;
            this.btnPos23.IsRightLampOn = true;
            this.btnPos23.LampOnWiatTime = 1000;
            this.btnPos23.LampSize = 5;
            this.btnPos23.LeftDelayOff = true;
            this.btnPos23.LeftLampColor = System.Drawing.Color.Red;
            this.btnPos23.Location = new System.Drawing.Point(254, 176);
            this.btnPos23.Name = "btnPos23";
            this.btnPos23.OffColor = System.Drawing.Color.Transparent;
            this.btnPos23.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btnPos23.OnOff = false;
            this.btnPos23.RightDelayOff = true;
            this.btnPos23.RightLampColor = System.Drawing.Color.Green;
            this.btnPos23.Size = new System.Drawing.Size(60, 33);
            this.btnPos23.TabIndex = 93;
            this.btnPos23.TabStop = false;
            this.btnPos23.Text = "2 Scan";
            this.btnPos23.Text2 = "";
            this.btnPos23.UseVisualStyleBackColor = false;
            this.btnPos23.VisibleLeftLamp = true;
            this.btnPos23.VisibleRightLamp = true;
            this.btnPos23.DelayClick += new System.EventHandler(this.btnStepReset_DelayClick);
            this.btnPos23.Click += new System.EventHandler(this.btnMovePosition_Click);
            // 
            // btnPos17
            // 
            this.btnPos17.BackColor = System.Drawing.Color.Transparent;
            this.btnPos17.Delay = 3000;
            this.btnPos17.Flicker = false;
            this.btnPos17.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnPos17.ForeColor = System.Drawing.Color.Black;
            this.btnPos17.IsLeftLampOn = true;
            this.btnPos17.IsRightLampOn = true;
            this.btnPos17.LampOnWiatTime = 1000;
            this.btnPos17.LampSize = 5;
            this.btnPos17.LeftDelayOff = true;
            this.btnPos17.LeftLampColor = System.Drawing.Color.Red;
            this.btnPos17.Location = new System.Drawing.Point(254, 141);
            this.btnPos17.Name = "btnPos17";
            this.btnPos17.OffColor = System.Drawing.Color.Transparent;
            this.btnPos17.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btnPos17.OnOff = false;
            this.btnPos17.RightDelayOff = true;
            this.btnPos17.RightLampColor = System.Drawing.Color.Green;
            this.btnPos17.Size = new System.Drawing.Size(60, 33);
            this.btnPos17.TabIndex = 93;
            this.btnPos17.TabStop = false;
            this.btnPos17.Text = "2 Scan";
            this.btnPos17.Text2 = "";
            this.btnPos17.UseVisualStyleBackColor = false;
            this.btnPos17.VisibleLeftLamp = true;
            this.btnPos17.VisibleRightLamp = true;
            this.btnPos17.DelayClick += new System.EventHandler(this.btnStepReset_DelayClick);
            this.btnPos17.Click += new System.EventHandler(this.btnMovePosition_Click);
            // 
            // btnPos28
            // 
            this.btnPos28.BackColor = System.Drawing.Color.Transparent;
            this.btnPos28.Delay = 3000;
            this.btnPos28.Flicker = false;
            this.btnPos28.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnPos28.ForeColor = System.Drawing.Color.Black;
            this.btnPos28.IsLeftLampOn = true;
            this.btnPos28.IsRightLampOn = true;
            this.btnPos28.LampOnWiatTime = 1000;
            this.btnPos28.LampSize = 5;
            this.btnPos28.LeftDelayOff = true;
            this.btnPos28.LeftLampColor = System.Drawing.Color.Red;
            this.btnPos28.Location = new System.Drawing.Point(192, 211);
            this.btnPos28.Name = "btnPos28";
            this.btnPos28.OffColor = System.Drawing.Color.Transparent;
            this.btnPos28.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btnPos28.OnOff = false;
            this.btnPos28.RightDelayOff = true;
            this.btnPos28.RightLampColor = System.Drawing.Color.Green;
            this.btnPos28.Size = new System.Drawing.Size(60, 33);
            this.btnPos28.TabIndex = 93;
            this.btnPos28.TabStop = false;
            this.btnPos28.Text = "2 Scan";
            this.btnPos28.Text2 = "";
            this.btnPos28.UseVisualStyleBackColor = false;
            this.btnPos28.VisibleLeftLamp = true;
            this.btnPos28.VisibleRightLamp = true;
            this.btnPos28.DelayClick += new System.EventHandler(this.btnStepReset_DelayClick);
            this.btnPos28.Click += new System.EventHandler(this.btnMovePosition_Click);
            // 
            // btnPos22
            // 
            this.btnPos22.BackColor = System.Drawing.Color.Transparent;
            this.btnPos22.Delay = 3000;
            this.btnPos22.Flicker = false;
            this.btnPos22.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnPos22.ForeColor = System.Drawing.Color.Black;
            this.btnPos22.IsLeftLampOn = true;
            this.btnPos22.IsRightLampOn = true;
            this.btnPos22.LampOnWiatTime = 1000;
            this.btnPos22.LampSize = 5;
            this.btnPos22.LeftDelayOff = true;
            this.btnPos22.LeftLampColor = System.Drawing.Color.Red;
            this.btnPos22.Location = new System.Drawing.Point(192, 176);
            this.btnPos22.Name = "btnPos22";
            this.btnPos22.OffColor = System.Drawing.Color.Transparent;
            this.btnPos22.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btnPos22.OnOff = false;
            this.btnPos22.RightDelayOff = true;
            this.btnPos22.RightLampColor = System.Drawing.Color.Green;
            this.btnPos22.Size = new System.Drawing.Size(60, 33);
            this.btnPos22.TabIndex = 93;
            this.btnPos22.TabStop = false;
            this.btnPos22.Text = "2 Scan";
            this.btnPos22.Text2 = "";
            this.btnPos22.UseVisualStyleBackColor = false;
            this.btnPos22.VisibleLeftLamp = true;
            this.btnPos22.VisibleRightLamp = true;
            this.btnPos22.DelayClick += new System.EventHandler(this.btnStepReset_DelayClick);
            this.btnPos22.Click += new System.EventHandler(this.btnMovePosition_Click);
            // 
            // btnPos16
            // 
            this.btnPos16.BackColor = System.Drawing.Color.Transparent;
            this.btnPos16.Delay = 3000;
            this.btnPos16.Flicker = false;
            this.btnPos16.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnPos16.ForeColor = System.Drawing.Color.Black;
            this.btnPos16.IsLeftLampOn = true;
            this.btnPos16.IsRightLampOn = true;
            this.btnPos16.LampOnWiatTime = 1000;
            this.btnPos16.LampSize = 5;
            this.btnPos16.LeftDelayOff = true;
            this.btnPos16.LeftLampColor = System.Drawing.Color.Red;
            this.btnPos16.Location = new System.Drawing.Point(192, 141);
            this.btnPos16.Name = "btnPos16";
            this.btnPos16.OffColor = System.Drawing.Color.Transparent;
            this.btnPos16.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btnPos16.OnOff = false;
            this.btnPos16.RightDelayOff = true;
            this.btnPos16.RightLampColor = System.Drawing.Color.Green;
            this.btnPos16.Size = new System.Drawing.Size(60, 33);
            this.btnPos16.TabIndex = 93;
            this.btnPos16.TabStop = false;
            this.btnPos16.Text = "2 Scan";
            this.btnPos16.Text2 = "";
            this.btnPos16.UseVisualStyleBackColor = false;
            this.btnPos16.VisibleLeftLamp = true;
            this.btnPos16.VisibleRightLamp = true;
            this.btnPos16.DelayClick += new System.EventHandler(this.btnStepReset_DelayClick);
            this.btnPos16.Click += new System.EventHandler(this.btnMovePosition_Click);
            // 
            // btnPos27
            // 
            this.btnPos27.BackColor = System.Drawing.Color.Transparent;
            this.btnPos27.Delay = 3000;
            this.btnPos27.Flicker = false;
            this.btnPos27.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnPos27.ForeColor = System.Drawing.Color.Black;
            this.btnPos27.IsLeftLampOn = true;
            this.btnPos27.IsRightLampOn = true;
            this.btnPos27.LampOnWiatTime = 1000;
            this.btnPos27.LampSize = 5;
            this.btnPos27.LeftDelayOff = true;
            this.btnPos27.LeftLampColor = System.Drawing.Color.Red;
            this.btnPos27.Location = new System.Drawing.Point(130, 211);
            this.btnPos27.Name = "btnPos27";
            this.btnPos27.OffColor = System.Drawing.Color.Transparent;
            this.btnPos27.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btnPos27.OnOff = false;
            this.btnPos27.RightDelayOff = true;
            this.btnPos27.RightLampColor = System.Drawing.Color.Green;
            this.btnPos27.Size = new System.Drawing.Size(60, 33);
            this.btnPos27.TabIndex = 93;
            this.btnPos27.TabStop = false;
            this.btnPos27.Text = "2 Scan";
            this.btnPos27.Text2 = "";
            this.btnPos27.UseVisualStyleBackColor = false;
            this.btnPos27.VisibleLeftLamp = true;
            this.btnPos27.VisibleRightLamp = true;
            this.btnPos27.DelayClick += new System.EventHandler(this.btnStepReset_DelayClick);
            this.btnPos27.Click += new System.EventHandler(this.btnMovePosition_Click);
            // 
            // btnPos21
            // 
            this.btnPos21.BackColor = System.Drawing.Color.Transparent;
            this.btnPos21.Delay = 3000;
            this.btnPos21.Flicker = false;
            this.btnPos21.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnPos21.ForeColor = System.Drawing.Color.Black;
            this.btnPos21.IsLeftLampOn = true;
            this.btnPos21.IsRightLampOn = true;
            this.btnPos21.LampOnWiatTime = 1000;
            this.btnPos21.LampSize = 5;
            this.btnPos21.LeftDelayOff = true;
            this.btnPos21.LeftLampColor = System.Drawing.Color.Red;
            this.btnPos21.Location = new System.Drawing.Point(130, 176);
            this.btnPos21.Name = "btnPos21";
            this.btnPos21.OffColor = System.Drawing.Color.Transparent;
            this.btnPos21.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btnPos21.OnOff = false;
            this.btnPos21.RightDelayOff = true;
            this.btnPos21.RightLampColor = System.Drawing.Color.Green;
            this.btnPos21.Size = new System.Drawing.Size(60, 33);
            this.btnPos21.TabIndex = 93;
            this.btnPos21.TabStop = false;
            this.btnPos21.Text = "2 Scan";
            this.btnPos21.Text2 = "";
            this.btnPos21.UseVisualStyleBackColor = false;
            this.btnPos21.VisibleLeftLamp = true;
            this.btnPos21.VisibleRightLamp = true;
            this.btnPos21.DelayClick += new System.EventHandler(this.btnStepReset_DelayClick);
            this.btnPos21.Click += new System.EventHandler(this.btnMovePosition_Click);
            // 
            // btnPos15
            // 
            this.btnPos15.BackColor = System.Drawing.Color.Transparent;
            this.btnPos15.Delay = 3000;
            this.btnPos15.Flicker = false;
            this.btnPos15.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnPos15.ForeColor = System.Drawing.Color.Black;
            this.btnPos15.IsLeftLampOn = true;
            this.btnPos15.IsRightLampOn = true;
            this.btnPos15.LampOnWiatTime = 1000;
            this.btnPos15.LampSize = 5;
            this.btnPos15.LeftDelayOff = true;
            this.btnPos15.LeftLampColor = System.Drawing.Color.Red;
            this.btnPos15.Location = new System.Drawing.Point(130, 141);
            this.btnPos15.Name = "btnPos15";
            this.btnPos15.OffColor = System.Drawing.Color.Transparent;
            this.btnPos15.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btnPos15.OnOff = false;
            this.btnPos15.RightDelayOff = true;
            this.btnPos15.RightLampColor = System.Drawing.Color.Green;
            this.btnPos15.Size = new System.Drawing.Size(60, 33);
            this.btnPos15.TabIndex = 93;
            this.btnPos15.TabStop = false;
            this.btnPos15.Text = "2 Scan";
            this.btnPos15.Text2 = "";
            this.btnPos15.UseVisualStyleBackColor = false;
            this.btnPos15.VisibleLeftLamp = true;
            this.btnPos15.VisibleRightLamp = true;
            this.btnPos15.DelayClick += new System.EventHandler(this.btnStepReset_DelayClick);
            this.btnPos15.Click += new System.EventHandler(this.btnMovePosition_Click);
            // 
            // btnPos26
            // 
            this.btnPos26.BackColor = System.Drawing.Color.Transparent;
            this.btnPos26.Delay = 3000;
            this.btnPos26.Flicker = false;
            this.btnPos26.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnPos26.ForeColor = System.Drawing.Color.Black;
            this.btnPos26.IsLeftLampOn = true;
            this.btnPos26.IsRightLampOn = true;
            this.btnPos26.LampOnWiatTime = 1000;
            this.btnPos26.LampSize = 5;
            this.btnPos26.LeftDelayOff = true;
            this.btnPos26.LeftLampColor = System.Drawing.Color.Red;
            this.btnPos26.Location = new System.Drawing.Point(68, 211);
            this.btnPos26.Name = "btnPos26";
            this.btnPos26.OffColor = System.Drawing.Color.Transparent;
            this.btnPos26.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btnPos26.OnOff = false;
            this.btnPos26.RightDelayOff = true;
            this.btnPos26.RightLampColor = System.Drawing.Color.Green;
            this.btnPos26.Size = new System.Drawing.Size(60, 33);
            this.btnPos26.TabIndex = 93;
            this.btnPos26.TabStop = false;
            this.btnPos26.Text = "2 Scan";
            this.btnPos26.Text2 = "";
            this.btnPos26.UseVisualStyleBackColor = false;
            this.btnPos26.VisibleLeftLamp = true;
            this.btnPos26.VisibleRightLamp = true;
            this.btnPos26.DelayClick += new System.EventHandler(this.btnStepReset_DelayClick);
            this.btnPos26.Click += new System.EventHandler(this.btnMovePosition_Click);
            // 
            // btnPos20
            // 
            this.btnPos20.BackColor = System.Drawing.Color.Transparent;
            this.btnPos20.Delay = 3000;
            this.btnPos20.Flicker = false;
            this.btnPos20.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnPos20.ForeColor = System.Drawing.Color.Black;
            this.btnPos20.IsLeftLampOn = true;
            this.btnPos20.IsRightLampOn = true;
            this.btnPos20.LampOnWiatTime = 1000;
            this.btnPos20.LampSize = 5;
            this.btnPos20.LeftDelayOff = true;
            this.btnPos20.LeftLampColor = System.Drawing.Color.Red;
            this.btnPos20.Location = new System.Drawing.Point(68, 176);
            this.btnPos20.Name = "btnPos20";
            this.btnPos20.OffColor = System.Drawing.Color.Transparent;
            this.btnPos20.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btnPos20.OnOff = false;
            this.btnPos20.RightDelayOff = true;
            this.btnPos20.RightLampColor = System.Drawing.Color.Green;
            this.btnPos20.Size = new System.Drawing.Size(60, 33);
            this.btnPos20.TabIndex = 93;
            this.btnPos20.TabStop = false;
            this.btnPos20.Text = "2 Scan";
            this.btnPos20.Text2 = "";
            this.btnPos20.UseVisualStyleBackColor = false;
            this.btnPos20.VisibleLeftLamp = true;
            this.btnPos20.VisibleRightLamp = true;
            this.btnPos20.DelayClick += new System.EventHandler(this.btnStepReset_DelayClick);
            this.btnPos20.Click += new System.EventHandler(this.btnMovePosition_Click);
            // 
            // btnPos14
            // 
            this.btnPos14.BackColor = System.Drawing.Color.Transparent;
            this.btnPos14.Delay = 3000;
            this.btnPos14.Flicker = false;
            this.btnPos14.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnPos14.ForeColor = System.Drawing.Color.Black;
            this.btnPos14.IsLeftLampOn = true;
            this.btnPos14.IsRightLampOn = true;
            this.btnPos14.LampOnWiatTime = 1000;
            this.btnPos14.LampSize = 5;
            this.btnPos14.LeftDelayOff = true;
            this.btnPos14.LeftLampColor = System.Drawing.Color.Red;
            this.btnPos14.Location = new System.Drawing.Point(68, 141);
            this.btnPos14.Name = "btnPos14";
            this.btnPos14.OffColor = System.Drawing.Color.Transparent;
            this.btnPos14.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btnPos14.OnOff = false;
            this.btnPos14.RightDelayOff = true;
            this.btnPos14.RightLampColor = System.Drawing.Color.Green;
            this.btnPos14.Size = new System.Drawing.Size(60, 33);
            this.btnPos14.TabIndex = 93;
            this.btnPos14.TabStop = false;
            this.btnPos14.Text = "2 Scan";
            this.btnPos14.Text2 = "";
            this.btnPos14.UseVisualStyleBackColor = false;
            this.btnPos14.VisibleLeftLamp = true;
            this.btnPos14.VisibleRightLamp = true;
            this.btnPos14.DelayClick += new System.EventHandler(this.btnStepReset_DelayClick);
            this.btnPos14.Click += new System.EventHandler(this.btnMovePosition_Click);
            // 
            // btnPos13
            // 
            this.btnPos13.BackColor = System.Drawing.Color.Transparent;
            this.btnPos13.Delay = 3000;
            this.btnPos13.Flicker = false;
            this.btnPos13.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnPos13.ForeColor = System.Drawing.Color.Black;
            this.btnPos13.IsLeftLampOn = true;
            this.btnPos13.IsRightLampOn = true;
            this.btnPos13.LampOnWiatTime = 1000;
            this.btnPos13.LampSize = 5;
            this.btnPos13.LeftDelayOff = true;
            this.btnPos13.LeftLampColor = System.Drawing.Color.Red;
            this.btnPos13.Location = new System.Drawing.Point(377, 106);
            this.btnPos13.Name = "btnPos13";
            this.btnPos13.OffColor = System.Drawing.Color.Transparent;
            this.btnPos13.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btnPos13.OnOff = false;
            this.btnPos13.RightDelayOff = true;
            this.btnPos13.RightLampColor = System.Drawing.Color.Green;
            this.btnPos13.Size = new System.Drawing.Size(60, 33);
            this.btnPos13.TabIndex = 92;
            this.btnPos13.TabStop = false;
            this.btnPos13.Text = "2 Scan";
            this.btnPos13.Text2 = "";
            this.btnPos13.UseVisualStyleBackColor = false;
            this.btnPos13.VisibleLeftLamp = true;
            this.btnPos13.VisibleRightLamp = true;
            this.btnPos13.DelayClick += new System.EventHandler(this.btnStepReset_DelayClick);
            this.btnPos13.Click += new System.EventHandler(this.btnMovePosition_Click);
            // 
            // btnPos12
            // 
            this.btnPos12.BackColor = System.Drawing.Color.Transparent;
            this.btnPos12.Delay = 3000;
            this.btnPos12.Flicker = false;
            this.btnPos12.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnPos12.ForeColor = System.Drawing.Color.Black;
            this.btnPos12.IsLeftLampOn = true;
            this.btnPos12.IsRightLampOn = true;
            this.btnPos12.LampOnWiatTime = 1000;
            this.btnPos12.LampSize = 5;
            this.btnPos12.LeftDelayOff = true;
            this.btnPos12.LeftLampColor = System.Drawing.Color.Red;
            this.btnPos12.Location = new System.Drawing.Point(316, 106);
            this.btnPos12.Name = "btnPos12";
            this.btnPos12.OffColor = System.Drawing.Color.Transparent;
            this.btnPos12.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btnPos12.OnOff = false;
            this.btnPos12.RightDelayOff = true;
            this.btnPos12.RightLampColor = System.Drawing.Color.Green;
            this.btnPos12.Size = new System.Drawing.Size(60, 33);
            this.btnPos12.TabIndex = 92;
            this.btnPos12.TabStop = false;
            this.btnPos12.Text = "2 Scan";
            this.btnPos12.Text2 = "";
            this.btnPos12.UseVisualStyleBackColor = false;
            this.btnPos12.VisibleLeftLamp = true;
            this.btnPos12.VisibleRightLamp = true;
            this.btnPos12.DelayClick += new System.EventHandler(this.btnStepReset_DelayClick);
            this.btnPos12.Click += new System.EventHandler(this.btnMovePosition_Click);
            // 
            // btnPos11
            // 
            this.btnPos11.BackColor = System.Drawing.Color.Transparent;
            this.btnPos11.Delay = 3000;
            this.btnPos11.Flicker = false;
            this.btnPos11.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnPos11.ForeColor = System.Drawing.Color.Black;
            this.btnPos11.IsLeftLampOn = true;
            this.btnPos11.IsRightLampOn = true;
            this.btnPos11.LampOnWiatTime = 1000;
            this.btnPos11.LampSize = 5;
            this.btnPos11.LeftDelayOff = true;
            this.btnPos11.LeftLampColor = System.Drawing.Color.Red;
            this.btnPos11.Location = new System.Drawing.Point(254, 106);
            this.btnPos11.Name = "btnPos11";
            this.btnPos11.OffColor = System.Drawing.Color.Transparent;
            this.btnPos11.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btnPos11.OnOff = false;
            this.btnPos11.RightDelayOff = true;
            this.btnPos11.RightLampColor = System.Drawing.Color.Green;
            this.btnPos11.Size = new System.Drawing.Size(60, 33);
            this.btnPos11.TabIndex = 91;
            this.btnPos11.TabStop = false;
            this.btnPos11.Text = "2 Scan";
            this.btnPos11.Text2 = "";
            this.btnPos11.UseVisualStyleBackColor = false;
            this.btnPos11.VisibleLeftLamp = true;
            this.btnPos11.VisibleRightLamp = true;
            this.btnPos11.DelayClick += new System.EventHandler(this.btnStepReset_DelayClick);
            this.btnPos11.Click += new System.EventHandler(this.btnMovePosition_Click);
            // 
            // btnPos10
            // 
            this.btnPos10.BackColor = System.Drawing.Color.Transparent;
            this.btnPos10.Delay = 3000;
            this.btnPos10.Flicker = false;
            this.btnPos10.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnPos10.ForeColor = System.Drawing.Color.Black;
            this.btnPos10.IsLeftLampOn = true;
            this.btnPos10.IsRightLampOn = true;
            this.btnPos10.LampOnWiatTime = 1000;
            this.btnPos10.LampSize = 5;
            this.btnPos10.LeftDelayOff = true;
            this.btnPos10.LeftLampColor = System.Drawing.Color.Red;
            this.btnPos10.Location = new System.Drawing.Point(192, 106);
            this.btnPos10.Name = "btnPos10";
            this.btnPos10.OffColor = System.Drawing.Color.Transparent;
            this.btnPos10.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btnPos10.OnOff = false;
            this.btnPos10.RightDelayOff = true;
            this.btnPos10.RightLampColor = System.Drawing.Color.Green;
            this.btnPos10.Size = new System.Drawing.Size(60, 33);
            this.btnPos10.TabIndex = 90;
            this.btnPos10.TabStop = false;
            this.btnPos10.Text = "2 Scan";
            this.btnPos10.Text2 = "";
            this.btnPos10.UseVisualStyleBackColor = false;
            this.btnPos10.VisibleLeftLamp = true;
            this.btnPos10.VisibleRightLamp = true;
            this.btnPos10.DelayClick += new System.EventHandler(this.btnStepReset_DelayClick);
            this.btnPos10.Click += new System.EventHandler(this.btnMovePosition_Click);
            // 
            // btnPos9
            // 
            this.btnPos9.BackColor = System.Drawing.Color.Transparent;
            this.btnPos9.Delay = 3000;
            this.btnPos9.Flicker = false;
            this.btnPos9.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnPos9.ForeColor = System.Drawing.Color.Black;
            this.btnPos9.IsLeftLampOn = true;
            this.btnPos9.IsRightLampOn = true;
            this.btnPos9.LampOnWiatTime = 1000;
            this.btnPos9.LampSize = 5;
            this.btnPos9.LeftDelayOff = true;
            this.btnPos9.LeftLampColor = System.Drawing.Color.Red;
            this.btnPos9.Location = new System.Drawing.Point(130, 106);
            this.btnPos9.Name = "btnPos9";
            this.btnPos9.OffColor = System.Drawing.Color.Transparent;
            this.btnPos9.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btnPos9.OnOff = false;
            this.btnPos9.RightDelayOff = true;
            this.btnPos9.RightLampColor = System.Drawing.Color.Green;
            this.btnPos9.Size = new System.Drawing.Size(60, 33);
            this.btnPos9.TabIndex = 89;
            this.btnPos9.TabStop = false;
            this.btnPos9.Text = "2 Scan";
            this.btnPos9.Text2 = "";
            this.btnPos9.UseVisualStyleBackColor = false;
            this.btnPos9.VisibleLeftLamp = true;
            this.btnPos9.VisibleRightLamp = true;
            this.btnPos9.DelayClick += new System.EventHandler(this.btnStepReset_DelayClick);
            this.btnPos9.Click += new System.EventHandler(this.btnMovePosition_Click);
            // 
            // btnPos8
            // 
            this.btnPos8.BackColor = System.Drawing.Color.Transparent;
            this.btnPos8.Delay = 3000;
            this.btnPos8.Flicker = false;
            this.btnPos8.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnPos8.ForeColor = System.Drawing.Color.Black;
            this.btnPos8.IsLeftLampOn = true;
            this.btnPos8.IsRightLampOn = true;
            this.btnPos8.LampOnWiatTime = 1000;
            this.btnPos8.LampSize = 5;
            this.btnPos8.LeftDelayOff = true;
            this.btnPos8.LeftLampColor = System.Drawing.Color.Red;
            this.btnPos8.Location = new System.Drawing.Point(68, 106);
            this.btnPos8.Name = "btnPos8";
            this.btnPos8.OffColor = System.Drawing.Color.Transparent;
            this.btnPos8.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btnPos8.OnOff = false;
            this.btnPos8.RightDelayOff = true;
            this.btnPos8.RightLampColor = System.Drawing.Color.Green;
            this.btnPos8.Size = new System.Drawing.Size(60, 33);
            this.btnPos8.TabIndex = 88;
            this.btnPos8.TabStop = false;
            this.btnPos8.Text = "2 Scan";
            this.btnPos8.Text2 = "";
            this.btnPos8.UseVisualStyleBackColor = false;
            this.btnPos8.VisibleLeftLamp = true;
            this.btnPos8.VisibleRightLamp = true;
            this.btnPos8.DelayClick += new System.EventHandler(this.btnStepReset_DelayClick);
            this.btnPos8.Click += new System.EventHandler(this.btnMovePosition_Click);
            // 
            // btnPos7
            // 
            this.btnPos7.BackColor = System.Drawing.Color.Transparent;
            this.btnPos7.Delay = 3000;
            this.btnPos7.Flicker = false;
            this.btnPos7.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnPos7.ForeColor = System.Drawing.Color.Black;
            this.btnPos7.IsLeftLampOn = true;
            this.btnPos7.IsRightLampOn = true;
            this.btnPos7.LampOnWiatTime = 1000;
            this.btnPos7.LampSize = 5;
            this.btnPos7.LeftDelayOff = true;
            this.btnPos7.LeftLampColor = System.Drawing.Color.Red;
            this.btnPos7.Location = new System.Drawing.Point(377, 71);
            this.btnPos7.Name = "btnPos7";
            this.btnPos7.OffColor = System.Drawing.Color.Transparent;
            this.btnPos7.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btnPos7.OnOff = false;
            this.btnPos7.RightDelayOff = true;
            this.btnPos7.RightLampColor = System.Drawing.Color.Green;
            this.btnPos7.Size = new System.Drawing.Size(60, 33);
            this.btnPos7.TabIndex = 87;
            this.btnPos7.TabStop = false;
            this.btnPos7.Text = "2 Scan";
            this.btnPos7.Text2 = "";
            this.btnPos7.UseVisualStyleBackColor = false;
            this.btnPos7.VisibleLeftLamp = true;
            this.btnPos7.VisibleRightLamp = true;
            this.btnPos7.DelayClick += new System.EventHandler(this.btnStepReset_DelayClick);
            this.btnPos7.Click += new System.EventHandler(this.btnMovePosition_Click);
            // 
            // btnPos6
            // 
            this.btnPos6.BackColor = System.Drawing.Color.Transparent;
            this.btnPos6.Delay = 3000;
            this.btnPos6.Flicker = false;
            this.btnPos6.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnPos6.ForeColor = System.Drawing.Color.Black;
            this.btnPos6.IsLeftLampOn = true;
            this.btnPos6.IsRightLampOn = true;
            this.btnPos6.LampOnWiatTime = 1000;
            this.btnPos6.LampSize = 5;
            this.btnPos6.LeftDelayOff = true;
            this.btnPos6.LeftLampColor = System.Drawing.Color.Red;
            this.btnPos6.Location = new System.Drawing.Point(316, 71);
            this.btnPos6.Name = "btnPos6";
            this.btnPos6.OffColor = System.Drawing.Color.Transparent;
            this.btnPos6.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btnPos6.OnOff = false;
            this.btnPos6.RightDelayOff = true;
            this.btnPos6.RightLampColor = System.Drawing.Color.Green;
            this.btnPos6.Size = new System.Drawing.Size(60, 33);
            this.btnPos6.TabIndex = 87;
            this.btnPos6.TabStop = false;
            this.btnPos6.Text = "2 Scan";
            this.btnPos6.Text2 = "";
            this.btnPos6.UseVisualStyleBackColor = false;
            this.btnPos6.VisibleLeftLamp = true;
            this.btnPos6.VisibleRightLamp = true;
            this.btnPos6.DelayClick += new System.EventHandler(this.btnStepReset_DelayClick);
            this.btnPos6.Click += new System.EventHandler(this.btnMovePosition_Click);
            // 
            // btnPos5
            // 
            this.btnPos5.BackColor = System.Drawing.Color.Transparent;
            this.btnPos5.Delay = 3000;
            this.btnPos5.Flicker = false;
            this.btnPos5.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnPos5.ForeColor = System.Drawing.Color.Black;
            this.btnPos5.IsLeftLampOn = true;
            this.btnPos5.IsRightLampOn = true;
            this.btnPos5.LampOnWiatTime = 1000;
            this.btnPos5.LampSize = 5;
            this.btnPos5.LeftDelayOff = true;
            this.btnPos5.LeftLampColor = System.Drawing.Color.Red;
            this.btnPos5.Location = new System.Drawing.Point(254, 71);
            this.btnPos5.Name = "btnPos5";
            this.btnPos5.OffColor = System.Drawing.Color.Transparent;
            this.btnPos5.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btnPos5.OnOff = false;
            this.btnPos5.RightDelayOff = true;
            this.btnPos5.RightLampColor = System.Drawing.Color.Green;
            this.btnPos5.Size = new System.Drawing.Size(60, 33);
            this.btnPos5.TabIndex = 86;
            this.btnPos5.TabStop = false;
            this.btnPos5.Text = "2 Scan";
            this.btnPos5.Text2 = "";
            this.btnPos5.UseVisualStyleBackColor = false;
            this.btnPos5.VisibleLeftLamp = true;
            this.btnPos5.VisibleRightLamp = true;
            this.btnPos5.DelayClick += new System.EventHandler(this.btnStepReset_DelayClick);
            this.btnPos5.Click += new System.EventHandler(this.btnMovePosition_Click);
            // 
            // btnPos4
            // 
            this.btnPos4.BackColor = System.Drawing.Color.Transparent;
            this.btnPos4.Delay = 3000;
            this.btnPos4.Flicker = false;
            this.btnPos4.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnPos4.ForeColor = System.Drawing.Color.Black;
            this.btnPos4.IsLeftLampOn = true;
            this.btnPos4.IsRightLampOn = true;
            this.btnPos4.LampOnWiatTime = 1000;
            this.btnPos4.LampSize = 5;
            this.btnPos4.LeftDelayOff = true;
            this.btnPos4.LeftLampColor = System.Drawing.Color.Red;
            this.btnPos4.Location = new System.Drawing.Point(192, 71);
            this.btnPos4.Name = "btnPos4";
            this.btnPos4.OffColor = System.Drawing.Color.Transparent;
            this.btnPos4.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btnPos4.OnOff = false;
            this.btnPos4.RightDelayOff = true;
            this.btnPos4.RightLampColor = System.Drawing.Color.Green;
            this.btnPos4.Size = new System.Drawing.Size(60, 33);
            this.btnPos4.TabIndex = 85;
            this.btnPos4.TabStop = false;
            this.btnPos4.Text = "2 Scan";
            this.btnPos4.Text2 = "";
            this.btnPos4.UseVisualStyleBackColor = false;
            this.btnPos4.VisibleLeftLamp = true;
            this.btnPos4.VisibleRightLamp = true;
            this.btnPos4.DelayClick += new System.EventHandler(this.btnStepReset_DelayClick);
            this.btnPos4.Click += new System.EventHandler(this.btnMovePosition_Click);
            // 
            // btnMoveLoading
            // 
            this.btnMoveLoading.BackColor = System.Drawing.Color.Transparent;
            this.btnMoveLoading.Delay = 3000;
            this.btnMoveLoading.Flicker = false;
            this.btnMoveLoading.Font = new System.Drawing.Font("맑은 고딕", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMoveLoading.ForeColor = System.Drawing.Color.Black;
            this.btnMoveLoading.IsLeftLampOn = true;
            this.btnMoveLoading.IsRightLampOn = true;
            this.btnMoveLoading.LampOnWiatTime = 1000;
            this.btnMoveLoading.LampSize = 5;
            this.btnMoveLoading.LeftDelayOff = true;
            this.btnMoveLoading.LeftLampColor = System.Drawing.Color.Red;
            this.btnMoveLoading.Location = new System.Drawing.Point(150, 30);
            this.btnMoveLoading.Name = "btnMoveLoading";
            this.btnMoveLoading.OffColor = System.Drawing.Color.Transparent;
            this.btnMoveLoading.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btnMoveLoading.OnOff = false;
            this.btnMoveLoading.RightDelayOff = true;
            this.btnMoveLoading.RightLampColor = System.Drawing.Color.Green;
            this.btnMoveLoading.Size = new System.Drawing.Size(80, 40);
            this.btnMoveLoading.TabIndex = 16;
            this.btnMoveLoading.TabStop = false;
            this.btnMoveLoading.Text = "Loading";
            this.btnMoveLoading.Text2 = "";
            this.btnMoveLoading.UseVisualStyleBackColor = false;
            this.btnMoveLoading.VisibleLeftLamp = true;
            this.btnMoveLoading.VisibleRightLamp = true;
            this.btnMoveLoading.DelayClick += new System.EventHandler(this.btnStepReset_DelayClick);
            this.btnMoveLoading.Click += new System.EventHandler(this.btnMovePosition_Click);
            // 
            // btnPos2
            // 
            this.btnPos2.BackColor = System.Drawing.Color.Transparent;
            this.btnPos2.Delay = 3000;
            this.btnPos2.Flicker = false;
            this.btnPos2.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnPos2.ForeColor = System.Drawing.Color.Black;
            this.btnPos2.IsLeftLampOn = true;
            this.btnPos2.IsRightLampOn = true;
            this.btnPos2.LampOnWiatTime = 1000;
            this.btnPos2.LampSize = 5;
            this.btnPos2.LeftDelayOff = true;
            this.btnPos2.LeftLampColor = System.Drawing.Color.Red;
            this.btnPos2.Location = new System.Drawing.Point(68, 71);
            this.btnPos2.Name = "btnPos2";
            this.btnPos2.OffColor = System.Drawing.Color.Transparent;
            this.btnPos2.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btnPos2.OnOff = false;
            this.btnPos2.RightDelayOff = true;
            this.btnPos2.RightLampColor = System.Drawing.Color.Green;
            this.btnPos2.Size = new System.Drawing.Size(60, 33);
            this.btnPos2.TabIndex = 10;
            this.btnPos2.TabStop = false;
            this.btnPos2.Text = "1 Scan";
            this.btnPos2.Text2 = "";
            this.btnPos2.UseVisualStyleBackColor = false;
            this.btnPos2.VisibleLeftLamp = true;
            this.btnPos2.VisibleRightLamp = true;
            this.btnPos2.DelayClick += new System.EventHandler(this.btnStepReset_DelayClick);
            this.btnPos2.Click += new System.EventHandler(this.btnMovePosition_Click);
            // 
            // btnPos3
            // 
            this.btnPos3.BackColor = System.Drawing.Color.Transparent;
            this.btnPos3.Delay = 3000;
            this.btnPos3.Flicker = false;
            this.btnPos3.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnPos3.ForeColor = System.Drawing.Color.Black;
            this.btnPos3.IsLeftLampOn = true;
            this.btnPos3.IsRightLampOn = true;
            this.btnPos3.LampOnWiatTime = 1000;
            this.btnPos3.LampSize = 5;
            this.btnPos3.LeftDelayOff = true;
            this.btnPos3.LeftLampColor = System.Drawing.Color.Red;
            this.btnPos3.Location = new System.Drawing.Point(130, 71);
            this.btnPos3.Name = "btnPos3";
            this.btnPos3.OffColor = System.Drawing.Color.Transparent;
            this.btnPos3.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btnPos3.OnOff = false;
            this.btnPos3.RightDelayOff = true;
            this.btnPos3.RightLampColor = System.Drawing.Color.Green;
            this.btnPos3.Size = new System.Drawing.Size(60, 33);
            this.btnPos3.TabIndex = 10;
            this.btnPos3.TabStop = false;
            this.btnPos3.Text = "2 Scan";
            this.btnPos3.Text2 = "";
            this.btnPos3.UseVisualStyleBackColor = false;
            this.btnPos3.VisibleLeftLamp = true;
            this.btnPos3.VisibleRightLamp = true;
            this.btnPos3.DelayClick += new System.EventHandler(this.btnStepReset_DelayClick);
            this.btnPos3.Click += new System.EventHandler(this.btnMovePosition_Click);
            // 
            // btnMoveHome
            // 
            this.btnMoveHome.BackColor = System.Drawing.Color.Transparent;
            this.btnMoveHome.Delay = 3000;
            this.btnMoveHome.Flicker = false;
            this.btnMoveHome.Font = new System.Drawing.Font("맑은 고딕", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMoveHome.ForeColor = System.Drawing.Color.Black;
            this.btnMoveHome.IsLeftLampOn = true;
            this.btnMoveHome.IsRightLampOn = true;
            this.btnMoveHome.LampOnWiatTime = 1000;
            this.btnMoveHome.LampSize = 5;
            this.btnMoveHome.LeftDelayOff = true;
            this.btnMoveHome.LeftLampColor = System.Drawing.Color.Red;
            this.btnMoveHome.Location = new System.Drawing.Point(68, 29);
            this.btnMoveHome.Name = "btnMoveHome";
            this.btnMoveHome.OffColor = System.Drawing.Color.Transparent;
            this.btnMoveHome.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btnMoveHome.OnOff = false;
            this.btnMoveHome.RightDelayOff = true;
            this.btnMoveHome.RightLampColor = System.Drawing.Color.Green;
            this.btnMoveHome.Size = new System.Drawing.Size(80, 40);
            this.btnMoveHome.TabIndex = 10;
            this.btnMoveHome.TabStop = false;
            this.btnMoveHome.Text = "Home";
            this.btnMoveHome.Text2 = "";
            this.btnMoveHome.UseVisualStyleBackColor = false;
            this.btnMoveHome.VisibleLeftLamp = true;
            this.btnMoveHome.VisibleRightLamp = true;
            this.btnMoveHome.DelayClick += new System.EventHandler(this.btnStepReset_DelayClick);
            this.btnMoveHome.Click += new System.EventHandler(this.btnMoveHome_Click);
            // 
            // btnMoveJogMinus
            // 
            this.btnMoveJogMinus.BackColor = System.Drawing.Color.Transparent;
            this.btnMoveJogMinus.Delay = 2;
            this.btnMoveJogMinus.Flicker = false;
            this.btnMoveJogMinus.Font = new System.Drawing.Font("맑은 고딕", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMoveJogMinus.ForeColor = System.Drawing.Color.Black;
            this.btnMoveJogMinus.IsLeftLampOn = false;
            this.btnMoveJogMinus.IsRightLampOn = false;
            this.btnMoveJogMinus.LampOnWiatTime = 0;
            this.btnMoveJogMinus.LampSize = 1;
            this.btnMoveJogMinus.LeftDelayOff = false;
            this.btnMoveJogMinus.LeftLampColor = System.Drawing.Color.Red;
            this.btnMoveJogMinus.Location = new System.Drawing.Point(234, 29);
            this.btnMoveJogMinus.Name = "btnMoveJogMinus";
            this.btnMoveJogMinus.OffColor = System.Drawing.Color.Transparent;
            this.btnMoveJogMinus.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btnMoveJogMinus.OnOff = false;
            this.btnMoveJogMinus.RightDelayOff = false;
            this.btnMoveJogMinus.RightLampColor = System.Drawing.Color.Green;
            this.btnMoveJogMinus.Size = new System.Drawing.Size(91, 40);
            this.btnMoveJogMinus.TabIndex = 10;
            this.btnMoveJogMinus.TabStop = false;
            this.btnMoveJogMinus.Text = "(-)◀◀";
            this.btnMoveJogMinus.Text2 = "";
            this.btnMoveJogMinus.UseVisualStyleBackColor = false;
            this.btnMoveJogMinus.VisibleLeftLamp = false;
            this.btnMoveJogMinus.VisibleRightLamp = false;
            this.btnMoveJogMinus.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnMoveJogMinus_MouseDown);
            this.btnMoveJogMinus.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnInspZMoveJogMinus_MouseUp);
            // 
            // btnMoveJogPlus
            // 
            this.btnMoveJogPlus.BackColor = System.Drawing.Color.Transparent;
            this.btnMoveJogPlus.Delay = 2;
            this.btnMoveJogPlus.Flicker = false;
            this.btnMoveJogPlus.Font = new System.Drawing.Font("맑은 고딕", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMoveJogPlus.ForeColor = System.Drawing.Color.Black;
            this.btnMoveJogPlus.IsLeftLampOn = false;
            this.btnMoveJogPlus.IsRightLampOn = false;
            this.btnMoveJogPlus.LampOnWiatTime = 0;
            this.btnMoveJogPlus.LampSize = 1;
            this.btnMoveJogPlus.LeftDelayOff = false;
            this.btnMoveJogPlus.LeftLampColor = System.Drawing.Color.Red;
            this.btnMoveJogPlus.Location = new System.Drawing.Point(331, 29);
            this.btnMoveJogPlus.Name = "btnMoveJogPlus";
            this.btnMoveJogPlus.OffColor = System.Drawing.Color.Transparent;
            this.btnMoveJogPlus.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btnMoveJogPlus.OnOff = false;
            this.btnMoveJogPlus.RightDelayOff = false;
            this.btnMoveJogPlus.RightLampColor = System.Drawing.Color.Green;
            this.btnMoveJogPlus.Size = new System.Drawing.Size(91, 40);
            this.btnMoveJogPlus.TabIndex = 10;
            this.btnMoveJogPlus.TabStop = false;
            this.btnMoveJogPlus.Text = "▶▶(+)";
            this.btnMoveJogPlus.Text2 = "";
            this.btnMoveJogPlus.UseVisualStyleBackColor = false;
            this.btnMoveJogPlus.VisibleLeftLamp = false;
            this.btnMoveJogPlus.VisibleRightLamp = false;
            this.btnMoveJogPlus.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnInspZMoveJogPlus_MouseDown);
            this.btnMoveJogPlus.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnInspZMoveJogPlus_MouseUp);
            // 
            // lblErr
            // 
            this.lblErr.AutoEllipsis = true;
            this.lblErr.BackColor = System.Drawing.Color.White;
            this.lblErr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblErr.Delay = 500;
            this.lblErr.DelayOff = false;
            this.lblErr.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErr.ForeColor = System.Drawing.Color.Black;
            this.lblErr.Location = new System.Drawing.Point(4, 157);
            this.lblErr.Name = "lblErr";
            this.lblErr.OffColor = System.Drawing.Color.White;
            this.lblErr.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblErr.OnOff = false;
            this.lblErr.Size = new System.Drawing.Size(60, 30);
            this.lblErr.TabIndex = 72;
            this.lblErr.Text = "Error";
            this.lblErr.Text2 = "";
            this.lblErr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblServoOn
            // 
            this.lblServoOn.AutoEllipsis = true;
            this.lblServoOn.BackColor = System.Drawing.Color.White;
            this.lblServoOn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblServoOn.Delay = 500;
            this.lblServoOn.DelayOff = false;
            this.lblServoOn.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblServoOn.ForeColor = System.Drawing.Color.Black;
            this.lblServoOn.Location = new System.Drawing.Point(4, 127);
            this.lblServoOn.Name = "lblServoOn";
            this.lblServoOn.OffColor = System.Drawing.Color.White;
            this.lblServoOn.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblServoOn.OnOff = false;
            this.lblServoOn.Size = new System.Drawing.Size(60, 30);
            this.lblServoOn.TabIndex = 72;
            this.lblServoOn.Text = "SVR On";
            this.lblServoOn.Text2 = "";
            this.lblServoOn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPositiveLimit
            // 
            this.lblPositiveLimit.AutoEllipsis = true;
            this.lblPositiveLimit.BackColor = System.Drawing.Color.White;
            this.lblPositiveLimit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPositiveLimit.Delay = 500;
            this.lblPositiveLimit.DelayOff = false;
            this.lblPositiveLimit.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPositiveLimit.ForeColor = System.Drawing.Color.Black;
            this.lblPositiveLimit.Location = new System.Drawing.Point(4, 96);
            this.lblPositiveLimit.Name = "lblPositiveLimit";
            this.lblPositiveLimit.OffColor = System.Drawing.Color.White;
            this.lblPositiveLimit.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblPositiveLimit.OnOff = false;
            this.lblPositiveLimit.Size = new System.Drawing.Size(60, 30);
            this.lblPositiveLimit.TabIndex = 73;
            this.lblPositiveLimit.Text = "P Limit";
            this.lblPositiveLimit.Text2 = "";
            this.lblPositiveLimit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNegativeLimit
            // 
            this.lblNegativeLimit.AutoEllipsis = true;
            this.lblNegativeLimit.BackColor = System.Drawing.Color.White;
            this.lblNegativeLimit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNegativeLimit.Delay = 500;
            this.lblNegativeLimit.DelayOff = false;
            this.lblNegativeLimit.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNegativeLimit.ForeColor = System.Drawing.Color.Black;
            this.lblNegativeLimit.Location = new System.Drawing.Point(4, 65);
            this.lblNegativeLimit.Name = "lblNegativeLimit";
            this.lblNegativeLimit.OffColor = System.Drawing.Color.White;
            this.lblNegativeLimit.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblNegativeLimit.OnOff = false;
            this.lblNegativeLimit.Size = new System.Drawing.Size(60, 30);
            this.lblNegativeLimit.TabIndex = 70;
            this.lblNegativeLimit.Text = "N Limit";
            this.lblNegativeLimit.Text2 = "";
            this.lblNegativeLimit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMoving
            // 
            this.lblMoving.AutoEllipsis = true;
            this.lblMoving.BackColor = System.Drawing.Color.White;
            this.lblMoving.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMoving.Delay = 500;
            this.lblMoving.DelayOff = false;
            this.lblMoving.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMoving.ForeColor = System.Drawing.Color.Black;
            this.lblMoving.Location = new System.Drawing.Point(4, 34);
            this.lblMoving.Name = "lblMoving";
            this.lblMoving.OffColor = System.Drawing.Color.White;
            this.lblMoving.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblMoving.OnOff = false;
            this.lblMoving.Size = new System.Drawing.Size(60, 30);
            this.lblMoving.TabIndex = 71;
            this.lblMoving.Text = "Moving";
            this.lblMoving.Text2 = "";
            this.lblMoving.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblHomeCompleteBit
            // 
            this.lblHomeCompleteBit.AutoEllipsis = true;
            this.lblHomeCompleteBit.BackColor = System.Drawing.Color.White;
            this.lblHomeCompleteBit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHomeCompleteBit.Delay = 500;
            this.lblHomeCompleteBit.DelayOff = false;
            this.lblHomeCompleteBit.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHomeCompleteBit.ForeColor = System.Drawing.Color.Black;
            this.lblHomeCompleteBit.Location = new System.Drawing.Point(4, 3);
            this.lblHomeCompleteBit.Name = "lblHomeCompleteBit";
            this.lblHomeCompleteBit.OffColor = System.Drawing.Color.White;
            this.lblHomeCompleteBit.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblHomeCompleteBit.OnOff = false;
            this.lblHomeCompleteBit.Size = new System.Drawing.Size(60, 30);
            this.lblHomeCompleteBit.TabIndex = 63;
            this.lblHomeCompleteBit.Text = "H Bit";
            this.lblHomeCompleteBit.Text2 = "";
            this.lblHomeCompleteBit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblJogSpeed
            // 
            this.lblJogSpeed.AutoEllipsis = true;
            this.lblJogSpeed.BackColor = System.Drawing.Color.Silver;
            this.lblJogSpeed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJogSpeed.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJogSpeed.ForeColor = System.Drawing.Color.Firebrick;
            this.lblJogSpeed.Location = new System.Drawing.Point(256, 6);
            this.lblJogSpeed.Name = "lblJogSpeed";
            this.lblJogSpeed.Size = new System.Drawing.Size(80, 21);
            this.lblJogSpeed.TabIndex = 83;
            this.lblJogSpeed.Text = "0";
            this.lblJogSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoEllipsis = true;
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Firebrick;
            this.label3.Location = new System.Drawing.Point(206, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 21);
            this.label3.TabIndex = 84;
            this.label3.Text = "속도";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblTitle);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(151, 15);
            this.panel1.TabIndex = 94;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoEllipsis = true;
            this.lblTitle.BackColor = System.Drawing.Color.Gainsboro;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitle.Font = new System.Drawing.Font("맑은 고딕", 8.25F);
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(149, 13);
            this.lblTitle.TabIndex = 9;
            this.lblTitle.Text = "■ INSPECT Z1";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPosition
            // 
            this.lblPosition.AutoEllipsis = true;
            this.lblPosition.BackColor = System.Drawing.Color.Silver;
            this.lblPosition.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPosition.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPosition.ForeColor = System.Drawing.Color.Firebrick;
            this.lblPosition.Location = new System.Drawing.Point(119, 7);
            this.lblPosition.Name = "lblPosition";
            this.lblPosition.Size = new System.Drawing.Size(80, 21);
            this.lblPosition.TabIndex = 95;
            this.lblPosition.Text = "0";
            this.lblPosition.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoEllipsis = true;
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Firebrick;
            this.label2.Location = new System.Drawing.Point(69, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 21);
            this.label2.TabIndex = 96;
            this.label2.Text = "위치";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tmrUiUpdate
            // 
            this.tmrUiUpdate.Tick += new System.EventHandler(this.tmrUiUpdate_Tick);
            // 
            // UcrlServo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlServo);
            this.Controls.Add(this.panel1);
            this.Name = "UcrlServo";
            this.Size = new System.Drawing.Size(470, 263);
            this.pnlServo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nuJobSpeed)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlServo;
        internal Dit.Framework.UI.ButtonDelay btnMoveLoading;
        internal Dit.Framework.UI.ButtonDelay btnPos2;
        internal Dit.Framework.UI.ButtonDelay btnPos3;
        internal Dit.Framework.UI.ButtonDelay btnMoveHome;
        internal Dit.Framework.UI.ButtonDelay btnMoveJogMinus;
        internal Dit.Framework.UI.ButtonDelay btnMoveJogPlus;
        internal Dit.Framework.UI.LabelDelay lblErr;
        internal Dit.Framework.UI.LabelDelay lblServoOn;
        internal Dit.Framework.UI.LabelDelay lblPositiveLimit;
        internal Dit.Framework.UI.LabelDelay lblNegativeLimit;
        internal Dit.Framework.UI.LabelDelay lblMoving;
        internal Dit.Framework.UI.LabelDelay lblHomeCompleteBit;
        internal System.Windows.Forms.Label lblJogSpeed;
        internal System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        internal System.Windows.Forms.Label lblTitle;
        internal Dit.Framework.UI.ButtonDelay btnPos24;
        internal Dit.Framework.UI.ButtonDelay btnPos18;
        internal Dit.Framework.UI.ButtonDelay btnPos23;
        internal Dit.Framework.UI.ButtonDelay btnPos17;
        internal Dit.Framework.UI.ButtonDelay btnPos22;
        internal Dit.Framework.UI.ButtonDelay btnPos16;
        internal Dit.Framework.UI.ButtonDelay btnPos21;
        internal Dit.Framework.UI.ButtonDelay btnPos15;
        internal Dit.Framework.UI.ButtonDelay btnPos20;
        internal Dit.Framework.UI.ButtonDelay btnPos14;
        internal Dit.Framework.UI.ButtonDelay btnPos12;
        internal Dit.Framework.UI.ButtonDelay btnPos11;
        internal Dit.Framework.UI.ButtonDelay btnPos10;
        internal Dit.Framework.UI.ButtonDelay btnPos9;
        internal Dit.Framework.UI.ButtonDelay btnPos8;
        internal Dit.Framework.UI.ButtonDelay btnPos6;
        internal Dit.Framework.UI.ButtonDelay btnPos5;
        internal Dit.Framework.UI.ButtonDelay btnPos4;
        private System.Windows.Forms.NumericUpDown nuJobSpeed;
        internal Dit.Framework.UI.ButtonDelay btnPos31;
        internal Dit.Framework.UI.ButtonDelay btnPos25;
        internal Dit.Framework.UI.ButtonDelay btnPos30;
        internal Dit.Framework.UI.ButtonDelay btnPos19;
        internal Dit.Framework.UI.ButtonDelay btnPos29;
        internal Dit.Framework.UI.ButtonDelay btnPos28;
        internal Dit.Framework.UI.ButtonDelay btnPos27;
        internal Dit.Framework.UI.ButtonDelay btnPos26;
        internal Dit.Framework.UI.ButtonDelay btnPos13;
        internal Dit.Framework.UI.ButtonDelay btnPos7;
        internal System.Windows.Forms.Label lblPosition;
        internal System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer tmrUiUpdate;


    }
}
