﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Dit.Framework.UI;


namespace DIT.TLC.CTRL
{
    [System.ComponentModel.ToolboxItem(true)]
    public partial class UcrlServo : UserControl
    {
        private ServoMotorControl _servo;
        private Equipment _equip;

        private bool _isDelayClicked = false;

        private ButtonDelay[] btns = null;
        private String[] names = null;

        public string PositionName01 { get; set; }
        public string PositionName02 { get; set; }
        public string PositionName03 { get; set; }
        public string PositionName04 { get; set; }
        public string PositionName05 { get; set; }
        public string PositionName06 { get; set; }
        public string PositionName07 { get; set; }
        public string PositionName08 { get; set; }
        public string PositionName09 { get; set; }
        public string PositionName10 { get; set; }
        public string PositionName11 { get; set; }
        public string PositionName12 { get; set; }
        public string PositionName13 { get; set; }
        public string PositionName14 { get; set; }
        public string PositionName15 { get; set; }
        public string PositionName16 { get; set; }
        public string PositionName17 { get; set; }
        public string PositionName18 { get; set; }
        public string PositionName19 { get; set; }
        public string PositionName20 { get; set; }
        public string PositionName21 { get; set; }
        public string PositionName22 { get; set; }
        public string PositionName23 { get; set; }
        public string PositionName24 { get; set; }
        public string PositionName25 { get; set; }
        public string PositionName26 { get; set; }
        public string PositionName27 { get; set; }
        public string PositionName28 { get; set; }
        public string PositionName29 { get; set; }
        public string PositionName30 { get; set; }
        public string PositionName31 { get; set; }

        public UcrlServo()
        {
            InitializeComponent();

            btns = new ButtonDelay[]
            {
                null, null, btnPos2, btnPos3, btnPos4,
                btnPos5, btnPos6, btnPos7, btnPos8, btnPos9, btnPos10,
                btnPos11, btnPos12, btnPos13, btnPos14, btnPos15, btnPos16,
                btnPos17, btnPos18, btnPos19, btnPos20, btnPos21, btnPos22,
                btnPos23, btnPos24, btnPos25, btnPos26, btnPos27,
                btnPos28, btnPos29, btnPos30, btnPos31,
            };
        }
        public void SetInstance(Equipment equip, ServoMotorControl servo, string name)
        {
            _servo = servo;
            _equip = equip;
            lblTitle.Text = name;

            names = new string[]
            {
                string.Empty, string.Empty, PositionName01, PositionName02, PositionName03,
                PositionName04, PositionName05, PositionName06, PositionName07, PositionName08,
                PositionName09, PositionName10, PositionName11, PositionName12, PositionName13,
                PositionName14, PositionName15, PositionName16, PositionName17, PositionName18,
                PositionName19, PositionName20, PositionName21, PositionName22, PositionName23,
                PositionName24, PositionName25, PositionName26, PositionName27, PositionName28,
                PositionName29, PositionName30, PositionName31
            };

            for (int iPos = 2; iPos < btns.Length; iPos++)
            {
                if (iPos < _servo.PositionCount)
                {
                    btns[iPos].Visible = true;
                    btns[iPos].Text = names[iPos];
                }
                else
                {
                    btns[iPos].Visible = false;
                }
            }
        }
        public void UpdateUI()
        {

            //SetEnabled(pnlServo, _equip.EquipRunMode == EmEquipRunMode.Manual && _equip.IsHomePositioning == false);

            //CMD & ACK                                          
            btnMoveHome.LeftDelayOff                  /**/ = _servo.YB_Position0MoveCmd[0].vBit || _servo.YB_HomeCmd;
            btnMoveHome.RightDelayOff                 /**/ = _servo.XB_Position0MoveCmdAck[0].vBit || _servo.XB_HomeCmdAck;
            btnMoveLoading.LeftDelayOff               /**/ = _servo.YB_Position0MoveCmd[1].vBit;
            btnMoveLoading.RightDelayOff              /**/ = _servo.XB_Position0MoveCmdAck[1].vBit;

            for (int iPos = 2; iPos < _servo.PositionCount; iPos++)
            {
                btns[iPos].LeftDelayOff                     /**/ = _servo.YB_Position0MoveCmd[iPos].vBit;
                btns[iPos].RightDelayOff                    /**/ = _servo.XB_Position0MoveCmdAck[iPos].vBit;

                btns[iPos].OnOff                            /**/ = _servo.IsMoveOnPosition(iPos);
                btns[iPos].Flicker                          /**/ = _servo.IsMovingOnPosition(iPos);
            }

            //INSP Z SERVO STATE 
            lblHomeCompleteBit.BackColor              /**/ = _servo.IsHomeCompleteBit ? Color.FromArgb(255, 100, 100) : Color.Gainsboro;
            lblMoving.BackColor                       /**/ = _servo.IsMoving ? Color.FromArgb(255, 100, 100) : Color.Gainsboro;
            lblNegativeLimit.BackColor                /**/ = _servo.IsNegativeLimit ? Color.FromArgb(255, 100, 100) : Color.Gainsboro;
            lblPositiveLimit.BackColor                /**/ = _servo.IsPositiveLimit ? Color.FromArgb(255, 100, 100) : Color.Gainsboro;
            lblServoOn.BackColor                      /**/ = _servo.IsServoOnBit ? Color.FromArgb(255, 100, 100) : Color.Gainsboro;
            lblErr.BackColor                          /**/ = _servo.IsFatalServoError ? Color.FromArgb(255, 100, 100) : Color.Gainsboro;

            //INSPZ BTN STATE
            btnMoveHome.OnOff                         /**/ = _servo.IsMoveOnPosition(0) && _servo.IsHomeOnPosition();
            btnMoveHome.Flicker                       /**/ = _servo.IsMovingOnPosition(0) || _servo.IsMovingOnHome();
            btnMoveJogMinus.OnOff                     /**/ = _servo.YB_MotorJogMinusMove.vBit == true;
            btnMoveJogPlus.OnOff                      /**/ = _servo.YB_MotorJogPlusMove.vBit == true;

            btnMoveLoading.OnOff                      /**/ = _servo.IsMoveOnPosition(1);
            btnMoveLoading.Flicker                    /**/ = _servo.IsMovingOnPosition(1);
        }
        public void SetEnabled(Panel pnl, bool value)
        {
            foreach (Control btn in pnl.Controls)
            {
                if (btn is ButtonDelay)
                    ((ButtonDelay)btn).Enabled = value;
                else if (btn is Panel)
                    SetEnabled((Panel)btn, value);
            }
        }
        private void nuJogSpeed_ValueChanged(object sender, EventArgs e)
        {
            float speed = ((float)nuJobSpeed.Value > (float)_servo.SoftJogSpeedLimit * 1000.0f ? (float)_servo.SoftJogSpeedLimit : (float)nuJobSpeed.Value / 1000.0f);
            lblJogSpeed.Text = string.Format("{0:N2}", speed);
        }
        private void nuJobSpeed_KeyDown(object sender, KeyEventArgs e)
        {
            float speed = ((float)nuJobSpeed.Value > (float)_servo.SoftJogSpeedLimit * 1000.0f ? (float)_servo.SoftJogSpeedLimit : (float)nuJobSpeed.Value / 1000.0f);
            lblJogSpeed.Text = string.Format("{0:N2}", speed);
        }
        private void btnMoveHome_Click(object sender, EventArgs e)
        {
            if (_isDelayClicked == true)
            {
                _isDelayClicked = false;
                return;
            }
            if ((ModifierKeys & Keys.Control) == Keys.Control)
                _servo.GoHome(_equip);
            else
                _servo.GoHomeOrPositionOne(_equip);
        }
        private void btnMoveJogMinus_MouseDown(object sender, MouseEventArgs e)
        {
            float speed = ((float)nuJobSpeed.Value > (float)_servo.SoftJogSpeedLimit * 1000.0f ? (float)_servo.SoftJogSpeedLimit : (float)nuJobSpeed.Value / 1000.0f);
            _servo.JogMove(_equip, EM_SERVO_JOG.JOG_MINUS, speed);
        }
        private void btnInspZMoveJogMinus_MouseUp(object sender, MouseEventArgs e)
        {
            _servo.JogMove(_equip, EM_SERVO_JOG.JOG_STOP, 0);
        }
        private void btnInspZMoveJogPlus_MouseDown(object sender, MouseEventArgs e)
        {
            float speed = ((float)nuJobSpeed.Value > (float)_servo.SoftJogSpeedLimit * 1000.0f ? (float)_servo.SoftJogSpeedLimit : (float)nuJobSpeed.Value / 1000.0f);
            _servo.JogMove(_equip, EM_SERVO_JOG.JOG_PLUS, speed);
        }
        private void btnInspZMoveJogPlus_MouseUp(object sender, MouseEventArgs e)
        {
            _servo.JogMove(_equip, EM_SERVO_JOG.JOG_STOP, 0);
        }
        private void btnMovePosition_Click(object sender, EventArgs e)
        {
            ButtonDelay btn = sender as ButtonDelay;

            if (_isDelayClicked == true)
            {
                _isDelayClicked = false;
                return;
            }

            if (btn == btnMoveLoading)
                _servo.MovePosition(_equip, 1);
            else if (btn == btnPos2)
                _servo.MovePosition(_equip, 2);
            else if (btn == btnPos3)
                _servo.MovePosition(_equip, 3);
            else if (btn == btnPos4)
                _servo.MovePosition(_equip, 4);
            else if (btn == btnPos5)
                _servo.MovePosition(_equip, 5);
            else if (btn == btnPos6)
                _servo.MovePosition(_equip, 6);
            else if (btn == btnPos7)
                _servo.MovePosition(_equip, 7);
            else if (btn == btnPos8)
                _servo.MovePosition(_equip, 8);
            else if (btn == btnPos9)
                _servo.MovePosition(_equip, 9);
            else if (btn == btnPos10)
                _servo.MovePosition(_equip, 10);
            else if (btn == btnPos11)
                _servo.MovePosition(_equip, 11);
            else if (btn == btnPos12)
                _servo.MovePosition(_equip, 12);
            else if (btn == btnPos13)
                _servo.MovePosition(_equip, 13);
            else if (btn == btnPos14)
                _servo.MovePosition(_equip, 14);
            else if (btn == btnPos15)
                _servo.MovePosition(_equip, 15);
            else if (btn == btnPos16)
                _servo.MovePosition(_equip, 16);
            else if (btn == btnPos17)
                _servo.MovePosition(_equip, 17);
            else if (btn == btnPos18)
                _servo.MovePosition(_equip, 18);
            else if (btn == btnPos19)
                _servo.MovePosition(_equip, 19);
            else if (btn == btnPos20)
                _servo.MovePosition(_equip, 20);
            else if (btn == btnPos21)
                _servo.MovePosition(_equip, 21);
            else if (btn == btnPos22)
                _servo.MovePosition(_equip, 22);
            else if (btn == btnPos23)
                _servo.MovePosition(_equip, 23);
            else if (btn == btnPos24)
                _servo.MovePosition(_equip, 24);
            else if (btn == btnPos25)
                _servo.MovePosition(_equip, 25);
            else if (btn == btnPos26)
                _servo.MovePosition(_equip, 26);
            else if (btn == btnPos27)
                _servo.MovePosition(_equip, 27);
            else if (btn == btnPos28)
                _servo.MovePosition(_equip, 28);
            else if (btn == btnPos29)
                _servo.MovePosition(_equip, 29);
            else if (btn == btnPos30)
                _servo.MovePosition(_equip, 30);
            else if (btn == btnPos31)
                _servo.MovePosition(_equip, 31);
        }
        private void btnStepReset_DelayClick(object sender, EventArgs e)
        {
            _isDelayClicked = true;

            ButtonDelay btnd = sender as ButtonDelay;

            if (btnd == btnMoveHome)
                _servo.HomeStepReset();
            else if (btnd == btnMoveLoading)
                _servo.StepReset(1);
            else if (btnd == btnPos2)
                _servo.StepReset(2);
            else if (btnd == btnPos3)
                _servo.StepReset(3);
            else if (btnd == btnPos4)
                _servo.StepReset(4);
            else if (btnd == btnPos5)
                _servo.StepReset(5);
            else if (btnd == btnPos6)
                _servo.StepReset(6);
            else if (btnd == btnPos7)
                _servo.StepReset(7);
            else if (btnd == btnPos8)
                _servo.StepReset(8);
            else if (btnd == btnPos9)
                _servo.StepReset(9);
            else if (btnd == btnPos10)
                _servo.StepReset(10);
            else if (btnd == btnPos11)
                _servo.StepReset(11);
            else if (btnd == btnPos12)
                _servo.StepReset(12);
            else if (btnd == btnPos13)
                _servo.StepReset(13);
            else if (btnd == btnPos14)
                _servo.StepReset(14);
            else if (btnd == btnPos15)
                _servo.StepReset(15);
            else if (btnd == btnPos16)
                _servo.StepReset(16);
            else if (btnd == btnPos17)
                _servo.StepReset(17);
            else if (btnd == btnPos18)
                _servo.StepReset(18);
            else if (btnd == btnPos19)
                _servo.StepReset(19);
            else if (btnd == btnPos20)
                _servo.StepReset(20);
            else if (btnd == btnPos21)
                _servo.StepReset(21);
            else if (btnd == btnPos22)
                _servo.StepReset(22);
            else if (btnd == btnPos23)
                _servo.StepReset(23);
            else if (btnd == btnPos24)
                _servo.StepReset(24);
            else if (btnd == btnPos25)
                _servo.StepReset(25);
            else if (btnd == btnPos26)
                _servo.StepReset(26);
            else if (btnd == btnPos27)
                _servo.StepReset(27);
            else if (btnd == btnPos28)
                _servo.StepReset(28);
            else if (btnd == btnPos29)
                _servo.StepReset(29);
            else if (btnd == btnPos30)
                _servo.StepReset(30);
            else if (btnd == btnPos31)
                _servo.StepReset(31);
        }

        private void tmrUiUpdate_Tick(object sender, EventArgs e)
        {
            if (_servo == null)
                lblPosition.Text = _servo.XF_CurrMotorPosition.vFloat.ToString("0.00#");
            else
                lblPosition.Text = "0";
        }
    }
}
