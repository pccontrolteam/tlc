﻿namespace DIT.TLC.CTRL
{
    partial class FrmMain
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstLogger = new System.Windows.Forms.ListView();
            this.flPnlServo = new System.Windows.Forms.FlowLayoutPanel();
            this.ucrlServo1 = new DIT.TLC.CTRL.UcrlServo();
            this.SuspendLayout();
            // 
            // lstLogger
            // 
            this.lstLogger.Location = new System.Drawing.Point(531, 12);
            this.lstLogger.Name = "lstLogger";
            this.lstLogger.Size = new System.Drawing.Size(758, 386);
            this.lstLogger.TabIndex = 96;
            this.lstLogger.UseCompatibleStateImageBehavior = false;
            this.lstLogger.View = System.Windows.Forms.View.Details;
            // 
            // flPnlServo
            // 
            this.flPnlServo.Location = new System.Drawing.Point(44, 34);
            this.flPnlServo.Name = "flPnlServo";
            this.flPnlServo.Size = new System.Drawing.Size(468, 336);
            this.flPnlServo.TabIndex = 98;
            // 
            // ucrlServo1
            // 
            this.ucrlServo1.Location = new System.Drawing.Point(44, 387);
            this.ucrlServo1.Name = "ucrlServo1";
            this.ucrlServo1.PositionName01 = null;
            this.ucrlServo1.PositionName02 = null;
            this.ucrlServo1.PositionName03 = null;
            this.ucrlServo1.PositionName04 = null;
            this.ucrlServo1.PositionName05 = null;
            this.ucrlServo1.PositionName06 = null;
            this.ucrlServo1.PositionName07 = null;
            this.ucrlServo1.PositionName08 = null;
            this.ucrlServo1.PositionName09 = null;
            this.ucrlServo1.PositionName10 = null;
            this.ucrlServo1.PositionName11 = null;
            this.ucrlServo1.PositionName12 = null;
            this.ucrlServo1.PositionName13 = null;
            this.ucrlServo1.PositionName14 = null;
            this.ucrlServo1.PositionName15 = null;
            this.ucrlServo1.PositionName16 = null;
            this.ucrlServo1.PositionName17 = null;
            this.ucrlServo1.PositionName18 = null;
            this.ucrlServo1.PositionName19 = null;
            this.ucrlServo1.PositionName20 = null;
            this.ucrlServo1.PositionName21 = null;
            this.ucrlServo1.PositionName22 = null;
            this.ucrlServo1.PositionName23 = null;
            this.ucrlServo1.PositionName24 = null;
            this.ucrlServo1.PositionName25 = null;
            this.ucrlServo1.PositionName26 = null;
            this.ucrlServo1.PositionName27 = null;
            this.ucrlServo1.PositionName28 = null;
            this.ucrlServo1.PositionName29 = null;
            this.ucrlServo1.PositionName30 = null;
            this.ucrlServo1.PositionName31 = null;
            this.ucrlServo1.Size = new System.Drawing.Size(425, 295);
            this.ucrlServo1.TabIndex = 99;
            // 
            // FrmMain
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1447, 795);
            this.Controls.Add(this.ucrlServo1);
            this.Controls.Add(this.flPnlServo);
            this.Controls.Add(this.lstLogger);
            this.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Name = "FrmMain";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lstLogger;
        private System.Windows.Forms.FlowLayoutPanel flPnlServo;
        private UcrlServo ucrlServo1;
    }
}

