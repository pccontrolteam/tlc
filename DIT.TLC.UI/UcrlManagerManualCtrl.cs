﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DIT.TLC.CTRL;

namespace DIT.TLC.UI
{
    public partial class UcrlManagerServoManualCtrl : UserControl
    {
        public UcrlManagerServoManualCtrl()
        {
            InitializeComponent();
        }


        public void InitalizeUserControlUI(Equipment equip)
        {
            ajin_Setting_Cellpurge1.Servo = equip.LD.Loader.X1Axis;
            ajin_Setting_Cellpurge2.Servo = equip.LD.Loader.X2Axis;
            ajin_Setting_Cellpurge3.Servo = equip.LD.Loader.Y1Axis;
            ajin_Setting_Cellpurge4.Servo = equip.LD.Loader.Y2Axis;        
        }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
