﻿namespace DIT.TLC.UI
{
    partial class Recipe_Recipe
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.gbox_dxfinfo = new System.Windows.Forms.GroupBox();
            this.gbox_layerinfo = new System.Windows.Forms.GroupBox();
            this.lb_ta_thickness = new System.Windows.Forms.Label();
            this.tbox_ta_thickness = new System.Windows.Forms.TextBox();
            this.combox_cell_type = new System.Windows.Forms.ComboBox();
            this.lb_cell_type = new System.Windows.Forms.Label();
            this.combox_process_param = new System.Windows.Forms.ComboBox();
            this.lb_process_param = new System.Windows.Forms.Label();
            this.combox_guide_layer = new System.Windows.Forms.ComboBox();
            this.lb_guide_layer = new System.Windows.Forms.Label();
            this.combox_process_layer = new System.Windows.Forms.ComboBox();
            this.lb_process_layer = new System.Windows.Forms.Label();
            this.combox_alignment_layer = new System.Windows.Forms.ComboBox();
            this.lb_alignment_layer = new System.Windows.Forms.Label();
            this.panel_dxfpath_load = new System.Windows.Forms.Panel();
            this.lb_dxfpath_load = new System.Windows.Forms.Label();
            this.tbox_dxfpath = new System.Windows.Forms.TextBox();
            this.lb_dxfpath = new System.Windows.Forms.Label();
            this.gbox_item = new System.Windows.Forms.GroupBox();
            this.tbox_cell_size_2 = new System.Windows.Forms.TextBox();
            this.tbox_cell_size_1 = new System.Windows.Forms.TextBox();
            this.lb_cell_size = new System.Windows.Forms.Label();
            this.panel_z_matrix = new System.Windows.Forms.Panel();
            this.lb_z_matrix = new System.Windows.Forms.Label();
            this.lb_a_matrix = new System.Windows.Forms.Label();
            this.panel53 = new System.Windows.Forms.Panel();
            this.label74 = new System.Windows.Forms.Label();
            this.panel_break_left_y_calc = new System.Windows.Forms.Panel();
            this.lb_break_left_y_calc = new System.Windows.Forms.Label();
            this.panel_break_right_2_calc = new System.Windows.Forms.Panel();
            this.lb_break_right_2_calc = new System.Windows.Forms.Label();
            this.panel_break_left_2_calc = new System.Windows.Forms.Panel();
            this.lb_break_left_2_calc = new System.Windows.Forms.Label();
            this.panel55 = new System.Windows.Forms.Panel();
            this.label76 = new System.Windows.Forms.Label();
            this.panel_break_left_y_move = new System.Windows.Forms.Panel();
            this.lb_break_left_y_move = new System.Windows.Forms.Label();
            this.panel56 = new System.Windows.Forms.Panel();
            this.label77 = new System.Windows.Forms.Label();
            this.panel_break_left_y_read = new System.Windows.Forms.Panel();
            this.lb_break_left_y_read = new System.Windows.Forms.Label();
            this.panel_break_right_1_calc = new System.Windows.Forms.Panel();
            this.lb_break_right_1_calc = new System.Windows.Forms.Label();
            this.panel_break_left_1_calc = new System.Windows.Forms.Panel();
            this.lb_break_left_1_calc = new System.Windows.Forms.Label();
            this.panel_break_right_2_move = new System.Windows.Forms.Panel();
            this.lb_break_right_2_move = new System.Windows.Forms.Label();
            this.panel_break_left_2_move = new System.Windows.Forms.Panel();
            this.lb_break_left_2_move = new System.Windows.Forms.Label();
            this.tbox_break_right_y = new System.Windows.Forms.TextBox();
            this.tbox_break_left_y = new System.Windows.Forms.TextBox();
            this.lb_break_right_y = new System.Windows.Forms.Label();
            this.panel_table2_alignmark2_calc = new System.Windows.Forms.Panel();
            this.lb_table2_alignmark2_calc = new System.Windows.Forms.Label();
            this.panel_break_right_2_read = new System.Windows.Forms.Panel();
            this.lb_break_right_2_read = new System.Windows.Forms.Label();
            this.lb_break_left_y = new System.Windows.Forms.Label();
            this.tbox_break_right_2_2 = new System.Windows.Forms.TextBox();
            this.panel4_break_left_2_read = new System.Windows.Forms.Panel();
            this.lb_break_left_2_read = new System.Windows.Forms.Label();
            this.panel_break_right_1_move = new System.Windows.Forms.Panel();
            this.lb_break_right_1_move = new System.Windows.Forms.Label();
            this.tbox_break_left_2_2 = new System.Windows.Forms.TextBox();
            this.tbox_break_right_2_1 = new System.Windows.Forms.TextBox();
            this.panel_break_left_1_move = new System.Windows.Forms.Panel();
            this.lb_break_left_1_move = new System.Windows.Forms.Label();
            this.lb_break_right_2 = new System.Windows.Forms.Label();
            this.tbox_break_left_2_1 = new System.Windows.Forms.TextBox();
            this.panel_break_right_1_read = new System.Windows.Forms.Panel();
            this.lb_break_right_1_read = new System.Windows.Forms.Label();
            this.tbox_break_right_1_2 = new System.Windows.Forms.TextBox();
            this.panel_table2_alignmark1_calc = new System.Windows.Forms.Panel();
            this.lb_table2_alignmark1_calc = new System.Windows.Forms.Label();
            this.tbox_break_right_1_1 = new System.Windows.Forms.TextBox();
            this.lb_break_left_2 = new System.Windows.Forms.Label();
            this.lb_break_right_1 = new System.Windows.Forms.Label();
            this.panel_break_left_1_read = new System.Windows.Forms.Panel();
            this.lb_break_left_1_read = new System.Windows.Forms.Label();
            this.tbox_break_left_1_2 = new System.Windows.Forms.TextBox();
            this.panel_table2_alignmark2_move = new System.Windows.Forms.Panel();
            this.lb_table2_alignmark2_move = new System.Windows.Forms.Label();
            this.tbox_break_left_1_1 = new System.Windows.Forms.TextBox();
            this.panel_table1_alignmark2_calc = new System.Windows.Forms.Panel();
            this.lb_table1_alignmark2_calc = new System.Windows.Forms.Label();
            this.lb_break_left_1 = new System.Windows.Forms.Label();
            this.panel_table2_alignmark2_read = new System.Windows.Forms.Panel();
            this.lb_table2_alignmark2_read = new System.Windows.Forms.Label();
            this.panel_table2_alignmark1_move = new System.Windows.Forms.Panel();
            this.lb_table2_alignmark1_move = new System.Windows.Forms.Label();
            this.tbox_table2_alignmark2_2 = new System.Windows.Forms.TextBox();
            this.panel_table2_alignmark1_read = new System.Windows.Forms.Panel();
            this.lb_table2_alignmark1_read = new System.Windows.Forms.Label();
            this.tbox_table2_alignmark2_1 = new System.Windows.Forms.TextBox();
            this.lb_table2_alignmark2 = new System.Windows.Forms.Label();
            this.panel_table1_alignmark1_calc = new System.Windows.Forms.Panel();
            this.lb_table1_alignmark1_calc = new System.Windows.Forms.Label();
            this.tbox_table2_alignmark1_2 = new System.Windows.Forms.TextBox();
            this.panel_table1_alignmark2_move = new System.Windows.Forms.Panel();
            this.lb_table1_alignmark2_move = new System.Windows.Forms.Label();
            this.tbox_table2_alignmark1_1 = new System.Windows.Forms.TextBox();
            this.panel_table1_alignmark1_move = new System.Windows.Forms.Panel();
            this.lb_table1_alignmark1_move = new System.Windows.Forms.Label();
            this.lb_table2_alignmark1 = new System.Windows.Forms.Label();
            this.panel_table1_alignmark2_read = new System.Windows.Forms.Panel();
            this.lb_table1_alignmark2_read = new System.Windows.Forms.Label();
            this.tbox_table1_alignmark2_2 = new System.Windows.Forms.TextBox();
            this.panel_table1_alignmark1_read = new System.Windows.Forms.Panel();
            this.lb_table1_alignmark1_read = new System.Windows.Forms.Label();
            this.tbox_table1_alignmark2_1 = new System.Windows.Forms.TextBox();
            this.tbox_table1_alignmark1_2 = new System.Windows.Forms.TextBox();
            this.lb_table1_alignmark2 = new System.Windows.Forms.Label();
            this.tbox_table1_alignmark1_1 = new System.Windows.Forms.TextBox();
            this.lb_table1_alignmark1 = new System.Windows.Forms.Label();
            this.panel_align = new System.Windows.Forms.Panel();
            this.lb_align = new System.Windows.Forms.Label();
            this.panel_recipe_load = new System.Windows.Forms.Panel();
            this.lb_recipe_load = new System.Windows.Forms.Label();
            this.tbox_recipe_path = new System.Windows.Forms.TextBox();
            this.tbox_recipe = new System.Windows.Forms.TextBox();
            this.lb_recipe = new System.Windows.Forms.Label();
            this.gbox_y_offset = new System.Windows.Forms.GroupBox();
            this.tbox_y_offset_break = new System.Windows.Forms.TextBox();
            this.lb_y_offset_break = new System.Windows.Forms.Label();
            this.tbox_y_offset_pre = new System.Windows.Forms.TextBox();
            this.lb_y_offset_pre = new System.Windows.Forms.Label();
            this.gbox_lds_break = new System.Windows.Forms.GroupBox();
            this.panel_lds_break4_move = new System.Windows.Forms.Panel();
            this.lb_lds_break4_move = new System.Windows.Forms.Label();
            this.tbox_lds_break4_2 = new System.Windows.Forms.TextBox();
            this.tbox_lds_break4_1 = new System.Windows.Forms.TextBox();
            this.panel_lds_break4_read = new System.Windows.Forms.Panel();
            this.lb_lds_break4_read = new System.Windows.Forms.Label();
            this.panel_lds_break3_move = new System.Windows.Forms.Panel();
            this.lb_lds_break3_move = new System.Windows.Forms.Label();
            this.tbox_lds_break3_2 = new System.Windows.Forms.TextBox();
            this.tbox_lds_break3_1 = new System.Windows.Forms.TextBox();
            this.panel_lds_break3_read = new System.Windows.Forms.Panel();
            this.lb_lds_break3_read = new System.Windows.Forms.Label();
            this.panel_lds_break2_move = new System.Windows.Forms.Panel();
            this.lb_lds_break2_move = new System.Windows.Forms.Label();
            this.panel_lds_break1_move = new System.Windows.Forms.Panel();
            this.lb_lds_break1_move = new System.Windows.Forms.Label();
            this.tbox_lds_break2_2 = new System.Windows.Forms.TextBox();
            this.tbox_lds_break2_1 = new System.Windows.Forms.TextBox();
            this.tbox_lds_break1_2 = new System.Windows.Forms.TextBox();
            this.panel_lds_break2_read = new System.Windows.Forms.Panel();
            this.lb_lds_break2_read = new System.Windows.Forms.Label();
            this.lb_lds_break4 = new System.Windows.Forms.Label();
            this.lb_lds_break3 = new System.Windows.Forms.Label();
            this.lb_lds_break2 = new System.Windows.Forms.Label();
            this.tbox_lds_break1_1 = new System.Windows.Forms.TextBox();
            this.panel_lds_break1_read = new System.Windows.Forms.Panel();
            this.lb_lds_break1_read = new System.Windows.Forms.Label();
            this.lb_lds_break1 = new System.Windows.Forms.Label();
            this.panel19 = new System.Windows.Forms.Panel();
            this.btn_recipe_cim_delete = new System.Windows.Forms.Button();
            this.btn_recipe_cim_apply = new System.Windows.Forms.Button();
            this.btn_recipe_delete = new System.Windows.Forms.Button();
            this.btn_recipe_apply = new System.Windows.Forms.Button();
            this.btn_recipe_save = new System.Windows.Forms.Button();
            this.btn_recipe_copy = new System.Windows.Forms.Button();
            this.btn_recipe_make = new System.Windows.Forms.Button();
            this.gbox_lds_process = new System.Windows.Forms.GroupBox();
            this.panel24 = new System.Windows.Forms.Panel();
            this.label37 = new System.Windows.Forms.Label();
            this.tbox_lds_process4_2 = new System.Windows.Forms.TextBox();
            this.tbox_lds_process4_1 = new System.Windows.Forms.TextBox();
            this.panel_lds_process4_read = new System.Windows.Forms.Panel();
            this.label38 = new System.Windows.Forms.Label();
            this.panel_lds_process3_move = new System.Windows.Forms.Panel();
            this.lb_lds_process3_move = new System.Windows.Forms.Label();
            this.tbox_lds_process3_2 = new System.Windows.Forms.TextBox();
            this.tbox_lds_process3_1 = new System.Windows.Forms.TextBox();
            this.panel_lds_process3_read = new System.Windows.Forms.Panel();
            this.lb_lds_process3_read = new System.Windows.Forms.Label();
            this.panel_lds_process2_move = new System.Windows.Forms.Panel();
            this.lb_lds_process2_move = new System.Windows.Forms.Label();
            this.panel_lds_process1_move = new System.Windows.Forms.Panel();
            this.lb_lds_process1_move = new System.Windows.Forms.Label();
            this.tbox_lds_process2_2 = new System.Windows.Forms.TextBox();
            this.tbox_lds_process2_1 = new System.Windows.Forms.TextBox();
            this.tbox_lds_process1_2 = new System.Windows.Forms.TextBox();
            this.panel_lds_process2_read = new System.Windows.Forms.Panel();
            this.lb_lds_process2_read = new System.Windows.Forms.Label();
            this.lb_lds_process4 = new System.Windows.Forms.Label();
            this.lb_lds_process3 = new System.Windows.Forms.Label();
            this.lb_lds_process2 = new System.Windows.Forms.Label();
            this.tbox_lds_process1_1 = new System.Windows.Forms.TextBox();
            this.panel_lds_process1_read = new System.Windows.Forms.Panel();
            this.lb_lds_process1_read = new System.Windows.Forms.Label();
            this.lb_lds_process1 = new System.Windows.Forms.Label();
            this.gbox_picker = new System.Windows.Forms.GroupBox();
            this.panel_picker_cw180_2 = new System.Windows.Forms.Panel();
            this.lb_picker_cw180_2 = new System.Windows.Forms.Label();
            this.panel_picker_cw180_1 = new System.Windows.Forms.Panel();
            this.lb_picker_cw180_1 = new System.Windows.Forms.Label();
            this.panel_picker_ccw90_2 = new System.Windows.Forms.Panel();
            this.lb_picker_ccw90_2 = new System.Windows.Forms.Label();
            this.panel_picker_ccw90_1 = new System.Windows.Forms.Panel();
            this.lb_picker_ccw90_1 = new System.Windows.Forms.Label();
            this.panel_picker_cw90_2 = new System.Windows.Forms.Panel();
            this.lb_picker_cw90_2 = new System.Windows.Forms.Label();
            this.panel_picker_cw90_1 = new System.Windows.Forms.Panel();
            this.lb_picker_cw90_1 = new System.Windows.Forms.Label();
            this.panel_picker_normal_2 = new System.Windows.Forms.Panel();
            this.lb_picker_normal_2 = new System.Windows.Forms.Label();
            this.panel_picker_norma_l1 = new System.Windows.Forms.Panel();
            this.lb_picker_normal_1 = new System.Windows.Forms.Label();
            this.gbox_cassette = new System.Windows.Forms.GroupBox();
            this.panel_cassette4 = new System.Windows.Forms.Panel();
            this.lb_cassette4 = new System.Windows.Forms.Label();
            this.panel_cassette2 = new System.Windows.Forms.Panel();
            this.lb_cassette2 = new System.Windows.Forms.Label();
            this.panel_cassette1 = new System.Windows.Forms.Panel();
            this.lb_cassette1 = new System.Windows.Forms.Label();
            this.panel_cassette_reverse = new System.Windows.Forms.Panel();
            this.lb_cassette_reverse = new System.Windows.Forms.Label();
            this.tbox_cell_count = new System.Windows.Forms.TextBox();
            this.lb_cell_count = new System.Windows.Forms.Label();
            this.tbox_max_height_count = new System.Windows.Forms.TextBox();
            this.lb_max_height_count = new System.Windows.Forms.Label();
            this.tbox_height = new System.Windows.Forms.TextBox();
            this.lb_height = new System.Windows.Forms.Label();
            this.tbox_width = new System.Windows.Forms.TextBox();
            this.panel_cassette_normal = new System.Windows.Forms.Panel();
            this.lb_cassette_normal = new System.Windows.Forms.Label();
            this.lb_width = new System.Windows.Forms.Label();
            this.listView2 = new System.Windows.Forms.ListView();
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lv_recipe = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.gbox_layerinfo.SuspendLayout();
            this.panel_dxfpath_load.SuspendLayout();
            this.gbox_item.SuspendLayout();
            this.panel_z_matrix.SuspendLayout();
            this.panel53.SuspendLayout();
            this.panel_break_left_y_calc.SuspendLayout();
            this.panel_break_right_2_calc.SuspendLayout();
            this.panel_break_left_2_calc.SuspendLayout();
            this.panel55.SuspendLayout();
            this.panel_break_left_y_move.SuspendLayout();
            this.panel56.SuspendLayout();
            this.panel_break_left_y_read.SuspendLayout();
            this.panel_break_right_1_calc.SuspendLayout();
            this.panel_break_left_1_calc.SuspendLayout();
            this.panel_break_right_2_move.SuspendLayout();
            this.panel_break_left_2_move.SuspendLayout();
            this.panel_table2_alignmark2_calc.SuspendLayout();
            this.panel_break_right_2_read.SuspendLayout();
            this.panel4_break_left_2_read.SuspendLayout();
            this.panel_break_right_1_move.SuspendLayout();
            this.panel_break_left_1_move.SuspendLayout();
            this.panel_break_right_1_read.SuspendLayout();
            this.panel_table2_alignmark1_calc.SuspendLayout();
            this.panel_break_left_1_read.SuspendLayout();
            this.panel_table2_alignmark2_move.SuspendLayout();
            this.panel_table1_alignmark2_calc.SuspendLayout();
            this.panel_table2_alignmark2_read.SuspendLayout();
            this.panel_table2_alignmark1_move.SuspendLayout();
            this.panel_table2_alignmark1_read.SuspendLayout();
            this.panel_table1_alignmark1_calc.SuspendLayout();
            this.panel_table1_alignmark2_move.SuspendLayout();
            this.panel_table1_alignmark1_move.SuspendLayout();
            this.panel_table1_alignmark2_read.SuspendLayout();
            this.panel_table1_alignmark1_read.SuspendLayout();
            this.panel_align.SuspendLayout();
            this.panel_recipe_load.SuspendLayout();
            this.gbox_y_offset.SuspendLayout();
            this.gbox_lds_break.SuspendLayout();
            this.panel_lds_break4_move.SuspendLayout();
            this.panel_lds_break4_read.SuspendLayout();
            this.panel_lds_break3_move.SuspendLayout();
            this.panel_lds_break3_read.SuspendLayout();
            this.panel_lds_break2_move.SuspendLayout();
            this.panel_lds_break1_move.SuspendLayout();
            this.panel_lds_break2_read.SuspendLayout();
            this.panel_lds_break1_read.SuspendLayout();
            this.panel19.SuspendLayout();
            this.gbox_lds_process.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel_lds_process4_read.SuspendLayout();
            this.panel_lds_process3_move.SuspendLayout();
            this.panel_lds_process3_read.SuspendLayout();
            this.panel_lds_process2_move.SuspendLayout();
            this.panel_lds_process1_move.SuspendLayout();
            this.panel_lds_process2_read.SuspendLayout();
            this.panel_lds_process1_read.SuspendLayout();
            this.gbox_picker.SuspendLayout();
            this.panel_picker_cw180_2.SuspendLayout();
            this.panel_picker_cw180_1.SuspendLayout();
            this.panel_picker_ccw90_2.SuspendLayout();
            this.panel_picker_ccw90_1.SuspendLayout();
            this.panel_picker_cw90_2.SuspendLayout();
            this.panel_picker_cw90_1.SuspendLayout();
            this.panel_picker_normal_2.SuspendLayout();
            this.panel_picker_norma_l1.SuspendLayout();
            this.gbox_cassette.SuspendLayout();
            this.panel_cassette4.SuspendLayout();
            this.panel_cassette2.SuspendLayout();
            this.panel_cassette1.SuspendLayout();
            this.panel_cassette_reverse.SuspendLayout();
            this.panel_cassette_normal.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.Color.DimGray;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.splitContainer1.Panel1.Controls.Add(this.gbox_dxfinfo);
            this.splitContainer1.Panel1.Controls.Add(this.gbox_layerinfo);
            this.splitContainer1.Panel1.Controls.Add(this.gbox_item);
            this.splitContainer1.Panel1.Controls.Add(this.gbox_y_offset);
            this.splitContainer1.Panel1.Controls.Add(this.gbox_lds_break);
            this.splitContainer1.Panel1.Controls.Add(this.panel19);
            this.splitContainer1.Panel1.Controls.Add(this.gbox_lds_process);
            this.splitContainer1.Panel1.Controls.Add(this.gbox_picker);
            this.splitContainer1.Panel1.Controls.Add(this.gbox_cassette);
            this.splitContainer1.Panel1.Controls.Add(this.listView2);
            this.splitContainer1.Panel1.Controls.Add(this.lv_recipe);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.DarkSlateGray;
            this.splitContainer1.Panel2Collapsed = true;
            this.splitContainer1.Size = new System.Drawing.Size(1740, 860);
            this.splitContainer1.SplitterDistance = 835;
            this.splitContainer1.TabIndex = 0;
            // 
            // gbox_dxfinfo
            // 
            this.gbox_dxfinfo.BackColor = System.Drawing.Color.Transparent;
            this.gbox_dxfinfo.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.gbox_dxfinfo.ForeColor = System.Drawing.Color.White;
            this.gbox_dxfinfo.Location = new System.Drawing.Point(284, 571);
            this.gbox_dxfinfo.Name = "gbox_dxfinfo";
            this.gbox_dxfinfo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gbox_dxfinfo.Size = new System.Drawing.Size(1104, 209);
            this.gbox_dxfinfo.TabIndex = 69;
            this.gbox_dxfinfo.TabStop = false;
            this.gbox_dxfinfo.Text = "DxfInfo";
            // 
            // gbox_layerinfo
            // 
            this.gbox_layerinfo.BackColor = System.Drawing.Color.Transparent;
            this.gbox_layerinfo.Controls.Add(this.lb_ta_thickness);
            this.gbox_layerinfo.Controls.Add(this.tbox_ta_thickness);
            this.gbox_layerinfo.Controls.Add(this.combox_cell_type);
            this.gbox_layerinfo.Controls.Add(this.lb_cell_type);
            this.gbox_layerinfo.Controls.Add(this.combox_process_param);
            this.gbox_layerinfo.Controls.Add(this.lb_process_param);
            this.gbox_layerinfo.Controls.Add(this.combox_guide_layer);
            this.gbox_layerinfo.Controls.Add(this.lb_guide_layer);
            this.gbox_layerinfo.Controls.Add(this.combox_process_layer);
            this.gbox_layerinfo.Controls.Add(this.lb_process_layer);
            this.gbox_layerinfo.Controls.Add(this.combox_alignment_layer);
            this.gbox_layerinfo.Controls.Add(this.lb_alignment_layer);
            this.gbox_layerinfo.Controls.Add(this.panel_dxfpath_load);
            this.gbox_layerinfo.Controls.Add(this.tbox_dxfpath);
            this.gbox_layerinfo.Controls.Add(this.lb_dxfpath);
            this.gbox_layerinfo.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.gbox_layerinfo.ForeColor = System.Drawing.Color.White;
            this.gbox_layerinfo.Location = new System.Drawing.Point(284, 358);
            this.gbox_layerinfo.Name = "gbox_layerinfo";
            this.gbox_layerinfo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gbox_layerinfo.Size = new System.Drawing.Size(1104, 170);
            this.gbox_layerinfo.TabIndex = 68;
            this.gbox_layerinfo.TabStop = false;
            this.gbox_layerinfo.Text = "LayerInfo";
            // 
            // lb_ta_thickness
            // 
            this.lb_ta_thickness.AutoSize = true;
            this.lb_ta_thickness.Font = new System.Drawing.Font("맑은 고딕", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_ta_thickness.Location = new System.Drawing.Point(6, 129);
            this.lb_ta_thickness.Name = "lb_ta_thickness";
            this.lb_ta_thickness.Size = new System.Drawing.Size(113, 15);
            this.lb_ta_thickness.TabIndex = 75;
            this.lb_ta_thickness.Text = "TA/Thickness[mm]";
            // 
            // tbox_ta_thickness
            // 
            this.tbox_ta_thickness.Location = new System.Drawing.Point(138, 123);
            this.tbox_ta_thickness.Name = "tbox_ta_thickness";
            this.tbox_ta_thickness.Size = new System.Drawing.Size(121, 29);
            this.tbox_ta_thickness.TabIndex = 65;
            // 
            // combox_cell_type
            // 
            this.combox_cell_type.FormattingEnabled = true;
            this.combox_cell_type.Location = new System.Drawing.Point(377, 90);
            this.combox_cell_type.Name = "combox_cell_type";
            this.combox_cell_type.Size = new System.Drawing.Size(121, 29);
            this.combox_cell_type.TabIndex = 74;
            // 
            // lb_cell_type
            // 
            this.lb_cell_type.AutoSize = true;
            this.lb_cell_type.Font = new System.Drawing.Font("맑은 고딕", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_cell_type.Location = new System.Drawing.Point(273, 97);
            this.lb_cell_type.Name = "lb_cell_type";
            this.lb_cell_type.Size = new System.Drawing.Size(60, 15);
            this.lb_cell_type.TabIndex = 73;
            this.lb_cell_type.Text = "Cell Type";
            // 
            // combox_process_param
            // 
            this.combox_process_param.FormattingEnabled = true;
            this.combox_process_param.Location = new System.Drawing.Point(138, 90);
            this.combox_process_param.Name = "combox_process_param";
            this.combox_process_param.Size = new System.Drawing.Size(121, 29);
            this.combox_process_param.TabIndex = 72;
            // 
            // lb_process_param
            // 
            this.lb_process_param.AutoSize = true;
            this.lb_process_param.Font = new System.Drawing.Font("맑은 고딕", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_process_param.Location = new System.Drawing.Point(6, 97);
            this.lb_process_param.Name = "lb_process_param";
            this.lb_process_param.Size = new System.Drawing.Size(92, 15);
            this.lb_process_param.TabIndex = 71;
            this.lb_process_param.Text = "Process Param";
            // 
            // combox_guide_layer
            // 
            this.combox_guide_layer.FormattingEnabled = true;
            this.combox_guide_layer.Location = new System.Drawing.Point(613, 57);
            this.combox_guide_layer.Name = "combox_guide_layer";
            this.combox_guide_layer.Size = new System.Drawing.Size(121, 29);
            this.combox_guide_layer.TabIndex = 70;
            // 
            // lb_guide_layer
            // 
            this.lb_guide_layer.AutoSize = true;
            this.lb_guide_layer.Font = new System.Drawing.Font("맑은 고딕", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_guide_layer.Location = new System.Drawing.Point(523, 64);
            this.lb_guide_layer.Name = "lb_guide_layer";
            this.lb_guide_layer.Size = new System.Drawing.Size(76, 15);
            this.lb_guide_layer.TabIndex = 69;
            this.lb_guide_layer.Text = "Guide Layer";
            // 
            // combox_process_layer
            // 
            this.combox_process_layer.FormattingEnabled = true;
            this.combox_process_layer.Location = new System.Drawing.Point(377, 57);
            this.combox_process_layer.Name = "combox_process_layer";
            this.combox_process_layer.Size = new System.Drawing.Size(121, 29);
            this.combox_process_layer.TabIndex = 68;
            // 
            // lb_process_layer
            // 
            this.lb_process_layer.AutoSize = true;
            this.lb_process_layer.Font = new System.Drawing.Font("맑은 고딕", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_process_layer.Location = new System.Drawing.Point(273, 64);
            this.lb_process_layer.Name = "lb_process_layer";
            this.lb_process_layer.Size = new System.Drawing.Size(86, 15);
            this.lb_process_layer.TabIndex = 67;
            this.lb_process_layer.Text = "Process Layer";
            // 
            // combox_alignment_layer
            // 
            this.combox_alignment_layer.FormattingEnabled = true;
            this.combox_alignment_layer.Location = new System.Drawing.Point(138, 57);
            this.combox_alignment_layer.Name = "combox_alignment_layer";
            this.combox_alignment_layer.Size = new System.Drawing.Size(121, 29);
            this.combox_alignment_layer.TabIndex = 66;
            // 
            // lb_alignment_layer
            // 
            this.lb_alignment_layer.AutoSize = true;
            this.lb_alignment_layer.Font = new System.Drawing.Font("맑은 고딕", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_alignment_layer.Location = new System.Drawing.Point(6, 64);
            this.lb_alignment_layer.Name = "lb_alignment_layer";
            this.lb_alignment_layer.Size = new System.Drawing.Size(102, 15);
            this.lb_alignment_layer.TabIndex = 65;
            this.lb_alignment_layer.Text = "AlignMent Layer";
            // 
            // panel_dxfpath_load
            // 
            this.panel_dxfpath_load.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_dxfpath_load.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_dxfpath_load.Controls.Add(this.lb_dxfpath_load);
            this.panel_dxfpath_load.ForeColor = System.Drawing.Color.White;
            this.panel_dxfpath_load.Location = new System.Drawing.Point(825, 25);
            this.panel_dxfpath_load.Name = "panel_dxfpath_load";
            this.panel_dxfpath_load.Size = new System.Drawing.Size(273, 26);
            this.panel_dxfpath_load.TabIndex = 19;
            // 
            // lb_dxfpath_load
            // 
            this.lb_dxfpath_load.AutoSize = true;
            this.lb_dxfpath_load.BackColor = System.Drawing.Color.Transparent;
            this.lb_dxfpath_load.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_dxfpath_load.ForeColor = System.Drawing.Color.White;
            this.lb_dxfpath_load.Location = new System.Drawing.Point(110, 2);
            this.lb_dxfpath_load.Name = "lb_dxfpath_load";
            this.lb_dxfpath_load.Size = new System.Drawing.Size(69, 20);
            this.lb_dxfpath_load.TabIndex = 4;
            this.lb_dxfpath_load.Text = "불러오기";
            // 
            // tbox_dxfpath
            // 
            this.tbox_dxfpath.Location = new System.Drawing.Point(138, 25);
            this.tbox_dxfpath.Name = "tbox_dxfpath";
            this.tbox_dxfpath.ReadOnly = true;
            this.tbox_dxfpath.Size = new System.Drawing.Size(681, 29);
            this.tbox_dxfpath.TabIndex = 5;
            // 
            // lb_dxfpath
            // 
            this.lb_dxfpath.AutoSize = true;
            this.lb_dxfpath.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_dxfpath.Location = new System.Drawing.Point(6, 29);
            this.lb_dxfpath.Name = "lb_dxfpath";
            this.lb_dxfpath.Size = new System.Drawing.Size(66, 20);
            this.lb_dxfpath.TabIndex = 4;
            this.lb_dxfpath.Text = "DxfPath";
            // 
            // gbox_item
            // 
            this.gbox_item.BackColor = System.Drawing.Color.Transparent;
            this.gbox_item.Controls.Add(this.tbox_cell_size_2);
            this.gbox_item.Controls.Add(this.tbox_cell_size_1);
            this.gbox_item.Controls.Add(this.lb_cell_size);
            this.gbox_item.Controls.Add(this.panel_z_matrix);
            this.gbox_item.Controls.Add(this.lb_a_matrix);
            this.gbox_item.Controls.Add(this.panel53);
            this.gbox_item.Controls.Add(this.panel_break_left_y_calc);
            this.gbox_item.Controls.Add(this.panel_break_right_2_calc);
            this.gbox_item.Controls.Add(this.panel_break_left_2_calc);
            this.gbox_item.Controls.Add(this.panel55);
            this.gbox_item.Controls.Add(this.panel_break_left_y_move);
            this.gbox_item.Controls.Add(this.panel56);
            this.gbox_item.Controls.Add(this.panel_break_left_y_read);
            this.gbox_item.Controls.Add(this.panel_break_right_1_calc);
            this.gbox_item.Controls.Add(this.panel_break_left_1_calc);
            this.gbox_item.Controls.Add(this.panel_break_right_2_move);
            this.gbox_item.Controls.Add(this.panel_break_left_2_move);
            this.gbox_item.Controls.Add(this.tbox_break_right_y);
            this.gbox_item.Controls.Add(this.tbox_break_left_y);
            this.gbox_item.Controls.Add(this.lb_break_right_y);
            this.gbox_item.Controls.Add(this.panel_table2_alignmark2_calc);
            this.gbox_item.Controls.Add(this.panel_break_right_2_read);
            this.gbox_item.Controls.Add(this.lb_break_left_y);
            this.gbox_item.Controls.Add(this.tbox_break_right_2_2);
            this.gbox_item.Controls.Add(this.panel4_break_left_2_read);
            this.gbox_item.Controls.Add(this.panel_break_right_1_move);
            this.gbox_item.Controls.Add(this.tbox_break_left_2_2);
            this.gbox_item.Controls.Add(this.tbox_break_right_2_1);
            this.gbox_item.Controls.Add(this.panel_break_left_1_move);
            this.gbox_item.Controls.Add(this.lb_break_right_2);
            this.gbox_item.Controls.Add(this.tbox_break_left_2_1);
            this.gbox_item.Controls.Add(this.panel_break_right_1_read);
            this.gbox_item.Controls.Add(this.tbox_break_right_1_2);
            this.gbox_item.Controls.Add(this.panel_table2_alignmark1_calc);
            this.gbox_item.Controls.Add(this.tbox_break_right_1_1);
            this.gbox_item.Controls.Add(this.lb_break_left_2);
            this.gbox_item.Controls.Add(this.lb_break_right_1);
            this.gbox_item.Controls.Add(this.panel_break_left_1_read);
            this.gbox_item.Controls.Add(this.tbox_break_left_1_2);
            this.gbox_item.Controls.Add(this.panel_table2_alignmark2_move);
            this.gbox_item.Controls.Add(this.tbox_break_left_1_1);
            this.gbox_item.Controls.Add(this.panel_table1_alignmark2_calc);
            this.gbox_item.Controls.Add(this.lb_break_left_1);
            this.gbox_item.Controls.Add(this.panel_table2_alignmark2_read);
            this.gbox_item.Controls.Add(this.panel_table2_alignmark1_move);
            this.gbox_item.Controls.Add(this.tbox_table2_alignmark2_2);
            this.gbox_item.Controls.Add(this.panel_table2_alignmark1_read);
            this.gbox_item.Controls.Add(this.tbox_table2_alignmark2_1);
            this.gbox_item.Controls.Add(this.lb_table2_alignmark2);
            this.gbox_item.Controls.Add(this.panel_table1_alignmark1_calc);
            this.gbox_item.Controls.Add(this.tbox_table2_alignmark1_2);
            this.gbox_item.Controls.Add(this.panel_table1_alignmark2_move);
            this.gbox_item.Controls.Add(this.tbox_table2_alignmark1_1);
            this.gbox_item.Controls.Add(this.panel_table1_alignmark1_move);
            this.gbox_item.Controls.Add(this.lb_table2_alignmark1);
            this.gbox_item.Controls.Add(this.panel_table1_alignmark2_read);
            this.gbox_item.Controls.Add(this.tbox_table1_alignmark2_2);
            this.gbox_item.Controls.Add(this.panel_table1_alignmark1_read);
            this.gbox_item.Controls.Add(this.tbox_table1_alignmark2_1);
            this.gbox_item.Controls.Add(this.tbox_table1_alignmark1_2);
            this.gbox_item.Controls.Add(this.lb_table1_alignmark2);
            this.gbox_item.Controls.Add(this.tbox_table1_alignmark1_1);
            this.gbox_item.Controls.Add(this.lb_table1_alignmark1);
            this.gbox_item.Controls.Add(this.panel_align);
            this.gbox_item.Controls.Add(this.panel_recipe_load);
            this.gbox_item.Controls.Add(this.tbox_recipe_path);
            this.gbox_item.Controls.Add(this.tbox_recipe);
            this.gbox_item.Controls.Add(this.lb_recipe);
            this.gbox_item.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_item.ForeColor = System.Drawing.Color.White;
            this.gbox_item.Location = new System.Drawing.Point(284, 3);
            this.gbox_item.Name = "gbox_item";
            this.gbox_item.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gbox_item.Size = new System.Drawing.Size(1104, 346);
            this.gbox_item.TabIndex = 67;
            this.gbox_item.TabStop = false;
            this.gbox_item.Text = "Item";
            // 
            // tbox_cell_size_2
            // 
            this.tbox_cell_size_2.Location = new System.Drawing.Point(385, 308);
            this.tbox_cell_size_2.Name = "tbox_cell_size_2";
            this.tbox_cell_size_2.Size = new System.Drawing.Size(66, 26);
            this.tbox_cell_size_2.TabIndex = 64;
            // 
            // tbox_cell_size_1
            // 
            this.tbox_cell_size_1.Location = new System.Drawing.Point(313, 308);
            this.tbox_cell_size_1.Name = "tbox_cell_size_1";
            this.tbox_cell_size_1.Size = new System.Drawing.Size(66, 26);
            this.tbox_cell_size_1.TabIndex = 63;
            // 
            // lb_cell_size
            // 
            this.lb_cell_size.AutoSize = true;
            this.lb_cell_size.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.999999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_cell_size.Location = new System.Drawing.Point(243, 316);
            this.lb_cell_size.Name = "lb_cell_size";
            this.lb_cell_size.Size = new System.Drawing.Size(67, 15);
            this.lb_cell_size.TabIndex = 62;
            this.lb_cell_size.Text = "Size[mm]";
            // 
            // panel_z_matrix
            // 
            this.panel_z_matrix.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_z_matrix.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_z_matrix.Controls.Add(this.lb_z_matrix);
            this.panel_z_matrix.Location = new System.Drawing.Point(155, 309);
            this.panel_z_matrix.Name = "panel_z_matrix";
            this.panel_z_matrix.Size = new System.Drawing.Size(73, 26);
            this.panel_z_matrix.TabIndex = 57;
            // 
            // lb_z_matrix
            // 
            this.lb_z_matrix.AutoSize = true;
            this.lb_z_matrix.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_z_matrix.Location = new System.Drawing.Point(8, 4);
            this.lb_z_matrix.Name = "lb_z_matrix";
            this.lb_z_matrix.Size = new System.Drawing.Size(59, 16);
            this.lb_z_matrix.TabIndex = 0;
            this.lb_z_matrix.Text = "Z-Mat...";
            // 
            // lb_a_matrix
            // 
            this.lb_a_matrix.AutoSize = true;
            this.lb_a_matrix.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.999999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_a_matrix.Location = new System.Drawing.Point(6, 316);
            this.lb_a_matrix.Name = "lb_a_matrix";
            this.lb_a_matrix.Size = new System.Drawing.Size(60, 15);
            this.lb_a_matrix.TabIndex = 61;
            this.lb_a_matrix.Text = "A-Matrix";
            // 
            // panel53
            // 
            this.panel53.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel53.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel53.Controls.Add(this.label74);
            this.panel53.Location = new System.Drawing.Point(498, 262);
            this.panel53.Name = "panel53";
            this.panel53.Size = new System.Drawing.Size(50, 26);
            this.panel53.TabIndex = 60;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.label74.Location = new System.Drawing.Point(4, 4);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(40, 17);
            this.label74.TabIndex = 0;
            this.label74.Text = "CALC";
            // 
            // panel_break_left_y_calc
            // 
            this.panel_break_left_y_calc.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_break_left_y_calc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_break_left_y_calc.Controls.Add(this.lb_break_left_y_calc);
            this.panel_break_left_y_calc.Location = new System.Drawing.Point(498, 198);
            this.panel_break_left_y_calc.Name = "panel_break_left_y_calc";
            this.panel_break_left_y_calc.Size = new System.Drawing.Size(50, 26);
            this.panel_break_left_y_calc.TabIndex = 43;
            // 
            // lb_break_left_y_calc
            // 
            this.lb_break_left_y_calc.AutoSize = true;
            this.lb_break_left_y_calc.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_break_left_y_calc.Location = new System.Drawing.Point(4, 4);
            this.lb_break_left_y_calc.Name = "lb_break_left_y_calc";
            this.lb_break_left_y_calc.Size = new System.Drawing.Size(40, 17);
            this.lb_break_left_y_calc.TabIndex = 0;
            this.lb_break_left_y_calc.Text = "CALC";
            // 
            // panel_break_right_2_calc
            // 
            this.panel_break_right_2_calc.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_break_right_2_calc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_break_right_2_calc.Controls.Add(this.lb_break_right_2_calc);
            this.panel_break_right_2_calc.Location = new System.Drawing.Point(1048, 230);
            this.panel_break_right_2_calc.Name = "panel_break_right_2_calc";
            this.panel_break_right_2_calc.Size = new System.Drawing.Size(50, 26);
            this.panel_break_right_2_calc.TabIndex = 59;
            // 
            // lb_break_right_2_calc
            // 
            this.lb_break_right_2_calc.AutoSize = true;
            this.lb_break_right_2_calc.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_break_right_2_calc.Location = new System.Drawing.Point(4, 4);
            this.lb_break_right_2_calc.Name = "lb_break_right_2_calc";
            this.lb_break_right_2_calc.Size = new System.Drawing.Size(40, 17);
            this.lb_break_right_2_calc.TabIndex = 0;
            this.lb_break_right_2_calc.Text = "CALC";
            // 
            // panel_break_left_2_calc
            // 
            this.panel_break_left_2_calc.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_break_left_2_calc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_break_left_2_calc.Controls.Add(this.lb_break_left_2_calc);
            this.panel_break_left_2_calc.Location = new System.Drawing.Point(1048, 166);
            this.panel_break_left_2_calc.Name = "panel_break_left_2_calc";
            this.panel_break_left_2_calc.Size = new System.Drawing.Size(50, 26);
            this.panel_break_left_2_calc.TabIndex = 43;
            // 
            // lb_break_left_2_calc
            // 
            this.lb_break_left_2_calc.AutoSize = true;
            this.lb_break_left_2_calc.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_break_left_2_calc.Location = new System.Drawing.Point(4, 4);
            this.lb_break_left_2_calc.Name = "lb_break_left_2_calc";
            this.lb_break_left_2_calc.Size = new System.Drawing.Size(40, 17);
            this.lb_break_left_2_calc.TabIndex = 0;
            this.lb_break_left_2_calc.Text = "CALC";
            // 
            // panel55
            // 
            this.panel55.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel55.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel55.Controls.Add(this.label76);
            this.panel55.Location = new System.Drawing.Point(442, 262);
            this.panel55.Name = "panel55";
            this.panel55.Size = new System.Drawing.Size(50, 26);
            this.panel55.TabIndex = 57;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.label76.Location = new System.Drawing.Point(9, 4);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(34, 17);
            this.label76.TabIndex = 0;
            this.label76.Text = "이동";
            // 
            // panel_break_left_y_move
            // 
            this.panel_break_left_y_move.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_break_left_y_move.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_break_left_y_move.Controls.Add(this.lb_break_left_y_move);
            this.panel_break_left_y_move.Location = new System.Drawing.Point(442, 198);
            this.panel_break_left_y_move.Name = "panel_break_left_y_move";
            this.panel_break_left_y_move.Size = new System.Drawing.Size(50, 26);
            this.panel_break_left_y_move.TabIndex = 42;
            // 
            // lb_break_left_y_move
            // 
            this.lb_break_left_y_move.AutoSize = true;
            this.lb_break_left_y_move.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_break_left_y_move.Location = new System.Drawing.Point(9, 4);
            this.lb_break_left_y_move.Name = "lb_break_left_y_move";
            this.lb_break_left_y_move.Size = new System.Drawing.Size(34, 17);
            this.lb_break_left_y_move.TabIndex = 0;
            this.lb_break_left_y_move.Text = "이동";
            // 
            // panel56
            // 
            this.panel56.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel56.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel56.Controls.Add(this.label77);
            this.panel56.Location = new System.Drawing.Point(386, 262);
            this.panel56.Name = "panel56";
            this.panel56.Size = new System.Drawing.Size(50, 26);
            this.panel56.TabIndex = 56;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.label77.Location = new System.Drawing.Point(9, 4);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(34, 17);
            this.label77.TabIndex = 0;
            this.label77.Text = "읽기";
            // 
            // panel_break_left_y_read
            // 
            this.panel_break_left_y_read.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_break_left_y_read.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_break_left_y_read.Controls.Add(this.lb_break_left_y_read);
            this.panel_break_left_y_read.Location = new System.Drawing.Point(386, 198);
            this.panel_break_left_y_read.Name = "panel_break_left_y_read";
            this.panel_break_left_y_read.Size = new System.Drawing.Size(50, 26);
            this.panel_break_left_y_read.TabIndex = 41;
            // 
            // lb_break_left_y_read
            // 
            this.lb_break_left_y_read.AutoSize = true;
            this.lb_break_left_y_read.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_break_left_y_read.Location = new System.Drawing.Point(9, 4);
            this.lb_break_left_y_read.Name = "lb_break_left_y_read";
            this.lb_break_left_y_read.Size = new System.Drawing.Size(34, 17);
            this.lb_break_left_y_read.TabIndex = 0;
            this.lb_break_left_y_read.Text = "읽기";
            // 
            // panel_break_right_1_calc
            // 
            this.panel_break_right_1_calc.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_break_right_1_calc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_break_right_1_calc.Controls.Add(this.lb_break_right_1_calc);
            this.panel_break_right_1_calc.Location = new System.Drawing.Point(498, 230);
            this.panel_break_right_1_calc.Name = "panel_break_right_1_calc";
            this.panel_break_right_1_calc.Size = new System.Drawing.Size(50, 26);
            this.panel_break_right_1_calc.TabIndex = 49;
            // 
            // lb_break_right_1_calc
            // 
            this.lb_break_right_1_calc.AutoSize = true;
            this.lb_break_right_1_calc.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_break_right_1_calc.Location = new System.Drawing.Point(4, 4);
            this.lb_break_right_1_calc.Name = "lb_break_right_1_calc";
            this.lb_break_right_1_calc.Size = new System.Drawing.Size(40, 17);
            this.lb_break_right_1_calc.TabIndex = 0;
            this.lb_break_right_1_calc.Text = "CALC";
            // 
            // panel_break_left_1_calc
            // 
            this.panel_break_left_1_calc.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_break_left_1_calc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_break_left_1_calc.Controls.Add(this.lb_break_left_1_calc);
            this.panel_break_left_1_calc.Location = new System.Drawing.Point(498, 166);
            this.panel_break_left_1_calc.Name = "panel_break_left_1_calc";
            this.panel_break_left_1_calc.Size = new System.Drawing.Size(50, 26);
            this.panel_break_left_1_calc.TabIndex = 37;
            // 
            // lb_break_left_1_calc
            // 
            this.lb_break_left_1_calc.AutoSize = true;
            this.lb_break_left_1_calc.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_break_left_1_calc.Location = new System.Drawing.Point(4, 4);
            this.lb_break_left_1_calc.Name = "lb_break_left_1_calc";
            this.lb_break_left_1_calc.Size = new System.Drawing.Size(40, 17);
            this.lb_break_left_1_calc.TabIndex = 0;
            this.lb_break_left_1_calc.Text = "CALC";
            // 
            // panel_break_right_2_move
            // 
            this.panel_break_right_2_move.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_break_right_2_move.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_break_right_2_move.Controls.Add(this.lb_break_right_2_move);
            this.panel_break_right_2_move.Location = new System.Drawing.Point(992, 230);
            this.panel_break_right_2_move.Name = "panel_break_right_2_move";
            this.panel_break_right_2_move.Size = new System.Drawing.Size(50, 26);
            this.panel_break_right_2_move.TabIndex = 58;
            // 
            // lb_break_right_2_move
            // 
            this.lb_break_right_2_move.AutoSize = true;
            this.lb_break_right_2_move.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_break_right_2_move.Location = new System.Drawing.Point(9, 4);
            this.lb_break_right_2_move.Name = "lb_break_right_2_move";
            this.lb_break_right_2_move.Size = new System.Drawing.Size(30, 16);
            this.lb_break_right_2_move.TabIndex = 0;
            this.lb_break_right_2_move.Text = "이동";
            // 
            // panel_break_left_2_move
            // 
            this.panel_break_left_2_move.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_break_left_2_move.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_break_left_2_move.Controls.Add(this.lb_break_left_2_move);
            this.panel_break_left_2_move.Location = new System.Drawing.Point(992, 166);
            this.panel_break_left_2_move.Name = "panel_break_left_2_move";
            this.panel_break_left_2_move.Size = new System.Drawing.Size(50, 26);
            this.panel_break_left_2_move.TabIndex = 42;
            // 
            // lb_break_left_2_move
            // 
            this.lb_break_left_2_move.AutoSize = true;
            this.lb_break_left_2_move.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_break_left_2_move.Location = new System.Drawing.Point(9, 4);
            this.lb_break_left_2_move.Name = "lb_break_left_2_move";
            this.lb_break_left_2_move.Size = new System.Drawing.Size(30, 16);
            this.lb_break_left_2_move.TabIndex = 0;
            this.lb_break_left_2_move.Text = "이동";
            // 
            // tbox_break_right_y
            // 
            this.tbox_break_right_y.Location = new System.Drawing.Point(164, 262);
            this.tbox_break_right_y.Name = "tbox_break_right_y";
            this.tbox_break_right_y.Size = new System.Drawing.Size(105, 26);
            this.tbox_break_right_y.TabIndex = 53;
            // 
            // tbox_break_left_y
            // 
            this.tbox_break_left_y.Location = new System.Drawing.Point(164, 198);
            this.tbox_break_left_y.Name = "tbox_break_left_y";
            this.tbox_break_left_y.Size = new System.Drawing.Size(105, 26);
            this.tbox_break_left_y.TabIndex = 39;
            // 
            // lb_break_right_y
            // 
            this.lb_break_right_y.AutoSize = true;
            this.lb_break_right_y.Font = new System.Drawing.Font("맑은 고딕", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_break_right_y.Location = new System.Drawing.Point(6, 269);
            this.lb_break_right_y.Name = "lb_break_right_y";
            this.lb_break_right_y.Size = new System.Drawing.Size(116, 15);
            this.lb_break_right_y.TabIndex = 51;
            this.lb_break_right_y.Text = "Break_right_Y[mm]";
            // 
            // panel_table2_alignmark2_calc
            // 
            this.panel_table2_alignmark2_calc.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_table2_alignmark2_calc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_table2_alignmark2_calc.Controls.Add(this.lb_table2_alignmark2_calc);
            this.panel_table2_alignmark2_calc.Location = new System.Drawing.Point(1048, 134);
            this.panel_table2_alignmark2_calc.Name = "panel_table2_alignmark2_calc";
            this.panel_table2_alignmark2_calc.Size = new System.Drawing.Size(50, 26);
            this.panel_table2_alignmark2_calc.TabIndex = 37;
            // 
            // lb_table2_alignmark2_calc
            // 
            this.lb_table2_alignmark2_calc.AutoSize = true;
            this.lb_table2_alignmark2_calc.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_table2_alignmark2_calc.Location = new System.Drawing.Point(4, 4);
            this.lb_table2_alignmark2_calc.Name = "lb_table2_alignmark2_calc";
            this.lb_table2_alignmark2_calc.Size = new System.Drawing.Size(40, 17);
            this.lb_table2_alignmark2_calc.TabIndex = 0;
            this.lb_table2_alignmark2_calc.Text = "CALC";
            // 
            // panel_break_right_2_read
            // 
            this.panel_break_right_2_read.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_break_right_2_read.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_break_right_2_read.Controls.Add(this.lb_break_right_2_read);
            this.panel_break_right_2_read.Location = new System.Drawing.Point(936, 230);
            this.panel_break_right_2_read.Name = "panel_break_right_2_read";
            this.panel_break_right_2_read.Size = new System.Drawing.Size(50, 26);
            this.panel_break_right_2_read.TabIndex = 55;
            // 
            // lb_break_right_2_read
            // 
            this.lb_break_right_2_read.AutoSize = true;
            this.lb_break_right_2_read.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_break_right_2_read.Location = new System.Drawing.Point(9, 4);
            this.lb_break_right_2_read.Name = "lb_break_right_2_read";
            this.lb_break_right_2_read.Size = new System.Drawing.Size(30, 16);
            this.lb_break_right_2_read.TabIndex = 0;
            this.lb_break_right_2_read.Text = "읽기";
            // 
            // lb_break_left_y
            // 
            this.lb_break_left_y.AutoSize = true;
            this.lb_break_left_y.Font = new System.Drawing.Font("맑은 고딕", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_break_left_y.Location = new System.Drawing.Point(6, 205);
            this.lb_break_left_y.Name = "lb_break_left_y";
            this.lb_break_left_y.Size = new System.Drawing.Size(107, 15);
            this.lb_break_left_y.TabIndex = 38;
            this.lb_break_left_y.Text = "Break_left_Y[mm]";
            // 
            // tbox_break_right_2_2
            // 
            this.tbox_break_right_2_2.Location = new System.Drawing.Point(825, 230);
            this.tbox_break_right_2_2.Name = "tbox_break_right_2_2";
            this.tbox_break_right_2_2.Size = new System.Drawing.Size(105, 26);
            this.tbox_break_right_2_2.TabIndex = 54;
            // 
            // panel4_break_left_2_read
            // 
            this.panel4_break_left_2_read.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel4_break_left_2_read.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4_break_left_2_read.Controls.Add(this.lb_break_left_2_read);
            this.panel4_break_left_2_read.Location = new System.Drawing.Point(936, 166);
            this.panel4_break_left_2_read.Name = "panel4_break_left_2_read";
            this.panel4_break_left_2_read.Size = new System.Drawing.Size(50, 26);
            this.panel4_break_left_2_read.TabIndex = 41;
            // 
            // lb_break_left_2_read
            // 
            this.lb_break_left_2_read.AutoSize = true;
            this.lb_break_left_2_read.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_break_left_2_read.Location = new System.Drawing.Point(9, 4);
            this.lb_break_left_2_read.Name = "lb_break_left_2_read";
            this.lb_break_left_2_read.Size = new System.Drawing.Size(30, 16);
            this.lb_break_left_2_read.TabIndex = 0;
            this.lb_break_left_2_read.Text = "읽기";
            // 
            // panel_break_right_1_move
            // 
            this.panel_break_right_1_move.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_break_right_1_move.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_break_right_1_move.Controls.Add(this.lb_break_right_1_move);
            this.panel_break_right_1_move.Location = new System.Drawing.Point(442, 230);
            this.panel_break_right_1_move.Name = "panel_break_right_1_move";
            this.panel_break_right_1_move.Size = new System.Drawing.Size(50, 26);
            this.panel_break_right_1_move.TabIndex = 48;
            // 
            // lb_break_right_1_move
            // 
            this.lb_break_right_1_move.AutoSize = true;
            this.lb_break_right_1_move.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_break_right_1_move.Location = new System.Drawing.Point(9, 4);
            this.lb_break_right_1_move.Name = "lb_break_right_1_move";
            this.lb_break_right_1_move.Size = new System.Drawing.Size(34, 17);
            this.lb_break_right_1_move.TabIndex = 0;
            this.lb_break_right_1_move.Text = "이동";
            // 
            // tbox_break_left_2_2
            // 
            this.tbox_break_left_2_2.Location = new System.Drawing.Point(825, 166);
            this.tbox_break_left_2_2.Name = "tbox_break_left_2_2";
            this.tbox_break_left_2_2.Size = new System.Drawing.Size(105, 26);
            this.tbox_break_left_2_2.TabIndex = 40;
            // 
            // tbox_break_right_2_1
            // 
            this.tbox_break_right_2_1.Location = new System.Drawing.Point(714, 230);
            this.tbox_break_right_2_1.Name = "tbox_break_right_2_1";
            this.tbox_break_right_2_1.Size = new System.Drawing.Size(105, 26);
            this.tbox_break_right_2_1.TabIndex = 52;
            // 
            // panel_break_left_1_move
            // 
            this.panel_break_left_1_move.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_break_left_1_move.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_break_left_1_move.Controls.Add(this.lb_break_left_1_move);
            this.panel_break_left_1_move.Location = new System.Drawing.Point(442, 166);
            this.panel_break_left_1_move.Name = "panel_break_left_1_move";
            this.panel_break_left_1_move.Size = new System.Drawing.Size(50, 26);
            this.panel_break_left_1_move.TabIndex = 36;
            // 
            // lb_break_left_1_move
            // 
            this.lb_break_left_1_move.AutoSize = true;
            this.lb_break_left_1_move.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_break_left_1_move.Location = new System.Drawing.Point(9, 4);
            this.lb_break_left_1_move.Name = "lb_break_left_1_move";
            this.lb_break_left_1_move.Size = new System.Drawing.Size(34, 17);
            this.lb_break_left_1_move.TabIndex = 0;
            this.lb_break_left_1_move.Text = "이동";
            // 
            // lb_break_right_2
            // 
            this.lb_break_right_2.AutoSize = true;
            this.lb_break_right_2.Font = new System.Drawing.Font("맑은 고딕", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_break_right_2.Location = new System.Drawing.Point(556, 237);
            this.lb_break_right_2.Name = "lb_break_right_2";
            this.lb_break_right_2.Size = new System.Drawing.Size(116, 15);
            this.lb_break_right_2.TabIndex = 50;
            this.lb_break_right_2.Text = "Break_right_2[mm]";
            // 
            // tbox_break_left_2_1
            // 
            this.tbox_break_left_2_1.Location = new System.Drawing.Point(714, 166);
            this.tbox_break_left_2_1.Name = "tbox_break_left_2_1";
            this.tbox_break_left_2_1.Size = new System.Drawing.Size(105, 26);
            this.tbox_break_left_2_1.TabIndex = 39;
            // 
            // panel_break_right_1_read
            // 
            this.panel_break_right_1_read.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_break_right_1_read.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_break_right_1_read.Controls.Add(this.lb_break_right_1_read);
            this.panel_break_right_1_read.Location = new System.Drawing.Point(386, 230);
            this.panel_break_right_1_read.Name = "panel_break_right_1_read";
            this.panel_break_right_1_read.Size = new System.Drawing.Size(50, 26);
            this.panel_break_right_1_read.TabIndex = 47;
            // 
            // lb_break_right_1_read
            // 
            this.lb_break_right_1_read.AutoSize = true;
            this.lb_break_right_1_read.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_break_right_1_read.Location = new System.Drawing.Point(9, 4);
            this.lb_break_right_1_read.Name = "lb_break_right_1_read";
            this.lb_break_right_1_read.Size = new System.Drawing.Size(34, 17);
            this.lb_break_right_1_read.TabIndex = 0;
            this.lb_break_right_1_read.Text = "읽기";
            // 
            // tbox_break_right_1_2
            // 
            this.tbox_break_right_1_2.Location = new System.Drawing.Point(275, 230);
            this.tbox_break_right_1_2.Name = "tbox_break_right_1_2";
            this.tbox_break_right_1_2.Size = new System.Drawing.Size(105, 26);
            this.tbox_break_right_1_2.TabIndex = 46;
            // 
            // panel_table2_alignmark1_calc
            // 
            this.panel_table2_alignmark1_calc.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_table2_alignmark1_calc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_table2_alignmark1_calc.Controls.Add(this.lb_table2_alignmark1_calc);
            this.panel_table2_alignmark1_calc.Location = new System.Drawing.Point(498, 134);
            this.panel_table2_alignmark1_calc.Name = "panel_table2_alignmark1_calc";
            this.panel_table2_alignmark1_calc.Size = new System.Drawing.Size(50, 26);
            this.panel_table2_alignmark1_calc.TabIndex = 31;
            // 
            // lb_table2_alignmark1_calc
            // 
            this.lb_table2_alignmark1_calc.AutoSize = true;
            this.lb_table2_alignmark1_calc.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_table2_alignmark1_calc.Location = new System.Drawing.Point(4, 4);
            this.lb_table2_alignmark1_calc.Name = "lb_table2_alignmark1_calc";
            this.lb_table2_alignmark1_calc.Size = new System.Drawing.Size(40, 17);
            this.lb_table2_alignmark1_calc.TabIndex = 0;
            this.lb_table2_alignmark1_calc.Text = "CALC";
            // 
            // tbox_break_right_1_1
            // 
            this.tbox_break_right_1_1.Location = new System.Drawing.Point(164, 230);
            this.tbox_break_right_1_1.Name = "tbox_break_right_1_1";
            this.tbox_break_right_1_1.Size = new System.Drawing.Size(105, 26);
            this.tbox_break_right_1_1.TabIndex = 45;
            // 
            // lb_break_left_2
            // 
            this.lb_break_left_2.AutoSize = true;
            this.lb_break_left_2.Font = new System.Drawing.Font("맑은 고딕", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_break_left_2.Location = new System.Drawing.Point(556, 173);
            this.lb_break_left_2.Name = "lb_break_left_2";
            this.lb_break_left_2.Size = new System.Drawing.Size(107, 15);
            this.lb_break_left_2.TabIndex = 38;
            this.lb_break_left_2.Text = "Break_left_2[mm]";
            // 
            // lb_break_right_1
            // 
            this.lb_break_right_1.AutoSize = true;
            this.lb_break_right_1.Font = new System.Drawing.Font("맑은 고딕", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_break_right_1.Location = new System.Drawing.Point(6, 237);
            this.lb_break_right_1.Name = "lb_break_right_1";
            this.lb_break_right_1.Size = new System.Drawing.Size(116, 15);
            this.lb_break_right_1.TabIndex = 44;
            this.lb_break_right_1.Text = "Break_right_1[mm]";
            // 
            // panel_break_left_1_read
            // 
            this.panel_break_left_1_read.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_break_left_1_read.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_break_left_1_read.Controls.Add(this.lb_break_left_1_read);
            this.panel_break_left_1_read.Location = new System.Drawing.Point(386, 166);
            this.panel_break_left_1_read.Name = "panel_break_left_1_read";
            this.panel_break_left_1_read.Size = new System.Drawing.Size(50, 26);
            this.panel_break_left_1_read.TabIndex = 35;
            // 
            // lb_break_left_1_read
            // 
            this.lb_break_left_1_read.AutoSize = true;
            this.lb_break_left_1_read.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_break_left_1_read.Location = new System.Drawing.Point(9, 4);
            this.lb_break_left_1_read.Name = "lb_break_left_1_read";
            this.lb_break_left_1_read.Size = new System.Drawing.Size(34, 17);
            this.lb_break_left_1_read.TabIndex = 0;
            this.lb_break_left_1_read.Text = "읽기";
            // 
            // tbox_break_left_1_2
            // 
            this.tbox_break_left_1_2.Location = new System.Drawing.Point(275, 166);
            this.tbox_break_left_1_2.Name = "tbox_break_left_1_2";
            this.tbox_break_left_1_2.Size = new System.Drawing.Size(105, 26);
            this.tbox_break_left_1_2.TabIndex = 34;
            // 
            // panel_table2_alignmark2_move
            // 
            this.panel_table2_alignmark2_move.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_table2_alignmark2_move.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_table2_alignmark2_move.Controls.Add(this.lb_table2_alignmark2_move);
            this.panel_table2_alignmark2_move.Location = new System.Drawing.Point(992, 134);
            this.panel_table2_alignmark2_move.Name = "panel_table2_alignmark2_move";
            this.panel_table2_alignmark2_move.Size = new System.Drawing.Size(50, 26);
            this.panel_table2_alignmark2_move.TabIndex = 36;
            // 
            // lb_table2_alignmark2_move
            // 
            this.lb_table2_alignmark2_move.AutoSize = true;
            this.lb_table2_alignmark2_move.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_table2_alignmark2_move.Location = new System.Drawing.Point(9, 4);
            this.lb_table2_alignmark2_move.Name = "lb_table2_alignmark2_move";
            this.lb_table2_alignmark2_move.Size = new System.Drawing.Size(30, 16);
            this.lb_table2_alignmark2_move.TabIndex = 0;
            this.lb_table2_alignmark2_move.Text = "이동";
            // 
            // tbox_break_left_1_1
            // 
            this.tbox_break_left_1_1.Location = new System.Drawing.Point(164, 166);
            this.tbox_break_left_1_1.Name = "tbox_break_left_1_1";
            this.tbox_break_left_1_1.Size = new System.Drawing.Size(105, 26);
            this.tbox_break_left_1_1.TabIndex = 33;
            // 
            // panel_table1_alignmark2_calc
            // 
            this.panel_table1_alignmark2_calc.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_table1_alignmark2_calc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_table1_alignmark2_calc.Controls.Add(this.lb_table1_alignmark2_calc);
            this.panel_table1_alignmark2_calc.Location = new System.Drawing.Point(1048, 102);
            this.panel_table1_alignmark2_calc.Name = "panel_table1_alignmark2_calc";
            this.panel_table1_alignmark2_calc.Size = new System.Drawing.Size(50, 26);
            this.panel_table1_alignmark2_calc.TabIndex = 31;
            // 
            // lb_table1_alignmark2_calc
            // 
            this.lb_table1_alignmark2_calc.AutoSize = true;
            this.lb_table1_alignmark2_calc.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_table1_alignmark2_calc.Location = new System.Drawing.Point(4, 4);
            this.lb_table1_alignmark2_calc.Name = "lb_table1_alignmark2_calc";
            this.lb_table1_alignmark2_calc.Size = new System.Drawing.Size(40, 17);
            this.lb_table1_alignmark2_calc.TabIndex = 0;
            this.lb_table1_alignmark2_calc.Text = "CALC";
            // 
            // lb_break_left_1
            // 
            this.lb_break_left_1.AutoSize = true;
            this.lb_break_left_1.Font = new System.Drawing.Font("맑은 고딕", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_break_left_1.Location = new System.Drawing.Point(6, 173);
            this.lb_break_left_1.Name = "lb_break_left_1";
            this.lb_break_left_1.Size = new System.Drawing.Size(107, 15);
            this.lb_break_left_1.TabIndex = 32;
            this.lb_break_left_1.Text = "Break_left_1[mm]";
            // 
            // panel_table2_alignmark2_read
            // 
            this.panel_table2_alignmark2_read.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_table2_alignmark2_read.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_table2_alignmark2_read.Controls.Add(this.lb_table2_alignmark2_read);
            this.panel_table2_alignmark2_read.Location = new System.Drawing.Point(936, 134);
            this.panel_table2_alignmark2_read.Name = "panel_table2_alignmark2_read";
            this.panel_table2_alignmark2_read.Size = new System.Drawing.Size(50, 26);
            this.panel_table2_alignmark2_read.TabIndex = 35;
            // 
            // lb_table2_alignmark2_read
            // 
            this.lb_table2_alignmark2_read.AutoSize = true;
            this.lb_table2_alignmark2_read.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_table2_alignmark2_read.Location = new System.Drawing.Point(9, 4);
            this.lb_table2_alignmark2_read.Name = "lb_table2_alignmark2_read";
            this.lb_table2_alignmark2_read.Size = new System.Drawing.Size(30, 16);
            this.lb_table2_alignmark2_read.TabIndex = 0;
            this.lb_table2_alignmark2_read.Text = "읽기";
            // 
            // panel_table2_alignmark1_move
            // 
            this.panel_table2_alignmark1_move.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_table2_alignmark1_move.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_table2_alignmark1_move.Controls.Add(this.lb_table2_alignmark1_move);
            this.panel_table2_alignmark1_move.Location = new System.Drawing.Point(442, 134);
            this.panel_table2_alignmark1_move.Name = "panel_table2_alignmark1_move";
            this.panel_table2_alignmark1_move.Size = new System.Drawing.Size(50, 26);
            this.panel_table2_alignmark1_move.TabIndex = 30;
            // 
            // lb_table2_alignmark1_move
            // 
            this.lb_table2_alignmark1_move.AutoSize = true;
            this.lb_table2_alignmark1_move.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_table2_alignmark1_move.Location = new System.Drawing.Point(9, 4);
            this.lb_table2_alignmark1_move.Name = "lb_table2_alignmark1_move";
            this.lb_table2_alignmark1_move.Size = new System.Drawing.Size(34, 17);
            this.lb_table2_alignmark1_move.TabIndex = 0;
            this.lb_table2_alignmark1_move.Text = "이동";
            // 
            // tbox_table2_alignmark2_2
            // 
            this.tbox_table2_alignmark2_2.Location = new System.Drawing.Point(825, 134);
            this.tbox_table2_alignmark2_2.Name = "tbox_table2_alignmark2_2";
            this.tbox_table2_alignmark2_2.Size = new System.Drawing.Size(105, 26);
            this.tbox_table2_alignmark2_2.TabIndex = 34;
            // 
            // panel_table2_alignmark1_read
            // 
            this.panel_table2_alignmark1_read.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_table2_alignmark1_read.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_table2_alignmark1_read.Controls.Add(this.lb_table2_alignmark1_read);
            this.panel_table2_alignmark1_read.Location = new System.Drawing.Point(386, 134);
            this.panel_table2_alignmark1_read.Name = "panel_table2_alignmark1_read";
            this.panel_table2_alignmark1_read.Size = new System.Drawing.Size(50, 26);
            this.panel_table2_alignmark1_read.TabIndex = 29;
            // 
            // lb_table2_alignmark1_read
            // 
            this.lb_table2_alignmark1_read.AutoSize = true;
            this.lb_table2_alignmark1_read.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_table2_alignmark1_read.Location = new System.Drawing.Point(9, 4);
            this.lb_table2_alignmark1_read.Name = "lb_table2_alignmark1_read";
            this.lb_table2_alignmark1_read.Size = new System.Drawing.Size(34, 17);
            this.lb_table2_alignmark1_read.TabIndex = 0;
            this.lb_table2_alignmark1_read.Text = "읽기";
            // 
            // tbox_table2_alignmark2_1
            // 
            this.tbox_table2_alignmark2_1.Location = new System.Drawing.Point(714, 134);
            this.tbox_table2_alignmark2_1.Name = "tbox_table2_alignmark2_1";
            this.tbox_table2_alignmark2_1.Size = new System.Drawing.Size(105, 26);
            this.tbox_table2_alignmark2_1.TabIndex = 33;
            // 
            // lb_table2_alignmark2
            // 
            this.lb_table2_alignmark2.AutoSize = true;
            this.lb_table2_alignmark2.Font = new System.Drawing.Font("맑은 고딕", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_table2_alignmark2.Location = new System.Drawing.Point(556, 141);
            this.lb_table2_alignmark2.Name = "lb_table2_alignmark2";
            this.lb_table2_alignmark2.Size = new System.Drawing.Size(147, 15);
            this.lb_table2_alignmark2.TabIndex = 32;
            this.lb_table2_alignmark2.Text = "Table2AlignMark 2[mm]";
            // 
            // panel_table1_alignmark1_calc
            // 
            this.panel_table1_alignmark1_calc.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_table1_alignmark1_calc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_table1_alignmark1_calc.Controls.Add(this.lb_table1_alignmark1_calc);
            this.panel_table1_alignmark1_calc.Location = new System.Drawing.Point(498, 102);
            this.panel_table1_alignmark1_calc.Name = "panel_table1_alignmark1_calc";
            this.panel_table1_alignmark1_calc.Size = new System.Drawing.Size(50, 26);
            this.panel_table1_alignmark1_calc.TabIndex = 25;
            // 
            // lb_table1_alignmark1_calc
            // 
            this.lb_table1_alignmark1_calc.AutoSize = true;
            this.lb_table1_alignmark1_calc.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_table1_alignmark1_calc.Location = new System.Drawing.Point(4, 4);
            this.lb_table1_alignmark1_calc.Name = "lb_table1_alignmark1_calc";
            this.lb_table1_alignmark1_calc.Size = new System.Drawing.Size(40, 17);
            this.lb_table1_alignmark1_calc.TabIndex = 0;
            this.lb_table1_alignmark1_calc.Text = "CALC";
            // 
            // tbox_table2_alignmark1_2
            // 
            this.tbox_table2_alignmark1_2.Location = new System.Drawing.Point(275, 134);
            this.tbox_table2_alignmark1_2.Name = "tbox_table2_alignmark1_2";
            this.tbox_table2_alignmark1_2.Size = new System.Drawing.Size(105, 26);
            this.tbox_table2_alignmark1_2.TabIndex = 28;
            // 
            // panel_table1_alignmark2_move
            // 
            this.panel_table1_alignmark2_move.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_table1_alignmark2_move.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_table1_alignmark2_move.Controls.Add(this.lb_table1_alignmark2_move);
            this.panel_table1_alignmark2_move.Location = new System.Drawing.Point(992, 102);
            this.panel_table1_alignmark2_move.Name = "panel_table1_alignmark2_move";
            this.panel_table1_alignmark2_move.Size = new System.Drawing.Size(50, 26);
            this.panel_table1_alignmark2_move.TabIndex = 30;
            // 
            // lb_table1_alignmark2_move
            // 
            this.lb_table1_alignmark2_move.AutoSize = true;
            this.lb_table1_alignmark2_move.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_table1_alignmark2_move.Location = new System.Drawing.Point(9, 4);
            this.lb_table1_alignmark2_move.Name = "lb_table1_alignmark2_move";
            this.lb_table1_alignmark2_move.Size = new System.Drawing.Size(30, 16);
            this.lb_table1_alignmark2_move.TabIndex = 0;
            this.lb_table1_alignmark2_move.Text = "이동";
            // 
            // tbox_table2_alignmark1_1
            // 
            this.tbox_table2_alignmark1_1.Location = new System.Drawing.Point(164, 134);
            this.tbox_table2_alignmark1_1.Name = "tbox_table2_alignmark1_1";
            this.tbox_table2_alignmark1_1.Size = new System.Drawing.Size(105, 26);
            this.tbox_table2_alignmark1_1.TabIndex = 27;
            // 
            // panel_table1_alignmark1_move
            // 
            this.panel_table1_alignmark1_move.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_table1_alignmark1_move.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_table1_alignmark1_move.Controls.Add(this.lb_table1_alignmark1_move);
            this.panel_table1_alignmark1_move.Location = new System.Drawing.Point(442, 102);
            this.panel_table1_alignmark1_move.Name = "panel_table1_alignmark1_move";
            this.panel_table1_alignmark1_move.Size = new System.Drawing.Size(50, 26);
            this.panel_table1_alignmark1_move.TabIndex = 24;
            // 
            // lb_table1_alignmark1_move
            // 
            this.lb_table1_alignmark1_move.AutoSize = true;
            this.lb_table1_alignmark1_move.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_table1_alignmark1_move.Location = new System.Drawing.Point(9, 4);
            this.lb_table1_alignmark1_move.Name = "lb_table1_alignmark1_move";
            this.lb_table1_alignmark1_move.Size = new System.Drawing.Size(34, 17);
            this.lb_table1_alignmark1_move.TabIndex = 0;
            this.lb_table1_alignmark1_move.Text = "이동";
            // 
            // lb_table2_alignmark1
            // 
            this.lb_table2_alignmark1.AutoSize = true;
            this.lb_table2_alignmark1.Font = new System.Drawing.Font("맑은 고딕", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_table2_alignmark1.Location = new System.Drawing.Point(6, 141);
            this.lb_table2_alignmark1.Name = "lb_table2_alignmark1";
            this.lb_table2_alignmark1.Size = new System.Drawing.Size(147, 15);
            this.lb_table2_alignmark1.TabIndex = 26;
            this.lb_table2_alignmark1.Text = "Table2AlignMark 1[mm]";
            // 
            // panel_table1_alignmark2_read
            // 
            this.panel_table1_alignmark2_read.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_table1_alignmark2_read.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_table1_alignmark2_read.Controls.Add(this.lb_table1_alignmark2_read);
            this.panel_table1_alignmark2_read.Location = new System.Drawing.Point(936, 102);
            this.panel_table1_alignmark2_read.Name = "panel_table1_alignmark2_read";
            this.panel_table1_alignmark2_read.Size = new System.Drawing.Size(50, 26);
            this.panel_table1_alignmark2_read.TabIndex = 29;
            // 
            // lb_table1_alignmark2_read
            // 
            this.lb_table1_alignmark2_read.AutoSize = true;
            this.lb_table1_alignmark2_read.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_table1_alignmark2_read.Location = new System.Drawing.Point(9, 4);
            this.lb_table1_alignmark2_read.Name = "lb_table1_alignmark2_read";
            this.lb_table1_alignmark2_read.Size = new System.Drawing.Size(30, 16);
            this.lb_table1_alignmark2_read.TabIndex = 0;
            this.lb_table1_alignmark2_read.Text = "읽기";
            // 
            // tbox_table1_alignmark2_2
            // 
            this.tbox_table1_alignmark2_2.Location = new System.Drawing.Point(825, 102);
            this.tbox_table1_alignmark2_2.Name = "tbox_table1_alignmark2_2";
            this.tbox_table1_alignmark2_2.Size = new System.Drawing.Size(105, 26);
            this.tbox_table1_alignmark2_2.TabIndex = 28;
            // 
            // panel_table1_alignmark1_read
            // 
            this.panel_table1_alignmark1_read.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_table1_alignmark1_read.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_table1_alignmark1_read.Controls.Add(this.lb_table1_alignmark1_read);
            this.panel_table1_alignmark1_read.Location = new System.Drawing.Point(386, 102);
            this.panel_table1_alignmark1_read.Name = "panel_table1_alignmark1_read";
            this.panel_table1_alignmark1_read.Size = new System.Drawing.Size(50, 26);
            this.panel_table1_alignmark1_read.TabIndex = 23;
            // 
            // lb_table1_alignmark1_read
            // 
            this.lb_table1_alignmark1_read.AutoSize = true;
            this.lb_table1_alignmark1_read.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_table1_alignmark1_read.Location = new System.Drawing.Point(9, 4);
            this.lb_table1_alignmark1_read.Name = "lb_table1_alignmark1_read";
            this.lb_table1_alignmark1_read.Size = new System.Drawing.Size(34, 17);
            this.lb_table1_alignmark1_read.TabIndex = 0;
            this.lb_table1_alignmark1_read.Text = "읽기";
            // 
            // tbox_table1_alignmark2_1
            // 
            this.tbox_table1_alignmark2_1.Location = new System.Drawing.Point(714, 102);
            this.tbox_table1_alignmark2_1.Name = "tbox_table1_alignmark2_1";
            this.tbox_table1_alignmark2_1.Size = new System.Drawing.Size(105, 26);
            this.tbox_table1_alignmark2_1.TabIndex = 27;
            // 
            // tbox_table1_alignmark1_2
            // 
            this.tbox_table1_alignmark1_2.Location = new System.Drawing.Point(275, 102);
            this.tbox_table1_alignmark1_2.Name = "tbox_table1_alignmark1_2";
            this.tbox_table1_alignmark1_2.Size = new System.Drawing.Size(105, 26);
            this.tbox_table1_alignmark1_2.TabIndex = 22;
            // 
            // lb_table1_alignmark2
            // 
            this.lb_table1_alignmark2.AutoSize = true;
            this.lb_table1_alignmark2.Font = new System.Drawing.Font("맑은 고딕", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_table1_alignmark2.Location = new System.Drawing.Point(556, 109);
            this.lb_table1_alignmark2.Name = "lb_table1_alignmark2";
            this.lb_table1_alignmark2.Size = new System.Drawing.Size(147, 15);
            this.lb_table1_alignmark2.TabIndex = 26;
            this.lb_table1_alignmark2.Text = "Table1AlignMark 2[mm]";
            // 
            // tbox_table1_alignmark1_1
            // 
            this.tbox_table1_alignmark1_1.Location = new System.Drawing.Point(164, 102);
            this.tbox_table1_alignmark1_1.Name = "tbox_table1_alignmark1_1";
            this.tbox_table1_alignmark1_1.Size = new System.Drawing.Size(105, 26);
            this.tbox_table1_alignmark1_1.TabIndex = 21;
            // 
            // lb_table1_alignmark1
            // 
            this.lb_table1_alignmark1.AutoSize = true;
            this.lb_table1_alignmark1.Font = new System.Drawing.Font("맑은 고딕", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_table1_alignmark1.Location = new System.Drawing.Point(6, 109);
            this.lb_table1_alignmark1.Name = "lb_table1_alignmark1";
            this.lb_table1_alignmark1.Size = new System.Drawing.Size(147, 15);
            this.lb_table1_alignmark1.TabIndex = 20;
            this.lb_table1_alignmark1.Text = "Table1AlignMark 1[mm]";
            // 
            // panel_align
            // 
            this.panel_align.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_align.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_align.Controls.Add(this.lb_align);
            this.panel_align.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.panel_align.ForeColor = System.Drawing.Color.White;
            this.panel_align.Location = new System.Drawing.Point(717, 56);
            this.panel_align.Name = "panel_align";
            this.panel_align.Size = new System.Drawing.Size(102, 26);
            this.panel_align.TabIndex = 19;
            // 
            // lb_align
            // 
            this.lb_align.AutoSize = true;
            this.lb_align.BackColor = System.Drawing.Color.Transparent;
            this.lb_align.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_align.ForeColor = System.Drawing.Color.White;
            this.lb_align.Location = new System.Drawing.Point(29, 2);
            this.lb_align.Name = "lb_align";
            this.lb_align.Size = new System.Drawing.Size(46, 20);
            this.lb_align.TabIndex = 4;
            this.lb_align.Text = "Align";
            // 
            // panel_recipe_load
            // 
            this.panel_recipe_load.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_recipe_load.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_recipe_load.Controls.Add(this.lb_recipe_load);
            this.panel_recipe_load.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.panel_recipe_load.ForeColor = System.Drawing.Color.White;
            this.panel_recipe_load.Location = new System.Drawing.Point(603, 56);
            this.panel_recipe_load.Name = "panel_recipe_load";
            this.panel_recipe_load.Size = new System.Drawing.Size(102, 26);
            this.panel_recipe_load.TabIndex = 18;
            // 
            // lb_recipe_load
            // 
            this.lb_recipe_load.AutoSize = true;
            this.lb_recipe_load.BackColor = System.Drawing.Color.Transparent;
            this.lb_recipe_load.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_recipe_load.ForeColor = System.Drawing.Color.White;
            this.lb_recipe_load.Location = new System.Drawing.Point(15, 3);
            this.lb_recipe_load.Name = "lb_recipe_load";
            this.lb_recipe_load.Size = new System.Drawing.Size(69, 20);
            this.lb_recipe_load.TabIndex = 4;
            this.lb_recipe_load.Text = "불러오기";
            // 
            // tbox_recipe_path
            // 
            this.tbox_recipe_path.Location = new System.Drawing.Point(65, 56);
            this.tbox_recipe_path.Name = "tbox_recipe_path";
            this.tbox_recipe_path.ReadOnly = true;
            this.tbox_recipe_path.Size = new System.Drawing.Size(532, 26);
            this.tbox_recipe_path.TabIndex = 4;
            // 
            // tbox_recipe
            // 
            this.tbox_recipe.Location = new System.Drawing.Point(65, 23);
            this.tbox_recipe.Name = "tbox_recipe";
            this.tbox_recipe.ReadOnly = true;
            this.tbox_recipe.Size = new System.Drawing.Size(754, 26);
            this.tbox_recipe.TabIndex = 3;
            // 
            // lb_recipe
            // 
            this.lb_recipe.AutoSize = true;
            this.lb_recipe.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_recipe.Location = new System.Drawing.Point(6, 27);
            this.lb_recipe.Name = "lb_recipe";
            this.lb_recipe.Size = new System.Drawing.Size(55, 20);
            this.lb_recipe.TabIndex = 2;
            this.lb_recipe.Text = "Recipe";
            // 
            // gbox_y_offset
            // 
            this.gbox_y_offset.BackColor = System.Drawing.Color.Transparent;
            this.gbox_y_offset.Controls.Add(this.tbox_y_offset_break);
            this.gbox_y_offset.Controls.Add(this.lb_y_offset_break);
            this.gbox_y_offset.Controls.Add(this.tbox_y_offset_pre);
            this.gbox_y_offset.Controls.Add(this.lb_y_offset_pre);
            this.gbox_y_offset.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.gbox_y_offset.ForeColor = System.Drawing.Color.White;
            this.gbox_y_offset.Location = new System.Drawing.Point(1394, 671);
            this.gbox_y_offset.Name = "gbox_y_offset";
            this.gbox_y_offset.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gbox_y_offset.Size = new System.Drawing.Size(343, 109);
            this.gbox_y_offset.TabIndex = 66;
            this.gbox_y_offset.TabStop = false;
            this.gbox_y_offset.Text = "Y Offset";
            // 
            // tbox_y_offset_break
            // 
            this.tbox_y_offset_break.Location = new System.Drawing.Point(151, 64);
            this.tbox_y_offset_break.Name = "tbox_y_offset_break";
            this.tbox_y_offset_break.Size = new System.Drawing.Size(181, 29);
            this.tbox_y_offset_break.TabIndex = 6;
            // 
            // lb_y_offset_break
            // 
            this.lb_y_offset_break.AutoSize = true;
            this.lb_y_offset_break.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_y_offset_break.Location = new System.Drawing.Point(6, 68);
            this.lb_y_offset_break.Name = "lb_y_offset_break";
            this.lb_y_offset_break.Size = new System.Drawing.Size(119, 17);
            this.lb_y_offset_break.TabIndex = 5;
            this.lb_y_offset_break.Text = "Offset Break[mm]";
            // 
            // tbox_y_offset_pre
            // 
            this.tbox_y_offset_pre.Location = new System.Drawing.Point(151, 32);
            this.tbox_y_offset_pre.Name = "tbox_y_offset_pre";
            this.tbox_y_offset_pre.Size = new System.Drawing.Size(181, 29);
            this.tbox_y_offset_pre.TabIndex = 4;
            // 
            // lb_y_offset_pre
            // 
            this.lb_y_offset_pre.AutoSize = true;
            this.lb_y_offset_pre.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_y_offset_pre.Location = new System.Drawing.Point(6, 38);
            this.lb_y_offset_pre.Name = "lb_y_offset_pre";
            this.lb_y_offset_pre.Size = new System.Drawing.Size(105, 17);
            this.lb_y_offset_pre.TabIndex = 1;
            this.lb_y_offset_pre.Text = "Offset Pre[mm]";
            // 
            // gbox_lds_break
            // 
            this.gbox_lds_break.BackColor = System.Drawing.Color.Transparent;
            this.gbox_lds_break.Controls.Add(this.panel_lds_break4_move);
            this.gbox_lds_break.Controls.Add(this.tbox_lds_break4_2);
            this.gbox_lds_break.Controls.Add(this.tbox_lds_break4_1);
            this.gbox_lds_break.Controls.Add(this.panel_lds_break4_read);
            this.gbox_lds_break.Controls.Add(this.panel_lds_break3_move);
            this.gbox_lds_break.Controls.Add(this.tbox_lds_break3_2);
            this.gbox_lds_break.Controls.Add(this.tbox_lds_break3_1);
            this.gbox_lds_break.Controls.Add(this.panel_lds_break3_read);
            this.gbox_lds_break.Controls.Add(this.panel_lds_break2_move);
            this.gbox_lds_break.Controls.Add(this.panel_lds_break1_move);
            this.gbox_lds_break.Controls.Add(this.tbox_lds_break2_2);
            this.gbox_lds_break.Controls.Add(this.tbox_lds_break2_1);
            this.gbox_lds_break.Controls.Add(this.tbox_lds_break1_2);
            this.gbox_lds_break.Controls.Add(this.panel_lds_break2_read);
            this.gbox_lds_break.Controls.Add(this.lb_lds_break4);
            this.gbox_lds_break.Controls.Add(this.lb_lds_break3);
            this.gbox_lds_break.Controls.Add(this.lb_lds_break2);
            this.gbox_lds_break.Controls.Add(this.tbox_lds_break1_1);
            this.gbox_lds_break.Controls.Add(this.panel_lds_break1_read);
            this.gbox_lds_break.Controls.Add(this.lb_lds_break1);
            this.gbox_lds_break.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.gbox_lds_break.ForeColor = System.Drawing.Color.White;
            this.gbox_lds_break.Location = new System.Drawing.Point(1394, 512);
            this.gbox_lds_break.Name = "gbox_lds_break";
            this.gbox_lds_break.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gbox_lds_break.Size = new System.Drawing.Size(343, 153);
            this.gbox_lds_break.TabIndex = 65;
            this.gbox_lds_break.TabStop = false;
            this.gbox_lds_break.Text = "LDS Break";
            // 
            // panel_lds_break4_move
            // 
            this.panel_lds_break4_move.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_lds_break4_move.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_lds_break4_move.Controls.Add(this.lb_lds_break4_move);
            this.panel_lds_break4_move.Location = new System.Drawing.Point(295, 116);
            this.panel_lds_break4_move.Name = "panel_lds_break4_move";
            this.panel_lds_break4_move.Size = new System.Drawing.Size(42, 26);
            this.panel_lds_break4_move.TabIndex = 45;
            // 
            // lb_lds_break4_move
            // 
            this.lb_lds_break4_move.AutoSize = true;
            this.lb_lds_break4_move.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_lds_break4_move.Location = new System.Drawing.Point(5, 4);
            this.lb_lds_break4_move.Name = "lb_lds_break4_move";
            this.lb_lds_break4_move.Size = new System.Drawing.Size(34, 17);
            this.lb_lds_break4_move.TabIndex = 0;
            this.lb_lds_break4_move.Text = "이동";
            // 
            // tbox_lds_break4_2
            // 
            this.tbox_lds_break4_2.Location = new System.Drawing.Point(172, 116);
            this.tbox_lds_break4_2.Name = "tbox_lds_break4_2";
            this.tbox_lds_break4_2.Size = new System.Drawing.Size(69, 29);
            this.tbox_lds_break4_2.TabIndex = 47;
            // 
            // tbox_lds_break4_1
            // 
            this.tbox_lds_break4_1.Location = new System.Drawing.Point(97, 116);
            this.tbox_lds_break4_1.Name = "tbox_lds_break4_1";
            this.tbox_lds_break4_1.Size = new System.Drawing.Size(69, 29);
            this.tbox_lds_break4_1.TabIndex = 46;
            // 
            // panel_lds_break4_read
            // 
            this.panel_lds_break4_read.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_lds_break4_read.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_lds_break4_read.Controls.Add(this.lb_lds_break4_read);
            this.panel_lds_break4_read.Location = new System.Drawing.Point(247, 116);
            this.panel_lds_break4_read.Name = "panel_lds_break4_read";
            this.panel_lds_break4_read.Size = new System.Drawing.Size(42, 26);
            this.panel_lds_break4_read.TabIndex = 44;
            // 
            // lb_lds_break4_read
            // 
            this.lb_lds_break4_read.AutoSize = true;
            this.lb_lds_break4_read.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_lds_break4_read.Location = new System.Drawing.Point(5, 4);
            this.lb_lds_break4_read.Name = "lb_lds_break4_read";
            this.lb_lds_break4_read.Size = new System.Drawing.Size(34, 17);
            this.lb_lds_break4_read.TabIndex = 0;
            this.lb_lds_break4_read.Text = "읽기";
            // 
            // panel_lds_break3_move
            // 
            this.panel_lds_break3_move.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_lds_break3_move.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_lds_break3_move.Controls.Add(this.lb_lds_break3_move);
            this.panel_lds_break3_move.Location = new System.Drawing.Point(295, 85);
            this.panel_lds_break3_move.Name = "panel_lds_break3_move";
            this.panel_lds_break3_move.Size = new System.Drawing.Size(42, 26);
            this.panel_lds_break3_move.TabIndex = 41;
            // 
            // lb_lds_break3_move
            // 
            this.lb_lds_break3_move.AutoSize = true;
            this.lb_lds_break3_move.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_lds_break3_move.Location = new System.Drawing.Point(5, 4);
            this.lb_lds_break3_move.Name = "lb_lds_break3_move";
            this.lb_lds_break3_move.Size = new System.Drawing.Size(34, 17);
            this.lb_lds_break3_move.TabIndex = 0;
            this.lb_lds_break3_move.Text = "이동";
            // 
            // tbox_lds_break3_2
            // 
            this.tbox_lds_break3_2.Location = new System.Drawing.Point(172, 85);
            this.tbox_lds_break3_2.Name = "tbox_lds_break3_2";
            this.tbox_lds_break3_2.Size = new System.Drawing.Size(69, 29);
            this.tbox_lds_break3_2.TabIndex = 43;
            // 
            // tbox_lds_break3_1
            // 
            this.tbox_lds_break3_1.Location = new System.Drawing.Point(97, 85);
            this.tbox_lds_break3_1.Name = "tbox_lds_break3_1";
            this.tbox_lds_break3_1.Size = new System.Drawing.Size(69, 29);
            this.tbox_lds_break3_1.TabIndex = 42;
            // 
            // panel_lds_break3_read
            // 
            this.panel_lds_break3_read.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_lds_break3_read.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_lds_break3_read.Controls.Add(this.lb_lds_break3_read);
            this.panel_lds_break3_read.Location = new System.Drawing.Point(247, 85);
            this.panel_lds_break3_read.Name = "panel_lds_break3_read";
            this.panel_lds_break3_read.Size = new System.Drawing.Size(42, 26);
            this.panel_lds_break3_read.TabIndex = 40;
            // 
            // lb_lds_break3_read
            // 
            this.lb_lds_break3_read.AutoSize = true;
            this.lb_lds_break3_read.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_lds_break3_read.Location = new System.Drawing.Point(5, 4);
            this.lb_lds_break3_read.Name = "lb_lds_break3_read";
            this.lb_lds_break3_read.Size = new System.Drawing.Size(34, 17);
            this.lb_lds_break3_read.TabIndex = 0;
            this.lb_lds_break3_read.Text = "읽기";
            // 
            // panel_lds_break2_move
            // 
            this.panel_lds_break2_move.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_lds_break2_move.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_lds_break2_move.Controls.Add(this.lb_lds_break2_move);
            this.panel_lds_break2_move.Location = new System.Drawing.Point(295, 53);
            this.panel_lds_break2_move.Name = "panel_lds_break2_move";
            this.panel_lds_break2_move.Size = new System.Drawing.Size(42, 26);
            this.panel_lds_break2_move.TabIndex = 37;
            // 
            // lb_lds_break2_move
            // 
            this.lb_lds_break2_move.AutoSize = true;
            this.lb_lds_break2_move.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_lds_break2_move.Location = new System.Drawing.Point(5, 4);
            this.lb_lds_break2_move.Name = "lb_lds_break2_move";
            this.lb_lds_break2_move.Size = new System.Drawing.Size(34, 17);
            this.lb_lds_break2_move.TabIndex = 0;
            this.lb_lds_break2_move.Text = "이동";
            // 
            // panel_lds_break1_move
            // 
            this.panel_lds_break1_move.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_lds_break1_move.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_lds_break1_move.Controls.Add(this.lb_lds_break1_move);
            this.panel_lds_break1_move.Location = new System.Drawing.Point(295, 21);
            this.panel_lds_break1_move.Name = "panel_lds_break1_move";
            this.panel_lds_break1_move.Size = new System.Drawing.Size(42, 26);
            this.panel_lds_break1_move.TabIndex = 31;
            // 
            // lb_lds_break1_move
            // 
            this.lb_lds_break1_move.AutoSize = true;
            this.lb_lds_break1_move.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_lds_break1_move.Location = new System.Drawing.Point(5, 4);
            this.lb_lds_break1_move.Name = "lb_lds_break1_move";
            this.lb_lds_break1_move.Size = new System.Drawing.Size(34, 17);
            this.lb_lds_break1_move.TabIndex = 0;
            this.lb_lds_break1_move.Text = "이동";
            // 
            // tbox_lds_break2_2
            // 
            this.tbox_lds_break2_2.Location = new System.Drawing.Point(172, 53);
            this.tbox_lds_break2_2.Name = "tbox_lds_break2_2";
            this.tbox_lds_break2_2.Size = new System.Drawing.Size(69, 29);
            this.tbox_lds_break2_2.TabIndex = 39;
            // 
            // tbox_lds_break2_1
            // 
            this.tbox_lds_break2_1.Location = new System.Drawing.Point(97, 53);
            this.tbox_lds_break2_1.Name = "tbox_lds_break2_1";
            this.tbox_lds_break2_1.Size = new System.Drawing.Size(69, 29);
            this.tbox_lds_break2_1.TabIndex = 38;
            // 
            // tbox_lds_break1_2
            // 
            this.tbox_lds_break1_2.Location = new System.Drawing.Point(172, 21);
            this.tbox_lds_break1_2.Name = "tbox_lds_break1_2";
            this.tbox_lds_break1_2.Size = new System.Drawing.Size(69, 29);
            this.tbox_lds_break1_2.TabIndex = 35;
            // 
            // panel_lds_break2_read
            // 
            this.panel_lds_break2_read.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_lds_break2_read.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_lds_break2_read.Controls.Add(this.lb_lds_break2_read);
            this.panel_lds_break2_read.Location = new System.Drawing.Point(247, 53);
            this.panel_lds_break2_read.Name = "panel_lds_break2_read";
            this.panel_lds_break2_read.Size = new System.Drawing.Size(42, 26);
            this.panel_lds_break2_read.TabIndex = 36;
            // 
            // lb_lds_break2_read
            // 
            this.lb_lds_break2_read.AutoSize = true;
            this.lb_lds_break2_read.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_lds_break2_read.Location = new System.Drawing.Point(5, 4);
            this.lb_lds_break2_read.Name = "lb_lds_break2_read";
            this.lb_lds_break2_read.Size = new System.Drawing.Size(34, 17);
            this.lb_lds_break2_read.TabIndex = 0;
            this.lb_lds_break2_read.Text = "읽기";
            // 
            // lb_lds_break4
            // 
            this.lb_lds_break4.AutoSize = true;
            this.lb_lds_break4.Font = new System.Drawing.Font("맑은 고딕", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_lds_break4.Location = new System.Drawing.Point(6, 123);
            this.lb_lds_break4.Name = "lb_lds_break4";
            this.lb_lds_break4.Size = new System.Drawing.Size(78, 15);
            this.lb_lds_break4.TabIndex = 34;
            this.lb_lds_break4.Text = "Break4[mm]";
            // 
            // lb_lds_break3
            // 
            this.lb_lds_break3.AutoSize = true;
            this.lb_lds_break3.Font = new System.Drawing.Font("맑은 고딕", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_lds_break3.Location = new System.Drawing.Point(6, 91);
            this.lb_lds_break3.Name = "lb_lds_break3";
            this.lb_lds_break3.Size = new System.Drawing.Size(78, 15);
            this.lb_lds_break3.TabIndex = 33;
            this.lb_lds_break3.Text = "Break3[mm]";
            // 
            // lb_lds_break2
            // 
            this.lb_lds_break2.AutoSize = true;
            this.lb_lds_break2.Font = new System.Drawing.Font("맑은 고딕", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_lds_break2.Location = new System.Drawing.Point(6, 59);
            this.lb_lds_break2.Name = "lb_lds_break2";
            this.lb_lds_break2.Size = new System.Drawing.Size(78, 15);
            this.lb_lds_break2.TabIndex = 32;
            this.lb_lds_break2.Text = "Break2[mm]";
            // 
            // tbox_lds_break1_1
            // 
            this.tbox_lds_break1_1.Location = new System.Drawing.Point(97, 21);
            this.tbox_lds_break1_1.Name = "tbox_lds_break1_1";
            this.tbox_lds_break1_1.Size = new System.Drawing.Size(69, 29);
            this.tbox_lds_break1_1.TabIndex = 30;
            // 
            // panel_lds_break1_read
            // 
            this.panel_lds_break1_read.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_lds_break1_read.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_lds_break1_read.Controls.Add(this.lb_lds_break1_read);
            this.panel_lds_break1_read.Location = new System.Drawing.Point(247, 21);
            this.panel_lds_break1_read.Name = "panel_lds_break1_read";
            this.panel_lds_break1_read.Size = new System.Drawing.Size(42, 26);
            this.panel_lds_break1_read.TabIndex = 29;
            // 
            // lb_lds_break1_read
            // 
            this.lb_lds_break1_read.AutoSize = true;
            this.lb_lds_break1_read.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_lds_break1_read.Location = new System.Drawing.Point(5, 4);
            this.lb_lds_break1_read.Name = "lb_lds_break1_read";
            this.lb_lds_break1_read.Size = new System.Drawing.Size(34, 17);
            this.lb_lds_break1_read.TabIndex = 0;
            this.lb_lds_break1_read.Text = "읽기";
            // 
            // lb_lds_break1
            // 
            this.lb_lds_break1.AutoSize = true;
            this.lb_lds_break1.Font = new System.Drawing.Font("맑은 고딕", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_lds_break1.Location = new System.Drawing.Point(6, 27);
            this.lb_lds_break1.Name = "lb_lds_break1";
            this.lb_lds_break1.Size = new System.Drawing.Size(78, 15);
            this.lb_lds_break1.TabIndex = 28;
            this.lb_lds_break1.Text = "Break1[mm]";
            // 
            // panel19
            // 
            this.panel19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel19.Controls.Add(this.btn_recipe_cim_delete);
            this.panel19.Controls.Add(this.btn_recipe_cim_apply);
            this.panel19.Controls.Add(this.btn_recipe_delete);
            this.panel19.Controls.Add(this.btn_recipe_apply);
            this.panel19.Controls.Add(this.btn_recipe_save);
            this.panel19.Controls.Add(this.btn_recipe_copy);
            this.panel19.Controls.Add(this.btn_recipe_make);
            this.panel19.ForeColor = System.Drawing.Color.White;
            this.panel19.Location = new System.Drawing.Point(284, 800);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(1453, 57);
            this.panel19.TabIndex = 65;
            // 
            // btn_recipe_cim_delete
            // 
            this.btn_recipe_cim_delete.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_recipe_cim_delete.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.btn_recipe_cim_delete.ForeColor = System.Drawing.Color.White;
            this.btn_recipe_cim_delete.Location = new System.Drawing.Point(1035, 3);
            this.btn_recipe_cim_delete.Name = "btn_recipe_cim_delete";
            this.btn_recipe_cim_delete.Size = new System.Drawing.Size(132, 50);
            this.btn_recipe_cim_delete.TabIndex = 43;
            this.btn_recipe_cim_delete.Text = "CIM Delete";
            this.btn_recipe_cim_delete.UseVisualStyleBackColor = false;
            // 
            // btn_recipe_cim_apply
            // 
            this.btn_recipe_cim_apply.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_recipe_cim_apply.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_recipe_cim_apply.ForeColor = System.Drawing.Color.White;
            this.btn_recipe_cim_apply.Location = new System.Drawing.Point(897, 3);
            this.btn_recipe_cim_apply.Name = "btn_recipe_cim_apply";
            this.btn_recipe_cim_apply.Size = new System.Drawing.Size(132, 50);
            this.btn_recipe_cim_apply.TabIndex = 42;
            this.btn_recipe_cim_apply.Text = "CIM Apply";
            this.btn_recipe_cim_apply.UseVisualStyleBackColor = false;
            // 
            // btn_recipe_delete
            // 
            this.btn_recipe_delete.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_recipe_delete.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_recipe_delete.ForeColor = System.Drawing.Color.White;
            this.btn_recipe_delete.Location = new System.Drawing.Point(551, 3);
            this.btn_recipe_delete.Name = "btn_recipe_delete";
            this.btn_recipe_delete.Size = new System.Drawing.Size(132, 50);
            this.btn_recipe_delete.TabIndex = 41;
            this.btn_recipe_delete.Text = "Delete";
            this.btn_recipe_delete.UseVisualStyleBackColor = false;
            // 
            // btn_recipe_apply
            // 
            this.btn_recipe_apply.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_recipe_apply.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_recipe_apply.ForeColor = System.Drawing.Color.White;
            this.btn_recipe_apply.Location = new System.Drawing.Point(413, 3);
            this.btn_recipe_apply.Name = "btn_recipe_apply";
            this.btn_recipe_apply.Size = new System.Drawing.Size(132, 50);
            this.btn_recipe_apply.TabIndex = 40;
            this.btn_recipe_apply.Text = "Apply";
            this.btn_recipe_apply.UseVisualStyleBackColor = false;
            // 
            // btn_recipe_save
            // 
            this.btn_recipe_save.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_recipe_save.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_recipe_save.ForeColor = System.Drawing.Color.White;
            this.btn_recipe_save.Location = new System.Drawing.Point(275, 3);
            this.btn_recipe_save.Name = "btn_recipe_save";
            this.btn_recipe_save.Size = new System.Drawing.Size(132, 50);
            this.btn_recipe_save.TabIndex = 39;
            this.btn_recipe_save.Text = "Save";
            this.btn_recipe_save.UseVisualStyleBackColor = false;
            // 
            // btn_recipe_copy
            // 
            this.btn_recipe_copy.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_recipe_copy.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_recipe_copy.ForeColor = System.Drawing.Color.White;
            this.btn_recipe_copy.Location = new System.Drawing.Point(137, 3);
            this.btn_recipe_copy.Name = "btn_recipe_copy";
            this.btn_recipe_copy.Size = new System.Drawing.Size(132, 50);
            this.btn_recipe_copy.TabIndex = 38;
            this.btn_recipe_copy.Text = "Copy";
            this.btn_recipe_copy.UseVisualStyleBackColor = false;
            // 
            // btn_recipe_make
            // 
            this.btn_recipe_make.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_recipe_make.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_recipe_make.ForeColor = System.Drawing.Color.White;
            this.btn_recipe_make.Location = new System.Drawing.Point(-1, 3);
            this.btn_recipe_make.Name = "btn_recipe_make";
            this.btn_recipe_make.Size = new System.Drawing.Size(132, 50);
            this.btn_recipe_make.TabIndex = 37;
            this.btn_recipe_make.Text = "Make";
            this.btn_recipe_make.UseVisualStyleBackColor = false;
            // 
            // gbox_lds_process
            // 
            this.gbox_lds_process.BackColor = System.Drawing.Color.Transparent;
            this.gbox_lds_process.Controls.Add(this.panel24);
            this.gbox_lds_process.Controls.Add(this.tbox_lds_process4_2);
            this.gbox_lds_process.Controls.Add(this.tbox_lds_process4_1);
            this.gbox_lds_process.Controls.Add(this.panel_lds_process4_read);
            this.gbox_lds_process.Controls.Add(this.panel_lds_process3_move);
            this.gbox_lds_process.Controls.Add(this.tbox_lds_process3_2);
            this.gbox_lds_process.Controls.Add(this.tbox_lds_process3_1);
            this.gbox_lds_process.Controls.Add(this.panel_lds_process3_read);
            this.gbox_lds_process.Controls.Add(this.panel_lds_process2_move);
            this.gbox_lds_process.Controls.Add(this.panel_lds_process1_move);
            this.gbox_lds_process.Controls.Add(this.tbox_lds_process2_2);
            this.gbox_lds_process.Controls.Add(this.tbox_lds_process2_1);
            this.gbox_lds_process.Controls.Add(this.tbox_lds_process1_2);
            this.gbox_lds_process.Controls.Add(this.panel_lds_process2_read);
            this.gbox_lds_process.Controls.Add(this.lb_lds_process4);
            this.gbox_lds_process.Controls.Add(this.lb_lds_process3);
            this.gbox_lds_process.Controls.Add(this.lb_lds_process2);
            this.gbox_lds_process.Controls.Add(this.tbox_lds_process1_1);
            this.gbox_lds_process.Controls.Add(this.panel_lds_process1_read);
            this.gbox_lds_process.Controls.Add(this.lb_lds_process1);
            this.gbox_lds_process.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.gbox_lds_process.ForeColor = System.Drawing.Color.White;
            this.gbox_lds_process.Location = new System.Drawing.Point(1394, 358);
            this.gbox_lds_process.Name = "gbox_lds_process";
            this.gbox_lds_process.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gbox_lds_process.Size = new System.Drawing.Size(343, 153);
            this.gbox_lds_process.TabIndex = 64;
            this.gbox_lds_process.TabStop = false;
            this.gbox_lds_process.Text = "LDS Process";
            // 
            // panel24
            // 
            this.panel24.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel24.Controls.Add(this.label37);
            this.panel24.Location = new System.Drawing.Point(295, 117);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(42, 26);
            this.panel24.TabIndex = 25;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.label37.Location = new System.Drawing.Point(5, 4);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(34, 17);
            this.label37.TabIndex = 0;
            this.label37.Text = "이동";
            // 
            // tbox_lds_process4_2
            // 
            this.tbox_lds_process4_2.Location = new System.Drawing.Point(172, 117);
            this.tbox_lds_process4_2.Name = "tbox_lds_process4_2";
            this.tbox_lds_process4_2.Size = new System.Drawing.Size(69, 29);
            this.tbox_lds_process4_2.TabIndex = 27;
            // 
            // tbox_lds_process4_1
            // 
            this.tbox_lds_process4_1.Location = new System.Drawing.Point(97, 117);
            this.tbox_lds_process4_1.Name = "tbox_lds_process4_1";
            this.tbox_lds_process4_1.Size = new System.Drawing.Size(69, 29);
            this.tbox_lds_process4_1.TabIndex = 26;
            // 
            // panel_lds_process4_read
            // 
            this.panel_lds_process4_read.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_lds_process4_read.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_lds_process4_read.Controls.Add(this.label38);
            this.panel_lds_process4_read.Location = new System.Drawing.Point(247, 117);
            this.panel_lds_process4_read.Name = "panel_lds_process4_read";
            this.panel_lds_process4_read.Size = new System.Drawing.Size(42, 26);
            this.panel_lds_process4_read.TabIndex = 24;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.label38.Location = new System.Drawing.Point(5, 4);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(34, 17);
            this.label38.TabIndex = 0;
            this.label38.Text = "읽기";
            // 
            // panel_lds_process3_move
            // 
            this.panel_lds_process3_move.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_lds_process3_move.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_lds_process3_move.Controls.Add(this.lb_lds_process3_move);
            this.panel_lds_process3_move.Location = new System.Drawing.Point(295, 86);
            this.panel_lds_process3_move.Name = "panel_lds_process3_move";
            this.panel_lds_process3_move.Size = new System.Drawing.Size(42, 26);
            this.panel_lds_process3_move.TabIndex = 21;
            // 
            // lb_lds_process3_move
            // 
            this.lb_lds_process3_move.AutoSize = true;
            this.lb_lds_process3_move.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_lds_process3_move.Location = new System.Drawing.Point(5, 4);
            this.lb_lds_process3_move.Name = "lb_lds_process3_move";
            this.lb_lds_process3_move.Size = new System.Drawing.Size(34, 17);
            this.lb_lds_process3_move.TabIndex = 0;
            this.lb_lds_process3_move.Text = "이동";
            // 
            // tbox_lds_process3_2
            // 
            this.tbox_lds_process3_2.Location = new System.Drawing.Point(172, 86);
            this.tbox_lds_process3_2.Name = "tbox_lds_process3_2";
            this.tbox_lds_process3_2.Size = new System.Drawing.Size(69, 29);
            this.tbox_lds_process3_2.TabIndex = 23;
            // 
            // tbox_lds_process3_1
            // 
            this.tbox_lds_process3_1.Location = new System.Drawing.Point(97, 86);
            this.tbox_lds_process3_1.Name = "tbox_lds_process3_1";
            this.tbox_lds_process3_1.Size = new System.Drawing.Size(69, 29);
            this.tbox_lds_process3_1.TabIndex = 22;
            // 
            // panel_lds_process3_read
            // 
            this.panel_lds_process3_read.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_lds_process3_read.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_lds_process3_read.Controls.Add(this.lb_lds_process3_read);
            this.panel_lds_process3_read.Location = new System.Drawing.Point(247, 86);
            this.panel_lds_process3_read.Name = "panel_lds_process3_read";
            this.panel_lds_process3_read.Size = new System.Drawing.Size(42, 26);
            this.panel_lds_process3_read.TabIndex = 20;
            // 
            // lb_lds_process3_read
            // 
            this.lb_lds_process3_read.AutoSize = true;
            this.lb_lds_process3_read.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_lds_process3_read.Location = new System.Drawing.Point(5, 4);
            this.lb_lds_process3_read.Name = "lb_lds_process3_read";
            this.lb_lds_process3_read.Size = new System.Drawing.Size(34, 17);
            this.lb_lds_process3_read.TabIndex = 0;
            this.lb_lds_process3_read.Text = "읽기";
            // 
            // panel_lds_process2_move
            // 
            this.panel_lds_process2_move.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_lds_process2_move.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_lds_process2_move.Controls.Add(this.lb_lds_process2_move);
            this.panel_lds_process2_move.Location = new System.Drawing.Point(295, 54);
            this.panel_lds_process2_move.Name = "panel_lds_process2_move";
            this.panel_lds_process2_move.Size = new System.Drawing.Size(42, 26);
            this.panel_lds_process2_move.TabIndex = 17;
            // 
            // lb_lds_process2_move
            // 
            this.lb_lds_process2_move.AutoSize = true;
            this.lb_lds_process2_move.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_lds_process2_move.Location = new System.Drawing.Point(5, 4);
            this.lb_lds_process2_move.Name = "lb_lds_process2_move";
            this.lb_lds_process2_move.Size = new System.Drawing.Size(34, 17);
            this.lb_lds_process2_move.TabIndex = 0;
            this.lb_lds_process2_move.Text = "이동";
            // 
            // panel_lds_process1_move
            // 
            this.panel_lds_process1_move.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_lds_process1_move.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_lds_process1_move.Controls.Add(this.lb_lds_process1_move);
            this.panel_lds_process1_move.Location = new System.Drawing.Point(295, 22);
            this.panel_lds_process1_move.Name = "panel_lds_process1_move";
            this.panel_lds_process1_move.Size = new System.Drawing.Size(42, 26);
            this.panel_lds_process1_move.TabIndex = 4;
            // 
            // lb_lds_process1_move
            // 
            this.lb_lds_process1_move.AutoSize = true;
            this.lb_lds_process1_move.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_lds_process1_move.Location = new System.Drawing.Point(5, 4);
            this.lb_lds_process1_move.Name = "lb_lds_process1_move";
            this.lb_lds_process1_move.Size = new System.Drawing.Size(34, 17);
            this.lb_lds_process1_move.TabIndex = 0;
            this.lb_lds_process1_move.Text = "이동";
            // 
            // tbox_lds_process2_2
            // 
            this.tbox_lds_process2_2.Location = new System.Drawing.Point(172, 54);
            this.tbox_lds_process2_2.Name = "tbox_lds_process2_2";
            this.tbox_lds_process2_2.Size = new System.Drawing.Size(69, 29);
            this.tbox_lds_process2_2.TabIndex = 19;
            // 
            // tbox_lds_process2_1
            // 
            this.tbox_lds_process2_1.Location = new System.Drawing.Point(97, 54);
            this.tbox_lds_process2_1.Name = "tbox_lds_process2_1";
            this.tbox_lds_process2_1.Size = new System.Drawing.Size(69, 29);
            this.tbox_lds_process2_1.TabIndex = 18;
            // 
            // tbox_lds_process1_2
            // 
            this.tbox_lds_process1_2.Location = new System.Drawing.Point(172, 22);
            this.tbox_lds_process1_2.Name = "tbox_lds_process1_2";
            this.tbox_lds_process1_2.Size = new System.Drawing.Size(69, 29);
            this.tbox_lds_process1_2.TabIndex = 15;
            // 
            // panel_lds_process2_read
            // 
            this.panel_lds_process2_read.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_lds_process2_read.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_lds_process2_read.Controls.Add(this.lb_lds_process2_read);
            this.panel_lds_process2_read.Location = new System.Drawing.Point(247, 54);
            this.panel_lds_process2_read.Name = "panel_lds_process2_read";
            this.panel_lds_process2_read.Size = new System.Drawing.Size(42, 26);
            this.panel_lds_process2_read.TabIndex = 16;
            // 
            // lb_lds_process2_read
            // 
            this.lb_lds_process2_read.AutoSize = true;
            this.lb_lds_process2_read.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_lds_process2_read.Location = new System.Drawing.Point(5, 4);
            this.lb_lds_process2_read.Name = "lb_lds_process2_read";
            this.lb_lds_process2_read.Size = new System.Drawing.Size(34, 17);
            this.lb_lds_process2_read.TabIndex = 0;
            this.lb_lds_process2_read.Text = "읽기";
            // 
            // lb_lds_process4
            // 
            this.lb_lds_process4.AutoSize = true;
            this.lb_lds_process4.Font = new System.Drawing.Font("맑은 고딕", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_lds_process4.Location = new System.Drawing.Point(2, 124);
            this.lb_lds_process4.Name = "lb_lds_process4";
            this.lb_lds_process4.Size = new System.Drawing.Size(88, 15);
            this.lb_lds_process4.TabIndex = 14;
            this.lb_lds_process4.Text = "Process4[mm]";
            // 
            // lb_lds_process3
            // 
            this.lb_lds_process3.AutoSize = true;
            this.lb_lds_process3.Font = new System.Drawing.Font("맑은 고딕", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_lds_process3.Location = new System.Drawing.Point(2, 92);
            this.lb_lds_process3.Name = "lb_lds_process3";
            this.lb_lds_process3.Size = new System.Drawing.Size(88, 15);
            this.lb_lds_process3.TabIndex = 13;
            this.lb_lds_process3.Text = "Process3[mm]";
            // 
            // lb_lds_process2
            // 
            this.lb_lds_process2.AutoSize = true;
            this.lb_lds_process2.Font = new System.Drawing.Font("맑은 고딕", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_lds_process2.Location = new System.Drawing.Point(2, 60);
            this.lb_lds_process2.Name = "lb_lds_process2";
            this.lb_lds_process2.Size = new System.Drawing.Size(88, 15);
            this.lb_lds_process2.TabIndex = 12;
            this.lb_lds_process2.Text = "Process2[mm]";
            // 
            // tbox_lds_process1_1
            // 
            this.tbox_lds_process1_1.Location = new System.Drawing.Point(97, 22);
            this.tbox_lds_process1_1.Name = "tbox_lds_process1_1";
            this.tbox_lds_process1_1.Size = new System.Drawing.Size(69, 29);
            this.tbox_lds_process1_1.TabIndex = 4;
            // 
            // panel_lds_process1_read
            // 
            this.panel_lds_process1_read.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_lds_process1_read.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_lds_process1_read.Controls.Add(this.lb_lds_process1_read);
            this.panel_lds_process1_read.Location = new System.Drawing.Point(247, 22);
            this.panel_lds_process1_read.Name = "panel_lds_process1_read";
            this.panel_lds_process1_read.Size = new System.Drawing.Size(42, 26);
            this.panel_lds_process1_read.TabIndex = 3;
            // 
            // lb_lds_process1_read
            // 
            this.lb_lds_process1_read.AutoSize = true;
            this.lb_lds_process1_read.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_lds_process1_read.Location = new System.Drawing.Point(5, 4);
            this.lb_lds_process1_read.Name = "lb_lds_process1_read";
            this.lb_lds_process1_read.Size = new System.Drawing.Size(34, 17);
            this.lb_lds_process1_read.TabIndex = 0;
            this.lb_lds_process1_read.Text = "읽기";
            // 
            // lb_lds_process1
            // 
            this.lb_lds_process1.AutoSize = true;
            this.lb_lds_process1.Font = new System.Drawing.Font("맑은 고딕", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_lds_process1.Location = new System.Drawing.Point(2, 28);
            this.lb_lds_process1.Name = "lb_lds_process1";
            this.lb_lds_process1.Size = new System.Drawing.Size(88, 15);
            this.lb_lds_process1.TabIndex = 1;
            this.lb_lds_process1.Text = "Process1[mm]";
            // 
            // gbox_picker
            // 
            this.gbox_picker.BackColor = System.Drawing.Color.Transparent;
            this.gbox_picker.Controls.Add(this.panel_picker_cw180_2);
            this.gbox_picker.Controls.Add(this.panel_picker_cw180_1);
            this.gbox_picker.Controls.Add(this.panel_picker_ccw90_2);
            this.gbox_picker.Controls.Add(this.panel_picker_ccw90_1);
            this.gbox_picker.Controls.Add(this.panel_picker_cw90_2);
            this.gbox_picker.Controls.Add(this.panel_picker_cw90_1);
            this.gbox_picker.Controls.Add(this.panel_picker_normal_2);
            this.gbox_picker.Controls.Add(this.panel_picker_norma_l1);
            this.gbox_picker.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.gbox_picker.ForeColor = System.Drawing.Color.White;
            this.gbox_picker.Location = new System.Drawing.Point(1394, 186);
            this.gbox_picker.Name = "gbox_picker";
            this.gbox_picker.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gbox_picker.Size = new System.Drawing.Size(343, 171);
            this.gbox_picker.TabIndex = 64;
            this.gbox_picker.TabStop = false;
            this.gbox_picker.Text = "Picker";
            // 
            // panel_picker_cw180_2
            // 
            this.panel_picker_cw180_2.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_picker_cw180_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_picker_cw180_2.Controls.Add(this.lb_picker_cw180_2);
            this.panel_picker_cw180_2.Location = new System.Drawing.Point(178, 133);
            this.panel_picker_cw180_2.Name = "panel_picker_cw180_2";
            this.panel_picker_cw180_2.Size = new System.Drawing.Size(157, 30);
            this.panel_picker_cw180_2.TabIndex = 11;
            // 
            // lb_picker_cw180_2
            // 
            this.lb_picker_cw180_2.AutoSize = true;
            this.lb_picker_cw180_2.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lb_picker_cw180_2.Location = new System.Drawing.Point(41, 5);
            this.lb_picker_cw180_2.Name = "lb_picker_cw180_2";
            this.lb_picker_cw180_2.Size = new System.Drawing.Size(70, 21);
            this.lb_picker_cw180_2.TabIndex = 0;
            this.lb_picker_cw180_2.Text = "CW_180";
            // 
            // panel_picker_cw180_1
            // 
            this.panel_picker_cw180_1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_picker_cw180_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_picker_cw180_1.Controls.Add(this.lb_picker_cw180_1);
            this.panel_picker_cw180_1.Location = new System.Drawing.Point(9, 133);
            this.panel_picker_cw180_1.Name = "panel_picker_cw180_1";
            this.panel_picker_cw180_1.Size = new System.Drawing.Size(157, 30);
            this.panel_picker_cw180_1.TabIndex = 8;
            // 
            // lb_picker_cw180_1
            // 
            this.lb_picker_cw180_1.AutoSize = true;
            this.lb_picker_cw180_1.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lb_picker_cw180_1.Location = new System.Drawing.Point(41, 5);
            this.lb_picker_cw180_1.Name = "lb_picker_cw180_1";
            this.lb_picker_cw180_1.Size = new System.Drawing.Size(70, 21);
            this.lb_picker_cw180_1.TabIndex = 0;
            this.lb_picker_cw180_1.Text = "CW_180";
            // 
            // panel_picker_ccw90_2
            // 
            this.panel_picker_ccw90_2.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_picker_ccw90_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_picker_ccw90_2.Controls.Add(this.lb_picker_ccw90_2);
            this.panel_picker_ccw90_2.Location = new System.Drawing.Point(178, 97);
            this.panel_picker_ccw90_2.Name = "panel_picker_ccw90_2";
            this.panel_picker_ccw90_2.Size = new System.Drawing.Size(157, 30);
            this.panel_picker_ccw90_2.TabIndex = 10;
            // 
            // lb_picker_ccw90_2
            // 
            this.lb_picker_ccw90_2.AutoSize = true;
            this.lb_picker_ccw90_2.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lb_picker_ccw90_2.Location = new System.Drawing.Point(39, 5);
            this.lb_picker_ccw90_2.Name = "lb_picker_ccw90_2";
            this.lb_picker_ccw90_2.Size = new System.Drawing.Size(71, 21);
            this.lb_picker_ccw90_2.TabIndex = 0;
            this.lb_picker_ccw90_2.Text = "CCW_90";
            // 
            // panel_picker_ccw90_1
            // 
            this.panel_picker_ccw90_1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_picker_ccw90_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_picker_ccw90_1.Controls.Add(this.lb_picker_ccw90_1);
            this.panel_picker_ccw90_1.Location = new System.Drawing.Point(9, 97);
            this.panel_picker_ccw90_1.Name = "panel_picker_ccw90_1";
            this.panel_picker_ccw90_1.Size = new System.Drawing.Size(157, 30);
            this.panel_picker_ccw90_1.TabIndex = 7;
            // 
            // lb_picker_ccw90_1
            // 
            this.lb_picker_ccw90_1.AutoSize = true;
            this.lb_picker_ccw90_1.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lb_picker_ccw90_1.Location = new System.Drawing.Point(39, 5);
            this.lb_picker_ccw90_1.Name = "lb_picker_ccw90_1";
            this.lb_picker_ccw90_1.Size = new System.Drawing.Size(71, 21);
            this.lb_picker_ccw90_1.TabIndex = 0;
            this.lb_picker_ccw90_1.Text = "CCW_90";
            // 
            // panel_picker_cw90_2
            // 
            this.panel_picker_cw90_2.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_picker_cw90_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_picker_cw90_2.Controls.Add(this.lb_picker_cw90_2);
            this.panel_picker_cw90_2.Location = new System.Drawing.Point(178, 61);
            this.panel_picker_cw90_2.Name = "panel_picker_cw90_2";
            this.panel_picker_cw90_2.Size = new System.Drawing.Size(157, 30);
            this.panel_picker_cw90_2.TabIndex = 9;
            // 
            // lb_picker_cw90_2
            // 
            this.lb_picker_cw90_2.AutoSize = true;
            this.lb_picker_cw90_2.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lb_picker_cw90_2.Location = new System.Drawing.Point(43, 4);
            this.lb_picker_cw90_2.Name = "lb_picker_cw90_2";
            this.lb_picker_cw90_2.Size = new System.Drawing.Size(61, 21);
            this.lb_picker_cw90_2.TabIndex = 0;
            this.lb_picker_cw90_2.Text = "CW_90";
            // 
            // panel_picker_cw90_1
            // 
            this.panel_picker_cw90_1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_picker_cw90_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_picker_cw90_1.Controls.Add(this.lb_picker_cw90_1);
            this.panel_picker_cw90_1.Location = new System.Drawing.Point(9, 61);
            this.panel_picker_cw90_1.Name = "panel_picker_cw90_1";
            this.panel_picker_cw90_1.Size = new System.Drawing.Size(157, 30);
            this.panel_picker_cw90_1.TabIndex = 6;
            // 
            // lb_picker_cw90_1
            // 
            this.lb_picker_cw90_1.AutoSize = true;
            this.lb_picker_cw90_1.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lb_picker_cw90_1.Location = new System.Drawing.Point(43, 4);
            this.lb_picker_cw90_1.Name = "lb_picker_cw90_1";
            this.lb_picker_cw90_1.Size = new System.Drawing.Size(61, 21);
            this.lb_picker_cw90_1.TabIndex = 0;
            this.lb_picker_cw90_1.Text = "CW_90";
            // 
            // panel_picker_normal_2
            // 
            this.panel_picker_normal_2.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_picker_normal_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_picker_normal_2.Controls.Add(this.lb_picker_normal_2);
            this.panel_picker_normal_2.Location = new System.Drawing.Point(178, 25);
            this.panel_picker_normal_2.Name = "panel_picker_normal_2";
            this.panel_picker_normal_2.Size = new System.Drawing.Size(157, 30);
            this.panel_picker_normal_2.TabIndex = 6;
            // 
            // lb_picker_normal_2
            // 
            this.lb_picker_normal_2.AutoSize = true;
            this.lb_picker_normal_2.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lb_picker_normal_2.Location = new System.Drawing.Point(43, 4);
            this.lb_picker_normal_2.Name = "lb_picker_normal_2";
            this.lb_picker_normal_2.Size = new System.Drawing.Size(67, 21);
            this.lb_picker_normal_2.TabIndex = 0;
            this.lb_picker_normal_2.Text = "Normal";
            // 
            // panel_picker_norma_l1
            // 
            this.panel_picker_norma_l1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_picker_norma_l1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_picker_norma_l1.Controls.Add(this.lb_picker_normal_1);
            this.panel_picker_norma_l1.Location = new System.Drawing.Point(9, 25);
            this.panel_picker_norma_l1.Name = "panel_picker_norma_l1";
            this.panel_picker_norma_l1.Size = new System.Drawing.Size(157, 30);
            this.panel_picker_norma_l1.TabIndex = 5;
            // 
            // lb_picker_normal_1
            // 
            this.lb_picker_normal_1.AutoSize = true;
            this.lb_picker_normal_1.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lb_picker_normal_1.Location = new System.Drawing.Point(43, 4);
            this.lb_picker_normal_1.Name = "lb_picker_normal_1";
            this.lb_picker_normal_1.Size = new System.Drawing.Size(67, 21);
            this.lb_picker_normal_1.TabIndex = 0;
            this.lb_picker_normal_1.Text = "Normal";
            // 
            // gbox_cassette
            // 
            this.gbox_cassette.BackColor = System.Drawing.Color.Transparent;
            this.gbox_cassette.Controls.Add(this.panel_cassette4);
            this.gbox_cassette.Controls.Add(this.panel_cassette2);
            this.gbox_cassette.Controls.Add(this.panel_cassette1);
            this.gbox_cassette.Controls.Add(this.panel_cassette_reverse);
            this.gbox_cassette.Controls.Add(this.tbox_cell_count);
            this.gbox_cassette.Controls.Add(this.lb_cell_count);
            this.gbox_cassette.Controls.Add(this.tbox_max_height_count);
            this.gbox_cassette.Controls.Add(this.lb_max_height_count);
            this.gbox_cassette.Controls.Add(this.tbox_height);
            this.gbox_cassette.Controls.Add(this.lb_height);
            this.gbox_cassette.Controls.Add(this.tbox_width);
            this.gbox_cassette.Controls.Add(this.panel_cassette_normal);
            this.gbox_cassette.Controls.Add(this.lb_width);
            this.gbox_cassette.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.gbox_cassette.ForeColor = System.Drawing.Color.White;
            this.gbox_cassette.Location = new System.Drawing.Point(1394, 3);
            this.gbox_cassette.Name = "gbox_cassette";
            this.gbox_cassette.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gbox_cassette.Size = new System.Drawing.Size(343, 182);
            this.gbox_cassette.TabIndex = 63;
            this.gbox_cassette.TabStop = false;
            this.gbox_cassette.Text = "Cassette";
            // 
            // panel_cassette4
            // 
            this.panel_cassette4.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_cassette4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_cassette4.Controls.Add(this.lb_cassette4);
            this.panel_cassette4.Location = new System.Drawing.Point(233, 150);
            this.panel_cassette4.Name = "panel_cassette4";
            this.panel_cassette4.Size = new System.Drawing.Size(105, 27);
            this.panel_cassette4.TabIndex = 7;
            // 
            // lb_cassette4
            // 
            this.lb_cassette4.AutoSize = true;
            this.lb_cassette4.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lb_cassette4.Location = new System.Drawing.Point(6, 3);
            this.lb_cassette4.Name = "lb_cassette4";
            this.lb_cassette4.Size = new System.Drawing.Size(89, 21);
            this.lb_cassette4.TabIndex = 0;
            this.lb_cassette4.Text = "Cassette_4";
            // 
            // panel_cassette2
            // 
            this.panel_cassette2.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_cassette2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_cassette2.Controls.Add(this.lb_cassette2);
            this.panel_cassette2.Location = new System.Drawing.Point(121, 150);
            this.panel_cassette2.Name = "panel_cassette2";
            this.panel_cassette2.Size = new System.Drawing.Size(105, 27);
            this.panel_cassette2.TabIndex = 6;
            // 
            // lb_cassette2
            // 
            this.lb_cassette2.AutoSize = true;
            this.lb_cassette2.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lb_cassette2.Location = new System.Drawing.Point(6, 3);
            this.lb_cassette2.Name = "lb_cassette2";
            this.lb_cassette2.Size = new System.Drawing.Size(89, 21);
            this.lb_cassette2.TabIndex = 0;
            this.lb_cassette2.Text = "Cassette_2";
            // 
            // panel_cassette1
            // 
            this.panel_cassette1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_cassette1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_cassette1.Controls.Add(this.lb_cassette1);
            this.panel_cassette1.Location = new System.Drawing.Point(9, 150);
            this.panel_cassette1.Name = "panel_cassette1";
            this.panel_cassette1.Size = new System.Drawing.Size(105, 27);
            this.panel_cassette1.TabIndex = 5;
            // 
            // lb_cassette1
            // 
            this.lb_cassette1.AutoSize = true;
            this.lb_cassette1.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lb_cassette1.Location = new System.Drawing.Point(6, 3);
            this.lb_cassette1.Name = "lb_cassette1";
            this.lb_cassette1.Size = new System.Drawing.Size(89, 21);
            this.lb_cassette1.TabIndex = 0;
            this.lb_cassette1.Text = "Cassette_1";
            // 
            // panel_cassette_reverse
            // 
            this.panel_cassette_reverse.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_cassette_reverse.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_cassette_reverse.Controls.Add(this.lb_cassette_reverse);
            this.panel_cassette_reverse.Location = new System.Drawing.Point(247, 84);
            this.panel_cassette_reverse.Name = "panel_cassette_reverse";
            this.panel_cassette_reverse.Size = new System.Drawing.Size(90, 58);
            this.panel_cassette_reverse.TabIndex = 4;
            // 
            // lb_cassette_reverse
            // 
            this.lb_cassette_reverse.AutoSize = true;
            this.lb_cassette_reverse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lb_cassette_reverse.Location = new System.Drawing.Point(6, 22);
            this.lb_cassette_reverse.Name = "lb_cassette_reverse";
            this.lb_cassette_reverse.Size = new System.Drawing.Size(78, 15);
            this.lb_cassette_reverse.TabIndex = 0;
            this.lb_cassette_reverse.Text = "REVERSE(60)";
            // 
            // tbox_cell_count
            // 
            this.tbox_cell_count.Location = new System.Drawing.Point(141, 116);
            this.tbox_cell_count.Name = "tbox_cell_count";
            this.tbox_cell_count.Size = new System.Drawing.Size(100, 29);
            this.tbox_cell_count.TabIndex = 11;
            // 
            // lb_cell_count
            // 
            this.lb_cell_count.AutoSize = true;
            this.lb_cell_count.Font = new System.Drawing.Font("맑은 고딕", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_cell_count.Location = new System.Drawing.Point(6, 123);
            this.lb_cell_count.Name = "lb_cell_count";
            this.lb_cell_count.Size = new System.Drawing.Size(66, 15);
            this.lb_cell_count.TabIndex = 10;
            this.lb_cell_count.Text = "Cell Count";
            // 
            // tbox_max_height_count
            // 
            this.tbox_max_height_count.Location = new System.Drawing.Point(141, 84);
            this.tbox_max_height_count.Name = "tbox_max_height_count";
            this.tbox_max_height_count.Size = new System.Drawing.Size(100, 29);
            this.tbox_max_height_count.TabIndex = 9;
            // 
            // lb_max_height_count
            // 
            this.lb_max_height_count.AutoSize = true;
            this.lb_max_height_count.Font = new System.Drawing.Font("맑은 고딕", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_max_height_count.Location = new System.Drawing.Point(6, 91);
            this.lb_max_height_count.Name = "lb_max_height_count";
            this.lb_max_height_count.Size = new System.Drawing.Size(113, 15);
            this.lb_max_height_count.TabIndex = 7;
            this.lb_max_height_count.Text = "Max Height Count";
            // 
            // tbox_height
            // 
            this.tbox_height.Location = new System.Drawing.Point(141, 52);
            this.tbox_height.Name = "tbox_height";
            this.tbox_height.Size = new System.Drawing.Size(100, 29);
            this.tbox_height.TabIndex = 6;
            // 
            // lb_height
            // 
            this.lb_height.AutoSize = true;
            this.lb_height.Font = new System.Drawing.Font("맑은 고딕", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_height.Location = new System.Drawing.Point(6, 58);
            this.lb_height.Name = "lb_height";
            this.lb_height.Size = new System.Drawing.Size(76, 15);
            this.lb_height.TabIndex = 5;
            this.lb_height.Text = "Height[mm]";
            // 
            // tbox_width
            // 
            this.tbox_width.Location = new System.Drawing.Point(141, 20);
            this.tbox_width.Name = "tbox_width";
            this.tbox_width.Size = new System.Drawing.Size(100, 29);
            this.tbox_width.TabIndex = 4;
            // 
            // panel_cassette_normal
            // 
            this.panel_cassette_normal.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_cassette_normal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_cassette_normal.Controls.Add(this.lb_cassette_normal);
            this.panel_cassette_normal.Location = new System.Drawing.Point(247, 20);
            this.panel_cassette_normal.Name = "panel_cassette_normal";
            this.panel_cassette_normal.Size = new System.Drawing.Size(90, 58);
            this.panel_cassette_normal.TabIndex = 3;
            // 
            // lb_cassette_normal
            // 
            this.lb_cassette_normal.AutoSize = true;
            this.lb_cassette_normal.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lb_cassette_normal.Location = new System.Drawing.Point(5, 22);
            this.lb_cassette_normal.Name = "lb_cassette_normal";
            this.lb_cassette_normal.Size = new System.Drawing.Size(82, 15);
            this.lb_cassette_normal.TabIndex = 0;
            this.lb_cassette_normal.Text = "NORMAL(40)";
            // 
            // lb_width
            // 
            this.lb_width.AutoSize = true;
            this.lb_width.Font = new System.Drawing.Font("맑은 고딕", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_width.Location = new System.Drawing.Point(6, 27);
            this.lb_width.Name = "lb_width";
            this.lb_width.Size = new System.Drawing.Size(72, 15);
            this.lb_width.TabIndex = 1;
            this.lb_width.Text = "Width[mm]";
            // 
            // listView2
            // 
            this.listView2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader5,
            this.columnHeader6});
            this.listView2.FullRowSelect = true;
            this.listView2.GridLines = true;
            this.listView2.Location = new System.Drawing.Point(3, 642);
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(275, 215);
            this.listView2.TabIndex = 1;
            this.listView2.UseCompatibleStateImageBehavior = false;
            this.listView2.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "No.";
            this.columnHeader5.Width = 45;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Name";
            this.columnHeader6.Width = 130;
            // 
            // lv_recipe
            // 
            this.lv_recipe.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.lv_recipe.FullRowSelect = true;
            this.lv_recipe.GridLines = true;
            this.lv_recipe.Location = new System.Drawing.Point(3, 3);
            this.lv_recipe.Name = "lv_recipe";
            this.lv_recipe.Size = new System.Drawing.Size(275, 624);
            this.lv_recipe.TabIndex = 0;
            this.lv_recipe.UseCompatibleStateImageBehavior = false;
            this.lv_recipe.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "No.";
            this.columnHeader1.Width = 45;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Name";
            this.columnHeader2.Width = 130;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "CIM";
            this.columnHeader3.Width = 45;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Use";
            this.columnHeader4.Width = 45;
            // 
            // Recipe_Recipe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "Recipe_Recipe";
            this.Size = new System.Drawing.Size(1740, 860);
            this.splitContainer1.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.gbox_layerinfo.ResumeLayout(false);
            this.gbox_layerinfo.PerformLayout();
            this.panel_dxfpath_load.ResumeLayout(false);
            this.panel_dxfpath_load.PerformLayout();
            this.gbox_item.ResumeLayout(false);
            this.gbox_item.PerformLayout();
            this.panel_z_matrix.ResumeLayout(false);
            this.panel_z_matrix.PerformLayout();
            this.panel53.ResumeLayout(false);
            this.panel53.PerformLayout();
            this.panel_break_left_y_calc.ResumeLayout(false);
            this.panel_break_left_y_calc.PerformLayout();
            this.panel_break_right_2_calc.ResumeLayout(false);
            this.panel_break_right_2_calc.PerformLayout();
            this.panel_break_left_2_calc.ResumeLayout(false);
            this.panel_break_left_2_calc.PerformLayout();
            this.panel55.ResumeLayout(false);
            this.panel55.PerformLayout();
            this.panel_break_left_y_move.ResumeLayout(false);
            this.panel_break_left_y_move.PerformLayout();
            this.panel56.ResumeLayout(false);
            this.panel56.PerformLayout();
            this.panel_break_left_y_read.ResumeLayout(false);
            this.panel_break_left_y_read.PerformLayout();
            this.panel_break_right_1_calc.ResumeLayout(false);
            this.panel_break_right_1_calc.PerformLayout();
            this.panel_break_left_1_calc.ResumeLayout(false);
            this.panel_break_left_1_calc.PerformLayout();
            this.panel_break_right_2_move.ResumeLayout(false);
            this.panel_break_right_2_move.PerformLayout();
            this.panel_break_left_2_move.ResumeLayout(false);
            this.panel_break_left_2_move.PerformLayout();
            this.panel_table2_alignmark2_calc.ResumeLayout(false);
            this.panel_table2_alignmark2_calc.PerformLayout();
            this.panel_break_right_2_read.ResumeLayout(false);
            this.panel_break_right_2_read.PerformLayout();
            this.panel4_break_left_2_read.ResumeLayout(false);
            this.panel4_break_left_2_read.PerformLayout();
            this.panel_break_right_1_move.ResumeLayout(false);
            this.panel_break_right_1_move.PerformLayout();
            this.panel_break_left_1_move.ResumeLayout(false);
            this.panel_break_left_1_move.PerformLayout();
            this.panel_break_right_1_read.ResumeLayout(false);
            this.panel_break_right_1_read.PerformLayout();
            this.panel_table2_alignmark1_calc.ResumeLayout(false);
            this.panel_table2_alignmark1_calc.PerformLayout();
            this.panel_break_left_1_read.ResumeLayout(false);
            this.panel_break_left_1_read.PerformLayout();
            this.panel_table2_alignmark2_move.ResumeLayout(false);
            this.panel_table2_alignmark2_move.PerformLayout();
            this.panel_table1_alignmark2_calc.ResumeLayout(false);
            this.panel_table1_alignmark2_calc.PerformLayout();
            this.panel_table2_alignmark2_read.ResumeLayout(false);
            this.panel_table2_alignmark2_read.PerformLayout();
            this.panel_table2_alignmark1_move.ResumeLayout(false);
            this.panel_table2_alignmark1_move.PerformLayout();
            this.panel_table2_alignmark1_read.ResumeLayout(false);
            this.panel_table2_alignmark1_read.PerformLayout();
            this.panel_table1_alignmark1_calc.ResumeLayout(false);
            this.panel_table1_alignmark1_calc.PerformLayout();
            this.panel_table1_alignmark2_move.ResumeLayout(false);
            this.panel_table1_alignmark2_move.PerformLayout();
            this.panel_table1_alignmark1_move.ResumeLayout(false);
            this.panel_table1_alignmark1_move.PerformLayout();
            this.panel_table1_alignmark2_read.ResumeLayout(false);
            this.panel_table1_alignmark2_read.PerformLayout();
            this.panel_table1_alignmark1_read.ResumeLayout(false);
            this.panel_table1_alignmark1_read.PerformLayout();
            this.panel_align.ResumeLayout(false);
            this.panel_align.PerformLayout();
            this.panel_recipe_load.ResumeLayout(false);
            this.panel_recipe_load.PerformLayout();
            this.gbox_y_offset.ResumeLayout(false);
            this.gbox_y_offset.PerformLayout();
            this.gbox_lds_break.ResumeLayout(false);
            this.gbox_lds_break.PerformLayout();
            this.panel_lds_break4_move.ResumeLayout(false);
            this.panel_lds_break4_move.PerformLayout();
            this.panel_lds_break4_read.ResumeLayout(false);
            this.panel_lds_break4_read.PerformLayout();
            this.panel_lds_break3_move.ResumeLayout(false);
            this.panel_lds_break3_move.PerformLayout();
            this.panel_lds_break3_read.ResumeLayout(false);
            this.panel_lds_break3_read.PerformLayout();
            this.panel_lds_break2_move.ResumeLayout(false);
            this.panel_lds_break2_move.PerformLayout();
            this.panel_lds_break1_move.ResumeLayout(false);
            this.panel_lds_break1_move.PerformLayout();
            this.panel_lds_break2_read.ResumeLayout(false);
            this.panel_lds_break2_read.PerformLayout();
            this.panel_lds_break1_read.ResumeLayout(false);
            this.panel_lds_break1_read.PerformLayout();
            this.panel19.ResumeLayout(false);
            this.gbox_lds_process.ResumeLayout(false);
            this.gbox_lds_process.PerformLayout();
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            this.panel_lds_process4_read.ResumeLayout(false);
            this.panel_lds_process4_read.PerformLayout();
            this.panel_lds_process3_move.ResumeLayout(false);
            this.panel_lds_process3_move.PerformLayout();
            this.panel_lds_process3_read.ResumeLayout(false);
            this.panel_lds_process3_read.PerformLayout();
            this.panel_lds_process2_move.ResumeLayout(false);
            this.panel_lds_process2_move.PerformLayout();
            this.panel_lds_process1_move.ResumeLayout(false);
            this.panel_lds_process1_move.PerformLayout();
            this.panel_lds_process2_read.ResumeLayout(false);
            this.panel_lds_process2_read.PerformLayout();
            this.panel_lds_process1_read.ResumeLayout(false);
            this.panel_lds_process1_read.PerformLayout();
            this.gbox_picker.ResumeLayout(false);
            this.panel_picker_cw180_2.ResumeLayout(false);
            this.panel_picker_cw180_2.PerformLayout();
            this.panel_picker_cw180_1.ResumeLayout(false);
            this.panel_picker_cw180_1.PerformLayout();
            this.panel_picker_ccw90_2.ResumeLayout(false);
            this.panel_picker_ccw90_2.PerformLayout();
            this.panel_picker_ccw90_1.ResumeLayout(false);
            this.panel_picker_ccw90_1.PerformLayout();
            this.panel_picker_cw90_2.ResumeLayout(false);
            this.panel_picker_cw90_2.PerformLayout();
            this.panel_picker_cw90_1.ResumeLayout(false);
            this.panel_picker_cw90_1.PerformLayout();
            this.panel_picker_normal_2.ResumeLayout(false);
            this.panel_picker_normal_2.PerformLayout();
            this.panel_picker_norma_l1.ResumeLayout(false);
            this.panel_picker_norma_l1.PerformLayout();
            this.gbox_cassette.ResumeLayout(false);
            this.gbox_cassette.PerformLayout();
            this.panel_cassette4.ResumeLayout(false);
            this.panel_cassette4.PerformLayout();
            this.panel_cassette2.ResumeLayout(false);
            this.panel_cassette2.PerformLayout();
            this.panel_cassette1.ResumeLayout(false);
            this.panel_cassette1.PerformLayout();
            this.panel_cassette_reverse.ResumeLayout(false);
            this.panel_cassette_reverse.PerformLayout();
            this.panel_cassette_normal.ResumeLayout(false);
            this.panel_cassette_normal.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ListView lv_recipe;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.GroupBox gbox_cassette;
        private System.Windows.Forms.Panel panel_cassette_normal;
        private System.Windows.Forms.Label lb_cassette_normal;
        private System.Windows.Forms.Label lb_width;
        private System.Windows.Forms.Panel panel_cassette4;
        private System.Windows.Forms.Label lb_cassette4;
        private System.Windows.Forms.Panel panel_cassette2;
        private System.Windows.Forms.Label lb_cassette2;
        private System.Windows.Forms.Panel panel_cassette1;
        private System.Windows.Forms.Label lb_cassette1;
        private System.Windows.Forms.Panel panel_cassette_reverse;
        private System.Windows.Forms.Label lb_cassette_reverse;
        private System.Windows.Forms.TextBox tbox_cell_count;
        private System.Windows.Forms.Label lb_cell_count;
        private System.Windows.Forms.TextBox tbox_max_height_count;
        private System.Windows.Forms.Label lb_max_height_count;
        private System.Windows.Forms.TextBox tbox_height;
        private System.Windows.Forms.Label lb_height;
        private System.Windows.Forms.TextBox tbox_width;
        private System.Windows.Forms.GroupBox gbox_picker;
        private System.Windows.Forms.Panel panel_picker_cw180_2;
        private System.Windows.Forms.Label lb_picker_cw180_2;
        private System.Windows.Forms.Panel panel_picker_cw180_1;
        private System.Windows.Forms.Label lb_picker_cw180_1;
        private System.Windows.Forms.Panel panel_picker_ccw90_2;
        private System.Windows.Forms.Label lb_picker_ccw90_2;
        private System.Windows.Forms.Panel panel_picker_ccw90_1;
        private System.Windows.Forms.Label lb_picker_ccw90_1;
        private System.Windows.Forms.Panel panel_picker_cw90_2;
        private System.Windows.Forms.Label lb_picker_cw90_2;
        private System.Windows.Forms.Panel panel_picker_cw90_1;
        private System.Windows.Forms.Label lb_picker_cw90_1;
        private System.Windows.Forms.Panel panel_picker_normal_2;
        private System.Windows.Forms.Label lb_picker_normal_2;
        private System.Windows.Forms.Panel panel_picker_norma_l1;
        private System.Windows.Forms.Label lb_picker_normal_1;
        private System.Windows.Forms.GroupBox gbox_lds_process;
        private System.Windows.Forms.TextBox tbox_lds_process1_1;
        private System.Windows.Forms.Panel panel_lds_process1_read;
        private System.Windows.Forms.Label lb_lds_process1_read;
        private System.Windows.Forms.Label lb_lds_process1;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Button btn_recipe_delete;
        private System.Windows.Forms.Button btn_recipe_apply;
        private System.Windows.Forms.Button btn_recipe_save;
        private System.Windows.Forms.Button btn_recipe_copy;
        private System.Windows.Forms.Button btn_recipe_make;
        private System.Windows.Forms.Button btn_recipe_cim_delete;
        private System.Windows.Forms.Button btn_recipe_cim_apply;
        private System.Windows.Forms.GroupBox gbox_y_offset;
        private System.Windows.Forms.TextBox tbox_y_offset_break;
        private System.Windows.Forms.Label lb_y_offset_break;
        private System.Windows.Forms.TextBox tbox_y_offset_pre;
        private System.Windows.Forms.Label lb_y_offset_pre;
        private System.Windows.Forms.GroupBox gbox_lds_break;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox tbox_lds_process4_2;
        private System.Windows.Forms.TextBox tbox_lds_process4_1;
        private System.Windows.Forms.Panel panel_lds_process4_read;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Panel panel_lds_process3_move;
        private System.Windows.Forms.Label lb_lds_process3_move;
        private System.Windows.Forms.TextBox tbox_lds_process3_2;
        private System.Windows.Forms.TextBox tbox_lds_process3_1;
        private System.Windows.Forms.Panel panel_lds_process3_read;
        private System.Windows.Forms.Label lb_lds_process3_read;
        private System.Windows.Forms.Panel panel_lds_process2_move;
        private System.Windows.Forms.Label lb_lds_process2_move;
        private System.Windows.Forms.Panel panel_lds_process1_move;
        private System.Windows.Forms.Label lb_lds_process1_move;
        private System.Windows.Forms.TextBox tbox_lds_process2_2;
        private System.Windows.Forms.TextBox tbox_lds_process2_1;
        private System.Windows.Forms.TextBox tbox_lds_process1_2;
        private System.Windows.Forms.Panel panel_lds_process2_read;
        private System.Windows.Forms.Label lb_lds_process2_read;
        private System.Windows.Forms.Label lb_lds_process4;
        private System.Windows.Forms.Label lb_lds_process3;
        private System.Windows.Forms.Label lb_lds_process2;
        private System.Windows.Forms.Panel panel_lds_break4_move;
        private System.Windows.Forms.Label lb_lds_break4_move;
        private System.Windows.Forms.TextBox tbox_lds_break4_2;
        private System.Windows.Forms.TextBox tbox_lds_break4_1;
        private System.Windows.Forms.Panel panel_lds_break4_read;
        private System.Windows.Forms.Label lb_lds_break4_read;
        private System.Windows.Forms.Panel panel_lds_break3_move;
        private System.Windows.Forms.Label lb_lds_break3_move;
        private System.Windows.Forms.TextBox tbox_lds_break3_2;
        private System.Windows.Forms.TextBox tbox_lds_break3_1;
        private System.Windows.Forms.Panel panel_lds_break3_read;
        private System.Windows.Forms.Label lb_lds_break3_read;
        private System.Windows.Forms.Panel panel_lds_break2_move;
        private System.Windows.Forms.Label lb_lds_break2_move;
        private System.Windows.Forms.Panel panel_lds_break1_move;
        private System.Windows.Forms.Label lb_lds_break1_move;
        private System.Windows.Forms.TextBox tbox_lds_break2_2;
        private System.Windows.Forms.TextBox tbox_lds_break2_1;
        private System.Windows.Forms.TextBox tbox_lds_break1_2;
        private System.Windows.Forms.Panel panel_lds_break2_read;
        private System.Windows.Forms.Label lb_lds_break2_read;
        private System.Windows.Forms.Label lb_lds_break4;
        private System.Windows.Forms.Label lb_lds_break3;
        private System.Windows.Forms.Label lb_lds_break2;
        private System.Windows.Forms.TextBox tbox_lds_break1_1;
        private System.Windows.Forms.Panel panel_lds_break1_read;
        private System.Windows.Forms.Label lb_lds_break1_read;
        private System.Windows.Forms.Label lb_lds_break1;
        private System.Windows.Forms.GroupBox gbox_dxfinfo;
        private System.Windows.Forms.GroupBox gbox_layerinfo;
        private System.Windows.Forms.GroupBox gbox_item;
        private System.Windows.Forms.TextBox tbox_recipe_path;
        private System.Windows.Forms.TextBox tbox_recipe;
        private System.Windows.Forms.Label lb_recipe;
        private System.Windows.Forms.Panel panel_recipe_load;
        private System.Windows.Forms.Label lb_recipe_load;
        private System.Windows.Forms.Panel panel_align;
        private System.Windows.Forms.Label lb_align;
        private System.Windows.Forms.Panel panel_table2_alignmark2_calc;
        private System.Windows.Forms.Label lb_table2_alignmark2_calc;
        private System.Windows.Forms.Panel panel_table2_alignmark1_calc;
        private System.Windows.Forms.Label lb_table2_alignmark1_calc;
        private System.Windows.Forms.Panel panel_table2_alignmark2_move;
        private System.Windows.Forms.Label lb_table2_alignmark2_move;
        private System.Windows.Forms.Panel panel_table1_alignmark2_calc;
        private System.Windows.Forms.Label lb_table1_alignmark2_calc;
        private System.Windows.Forms.Panel panel_table2_alignmark2_read;
        private System.Windows.Forms.Label lb_table2_alignmark2_read;
        private System.Windows.Forms.Panel panel_table2_alignmark1_move;
        private System.Windows.Forms.Label lb_table2_alignmark1_move;
        private System.Windows.Forms.TextBox tbox_table2_alignmark2_2;
        private System.Windows.Forms.Panel panel_table2_alignmark1_read;
        private System.Windows.Forms.Label lb_table2_alignmark1_read;
        private System.Windows.Forms.TextBox tbox_table2_alignmark2_1;
        private System.Windows.Forms.Label lb_table2_alignmark2;
        private System.Windows.Forms.Panel panel_table1_alignmark1_calc;
        private System.Windows.Forms.Label lb_table1_alignmark1_calc;
        private System.Windows.Forms.TextBox tbox_table2_alignmark1_2;
        private System.Windows.Forms.Panel panel_table1_alignmark2_move;
        private System.Windows.Forms.Label lb_table1_alignmark2_move;
        private System.Windows.Forms.TextBox tbox_table2_alignmark1_1;
        private System.Windows.Forms.Panel panel_table1_alignmark1_move;
        private System.Windows.Forms.Label lb_table1_alignmark1_move;
        private System.Windows.Forms.Label lb_table2_alignmark1;
        private System.Windows.Forms.Panel panel_table1_alignmark2_read;
        private System.Windows.Forms.Label lb_table1_alignmark2_read;
        private System.Windows.Forms.TextBox tbox_table1_alignmark2_2;
        private System.Windows.Forms.Panel panel_table1_alignmark1_read;
        private System.Windows.Forms.Label lb_table1_alignmark1_read;
        private System.Windows.Forms.TextBox tbox_table1_alignmark2_1;
        private System.Windows.Forms.TextBox tbox_table1_alignmark1_2;
        private System.Windows.Forms.Label lb_table1_alignmark2;
        private System.Windows.Forms.TextBox tbox_table1_alignmark1_1;
        private System.Windows.Forms.Label lb_table1_alignmark1;
        private System.Windows.Forms.TextBox tbox_cell_size_2;
        private System.Windows.Forms.TextBox tbox_cell_size_1;
        private System.Windows.Forms.Label lb_cell_size;
        private System.Windows.Forms.Panel panel_z_matrix;
        private System.Windows.Forms.Label lb_z_matrix;
        private System.Windows.Forms.Label lb_a_matrix;
        private System.Windows.Forms.Panel panel53;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Panel panel_break_left_y_calc;
        private System.Windows.Forms.Label lb_break_left_y_calc;
        private System.Windows.Forms.Panel panel_break_right_2_calc;
        private System.Windows.Forms.Label lb_break_right_2_calc;
        private System.Windows.Forms.Panel panel_break_left_2_calc;
        private System.Windows.Forms.Label lb_break_left_2_calc;
        private System.Windows.Forms.Panel panel55;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Panel panel_break_left_y_move;
        private System.Windows.Forms.Label lb_break_left_y_move;
        private System.Windows.Forms.Panel panel56;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Panel panel_break_left_y_read;
        private System.Windows.Forms.Label lb_break_left_y_read;
        private System.Windows.Forms.Panel panel_break_right_1_calc;
        private System.Windows.Forms.Label lb_break_right_1_calc;
        private System.Windows.Forms.Panel panel_break_left_1_calc;
        private System.Windows.Forms.Label lb_break_left_1_calc;
        private System.Windows.Forms.Panel panel_break_right_2_move;
        private System.Windows.Forms.Label lb_break_right_2_move;
        private System.Windows.Forms.Panel panel_break_left_2_move;
        private System.Windows.Forms.Label lb_break_left_2_move;
        private System.Windows.Forms.TextBox tbox_break_right_y;
        private System.Windows.Forms.TextBox tbox_break_left_y;
        private System.Windows.Forms.Label lb_break_right_y;
        private System.Windows.Forms.Panel panel_break_right_2_read;
        private System.Windows.Forms.Label lb_break_right_2_read;
        private System.Windows.Forms.Label lb_break_left_y;
        private System.Windows.Forms.TextBox tbox_break_right_2_2;
        private System.Windows.Forms.Panel panel4_break_left_2_read;
        private System.Windows.Forms.Label lb_break_left_2_read;
        private System.Windows.Forms.Panel panel_break_right_1_move;
        private System.Windows.Forms.Label lb_break_right_1_move;
        private System.Windows.Forms.TextBox tbox_break_left_2_2;
        private System.Windows.Forms.TextBox tbox_break_right_2_1;
        private System.Windows.Forms.Panel panel_break_left_1_move;
        private System.Windows.Forms.Label lb_break_left_1_move;
        private System.Windows.Forms.Label lb_break_right_2;
        private System.Windows.Forms.TextBox tbox_break_left_2_1;
        private System.Windows.Forms.Panel panel_break_right_1_read;
        private System.Windows.Forms.Label lb_break_right_1_read;
        private System.Windows.Forms.TextBox tbox_break_right_1_2;
        private System.Windows.Forms.TextBox tbox_break_right_1_1;
        private System.Windows.Forms.Label lb_break_left_2;
        private System.Windows.Forms.Label lb_break_right_1;
        private System.Windows.Forms.Panel panel_break_left_1_read;
        private System.Windows.Forms.Label lb_break_left_1_read;
        private System.Windows.Forms.TextBox tbox_break_left_1_2;
        private System.Windows.Forms.TextBox tbox_break_left_1_1;
        private System.Windows.Forms.Label lb_break_left_1;
        private System.Windows.Forms.Panel panel_dxfpath_load;
        private System.Windows.Forms.Label lb_dxfpath_load;
        private System.Windows.Forms.TextBox tbox_dxfpath;
        private System.Windows.Forms.Label lb_dxfpath;
        private System.Windows.Forms.Label lb_ta_thickness;
        private System.Windows.Forms.TextBox tbox_ta_thickness;
        private System.Windows.Forms.ComboBox combox_cell_type;
        private System.Windows.Forms.Label lb_cell_type;
        private System.Windows.Forms.ComboBox combox_process_param;
        private System.Windows.Forms.Label lb_process_param;
        private System.Windows.Forms.ComboBox combox_guide_layer;
        private System.Windows.Forms.Label lb_guide_layer;
        private System.Windows.Forms.ComboBox combox_process_layer;
        private System.Windows.Forms.Label lb_process_layer;
        private System.Windows.Forms.ComboBox combox_alignment_layer;
        private System.Windows.Forms.Label lb_alignment_layer;
    }
}
