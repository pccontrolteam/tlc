﻿namespace DIT.TLC.UI
{
    partial class Recipe_SubMenu
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_sub_procondition = new System.Windows.Forms.Button();
            this.btn_sub_recipe = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_sub_procondition
            // 
            this.btn_sub_procondition.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_sub_procondition.Font = new System.Drawing.Font("나눔고딕 ExtraBold", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_sub_procondition.ForeColor = System.Drawing.Color.White;
            this.btn_sub_procondition.Location = new System.Drawing.Point(2, 79);
            this.btn_sub_procondition.Name = "btn_sub_procondition";
            this.btn_sub_procondition.Size = new System.Drawing.Size(132, 50);
            this.btn_sub_procondition.TabIndex = 39;
            this.btn_sub_procondition.Text = "가공조건";
            this.btn_sub_procondition.UseVisualStyleBackColor = false;
            this.btn_sub_procondition.Click += new System.EventHandler(this.btn_sub_procondition_Click);
            // 
            // btn_sub_recipe
            // 
            this.btn_sub_recipe.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_sub_recipe.Font = new System.Drawing.Font("나눔고딕 ExtraBold", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_sub_recipe.ForeColor = System.Drawing.Color.White;
            this.btn_sub_recipe.Location = new System.Drawing.Point(2, 23);
            this.btn_sub_recipe.Name = "btn_sub_recipe";
            this.btn_sub_recipe.Size = new System.Drawing.Size(132, 50);
            this.btn_sub_recipe.TabIndex = 38;
            this.btn_sub_recipe.Text = "레시피";
            this.btn_sub_recipe.UseVisualStyleBackColor = false;
            this.btn_sub_recipe.Click += new System.EventHandler(this.btn_sub_recipe_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.Color.DimGray;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.DarkSlateGray;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.DarkSlateGray;
            this.splitContainer1.Panel2.Controls.Add(this.btn_sub_recipe);
            this.splitContainer1.Panel2.Controls.Add(this.btn_sub_procondition);
            this.splitContainer1.Size = new System.Drawing.Size(1880, 860);
            this.splitContainer1.SplitterDistance = 1740;
            this.splitContainer1.TabIndex = 40;
            // 
            // Recipe_SubMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.Controls.Add(this.splitContainer1);
            this.Name = "Recipe_SubMenu";
            this.Size = new System.Drawing.Size(1880, 860);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_sub_procondition;
        private System.Windows.Forms.Button btn_sub_recipe;
        private System.Windows.Forms.SplitContainer splitContainer1;
    }
}
