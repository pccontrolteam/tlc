﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dit.Framework.PLC;
using Dit.Framework.Comm;
using DIT.TLC.CTRL;
using System.IO;
using Dit.Framework.Log;
using DIT.TLC.UI.LOG;
using System.Windows.Forms;

namespace DIT.TLC.UI
{
    public partial class FrmMain : Form
    {
        private Login loginform = new Login();                          // 로그인 UI
        private MainWindow mainform = new MainWindow();
        private Recipe_SubMenu recipe_smform = new Recipe_SubMenu();    // 레시피서브메뉴 UI
        private UcrlManagerSubMenu ucrlMgrSubManu = new UcrlManagerSubMenu(); // 유지보수서브메뉴 UI
        private Parameter_SubMenu parameter_smform = new Parameter_SubMenu(); // 파라미터서브메뉴 UI
        private Log_SubMenu log_submenu = new Log_SubMenu();            // 로그서브메뉴 UI
        private Measure.Measure measure = new Measure.Measure();        // 측정 UI
        

        public FrmMain()
        {
            InitializeComponent();

            InitalizeUserControlUI();


            splitContainer3.Panel1.Controls.Clear();
            splitContainer3.Panel1.Controls.Add(loginform);
            loginform.Location = new Point(0, 0);
        }

        // 메뉴 버튼 클릭시 색 변경 함수
        Button SelectedButton = null;
        private void MenuButtonColorChange(object sender, EventArgs e)
        {
            if (SelectedButton != null)
                SelectedButton.BackColor = Color.DarkSlateGray;

            SelectedButton = sender as Button;
            SelectedButton.BackColor = Color.Blue;
        }

        // 종료 버튼 클릭
        private void btn_exit_Click(object sender, EventArgs e)
        {

        }

        // 로그인 버튼 클릭
        private void btn_login_Click(object sender, EventArgs e)
        {
            splitContainer3.Panel1.Controls.Clear();
            splitContainer3.Panel1.Controls.Add(loginform);
            loginform.Location = new Point(0, 0);

            MenuButtonColorChange(sender, e);
        }

        // 메인화면 버튼 클릭
        private void btn_main_Click(object sender, EventArgs e)
        {
            splitContainer3.Panel1.Controls.Clear();
            splitContainer3.Panel1.Controls.Add(mainform);
            mainform.Location = new Point(0, 0);
            
            MenuButtonColorChange(sender, e);
        }

        // 레시피 버튼 클릭
        private void btn_recipe_Click(object sender, EventArgs e)
        {
            splitContainer3.Panel1.Controls.Clear();
            splitContainer3.Panel1.Controls.Add(recipe_smform);
            recipe_smform.Location = new Point(0, 0);

            MenuButtonColorChange(sender, e);
        }

        // 유지보수 버튼 클릭
        private void btn_manager_Click(object sender, EventArgs e)
        {
            splitContainer3.Panel1.Controls.Clear();
            splitContainer3.Panel1.Controls.Add(ucrlMgrSubManu);
            ucrlMgrSubManu.Location = new Point(0, 0);

            MenuButtonColorChange(sender, e);
        }

        // 파라미터 버튼 클릭
        private void btn_param_Click(object sender, EventArgs e)
        {
            splitContainer3.Panel1.Controls.Clear();
            splitContainer3.Panel1.Controls.Add(parameter_smform);
            parameter_smform.Location = new Point(0, 0);

            MenuButtonColorChange(sender, e);
        }

        // 로그 버튼 클릭
        private void btn_log_Click(object sender, EventArgs e)
        {
            splitContainer3.Panel1.Controls.Clear();
            splitContainer3.Panel1.Controls.Add(log_submenu);
            log_submenu.Location = new Point(0, 0);

            MenuButtonColorChange(sender, e);
        }

        // 측정 버튼 클릭
        private void btn_measure_Click(object sender, EventArgs e)
        {
            splitContainer3.Panel1.Controls.Clear();
            splitContainer3.Panel1.Controls.Add(measure);
            measure.Location = new Point(0, 0);

            MenuButtonColorChange(sender, e);
        }

        // 그래프 버튼 클릭
        private void btn_graph_Click(object sender, EventArgs e)
        {
            MenuButtonColorChange(sender, e);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            digitalTimer1.DigitText = DateTime.Now.ToString("HH:mm:ss");
        }

        private void InitalizeUserControlUI()
        {
            //ExtensionUI.AddClickEventLog(this);

            GG.Equip = new Equipment();

            if (GG.TestMode)
            {
                Logger.Log = new SimpleFileLoggerMark6(Path.Combine(Application.StartupPath, "Log"), "CLIENT", 500, 1024 * 1024 * 20, true, null);
                Logger.AlarmLog = new SimpleFileLoggerMark6(Path.Combine(Application.StartupPath, "AlarmLog"), "CLIENT", 500, 1024 * 1024 * 20, true, null);
                Logger.Hsms = new SimpleFileLoggerMark6(Path.Combine(Application.StartupPath, "Log"), "HSMS", 500, 1024 * 1024 * 20, true, null);
            }
            else
            {
                System.IO.DirectoryInfo Di = new System.IO.DirectoryInfo("E:/DitCtrl");
                if (Di.Exists)
                {
                    Logger.Log = new SimpleFileLoggerMark6(Path.Combine("E:/DitCtrl", "Log"), "CLIENT", 500, 1024 * 1024 * 20, true, null);
                    Logger.AlarmLog = new SimpleFileLoggerMark6(Path.Combine("E:/DitCtrl", "Log"), "CLIENT", 500, 1024 * 1024 * 20, true, null);
                    Logger.Hsms = new SimpleFileLoggerMark6(Path.Combine("E:/DitCtrl", "Log"), "HSMS", 500, 1024 * 1024 * 20, true, null);
                }
                else
                {
                    Logger.Log = new SimpleFileLoggerMark6(Path.Combine(Application.StartupPath, "Log"), "CLIENT", 500, 1024 * 1024 * 20, true, null);
                    Logger.AlarmLog = new SimpleFileLoggerMark6(Path.Combine(Application.StartupPath, "AlarmLog"), "CLIENT", 500, 1024 * 1024 * 20, true, null);
                    Logger.Hsms = new SimpleFileLoggerMark6(Path.Combine(Application.StartupPath, "Log"), "HSMS", 500, 1024 * 1024 * 20, true,   null);
                }
            }


            if (false == GG.TestMode)
            {
                //GG.CCLINK = (IVirtualMem)new VirtualCCLink(81, -1,  "CCLINK");
                //GG.CCLINK = (IVirtualMem)new VirtualCCLinkEx(81, 0xff, -1, "CCLINK");
                //GG.PMAC = (IVirtualMem)(new VirtualUMac("PMAC", Properties.Settings.Default.PmacIP, Properties.Settings.Default.PmacPort));
                //GG.HSMS = (IVirtualMem)new VirtualCCLinkEx(151, 255, 0, "CCLINK") { IsDirect = true };
                //GG.MACRO = (IVirtualMem)new VirtualShare("DIT.PLC.MACRO_MEM.S", 102400);
                //GG.MEM_DIT = (IVirtualMem)new VirtualShare("DIT.CTRL.SHARE.MEM", 102400);
            }
            else
            {
                GG.CCLINK = (IVirtualMem)new VirtualMem("CCLINK");
                GG.PMAC = (IVirtualMem)new VirtualMem("PMAC");
                //GG.HSMS = (IVirtualMem)new VirtualCCLinkEx(151, 255, 0, "CCLINK") { IsDirect = true };
                GG.HSMS = (IVirtualMem)new VirtualShare("DIT.CTRL.HSMS.MEM", 102400);
                GG.MEM_DIT = (IVirtualMem)new VirtualShare("DIT.CTRL.SHARE.MEM", 102400);
            }

            int cclinkResult = GG.CCLINK.Open();
            int pmacResult = GG.PMAC.Open();
            int hsmsResult = GG.HSMS.Open();
            int ditMemResult = GG.MEM_DIT.Open();

            AddressMgr.Load(GG.ADDRESS_CC_LINK,             /**/GG.CCLINK);
            AddressMgr.Load(GG.ADDRESS_PMAC,                /**/GG.PMAC);
            AddressMgr.Load(GG.ADDRESS_PIO,                 /**/GG.CCLINK);

            GG.Equip.SetAddress();
            GG.Equip.StartLogic();

 
            //UI 
            ucrlMgrSubManu.InitalizeUserControlUI(GG.Equip);



            //_equip.Initalize();

            //_equip.PMac.Initailize();
            //_equip.ReviPc.Initailize(GG.MEM_DIT);
            //_equip.InspPc.Initailize(GG.MEM_DIT);
            //_equip.MacroPc.Initailize(GG.MACRO);
            //_equip.HsmsPc.Initailize(GG.HSMS);
            //_equip.InitalizeAlarmList();
            //_equip.UpdateDelaySetting();
            //_equip.UpdateCimDelaySetting();

            //_logicWorker.Equip = _equip;
            //_logicWorker.Start();

            //btnGlassInfo_Click(btnLoadingGlassInfo, null);

            //tmrUiUpdate.Start();

            //if (GG.TestMode == false)
            //{
            //    this.SetDesktopBounds(0, 0, 384, 1080);
            //    buttonExpanding.Text = "Details";
            //    _isExpending = false;
            //}
            //else
            //    FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;

            //ChangeView(ref pnlEquipDraw);

            //_equip.IsLongTest = false;
            //_equip.IsCycleStop = EmCycleStop.None;

            //_frmOperatorCall = new FrmOperatorCall(_equip);

            //_tabCtrl_Operator = new FrmOperator(_equip);
            //_tabCtrl_Operator.FormBorderStyle = FormBorderStyle.None;
            //_tabCtrl_Operator.Name = "FrmOpMain";
            //_tabCtrl_Operator.TopLevel = false;
            //pnlOperator.Controls.Add(_tabCtrl_Operator);
            //_tabCtrl_Operator.Parent = pnlOperator;
            //_tabCtrl_Operator.Text = string.Empty;
            //_tabCtrl_Operator.ControlBox = false;
            //_tabCtrl_Operator.Show();
            //InitMotorDataGridView(dgvMotorState);
            //InitMotorDataGridView(dgvMotorState1);

            //_equip.ADC.Adc1.ErrorReset();
            //_equip.ADC.Temperature.ErrorReset();


            ////30일 이전 데이터 삭제
            //_equip.DacEquipData.DeleteDatabaseInfo(-30);

            //tabCtrl_MainMenu.SelectedIndex = 1;
        }
    }
}
