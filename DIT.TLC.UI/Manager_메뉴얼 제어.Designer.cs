﻿namespace DIT.TLC.UI
{
    partial class Manager_메뉴얼_제어
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tc_iostatus_info = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.ajin_Setting_CasseteLoad4 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_CasseteLoad6 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_CasseteLoad2 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_CasseteLoad3 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_CasseteLoad5 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_CasseteLoad1 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.ajin_Setting_Cellpurge1 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_Cellpurge3 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_Cellpurge2 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_Cellpurge4 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.ajin_Setting_CellLoad4 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_CellLoad3 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_CellLoad5 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_CellLoad7 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_CellLoad1 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_CellLoad2 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_CellLoad6 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.ajin_Setting_IRCutProcess2 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_IRCutProcess4 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_IRCutProcess1 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_IRCutProcess3 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_IRCutProcess5 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.ajin_Setting_BreakTransfer2 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_BreakTransfer4 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_BreakTransfer1 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_BreakTransfer3 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.ajin_Setting_BreakUnitXZ6 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_BreakUnitXZ4 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_BreakUnitXZ8 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_BreakUnitXZ2 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_BreakUnitXZ5 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_BreakUnitXZ1 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_BreakUnitXZ3 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_BreakUnitXZ7 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.ajin_Setting_BreakUnitTY5 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_BreakUnitTY7 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_BreakUnitTY2 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_BreakUnitTY4 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_BreakUnitTY1 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_BreakUnitTY3 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_BreakUnitTY6 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.ajin_Setting_UnloaderTransfer6 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_UnloaderTransfer4 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_UnloaderTransfer2 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_UnloaderTransfer5 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_UnloaderTransfer1 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_UnloaderTransfer3 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.ajin_Setting_CameraUnit2 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_CameraUnit1 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.ajin_Setting_CellInput4 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_CellInput2 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_CellInput1 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_CellInput3 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.ajin_Setting_CasseteUnload6 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_CasseteUnload4 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_CasseteUnload2 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_CasseteUnload5 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_CasseteUnload1 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.ajin_Setting_CasseteUnload3 = new DIT.TLC.UI.AJIN_제어.Ajin_Setting();
            this.tc_iostatus_info.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.tabPage9.SuspendLayout();
            this.tabPage10.SuspendLayout();
            this.tabPage11.SuspendLayout();
            this.tabPage12.SuspendLayout();
            this.SuspendLayout();
            // 
            // tc_iostatus_info
            // 
            this.tc_iostatus_info.Controls.Add(this.tabPage2);
            this.tc_iostatus_info.Controls.Add(this.tabPage3);
            this.tc_iostatus_info.Controls.Add(this.tabPage4);
            this.tc_iostatus_info.Controls.Add(this.tabPage5);
            this.tc_iostatus_info.Controls.Add(this.tabPage6);
            this.tc_iostatus_info.Controls.Add(this.tabPage7);
            this.tc_iostatus_info.Controls.Add(this.tabPage8);
            this.tc_iostatus_info.Controls.Add(this.tabPage9);
            this.tc_iostatus_info.Controls.Add(this.tabPage10);
            this.tc_iostatus_info.Controls.Add(this.tabPage11);
            this.tc_iostatus_info.Controls.Add(this.tabPage12);
            this.tc_iostatus_info.ItemSize = new System.Drawing.Size(156, 30);
            this.tc_iostatus_info.Location = new System.Drawing.Point(3, 3);
            this.tc_iostatus_info.Name = "tc_iostatus_info";
            this.tc_iostatus_info.SelectedIndex = 0;
            this.tc_iostatus_info.Size = new System.Drawing.Size(1734, 854);
            this.tc_iostatus_info.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tc_iostatus_info.TabIndex = 1;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tabPage2.Controls.Add(this.ajin_Setting_CasseteLoad4);
            this.tabPage2.Controls.Add(this.ajin_Setting_CasseteLoad6);
            this.tabPage2.Controls.Add(this.ajin_Setting_CasseteLoad2);
            this.tabPage2.Controls.Add(this.ajin_Setting_CasseteLoad3);
            this.tabPage2.Controls.Add(this.ajin_Setting_CasseteLoad5);
            this.tabPage2.Controls.Add(this.ajin_Setting_CasseteLoad1);
            this.tabPage2.Location = new System.Drawing.Point(4, 34);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(1726, 816);
            this.tabPage2.TabIndex = 0;
            this.tabPage2.Text = "카세트 로드";
            // 
            // ajin_Setting_CasseteLoad4
            // 
            this.ajin_Setting_CasseteLoad4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CasseteLoad4.Location = new System.Drawing.Point(860, 225);
            this.ajin_Setting_CasseteLoad4.Name = "ajin_Setting_CasseteLoad4";
            this.ajin_Setting_CasseteLoad4.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_CasseteLoad4.TabIndex = 6;
            // 
            // ajin_Setting_CasseteLoad6
            // 
            this.ajin_Setting_CasseteLoad6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CasseteLoad6.Location = new System.Drawing.Point(860, 414);
            this.ajin_Setting_CasseteLoad6.Name = "ajin_Setting_CasseteLoad6";
            this.ajin_Setting_CasseteLoad6.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_CasseteLoad6.TabIndex = 5;
            // 
            // ajin_Setting_CasseteLoad2
            // 
            this.ajin_Setting_CasseteLoad2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CasseteLoad2.Location = new System.Drawing.Point(860, 36);
            this.ajin_Setting_CasseteLoad2.Name = "ajin_Setting_CasseteLoad2";
            this.ajin_Setting_CasseteLoad2.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_CasseteLoad2.TabIndex = 4;
            // 
            // ajin_Setting_CasseteLoad3
            // 
            this.ajin_Setting_CasseteLoad3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CasseteLoad3.Location = new System.Drawing.Point(6, 225);
            this.ajin_Setting_CasseteLoad3.Name = "ajin_Setting_CasseteLoad3";
            this.ajin_Setting_CasseteLoad3.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_CasseteLoad3.TabIndex = 3;
            // 
            // ajin_Setting_CasseteLoad5
            // 
            this.ajin_Setting_CasseteLoad5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CasseteLoad5.Location = new System.Drawing.Point(6, 414);
            this.ajin_Setting_CasseteLoad5.Name = "ajin_Setting_CasseteLoad5";
            this.ajin_Setting_CasseteLoad5.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_CasseteLoad5.TabIndex = 2;
            // 
            // ajin_Setting_CasseteLoad1
            // 
            this.ajin_Setting_CasseteLoad1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CasseteLoad1.Font = new System.Drawing.Font("굴림", 9F);
            this.ajin_Setting_CasseteLoad1.Location = new System.Drawing.Point(6, 36);
            this.ajin_Setting_CasseteLoad1.Name = "ajin_Setting_CasseteLoad1";
            this.ajin_Setting_CasseteLoad1.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_CasseteLoad1.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tabPage3.Controls.Add(this.ajin_Setting_Cellpurge1);
            this.tabPage3.Controls.Add(this.ajin_Setting_Cellpurge3);
            this.tabPage3.Controls.Add(this.ajin_Setting_Cellpurge2);
            this.tabPage3.Controls.Add(this.ajin_Setting_Cellpurge4);
            this.tabPage3.Location = new System.Drawing.Point(4, 34);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1726, 816);
            this.tabPage3.TabIndex = 1;
            this.tabPage3.Text = "셀 취출 이재기";
            // 
            // ajin_Setting_Cellpurge1
            // 
            this.ajin_Setting_Cellpurge1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_Cellpurge1.Location = new System.Drawing.Point(6, 36);
            this.ajin_Setting_Cellpurge1.Name = "ajin_Setting_Cellpurge1";
            this.ajin_Setting_Cellpurge1.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_Cellpurge1.TabIndex = 8;
            // 
            // ajin_Setting_Cellpurge3
            // 
            this.ajin_Setting_Cellpurge3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_Cellpurge3.Location = new System.Drawing.Point(6, 224);
            this.ajin_Setting_Cellpurge3.Name = "ajin_Setting_Cellpurge3";
            this.ajin_Setting_Cellpurge3.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_Cellpurge3.TabIndex = 7;
            // 
            // ajin_Setting_Cellpurge2
            // 
            this.ajin_Setting_Cellpurge2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_Cellpurge2.Location = new System.Drawing.Point(860, 36);
            this.ajin_Setting_Cellpurge2.Name = "ajin_Setting_Cellpurge2";
            this.ajin_Setting_Cellpurge2.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_Cellpurge2.TabIndex = 6;
            // 
            // ajin_Setting_Cellpurge4
            // 
            this.ajin_Setting_Cellpurge4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_Cellpurge4.Location = new System.Drawing.Point(860, 224);
            this.ajin_Setting_Cellpurge4.Name = "ajin_Setting_Cellpurge4";
            this.ajin_Setting_Cellpurge4.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_Cellpurge4.TabIndex = 5;
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tabPage4.Controls.Add(this.ajin_Setting_CellLoad4);
            this.tabPage4.Controls.Add(this.ajin_Setting_CellLoad3);
            this.tabPage4.Controls.Add(this.ajin_Setting_CellLoad5);
            this.tabPage4.Controls.Add(this.ajin_Setting_CellLoad7);
            this.tabPage4.Controls.Add(this.ajin_Setting_CellLoad1);
            this.tabPage4.Controls.Add(this.ajin_Setting_CellLoad2);
            this.tabPage4.Controls.Add(this.ajin_Setting_CellLoad6);
            this.tabPage4.Location = new System.Drawing.Point(4, 34);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(1726, 816);
            this.tabPage4.TabIndex = 2;
            this.tabPage4.Text = "셀 로드 이재기";
            // 
            // ajin_Setting_CellLoad4
            // 
            this.ajin_Setting_CellLoad4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CellLoad4.Location = new System.Drawing.Point(6, 414);
            this.ajin_Setting_CellLoad4.Name = "ajin_Setting_CellLoad4";
            this.ajin_Setting_CellLoad4.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_CellLoad4.TabIndex = 16;
            // 
            // ajin_Setting_CellLoad3
            // 
            this.ajin_Setting_CellLoad3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CellLoad3.Location = new System.Drawing.Point(860, 225);
            this.ajin_Setting_CellLoad3.Name = "ajin_Setting_CellLoad3";
            this.ajin_Setting_CellLoad3.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_CellLoad3.TabIndex = 15;
            // 
            // ajin_Setting_CellLoad5
            // 
            this.ajin_Setting_CellLoad5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CellLoad5.Location = new System.Drawing.Point(860, 414);
            this.ajin_Setting_CellLoad5.Name = "ajin_Setting_CellLoad5";
            this.ajin_Setting_CellLoad5.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_CellLoad5.TabIndex = 14;
            // 
            // ajin_Setting_CellLoad7
            // 
            this.ajin_Setting_CellLoad7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CellLoad7.Location = new System.Drawing.Point(860, 603);
            this.ajin_Setting_CellLoad7.Name = "ajin_Setting_CellLoad7";
            this.ajin_Setting_CellLoad7.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_CellLoad7.TabIndex = 13;
            // 
            // ajin_Setting_CellLoad1
            // 
            this.ajin_Setting_CellLoad1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CellLoad1.Location = new System.Drawing.Point(6, 36);
            this.ajin_Setting_CellLoad1.Name = "ajin_Setting_CellLoad1";
            this.ajin_Setting_CellLoad1.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_CellLoad1.TabIndex = 12;
            // 
            // ajin_Setting_CellLoad2
            // 
            this.ajin_Setting_CellLoad2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CellLoad2.Location = new System.Drawing.Point(6, 225);
            this.ajin_Setting_CellLoad2.Name = "ajin_Setting_CellLoad2";
            this.ajin_Setting_CellLoad2.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_CellLoad2.TabIndex = 11;
            // 
            // ajin_Setting_CellLoad6
            // 
            this.ajin_Setting_CellLoad6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CellLoad6.Location = new System.Drawing.Point(6, 603);
            this.ajin_Setting_CellLoad6.Name = "ajin_Setting_CellLoad6";
            this.ajin_Setting_CellLoad6.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_CellLoad6.TabIndex = 9;
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tabPage5.Controls.Add(this.ajin_Setting_IRCutProcess2);
            this.tabPage5.Controls.Add(this.ajin_Setting_IRCutProcess4);
            this.tabPage5.Controls.Add(this.ajin_Setting_IRCutProcess1);
            this.tabPage5.Controls.Add(this.ajin_Setting_IRCutProcess3);
            this.tabPage5.Controls.Add(this.ajin_Setting_IRCutProcess5);
            this.tabPage5.Location = new System.Drawing.Point(4, 34);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(1726, 816);
            this.tabPage5.TabIndex = 3;
            this.tabPage5.Text = "IR Cut 프로세스";
            // 
            // ajin_Setting_IRCutProcess2
            // 
            this.ajin_Setting_IRCutProcess2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_IRCutProcess2.Location = new System.Drawing.Point(860, 36);
            this.ajin_Setting_IRCutProcess2.Name = "ajin_Setting_IRCutProcess2";
            this.ajin_Setting_IRCutProcess2.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_IRCutProcess2.TabIndex = 21;
            // 
            // ajin_Setting_IRCutProcess4
            // 
            this.ajin_Setting_IRCutProcess4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_IRCutProcess4.Location = new System.Drawing.Point(6, 414);
            this.ajin_Setting_IRCutProcess4.Name = "ajin_Setting_IRCutProcess4";
            this.ajin_Setting_IRCutProcess4.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_IRCutProcess4.TabIndex = 20;
            // 
            // ajin_Setting_IRCutProcess1
            // 
            this.ajin_Setting_IRCutProcess1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_IRCutProcess1.Location = new System.Drawing.Point(6, 36);
            this.ajin_Setting_IRCutProcess1.Name = "ajin_Setting_IRCutProcess1";
            this.ajin_Setting_IRCutProcess1.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_IRCutProcess1.TabIndex = 19;
            // 
            // ajin_Setting_IRCutProcess3
            // 
            this.ajin_Setting_IRCutProcess3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_IRCutProcess3.Location = new System.Drawing.Point(6, 225);
            this.ajin_Setting_IRCutProcess3.Name = "ajin_Setting_IRCutProcess3";
            this.ajin_Setting_IRCutProcess3.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_IRCutProcess3.TabIndex = 18;
            // 
            // ajin_Setting_IRCutProcess5
            // 
            this.ajin_Setting_IRCutProcess5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_IRCutProcess5.Location = new System.Drawing.Point(6, 603);
            this.ajin_Setting_IRCutProcess5.Name = "ajin_Setting_IRCutProcess5";
            this.ajin_Setting_IRCutProcess5.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_IRCutProcess5.TabIndex = 17;
            // 
            // tabPage6
            // 
            this.tabPage6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tabPage6.Controls.Add(this.ajin_Setting_BreakTransfer2);
            this.tabPage6.Controls.Add(this.ajin_Setting_BreakTransfer4);
            this.tabPage6.Controls.Add(this.ajin_Setting_BreakTransfer1);
            this.tabPage6.Controls.Add(this.ajin_Setting_BreakTransfer3);
            this.tabPage6.Location = new System.Drawing.Point(4, 34);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(1726, 816);
            this.tabPage6.TabIndex = 4;
            this.tabPage6.Text = "Break 트랜스퍼";
            // 
            // ajin_Setting_BreakTransfer2
            // 
            this.ajin_Setting_BreakTransfer2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_BreakTransfer2.Location = new System.Drawing.Point(860, 36);
            this.ajin_Setting_BreakTransfer2.Name = "ajin_Setting_BreakTransfer2";
            this.ajin_Setting_BreakTransfer2.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_BreakTransfer2.TabIndex = 25;
            // 
            // ajin_Setting_BreakTransfer4
            // 
            this.ajin_Setting_BreakTransfer4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_BreakTransfer4.Location = new System.Drawing.Point(6, 414);
            this.ajin_Setting_BreakTransfer4.Name = "ajin_Setting_BreakTransfer4";
            this.ajin_Setting_BreakTransfer4.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_BreakTransfer4.TabIndex = 24;
            // 
            // ajin_Setting_BreakTransfer1
            // 
            this.ajin_Setting_BreakTransfer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_BreakTransfer1.Location = new System.Drawing.Point(6, 36);
            this.ajin_Setting_BreakTransfer1.Name = "ajin_Setting_BreakTransfer1";
            this.ajin_Setting_BreakTransfer1.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_BreakTransfer1.TabIndex = 23;
            // 
            // ajin_Setting_BreakTransfer3
            // 
            this.ajin_Setting_BreakTransfer3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_BreakTransfer3.Location = new System.Drawing.Point(6, 225);
            this.ajin_Setting_BreakTransfer3.Name = "ajin_Setting_BreakTransfer3";
            this.ajin_Setting_BreakTransfer3.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_BreakTransfer3.TabIndex = 22;
            // 
            // tabPage7
            // 
            this.tabPage7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tabPage7.Controls.Add(this.ajin_Setting_BreakUnitXZ6);
            this.tabPage7.Controls.Add(this.ajin_Setting_BreakUnitXZ4);
            this.tabPage7.Controls.Add(this.ajin_Setting_BreakUnitXZ8);
            this.tabPage7.Controls.Add(this.ajin_Setting_BreakUnitXZ2);
            this.tabPage7.Controls.Add(this.ajin_Setting_BreakUnitXZ5);
            this.tabPage7.Controls.Add(this.ajin_Setting_BreakUnitXZ1);
            this.tabPage7.Controls.Add(this.ajin_Setting_BreakUnitXZ3);
            this.tabPage7.Controls.Add(this.ajin_Setting_BreakUnitXZ7);
            this.tabPage7.Location = new System.Drawing.Point(4, 34);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(1726, 816);
            this.tabPage7.TabIndex = 5;
            this.tabPage7.Text = "Break 유닛(X축 Z축)";
            // 
            // ajin_Setting_BreakUnitXZ6
            // 
            this.ajin_Setting_BreakUnitXZ6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_BreakUnitXZ6.Location = new System.Drawing.Point(860, 414);
            this.ajin_Setting_BreakUnitXZ6.Name = "ajin_Setting_BreakUnitXZ6";
            this.ajin_Setting_BreakUnitXZ6.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_BreakUnitXZ6.TabIndex = 29;
            // 
            // ajin_Setting_BreakUnitXZ4
            // 
            this.ajin_Setting_BreakUnitXZ4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_BreakUnitXZ4.Location = new System.Drawing.Point(860, 225);
            this.ajin_Setting_BreakUnitXZ4.Name = "ajin_Setting_BreakUnitXZ4";
            this.ajin_Setting_BreakUnitXZ4.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_BreakUnitXZ4.TabIndex = 28;
            // 
            // ajin_Setting_BreakUnitXZ8
            // 
            this.ajin_Setting_BreakUnitXZ8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_BreakUnitXZ8.Location = new System.Drawing.Point(860, 603);
            this.ajin_Setting_BreakUnitXZ8.Name = "ajin_Setting_BreakUnitXZ8";
            this.ajin_Setting_BreakUnitXZ8.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_BreakUnitXZ8.TabIndex = 27;
            // 
            // ajin_Setting_BreakUnitXZ2
            // 
            this.ajin_Setting_BreakUnitXZ2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_BreakUnitXZ2.Location = new System.Drawing.Point(860, 36);
            this.ajin_Setting_BreakUnitXZ2.Name = "ajin_Setting_BreakUnitXZ2";
            this.ajin_Setting_BreakUnitXZ2.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_BreakUnitXZ2.TabIndex = 26;
            // 
            // ajin_Setting_BreakUnitXZ5
            // 
            this.ajin_Setting_BreakUnitXZ5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_BreakUnitXZ5.Location = new System.Drawing.Point(6, 414);
            this.ajin_Setting_BreakUnitXZ5.Name = "ajin_Setting_BreakUnitXZ5";
            this.ajin_Setting_BreakUnitXZ5.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_BreakUnitXZ5.TabIndex = 25;
            // 
            // ajin_Setting_BreakUnitXZ1
            // 
            this.ajin_Setting_BreakUnitXZ1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_BreakUnitXZ1.Location = new System.Drawing.Point(6, 36);
            this.ajin_Setting_BreakUnitXZ1.Name = "ajin_Setting_BreakUnitXZ1";
            this.ajin_Setting_BreakUnitXZ1.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_BreakUnitXZ1.TabIndex = 24;
            // 
            // ajin_Setting_BreakUnitXZ3
            // 
            this.ajin_Setting_BreakUnitXZ3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_BreakUnitXZ3.Location = new System.Drawing.Point(6, 225);
            this.ajin_Setting_BreakUnitXZ3.Name = "ajin_Setting_BreakUnitXZ3";
            this.ajin_Setting_BreakUnitXZ3.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_BreakUnitXZ3.TabIndex = 23;
            // 
            // ajin_Setting_BreakUnitXZ7
            // 
            this.ajin_Setting_BreakUnitXZ7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_BreakUnitXZ7.Location = new System.Drawing.Point(6, 603);
            this.ajin_Setting_BreakUnitXZ7.Name = "ajin_Setting_BreakUnitXZ7";
            this.ajin_Setting_BreakUnitXZ7.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_BreakUnitXZ7.TabIndex = 22;
            // 
            // tabPage8
            // 
            this.tabPage8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tabPage8.Controls.Add(this.ajin_Setting_BreakUnitTY5);
            this.tabPage8.Controls.Add(this.ajin_Setting_BreakUnitTY7);
            this.tabPage8.Controls.Add(this.ajin_Setting_BreakUnitTY2);
            this.tabPage8.Controls.Add(this.ajin_Setting_BreakUnitTY4);
            this.tabPage8.Controls.Add(this.ajin_Setting_BreakUnitTY1);
            this.tabPage8.Controls.Add(this.ajin_Setting_BreakUnitTY3);
            this.tabPage8.Controls.Add(this.ajin_Setting_BreakUnitTY6);
            this.tabPage8.Location = new System.Drawing.Point(4, 34);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(1726, 816);
            this.tabPage8.TabIndex = 6;
            this.tabPage8.Text = "Break 유닛(T축 Y축)";
            // 
            // ajin_Setting_BreakUnitTY5
            // 
            this.ajin_Setting_BreakUnitTY5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_BreakUnitTY5.Location = new System.Drawing.Point(860, 414);
            this.ajin_Setting_BreakUnitTY5.Name = "ajin_Setting_BreakUnitTY5";
            this.ajin_Setting_BreakUnitTY5.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_BreakUnitTY5.TabIndex = 37;
            // 
            // ajin_Setting_BreakUnitTY7
            // 
            this.ajin_Setting_BreakUnitTY7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_BreakUnitTY7.Location = new System.Drawing.Point(860, 603);
            this.ajin_Setting_BreakUnitTY7.Name = "ajin_Setting_BreakUnitTY7";
            this.ajin_Setting_BreakUnitTY7.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_BreakUnitTY7.TabIndex = 35;
            // 
            // ajin_Setting_BreakUnitTY2
            // 
            this.ajin_Setting_BreakUnitTY2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_BreakUnitTY2.Location = new System.Drawing.Point(860, 36);
            this.ajin_Setting_BreakUnitTY2.Name = "ajin_Setting_BreakUnitTY2";
            this.ajin_Setting_BreakUnitTY2.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_BreakUnitTY2.TabIndex = 34;
            // 
            // ajin_Setting_BreakUnitTY4
            // 
            this.ajin_Setting_BreakUnitTY4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_BreakUnitTY4.Location = new System.Drawing.Point(6, 414);
            this.ajin_Setting_BreakUnitTY4.Name = "ajin_Setting_BreakUnitTY4";
            this.ajin_Setting_BreakUnitTY4.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_BreakUnitTY4.TabIndex = 33;
            // 
            // ajin_Setting_BreakUnitTY1
            // 
            this.ajin_Setting_BreakUnitTY1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_BreakUnitTY1.Location = new System.Drawing.Point(6, 36);
            this.ajin_Setting_BreakUnitTY1.Name = "ajin_Setting_BreakUnitTY1";
            this.ajin_Setting_BreakUnitTY1.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_BreakUnitTY1.TabIndex = 32;
            // 
            // ajin_Setting_BreakUnitTY3
            // 
            this.ajin_Setting_BreakUnitTY3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_BreakUnitTY3.Location = new System.Drawing.Point(6, 225);
            this.ajin_Setting_BreakUnitTY3.Name = "ajin_Setting_BreakUnitTY3";
            this.ajin_Setting_BreakUnitTY3.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_BreakUnitTY3.TabIndex = 31;
            // 
            // ajin_Setting_BreakUnitTY6
            // 
            this.ajin_Setting_BreakUnitTY6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_BreakUnitTY6.Location = new System.Drawing.Point(6, 603);
            this.ajin_Setting_BreakUnitTY6.Name = "ajin_Setting_BreakUnitTY6";
            this.ajin_Setting_BreakUnitTY6.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_BreakUnitTY6.TabIndex = 30;
            // 
            // tabPage9
            // 
            this.tabPage9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tabPage9.Controls.Add(this.ajin_Setting_UnloaderTransfer6);
            this.tabPage9.Controls.Add(this.ajin_Setting_UnloaderTransfer4);
            this.tabPage9.Controls.Add(this.ajin_Setting_UnloaderTransfer2);
            this.tabPage9.Controls.Add(this.ajin_Setting_UnloaderTransfer5);
            this.tabPage9.Controls.Add(this.ajin_Setting_UnloaderTransfer1);
            this.tabPage9.Controls.Add(this.ajin_Setting_UnloaderTransfer3);
            this.tabPage9.Location = new System.Drawing.Point(4, 34);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Size = new System.Drawing.Size(1726, 816);
            this.tabPage9.TabIndex = 7;
            this.tabPage9.Text = "언로더 트랜스퍼";
            // 
            // ajin_Setting_UnloaderTransfer6
            // 
            this.ajin_Setting_UnloaderTransfer6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_UnloaderTransfer6.Location = new System.Drawing.Point(860, 414);
            this.ajin_Setting_UnloaderTransfer6.Name = "ajin_Setting_UnloaderTransfer6";
            this.ajin_Setting_UnloaderTransfer6.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_UnloaderTransfer6.TabIndex = 35;
            // 
            // ajin_Setting_UnloaderTransfer4
            // 
            this.ajin_Setting_UnloaderTransfer4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_UnloaderTransfer4.Location = new System.Drawing.Point(860, 225);
            this.ajin_Setting_UnloaderTransfer4.Name = "ajin_Setting_UnloaderTransfer4";
            this.ajin_Setting_UnloaderTransfer4.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_UnloaderTransfer4.TabIndex = 34;
            // 
            // ajin_Setting_UnloaderTransfer2
            // 
            this.ajin_Setting_UnloaderTransfer2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_UnloaderTransfer2.Location = new System.Drawing.Point(860, 36);
            this.ajin_Setting_UnloaderTransfer2.Name = "ajin_Setting_UnloaderTransfer2";
            this.ajin_Setting_UnloaderTransfer2.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_UnloaderTransfer2.TabIndex = 33;
            // 
            // ajin_Setting_UnloaderTransfer5
            // 
            this.ajin_Setting_UnloaderTransfer5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_UnloaderTransfer5.Location = new System.Drawing.Point(6, 414);
            this.ajin_Setting_UnloaderTransfer5.Name = "ajin_Setting_UnloaderTransfer5";
            this.ajin_Setting_UnloaderTransfer5.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_UnloaderTransfer5.TabIndex = 32;
            // 
            // ajin_Setting_UnloaderTransfer1
            // 
            this.ajin_Setting_UnloaderTransfer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_UnloaderTransfer1.Location = new System.Drawing.Point(6, 36);
            this.ajin_Setting_UnloaderTransfer1.Name = "ajin_Setting_UnloaderTransfer1";
            this.ajin_Setting_UnloaderTransfer1.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_UnloaderTransfer1.TabIndex = 31;
            // 
            // ajin_Setting_UnloaderTransfer3
            // 
            this.ajin_Setting_UnloaderTransfer3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_UnloaderTransfer3.Location = new System.Drawing.Point(6, 225);
            this.ajin_Setting_UnloaderTransfer3.Name = "ajin_Setting_UnloaderTransfer3";
            this.ajin_Setting_UnloaderTransfer3.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_UnloaderTransfer3.TabIndex = 30;
            // 
            // tabPage10
            // 
            this.tabPage10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tabPage10.Controls.Add(this.ajin_Setting_CameraUnit2);
            this.tabPage10.Controls.Add(this.ajin_Setting_CameraUnit1);
            this.tabPage10.Location = new System.Drawing.Point(4, 34);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Size = new System.Drawing.Size(1726, 816);
            this.tabPage10.TabIndex = 8;
            this.tabPage10.Text = "카메라 유닛";
            // 
            // ajin_Setting_CameraUnit2
            // 
            this.ajin_Setting_CameraUnit2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CameraUnit2.Location = new System.Drawing.Point(860, 36);
            this.ajin_Setting_CameraUnit2.Name = "ajin_Setting_CameraUnit2";
            this.ajin_Setting_CameraUnit2.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_CameraUnit2.TabIndex = 35;
            // 
            // ajin_Setting_CameraUnit1
            // 
            this.ajin_Setting_CameraUnit1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CameraUnit1.Location = new System.Drawing.Point(6, 36);
            this.ajin_Setting_CameraUnit1.Name = "ajin_Setting_CameraUnit1";
            this.ajin_Setting_CameraUnit1.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_CameraUnit1.TabIndex = 34;
            // 
            // tabPage11
            // 
            this.tabPage11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tabPage11.Controls.Add(this.ajin_Setting_CellInput4);
            this.tabPage11.Controls.Add(this.ajin_Setting_CellInput2);
            this.tabPage11.Controls.Add(this.ajin_Setting_CellInput1);
            this.tabPage11.Controls.Add(this.ajin_Setting_CellInput3);
            this.tabPage11.Location = new System.Drawing.Point(4, 34);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Size = new System.Drawing.Size(1726, 816);
            this.tabPage11.TabIndex = 9;
            this.tabPage11.Text = "셀 투입 이재기";
            // 
            // ajin_Setting_CellInput4
            // 
            this.ajin_Setting_CellInput4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CellInput4.Location = new System.Drawing.Point(860, 225);
            this.ajin_Setting_CellInput4.Name = "ajin_Setting_CellInput4";
            this.ajin_Setting_CellInput4.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_CellInput4.TabIndex = 38;
            // 
            // ajin_Setting_CellInput2
            // 
            this.ajin_Setting_CellInput2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CellInput2.Location = new System.Drawing.Point(860, 36);
            this.ajin_Setting_CellInput2.Name = "ajin_Setting_CellInput2";
            this.ajin_Setting_CellInput2.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_CellInput2.TabIndex = 37;
            // 
            // ajin_Setting_CellInput1
            // 
            this.ajin_Setting_CellInput1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CellInput1.Location = new System.Drawing.Point(6, 36);
            this.ajin_Setting_CellInput1.Name = "ajin_Setting_CellInput1";
            this.ajin_Setting_CellInput1.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_CellInput1.TabIndex = 36;
            // 
            // ajin_Setting_CellInput3
            // 
            this.ajin_Setting_CellInput3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CellInput3.Location = new System.Drawing.Point(6, 225);
            this.ajin_Setting_CellInput3.Name = "ajin_Setting_CellInput3";
            this.ajin_Setting_CellInput3.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_CellInput3.TabIndex = 35;
            // 
            // tabPage12
            // 
            this.tabPage12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tabPage12.Controls.Add(this.ajin_Setting_CasseteUnload6);
            this.tabPage12.Controls.Add(this.ajin_Setting_CasseteUnload4);
            this.tabPage12.Controls.Add(this.ajin_Setting_CasseteUnload2);
            this.tabPage12.Controls.Add(this.ajin_Setting_CasseteUnload5);
            this.tabPage12.Controls.Add(this.ajin_Setting_CasseteUnload1);
            this.tabPage12.Controls.Add(this.ajin_Setting_CasseteUnload3);
            this.tabPage12.Location = new System.Drawing.Point(4, 34);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Size = new System.Drawing.Size(1726, 816);
            this.tabPage12.TabIndex = 10;
            this.tabPage12.Text = "카세트 언로드";
            // 
            // ajin_Setting_CasseteUnload6
            // 
            this.ajin_Setting_CasseteUnload6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CasseteUnload6.Location = new System.Drawing.Point(860, 414);
            this.ajin_Setting_CasseteUnload6.Name = "ajin_Setting_CasseteUnload6";
            this.ajin_Setting_CasseteUnload6.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_CasseteUnload6.TabIndex = 41;
            // 
            // ajin_Setting_CasseteUnload4
            // 
            this.ajin_Setting_CasseteUnload4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CasseteUnload4.Location = new System.Drawing.Point(860, 225);
            this.ajin_Setting_CasseteUnload4.Name = "ajin_Setting_CasseteUnload4";
            this.ajin_Setting_CasseteUnload4.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_CasseteUnload4.TabIndex = 40;
            // 
            // ajin_Setting_CasseteUnload2
            // 
            this.ajin_Setting_CasseteUnload2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CasseteUnload2.Location = new System.Drawing.Point(860, 36);
            this.ajin_Setting_CasseteUnload2.Name = "ajin_Setting_CasseteUnload2";
            this.ajin_Setting_CasseteUnload2.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_CasseteUnload2.TabIndex = 39;
            // 
            // ajin_Setting_CasseteUnload5
            // 
            this.ajin_Setting_CasseteUnload5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CasseteUnload5.Location = new System.Drawing.Point(6, 414);
            this.ajin_Setting_CasseteUnload5.Name = "ajin_Setting_CasseteUnload5";
            this.ajin_Setting_CasseteUnload5.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_CasseteUnload5.TabIndex = 38;
            // 
            // ajin_Setting_CasseteUnload1
            // 
            this.ajin_Setting_CasseteUnload1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CasseteUnload1.Location = new System.Drawing.Point(6, 36);
            this.ajin_Setting_CasseteUnload1.Name = "ajin_Setting_CasseteUnload1";
            this.ajin_Setting_CasseteUnload1.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_CasseteUnload1.TabIndex = 37;
            // 
            // ajin_Setting_CasseteUnload3
            // 
            this.ajin_Setting_CasseteUnload3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CasseteUnload3.Location = new System.Drawing.Point(6, 225);
            this.ajin_Setting_CasseteUnload3.Name = "ajin_Setting_CasseteUnload3";
            this.ajin_Setting_CasseteUnload3.Size = new System.Drawing.Size(848, 183);
            this.ajin_Setting_CasseteUnload3.TabIndex = 36;
            // 
            // Manager_메뉴얼_제어
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.Controls.Add(this.tc_iostatus_info);
            this.Name = "Manager_메뉴얼_제어";
            this.Size = new System.Drawing.Size(1740, 860);
            this.tc_iostatus_info.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.tabPage7.ResumeLayout(false);
            this.tabPage8.ResumeLayout(false);
            this.tabPage9.ResumeLayout(false);
            this.tabPage10.ResumeLayout(false);
            this.tabPage11.ResumeLayout(false);
            this.tabPage12.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tc_iostatus_info;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.TabPage tabPage12;
        private AJIN_제어.Ajin_Setting ajin_Setting_CasseteLoad1;
        private AJIN_제어.Ajin_Setting ajin_Setting_CasseteLoad5;
        private AJIN_제어.Ajin_Setting ajin_Setting_CasseteLoad4;
        private AJIN_제어.Ajin_Setting ajin_Setting_CasseteLoad6;
        private AJIN_제어.Ajin_Setting ajin_Setting_CasseteLoad2;
        private AJIN_제어.Ajin_Setting ajin_Setting_CasseteLoad3;
        private AJIN_제어.Ajin_Setting ajin_Setting_Cellpurge1;
        private AJIN_제어.Ajin_Setting ajin_Setting_Cellpurge3;
        private AJIN_제어.Ajin_Setting ajin_Setting_Cellpurge2;
        private AJIN_제어.Ajin_Setting ajin_Setting_Cellpurge4;
        private AJIN_제어.Ajin_Setting ajin_Setting_CellLoad4;
        private AJIN_제어.Ajin_Setting ajin_Setting_CellLoad3;
        private AJIN_제어.Ajin_Setting ajin_Setting_CellLoad5;
        private AJIN_제어.Ajin_Setting ajin_Setting_CellLoad7;
        private AJIN_제어.Ajin_Setting ajin_Setting_CellLoad1;
        private AJIN_제어.Ajin_Setting ajin_Setting_CellLoad2;
        private AJIN_제어.Ajin_Setting ajin_Setting_CellLoad6;
        private AJIN_제어.Ajin_Setting ajin_Setting_IRCutProcess2;
        private AJIN_제어.Ajin_Setting ajin_Setting_IRCutProcess4;
        private AJIN_제어.Ajin_Setting ajin_Setting_IRCutProcess1;
        private AJIN_제어.Ajin_Setting ajin_Setting_IRCutProcess3;
        private AJIN_제어.Ajin_Setting ajin_Setting_IRCutProcess5;
        private AJIN_제어.Ajin_Setting ajin_Setting_BreakTransfer2;
        private AJIN_제어.Ajin_Setting ajin_Setting_BreakTransfer4;
        private AJIN_제어.Ajin_Setting ajin_Setting_BreakTransfer1;
        private AJIN_제어.Ajin_Setting ajin_Setting_BreakTransfer3;
        private AJIN_제어.Ajin_Setting ajin_Setting_BreakUnitXZ6;
        private AJIN_제어.Ajin_Setting ajin_Setting_BreakUnitXZ4;
        private AJIN_제어.Ajin_Setting ajin_Setting_BreakUnitXZ8;
        private AJIN_제어.Ajin_Setting ajin_Setting_BreakUnitXZ2;
        private AJIN_제어.Ajin_Setting ajin_Setting_BreakUnitXZ5;
        private AJIN_제어.Ajin_Setting ajin_Setting_BreakUnitXZ1;
        private AJIN_제어.Ajin_Setting ajin_Setting_BreakUnitXZ3;
        private AJIN_제어.Ajin_Setting ajin_Setting_BreakUnitXZ7;
        private AJIN_제어.Ajin_Setting ajin_Setting_BreakUnitTY5;
        private AJIN_제어.Ajin_Setting ajin_Setting_BreakUnitTY7;
        private AJIN_제어.Ajin_Setting ajin_Setting_BreakUnitTY2;
        private AJIN_제어.Ajin_Setting ajin_Setting_BreakUnitTY4;
        private AJIN_제어.Ajin_Setting ajin_Setting_BreakUnitTY1;
        private AJIN_제어.Ajin_Setting ajin_Setting_BreakUnitTY3;
        private AJIN_제어.Ajin_Setting ajin_Setting_BreakUnitTY6;
        private AJIN_제어.Ajin_Setting ajin_Setting_UnloaderTransfer6;
        private AJIN_제어.Ajin_Setting ajin_Setting_UnloaderTransfer4;
        private AJIN_제어.Ajin_Setting ajin_Setting_UnloaderTransfer2;
        private AJIN_제어.Ajin_Setting ajin_Setting_UnloaderTransfer5;
        private AJIN_제어.Ajin_Setting ajin_Setting_UnloaderTransfer1;
        private AJIN_제어.Ajin_Setting ajin_Setting_UnloaderTransfer3;
        private AJIN_제어.Ajin_Setting ajin_Setting_CameraUnit2;
        private AJIN_제어.Ajin_Setting ajin_Setting_CameraUnit1;
        private AJIN_제어.Ajin_Setting ajin_Setting_CellInput4;
        private AJIN_제어.Ajin_Setting ajin_Setting_CellInput2;
        private AJIN_제어.Ajin_Setting ajin_Setting_CellInput1;
        private AJIN_제어.Ajin_Setting ajin_Setting_CellInput3;
        private AJIN_제어.Ajin_Setting ajin_Setting_CasseteUnload6;
        private AJIN_제어.Ajin_Setting ajin_Setting_CasseteUnload4;
        private AJIN_제어.Ajin_Setting ajin_Setting_CasseteUnload2;
        private AJIN_제어.Ajin_Setting ajin_Setting_CasseteUnload5;
        private AJIN_제어.Ajin_Setting ajin_Setting_CasseteUnload1;
        private AJIN_제어.Ajin_Setting ajin_Setting_CasseteUnload3;
    }
}
