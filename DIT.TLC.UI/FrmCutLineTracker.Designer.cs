﻿namespace DIT.TLC.UI
{
    partial class FrmCutLineTracker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btn1Table = new System.Windows.Forms.Button();
            this.btnALine = new System.Windows.Forms.Button();
            this.btnBLine = new System.Windows.Forms.Button();
            this.btn2Table = new System.Windows.Forms.Button();
            this.btnAlign = new System.Windows.Forms.Button();
            this.cbbox = new System.Windows.Forms.ComboBox();
            this.btnPlus = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.lblLog = new System.Windows.Forms.TextBox();
            this.btnMinus = new System.Windows.Forms.Button();
            this.btn_Close = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.btn1Table, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnALine, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnBLine, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.btn2Table, 1, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(423, 192);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // btn1Table
            // 
            this.btn1Table.BackColor = System.Drawing.Color.DimGray;
            this.btn1Table.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btn1Table.ForeColor = System.Drawing.Color.Black;
            this.btn1Table.Location = new System.Drawing.Point(3, 99);
            this.btn1Table.Name = "btn1Table";
            this.btn1Table.Size = new System.Drawing.Size(205, 90);
            this.btn1Table.TabIndex = 3;
            this.btn1Table.Text = "1Table";
            this.btn1Table.UseVisualStyleBackColor = false;
            // 
            // btnALine
            // 
            this.btnALine.BackColor = System.Drawing.Color.DimGray;
            this.btnALine.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btnALine.ForeColor = System.Drawing.Color.Black;
            this.btnALine.Location = new System.Drawing.Point(3, 3);
            this.btnALine.Name = "btnALine";
            this.btnALine.Size = new System.Drawing.Size(205, 90);
            this.btnALine.TabIndex = 1;
            this.btnALine.Text = "A-Line";
            this.btnALine.UseVisualStyleBackColor = false;
            // 
            // btnBLine
            // 
            this.btnBLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBLine.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btnBLine.ForeColor = System.Drawing.Color.White;
            this.btnBLine.Location = new System.Drawing.Point(214, 3);
            this.btnBLine.Name = "btnBLine";
            this.btnBLine.Size = new System.Drawing.Size(206, 90);
            this.btnBLine.TabIndex = 56;
            this.btnBLine.Text = "B-Line";
            this.btnBLine.UseVisualStyleBackColor = false;
            // 
            // btn2Table
            // 
            this.btn2Table.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn2Table.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btn2Table.ForeColor = System.Drawing.Color.White;
            this.btn2Table.Location = new System.Drawing.Point(214, 99);
            this.btn2Table.Name = "btn2Table";
            this.btn2Table.Size = new System.Drawing.Size(206, 90);
            this.btn2Table.TabIndex = 4;
            this.btn2Table.Text = "2Table";
            this.btn2Table.UseVisualStyleBackColor = false;
            // 
            // btnAlign
            // 
            this.btnAlign.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnAlign.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btnAlign.ForeColor = System.Drawing.Color.White;
            this.btnAlign.Location = new System.Drawing.Point(806, 15);
            this.btnAlign.Name = "btnAlign";
            this.btnAlign.Size = new System.Drawing.Size(206, 186);
            this.btnAlign.TabIndex = 5;
            this.btnAlign.Text = "ALIGN";
            this.btnAlign.UseVisualStyleBackColor = false;
            // 
            // cbbox
            // 
            this.cbbox.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.cbbox.FormattingEnabled = true;
            this.cbbox.Location = new System.Drawing.Point(3, 3);
            this.cbbox.Name = "cbbox";
            this.cbbox.Size = new System.Drawing.Size(60, 28);
            this.cbbox.TabIndex = 0;
            // 
            // btnPlus
            // 
            this.btnPlus.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnPlus.Location = new System.Drawing.Point(69, 3);
            this.btnPlus.Name = "btnPlus";
            this.btnPlus.Size = new System.Drawing.Size(33, 28);
            this.btnPlus.TabIndex = 6;
            this.btnPlus.Text = "+";
            this.btnPlus.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(12, 222);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.427536F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 92.57246F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1000, 562);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.641366F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.98482F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.98482F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 85.38899F));
            this.tableLayoutPanel3.Controls.Add(this.cbbox, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.lblLog, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnPlus, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnMinus, 2, 0);
            this.tableLayoutPanel3.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(994, 34);
            this.tableLayoutPanel3.TabIndex = 60;
            // 
            // lblLog
            // 
            this.lblLog.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblLog.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblLog.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLog.Location = new System.Drawing.Point(147, 7);
            this.lblLog.Margin = new System.Windows.Forms.Padding(3, 7, 3, 3);
            this.lblLog.Name = "lblLog";
            this.lblLog.ReadOnly = true;
            this.lblLog.Size = new System.Drawing.Size(844, 22);
            this.lblLog.TabIndex = 0;
            this.lblLog.Text = "TEXT";
            // 
            // btnMinus
            // 
            this.btnMinus.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnMinus.Location = new System.Drawing.Point(108, 3);
            this.btnMinus.Name = "btnMinus";
            this.btnMinus.Size = new System.Drawing.Size(33, 28);
            this.btnMinus.TabIndex = 7;
            this.btnMinus.Text = "-";
            this.btnMinus.UseVisualStyleBackColor = true;
            // 
            // btn_Close
            // 
            this.btn_Close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Close.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btn_Close.ForeColor = System.Drawing.Color.White;
            this.btn_Close.Location = new System.Drawing.Point(899, 790);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(113, 34);
            this.btn_Close.TabIndex = 59;
            this.btn_Close.Text = "Close";
            this.btn_Close.UseVisualStyleBackColor = false;
            // 
            // FrmCutLineTracker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1024, 836);
            this.Controls.Add(this.btn_Close);
            this.Controls.Add(this.btnAlign);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.tableLayoutPanel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCutLineTracker";
            this.Text = "FrmCutLineTracker";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btn1Table;
        private System.Windows.Forms.Button btnALine;
        private System.Windows.Forms.Button btnBLine;
        private System.Windows.Forms.Button btn2Table;
        private System.Windows.Forms.Button btnAlign;
        private System.Windows.Forms.ComboBox cbbox;
        private System.Windows.Forms.Button btnPlus;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button btnMinus;
        private System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.TextBox lblLog;
    }
}