﻿namespace DIT.TLC.UI
{
    partial class FrmMain
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.digitalTimer1 = new DIT.TLC.UI.DigitalTimer();
            this.panelSafety = new System.Windows.Forms.Panel();
            this.panelWarning = new System.Windows.Forms.Panel();
            this.panelDanger = new System.Windows.Forms.Panel();
            this.btnALarmLog = new System.Windows.Forms.Button();
            this.btnHostMessage = new System.Windows.Forms.Button();
            this.btnSem = new System.Windows.Forms.Button();
            this.btnOnoffLine = new System.Windows.Forms.Button();
            this.txtterminalMessage = new System.Windows.Forms.TextBox();
            this.lbTerminalMessage = new System.Windows.Forms.Label();
            this.lb_title = new System.Windows.Forms.Label();
            this.panel_logo = new System.Windows.Forms.Panel();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.btn_graph = new System.Windows.Forms.Button();
            this.btn_measure = new System.Windows.Forms.Button();
            this.btn_log = new System.Windows.Forms.Button();
            this.btn_param = new System.Windows.Forms.Button();
            this.btn_manager = new System.Windows.Forms.Button();
            this.btn_recipe = new System.Windows.Forms.Button();
            this.btn_main = new System.Windows.Forms.Button();
            this.btn_login = new System.Windows.Forms.Button();
            this.btn_exit = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(12, 12);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.splitContainer1.Panel1.Controls.Add(this.digitalTimer1);
            this.splitContainer1.Panel1.Controls.Add(this.panelSafety);
            this.splitContainer1.Panel1.Controls.Add(this.panelWarning);
            this.splitContainer1.Panel1.Controls.Add(this.panelDanger);
            this.splitContainer1.Panel1.Controls.Add(this.btnALarmLog);
            this.splitContainer1.Panel1.Controls.Add(this.btnHostMessage);
            this.splitContainer1.Panel1.Controls.Add(this.btnSem);
            this.splitContainer1.Panel1.Controls.Add(this.btnOnoffLine);
            this.splitContainer1.Panel1.Controls.Add(this.txtterminalMessage);
            this.splitContainer1.Panel1.Controls.Add(this.lbTerminalMessage);
            this.splitContainer1.Panel1.Controls.Add(this.lb_title);
            this.splitContainer1.Panel1.Controls.Add(this.panel_logo);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(1880, 1017);
            this.splitContainer1.SplitterDistance = 70;
            this.splitContainer1.TabIndex = 0;
            // 
            // digitalTimer1
            // 
            this.digitalTimer1.BackColor = System.Drawing.Color.Black;
            this.digitalTimer1.DigitColor = System.Drawing.Color.White;
            this.digitalTimer1.Location = new System.Drawing.Point(1611, 3);
            this.digitalTimer1.Name = "digitalTimer1";
            this.digitalTimer1.Size = new System.Drawing.Size(266, 65);
            this.digitalTimer1.TabIndex = 8;
            // 
            // panelSafety
            // 
            this.panelSafety.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelSafety.BackgroundImage")));
            this.panelSafety.Location = new System.Drawing.Point(1528, 3);
            this.panelSafety.Name = "panelSafety";
            this.panelSafety.Size = new System.Drawing.Size(64, 64);
            this.panelSafety.TabIndex = 2;
            // 
            // panelWarning
            // 
            this.panelWarning.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelWarning.BackgroundImage")));
            this.panelWarning.Location = new System.Drawing.Point(1458, 3);
            this.panelWarning.Name = "panelWarning";
            this.panelWarning.Size = new System.Drawing.Size(64, 64);
            this.panelWarning.TabIndex = 1;
            // 
            // panelDanger
            // 
            this.panelDanger.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelDanger.BackgroundImage")));
            this.panelDanger.Location = new System.Drawing.Point(1388, 3);
            this.panelDanger.Name = "panelDanger";
            this.panelDanger.Size = new System.Drawing.Size(64, 64);
            this.panelDanger.TabIndex = 0;
            // 
            // btnALarmLog
            // 
            this.btnALarmLog.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnALarmLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnALarmLog.Location = new System.Drawing.Point(1258, 3);
            this.btnALarmLog.Name = "btnALarmLog";
            this.btnALarmLog.Size = new System.Drawing.Size(104, 64);
            this.btnALarmLog.TabIndex = 7;
            this.btnALarmLog.Text = "알람 이력";
            this.btnALarmLog.UseVisualStyleBackColor = false;
            // 
            // btnHostMessage
            // 
            this.btnHostMessage.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnHostMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnHostMessage.Location = new System.Drawing.Point(1148, 3);
            this.btnHostMessage.Name = "btnHostMessage";
            this.btnHostMessage.Size = new System.Drawing.Size(104, 64);
            this.btnHostMessage.TabIndex = 6;
            this.btnHostMessage.Text = "호스트 메세지";
            this.btnHostMessage.UseVisualStyleBackColor = false;
            // 
            // btnSem
            // 
            this.btnSem.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnSem.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnSem.Location = new System.Drawing.Point(1038, 3);
            this.btnSem.Name = "btnSem";
            this.btnSem.Size = new System.Drawing.Size(104, 64);
            this.btnSem.TabIndex = 5;
            this.btnSem.Text = "SEM";
            this.btnSem.UseVisualStyleBackColor = false;
            // 
            // btnOnoffLine
            // 
            this.btnOnoffLine.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnOnoffLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnOnoffLine.Location = new System.Drawing.Point(928, 3);
            this.btnOnoffLine.Name = "btnOnoffLine";
            this.btnOnoffLine.Size = new System.Drawing.Size(104, 64);
            this.btnOnoffLine.TabIndex = 4;
            this.btnOnoffLine.Text = "Off-Line";
            this.btnOnoffLine.UseVisualStyleBackColor = false;
            // 
            // txtterminalMessage
            // 
            this.txtterminalMessage.Font = new System.Drawing.Font("굴림", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtterminalMessage.Location = new System.Drawing.Point(503, 13);
            this.txtterminalMessage.Name = "txtterminalMessage";
            this.txtterminalMessage.ReadOnly = true;
            this.txtterminalMessage.Size = new System.Drawing.Size(419, 44);
            this.txtterminalMessage.TabIndex = 3;
            // 
            // lbTerminalMessage
            // 
            this.lbTerminalMessage.AutoSize = true;
            this.lbTerminalMessage.BackColor = System.Drawing.Color.Transparent;
            this.lbTerminalMessage.ForeColor = System.Drawing.Color.White;
            this.lbTerminalMessage.Location = new System.Drawing.Point(416, 29);
            this.lbTerminalMessage.Name = "lbTerminalMessage";
            this.lbTerminalMessage.Size = new System.Drawing.Size(81, 12);
            this.lbTerminalMessage.TabIndex = 2;
            this.lbTerminalMessage.Text = "터미널 메세지";
            // 
            // lb_title
            // 
            this.lb_title.AutoSize = true;
            this.lb_title.Font = new System.Drawing.Font("휴먼둥근헤드라인", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_title.ForeColor = System.Drawing.Color.White;
            this.lb_title.Location = new System.Drawing.Point(130, 25);
            this.lb_title.Name = "lb_title";
            this.lb_title.Size = new System.Drawing.Size(280, 19);
            this.lb_title.TabIndex = 1;
            this.lb_title.Text = "Trench Laser Cutting";
            // 
            // panel_logo
            // 
            this.panel_logo.BackColor = System.Drawing.Color.White;
            this.panel_logo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel_logo.BackgroundImage")));
            this.panel_logo.Location = new System.Drawing.Point(4, 3);
            this.panel_logo.Name = "panel_logo";
            this.panel_logo.Size = new System.Drawing.Size(120, 65);
            this.panel_logo.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.BackColor = System.Drawing.Color.Black;
            this.splitContainer2.Panel1.Controls.Add(this.splitContainer3);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.BackColor = System.Drawing.Color.DarkSlateGray;
            this.splitContainer2.Panel2.Controls.Add(this.btn_graph);
            this.splitContainer2.Panel2.Controls.Add(this.btn_measure);
            this.splitContainer2.Panel2.Controls.Add(this.btn_log);
            this.splitContainer2.Panel2.Controls.Add(this.btn_param);
            this.splitContainer2.Panel2.Controls.Add(this.btn_manager);
            this.splitContainer2.Panel2.Controls.Add(this.btn_recipe);
            this.splitContainer2.Panel2.Controls.Add(this.btn_main);
            this.splitContainer2.Panel2.Controls.Add(this.btn_login);
            this.splitContainer2.Panel2.Controls.Add(this.btn_exit);
            this.splitContainer2.Size = new System.Drawing.Size(1880, 943);
            this.splitContainer2.SplitterDistance = 860;
            this.splitContainer2.TabIndex = 0;
            // 
            // splitContainer3
            // 
            this.splitContainer3.BackColor = System.Drawing.Color.Gray;
            this.splitContainer3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.splitContainer3.IsSplitterFixed = true;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.splitContainer3.Panel1.Margin = new System.Windows.Forms.Padding(3);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.BackColor = System.Drawing.Color.DarkSlateGray;
            this.splitContainer3.Panel2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.splitContainer3.Panel2Collapsed = true;
            this.splitContainer3.Size = new System.Drawing.Size(1880, 860);
            this.splitContainer3.SplitterDistance = 1740;
            this.splitContainer3.TabIndex = 0;
            // 
            // btn_graph
            // 
            this.btn_graph.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_graph.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_graph.ForeColor = System.Drawing.Color.White;
            this.btn_graph.Location = new System.Drawing.Point(1360, 3);
            this.btn_graph.Name = "btn_graph";
            this.btn_graph.Size = new System.Drawing.Size(150, 73);
            this.btn_graph.TabIndex = 8;
            this.btn_graph.Text = "그래프";
            this.btn_graph.UseVisualStyleBackColor = false;
            this.btn_graph.Click += new System.EventHandler(this.btn_graph_Click);
            // 
            // btn_measure
            // 
            this.btn_measure.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_measure.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_measure.ForeColor = System.Drawing.Color.White;
            this.btn_measure.Location = new System.Drawing.Point(1204, 3);
            this.btn_measure.Name = "btn_measure";
            this.btn_measure.Size = new System.Drawing.Size(150, 73);
            this.btn_measure.TabIndex = 7;
            this.btn_measure.Text = "측정";
            this.btn_measure.UseVisualStyleBackColor = false;
            this.btn_measure.Click += new System.EventHandler(this.btn_measure_Click);
            // 
            // btn_log
            // 
            this.btn_log.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_log.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_log.ForeColor = System.Drawing.Color.White;
            this.btn_log.Location = new System.Drawing.Point(1048, 3);
            this.btn_log.Name = "btn_log";
            this.btn_log.Size = new System.Drawing.Size(150, 73);
            this.btn_log.TabIndex = 6;
            this.btn_log.Text = "로그";
            this.btn_log.UseVisualStyleBackColor = false;
            this.btn_log.Click += new System.EventHandler(this.btn_log_Click);
            // 
            // btn_param
            // 
            this.btn_param.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_param.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_param.ForeColor = System.Drawing.Color.White;
            this.btn_param.Location = new System.Drawing.Point(892, 3);
            this.btn_param.Name = "btn_param";
            this.btn_param.Size = new System.Drawing.Size(150, 73);
            this.btn_param.TabIndex = 5;
            this.btn_param.Text = "파라미터";
            this.btn_param.UseVisualStyleBackColor = false;
            this.btn_param.Click += new System.EventHandler(this.btn_param_Click);
            // 
            // btn_manager
            // 
            this.btn_manager.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_manager.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_manager.ForeColor = System.Drawing.Color.White;
            this.btn_manager.Location = new System.Drawing.Point(736, 3);
            this.btn_manager.Name = "btn_manager";
            this.btn_manager.Size = new System.Drawing.Size(150, 73);
            this.btn_manager.TabIndex = 4;
            this.btn_manager.Text = "유지보수";
            this.btn_manager.UseVisualStyleBackColor = false;
            this.btn_manager.Click += new System.EventHandler(this.btn_manager_Click);
            // 
            // btn_recipe
            // 
            this.btn_recipe.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_recipe.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_recipe.ForeColor = System.Drawing.Color.White;
            this.btn_recipe.Location = new System.Drawing.Point(580, 3);
            this.btn_recipe.Name = "btn_recipe";
            this.btn_recipe.Size = new System.Drawing.Size(150, 73);
            this.btn_recipe.TabIndex = 3;
            this.btn_recipe.Text = "레시피";
            this.btn_recipe.UseVisualStyleBackColor = false;
            this.btn_recipe.Click += new System.EventHandler(this.btn_recipe_Click);
            // 
            // btn_main
            // 
            this.btn_main.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_main.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_main.ForeColor = System.Drawing.Color.White;
            this.btn_main.Location = new System.Drawing.Point(424, 3);
            this.btn_main.Name = "btn_main";
            this.btn_main.Size = new System.Drawing.Size(150, 73);
            this.btn_main.TabIndex = 2;
            this.btn_main.Text = "메인화면";
            this.btn_main.UseVisualStyleBackColor = false;
            this.btn_main.Click += new System.EventHandler(this.btn_main_Click);
            // 
            // btn_login
            // 
            this.btn_login.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_login.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_login.ForeColor = System.Drawing.Color.White;
            this.btn_login.Location = new System.Drawing.Point(268, 3);
            this.btn_login.Name = "btn_login";
            this.btn_login.Size = new System.Drawing.Size(150, 73);
            this.btn_login.TabIndex = 1;
            this.btn_login.Text = "로그인";
            this.btn_login.UseVisualStyleBackColor = false;
            this.btn_login.Click += new System.EventHandler(this.btn_login_Click);
            // 
            // btn_exit
            // 
            this.btn_exit.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_exit.ForeColor = System.Drawing.Color.White;
            this.btn_exit.Image = ((System.Drawing.Image)(resources.GetObject("btn_exit.Image")));
            this.btn_exit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_exit.Location = new System.Drawing.Point(3, 3);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(150, 73);
            this.btn_exit.TabIndex = 0;
            this.btn_exit.Text = "종  료  ";
            this.btn_exit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_exit.UseVisualStyleBackColor = false;
            this.btn_exit.Click += new System.EventHandler(this.btn_exit_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(1904, 1041);
            this.Controls.Add(this.splitContainer1);
            this.Name = "FrmMain";
            this.Text = "Main";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.Panel panel_logo;
        private System.Windows.Forms.Label lb_title;
        private System.Windows.Forms.Label lbTerminalMessage;
        private System.Windows.Forms.TextBox txtterminalMessage;
        private System.Windows.Forms.Button btnOnoffLine;
        private System.Windows.Forms.Button btnSem;
        private System.Windows.Forms.Button btnALarmLog;
        private System.Windows.Forms.Button btnHostMessage;
        private System.Windows.Forms.Button btn_exit;
        private System.Windows.Forms.Button btn_main;
        private System.Windows.Forms.Button btn_login;
        private System.Windows.Forms.Button btn_graph;
        private System.Windows.Forms.Button btn_measure;
        private System.Windows.Forms.Button btn_log;
        private System.Windows.Forms.Button btn_param;
        private System.Windows.Forms.Button btn_manager;
        private System.Windows.Forms.Button btn_recipe;
        private System.Windows.Forms.Panel panelDanger;
        private System.Windows.Forms.Panel panelSafety;
        private System.Windows.Forms.Panel panelWarning;
        private System.Windows.Forms.Timer timer1;
        private DigitalTimer digitalTimer1;
    }
}

