﻿namespace DIT.TLC.UI
{
    partial class Manager_Control
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btn_Loading_LoaderTransfer_LoadingTransfer2Align = new System.Windows.Forms.Button();
            this.btn_Loading_LoaderTransfer_CylinderReset = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btn_Loading_LoaderTransfer_B_DestructionOff = new System.Windows.Forms.Button();
            this.btn_Loading_LoaderTransfer_B_PneumaticOff = new System.Windows.Forms.Button();
            this.btn_Loading_LoaderTransfer_B_DestructionOn = new System.Windows.Forms.Button();
            this.btn_Loading_LoaderTransfer_B_PneumaticOn = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btn_Loading_LoaderTransfer_A_DestructionOff = new System.Windows.Forms.Button();
            this.btn_Loading_LoaderTransfer_A_DestructionOn = new System.Windows.Forms.Button();
            this.btn_Loading_LoaderTransfer_A_PneumaticOff = new System.Windows.Forms.Button();
            this.btn_Loading_LoaderTransfer_A_PneumaticOn = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btn_Loading_B_FreeAilgn = new System.Windows.Forms.Button();
            this.btn_Loading_B_CST2Loader = new System.Windows.Forms.Button();
            this.btn_Loading_B_CST2Stage = new System.Windows.Forms.Button();
            this.btn_Loading_B_CST2Lift = new System.Windows.Forms.Button();
            this.btn_Loading_B_CSTLift2Buffer = new System.Windows.Forms.Button();
            this.btn_Loading_B_Loader2LoadingTransfer = new System.Windows.Forms.Button();
            this.btn_Loading_B_LoadingTransfer2Stage = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btn_Loading_Loader_CylinderReset = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbox_Loading_Loader_B_Gear = new System.Windows.Forms.TextBox();
            this.btn_Loading_Loader_B_CylinderDown = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_Loading_Loader_B_UldY = new System.Windows.Forms.Button();
            this.btn_Loading_Loader_B_XYCst = new System.Windows.Forms.Button();
            this.btn_Loading_Loader_B_PneumaticOff = new System.Windows.Forms.Button();
            this.btn_Loading_Loader_B_XYMid = new System.Windows.Forms.Button();
            this.btn_Loading_Loader_B_DestructionOff = new System.Windows.Forms.Button();
            this.btn_Loading_Loader_B_XMove = new System.Windows.Forms.Button();
            this.btn_Loading_Loader_B_Col2_4 = new System.Windows.Forms.Button();
            this.btn_Loading_Loader_B_Col1_3 = new System.Windows.Forms.Button();
            this.btn_Loading_Loader_B_CylinderUp = new System.Windows.Forms.Button();
            this.btn_Loading_Loader_B_DestructionOn = new System.Windows.Forms.Button();
            this.btn_Loading_Loader_B_PneumaticOn = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbox_Loading_Loader_A_Gear = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btn_Loading_Loader_A_CylinderDown = new System.Windows.Forms.Button();
            this.btn_Loading_Loader_A_PneumaticOff = new System.Windows.Forms.Button();
            this.btn_Loading_Loader_A_DestructionOff = new System.Windows.Forms.Button();
            this.btn_Loading_Loader_A_Col2_4 = new System.Windows.Forms.Button();
            this.btn_Loading_Loader_A_CylinderUp = new System.Windows.Forms.Button();
            this.btn_Loading_Loader_A_PneumaticOn = new System.Windows.Forms.Button();
            this.btn_Loading_Loader_A_DestructionOn = new System.Windows.Forms.Button();
            this.btn_Loading_Loader_A_Col1_3 = new System.Windows.Forms.Button();
            this.btn_Loading_Loader_A_XMove = new System.Windows.Forms.Button();
            this.btn_Loading_Loader_A_XYMid = new System.Windows.Forms.Button();
            this.btn_Loading_Loader_A_XYCst = new System.Windows.Forms.Button();
            this.btn_Loading_Loader_A_UldY = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btn_Loading_A_FreeAilgn = new System.Windows.Forms.Button();
            this.btn_Loading_A_CST2Stage = new System.Windows.Forms.Button();
            this.btn_Loading_A_CSTLift2Buffer = new System.Windows.Forms.Button();
            this.btn_Loading_A_LoadingTransfer2Stage = new System.Windows.Forms.Button();
            this.btn_Loading_A_Loader2LoadingTransfer = new System.Windows.Forms.Button();
            this.btn_Loading_A_CST2Loader = new System.Windows.Forms.Button();
            this.btn_Loading_A_CST2Lift = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btn_Unloading_UnloaderTransfer_CylinderReset = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.btn_Unloading_UnloaderTransfer_B_UnloadingTransfer2Inspaction = new System.Windows.Forms.Button();
            this.btn_Unloading_UnloaderTransfer_B_UnloadingTransfer2MCR = new System.Windows.Forms.Button();
            this.btn_Unloading_UnloaderTransfer_B_DestructionOff = new System.Windows.Forms.Button();
            this.btn_Unloading_UnloaderTransfer_B_DestructionOn = new System.Windows.Forms.Button();
            this.btn_Unloading_UnloaderTransfer_B_PneumaticOff = new System.Windows.Forms.Button();
            this.btn_Unloading_UnloaderTransfer_B_PneumaticOn = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.btn_Unloading_UnloaderTransfer_A_UnloadingTransfer2Inspaction = new System.Windows.Forms.Button();
            this.btn_Unloading_UnloaderTransfer_A_UnloadingTransfer2MCR = new System.Windows.Forms.Button();
            this.btn_Unloading_UnloaderTransfer_A_DestructionOff = new System.Windows.Forms.Button();
            this.btn_Unloading_UnloaderTransfer_A_DestructionOn = new System.Windows.Forms.Button();
            this.btn_Unloading_UnloaderTransfer_A_PneumaticOff = new System.Windows.Forms.Button();
            this.btn_Unloading_UnloaderTransfer_A_PneumaticOn = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btn_Unloading_B_CIM = new System.Windows.Forms.Button();
            this.btn_Unloading_B_CST2Lift = new System.Windows.Forms.Button();
            this.btn_Unloading_B_CSTLift2Buffer = new System.Windows.Forms.Button();
            this.btn_Unloading_B_Unloader2CST = new System.Windows.Forms.Button();
            this.btn_Unloading_B_UnloadingTransfer2Unloader = new System.Windows.Forms.Button();
            this.btn_Unloading_B_Stage2UnloadingTransfer = new System.Windows.Forms.Button();
            this.panel9 = new System.Windows.Forms.Panel();
            this.btn_Unloading_Unloader_CylinderReset = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.tbox_Unloading_Unloader_B_Gear = new System.Windows.Forms.TextBox();
            this.btn_Unloading_Unloader_B_PneumaticOff = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_Unloading_Unloader_B_UldY = new System.Windows.Forms.Button();
            this.btn_Unloading_Unloader_B_CylinderDown = new System.Windows.Forms.Button();
            this.btn_Unloading_Unloader_B_XYCst = new System.Windows.Forms.Button();
            this.btn_Unloading_Unloader_B_XYMid = new System.Windows.Forms.Button();
            this.btn_Unloading_Unloader_B_DestructionOff = new System.Windows.Forms.Button();
            this.btn_Unloading_Unloader_B_XMove = new System.Windows.Forms.Button();
            this.btn_Unloading_Unloader_B_Col2_4 = new System.Windows.Forms.Button();
            this.btn_Unloading_Unloader_B_Col1_3 = new System.Windows.Forms.Button();
            this.btn_Unloading_Unloader_B_CylinderUp = new System.Windows.Forms.Button();
            this.btn_Unloading_Unloader_B_DestructionOn = new System.Windows.Forms.Button();
            this.btn_Unloading_Unloader_B_PneumaticOn = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.tbox_Unloading_Unloader_A_Gear = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_Unloading_Unloader_A_CylinderDown = new System.Windows.Forms.Button();
            this.btn_Unloading_Unloader_A_PneumaticOff = new System.Windows.Forms.Button();
            this.btn_Unloading_Unloader_A_DestructionOff = new System.Windows.Forms.Button();
            this.btn_Unloading_Unloader_A_Col2_4 = new System.Windows.Forms.Button();
            this.btn_Unloading_Unloader_A_CylinderUp = new System.Windows.Forms.Button();
            this.btn_Unloading_Unloader_A_PneumaticOn = new System.Windows.Forms.Button();
            this.btn_Unloading_Unloader_A_DestructionOn = new System.Windows.Forms.Button();
            this.btn_Unloading_Unloader_A_Col1_3 = new System.Windows.Forms.Button();
            this.btn_Unloading_Unloader_A_XMove = new System.Windows.Forms.Button();
            this.btn_Unloading_Unloader_A_XYMid = new System.Windows.Forms.Button();
            this.btn_Unloading_Unloader_A_XYCst = new System.Windows.Forms.Button();
            this.btn_Unloading_Unloader_A_UldY = new System.Windows.Forms.Button();
            this.panel10 = new System.Windows.Forms.Panel();
            this.btn_Unloading_A_CIM = new System.Windows.Forms.Button();
            this.btn_Unloading_A_CST2Lift = new System.Windows.Forms.Button();
            this.btn_Unloading_A_Unloader2CST = new System.Windows.Forms.Button();
            this.btn_Unloading_A_Stage2UnloadingTransfer = new System.Windows.Forms.Button();
            this.btn_Unloading_A_UnloadingTransfer2Unloader = new System.Windows.Forms.Button();
            this.btn_Unloading_A_CSTLift2Buffer = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.panel23 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel6.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel7.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel11.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel23);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(865, 869);
            this.panel1.TabIndex = 2;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Purple;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.label7);
            this.panel5.Controls.Add(this.btn_Loading_LoaderTransfer_LoadingTransfer2Align);
            this.panel5.Controls.Add(this.btn_Loading_LoaderTransfer_CylinderReset);
            this.panel5.Controls.Add(this.groupBox3);
            this.panel5.Controls.Add(this.groupBox4);
            this.panel5.Location = new System.Drawing.Point(442, 417);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(409, 438);
            this.panel5.TabIndex = 4;
            // 
            // btn_Loading_LoaderTransfer_LoadingTransfer2Align
            // 
            this.btn_Loading_LoaderTransfer_LoadingTransfer2Align.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_LoaderTransfer_LoadingTransfer2Align.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.btn_Loading_LoaderTransfer_LoadingTransfer2Align.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_LoaderTransfer_LoadingTransfer2Align.Location = new System.Drawing.Point(204, 21);
            this.btn_Loading_LoaderTransfer_LoadingTransfer2Align.Name = "btn_Loading_LoaderTransfer_LoadingTransfer2Align";
            this.btn_Loading_LoaderTransfer_LoadingTransfer2Align.Size = new System.Drawing.Size(188, 38);
            this.btn_Loading_LoaderTransfer_LoadingTransfer2Align.TabIndex = 9;
            this.btn_Loading_LoaderTransfer_LoadingTransfer2Align.Text = "로딩 트랜스퍼 → 얼라인 ";
            this.btn_Loading_LoaderTransfer_LoadingTransfer2Align.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_LoaderTransfer_CylinderReset
            // 
            this.btn_Loading_LoaderTransfer_CylinderReset.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_LoaderTransfer_CylinderReset.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Loading_LoaderTransfer_CylinderReset.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_LoaderTransfer_CylinderReset.Location = new System.Drawing.Point(10, 21);
            this.btn_Loading_LoaderTransfer_CylinderReset.Name = "btn_Loading_LoaderTransfer_CylinderReset";
            this.btn_Loading_LoaderTransfer_CylinderReset.Size = new System.Drawing.Size(188, 38);
            this.btn_Loading_LoaderTransfer_CylinderReset.TabIndex = 8;
            this.btn_Loading_LoaderTransfer_CylinderReset.Text = "실린더 초기화";
            this.btn_Loading_LoaderTransfer_CylinderReset.UseVisualStyleBackColor = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btn_Loading_LoaderTransfer_B_DestructionOff);
            this.groupBox3.Controls.Add(this.btn_Loading_LoaderTransfer_B_PneumaticOff);
            this.groupBox3.Controls.Add(this.btn_Loading_LoaderTransfer_B_DestructionOn);
            this.groupBox3.Controls.Add(this.btn_Loading_LoaderTransfer_B_PneumaticOn);
            this.groupBox3.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold);
            this.groupBox3.ForeColor = System.Drawing.Color.White;
            this.groupBox3.Location = new System.Drawing.Point(204, 57);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(188, 176);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = " B ";
            // 
            // btn_Loading_LoaderTransfer_B_DestructionOff
            // 
            this.btn_Loading_LoaderTransfer_B_DestructionOff.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_LoaderTransfer_B_DestructionOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Loading_LoaderTransfer_B_DestructionOff.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_LoaderTransfer_B_DestructionOff.Location = new System.Drawing.Point(96, 99);
            this.btn_Loading_LoaderTransfer_B_DestructionOff.Name = "btn_Loading_LoaderTransfer_B_DestructionOff";
            this.btn_Loading_LoaderTransfer_B_DestructionOff.Size = new System.Drawing.Size(85, 67);
            this.btn_Loading_LoaderTransfer_B_DestructionOff.TabIndex = 38;
            this.btn_Loading_LoaderTransfer_B_DestructionOff.Text = "파기 오프";
            this.btn_Loading_LoaderTransfer_B_DestructionOff.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_LoaderTransfer_B_PneumaticOff
            // 
            this.btn_Loading_LoaderTransfer_B_PneumaticOff.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_LoaderTransfer_B_PneumaticOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Loading_LoaderTransfer_B_PneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_LoaderTransfer_B_PneumaticOff.Location = new System.Drawing.Point(97, 26);
            this.btn_Loading_LoaderTransfer_B_PneumaticOff.Name = "btn_Loading_LoaderTransfer_B_PneumaticOff";
            this.btn_Loading_LoaderTransfer_B_PneumaticOff.Size = new System.Drawing.Size(85, 67);
            this.btn_Loading_LoaderTransfer_B_PneumaticOff.TabIndex = 36;
            this.btn_Loading_LoaderTransfer_B_PneumaticOff.Text = "공압 오프";
            this.btn_Loading_LoaderTransfer_B_PneumaticOff.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_LoaderTransfer_B_DestructionOn
            // 
            this.btn_Loading_LoaderTransfer_B_DestructionOn.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_LoaderTransfer_B_DestructionOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Loading_LoaderTransfer_B_DestructionOn.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_LoaderTransfer_B_DestructionOn.Location = new System.Drawing.Point(7, 99);
            this.btn_Loading_LoaderTransfer_B_DestructionOn.Name = "btn_Loading_LoaderTransfer_B_DestructionOn";
            this.btn_Loading_LoaderTransfer_B_DestructionOn.Size = new System.Drawing.Size(85, 67);
            this.btn_Loading_LoaderTransfer_B_DestructionOn.TabIndex = 37;
            this.btn_Loading_LoaderTransfer_B_DestructionOn.Text = "파기 온";
            this.btn_Loading_LoaderTransfer_B_DestructionOn.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_LoaderTransfer_B_PneumaticOn
            // 
            this.btn_Loading_LoaderTransfer_B_PneumaticOn.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_LoaderTransfer_B_PneumaticOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Loading_LoaderTransfer_B_PneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_LoaderTransfer_B_PneumaticOn.Location = new System.Drawing.Point(8, 26);
            this.btn_Loading_LoaderTransfer_B_PneumaticOn.Name = "btn_Loading_LoaderTransfer_B_PneumaticOn";
            this.btn_Loading_LoaderTransfer_B_PneumaticOn.Size = new System.Drawing.Size(85, 67);
            this.btn_Loading_LoaderTransfer_B_PneumaticOn.TabIndex = 35;
            this.btn_Loading_LoaderTransfer_B_PneumaticOn.Text = "공압 온";
            this.btn_Loading_LoaderTransfer_B_PneumaticOn.UseVisualStyleBackColor = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btn_Loading_LoaderTransfer_A_DestructionOff);
            this.groupBox4.Controls.Add(this.btn_Loading_LoaderTransfer_A_DestructionOn);
            this.groupBox4.Controls.Add(this.btn_Loading_LoaderTransfer_A_PneumaticOff);
            this.groupBox4.Controls.Add(this.btn_Loading_LoaderTransfer_A_PneumaticOn);
            this.groupBox4.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox4.ForeColor = System.Drawing.Color.White;
            this.groupBox4.Location = new System.Drawing.Point(10, 57);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(188, 176);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = " A ";
            // 
            // btn_Loading_LoaderTransfer_A_DestructionOff
            // 
            this.btn_Loading_LoaderTransfer_A_DestructionOff.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_LoaderTransfer_A_DestructionOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Loading_LoaderTransfer_A_DestructionOff.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_LoaderTransfer_A_DestructionOff.Location = new System.Drawing.Point(95, 99);
            this.btn_Loading_LoaderTransfer_A_DestructionOff.Name = "btn_Loading_LoaderTransfer_A_DestructionOff";
            this.btn_Loading_LoaderTransfer_A_DestructionOff.Size = new System.Drawing.Size(85, 67);
            this.btn_Loading_LoaderTransfer_A_DestructionOff.TabIndex = 34;
            this.btn_Loading_LoaderTransfer_A_DestructionOff.Text = "파기 오프";
            this.btn_Loading_LoaderTransfer_A_DestructionOff.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_LoaderTransfer_A_DestructionOn
            // 
            this.btn_Loading_LoaderTransfer_A_DestructionOn.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_LoaderTransfer_A_DestructionOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Loading_LoaderTransfer_A_DestructionOn.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_LoaderTransfer_A_DestructionOn.Location = new System.Drawing.Point(6, 99);
            this.btn_Loading_LoaderTransfer_A_DestructionOn.Name = "btn_Loading_LoaderTransfer_A_DestructionOn";
            this.btn_Loading_LoaderTransfer_A_DestructionOn.Size = new System.Drawing.Size(85, 67);
            this.btn_Loading_LoaderTransfer_A_DestructionOn.TabIndex = 33;
            this.btn_Loading_LoaderTransfer_A_DestructionOn.Text = "파기 온";
            this.btn_Loading_LoaderTransfer_A_DestructionOn.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_LoaderTransfer_A_PneumaticOff
            // 
            this.btn_Loading_LoaderTransfer_A_PneumaticOff.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_LoaderTransfer_A_PneumaticOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Loading_LoaderTransfer_A_PneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_LoaderTransfer_A_PneumaticOff.Location = new System.Drawing.Point(96, 26);
            this.btn_Loading_LoaderTransfer_A_PneumaticOff.Name = "btn_Loading_LoaderTransfer_A_PneumaticOff";
            this.btn_Loading_LoaderTransfer_A_PneumaticOff.Size = new System.Drawing.Size(85, 67);
            this.btn_Loading_LoaderTransfer_A_PneumaticOff.TabIndex = 32;
            this.btn_Loading_LoaderTransfer_A_PneumaticOff.Text = "공압 오프";
            this.btn_Loading_LoaderTransfer_A_PneumaticOff.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_LoaderTransfer_A_PneumaticOn
            // 
            this.btn_Loading_LoaderTransfer_A_PneumaticOn.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_LoaderTransfer_A_PneumaticOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Loading_LoaderTransfer_A_PneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_LoaderTransfer_A_PneumaticOn.Location = new System.Drawing.Point(7, 26);
            this.btn_Loading_LoaderTransfer_A_PneumaticOn.Name = "btn_Loading_LoaderTransfer_A_PneumaticOn";
            this.btn_Loading_LoaderTransfer_A_PneumaticOn.Size = new System.Drawing.Size(85, 67);
            this.btn_Loading_LoaderTransfer_A_PneumaticOn.TabIndex = 31;
            this.btn_Loading_LoaderTransfer_A_PneumaticOn.Text = "공압 온";
            this.btn_Loading_LoaderTransfer_A_PneumaticOn.UseVisualStyleBackColor = false;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Purple;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.btn_Loading_B_FreeAilgn);
            this.panel4.Controls.Add(this.btn_Loading_B_CST2Loader);
            this.panel4.Controls.Add(this.btn_Loading_B_CST2Stage);
            this.panel4.Controls.Add(this.btn_Loading_B_CST2Lift);
            this.panel4.Controls.Add(this.btn_Loading_B_CSTLift2Buffer);
            this.panel4.Controls.Add(this.btn_Loading_B_Loader2LoadingTransfer);
            this.panel4.Controls.Add(this.btn_Loading_B_LoadingTransfer2Stage);
            this.panel4.Location = new System.Drawing.Point(442, 65);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(409, 330);
            this.panel4.TabIndex = 2;
            // 
            // btn_Loading_B_FreeAilgn
            // 
            this.btn_Loading_B_FreeAilgn.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_B_FreeAilgn.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Loading_B_FreeAilgn.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_B_FreeAilgn.Location = new System.Drawing.Point(242, 124);
            this.btn_Loading_B_FreeAilgn.Name = "btn_Loading_B_FreeAilgn";
            this.btn_Loading_B_FreeAilgn.Size = new System.Drawing.Size(126, 38);
            this.btn_Loading_B_FreeAilgn.TabIndex = 13;
            this.btn_Loading_B_FreeAilgn.Text = "프리 얼라인";
            this.btn_Loading_B_FreeAilgn.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_B_CST2Loader
            // 
            this.btn_Loading_B_CST2Loader.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_B_CST2Loader.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Loading_B_CST2Loader.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_B_CST2Loader.Location = new System.Drawing.Point(39, 73);
            this.btn_Loading_B_CST2Loader.Name = "btn_Loading_B_CST2Loader";
            this.btn_Loading_B_CST2Loader.Size = new System.Drawing.Size(329, 38);
            this.btn_Loading_B_CST2Loader.TabIndex = 8;
            this.btn_Loading_B_CST2Loader.Text = "카세트  → 로더";
            this.btn_Loading_B_CST2Loader.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_B_CST2Stage
            // 
            this.btn_Loading_B_CST2Stage.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_B_CST2Stage.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Loading_B_CST2Stage.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_B_CST2Stage.Location = new System.Drawing.Point(39, 277);
            this.btn_Loading_B_CST2Stage.Name = "btn_Loading_B_CST2Stage";
            this.btn_Loading_B_CST2Stage.Size = new System.Drawing.Size(329, 38);
            this.btn_Loading_B_CST2Stage.TabIndex = 12;
            this.btn_Loading_B_CST2Stage.Text = "카세트  → 스테이지";
            this.btn_Loading_B_CST2Stage.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_B_CST2Lift
            // 
            this.btn_Loading_B_CST2Lift.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_B_CST2Lift.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Loading_B_CST2Lift.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_B_CST2Lift.Location = new System.Drawing.Point(39, 22);
            this.btn_Loading_B_CST2Lift.Name = "btn_Loading_B_CST2Lift";
            this.btn_Loading_B_CST2Lift.Size = new System.Drawing.Size(329, 38);
            this.btn_Loading_B_CST2Lift.TabIndex = 7;
            this.btn_Loading_B_CST2Lift.Text = "카세트 → 리프트";
            this.btn_Loading_B_CST2Lift.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_B_CSTLift2Buffer
            // 
            this.btn_Loading_B_CSTLift2Buffer.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_B_CSTLift2Buffer.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Loading_B_CSTLift2Buffer.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_B_CSTLift2Buffer.Location = new System.Drawing.Point(39, 226);
            this.btn_Loading_B_CSTLift2Buffer.Name = "btn_Loading_B_CSTLift2Buffer";
            this.btn_Loading_B_CSTLift2Buffer.Size = new System.Drawing.Size(329, 38);
            this.btn_Loading_B_CSTLift2Buffer.TabIndex = 11;
            this.btn_Loading_B_CSTLift2Buffer.Text = "카세트 리프트  → 버퍼";
            this.btn_Loading_B_CSTLift2Buffer.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_B_Loader2LoadingTransfer
            // 
            this.btn_Loading_B_Loader2LoadingTransfer.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_B_Loader2LoadingTransfer.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Loading_B_Loader2LoadingTransfer.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_B_Loader2LoadingTransfer.Location = new System.Drawing.Point(39, 124);
            this.btn_Loading_B_Loader2LoadingTransfer.Name = "btn_Loading_B_Loader2LoadingTransfer";
            this.btn_Loading_B_Loader2LoadingTransfer.Size = new System.Drawing.Size(182, 38);
            this.btn_Loading_B_Loader2LoadingTransfer.TabIndex = 9;
            this.btn_Loading_B_Loader2LoadingTransfer.Text = "로더 → 로딩 트랜스퍼";
            this.btn_Loading_B_Loader2LoadingTransfer.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_B_LoadingTransfer2Stage
            // 
            this.btn_Loading_B_LoadingTransfer2Stage.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_B_LoadingTransfer2Stage.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Loading_B_LoadingTransfer2Stage.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_B_LoadingTransfer2Stage.Location = new System.Drawing.Point(39, 175);
            this.btn_Loading_B_LoadingTransfer2Stage.Name = "btn_Loading_B_LoadingTransfer2Stage";
            this.btn_Loading_B_LoadingTransfer2Stage.Size = new System.Drawing.Size(329, 38);
            this.btn_Loading_B_LoadingTransfer2Stage.TabIndex = 10;
            this.btn_Loading_B_LoadingTransfer2Stage.Text = "로딩 트랜스퍼  → 스테이지";
            this.btn_Loading_B_LoadingTransfer2Stage.UseVisualStyleBackColor = false;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.SlateGray;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.label6);
            this.panel6.Controls.Add(this.btn_Loading_Loader_CylinderReset);
            this.panel6.Controls.Add(this.groupBox2);
            this.panel6.Controls.Add(this.groupBox1);
            this.panel6.Location = new System.Drawing.Point(18, 417);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(409, 438);
            this.panel6.TabIndex = 3;
            // 
            // btn_Loading_Loader_CylinderReset
            // 
            this.btn_Loading_Loader_CylinderReset.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_Loader_CylinderReset.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Loading_Loader_CylinderReset.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_Loader_CylinderReset.Location = new System.Drawing.Point(13, 21);
            this.btn_Loading_Loader_CylinderReset.Name = "btn_Loading_Loader_CylinderReset";
            this.btn_Loading_Loader_CylinderReset.Size = new System.Drawing.Size(188, 38);
            this.btn_Loading_Loader_CylinderReset.TabIndex = 7;
            this.btn_Loading_Loader_CylinderReset.Text = "실린더 초기화";
            this.btn_Loading_Loader_CylinderReset.UseVisualStyleBackColor = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tbox_Loading_Loader_B_Gear);
            this.groupBox2.Controls.Add(this.btn_Loading_Loader_B_CylinderDown);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.btn_Loading_Loader_B_UldY);
            this.groupBox2.Controls.Add(this.btn_Loading_Loader_B_XYCst);
            this.groupBox2.Controls.Add(this.btn_Loading_Loader_B_PneumaticOff);
            this.groupBox2.Controls.Add(this.btn_Loading_Loader_B_XYMid);
            this.groupBox2.Controls.Add(this.btn_Loading_Loader_B_DestructionOff);
            this.groupBox2.Controls.Add(this.btn_Loading_Loader_B_XMove);
            this.groupBox2.Controls.Add(this.btn_Loading_Loader_B_Col2_4);
            this.groupBox2.Controls.Add(this.btn_Loading_Loader_B_Col1_3);
            this.groupBox2.Controls.Add(this.btn_Loading_Loader_B_CylinderUp);
            this.groupBox2.Controls.Add(this.btn_Loading_Loader_B_DestructionOn);
            this.groupBox2.Controls.Add(this.btn_Loading_Loader_B_PneumaticOn);
            this.groupBox2.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold);
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(207, 57);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(188, 370);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = " B ";
            // 
            // tbox_Loading_Loader_B_Gear
            // 
            this.tbox_Loading_Loader_B_Gear.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.tbox_Loading_Loader_B_Gear.Location = new System.Drawing.Point(97, 182);
            this.tbox_Loading_Loader_B_Gear.Name = "tbox_Loading_Loader_B_Gear";
            this.tbox_Loading_Loader_B_Gear.Size = new System.Drawing.Size(85, 25);
            this.tbox_Loading_Loader_B_Gear.TabIndex = 36;
            // 
            // btn_Loading_Loader_B_CylinderDown
            // 
            this.btn_Loading_Loader_B_CylinderDown.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_Loader_B_CylinderDown.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Loading_Loader_B_CylinderDown.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_Loader_B_CylinderDown.Location = new System.Drawing.Point(97, 26);
            this.btn_Loading_Loader_B_CylinderDown.Name = "btn_Loading_Loader_B_CylinderDown";
            this.btn_Loading_Loader_B_CylinderDown.Size = new System.Drawing.Size(85, 32);
            this.btn_Loading_Loader_B_CylinderDown.TabIndex = 34;
            this.btn_Loading_Loader_B_CylinderDown.Text = "실린더 다운";
            this.btn_Loading_Loader_B_CylinderDown.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(10, 184);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 19);
            this.label1.TabIndex = 35;
            this.label1.Text = "단수 :";
            // 
            // btn_Loading_Loader_B_UldY
            // 
            this.btn_Loading_Loader_B_UldY.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_Loader_B_UldY.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Loading_Loader_B_UldY.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_Loader_B_UldY.Location = new System.Drawing.Point(6, 330);
            this.btn_Loading_Loader_B_UldY.Name = "btn_Loading_Loader_B_UldY";
            this.btn_Loading_Loader_B_UldY.Size = new System.Drawing.Size(176, 32);
            this.btn_Loading_Loader_B_UldY.TabIndex = 23;
            this.btn_Loading_Loader_B_UldY.Text = "Uld Y";
            this.btn_Loading_Loader_B_UldY.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_Loader_B_XYCst
            // 
            this.btn_Loading_Loader_B_XYCst.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_Loader_B_XYCst.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Loading_Loader_B_XYCst.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_Loader_B_XYCst.Location = new System.Drawing.Point(6, 292);
            this.btn_Loading_Loader_B_XYCst.Name = "btn_Loading_Loader_B_XYCst";
            this.btn_Loading_Loader_B_XYCst.Size = new System.Drawing.Size(176, 32);
            this.btn_Loading_Loader_B_XYCst.TabIndex = 24;
            this.btn_Loading_Loader_B_XYCst.Text = "X, Y Cst";
            this.btn_Loading_Loader_B_XYCst.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_Loader_B_PneumaticOff
            // 
            this.btn_Loading_Loader_B_PneumaticOff.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_Loader_B_PneumaticOff.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Loading_Loader_B_PneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_Loader_B_PneumaticOff.Location = new System.Drawing.Point(97, 64);
            this.btn_Loading_Loader_B_PneumaticOff.Name = "btn_Loading_Loader_B_PneumaticOff";
            this.btn_Loading_Loader_B_PneumaticOff.Size = new System.Drawing.Size(85, 32);
            this.btn_Loading_Loader_B_PneumaticOff.TabIndex = 33;
            this.btn_Loading_Loader_B_PneumaticOff.Text = "공압 오프";
            this.btn_Loading_Loader_B_PneumaticOff.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_Loader_B_XYMid
            // 
            this.btn_Loading_Loader_B_XYMid.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_Loader_B_XYMid.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Loading_Loader_B_XYMid.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_Loader_B_XYMid.Location = new System.Drawing.Point(6, 254);
            this.btn_Loading_Loader_B_XYMid.Name = "btn_Loading_Loader_B_XYMid";
            this.btn_Loading_Loader_B_XYMid.Size = new System.Drawing.Size(176, 32);
            this.btn_Loading_Loader_B_XYMid.TabIndex = 25;
            this.btn_Loading_Loader_B_XYMid.Text = "X, Y Mid";
            this.btn_Loading_Loader_B_XYMid.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_Loader_B_DestructionOff
            // 
            this.btn_Loading_Loader_B_DestructionOff.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_Loader_B_DestructionOff.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Loading_Loader_B_DestructionOff.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_Loader_B_DestructionOff.Location = new System.Drawing.Point(97, 102);
            this.btn_Loading_Loader_B_DestructionOff.Name = "btn_Loading_Loader_B_DestructionOff";
            this.btn_Loading_Loader_B_DestructionOff.Size = new System.Drawing.Size(85, 32);
            this.btn_Loading_Loader_B_DestructionOff.TabIndex = 32;
            this.btn_Loading_Loader_B_DestructionOff.Text = "파기 오프";
            this.btn_Loading_Loader_B_DestructionOff.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_Loader_B_XMove
            // 
            this.btn_Loading_Loader_B_XMove.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_Loader_B_XMove.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Loading_Loader_B_XMove.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_Loader_B_XMove.Location = new System.Drawing.Point(6, 216);
            this.btn_Loading_Loader_B_XMove.Name = "btn_Loading_Loader_B_XMove";
            this.btn_Loading_Loader_B_XMove.Size = new System.Drawing.Size(176, 32);
            this.btn_Loading_Loader_B_XMove.TabIndex = 26;
            this.btn_Loading_Loader_B_XMove.Text = "X Move";
            this.btn_Loading_Loader_B_XMove.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_Loader_B_Col2_4
            // 
            this.btn_Loading_Loader_B_Col2_4.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_Loader_B_Col2_4.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Loading_Loader_B_Col2_4.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_Loader_B_Col2_4.Location = new System.Drawing.Point(97, 140);
            this.btn_Loading_Loader_B_Col2_4.Name = "btn_Loading_Loader_B_Col2_4";
            this.btn_Loading_Loader_B_Col2_4.Size = new System.Drawing.Size(85, 32);
            this.btn_Loading_Loader_B_Col2_4.TabIndex = 31;
            this.btn_Loading_Loader_B_Col2_4.Text = "2, 4열";
            this.btn_Loading_Loader_B_Col2_4.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_Loader_B_Col1_3
            // 
            this.btn_Loading_Loader_B_Col1_3.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_Loader_B_Col1_3.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Loading_Loader_B_Col1_3.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_Loader_B_Col1_3.Location = new System.Drawing.Point(6, 140);
            this.btn_Loading_Loader_B_Col1_3.Name = "btn_Loading_Loader_B_Col1_3";
            this.btn_Loading_Loader_B_Col1_3.Size = new System.Drawing.Size(85, 32);
            this.btn_Loading_Loader_B_Col1_3.TabIndex = 27;
            this.btn_Loading_Loader_B_Col1_3.Text = "1, 3열";
            this.btn_Loading_Loader_B_Col1_3.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_Loader_B_CylinderUp
            // 
            this.btn_Loading_Loader_B_CylinderUp.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_Loader_B_CylinderUp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Loading_Loader_B_CylinderUp.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_Loader_B_CylinderUp.Location = new System.Drawing.Point(6, 26);
            this.btn_Loading_Loader_B_CylinderUp.Name = "btn_Loading_Loader_B_CylinderUp";
            this.btn_Loading_Loader_B_CylinderUp.Size = new System.Drawing.Size(85, 32);
            this.btn_Loading_Loader_B_CylinderUp.TabIndex = 30;
            this.btn_Loading_Loader_B_CylinderUp.Text = "실린더 업";
            this.btn_Loading_Loader_B_CylinderUp.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_Loader_B_DestructionOn
            // 
            this.btn_Loading_Loader_B_DestructionOn.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_Loader_B_DestructionOn.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Loading_Loader_B_DestructionOn.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_Loader_B_DestructionOn.Location = new System.Drawing.Point(6, 102);
            this.btn_Loading_Loader_B_DestructionOn.Name = "btn_Loading_Loader_B_DestructionOn";
            this.btn_Loading_Loader_B_DestructionOn.Size = new System.Drawing.Size(85, 32);
            this.btn_Loading_Loader_B_DestructionOn.TabIndex = 28;
            this.btn_Loading_Loader_B_DestructionOn.Text = "파기 온";
            this.btn_Loading_Loader_B_DestructionOn.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_Loader_B_PneumaticOn
            // 
            this.btn_Loading_Loader_B_PneumaticOn.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_Loader_B_PneumaticOn.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Loading_Loader_B_PneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_Loader_B_PneumaticOn.Location = new System.Drawing.Point(6, 64);
            this.btn_Loading_Loader_B_PneumaticOn.Name = "btn_Loading_Loader_B_PneumaticOn";
            this.btn_Loading_Loader_B_PneumaticOn.Size = new System.Drawing.Size(85, 32);
            this.btn_Loading_Loader_B_PneumaticOn.TabIndex = 29;
            this.btn_Loading_Loader_B_PneumaticOn.Text = "공압 온";
            this.btn_Loading_Loader_B_PneumaticOn.UseVisualStyleBackColor = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbox_Loading_Loader_A_Gear);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.btn_Loading_Loader_A_CylinderDown);
            this.groupBox1.Controls.Add(this.btn_Loading_Loader_A_PneumaticOff);
            this.groupBox1.Controls.Add(this.btn_Loading_Loader_A_DestructionOff);
            this.groupBox1.Controls.Add(this.btn_Loading_Loader_A_Col2_4);
            this.groupBox1.Controls.Add(this.btn_Loading_Loader_A_CylinderUp);
            this.groupBox1.Controls.Add(this.btn_Loading_Loader_A_PneumaticOn);
            this.groupBox1.Controls.Add(this.btn_Loading_Loader_A_DestructionOn);
            this.groupBox1.Controls.Add(this.btn_Loading_Loader_A_Col1_3);
            this.groupBox1.Controls.Add(this.btn_Loading_Loader_A_XMove);
            this.groupBox1.Controls.Add(this.btn_Loading_Loader_A_XYMid);
            this.groupBox1.Controls.Add(this.btn_Loading_Loader_A_XYCst);
            this.groupBox1.Controls.Add(this.btn_Loading_Loader_A_UldY);
            this.groupBox1.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(13, 57);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(188, 370);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = " A ";
            // 
            // tbox_Loading_Loader_A_Gear
            // 
            this.tbox_Loading_Loader_A_Gear.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.tbox_Loading_Loader_A_Gear.Location = new System.Drawing.Point(97, 182);
            this.tbox_Loading_Loader_A_Gear.Name = "tbox_Loading_Loader_A_Gear";
            this.tbox_Loading_Loader_A_Gear.Size = new System.Drawing.Size(85, 25);
            this.tbox_Loading_Loader_A_Gear.TabIndex = 22;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(10, 184);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 19);
            this.label9.TabIndex = 21;
            this.label9.Text = "단수 :";
            // 
            // btn_Loading_Loader_A_CylinderDown
            // 
            this.btn_Loading_Loader_A_CylinderDown.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_Loader_A_CylinderDown.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Loading_Loader_A_CylinderDown.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_Loader_A_CylinderDown.Location = new System.Drawing.Point(97, 26);
            this.btn_Loading_Loader_A_CylinderDown.Name = "btn_Loading_Loader_A_CylinderDown";
            this.btn_Loading_Loader_A_CylinderDown.Size = new System.Drawing.Size(85, 32);
            this.btn_Loading_Loader_A_CylinderDown.TabIndex = 20;
            this.btn_Loading_Loader_A_CylinderDown.Text = "실린더 다운";
            this.btn_Loading_Loader_A_CylinderDown.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_Loader_A_PneumaticOff
            // 
            this.btn_Loading_Loader_A_PneumaticOff.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_Loader_A_PneumaticOff.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Loading_Loader_A_PneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_Loader_A_PneumaticOff.Location = new System.Drawing.Point(97, 64);
            this.btn_Loading_Loader_A_PneumaticOff.Name = "btn_Loading_Loader_A_PneumaticOff";
            this.btn_Loading_Loader_A_PneumaticOff.Size = new System.Drawing.Size(85, 32);
            this.btn_Loading_Loader_A_PneumaticOff.TabIndex = 19;
            this.btn_Loading_Loader_A_PneumaticOff.Text = "공압 오프";
            this.btn_Loading_Loader_A_PneumaticOff.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_Loader_A_DestructionOff
            // 
            this.btn_Loading_Loader_A_DestructionOff.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_Loader_A_DestructionOff.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Loading_Loader_A_DestructionOff.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_Loader_A_DestructionOff.Location = new System.Drawing.Point(97, 102);
            this.btn_Loading_Loader_A_DestructionOff.Name = "btn_Loading_Loader_A_DestructionOff";
            this.btn_Loading_Loader_A_DestructionOff.Size = new System.Drawing.Size(85, 32);
            this.btn_Loading_Loader_A_DestructionOff.TabIndex = 18;
            this.btn_Loading_Loader_A_DestructionOff.Text = "파기 오프";
            this.btn_Loading_Loader_A_DestructionOff.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_Loader_A_Col2_4
            // 
            this.btn_Loading_Loader_A_Col2_4.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_Loader_A_Col2_4.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Loading_Loader_A_Col2_4.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_Loader_A_Col2_4.Location = new System.Drawing.Point(97, 140);
            this.btn_Loading_Loader_A_Col2_4.Name = "btn_Loading_Loader_A_Col2_4";
            this.btn_Loading_Loader_A_Col2_4.Size = new System.Drawing.Size(85, 32);
            this.btn_Loading_Loader_A_Col2_4.TabIndex = 17;
            this.btn_Loading_Loader_A_Col2_4.Text = "2, 4열";
            this.btn_Loading_Loader_A_Col2_4.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_Loader_A_CylinderUp
            // 
            this.btn_Loading_Loader_A_CylinderUp.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_Loader_A_CylinderUp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Loading_Loader_A_CylinderUp.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_Loader_A_CylinderUp.Location = new System.Drawing.Point(6, 26);
            this.btn_Loading_Loader_A_CylinderUp.Name = "btn_Loading_Loader_A_CylinderUp";
            this.btn_Loading_Loader_A_CylinderUp.Size = new System.Drawing.Size(85, 32);
            this.btn_Loading_Loader_A_CylinderUp.TabIndex = 16;
            this.btn_Loading_Loader_A_CylinderUp.Text = "실린더 업";
            this.btn_Loading_Loader_A_CylinderUp.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_Loader_A_PneumaticOn
            // 
            this.btn_Loading_Loader_A_PneumaticOn.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_Loader_A_PneumaticOn.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Loading_Loader_A_PneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_Loader_A_PneumaticOn.Location = new System.Drawing.Point(6, 64);
            this.btn_Loading_Loader_A_PneumaticOn.Name = "btn_Loading_Loader_A_PneumaticOn";
            this.btn_Loading_Loader_A_PneumaticOn.Size = new System.Drawing.Size(85, 32);
            this.btn_Loading_Loader_A_PneumaticOn.TabIndex = 15;
            this.btn_Loading_Loader_A_PneumaticOn.Text = "공압 온";
            this.btn_Loading_Loader_A_PneumaticOn.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_Loader_A_DestructionOn
            // 
            this.btn_Loading_Loader_A_DestructionOn.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_Loader_A_DestructionOn.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Loading_Loader_A_DestructionOn.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_Loader_A_DestructionOn.Location = new System.Drawing.Point(6, 102);
            this.btn_Loading_Loader_A_DestructionOn.Name = "btn_Loading_Loader_A_DestructionOn";
            this.btn_Loading_Loader_A_DestructionOn.Size = new System.Drawing.Size(85, 32);
            this.btn_Loading_Loader_A_DestructionOn.TabIndex = 14;
            this.btn_Loading_Loader_A_DestructionOn.Text = "파기 온";
            this.btn_Loading_Loader_A_DestructionOn.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_Loader_A_Col1_3
            // 
            this.btn_Loading_Loader_A_Col1_3.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_Loader_A_Col1_3.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Loading_Loader_A_Col1_3.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_Loader_A_Col1_3.Location = new System.Drawing.Point(6, 140);
            this.btn_Loading_Loader_A_Col1_3.Name = "btn_Loading_Loader_A_Col1_3";
            this.btn_Loading_Loader_A_Col1_3.Size = new System.Drawing.Size(85, 32);
            this.btn_Loading_Loader_A_Col1_3.TabIndex = 13;
            this.btn_Loading_Loader_A_Col1_3.Text = "1, 3열";
            this.btn_Loading_Loader_A_Col1_3.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_Loader_A_XMove
            // 
            this.btn_Loading_Loader_A_XMove.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_Loader_A_XMove.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Loading_Loader_A_XMove.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_Loader_A_XMove.Location = new System.Drawing.Point(6, 216);
            this.btn_Loading_Loader_A_XMove.Name = "btn_Loading_Loader_A_XMove";
            this.btn_Loading_Loader_A_XMove.Size = new System.Drawing.Size(176, 32);
            this.btn_Loading_Loader_A_XMove.TabIndex = 11;
            this.btn_Loading_Loader_A_XMove.Text = "X Move";
            this.btn_Loading_Loader_A_XMove.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_Loader_A_XYMid
            // 
            this.btn_Loading_Loader_A_XYMid.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_Loader_A_XYMid.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Loading_Loader_A_XYMid.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_Loader_A_XYMid.Location = new System.Drawing.Point(6, 254);
            this.btn_Loading_Loader_A_XYMid.Name = "btn_Loading_Loader_A_XYMid";
            this.btn_Loading_Loader_A_XYMid.Size = new System.Drawing.Size(176, 32);
            this.btn_Loading_Loader_A_XYMid.TabIndex = 10;
            this.btn_Loading_Loader_A_XYMid.Text = "X, Y Mid";
            this.btn_Loading_Loader_A_XYMid.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_Loader_A_XYCst
            // 
            this.btn_Loading_Loader_A_XYCst.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_Loader_A_XYCst.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Loading_Loader_A_XYCst.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_Loader_A_XYCst.Location = new System.Drawing.Point(6, 292);
            this.btn_Loading_Loader_A_XYCst.Name = "btn_Loading_Loader_A_XYCst";
            this.btn_Loading_Loader_A_XYCst.Size = new System.Drawing.Size(176, 32);
            this.btn_Loading_Loader_A_XYCst.TabIndex = 9;
            this.btn_Loading_Loader_A_XYCst.Text = "X, Y Cst";
            this.btn_Loading_Loader_A_XYCst.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_Loader_A_UldY
            // 
            this.btn_Loading_Loader_A_UldY.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_Loader_A_UldY.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Loading_Loader_A_UldY.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_Loader_A_UldY.Location = new System.Drawing.Point(6, 330);
            this.btn_Loading_Loader_A_UldY.Name = "btn_Loading_Loader_A_UldY";
            this.btn_Loading_Loader_A_UldY.Size = new System.Drawing.Size(176, 32);
            this.btn_Loading_Loader_A_UldY.TabIndex = 8;
            this.btn_Loading_Loader_A_UldY.Text = "Uld Y";
            this.btn_Loading_Loader_A_UldY.UseVisualStyleBackColor = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.SlateGray;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.btn_Loading_A_FreeAilgn);
            this.panel3.Controls.Add(this.btn_Loading_A_CST2Stage);
            this.panel3.Controls.Add(this.btn_Loading_A_CSTLift2Buffer);
            this.panel3.Controls.Add(this.btn_Loading_A_LoadingTransfer2Stage);
            this.panel3.Controls.Add(this.btn_Loading_A_Loader2LoadingTransfer);
            this.panel3.Controls.Add(this.btn_Loading_A_CST2Loader);
            this.panel3.Controls.Add(this.btn_Loading_A_CST2Lift);
            this.panel3.Location = new System.Drawing.Point(18, 65);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(409, 330);
            this.panel3.TabIndex = 1;
            // 
            // btn_Loading_A_FreeAilgn
            // 
            this.btn_Loading_A_FreeAilgn.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_A_FreeAilgn.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Loading_A_FreeAilgn.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_A_FreeAilgn.Location = new System.Drawing.Point(241, 124);
            this.btn_Loading_A_FreeAilgn.Name = "btn_Loading_A_FreeAilgn";
            this.btn_Loading_A_FreeAilgn.Size = new System.Drawing.Size(126, 38);
            this.btn_Loading_A_FreeAilgn.TabIndex = 6;
            this.btn_Loading_A_FreeAilgn.Text = "프리 얼라인";
            this.btn_Loading_A_FreeAilgn.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_A_CST2Stage
            // 
            this.btn_Loading_A_CST2Stage.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_A_CST2Stage.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Loading_A_CST2Stage.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_A_CST2Stage.Location = new System.Drawing.Point(38, 277);
            this.btn_Loading_A_CST2Stage.Name = "btn_Loading_A_CST2Stage";
            this.btn_Loading_A_CST2Stage.Size = new System.Drawing.Size(329, 38);
            this.btn_Loading_A_CST2Stage.TabIndex = 5;
            this.btn_Loading_A_CST2Stage.Text = "카세트  → 스테이지";
            this.btn_Loading_A_CST2Stage.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_A_CSTLift2Buffer
            // 
            this.btn_Loading_A_CSTLift2Buffer.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_A_CSTLift2Buffer.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Loading_A_CSTLift2Buffer.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_A_CSTLift2Buffer.Location = new System.Drawing.Point(38, 226);
            this.btn_Loading_A_CSTLift2Buffer.Name = "btn_Loading_A_CSTLift2Buffer";
            this.btn_Loading_A_CSTLift2Buffer.Size = new System.Drawing.Size(329, 38);
            this.btn_Loading_A_CSTLift2Buffer.TabIndex = 4;
            this.btn_Loading_A_CSTLift2Buffer.Text = "카세트 리프트  → 버퍼";
            this.btn_Loading_A_CSTLift2Buffer.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_A_LoadingTransfer2Stage
            // 
            this.btn_Loading_A_LoadingTransfer2Stage.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_A_LoadingTransfer2Stage.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Loading_A_LoadingTransfer2Stage.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_A_LoadingTransfer2Stage.Location = new System.Drawing.Point(38, 175);
            this.btn_Loading_A_LoadingTransfer2Stage.Name = "btn_Loading_A_LoadingTransfer2Stage";
            this.btn_Loading_A_LoadingTransfer2Stage.Size = new System.Drawing.Size(329, 38);
            this.btn_Loading_A_LoadingTransfer2Stage.TabIndex = 3;
            this.btn_Loading_A_LoadingTransfer2Stage.Text = "로딩 트랜스퍼  → 스테이지";
            this.btn_Loading_A_LoadingTransfer2Stage.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_A_Loader2LoadingTransfer
            // 
            this.btn_Loading_A_Loader2LoadingTransfer.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_A_Loader2LoadingTransfer.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Loading_A_Loader2LoadingTransfer.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_A_Loader2LoadingTransfer.Location = new System.Drawing.Point(38, 124);
            this.btn_Loading_A_Loader2LoadingTransfer.Name = "btn_Loading_A_Loader2LoadingTransfer";
            this.btn_Loading_A_Loader2LoadingTransfer.Size = new System.Drawing.Size(182, 38);
            this.btn_Loading_A_Loader2LoadingTransfer.TabIndex = 2;
            this.btn_Loading_A_Loader2LoadingTransfer.Text = "로더 → 로딩 트랜스퍼";
            this.btn_Loading_A_Loader2LoadingTransfer.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_A_CST2Loader
            // 
            this.btn_Loading_A_CST2Loader.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_A_CST2Loader.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Loading_A_CST2Loader.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_A_CST2Loader.Location = new System.Drawing.Point(38, 73);
            this.btn_Loading_A_CST2Loader.Name = "btn_Loading_A_CST2Loader";
            this.btn_Loading_A_CST2Loader.Size = new System.Drawing.Size(329, 38);
            this.btn_Loading_A_CST2Loader.TabIndex = 1;
            this.btn_Loading_A_CST2Loader.Text = "카세트  → 로더";
            this.btn_Loading_A_CST2Loader.UseVisualStyleBackColor = false;
            // 
            // btn_Loading_A_CST2Lift
            // 
            this.btn_Loading_A_CST2Lift.BackColor = System.Drawing.Color.DimGray;
            this.btn_Loading_A_CST2Lift.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Loading_A_CST2Lift.ForeColor = System.Drawing.Color.White;
            this.btn_Loading_A_CST2Lift.Location = new System.Drawing.Point(38, 22);
            this.btn_Loading_A_CST2Lift.Name = "btn_Loading_A_CST2Lift";
            this.btn_Loading_A_CST2Lift.Size = new System.Drawing.Size(329, 38);
            this.btn_Loading_A_CST2Lift.TabIndex = 0;
            this.btn_Loading_A_CST2Lift.Text = "카세트 → 리프트";
            this.btn_Loading_A_CST2Lift.UseVisualStyleBackColor = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel11);
            this.panel2.Controls.Add(this.panel7);
            this.panel2.Controls.Add(this.panel8);
            this.panel2.Controls.Add(this.panel9);
            this.panel2.Controls.Add(this.panel10);
            this.panel2.Location = new System.Drawing.Point(873, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(864, 869);
            this.panel2.TabIndex = 3;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Purple;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.label12);
            this.panel7.Controls.Add(this.btn_Unloading_UnloaderTransfer_CylinderReset);
            this.panel7.Controls.Add(this.groupBox7);
            this.panel7.Controls.Add(this.groupBox8);
            this.panel7.Location = new System.Drawing.Point(438, 417);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(409, 438);
            this.panel7.TabIndex = 8;
            // 
            // btn_Unloading_UnloaderTransfer_CylinderReset
            // 
            this.btn_Unloading_UnloaderTransfer_CylinderReset.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_UnloaderTransfer_CylinderReset.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Unloading_UnloaderTransfer_CylinderReset.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_UnloaderTransfer_CylinderReset.Location = new System.Drawing.Point(12, 21);
            this.btn_Unloading_UnloaderTransfer_CylinderReset.Name = "btn_Unloading_UnloaderTransfer_CylinderReset";
            this.btn_Unloading_UnloaderTransfer_CylinderReset.Size = new System.Drawing.Size(188, 38);
            this.btn_Unloading_UnloaderTransfer_CylinderReset.TabIndex = 11;
            this.btn_Unloading_UnloaderTransfer_CylinderReset.Text = "실린더 초기화";
            this.btn_Unloading_UnloaderTransfer_CylinderReset.UseVisualStyleBackColor = false;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.btn_Unloading_UnloaderTransfer_B_UnloadingTransfer2Inspaction);
            this.groupBox7.Controls.Add(this.btn_Unloading_UnloaderTransfer_B_UnloadingTransfer2MCR);
            this.groupBox7.Controls.Add(this.btn_Unloading_UnloaderTransfer_B_DestructionOff);
            this.groupBox7.Controls.Add(this.btn_Unloading_UnloaderTransfer_B_DestructionOn);
            this.groupBox7.Controls.Add(this.btn_Unloading_UnloaderTransfer_B_PneumaticOff);
            this.groupBox7.Controls.Add(this.btn_Unloading_UnloaderTransfer_B_PneumaticOn);
            this.groupBox7.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold);
            this.groupBox7.ForeColor = System.Drawing.Color.White;
            this.groupBox7.Location = new System.Drawing.Point(206, 57);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(188, 284);
            this.groupBox7.TabIndex = 7;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = " B ";
            // 
            // btn_Unloading_UnloaderTransfer_B_UnloadingTransfer2Inspaction
            // 
            this.btn_Unloading_UnloaderTransfer_B_UnloadingTransfer2Inspaction.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_UnloaderTransfer_B_UnloadingTransfer2Inspaction.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_UnloaderTransfer_B_UnloadingTransfer2Inspaction.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_UnloaderTransfer_B_UnloadingTransfer2Inspaction.Location = new System.Drawing.Point(7, 174);
            this.btn_Unloading_UnloaderTransfer_B_UnloadingTransfer2Inspaction.Name = "btn_Unloading_UnloaderTransfer_B_UnloadingTransfer2Inspaction";
            this.btn_Unloading_UnloaderTransfer_B_UnloadingTransfer2Inspaction.Size = new System.Drawing.Size(173, 48);
            this.btn_Unloading_UnloaderTransfer_B_UnloadingTransfer2Inspaction.TabIndex = 44;
            this.btn_Unloading_UnloaderTransfer_B_UnloadingTransfer2Inspaction.Text = "   언로딩 트랜스퍼     → 인스펙션";
            this.btn_Unloading_UnloaderTransfer_B_UnloadingTransfer2Inspaction.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_UnloaderTransfer_B_UnloadingTransfer2MCR
            // 
            this.btn_Unloading_UnloaderTransfer_B_UnloadingTransfer2MCR.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_UnloaderTransfer_B_UnloadingTransfer2MCR.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_UnloaderTransfer_B_UnloadingTransfer2MCR.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_UnloaderTransfer_B_UnloadingTransfer2MCR.Location = new System.Drawing.Point(7, 228);
            this.btn_Unloading_UnloaderTransfer_B_UnloadingTransfer2MCR.Name = "btn_Unloading_UnloaderTransfer_B_UnloadingTransfer2MCR";
            this.btn_Unloading_UnloaderTransfer_B_UnloadingTransfer2MCR.Size = new System.Drawing.Size(173, 48);
            this.btn_Unloading_UnloaderTransfer_B_UnloadingTransfer2MCR.TabIndex = 43;
            this.btn_Unloading_UnloaderTransfer_B_UnloadingTransfer2MCR.Text = "   언로딩 트랜스퍼     → MCR";
            this.btn_Unloading_UnloaderTransfer_B_UnloadingTransfer2MCR.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_UnloaderTransfer_B_DestructionOff
            // 
            this.btn_Unloading_UnloaderTransfer_B_DestructionOff.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_UnloaderTransfer_B_DestructionOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_UnloaderTransfer_B_DestructionOff.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_UnloaderTransfer_B_DestructionOff.Location = new System.Drawing.Point(94, 99);
            this.btn_Unloading_UnloaderTransfer_B_DestructionOff.Name = "btn_Unloading_UnloaderTransfer_B_DestructionOff";
            this.btn_Unloading_UnloaderTransfer_B_DestructionOff.Size = new System.Drawing.Size(85, 67);
            this.btn_Unloading_UnloaderTransfer_B_DestructionOff.TabIndex = 42;
            this.btn_Unloading_UnloaderTransfer_B_DestructionOff.Text = "파기 오프";
            this.btn_Unloading_UnloaderTransfer_B_DestructionOff.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_UnloaderTransfer_B_DestructionOn
            // 
            this.btn_Unloading_UnloaderTransfer_B_DestructionOn.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_UnloaderTransfer_B_DestructionOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_UnloaderTransfer_B_DestructionOn.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_UnloaderTransfer_B_DestructionOn.Location = new System.Drawing.Point(5, 99);
            this.btn_Unloading_UnloaderTransfer_B_DestructionOn.Name = "btn_Unloading_UnloaderTransfer_B_DestructionOn";
            this.btn_Unloading_UnloaderTransfer_B_DestructionOn.Size = new System.Drawing.Size(85, 67);
            this.btn_Unloading_UnloaderTransfer_B_DestructionOn.TabIndex = 41;
            this.btn_Unloading_UnloaderTransfer_B_DestructionOn.Text = "파기 온";
            this.btn_Unloading_UnloaderTransfer_B_DestructionOn.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_UnloaderTransfer_B_PneumaticOff
            // 
            this.btn_Unloading_UnloaderTransfer_B_PneumaticOff.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_UnloaderTransfer_B_PneumaticOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_UnloaderTransfer_B_PneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_UnloaderTransfer_B_PneumaticOff.Location = new System.Drawing.Point(95, 26);
            this.btn_Unloading_UnloaderTransfer_B_PneumaticOff.Name = "btn_Unloading_UnloaderTransfer_B_PneumaticOff";
            this.btn_Unloading_UnloaderTransfer_B_PneumaticOff.Size = new System.Drawing.Size(85, 67);
            this.btn_Unloading_UnloaderTransfer_B_PneumaticOff.TabIndex = 40;
            this.btn_Unloading_UnloaderTransfer_B_PneumaticOff.Text = "공압 오프";
            this.btn_Unloading_UnloaderTransfer_B_PneumaticOff.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_UnloaderTransfer_B_PneumaticOn
            // 
            this.btn_Unloading_UnloaderTransfer_B_PneumaticOn.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_UnloaderTransfer_B_PneumaticOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_UnloaderTransfer_B_PneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_UnloaderTransfer_B_PneumaticOn.Location = new System.Drawing.Point(6, 26);
            this.btn_Unloading_UnloaderTransfer_B_PneumaticOn.Name = "btn_Unloading_UnloaderTransfer_B_PneumaticOn";
            this.btn_Unloading_UnloaderTransfer_B_PneumaticOn.Size = new System.Drawing.Size(85, 67);
            this.btn_Unloading_UnloaderTransfer_B_PneumaticOn.TabIndex = 39;
            this.btn_Unloading_UnloaderTransfer_B_PneumaticOn.Text = "공압 온";
            this.btn_Unloading_UnloaderTransfer_B_PneumaticOn.UseVisualStyleBackColor = false;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.btn_Unloading_UnloaderTransfer_A_UnloadingTransfer2Inspaction);
            this.groupBox8.Controls.Add(this.btn_Unloading_UnloaderTransfer_A_UnloadingTransfer2MCR);
            this.groupBox8.Controls.Add(this.btn_Unloading_UnloaderTransfer_A_DestructionOff);
            this.groupBox8.Controls.Add(this.btn_Unloading_UnloaderTransfer_A_DestructionOn);
            this.groupBox8.Controls.Add(this.btn_Unloading_UnloaderTransfer_A_PneumaticOff);
            this.groupBox8.Controls.Add(this.btn_Unloading_UnloaderTransfer_A_PneumaticOn);
            this.groupBox8.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox8.ForeColor = System.Drawing.Color.White;
            this.groupBox8.Location = new System.Drawing.Point(12, 57);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(188, 284);
            this.groupBox8.TabIndex = 6;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = " A ";
            // 
            // btn_Unloading_UnloaderTransfer_A_UnloadingTransfer2Inspaction
            // 
            this.btn_Unloading_UnloaderTransfer_A_UnloadingTransfer2Inspaction.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_UnloaderTransfer_A_UnloadingTransfer2Inspaction.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_UnloaderTransfer_A_UnloadingTransfer2Inspaction.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_UnloaderTransfer_A_UnloadingTransfer2Inspaction.Location = new System.Drawing.Point(8, 174);
            this.btn_Unloading_UnloaderTransfer_A_UnloadingTransfer2Inspaction.Name = "btn_Unloading_UnloaderTransfer_A_UnloadingTransfer2Inspaction";
            this.btn_Unloading_UnloaderTransfer_A_UnloadingTransfer2Inspaction.Size = new System.Drawing.Size(173, 48);
            this.btn_Unloading_UnloaderTransfer_A_UnloadingTransfer2Inspaction.TabIndex = 40;
            this.btn_Unloading_UnloaderTransfer_A_UnloadingTransfer2Inspaction.Text = "   언로딩 트랜스퍼     → 인스펙션";
            this.btn_Unloading_UnloaderTransfer_A_UnloadingTransfer2Inspaction.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_UnloaderTransfer_A_UnloadingTransfer2MCR
            // 
            this.btn_Unloading_UnloaderTransfer_A_UnloadingTransfer2MCR.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_UnloaderTransfer_A_UnloadingTransfer2MCR.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_UnloaderTransfer_A_UnloadingTransfer2MCR.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_UnloaderTransfer_A_UnloadingTransfer2MCR.Location = new System.Drawing.Point(8, 228);
            this.btn_Unloading_UnloaderTransfer_A_UnloadingTransfer2MCR.Name = "btn_Unloading_UnloaderTransfer_A_UnloadingTransfer2MCR";
            this.btn_Unloading_UnloaderTransfer_A_UnloadingTransfer2MCR.Size = new System.Drawing.Size(173, 48);
            this.btn_Unloading_UnloaderTransfer_A_UnloadingTransfer2MCR.TabIndex = 39;
            this.btn_Unloading_UnloaderTransfer_A_UnloadingTransfer2MCR.Text = "   언로딩 트랜스퍼     → MCR";
            this.btn_Unloading_UnloaderTransfer_A_UnloadingTransfer2MCR.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_UnloaderTransfer_A_DestructionOff
            // 
            this.btn_Unloading_UnloaderTransfer_A_DestructionOff.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_UnloaderTransfer_A_DestructionOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_UnloaderTransfer_A_DestructionOff.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_UnloaderTransfer_A_DestructionOff.Location = new System.Drawing.Point(96, 99);
            this.btn_Unloading_UnloaderTransfer_A_DestructionOff.Name = "btn_Unloading_UnloaderTransfer_A_DestructionOff";
            this.btn_Unloading_UnloaderTransfer_A_DestructionOff.Size = new System.Drawing.Size(85, 67);
            this.btn_Unloading_UnloaderTransfer_A_DestructionOff.TabIndex = 38;
            this.btn_Unloading_UnloaderTransfer_A_DestructionOff.Text = "파기 오프";
            this.btn_Unloading_UnloaderTransfer_A_DestructionOff.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_UnloaderTransfer_A_DestructionOn
            // 
            this.btn_Unloading_UnloaderTransfer_A_DestructionOn.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_UnloaderTransfer_A_DestructionOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_UnloaderTransfer_A_DestructionOn.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_UnloaderTransfer_A_DestructionOn.Location = new System.Drawing.Point(7, 99);
            this.btn_Unloading_UnloaderTransfer_A_DestructionOn.Name = "btn_Unloading_UnloaderTransfer_A_DestructionOn";
            this.btn_Unloading_UnloaderTransfer_A_DestructionOn.Size = new System.Drawing.Size(85, 67);
            this.btn_Unloading_UnloaderTransfer_A_DestructionOn.TabIndex = 37;
            this.btn_Unloading_UnloaderTransfer_A_DestructionOn.Text = "파기 온";
            this.btn_Unloading_UnloaderTransfer_A_DestructionOn.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_UnloaderTransfer_A_PneumaticOff
            // 
            this.btn_Unloading_UnloaderTransfer_A_PneumaticOff.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_UnloaderTransfer_A_PneumaticOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_UnloaderTransfer_A_PneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_UnloaderTransfer_A_PneumaticOff.Location = new System.Drawing.Point(97, 26);
            this.btn_Unloading_UnloaderTransfer_A_PneumaticOff.Name = "btn_Unloading_UnloaderTransfer_A_PneumaticOff";
            this.btn_Unloading_UnloaderTransfer_A_PneumaticOff.Size = new System.Drawing.Size(85, 67);
            this.btn_Unloading_UnloaderTransfer_A_PneumaticOff.TabIndex = 36;
            this.btn_Unloading_UnloaderTransfer_A_PneumaticOff.Text = "공압 오프";
            this.btn_Unloading_UnloaderTransfer_A_PneumaticOff.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_UnloaderTransfer_A_PneumaticOn
            // 
            this.btn_Unloading_UnloaderTransfer_A_PneumaticOn.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_UnloaderTransfer_A_PneumaticOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_UnloaderTransfer_A_PneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_UnloaderTransfer_A_PneumaticOn.Location = new System.Drawing.Point(8, 26);
            this.btn_Unloading_UnloaderTransfer_A_PneumaticOn.Name = "btn_Unloading_UnloaderTransfer_A_PneumaticOn";
            this.btn_Unloading_UnloaderTransfer_A_PneumaticOn.Size = new System.Drawing.Size(85, 67);
            this.btn_Unloading_UnloaderTransfer_A_PneumaticOn.TabIndex = 35;
            this.btn_Unloading_UnloaderTransfer_A_PneumaticOn.Text = "공압 온";
            this.btn_Unloading_UnloaderTransfer_A_PneumaticOn.UseVisualStyleBackColor = false;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Purple;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.label10);
            this.panel8.Controls.Add(this.btn_Unloading_B_CIM);
            this.panel8.Controls.Add(this.btn_Unloading_B_CST2Lift);
            this.panel8.Controls.Add(this.btn_Unloading_B_CSTLift2Buffer);
            this.panel8.Controls.Add(this.btn_Unloading_B_Unloader2CST);
            this.panel8.Controls.Add(this.btn_Unloading_B_UnloadingTransfer2Unloader);
            this.panel8.Controls.Add(this.btn_Unloading_B_Stage2UnloadingTransfer);
            this.panel8.Location = new System.Drawing.Point(438, 65);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(409, 330);
            this.panel8.TabIndex = 6;
            // 
            // btn_Unloading_B_CIM
            // 
            this.btn_Unloading_B_CIM.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_B_CIM.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_B_CIM.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_B_CIM.Location = new System.Drawing.Point(292, 274);
            this.btn_Unloading_B_CIM.Name = "btn_Unloading_B_CIM";
            this.btn_Unloading_B_CIM.Size = new System.Drawing.Size(77, 38);
            this.btn_Unloading_B_CIM.TabIndex = 25;
            this.btn_Unloading_B_CIM.Text = "CIM";
            this.btn_Unloading_B_CIM.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_B_CST2Lift
            // 
            this.btn_Unloading_B_CST2Lift.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_B_CST2Lift.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_B_CST2Lift.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_B_CST2Lift.Location = new System.Drawing.Point(40, 22);
            this.btn_Unloading_B_CST2Lift.Name = "btn_Unloading_B_CST2Lift";
            this.btn_Unloading_B_CST2Lift.Size = new System.Drawing.Size(329, 38);
            this.btn_Unloading_B_CST2Lift.TabIndex = 20;
            this.btn_Unloading_B_CST2Lift.Text = "카세트 → 리프트";
            this.btn_Unloading_B_CST2Lift.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_B_CSTLift2Buffer
            // 
            this.btn_Unloading_B_CSTLift2Buffer.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_B_CSTLift2Buffer.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_B_CSTLift2Buffer.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_B_CSTLift2Buffer.Location = new System.Drawing.Point(40, 274);
            this.btn_Unloading_B_CSTLift2Buffer.Name = "btn_Unloading_B_CSTLift2Buffer";
            this.btn_Unloading_B_CSTLift2Buffer.Size = new System.Drawing.Size(231, 38);
            this.btn_Unloading_B_CSTLift2Buffer.TabIndex = 24;
            this.btn_Unloading_B_CSTLift2Buffer.Text = "카세트 리프트  → 버퍼";
            this.btn_Unloading_B_CSTLift2Buffer.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_B_Unloader2CST
            // 
            this.btn_Unloading_B_Unloader2CST.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_B_Unloader2CST.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_B_Unloader2CST.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_B_Unloader2CST.Location = new System.Drawing.Point(40, 211);
            this.btn_Unloading_B_Unloader2CST.Name = "btn_Unloading_B_Unloader2CST";
            this.btn_Unloading_B_Unloader2CST.Size = new System.Drawing.Size(329, 38);
            this.btn_Unloading_B_Unloader2CST.TabIndex = 23;
            this.btn_Unloading_B_Unloader2CST.Text = "언로더  → 카세트";
            this.btn_Unloading_B_Unloader2CST.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_B_UnloadingTransfer2Unloader
            // 
            this.btn_Unloading_B_UnloadingTransfer2Unloader.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_B_UnloadingTransfer2Unloader.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_B_UnloadingTransfer2Unloader.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_B_UnloadingTransfer2Unloader.Location = new System.Drawing.Point(40, 148);
            this.btn_Unloading_B_UnloadingTransfer2Unloader.Name = "btn_Unloading_B_UnloadingTransfer2Unloader";
            this.btn_Unloading_B_UnloadingTransfer2Unloader.Size = new System.Drawing.Size(329, 38);
            this.btn_Unloading_B_UnloadingTransfer2Unloader.TabIndex = 22;
            this.btn_Unloading_B_UnloadingTransfer2Unloader.Text = "언로딩 트랜스퍼  → 언로더";
            this.btn_Unloading_B_UnloadingTransfer2Unloader.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_B_Stage2UnloadingTransfer
            // 
            this.btn_Unloading_B_Stage2UnloadingTransfer.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_B_Stage2UnloadingTransfer.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_B_Stage2UnloadingTransfer.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_B_Stage2UnloadingTransfer.Location = new System.Drawing.Point(40, 85);
            this.btn_Unloading_B_Stage2UnloadingTransfer.Name = "btn_Unloading_B_Stage2UnloadingTransfer";
            this.btn_Unloading_B_Stage2UnloadingTransfer.Size = new System.Drawing.Size(329, 38);
            this.btn_Unloading_B_Stage2UnloadingTransfer.TabIndex = 21;
            this.btn_Unloading_B_Stage2UnloadingTransfer.Text = "스테이지  →  언로딩 트랜스퍼";
            this.btn_Unloading_B_Stage2UnloadingTransfer.UseVisualStyleBackColor = false;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.SlateGray;
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.label11);
            this.panel9.Controls.Add(this.btn_Unloading_Unloader_CylinderReset);
            this.panel9.Controls.Add(this.groupBox5);
            this.panel9.Controls.Add(this.groupBox6);
            this.panel9.Location = new System.Drawing.Point(14, 417);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(409, 438);
            this.panel9.TabIndex = 7;
            // 
            // btn_Unloading_Unloader_CylinderReset
            // 
            this.btn_Unloading_Unloader_CylinderReset.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_Unloader_CylinderReset.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Unloading_Unloader_CylinderReset.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_Unloader_CylinderReset.Location = new System.Drawing.Point(15, 21);
            this.btn_Unloading_Unloader_CylinderReset.Name = "btn_Unloading_Unloader_CylinderReset";
            this.btn_Unloading_Unloader_CylinderReset.Size = new System.Drawing.Size(188, 38);
            this.btn_Unloading_Unloader_CylinderReset.TabIndex = 10;
            this.btn_Unloading_Unloader_CylinderReset.Text = "실린더 초기화";
            this.btn_Unloading_Unloader_CylinderReset.UseVisualStyleBackColor = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.tbox_Unloading_Unloader_B_Gear);
            this.groupBox5.Controls.Add(this.btn_Unloading_Unloader_B_PneumaticOff);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Controls.Add(this.btn_Unloading_Unloader_B_UldY);
            this.groupBox5.Controls.Add(this.btn_Unloading_Unloader_B_CylinderDown);
            this.groupBox5.Controls.Add(this.btn_Unloading_Unloader_B_XYCst);
            this.groupBox5.Controls.Add(this.btn_Unloading_Unloader_B_XYMid);
            this.groupBox5.Controls.Add(this.btn_Unloading_Unloader_B_DestructionOff);
            this.groupBox5.Controls.Add(this.btn_Unloading_Unloader_B_XMove);
            this.groupBox5.Controls.Add(this.btn_Unloading_Unloader_B_Col2_4);
            this.groupBox5.Controls.Add(this.btn_Unloading_Unloader_B_Col1_3);
            this.groupBox5.Controls.Add(this.btn_Unloading_Unloader_B_CylinderUp);
            this.groupBox5.Controls.Add(this.btn_Unloading_Unloader_B_DestructionOn);
            this.groupBox5.Controls.Add(this.btn_Unloading_Unloader_B_PneumaticOn);
            this.groupBox5.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold);
            this.groupBox5.ForeColor = System.Drawing.Color.White;
            this.groupBox5.Location = new System.Drawing.Point(209, 57);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(188, 370);
            this.groupBox5.TabIndex = 5;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = " B ";
            // 
            // tbox_Unloading_Unloader_B_Gear
            // 
            this.tbox_Unloading_Unloader_B_Gear.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.tbox_Unloading_Unloader_B_Gear.Location = new System.Drawing.Point(97, 179);
            this.tbox_Unloading_Unloader_B_Gear.Name = "tbox_Unloading_Unloader_B_Gear";
            this.tbox_Unloading_Unloader_B_Gear.Size = new System.Drawing.Size(85, 25);
            this.tbox_Unloading_Unloader_B_Gear.TabIndex = 50;
            // 
            // btn_Unloading_Unloader_B_PneumaticOff
            // 
            this.btn_Unloading_Unloader_B_PneumaticOff.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_Unloader_B_PneumaticOff.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_Unloader_B_PneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_Unloader_B_PneumaticOff.Location = new System.Drawing.Point(97, 64);
            this.btn_Unloading_Unloader_B_PneumaticOff.Name = "btn_Unloading_Unloader_B_PneumaticOff";
            this.btn_Unloading_Unloader_B_PneumaticOff.Size = new System.Drawing.Size(85, 32);
            this.btn_Unloading_Unloader_B_PneumaticOff.TabIndex = 47;
            this.btn_Unloading_Unloader_B_PneumaticOff.Text = "공압 오프";
            this.btn_Unloading_Unloader_B_PneumaticOff.UseVisualStyleBackColor = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(10, 182);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 19);
            this.label3.TabIndex = 49;
            this.label3.Text = "단수 :";
            // 
            // btn_Unloading_Unloader_B_UldY
            // 
            this.btn_Unloading_Unloader_B_UldY.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_Unloader_B_UldY.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Unloading_Unloader_B_UldY.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_Unloader_B_UldY.Location = new System.Drawing.Point(6, 330);
            this.btn_Unloading_Unloader_B_UldY.Name = "btn_Unloading_Unloader_B_UldY";
            this.btn_Unloading_Unloader_B_UldY.Size = new System.Drawing.Size(176, 32);
            this.btn_Unloading_Unloader_B_UldY.TabIndex = 37;
            this.btn_Unloading_Unloader_B_UldY.Text = "Uld Y";
            this.btn_Unloading_Unloader_B_UldY.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_Unloader_B_CylinderDown
            // 
            this.btn_Unloading_Unloader_B_CylinderDown.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_Unloader_B_CylinderDown.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_Unloader_B_CylinderDown.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_Unloader_B_CylinderDown.Location = new System.Drawing.Point(97, 26);
            this.btn_Unloading_Unloader_B_CylinderDown.Name = "btn_Unloading_Unloader_B_CylinderDown";
            this.btn_Unloading_Unloader_B_CylinderDown.Size = new System.Drawing.Size(85, 32);
            this.btn_Unloading_Unloader_B_CylinderDown.TabIndex = 48;
            this.btn_Unloading_Unloader_B_CylinderDown.Text = "실린더 다운";
            this.btn_Unloading_Unloader_B_CylinderDown.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_Unloader_B_XYCst
            // 
            this.btn_Unloading_Unloader_B_XYCst.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_Unloader_B_XYCst.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Unloading_Unloader_B_XYCst.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_Unloader_B_XYCst.Location = new System.Drawing.Point(6, 292);
            this.btn_Unloading_Unloader_B_XYCst.Name = "btn_Unloading_Unloader_B_XYCst";
            this.btn_Unloading_Unloader_B_XYCst.Size = new System.Drawing.Size(176, 32);
            this.btn_Unloading_Unloader_B_XYCst.TabIndex = 38;
            this.btn_Unloading_Unloader_B_XYCst.Text = "X, Y Cst";
            this.btn_Unloading_Unloader_B_XYCst.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_Unloader_B_XYMid
            // 
            this.btn_Unloading_Unloader_B_XYMid.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_Unloader_B_XYMid.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Unloading_Unloader_B_XYMid.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_Unloader_B_XYMid.Location = new System.Drawing.Point(6, 254);
            this.btn_Unloading_Unloader_B_XYMid.Name = "btn_Unloading_Unloader_B_XYMid";
            this.btn_Unloading_Unloader_B_XYMid.Size = new System.Drawing.Size(176, 32);
            this.btn_Unloading_Unloader_B_XYMid.TabIndex = 39;
            this.btn_Unloading_Unloader_B_XYMid.Text = "X, Y Mid";
            this.btn_Unloading_Unloader_B_XYMid.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_Unloader_B_DestructionOff
            // 
            this.btn_Unloading_Unloader_B_DestructionOff.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_Unloader_B_DestructionOff.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_Unloader_B_DestructionOff.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_Unloader_B_DestructionOff.Location = new System.Drawing.Point(97, 102);
            this.btn_Unloading_Unloader_B_DestructionOff.Name = "btn_Unloading_Unloader_B_DestructionOff";
            this.btn_Unloading_Unloader_B_DestructionOff.Size = new System.Drawing.Size(85, 32);
            this.btn_Unloading_Unloader_B_DestructionOff.TabIndex = 46;
            this.btn_Unloading_Unloader_B_DestructionOff.Text = "파기 오프";
            this.btn_Unloading_Unloader_B_DestructionOff.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_Unloader_B_XMove
            // 
            this.btn_Unloading_Unloader_B_XMove.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_Unloader_B_XMove.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Unloading_Unloader_B_XMove.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_Unloader_B_XMove.Location = new System.Drawing.Point(6, 216);
            this.btn_Unloading_Unloader_B_XMove.Name = "btn_Unloading_Unloader_B_XMove";
            this.btn_Unloading_Unloader_B_XMove.Size = new System.Drawing.Size(176, 32);
            this.btn_Unloading_Unloader_B_XMove.TabIndex = 40;
            this.btn_Unloading_Unloader_B_XMove.Text = "X Move";
            this.btn_Unloading_Unloader_B_XMove.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_Unloader_B_Col2_4
            // 
            this.btn_Unloading_Unloader_B_Col2_4.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_Unloader_B_Col2_4.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_Unloader_B_Col2_4.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_Unloader_B_Col2_4.Location = new System.Drawing.Point(97, 140);
            this.btn_Unloading_Unloader_B_Col2_4.Name = "btn_Unloading_Unloader_B_Col2_4";
            this.btn_Unloading_Unloader_B_Col2_4.Size = new System.Drawing.Size(85, 32);
            this.btn_Unloading_Unloader_B_Col2_4.TabIndex = 45;
            this.btn_Unloading_Unloader_B_Col2_4.Text = "2, 4열";
            this.btn_Unloading_Unloader_B_Col2_4.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_Unloader_B_Col1_3
            // 
            this.btn_Unloading_Unloader_B_Col1_3.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_Unloader_B_Col1_3.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_Unloader_B_Col1_3.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_Unloader_B_Col1_3.Location = new System.Drawing.Point(6, 140);
            this.btn_Unloading_Unloader_B_Col1_3.Name = "btn_Unloading_Unloader_B_Col1_3";
            this.btn_Unloading_Unloader_B_Col1_3.Size = new System.Drawing.Size(85, 32);
            this.btn_Unloading_Unloader_B_Col1_3.TabIndex = 41;
            this.btn_Unloading_Unloader_B_Col1_3.Text = "1, 3열";
            this.btn_Unloading_Unloader_B_Col1_3.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_Unloader_B_CylinderUp
            // 
            this.btn_Unloading_Unloader_B_CylinderUp.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_Unloader_B_CylinderUp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_Unloader_B_CylinderUp.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_Unloader_B_CylinderUp.Location = new System.Drawing.Point(6, 26);
            this.btn_Unloading_Unloader_B_CylinderUp.Name = "btn_Unloading_Unloader_B_CylinderUp";
            this.btn_Unloading_Unloader_B_CylinderUp.Size = new System.Drawing.Size(85, 32);
            this.btn_Unloading_Unloader_B_CylinderUp.TabIndex = 44;
            this.btn_Unloading_Unloader_B_CylinderUp.Text = "실린더 업";
            this.btn_Unloading_Unloader_B_CylinderUp.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_Unloader_B_DestructionOn
            // 
            this.btn_Unloading_Unloader_B_DestructionOn.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_Unloader_B_DestructionOn.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_Unloader_B_DestructionOn.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_Unloader_B_DestructionOn.Location = new System.Drawing.Point(6, 102);
            this.btn_Unloading_Unloader_B_DestructionOn.Name = "btn_Unloading_Unloader_B_DestructionOn";
            this.btn_Unloading_Unloader_B_DestructionOn.Size = new System.Drawing.Size(85, 32);
            this.btn_Unloading_Unloader_B_DestructionOn.TabIndex = 42;
            this.btn_Unloading_Unloader_B_DestructionOn.Text = "파기 온";
            this.btn_Unloading_Unloader_B_DestructionOn.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_Unloader_B_PneumaticOn
            // 
            this.btn_Unloading_Unloader_B_PneumaticOn.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_Unloader_B_PneumaticOn.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_Unloader_B_PneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_Unloader_B_PneumaticOn.Location = new System.Drawing.Point(6, 64);
            this.btn_Unloading_Unloader_B_PneumaticOn.Name = "btn_Unloading_Unloader_B_PneumaticOn";
            this.btn_Unloading_Unloader_B_PneumaticOn.Size = new System.Drawing.Size(85, 32);
            this.btn_Unloading_Unloader_B_PneumaticOn.TabIndex = 43;
            this.btn_Unloading_Unloader_B_PneumaticOn.Text = "공압 온";
            this.btn_Unloading_Unloader_B_PneumaticOn.UseVisualStyleBackColor = false;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.tbox_Unloading_Unloader_A_Gear);
            this.groupBox6.Controls.Add(this.label2);
            this.groupBox6.Controls.Add(this.btn_Unloading_Unloader_A_CylinderDown);
            this.groupBox6.Controls.Add(this.btn_Unloading_Unloader_A_PneumaticOff);
            this.groupBox6.Controls.Add(this.btn_Unloading_Unloader_A_DestructionOff);
            this.groupBox6.Controls.Add(this.btn_Unloading_Unloader_A_Col2_4);
            this.groupBox6.Controls.Add(this.btn_Unloading_Unloader_A_CylinderUp);
            this.groupBox6.Controls.Add(this.btn_Unloading_Unloader_A_PneumaticOn);
            this.groupBox6.Controls.Add(this.btn_Unloading_Unloader_A_DestructionOn);
            this.groupBox6.Controls.Add(this.btn_Unloading_Unloader_A_Col1_3);
            this.groupBox6.Controls.Add(this.btn_Unloading_Unloader_A_XMove);
            this.groupBox6.Controls.Add(this.btn_Unloading_Unloader_A_XYMid);
            this.groupBox6.Controls.Add(this.btn_Unloading_Unloader_A_XYCst);
            this.groupBox6.Controls.Add(this.btn_Unloading_Unloader_A_UldY);
            this.groupBox6.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox6.ForeColor = System.Drawing.Color.White;
            this.groupBox6.Location = new System.Drawing.Point(15, 57);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(188, 370);
            this.groupBox6.TabIndex = 4;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = " A ";
            // 
            // tbox_Unloading_Unloader_A_Gear
            // 
            this.tbox_Unloading_Unloader_A_Gear.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.tbox_Unloading_Unloader_A_Gear.Location = new System.Drawing.Point(97, 182);
            this.tbox_Unloading_Unloader_A_Gear.Name = "tbox_Unloading_Unloader_A_Gear";
            this.tbox_Unloading_Unloader_A_Gear.Size = new System.Drawing.Size(85, 25);
            this.tbox_Unloading_Unloader_A_Gear.TabIndex = 36;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(10, 185);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 19);
            this.label2.TabIndex = 35;
            this.label2.Text = "단수 :";
            // 
            // btn_Unloading_Unloader_A_CylinderDown
            // 
            this.btn_Unloading_Unloader_A_CylinderDown.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_Unloader_A_CylinderDown.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_Unloader_A_CylinderDown.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_Unloader_A_CylinderDown.Location = new System.Drawing.Point(97, 26);
            this.btn_Unloading_Unloader_A_CylinderDown.Name = "btn_Unloading_Unloader_A_CylinderDown";
            this.btn_Unloading_Unloader_A_CylinderDown.Size = new System.Drawing.Size(85, 32);
            this.btn_Unloading_Unloader_A_CylinderDown.TabIndex = 34;
            this.btn_Unloading_Unloader_A_CylinderDown.Text = "실린더 다운";
            this.btn_Unloading_Unloader_A_CylinderDown.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_Unloader_A_PneumaticOff
            // 
            this.btn_Unloading_Unloader_A_PneumaticOff.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_Unloader_A_PneumaticOff.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_Unloader_A_PneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_Unloader_A_PneumaticOff.Location = new System.Drawing.Point(97, 64);
            this.btn_Unloading_Unloader_A_PneumaticOff.Name = "btn_Unloading_Unloader_A_PneumaticOff";
            this.btn_Unloading_Unloader_A_PneumaticOff.Size = new System.Drawing.Size(85, 32);
            this.btn_Unloading_Unloader_A_PneumaticOff.TabIndex = 33;
            this.btn_Unloading_Unloader_A_PneumaticOff.Text = "공압 오프";
            this.btn_Unloading_Unloader_A_PneumaticOff.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_Unloader_A_DestructionOff
            // 
            this.btn_Unloading_Unloader_A_DestructionOff.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_Unloader_A_DestructionOff.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_Unloader_A_DestructionOff.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_Unloader_A_DestructionOff.Location = new System.Drawing.Point(97, 102);
            this.btn_Unloading_Unloader_A_DestructionOff.Name = "btn_Unloading_Unloader_A_DestructionOff";
            this.btn_Unloading_Unloader_A_DestructionOff.Size = new System.Drawing.Size(85, 32);
            this.btn_Unloading_Unloader_A_DestructionOff.TabIndex = 32;
            this.btn_Unloading_Unloader_A_DestructionOff.Text = "파기 오프";
            this.btn_Unloading_Unloader_A_DestructionOff.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_Unloader_A_Col2_4
            // 
            this.btn_Unloading_Unloader_A_Col2_4.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_Unloader_A_Col2_4.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_Unloader_A_Col2_4.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_Unloader_A_Col2_4.Location = new System.Drawing.Point(97, 140);
            this.btn_Unloading_Unloader_A_Col2_4.Name = "btn_Unloading_Unloader_A_Col2_4";
            this.btn_Unloading_Unloader_A_Col2_4.Size = new System.Drawing.Size(85, 32);
            this.btn_Unloading_Unloader_A_Col2_4.TabIndex = 31;
            this.btn_Unloading_Unloader_A_Col2_4.Text = "2, 4열";
            this.btn_Unloading_Unloader_A_Col2_4.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_Unloader_A_CylinderUp
            // 
            this.btn_Unloading_Unloader_A_CylinderUp.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_Unloader_A_CylinderUp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_Unloader_A_CylinderUp.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_Unloader_A_CylinderUp.Location = new System.Drawing.Point(6, 26);
            this.btn_Unloading_Unloader_A_CylinderUp.Name = "btn_Unloading_Unloader_A_CylinderUp";
            this.btn_Unloading_Unloader_A_CylinderUp.Size = new System.Drawing.Size(85, 32);
            this.btn_Unloading_Unloader_A_CylinderUp.TabIndex = 30;
            this.btn_Unloading_Unloader_A_CylinderUp.Text = "실린더 업";
            this.btn_Unloading_Unloader_A_CylinderUp.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_Unloader_A_PneumaticOn
            // 
            this.btn_Unloading_Unloader_A_PneumaticOn.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_Unloader_A_PneumaticOn.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_Unloader_A_PneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_Unloader_A_PneumaticOn.Location = new System.Drawing.Point(6, 64);
            this.btn_Unloading_Unloader_A_PneumaticOn.Name = "btn_Unloading_Unloader_A_PneumaticOn";
            this.btn_Unloading_Unloader_A_PneumaticOn.Size = new System.Drawing.Size(85, 32);
            this.btn_Unloading_Unloader_A_PneumaticOn.TabIndex = 29;
            this.btn_Unloading_Unloader_A_PneumaticOn.Text = "공압 온";
            this.btn_Unloading_Unloader_A_PneumaticOn.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_Unloader_A_DestructionOn
            // 
            this.btn_Unloading_Unloader_A_DestructionOn.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_Unloader_A_DestructionOn.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_Unloader_A_DestructionOn.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_Unloader_A_DestructionOn.Location = new System.Drawing.Point(6, 102);
            this.btn_Unloading_Unloader_A_DestructionOn.Name = "btn_Unloading_Unloader_A_DestructionOn";
            this.btn_Unloading_Unloader_A_DestructionOn.Size = new System.Drawing.Size(85, 32);
            this.btn_Unloading_Unloader_A_DestructionOn.TabIndex = 28;
            this.btn_Unloading_Unloader_A_DestructionOn.Text = "파기 온";
            this.btn_Unloading_Unloader_A_DestructionOn.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_Unloader_A_Col1_3
            // 
            this.btn_Unloading_Unloader_A_Col1_3.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_Unloader_A_Col1_3.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_Unloader_A_Col1_3.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_Unloader_A_Col1_3.Location = new System.Drawing.Point(6, 140);
            this.btn_Unloading_Unloader_A_Col1_3.Name = "btn_Unloading_Unloader_A_Col1_3";
            this.btn_Unloading_Unloader_A_Col1_3.Size = new System.Drawing.Size(85, 32);
            this.btn_Unloading_Unloader_A_Col1_3.TabIndex = 27;
            this.btn_Unloading_Unloader_A_Col1_3.Text = "1, 3열";
            this.btn_Unloading_Unloader_A_Col1_3.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_Unloader_A_XMove
            // 
            this.btn_Unloading_Unloader_A_XMove.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_Unloader_A_XMove.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Unloading_Unloader_A_XMove.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_Unloader_A_XMove.Location = new System.Drawing.Point(6, 216);
            this.btn_Unloading_Unloader_A_XMove.Name = "btn_Unloading_Unloader_A_XMove";
            this.btn_Unloading_Unloader_A_XMove.Size = new System.Drawing.Size(176, 32);
            this.btn_Unloading_Unloader_A_XMove.TabIndex = 26;
            this.btn_Unloading_Unloader_A_XMove.Text = "X Move";
            this.btn_Unloading_Unloader_A_XMove.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_Unloader_A_XYMid
            // 
            this.btn_Unloading_Unloader_A_XYMid.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_Unloader_A_XYMid.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Unloading_Unloader_A_XYMid.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_Unloader_A_XYMid.Location = new System.Drawing.Point(6, 254);
            this.btn_Unloading_Unloader_A_XYMid.Name = "btn_Unloading_Unloader_A_XYMid";
            this.btn_Unloading_Unloader_A_XYMid.Size = new System.Drawing.Size(176, 32);
            this.btn_Unloading_Unloader_A_XYMid.TabIndex = 25;
            this.btn_Unloading_Unloader_A_XYMid.Text = "X, Y Mid";
            this.btn_Unloading_Unloader_A_XYMid.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_Unloader_A_XYCst
            // 
            this.btn_Unloading_Unloader_A_XYCst.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_Unloader_A_XYCst.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Unloading_Unloader_A_XYCst.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_Unloader_A_XYCst.Location = new System.Drawing.Point(6, 292);
            this.btn_Unloading_Unloader_A_XYCst.Name = "btn_Unloading_Unloader_A_XYCst";
            this.btn_Unloading_Unloader_A_XYCst.Size = new System.Drawing.Size(176, 32);
            this.btn_Unloading_Unloader_A_XYCst.TabIndex = 24;
            this.btn_Unloading_Unloader_A_XYCst.Text = "X, Y Cst";
            this.btn_Unloading_Unloader_A_XYCst.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_Unloader_A_UldY
            // 
            this.btn_Unloading_Unloader_A_UldY.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_Unloader_A_UldY.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Unloading_Unloader_A_UldY.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_Unloader_A_UldY.Location = new System.Drawing.Point(6, 330);
            this.btn_Unloading_Unloader_A_UldY.Name = "btn_Unloading_Unloader_A_UldY";
            this.btn_Unloading_Unloader_A_UldY.Size = new System.Drawing.Size(176, 32);
            this.btn_Unloading_Unloader_A_UldY.TabIndex = 23;
            this.btn_Unloading_Unloader_A_UldY.Text = "Uld Y";
            this.btn_Unloading_Unloader_A_UldY.UseVisualStyleBackColor = false;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.SlateGray;
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Controls.Add(this.label8);
            this.panel10.Controls.Add(this.btn_Unloading_A_CIM);
            this.panel10.Controls.Add(this.btn_Unloading_A_CST2Lift);
            this.panel10.Controls.Add(this.btn_Unloading_A_Unloader2CST);
            this.panel10.Controls.Add(this.btn_Unloading_A_Stage2UnloadingTransfer);
            this.panel10.Controls.Add(this.btn_Unloading_A_UnloadingTransfer2Unloader);
            this.panel10.Controls.Add(this.btn_Unloading_A_CSTLift2Buffer);
            this.panel10.Location = new System.Drawing.Point(14, 65);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(409, 330);
            this.panel10.TabIndex = 5;
            // 
            // btn_Unloading_A_CIM
            // 
            this.btn_Unloading_A_CIM.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_A_CIM.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_A_CIM.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_A_CIM.Location = new System.Drawing.Point(294, 274);
            this.btn_Unloading_A_CIM.Name = "btn_Unloading_A_CIM";
            this.btn_Unloading_A_CIM.Size = new System.Drawing.Size(77, 38);
            this.btn_Unloading_A_CIM.TabIndex = 19;
            this.btn_Unloading_A_CIM.Text = "CIM";
            this.btn_Unloading_A_CIM.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_A_CST2Lift
            // 
            this.btn_Unloading_A_CST2Lift.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_A_CST2Lift.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_A_CST2Lift.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_A_CST2Lift.Location = new System.Drawing.Point(42, 22);
            this.btn_Unloading_A_CST2Lift.Name = "btn_Unloading_A_CST2Lift";
            this.btn_Unloading_A_CST2Lift.Size = new System.Drawing.Size(329, 38);
            this.btn_Unloading_A_CST2Lift.TabIndex = 14;
            this.btn_Unloading_A_CST2Lift.Text = "카세트 → 리프트";
            this.btn_Unloading_A_CST2Lift.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_A_Unloader2CST
            // 
            this.btn_Unloading_A_Unloader2CST.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_A_Unloader2CST.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_A_Unloader2CST.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_A_Unloader2CST.Location = new System.Drawing.Point(42, 211);
            this.btn_Unloading_A_Unloader2CST.Name = "btn_Unloading_A_Unloader2CST";
            this.btn_Unloading_A_Unloader2CST.Size = new System.Drawing.Size(329, 38);
            this.btn_Unloading_A_Unloader2CST.TabIndex = 17;
            this.btn_Unloading_A_Unloader2CST.Text = "언로더  → 카세트";
            this.btn_Unloading_A_Unloader2CST.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_A_Stage2UnloadingTransfer
            // 
            this.btn_Unloading_A_Stage2UnloadingTransfer.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_A_Stage2UnloadingTransfer.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_A_Stage2UnloadingTransfer.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_A_Stage2UnloadingTransfer.Location = new System.Drawing.Point(42, 85);
            this.btn_Unloading_A_Stage2UnloadingTransfer.Name = "btn_Unloading_A_Stage2UnloadingTransfer";
            this.btn_Unloading_A_Stage2UnloadingTransfer.Size = new System.Drawing.Size(329, 38);
            this.btn_Unloading_A_Stage2UnloadingTransfer.TabIndex = 15;
            this.btn_Unloading_A_Stage2UnloadingTransfer.Text = "스테이지  →  언로딩 트랜스퍼";
            this.btn_Unloading_A_Stage2UnloadingTransfer.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_A_UnloadingTransfer2Unloader
            // 
            this.btn_Unloading_A_UnloadingTransfer2Unloader.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_A_UnloadingTransfer2Unloader.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_A_UnloadingTransfer2Unloader.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_A_UnloadingTransfer2Unloader.Location = new System.Drawing.Point(42, 148);
            this.btn_Unloading_A_UnloadingTransfer2Unloader.Name = "btn_Unloading_A_UnloadingTransfer2Unloader";
            this.btn_Unloading_A_UnloadingTransfer2Unloader.Size = new System.Drawing.Size(329, 38);
            this.btn_Unloading_A_UnloadingTransfer2Unloader.TabIndex = 16;
            this.btn_Unloading_A_UnloadingTransfer2Unloader.Text = "언로딩 트랜스퍼  → 언로더";
            this.btn_Unloading_A_UnloadingTransfer2Unloader.UseVisualStyleBackColor = false;
            // 
            // btn_Unloading_A_CSTLift2Buffer
            // 
            this.btn_Unloading_A_CSTLift2Buffer.BackColor = System.Drawing.Color.DimGray;
            this.btn_Unloading_A_CSTLift2Buffer.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btn_Unloading_A_CSTLift2Buffer.ForeColor = System.Drawing.Color.White;
            this.btn_Unloading_A_CSTLift2Buffer.Location = new System.Drawing.Point(42, 274);
            this.btn_Unloading_A_CSTLift2Buffer.Name = "btn_Unloading_A_CSTLift2Buffer";
            this.btn_Unloading_A_CSTLift2Buffer.Size = new System.Drawing.Size(231, 38);
            this.btn_Unloading_A_CSTLift2Buffer.TabIndex = 18;
            this.btn_Unloading_A_CSTLift2Buffer.Text = "카세트 리프트  → 버퍼";
            this.btn_Unloading_A_CSTLift2Buffer.UseVisualStyleBackColor = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(192, -11);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 30);
            this.label4.TabIndex = 16;
            this.label4.Text = "A";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(193, -10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(26, 30);
            this.label5.TabIndex = 16;
            this.label5.Text = "B";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(168, -9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 30);
            this.label6.TabIndex = 16;
            this.label6.Text = "Loader";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(123, -9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(170, 30);
            this.label7.TabIndex = 16;
            this.label7.Text = "Loader Transfer";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(193, -9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(28, 30);
            this.label8.TabIndex = 16;
            this.label8.Text = "A";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(192, -10);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(26, 30);
            this.label10.TabIndex = 16;
            this.label10.Text = "B";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(165, -9);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(104, 30);
            this.label11.TabIndex = 16;
            this.label11.Text = "Unloader";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(117, -9);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(193, 30);
            this.label12.TabIndex = 16;
            this.label12.Text = "Unloader Transfer";
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel23.Controls.Add(this.label19);
            this.panel23.ForeColor = System.Drawing.Color.White;
            this.panel23.Location = new System.Drawing.Point(1, 3);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(862, 38);
            this.panel23.TabIndex = 54;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold);
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(383, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(107, 32);
            this.label19.TabIndex = 53;
            this.label19.Text = "Loading";
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel11.Controls.Add(this.label13);
            this.panel11.ForeColor = System.Drawing.Color.White;
            this.panel11.Location = new System.Drawing.Point(1, 3);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(863, 38);
            this.panel11.TabIndex = 54;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold);
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(357, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(135, 32);
            this.label13.TabIndex = 53;
            this.label13.Text = "Unloading";
            // 
            // Manager_Control
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Name = "Manager_Control";
            this.Size = new System.Drawing.Size(1740, 875);
            this.panel1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Button btn_Loading_LoaderTransfer_LoadingTransfer2Align;
        private System.Windows.Forms.Button btn_Loading_LoaderTransfer_CylinderReset;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btn_Loading_B_FreeAilgn;
        private System.Windows.Forms.Button btn_Loading_B_CST2Loader;
        private System.Windows.Forms.Button btn_Loading_B_CST2Stage;
        private System.Windows.Forms.Button btn_Loading_B_CST2Lift;
        private System.Windows.Forms.Button btn_Loading_B_CSTLift2Buffer;
        private System.Windows.Forms.Button btn_Loading_B_Loader2LoadingTransfer;
        private System.Windows.Forms.Button btn_Loading_B_LoadingTransfer2Stage;
        private System.Windows.Forms.Button btn_Loading_Loader_CylinderReset;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_Loading_Loader_A_CylinderDown;
        private System.Windows.Forms.Button btn_Loading_Loader_A_PneumaticOff;
        private System.Windows.Forms.Button btn_Loading_Loader_A_DestructionOff;
        private System.Windows.Forms.Button btn_Loading_Loader_A_Col2_4;
        private System.Windows.Forms.Button btn_Loading_Loader_A_CylinderUp;
        private System.Windows.Forms.Button btn_Loading_Loader_A_PneumaticOn;
        private System.Windows.Forms.Button btn_Loading_Loader_A_DestructionOn;
        private System.Windows.Forms.Button btn_Loading_Loader_A_Col1_3;
        private System.Windows.Forms.Button btn_Loading_Loader_A_XMove;
        private System.Windows.Forms.Button btn_Loading_Loader_A_XYMid;
        private System.Windows.Forms.Button btn_Loading_Loader_A_XYCst;
        private System.Windows.Forms.Button btn_Loading_Loader_A_UldY;
        private System.Windows.Forms.Button btn_Loading_A_FreeAilgn;
        private System.Windows.Forms.Button btn_Loading_A_CST2Stage;
        private System.Windows.Forms.Button btn_Loading_A_CSTLift2Buffer;
        private System.Windows.Forms.Button btn_Loading_A_LoadingTransfer2Stage;
        private System.Windows.Forms.Button btn_Loading_A_Loader2LoadingTransfer;
        private System.Windows.Forms.Button btn_Loading_A_CST2Loader;
        private System.Windows.Forms.Button btn_Loading_A_CST2Lift;
        private System.Windows.Forms.Button btn_Unloading_UnloaderTransfer_CylinderReset;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button btn_Unloading_B_CIM;
        private System.Windows.Forms.Button btn_Unloading_B_CST2Lift;
        private System.Windows.Forms.Button btn_Unloading_B_CSTLift2Buffer;
        private System.Windows.Forms.Button btn_Unloading_B_Unloader2CST;
        private System.Windows.Forms.Button btn_Unloading_B_UnloadingTransfer2Unloader;
        private System.Windows.Forms.Button btn_Unloading_B_Stage2UnloadingTransfer;
        private System.Windows.Forms.Button btn_Unloading_Unloader_CylinderReset;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button btn_Unloading_A_CIM;
        private System.Windows.Forms.Button btn_Unloading_A_CST2Lift;
        private System.Windows.Forms.Button btn_Unloading_A_Unloader2CST;
        private System.Windows.Forms.Button btn_Unloading_A_Stage2UnloadingTransfer;
        private System.Windows.Forms.Button btn_Unloading_A_UnloadingTransfer2Unloader;
        private System.Windows.Forms.Button btn_Unloading_A_CSTLift2Buffer;
        private System.Windows.Forms.TextBox tbox_Loading_Loader_A_Gear;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btn_Loading_LoaderTransfer_B_DestructionOff;
        private System.Windows.Forms.Button btn_Loading_LoaderTransfer_B_PneumaticOff;
        private System.Windows.Forms.Button btn_Loading_LoaderTransfer_B_DestructionOn;
        private System.Windows.Forms.Button btn_Loading_LoaderTransfer_B_PneumaticOn;
        private System.Windows.Forms.Button btn_Loading_LoaderTransfer_A_DestructionOff;
        private System.Windows.Forms.Button btn_Loading_LoaderTransfer_A_DestructionOn;
        private System.Windows.Forms.Button btn_Loading_LoaderTransfer_A_PneumaticOff;
        private System.Windows.Forms.Button btn_Loading_LoaderTransfer_A_PneumaticOn;
        private System.Windows.Forms.TextBox tbox_Loading_Loader_B_Gear;
        private System.Windows.Forms.Button btn_Loading_Loader_B_CylinderDown;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_Loading_Loader_B_UldY;
        private System.Windows.Forms.Button btn_Loading_Loader_B_XYCst;
        private System.Windows.Forms.Button btn_Loading_Loader_B_PneumaticOff;
        private System.Windows.Forms.Button btn_Loading_Loader_B_XYMid;
        private System.Windows.Forms.Button btn_Loading_Loader_B_DestructionOff;
        private System.Windows.Forms.Button btn_Loading_Loader_B_XMove;
        private System.Windows.Forms.Button btn_Loading_Loader_B_Col2_4;
        private System.Windows.Forms.Button btn_Loading_Loader_B_Col1_3;
        private System.Windows.Forms.Button btn_Loading_Loader_B_CylinderUp;
        private System.Windows.Forms.Button btn_Loading_Loader_B_DestructionOn;
        private System.Windows.Forms.Button btn_Loading_Loader_B_PneumaticOn;
        private System.Windows.Forms.TextBox tbox_Unloading_Unloader_A_Gear;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_Unloading_Unloader_A_CylinderDown;
        private System.Windows.Forms.Button btn_Unloading_Unloader_A_PneumaticOff;
        private System.Windows.Forms.Button btn_Unloading_Unloader_A_DestructionOff;
        private System.Windows.Forms.Button btn_Unloading_Unloader_A_Col2_4;
        private System.Windows.Forms.Button btn_Unloading_Unloader_A_CylinderUp;
        private System.Windows.Forms.Button btn_Unloading_Unloader_A_PneumaticOn;
        private System.Windows.Forms.Button btn_Unloading_Unloader_A_DestructionOn;
        private System.Windows.Forms.Button btn_Unloading_Unloader_A_Col1_3;
        private System.Windows.Forms.Button btn_Unloading_Unloader_A_XMove;
        private System.Windows.Forms.Button btn_Unloading_Unloader_A_XYMid;
        private System.Windows.Forms.Button btn_Unloading_Unloader_A_XYCst;
        private System.Windows.Forms.Button btn_Unloading_Unloader_A_UldY;
        private System.Windows.Forms.Button btn_Unloading_UnloaderTransfer_B_UnloadingTransfer2Inspaction;
        private System.Windows.Forms.Button btn_Unloading_UnloaderTransfer_B_UnloadingTransfer2MCR;
        private System.Windows.Forms.Button btn_Unloading_UnloaderTransfer_B_DestructionOff;
        private System.Windows.Forms.Button btn_Unloading_UnloaderTransfer_B_DestructionOn;
        private System.Windows.Forms.Button btn_Unloading_UnloaderTransfer_B_PneumaticOff;
        private System.Windows.Forms.Button btn_Unloading_UnloaderTransfer_B_PneumaticOn;
        private System.Windows.Forms.Button btn_Unloading_UnloaderTransfer_A_UnloadingTransfer2Inspaction;
        private System.Windows.Forms.Button btn_Unloading_UnloaderTransfer_A_UnloadingTransfer2MCR;
        private System.Windows.Forms.Button btn_Unloading_UnloaderTransfer_A_DestructionOff;
        private System.Windows.Forms.Button btn_Unloading_UnloaderTransfer_A_DestructionOn;
        private System.Windows.Forms.Button btn_Unloading_UnloaderTransfer_A_PneumaticOff;
        private System.Windows.Forms.Button btn_Unloading_UnloaderTransfer_A_PneumaticOn;
        private System.Windows.Forms.TextBox tbox_Unloading_Unloader_B_Gear;
        private System.Windows.Forms.Button btn_Unloading_Unloader_B_PneumaticOff;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_Unloading_Unloader_B_UldY;
        private System.Windows.Forms.Button btn_Unloading_Unloader_B_CylinderDown;
        private System.Windows.Forms.Button btn_Unloading_Unloader_B_XYCst;
        private System.Windows.Forms.Button btn_Unloading_Unloader_B_XYMid;
        private System.Windows.Forms.Button btn_Unloading_Unloader_B_DestructionOff;
        private System.Windows.Forms.Button btn_Unloading_Unloader_B_XMove;
        private System.Windows.Forms.Button btn_Unloading_Unloader_B_Col2_4;
        private System.Windows.Forms.Button btn_Unloading_Unloader_B_Col1_3;
        private System.Windows.Forms.Button btn_Unloading_Unloader_B_CylinderUp;
        private System.Windows.Forms.Button btn_Unloading_Unloader_B_DestructionOn;
        private System.Windows.Forms.Button btn_Unloading_Unloader_B_PneumaticOn;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label13;
    }
}
