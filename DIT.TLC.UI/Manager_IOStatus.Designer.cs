﻿namespace DIT.TLC.UI
{
    partial class Manager_IOStatus
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.tc_iostatus_info = new System.Windows.Forms.TabControl();
            this.tp_iostatus_ld = new System.Windows.Forms.TabPage();
            this.tc_iostatus_ld_out = new System.Windows.Forms.TabControl();
            this.tp_iostatus_ld_out1 = new System.Windows.Forms.TabPage();
            this.lb_y19f_4_ld_cst_d_ungrip = new System.Windows.Forms.Label();
            this.pn_Loader_OUT1_Y19F = new System.Windows.Forms.Panel();
            this.lb_y19e_4_ld_cst_d_grip = new System.Windows.Forms.Label();
            this.pn_Loader_OUT1_Y19E = new System.Windows.Forms.Panel();
            this.lb_y19d_4_ld_cst_lr_ungrip = new System.Windows.Forms.Label();
            this.pn_Loader_OUT1_Y19D = new System.Windows.Forms.Panel();
            this.lb_y19c_4_ld_cst_lr_grip = new System.Windows.Forms.Label();
            this.pn_Loader_OUT1_Y19C = new System.Windows.Forms.Panel();
            this.lb_y19b_3_ld_cst_d_ungrip = new System.Windows.Forms.Label();
            this.pn_Loader_OUT1_Y19B = new System.Windows.Forms.Panel();
            this.lb_y19a_3_ld_cst_d_grip = new System.Windows.Forms.Label();
            this.pn_Loader_OUT1_Y19A = new System.Windows.Forms.Panel();
            this.lb_y199_3_ld_cst_lr_ungrip = new System.Windows.Forms.Label();
            this.pn_Loader_OUT1_Y199 = new System.Windows.Forms.Panel();
            this.lb_y198_3_ld_cst_lr_grip = new System.Windows.Forms.Label();
            this.pn_Loader_OUT1_Y198 = new System.Windows.Forms.Panel();
            this.lb_y197_2_ld_cst_d_ungrip = new System.Windows.Forms.Label();
            this.pn_Loader_OUT1_Y197 = new System.Windows.Forms.Panel();
            this.lb_y196_2_ld_cst_d_grip = new System.Windows.Forms.Label();
            this.pn_Loader_OUT1_Y196 = new System.Windows.Forms.Panel();
            this.lb_y195_2_ld_cst_lr_ungrip = new System.Windows.Forms.Label();
            this.pn_Loader_OUT1_Y195 = new System.Windows.Forms.Panel();
            this.lb_y194_2_ld_cst_lr_grip = new System.Windows.Forms.Label();
            this.pn_Loader_OUT1_Y194 = new System.Windows.Forms.Panel();
            this.lb_y193_1_ld_cst_d_ungrip = new System.Windows.Forms.Label();
            this.pn_Loader_OUT1_Y193 = new System.Windows.Forms.Panel();
            this.lb_y192_1_ld_cst_d_grip = new System.Windows.Forms.Label();
            this.pn_Loader_OUT1_Y192 = new System.Windows.Forms.Panel();
            this.lb_y191_1_ld_cst_lr_ungrip = new System.Windows.Forms.Label();
            this.pn_Loader_OUT1_Y191 = new System.Windows.Forms.Panel();
            this.lb_y190_1_ld_cst_lr_grip = new System.Windows.Forms.Label();
            this.pn_Loader_OUT1_Y190 = new System.Windows.Forms.Panel();
            this.lb_y18f_mode_select_sw = new System.Windows.Forms.Label();
            this.pn_Loader_OUT1_Y18F = new System.Windows.Forms.Panel();
            this.lb_y18e_ld_reset = new System.Windows.Forms.Label();
            this.pn_Loader_OUT1_Y18E = new System.Windows.Forms.Panel();
            this.lb_y18d_cst_out_ready_buzzer = new System.Windows.Forms.Label();
            this.pn_Loader_OUT1_Y18D = new System.Windows.Forms.Panel();
            this.lb_y18c_cst_in_ready_buzzer = new System.Windows.Forms.Label();
            this.pn_Loader_OUT1_Y18C = new System.Windows.Forms.Panel();
            this.lb_y18b_control_select_ld_feedback = new System.Windows.Forms.Label();
            this.pn_Loader_OUT1_Y18B = new System.Windows.Forms.Panel();
            this.lb_y18a_buzzer = new System.Windows.Forms.Label();
            this.pn_Loader_OUT1_Y18A = new System.Windows.Forms.Panel();
            this.lb_y189_1_tower_lamp_g = new System.Windows.Forms.Label();
            this.pn_Loader_OUT1_Y189 = new System.Windows.Forms.Panel();
            this.lb_y188_1_tower_lamp_y = new System.Windows.Forms.Label();
            this.pn_Loader_OUT1_Y188 = new System.Windows.Forms.Panel();
            this.lb_y187_1_tower_lamp_r = new System.Windows.Forms.Label();
            this.pn_Loader_OUT1_Y187 = new System.Windows.Forms.Panel();
            this.lb_y186_safty_reset = new System.Windows.Forms.Label();
            this.pn_Loader_OUT1_Y186 = new System.Windows.Forms.Panel();
            this.lb_y185_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT1_Y185 = new System.Windows.Forms.Panel();
            this.lb_y184_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT1_Y184 = new System.Windows.Forms.Panel();
            this.lb_y183_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT1_Y183 = new System.Windows.Forms.Panel();
            this.lb_y182_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT1_Y182 = new System.Windows.Forms.Panel();
            this.lb_y181_laser_ld_motor_control_on_cmd = new System.Windows.Forms.Label();
            this.pn_Loader_OUT1_Y181 = new System.Windows.Forms.Panel();
            this.lb_y180_ld_motor_control_on_cmd = new System.Windows.Forms.Label();
            this.pn_Loader_OUT1_Y180 = new System.Windows.Forms.Panel();
            this.tp_iostatus_ld_out2 = new System.Windows.Forms.TabPage();
            this.lb_y1bf_spare_air_on_cmd = new System.Windows.Forms.Label();
            this.pn_Loader_OUT2_Y1BF = new System.Windows.Forms.Panel();
            this.lb_y1be_spare_air_on_cmd = new System.Windows.Forms.Label();
            this.pn_Loader_OUT2_Y1BE = new System.Windows.Forms.Panel();
            this.lb_y1bd_2_cst_trans_air_on = new System.Windows.Forms.Label();
            this.pn_Loader_OUT2_Y1BD = new System.Windows.Forms.Panel();
            this.lb_y1bc_2_cst_trans_vacuum_off = new System.Windows.Forms.Label();
            this.pn_Loader_OUT2_Y1BC = new System.Windows.Forms.Panel();
            this.lb_y1bb_2_cst_trans_vacuum_on = new System.Windows.Forms.Label();
            this.pn_Loader_OUT2_Y1BB = new System.Windows.Forms.Panel();
            this.lb_y1ba_1_cst_trans_air_on = new System.Windows.Forms.Label();
            this.pn_Loader_OUT2_Y1BA = new System.Windows.Forms.Panel();
            this.lb_y1b9_1_cst_trans_vacuum_off = new System.Windows.Forms.Label();
            this.pn_Loader_OUT2_Y1B9 = new System.Windows.Forms.Panel();
            this.lb_y1b8_1_cst_trans_vacuum_on = new System.Windows.Forms.Label();
            this.pn_Loader_OUT2_Y1B8 = new System.Windows.Forms.Panel();
            this.lb_y1b6_2_ld_cst_lift_tiltsensor_down = new System.Windows.Forms.Label();
            this.pn_Loader_OUT2_Y1B7 = new System.Windows.Forms.Panel();
            this.lb_y1b6_2_ld_cst_lift_tiltsensor_up = new System.Windows.Forms.Label();
            this.pn_Loader_OUT2_Y1B6 = new System.Windows.Forms.Panel();
            this.lb_y1b5_2_ld_trans_2ch_air_on = new System.Windows.Forms.Label();
            this.pn_Loader_OUT2_Y1B5 = new System.Windows.Forms.Panel();
            this.lb_y1b4_2_ld_trans_2ch_vacuum_off = new System.Windows.Forms.Label();
            this.pn_Loader_OUT2_Y1B4 = new System.Windows.Forms.Panel();
            this.lb_y1b3_2_ld_trans_2ch_vacuum_on = new System.Windows.Forms.Label();
            this.pn_Loader_OUT2_Y1B3 = new System.Windows.Forms.Panel();
            this.lb_y1b2_2_ld_trans_1ch_air_on = new System.Windows.Forms.Label();
            this.pn_Loader_OUT2_Y1B2 = new System.Windows.Forms.Panel();
            this.lb_y1b1_2_ld_trans_1ch_vacuum_off = new System.Windows.Forms.Label();
            this.pn_Loader_OUT2_Y1B1 = new System.Windows.Forms.Panel();
            this.lb_y1b0_2_lld_trans_1ch_vacuum_on = new System.Windows.Forms.Label();
            this.pn_Loader_OUT2_Y1B0 = new System.Windows.Forms.Panel();
            this.lb_y1af_1_ld_trans_2ch_air_on = new System.Windows.Forms.Label();
            this.pn_Loader_OUT2_Y1AF = new System.Windows.Forms.Panel();
            this.lb_y1ae_1_ld_trans_2ch_vacuum_off = new System.Windows.Forms.Label();
            this.pn_Loader_OUT2_Y1AE = new System.Windows.Forms.Panel();
            this.lb_y1ad_1_ld_trans_2ch_vacuum_on = new System.Windows.Forms.Label();
            this.pn_Loader_OUT2_Y1AD = new System.Windows.Forms.Panel();
            this.lb_y1ac_1_ld_trans_1ch_air_on = new System.Windows.Forms.Label();
            this.pn_Loader_OUT2_Y1AC = new System.Windows.Forms.Panel();
            this.lb_y1ab_1_ld_trans_1ch_vacuum_off = new System.Windows.Forms.Label();
            this.pn_Loader_OUT2_Y1AB = new System.Windows.Forms.Panel();
            this.lb_y1aa_1_ld_trans_1ch_vacuum_on = new System.Windows.Forms.Label();
            this.pn_Loader_OUT2_Y1AA = new System.Windows.Forms.Panel();
            this.lb_y1a9_2_ld_trans_down = new System.Windows.Forms.Label();
            this.pn_Loader_OUT2_Y1A9 = new System.Windows.Forms.Panel();
            this.lb_y1a8_2_ld_trans_up = new System.Windows.Forms.Label();
            this.pn_Loader_OUT2_Y1A8 = new System.Windows.Forms.Panel();
            this.lb_y1a7_1_ld_trans_down = new System.Windows.Forms.Label();
            this.pn_Loader_OUT2_Y1A7 = new System.Windows.Forms.Panel();
            this.lb_y1a6_1_ld_trans_up = new System.Windows.Forms.Label();
            this.pn_Loader_OUT2_Y1A6 = new System.Windows.Forms.Panel();
            this.lb_y1a5_1_ld_cst_lift_tiltsensor_down = new System.Windows.Forms.Label();
            this.pn_Loader_OUT2_Y1A5 = new System.Windows.Forms.Panel();
            this.lb_y1a4_1_ld_cst_lift_tiltsensor_up = new System.Windows.Forms.Label();
            this.pn_Loader_OUT2_Y1A4 = new System.Windows.Forms.Panel();
            this.lb_y1a3_2_ld_cst_lifter_ungrip = new System.Windows.Forms.Label();
            this.pn_Loader_OUT2_Y1A3 = new System.Windows.Forms.Panel();
            this.lb_y1a2_2_ld_cst_lifter_grip = new System.Windows.Forms.Label();
            this.pn_Loader_OUT2_Y1A2 = new System.Windows.Forms.Panel();
            this.lb_y1a1_1_ld_cst_lifter_ungrip = new System.Windows.Forms.Label();
            this.pn_Loader_OUT2_Y1A1 = new System.Windows.Forms.Panel();
            this.lb_y1a0_1_ld_cst_lifter_grip = new System.Windows.Forms.Label();
            this.pn_Loader_OUT2_Y1A0 = new System.Windows.Forms.Panel();
            this.tp_iostatus_ld_out3 = new System.Windows.Forms.TabPage();
            this.lb_y1df_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT3_Y1DF = new System.Windows.Forms.Panel();
            this.lb_y1de_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT3_Y1DE = new System.Windows.Forms.Panel();
            this.lb_y1dd_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT3_Y1DD = new System.Windows.Forms.Panel();
            this.lb_y1dc_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT3_Y1DC = new System.Windows.Forms.Panel();
            this.lb_y1db_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT3_Y1DB = new System.Windows.Forms.Panel();
            this.lb_y1da_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT3_Y1DA = new System.Windows.Forms.Panel();
            this.lb_y1d9_laser_led = new System.Windows.Forms.Label();
            this.pn_Loader_OUT3_Y1D9 = new System.Windows.Forms.Panel();
            this.lb_y1d8_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT3_Y1D8 = new System.Windows.Forms.Panel();
            this.lb_y1d7_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT3_Y1D7 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.pn_Loader_OUT3_Y1D6 = new System.Windows.Forms.Panel();
            this.lb_y1d5_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT3_Y1D5 = new System.Windows.Forms.Panel();
            this.lb_y1d4_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT3_Y1D4 = new System.Windows.Forms.Panel();
            this.lb_y1d3_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT3_Y1D3 = new System.Windows.Forms.Panel();
            this.lb_y1d2_light_3_onoff = new System.Windows.Forms.Label();
            this.pn_Loader_OUT3_Y1D2 = new System.Windows.Forms.Panel();
            this.lb_y1d1_light_2_onoff = new System.Windows.Forms.Label();
            this.pn_Loader_OUT3_Y1D1 = new System.Windows.Forms.Panel();
            this.lb_y1d0_light_1_onoff = new System.Windows.Forms.Label();
            this.pn_Loader_OUT3_Y1D0 = new System.Windows.Forms.Panel();
            this.lb_y1cf_spare_air_on_cmd = new System.Windows.Forms.Label();
            this.pn_Loader_OUT3_Y1CF = new System.Windows.Forms.Panel();
            this.lb_y1ce_spare_air_on_cmd = new System.Windows.Forms.Label();
            this.pn_Loader_OUT3_Y1CE = new System.Windows.Forms.Panel();
            this.lb_y1cd_spare_sol_on_cmd = new System.Windows.Forms.Label();
            this.pn_Loader_OUT3_Y1CD = new System.Windows.Forms.Panel();
            this.lb_y1cc_spare_sol_on_cmd = new System.Windows.Forms.Label();
            this.pn_Loader_OUT3_Y1CC = new System.Windows.Forms.Panel();
            this.lb_y1cb_spare_sol_on_cmd = new System.Windows.Forms.Label();
            this.pn_Loader_OUT3_Y1CB = new System.Windows.Forms.Panel();
            this.lb_y1ca_spare_sol_on_cmd = new System.Windows.Forms.Label();
            this.pn_Loader_OUT3_Y1CA = new System.Windows.Forms.Panel();
            this.lb_y1c9_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT3_Y1C9 = new System.Windows.Forms.Panel();
            this.lb_y1c8_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT3_Y1C8 = new System.Windows.Forms.Panel();
            this.lb_y1c7_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT3_Y1C7 = new System.Windows.Forms.Panel();
            this.lb_y1c6_7_safety_door_sw_open = new System.Windows.Forms.Label();
            this.pn_Loader_OUT3_Y1C6 = new System.Windows.Forms.Panel();
            this.lb_y1c5_6_safety_door_sw_open = new System.Windows.Forms.Label();
            this.pn_Loader_OUT3_Y1C5 = new System.Windows.Forms.Panel();
            this.lb_y1c4_5_safety_door_sw_open = new System.Windows.Forms.Label();
            this.pn_Loader_OUT3_Y1C4 = new System.Windows.Forms.Panel();
            this.lb_y1c3_4_safety_door_sw_open = new System.Windows.Forms.Label();
            this.pn_Loader_OUT3_Y1C3 = new System.Windows.Forms.Panel();
            this.lb_y1c2_3_safety_door_sw_open = new System.Windows.Forms.Label();
            this.pn_Loader_OUT3_Y1C2 = new System.Windows.Forms.Panel();
            this.lb_y1c1_2_safety_door_sw_open = new System.Windows.Forms.Label();
            this.pn_Loader_OUT3_Y1C1 = new System.Windows.Forms.Panel();
            this.lb_y1c0_1_safety_door_sw_open = new System.Windows.Forms.Label();
            this.pn_Loader_OUT3_Y1C0 = new System.Windows.Forms.Panel();
            this.tp_iostatus_ld_out4 = new System.Windows.Forms.TabPage();
            this.lb_y1ff_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT4_Y1FF = new System.Windows.Forms.Panel();
            this.lb_y1fe_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT4_Y1FE = new System.Windows.Forms.Panel();
            this.lb_y1fd_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT4_Y1FD = new System.Windows.Forms.Panel();
            this.lb_y1fc_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT4_Y1FC = new System.Windows.Forms.Panel();
            this.lb_y1fb_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT4_Y1FB = new System.Windows.Forms.Panel();
            this.lb_y1fa_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT4_Y1FA = new System.Windows.Forms.Panel();
            this.lb_y1f9_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT4_Y1F9 = new System.Windows.Forms.Panel();
            this.lb_y1f8_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT4_Y1F8 = new System.Windows.Forms.Panel();
            this.lb_y1f7_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT4_Y1F7 = new System.Windows.Forms.Panel();
            this.lb_y1f6_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT4_Y1F6 = new System.Windows.Forms.Panel();
            this.lb_y1f5_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT4_Y1F5 = new System.Windows.Forms.Panel();
            this.lb_y1f4_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT4_Y1F4 = new System.Windows.Forms.Panel();
            this.lb_y1f3_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT4_Y1F3 = new System.Windows.Forms.Panel();
            this.lb_y1f2_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT4_Y1F2 = new System.Windows.Forms.Panel();
            this.lb_y1f1_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT4_Y1F1 = new System.Windows.Forms.Panel();
            this.lb_y1f0_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT4_Y1F0 = new System.Windows.Forms.Panel();
            this.lb_y1ef_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT4_Y1EF = new System.Windows.Forms.Panel();
            this.lb_y1ee_load_muting_down = new System.Windows.Forms.Label();
            this.pn_Loader_OUT4_Y1EE = new System.Windows.Forms.Panel();
            this.lb_y1ed_load_muting_up = new System.Windows.Forms.Label();
            this.pn_Loader_OUT4_Y1ED = new System.Windows.Forms.Panel();
            this.lb_y1ec_spare = new System.Windows.Forms.Label();
            this.pn_Loader_OUT4_Y1EC = new System.Windows.Forms.Panel();
            this.label22 = new System.Windows.Forms.Label();
            this.pn_Loader_OUT4_Y1EB = new System.Windows.Forms.Panel();
            this.lb_y1ea_4_1_ld_lifter_muting_onoff = new System.Windows.Forms.Label();
            this.pn_Loader_OUT4_Y1EA = new System.Windows.Forms.Panel();
            this.lb_y1e9_3_2_ld_lifter_muting_onoff = new System.Windows.Forms.Label();
            this.pn_Loader_OUT4_Y1E9 = new System.Windows.Forms.Panel();
            this.lb_y1e8_3_1_ld_lifter_muting_onoff = new System.Windows.Forms.Label();
            this.pn_Loader_OUT4_Y1E8 = new System.Windows.Forms.Panel();
            this.lb_y1e7_2_2_ld_lifter_muting_onoff = new System.Windows.Forms.Label();
            this.pn_Loader_OUT4_Y1E7 = new System.Windows.Forms.Panel();
            this.lb_y1e6_2_1_ld_lifter_muting_onoff = new System.Windows.Forms.Label();
            this.pn_Loader_OUT4_Y1E6 = new System.Windows.Forms.Panel();
            this.lb_y1e5_1_2_ld_lifter_muting_onoff = new System.Windows.Forms.Label();
            this.pn_Loader_OUT4_Y1E5 = new System.Windows.Forms.Panel();
            this.lb_y1e4_1_1_ld_lifter_muting_onoff = new System.Windows.Forms.Label();
            this.pn_Loader_OUT4_Y1E4 = new System.Windows.Forms.Panel();
            this.lb_y1e3_2_2_ld_out_muting_onoff = new System.Windows.Forms.Label();
            this.pn_Loader_OUT4_Y1E3 = new System.Windows.Forms.Panel();
            this.lb_y1e2_2_1_ld_out_muting_onoff = new System.Windows.Forms.Label();
            this.pn_Loader_OUT4_Y1E2 = new System.Windows.Forms.Panel();
            this.lb_y1e1_1_2_ld_in_muting_onoff = new System.Windows.Forms.Label();
            this.pn_Loader_OUT4_Y1E1 = new System.Windows.Forms.Panel();
            this.lb_y1e0_1_1_ld_in_muting_onoff = new System.Windows.Forms.Label();
            this.pn_Loader_OUT4_Y1E0 = new System.Windows.Forms.Panel();
            this.tc_iostatus_ld_in = new System.Windows.Forms.TabControl();
            this.tp_iostatus_ld_in1 = new System.Windows.Forms.TabPage();
            this.lb_x11f_spare = new System.Windows.Forms.Label();
            this.pn_Loader_IN1_X11F = new System.Windows.Forms.Panel();
            this.lb_x11e_spare = new System.Windows.Forms.Label();
            this.pn_Loader_IN1_X11E = new System.Windows.Forms.Panel();
            this.lb_x11d_cell_trans_crush_sensor = new System.Windows.Forms.Label();
            this.pn_Loader_IN1_X11D = new System.Windows.Forms.Panel();
            this.lb_x11c_spare = new System.Windows.Forms.Label();
            this.pn_Loader_IN1_X11C = new System.Windows.Forms.Panel();
            this.lb_x11b_spare = new System.Windows.Forms.Label();
            this.pn_Loader_IN1_X11B = new System.Windows.Forms.Panel();
            this.lb_x11a_spare = new System.Windows.Forms.Label();
            this.pn_Loader_IN1_X11A = new System.Windows.Forms.Panel();
            this.lb_x119_7_safety_door_sw_open = new System.Windows.Forms.Label();
            this.pn_Loader_IN1_X119 = new System.Windows.Forms.Panel();
            this.lb_x118_6_safety_door_sw_open = new System.Windows.Forms.Label();
            this.pn_Loader_IN1_X118 = new System.Windows.Forms.Panel();
            this.lb_x117_5_safety_door_sw_open = new System.Windows.Forms.Label();
            this.pn_Loader_IN1_X117 = new System.Windows.Forms.Panel();
            this.lb_x116_4_safety_door_sw_open = new System.Windows.Forms.Label();
            this.pn_Loader_IN1_X116 = new System.Windows.Forms.Panel();
            this.lb_x115_3_safety_door_sw_open = new System.Windows.Forms.Label();
            this.pn_Loader_IN1_X115 = new System.Windows.Forms.Panel();
            this.lb_x114_2_safety_door_sw_open = new System.Windows.Forms.Label();
            this.pn_Loader_IN1_X114 = new System.Windows.Forms.Panel();
            this.lb_x113_1_safety_door_sw_open = new System.Windows.Forms.Label();
            this.pn_Loader_IN1_X113 = new System.Windows.Forms.Panel();
            this.lb_x112_3_safety_enable_grip_sw_on = new System.Windows.Forms.Label();
            this.pn_Loader_IN1_X112 = new System.Windows.Forms.Panel();
            this.lb_x111_2_safety_enable_grip_sw_on = new System.Windows.Forms.Label();
            this.pn_Loader_IN1_X111 = new System.Windows.Forms.Panel();
            this.lb_x110_1_safety_enable_grip_sw_on = new System.Windows.Forms.Label();
            this.pn_Loader_IN1_X110 = new System.Windows.Forms.Panel();
            this.lb_x10f_3_emergency_stop_enable_grip_sw = new System.Windows.Forms.Label();
            this.pn_Loader_IN1_X10F = new System.Windows.Forms.Panel();
            this.lb_x10e_2_emergency_stop_enable_grip_sw = new System.Windows.Forms.Label();
            this.pn_Loader_IN1_X10E = new System.Windows.Forms.Panel();
            this.lb_x10d_1_emergency_stop_enable_grip_sw = new System.Windows.Forms.Label();
            this.pn_Loader_IN1_X10D = new System.Windows.Forms.Panel();
            this.lb_x10c_3_emergency_stop = new System.Windows.Forms.Label();
            this.pn_Loader_IN1_X10C = new System.Windows.Forms.Panel();
            this.lb_x10b_2_emergency_stop = new System.Windows.Forms.Label();
            this.pn_Loader_IN1_X10B = new System.Windows.Forms.Panel();
            this.lb_x10a_1_emergency_stop = new System.Windows.Forms.Label();
            this.pn_Loader_IN1_X10A = new System.Windows.Forms.Panel();
            this.lb_x109_ld_lifter_muting_on = new System.Windows.Forms.Label();
            this.pn_Loader_IN1_X109 = new System.Windows.Forms.Panel();
            this.lb_x108_ld_lifter_muting_on = new System.Windows.Forms.Label();
            this.pn_Loader_IN1_X108 = new System.Windows.Forms.Panel();
            this.lb_x107_ld_lifter_muting_on = new System.Windows.Forms.Label();
            this.pn_Loader_IN1_X107 = new System.Windows.Forms.Panel();
            this.lb_x106_ld_lifter_muting_on = new System.Windows.Forms.Label();
            this.pn_Loader_IN1_X106 = new System.Windows.Forms.Panel();
            this.lb_x105_ld_out_muting_on = new System.Windows.Forms.Label();
            this.pn_Loader_IN1_X105 = new System.Windows.Forms.Panel();
            this.lb_x104_ld_in_muting_on = new System.Windows.Forms.Label();
            this.pn_Loader_IN1_X104 = new System.Windows.Forms.Panel();
            this.lb_x103_control_reset_switch = new System.Windows.Forms.Label();
            this.pn_Loader_IN1_X103 = new System.Windows.Forms.Panel();
            this.lb_x102_ld_reset_switch = new System.Windows.Forms.Label();
            this.pn_Loader_IN1_X102 = new System.Windows.Forms.Panel();
            this.lb_x101_ld_out_muting_switch_on_off = new System.Windows.Forms.Label();
            this.pn_Loader_IN1_X101 = new System.Windows.Forms.Panel();
            this.lb_ld_in_muting_switch_on_off = new System.Windows.Forms.Label();
            this.pn_Loader_IN1_X100 = new System.Windows.Forms.Panel();
            this.tp_iostatus_ld_in2 = new System.Windows.Forms.TabPage();
            this.lb_x13f_laser_exhaust_fan_run = new System.Windows.Forms.Label();
            this.pn_Loader_IN2_X13F = new System.Windows.Forms.Panel();
            this.lb_x13e_laser_inspiration_fan_run = new System.Windows.Forms.Label();
            this.pn_Loader_IN2_X13E = new System.Windows.Forms.Panel();
            this.label_x13d_laser_panel_critical_alarm = new System.Windows.Forms.Label();
            this.pn_Loader_IN2_X13D = new System.Windows.Forms.Panel();
            this.lb_x13c_ld_panel_critical_alarm = new System.Windows.Forms.Label();
            this.pn_Loader_IN2_X13C = new System.Windows.Forms.Panel();
            this.lb_x13b_2_ld_cst_d_ungrip_pos = new System.Windows.Forms.Label();
            this.pn_Loader_IN2_X13B = new System.Windows.Forms.Panel();
            this.lb_x13a_2_ld_cst_d_grip_pos = new System.Windows.Forms.Label();
            this.pn_Loader_IN2_X13A = new System.Windows.Forms.Panel();
            this.lb_x139_2_2_ld_cst_lr_ungrip_pos = new System.Windows.Forms.Label();
            this.pn_Loader_IN2_X139 = new System.Windows.Forms.Panel();
            this.lb_x138_2_2_ld_cst_lr_grip_pos = new System.Windows.Forms.Label();
            this.pn_Loader_IN2_X138 = new System.Windows.Forms.Panel();
            this.lb_x137_2_1_ld_cst_lr_ungrip_pos = new System.Windows.Forms.Label();
            this.pn_Loader_IN2_X137 = new System.Windows.Forms.Panel();
            this.lb_x136_2_1_ld_cst_lr_grip_pos = new System.Windows.Forms.Label();
            this.pn_Loader_IN2_X136 = new System.Windows.Forms.Panel();
            this.lb_x135_1_ld_cst_d_ungrip_pos = new System.Windows.Forms.Label();
            this.pn_Loader_IN2_X135 = new System.Windows.Forms.Panel();
            this.lb_x134_1_ld_cst_d_grip_pos = new System.Windows.Forms.Label();
            this.pn_Loader_IN2_X134 = new System.Windows.Forms.Panel();
            this.lb_x133_1_2_ld_cst_lr_ungrip_pos = new System.Windows.Forms.Label();
            this.pn_Loader_IN2_X133 = new System.Windows.Forms.Panel();
            this.lb_x132_1_2_ld_cst_lr_grip_pos = new System.Windows.Forms.Label();
            this.pn_Loader_IN2_X132 = new System.Windows.Forms.Panel();
            this.lb_x131_1_1_ld_cst_lr_ungrip_pos = new System.Windows.Forms.Label();
            this.pn_Loader_IN2_X131 = new System.Windows.Forms.Panel();
            this.lb_x130_1_1_ld_cst_lr_grip_pos = new System.Windows.Forms.Label();
            this.pn_Loader_IN2_X130 = new System.Windows.Forms.Panel();
            this.lb_x12f_spare = new System.Windows.Forms.Label();
            this.pn_Loader_IN2_X12F = new System.Windows.Forms.Panel();
            this.lb_x12e_2_3_ld_cst_dettect = new System.Windows.Forms.Label();
            this.pn_Loader_IN2_X1E = new System.Windows.Forms.Panel();
            this.lb_x12d_2_2_ld_cst_dettect = new System.Windows.Forms.Label();
            this.pn_Loader_IN2_X12D = new System.Windows.Forms.Panel();
            this.lb_x12c_2_1_ld_cst_dettect = new System.Windows.Forms.Label();
            this.pn_Loader_IN2_X12C = new System.Windows.Forms.Panel();
            this.lb_x12b_1_3_ld_cst_dettect = new System.Windows.Forms.Label();
            this.pn_Loader_IN2_X12B = new System.Windows.Forms.Panel();
            this.lb_x12a_1_2_ld_cst_dettect = new System.Windows.Forms.Label();
            this.pn_Loader_IN2_X12A = new System.Windows.Forms.Panel();
            this.lb_x129_1_1_ld_cst_dettect = new System.Windows.Forms.Label();
            this.pn_Loader_IN2_X129 = new System.Windows.Forms.Panel();
            this.lb_x128_control_select_ld = new System.Windows.Forms.Label();
            this.pn_Loader_IN2_X128 = new System.Windows.Forms.Panel();
            this.lb_x127_spare = new System.Windows.Forms.Label();
            this.pn_Loader_IN2_X127 = new System.Windows.Forms.Panel();
            this.lb_x126_spare = new System.Windows.Forms.Label();
            this.pn_Loader_IN2_X126 = new System.Windows.Forms.Panel();
            this.lb_x125_spare = new System.Windows.Forms.Label();
            this.pn_Loader_IN2_X125 = new System.Windows.Forms.Panel();
            this.lb_x124_efu_alarm = new System.Windows.Forms.Label();
            this.pn_Loader_IN2_X124 = new System.Windows.Forms.Panel();
            this.lb_x123_laser_mc_on = new System.Windows.Forms.Label();
            this.pn_Loader_IN2_X123 = new System.Windows.Forms.Panel();
            this.lb_x122_ld_mc_on = new System.Windows.Forms.Label();
            this.pn_Loader_IN2_X122 = new System.Windows.Forms.Panel();
            this.lb_x121_mode_select_sw_teach = new System.Windows.Forms.Label();
            this.pn_Loader_IN2_X121 = new System.Windows.Forms.Panel();
            this.lb_x120_mode_select_sw_auto = new System.Windows.Forms.Label();
            this.pn_Loader_IN2_X120 = new System.Windows.Forms.Panel();
            this.tp_iostatus_ld_in3 = new System.Windows.Forms.TabPage();
            this.lb_x15f_spare = new System.Windows.Forms.Label();
            this.pn_Loader_IN3_X15F = new System.Windows.Forms.Panel();
            this.lb_x15e_6_laser_fan_stop_alarm = new System.Windows.Forms.Label();
            this.pn_Loader_IN3_X15E = new System.Windows.Forms.Panel();
            this.lb_x15d_2_ld_trans_down = new System.Windows.Forms.Label();
            this.pn_Loader_IN3_X15D = new System.Windows.Forms.Panel();
            this.lb_x15c_2_ld_trans_up = new System.Windows.Forms.Label();
            this.pn_Loader_IN3_X15C = new System.Windows.Forms.Panel();
            this.lb_x15b_1_ld_trans_down = new System.Windows.Forms.Label();
            this.pn_Loader_IN3_X15B = new System.Windows.Forms.Panel();
            this.lb_x15a_1_ld_trans_up = new System.Windows.Forms.Label();
            this.pn_Loader_IN3_X15A = new System.Windows.Forms.Panel();
            this.lb_x159_2_ld_cst_lift_tiltsensor_down = new System.Windows.Forms.Label();
            this.pn_Loader_IN3_X159 = new System.Windows.Forms.Panel();
            this.lb_x158_2_ld_cst_lift_tiltsensor_up = new System.Windows.Forms.Label();
            this.pn_Loader_IN3_X158 = new System.Windows.Forms.Panel();
            this.lb_x157_2_cst_trans_pressure_sw = new System.Windows.Forms.Label();
            this.pn_Loader_IN3_X157 = new System.Windows.Forms.Panel();
            this.lb_x156_1_cst_trans_pressure_sw = new System.Windows.Forms.Label();
            this.pn_Loader_IN3_X156 = new System.Windows.Forms.Panel();
            this.lb_x155_1_ld_cst_lift_tiltsensor_down = new System.Windows.Forms.Label();
            this.pn_Loader_IN3_X155 = new System.Windows.Forms.Panel();
            this.lb_x154_1_ld_cst_lift_tiltsensor_up = new System.Windows.Forms.Label();
            this.pn_Loader_IN3_X154 = new System.Windows.Forms.Panel();
            this.lb_x153_2_2_ld_cst_lifter_ungrip_pos = new System.Windows.Forms.Label();
            this.pn_Loader_IN3_X153 = new System.Windows.Forms.Panel();
            this.lb_x152_2_2_ld_cst_lifter_grip_pos = new System.Windows.Forms.Label();
            this.pn_Loader_IN3_X152 = new System.Windows.Forms.Panel();
            this.lb_x151_2_1_ld_cst_lifter_ungrip_pos = new System.Windows.Forms.Label();
            this.pn_Loader_IN3_X151 = new System.Windows.Forms.Panel();
            this.lb_x150_2_1_ld_cst_lifter_grip_pos = new System.Windows.Forms.Label();
            this.pn_Loader_IN3_X150 = new System.Windows.Forms.Panel();
            this.lb_x14f_1_2_ld_cst_lifter_ungrip_pos = new System.Windows.Forms.Label();
            this.pn_Loader_IN3_X14F = new System.Windows.Forms.Panel();
            this.lb_x14e_1_2_ld_cst_lifter_grip_pos = new System.Windows.Forms.Label();
            this.pn_Loader_IN3_X14E = new System.Windows.Forms.Panel();
            this.lb_x14d_1_1_ld_cst_lifter_ungrip_pos = new System.Windows.Forms.Label();
            this.pn_Loader_IN3_X14D = new System.Windows.Forms.Panel();
            this.lb_x14c_1_1_ld_cst_lifter_grip_pos = new System.Windows.Forms.Label();
            this.pn_Loader_IN3_X14C = new System.Windows.Forms.Panel();
            this.lb_x14b_4_ld_cst_d_ungrip_pos = new System.Windows.Forms.Label();
            this.pn_Loader_IN3_X14B = new System.Windows.Forms.Panel();
            this.lb_x14a_4_ld_cst_d_grip_pos = new System.Windows.Forms.Label();
            this.pn_Loader_IN3_X14A = new System.Windows.Forms.Panel();
            this.lb_x149_4_2_ld_cst_lr_ungrip_pos = new System.Windows.Forms.Label();
            this.pn_Loader_IN3_X149 = new System.Windows.Forms.Panel();
            this.lb_x148_4_2_ld_cst_lr_grip_pos = new System.Windows.Forms.Label();
            this.pn_Loader_IN3_X148 = new System.Windows.Forms.Panel();
            this.lb_x147_4_1_ld_cst_lr_ungrip_pos = new System.Windows.Forms.Label();
            this.pn_Loader_IN3_X147 = new System.Windows.Forms.Panel();
            this.lb_x146_4_1_ld_cst_lr_grip_pos = new System.Windows.Forms.Label();
            this.pn_Loader_IN3_X146 = new System.Windows.Forms.Panel();
            this.lb_x145_3_ld_cst_d_ungrip_pos = new System.Windows.Forms.Label();
            this.pn_Loader_IN3_X145 = new System.Windows.Forms.Panel();
            this.lb_x144_3_ld_cst_d_grip_pos = new System.Windows.Forms.Label();
            this.pn_Loader_IN3_X144 = new System.Windows.Forms.Panel();
            this.lb_x143_3_2_ld_cst_lr_ungrip_pos = new System.Windows.Forms.Label();
            this.pn_Loader_IN3_X143 = new System.Windows.Forms.Panel();
            this.lb_x142_3_2_ld_cst_lr_grip_pos = new System.Windows.Forms.Label();
            this.pn_Loader_IN3_X142 = new System.Windows.Forms.Panel();
            this.lb_x141_3_1_ld_cst_lr_ungrip_pos = new System.Windows.Forms.Label();
            this.pn_Loader_IN3_X141 = new System.Windows.Forms.Panel();
            this.lb_x140_3_1_ld_cst_lr_grip_pos = new System.Windows.Forms.Label();
            this.pn_Loader_IN3_X140 = new System.Windows.Forms.Panel();
            this.tp_iostatus_ld_in4 = new System.Windows.Forms.TabPage();
            this.lb_x17f_5_process_fan_stop_alarm = new System.Windows.Forms.Label();
            this.lb_x16f_1_ld_light_curtain = new System.Windows.Forms.Label();
            this.pn_Loader_IN4_X17F = new System.Windows.Forms.Panel();
            this.lb_x17e_4_process_fan_stop_alarm = new System.Windows.Forms.Label();
            this.pn_Loader_IN4_X17E = new System.Windows.Forms.Panel();
            this.lb_x17d_3_process_fan_stop_alarm = new System.Windows.Forms.Label();
            this.pn_Loader_IN4_X17D = new System.Windows.Forms.Panel();
            this.lb_x17c_2_process_fan_stop_alarm = new System.Windows.Forms.Label();
            this.pn_Loader_IN4_X17C = new System.Windows.Forms.Panel();
            this.lb_x17b_1_process_fan_stop_alarm = new System.Windows.Forms.Label();
            this.pn_Loader_IN4_X17B = new System.Windows.Forms.Panel();
            this.lb_x17a_laserhead_blow = new System.Windows.Forms.Label();
            this.pn_Loader_IN4_X17A = new System.Windows.Forms.Panel();
            this.lb_x179_ld_exhaust_fan_run = new System.Windows.Forms.Label();
            this.pn_Loader_IN4_X179 = new System.Windows.Forms.Panel();
            this.lb_x178_ld_inspiration_fan_run = new System.Windows.Forms.Label();
            this.pn_Loader_IN4_X178 = new System.Windows.Forms.Panel();
            this.lb_x177_6_ld_fan_stop_alarm = new System.Windows.Forms.Label();
            this.pn_Loader_IN4_X177 = new System.Windows.Forms.Panel();
            this.lb_x176_5_ld_fan_stop_alarm = new System.Windows.Forms.Label();
            this.pn_Loader_IN4_X176 = new System.Windows.Forms.Panel();
            this.lb_x175_4_ld_fan_stop_alarm = new System.Windows.Forms.Label();
            this.pn_Loader_IN4_X175 = new System.Windows.Forms.Panel();
            this.lb_x174_3_ld_fan_stop_alarm = new System.Windows.Forms.Label();
            this.pn_Loader_IN4_X174 = new System.Windows.Forms.Panel();
            this.lb_x173_2_ld_fan_stop_alarm = new System.Windows.Forms.Label();
            this.pn_Loader_IN4_X173 = new System.Windows.Forms.Panel();
            this.lb_x172_1_ld_fan_stop_alarm = new System.Windows.Forms.Label();
            this.pn_Loader_IN4_X172 = new System.Windows.Forms.Panel();
            this.lb_x171_motion_off_timer = new System.Windows.Forms.Label();
            this.pn_Loader_IN4_X171 = new System.Windows.Forms.Panel();
            this.lb_x170_2_ld_light_curtain = new System.Windows.Forms.Label();
            this.pn_Loader_IN4_X170 = new System.Windows.Forms.Panel();
            this.pn_Loader_IN4_X16F = new System.Windows.Forms.Panel();
            this.lb_x16e_spare_cylinder_sensor = new System.Windows.Forms.Label();
            this.pn_Loader_IN4_X16E = new System.Windows.Forms.Panel();
            this.lb_x16d_spare_cylinder_sensor = new System.Windows.Forms.Label();
            this.pn_Loader_IN4_X16D = new System.Windows.Forms.Panel();
            this.lb_x16c_spare_cylinder_sensor = new System.Windows.Forms.Label();
            this.pn_Loader_IN4_X16C = new System.Windows.Forms.Panel();
            this.lb_x16b_spare_cylinder_sensor = new System.Windows.Forms.Label();
            this.pn_Loader_IN4_X16B = new System.Windows.Forms.Panel();
            this.lb_x16a_spare_cylinder_sensor = new System.Windows.Forms.Label();
            this.pn_Loader_IN4_X16A = new System.Windows.Forms.Panel();
            this.lb_x169_spare_cylinder_sensor = new System.Windows.Forms.Label();
            this.pn_Loader_IN4_X169 = new System.Windows.Forms.Panel();
            this.lb_x168_spare_cylinder_sensor = new System.Windows.Forms.Label();
            this.pn_Loader_IN4_X168 = new System.Windows.Forms.Panel();
            this.lb_x167_spare_cylinder_sensor = new System.Windows.Forms.Label();
            this.pn_Loader_IN4_X167 = new System.Windows.Forms.Panel();
            this.lb_x166_3_main_pressure_sw = new System.Windows.Forms.Label();
            this.pn_Loader_IN4_X166 = new System.Windows.Forms.Panel();
            this.lb_x165_2_main_pressure_sw = new System.Windows.Forms.Label();
            this.pn_Loader_IN4_X165 = new System.Windows.Forms.Panel();
            this.lb_x164_1_main_pressure_sw = new System.Windows.Forms.Label();
            this.pn_Loader_IN4_X164 = new System.Windows.Forms.Panel();
            this.lb_x163_2_ld_trans_2ch_pressure_sw = new System.Windows.Forms.Label();
            this.pn_Loader_IN4_X163 = new System.Windows.Forms.Panel();
            this.lb_x162_2_ld_trans_1ch_pressure_sw = new System.Windows.Forms.Label();
            this.pn_Loader_IN4_X162 = new System.Windows.Forms.Panel();
            this.lb_x161_1_ld_trans_2ch_pressure_sw = new System.Windows.Forms.Label();
            this.pn_Loader_IN4_X161 = new System.Windows.Forms.Panel();
            this.lb_x160_1_ld_trans_1ch_pressure_sw = new System.Windows.Forms.Label();
            this.pn_Loader_IN4_X160 = new System.Windows.Forms.Panel();
            this.tp_iostatus_process = new System.Windows.Forms.TabPage();
            this.tc_iostatus_process_out = new System.Windows.Forms.TabControl();
            this.tp_iostatus_process_out1 = new System.Windows.Forms.TabPage();
            this.label131 = new System.Windows.Forms.Label();
            this.panel131 = new System.Windows.Forms.Panel();
            this.label132 = new System.Windows.Forms.Label();
            this.panel132 = new System.Windows.Forms.Panel();
            this.label133 = new System.Windows.Forms.Label();
            this.panel133 = new System.Windows.Forms.Panel();
            this.label134 = new System.Windows.Forms.Label();
            this.panel134 = new System.Windows.Forms.Panel();
            this.label135 = new System.Windows.Forms.Label();
            this.panel135 = new System.Windows.Forms.Panel();
            this.label136 = new System.Windows.Forms.Label();
            this.panel136 = new System.Windows.Forms.Panel();
            this.label137 = new System.Windows.Forms.Label();
            this.panel137 = new System.Windows.Forms.Panel();
            this.label138 = new System.Windows.Forms.Label();
            this.panel138 = new System.Windows.Forms.Panel();
            this.label139 = new System.Windows.Forms.Label();
            this.panel139 = new System.Windows.Forms.Panel();
            this.label140 = new System.Windows.Forms.Label();
            this.panel140 = new System.Windows.Forms.Panel();
            this.label141 = new System.Windows.Forms.Label();
            this.panel141 = new System.Windows.Forms.Panel();
            this.label142 = new System.Windows.Forms.Label();
            this.panel142 = new System.Windows.Forms.Panel();
            this.label143 = new System.Windows.Forms.Label();
            this.panel143 = new System.Windows.Forms.Panel();
            this.label144 = new System.Windows.Forms.Label();
            this.panel144 = new System.Windows.Forms.Panel();
            this.label145 = new System.Windows.Forms.Label();
            this.panel145 = new System.Windows.Forms.Panel();
            this.label146 = new System.Windows.Forms.Label();
            this.panel146 = new System.Windows.Forms.Panel();
            this.label147 = new System.Windows.Forms.Label();
            this.panel147 = new System.Windows.Forms.Panel();
            this.label148 = new System.Windows.Forms.Label();
            this.panel148 = new System.Windows.Forms.Panel();
            this.label149 = new System.Windows.Forms.Label();
            this.panel149 = new System.Windows.Forms.Panel();
            this.label150 = new System.Windows.Forms.Label();
            this.panel150 = new System.Windows.Forms.Panel();
            this.label151 = new System.Windows.Forms.Label();
            this.panel151 = new System.Windows.Forms.Panel();
            this.label152 = new System.Windows.Forms.Label();
            this.panel152 = new System.Windows.Forms.Panel();
            this.label153 = new System.Windows.Forms.Label();
            this.panel153 = new System.Windows.Forms.Panel();
            this.label154 = new System.Windows.Forms.Label();
            this.panel154 = new System.Windows.Forms.Panel();
            this.label155 = new System.Windows.Forms.Label();
            this.panel155 = new System.Windows.Forms.Panel();
            this.label156 = new System.Windows.Forms.Label();
            this.panel156 = new System.Windows.Forms.Panel();
            this.label157 = new System.Windows.Forms.Label();
            this.panel157 = new System.Windows.Forms.Panel();
            this.label158 = new System.Windows.Forms.Label();
            this.panel158 = new System.Windows.Forms.Panel();
            this.label159 = new System.Windows.Forms.Label();
            this.panel159 = new System.Windows.Forms.Panel();
            this.lb_y322_laser_emergency_stop = new System.Windows.Forms.Label();
            this.panel_y322_laser_emergency_stop = new System.Windows.Forms.Panel();
            this.lb_y321_laser_stop_external_interlock = new System.Windows.Forms.Label();
            this.panel_y321_laser_stop_external_interlock = new System.Windows.Forms.Panel();
            this.lb_y320_laser_stop_internal_interlock = new System.Windows.Forms.Label();
            this.panel_y320_laser_stop_internal_interlock = new System.Windows.Forms.Panel();
            this.tp_iostatus_process_out2 = new System.Windows.Forms.TabPage();
            this.label163 = new System.Windows.Forms.Label();
            this.panel163 = new System.Windows.Forms.Panel();
            this.label164 = new System.Windows.Forms.Label();
            this.panel164 = new System.Windows.Forms.Panel();
            this.label165 = new System.Windows.Forms.Label();
            this.panel165 = new System.Windows.Forms.Panel();
            this.label166 = new System.Windows.Forms.Label();
            this.panel166 = new System.Windows.Forms.Panel();
            this.label167 = new System.Windows.Forms.Label();
            this.panel167 = new System.Windows.Forms.Panel();
            this.label168 = new System.Windows.Forms.Label();
            this.panel168 = new System.Windows.Forms.Panel();
            this.label169 = new System.Windows.Forms.Label();
            this.panel169 = new System.Windows.Forms.Panel();
            this.label170 = new System.Windows.Forms.Label();
            this.panel170 = new System.Windows.Forms.Panel();
            this.label171 = new System.Windows.Forms.Label();
            this.panel171 = new System.Windows.Forms.Panel();
            this.label172 = new System.Windows.Forms.Label();
            this.panel172 = new System.Windows.Forms.Panel();
            this.label173 = new System.Windows.Forms.Label();
            this.panel173 = new System.Windows.Forms.Panel();
            this.label174 = new System.Windows.Forms.Label();
            this.panel174 = new System.Windows.Forms.Panel();
            this.label175 = new System.Windows.Forms.Label();
            this.panel175 = new System.Windows.Forms.Panel();
            this.label176 = new System.Windows.Forms.Label();
            this.panel176 = new System.Windows.Forms.Panel();
            this.label177 = new System.Windows.Forms.Label();
            this.panel177 = new System.Windows.Forms.Panel();
            this.label178 = new System.Windows.Forms.Label();
            this.panel178 = new System.Windows.Forms.Panel();
            this.label179 = new System.Windows.Forms.Label();
            this.panel179 = new System.Windows.Forms.Panel();
            this.label180 = new System.Windows.Forms.Label();
            this.panel180 = new System.Windows.Forms.Panel();
            this.label181 = new System.Windows.Forms.Label();
            this.panel181 = new System.Windows.Forms.Panel();
            this.label182 = new System.Windows.Forms.Label();
            this.panel182 = new System.Windows.Forms.Panel();
            this.label183 = new System.Windows.Forms.Label();
            this.panel183 = new System.Windows.Forms.Panel();
            this.label184 = new System.Windows.Forms.Label();
            this.panel184 = new System.Windows.Forms.Panel();
            this.label185 = new System.Windows.Forms.Label();
            this.panel185 = new System.Windows.Forms.Panel();
            this.label186 = new System.Windows.Forms.Label();
            this.panel186 = new System.Windows.Forms.Panel();
            this.label187 = new System.Windows.Forms.Label();
            this.panel187 = new System.Windows.Forms.Panel();
            this.label188 = new System.Windows.Forms.Label();
            this.panel188 = new System.Windows.Forms.Panel();
            this.label189 = new System.Windows.Forms.Label();
            this.panel189 = new System.Windows.Forms.Panel();
            this.label190 = new System.Windows.Forms.Label();
            this.panel190 = new System.Windows.Forms.Panel();
            this.label191 = new System.Windows.Forms.Label();
            this.panel191 = new System.Windows.Forms.Panel();
            this.label192 = new System.Windows.Forms.Label();
            this.panel192 = new System.Windows.Forms.Panel();
            this.label193 = new System.Windows.Forms.Label();
            this.panel193 = new System.Windows.Forms.Panel();
            this.label194 = new System.Windows.Forms.Label();
            this.panel194 = new System.Windows.Forms.Panel();
            this.tp_iostatus_process_out3 = new System.Windows.Forms.TabPage();
            this.label195 = new System.Windows.Forms.Label();
            this.panel195 = new System.Windows.Forms.Panel();
            this.label196 = new System.Windows.Forms.Label();
            this.panel196 = new System.Windows.Forms.Panel();
            this.label197 = new System.Windows.Forms.Label();
            this.panel197 = new System.Windows.Forms.Panel();
            this.label198 = new System.Windows.Forms.Label();
            this.panel198 = new System.Windows.Forms.Panel();
            this.label199 = new System.Windows.Forms.Label();
            this.panel199 = new System.Windows.Forms.Panel();
            this.label200 = new System.Windows.Forms.Label();
            this.panel200 = new System.Windows.Forms.Panel();
            this.label201 = new System.Windows.Forms.Label();
            this.panel201 = new System.Windows.Forms.Panel();
            this.label202 = new System.Windows.Forms.Label();
            this.panel202 = new System.Windows.Forms.Panel();
            this.label203 = new System.Windows.Forms.Label();
            this.panel203 = new System.Windows.Forms.Panel();
            this.label204 = new System.Windows.Forms.Label();
            this.panel204 = new System.Windows.Forms.Panel();
            this.label205 = new System.Windows.Forms.Label();
            this.panel205 = new System.Windows.Forms.Panel();
            this.label206 = new System.Windows.Forms.Label();
            this.panel206 = new System.Windows.Forms.Panel();
            this.label207 = new System.Windows.Forms.Label();
            this.panel207 = new System.Windows.Forms.Panel();
            this.label208 = new System.Windows.Forms.Label();
            this.panel208 = new System.Windows.Forms.Panel();
            this.label209 = new System.Windows.Forms.Label();
            this.panel209 = new System.Windows.Forms.Panel();
            this.label210 = new System.Windows.Forms.Label();
            this.panel210 = new System.Windows.Forms.Panel();
            this.label211 = new System.Windows.Forms.Label();
            this.panel211 = new System.Windows.Forms.Panel();
            this.label212 = new System.Windows.Forms.Label();
            this.panel212 = new System.Windows.Forms.Panel();
            this.label213 = new System.Windows.Forms.Label();
            this.panel213 = new System.Windows.Forms.Panel();
            this.label214 = new System.Windows.Forms.Label();
            this.panel214 = new System.Windows.Forms.Panel();
            this.label215 = new System.Windows.Forms.Label();
            this.panel215 = new System.Windows.Forms.Panel();
            this.label216 = new System.Windows.Forms.Label();
            this.panel216 = new System.Windows.Forms.Panel();
            this.label217 = new System.Windows.Forms.Label();
            this.panel217 = new System.Windows.Forms.Panel();
            this.label218 = new System.Windows.Forms.Label();
            this.panel218 = new System.Windows.Forms.Panel();
            this.label219 = new System.Windows.Forms.Label();
            this.panel219 = new System.Windows.Forms.Panel();
            this.label220 = new System.Windows.Forms.Label();
            this.panel220 = new System.Windows.Forms.Panel();
            this.label221 = new System.Windows.Forms.Label();
            this.panel221 = new System.Windows.Forms.Panel();
            this.label222 = new System.Windows.Forms.Label();
            this.panel222 = new System.Windows.Forms.Panel();
            this.label223 = new System.Windows.Forms.Label();
            this.panel223 = new System.Windows.Forms.Panel();
            this.label224 = new System.Windows.Forms.Label();
            this.panel224 = new System.Windows.Forms.Panel();
            this.label225 = new System.Windows.Forms.Label();
            this.panel225 = new System.Windows.Forms.Panel();
            this.label226 = new System.Windows.Forms.Label();
            this.panel226 = new System.Windows.Forms.Panel();
            this.tc_iostatus_process_in = new System.Windows.Forms.TabControl();
            this.tp_iostatus_process_in1 = new System.Windows.Forms.TabPage();
            this.lb_x31f_1_breaking_stage_brush_down = new System.Windows.Forms.Label();
            this.pn_Process_IN1_X31F = new System.Windows.Forms.Panel();
            this.lb_x31e_2_breaking_stage_brush_up = new System.Windows.Forms.Label();
            this.pn_Process_IN1_X31E = new System.Windows.Forms.Panel();
            this.lb_x31d_1_breaking_stage_brush_down = new System.Windows.Forms.Label();
            this.pn_Process_IN1_X31D = new System.Windows.Forms.Panel();
            this.lb_x31c_1_breaking_stage_brush_up = new System.Windows.Forms.Label();
            this.pn_Process_IN1_X31C = new System.Windows.Forms.Panel();
            this.lb_x31b_2_after_ircut_trans_down = new System.Windows.Forms.Label();
            this.pn_Process_IN1_X31B = new System.Windows.Forms.Panel();
            this.lb_x31a_2_after_ircut_trans_up = new System.Windows.Forms.Label();
            this.pn_Process_IN1_X31A = new System.Windows.Forms.Panel();
            this.lb_x319_1_after_ircut_trans_down = new System.Windows.Forms.Label();
            this.pn_Process_IN1_X319 = new System.Windows.Forms.Panel();
            this.lb_x318_1_after_ircut_trans_up = new System.Windows.Forms.Label();
            this.pn_Process_IN1_X318 = new System.Windows.Forms.Panel();
            this.lb_x317_2_laser_stage_brush_down = new System.Windows.Forms.Label();
            this.pn_Process_IN1_X317 = new System.Windows.Forms.Panel();
            this.lb_x316_2_laser_stage_brush_up = new System.Windows.Forms.Label();
            this.pn_Process_IN1_X316 = new System.Windows.Forms.Panel();
            this.lb_x315_1_laser_stage_brush_down = new System.Windows.Forms.Label();
            this.pn_Process_IN1_X315 = new System.Windows.Forms.Panel();
            this.lb_x314_1_laser_stage_brush_up = new System.Windows.Forms.Label();
            this.pn_Process_IN1_X314 = new System.Windows.Forms.Panel();
            this.lb_x313_laser_shutter_close = new System.Windows.Forms.Label();
            this.pn_Process_IN1_X313 = new System.Windows.Forms.Panel();
            this.lb_x312_laser_shutter_open = new System.Windows.Forms.Label();
            this.pn_Process_IN1_X312 = new System.Windows.Forms.Panel();
            this.lb_x311_2_2_breaking_stage_cell_detect = new System.Windows.Forms.Label();
            this.pn_Process_IN1_X311 = new System.Windows.Forms.Panel();
            this.lb_x310_2_1_breaking_stage_cell_detect = new System.Windows.Forms.Label();
            this.pn_Process_IN1_X310 = new System.Windows.Forms.Panel();
            this.lb_x30f_1_2_breaking_stage_cell_detect = new System.Windows.Forms.Label();
            this.pn_Process_IN1_X30F = new System.Windows.Forms.Panel();
            this.lb_x30e_1_1_breaking_stage_cell_detect = new System.Windows.Forms.Label();
            this.pn_Process_IN1_X30E = new System.Windows.Forms.Panel();
            this.lb_x30d_2_2_laser_stage_cell_detect = new System.Windows.Forms.Label();
            this.pn_Process_IN1_X30D = new System.Windows.Forms.Panel();
            this.lb_x30c_2_1_laser_stage_cell_detect = new System.Windows.Forms.Label();
            this.pn_Process_IN1_X30C = new System.Windows.Forms.Panel();
            this.lb_x30b_1_2_laser_stage_cell_detect = new System.Windows.Forms.Label();
            this.pn_Process_IN1_X30B = new System.Windows.Forms.Panel();
            this.lb_x30a_1_1_laser_stage_cell_detect = new System.Windows.Forms.Label();
            this.pn_Process_IN1_X30A = new System.Windows.Forms.Panel();
            this.lb_x309_leak_sensor = new System.Windows.Forms.Label();
            this.pn_Process_IN1_X309 = new System.Windows.Forms.Panel();
            this.lb_x308_laser_cover_interlock = new System.Windows.Forms.Label();
            this.pn_Process_IN1_X308 = new System.Windows.Forms.Panel();
            this.lb_x307_laser_diodes_onoff_2 = new System.Windows.Forms.Label();
            this.pn_Process_IN1_X307 = new System.Windows.Forms.Panel();
            this.lb_x306_laser_interlock_error_2 = new System.Windows.Forms.Label();
            this.pn_Process_IN1_X306 = new System.Windows.Forms.Panel();
            this.lb_x305_laser_shutter_position_2 = new System.Windows.Forms.Label();
            this.pn_Process_IN1_X305 = new System.Windows.Forms.Panel();
            this.lb_x304_laser_interlock_error_1 = new System.Windows.Forms.Label();
            this.pn_Process_IN1_X304 = new System.Windows.Forms.Panel();
            this.lb__x303_laser_shutter_position_1 = new System.Windows.Forms.Label();
            this.pn_Process_IN1_X303 = new System.Windows.Forms.Panel();
            this.lb_x302_laser_diodes_onoff_1 = new System.Windows.Forms.Label();
            this.pn_Process_IN1_X302 = new System.Windows.Forms.Panel();
            this.lb_x301_laser_on_feedback = new System.Windows.Forms.Label();
            this.pn_Process_IN1_X301 = new System.Windows.Forms.Panel();
            this.lb_x300_laser_alarm = new System.Windows.Forms.Label();
            this.pn_Process_IN1_X300 = new System.Windows.Forms.Panel();
            this.tp_iostatus_process_in2 = new System.Windows.Forms.TabPage();
            this.label35 = new System.Windows.Forms.Label();
            this.panel35 = new System.Windows.Forms.Panel();
            this.label36 = new System.Windows.Forms.Label();
            this.panel36 = new System.Windows.Forms.Panel();
            this.label37 = new System.Windows.Forms.Label();
            this.panel37 = new System.Windows.Forms.Panel();
            this.label38 = new System.Windows.Forms.Label();
            this.panel38 = new System.Windows.Forms.Panel();
            this.label39 = new System.Windows.Forms.Label();
            this.panel39 = new System.Windows.Forms.Panel();
            this.label40 = new System.Windows.Forms.Label();
            this.panel40 = new System.Windows.Forms.Panel();
            this.label41 = new System.Windows.Forms.Label();
            this.panel41 = new System.Windows.Forms.Panel();
            this.label42 = new System.Windows.Forms.Label();
            this.panel42 = new System.Windows.Forms.Panel();
            this.label43 = new System.Windows.Forms.Label();
            this.panel43 = new System.Windows.Forms.Panel();
            this.label44 = new System.Windows.Forms.Label();
            this.panel44 = new System.Windows.Forms.Panel();
            this.label45 = new System.Windows.Forms.Label();
            this.panel45 = new System.Windows.Forms.Panel();
            this.label46 = new System.Windows.Forms.Label();
            this.panel46 = new System.Windows.Forms.Panel();
            this.label47 = new System.Windows.Forms.Label();
            this.panel47 = new System.Windows.Forms.Panel();
            this.label48 = new System.Windows.Forms.Label();
            this.panel48 = new System.Windows.Forms.Panel();
            this.label49 = new System.Windows.Forms.Label();
            this.panel49 = new System.Windows.Forms.Panel();
            this.label50 = new System.Windows.Forms.Label();
            this.panel50 = new System.Windows.Forms.Panel();
            this.label51 = new System.Windows.Forms.Label();
            this.panel51 = new System.Windows.Forms.Panel();
            this.label52 = new System.Windows.Forms.Label();
            this.panel52 = new System.Windows.Forms.Panel();
            this.label53 = new System.Windows.Forms.Label();
            this.panel53 = new System.Windows.Forms.Panel();
            this.label54 = new System.Windows.Forms.Label();
            this.panel54 = new System.Windows.Forms.Panel();
            this.label55 = new System.Windows.Forms.Label();
            this.panel55 = new System.Windows.Forms.Panel();
            this.label56 = new System.Windows.Forms.Label();
            this.panel56 = new System.Windows.Forms.Panel();
            this.label57 = new System.Windows.Forms.Label();
            this.panel57 = new System.Windows.Forms.Panel();
            this.label58 = new System.Windows.Forms.Label();
            this.panel58 = new System.Windows.Forms.Panel();
            this.label59 = new System.Windows.Forms.Label();
            this.panel59 = new System.Windows.Forms.Panel();
            this.label60 = new System.Windows.Forms.Label();
            this.panel60 = new System.Windows.Forms.Panel();
            this.label61 = new System.Windows.Forms.Label();
            this.panel61 = new System.Windows.Forms.Panel();
            this.label62 = new System.Windows.Forms.Label();
            this.panel62 = new System.Windows.Forms.Panel();
            this.label63 = new System.Windows.Forms.Label();
            this.panel63 = new System.Windows.Forms.Panel();
            this.label64 = new System.Windows.Forms.Label();
            this.panel64 = new System.Windows.Forms.Panel();
            this.label65 = new System.Windows.Forms.Label();
            this.panel65 = new System.Windows.Forms.Panel();
            this.label66 = new System.Windows.Forms.Label();
            this.panel66 = new System.Windows.Forms.Panel();
            this.tp_iostatus_process_in3 = new System.Windows.Forms.TabPage();
            this.label67 = new System.Windows.Forms.Label();
            this.panel67 = new System.Windows.Forms.Panel();
            this.label68 = new System.Windows.Forms.Label();
            this.panel68 = new System.Windows.Forms.Panel();
            this.label69 = new System.Windows.Forms.Label();
            this.panel69 = new System.Windows.Forms.Panel();
            this.label70 = new System.Windows.Forms.Label();
            this.panel70 = new System.Windows.Forms.Panel();
            this.label71 = new System.Windows.Forms.Label();
            this.panel71 = new System.Windows.Forms.Panel();
            this.label72 = new System.Windows.Forms.Label();
            this.panel72 = new System.Windows.Forms.Panel();
            this.label73 = new System.Windows.Forms.Label();
            this.panel73 = new System.Windows.Forms.Panel();
            this.label74 = new System.Windows.Forms.Label();
            this.panel74 = new System.Windows.Forms.Panel();
            this.label75 = new System.Windows.Forms.Label();
            this.panel75 = new System.Windows.Forms.Panel();
            this.label76 = new System.Windows.Forms.Label();
            this.panel76 = new System.Windows.Forms.Panel();
            this.label77 = new System.Windows.Forms.Label();
            this.panel77 = new System.Windows.Forms.Panel();
            this.label78 = new System.Windows.Forms.Label();
            this.panel78 = new System.Windows.Forms.Panel();
            this.label79 = new System.Windows.Forms.Label();
            this.panel79 = new System.Windows.Forms.Panel();
            this.label80 = new System.Windows.Forms.Label();
            this.panel80 = new System.Windows.Forms.Panel();
            this.label81 = new System.Windows.Forms.Label();
            this.panel81 = new System.Windows.Forms.Panel();
            this.label82 = new System.Windows.Forms.Label();
            this.panel82 = new System.Windows.Forms.Panel();
            this.label83 = new System.Windows.Forms.Label();
            this.panel83 = new System.Windows.Forms.Panel();
            this.label84 = new System.Windows.Forms.Label();
            this.panel84 = new System.Windows.Forms.Panel();
            this.label85 = new System.Windows.Forms.Label();
            this.panel85 = new System.Windows.Forms.Panel();
            this.label86 = new System.Windows.Forms.Label();
            this.panel86 = new System.Windows.Forms.Panel();
            this.label87 = new System.Windows.Forms.Label();
            this.panel87 = new System.Windows.Forms.Panel();
            this.label88 = new System.Windows.Forms.Label();
            this.panel88 = new System.Windows.Forms.Panel();
            this.label89 = new System.Windows.Forms.Label();
            this.panel89 = new System.Windows.Forms.Panel();
            this.label90 = new System.Windows.Forms.Label();
            this.panel90 = new System.Windows.Forms.Panel();
            this.label91 = new System.Windows.Forms.Label();
            this.panel91 = new System.Windows.Forms.Panel();
            this.label92 = new System.Windows.Forms.Label();
            this.panel92 = new System.Windows.Forms.Panel();
            this.label93 = new System.Windows.Forms.Label();
            this.panel93 = new System.Windows.Forms.Panel();
            this.label94 = new System.Windows.Forms.Label();
            this.panel94 = new System.Windows.Forms.Panel();
            this.label95 = new System.Windows.Forms.Label();
            this.panel95 = new System.Windows.Forms.Panel();
            this.label96 = new System.Windows.Forms.Label();
            this.panel96 = new System.Windows.Forms.Panel();
            this.label97 = new System.Windows.Forms.Label();
            this.panel97 = new System.Windows.Forms.Panel();
            this.label98 = new System.Windows.Forms.Label();
            this.panel98 = new System.Windows.Forms.Panel();
            this.tp_iostatus_uld = new System.Windows.Forms.TabPage();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.inOutTotalControl1 = new DIT.TLC.UI.Moter_In_Out.InOutTotalControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.label256 = new System.Windows.Forms.Label();
            this.panel257 = new System.Windows.Forms.Panel();
            this.label257 = new System.Windows.Forms.Label();
            this.panel258 = new System.Windows.Forms.Panel();
            this.label258 = new System.Windows.Forms.Label();
            this.panel259 = new System.Windows.Forms.Panel();
            this.label259 = new System.Windows.Forms.Label();
            this.panel260 = new System.Windows.Forms.Panel();
            this.label260 = new System.Windows.Forms.Label();
            this.panel261 = new System.Windows.Forms.Panel();
            this.label261 = new System.Windows.Forms.Label();
            this.panel262 = new System.Windows.Forms.Panel();
            this.label262 = new System.Windows.Forms.Label();
            this.panel263 = new System.Windows.Forms.Panel();
            this.label263 = new System.Windows.Forms.Label();
            this.panel264 = new System.Windows.Forms.Panel();
            this.label264 = new System.Windows.Forms.Label();
            this.panel265 = new System.Windows.Forms.Panel();
            this.label265 = new System.Windows.Forms.Label();
            this.panel266 = new System.Windows.Forms.Panel();
            this.label266 = new System.Windows.Forms.Label();
            this.panel267 = new System.Windows.Forms.Panel();
            this.label267 = new System.Windows.Forms.Label();
            this.panel268 = new System.Windows.Forms.Panel();
            this.label268 = new System.Windows.Forms.Label();
            this.panel269 = new System.Windows.Forms.Panel();
            this.label269 = new System.Windows.Forms.Label();
            this.panel270 = new System.Windows.Forms.Panel();
            this.label270 = new System.Windows.Forms.Label();
            this.panel271 = new System.Windows.Forms.Panel();
            this.label271 = new System.Windows.Forms.Label();
            this.panel272 = new System.Windows.Forms.Panel();
            this.label272 = new System.Windows.Forms.Label();
            this.panel273 = new System.Windows.Forms.Panel();
            this.label273 = new System.Windows.Forms.Label();
            this.panel274 = new System.Windows.Forms.Panel();
            this.label274 = new System.Windows.Forms.Label();
            this.panel275 = new System.Windows.Forms.Panel();
            this.label275 = new System.Windows.Forms.Label();
            this.panel276 = new System.Windows.Forms.Panel();
            this.label276 = new System.Windows.Forms.Label();
            this.panel277 = new System.Windows.Forms.Panel();
            this.label277 = new System.Windows.Forms.Label();
            this.panel278 = new System.Windows.Forms.Panel();
            this.label278 = new System.Windows.Forms.Label();
            this.panel279 = new System.Windows.Forms.Panel();
            this.label279 = new System.Windows.Forms.Label();
            this.panel280 = new System.Windows.Forms.Panel();
            this.label280 = new System.Windows.Forms.Label();
            this.panel281 = new System.Windows.Forms.Panel();
            this.label281 = new System.Windows.Forms.Label();
            this.panel282 = new System.Windows.Forms.Panel();
            this.label282 = new System.Windows.Forms.Label();
            this.panel283 = new System.Windows.Forms.Panel();
            this.label283 = new System.Windows.Forms.Label();
            this.panel284 = new System.Windows.Forms.Panel();
            this.label284 = new System.Windows.Forms.Label();
            this.panel285 = new System.Windows.Forms.Panel();
            this.label285 = new System.Windows.Forms.Label();
            this.panel286 = new System.Windows.Forms.Panel();
            this.label286 = new System.Windows.Forms.Label();
            this.panel287 = new System.Windows.Forms.Panel();
            this.label287 = new System.Windows.Forms.Label();
            this.panel288 = new System.Windows.Forms.Panel();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.label288 = new System.Windows.Forms.Label();
            this.panel289 = new System.Windows.Forms.Panel();
            this.label289 = new System.Windows.Forms.Label();
            this.panel290 = new System.Windows.Forms.Panel();
            this.label290 = new System.Windows.Forms.Label();
            this.panel291 = new System.Windows.Forms.Panel();
            this.label291 = new System.Windows.Forms.Label();
            this.panel292 = new System.Windows.Forms.Panel();
            this.label292 = new System.Windows.Forms.Label();
            this.panel293 = new System.Windows.Forms.Panel();
            this.label293 = new System.Windows.Forms.Label();
            this.panel294 = new System.Windows.Forms.Panel();
            this.label294 = new System.Windows.Forms.Label();
            this.panel295 = new System.Windows.Forms.Panel();
            this.label295 = new System.Windows.Forms.Label();
            this.panel296 = new System.Windows.Forms.Panel();
            this.label296 = new System.Windows.Forms.Label();
            this.panel297 = new System.Windows.Forms.Panel();
            this.label297 = new System.Windows.Forms.Label();
            this.panel298 = new System.Windows.Forms.Panel();
            this.label298 = new System.Windows.Forms.Label();
            this.panel299 = new System.Windows.Forms.Panel();
            this.label299 = new System.Windows.Forms.Label();
            this.panel300 = new System.Windows.Forms.Panel();
            this.label300 = new System.Windows.Forms.Label();
            this.panel301 = new System.Windows.Forms.Panel();
            this.label301 = new System.Windows.Forms.Label();
            this.panel302 = new System.Windows.Forms.Panel();
            this.label302 = new System.Windows.Forms.Label();
            this.panel303 = new System.Windows.Forms.Panel();
            this.label303 = new System.Windows.Forms.Label();
            this.panel304 = new System.Windows.Forms.Panel();
            this.label304 = new System.Windows.Forms.Label();
            this.panel305 = new System.Windows.Forms.Panel();
            this.label305 = new System.Windows.Forms.Label();
            this.panel306 = new System.Windows.Forms.Panel();
            this.label306 = new System.Windows.Forms.Label();
            this.panel307 = new System.Windows.Forms.Panel();
            this.label307 = new System.Windows.Forms.Label();
            this.panel308 = new System.Windows.Forms.Panel();
            this.label308 = new System.Windows.Forms.Label();
            this.panel309 = new System.Windows.Forms.Panel();
            this.label309 = new System.Windows.Forms.Label();
            this.panel310 = new System.Windows.Forms.Panel();
            this.label310 = new System.Windows.Forms.Label();
            this.panel311 = new System.Windows.Forms.Panel();
            this.label311 = new System.Windows.Forms.Label();
            this.panel312 = new System.Windows.Forms.Panel();
            this.label312 = new System.Windows.Forms.Label();
            this.panel313 = new System.Windows.Forms.Panel();
            this.label313 = new System.Windows.Forms.Label();
            this.panel314 = new System.Windows.Forms.Panel();
            this.label314 = new System.Windows.Forms.Label();
            this.panel315 = new System.Windows.Forms.Panel();
            this.label315 = new System.Windows.Forms.Label();
            this.panel316 = new System.Windows.Forms.Panel();
            this.label316 = new System.Windows.Forms.Label();
            this.panel317 = new System.Windows.Forms.Panel();
            this.label317 = new System.Windows.Forms.Label();
            this.panel318 = new System.Windows.Forms.Panel();
            this.label318 = new System.Windows.Forms.Label();
            this.panel319 = new System.Windows.Forms.Panel();
            this.label319 = new System.Windows.Forms.Label();
            this.panel320 = new System.Windows.Forms.Panel();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.label320 = new System.Windows.Forms.Label();
            this.panel321 = new System.Windows.Forms.Panel();
            this.label321 = new System.Windows.Forms.Label();
            this.panel322 = new System.Windows.Forms.Panel();
            this.label322 = new System.Windows.Forms.Label();
            this.panel323 = new System.Windows.Forms.Panel();
            this.label323 = new System.Windows.Forms.Label();
            this.panel324 = new System.Windows.Forms.Panel();
            this.label324 = new System.Windows.Forms.Label();
            this.panel325 = new System.Windows.Forms.Panel();
            this.label325 = new System.Windows.Forms.Label();
            this.panel326 = new System.Windows.Forms.Panel();
            this.label326 = new System.Windows.Forms.Label();
            this.panel327 = new System.Windows.Forms.Panel();
            this.label327 = new System.Windows.Forms.Label();
            this.panel328 = new System.Windows.Forms.Panel();
            this.label328 = new System.Windows.Forms.Label();
            this.panel329 = new System.Windows.Forms.Panel();
            this.label329 = new System.Windows.Forms.Label();
            this.panel330 = new System.Windows.Forms.Panel();
            this.label330 = new System.Windows.Forms.Label();
            this.panel331 = new System.Windows.Forms.Panel();
            this.label331 = new System.Windows.Forms.Label();
            this.panel332 = new System.Windows.Forms.Panel();
            this.label332 = new System.Windows.Forms.Label();
            this.panel333 = new System.Windows.Forms.Panel();
            this.label333 = new System.Windows.Forms.Label();
            this.panel334 = new System.Windows.Forms.Panel();
            this.label334 = new System.Windows.Forms.Label();
            this.panel335 = new System.Windows.Forms.Panel();
            this.label335 = new System.Windows.Forms.Label();
            this.panel336 = new System.Windows.Forms.Panel();
            this.label336 = new System.Windows.Forms.Label();
            this.panel337 = new System.Windows.Forms.Panel();
            this.label337 = new System.Windows.Forms.Label();
            this.panel338 = new System.Windows.Forms.Panel();
            this.label338 = new System.Windows.Forms.Label();
            this.panel339 = new System.Windows.Forms.Panel();
            this.label339 = new System.Windows.Forms.Label();
            this.panel340 = new System.Windows.Forms.Panel();
            this.label340 = new System.Windows.Forms.Label();
            this.panel341 = new System.Windows.Forms.Panel();
            this.label341 = new System.Windows.Forms.Label();
            this.panel342 = new System.Windows.Forms.Panel();
            this.label342 = new System.Windows.Forms.Label();
            this.panel343 = new System.Windows.Forms.Panel();
            this.label343 = new System.Windows.Forms.Label();
            this.panel344 = new System.Windows.Forms.Panel();
            this.label344 = new System.Windows.Forms.Label();
            this.panel345 = new System.Windows.Forms.Panel();
            this.label345 = new System.Windows.Forms.Label();
            this.panel346 = new System.Windows.Forms.Panel();
            this.label346 = new System.Windows.Forms.Label();
            this.panel347 = new System.Windows.Forms.Panel();
            this.label347 = new System.Windows.Forms.Label();
            this.panel348 = new System.Windows.Forms.Panel();
            this.label348 = new System.Windows.Forms.Label();
            this.panel349 = new System.Windows.Forms.Panel();
            this.label349 = new System.Windows.Forms.Label();
            this.panel350 = new System.Windows.Forms.Panel();
            this.label350 = new System.Windows.Forms.Label();
            this.panel351 = new System.Windows.Forms.Panel();
            this.label351 = new System.Windows.Forms.Label();
            this.panel352 = new System.Windows.Forms.Panel();
            this.tc_iostatus_info.SuspendLayout();
            this.tp_iostatus_ld.SuspendLayout();
            this.tc_iostatus_ld_out.SuspendLayout();
            this.tp_iostatus_ld_out1.SuspendLayout();
            this.tp_iostatus_ld_out2.SuspendLayout();
            this.tp_iostatus_ld_out3.SuspendLayout();
            this.tp_iostatus_ld_out4.SuspendLayout();
            this.tc_iostatus_ld_in.SuspendLayout();
            this.tp_iostatus_ld_in1.SuspendLayout();
            this.tp_iostatus_ld_in2.SuspendLayout();
            this.tp_iostatus_ld_in3.SuspendLayout();
            this.tp_iostatus_ld_in4.SuspendLayout();
            this.tp_iostatus_process.SuspendLayout();
            this.tc_iostatus_process_out.SuspendLayout();
            this.tp_iostatus_process_out1.SuspendLayout();
            this.tp_iostatus_process_out2.SuspendLayout();
            this.tp_iostatus_process_out3.SuspendLayout();
            this.tc_iostatus_process_in.SuspendLayout();
            this.tp_iostatus_process_in1.SuspendLayout();
            this.tp_iostatus_process_in2.SuspendLayout();
            this.tp_iostatus_process_in3.SuspendLayout();
            this.tp_iostatus_uld.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.SuspendLayout();
            // 
            // tc_iostatus_info
            // 
            this.tc_iostatus_info.Controls.Add(this.tp_iostatus_ld);
            this.tc_iostatus_info.Controls.Add(this.tp_iostatus_process);
            this.tc_iostatus_info.Controls.Add(this.tp_iostatus_uld);
            this.tc_iostatus_info.ItemSize = new System.Drawing.Size(577, 30);
            this.tc_iostatus_info.Location = new System.Drawing.Point(3, 3);
            this.tc_iostatus_info.Name = "tc_iostatus_info";
            this.tc_iostatus_info.SelectedIndex = 0;
            this.tc_iostatus_info.Size = new System.Drawing.Size(1734, 854);
            this.tc_iostatus_info.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tc_iostatus_info.TabIndex = 0;
            // 
            // tp_iostatus_ld
            // 
            this.tp_iostatus_ld.BackColor = System.Drawing.Color.DarkSlateGray;
            this.tp_iostatus_ld.Controls.Add(this.tc_iostatus_ld_out);
            this.tp_iostatus_ld.Controls.Add(this.tc_iostatus_ld_in);
            this.tp_iostatus_ld.Location = new System.Drawing.Point(4, 34);
            this.tp_iostatus_ld.Name = "tp_iostatus_ld";
            this.tp_iostatus_ld.Padding = new System.Windows.Forms.Padding(3);
            this.tp_iostatus_ld.Size = new System.Drawing.Size(1726, 816);
            this.tp_iostatus_ld.TabIndex = 0;
            this.tp_iostatus_ld.Text = "로더";
            // 
            // tc_iostatus_ld_out
            // 
            this.tc_iostatus_ld_out.Controls.Add(this.tp_iostatus_ld_out1);
            this.tc_iostatus_ld_out.Controls.Add(this.tp_iostatus_ld_out2);
            this.tc_iostatus_ld_out.Controls.Add(this.tp_iostatus_ld_out3);
            this.tc_iostatus_ld_out.Controls.Add(this.tp_iostatus_ld_out4);
            this.tc_iostatus_ld_out.ItemSize = new System.Drawing.Size(212, 30);
            this.tc_iostatus_ld_out.Location = new System.Drawing.Point(866, 6);
            this.tc_iostatus_ld_out.Name = "tc_iostatus_ld_out";
            this.tc_iostatus_ld_out.SelectedIndex = 0;
            this.tc_iostatus_ld_out.Size = new System.Drawing.Size(854, 816);
            this.tc_iostatus_ld_out.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tc_iostatus_ld_out.TabIndex = 1;
            // 
            // tp_iostatus_ld_out1
            // 
            this.tp_iostatus_ld_out1.BackColor = System.Drawing.Color.DimGray;
            this.tp_iostatus_ld_out1.Controls.Add(this.lb_y19f_4_ld_cst_d_ungrip);
            this.tp_iostatus_ld_out1.Controls.Add(this.pn_Loader_OUT1_Y19F);
            this.tp_iostatus_ld_out1.Controls.Add(this.lb_y19e_4_ld_cst_d_grip);
            this.tp_iostatus_ld_out1.Controls.Add(this.pn_Loader_OUT1_Y19E);
            this.tp_iostatus_ld_out1.Controls.Add(this.lb_y19d_4_ld_cst_lr_ungrip);
            this.tp_iostatus_ld_out1.Controls.Add(this.pn_Loader_OUT1_Y19D);
            this.tp_iostatus_ld_out1.Controls.Add(this.lb_y19c_4_ld_cst_lr_grip);
            this.tp_iostatus_ld_out1.Controls.Add(this.pn_Loader_OUT1_Y19C);
            this.tp_iostatus_ld_out1.Controls.Add(this.lb_y19b_3_ld_cst_d_ungrip);
            this.tp_iostatus_ld_out1.Controls.Add(this.pn_Loader_OUT1_Y19B);
            this.tp_iostatus_ld_out1.Controls.Add(this.lb_y19a_3_ld_cst_d_grip);
            this.tp_iostatus_ld_out1.Controls.Add(this.pn_Loader_OUT1_Y19A);
            this.tp_iostatus_ld_out1.Controls.Add(this.lb_y199_3_ld_cst_lr_ungrip);
            this.tp_iostatus_ld_out1.Controls.Add(this.pn_Loader_OUT1_Y199);
            this.tp_iostatus_ld_out1.Controls.Add(this.lb_y198_3_ld_cst_lr_grip);
            this.tp_iostatus_ld_out1.Controls.Add(this.pn_Loader_OUT1_Y198);
            this.tp_iostatus_ld_out1.Controls.Add(this.lb_y197_2_ld_cst_d_ungrip);
            this.tp_iostatus_ld_out1.Controls.Add(this.pn_Loader_OUT1_Y197);
            this.tp_iostatus_ld_out1.Controls.Add(this.lb_y196_2_ld_cst_d_grip);
            this.tp_iostatus_ld_out1.Controls.Add(this.pn_Loader_OUT1_Y196);
            this.tp_iostatus_ld_out1.Controls.Add(this.lb_y195_2_ld_cst_lr_ungrip);
            this.tp_iostatus_ld_out1.Controls.Add(this.pn_Loader_OUT1_Y195);
            this.tp_iostatus_ld_out1.Controls.Add(this.lb_y194_2_ld_cst_lr_grip);
            this.tp_iostatus_ld_out1.Controls.Add(this.pn_Loader_OUT1_Y194);
            this.tp_iostatus_ld_out1.Controls.Add(this.lb_y193_1_ld_cst_d_ungrip);
            this.tp_iostatus_ld_out1.Controls.Add(this.pn_Loader_OUT1_Y193);
            this.tp_iostatus_ld_out1.Controls.Add(this.lb_y192_1_ld_cst_d_grip);
            this.tp_iostatus_ld_out1.Controls.Add(this.pn_Loader_OUT1_Y192);
            this.tp_iostatus_ld_out1.Controls.Add(this.lb_y191_1_ld_cst_lr_ungrip);
            this.tp_iostatus_ld_out1.Controls.Add(this.pn_Loader_OUT1_Y191);
            this.tp_iostatus_ld_out1.Controls.Add(this.lb_y190_1_ld_cst_lr_grip);
            this.tp_iostatus_ld_out1.Controls.Add(this.pn_Loader_OUT1_Y190);
            this.tp_iostatus_ld_out1.Controls.Add(this.lb_y18f_mode_select_sw);
            this.tp_iostatus_ld_out1.Controls.Add(this.pn_Loader_OUT1_Y18F);
            this.tp_iostatus_ld_out1.Controls.Add(this.lb_y18e_ld_reset);
            this.tp_iostatus_ld_out1.Controls.Add(this.pn_Loader_OUT1_Y18E);
            this.tp_iostatus_ld_out1.Controls.Add(this.lb_y18d_cst_out_ready_buzzer);
            this.tp_iostatus_ld_out1.Controls.Add(this.pn_Loader_OUT1_Y18D);
            this.tp_iostatus_ld_out1.Controls.Add(this.lb_y18c_cst_in_ready_buzzer);
            this.tp_iostatus_ld_out1.Controls.Add(this.pn_Loader_OUT1_Y18C);
            this.tp_iostatus_ld_out1.Controls.Add(this.lb_y18b_control_select_ld_feedback);
            this.tp_iostatus_ld_out1.Controls.Add(this.pn_Loader_OUT1_Y18B);
            this.tp_iostatus_ld_out1.Controls.Add(this.lb_y18a_buzzer);
            this.tp_iostatus_ld_out1.Controls.Add(this.pn_Loader_OUT1_Y18A);
            this.tp_iostatus_ld_out1.Controls.Add(this.lb_y189_1_tower_lamp_g);
            this.tp_iostatus_ld_out1.Controls.Add(this.pn_Loader_OUT1_Y189);
            this.tp_iostatus_ld_out1.Controls.Add(this.lb_y188_1_tower_lamp_y);
            this.tp_iostatus_ld_out1.Controls.Add(this.pn_Loader_OUT1_Y188);
            this.tp_iostatus_ld_out1.Controls.Add(this.lb_y187_1_tower_lamp_r);
            this.tp_iostatus_ld_out1.Controls.Add(this.pn_Loader_OUT1_Y187);
            this.tp_iostatus_ld_out1.Controls.Add(this.lb_y186_safty_reset);
            this.tp_iostatus_ld_out1.Controls.Add(this.pn_Loader_OUT1_Y186);
            this.tp_iostatus_ld_out1.Controls.Add(this.lb_y185_spare);
            this.tp_iostatus_ld_out1.Controls.Add(this.pn_Loader_OUT1_Y185);
            this.tp_iostatus_ld_out1.Controls.Add(this.lb_y184_spare);
            this.tp_iostatus_ld_out1.Controls.Add(this.pn_Loader_OUT1_Y184);
            this.tp_iostatus_ld_out1.Controls.Add(this.lb_y183_spare);
            this.tp_iostatus_ld_out1.Controls.Add(this.pn_Loader_OUT1_Y183);
            this.tp_iostatus_ld_out1.Controls.Add(this.lb_y182_spare);
            this.tp_iostatus_ld_out1.Controls.Add(this.pn_Loader_OUT1_Y182);
            this.tp_iostatus_ld_out1.Controls.Add(this.lb_y181_laser_ld_motor_control_on_cmd);
            this.tp_iostatus_ld_out1.Controls.Add(this.pn_Loader_OUT1_Y181);
            this.tp_iostatus_ld_out1.Controls.Add(this.lb_y180_ld_motor_control_on_cmd);
            this.tp_iostatus_ld_out1.Controls.Add(this.pn_Loader_OUT1_Y180);
            this.tp_iostatus_ld_out1.Location = new System.Drawing.Point(4, 34);
            this.tp_iostatus_ld_out1.Name = "tp_iostatus_ld_out1";
            this.tp_iostatus_ld_out1.Padding = new System.Windows.Forms.Padding(3);
            this.tp_iostatus_ld_out1.Size = new System.Drawing.Size(846, 778);
            this.tp_iostatus_ld_out1.TabIndex = 0;
            this.tp_iostatus_ld_out1.Text = "OUT - 1";
            // 
            // lb_y19f_4_ld_cst_d_ungrip
            // 
            this.lb_y19f_4_ld_cst_d_ungrip.AutoSize = true;
            this.lb_y19f_4_ld_cst_d_ungrip.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y19f_4_ld_cst_d_ungrip.ForeColor = System.Drawing.Color.White;
            this.lb_y19f_4_ld_cst_d_ungrip.Location = new System.Drawing.Point(479, 730);
            this.lb_y19f_4_ld_cst_d_ungrip.Name = "lb_y19f_4_ld_cst_d_ungrip";
            this.lb_y19f_4_ld_cst_d_ungrip.Size = new System.Drawing.Size(264, 13);
            this.lb_y19f_4_ld_cst_d_ungrip.TabIndex = 127;
            this.lb_y19f_4_ld_cst_d_ungrip.Text = "[ Y19F ] 4 L/D Cassette 하부 UnGrip";
            // 
            // pn_Loader_OUT1_Y19F
            // 
            this.pn_Loader_OUT1_Y19F.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT1_Y19F.Location = new System.Drawing.Point(433, 718);
            this.pn_Loader_OUT1_Y19F.Name = "pn_Loader_OUT1_Y19F";
            this.pn_Loader_OUT1_Y19F.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT1_Y19F.TabIndex = 126;
            // 
            // lb_y19e_4_ld_cst_d_grip
            // 
            this.lb_y19e_4_ld_cst_d_grip.AutoSize = true;
            this.lb_y19e_4_ld_cst_d_grip.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y19e_4_ld_cst_d_grip.ForeColor = System.Drawing.Color.White;
            this.lb_y19e_4_ld_cst_d_grip.Location = new System.Drawing.Point(479, 685);
            this.lb_y19e_4_ld_cst_d_grip.Name = "lb_y19e_4_ld_cst_d_grip";
            this.lb_y19e_4_ld_cst_d_grip.Size = new System.Drawing.Size(246, 13);
            this.lb_y19e_4_ld_cst_d_grip.TabIndex = 125;
            this.lb_y19e_4_ld_cst_d_grip.Text = "[ Y19E ] 4 L/D Cassette 하부 Grip";
            // 
            // pn_Loader_OUT1_Y19E
            // 
            this.pn_Loader_OUT1_Y19E.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT1_Y19E.Location = new System.Drawing.Point(433, 672);
            this.pn_Loader_OUT1_Y19E.Name = "pn_Loader_OUT1_Y19E";
            this.pn_Loader_OUT1_Y19E.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT1_Y19E.TabIndex = 124;
            // 
            // lb_y19d_4_ld_cst_lr_ungrip
            // 
            this.lb_y19d_4_ld_cst_lr_ungrip.AutoSize = true;
            this.lb_y19d_4_ld_cst_lr_ungrip.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y19d_4_ld_cst_lr_ungrip.ForeColor = System.Drawing.Color.White;
            this.lb_y19d_4_ld_cst_lr_ungrip.Location = new System.Drawing.Point(479, 640);
            this.lb_y19d_4_ld_cst_lr_ungrip.Name = "lb_y19d_4_ld_cst_lr_ungrip";
            this.lb_y19d_4_ld_cst_lr_ungrip.Size = new System.Drawing.Size(266, 13);
            this.lb_y19d_4_ld_cst_lr_ungrip.TabIndex = 123;
            this.lb_y19d_4_ld_cst_lr_ungrip.Text = "[ Y19D ] 4 L/D Cassette 좌우 UnGrip";
            // 
            // pn_Loader_OUT1_Y19D
            // 
            this.pn_Loader_OUT1_Y19D.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT1_Y19D.Location = new System.Drawing.Point(433, 626);
            this.pn_Loader_OUT1_Y19D.Name = "pn_Loader_OUT1_Y19D";
            this.pn_Loader_OUT1_Y19D.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT1_Y19D.TabIndex = 122;
            // 
            // lb_y19c_4_ld_cst_lr_grip
            // 
            this.lb_y19c_4_ld_cst_lr_grip.AutoSize = true;
            this.lb_y19c_4_ld_cst_lr_grip.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y19c_4_ld_cst_lr_grip.ForeColor = System.Drawing.Color.White;
            this.lb_y19c_4_ld_cst_lr_grip.Location = new System.Drawing.Point(479, 594);
            this.lb_y19c_4_ld_cst_lr_grip.Name = "lb_y19c_4_ld_cst_lr_grip";
            this.lb_y19c_4_ld_cst_lr_grip.Size = new System.Drawing.Size(247, 13);
            this.lb_y19c_4_ld_cst_lr_grip.TabIndex = 121;
            this.lb_y19c_4_ld_cst_lr_grip.Text = "[ Y19C ] 4 L/D Cassette 좌우 Grip";
            // 
            // pn_Loader_OUT1_Y19C
            // 
            this.pn_Loader_OUT1_Y19C.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT1_Y19C.Location = new System.Drawing.Point(433, 580);
            this.pn_Loader_OUT1_Y19C.Name = "pn_Loader_OUT1_Y19C";
            this.pn_Loader_OUT1_Y19C.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT1_Y19C.TabIndex = 120;
            // 
            // lb_y19b_3_ld_cst_d_ungrip
            // 
            this.lb_y19b_3_ld_cst_d_ungrip.AutoSize = true;
            this.lb_y19b_3_ld_cst_d_ungrip.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y19b_3_ld_cst_d_ungrip.ForeColor = System.Drawing.Color.White;
            this.lb_y19b_3_ld_cst_d_ungrip.Location = new System.Drawing.Point(479, 548);
            this.lb_y19b_3_ld_cst_d_ungrip.Name = "lb_y19b_3_ld_cst_d_ungrip";
            this.lb_y19b_3_ld_cst_d_ungrip.Size = new System.Drawing.Size(266, 13);
            this.lb_y19b_3_ld_cst_d_ungrip.TabIndex = 119;
            this.lb_y19b_3_ld_cst_d_ungrip.Text = "[ Y19B ] 3 L/D Cassette 하부 UnGrip";
            // 
            // pn_Loader_OUT1_Y19B
            // 
            this.pn_Loader_OUT1_Y19B.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT1_Y19B.Location = new System.Drawing.Point(433, 534);
            this.pn_Loader_OUT1_Y19B.Name = "pn_Loader_OUT1_Y19B";
            this.pn_Loader_OUT1_Y19B.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT1_Y19B.TabIndex = 118;
            // 
            // lb_y19a_3_ld_cst_d_grip
            // 
            this.lb_y19a_3_ld_cst_d_grip.AutoSize = true;
            this.lb_y19a_3_ld_cst_d_grip.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y19a_3_ld_cst_d_grip.ForeColor = System.Drawing.Color.White;
            this.lb_y19a_3_ld_cst_d_grip.Location = new System.Drawing.Point(479, 501);
            this.lb_y19a_3_ld_cst_d_grip.Name = "lb_y19a_3_ld_cst_d_grip";
            this.lb_y19a_3_ld_cst_d_grip.Size = new System.Drawing.Size(246, 13);
            this.lb_y19a_3_ld_cst_d_grip.TabIndex = 117;
            this.lb_y19a_3_ld_cst_d_grip.Text = "[ Y19A ] 3 L/D Cassette 하부 Grip";
            // 
            // pn_Loader_OUT1_Y19A
            // 
            this.pn_Loader_OUT1_Y19A.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT1_Y19A.Location = new System.Drawing.Point(433, 488);
            this.pn_Loader_OUT1_Y19A.Name = "pn_Loader_OUT1_Y19A";
            this.pn_Loader_OUT1_Y19A.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT1_Y19A.TabIndex = 116;
            // 
            // lb_y199_3_ld_cst_lr_ungrip
            // 
            this.lb_y199_3_ld_cst_lr_ungrip.AutoSize = true;
            this.lb_y199_3_ld_cst_lr_ungrip.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y199_3_ld_cst_lr_ungrip.ForeColor = System.Drawing.Color.White;
            this.lb_y199_3_ld_cst_lr_ungrip.Location = new System.Drawing.Point(479, 454);
            this.lb_y199_3_ld_cst_lr_ungrip.Name = "lb_y199_3_ld_cst_lr_ungrip";
            this.lb_y199_3_ld_cst_lr_ungrip.Size = new System.Drawing.Size(264, 13);
            this.lb_y199_3_ld_cst_lr_ungrip.TabIndex = 115;
            this.lb_y199_3_ld_cst_lr_ungrip.Text = "[ Y199 ] 3 L/D Cassette 좌우 UnGrip";
            // 
            // pn_Loader_OUT1_Y199
            // 
            this.pn_Loader_OUT1_Y199.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT1_Y199.Location = new System.Drawing.Point(433, 442);
            this.pn_Loader_OUT1_Y199.Name = "pn_Loader_OUT1_Y199";
            this.pn_Loader_OUT1_Y199.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT1_Y199.TabIndex = 114;
            // 
            // lb_y198_3_ld_cst_lr_grip
            // 
            this.lb_y198_3_ld_cst_lr_grip.AutoSize = true;
            this.lb_y198_3_ld_cst_lr_grip.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y198_3_ld_cst_lr_grip.ForeColor = System.Drawing.Color.White;
            this.lb_y198_3_ld_cst_lr_grip.Location = new System.Drawing.Point(479, 409);
            this.lb_y198_3_ld_cst_lr_grip.Name = "lb_y198_3_ld_cst_lr_grip";
            this.lb_y198_3_ld_cst_lr_grip.Size = new System.Drawing.Size(245, 13);
            this.lb_y198_3_ld_cst_lr_grip.TabIndex = 113;
            this.lb_y198_3_ld_cst_lr_grip.Text = "[ Y198 ] 3 L/D Cassette 좌우 Grip";
            // 
            // pn_Loader_OUT1_Y198
            // 
            this.pn_Loader_OUT1_Y198.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT1_Y198.Location = new System.Drawing.Point(433, 396);
            this.pn_Loader_OUT1_Y198.Name = "pn_Loader_OUT1_Y198";
            this.pn_Loader_OUT1_Y198.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT1_Y198.TabIndex = 112;
            // 
            // lb_y197_2_ld_cst_d_ungrip
            // 
            this.lb_y197_2_ld_cst_d_ungrip.AutoSize = true;
            this.lb_y197_2_ld_cst_d_ungrip.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y197_2_ld_cst_d_ungrip.ForeColor = System.Drawing.Color.White;
            this.lb_y197_2_ld_cst_d_ungrip.Location = new System.Drawing.Point(479, 364);
            this.lb_y197_2_ld_cst_d_ungrip.Name = "lb_y197_2_ld_cst_d_ungrip";
            this.lb_y197_2_ld_cst_d_ungrip.Size = new System.Drawing.Size(264, 13);
            this.lb_y197_2_ld_cst_d_ungrip.TabIndex = 111;
            this.lb_y197_2_ld_cst_d_ungrip.Text = "[ Y197 ] 2 L/D Cassette 하부 UnGrip";
            // 
            // pn_Loader_OUT1_Y197
            // 
            this.pn_Loader_OUT1_Y197.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT1_Y197.Location = new System.Drawing.Point(433, 350);
            this.pn_Loader_OUT1_Y197.Name = "pn_Loader_OUT1_Y197";
            this.pn_Loader_OUT1_Y197.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT1_Y197.TabIndex = 110;
            // 
            // lb_y196_2_ld_cst_d_grip
            // 
            this.lb_y196_2_ld_cst_d_grip.AutoSize = true;
            this.lb_y196_2_ld_cst_d_grip.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y196_2_ld_cst_d_grip.ForeColor = System.Drawing.Color.White;
            this.lb_y196_2_ld_cst_d_grip.Location = new System.Drawing.Point(479, 319);
            this.lb_y196_2_ld_cst_d_grip.Name = "lb_y196_2_ld_cst_d_grip";
            this.lb_y196_2_ld_cst_d_grip.Size = new System.Drawing.Size(245, 13);
            this.lb_y196_2_ld_cst_d_grip.TabIndex = 109;
            this.lb_y196_2_ld_cst_d_grip.Text = "[ Y196 ] 2 L/D Cassette 하부 Grip";
            // 
            // pn_Loader_OUT1_Y196
            // 
            this.pn_Loader_OUT1_Y196.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT1_Y196.Location = new System.Drawing.Point(433, 304);
            this.pn_Loader_OUT1_Y196.Name = "pn_Loader_OUT1_Y196";
            this.pn_Loader_OUT1_Y196.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT1_Y196.TabIndex = 108;
            // 
            // lb_y195_2_ld_cst_lr_ungrip
            // 
            this.lb_y195_2_ld_cst_lr_ungrip.AutoSize = true;
            this.lb_y195_2_ld_cst_lr_ungrip.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y195_2_ld_cst_lr_ungrip.ForeColor = System.Drawing.Color.White;
            this.lb_y195_2_ld_cst_lr_ungrip.Location = new System.Drawing.Point(479, 271);
            this.lb_y195_2_ld_cst_lr_ungrip.Name = "lb_y195_2_ld_cst_lr_ungrip";
            this.lb_y195_2_ld_cst_lr_ungrip.Size = new System.Drawing.Size(264, 13);
            this.lb_y195_2_ld_cst_lr_ungrip.TabIndex = 107;
            this.lb_y195_2_ld_cst_lr_ungrip.Text = "[ Y195 ] 2 L/D Cassette 좌우 UnGrip";
            // 
            // pn_Loader_OUT1_Y195
            // 
            this.pn_Loader_OUT1_Y195.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT1_Y195.Location = new System.Drawing.Point(433, 258);
            this.pn_Loader_OUT1_Y195.Name = "pn_Loader_OUT1_Y195";
            this.pn_Loader_OUT1_Y195.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT1_Y195.TabIndex = 106;
            // 
            // lb_y194_2_ld_cst_lr_grip
            // 
            this.lb_y194_2_ld_cst_lr_grip.AutoSize = true;
            this.lb_y194_2_ld_cst_lr_grip.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y194_2_ld_cst_lr_grip.ForeColor = System.Drawing.Color.White;
            this.lb_y194_2_ld_cst_lr_grip.Location = new System.Drawing.Point(479, 226);
            this.lb_y194_2_ld_cst_lr_grip.Name = "lb_y194_2_ld_cst_lr_grip";
            this.lb_y194_2_ld_cst_lr_grip.Size = new System.Drawing.Size(245, 13);
            this.lb_y194_2_ld_cst_lr_grip.TabIndex = 105;
            this.lb_y194_2_ld_cst_lr_grip.Text = "[ Y194 ] 2 L/D Cassette 좌우 Grip";
            // 
            // pn_Loader_OUT1_Y194
            // 
            this.pn_Loader_OUT1_Y194.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT1_Y194.Location = new System.Drawing.Point(433, 212);
            this.pn_Loader_OUT1_Y194.Name = "pn_Loader_OUT1_Y194";
            this.pn_Loader_OUT1_Y194.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT1_Y194.TabIndex = 104;
            // 
            // lb_y193_1_ld_cst_d_ungrip
            // 
            this.lb_y193_1_ld_cst_d_ungrip.AutoSize = true;
            this.lb_y193_1_ld_cst_d_ungrip.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y193_1_ld_cst_d_ungrip.ForeColor = System.Drawing.Color.White;
            this.lb_y193_1_ld_cst_d_ungrip.Location = new System.Drawing.Point(479, 179);
            this.lb_y193_1_ld_cst_d_ungrip.Name = "lb_y193_1_ld_cst_d_ungrip";
            this.lb_y193_1_ld_cst_d_ungrip.Size = new System.Drawing.Size(264, 13);
            this.lb_y193_1_ld_cst_d_ungrip.TabIndex = 103;
            this.lb_y193_1_ld_cst_d_ungrip.Text = "[ Y193 ] 1 L/D Cassette 하부 UnGrip";
            // 
            // pn_Loader_OUT1_Y193
            // 
            this.pn_Loader_OUT1_Y193.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT1_Y193.Location = new System.Drawing.Point(433, 166);
            this.pn_Loader_OUT1_Y193.Name = "pn_Loader_OUT1_Y193";
            this.pn_Loader_OUT1_Y193.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT1_Y193.TabIndex = 102;
            // 
            // lb_y192_1_ld_cst_d_grip
            // 
            this.lb_y192_1_ld_cst_d_grip.AutoSize = true;
            this.lb_y192_1_ld_cst_d_grip.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y192_1_ld_cst_d_grip.ForeColor = System.Drawing.Color.White;
            this.lb_y192_1_ld_cst_d_grip.Location = new System.Drawing.Point(479, 133);
            this.lb_y192_1_ld_cst_d_grip.Name = "lb_y192_1_ld_cst_d_grip";
            this.lb_y192_1_ld_cst_d_grip.Size = new System.Drawing.Size(245, 13);
            this.lb_y192_1_ld_cst_d_grip.TabIndex = 101;
            this.lb_y192_1_ld_cst_d_grip.Text = "[ Y192 ] 1 L/D Cassette 하부 Grip";
            // 
            // pn_Loader_OUT1_Y192
            // 
            this.pn_Loader_OUT1_Y192.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT1_Y192.Location = new System.Drawing.Point(433, 120);
            this.pn_Loader_OUT1_Y192.Name = "pn_Loader_OUT1_Y192";
            this.pn_Loader_OUT1_Y192.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT1_Y192.TabIndex = 100;
            // 
            // lb_y191_1_ld_cst_lr_ungrip
            // 
            this.lb_y191_1_ld_cst_lr_ungrip.AutoSize = true;
            this.lb_y191_1_ld_cst_lr_ungrip.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y191_1_ld_cst_lr_ungrip.ForeColor = System.Drawing.Color.White;
            this.lb_y191_1_ld_cst_lr_ungrip.Location = new System.Drawing.Point(479, 86);
            this.lb_y191_1_ld_cst_lr_ungrip.Name = "lb_y191_1_ld_cst_lr_ungrip";
            this.lb_y191_1_ld_cst_lr_ungrip.Size = new System.Drawing.Size(264, 13);
            this.lb_y191_1_ld_cst_lr_ungrip.TabIndex = 99;
            this.lb_y191_1_ld_cst_lr_ungrip.Text = "[ Y191 ] 1 L/D Cassette 좌우 UnGrip";
            // 
            // pn_Loader_OUT1_Y191
            // 
            this.pn_Loader_OUT1_Y191.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT1_Y191.Location = new System.Drawing.Point(433, 74);
            this.pn_Loader_OUT1_Y191.Name = "pn_Loader_OUT1_Y191";
            this.pn_Loader_OUT1_Y191.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT1_Y191.TabIndex = 98;
            // 
            // lb_y190_1_ld_cst_lr_grip
            // 
            this.lb_y190_1_ld_cst_lr_grip.AutoSize = true;
            this.lb_y190_1_ld_cst_lr_grip.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y190_1_ld_cst_lr_grip.ForeColor = System.Drawing.Color.White;
            this.lb_y190_1_ld_cst_lr_grip.Location = new System.Drawing.Point(479, 42);
            this.lb_y190_1_ld_cst_lr_grip.Name = "lb_y190_1_ld_cst_lr_grip";
            this.lb_y190_1_ld_cst_lr_grip.Size = new System.Drawing.Size(245, 13);
            this.lb_y190_1_ld_cst_lr_grip.TabIndex = 97;
            this.lb_y190_1_ld_cst_lr_grip.Text = "[ Y190 ] 1 L/D Cassette 좌우 Grip";
            // 
            // pn_Loader_OUT1_Y190
            // 
            this.pn_Loader_OUT1_Y190.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT1_Y190.Location = new System.Drawing.Point(433, 28);
            this.pn_Loader_OUT1_Y190.Name = "pn_Loader_OUT1_Y190";
            this.pn_Loader_OUT1_Y190.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT1_Y190.TabIndex = 96;
            // 
            // lb_y18f_mode_select_sw
            // 
            this.lb_y18f_mode_select_sw.AutoSize = true;
            this.lb_y18f_mode_select_sw.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y18f_mode_select_sw.ForeColor = System.Drawing.Color.White;
            this.lb_y18f_mode_select_sw.Location = new System.Drawing.Point(52, 730);
            this.lb_y18f_mode_select_sw.Name = "lb_y18f_mode_select_sw";
            this.lb_y18f_mode_select_sw.Size = new System.Drawing.Size(326, 13);
            this.lb_y18f_mode_select_sw.TabIndex = 95;
            this.lb_y18f_mode_select_sw.Text = "[ Y18F ] Mode Select SW - Auto/THACH SOL";
            // 
            // pn_Loader_OUT1_Y18F
            // 
            this.pn_Loader_OUT1_Y18F.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT1_Y18F.Location = new System.Drawing.Point(6, 718);
            this.pn_Loader_OUT1_Y18F.Name = "pn_Loader_OUT1_Y18F";
            this.pn_Loader_OUT1_Y18F.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT1_Y18F.TabIndex = 94;
            // 
            // lb_y18e_ld_reset
            // 
            this.lb_y18e_ld_reset.AutoSize = true;
            this.lb_y18e_ld_reset.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y18e_ld_reset.ForeColor = System.Drawing.Color.White;
            this.lb_y18e_ld_reset.Location = new System.Drawing.Point(52, 685);
            this.lb_y18e_ld_reset.Name = "lb_y18e_ld_reset";
            this.lb_y18e_ld_reset.Size = new System.Drawing.Size(143, 13);
            this.lb_y18e_ld_reset.TabIndex = 93;
            this.lb_y18e_ld_reset.Text = "[ Y18E ] L/D Reset";
            // 
            // pn_Loader_OUT1_Y18E
            // 
            this.pn_Loader_OUT1_Y18E.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT1_Y18E.Location = new System.Drawing.Point(6, 672);
            this.pn_Loader_OUT1_Y18E.Name = "pn_Loader_OUT1_Y18E";
            this.pn_Loader_OUT1_Y18E.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT1_Y18E.TabIndex = 92;
            // 
            // lb_y18d_cst_out_ready_buzzer
            // 
            this.lb_y18d_cst_out_ready_buzzer.AutoSize = true;
            this.lb_y18d_cst_out_ready_buzzer.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y18d_cst_out_ready_buzzer.ForeColor = System.Drawing.Color.White;
            this.lb_y18d_cst_out_ready_buzzer.Location = new System.Drawing.Point(52, 640);
            this.lb_y18d_cst_out_ready_buzzer.Name = "lb_y18d_cst_out_ready_buzzer";
            this.lb_y18d_cst_out_ready_buzzer.Size = new System.Drawing.Size(257, 13);
            this.lb_y18d_cst_out_ready_buzzer.TabIndex = 91;
            this.lb_y18d_cst_out_ready_buzzer.Text = "[ Y18D ] Cassette 배출 대기 Buzzer";
            // 
            // pn_Loader_OUT1_Y18D
            // 
            this.pn_Loader_OUT1_Y18D.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT1_Y18D.Location = new System.Drawing.Point(6, 626);
            this.pn_Loader_OUT1_Y18D.Name = "pn_Loader_OUT1_Y18D";
            this.pn_Loader_OUT1_Y18D.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT1_Y18D.TabIndex = 90;
            // 
            // lb_y18c_cst_in_ready_buzzer
            // 
            this.lb_y18c_cst_in_ready_buzzer.AutoSize = true;
            this.lb_y18c_cst_in_ready_buzzer.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y18c_cst_in_ready_buzzer.ForeColor = System.Drawing.Color.White;
            this.lb_y18c_cst_in_ready_buzzer.Location = new System.Drawing.Point(52, 594);
            this.lb_y18c_cst_in_ready_buzzer.Name = "lb_y18c_cst_in_ready_buzzer";
            this.lb_y18c_cst_in_ready_buzzer.Size = new System.Drawing.Size(257, 13);
            this.lb_y18c_cst_in_ready_buzzer.TabIndex = 89;
            this.lb_y18c_cst_in_ready_buzzer.Text = "[ Y18C ] Cassette 투입 대기 Buzzer";
            // 
            // pn_Loader_OUT1_Y18C
            // 
            this.pn_Loader_OUT1_Y18C.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT1_Y18C.Location = new System.Drawing.Point(6, 580);
            this.pn_Loader_OUT1_Y18C.Name = "pn_Loader_OUT1_Y18C";
            this.pn_Loader_OUT1_Y18C.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT1_Y18C.TabIndex = 88;
            // 
            // lb_y18b_control_select_ld_feedback
            // 
            this.lb_y18b_control_select_ld_feedback.AutoSize = true;
            this.lb_y18b_control_select_ld_feedback.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y18b_control_select_ld_feedback.ForeColor = System.Drawing.Color.White;
            this.lb_y18b_control_select_ld_feedback.Location = new System.Drawing.Point(52, 548);
            this.lb_y18b_control_select_ld_feedback.Name = "lb_y18b_control_select_ld_feedback";
            this.lb_y18b_control_select_ld_feedback.Size = new System.Drawing.Size(278, 13);
            this.lb_y18b_control_select_ld_feedback.TabIndex = 87;
            this.lb_y18b_control_select_ld_feedback.Text = "[ Y18B ] Control Select L/D Feedback";
            // 
            // pn_Loader_OUT1_Y18B
            // 
            this.pn_Loader_OUT1_Y18B.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT1_Y18B.Location = new System.Drawing.Point(6, 534);
            this.pn_Loader_OUT1_Y18B.Name = "pn_Loader_OUT1_Y18B";
            this.pn_Loader_OUT1_Y18B.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT1_Y18B.TabIndex = 86;
            // 
            // lb_y18a_buzzer
            // 
            this.lb_y18a_buzzer.AutoSize = true;
            this.lb_y18a_buzzer.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y18a_buzzer.ForeColor = System.Drawing.Color.White;
            this.lb_y18a_buzzer.Location = new System.Drawing.Point(52, 501);
            this.lb_y18a_buzzer.Name = "lb_y18a_buzzer";
            this.lb_y18a_buzzer.Size = new System.Drawing.Size(122, 13);
            this.lb_y18a_buzzer.TabIndex = 85;
            this.lb_y18a_buzzer.Text = "[ Y18A ] Buzzer";
            // 
            // pn_Loader_OUT1_Y18A
            // 
            this.pn_Loader_OUT1_Y18A.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT1_Y18A.Location = new System.Drawing.Point(6, 488);
            this.pn_Loader_OUT1_Y18A.Name = "pn_Loader_OUT1_Y18A";
            this.pn_Loader_OUT1_Y18A.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT1_Y18A.TabIndex = 84;
            // 
            // lb_y189_1_tower_lamp_g
            // 
            this.lb_y189_1_tower_lamp_g.AutoSize = true;
            this.lb_y189_1_tower_lamp_g.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y189_1_tower_lamp_g.ForeColor = System.Drawing.Color.White;
            this.lb_y189_1_tower_lamp_g.Location = new System.Drawing.Point(52, 454);
            this.lb_y189_1_tower_lamp_g.Name = "lb_y189_1_tower_lamp_g";
            this.lb_y189_1_tower_lamp_g.Size = new System.Drawing.Size(197, 13);
            this.lb_y189_1_tower_lamp_g.TabIndex = 83;
            this.lb_y189_1_tower_lamp_g.Text = "[ Y189 ] 1 Tower Lamp \"G\"";
            // 
            // pn_Loader_OUT1_Y189
            // 
            this.pn_Loader_OUT1_Y189.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT1_Y189.Location = new System.Drawing.Point(6, 442);
            this.pn_Loader_OUT1_Y189.Name = "pn_Loader_OUT1_Y189";
            this.pn_Loader_OUT1_Y189.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT1_Y189.TabIndex = 82;
            // 
            // lb_y188_1_tower_lamp_y
            // 
            this.lb_y188_1_tower_lamp_y.AutoSize = true;
            this.lb_y188_1_tower_lamp_y.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y188_1_tower_lamp_y.ForeColor = System.Drawing.Color.White;
            this.lb_y188_1_tower_lamp_y.Location = new System.Drawing.Point(52, 409);
            this.lb_y188_1_tower_lamp_y.Name = "lb_y188_1_tower_lamp_y";
            this.lb_y188_1_tower_lamp_y.Size = new System.Drawing.Size(195, 13);
            this.lb_y188_1_tower_lamp_y.TabIndex = 81;
            this.lb_y188_1_tower_lamp_y.Text = "[ Y188 ] 1 Tower Lamp \"Y\"";
            // 
            // pn_Loader_OUT1_Y188
            // 
            this.pn_Loader_OUT1_Y188.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT1_Y188.Location = new System.Drawing.Point(6, 396);
            this.pn_Loader_OUT1_Y188.Name = "pn_Loader_OUT1_Y188";
            this.pn_Loader_OUT1_Y188.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT1_Y188.TabIndex = 80;
            // 
            // lb_y187_1_tower_lamp_r
            // 
            this.lb_y187_1_tower_lamp_r.AutoSize = true;
            this.lb_y187_1_tower_lamp_r.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y187_1_tower_lamp_r.ForeColor = System.Drawing.Color.White;
            this.lb_y187_1_tower_lamp_r.Location = new System.Drawing.Point(52, 364);
            this.lb_y187_1_tower_lamp_r.Name = "lb_y187_1_tower_lamp_r";
            this.lb_y187_1_tower_lamp_r.Size = new System.Drawing.Size(196, 13);
            this.lb_y187_1_tower_lamp_r.TabIndex = 79;
            this.lb_y187_1_tower_lamp_r.Text = "[ Y187 ] 1 Tower Lamp \"R\"";
            // 
            // pn_Loader_OUT1_Y187
            // 
            this.pn_Loader_OUT1_Y187.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT1_Y187.Location = new System.Drawing.Point(6, 350);
            this.pn_Loader_OUT1_Y187.Name = "pn_Loader_OUT1_Y187";
            this.pn_Loader_OUT1_Y187.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT1_Y187.TabIndex = 78;
            // 
            // lb_y186_safty_reset
            // 
            this.lb_y186_safty_reset.AutoSize = true;
            this.lb_y186_safty_reset.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y186_safty_reset.ForeColor = System.Drawing.Color.White;
            this.lb_y186_safty_reset.Location = new System.Drawing.Point(52, 319);
            this.lb_y186_safty_reset.Name = "lb_y186_safty_reset";
            this.lb_y186_safty_reset.Size = new System.Drawing.Size(173, 13);
            this.lb_y186_safty_reset.TabIndex = 77;
            this.lb_y186_safty_reset.Text = "[ Y186 ] 안전회로 Reset";
            // 
            // pn_Loader_OUT1_Y186
            // 
            this.pn_Loader_OUT1_Y186.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT1_Y186.Location = new System.Drawing.Point(6, 304);
            this.pn_Loader_OUT1_Y186.Name = "pn_Loader_OUT1_Y186";
            this.pn_Loader_OUT1_Y186.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT1_Y186.TabIndex = 76;
            // 
            // lb_y185_spare
            // 
            this.lb_y185_spare.AutoSize = true;
            this.lb_y185_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y185_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y185_spare.Location = new System.Drawing.Point(52, 271);
            this.lb_y185_spare.Name = "lb_y185_spare";
            this.lb_y185_spare.Size = new System.Drawing.Size(114, 13);
            this.lb_y185_spare.TabIndex = 75;
            this.lb_y185_spare.Text = "[ Y185 ] Spare";
            // 
            // pn_Loader_OUT1_Y185
            // 
            this.pn_Loader_OUT1_Y185.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT1_Y185.Location = new System.Drawing.Point(6, 258);
            this.pn_Loader_OUT1_Y185.Name = "pn_Loader_OUT1_Y185";
            this.pn_Loader_OUT1_Y185.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT1_Y185.TabIndex = 74;
            // 
            // lb_y184_spare
            // 
            this.lb_y184_spare.AutoSize = true;
            this.lb_y184_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y184_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y184_spare.Location = new System.Drawing.Point(52, 226);
            this.lb_y184_spare.Name = "lb_y184_spare";
            this.lb_y184_spare.Size = new System.Drawing.Size(114, 13);
            this.lb_y184_spare.TabIndex = 73;
            this.lb_y184_spare.Text = "[ Y184 ] Spare";
            // 
            // pn_Loader_OUT1_Y184
            // 
            this.pn_Loader_OUT1_Y184.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT1_Y184.Location = new System.Drawing.Point(6, 212);
            this.pn_Loader_OUT1_Y184.Name = "pn_Loader_OUT1_Y184";
            this.pn_Loader_OUT1_Y184.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT1_Y184.TabIndex = 72;
            // 
            // lb_y183_spare
            // 
            this.lb_y183_spare.AutoSize = true;
            this.lb_y183_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y183_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y183_spare.Location = new System.Drawing.Point(52, 179);
            this.lb_y183_spare.Name = "lb_y183_spare";
            this.lb_y183_spare.Size = new System.Drawing.Size(114, 13);
            this.lb_y183_spare.TabIndex = 71;
            this.lb_y183_spare.Text = "[ Y183 ] Spare";
            // 
            // pn_Loader_OUT1_Y183
            // 
            this.pn_Loader_OUT1_Y183.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT1_Y183.Location = new System.Drawing.Point(6, 166);
            this.pn_Loader_OUT1_Y183.Name = "pn_Loader_OUT1_Y183";
            this.pn_Loader_OUT1_Y183.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT1_Y183.TabIndex = 70;
            // 
            // lb_y182_spare
            // 
            this.lb_y182_spare.AutoSize = true;
            this.lb_y182_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y182_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y182_spare.Location = new System.Drawing.Point(52, 133);
            this.lb_y182_spare.Name = "lb_y182_spare";
            this.lb_y182_spare.Size = new System.Drawing.Size(114, 13);
            this.lb_y182_spare.TabIndex = 69;
            this.lb_y182_spare.Text = "[ Y182 ] Spare";
            // 
            // pn_Loader_OUT1_Y182
            // 
            this.pn_Loader_OUT1_Y182.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT1_Y182.Location = new System.Drawing.Point(6, 120);
            this.pn_Loader_OUT1_Y182.Name = "pn_Loader_OUT1_Y182";
            this.pn_Loader_OUT1_Y182.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT1_Y182.TabIndex = 68;
            // 
            // lb_y181_laser_ld_motor_control_on_cmd
            // 
            this.lb_y181_laser_ld_motor_control_on_cmd.AutoSize = true;
            this.lb_y181_laser_ld_motor_control_on_cmd.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y181_laser_ld_motor_control_on_cmd.ForeColor = System.Drawing.Color.White;
            this.lb_y181_laser_ld_motor_control_on_cmd.Location = new System.Drawing.Point(52, 86);
            this.lb_y181_laser_ld_motor_control_on_cmd.Name = "lb_y181_laser_ld_motor_control_on_cmd";
            this.lb_y181_laser_ld_motor_control_on_cmd.Size = new System.Drawing.Size(299, 13);
            this.lb_y181_laser_ld_motor_control_on_cmd.TabIndex = 67;
            this.lb_y181_laser_ld_motor_control_on_cmd.Text = "[ Y181 ] 가공부 LD Motor Control On CMD";
            // 
            // pn_Loader_OUT1_Y181
            // 
            this.pn_Loader_OUT1_Y181.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT1_Y181.Location = new System.Drawing.Point(6, 74);
            this.pn_Loader_OUT1_Y181.Name = "pn_Loader_OUT1_Y181";
            this.pn_Loader_OUT1_Y181.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT1_Y181.TabIndex = 66;
            // 
            // lb_y180_ld_motor_control_on_cmd
            // 
            this.lb_y180_ld_motor_control_on_cmd.AutoSize = true;
            this.lb_y180_ld_motor_control_on_cmd.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y180_ld_motor_control_on_cmd.ForeColor = System.Drawing.Color.White;
            this.lb_y180_ld_motor_control_on_cmd.Location = new System.Drawing.Point(52, 42);
            this.lb_y180_ld_motor_control_on_cmd.Name = "lb_y180_ld_motor_control_on_cmd";
            this.lb_y180_ld_motor_control_on_cmd.Size = new System.Drawing.Size(252, 13);
            this.lb_y180_ld_motor_control_on_cmd.TabIndex = 65;
            this.lb_y180_ld_motor_control_on_cmd.Text = "[ Y180 ] LD Motor Control On CMD";
            // 
            // pn_Loader_OUT1_Y180
            // 
            this.pn_Loader_OUT1_Y180.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT1_Y180.Location = new System.Drawing.Point(6, 28);
            this.pn_Loader_OUT1_Y180.Name = "pn_Loader_OUT1_Y180";
            this.pn_Loader_OUT1_Y180.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT1_Y180.TabIndex = 64;
            // 
            // tp_iostatus_ld_out2
            // 
            this.tp_iostatus_ld_out2.BackColor = System.Drawing.Color.DimGray;
            this.tp_iostatus_ld_out2.Controls.Add(this.lb_y1bf_spare_air_on_cmd);
            this.tp_iostatus_ld_out2.Controls.Add(this.pn_Loader_OUT2_Y1BF);
            this.tp_iostatus_ld_out2.Controls.Add(this.lb_y1be_spare_air_on_cmd);
            this.tp_iostatus_ld_out2.Controls.Add(this.pn_Loader_OUT2_Y1BE);
            this.tp_iostatus_ld_out2.Controls.Add(this.lb_y1bd_2_cst_trans_air_on);
            this.tp_iostatus_ld_out2.Controls.Add(this.pn_Loader_OUT2_Y1BD);
            this.tp_iostatus_ld_out2.Controls.Add(this.lb_y1bc_2_cst_trans_vacuum_off);
            this.tp_iostatus_ld_out2.Controls.Add(this.pn_Loader_OUT2_Y1BC);
            this.tp_iostatus_ld_out2.Controls.Add(this.lb_y1bb_2_cst_trans_vacuum_on);
            this.tp_iostatus_ld_out2.Controls.Add(this.pn_Loader_OUT2_Y1BB);
            this.tp_iostatus_ld_out2.Controls.Add(this.lb_y1ba_1_cst_trans_air_on);
            this.tp_iostatus_ld_out2.Controls.Add(this.pn_Loader_OUT2_Y1BA);
            this.tp_iostatus_ld_out2.Controls.Add(this.lb_y1b9_1_cst_trans_vacuum_off);
            this.tp_iostatus_ld_out2.Controls.Add(this.pn_Loader_OUT2_Y1B9);
            this.tp_iostatus_ld_out2.Controls.Add(this.lb_y1b8_1_cst_trans_vacuum_on);
            this.tp_iostatus_ld_out2.Controls.Add(this.pn_Loader_OUT2_Y1B8);
            this.tp_iostatus_ld_out2.Controls.Add(this.lb_y1b6_2_ld_cst_lift_tiltsensor_down);
            this.tp_iostatus_ld_out2.Controls.Add(this.pn_Loader_OUT2_Y1B7);
            this.tp_iostatus_ld_out2.Controls.Add(this.lb_y1b6_2_ld_cst_lift_tiltsensor_up);
            this.tp_iostatus_ld_out2.Controls.Add(this.pn_Loader_OUT2_Y1B6);
            this.tp_iostatus_ld_out2.Controls.Add(this.lb_y1b5_2_ld_trans_2ch_air_on);
            this.tp_iostatus_ld_out2.Controls.Add(this.pn_Loader_OUT2_Y1B5);
            this.tp_iostatus_ld_out2.Controls.Add(this.lb_y1b4_2_ld_trans_2ch_vacuum_off);
            this.tp_iostatus_ld_out2.Controls.Add(this.pn_Loader_OUT2_Y1B4);
            this.tp_iostatus_ld_out2.Controls.Add(this.lb_y1b3_2_ld_trans_2ch_vacuum_on);
            this.tp_iostatus_ld_out2.Controls.Add(this.pn_Loader_OUT2_Y1B3);
            this.tp_iostatus_ld_out2.Controls.Add(this.lb_y1b2_2_ld_trans_1ch_air_on);
            this.tp_iostatus_ld_out2.Controls.Add(this.pn_Loader_OUT2_Y1B2);
            this.tp_iostatus_ld_out2.Controls.Add(this.lb_y1b1_2_ld_trans_1ch_vacuum_off);
            this.tp_iostatus_ld_out2.Controls.Add(this.pn_Loader_OUT2_Y1B1);
            this.tp_iostatus_ld_out2.Controls.Add(this.lb_y1b0_2_lld_trans_1ch_vacuum_on);
            this.tp_iostatus_ld_out2.Controls.Add(this.pn_Loader_OUT2_Y1B0);
            this.tp_iostatus_ld_out2.Controls.Add(this.lb_y1af_1_ld_trans_2ch_air_on);
            this.tp_iostatus_ld_out2.Controls.Add(this.pn_Loader_OUT2_Y1AF);
            this.tp_iostatus_ld_out2.Controls.Add(this.lb_y1ae_1_ld_trans_2ch_vacuum_off);
            this.tp_iostatus_ld_out2.Controls.Add(this.pn_Loader_OUT2_Y1AE);
            this.tp_iostatus_ld_out2.Controls.Add(this.lb_y1ad_1_ld_trans_2ch_vacuum_on);
            this.tp_iostatus_ld_out2.Controls.Add(this.pn_Loader_OUT2_Y1AD);
            this.tp_iostatus_ld_out2.Controls.Add(this.lb_y1ac_1_ld_trans_1ch_air_on);
            this.tp_iostatus_ld_out2.Controls.Add(this.pn_Loader_OUT2_Y1AC);
            this.tp_iostatus_ld_out2.Controls.Add(this.lb_y1ab_1_ld_trans_1ch_vacuum_off);
            this.tp_iostatus_ld_out2.Controls.Add(this.pn_Loader_OUT2_Y1AB);
            this.tp_iostatus_ld_out2.Controls.Add(this.lb_y1aa_1_ld_trans_1ch_vacuum_on);
            this.tp_iostatus_ld_out2.Controls.Add(this.pn_Loader_OUT2_Y1AA);
            this.tp_iostatus_ld_out2.Controls.Add(this.lb_y1a9_2_ld_trans_down);
            this.tp_iostatus_ld_out2.Controls.Add(this.pn_Loader_OUT2_Y1A9);
            this.tp_iostatus_ld_out2.Controls.Add(this.lb_y1a8_2_ld_trans_up);
            this.tp_iostatus_ld_out2.Controls.Add(this.pn_Loader_OUT2_Y1A8);
            this.tp_iostatus_ld_out2.Controls.Add(this.lb_y1a7_1_ld_trans_down);
            this.tp_iostatus_ld_out2.Controls.Add(this.pn_Loader_OUT2_Y1A7);
            this.tp_iostatus_ld_out2.Controls.Add(this.lb_y1a6_1_ld_trans_up);
            this.tp_iostatus_ld_out2.Controls.Add(this.pn_Loader_OUT2_Y1A6);
            this.tp_iostatus_ld_out2.Controls.Add(this.lb_y1a5_1_ld_cst_lift_tiltsensor_down);
            this.tp_iostatus_ld_out2.Controls.Add(this.pn_Loader_OUT2_Y1A5);
            this.tp_iostatus_ld_out2.Controls.Add(this.lb_y1a4_1_ld_cst_lift_tiltsensor_up);
            this.tp_iostatus_ld_out2.Controls.Add(this.pn_Loader_OUT2_Y1A4);
            this.tp_iostatus_ld_out2.Controls.Add(this.lb_y1a3_2_ld_cst_lifter_ungrip);
            this.tp_iostatus_ld_out2.Controls.Add(this.pn_Loader_OUT2_Y1A3);
            this.tp_iostatus_ld_out2.Controls.Add(this.lb_y1a2_2_ld_cst_lifter_grip);
            this.tp_iostatus_ld_out2.Controls.Add(this.pn_Loader_OUT2_Y1A2);
            this.tp_iostatus_ld_out2.Controls.Add(this.lb_y1a1_1_ld_cst_lifter_ungrip);
            this.tp_iostatus_ld_out2.Controls.Add(this.pn_Loader_OUT2_Y1A1);
            this.tp_iostatus_ld_out2.Controls.Add(this.lb_y1a0_1_ld_cst_lifter_grip);
            this.tp_iostatus_ld_out2.Controls.Add(this.pn_Loader_OUT2_Y1A0);
            this.tp_iostatus_ld_out2.Location = new System.Drawing.Point(4, 34);
            this.tp_iostatus_ld_out2.Name = "tp_iostatus_ld_out2";
            this.tp_iostatus_ld_out2.Padding = new System.Windows.Forms.Padding(3);
            this.tp_iostatus_ld_out2.Size = new System.Drawing.Size(846, 778);
            this.tp_iostatus_ld_out2.TabIndex = 1;
            this.tp_iostatus_ld_out2.Text = "OUT - 2";
            // 
            // lb_y1bf_spare_air_on_cmd
            // 
            this.lb_y1bf_spare_air_on_cmd.AutoSize = true;
            this.lb_y1bf_spare_air_on_cmd.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1bf_spare_air_on_cmd.ForeColor = System.Drawing.Color.White;
            this.lb_y1bf_spare_air_on_cmd.Location = new System.Drawing.Point(479, 730);
            this.lb_y1bf_spare_air_on_cmd.Name = "lb_y1bf_spare_air_on_cmd";
            this.lb_y1bf_spare_air_on_cmd.Size = new System.Drawing.Size(240, 13);
            this.lb_y1bf_spare_air_on_cmd.TabIndex = 191;
            this.lb_y1bf_spare_air_on_cmd.Text = "[ Y1BF ] Spare 파기 AIR ON CMD";
            // 
            // pn_Loader_OUT2_Y1BF
            // 
            this.pn_Loader_OUT2_Y1BF.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT2_Y1BF.Location = new System.Drawing.Point(433, 718);
            this.pn_Loader_OUT2_Y1BF.Name = "pn_Loader_OUT2_Y1BF";
            this.pn_Loader_OUT2_Y1BF.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT2_Y1BF.TabIndex = 190;
            // 
            // lb_y1be_spare_air_on_cmd
            // 
            this.lb_y1be_spare_air_on_cmd.AutoSize = true;
            this.lb_y1be_spare_air_on_cmd.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1be_spare_air_on_cmd.ForeColor = System.Drawing.Color.White;
            this.lb_y1be_spare_air_on_cmd.Location = new System.Drawing.Point(479, 685);
            this.lb_y1be_spare_air_on_cmd.Name = "lb_y1be_spare_air_on_cmd";
            this.lb_y1be_spare_air_on_cmd.Size = new System.Drawing.Size(241, 13);
            this.lb_y1be_spare_air_on_cmd.TabIndex = 189;
            this.lb_y1be_spare_air_on_cmd.Text = "[ Y1BE ] Spare 파기 AIR ON CMD";
            // 
            // pn_Loader_OUT2_Y1BE
            // 
            this.pn_Loader_OUT2_Y1BE.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT2_Y1BE.Location = new System.Drawing.Point(433, 672);
            this.pn_Loader_OUT2_Y1BE.Name = "pn_Loader_OUT2_Y1BE";
            this.pn_Loader_OUT2_Y1BE.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT2_Y1BE.TabIndex = 188;
            // 
            // lb_y1bd_2_cst_trans_air_on
            // 
            this.lb_y1bd_2_cst_trans_air_on.AutoSize = true;
            this.lb_y1bd_2_cst_trans_air_on.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1bd_2_cst_trans_air_on.ForeColor = System.Drawing.Color.White;
            this.lb_y1bd_2_cst_trans_air_on.Location = new System.Drawing.Point(479, 640);
            this.lb_y1bd_2_cst_trans_air_on.Name = "lb_y1bd_2_cst_trans_air_on";
            this.lb_y1bd_2_cst_trans_air_on.Size = new System.Drawing.Size(318, 13);
            this.lb_y1bd_2_cst_trans_air_on.TabIndex = 187;
            this.lb_y1bd_2_cst_trans_air_on.Text = "[ Y1BD ] 2 Cassette 취출 이재기 파기 AIR ON";
            // 
            // pn_Loader_OUT2_Y1BD
            // 
            this.pn_Loader_OUT2_Y1BD.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT2_Y1BD.Location = new System.Drawing.Point(433, 626);
            this.pn_Loader_OUT2_Y1BD.Name = "pn_Loader_OUT2_Y1BD";
            this.pn_Loader_OUT2_Y1BD.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT2_Y1BD.TabIndex = 186;
            // 
            // lb_y1bc_2_cst_trans_vacuum_off
            // 
            this.lb_y1bc_2_cst_trans_vacuum_off.AutoSize = true;
            this.lb_y1bc_2_cst_trans_vacuum_off.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1bc_2_cst_trans_vacuum_off.ForeColor = System.Drawing.Color.White;
            this.lb_y1bc_2_cst_trans_vacuum_off.Location = new System.Drawing.Point(479, 594);
            this.lb_y1bc_2_cst_trans_vacuum_off.Name = "lb_y1bc_2_cst_trans_vacuum_off";
            this.lb_y1bc_2_cst_trans_vacuum_off.Size = new System.Drawing.Size(295, 13);
            this.lb_y1bc_2_cst_trans_vacuum_off.TabIndex = 185;
            this.lb_y1bc_2_cst_trans_vacuum_off.Text = "[ Y1BC ] 2 Cassette 취출 이재기 Vac OFF";
            // 
            // pn_Loader_OUT2_Y1BC
            // 
            this.pn_Loader_OUT2_Y1BC.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT2_Y1BC.Location = new System.Drawing.Point(433, 580);
            this.pn_Loader_OUT2_Y1BC.Name = "pn_Loader_OUT2_Y1BC";
            this.pn_Loader_OUT2_Y1BC.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT2_Y1BC.TabIndex = 184;
            // 
            // lb_y1bb_2_cst_trans_vacuum_on
            // 
            this.lb_y1bb_2_cst_trans_vacuum_on.AutoSize = true;
            this.lb_y1bb_2_cst_trans_vacuum_on.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1bb_2_cst_trans_vacuum_on.ForeColor = System.Drawing.Color.White;
            this.lb_y1bb_2_cst_trans_vacuum_on.Location = new System.Drawing.Point(479, 548);
            this.lb_y1bb_2_cst_trans_vacuum_on.Name = "lb_y1bb_2_cst_trans_vacuum_on";
            this.lb_y1bb_2_cst_trans_vacuum_on.Size = new System.Drawing.Size(289, 13);
            this.lb_y1bb_2_cst_trans_vacuum_on.TabIndex = 183;
            this.lb_y1bb_2_cst_trans_vacuum_on.Text = "[ Y1BB ] 2 Cassette 취출 이재기 Vac ON";
            // 
            // pn_Loader_OUT2_Y1BB
            // 
            this.pn_Loader_OUT2_Y1BB.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT2_Y1BB.Location = new System.Drawing.Point(433, 534);
            this.pn_Loader_OUT2_Y1BB.Name = "pn_Loader_OUT2_Y1BB";
            this.pn_Loader_OUT2_Y1BB.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT2_Y1BB.TabIndex = 182;
            // 
            // lb_y1ba_1_cst_trans_air_on
            // 
            this.lb_y1ba_1_cst_trans_air_on.AutoSize = true;
            this.lb_y1ba_1_cst_trans_air_on.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1ba_1_cst_trans_air_on.ForeColor = System.Drawing.Color.White;
            this.lb_y1ba_1_cst_trans_air_on.Location = new System.Drawing.Point(479, 501);
            this.lb_y1ba_1_cst_trans_air_on.Name = "lb_y1ba_1_cst_trans_air_on";
            this.lb_y1ba_1_cst_trans_air_on.Size = new System.Drawing.Size(317, 13);
            this.lb_y1ba_1_cst_trans_air_on.TabIndex = 181;
            this.lb_y1ba_1_cst_trans_air_on.Text = "[ Y1BA ] 1 Cassette 취출 이재기 파기 AIR ON";
            // 
            // pn_Loader_OUT2_Y1BA
            // 
            this.pn_Loader_OUT2_Y1BA.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT2_Y1BA.Location = new System.Drawing.Point(433, 488);
            this.pn_Loader_OUT2_Y1BA.Name = "pn_Loader_OUT2_Y1BA";
            this.pn_Loader_OUT2_Y1BA.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT2_Y1BA.TabIndex = 180;
            // 
            // lb_y1b9_1_cst_trans_vacuum_off
            // 
            this.lb_y1b9_1_cst_trans_vacuum_off.AutoSize = true;
            this.lb_y1b9_1_cst_trans_vacuum_off.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1b9_1_cst_trans_vacuum_off.ForeColor = System.Drawing.Color.White;
            this.lb_y1b9_1_cst_trans_vacuum_off.Location = new System.Drawing.Point(479, 454);
            this.lb_y1b9_1_cst_trans_vacuum_off.Name = "lb_y1b9_1_cst_trans_vacuum_off";
            this.lb_y1b9_1_cst_trans_vacuum_off.Size = new System.Drawing.Size(293, 13);
            this.lb_y1b9_1_cst_trans_vacuum_off.TabIndex = 179;
            this.lb_y1b9_1_cst_trans_vacuum_off.Text = "[ Y1B9 ] 1 Cassette 취출 이재기 Vac OFF";
            // 
            // pn_Loader_OUT2_Y1B9
            // 
            this.pn_Loader_OUT2_Y1B9.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT2_Y1B9.Location = new System.Drawing.Point(433, 442);
            this.pn_Loader_OUT2_Y1B9.Name = "pn_Loader_OUT2_Y1B9";
            this.pn_Loader_OUT2_Y1B9.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT2_Y1B9.TabIndex = 178;
            // 
            // lb_y1b8_1_cst_trans_vacuum_on
            // 
            this.lb_y1b8_1_cst_trans_vacuum_on.AutoSize = true;
            this.lb_y1b8_1_cst_trans_vacuum_on.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1b8_1_cst_trans_vacuum_on.ForeColor = System.Drawing.Color.White;
            this.lb_y1b8_1_cst_trans_vacuum_on.Location = new System.Drawing.Point(479, 409);
            this.lb_y1b8_1_cst_trans_vacuum_on.Name = "lb_y1b8_1_cst_trans_vacuum_on";
            this.lb_y1b8_1_cst_trans_vacuum_on.Size = new System.Drawing.Size(287, 13);
            this.lb_y1b8_1_cst_trans_vacuum_on.TabIndex = 177;
            this.lb_y1b8_1_cst_trans_vacuum_on.Text = "[ Y1B8 ] 1 Cassette 취출 이재기 Vac ON";
            // 
            // pn_Loader_OUT2_Y1B8
            // 
            this.pn_Loader_OUT2_Y1B8.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT2_Y1B8.Location = new System.Drawing.Point(433, 396);
            this.pn_Loader_OUT2_Y1B8.Name = "pn_Loader_OUT2_Y1B8";
            this.pn_Loader_OUT2_Y1B8.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT2_Y1B8.TabIndex = 176;
            // 
            // lb_y1b6_2_ld_cst_lift_tiltsensor_down
            // 
            this.lb_y1b6_2_ld_cst_lift_tiltsensor_down.AutoSize = true;
            this.lb_y1b6_2_ld_cst_lift_tiltsensor_down.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1b6_2_ld_cst_lift_tiltsensor_down.ForeColor = System.Drawing.Color.White;
            this.lb_y1b6_2_ld_cst_lift_tiltsensor_down.Location = new System.Drawing.Point(479, 364);
            this.lb_y1b6_2_ld_cst_lift_tiltsensor_down.Name = "lb_y1b6_2_ld_cst_lift_tiltsensor_down";
            this.lb_y1b6_2_ld_cst_lift_tiltsensor_down.Size = new System.Drawing.Size(323, 13);
            this.lb_y1b6_2_ld_cst_lift_tiltsensor_down.TabIndex = 175;
            this.lb_y1b6_2_ld_cst_lift_tiltsensor_down.Text = "[ Y1B7 ] 2 L/D Casset Lift Tilt Sensor DOWN";
            // 
            // pn_Loader_OUT2_Y1B7
            // 
            this.pn_Loader_OUT2_Y1B7.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT2_Y1B7.Location = new System.Drawing.Point(433, 350);
            this.pn_Loader_OUT2_Y1B7.Name = "pn_Loader_OUT2_Y1B7";
            this.pn_Loader_OUT2_Y1B7.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT2_Y1B7.TabIndex = 174;
            // 
            // lb_y1b6_2_ld_cst_lift_tiltsensor_up
            // 
            this.lb_y1b6_2_ld_cst_lift_tiltsensor_up.AutoSize = true;
            this.lb_y1b6_2_ld_cst_lift_tiltsensor_up.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1b6_2_ld_cst_lift_tiltsensor_up.ForeColor = System.Drawing.Color.White;
            this.lb_y1b6_2_ld_cst_lift_tiltsensor_up.Location = new System.Drawing.Point(479, 319);
            this.lb_y1b6_2_ld_cst_lift_tiltsensor_up.Name = "lb_y1b6_2_ld_cst_lift_tiltsensor_up";
            this.lb_y1b6_2_ld_cst_lift_tiltsensor_up.Size = new System.Drawing.Size(299, 13);
            this.lb_y1b6_2_ld_cst_lift_tiltsensor_up.TabIndex = 173;
            this.lb_y1b6_2_ld_cst_lift_tiltsensor_up.Text = "[ Y1B6 ] 2 L/D Casset Lift Tilt Sensor UP";
            // 
            // pn_Loader_OUT2_Y1B6
            // 
            this.pn_Loader_OUT2_Y1B6.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT2_Y1B6.Location = new System.Drawing.Point(433, 304);
            this.pn_Loader_OUT2_Y1B6.Name = "pn_Loader_OUT2_Y1B6";
            this.pn_Loader_OUT2_Y1B6.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT2_Y1B6.TabIndex = 172;
            // 
            // lb_y1b5_2_ld_trans_2ch_air_on
            // 
            this.lb_y1b5_2_ld_trans_2ch_air_on.AutoSize = true;
            this.lb_y1b5_2_ld_trans_2ch_air_on.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1b5_2_ld_trans_2ch_air_on.ForeColor = System.Drawing.Color.White;
            this.lb_y1b5_2_ld_trans_2ch_air_on.Location = new System.Drawing.Point(479, 271);
            this.lb_y1b5_2_ld_trans_2ch_air_on.Name = "lb_y1b5_2_ld_trans_2ch_air_on";
            this.lb_y1b5_2_ld_trans_2ch_air_on.Size = new System.Drawing.Size(276, 13);
            this.lb_y1b5_2_ld_trans_2ch_air_on.TabIndex = 171;
            this.lb_y1b5_2_ld_trans_2ch_air_on.Text = "[ Y1B5 ] 2 L/D 이재기 2ch 파기 AIR ON";
            // 
            // pn_Loader_OUT2_Y1B5
            // 
            this.pn_Loader_OUT2_Y1B5.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT2_Y1B5.Location = new System.Drawing.Point(433, 258);
            this.pn_Loader_OUT2_Y1B5.Name = "pn_Loader_OUT2_Y1B5";
            this.pn_Loader_OUT2_Y1B5.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT2_Y1B5.TabIndex = 170;
            // 
            // lb_y1b4_2_ld_trans_2ch_vacuum_off
            // 
            this.lb_y1b4_2_ld_trans_2ch_vacuum_off.AutoSize = true;
            this.lb_y1b4_2_ld_trans_2ch_vacuum_off.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1b4_2_ld_trans_2ch_vacuum_off.ForeColor = System.Drawing.Color.White;
            this.lb_y1b4_2_ld_trans_2ch_vacuum_off.Location = new System.Drawing.Point(479, 226);
            this.lb_y1b4_2_ld_trans_2ch_vacuum_off.Name = "lb_y1b4_2_ld_trans_2ch_vacuum_off";
            this.lb_y1b4_2_ld_trans_2ch_vacuum_off.Size = new System.Drawing.Size(253, 13);
            this.lb_y1b4_2_ld_trans_2ch_vacuum_off.TabIndex = 169;
            this.lb_y1b4_2_ld_trans_2ch_vacuum_off.Text = "[ Y1B4 ] 2 L/D 이재기 2ch Vac OFF";
            // 
            // pn_Loader_OUT2_Y1B4
            // 
            this.pn_Loader_OUT2_Y1B4.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT2_Y1B4.Location = new System.Drawing.Point(433, 212);
            this.pn_Loader_OUT2_Y1B4.Name = "pn_Loader_OUT2_Y1B4";
            this.pn_Loader_OUT2_Y1B4.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT2_Y1B4.TabIndex = 168;
            // 
            // lb_y1b3_2_ld_trans_2ch_vacuum_on
            // 
            this.lb_y1b3_2_ld_trans_2ch_vacuum_on.AutoSize = true;
            this.lb_y1b3_2_ld_trans_2ch_vacuum_on.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1b3_2_ld_trans_2ch_vacuum_on.ForeColor = System.Drawing.Color.White;
            this.lb_y1b3_2_ld_trans_2ch_vacuum_on.Location = new System.Drawing.Point(479, 179);
            this.lb_y1b3_2_ld_trans_2ch_vacuum_on.Name = "lb_y1b3_2_ld_trans_2ch_vacuum_on";
            this.lb_y1b3_2_ld_trans_2ch_vacuum_on.Size = new System.Drawing.Size(238, 13);
            this.lb_y1b3_2_ld_trans_2ch_vacuum_on.TabIndex = 167;
            this.lb_y1b3_2_ld_trans_2ch_vacuum_on.Text = "[ Y1B3 ] 2 L/D 이재기 2h Vac ON";
            // 
            // pn_Loader_OUT2_Y1B3
            // 
            this.pn_Loader_OUT2_Y1B3.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT2_Y1B3.Location = new System.Drawing.Point(433, 166);
            this.pn_Loader_OUT2_Y1B3.Name = "pn_Loader_OUT2_Y1B3";
            this.pn_Loader_OUT2_Y1B3.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT2_Y1B3.TabIndex = 166;
            // 
            // lb_y1b2_2_ld_trans_1ch_air_on
            // 
            this.lb_y1b2_2_ld_trans_1ch_air_on.AutoSize = true;
            this.lb_y1b2_2_ld_trans_1ch_air_on.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1b2_2_ld_trans_1ch_air_on.ForeColor = System.Drawing.Color.White;
            this.lb_y1b2_2_ld_trans_1ch_air_on.Location = new System.Drawing.Point(479, 133);
            this.lb_y1b2_2_ld_trans_1ch_air_on.Name = "lb_y1b2_2_ld_trans_1ch_air_on";
            this.lb_y1b2_2_ld_trans_1ch_air_on.Size = new System.Drawing.Size(276, 13);
            this.lb_y1b2_2_ld_trans_1ch_air_on.TabIndex = 165;
            this.lb_y1b2_2_ld_trans_1ch_air_on.Text = "[ Y1B2 ] 2 L/D 이재기 1ch 파기 AIR ON";
            // 
            // pn_Loader_OUT2_Y1B2
            // 
            this.pn_Loader_OUT2_Y1B2.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT2_Y1B2.Location = new System.Drawing.Point(433, 120);
            this.pn_Loader_OUT2_Y1B2.Name = "pn_Loader_OUT2_Y1B2";
            this.pn_Loader_OUT2_Y1B2.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT2_Y1B2.TabIndex = 164;
            // 
            // lb_y1b1_2_ld_trans_1ch_vacuum_off
            // 
            this.lb_y1b1_2_ld_trans_1ch_vacuum_off.AutoSize = true;
            this.lb_y1b1_2_ld_trans_1ch_vacuum_off.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1b1_2_ld_trans_1ch_vacuum_off.ForeColor = System.Drawing.Color.White;
            this.lb_y1b1_2_ld_trans_1ch_vacuum_off.Location = new System.Drawing.Point(479, 86);
            this.lb_y1b1_2_ld_trans_1ch_vacuum_off.Name = "lb_y1b1_2_ld_trans_1ch_vacuum_off";
            this.lb_y1b1_2_ld_trans_1ch_vacuum_off.Size = new System.Drawing.Size(253, 13);
            this.lb_y1b1_2_ld_trans_1ch_vacuum_off.TabIndex = 163;
            this.lb_y1b1_2_ld_trans_1ch_vacuum_off.Text = "[ Y1B1 ] 2 L/D 이재기 1ch Vac OFF";
            // 
            // pn_Loader_OUT2_Y1B1
            // 
            this.pn_Loader_OUT2_Y1B1.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT2_Y1B1.Location = new System.Drawing.Point(433, 74);
            this.pn_Loader_OUT2_Y1B1.Name = "pn_Loader_OUT2_Y1B1";
            this.pn_Loader_OUT2_Y1B1.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT2_Y1B1.TabIndex = 162;
            // 
            // lb_y1b0_2_lld_trans_1ch_vacuum_on
            // 
            this.lb_y1b0_2_lld_trans_1ch_vacuum_on.AutoSize = true;
            this.lb_y1b0_2_lld_trans_1ch_vacuum_on.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1b0_2_lld_trans_1ch_vacuum_on.ForeColor = System.Drawing.Color.White;
            this.lb_y1b0_2_lld_trans_1ch_vacuum_on.Location = new System.Drawing.Point(479, 42);
            this.lb_y1b0_2_lld_trans_1ch_vacuum_on.Name = "lb_y1b0_2_lld_trans_1ch_vacuum_on";
            this.lb_y1b0_2_lld_trans_1ch_vacuum_on.Size = new System.Drawing.Size(247, 13);
            this.lb_y1b0_2_lld_trans_1ch_vacuum_on.TabIndex = 161;
            this.lb_y1b0_2_lld_trans_1ch_vacuum_on.Text = "[ Y1B0 ] 2 L/D 이재기 1ch Vac ON";
            // 
            // pn_Loader_OUT2_Y1B0
            // 
            this.pn_Loader_OUT2_Y1B0.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT2_Y1B0.Location = new System.Drawing.Point(433, 28);
            this.pn_Loader_OUT2_Y1B0.Name = "pn_Loader_OUT2_Y1B0";
            this.pn_Loader_OUT2_Y1B0.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT2_Y1B0.TabIndex = 160;
            // 
            // lb_y1af_1_ld_trans_2ch_air_on
            // 
            this.lb_y1af_1_ld_trans_2ch_air_on.AutoSize = true;
            this.lb_y1af_1_ld_trans_2ch_air_on.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1af_1_ld_trans_2ch_air_on.ForeColor = System.Drawing.Color.White;
            this.lb_y1af_1_ld_trans_2ch_air_on.Location = new System.Drawing.Point(52, 730);
            this.lb_y1af_1_ld_trans_2ch_air_on.Name = "lb_y1af_1_ld_trans_2ch_air_on";
            this.lb_y1af_1_ld_trans_2ch_air_on.Size = new System.Drawing.Size(275, 13);
            this.lb_y1af_1_ld_trans_2ch_air_on.TabIndex = 159;
            this.lb_y1af_1_ld_trans_2ch_air_on.Text = "[ Y1AF ] 1 L/D 이재기 2ch 파기 AIR ON";
            // 
            // pn_Loader_OUT2_Y1AF
            // 
            this.pn_Loader_OUT2_Y1AF.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT2_Y1AF.Location = new System.Drawing.Point(6, 718);
            this.pn_Loader_OUT2_Y1AF.Name = "pn_Loader_OUT2_Y1AF";
            this.pn_Loader_OUT2_Y1AF.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT2_Y1AF.TabIndex = 158;
            // 
            // lb_y1ae_1_ld_trans_2ch_vacuum_off
            // 
            this.lb_y1ae_1_ld_trans_2ch_vacuum_off.AutoSize = true;
            this.lb_y1ae_1_ld_trans_2ch_vacuum_off.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1ae_1_ld_trans_2ch_vacuum_off.ForeColor = System.Drawing.Color.White;
            this.lb_y1ae_1_ld_trans_2ch_vacuum_off.Location = new System.Drawing.Point(52, 685);
            this.lb_y1ae_1_ld_trans_2ch_vacuum_off.Name = "lb_y1ae_1_ld_trans_2ch_vacuum_off";
            this.lb_y1ae_1_ld_trans_2ch_vacuum_off.Size = new System.Drawing.Size(253, 13);
            this.lb_y1ae_1_ld_trans_2ch_vacuum_off.TabIndex = 157;
            this.lb_y1ae_1_ld_trans_2ch_vacuum_off.Text = "[ Y1AE ] 1 L/D 이재기 2ch Vac OFF";
            // 
            // pn_Loader_OUT2_Y1AE
            // 
            this.pn_Loader_OUT2_Y1AE.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT2_Y1AE.Location = new System.Drawing.Point(6, 672);
            this.pn_Loader_OUT2_Y1AE.Name = "pn_Loader_OUT2_Y1AE";
            this.pn_Loader_OUT2_Y1AE.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT2_Y1AE.TabIndex = 156;
            // 
            // lb_y1ad_1_ld_trans_2ch_vacuum_on
            // 
            this.lb_y1ad_1_ld_trans_2ch_vacuum_on.AutoSize = true;
            this.lb_y1ad_1_ld_trans_2ch_vacuum_on.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1ad_1_ld_trans_2ch_vacuum_on.ForeColor = System.Drawing.Color.White;
            this.lb_y1ad_1_ld_trans_2ch_vacuum_on.Location = new System.Drawing.Point(52, 640);
            this.lb_y1ad_1_ld_trans_2ch_vacuum_on.Name = "lb_y1ad_1_ld_trans_2ch_vacuum_on";
            this.lb_y1ad_1_ld_trans_2ch_vacuum_on.Size = new System.Drawing.Size(248, 13);
            this.lb_y1ad_1_ld_trans_2ch_vacuum_on.TabIndex = 155;
            this.lb_y1ad_1_ld_trans_2ch_vacuum_on.Text = "[ Y1AD ] 1 L/D 이재기 2ch Vac ON";
            // 
            // pn_Loader_OUT2_Y1AD
            // 
            this.pn_Loader_OUT2_Y1AD.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT2_Y1AD.Location = new System.Drawing.Point(6, 626);
            this.pn_Loader_OUT2_Y1AD.Name = "pn_Loader_OUT2_Y1AD";
            this.pn_Loader_OUT2_Y1AD.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT2_Y1AD.TabIndex = 154;
            // 
            // lb_y1ac_1_ld_trans_1ch_air_on
            // 
            this.lb_y1ac_1_ld_trans_1ch_air_on.AutoSize = true;
            this.lb_y1ac_1_ld_trans_1ch_air_on.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1ac_1_ld_trans_1ch_air_on.ForeColor = System.Drawing.Color.White;
            this.lb_y1ac_1_ld_trans_1ch_air_on.Location = new System.Drawing.Point(52, 594);
            this.lb_y1ac_1_ld_trans_1ch_air_on.Name = "lb_y1ac_1_ld_trans_1ch_air_on";
            this.lb_y1ac_1_ld_trans_1ch_air_on.Size = new System.Drawing.Size(277, 13);
            this.lb_y1ac_1_ld_trans_1ch_air_on.TabIndex = 153;
            this.lb_y1ac_1_ld_trans_1ch_air_on.Text = "[ Y1AC ] 1 L/D 이재기 1ch 파기 AIR ON";
            // 
            // pn_Loader_OUT2_Y1AC
            // 
            this.pn_Loader_OUT2_Y1AC.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT2_Y1AC.Location = new System.Drawing.Point(6, 580);
            this.pn_Loader_OUT2_Y1AC.Name = "pn_Loader_OUT2_Y1AC";
            this.pn_Loader_OUT2_Y1AC.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT2_Y1AC.TabIndex = 152;
            // 
            // lb_y1ab_1_ld_trans_1ch_vacuum_off
            // 
            this.lb_y1ab_1_ld_trans_1ch_vacuum_off.AutoSize = true;
            this.lb_y1ab_1_ld_trans_1ch_vacuum_off.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1ab_1_ld_trans_1ch_vacuum_off.ForeColor = System.Drawing.Color.White;
            this.lb_y1ab_1_ld_trans_1ch_vacuum_off.Location = new System.Drawing.Point(52, 548);
            this.lb_y1ab_1_ld_trans_1ch_vacuum_off.Name = "lb_y1ab_1_ld_trans_1ch_vacuum_off";
            this.lb_y1ab_1_ld_trans_1ch_vacuum_off.Size = new System.Drawing.Size(254, 13);
            this.lb_y1ab_1_ld_trans_1ch_vacuum_off.TabIndex = 151;
            this.lb_y1ab_1_ld_trans_1ch_vacuum_off.Text = "[ Y1AB ] 1 L/D 이재기 1ch Vac OFF";
            // 
            // pn_Loader_OUT2_Y1AB
            // 
            this.pn_Loader_OUT2_Y1AB.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT2_Y1AB.Location = new System.Drawing.Point(6, 534);
            this.pn_Loader_OUT2_Y1AB.Name = "pn_Loader_OUT2_Y1AB";
            this.pn_Loader_OUT2_Y1AB.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT2_Y1AB.TabIndex = 150;
            // 
            // lb_y1aa_1_ld_trans_1ch_vacuum_on
            // 
            this.lb_y1aa_1_ld_trans_1ch_vacuum_on.AutoSize = true;
            this.lb_y1aa_1_ld_trans_1ch_vacuum_on.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1aa_1_ld_trans_1ch_vacuum_on.ForeColor = System.Drawing.Color.White;
            this.lb_y1aa_1_ld_trans_1ch_vacuum_on.Location = new System.Drawing.Point(52, 501);
            this.lb_y1aa_1_ld_trans_1ch_vacuum_on.Name = "lb_y1aa_1_ld_trans_1ch_vacuum_on";
            this.lb_y1aa_1_ld_trans_1ch_vacuum_on.Size = new System.Drawing.Size(247, 13);
            this.lb_y1aa_1_ld_trans_1ch_vacuum_on.TabIndex = 149;
            this.lb_y1aa_1_ld_trans_1ch_vacuum_on.Text = "[ Y1AA ] 1 L/D 이재기 1ch Vac ON";
            // 
            // pn_Loader_OUT2_Y1AA
            // 
            this.pn_Loader_OUT2_Y1AA.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT2_Y1AA.Location = new System.Drawing.Point(6, 488);
            this.pn_Loader_OUT2_Y1AA.Name = "pn_Loader_OUT2_Y1AA";
            this.pn_Loader_OUT2_Y1AA.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT2_Y1AA.TabIndex = 148;
            // 
            // lb_y1a9_2_ld_trans_down
            // 
            this.lb_y1a9_2_ld_trans_down.AutoSize = true;
            this.lb_y1a9_2_ld_trans_down.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1a9_2_ld_trans_down.ForeColor = System.Drawing.Color.White;
            this.lb_y1a9_2_ld_trans_down.Location = new System.Drawing.Point(52, 454);
            this.lb_y1a9_2_ld_trans_down.Name = "lb_y1a9_2_ld_trans_down";
            this.lb_y1a9_2_ld_trans_down.Size = new System.Drawing.Size(206, 13);
            this.lb_y1a9_2_ld_trans_down.TabIndex = 147;
            this.lb_y1a9_2_ld_trans_down.Text = "[ Y1A9 ] 2 L/D 이재기 DOWN";
            // 
            // pn_Loader_OUT2_Y1A9
            // 
            this.pn_Loader_OUT2_Y1A9.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT2_Y1A9.Location = new System.Drawing.Point(6, 442);
            this.pn_Loader_OUT2_Y1A9.Name = "pn_Loader_OUT2_Y1A9";
            this.pn_Loader_OUT2_Y1A9.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT2_Y1A9.TabIndex = 146;
            // 
            // lb_y1a8_2_ld_trans_up
            // 
            this.lb_y1a8_2_ld_trans_up.AutoSize = true;
            this.lb_y1a8_2_ld_trans_up.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1a8_2_ld_trans_up.ForeColor = System.Drawing.Color.White;
            this.lb_y1a8_2_ld_trans_up.Location = new System.Drawing.Point(52, 409);
            this.lb_y1a8_2_ld_trans_up.Name = "lb_y1a8_2_ld_trans_up";
            this.lb_y1a8_2_ld_trans_up.Size = new System.Drawing.Size(182, 13);
            this.lb_y1a8_2_ld_trans_up.TabIndex = 145;
            this.lb_y1a8_2_ld_trans_up.Text = "[ Y1A8 ] 2 L/D 이재기 UP";
            // 
            // pn_Loader_OUT2_Y1A8
            // 
            this.pn_Loader_OUT2_Y1A8.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT2_Y1A8.Location = new System.Drawing.Point(6, 396);
            this.pn_Loader_OUT2_Y1A8.Name = "pn_Loader_OUT2_Y1A8";
            this.pn_Loader_OUT2_Y1A8.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT2_Y1A8.TabIndex = 144;
            // 
            // lb_y1a7_1_ld_trans_down
            // 
            this.lb_y1a7_1_ld_trans_down.AutoSize = true;
            this.lb_y1a7_1_ld_trans_down.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1a7_1_ld_trans_down.ForeColor = System.Drawing.Color.White;
            this.lb_y1a7_1_ld_trans_down.Location = new System.Drawing.Point(52, 364);
            this.lb_y1a7_1_ld_trans_down.Name = "lb_y1a7_1_ld_trans_down";
            this.lb_y1a7_1_ld_trans_down.Size = new System.Drawing.Size(206, 13);
            this.lb_y1a7_1_ld_trans_down.TabIndex = 143;
            this.lb_y1a7_1_ld_trans_down.Text = "[ Y1A7 ] 1 L/D 이재기 DOWN";
            // 
            // pn_Loader_OUT2_Y1A7
            // 
            this.pn_Loader_OUT2_Y1A7.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT2_Y1A7.Location = new System.Drawing.Point(6, 350);
            this.pn_Loader_OUT2_Y1A7.Name = "pn_Loader_OUT2_Y1A7";
            this.pn_Loader_OUT2_Y1A7.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT2_Y1A7.TabIndex = 142;
            // 
            // lb_y1a6_1_ld_trans_up
            // 
            this.lb_y1a6_1_ld_trans_up.AutoSize = true;
            this.lb_y1a6_1_ld_trans_up.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1a6_1_ld_trans_up.ForeColor = System.Drawing.Color.White;
            this.lb_y1a6_1_ld_trans_up.Location = new System.Drawing.Point(52, 319);
            this.lb_y1a6_1_ld_trans_up.Name = "lb_y1a6_1_ld_trans_up";
            this.lb_y1a6_1_ld_trans_up.Size = new System.Drawing.Size(182, 13);
            this.lb_y1a6_1_ld_trans_up.TabIndex = 141;
            this.lb_y1a6_1_ld_trans_up.Text = "[ Y1A6 ] 1 L/D 이재기 UP";
            // 
            // pn_Loader_OUT2_Y1A6
            // 
            this.pn_Loader_OUT2_Y1A6.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT2_Y1A6.Location = new System.Drawing.Point(6, 304);
            this.pn_Loader_OUT2_Y1A6.Name = "pn_Loader_OUT2_Y1A6";
            this.pn_Loader_OUT2_Y1A6.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT2_Y1A6.TabIndex = 140;
            // 
            // lb_y1a5_1_ld_cst_lift_tiltsensor_down
            // 
            this.lb_y1a5_1_ld_cst_lift_tiltsensor_down.AutoSize = true;
            this.lb_y1a5_1_ld_cst_lift_tiltsensor_down.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1a5_1_ld_cst_lift_tiltsensor_down.ForeColor = System.Drawing.Color.White;
            this.lb_y1a5_1_ld_cst_lift_tiltsensor_down.Location = new System.Drawing.Point(52, 271);
            this.lb_y1a5_1_ld_cst_lift_tiltsensor_down.Name = "lb_y1a5_1_ld_cst_lift_tiltsensor_down";
            this.lb_y1a5_1_ld_cst_lift_tiltsensor_down.Size = new System.Drawing.Size(322, 13);
            this.lb_y1a5_1_ld_cst_lift_tiltsensor_down.TabIndex = 139;
            this.lb_y1a5_1_ld_cst_lift_tiltsensor_down.Text = "[ Y1A5 ] 1 L/D Casset Lift Tilt Sensor DOWN";
            // 
            // pn_Loader_OUT2_Y1A5
            // 
            this.pn_Loader_OUT2_Y1A5.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT2_Y1A5.Location = new System.Drawing.Point(6, 258);
            this.pn_Loader_OUT2_Y1A5.Name = "pn_Loader_OUT2_Y1A5";
            this.pn_Loader_OUT2_Y1A5.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT2_Y1A5.TabIndex = 138;
            // 
            // lb_y1a4_1_ld_cst_lift_tiltsensor_up
            // 
            this.lb_y1a4_1_ld_cst_lift_tiltsensor_up.AutoSize = true;
            this.lb_y1a4_1_ld_cst_lift_tiltsensor_up.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1a4_1_ld_cst_lift_tiltsensor_up.ForeColor = System.Drawing.Color.White;
            this.lb_y1a4_1_ld_cst_lift_tiltsensor_up.Location = new System.Drawing.Point(52, 226);
            this.lb_y1a4_1_ld_cst_lift_tiltsensor_up.Name = "lb_y1a4_1_ld_cst_lift_tiltsensor_up";
            this.lb_y1a4_1_ld_cst_lift_tiltsensor_up.Size = new System.Drawing.Size(298, 13);
            this.lb_y1a4_1_ld_cst_lift_tiltsensor_up.TabIndex = 137;
            this.lb_y1a4_1_ld_cst_lift_tiltsensor_up.Text = "[ Y1A4 ] 1 L/D Casset Lift Tilt Sensor UP";
            // 
            // pn_Loader_OUT2_Y1A4
            // 
            this.pn_Loader_OUT2_Y1A4.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT2_Y1A4.Location = new System.Drawing.Point(6, 212);
            this.pn_Loader_OUT2_Y1A4.Name = "pn_Loader_OUT2_Y1A4";
            this.pn_Loader_OUT2_Y1A4.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT2_Y1A4.TabIndex = 136;
            // 
            // lb_y1a3_2_ld_cst_lifter_ungrip
            // 
            this.lb_y1a3_2_ld_cst_lifter_ungrip.AutoSize = true;
            this.lb_y1a3_2_ld_cst_lifter_ungrip.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1a3_2_ld_cst_lifter_ungrip.ForeColor = System.Drawing.Color.White;
            this.lb_y1a3_2_ld_cst_lifter_ungrip.Location = new System.Drawing.Point(52, 179);
            this.lb_y1a3_2_ld_cst_lifter_ungrip.Name = "lb_y1a3_2_ld_cst_lifter_ungrip";
            this.lb_y1a3_2_ld_cst_lifter_ungrip.Size = new System.Drawing.Size(272, 13);
            this.lb_y1a3_2_ld_cst_lifter_ungrip.TabIndex = 135;
            this.lb_y1a3_2_ld_cst_lifter_ungrip.Text = "[ Y1A3 ] 2 L/D Cassette Lifter UnGrip";
            // 
            // pn_Loader_OUT2_Y1A3
            // 
            this.pn_Loader_OUT2_Y1A3.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT2_Y1A3.Location = new System.Drawing.Point(6, 166);
            this.pn_Loader_OUT2_Y1A3.Name = "pn_Loader_OUT2_Y1A3";
            this.pn_Loader_OUT2_Y1A3.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT2_Y1A3.TabIndex = 134;
            // 
            // lb_y1a2_2_ld_cst_lifter_grip
            // 
            this.lb_y1a2_2_ld_cst_lifter_grip.AutoSize = true;
            this.lb_y1a2_2_ld_cst_lifter_grip.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1a2_2_ld_cst_lifter_grip.ForeColor = System.Drawing.Color.White;
            this.lb_y1a2_2_ld_cst_lifter_grip.Location = new System.Drawing.Point(52, 133);
            this.lb_y1a2_2_ld_cst_lifter_grip.Name = "lb_y1a2_2_ld_cst_lifter_grip";
            this.lb_y1a2_2_ld_cst_lifter_grip.Size = new System.Drawing.Size(253, 13);
            this.lb_y1a2_2_ld_cst_lifter_grip.TabIndex = 133;
            this.lb_y1a2_2_ld_cst_lifter_grip.Text = "[ Y1A2 ] 2 L/D Cassette Lifter Grip";
            // 
            // pn_Loader_OUT2_Y1A2
            // 
            this.pn_Loader_OUT2_Y1A2.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT2_Y1A2.Location = new System.Drawing.Point(6, 120);
            this.pn_Loader_OUT2_Y1A2.Name = "pn_Loader_OUT2_Y1A2";
            this.pn_Loader_OUT2_Y1A2.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT2_Y1A2.TabIndex = 132;
            // 
            // lb_y1a1_1_ld_cst_lifter_ungrip
            // 
            this.lb_y1a1_1_ld_cst_lifter_ungrip.AutoSize = true;
            this.lb_y1a1_1_ld_cst_lifter_ungrip.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1a1_1_ld_cst_lifter_ungrip.ForeColor = System.Drawing.Color.White;
            this.lb_y1a1_1_ld_cst_lifter_ungrip.Location = new System.Drawing.Point(52, 86);
            this.lb_y1a1_1_ld_cst_lifter_ungrip.Name = "lb_y1a1_1_ld_cst_lifter_ungrip";
            this.lb_y1a1_1_ld_cst_lifter_ungrip.Size = new System.Drawing.Size(272, 13);
            this.lb_y1a1_1_ld_cst_lifter_ungrip.TabIndex = 131;
            this.lb_y1a1_1_ld_cst_lifter_ungrip.Text = "[ Y1A1 ] 1 L/D Cassette Lifter UnGrip";
            // 
            // pn_Loader_OUT2_Y1A1
            // 
            this.pn_Loader_OUT2_Y1A1.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT2_Y1A1.Location = new System.Drawing.Point(6, 74);
            this.pn_Loader_OUT2_Y1A1.Name = "pn_Loader_OUT2_Y1A1";
            this.pn_Loader_OUT2_Y1A1.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT2_Y1A1.TabIndex = 130;
            // 
            // lb_y1a0_1_ld_cst_lifter_grip
            // 
            this.lb_y1a0_1_ld_cst_lifter_grip.AutoSize = true;
            this.lb_y1a0_1_ld_cst_lifter_grip.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1a0_1_ld_cst_lifter_grip.ForeColor = System.Drawing.Color.White;
            this.lb_y1a0_1_ld_cst_lifter_grip.Location = new System.Drawing.Point(52, 42);
            this.lb_y1a0_1_ld_cst_lifter_grip.Name = "lb_y1a0_1_ld_cst_lifter_grip";
            this.lb_y1a0_1_ld_cst_lifter_grip.Size = new System.Drawing.Size(253, 13);
            this.lb_y1a0_1_ld_cst_lifter_grip.TabIndex = 129;
            this.lb_y1a0_1_ld_cst_lifter_grip.Text = "[ Y1A0 ] 1 L/D Cassette Lifter Grip";
            // 
            // pn_Loader_OUT2_Y1A0
            // 
            this.pn_Loader_OUT2_Y1A0.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT2_Y1A0.Location = new System.Drawing.Point(6, 28);
            this.pn_Loader_OUT2_Y1A0.Name = "pn_Loader_OUT2_Y1A0";
            this.pn_Loader_OUT2_Y1A0.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT2_Y1A0.TabIndex = 128;
            // 
            // tp_iostatus_ld_out3
            // 
            this.tp_iostatus_ld_out3.BackColor = System.Drawing.Color.DimGray;
            this.tp_iostatus_ld_out3.Controls.Add(this.lb_y1df_spare);
            this.tp_iostatus_ld_out3.Controls.Add(this.pn_Loader_OUT3_Y1DF);
            this.tp_iostatus_ld_out3.Controls.Add(this.lb_y1de_spare);
            this.tp_iostatus_ld_out3.Controls.Add(this.pn_Loader_OUT3_Y1DE);
            this.tp_iostatus_ld_out3.Controls.Add(this.lb_y1dd_spare);
            this.tp_iostatus_ld_out3.Controls.Add(this.pn_Loader_OUT3_Y1DD);
            this.tp_iostatus_ld_out3.Controls.Add(this.lb_y1dc_spare);
            this.tp_iostatus_ld_out3.Controls.Add(this.pn_Loader_OUT3_Y1DC);
            this.tp_iostatus_ld_out3.Controls.Add(this.lb_y1db_spare);
            this.tp_iostatus_ld_out3.Controls.Add(this.pn_Loader_OUT3_Y1DB);
            this.tp_iostatus_ld_out3.Controls.Add(this.lb_y1da_spare);
            this.tp_iostatus_ld_out3.Controls.Add(this.pn_Loader_OUT3_Y1DA);
            this.tp_iostatus_ld_out3.Controls.Add(this.lb_y1d9_laser_led);
            this.tp_iostatus_ld_out3.Controls.Add(this.pn_Loader_OUT3_Y1D9);
            this.tp_iostatus_ld_out3.Controls.Add(this.lb_y1d8_spare);
            this.tp_iostatus_ld_out3.Controls.Add(this.pn_Loader_OUT3_Y1D8);
            this.tp_iostatus_ld_out3.Controls.Add(this.lb_y1d7_spare);
            this.tp_iostatus_ld_out3.Controls.Add(this.pn_Loader_OUT3_Y1D7);
            this.tp_iostatus_ld_out3.Controls.Add(this.label10);
            this.tp_iostatus_ld_out3.Controls.Add(this.pn_Loader_OUT3_Y1D6);
            this.tp_iostatus_ld_out3.Controls.Add(this.lb_y1d5_spare);
            this.tp_iostatus_ld_out3.Controls.Add(this.pn_Loader_OUT3_Y1D5);
            this.tp_iostatus_ld_out3.Controls.Add(this.lb_y1d4_spare);
            this.tp_iostatus_ld_out3.Controls.Add(this.pn_Loader_OUT3_Y1D4);
            this.tp_iostatus_ld_out3.Controls.Add(this.lb_y1d3_spare);
            this.tp_iostatus_ld_out3.Controls.Add(this.pn_Loader_OUT3_Y1D3);
            this.tp_iostatus_ld_out3.Controls.Add(this.lb_y1d2_light_3_onoff);
            this.tp_iostatus_ld_out3.Controls.Add(this.pn_Loader_OUT3_Y1D2);
            this.tp_iostatus_ld_out3.Controls.Add(this.lb_y1d1_light_2_onoff);
            this.tp_iostatus_ld_out3.Controls.Add(this.pn_Loader_OUT3_Y1D1);
            this.tp_iostatus_ld_out3.Controls.Add(this.lb_y1d0_light_1_onoff);
            this.tp_iostatus_ld_out3.Controls.Add(this.pn_Loader_OUT3_Y1D0);
            this.tp_iostatus_ld_out3.Controls.Add(this.lb_y1cf_spare_air_on_cmd);
            this.tp_iostatus_ld_out3.Controls.Add(this.pn_Loader_OUT3_Y1CF);
            this.tp_iostatus_ld_out3.Controls.Add(this.lb_y1ce_spare_air_on_cmd);
            this.tp_iostatus_ld_out3.Controls.Add(this.pn_Loader_OUT3_Y1CE);
            this.tp_iostatus_ld_out3.Controls.Add(this.lb_y1cd_spare_sol_on_cmd);
            this.tp_iostatus_ld_out3.Controls.Add(this.pn_Loader_OUT3_Y1CD);
            this.tp_iostatus_ld_out3.Controls.Add(this.lb_y1cc_spare_sol_on_cmd);
            this.tp_iostatus_ld_out3.Controls.Add(this.pn_Loader_OUT3_Y1CC);
            this.tp_iostatus_ld_out3.Controls.Add(this.lb_y1cb_spare_sol_on_cmd);
            this.tp_iostatus_ld_out3.Controls.Add(this.pn_Loader_OUT3_Y1CB);
            this.tp_iostatus_ld_out3.Controls.Add(this.lb_y1ca_spare_sol_on_cmd);
            this.tp_iostatus_ld_out3.Controls.Add(this.pn_Loader_OUT3_Y1CA);
            this.tp_iostatus_ld_out3.Controls.Add(this.lb_y1c9_spare);
            this.tp_iostatus_ld_out3.Controls.Add(this.pn_Loader_OUT3_Y1C9);
            this.tp_iostatus_ld_out3.Controls.Add(this.lb_y1c8_spare);
            this.tp_iostatus_ld_out3.Controls.Add(this.pn_Loader_OUT3_Y1C8);
            this.tp_iostatus_ld_out3.Controls.Add(this.lb_y1c7_spare);
            this.tp_iostatus_ld_out3.Controls.Add(this.pn_Loader_OUT3_Y1C7);
            this.tp_iostatus_ld_out3.Controls.Add(this.lb_y1c6_7_safety_door_sw_open);
            this.tp_iostatus_ld_out3.Controls.Add(this.pn_Loader_OUT3_Y1C6);
            this.tp_iostatus_ld_out3.Controls.Add(this.lb_y1c5_6_safety_door_sw_open);
            this.tp_iostatus_ld_out3.Controls.Add(this.pn_Loader_OUT3_Y1C5);
            this.tp_iostatus_ld_out3.Controls.Add(this.lb_y1c4_5_safety_door_sw_open);
            this.tp_iostatus_ld_out3.Controls.Add(this.pn_Loader_OUT3_Y1C4);
            this.tp_iostatus_ld_out3.Controls.Add(this.lb_y1c3_4_safety_door_sw_open);
            this.tp_iostatus_ld_out3.Controls.Add(this.pn_Loader_OUT3_Y1C3);
            this.tp_iostatus_ld_out3.Controls.Add(this.lb_y1c2_3_safety_door_sw_open);
            this.tp_iostatus_ld_out3.Controls.Add(this.pn_Loader_OUT3_Y1C2);
            this.tp_iostatus_ld_out3.Controls.Add(this.lb_y1c1_2_safety_door_sw_open);
            this.tp_iostatus_ld_out3.Controls.Add(this.pn_Loader_OUT3_Y1C1);
            this.tp_iostatus_ld_out3.Controls.Add(this.lb_y1c0_1_safety_door_sw_open);
            this.tp_iostatus_ld_out3.Controls.Add(this.pn_Loader_OUT3_Y1C0);
            this.tp_iostatus_ld_out3.Location = new System.Drawing.Point(4, 34);
            this.tp_iostatus_ld_out3.Name = "tp_iostatus_ld_out3";
            this.tp_iostatus_ld_out3.Padding = new System.Windows.Forms.Padding(3);
            this.tp_iostatus_ld_out3.Size = new System.Drawing.Size(846, 778);
            this.tp_iostatus_ld_out3.TabIndex = 2;
            this.tp_iostatus_ld_out3.Text = "OUT - 3";
            // 
            // lb_y1df_spare
            // 
            this.lb_y1df_spare.AutoSize = true;
            this.lb_y1df_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1df_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y1df_spare.Location = new System.Drawing.Point(479, 730);
            this.lb_y1df_spare.Name = "lb_y1df_spare";
            this.lb_y1df_spare.Size = new System.Drawing.Size(116, 13);
            this.lb_y1df_spare.TabIndex = 255;
            this.lb_y1df_spare.Text = "[ Y1DF ] Spare";
            // 
            // pn_Loader_OUT3_Y1DF
            // 
            this.pn_Loader_OUT3_Y1DF.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT3_Y1DF.Location = new System.Drawing.Point(433, 718);
            this.pn_Loader_OUT3_Y1DF.Name = "pn_Loader_OUT3_Y1DF";
            this.pn_Loader_OUT3_Y1DF.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT3_Y1DF.TabIndex = 254;
            // 
            // lb_y1de_spare
            // 
            this.lb_y1de_spare.AutoSize = true;
            this.lb_y1de_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1de_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y1de_spare.Location = new System.Drawing.Point(479, 685);
            this.lb_y1de_spare.Name = "lb_y1de_spare";
            this.lb_y1de_spare.Size = new System.Drawing.Size(117, 13);
            this.lb_y1de_spare.TabIndex = 253;
            this.lb_y1de_spare.Text = "[ Y1DE ] Spare";
            // 
            // pn_Loader_OUT3_Y1DE
            // 
            this.pn_Loader_OUT3_Y1DE.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT3_Y1DE.Location = new System.Drawing.Point(433, 672);
            this.pn_Loader_OUT3_Y1DE.Name = "pn_Loader_OUT3_Y1DE";
            this.pn_Loader_OUT3_Y1DE.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT3_Y1DE.TabIndex = 252;
            // 
            // lb_y1dd_spare
            // 
            this.lb_y1dd_spare.AutoSize = true;
            this.lb_y1dd_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1dd_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y1dd_spare.Location = new System.Drawing.Point(479, 640);
            this.lb_y1dd_spare.Name = "lb_y1dd_spare";
            this.lb_y1dd_spare.Size = new System.Drawing.Size(118, 13);
            this.lb_y1dd_spare.TabIndex = 251;
            this.lb_y1dd_spare.Text = "[ Y1DD ] Spare";
            // 
            // pn_Loader_OUT3_Y1DD
            // 
            this.pn_Loader_OUT3_Y1DD.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT3_Y1DD.Location = new System.Drawing.Point(433, 626);
            this.pn_Loader_OUT3_Y1DD.Name = "pn_Loader_OUT3_Y1DD";
            this.pn_Loader_OUT3_Y1DD.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT3_Y1DD.TabIndex = 250;
            // 
            // lb_y1dc_spare
            // 
            this.lb_y1dc_spare.AutoSize = true;
            this.lb_y1dc_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1dc_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y1dc_spare.Location = new System.Drawing.Point(479, 594);
            this.lb_y1dc_spare.Name = "lb_y1dc_spare";
            this.lb_y1dc_spare.Size = new System.Drawing.Size(118, 13);
            this.lb_y1dc_spare.TabIndex = 249;
            this.lb_y1dc_spare.Text = "[ Y1DC ] Spare";
            // 
            // pn_Loader_OUT3_Y1DC
            // 
            this.pn_Loader_OUT3_Y1DC.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT3_Y1DC.Location = new System.Drawing.Point(433, 580);
            this.pn_Loader_OUT3_Y1DC.Name = "pn_Loader_OUT3_Y1DC";
            this.pn_Loader_OUT3_Y1DC.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT3_Y1DC.TabIndex = 248;
            // 
            // lb_y1db_spare
            // 
            this.lb_y1db_spare.AutoSize = true;
            this.lb_y1db_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1db_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y1db_spare.Location = new System.Drawing.Point(479, 548);
            this.lb_y1db_spare.Name = "lb_y1db_spare";
            this.lb_y1db_spare.Size = new System.Drawing.Size(118, 13);
            this.lb_y1db_spare.TabIndex = 247;
            this.lb_y1db_spare.Text = "[ Y1DB ] Spare";
            // 
            // pn_Loader_OUT3_Y1DB
            // 
            this.pn_Loader_OUT3_Y1DB.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT3_Y1DB.Location = new System.Drawing.Point(433, 534);
            this.pn_Loader_OUT3_Y1DB.Name = "pn_Loader_OUT3_Y1DB";
            this.pn_Loader_OUT3_Y1DB.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT3_Y1DB.TabIndex = 246;
            // 
            // lb_y1da_spare
            // 
            this.lb_y1da_spare.AutoSize = true;
            this.lb_y1da_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1da_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y1da_spare.Location = new System.Drawing.Point(479, 501);
            this.lb_y1da_spare.Name = "lb_y1da_spare";
            this.lb_y1da_spare.Size = new System.Drawing.Size(117, 13);
            this.lb_y1da_spare.TabIndex = 245;
            this.lb_y1da_spare.Text = "[ Y1DA ] Spare";
            // 
            // pn_Loader_OUT3_Y1DA
            // 
            this.pn_Loader_OUT3_Y1DA.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT3_Y1DA.Location = new System.Drawing.Point(433, 488);
            this.pn_Loader_OUT3_Y1DA.Name = "pn_Loader_OUT3_Y1DA";
            this.pn_Loader_OUT3_Y1DA.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT3_Y1DA.TabIndex = 244;
            // 
            // lb_y1d9_laser_led
            // 
            this.lb_y1d9_laser_led.AutoSize = true;
            this.lb_y1d9_laser_led.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1d9_laser_led.ForeColor = System.Drawing.Color.White;
            this.lb_y1d9_laser_led.Location = new System.Drawing.Point(479, 454);
            this.lb_y1d9_laser_led.Name = "lb_y1d9_laser_led";
            this.lb_y1d9_laser_led.Size = new System.Drawing.Size(162, 13);
            this.lb_y1d9_laser_led.TabIndex = 243;
            this.lb_y1d9_laser_led.Text = "[ Y1D9 ] 가공부 전광판";
            // 
            // pn_Loader_OUT3_Y1D9
            // 
            this.pn_Loader_OUT3_Y1D9.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT3_Y1D9.Location = new System.Drawing.Point(433, 442);
            this.pn_Loader_OUT3_Y1D9.Name = "pn_Loader_OUT3_Y1D9";
            this.pn_Loader_OUT3_Y1D9.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT3_Y1D9.TabIndex = 242;
            // 
            // lb_y1d8_spare
            // 
            this.lb_y1d8_spare.AutoSize = true;
            this.lb_y1d8_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1d8_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y1d8_spare.Location = new System.Drawing.Point(479, 409);
            this.lb_y1d8_spare.Name = "lb_y1d8_spare";
            this.lb_y1d8_spare.Size = new System.Drawing.Size(116, 13);
            this.lb_y1d8_spare.TabIndex = 241;
            this.lb_y1d8_spare.Text = "[ Y1D8 ] Spare";
            // 
            // pn_Loader_OUT3_Y1D8
            // 
            this.pn_Loader_OUT3_Y1D8.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT3_Y1D8.Location = new System.Drawing.Point(433, 396);
            this.pn_Loader_OUT3_Y1D8.Name = "pn_Loader_OUT3_Y1D8";
            this.pn_Loader_OUT3_Y1D8.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT3_Y1D8.TabIndex = 240;
            // 
            // lb_y1d7_spare
            // 
            this.lb_y1d7_spare.AutoSize = true;
            this.lb_y1d7_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1d7_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y1d7_spare.Location = new System.Drawing.Point(479, 364);
            this.lb_y1d7_spare.Name = "lb_y1d7_spare";
            this.lb_y1d7_spare.Size = new System.Drawing.Size(116, 13);
            this.lb_y1d7_spare.TabIndex = 239;
            this.lb_y1d7_spare.Text = "[ Y1D7 ] Spare";
            // 
            // pn_Loader_OUT3_Y1D7
            // 
            this.pn_Loader_OUT3_Y1D7.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT3_Y1D7.Location = new System.Drawing.Point(433, 350);
            this.pn_Loader_OUT3_Y1D7.Name = "pn_Loader_OUT3_Y1D7";
            this.pn_Loader_OUT3_Y1D7.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT3_Y1D7.TabIndex = 238;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(479, 319);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(116, 13);
            this.label10.TabIndex = 237;
            this.label10.Text = "[ Y1D6 ] Spare";
            // 
            // pn_Loader_OUT3_Y1D6
            // 
            this.pn_Loader_OUT3_Y1D6.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT3_Y1D6.Location = new System.Drawing.Point(433, 304);
            this.pn_Loader_OUT3_Y1D6.Name = "pn_Loader_OUT3_Y1D6";
            this.pn_Loader_OUT3_Y1D6.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT3_Y1D6.TabIndex = 236;
            // 
            // lb_y1d5_spare
            // 
            this.lb_y1d5_spare.AutoSize = true;
            this.lb_y1d5_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1d5_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y1d5_spare.Location = new System.Drawing.Point(479, 271);
            this.lb_y1d5_spare.Name = "lb_y1d5_spare";
            this.lb_y1d5_spare.Size = new System.Drawing.Size(116, 13);
            this.lb_y1d5_spare.TabIndex = 235;
            this.lb_y1d5_spare.Text = "[ Y1D5 ] Spare";
            // 
            // pn_Loader_OUT3_Y1D5
            // 
            this.pn_Loader_OUT3_Y1D5.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT3_Y1D5.Location = new System.Drawing.Point(433, 258);
            this.pn_Loader_OUT3_Y1D5.Name = "pn_Loader_OUT3_Y1D5";
            this.pn_Loader_OUT3_Y1D5.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT3_Y1D5.TabIndex = 234;
            // 
            // lb_y1d4_spare
            // 
            this.lb_y1d4_spare.AutoSize = true;
            this.lb_y1d4_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1d4_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y1d4_spare.Location = new System.Drawing.Point(479, 226);
            this.lb_y1d4_spare.Name = "lb_y1d4_spare";
            this.lb_y1d4_spare.Size = new System.Drawing.Size(116, 13);
            this.lb_y1d4_spare.TabIndex = 233;
            this.lb_y1d4_spare.Text = "[ Y1D4 ] Spare";
            // 
            // pn_Loader_OUT3_Y1D4
            // 
            this.pn_Loader_OUT3_Y1D4.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT3_Y1D4.Location = new System.Drawing.Point(433, 212);
            this.pn_Loader_OUT3_Y1D4.Name = "pn_Loader_OUT3_Y1D4";
            this.pn_Loader_OUT3_Y1D4.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT3_Y1D4.TabIndex = 232;
            // 
            // lb_y1d3_spare
            // 
            this.lb_y1d3_spare.AutoSize = true;
            this.lb_y1d3_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1d3_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y1d3_spare.Location = new System.Drawing.Point(479, 179);
            this.lb_y1d3_spare.Name = "lb_y1d3_spare";
            this.lb_y1d3_spare.Size = new System.Drawing.Size(116, 13);
            this.lb_y1d3_spare.TabIndex = 231;
            this.lb_y1d3_spare.Text = "[ Y1D3 ] Spare";
            // 
            // pn_Loader_OUT3_Y1D3
            // 
            this.pn_Loader_OUT3_Y1D3.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT3_Y1D3.Location = new System.Drawing.Point(433, 166);
            this.pn_Loader_OUT3_Y1D3.Name = "pn_Loader_OUT3_Y1D3";
            this.pn_Loader_OUT3_Y1D3.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT3_Y1D3.TabIndex = 230;
            // 
            // lb_y1d2_light_3_onoff
            // 
            this.lb_y1d2_light_3_onoff.AutoSize = true;
            this.lb_y1d2_light_3_onoff.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1d2_light_3_onoff.ForeColor = System.Drawing.Color.White;
            this.lb_y1d2_light_3_onoff.Location = new System.Drawing.Point(479, 133);
            this.lb_y1d2_light_3_onoff.Name = "lb_y1d2_light_3_onoff";
            this.lb_y1d2_light_3_onoff.Size = new System.Drawing.Size(254, 13);
            this.lb_y1d2_light_3_onoff.TabIndex = 229;
            this.lb_y1d2_light_3_onoff.Text = "[ Y1D2 ] 설비 내부 형광등 3 ON/OFF";
            // 
            // pn_Loader_OUT3_Y1D2
            // 
            this.pn_Loader_OUT3_Y1D2.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT3_Y1D2.Location = new System.Drawing.Point(433, 120);
            this.pn_Loader_OUT3_Y1D2.Name = "pn_Loader_OUT3_Y1D2";
            this.pn_Loader_OUT3_Y1D2.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT3_Y1D2.TabIndex = 228;
            // 
            // lb_y1d1_light_2_onoff
            // 
            this.lb_y1d1_light_2_onoff.AutoSize = true;
            this.lb_y1d1_light_2_onoff.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1d1_light_2_onoff.ForeColor = System.Drawing.Color.White;
            this.lb_y1d1_light_2_onoff.Location = new System.Drawing.Point(479, 86);
            this.lb_y1d1_light_2_onoff.Name = "lb_y1d1_light_2_onoff";
            this.lb_y1d1_light_2_onoff.Size = new System.Drawing.Size(254, 13);
            this.lb_y1d1_light_2_onoff.TabIndex = 227;
            this.lb_y1d1_light_2_onoff.Text = "[ Y1D1 ] 설비 내부 형광등 2 ON/OFF";
            // 
            // pn_Loader_OUT3_Y1D1
            // 
            this.pn_Loader_OUT3_Y1D1.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT3_Y1D1.Location = new System.Drawing.Point(433, 74);
            this.pn_Loader_OUT3_Y1D1.Name = "pn_Loader_OUT3_Y1D1";
            this.pn_Loader_OUT3_Y1D1.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT3_Y1D1.TabIndex = 226;
            // 
            // lb_y1d0_light_1_onoff
            // 
            this.lb_y1d0_light_1_onoff.AutoSize = true;
            this.lb_y1d0_light_1_onoff.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1d0_light_1_onoff.ForeColor = System.Drawing.Color.White;
            this.lb_y1d0_light_1_onoff.Location = new System.Drawing.Point(479, 42);
            this.lb_y1d0_light_1_onoff.Name = "lb_y1d0_light_1_onoff";
            this.lb_y1d0_light_1_onoff.Size = new System.Drawing.Size(254, 13);
            this.lb_y1d0_light_1_onoff.TabIndex = 225;
            this.lb_y1d0_light_1_onoff.Text = "[ Y1D0 ] 설비 내부 형광등 1 ON/OFF";
            // 
            // pn_Loader_OUT3_Y1D0
            // 
            this.pn_Loader_OUT3_Y1D0.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT3_Y1D0.Location = new System.Drawing.Point(433, 28);
            this.pn_Loader_OUT3_Y1D0.Name = "pn_Loader_OUT3_Y1D0";
            this.pn_Loader_OUT3_Y1D0.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT3_Y1D0.TabIndex = 224;
            // 
            // lb_y1cf_spare_air_on_cmd
            // 
            this.lb_y1cf_spare_air_on_cmd.AutoSize = true;
            this.lb_y1cf_spare_air_on_cmd.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1cf_spare_air_on_cmd.ForeColor = System.Drawing.Color.White;
            this.lb_y1cf_spare_air_on_cmd.Location = new System.Drawing.Point(52, 730);
            this.lb_y1cf_spare_air_on_cmd.Name = "lb_y1cf_spare_air_on_cmd";
            this.lb_y1cf_spare_air_on_cmd.Size = new System.Drawing.Size(240, 13);
            this.lb_y1cf_spare_air_on_cmd.TabIndex = 223;
            this.lb_y1cf_spare_air_on_cmd.Text = "[ Y1CF ] Spare 파기 AIR ON CMD";
            // 
            // pn_Loader_OUT3_Y1CF
            // 
            this.pn_Loader_OUT3_Y1CF.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT3_Y1CF.Location = new System.Drawing.Point(6, 718);
            this.pn_Loader_OUT3_Y1CF.Name = "pn_Loader_OUT3_Y1CF";
            this.pn_Loader_OUT3_Y1CF.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT3_Y1CF.TabIndex = 222;
            // 
            // lb_y1ce_spare_air_on_cmd
            // 
            this.lb_y1ce_spare_air_on_cmd.AutoSize = true;
            this.lb_y1ce_spare_air_on_cmd.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1ce_spare_air_on_cmd.ForeColor = System.Drawing.Color.White;
            this.lb_y1ce_spare_air_on_cmd.Location = new System.Drawing.Point(52, 685);
            this.lb_y1ce_spare_air_on_cmd.Name = "lb_y1ce_spare_air_on_cmd";
            this.lb_y1ce_spare_air_on_cmd.Size = new System.Drawing.Size(241, 13);
            this.lb_y1ce_spare_air_on_cmd.TabIndex = 221;
            this.lb_y1ce_spare_air_on_cmd.Text = "[ Y1CE ] Spare 파기 AIR ON CMD";
            // 
            // pn_Loader_OUT3_Y1CE
            // 
            this.pn_Loader_OUT3_Y1CE.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT3_Y1CE.Location = new System.Drawing.Point(6, 672);
            this.pn_Loader_OUT3_Y1CE.Name = "pn_Loader_OUT3_Y1CE";
            this.pn_Loader_OUT3_Y1CE.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT3_Y1CE.TabIndex = 220;
            // 
            // lb_y1cd_spare_sol_on_cmd
            // 
            this.lb_y1cd_spare_sol_on_cmd.AutoSize = true;
            this.lb_y1cd_spare_sol_on_cmd.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1cd_spare_sol_on_cmd.ForeColor = System.Drawing.Color.White;
            this.lb_y1cd_spare_sol_on_cmd.Location = new System.Drawing.Point(52, 640);
            this.lb_y1cd_spare_sol_on_cmd.Name = "lb_y1cd_spare_sol_on_cmd";
            this.lb_y1cd_spare_sol_on_cmd.Size = new System.Drawing.Size(245, 13);
            this.lb_y1cd_spare_sol_on_cmd.TabIndex = 219;
            this.lb_y1cd_spare_sol_on_cmd.Text = "[ Y1CD ] Spare SOL V/V ON CMD";
            // 
            // pn_Loader_OUT3_Y1CD
            // 
            this.pn_Loader_OUT3_Y1CD.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT3_Y1CD.Location = new System.Drawing.Point(6, 626);
            this.pn_Loader_OUT3_Y1CD.Name = "pn_Loader_OUT3_Y1CD";
            this.pn_Loader_OUT3_Y1CD.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT3_Y1CD.TabIndex = 218;
            // 
            // lb_y1cc_spare_sol_on_cmd
            // 
            this.lb_y1cc_spare_sol_on_cmd.AutoSize = true;
            this.lb_y1cc_spare_sol_on_cmd.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1cc_spare_sol_on_cmd.ForeColor = System.Drawing.Color.White;
            this.lb_y1cc_spare_sol_on_cmd.Location = new System.Drawing.Point(52, 594);
            this.lb_y1cc_spare_sol_on_cmd.Name = "lb_y1cc_spare_sol_on_cmd";
            this.lb_y1cc_spare_sol_on_cmd.Size = new System.Drawing.Size(245, 13);
            this.lb_y1cc_spare_sol_on_cmd.TabIndex = 217;
            this.lb_y1cc_spare_sol_on_cmd.Text = "[ Y1CC ] Spare SOL V/V ON CMD";
            // 
            // pn_Loader_OUT3_Y1CC
            // 
            this.pn_Loader_OUT3_Y1CC.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT3_Y1CC.Location = new System.Drawing.Point(6, 580);
            this.pn_Loader_OUT3_Y1CC.Name = "pn_Loader_OUT3_Y1CC";
            this.pn_Loader_OUT3_Y1CC.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT3_Y1CC.TabIndex = 216;
            // 
            // lb_y1cb_spare_sol_on_cmd
            // 
            this.lb_y1cb_spare_sol_on_cmd.AutoSize = true;
            this.lb_y1cb_spare_sol_on_cmd.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1cb_spare_sol_on_cmd.ForeColor = System.Drawing.Color.White;
            this.lb_y1cb_spare_sol_on_cmd.Location = new System.Drawing.Point(52, 548);
            this.lb_y1cb_spare_sol_on_cmd.Name = "lb_y1cb_spare_sol_on_cmd";
            this.lb_y1cb_spare_sol_on_cmd.Size = new System.Drawing.Size(245, 13);
            this.lb_y1cb_spare_sol_on_cmd.TabIndex = 215;
            this.lb_y1cb_spare_sol_on_cmd.Text = "[ Y1CB ] Spare SOL V/V ON CMD";
            // 
            // pn_Loader_OUT3_Y1CB
            // 
            this.pn_Loader_OUT3_Y1CB.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT3_Y1CB.Location = new System.Drawing.Point(6, 534);
            this.pn_Loader_OUT3_Y1CB.Name = "pn_Loader_OUT3_Y1CB";
            this.pn_Loader_OUT3_Y1CB.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT3_Y1CB.TabIndex = 214;
            // 
            // lb_y1ca_spare_sol_on_cmd
            // 
            this.lb_y1ca_spare_sol_on_cmd.AutoSize = true;
            this.lb_y1ca_spare_sol_on_cmd.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1ca_spare_sol_on_cmd.ForeColor = System.Drawing.Color.White;
            this.lb_y1ca_spare_sol_on_cmd.Location = new System.Drawing.Point(52, 501);
            this.lb_y1ca_spare_sol_on_cmd.Name = "lb_y1ca_spare_sol_on_cmd";
            this.lb_y1ca_spare_sol_on_cmd.Size = new System.Drawing.Size(244, 13);
            this.lb_y1ca_spare_sol_on_cmd.TabIndex = 213;
            this.lb_y1ca_spare_sol_on_cmd.Text = "[ Y1CA ] Spare SOL V/V ON CMD";
            // 
            // pn_Loader_OUT3_Y1CA
            // 
            this.pn_Loader_OUT3_Y1CA.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT3_Y1CA.Location = new System.Drawing.Point(6, 488);
            this.pn_Loader_OUT3_Y1CA.Name = "pn_Loader_OUT3_Y1CA";
            this.pn_Loader_OUT3_Y1CA.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT3_Y1CA.TabIndex = 212;
            // 
            // lb_y1c9_spare
            // 
            this.lb_y1c9_spare.AutoSize = true;
            this.lb_y1c9_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1c9_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y1c9_spare.Location = new System.Drawing.Point(52, 454);
            this.lb_y1c9_spare.Name = "lb_y1c9_spare";
            this.lb_y1c9_spare.Size = new System.Drawing.Size(116, 13);
            this.lb_y1c9_spare.TabIndex = 211;
            this.lb_y1c9_spare.Text = "[ Y1C9 ] Spare";
            // 
            // pn_Loader_OUT3_Y1C9
            // 
            this.pn_Loader_OUT3_Y1C9.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT3_Y1C9.Location = new System.Drawing.Point(6, 442);
            this.pn_Loader_OUT3_Y1C9.Name = "pn_Loader_OUT3_Y1C9";
            this.pn_Loader_OUT3_Y1C9.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT3_Y1C9.TabIndex = 210;
            // 
            // lb_y1c8_spare
            // 
            this.lb_y1c8_spare.AutoSize = true;
            this.lb_y1c8_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1c8_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y1c8_spare.Location = new System.Drawing.Point(52, 409);
            this.lb_y1c8_spare.Name = "lb_y1c8_spare";
            this.lb_y1c8_spare.Size = new System.Drawing.Size(116, 13);
            this.lb_y1c8_spare.TabIndex = 209;
            this.lb_y1c8_spare.Text = "[ Y1C8 ] Spare";
            // 
            // pn_Loader_OUT3_Y1C8
            // 
            this.pn_Loader_OUT3_Y1C8.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT3_Y1C8.Location = new System.Drawing.Point(6, 396);
            this.pn_Loader_OUT3_Y1C8.Name = "pn_Loader_OUT3_Y1C8";
            this.pn_Loader_OUT3_Y1C8.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT3_Y1C8.TabIndex = 208;
            // 
            // lb_y1c7_spare
            // 
            this.lb_y1c7_spare.AutoSize = true;
            this.lb_y1c7_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1c7_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y1c7_spare.Location = new System.Drawing.Point(52, 364);
            this.lb_y1c7_spare.Name = "lb_y1c7_spare";
            this.lb_y1c7_spare.Size = new System.Drawing.Size(116, 13);
            this.lb_y1c7_spare.TabIndex = 207;
            this.lb_y1c7_spare.Text = "[ Y1C7 ] Spare";
            // 
            // pn_Loader_OUT3_Y1C7
            // 
            this.pn_Loader_OUT3_Y1C7.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT3_Y1C7.Location = new System.Drawing.Point(6, 350);
            this.pn_Loader_OUT3_Y1C7.Name = "pn_Loader_OUT3_Y1C7";
            this.pn_Loader_OUT3_Y1C7.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT3_Y1C7.TabIndex = 206;
            // 
            // lb_y1c6_7_safety_door_sw_open
            // 
            this.lb_y1c6_7_safety_door_sw_open.AutoSize = true;
            this.lb_y1c6_7_safety_door_sw_open.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1c6_7_safety_door_sw_open.ForeColor = System.Drawing.Color.White;
            this.lb_y1c6_7_safety_door_sw_open.Location = new System.Drawing.Point(52, 319);
            this.lb_y1c6_7_safety_door_sw_open.Name = "lb_y1c6_7_safety_door_sw_open";
            this.lb_y1c6_7_safety_door_sw_open.Size = new System.Drawing.Size(240, 13);
            this.lb_y1c6_7_safety_door_sw_open.TabIndex = 205;
            this.lb_y1c6_7_safety_door_sw_open.Text = "[ Y1C6 ] 7 Safety Door SW Open";
            // 
            // pn_Loader_OUT3_Y1C6
            // 
            this.pn_Loader_OUT3_Y1C6.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT3_Y1C6.Location = new System.Drawing.Point(6, 304);
            this.pn_Loader_OUT3_Y1C6.Name = "pn_Loader_OUT3_Y1C6";
            this.pn_Loader_OUT3_Y1C6.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT3_Y1C6.TabIndex = 204;
            // 
            // lb_y1c5_6_safety_door_sw_open
            // 
            this.lb_y1c5_6_safety_door_sw_open.AutoSize = true;
            this.lb_y1c5_6_safety_door_sw_open.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1c5_6_safety_door_sw_open.ForeColor = System.Drawing.Color.White;
            this.lb_y1c5_6_safety_door_sw_open.Location = new System.Drawing.Point(52, 271);
            this.lb_y1c5_6_safety_door_sw_open.Name = "lb_y1c5_6_safety_door_sw_open";
            this.lb_y1c5_6_safety_door_sw_open.Size = new System.Drawing.Size(240, 13);
            this.lb_y1c5_6_safety_door_sw_open.TabIndex = 203;
            this.lb_y1c5_6_safety_door_sw_open.Text = "[ Y1C5 ] 6 Safety Door SW Open";
            // 
            // pn_Loader_OUT3_Y1C5
            // 
            this.pn_Loader_OUT3_Y1C5.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT3_Y1C5.Location = new System.Drawing.Point(6, 258);
            this.pn_Loader_OUT3_Y1C5.Name = "pn_Loader_OUT3_Y1C5";
            this.pn_Loader_OUT3_Y1C5.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT3_Y1C5.TabIndex = 202;
            // 
            // lb_y1c4_5_safety_door_sw_open
            // 
            this.lb_y1c4_5_safety_door_sw_open.AutoSize = true;
            this.lb_y1c4_5_safety_door_sw_open.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1c4_5_safety_door_sw_open.ForeColor = System.Drawing.Color.White;
            this.lb_y1c4_5_safety_door_sw_open.Location = new System.Drawing.Point(52, 226);
            this.lb_y1c4_5_safety_door_sw_open.Name = "lb_y1c4_5_safety_door_sw_open";
            this.lb_y1c4_5_safety_door_sw_open.Size = new System.Drawing.Size(240, 13);
            this.lb_y1c4_5_safety_door_sw_open.TabIndex = 201;
            this.lb_y1c4_5_safety_door_sw_open.Text = "[ Y1C4 ] 5 Safety Door SW Open";
            // 
            // pn_Loader_OUT3_Y1C4
            // 
            this.pn_Loader_OUT3_Y1C4.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT3_Y1C4.Location = new System.Drawing.Point(6, 212);
            this.pn_Loader_OUT3_Y1C4.Name = "pn_Loader_OUT3_Y1C4";
            this.pn_Loader_OUT3_Y1C4.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT3_Y1C4.TabIndex = 200;
            // 
            // lb_y1c3_4_safety_door_sw_open
            // 
            this.lb_y1c3_4_safety_door_sw_open.AutoSize = true;
            this.lb_y1c3_4_safety_door_sw_open.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1c3_4_safety_door_sw_open.ForeColor = System.Drawing.Color.White;
            this.lb_y1c3_4_safety_door_sw_open.Location = new System.Drawing.Point(52, 179);
            this.lb_y1c3_4_safety_door_sw_open.Name = "lb_y1c3_4_safety_door_sw_open";
            this.lb_y1c3_4_safety_door_sw_open.Size = new System.Drawing.Size(240, 13);
            this.lb_y1c3_4_safety_door_sw_open.TabIndex = 199;
            this.lb_y1c3_4_safety_door_sw_open.Text = "[ Y1C3 ] 4 Safety Door SW Open";
            // 
            // pn_Loader_OUT3_Y1C3
            // 
            this.pn_Loader_OUT3_Y1C3.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT3_Y1C3.Location = new System.Drawing.Point(6, 166);
            this.pn_Loader_OUT3_Y1C3.Name = "pn_Loader_OUT3_Y1C3";
            this.pn_Loader_OUT3_Y1C3.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT3_Y1C3.TabIndex = 198;
            // 
            // lb_y1c2_3_safety_door_sw_open
            // 
            this.lb_y1c2_3_safety_door_sw_open.AutoSize = true;
            this.lb_y1c2_3_safety_door_sw_open.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1c2_3_safety_door_sw_open.ForeColor = System.Drawing.Color.White;
            this.lb_y1c2_3_safety_door_sw_open.Location = new System.Drawing.Point(52, 133);
            this.lb_y1c2_3_safety_door_sw_open.Name = "lb_y1c2_3_safety_door_sw_open";
            this.lb_y1c2_3_safety_door_sw_open.Size = new System.Drawing.Size(240, 13);
            this.lb_y1c2_3_safety_door_sw_open.TabIndex = 197;
            this.lb_y1c2_3_safety_door_sw_open.Text = "[ Y1C2 ] 3 Safety Door SW Open";
            // 
            // pn_Loader_OUT3_Y1C2
            // 
            this.pn_Loader_OUT3_Y1C2.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT3_Y1C2.Location = new System.Drawing.Point(6, 120);
            this.pn_Loader_OUT3_Y1C2.Name = "pn_Loader_OUT3_Y1C2";
            this.pn_Loader_OUT3_Y1C2.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT3_Y1C2.TabIndex = 196;
            // 
            // lb_y1c1_2_safety_door_sw_open
            // 
            this.lb_y1c1_2_safety_door_sw_open.AutoSize = true;
            this.lb_y1c1_2_safety_door_sw_open.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1c1_2_safety_door_sw_open.ForeColor = System.Drawing.Color.White;
            this.lb_y1c1_2_safety_door_sw_open.Location = new System.Drawing.Point(52, 86);
            this.lb_y1c1_2_safety_door_sw_open.Name = "lb_y1c1_2_safety_door_sw_open";
            this.lb_y1c1_2_safety_door_sw_open.Size = new System.Drawing.Size(240, 13);
            this.lb_y1c1_2_safety_door_sw_open.TabIndex = 195;
            this.lb_y1c1_2_safety_door_sw_open.Text = "[ Y1C1 ] 2 Safety Door SW Open";
            // 
            // pn_Loader_OUT3_Y1C1
            // 
            this.pn_Loader_OUT3_Y1C1.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT3_Y1C1.Location = new System.Drawing.Point(6, 74);
            this.pn_Loader_OUT3_Y1C1.Name = "pn_Loader_OUT3_Y1C1";
            this.pn_Loader_OUT3_Y1C1.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT3_Y1C1.TabIndex = 194;
            // 
            // lb_y1c0_1_safety_door_sw_open
            // 
            this.lb_y1c0_1_safety_door_sw_open.AutoSize = true;
            this.lb_y1c0_1_safety_door_sw_open.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1c0_1_safety_door_sw_open.ForeColor = System.Drawing.Color.White;
            this.lb_y1c0_1_safety_door_sw_open.Location = new System.Drawing.Point(52, 42);
            this.lb_y1c0_1_safety_door_sw_open.Name = "lb_y1c0_1_safety_door_sw_open";
            this.lb_y1c0_1_safety_door_sw_open.Size = new System.Drawing.Size(240, 13);
            this.lb_y1c0_1_safety_door_sw_open.TabIndex = 193;
            this.lb_y1c0_1_safety_door_sw_open.Text = "[ Y1C0 ] 1 Safety Door SW Open";
            // 
            // pn_Loader_OUT3_Y1C0
            // 
            this.pn_Loader_OUT3_Y1C0.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT3_Y1C0.Location = new System.Drawing.Point(6, 28);
            this.pn_Loader_OUT3_Y1C0.Name = "pn_Loader_OUT3_Y1C0";
            this.pn_Loader_OUT3_Y1C0.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT3_Y1C0.TabIndex = 192;
            // 
            // tp_iostatus_ld_out4
            // 
            this.tp_iostatus_ld_out4.BackColor = System.Drawing.Color.DimGray;
            this.tp_iostatus_ld_out4.Controls.Add(this.lb_y1ff_spare);
            this.tp_iostatus_ld_out4.Controls.Add(this.pn_Loader_OUT4_Y1FF);
            this.tp_iostatus_ld_out4.Controls.Add(this.lb_y1fe_spare);
            this.tp_iostatus_ld_out4.Controls.Add(this.pn_Loader_OUT4_Y1FE);
            this.tp_iostatus_ld_out4.Controls.Add(this.lb_y1fd_spare);
            this.tp_iostatus_ld_out4.Controls.Add(this.pn_Loader_OUT4_Y1FD);
            this.tp_iostatus_ld_out4.Controls.Add(this.lb_y1fc_spare);
            this.tp_iostatus_ld_out4.Controls.Add(this.pn_Loader_OUT4_Y1FC);
            this.tp_iostatus_ld_out4.Controls.Add(this.lb_y1fb_spare);
            this.tp_iostatus_ld_out4.Controls.Add(this.pn_Loader_OUT4_Y1FB);
            this.tp_iostatus_ld_out4.Controls.Add(this.lb_y1fa_spare);
            this.tp_iostatus_ld_out4.Controls.Add(this.pn_Loader_OUT4_Y1FA);
            this.tp_iostatus_ld_out4.Controls.Add(this.lb_y1f9_spare);
            this.tp_iostatus_ld_out4.Controls.Add(this.pn_Loader_OUT4_Y1F9);
            this.tp_iostatus_ld_out4.Controls.Add(this.lb_y1f8_spare);
            this.tp_iostatus_ld_out4.Controls.Add(this.pn_Loader_OUT4_Y1F8);
            this.tp_iostatus_ld_out4.Controls.Add(this.lb_y1f7_spare);
            this.tp_iostatus_ld_out4.Controls.Add(this.pn_Loader_OUT4_Y1F7);
            this.tp_iostatus_ld_out4.Controls.Add(this.lb_y1f6_spare);
            this.tp_iostatus_ld_out4.Controls.Add(this.pn_Loader_OUT4_Y1F6);
            this.tp_iostatus_ld_out4.Controls.Add(this.lb_y1f5_spare);
            this.tp_iostatus_ld_out4.Controls.Add(this.pn_Loader_OUT4_Y1F5);
            this.tp_iostatus_ld_out4.Controls.Add(this.lb_y1f4_spare);
            this.tp_iostatus_ld_out4.Controls.Add(this.pn_Loader_OUT4_Y1F4);
            this.tp_iostatus_ld_out4.Controls.Add(this.lb_y1f3_spare);
            this.tp_iostatus_ld_out4.Controls.Add(this.pn_Loader_OUT4_Y1F3);
            this.tp_iostatus_ld_out4.Controls.Add(this.lb_y1f2_spare);
            this.tp_iostatus_ld_out4.Controls.Add(this.pn_Loader_OUT4_Y1F2);
            this.tp_iostatus_ld_out4.Controls.Add(this.lb_y1f1_spare);
            this.tp_iostatus_ld_out4.Controls.Add(this.pn_Loader_OUT4_Y1F1);
            this.tp_iostatus_ld_out4.Controls.Add(this.lb_y1f0_spare);
            this.tp_iostatus_ld_out4.Controls.Add(this.pn_Loader_OUT4_Y1F0);
            this.tp_iostatus_ld_out4.Controls.Add(this.lb_y1ef_spare);
            this.tp_iostatus_ld_out4.Controls.Add(this.pn_Loader_OUT4_Y1EF);
            this.tp_iostatus_ld_out4.Controls.Add(this.lb_y1ee_load_muting_down);
            this.tp_iostatus_ld_out4.Controls.Add(this.pn_Loader_OUT4_Y1EE);
            this.tp_iostatus_ld_out4.Controls.Add(this.lb_y1ed_load_muting_up);
            this.tp_iostatus_ld_out4.Controls.Add(this.pn_Loader_OUT4_Y1ED);
            this.tp_iostatus_ld_out4.Controls.Add(this.lb_y1ec_spare);
            this.tp_iostatus_ld_out4.Controls.Add(this.pn_Loader_OUT4_Y1EC);
            this.tp_iostatus_ld_out4.Controls.Add(this.label22);
            this.tp_iostatus_ld_out4.Controls.Add(this.pn_Loader_OUT4_Y1EB);
            this.tp_iostatus_ld_out4.Controls.Add(this.lb_y1ea_4_1_ld_lifter_muting_onoff);
            this.tp_iostatus_ld_out4.Controls.Add(this.pn_Loader_OUT4_Y1EA);
            this.tp_iostatus_ld_out4.Controls.Add(this.lb_y1e9_3_2_ld_lifter_muting_onoff);
            this.tp_iostatus_ld_out4.Controls.Add(this.pn_Loader_OUT4_Y1E9);
            this.tp_iostatus_ld_out4.Controls.Add(this.lb_y1e8_3_1_ld_lifter_muting_onoff);
            this.tp_iostatus_ld_out4.Controls.Add(this.pn_Loader_OUT4_Y1E8);
            this.tp_iostatus_ld_out4.Controls.Add(this.lb_y1e7_2_2_ld_lifter_muting_onoff);
            this.tp_iostatus_ld_out4.Controls.Add(this.pn_Loader_OUT4_Y1E7);
            this.tp_iostatus_ld_out4.Controls.Add(this.lb_y1e6_2_1_ld_lifter_muting_onoff);
            this.tp_iostatus_ld_out4.Controls.Add(this.pn_Loader_OUT4_Y1E6);
            this.tp_iostatus_ld_out4.Controls.Add(this.lb_y1e5_1_2_ld_lifter_muting_onoff);
            this.tp_iostatus_ld_out4.Controls.Add(this.pn_Loader_OUT4_Y1E5);
            this.tp_iostatus_ld_out4.Controls.Add(this.lb_y1e4_1_1_ld_lifter_muting_onoff);
            this.tp_iostatus_ld_out4.Controls.Add(this.pn_Loader_OUT4_Y1E4);
            this.tp_iostatus_ld_out4.Controls.Add(this.lb_y1e3_2_2_ld_out_muting_onoff);
            this.tp_iostatus_ld_out4.Controls.Add(this.pn_Loader_OUT4_Y1E3);
            this.tp_iostatus_ld_out4.Controls.Add(this.lb_y1e2_2_1_ld_out_muting_onoff);
            this.tp_iostatus_ld_out4.Controls.Add(this.pn_Loader_OUT4_Y1E2);
            this.tp_iostatus_ld_out4.Controls.Add(this.lb_y1e1_1_2_ld_in_muting_onoff);
            this.tp_iostatus_ld_out4.Controls.Add(this.pn_Loader_OUT4_Y1E1);
            this.tp_iostatus_ld_out4.Controls.Add(this.lb_y1e0_1_1_ld_in_muting_onoff);
            this.tp_iostatus_ld_out4.Controls.Add(this.pn_Loader_OUT4_Y1E0);
            this.tp_iostatus_ld_out4.Location = new System.Drawing.Point(4, 34);
            this.tp_iostatus_ld_out4.Name = "tp_iostatus_ld_out4";
            this.tp_iostatus_ld_out4.Padding = new System.Windows.Forms.Padding(3);
            this.tp_iostatus_ld_out4.Size = new System.Drawing.Size(846, 778);
            this.tp_iostatus_ld_out4.TabIndex = 3;
            this.tp_iostatus_ld_out4.Text = "OUT - 4";
            // 
            // lb_y1ff_spare
            // 
            this.lb_y1ff_spare.AutoSize = true;
            this.lb_y1ff_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1ff_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y1ff_spare.Location = new System.Drawing.Point(479, 730);
            this.lb_y1ff_spare.Name = "lb_y1ff_spare";
            this.lb_y1ff_spare.Size = new System.Drawing.Size(114, 13);
            this.lb_y1ff_spare.TabIndex = 319;
            this.lb_y1ff_spare.Text = "[ Y1FF ] Spare";
            // 
            // pn_Loader_OUT4_Y1FF
            // 
            this.pn_Loader_OUT4_Y1FF.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT4_Y1FF.Location = new System.Drawing.Point(433, 718);
            this.pn_Loader_OUT4_Y1FF.Name = "pn_Loader_OUT4_Y1FF";
            this.pn_Loader_OUT4_Y1FF.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT4_Y1FF.TabIndex = 318;
            // 
            // lb_y1fe_spare
            // 
            this.lb_y1fe_spare.AutoSize = true;
            this.lb_y1fe_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1fe_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y1fe_spare.Location = new System.Drawing.Point(479, 685);
            this.lb_y1fe_spare.Name = "lb_y1fe_spare";
            this.lb_y1fe_spare.Size = new System.Drawing.Size(115, 13);
            this.lb_y1fe_spare.TabIndex = 317;
            this.lb_y1fe_spare.Text = "[ Y1FE ] Spare";
            // 
            // pn_Loader_OUT4_Y1FE
            // 
            this.pn_Loader_OUT4_Y1FE.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT4_Y1FE.Location = new System.Drawing.Point(433, 672);
            this.pn_Loader_OUT4_Y1FE.Name = "pn_Loader_OUT4_Y1FE";
            this.pn_Loader_OUT4_Y1FE.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT4_Y1FE.TabIndex = 316;
            // 
            // lb_y1fd_spare
            // 
            this.lb_y1fd_spare.AutoSize = true;
            this.lb_y1fd_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1fd_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y1fd_spare.Location = new System.Drawing.Point(479, 640);
            this.lb_y1fd_spare.Name = "lb_y1fd_spare";
            this.lb_y1fd_spare.Size = new System.Drawing.Size(116, 13);
            this.lb_y1fd_spare.TabIndex = 315;
            this.lb_y1fd_spare.Text = "[ Y1FD ] Spare";
            // 
            // pn_Loader_OUT4_Y1FD
            // 
            this.pn_Loader_OUT4_Y1FD.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT4_Y1FD.Location = new System.Drawing.Point(433, 626);
            this.pn_Loader_OUT4_Y1FD.Name = "pn_Loader_OUT4_Y1FD";
            this.pn_Loader_OUT4_Y1FD.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT4_Y1FD.TabIndex = 314;
            // 
            // lb_y1fc_spare
            // 
            this.lb_y1fc_spare.AutoSize = true;
            this.lb_y1fc_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1fc_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y1fc_spare.Location = new System.Drawing.Point(479, 594);
            this.lb_y1fc_spare.Name = "lb_y1fc_spare";
            this.lb_y1fc_spare.Size = new System.Drawing.Size(116, 13);
            this.lb_y1fc_spare.TabIndex = 313;
            this.lb_y1fc_spare.Text = "[ Y1FC ] Spare";
            // 
            // pn_Loader_OUT4_Y1FC
            // 
            this.pn_Loader_OUT4_Y1FC.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT4_Y1FC.Location = new System.Drawing.Point(433, 580);
            this.pn_Loader_OUT4_Y1FC.Name = "pn_Loader_OUT4_Y1FC";
            this.pn_Loader_OUT4_Y1FC.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT4_Y1FC.TabIndex = 312;
            // 
            // lb_y1fb_spare
            // 
            this.lb_y1fb_spare.AutoSize = true;
            this.lb_y1fb_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1fb_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y1fb_spare.Location = new System.Drawing.Point(479, 548);
            this.lb_y1fb_spare.Name = "lb_y1fb_spare";
            this.lb_y1fb_spare.Size = new System.Drawing.Size(116, 13);
            this.lb_y1fb_spare.TabIndex = 311;
            this.lb_y1fb_spare.Text = "[ Y1FB ] Spare";
            // 
            // pn_Loader_OUT4_Y1FB
            // 
            this.pn_Loader_OUT4_Y1FB.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT4_Y1FB.Location = new System.Drawing.Point(433, 534);
            this.pn_Loader_OUT4_Y1FB.Name = "pn_Loader_OUT4_Y1FB";
            this.pn_Loader_OUT4_Y1FB.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT4_Y1FB.TabIndex = 310;
            // 
            // lb_y1fa_spare
            // 
            this.lb_y1fa_spare.AutoSize = true;
            this.lb_y1fa_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1fa_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y1fa_spare.Location = new System.Drawing.Point(479, 501);
            this.lb_y1fa_spare.Name = "lb_y1fa_spare";
            this.lb_y1fa_spare.Size = new System.Drawing.Size(115, 13);
            this.lb_y1fa_spare.TabIndex = 309;
            this.lb_y1fa_spare.Text = "[ Y1FA ] Spare";
            // 
            // pn_Loader_OUT4_Y1FA
            // 
            this.pn_Loader_OUT4_Y1FA.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT4_Y1FA.Location = new System.Drawing.Point(433, 488);
            this.pn_Loader_OUT4_Y1FA.Name = "pn_Loader_OUT4_Y1FA";
            this.pn_Loader_OUT4_Y1FA.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT4_Y1FA.TabIndex = 308;
            // 
            // lb_y1f9_spare
            // 
            this.lb_y1f9_spare.AutoSize = true;
            this.lb_y1f9_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1f9_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y1f9_spare.Location = new System.Drawing.Point(479, 454);
            this.lb_y1f9_spare.Name = "lb_y1f9_spare";
            this.lb_y1f9_spare.Size = new System.Drawing.Size(114, 13);
            this.lb_y1f9_spare.TabIndex = 307;
            this.lb_y1f9_spare.Text = "[ Y1F9 ] Spare";
            // 
            // pn_Loader_OUT4_Y1F9
            // 
            this.pn_Loader_OUT4_Y1F9.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT4_Y1F9.Location = new System.Drawing.Point(433, 442);
            this.pn_Loader_OUT4_Y1F9.Name = "pn_Loader_OUT4_Y1F9";
            this.pn_Loader_OUT4_Y1F9.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT4_Y1F9.TabIndex = 306;
            // 
            // lb_y1f8_spare
            // 
            this.lb_y1f8_spare.AutoSize = true;
            this.lb_y1f8_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1f8_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y1f8_spare.Location = new System.Drawing.Point(479, 409);
            this.lb_y1f8_spare.Name = "lb_y1f8_spare";
            this.lb_y1f8_spare.Size = new System.Drawing.Size(114, 13);
            this.lb_y1f8_spare.TabIndex = 305;
            this.lb_y1f8_spare.Text = "[ Y1F8 ] Spare";
            // 
            // pn_Loader_OUT4_Y1F8
            // 
            this.pn_Loader_OUT4_Y1F8.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT4_Y1F8.Location = new System.Drawing.Point(433, 396);
            this.pn_Loader_OUT4_Y1F8.Name = "pn_Loader_OUT4_Y1F8";
            this.pn_Loader_OUT4_Y1F8.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT4_Y1F8.TabIndex = 304;
            // 
            // lb_y1f7_spare
            // 
            this.lb_y1f7_spare.AutoSize = true;
            this.lb_y1f7_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1f7_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y1f7_spare.Location = new System.Drawing.Point(479, 364);
            this.lb_y1f7_spare.Name = "lb_y1f7_spare";
            this.lb_y1f7_spare.Size = new System.Drawing.Size(114, 13);
            this.lb_y1f7_spare.TabIndex = 303;
            this.lb_y1f7_spare.Text = "[ Y1F7 ] Spare";
            // 
            // pn_Loader_OUT4_Y1F7
            // 
            this.pn_Loader_OUT4_Y1F7.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT4_Y1F7.Location = new System.Drawing.Point(433, 350);
            this.pn_Loader_OUT4_Y1F7.Name = "pn_Loader_OUT4_Y1F7";
            this.pn_Loader_OUT4_Y1F7.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT4_Y1F7.TabIndex = 302;
            // 
            // lb_y1f6_spare
            // 
            this.lb_y1f6_spare.AutoSize = true;
            this.lb_y1f6_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1f6_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y1f6_spare.Location = new System.Drawing.Point(479, 319);
            this.lb_y1f6_spare.Name = "lb_y1f6_spare";
            this.lb_y1f6_spare.Size = new System.Drawing.Size(114, 13);
            this.lb_y1f6_spare.TabIndex = 301;
            this.lb_y1f6_spare.Text = "[ Y1F6 ] Spare";
            // 
            // pn_Loader_OUT4_Y1F6
            // 
            this.pn_Loader_OUT4_Y1F6.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT4_Y1F6.Location = new System.Drawing.Point(433, 304);
            this.pn_Loader_OUT4_Y1F6.Name = "pn_Loader_OUT4_Y1F6";
            this.pn_Loader_OUT4_Y1F6.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT4_Y1F6.TabIndex = 300;
            // 
            // lb_y1f5_spare
            // 
            this.lb_y1f5_spare.AutoSize = true;
            this.lb_y1f5_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1f5_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y1f5_spare.Location = new System.Drawing.Point(479, 271);
            this.lb_y1f5_spare.Name = "lb_y1f5_spare";
            this.lb_y1f5_spare.Size = new System.Drawing.Size(114, 13);
            this.lb_y1f5_spare.TabIndex = 299;
            this.lb_y1f5_spare.Text = "[ Y1F5 ] Spare";
            // 
            // pn_Loader_OUT4_Y1F5
            // 
            this.pn_Loader_OUT4_Y1F5.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT4_Y1F5.Location = new System.Drawing.Point(433, 258);
            this.pn_Loader_OUT4_Y1F5.Name = "pn_Loader_OUT4_Y1F5";
            this.pn_Loader_OUT4_Y1F5.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT4_Y1F5.TabIndex = 298;
            // 
            // lb_y1f4_spare
            // 
            this.lb_y1f4_spare.AutoSize = true;
            this.lb_y1f4_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1f4_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y1f4_spare.Location = new System.Drawing.Point(479, 226);
            this.lb_y1f4_spare.Name = "lb_y1f4_spare";
            this.lb_y1f4_spare.Size = new System.Drawing.Size(114, 13);
            this.lb_y1f4_spare.TabIndex = 297;
            this.lb_y1f4_spare.Text = "[ Y1F4 ] Spare";
            // 
            // pn_Loader_OUT4_Y1F4
            // 
            this.pn_Loader_OUT4_Y1F4.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT4_Y1F4.Location = new System.Drawing.Point(433, 212);
            this.pn_Loader_OUT4_Y1F4.Name = "pn_Loader_OUT4_Y1F4";
            this.pn_Loader_OUT4_Y1F4.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT4_Y1F4.TabIndex = 296;
            // 
            // lb_y1f3_spare
            // 
            this.lb_y1f3_spare.AutoSize = true;
            this.lb_y1f3_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1f3_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y1f3_spare.Location = new System.Drawing.Point(479, 179);
            this.lb_y1f3_spare.Name = "lb_y1f3_spare";
            this.lb_y1f3_spare.Size = new System.Drawing.Size(114, 13);
            this.lb_y1f3_spare.TabIndex = 295;
            this.lb_y1f3_spare.Text = "[ Y1F3 ] Spare";
            // 
            // pn_Loader_OUT4_Y1F3
            // 
            this.pn_Loader_OUT4_Y1F3.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT4_Y1F3.Location = new System.Drawing.Point(433, 166);
            this.pn_Loader_OUT4_Y1F3.Name = "pn_Loader_OUT4_Y1F3";
            this.pn_Loader_OUT4_Y1F3.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT4_Y1F3.TabIndex = 294;
            // 
            // lb_y1f2_spare
            // 
            this.lb_y1f2_spare.AutoSize = true;
            this.lb_y1f2_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1f2_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y1f2_spare.Location = new System.Drawing.Point(479, 133);
            this.lb_y1f2_spare.Name = "lb_y1f2_spare";
            this.lb_y1f2_spare.Size = new System.Drawing.Size(114, 13);
            this.lb_y1f2_spare.TabIndex = 293;
            this.lb_y1f2_spare.Text = "[ Y1F2 ] Spare";
            // 
            // pn_Loader_OUT4_Y1F2
            // 
            this.pn_Loader_OUT4_Y1F2.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT4_Y1F2.Location = new System.Drawing.Point(433, 120);
            this.pn_Loader_OUT4_Y1F2.Name = "pn_Loader_OUT4_Y1F2";
            this.pn_Loader_OUT4_Y1F2.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT4_Y1F2.TabIndex = 292;
            // 
            // lb_y1f1_spare
            // 
            this.lb_y1f1_spare.AutoSize = true;
            this.lb_y1f1_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1f1_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y1f1_spare.Location = new System.Drawing.Point(479, 86);
            this.lb_y1f1_spare.Name = "lb_y1f1_spare";
            this.lb_y1f1_spare.Size = new System.Drawing.Size(114, 13);
            this.lb_y1f1_spare.TabIndex = 291;
            this.lb_y1f1_spare.Text = "[ Y1F1 ] Spare";
            // 
            // pn_Loader_OUT4_Y1F1
            // 
            this.pn_Loader_OUT4_Y1F1.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT4_Y1F1.Location = new System.Drawing.Point(433, 74);
            this.pn_Loader_OUT4_Y1F1.Name = "pn_Loader_OUT4_Y1F1";
            this.pn_Loader_OUT4_Y1F1.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT4_Y1F1.TabIndex = 290;
            // 
            // lb_y1f0_spare
            // 
            this.lb_y1f0_spare.AutoSize = true;
            this.lb_y1f0_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1f0_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y1f0_spare.Location = new System.Drawing.Point(479, 42);
            this.lb_y1f0_spare.Name = "lb_y1f0_spare";
            this.lb_y1f0_spare.Size = new System.Drawing.Size(114, 13);
            this.lb_y1f0_spare.TabIndex = 289;
            this.lb_y1f0_spare.Text = "[ Y1F0 ] Spare";
            // 
            // pn_Loader_OUT4_Y1F0
            // 
            this.pn_Loader_OUT4_Y1F0.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT4_Y1F0.Location = new System.Drawing.Point(433, 28);
            this.pn_Loader_OUT4_Y1F0.Name = "pn_Loader_OUT4_Y1F0";
            this.pn_Loader_OUT4_Y1F0.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT4_Y1F0.TabIndex = 288;
            // 
            // lb_y1ef_spare
            // 
            this.lb_y1ef_spare.AutoSize = true;
            this.lb_y1ef_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1ef_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y1ef_spare.Location = new System.Drawing.Point(52, 730);
            this.lb_y1ef_spare.Name = "lb_y1ef_spare";
            this.lb_y1ef_spare.Size = new System.Drawing.Size(115, 13);
            this.lb_y1ef_spare.TabIndex = 287;
            this.lb_y1ef_spare.Text = "[ Y1EF ] Spare";
            // 
            // pn_Loader_OUT4_Y1EF
            // 
            this.pn_Loader_OUT4_Y1EF.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT4_Y1EF.Location = new System.Drawing.Point(6, 718);
            this.pn_Loader_OUT4_Y1EF.Name = "pn_Loader_OUT4_Y1EF";
            this.pn_Loader_OUT4_Y1EF.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT4_Y1EF.TabIndex = 286;
            // 
            // lb_y1ee_load_muting_down
            // 
            this.lb_y1ee_load_muting_down.AutoSize = true;
            this.lb_y1ee_load_muting_down.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1ee_load_muting_down.ForeColor = System.Drawing.Color.White;
            this.lb_y1ee_load_muting_down.Location = new System.Drawing.Point(52, 685);
            this.lb_y1ee_load_muting_down.Name = "lb_y1ee_load_muting_down";
            this.lb_y1ee_load_muting_down.Size = new System.Drawing.Size(219, 13);
            this.lb_y1ee_load_muting_down.TabIndex = 285;
            this.lb_y1ee_load_muting_down.Text = "[ Y1EE ] load 뮤팅 down lamp";
            // 
            // pn_Loader_OUT4_Y1EE
            // 
            this.pn_Loader_OUT4_Y1EE.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT4_Y1EE.Location = new System.Drawing.Point(6, 672);
            this.pn_Loader_OUT4_Y1EE.Name = "pn_Loader_OUT4_Y1EE";
            this.pn_Loader_OUT4_Y1EE.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT4_Y1EE.TabIndex = 284;
            // 
            // lb_y1ed_load_muting_up
            // 
            this.lb_y1ed_load_muting_up.AutoSize = true;
            this.lb_y1ed_load_muting_up.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1ed_load_muting_up.ForeColor = System.Drawing.Color.White;
            this.lb_y1ed_load_muting_up.Location = new System.Drawing.Point(52, 640);
            this.lb_y1ed_load_muting_up.Name = "lb_y1ed_load_muting_up";
            this.lb_y1ed_load_muting_up.Size = new System.Drawing.Size(200, 13);
            this.lb_y1ed_load_muting_up.TabIndex = 283;
            this.lb_y1ed_load_muting_up.Text = "[ Y1ED ] load 뮤팅 up lamp";
            // 
            // pn_Loader_OUT4_Y1ED
            // 
            this.pn_Loader_OUT4_Y1ED.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT4_Y1ED.Location = new System.Drawing.Point(6, 626);
            this.pn_Loader_OUT4_Y1ED.Name = "pn_Loader_OUT4_Y1ED";
            this.pn_Loader_OUT4_Y1ED.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT4_Y1ED.TabIndex = 282;
            // 
            // lb_y1ec_spare
            // 
            this.lb_y1ec_spare.AutoSize = true;
            this.lb_y1ec_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1ec_spare.ForeColor = System.Drawing.Color.White;
            this.lb_y1ec_spare.Location = new System.Drawing.Point(52, 594);
            this.lb_y1ec_spare.Name = "lb_y1ec_spare";
            this.lb_y1ec_spare.Size = new System.Drawing.Size(117, 13);
            this.lb_y1ec_spare.TabIndex = 281;
            this.lb_y1ec_spare.Text = "[ Y1EC ] Spare";
            // 
            // pn_Loader_OUT4_Y1EC
            // 
            this.pn_Loader_OUT4_Y1EC.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT4_Y1EC.Location = new System.Drawing.Point(6, 580);
            this.pn_Loader_OUT4_Y1EC.Name = "pn_Loader_OUT4_Y1EC";
            this.pn_Loader_OUT4_Y1EC.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT4_Y1EC.TabIndex = 280;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label22.ForeColor = System.Drawing.Color.White;
            this.label22.Location = new System.Drawing.Point(52, 548);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(271, 13);
            this.label22.TabIndex = 279;
            this.label22.Text = "[ Y1EB ] 4-2 L/D Lifter Muting On/Off";
            // 
            // pn_Loader_OUT4_Y1EB
            // 
            this.pn_Loader_OUT4_Y1EB.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT4_Y1EB.Location = new System.Drawing.Point(6, 534);
            this.pn_Loader_OUT4_Y1EB.Name = "pn_Loader_OUT4_Y1EB";
            this.pn_Loader_OUT4_Y1EB.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT4_Y1EB.TabIndex = 278;
            // 
            // lb_y1ea_4_1_ld_lifter_muting_onoff
            // 
            this.lb_y1ea_4_1_ld_lifter_muting_onoff.AutoSize = true;
            this.lb_y1ea_4_1_ld_lifter_muting_onoff.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1ea_4_1_ld_lifter_muting_onoff.ForeColor = System.Drawing.Color.White;
            this.lb_y1ea_4_1_ld_lifter_muting_onoff.Location = new System.Drawing.Point(52, 501);
            this.lb_y1ea_4_1_ld_lifter_muting_onoff.Name = "lb_y1ea_4_1_ld_lifter_muting_onoff";
            this.lb_y1ea_4_1_ld_lifter_muting_onoff.Size = new System.Drawing.Size(270, 13);
            this.lb_y1ea_4_1_ld_lifter_muting_onoff.TabIndex = 277;
            this.lb_y1ea_4_1_ld_lifter_muting_onoff.Text = "[ Y1EA ] 4-1 L/D Lifter Muting On/Off";
            // 
            // pn_Loader_OUT4_Y1EA
            // 
            this.pn_Loader_OUT4_Y1EA.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT4_Y1EA.Location = new System.Drawing.Point(6, 488);
            this.pn_Loader_OUT4_Y1EA.Name = "pn_Loader_OUT4_Y1EA";
            this.pn_Loader_OUT4_Y1EA.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT4_Y1EA.TabIndex = 276;
            // 
            // lb_y1e9_3_2_ld_lifter_muting_onoff
            // 
            this.lb_y1e9_3_2_ld_lifter_muting_onoff.AutoSize = true;
            this.lb_y1e9_3_2_ld_lifter_muting_onoff.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1e9_3_2_ld_lifter_muting_onoff.ForeColor = System.Drawing.Color.White;
            this.lb_y1e9_3_2_ld_lifter_muting_onoff.Location = new System.Drawing.Point(52, 454);
            this.lb_y1e9_3_2_ld_lifter_muting_onoff.Name = "lb_y1e9_3_2_ld_lifter_muting_onoff";
            this.lb_y1e9_3_2_ld_lifter_muting_onoff.Size = new System.Drawing.Size(269, 13);
            this.lb_y1e9_3_2_ld_lifter_muting_onoff.TabIndex = 275;
            this.lb_y1e9_3_2_ld_lifter_muting_onoff.Text = "[ Y1E9 ] 3-2 L/D Lifter Muting On/Off";
            // 
            // pn_Loader_OUT4_Y1E9
            // 
            this.pn_Loader_OUT4_Y1E9.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT4_Y1E9.Location = new System.Drawing.Point(6, 442);
            this.pn_Loader_OUT4_Y1E9.Name = "pn_Loader_OUT4_Y1E9";
            this.pn_Loader_OUT4_Y1E9.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT4_Y1E9.TabIndex = 274;
            // 
            // lb_y1e8_3_1_ld_lifter_muting_onoff
            // 
            this.lb_y1e8_3_1_ld_lifter_muting_onoff.AutoSize = true;
            this.lb_y1e8_3_1_ld_lifter_muting_onoff.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1e8_3_1_ld_lifter_muting_onoff.ForeColor = System.Drawing.Color.White;
            this.lb_y1e8_3_1_ld_lifter_muting_onoff.Location = new System.Drawing.Point(52, 409);
            this.lb_y1e8_3_1_ld_lifter_muting_onoff.Name = "lb_y1e8_3_1_ld_lifter_muting_onoff";
            this.lb_y1e8_3_1_ld_lifter_muting_onoff.Size = new System.Drawing.Size(269, 13);
            this.lb_y1e8_3_1_ld_lifter_muting_onoff.TabIndex = 273;
            this.lb_y1e8_3_1_ld_lifter_muting_onoff.Text = "[ Y1E8 ] 3-1 L/D Lifter Muting On/Off";
            // 
            // pn_Loader_OUT4_Y1E8
            // 
            this.pn_Loader_OUT4_Y1E8.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT4_Y1E8.Location = new System.Drawing.Point(6, 396);
            this.pn_Loader_OUT4_Y1E8.Name = "pn_Loader_OUT4_Y1E8";
            this.pn_Loader_OUT4_Y1E8.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT4_Y1E8.TabIndex = 272;
            // 
            // lb_y1e7_2_2_ld_lifter_muting_onoff
            // 
            this.lb_y1e7_2_2_ld_lifter_muting_onoff.AutoSize = true;
            this.lb_y1e7_2_2_ld_lifter_muting_onoff.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1e7_2_2_ld_lifter_muting_onoff.ForeColor = System.Drawing.Color.White;
            this.lb_y1e7_2_2_ld_lifter_muting_onoff.Location = new System.Drawing.Point(52, 364);
            this.lb_y1e7_2_2_ld_lifter_muting_onoff.Name = "lb_y1e7_2_2_ld_lifter_muting_onoff";
            this.lb_y1e7_2_2_ld_lifter_muting_onoff.Size = new System.Drawing.Size(269, 13);
            this.lb_y1e7_2_2_ld_lifter_muting_onoff.TabIndex = 271;
            this.lb_y1e7_2_2_ld_lifter_muting_onoff.Text = "[ Y1E7 ] 2-2 L/D Lifter Muting On/Off";
            // 
            // pn_Loader_OUT4_Y1E7
            // 
            this.pn_Loader_OUT4_Y1E7.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT4_Y1E7.Location = new System.Drawing.Point(6, 350);
            this.pn_Loader_OUT4_Y1E7.Name = "pn_Loader_OUT4_Y1E7";
            this.pn_Loader_OUT4_Y1E7.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT4_Y1E7.TabIndex = 270;
            // 
            // lb_y1e6_2_1_ld_lifter_muting_onoff
            // 
            this.lb_y1e6_2_1_ld_lifter_muting_onoff.AutoSize = true;
            this.lb_y1e6_2_1_ld_lifter_muting_onoff.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1e6_2_1_ld_lifter_muting_onoff.ForeColor = System.Drawing.Color.White;
            this.lb_y1e6_2_1_ld_lifter_muting_onoff.Location = new System.Drawing.Point(52, 319);
            this.lb_y1e6_2_1_ld_lifter_muting_onoff.Name = "lb_y1e6_2_1_ld_lifter_muting_onoff";
            this.lb_y1e6_2_1_ld_lifter_muting_onoff.Size = new System.Drawing.Size(269, 13);
            this.lb_y1e6_2_1_ld_lifter_muting_onoff.TabIndex = 269;
            this.lb_y1e6_2_1_ld_lifter_muting_onoff.Text = "[ Y1E6 ] 2-1 L/D Lifter Muting On/Off";
            // 
            // pn_Loader_OUT4_Y1E6
            // 
            this.pn_Loader_OUT4_Y1E6.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT4_Y1E6.Location = new System.Drawing.Point(6, 304);
            this.pn_Loader_OUT4_Y1E6.Name = "pn_Loader_OUT4_Y1E6";
            this.pn_Loader_OUT4_Y1E6.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT4_Y1E6.TabIndex = 268;
            // 
            // lb_y1e5_1_2_ld_lifter_muting_onoff
            // 
            this.lb_y1e5_1_2_ld_lifter_muting_onoff.AutoSize = true;
            this.lb_y1e5_1_2_ld_lifter_muting_onoff.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1e5_1_2_ld_lifter_muting_onoff.ForeColor = System.Drawing.Color.White;
            this.lb_y1e5_1_2_ld_lifter_muting_onoff.Location = new System.Drawing.Point(52, 271);
            this.lb_y1e5_1_2_ld_lifter_muting_onoff.Name = "lb_y1e5_1_2_ld_lifter_muting_onoff";
            this.lb_y1e5_1_2_ld_lifter_muting_onoff.Size = new System.Drawing.Size(269, 13);
            this.lb_y1e5_1_2_ld_lifter_muting_onoff.TabIndex = 267;
            this.lb_y1e5_1_2_ld_lifter_muting_onoff.Text = "[ Y1E5 ] 1-2 L/D Lifter Muting On/Off";
            // 
            // pn_Loader_OUT4_Y1E5
            // 
            this.pn_Loader_OUT4_Y1E5.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT4_Y1E5.Location = new System.Drawing.Point(6, 258);
            this.pn_Loader_OUT4_Y1E5.Name = "pn_Loader_OUT4_Y1E5";
            this.pn_Loader_OUT4_Y1E5.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT4_Y1E5.TabIndex = 266;
            // 
            // lb_y1e4_1_1_ld_lifter_muting_onoff
            // 
            this.lb_y1e4_1_1_ld_lifter_muting_onoff.AutoSize = true;
            this.lb_y1e4_1_1_ld_lifter_muting_onoff.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1e4_1_1_ld_lifter_muting_onoff.ForeColor = System.Drawing.Color.White;
            this.lb_y1e4_1_1_ld_lifter_muting_onoff.Location = new System.Drawing.Point(52, 226);
            this.lb_y1e4_1_1_ld_lifter_muting_onoff.Name = "lb_y1e4_1_1_ld_lifter_muting_onoff";
            this.lb_y1e4_1_1_ld_lifter_muting_onoff.Size = new System.Drawing.Size(269, 13);
            this.lb_y1e4_1_1_ld_lifter_muting_onoff.TabIndex = 265;
            this.lb_y1e4_1_1_ld_lifter_muting_onoff.Text = "[ Y1E4 ] 1-1 L/D Lifter Muting On/Off";
            // 
            // pn_Loader_OUT4_Y1E4
            // 
            this.pn_Loader_OUT4_Y1E4.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT4_Y1E4.Location = new System.Drawing.Point(6, 212);
            this.pn_Loader_OUT4_Y1E4.Name = "pn_Loader_OUT4_Y1E4";
            this.pn_Loader_OUT4_Y1E4.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT4_Y1E4.TabIndex = 264;
            // 
            // lb_y1e3_2_2_ld_out_muting_onoff
            // 
            this.lb_y1e3_2_2_ld_out_muting_onoff.AutoSize = true;
            this.lb_y1e3_2_2_ld_out_muting_onoff.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1e3_2_2_ld_out_muting_onoff.ForeColor = System.Drawing.Color.White;
            this.lb_y1e3_2_2_ld_out_muting_onoff.Location = new System.Drawing.Point(52, 179);
            this.lb_y1e3_2_2_ld_out_muting_onoff.Name = "lb_y1e3_2_2_ld_out_muting_onoff";
            this.lb_y1e3_2_2_ld_out_muting_onoff.Size = new System.Drawing.Size(262, 13);
            this.lb_y1e3_2_2_ld_out_muting_onoff.TabIndex = 263;
            this.lb_y1e3_2_2_ld_out_muting_onoff.Text = "[ Y1E3 ] 2-2 L/D 배출 Muting On/Off";
            // 
            // pn_Loader_OUT4_Y1E3
            // 
            this.pn_Loader_OUT4_Y1E3.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT4_Y1E3.Location = new System.Drawing.Point(6, 166);
            this.pn_Loader_OUT4_Y1E3.Name = "pn_Loader_OUT4_Y1E3";
            this.pn_Loader_OUT4_Y1E3.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT4_Y1E3.TabIndex = 262;
            // 
            // lb_y1e2_2_1_ld_out_muting_onoff
            // 
            this.lb_y1e2_2_1_ld_out_muting_onoff.AutoSize = true;
            this.lb_y1e2_2_1_ld_out_muting_onoff.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1e2_2_1_ld_out_muting_onoff.ForeColor = System.Drawing.Color.White;
            this.lb_y1e2_2_1_ld_out_muting_onoff.Location = new System.Drawing.Point(52, 133);
            this.lb_y1e2_2_1_ld_out_muting_onoff.Name = "lb_y1e2_2_1_ld_out_muting_onoff";
            this.lb_y1e2_2_1_ld_out_muting_onoff.Size = new System.Drawing.Size(262, 13);
            this.lb_y1e2_2_1_ld_out_muting_onoff.TabIndex = 261;
            this.lb_y1e2_2_1_ld_out_muting_onoff.Text = "[ Y1E2 ] 2-1 L/D 배출 Muting On/Off";
            // 
            // pn_Loader_OUT4_Y1E2
            // 
            this.pn_Loader_OUT4_Y1E2.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT4_Y1E2.Location = new System.Drawing.Point(6, 120);
            this.pn_Loader_OUT4_Y1E2.Name = "pn_Loader_OUT4_Y1E2";
            this.pn_Loader_OUT4_Y1E2.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT4_Y1E2.TabIndex = 260;
            // 
            // lb_y1e1_1_2_ld_in_muting_onoff
            // 
            this.lb_y1e1_1_2_ld_in_muting_onoff.AutoSize = true;
            this.lb_y1e1_1_2_ld_in_muting_onoff.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1e1_1_2_ld_in_muting_onoff.ForeColor = System.Drawing.Color.White;
            this.lb_y1e1_1_2_ld_in_muting_onoff.Location = new System.Drawing.Point(52, 86);
            this.lb_y1e1_1_2_ld_in_muting_onoff.Name = "lb_y1e1_1_2_ld_in_muting_onoff";
            this.lb_y1e1_1_2_ld_in_muting_onoff.Size = new System.Drawing.Size(262, 13);
            this.lb_y1e1_1_2_ld_in_muting_onoff.TabIndex = 259;
            this.lb_y1e1_1_2_ld_in_muting_onoff.Text = "[ Y1E1 ] 1-2 L/D 투입 Muting On/Off";
            // 
            // pn_Loader_OUT4_Y1E1
            // 
            this.pn_Loader_OUT4_Y1E1.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT4_Y1E1.Location = new System.Drawing.Point(6, 74);
            this.pn_Loader_OUT4_Y1E1.Name = "pn_Loader_OUT4_Y1E1";
            this.pn_Loader_OUT4_Y1E1.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT4_Y1E1.TabIndex = 258;
            // 
            // lb_y1e0_1_1_ld_in_muting_onoff
            // 
            this.lb_y1e0_1_1_ld_in_muting_onoff.AutoSize = true;
            this.lb_y1e0_1_1_ld_in_muting_onoff.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y1e0_1_1_ld_in_muting_onoff.ForeColor = System.Drawing.Color.White;
            this.lb_y1e0_1_1_ld_in_muting_onoff.Location = new System.Drawing.Point(52, 42);
            this.lb_y1e0_1_1_ld_in_muting_onoff.Name = "lb_y1e0_1_1_ld_in_muting_onoff";
            this.lb_y1e0_1_1_ld_in_muting_onoff.Size = new System.Drawing.Size(262, 13);
            this.lb_y1e0_1_1_ld_in_muting_onoff.TabIndex = 257;
            this.lb_y1e0_1_1_ld_in_muting_onoff.Text = "[ Y1E0 ] 1-1 L/D 투입 Muting On/Off";
            // 
            // pn_Loader_OUT4_Y1E0
            // 
            this.pn_Loader_OUT4_Y1E0.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_OUT4_Y1E0.Location = new System.Drawing.Point(6, 28);
            this.pn_Loader_OUT4_Y1E0.Name = "pn_Loader_OUT4_Y1E0";
            this.pn_Loader_OUT4_Y1E0.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_OUT4_Y1E0.TabIndex = 256;
            // 
            // tc_iostatus_ld_in
            // 
            this.tc_iostatus_ld_in.Controls.Add(this.tp_iostatus_ld_in1);
            this.tc_iostatus_ld_in.Controls.Add(this.tp_iostatus_ld_in2);
            this.tc_iostatus_ld_in.Controls.Add(this.tp_iostatus_ld_in3);
            this.tc_iostatus_ld_in.Controls.Add(this.tp_iostatus_ld_in4);
            this.tc_iostatus_ld_in.ItemSize = new System.Drawing.Size(212, 30);
            this.tc_iostatus_ld_in.Location = new System.Drawing.Point(6, 6);
            this.tc_iostatus_ld_in.Name = "tc_iostatus_ld_in";
            this.tc_iostatus_ld_in.SelectedIndex = 0;
            this.tc_iostatus_ld_in.Size = new System.Drawing.Size(854, 816);
            this.tc_iostatus_ld_in.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tc_iostatus_ld_in.TabIndex = 0;
            // 
            // tp_iostatus_ld_in1
            // 
            this.tp_iostatus_ld_in1.BackColor = System.Drawing.Color.DimGray;
            this.tp_iostatus_ld_in1.Controls.Add(this.lb_x11f_spare);
            this.tp_iostatus_ld_in1.Controls.Add(this.pn_Loader_IN1_X11F);
            this.tp_iostatus_ld_in1.Controls.Add(this.lb_x11e_spare);
            this.tp_iostatus_ld_in1.Controls.Add(this.pn_Loader_IN1_X11E);
            this.tp_iostatus_ld_in1.Controls.Add(this.lb_x11d_cell_trans_crush_sensor);
            this.tp_iostatus_ld_in1.Controls.Add(this.pn_Loader_IN1_X11D);
            this.tp_iostatus_ld_in1.Controls.Add(this.lb_x11c_spare);
            this.tp_iostatus_ld_in1.Controls.Add(this.pn_Loader_IN1_X11C);
            this.tp_iostatus_ld_in1.Controls.Add(this.lb_x11b_spare);
            this.tp_iostatus_ld_in1.Controls.Add(this.pn_Loader_IN1_X11B);
            this.tp_iostatus_ld_in1.Controls.Add(this.lb_x11a_spare);
            this.tp_iostatus_ld_in1.Controls.Add(this.pn_Loader_IN1_X11A);
            this.tp_iostatus_ld_in1.Controls.Add(this.lb_x119_7_safety_door_sw_open);
            this.tp_iostatus_ld_in1.Controls.Add(this.pn_Loader_IN1_X119);
            this.tp_iostatus_ld_in1.Controls.Add(this.lb_x118_6_safety_door_sw_open);
            this.tp_iostatus_ld_in1.Controls.Add(this.pn_Loader_IN1_X118);
            this.tp_iostatus_ld_in1.Controls.Add(this.lb_x117_5_safety_door_sw_open);
            this.tp_iostatus_ld_in1.Controls.Add(this.pn_Loader_IN1_X117);
            this.tp_iostatus_ld_in1.Controls.Add(this.lb_x116_4_safety_door_sw_open);
            this.tp_iostatus_ld_in1.Controls.Add(this.pn_Loader_IN1_X116);
            this.tp_iostatus_ld_in1.Controls.Add(this.lb_x115_3_safety_door_sw_open);
            this.tp_iostatus_ld_in1.Controls.Add(this.pn_Loader_IN1_X115);
            this.tp_iostatus_ld_in1.Controls.Add(this.lb_x114_2_safety_door_sw_open);
            this.tp_iostatus_ld_in1.Controls.Add(this.pn_Loader_IN1_X114);
            this.tp_iostatus_ld_in1.Controls.Add(this.lb_x113_1_safety_door_sw_open);
            this.tp_iostatus_ld_in1.Controls.Add(this.pn_Loader_IN1_X113);
            this.tp_iostatus_ld_in1.Controls.Add(this.lb_x112_3_safety_enable_grip_sw_on);
            this.tp_iostatus_ld_in1.Controls.Add(this.pn_Loader_IN1_X112);
            this.tp_iostatus_ld_in1.Controls.Add(this.lb_x111_2_safety_enable_grip_sw_on);
            this.tp_iostatus_ld_in1.Controls.Add(this.pn_Loader_IN1_X111);
            this.tp_iostatus_ld_in1.Controls.Add(this.lb_x110_1_safety_enable_grip_sw_on);
            this.tp_iostatus_ld_in1.Controls.Add(this.pn_Loader_IN1_X110);
            this.tp_iostatus_ld_in1.Controls.Add(this.lb_x10f_3_emergency_stop_enable_grip_sw);
            this.tp_iostatus_ld_in1.Controls.Add(this.pn_Loader_IN1_X10F);
            this.tp_iostatus_ld_in1.Controls.Add(this.lb_x10e_2_emergency_stop_enable_grip_sw);
            this.tp_iostatus_ld_in1.Controls.Add(this.pn_Loader_IN1_X10E);
            this.tp_iostatus_ld_in1.Controls.Add(this.lb_x10d_1_emergency_stop_enable_grip_sw);
            this.tp_iostatus_ld_in1.Controls.Add(this.pn_Loader_IN1_X10D);
            this.tp_iostatus_ld_in1.Controls.Add(this.lb_x10c_3_emergency_stop);
            this.tp_iostatus_ld_in1.Controls.Add(this.pn_Loader_IN1_X10C);
            this.tp_iostatus_ld_in1.Controls.Add(this.lb_x10b_2_emergency_stop);
            this.tp_iostatus_ld_in1.Controls.Add(this.pn_Loader_IN1_X10B);
            this.tp_iostatus_ld_in1.Controls.Add(this.lb_x10a_1_emergency_stop);
            this.tp_iostatus_ld_in1.Controls.Add(this.pn_Loader_IN1_X10A);
            this.tp_iostatus_ld_in1.Controls.Add(this.lb_x109_ld_lifter_muting_on);
            this.tp_iostatus_ld_in1.Controls.Add(this.pn_Loader_IN1_X109);
            this.tp_iostatus_ld_in1.Controls.Add(this.lb_x108_ld_lifter_muting_on);
            this.tp_iostatus_ld_in1.Controls.Add(this.pn_Loader_IN1_X108);
            this.tp_iostatus_ld_in1.Controls.Add(this.lb_x107_ld_lifter_muting_on);
            this.tp_iostatus_ld_in1.Controls.Add(this.pn_Loader_IN1_X107);
            this.tp_iostatus_ld_in1.Controls.Add(this.lb_x106_ld_lifter_muting_on);
            this.tp_iostatus_ld_in1.Controls.Add(this.pn_Loader_IN1_X106);
            this.tp_iostatus_ld_in1.Controls.Add(this.lb_x105_ld_out_muting_on);
            this.tp_iostatus_ld_in1.Controls.Add(this.pn_Loader_IN1_X105);
            this.tp_iostatus_ld_in1.Controls.Add(this.lb_x104_ld_in_muting_on);
            this.tp_iostatus_ld_in1.Controls.Add(this.pn_Loader_IN1_X104);
            this.tp_iostatus_ld_in1.Controls.Add(this.lb_x103_control_reset_switch);
            this.tp_iostatus_ld_in1.Controls.Add(this.pn_Loader_IN1_X103);
            this.tp_iostatus_ld_in1.Controls.Add(this.lb_x102_ld_reset_switch);
            this.tp_iostatus_ld_in1.Controls.Add(this.pn_Loader_IN1_X102);
            this.tp_iostatus_ld_in1.Controls.Add(this.lb_x101_ld_out_muting_switch_on_off);
            this.tp_iostatus_ld_in1.Controls.Add(this.pn_Loader_IN1_X101);
            this.tp_iostatus_ld_in1.Controls.Add(this.lb_ld_in_muting_switch_on_off);
            this.tp_iostatus_ld_in1.Controls.Add(this.pn_Loader_IN1_X100);
            this.tp_iostatus_ld_in1.Location = new System.Drawing.Point(4, 34);
            this.tp_iostatus_ld_in1.Name = "tp_iostatus_ld_in1";
            this.tp_iostatus_ld_in1.Padding = new System.Windows.Forms.Padding(3);
            this.tp_iostatus_ld_in1.Size = new System.Drawing.Size(846, 778);
            this.tp_iostatus_ld_in1.TabIndex = 0;
            this.tp_iostatus_ld_in1.Text = "IN - 1";
            // 
            // lb_x11f_spare
            // 
            this.lb_x11f_spare.AutoSize = true;
            this.lb_x11f_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x11f_spare.ForeColor = System.Drawing.Color.White;
            this.lb_x11f_spare.Location = new System.Drawing.Point(479, 730);
            this.lb_x11f_spare.Name = "lb_x11f_spare";
            this.lb_x11f_spare.Size = new System.Drawing.Size(114, 13);
            this.lb_x11f_spare.TabIndex = 63;
            this.lb_x11f_spare.Text = "[ X11F ] Spare";
            // 
            // pn_Loader_IN1_X11F
            // 
            this.pn_Loader_IN1_X11F.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN1_X11F.Location = new System.Drawing.Point(433, 718);
            this.pn_Loader_IN1_X11F.Name = "pn_Loader_IN1_X11F";
            this.pn_Loader_IN1_X11F.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN1_X11F.TabIndex = 62;
            // 
            // lb_x11e_spare
            // 
            this.lb_x11e_spare.AutoSize = true;
            this.lb_x11e_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x11e_spare.ForeColor = System.Drawing.Color.White;
            this.lb_x11e_spare.Location = new System.Drawing.Point(479, 685);
            this.lb_x11e_spare.Name = "lb_x11e_spare";
            this.lb_x11e_spare.Size = new System.Drawing.Size(115, 13);
            this.lb_x11e_spare.TabIndex = 61;
            this.lb_x11e_spare.Text = "[ X11E ] Spare";
            // 
            // pn_Loader_IN1_X11E
            // 
            this.pn_Loader_IN1_X11E.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN1_X11E.Location = new System.Drawing.Point(433, 672);
            this.pn_Loader_IN1_X11E.Name = "pn_Loader_IN1_X11E";
            this.pn_Loader_IN1_X11E.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN1_X11E.TabIndex = 60;
            // 
            // lb_x11d_cell_trans_crush_sensor
            // 
            this.lb_x11d_cell_trans_crush_sensor.AutoSize = true;
            this.lb_x11d_cell_trans_crush_sensor.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x11d_cell_trans_crush_sensor.ForeColor = System.Drawing.Color.White;
            this.lb_x11d_cell_trans_crush_sensor.Location = new System.Drawing.Point(479, 640);
            this.lb_x11d_cell_trans_crush_sensor.Name = "lb_x11d_cell_trans_crush_sensor";
            this.lb_x11d_cell_trans_crush_sensor.Size = new System.Drawing.Size(287, 13);
            this.lb_x11d_cell_trans_crush_sensor.TabIndex = 59;
            this.lb_x11d_cell_trans_crush_sensor.Text = "[ X11D ] 1 Cell 취출 이재기 충돌방지 센서";
            // 
            // pn_Loader_IN1_X11D
            // 
            this.pn_Loader_IN1_X11D.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN1_X11D.Location = new System.Drawing.Point(433, 626);
            this.pn_Loader_IN1_X11D.Name = "pn_Loader_IN1_X11D";
            this.pn_Loader_IN1_X11D.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN1_X11D.TabIndex = 58;
            // 
            // lb_x11c_spare
            // 
            this.lb_x11c_spare.AutoSize = true;
            this.lb_x11c_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x11c_spare.ForeColor = System.Drawing.Color.White;
            this.lb_x11c_spare.Location = new System.Drawing.Point(479, 594);
            this.lb_x11c_spare.Name = "lb_x11c_spare";
            this.lb_x11c_spare.Size = new System.Drawing.Size(116, 13);
            this.lb_x11c_spare.TabIndex = 57;
            this.lb_x11c_spare.Text = "[ X11C ] Spare";
            // 
            // pn_Loader_IN1_X11C
            // 
            this.pn_Loader_IN1_X11C.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN1_X11C.Location = new System.Drawing.Point(433, 580);
            this.pn_Loader_IN1_X11C.Name = "pn_Loader_IN1_X11C";
            this.pn_Loader_IN1_X11C.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN1_X11C.TabIndex = 56;
            // 
            // lb_x11b_spare
            // 
            this.lb_x11b_spare.AutoSize = true;
            this.lb_x11b_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x11b_spare.ForeColor = System.Drawing.Color.White;
            this.lb_x11b_spare.Location = new System.Drawing.Point(479, 548);
            this.lb_x11b_spare.Name = "lb_x11b_spare";
            this.lb_x11b_spare.Size = new System.Drawing.Size(116, 13);
            this.lb_x11b_spare.TabIndex = 55;
            this.lb_x11b_spare.Text = "[ X11B ] Spare";
            // 
            // pn_Loader_IN1_X11B
            // 
            this.pn_Loader_IN1_X11B.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN1_X11B.Location = new System.Drawing.Point(433, 534);
            this.pn_Loader_IN1_X11B.Name = "pn_Loader_IN1_X11B";
            this.pn_Loader_IN1_X11B.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN1_X11B.TabIndex = 54;
            // 
            // lb_x11a_spare
            // 
            this.lb_x11a_spare.AutoSize = true;
            this.lb_x11a_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x11a_spare.ForeColor = System.Drawing.Color.White;
            this.lb_x11a_spare.Location = new System.Drawing.Point(479, 501);
            this.lb_x11a_spare.Name = "lb_x11a_spare";
            this.lb_x11a_spare.Size = new System.Drawing.Size(115, 13);
            this.lb_x11a_spare.TabIndex = 53;
            this.lb_x11a_spare.Text = "[ X11A ] Spare";
            // 
            // pn_Loader_IN1_X11A
            // 
            this.pn_Loader_IN1_X11A.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN1_X11A.Location = new System.Drawing.Point(433, 488);
            this.pn_Loader_IN1_X11A.Name = "pn_Loader_IN1_X11A";
            this.pn_Loader_IN1_X11A.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN1_X11A.TabIndex = 52;
            // 
            // lb_x119_7_safety_door_sw_open
            // 
            this.lb_x119_7_safety_door_sw_open.AutoSize = true;
            this.lb_x119_7_safety_door_sw_open.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x119_7_safety_door_sw_open.ForeColor = System.Drawing.Color.White;
            this.lb_x119_7_safety_door_sw_open.Location = new System.Drawing.Point(479, 454);
            this.lb_x119_7_safety_door_sw_open.Name = "lb_x119_7_safety_door_sw_open";
            this.lb_x119_7_safety_door_sw_open.Size = new System.Drawing.Size(238, 13);
            this.lb_x119_7_safety_door_sw_open.TabIndex = 51;
            this.lb_x119_7_safety_door_sw_open.Text = "[ X119 ] 7 Safety Door SW Open";
            // 
            // pn_Loader_IN1_X119
            // 
            this.pn_Loader_IN1_X119.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN1_X119.Location = new System.Drawing.Point(433, 442);
            this.pn_Loader_IN1_X119.Name = "pn_Loader_IN1_X119";
            this.pn_Loader_IN1_X119.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN1_X119.TabIndex = 50;
            // 
            // lb_x118_6_safety_door_sw_open
            // 
            this.lb_x118_6_safety_door_sw_open.AutoSize = true;
            this.lb_x118_6_safety_door_sw_open.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x118_6_safety_door_sw_open.ForeColor = System.Drawing.Color.White;
            this.lb_x118_6_safety_door_sw_open.Location = new System.Drawing.Point(479, 409);
            this.lb_x118_6_safety_door_sw_open.Name = "lb_x118_6_safety_door_sw_open";
            this.lb_x118_6_safety_door_sw_open.Size = new System.Drawing.Size(238, 13);
            this.lb_x118_6_safety_door_sw_open.TabIndex = 49;
            this.lb_x118_6_safety_door_sw_open.Text = "[ X118 ] 6 Safety Door SW Open";
            // 
            // pn_Loader_IN1_X118
            // 
            this.pn_Loader_IN1_X118.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN1_X118.Location = new System.Drawing.Point(433, 396);
            this.pn_Loader_IN1_X118.Name = "pn_Loader_IN1_X118";
            this.pn_Loader_IN1_X118.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN1_X118.TabIndex = 48;
            // 
            // lb_x117_5_safety_door_sw_open
            // 
            this.lb_x117_5_safety_door_sw_open.AutoSize = true;
            this.lb_x117_5_safety_door_sw_open.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x117_5_safety_door_sw_open.ForeColor = System.Drawing.Color.White;
            this.lb_x117_5_safety_door_sw_open.Location = new System.Drawing.Point(479, 364);
            this.lb_x117_5_safety_door_sw_open.Name = "lb_x117_5_safety_door_sw_open";
            this.lb_x117_5_safety_door_sw_open.Size = new System.Drawing.Size(238, 13);
            this.lb_x117_5_safety_door_sw_open.TabIndex = 47;
            this.lb_x117_5_safety_door_sw_open.Text = "[ X117 ] 5 Safety Door SW Open";
            // 
            // pn_Loader_IN1_X117
            // 
            this.pn_Loader_IN1_X117.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN1_X117.Location = new System.Drawing.Point(433, 350);
            this.pn_Loader_IN1_X117.Name = "pn_Loader_IN1_X117";
            this.pn_Loader_IN1_X117.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN1_X117.TabIndex = 46;
            // 
            // lb_x116_4_safety_door_sw_open
            // 
            this.lb_x116_4_safety_door_sw_open.AutoSize = true;
            this.lb_x116_4_safety_door_sw_open.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x116_4_safety_door_sw_open.ForeColor = System.Drawing.Color.White;
            this.lb_x116_4_safety_door_sw_open.Location = new System.Drawing.Point(479, 319);
            this.lb_x116_4_safety_door_sw_open.Name = "lb_x116_4_safety_door_sw_open";
            this.lb_x116_4_safety_door_sw_open.Size = new System.Drawing.Size(238, 13);
            this.lb_x116_4_safety_door_sw_open.TabIndex = 45;
            this.lb_x116_4_safety_door_sw_open.Text = "[ X116 ] 4 Safety Door SW Open";
            // 
            // pn_Loader_IN1_X116
            // 
            this.pn_Loader_IN1_X116.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN1_X116.Location = new System.Drawing.Point(433, 304);
            this.pn_Loader_IN1_X116.Name = "pn_Loader_IN1_X116";
            this.pn_Loader_IN1_X116.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN1_X116.TabIndex = 44;
            // 
            // lb_x115_3_safety_door_sw_open
            // 
            this.lb_x115_3_safety_door_sw_open.AutoSize = true;
            this.lb_x115_3_safety_door_sw_open.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x115_3_safety_door_sw_open.ForeColor = System.Drawing.Color.White;
            this.lb_x115_3_safety_door_sw_open.Location = new System.Drawing.Point(479, 271);
            this.lb_x115_3_safety_door_sw_open.Name = "lb_x115_3_safety_door_sw_open";
            this.lb_x115_3_safety_door_sw_open.Size = new System.Drawing.Size(238, 13);
            this.lb_x115_3_safety_door_sw_open.TabIndex = 43;
            this.lb_x115_3_safety_door_sw_open.Text = "[ X115 ] 3 Safety Door SW Open";
            // 
            // pn_Loader_IN1_X115
            // 
            this.pn_Loader_IN1_X115.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN1_X115.Location = new System.Drawing.Point(433, 258);
            this.pn_Loader_IN1_X115.Name = "pn_Loader_IN1_X115";
            this.pn_Loader_IN1_X115.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN1_X115.TabIndex = 42;
            // 
            // lb_x114_2_safety_door_sw_open
            // 
            this.lb_x114_2_safety_door_sw_open.AutoSize = true;
            this.lb_x114_2_safety_door_sw_open.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x114_2_safety_door_sw_open.ForeColor = System.Drawing.Color.White;
            this.lb_x114_2_safety_door_sw_open.Location = new System.Drawing.Point(479, 226);
            this.lb_x114_2_safety_door_sw_open.Name = "lb_x114_2_safety_door_sw_open";
            this.lb_x114_2_safety_door_sw_open.Size = new System.Drawing.Size(238, 13);
            this.lb_x114_2_safety_door_sw_open.TabIndex = 41;
            this.lb_x114_2_safety_door_sw_open.Text = "[ X114 ] 2 Safety Door SW Open";
            // 
            // pn_Loader_IN1_X114
            // 
            this.pn_Loader_IN1_X114.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN1_X114.Location = new System.Drawing.Point(433, 212);
            this.pn_Loader_IN1_X114.Name = "pn_Loader_IN1_X114";
            this.pn_Loader_IN1_X114.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN1_X114.TabIndex = 40;
            // 
            // lb_x113_1_safety_door_sw_open
            // 
            this.lb_x113_1_safety_door_sw_open.AutoSize = true;
            this.lb_x113_1_safety_door_sw_open.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x113_1_safety_door_sw_open.ForeColor = System.Drawing.Color.White;
            this.lb_x113_1_safety_door_sw_open.Location = new System.Drawing.Point(479, 179);
            this.lb_x113_1_safety_door_sw_open.Name = "lb_x113_1_safety_door_sw_open";
            this.lb_x113_1_safety_door_sw_open.Size = new System.Drawing.Size(238, 13);
            this.lb_x113_1_safety_door_sw_open.TabIndex = 39;
            this.lb_x113_1_safety_door_sw_open.Text = "[ X113 ] 1 Safety Door SW Open";
            // 
            // pn_Loader_IN1_X113
            // 
            this.pn_Loader_IN1_X113.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN1_X113.Location = new System.Drawing.Point(433, 166);
            this.pn_Loader_IN1_X113.Name = "pn_Loader_IN1_X113";
            this.pn_Loader_IN1_X113.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN1_X113.TabIndex = 38;
            // 
            // lb_x112_3_safety_enable_grip_sw_on
            // 
            this.lb_x112_3_safety_enable_grip_sw_on.AutoSize = true;
            this.lb_x112_3_safety_enable_grip_sw_on.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x112_3_safety_enable_grip_sw_on.ForeColor = System.Drawing.Color.White;
            this.lb_x112_3_safety_enable_grip_sw_on.Location = new System.Drawing.Point(479, 133);
            this.lb_x112_3_safety_enable_grip_sw_on.Name = "lb_x112_3_safety_enable_grip_sw_on";
            this.lb_x112_3_safety_enable_grip_sw_on.Size = new System.Drawing.Size(270, 13);
            this.lb_x112_3_safety_enable_grip_sw_on.TabIndex = 37;
            this.lb_x112_3_safety_enable_grip_sw_on.Text = "[ X112 ] 3 Safety Enable Grip SW On";
            // 
            // pn_Loader_IN1_X112
            // 
            this.pn_Loader_IN1_X112.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN1_X112.Location = new System.Drawing.Point(433, 120);
            this.pn_Loader_IN1_X112.Name = "pn_Loader_IN1_X112";
            this.pn_Loader_IN1_X112.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN1_X112.TabIndex = 36;
            // 
            // lb_x111_2_safety_enable_grip_sw_on
            // 
            this.lb_x111_2_safety_enable_grip_sw_on.AutoSize = true;
            this.lb_x111_2_safety_enable_grip_sw_on.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x111_2_safety_enable_grip_sw_on.ForeColor = System.Drawing.Color.White;
            this.lb_x111_2_safety_enable_grip_sw_on.Location = new System.Drawing.Point(479, 86);
            this.lb_x111_2_safety_enable_grip_sw_on.Name = "lb_x111_2_safety_enable_grip_sw_on";
            this.lb_x111_2_safety_enable_grip_sw_on.Size = new System.Drawing.Size(270, 13);
            this.lb_x111_2_safety_enable_grip_sw_on.TabIndex = 35;
            this.lb_x111_2_safety_enable_grip_sw_on.Text = "[ X111 ] 2 Safety Enable Grip SW On";
            // 
            // pn_Loader_IN1_X111
            // 
            this.pn_Loader_IN1_X111.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN1_X111.Location = new System.Drawing.Point(433, 74);
            this.pn_Loader_IN1_X111.Name = "pn_Loader_IN1_X111";
            this.pn_Loader_IN1_X111.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN1_X111.TabIndex = 34;
            // 
            // lb_x110_1_safety_enable_grip_sw_on
            // 
            this.lb_x110_1_safety_enable_grip_sw_on.AutoSize = true;
            this.lb_x110_1_safety_enable_grip_sw_on.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x110_1_safety_enable_grip_sw_on.ForeColor = System.Drawing.Color.White;
            this.lb_x110_1_safety_enable_grip_sw_on.Location = new System.Drawing.Point(479, 42);
            this.lb_x110_1_safety_enable_grip_sw_on.Name = "lb_x110_1_safety_enable_grip_sw_on";
            this.lb_x110_1_safety_enable_grip_sw_on.Size = new System.Drawing.Size(270, 13);
            this.lb_x110_1_safety_enable_grip_sw_on.TabIndex = 33;
            this.lb_x110_1_safety_enable_grip_sw_on.Text = "[ X110 ] 1 Safety Enable Grip SW On";
            // 
            // pn_Loader_IN1_X110
            // 
            this.pn_Loader_IN1_X110.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN1_X110.Location = new System.Drawing.Point(433, 28);
            this.pn_Loader_IN1_X110.Name = "pn_Loader_IN1_X110";
            this.pn_Loader_IN1_X110.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN1_X110.TabIndex = 32;
            // 
            // lb_x10f_3_emergency_stop_enable_grip_sw
            // 
            this.lb_x10f_3_emergency_stop_enable_grip_sw.AutoSize = true;
            this.lb_x10f_3_emergency_stop_enable_grip_sw.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x10f_3_emergency_stop_enable_grip_sw.ForeColor = System.Drawing.Color.White;
            this.lb_x10f_3_emergency_stop_enable_grip_sw.Location = new System.Drawing.Point(52, 730);
            this.lb_x10f_3_emergency_stop_enable_grip_sw.Name = "lb_x10f_3_emergency_stop_enable_grip_sw";
            this.lb_x10f_3_emergency_stop_enable_grip_sw.Size = new System.Drawing.Size(318, 13);
            this.lb_x10f_3_emergency_stop_enable_grip_sw.TabIndex = 31;
            this.lb_x10f_3_emergency_stop_enable_grip_sw.Text = "[ X10F ] 3 Emergency Stop Enable Grip SW";
            // 
            // pn_Loader_IN1_X10F
            // 
            this.pn_Loader_IN1_X10F.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN1_X10F.Location = new System.Drawing.Point(6, 718);
            this.pn_Loader_IN1_X10F.Name = "pn_Loader_IN1_X10F";
            this.pn_Loader_IN1_X10F.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN1_X10F.TabIndex = 30;
            // 
            // lb_x10e_2_emergency_stop_enable_grip_sw
            // 
            this.lb_x10e_2_emergency_stop_enable_grip_sw.AutoSize = true;
            this.lb_x10e_2_emergency_stop_enable_grip_sw.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x10e_2_emergency_stop_enable_grip_sw.ForeColor = System.Drawing.Color.White;
            this.lb_x10e_2_emergency_stop_enable_grip_sw.Location = new System.Drawing.Point(52, 685);
            this.lb_x10e_2_emergency_stop_enable_grip_sw.Name = "lb_x10e_2_emergency_stop_enable_grip_sw";
            this.lb_x10e_2_emergency_stop_enable_grip_sw.Size = new System.Drawing.Size(319, 13);
            this.lb_x10e_2_emergency_stop_enable_grip_sw.TabIndex = 29;
            this.lb_x10e_2_emergency_stop_enable_grip_sw.Text = "[ X10E ] 2 Emergency Stop Enable Grip SW";
            // 
            // pn_Loader_IN1_X10E
            // 
            this.pn_Loader_IN1_X10E.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN1_X10E.Location = new System.Drawing.Point(6, 672);
            this.pn_Loader_IN1_X10E.Name = "pn_Loader_IN1_X10E";
            this.pn_Loader_IN1_X10E.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN1_X10E.TabIndex = 28;
            // 
            // lb_x10d_1_emergency_stop_enable_grip_sw
            // 
            this.lb_x10d_1_emergency_stop_enable_grip_sw.AutoSize = true;
            this.lb_x10d_1_emergency_stop_enable_grip_sw.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x10d_1_emergency_stop_enable_grip_sw.ForeColor = System.Drawing.Color.White;
            this.lb_x10d_1_emergency_stop_enable_grip_sw.Location = new System.Drawing.Point(52, 640);
            this.lb_x10d_1_emergency_stop_enable_grip_sw.Name = "lb_x10d_1_emergency_stop_enable_grip_sw";
            this.lb_x10d_1_emergency_stop_enable_grip_sw.Size = new System.Drawing.Size(320, 13);
            this.lb_x10d_1_emergency_stop_enable_grip_sw.TabIndex = 27;
            this.lb_x10d_1_emergency_stop_enable_grip_sw.Text = "[ X10D ] 1 Emergency Stop Enable Grip SW";
            // 
            // pn_Loader_IN1_X10D
            // 
            this.pn_Loader_IN1_X10D.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN1_X10D.Location = new System.Drawing.Point(6, 626);
            this.pn_Loader_IN1_X10D.Name = "pn_Loader_IN1_X10D";
            this.pn_Loader_IN1_X10D.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN1_X10D.TabIndex = 26;
            // 
            // lb_x10c_3_emergency_stop
            // 
            this.lb_x10c_3_emergency_stop.AutoSize = true;
            this.lb_x10c_3_emergency_stop.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x10c_3_emergency_stop.ForeColor = System.Drawing.Color.White;
            this.lb_x10c_3_emergency_stop.Location = new System.Drawing.Point(52, 594);
            this.lb_x10c_3_emergency_stop.Name = "lb_x10c_3_emergency_stop";
            this.lb_x10c_3_emergency_stop.Size = new System.Drawing.Size(203, 13);
            this.lb_x10c_3_emergency_stop.TabIndex = 25;
            this.lb_x10c_3_emergency_stop.Text = "[ X10C ] 3 Emergency Stop";
            // 
            // pn_Loader_IN1_X10C
            // 
            this.pn_Loader_IN1_X10C.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN1_X10C.Location = new System.Drawing.Point(6, 580);
            this.pn_Loader_IN1_X10C.Name = "pn_Loader_IN1_X10C";
            this.pn_Loader_IN1_X10C.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN1_X10C.TabIndex = 24;
            // 
            // lb_x10b_2_emergency_stop
            // 
            this.lb_x10b_2_emergency_stop.AutoSize = true;
            this.lb_x10b_2_emergency_stop.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x10b_2_emergency_stop.ForeColor = System.Drawing.Color.White;
            this.lb_x10b_2_emergency_stop.Location = new System.Drawing.Point(52, 548);
            this.lb_x10b_2_emergency_stop.Name = "lb_x10b_2_emergency_stop";
            this.lb_x10b_2_emergency_stop.Size = new System.Drawing.Size(203, 13);
            this.lb_x10b_2_emergency_stop.TabIndex = 23;
            this.lb_x10b_2_emergency_stop.Text = "[ X10B ] 2 Emergency Stop";
            // 
            // pn_Loader_IN1_X10B
            // 
            this.pn_Loader_IN1_X10B.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN1_X10B.Location = new System.Drawing.Point(6, 534);
            this.pn_Loader_IN1_X10B.Name = "pn_Loader_IN1_X10B";
            this.pn_Loader_IN1_X10B.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN1_X10B.TabIndex = 22;
            // 
            // lb_x10a_1_emergency_stop
            // 
            this.lb_x10a_1_emergency_stop.AutoSize = true;
            this.lb_x10a_1_emergency_stop.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x10a_1_emergency_stop.ForeColor = System.Drawing.Color.White;
            this.lb_x10a_1_emergency_stop.Location = new System.Drawing.Point(52, 501);
            this.lb_x10a_1_emergency_stop.Name = "lb_x10a_1_emergency_stop";
            this.lb_x10a_1_emergency_stop.Size = new System.Drawing.Size(202, 13);
            this.lb_x10a_1_emergency_stop.TabIndex = 21;
            this.lb_x10a_1_emergency_stop.Text = "[ X10A ] 1 Emergency Stop";
            // 
            // pn_Loader_IN1_X10A
            // 
            this.pn_Loader_IN1_X10A.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN1_X10A.Location = new System.Drawing.Point(6, 488);
            this.pn_Loader_IN1_X10A.Name = "pn_Loader_IN1_X10A";
            this.pn_Loader_IN1_X10A.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN1_X10A.TabIndex = 20;
            // 
            // lb_x109_ld_lifter_muting_on
            // 
            this.lb_x109_ld_lifter_muting_on.AutoSize = true;
            this.lb_x109_ld_lifter_muting_on.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x109_ld_lifter_muting_on.ForeColor = System.Drawing.Color.White;
            this.lb_x109_ld_lifter_muting_on.Location = new System.Drawing.Point(52, 454);
            this.lb_x109_ld_lifter_muting_on.Name = "lb_x109_ld_lifter_muting_on";
            this.lb_x109_ld_lifter_muting_on.Size = new System.Drawing.Size(242, 13);
            this.lb_x109_ld_lifter_muting_on.TabIndex = 19;
            this.lb_x109_ld_lifter_muting_on.Text = "[ X109 ] 2-2 L/D Lifter Muting On";
            // 
            // pn_Loader_IN1_X109
            // 
            this.pn_Loader_IN1_X109.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN1_X109.Location = new System.Drawing.Point(6, 442);
            this.pn_Loader_IN1_X109.Name = "pn_Loader_IN1_X109";
            this.pn_Loader_IN1_X109.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN1_X109.TabIndex = 18;
            // 
            // lb_x108_ld_lifter_muting_on
            // 
            this.lb_x108_ld_lifter_muting_on.AutoSize = true;
            this.lb_x108_ld_lifter_muting_on.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x108_ld_lifter_muting_on.ForeColor = System.Drawing.Color.White;
            this.lb_x108_ld_lifter_muting_on.Location = new System.Drawing.Point(52, 409);
            this.lb_x108_ld_lifter_muting_on.Name = "lb_x108_ld_lifter_muting_on";
            this.lb_x108_ld_lifter_muting_on.Size = new System.Drawing.Size(242, 13);
            this.lb_x108_ld_lifter_muting_on.TabIndex = 17;
            this.lb_x108_ld_lifter_muting_on.Text = "[ X108 ] 2-1 L/D Lifter Muting On";
            // 
            // pn_Loader_IN1_X108
            // 
            this.pn_Loader_IN1_X108.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN1_X108.Location = new System.Drawing.Point(6, 396);
            this.pn_Loader_IN1_X108.Name = "pn_Loader_IN1_X108";
            this.pn_Loader_IN1_X108.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN1_X108.TabIndex = 16;
            // 
            // lb_x107_ld_lifter_muting_on
            // 
            this.lb_x107_ld_lifter_muting_on.AutoSize = true;
            this.lb_x107_ld_lifter_muting_on.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x107_ld_lifter_muting_on.ForeColor = System.Drawing.Color.White;
            this.lb_x107_ld_lifter_muting_on.Location = new System.Drawing.Point(52, 364);
            this.lb_x107_ld_lifter_muting_on.Name = "lb_x107_ld_lifter_muting_on";
            this.lb_x107_ld_lifter_muting_on.Size = new System.Drawing.Size(242, 13);
            this.lb_x107_ld_lifter_muting_on.TabIndex = 15;
            this.lb_x107_ld_lifter_muting_on.Text = "[ X107 ] 1-2 L/D Lifter Muting On";
            // 
            // pn_Loader_IN1_X107
            // 
            this.pn_Loader_IN1_X107.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN1_X107.Location = new System.Drawing.Point(6, 350);
            this.pn_Loader_IN1_X107.Name = "pn_Loader_IN1_X107";
            this.pn_Loader_IN1_X107.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN1_X107.TabIndex = 14;
            // 
            // lb_x106_ld_lifter_muting_on
            // 
            this.lb_x106_ld_lifter_muting_on.AutoSize = true;
            this.lb_x106_ld_lifter_muting_on.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x106_ld_lifter_muting_on.ForeColor = System.Drawing.Color.White;
            this.lb_x106_ld_lifter_muting_on.Location = new System.Drawing.Point(52, 319);
            this.lb_x106_ld_lifter_muting_on.Name = "lb_x106_ld_lifter_muting_on";
            this.lb_x106_ld_lifter_muting_on.Size = new System.Drawing.Size(242, 13);
            this.lb_x106_ld_lifter_muting_on.TabIndex = 13;
            this.lb_x106_ld_lifter_muting_on.Text = "[ X106 ] 1-1 L/D Lifter Muting On";
            // 
            // pn_Loader_IN1_X106
            // 
            this.pn_Loader_IN1_X106.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN1_X106.Location = new System.Drawing.Point(6, 304);
            this.pn_Loader_IN1_X106.Name = "pn_Loader_IN1_X106";
            this.pn_Loader_IN1_X106.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN1_X106.TabIndex = 12;
            // 
            // lb_x105_ld_out_muting_on
            // 
            this.lb_x105_ld_out_muting_on.AutoSize = true;
            this.lb_x105_ld_out_muting_on.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x105_ld_out_muting_on.ForeColor = System.Drawing.Color.White;
            this.lb_x105_ld_out_muting_on.Location = new System.Drawing.Point(52, 271);
            this.lb_x105_ld_out_muting_on.Name = "lb_x105_ld_out_muting_on";
            this.lb_x105_ld_out_muting_on.Size = new System.Drawing.Size(206, 13);
            this.lb_x105_ld_out_muting_on.TabIndex = 11;
            this.lb_x105_ld_out_muting_on.Text = "[ X105 ] L/D 배출 Muting On";
            // 
            // pn_Loader_IN1_X105
            // 
            this.pn_Loader_IN1_X105.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN1_X105.Location = new System.Drawing.Point(6, 258);
            this.pn_Loader_IN1_X105.Name = "pn_Loader_IN1_X105";
            this.pn_Loader_IN1_X105.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN1_X105.TabIndex = 10;
            // 
            // lb_x104_ld_in_muting_on
            // 
            this.lb_x104_ld_in_muting_on.AutoSize = true;
            this.lb_x104_ld_in_muting_on.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x104_ld_in_muting_on.ForeColor = System.Drawing.Color.White;
            this.lb_x104_ld_in_muting_on.Location = new System.Drawing.Point(52, 226);
            this.lb_x104_ld_in_muting_on.Name = "lb_x104_ld_in_muting_on";
            this.lb_x104_ld_in_muting_on.Size = new System.Drawing.Size(206, 13);
            this.lb_x104_ld_in_muting_on.TabIndex = 9;
            this.lb_x104_ld_in_muting_on.Text = "[ X104 ] L/D 투입 Muting On";
            // 
            // pn_Loader_IN1_X104
            // 
            this.pn_Loader_IN1_X104.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN1_X104.Location = new System.Drawing.Point(6, 212);
            this.pn_Loader_IN1_X104.Name = "pn_Loader_IN1_X104";
            this.pn_Loader_IN1_X104.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN1_X104.TabIndex = 8;
            // 
            // lb_x103_control_reset_switch
            // 
            this.lb_x103_control_reset_switch.AutoSize = true;
            this.lb_x103_control_reset_switch.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x103_control_reset_switch.ForeColor = System.Drawing.Color.White;
            this.lb_x103_control_reset_switch.Location = new System.Drawing.Point(52, 179);
            this.lb_x103_control_reset_switch.Name = "lb_x103_control_reset_switch";
            this.lb_x103_control_reset_switch.Size = new System.Drawing.Size(276, 13);
            this.lb_x103_control_reset_switch.TabIndex = 7;
            this.lb_x103_control_reset_switch.Text = "[ X103 ] 2 - 조작부 Reset Switch User";
            // 
            // pn_Loader_IN1_X103
            // 
            this.pn_Loader_IN1_X103.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN1_X103.Location = new System.Drawing.Point(6, 166);
            this.pn_Loader_IN1_X103.Name = "pn_Loader_IN1_X103";
            this.pn_Loader_IN1_X103.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN1_X103.TabIndex = 6;
            // 
            // lb_x102_ld_reset_switch
            // 
            this.lb_x102_ld_reset_switch.AutoSize = true;
            this.lb_x102_ld_reset_switch.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x102_ld_reset_switch.ForeColor = System.Drawing.Color.White;
            this.lb_x102_ld_reset_switch.Location = new System.Drawing.Point(52, 133);
            this.lb_x102_ld_reset_switch.Name = "lb_x102_ld_reset_switch";
            this.lb_x102_ld_reset_switch.Size = new System.Drawing.Size(273, 13);
            this.lb_x102_ld_reset_switch.TabIndex = 5;
            this.lb_x102_ld_reset_switch.Text = "[ X102 ] 2 - L/D부 Reset Switch User";
            // 
            // pn_Loader_IN1_X102
            // 
            this.pn_Loader_IN1_X102.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN1_X102.Location = new System.Drawing.Point(6, 120);
            this.pn_Loader_IN1_X102.Name = "pn_Loader_IN1_X102";
            this.pn_Loader_IN1_X102.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN1_X102.TabIndex = 4;
            // 
            // lb_x101_ld_out_muting_switch_on_off
            // 
            this.lb_x101_ld_out_muting_switch_on_off.AutoSize = true;
            this.lb_x101_ld_out_muting_switch_on_off.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x101_ld_out_muting_switch_on_off.ForeColor = System.Drawing.Color.White;
            this.lb_x101_ld_out_muting_switch_on_off.Location = new System.Drawing.Point(52, 86);
            this.lb_x101_ld_out_muting_switch_on_off.Name = "lb_x101_ld_out_muting_switch_on_off";
            this.lb_x101_ld_out_muting_switch_on_off.Size = new System.Drawing.Size(349, 13);
            this.lb_x101_ld_out_muting_switch_on_off.TabIndex = 3;
            this.lb_x101_ld_out_muting_switch_on_off.Text = "[ X101 ] 1 - L/D 배출 Muting Switch On/Off User";
            // 
            // pn_Loader_IN1_X101
            // 
            this.pn_Loader_IN1_X101.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN1_X101.Location = new System.Drawing.Point(6, 74);
            this.pn_Loader_IN1_X101.Name = "pn_Loader_IN1_X101";
            this.pn_Loader_IN1_X101.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN1_X101.TabIndex = 2;
            // 
            // lb_ld_in_muting_switch_on_off
            // 
            this.lb_ld_in_muting_switch_on_off.AutoSize = true;
            this.lb_ld_in_muting_switch_on_off.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld_in_muting_switch_on_off.ForeColor = System.Drawing.Color.White;
            this.lb_ld_in_muting_switch_on_off.Location = new System.Drawing.Point(52, 42);
            this.lb_ld_in_muting_switch_on_off.Name = "lb_ld_in_muting_switch_on_off";
            this.lb_ld_in_muting_switch_on_off.Size = new System.Drawing.Size(349, 13);
            this.lb_ld_in_muting_switch_on_off.TabIndex = 1;
            this.lb_ld_in_muting_switch_on_off.Text = "[ X100 ] 1 - L/D 투입 Muting Switch On/Off User";
            // 
            // pn_Loader_IN1_X100
            // 
            this.pn_Loader_IN1_X100.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN1_X100.Location = new System.Drawing.Point(6, 28);
            this.pn_Loader_IN1_X100.Name = "pn_Loader_IN1_X100";
            this.pn_Loader_IN1_X100.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN1_X100.TabIndex = 0;
            // 
            // tp_iostatus_ld_in2
            // 
            this.tp_iostatus_ld_in2.BackColor = System.Drawing.Color.DimGray;
            this.tp_iostatus_ld_in2.Controls.Add(this.lb_x13f_laser_exhaust_fan_run);
            this.tp_iostatus_ld_in2.Controls.Add(this.pn_Loader_IN2_X13F);
            this.tp_iostatus_ld_in2.Controls.Add(this.lb_x13e_laser_inspiration_fan_run);
            this.tp_iostatus_ld_in2.Controls.Add(this.pn_Loader_IN2_X13E);
            this.tp_iostatus_ld_in2.Controls.Add(this.label_x13d_laser_panel_critical_alarm);
            this.tp_iostatus_ld_in2.Controls.Add(this.pn_Loader_IN2_X13D);
            this.tp_iostatus_ld_in2.Controls.Add(this.lb_x13c_ld_panel_critical_alarm);
            this.tp_iostatus_ld_in2.Controls.Add(this.pn_Loader_IN2_X13C);
            this.tp_iostatus_ld_in2.Controls.Add(this.lb_x13b_2_ld_cst_d_ungrip_pos);
            this.tp_iostatus_ld_in2.Controls.Add(this.pn_Loader_IN2_X13B);
            this.tp_iostatus_ld_in2.Controls.Add(this.lb_x13a_2_ld_cst_d_grip_pos);
            this.tp_iostatus_ld_in2.Controls.Add(this.pn_Loader_IN2_X13A);
            this.tp_iostatus_ld_in2.Controls.Add(this.lb_x139_2_2_ld_cst_lr_ungrip_pos);
            this.tp_iostatus_ld_in2.Controls.Add(this.pn_Loader_IN2_X139);
            this.tp_iostatus_ld_in2.Controls.Add(this.lb_x138_2_2_ld_cst_lr_grip_pos);
            this.tp_iostatus_ld_in2.Controls.Add(this.pn_Loader_IN2_X138);
            this.tp_iostatus_ld_in2.Controls.Add(this.lb_x137_2_1_ld_cst_lr_ungrip_pos);
            this.tp_iostatus_ld_in2.Controls.Add(this.pn_Loader_IN2_X137);
            this.tp_iostatus_ld_in2.Controls.Add(this.lb_x136_2_1_ld_cst_lr_grip_pos);
            this.tp_iostatus_ld_in2.Controls.Add(this.pn_Loader_IN2_X136);
            this.tp_iostatus_ld_in2.Controls.Add(this.lb_x135_1_ld_cst_d_ungrip_pos);
            this.tp_iostatus_ld_in2.Controls.Add(this.pn_Loader_IN2_X135);
            this.tp_iostatus_ld_in2.Controls.Add(this.lb_x134_1_ld_cst_d_grip_pos);
            this.tp_iostatus_ld_in2.Controls.Add(this.pn_Loader_IN2_X134);
            this.tp_iostatus_ld_in2.Controls.Add(this.lb_x133_1_2_ld_cst_lr_ungrip_pos);
            this.tp_iostatus_ld_in2.Controls.Add(this.pn_Loader_IN2_X133);
            this.tp_iostatus_ld_in2.Controls.Add(this.lb_x132_1_2_ld_cst_lr_grip_pos);
            this.tp_iostatus_ld_in2.Controls.Add(this.pn_Loader_IN2_X132);
            this.tp_iostatus_ld_in2.Controls.Add(this.lb_x131_1_1_ld_cst_lr_ungrip_pos);
            this.tp_iostatus_ld_in2.Controls.Add(this.pn_Loader_IN2_X131);
            this.tp_iostatus_ld_in2.Controls.Add(this.lb_x130_1_1_ld_cst_lr_grip_pos);
            this.tp_iostatus_ld_in2.Controls.Add(this.pn_Loader_IN2_X130);
            this.tp_iostatus_ld_in2.Controls.Add(this.lb_x12f_spare);
            this.tp_iostatus_ld_in2.Controls.Add(this.pn_Loader_IN2_X12F);
            this.tp_iostatus_ld_in2.Controls.Add(this.lb_x12e_2_3_ld_cst_dettect);
            this.tp_iostatus_ld_in2.Controls.Add(this.pn_Loader_IN2_X1E);
            this.tp_iostatus_ld_in2.Controls.Add(this.lb_x12d_2_2_ld_cst_dettect);
            this.tp_iostatus_ld_in2.Controls.Add(this.pn_Loader_IN2_X12D);
            this.tp_iostatus_ld_in2.Controls.Add(this.lb_x12c_2_1_ld_cst_dettect);
            this.tp_iostatus_ld_in2.Controls.Add(this.pn_Loader_IN2_X12C);
            this.tp_iostatus_ld_in2.Controls.Add(this.lb_x12b_1_3_ld_cst_dettect);
            this.tp_iostatus_ld_in2.Controls.Add(this.pn_Loader_IN2_X12B);
            this.tp_iostatus_ld_in2.Controls.Add(this.lb_x12a_1_2_ld_cst_dettect);
            this.tp_iostatus_ld_in2.Controls.Add(this.pn_Loader_IN2_X12A);
            this.tp_iostatus_ld_in2.Controls.Add(this.lb_x129_1_1_ld_cst_dettect);
            this.tp_iostatus_ld_in2.Controls.Add(this.pn_Loader_IN2_X129);
            this.tp_iostatus_ld_in2.Controls.Add(this.lb_x128_control_select_ld);
            this.tp_iostatus_ld_in2.Controls.Add(this.pn_Loader_IN2_X128);
            this.tp_iostatus_ld_in2.Controls.Add(this.lb_x127_spare);
            this.tp_iostatus_ld_in2.Controls.Add(this.pn_Loader_IN2_X127);
            this.tp_iostatus_ld_in2.Controls.Add(this.lb_x126_spare);
            this.tp_iostatus_ld_in2.Controls.Add(this.pn_Loader_IN2_X126);
            this.tp_iostatus_ld_in2.Controls.Add(this.lb_x125_spare);
            this.tp_iostatus_ld_in2.Controls.Add(this.pn_Loader_IN2_X125);
            this.tp_iostatus_ld_in2.Controls.Add(this.lb_x124_efu_alarm);
            this.tp_iostatus_ld_in2.Controls.Add(this.pn_Loader_IN2_X124);
            this.tp_iostatus_ld_in2.Controls.Add(this.lb_x123_laser_mc_on);
            this.tp_iostatus_ld_in2.Controls.Add(this.pn_Loader_IN2_X123);
            this.tp_iostatus_ld_in2.Controls.Add(this.lb_x122_ld_mc_on);
            this.tp_iostatus_ld_in2.Controls.Add(this.pn_Loader_IN2_X122);
            this.tp_iostatus_ld_in2.Controls.Add(this.lb_x121_mode_select_sw_teach);
            this.tp_iostatus_ld_in2.Controls.Add(this.pn_Loader_IN2_X121);
            this.tp_iostatus_ld_in2.Controls.Add(this.lb_x120_mode_select_sw_auto);
            this.tp_iostatus_ld_in2.Controls.Add(this.pn_Loader_IN2_X120);
            this.tp_iostatus_ld_in2.Location = new System.Drawing.Point(4, 34);
            this.tp_iostatus_ld_in2.Name = "tp_iostatus_ld_in2";
            this.tp_iostatus_ld_in2.Padding = new System.Windows.Forms.Padding(3);
            this.tp_iostatus_ld_in2.Size = new System.Drawing.Size(846, 778);
            this.tp_iostatus_ld_in2.TabIndex = 1;
            this.tp_iostatus_ld_in2.Text = "IN - 2";
            // 
            // lb_x13f_laser_exhaust_fan_run
            // 
            this.lb_x13f_laser_exhaust_fan_run.AutoSize = true;
            this.lb_x13f_laser_exhaust_fan_run.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x13f_laser_exhaust_fan_run.ForeColor = System.Drawing.Color.White;
            this.lb_x13f_laser_exhaust_fan_run.Location = new System.Drawing.Point(479, 730);
            this.lb_x13f_laser_exhaust_fan_run.Name = "lb_x13f_laser_exhaust_fan_run";
            this.lb_x13f_laser_exhaust_fan_run.Size = new System.Drawing.Size(211, 13);
            this.lb_x13f_laser_exhaust_fan_run.TabIndex = 127;
            this.lb_x13f_laser_exhaust_fan_run.Text = "[ X13F ] 가공부 배기 FAN 구동";
            // 
            // pn_Loader_IN2_X13F
            // 
            this.pn_Loader_IN2_X13F.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN2_X13F.Location = new System.Drawing.Point(433, 718);
            this.pn_Loader_IN2_X13F.Name = "pn_Loader_IN2_X13F";
            this.pn_Loader_IN2_X13F.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN2_X13F.TabIndex = 126;
            // 
            // lb_x13e_laser_inspiration_fan_run
            // 
            this.lb_x13e_laser_inspiration_fan_run.AutoSize = true;
            this.lb_x13e_laser_inspiration_fan_run.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x13e_laser_inspiration_fan_run.ForeColor = System.Drawing.Color.White;
            this.lb_x13e_laser_inspiration_fan_run.Location = new System.Drawing.Point(479, 685);
            this.lb_x13e_laser_inspiration_fan_run.Name = "lb_x13e_laser_inspiration_fan_run";
            this.lb_x13e_laser_inspiration_fan_run.Size = new System.Drawing.Size(212, 13);
            this.lb_x13e_laser_inspiration_fan_run.TabIndex = 125;
            this.lb_x13e_laser_inspiration_fan_run.Text = "[ X13E ] 가공부 흡기 FAN 구동";
            // 
            // pn_Loader_IN2_X13E
            // 
            this.pn_Loader_IN2_X13E.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN2_X13E.Location = new System.Drawing.Point(433, 672);
            this.pn_Loader_IN2_X13E.Name = "pn_Loader_IN2_X13E";
            this.pn_Loader_IN2_X13E.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN2_X13E.TabIndex = 124;
            // 
            // label_x13d_laser_panel_critical_alarm
            // 
            this.label_x13d_laser_panel_critical_alarm.AutoSize = true;
            this.label_x13d_laser_panel_critical_alarm.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label_x13d_laser_panel_critical_alarm.ForeColor = System.Drawing.Color.White;
            this.label_x13d_laser_panel_critical_alarm.Location = new System.Drawing.Point(479, 640);
            this.label_x13d_laser_panel_critical_alarm.Name = "label_x13d_laser_panel_critical_alarm";
            this.label_x13d_laser_panel_critical_alarm.Size = new System.Drawing.Size(214, 13);
            this.label_x13d_laser_panel_critical_alarm.TabIndex = 123;
            this.label_x13d_laser_panel_critical_alarm.Text = "[ X13D ] 가공부 판넬 화재 알람";
            // 
            // pn_Loader_IN2_X13D
            // 
            this.pn_Loader_IN2_X13D.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN2_X13D.Location = new System.Drawing.Point(433, 626);
            this.pn_Loader_IN2_X13D.Name = "pn_Loader_IN2_X13D";
            this.pn_Loader_IN2_X13D.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN2_X13D.TabIndex = 122;
            // 
            // lb_x13c_ld_panel_critical_alarm
            // 
            this.lb_x13c_ld_panel_critical_alarm.AutoSize = true;
            this.lb_x13c_ld_panel_critical_alarm.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x13c_ld_panel_critical_alarm.ForeColor = System.Drawing.Color.White;
            this.lb_x13c_ld_panel_critical_alarm.Location = new System.Drawing.Point(479, 594);
            this.lb_x13c_ld_panel_critical_alarm.Name = "lb_x13c_ld_panel_critical_alarm";
            this.lb_x13c_ld_panel_critical_alarm.Size = new System.Drawing.Size(211, 13);
            this.lb_x13c_ld_panel_critical_alarm.TabIndex = 121;
            this.lb_x13c_ld_panel_critical_alarm.Text = "[ X13C ] L/D부 판넬 화재 알람";
            // 
            // pn_Loader_IN2_X13C
            // 
            this.pn_Loader_IN2_X13C.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN2_X13C.Location = new System.Drawing.Point(433, 580);
            this.pn_Loader_IN2_X13C.Name = "pn_Loader_IN2_X13C";
            this.pn_Loader_IN2_X13C.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN2_X13C.TabIndex = 120;
            // 
            // lb_x13b_2_ld_cst_d_ungrip_pos
            // 
            this.lb_x13b_2_ld_cst_d_ungrip_pos.AutoSize = true;
            this.lb_x13b_2_ld_cst_d_ungrip_pos.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x13b_2_ld_cst_d_ungrip_pos.ForeColor = System.Drawing.Color.White;
            this.lb_x13b_2_ld_cst_d_ungrip_pos.Location = new System.Drawing.Point(479, 548);
            this.lb_x13b_2_ld_cst_d_ungrip_pos.Name = "lb_x13b_2_ld_cst_d_ungrip_pos";
            this.lb_x13b_2_ld_cst_d_ungrip_pos.Size = new System.Drawing.Size(302, 13);
            this.lb_x13b_2_ld_cst_d_ungrip_pos.TabIndex = 119;
            this.lb_x13b_2_ld_cst_d_ungrip_pos.Text = "[ X13B ] 2 L/D Cassette 하부 UnGrip POS";
            // 
            // pn_Loader_IN2_X13B
            // 
            this.pn_Loader_IN2_X13B.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN2_X13B.Location = new System.Drawing.Point(433, 534);
            this.pn_Loader_IN2_X13B.Name = "pn_Loader_IN2_X13B";
            this.pn_Loader_IN2_X13B.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN2_X13B.TabIndex = 118;
            // 
            // lb_x13a_2_ld_cst_d_grip_pos
            // 
            this.lb_x13a_2_ld_cst_d_grip_pos.AutoSize = true;
            this.lb_x13a_2_ld_cst_d_grip_pos.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x13a_2_ld_cst_d_grip_pos.ForeColor = System.Drawing.Color.White;
            this.lb_x13a_2_ld_cst_d_grip_pos.Location = new System.Drawing.Point(479, 501);
            this.lb_x13a_2_ld_cst_d_grip_pos.Name = "lb_x13a_2_ld_cst_d_grip_pos";
            this.lb_x13a_2_ld_cst_d_grip_pos.Size = new System.Drawing.Size(282, 13);
            this.lb_x13a_2_ld_cst_d_grip_pos.TabIndex = 117;
            this.lb_x13a_2_ld_cst_d_grip_pos.Text = "[ X13A ] 2 L/D Cassette 하부 Grip POS";
            // 
            // pn_Loader_IN2_X13A
            // 
            this.pn_Loader_IN2_X13A.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN2_X13A.Location = new System.Drawing.Point(433, 488);
            this.pn_Loader_IN2_X13A.Name = "pn_Loader_IN2_X13A";
            this.pn_Loader_IN2_X13A.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN2_X13A.TabIndex = 116;
            // 
            // lb_x139_2_2_ld_cst_lr_ungrip_pos
            // 
            this.lb_x139_2_2_ld_cst_lr_ungrip_pos.AutoSize = true;
            this.lb_x139_2_2_ld_cst_lr_ungrip_pos.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x139_2_2_ld_cst_lr_ungrip_pos.ForeColor = System.Drawing.Color.White;
            this.lb_x139_2_2_ld_cst_lr_ungrip_pos.Location = new System.Drawing.Point(479, 454);
            this.lb_x139_2_2_ld_cst_lr_ungrip_pos.Name = "lb_x139_2_2_ld_cst_lr_ungrip_pos";
            this.lb_x139_2_2_ld_cst_lr_ungrip_pos.Size = new System.Drawing.Size(316, 13);
            this.lb_x139_2_2_ld_cst_lr_ungrip_pos.TabIndex = 115;
            this.lb_x139_2_2_ld_cst_lr_ungrip_pos.Text = "[ X139 ] 2-2 L/D Cassette 좌우 UnGrip POS";
            // 
            // pn_Loader_IN2_X139
            // 
            this.pn_Loader_IN2_X139.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN2_X139.Location = new System.Drawing.Point(433, 442);
            this.pn_Loader_IN2_X139.Name = "pn_Loader_IN2_X139";
            this.pn_Loader_IN2_X139.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN2_X139.TabIndex = 114;
            // 
            // lb_x138_2_2_ld_cst_lr_grip_pos
            // 
            this.lb_x138_2_2_ld_cst_lr_grip_pos.AutoSize = true;
            this.lb_x138_2_2_ld_cst_lr_grip_pos.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x138_2_2_ld_cst_lr_grip_pos.ForeColor = System.Drawing.Color.White;
            this.lb_x138_2_2_ld_cst_lr_grip_pos.Location = new System.Drawing.Point(479, 409);
            this.lb_x138_2_2_ld_cst_lr_grip_pos.Name = "lb_x138_2_2_ld_cst_lr_grip_pos";
            this.lb_x138_2_2_ld_cst_lr_grip_pos.Size = new System.Drawing.Size(297, 13);
            this.lb_x138_2_2_ld_cst_lr_grip_pos.TabIndex = 113;
            this.lb_x138_2_2_ld_cst_lr_grip_pos.Text = "[ X138 ] 2-2 L/D Cassette 좌우 Grip POS";
            // 
            // pn_Loader_IN2_X138
            // 
            this.pn_Loader_IN2_X138.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN2_X138.Location = new System.Drawing.Point(433, 396);
            this.pn_Loader_IN2_X138.Name = "pn_Loader_IN2_X138";
            this.pn_Loader_IN2_X138.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN2_X138.TabIndex = 112;
            // 
            // lb_x137_2_1_ld_cst_lr_ungrip_pos
            // 
            this.lb_x137_2_1_ld_cst_lr_ungrip_pos.AutoSize = true;
            this.lb_x137_2_1_ld_cst_lr_ungrip_pos.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x137_2_1_ld_cst_lr_ungrip_pos.ForeColor = System.Drawing.Color.White;
            this.lb_x137_2_1_ld_cst_lr_ungrip_pos.Location = new System.Drawing.Point(479, 364);
            this.lb_x137_2_1_ld_cst_lr_ungrip_pos.Name = "lb_x137_2_1_ld_cst_lr_ungrip_pos";
            this.lb_x137_2_1_ld_cst_lr_ungrip_pos.Size = new System.Drawing.Size(316, 13);
            this.lb_x137_2_1_ld_cst_lr_ungrip_pos.TabIndex = 111;
            this.lb_x137_2_1_ld_cst_lr_ungrip_pos.Text = "[ X137 ] 2-1 L/D Cassette 좌우 UnGrip POS";
            // 
            // pn_Loader_IN2_X137
            // 
            this.pn_Loader_IN2_X137.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN2_X137.Location = new System.Drawing.Point(433, 350);
            this.pn_Loader_IN2_X137.Name = "pn_Loader_IN2_X137";
            this.pn_Loader_IN2_X137.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN2_X137.TabIndex = 110;
            // 
            // lb_x136_2_1_ld_cst_lr_grip_pos
            // 
            this.lb_x136_2_1_ld_cst_lr_grip_pos.AutoSize = true;
            this.lb_x136_2_1_ld_cst_lr_grip_pos.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x136_2_1_ld_cst_lr_grip_pos.ForeColor = System.Drawing.Color.White;
            this.lb_x136_2_1_ld_cst_lr_grip_pos.Location = new System.Drawing.Point(479, 319);
            this.lb_x136_2_1_ld_cst_lr_grip_pos.Name = "lb_x136_2_1_ld_cst_lr_grip_pos";
            this.lb_x136_2_1_ld_cst_lr_grip_pos.Size = new System.Drawing.Size(297, 13);
            this.lb_x136_2_1_ld_cst_lr_grip_pos.TabIndex = 109;
            this.lb_x136_2_1_ld_cst_lr_grip_pos.Text = "[ X136 ] 2-1 L/D Cassette 좌우 Grip POS";
            // 
            // pn_Loader_IN2_X136
            // 
            this.pn_Loader_IN2_X136.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN2_X136.Location = new System.Drawing.Point(433, 304);
            this.pn_Loader_IN2_X136.Name = "pn_Loader_IN2_X136";
            this.pn_Loader_IN2_X136.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN2_X136.TabIndex = 108;
            // 
            // lb_x135_1_ld_cst_d_ungrip_pos
            // 
            this.lb_x135_1_ld_cst_d_ungrip_pos.AutoSize = true;
            this.lb_x135_1_ld_cst_d_ungrip_pos.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x135_1_ld_cst_d_ungrip_pos.ForeColor = System.Drawing.Color.White;
            this.lb_x135_1_ld_cst_d_ungrip_pos.Location = new System.Drawing.Point(479, 271);
            this.lb_x135_1_ld_cst_d_ungrip_pos.Name = "lb_x135_1_ld_cst_d_ungrip_pos";
            this.lb_x135_1_ld_cst_d_ungrip_pos.Size = new System.Drawing.Size(300, 13);
            this.lb_x135_1_ld_cst_d_ungrip_pos.TabIndex = 107;
            this.lb_x135_1_ld_cst_d_ungrip_pos.Text = "[ X135 ] 1 L/D Cassette 하부 UnGrip POS";
            // 
            // pn_Loader_IN2_X135
            // 
            this.pn_Loader_IN2_X135.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN2_X135.Location = new System.Drawing.Point(433, 258);
            this.pn_Loader_IN2_X135.Name = "pn_Loader_IN2_X135";
            this.pn_Loader_IN2_X135.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN2_X135.TabIndex = 106;
            // 
            // lb_x134_1_ld_cst_d_grip_pos
            // 
            this.lb_x134_1_ld_cst_d_grip_pos.AutoSize = true;
            this.lb_x134_1_ld_cst_d_grip_pos.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x134_1_ld_cst_d_grip_pos.ForeColor = System.Drawing.Color.White;
            this.lb_x134_1_ld_cst_d_grip_pos.Location = new System.Drawing.Point(479, 226);
            this.lb_x134_1_ld_cst_d_grip_pos.Name = "lb_x134_1_ld_cst_d_grip_pos";
            this.lb_x134_1_ld_cst_d_grip_pos.Size = new System.Drawing.Size(281, 13);
            this.lb_x134_1_ld_cst_d_grip_pos.TabIndex = 105;
            this.lb_x134_1_ld_cst_d_grip_pos.Text = "[ X134 ] 1 L/D Cassette 하부 Grip POS";
            // 
            // pn_Loader_IN2_X134
            // 
            this.pn_Loader_IN2_X134.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN2_X134.Location = new System.Drawing.Point(433, 212);
            this.pn_Loader_IN2_X134.Name = "pn_Loader_IN2_X134";
            this.pn_Loader_IN2_X134.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN2_X134.TabIndex = 104;
            // 
            // lb_x133_1_2_ld_cst_lr_ungrip_pos
            // 
            this.lb_x133_1_2_ld_cst_lr_ungrip_pos.AutoSize = true;
            this.lb_x133_1_2_ld_cst_lr_ungrip_pos.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x133_1_2_ld_cst_lr_ungrip_pos.ForeColor = System.Drawing.Color.White;
            this.lb_x133_1_2_ld_cst_lr_ungrip_pos.Location = new System.Drawing.Point(479, 179);
            this.lb_x133_1_2_ld_cst_lr_ungrip_pos.Name = "lb_x133_1_2_ld_cst_lr_ungrip_pos";
            this.lb_x133_1_2_ld_cst_lr_ungrip_pos.Size = new System.Drawing.Size(316, 13);
            this.lb_x133_1_2_ld_cst_lr_ungrip_pos.TabIndex = 103;
            this.lb_x133_1_2_ld_cst_lr_ungrip_pos.Text = "[ X133 ] 1-2 L/D Cassette 좌우 UnGrip POS";
            // 
            // pn_Loader_IN2_X133
            // 
            this.pn_Loader_IN2_X133.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN2_X133.Location = new System.Drawing.Point(433, 166);
            this.pn_Loader_IN2_X133.Name = "pn_Loader_IN2_X133";
            this.pn_Loader_IN2_X133.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN2_X133.TabIndex = 102;
            // 
            // lb_x132_1_2_ld_cst_lr_grip_pos
            // 
            this.lb_x132_1_2_ld_cst_lr_grip_pos.AutoSize = true;
            this.lb_x132_1_2_ld_cst_lr_grip_pos.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x132_1_2_ld_cst_lr_grip_pos.ForeColor = System.Drawing.Color.White;
            this.lb_x132_1_2_ld_cst_lr_grip_pos.Location = new System.Drawing.Point(479, 133);
            this.lb_x132_1_2_ld_cst_lr_grip_pos.Name = "lb_x132_1_2_ld_cst_lr_grip_pos";
            this.lb_x132_1_2_ld_cst_lr_grip_pos.Size = new System.Drawing.Size(297, 13);
            this.lb_x132_1_2_ld_cst_lr_grip_pos.TabIndex = 101;
            this.lb_x132_1_2_ld_cst_lr_grip_pos.Text = "[ X132 ] 1-2 L/D Cassette 좌우 Grip POS";
            // 
            // pn_Loader_IN2_X132
            // 
            this.pn_Loader_IN2_X132.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN2_X132.Location = new System.Drawing.Point(433, 120);
            this.pn_Loader_IN2_X132.Name = "pn_Loader_IN2_X132";
            this.pn_Loader_IN2_X132.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN2_X132.TabIndex = 100;
            // 
            // lb_x131_1_1_ld_cst_lr_ungrip_pos
            // 
            this.lb_x131_1_1_ld_cst_lr_ungrip_pos.AutoSize = true;
            this.lb_x131_1_1_ld_cst_lr_ungrip_pos.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x131_1_1_ld_cst_lr_ungrip_pos.ForeColor = System.Drawing.Color.White;
            this.lb_x131_1_1_ld_cst_lr_ungrip_pos.Location = new System.Drawing.Point(479, 86);
            this.lb_x131_1_1_ld_cst_lr_ungrip_pos.Name = "lb_x131_1_1_ld_cst_lr_ungrip_pos";
            this.lb_x131_1_1_ld_cst_lr_ungrip_pos.Size = new System.Drawing.Size(316, 13);
            this.lb_x131_1_1_ld_cst_lr_ungrip_pos.TabIndex = 99;
            this.lb_x131_1_1_ld_cst_lr_ungrip_pos.Text = "[ X131 ] 1-1 L/D Cassette 좌우 UnGrip POS";
            // 
            // pn_Loader_IN2_X131
            // 
            this.pn_Loader_IN2_X131.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN2_X131.Location = new System.Drawing.Point(433, 74);
            this.pn_Loader_IN2_X131.Name = "pn_Loader_IN2_X131";
            this.pn_Loader_IN2_X131.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN2_X131.TabIndex = 98;
            // 
            // lb_x130_1_1_ld_cst_lr_grip_pos
            // 
            this.lb_x130_1_1_ld_cst_lr_grip_pos.AutoSize = true;
            this.lb_x130_1_1_ld_cst_lr_grip_pos.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x130_1_1_ld_cst_lr_grip_pos.ForeColor = System.Drawing.Color.White;
            this.lb_x130_1_1_ld_cst_lr_grip_pos.Location = new System.Drawing.Point(479, 42);
            this.lb_x130_1_1_ld_cst_lr_grip_pos.Name = "lb_x130_1_1_ld_cst_lr_grip_pos";
            this.lb_x130_1_1_ld_cst_lr_grip_pos.Size = new System.Drawing.Size(297, 13);
            this.lb_x130_1_1_ld_cst_lr_grip_pos.TabIndex = 97;
            this.lb_x130_1_1_ld_cst_lr_grip_pos.Text = "[ X130 ] 1-1 L/D Cassette 좌우 Grip POS";
            // 
            // pn_Loader_IN2_X130
            // 
            this.pn_Loader_IN2_X130.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN2_X130.Location = new System.Drawing.Point(433, 28);
            this.pn_Loader_IN2_X130.Name = "pn_Loader_IN2_X130";
            this.pn_Loader_IN2_X130.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN2_X130.TabIndex = 96;
            // 
            // lb_x12f_spare
            // 
            this.lb_x12f_spare.AutoSize = true;
            this.lb_x12f_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x12f_spare.ForeColor = System.Drawing.Color.White;
            this.lb_x12f_spare.Location = new System.Drawing.Point(52, 730);
            this.lb_x12f_spare.Name = "lb_x12f_spare";
            this.lb_x12f_spare.Size = new System.Drawing.Size(114, 13);
            this.lb_x12f_spare.TabIndex = 95;
            this.lb_x12f_spare.Text = "[ X12F ] Spare";
            // 
            // pn_Loader_IN2_X12F
            // 
            this.pn_Loader_IN2_X12F.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN2_X12F.Location = new System.Drawing.Point(6, 718);
            this.pn_Loader_IN2_X12F.Name = "pn_Loader_IN2_X12F";
            this.pn_Loader_IN2_X12F.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN2_X12F.TabIndex = 94;
            // 
            // lb_x12e_2_3_ld_cst_dettect
            // 
            this.lb_x12e_2_3_ld_cst_dettect.AutoSize = true;
            this.lb_x12e_2_3_ld_cst_dettect.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x12e_2_3_ld_cst_dettect.ForeColor = System.Drawing.Color.White;
            this.lb_x12e_2_3_ld_cst_dettect.Location = new System.Drawing.Point(52, 685);
            this.lb_x12e_2_3_ld_cst_dettect.Name = "lb_x12e_2_3_ld_cst_dettect";
            this.lb_x12e_2_3_ld_cst_dettect.Size = new System.Drawing.Size(244, 13);
            this.lb_x12e_2_3_ld_cst_dettect.TabIndex = 93;
            this.lb_x12e_2_3_ld_cst_dettect.Text = "[ X12E ] 2-3 L/D Cassette Detect";
            // 
            // pn_Loader_IN2_X1E
            // 
            this.pn_Loader_IN2_X1E.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN2_X1E.Location = new System.Drawing.Point(6, 672);
            this.pn_Loader_IN2_X1E.Name = "pn_Loader_IN2_X1E";
            this.pn_Loader_IN2_X1E.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN2_X1E.TabIndex = 92;
            // 
            // lb_x12d_2_2_ld_cst_dettect
            // 
            this.lb_x12d_2_2_ld_cst_dettect.AutoSize = true;
            this.lb_x12d_2_2_ld_cst_dettect.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x12d_2_2_ld_cst_dettect.ForeColor = System.Drawing.Color.White;
            this.lb_x12d_2_2_ld_cst_dettect.Location = new System.Drawing.Point(52, 640);
            this.lb_x12d_2_2_ld_cst_dettect.Name = "lb_x12d_2_2_ld_cst_dettect";
            this.lb_x12d_2_2_ld_cst_dettect.Size = new System.Drawing.Size(245, 13);
            this.lb_x12d_2_2_ld_cst_dettect.TabIndex = 91;
            this.lb_x12d_2_2_ld_cst_dettect.Text = "[ X12D ] 2-2 L/D Cassette Detect";
            // 
            // pn_Loader_IN2_X12D
            // 
            this.pn_Loader_IN2_X12D.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN2_X12D.Location = new System.Drawing.Point(6, 626);
            this.pn_Loader_IN2_X12D.Name = "pn_Loader_IN2_X12D";
            this.pn_Loader_IN2_X12D.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN2_X12D.TabIndex = 90;
            // 
            // lb_x12c_2_1_ld_cst_dettect
            // 
            this.lb_x12c_2_1_ld_cst_dettect.AutoSize = true;
            this.lb_x12c_2_1_ld_cst_dettect.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x12c_2_1_ld_cst_dettect.ForeColor = System.Drawing.Color.White;
            this.lb_x12c_2_1_ld_cst_dettect.Location = new System.Drawing.Point(52, 594);
            this.lb_x12c_2_1_ld_cst_dettect.Name = "lb_x12c_2_1_ld_cst_dettect";
            this.lb_x12c_2_1_ld_cst_dettect.Size = new System.Drawing.Size(245, 13);
            this.lb_x12c_2_1_ld_cst_dettect.TabIndex = 89;
            this.lb_x12c_2_1_ld_cst_dettect.Text = "[ X12C ] 2-1 L/D Cassette Detect";
            // 
            // pn_Loader_IN2_X12C
            // 
            this.pn_Loader_IN2_X12C.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN2_X12C.Location = new System.Drawing.Point(6, 580);
            this.pn_Loader_IN2_X12C.Name = "pn_Loader_IN2_X12C";
            this.pn_Loader_IN2_X12C.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN2_X12C.TabIndex = 88;
            // 
            // lb_x12b_1_3_ld_cst_dettect
            // 
            this.lb_x12b_1_3_ld_cst_dettect.AutoSize = true;
            this.lb_x12b_1_3_ld_cst_dettect.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x12b_1_3_ld_cst_dettect.ForeColor = System.Drawing.Color.White;
            this.lb_x12b_1_3_ld_cst_dettect.Location = new System.Drawing.Point(52, 548);
            this.lb_x12b_1_3_ld_cst_dettect.Name = "lb_x12b_1_3_ld_cst_dettect";
            this.lb_x12b_1_3_ld_cst_dettect.Size = new System.Drawing.Size(245, 13);
            this.lb_x12b_1_3_ld_cst_dettect.TabIndex = 87;
            this.lb_x12b_1_3_ld_cst_dettect.Text = "[ X12B ] 1-3 L/D Cassette Detect";
            // 
            // pn_Loader_IN2_X12B
            // 
            this.pn_Loader_IN2_X12B.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN2_X12B.Location = new System.Drawing.Point(6, 534);
            this.pn_Loader_IN2_X12B.Name = "pn_Loader_IN2_X12B";
            this.pn_Loader_IN2_X12B.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN2_X12B.TabIndex = 86;
            // 
            // lb_x12a_1_2_ld_cst_dettect
            // 
            this.lb_x12a_1_2_ld_cst_dettect.AutoSize = true;
            this.lb_x12a_1_2_ld_cst_dettect.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x12a_1_2_ld_cst_dettect.ForeColor = System.Drawing.Color.White;
            this.lb_x12a_1_2_ld_cst_dettect.Location = new System.Drawing.Point(52, 501);
            this.lb_x12a_1_2_ld_cst_dettect.Name = "lb_x12a_1_2_ld_cst_dettect";
            this.lb_x12a_1_2_ld_cst_dettect.Size = new System.Drawing.Size(244, 13);
            this.lb_x12a_1_2_ld_cst_dettect.TabIndex = 85;
            this.lb_x12a_1_2_ld_cst_dettect.Text = "[ X12A ] 1-2 L/D Cassette Detect";
            // 
            // pn_Loader_IN2_X12A
            // 
            this.pn_Loader_IN2_X12A.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN2_X12A.Location = new System.Drawing.Point(6, 488);
            this.pn_Loader_IN2_X12A.Name = "pn_Loader_IN2_X12A";
            this.pn_Loader_IN2_X12A.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN2_X12A.TabIndex = 84;
            // 
            // lb_x129_1_1_ld_cst_dettect
            // 
            this.lb_x129_1_1_ld_cst_dettect.AutoSize = true;
            this.lb_x129_1_1_ld_cst_dettect.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x129_1_1_ld_cst_dettect.ForeColor = System.Drawing.Color.White;
            this.lb_x129_1_1_ld_cst_dettect.Location = new System.Drawing.Point(52, 454);
            this.lb_x129_1_1_ld_cst_dettect.Name = "lb_x129_1_1_ld_cst_dettect";
            this.lb_x129_1_1_ld_cst_dettect.Size = new System.Drawing.Size(243, 13);
            this.lb_x129_1_1_ld_cst_dettect.TabIndex = 83;
            this.lb_x129_1_1_ld_cst_dettect.Text = "[ X129 ] 1-1 L/D Cassette Detect";
            // 
            // pn_Loader_IN2_X129
            // 
            this.pn_Loader_IN2_X129.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN2_X129.Location = new System.Drawing.Point(6, 442);
            this.pn_Loader_IN2_X129.Name = "pn_Loader_IN2_X129";
            this.pn_Loader_IN2_X129.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN2_X129.TabIndex = 82;
            // 
            // lb_x128_control_select_ld
            // 
            this.lb_x128_control_select_ld.AutoSize = true;
            this.lb_x128_control_select_ld.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x128_control_select_ld.ForeColor = System.Drawing.Color.White;
            this.lb_x128_control_select_ld.Location = new System.Drawing.Point(52, 409);
            this.lb_x128_control_select_ld.Name = "lb_x128_control_select_ld";
            this.lb_x128_control_select_ld.Size = new System.Drawing.Size(202, 13);
            this.lb_x128_control_select_ld.TabIndex = 81;
            this.lb_x128_control_select_ld.Text = "[ X128 ] Control Select L/D";
            // 
            // pn_Loader_IN2_X128
            // 
            this.pn_Loader_IN2_X128.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN2_X128.Location = new System.Drawing.Point(6, 396);
            this.pn_Loader_IN2_X128.Name = "pn_Loader_IN2_X128";
            this.pn_Loader_IN2_X128.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN2_X128.TabIndex = 80;
            // 
            // lb_x127_spare
            // 
            this.lb_x127_spare.AutoSize = true;
            this.lb_x127_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x127_spare.ForeColor = System.Drawing.Color.White;
            this.lb_x127_spare.Location = new System.Drawing.Point(52, 364);
            this.lb_x127_spare.Name = "lb_x127_spare";
            this.lb_x127_spare.Size = new System.Drawing.Size(114, 13);
            this.lb_x127_spare.TabIndex = 79;
            this.lb_x127_spare.Text = "[ X127 ] Spare";
            // 
            // pn_Loader_IN2_X127
            // 
            this.pn_Loader_IN2_X127.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN2_X127.Location = new System.Drawing.Point(6, 350);
            this.pn_Loader_IN2_X127.Name = "pn_Loader_IN2_X127";
            this.pn_Loader_IN2_X127.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN2_X127.TabIndex = 78;
            // 
            // lb_x126_spare
            // 
            this.lb_x126_spare.AutoSize = true;
            this.lb_x126_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x126_spare.ForeColor = System.Drawing.Color.White;
            this.lb_x126_spare.Location = new System.Drawing.Point(52, 319);
            this.lb_x126_spare.Name = "lb_x126_spare";
            this.lb_x126_spare.Size = new System.Drawing.Size(114, 13);
            this.lb_x126_spare.TabIndex = 77;
            this.lb_x126_spare.Text = "[ X126 ] Spare";
            // 
            // pn_Loader_IN2_X126
            // 
            this.pn_Loader_IN2_X126.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN2_X126.Location = new System.Drawing.Point(6, 304);
            this.pn_Loader_IN2_X126.Name = "pn_Loader_IN2_X126";
            this.pn_Loader_IN2_X126.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN2_X126.TabIndex = 76;
            // 
            // lb_x125_spare
            // 
            this.lb_x125_spare.AutoSize = true;
            this.lb_x125_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x125_spare.ForeColor = System.Drawing.Color.White;
            this.lb_x125_spare.Location = new System.Drawing.Point(52, 271);
            this.lb_x125_spare.Name = "lb_x125_spare";
            this.lb_x125_spare.Size = new System.Drawing.Size(114, 13);
            this.lb_x125_spare.TabIndex = 75;
            this.lb_x125_spare.Text = "[ X125 ] Spare";
            // 
            // pn_Loader_IN2_X125
            // 
            this.pn_Loader_IN2_X125.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN2_X125.Location = new System.Drawing.Point(6, 258);
            this.pn_Loader_IN2_X125.Name = "pn_Loader_IN2_X125";
            this.pn_Loader_IN2_X125.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN2_X125.TabIndex = 74;
            // 
            // lb_x124_efu_alarm
            // 
            this.lb_x124_efu_alarm.AutoSize = true;
            this.lb_x124_efu_alarm.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x124_efu_alarm.ForeColor = System.Drawing.Color.White;
            this.lb_x124_efu_alarm.Location = new System.Drawing.Point(52, 226);
            this.lb_x124_efu_alarm.Name = "lb_x124_efu_alarm";
            this.lb_x124_efu_alarm.Size = new System.Drawing.Size(143, 13);
            this.lb_x124_efu_alarm.TabIndex = 73;
            this.lb_x124_efu_alarm.Text = "[ X124 ] EFU Alarm";
            // 
            // pn_Loader_IN2_X124
            // 
            this.pn_Loader_IN2_X124.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN2_X124.Location = new System.Drawing.Point(6, 212);
            this.pn_Loader_IN2_X124.Name = "pn_Loader_IN2_X124";
            this.pn_Loader_IN2_X124.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN2_X124.TabIndex = 72;
            // 
            // lb_x123_laser_mc_on
            // 
            this.lb_x123_laser_mc_on.AutoSize = true;
            this.lb_x123_laser_mc_on.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x123_laser_mc_on.ForeColor = System.Drawing.Color.White;
            this.lb_x123_laser_mc_on.Location = new System.Drawing.Point(52, 179);
            this.lb_x123_laser_mc_on.Name = "lb_x123_laser_mc_on";
            this.lb_x123_laser_mc_on.Size = new System.Drawing.Size(166, 13);
            this.lb_x123_laser_mc_on.TabIndex = 71;
            this.lb_x123_laser_mc_on.Text = "[ X123 ] 가공부 MC ON";
            // 
            // pn_Loader_IN2_X123
            // 
            this.pn_Loader_IN2_X123.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN2_X123.Location = new System.Drawing.Point(6, 166);
            this.pn_Loader_IN2_X123.Name = "pn_Loader_IN2_X123";
            this.pn_Loader_IN2_X123.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN2_X123.TabIndex = 70;
            // 
            // lb_x122_ld_mc_on
            // 
            this.lb_x122_ld_mc_on.AutoSize = true;
            this.lb_x122_ld_mc_on.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x122_ld_mc_on.ForeColor = System.Drawing.Color.White;
            this.lb_x122_ld_mc_on.Location = new System.Drawing.Point(52, 133);
            this.lb_x122_ld_mc_on.Name = "lb_x122_ld_mc_on";
            this.lb_x122_ld_mc_on.Size = new System.Drawing.Size(166, 13);
            this.lb_x122_ld_mc_on.TabIndex = 69;
            this.lb_x122_ld_mc_on.Text = "[ X122 ] 로더부 MC ON";
            // 
            // pn_Loader_IN2_X122
            // 
            this.pn_Loader_IN2_X122.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN2_X122.Location = new System.Drawing.Point(6, 120);
            this.pn_Loader_IN2_X122.Name = "pn_Loader_IN2_X122";
            this.pn_Loader_IN2_X122.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN2_X122.TabIndex = 68;
            // 
            // lb_x121_mode_select_sw_teach
            // 
            this.lb_x121_mode_select_sw_teach.AutoSize = true;
            this.lb_x121_mode_select_sw_teach.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x121_mode_select_sw_teach.ForeColor = System.Drawing.Color.White;
            this.lb_x121_mode_select_sw_teach.Location = new System.Drawing.Point(52, 86);
            this.lb_x121_mode_select_sw_teach.Name = "lb_x121_mode_select_sw_teach";
            this.lb_x121_mode_select_sw_teach.Size = new System.Drawing.Size(251, 13);
            this.lb_x121_mode_select_sw_teach.TabIndex = 67;
            this.lb_x121_mode_select_sw_teach.Text = "[ X121 ] Mode Select SW - Teach";
            // 
            // pn_Loader_IN2_X121
            // 
            this.pn_Loader_IN2_X121.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN2_X121.Location = new System.Drawing.Point(6, 74);
            this.pn_Loader_IN2_X121.Name = "pn_Loader_IN2_X121";
            this.pn_Loader_IN2_X121.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN2_X121.TabIndex = 66;
            // 
            // lb_x120_mode_select_sw_auto
            // 
            this.lb_x120_mode_select_sw_auto.AutoSize = true;
            this.lb_x120_mode_select_sw_auto.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x120_mode_select_sw_auto.ForeColor = System.Drawing.Color.White;
            this.lb_x120_mode_select_sw_auto.Location = new System.Drawing.Point(52, 42);
            this.lb_x120_mode_select_sw_auto.Name = "lb_x120_mode_select_sw_auto";
            this.lb_x120_mode_select_sw_auto.Size = new System.Drawing.Size(237, 13);
            this.lb_x120_mode_select_sw_auto.TabIndex = 65;
            this.lb_x120_mode_select_sw_auto.Text = "[ X120 ] Mode Select SW - Auto";
            // 
            // pn_Loader_IN2_X120
            // 
            this.pn_Loader_IN2_X120.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN2_X120.Location = new System.Drawing.Point(6, 28);
            this.pn_Loader_IN2_X120.Name = "pn_Loader_IN2_X120";
            this.pn_Loader_IN2_X120.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN2_X120.TabIndex = 64;
            // 
            // tp_iostatus_ld_in3
            // 
            this.tp_iostatus_ld_in3.BackColor = System.Drawing.Color.DimGray;
            this.tp_iostatus_ld_in3.Controls.Add(this.lb_x15f_spare);
            this.tp_iostatus_ld_in3.Controls.Add(this.pn_Loader_IN3_X15F);
            this.tp_iostatus_ld_in3.Controls.Add(this.lb_x15e_6_laser_fan_stop_alarm);
            this.tp_iostatus_ld_in3.Controls.Add(this.pn_Loader_IN3_X15E);
            this.tp_iostatus_ld_in3.Controls.Add(this.lb_x15d_2_ld_trans_down);
            this.tp_iostatus_ld_in3.Controls.Add(this.pn_Loader_IN3_X15D);
            this.tp_iostatus_ld_in3.Controls.Add(this.lb_x15c_2_ld_trans_up);
            this.tp_iostatus_ld_in3.Controls.Add(this.pn_Loader_IN3_X15C);
            this.tp_iostatus_ld_in3.Controls.Add(this.lb_x15b_1_ld_trans_down);
            this.tp_iostatus_ld_in3.Controls.Add(this.pn_Loader_IN3_X15B);
            this.tp_iostatus_ld_in3.Controls.Add(this.lb_x15a_1_ld_trans_up);
            this.tp_iostatus_ld_in3.Controls.Add(this.pn_Loader_IN3_X15A);
            this.tp_iostatus_ld_in3.Controls.Add(this.lb_x159_2_ld_cst_lift_tiltsensor_down);
            this.tp_iostatus_ld_in3.Controls.Add(this.pn_Loader_IN3_X159);
            this.tp_iostatus_ld_in3.Controls.Add(this.lb_x158_2_ld_cst_lift_tiltsensor_up);
            this.tp_iostatus_ld_in3.Controls.Add(this.pn_Loader_IN3_X158);
            this.tp_iostatus_ld_in3.Controls.Add(this.lb_x157_2_cst_trans_pressure_sw);
            this.tp_iostatus_ld_in3.Controls.Add(this.pn_Loader_IN3_X157);
            this.tp_iostatus_ld_in3.Controls.Add(this.lb_x156_1_cst_trans_pressure_sw);
            this.tp_iostatus_ld_in3.Controls.Add(this.pn_Loader_IN3_X156);
            this.tp_iostatus_ld_in3.Controls.Add(this.lb_x155_1_ld_cst_lift_tiltsensor_down);
            this.tp_iostatus_ld_in3.Controls.Add(this.pn_Loader_IN3_X155);
            this.tp_iostatus_ld_in3.Controls.Add(this.lb_x154_1_ld_cst_lift_tiltsensor_up);
            this.tp_iostatus_ld_in3.Controls.Add(this.pn_Loader_IN3_X154);
            this.tp_iostatus_ld_in3.Controls.Add(this.lb_x153_2_2_ld_cst_lifter_ungrip_pos);
            this.tp_iostatus_ld_in3.Controls.Add(this.pn_Loader_IN3_X153);
            this.tp_iostatus_ld_in3.Controls.Add(this.lb_x152_2_2_ld_cst_lifter_grip_pos);
            this.tp_iostatus_ld_in3.Controls.Add(this.pn_Loader_IN3_X152);
            this.tp_iostatus_ld_in3.Controls.Add(this.lb_x151_2_1_ld_cst_lifter_ungrip_pos);
            this.tp_iostatus_ld_in3.Controls.Add(this.pn_Loader_IN3_X151);
            this.tp_iostatus_ld_in3.Controls.Add(this.lb_x150_2_1_ld_cst_lifter_grip_pos);
            this.tp_iostatus_ld_in3.Controls.Add(this.pn_Loader_IN3_X150);
            this.tp_iostatus_ld_in3.Controls.Add(this.lb_x14f_1_2_ld_cst_lifter_ungrip_pos);
            this.tp_iostatus_ld_in3.Controls.Add(this.pn_Loader_IN3_X14F);
            this.tp_iostatus_ld_in3.Controls.Add(this.lb_x14e_1_2_ld_cst_lifter_grip_pos);
            this.tp_iostatus_ld_in3.Controls.Add(this.pn_Loader_IN3_X14E);
            this.tp_iostatus_ld_in3.Controls.Add(this.lb_x14d_1_1_ld_cst_lifter_ungrip_pos);
            this.tp_iostatus_ld_in3.Controls.Add(this.pn_Loader_IN3_X14D);
            this.tp_iostatus_ld_in3.Controls.Add(this.lb_x14c_1_1_ld_cst_lifter_grip_pos);
            this.tp_iostatus_ld_in3.Controls.Add(this.pn_Loader_IN3_X14C);
            this.tp_iostatus_ld_in3.Controls.Add(this.lb_x14b_4_ld_cst_d_ungrip_pos);
            this.tp_iostatus_ld_in3.Controls.Add(this.pn_Loader_IN3_X14B);
            this.tp_iostatus_ld_in3.Controls.Add(this.lb_x14a_4_ld_cst_d_grip_pos);
            this.tp_iostatus_ld_in3.Controls.Add(this.pn_Loader_IN3_X14A);
            this.tp_iostatus_ld_in3.Controls.Add(this.lb_x149_4_2_ld_cst_lr_ungrip_pos);
            this.tp_iostatus_ld_in3.Controls.Add(this.pn_Loader_IN3_X149);
            this.tp_iostatus_ld_in3.Controls.Add(this.lb_x148_4_2_ld_cst_lr_grip_pos);
            this.tp_iostatus_ld_in3.Controls.Add(this.pn_Loader_IN3_X148);
            this.tp_iostatus_ld_in3.Controls.Add(this.lb_x147_4_1_ld_cst_lr_ungrip_pos);
            this.tp_iostatus_ld_in3.Controls.Add(this.pn_Loader_IN3_X147);
            this.tp_iostatus_ld_in3.Controls.Add(this.lb_x146_4_1_ld_cst_lr_grip_pos);
            this.tp_iostatus_ld_in3.Controls.Add(this.pn_Loader_IN3_X146);
            this.tp_iostatus_ld_in3.Controls.Add(this.lb_x145_3_ld_cst_d_ungrip_pos);
            this.tp_iostatus_ld_in3.Controls.Add(this.pn_Loader_IN3_X145);
            this.tp_iostatus_ld_in3.Controls.Add(this.lb_x144_3_ld_cst_d_grip_pos);
            this.tp_iostatus_ld_in3.Controls.Add(this.pn_Loader_IN3_X144);
            this.tp_iostatus_ld_in3.Controls.Add(this.lb_x143_3_2_ld_cst_lr_ungrip_pos);
            this.tp_iostatus_ld_in3.Controls.Add(this.pn_Loader_IN3_X143);
            this.tp_iostatus_ld_in3.Controls.Add(this.lb_x142_3_2_ld_cst_lr_grip_pos);
            this.tp_iostatus_ld_in3.Controls.Add(this.pn_Loader_IN3_X142);
            this.tp_iostatus_ld_in3.Controls.Add(this.lb_x141_3_1_ld_cst_lr_ungrip_pos);
            this.tp_iostatus_ld_in3.Controls.Add(this.pn_Loader_IN3_X141);
            this.tp_iostatus_ld_in3.Controls.Add(this.lb_x140_3_1_ld_cst_lr_grip_pos);
            this.tp_iostatus_ld_in3.Controls.Add(this.pn_Loader_IN3_X140);
            this.tp_iostatus_ld_in3.Location = new System.Drawing.Point(4, 34);
            this.tp_iostatus_ld_in3.Name = "tp_iostatus_ld_in3";
            this.tp_iostatus_ld_in3.Padding = new System.Windows.Forms.Padding(3);
            this.tp_iostatus_ld_in3.Size = new System.Drawing.Size(846, 778);
            this.tp_iostatus_ld_in3.TabIndex = 2;
            this.tp_iostatus_ld_in3.Text = "IN - 3";
            // 
            // lb_x15f_spare
            // 
            this.lb_x15f_spare.AutoSize = true;
            this.lb_x15f_spare.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x15f_spare.ForeColor = System.Drawing.Color.White;
            this.lb_x15f_spare.Location = new System.Drawing.Point(479, 730);
            this.lb_x15f_spare.Name = "lb_x15f_spare";
            this.lb_x15f_spare.Size = new System.Drawing.Size(114, 13);
            this.lb_x15f_spare.TabIndex = 191;
            this.lb_x15f_spare.Text = "[ X15F ] Spare";
            // 
            // pn_Loader_IN3_X15F
            // 
            this.pn_Loader_IN3_X15F.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN3_X15F.Location = new System.Drawing.Point(433, 718);
            this.pn_Loader_IN3_X15F.Name = "pn_Loader_IN3_X15F";
            this.pn_Loader_IN3_X15F.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN3_X15F.TabIndex = 190;
            // 
            // lb_x15e_6_laser_fan_stop_alarm
            // 
            this.lb_x15e_6_laser_fan_stop_alarm.AutoSize = true;
            this.lb_x15e_6_laser_fan_stop_alarm.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x15e_6_laser_fan_stop_alarm.ForeColor = System.Drawing.Color.White;
            this.lb_x15e_6_laser_fan_stop_alarm.Location = new System.Drawing.Point(479, 685);
            this.lb_x15e_6_laser_fan_stop_alarm.Name = "lb_x15e_6_laser_fan_stop_alarm";
            this.lb_x15e_6_laser_fan_stop_alarm.Size = new System.Drawing.Size(241, 13);
            this.lb_x15e_6_laser_fan_stop_alarm.TabIndex = 189;
            this.lb_x15e_6_laser_fan_stop_alarm.Text = "[ X15E ] 6 가공부 FAN Stop Alarm";
            // 
            // pn_Loader_IN3_X15E
            // 
            this.pn_Loader_IN3_X15E.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN3_X15E.Location = new System.Drawing.Point(433, 672);
            this.pn_Loader_IN3_X15E.Name = "pn_Loader_IN3_X15E";
            this.pn_Loader_IN3_X15E.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN3_X15E.TabIndex = 188;
            // 
            // lb_x15d_2_ld_trans_down
            // 
            this.lb_x15d_2_ld_trans_down.AutoSize = true;
            this.lb_x15d_2_ld_trans_down.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x15d_2_ld_trans_down.ForeColor = System.Drawing.Color.White;
            this.lb_x15d_2_ld_trans_down.Location = new System.Drawing.Point(479, 640);
            this.lb_x15d_2_ld_trans_down.Name = "lb_x15d_2_ld_trans_down";
            this.lb_x15d_2_ld_trans_down.Size = new System.Drawing.Size(207, 13);
            this.lb_x15d_2_ld_trans_down.TabIndex = 187;
            this.lb_x15d_2_ld_trans_down.Text = "[ X15D ] 2 L/D 이재기 DOWN";
            // 
            // pn_Loader_IN3_X15D
            // 
            this.pn_Loader_IN3_X15D.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN3_X15D.Location = new System.Drawing.Point(433, 626);
            this.pn_Loader_IN3_X15D.Name = "pn_Loader_IN3_X15D";
            this.pn_Loader_IN3_X15D.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN3_X15D.TabIndex = 186;
            // 
            // lb_x15c_2_ld_trans_up
            // 
            this.lb_x15c_2_ld_trans_up.AutoSize = true;
            this.lb_x15c_2_ld_trans_up.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x15c_2_ld_trans_up.ForeColor = System.Drawing.Color.White;
            this.lb_x15c_2_ld_trans_up.Location = new System.Drawing.Point(479, 594);
            this.lb_x15c_2_ld_trans_up.Name = "lb_x15c_2_ld_trans_up";
            this.lb_x15c_2_ld_trans_up.Size = new System.Drawing.Size(183, 13);
            this.lb_x15c_2_ld_trans_up.TabIndex = 185;
            this.lb_x15c_2_ld_trans_up.Text = "[ X15C ] 2 L/D 이재기 UP";
            // 
            // pn_Loader_IN3_X15C
            // 
            this.pn_Loader_IN3_X15C.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN3_X15C.Location = new System.Drawing.Point(433, 580);
            this.pn_Loader_IN3_X15C.Name = "pn_Loader_IN3_X15C";
            this.pn_Loader_IN3_X15C.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN3_X15C.TabIndex = 184;
            // 
            // lb_x15b_1_ld_trans_down
            // 
            this.lb_x15b_1_ld_trans_down.AutoSize = true;
            this.lb_x15b_1_ld_trans_down.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x15b_1_ld_trans_down.ForeColor = System.Drawing.Color.White;
            this.lb_x15b_1_ld_trans_down.Location = new System.Drawing.Point(479, 548);
            this.lb_x15b_1_ld_trans_down.Name = "lb_x15b_1_ld_trans_down";
            this.lb_x15b_1_ld_trans_down.Size = new System.Drawing.Size(207, 13);
            this.lb_x15b_1_ld_trans_down.TabIndex = 183;
            this.lb_x15b_1_ld_trans_down.Text = "[ X15B ] 1 L/D 이재기 DOWN";
            // 
            // pn_Loader_IN3_X15B
            // 
            this.pn_Loader_IN3_X15B.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN3_X15B.Location = new System.Drawing.Point(433, 534);
            this.pn_Loader_IN3_X15B.Name = "pn_Loader_IN3_X15B";
            this.pn_Loader_IN3_X15B.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN3_X15B.TabIndex = 182;
            // 
            // lb_x15a_1_ld_trans_up
            // 
            this.lb_x15a_1_ld_trans_up.AutoSize = true;
            this.lb_x15a_1_ld_trans_up.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x15a_1_ld_trans_up.ForeColor = System.Drawing.Color.White;
            this.lb_x15a_1_ld_trans_up.Location = new System.Drawing.Point(479, 501);
            this.lb_x15a_1_ld_trans_up.Name = "lb_x15a_1_ld_trans_up";
            this.lb_x15a_1_ld_trans_up.Size = new System.Drawing.Size(182, 13);
            this.lb_x15a_1_ld_trans_up.TabIndex = 181;
            this.lb_x15a_1_ld_trans_up.Text = "[ X15A ] 1 L/D 이재기 UP";
            // 
            // pn_Loader_IN3_X15A
            // 
            this.pn_Loader_IN3_X15A.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN3_X15A.Location = new System.Drawing.Point(433, 488);
            this.pn_Loader_IN3_X15A.Name = "pn_Loader_IN3_X15A";
            this.pn_Loader_IN3_X15A.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN3_X15A.TabIndex = 180;
            // 
            // lb_x159_2_ld_cst_lift_tiltsensor_down
            // 
            this.lb_x159_2_ld_cst_lift_tiltsensor_down.AutoSize = true;
            this.lb_x159_2_ld_cst_lift_tiltsensor_down.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x159_2_ld_cst_lift_tiltsensor_down.ForeColor = System.Drawing.Color.White;
            this.lb_x159_2_ld_cst_lift_tiltsensor_down.Location = new System.Drawing.Point(479, 454);
            this.lb_x159_2_ld_cst_lift_tiltsensor_down.Name = "lb_x159_2_ld_cst_lift_tiltsensor_down";
            this.lb_x159_2_ld_cst_lift_tiltsensor_down.Size = new System.Drawing.Size(356, 13);
            this.lb_x159_2_ld_cst_lift_tiltsensor_down.TabIndex = 179;
            this.lb_x159_2_ld_cst_lift_tiltsensor_down.Text = "[ X159 ] 2 L/D Cassette Lift Tilting Sensor DOWN";
            // 
            // pn_Loader_IN3_X159
            // 
            this.pn_Loader_IN3_X159.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN3_X159.Location = new System.Drawing.Point(433, 442);
            this.pn_Loader_IN3_X159.Name = "pn_Loader_IN3_X159";
            this.pn_Loader_IN3_X159.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN3_X159.TabIndex = 178;
            // 
            // lb_x158_2_ld_cst_lift_tiltsensor_up
            // 
            this.lb_x158_2_ld_cst_lift_tiltsensor_up.AutoSize = true;
            this.lb_x158_2_ld_cst_lift_tiltsensor_up.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x158_2_ld_cst_lift_tiltsensor_up.ForeColor = System.Drawing.Color.White;
            this.lb_x158_2_ld_cst_lift_tiltsensor_up.Location = new System.Drawing.Point(479, 409);
            this.lb_x158_2_ld_cst_lift_tiltsensor_up.Name = "lb_x158_2_ld_cst_lift_tiltsensor_up";
            this.lb_x158_2_ld_cst_lift_tiltsensor_up.Size = new System.Drawing.Size(332, 13);
            this.lb_x158_2_ld_cst_lift_tiltsensor_up.TabIndex = 177;
            this.lb_x158_2_ld_cst_lift_tiltsensor_up.Text = "[ X158 ] 2 L/D Cassette Lift Tilting Sensor UP";
            // 
            // pn_Loader_IN3_X158
            // 
            this.pn_Loader_IN3_X158.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN3_X158.Location = new System.Drawing.Point(433, 396);
            this.pn_Loader_IN3_X158.Name = "pn_Loader_IN3_X158";
            this.pn_Loader_IN3_X158.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN3_X158.TabIndex = 176;
            // 
            // lb_x157_2_cst_trans_pressure_sw
            // 
            this.lb_x157_2_cst_trans_pressure_sw.AutoSize = true;
            this.lb_x157_2_cst_trans_pressure_sw.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x157_2_cst_trans_pressure_sw.ForeColor = System.Drawing.Color.White;
            this.lb_x157_2_cst_trans_pressure_sw.Location = new System.Drawing.Point(479, 364);
            this.lb_x157_2_cst_trans_pressure_sw.Name = "lb_x157_2_cst_trans_pressure_sw";
            this.lb_x157_2_cst_trans_pressure_sw.Size = new System.Drawing.Size(327, 13);
            this.lb_x157_2_cst_trans_pressure_sw.TabIndex = 175;
            this.lb_x157_2_cst_trans_pressure_sw.Text = "[ X157 ] 2 Cassette 취출 이재기 Pressure SW";
            // 
            // pn_Loader_IN3_X157
            // 
            this.pn_Loader_IN3_X157.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN3_X157.Location = new System.Drawing.Point(433, 350);
            this.pn_Loader_IN3_X157.Name = "pn_Loader_IN3_X157";
            this.pn_Loader_IN3_X157.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN3_X157.TabIndex = 174;
            // 
            // lb_x156_1_cst_trans_pressure_sw
            // 
            this.lb_x156_1_cst_trans_pressure_sw.AutoSize = true;
            this.lb_x156_1_cst_trans_pressure_sw.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x156_1_cst_trans_pressure_sw.ForeColor = System.Drawing.Color.White;
            this.lb_x156_1_cst_trans_pressure_sw.Location = new System.Drawing.Point(479, 319);
            this.lb_x156_1_cst_trans_pressure_sw.Name = "lb_x156_1_cst_trans_pressure_sw";
            this.lb_x156_1_cst_trans_pressure_sw.Size = new System.Drawing.Size(327, 13);
            this.lb_x156_1_cst_trans_pressure_sw.TabIndex = 173;
            this.lb_x156_1_cst_trans_pressure_sw.Text = "[ X156 ] 1 Cassette 취출 이재기 Pressure SW";
            // 
            // pn_Loader_IN3_X156
            // 
            this.pn_Loader_IN3_X156.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN3_X156.Location = new System.Drawing.Point(433, 304);
            this.pn_Loader_IN3_X156.Name = "pn_Loader_IN3_X156";
            this.pn_Loader_IN3_X156.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN3_X156.TabIndex = 172;
            // 
            // lb_x155_1_ld_cst_lift_tiltsensor_down
            // 
            this.lb_x155_1_ld_cst_lift_tiltsensor_down.AutoSize = true;
            this.lb_x155_1_ld_cst_lift_tiltsensor_down.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x155_1_ld_cst_lift_tiltsensor_down.ForeColor = System.Drawing.Color.White;
            this.lb_x155_1_ld_cst_lift_tiltsensor_down.Location = new System.Drawing.Point(479, 271);
            this.lb_x155_1_ld_cst_lift_tiltsensor_down.Name = "lb_x155_1_ld_cst_lift_tiltsensor_down";
            this.lb_x155_1_ld_cst_lift_tiltsensor_down.Size = new System.Drawing.Size(356, 13);
            this.lb_x155_1_ld_cst_lift_tiltsensor_down.TabIndex = 171;
            this.lb_x155_1_ld_cst_lift_tiltsensor_down.Text = "[ X155 ] 1 L/D Cassette Lift Tilting Sensor DOWN";
            // 
            // pn_Loader_IN3_X155
            // 
            this.pn_Loader_IN3_X155.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN3_X155.Location = new System.Drawing.Point(433, 258);
            this.pn_Loader_IN3_X155.Name = "pn_Loader_IN3_X155";
            this.pn_Loader_IN3_X155.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN3_X155.TabIndex = 170;
            // 
            // lb_x154_1_ld_cst_lift_tiltsensor_up
            // 
            this.lb_x154_1_ld_cst_lift_tiltsensor_up.AutoSize = true;
            this.lb_x154_1_ld_cst_lift_tiltsensor_up.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x154_1_ld_cst_lift_tiltsensor_up.ForeColor = System.Drawing.Color.White;
            this.lb_x154_1_ld_cst_lift_tiltsensor_up.Location = new System.Drawing.Point(479, 226);
            this.lb_x154_1_ld_cst_lift_tiltsensor_up.Name = "lb_x154_1_ld_cst_lift_tiltsensor_up";
            this.lb_x154_1_ld_cst_lift_tiltsensor_up.Size = new System.Drawing.Size(332, 13);
            this.lb_x154_1_ld_cst_lift_tiltsensor_up.TabIndex = 169;
            this.lb_x154_1_ld_cst_lift_tiltsensor_up.Text = "[ X154 ] 1 L/D Cassette Lift Tilting Sensor UP";
            // 
            // pn_Loader_IN3_X154
            // 
            this.pn_Loader_IN3_X154.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN3_X154.Location = new System.Drawing.Point(433, 212);
            this.pn_Loader_IN3_X154.Name = "pn_Loader_IN3_X154";
            this.pn_Loader_IN3_X154.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN3_X154.TabIndex = 168;
            // 
            // lb_x153_2_2_ld_cst_lifter_ungrip_pos
            // 
            this.lb_x153_2_2_ld_cst_lifter_ungrip_pos.AutoSize = true;
            this.lb_x153_2_2_ld_cst_lifter_ungrip_pos.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x153_2_2_ld_cst_lifter_ungrip_pos.ForeColor = System.Drawing.Color.White;
            this.lb_x153_2_2_ld_cst_lifter_ungrip_pos.Location = new System.Drawing.Point(479, 179);
            this.lb_x153_2_2_ld_cst_lifter_ungrip_pos.Name = "lb_x153_2_2_ld_cst_lifter_ungrip_pos";
            this.lb_x153_2_2_ld_cst_lifter_ungrip_pos.Size = new System.Drawing.Size(321, 13);
            this.lb_x153_2_2_ld_cst_lifter_ungrip_pos.TabIndex = 167;
            this.lb_x153_2_2_ld_cst_lifter_ungrip_pos.Text = "[ X153 ] 2-2 L/D Cassette Lifter Ungrip POS";
            // 
            // pn_Loader_IN3_X153
            // 
            this.pn_Loader_IN3_X153.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN3_X153.Location = new System.Drawing.Point(433, 166);
            this.pn_Loader_IN3_X153.Name = "pn_Loader_IN3_X153";
            this.pn_Loader_IN3_X153.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN3_X153.TabIndex = 166;
            // 
            // lb_x152_2_2_ld_cst_lifter_grip_pos
            // 
            this.lb_x152_2_2_ld_cst_lifter_grip_pos.AutoSize = true;
            this.lb_x152_2_2_ld_cst_lifter_grip_pos.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x152_2_2_ld_cst_lifter_grip_pos.ForeColor = System.Drawing.Color.White;
            this.lb_x152_2_2_ld_cst_lifter_grip_pos.Location = new System.Drawing.Point(479, 133);
            this.lb_x152_2_2_ld_cst_lifter_grip_pos.Name = "lb_x152_2_2_ld_cst_lifter_grip_pos";
            this.lb_x152_2_2_ld_cst_lifter_grip_pos.Size = new System.Drawing.Size(304, 13);
            this.lb_x152_2_2_ld_cst_lifter_grip_pos.TabIndex = 165;
            this.lb_x152_2_2_ld_cst_lifter_grip_pos.Text = "[ X152 ] 2-2 L/D Cassette Lifter Grip POS";
            // 
            // pn_Loader_IN3_X152
            // 
            this.pn_Loader_IN3_X152.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN3_X152.Location = new System.Drawing.Point(433, 120);
            this.pn_Loader_IN3_X152.Name = "pn_Loader_IN3_X152";
            this.pn_Loader_IN3_X152.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN3_X152.TabIndex = 164;
            // 
            // lb_x151_2_1_ld_cst_lifter_ungrip_pos
            // 
            this.lb_x151_2_1_ld_cst_lifter_ungrip_pos.AutoSize = true;
            this.lb_x151_2_1_ld_cst_lifter_ungrip_pos.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x151_2_1_ld_cst_lifter_ungrip_pos.ForeColor = System.Drawing.Color.White;
            this.lb_x151_2_1_ld_cst_lifter_ungrip_pos.Location = new System.Drawing.Point(479, 86);
            this.lb_x151_2_1_ld_cst_lifter_ungrip_pos.Name = "lb_x151_2_1_ld_cst_lifter_ungrip_pos";
            this.lb_x151_2_1_ld_cst_lifter_ungrip_pos.Size = new System.Drawing.Size(321, 13);
            this.lb_x151_2_1_ld_cst_lifter_ungrip_pos.TabIndex = 163;
            this.lb_x151_2_1_ld_cst_lifter_ungrip_pos.Text = "[ X151 ] 2-1 L/D Cassette Lifter Ungrip POS";
            // 
            // pn_Loader_IN3_X151
            // 
            this.pn_Loader_IN3_X151.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN3_X151.Location = new System.Drawing.Point(433, 74);
            this.pn_Loader_IN3_X151.Name = "pn_Loader_IN3_X151";
            this.pn_Loader_IN3_X151.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN3_X151.TabIndex = 162;
            // 
            // lb_x150_2_1_ld_cst_lifter_grip_pos
            // 
            this.lb_x150_2_1_ld_cst_lifter_grip_pos.AutoSize = true;
            this.lb_x150_2_1_ld_cst_lifter_grip_pos.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x150_2_1_ld_cst_lifter_grip_pos.ForeColor = System.Drawing.Color.White;
            this.lb_x150_2_1_ld_cst_lifter_grip_pos.Location = new System.Drawing.Point(479, 42);
            this.lb_x150_2_1_ld_cst_lifter_grip_pos.Name = "lb_x150_2_1_ld_cst_lifter_grip_pos";
            this.lb_x150_2_1_ld_cst_lifter_grip_pos.Size = new System.Drawing.Size(304, 13);
            this.lb_x150_2_1_ld_cst_lifter_grip_pos.TabIndex = 161;
            this.lb_x150_2_1_ld_cst_lifter_grip_pos.Text = "[ X150 ] 2-1 L/D Cassette Lifter Grip POS";
            // 
            // pn_Loader_IN3_X150
            // 
            this.pn_Loader_IN3_X150.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN3_X150.Location = new System.Drawing.Point(433, 28);
            this.pn_Loader_IN3_X150.Name = "pn_Loader_IN3_X150";
            this.pn_Loader_IN3_X150.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN3_X150.TabIndex = 160;
            // 
            // lb_x14f_1_2_ld_cst_lifter_ungrip_pos
            // 
            this.lb_x14f_1_2_ld_cst_lifter_ungrip_pos.AutoSize = true;
            this.lb_x14f_1_2_ld_cst_lifter_ungrip_pos.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x14f_1_2_ld_cst_lifter_ungrip_pos.ForeColor = System.Drawing.Color.White;
            this.lb_x14f_1_2_ld_cst_lifter_ungrip_pos.Location = new System.Drawing.Point(52, 730);
            this.lb_x14f_1_2_ld_cst_lifter_ungrip_pos.Name = "lb_x14f_1_2_ld_cst_lifter_ungrip_pos";
            this.lb_x14f_1_2_ld_cst_lifter_ungrip_pos.Size = new System.Drawing.Size(321, 13);
            this.lb_x14f_1_2_ld_cst_lifter_ungrip_pos.TabIndex = 159;
            this.lb_x14f_1_2_ld_cst_lifter_ungrip_pos.Text = "[ X14F ] 1-2 L/D Cassette Lifter Ungrip POS";
            // 
            // pn_Loader_IN3_X14F
            // 
            this.pn_Loader_IN3_X14F.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN3_X14F.Location = new System.Drawing.Point(6, 718);
            this.pn_Loader_IN3_X14F.Name = "pn_Loader_IN3_X14F";
            this.pn_Loader_IN3_X14F.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN3_X14F.TabIndex = 158;
            // 
            // lb_x14e_1_2_ld_cst_lifter_grip_pos
            // 
            this.lb_x14e_1_2_ld_cst_lifter_grip_pos.AutoSize = true;
            this.lb_x14e_1_2_ld_cst_lifter_grip_pos.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x14e_1_2_ld_cst_lifter_grip_pos.ForeColor = System.Drawing.Color.White;
            this.lb_x14e_1_2_ld_cst_lifter_grip_pos.Location = new System.Drawing.Point(52, 685);
            this.lb_x14e_1_2_ld_cst_lifter_grip_pos.Name = "lb_x14e_1_2_ld_cst_lifter_grip_pos";
            this.lb_x14e_1_2_ld_cst_lifter_grip_pos.Size = new System.Drawing.Size(305, 13);
            this.lb_x14e_1_2_ld_cst_lifter_grip_pos.TabIndex = 157;
            this.lb_x14e_1_2_ld_cst_lifter_grip_pos.Text = "[ X14E ] 1-2 L/D Cassette Lifter Grip POS";
            // 
            // pn_Loader_IN3_X14E
            // 
            this.pn_Loader_IN3_X14E.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN3_X14E.Location = new System.Drawing.Point(6, 672);
            this.pn_Loader_IN3_X14E.Name = "pn_Loader_IN3_X14E";
            this.pn_Loader_IN3_X14E.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN3_X14E.TabIndex = 156;
            // 
            // lb_x14d_1_1_ld_cst_lifter_ungrip_pos
            // 
            this.lb_x14d_1_1_ld_cst_lifter_ungrip_pos.AutoSize = true;
            this.lb_x14d_1_1_ld_cst_lifter_ungrip_pos.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x14d_1_1_ld_cst_lifter_ungrip_pos.ForeColor = System.Drawing.Color.White;
            this.lb_x14d_1_1_ld_cst_lifter_ungrip_pos.Location = new System.Drawing.Point(52, 640);
            this.lb_x14d_1_1_ld_cst_lifter_ungrip_pos.Name = "lb_x14d_1_1_ld_cst_lifter_ungrip_pos";
            this.lb_x14d_1_1_ld_cst_lifter_ungrip_pos.Size = new System.Drawing.Size(323, 13);
            this.lb_x14d_1_1_ld_cst_lifter_ungrip_pos.TabIndex = 155;
            this.lb_x14d_1_1_ld_cst_lifter_ungrip_pos.Text = "[ X14D ] 1-1 L/D Cassette Lifter Ungrip POS";
            // 
            // pn_Loader_IN3_X14D
            // 
            this.pn_Loader_IN3_X14D.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN3_X14D.Location = new System.Drawing.Point(6, 626);
            this.pn_Loader_IN3_X14D.Name = "pn_Loader_IN3_X14D";
            this.pn_Loader_IN3_X14D.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN3_X14D.TabIndex = 154;
            // 
            // lb_x14c_1_1_ld_cst_lifter_grip_pos
            // 
            this.lb_x14c_1_1_ld_cst_lifter_grip_pos.AutoSize = true;
            this.lb_x14c_1_1_ld_cst_lifter_grip_pos.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x14c_1_1_ld_cst_lifter_grip_pos.ForeColor = System.Drawing.Color.White;
            this.lb_x14c_1_1_ld_cst_lifter_grip_pos.Location = new System.Drawing.Point(52, 594);
            this.lb_x14c_1_1_ld_cst_lifter_grip_pos.Name = "lb_x14c_1_1_ld_cst_lifter_grip_pos";
            this.lb_x14c_1_1_ld_cst_lifter_grip_pos.Size = new System.Drawing.Size(306, 13);
            this.lb_x14c_1_1_ld_cst_lifter_grip_pos.TabIndex = 153;
            this.lb_x14c_1_1_ld_cst_lifter_grip_pos.Text = "[ X14C ] 1-1 L/D Cassette Lifter Grip POS";
            // 
            // pn_Loader_IN3_X14C
            // 
            this.pn_Loader_IN3_X14C.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN3_X14C.Location = new System.Drawing.Point(6, 580);
            this.pn_Loader_IN3_X14C.Name = "pn_Loader_IN3_X14C";
            this.pn_Loader_IN3_X14C.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN3_X14C.TabIndex = 152;
            // 
            // lb_x14b_4_ld_cst_d_ungrip_pos
            // 
            this.lb_x14b_4_ld_cst_d_ungrip_pos.AutoSize = true;
            this.lb_x14b_4_ld_cst_d_ungrip_pos.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x14b_4_ld_cst_d_ungrip_pos.ForeColor = System.Drawing.Color.White;
            this.lb_x14b_4_ld_cst_d_ungrip_pos.Location = new System.Drawing.Point(52, 548);
            this.lb_x14b_4_ld_cst_d_ungrip_pos.Name = "lb_x14b_4_ld_cst_d_ungrip_pos";
            this.lb_x14b_4_ld_cst_d_ungrip_pos.Size = new System.Drawing.Size(300, 13);
            this.lb_x14b_4_ld_cst_d_ungrip_pos.TabIndex = 151;
            this.lb_x14b_4_ld_cst_d_ungrip_pos.Text = "[ X14B ] 4 L/D Cassette 하부 Ungrip POS";
            // 
            // pn_Loader_IN3_X14B
            // 
            this.pn_Loader_IN3_X14B.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN3_X14B.Location = new System.Drawing.Point(6, 534);
            this.pn_Loader_IN3_X14B.Name = "pn_Loader_IN3_X14B";
            this.pn_Loader_IN3_X14B.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN3_X14B.TabIndex = 150;
            // 
            // lb_x14a_4_ld_cst_d_grip_pos
            // 
            this.lb_x14a_4_ld_cst_d_grip_pos.AutoSize = true;
            this.lb_x14a_4_ld_cst_d_grip_pos.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x14a_4_ld_cst_d_grip_pos.ForeColor = System.Drawing.Color.White;
            this.lb_x14a_4_ld_cst_d_grip_pos.Location = new System.Drawing.Point(52, 501);
            this.lb_x14a_4_ld_cst_d_grip_pos.Name = "lb_x14a_4_ld_cst_d_grip_pos";
            this.lb_x14a_4_ld_cst_d_grip_pos.Size = new System.Drawing.Size(282, 13);
            this.lb_x14a_4_ld_cst_d_grip_pos.TabIndex = 149;
            this.lb_x14a_4_ld_cst_d_grip_pos.Text = "[ X14A ] 4 L/D Cassette 하부 Grip POS";
            // 
            // pn_Loader_IN3_X14A
            // 
            this.pn_Loader_IN3_X14A.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN3_X14A.Location = new System.Drawing.Point(6, 488);
            this.pn_Loader_IN3_X14A.Name = "pn_Loader_IN3_X14A";
            this.pn_Loader_IN3_X14A.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN3_X14A.TabIndex = 148;
            // 
            // lb_x149_4_2_ld_cst_lr_ungrip_pos
            // 
            this.lb_x149_4_2_ld_cst_lr_ungrip_pos.AutoSize = true;
            this.lb_x149_4_2_ld_cst_lr_ungrip_pos.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x149_4_2_ld_cst_lr_ungrip_pos.ForeColor = System.Drawing.Color.White;
            this.lb_x149_4_2_ld_cst_lr_ungrip_pos.Location = new System.Drawing.Point(52, 454);
            this.lb_x149_4_2_ld_cst_lr_ungrip_pos.Name = "lb_x149_4_2_ld_cst_lr_ungrip_pos";
            this.lb_x149_4_2_ld_cst_lr_ungrip_pos.Size = new System.Drawing.Size(314, 13);
            this.lb_x149_4_2_ld_cst_lr_ungrip_pos.TabIndex = 147;
            this.lb_x149_4_2_ld_cst_lr_ungrip_pos.Text = "[ X149 ] 4-2 L/D Cassette 좌우 Ungrip POS";
            // 
            // pn_Loader_IN3_X149
            // 
            this.pn_Loader_IN3_X149.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN3_X149.Location = new System.Drawing.Point(6, 442);
            this.pn_Loader_IN3_X149.Name = "pn_Loader_IN3_X149";
            this.pn_Loader_IN3_X149.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN3_X149.TabIndex = 146;
            // 
            // lb_x148_4_2_ld_cst_lr_grip_pos
            // 
            this.lb_x148_4_2_ld_cst_lr_grip_pos.AutoSize = true;
            this.lb_x148_4_2_ld_cst_lr_grip_pos.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x148_4_2_ld_cst_lr_grip_pos.ForeColor = System.Drawing.Color.White;
            this.lb_x148_4_2_ld_cst_lr_grip_pos.Location = new System.Drawing.Point(52, 409);
            this.lb_x148_4_2_ld_cst_lr_grip_pos.Name = "lb_x148_4_2_ld_cst_lr_grip_pos";
            this.lb_x148_4_2_ld_cst_lr_grip_pos.Size = new System.Drawing.Size(297, 13);
            this.lb_x148_4_2_ld_cst_lr_grip_pos.TabIndex = 145;
            this.lb_x148_4_2_ld_cst_lr_grip_pos.Text = "[ X148 ] 4-2 L/D Cassette 좌우 Grip POS";
            // 
            // pn_Loader_IN3_X148
            // 
            this.pn_Loader_IN3_X148.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN3_X148.Location = new System.Drawing.Point(6, 396);
            this.pn_Loader_IN3_X148.Name = "pn_Loader_IN3_X148";
            this.pn_Loader_IN3_X148.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN3_X148.TabIndex = 144;
            // 
            // lb_x147_4_1_ld_cst_lr_ungrip_pos
            // 
            this.lb_x147_4_1_ld_cst_lr_ungrip_pos.AutoSize = true;
            this.lb_x147_4_1_ld_cst_lr_ungrip_pos.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x147_4_1_ld_cst_lr_ungrip_pos.ForeColor = System.Drawing.Color.White;
            this.lb_x147_4_1_ld_cst_lr_ungrip_pos.Location = new System.Drawing.Point(52, 364);
            this.lb_x147_4_1_ld_cst_lr_ungrip_pos.Name = "lb_x147_4_1_ld_cst_lr_ungrip_pos";
            this.lb_x147_4_1_ld_cst_lr_ungrip_pos.Size = new System.Drawing.Size(314, 13);
            this.lb_x147_4_1_ld_cst_lr_ungrip_pos.TabIndex = 143;
            this.lb_x147_4_1_ld_cst_lr_ungrip_pos.Text = "[ X147 ] 4-1 L/D Cassette 좌우 Ungrip POS";
            // 
            // pn_Loader_IN3_X147
            // 
            this.pn_Loader_IN3_X147.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN3_X147.Location = new System.Drawing.Point(6, 350);
            this.pn_Loader_IN3_X147.Name = "pn_Loader_IN3_X147";
            this.pn_Loader_IN3_X147.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN3_X147.TabIndex = 142;
            // 
            // lb_x146_4_1_ld_cst_lr_grip_pos
            // 
            this.lb_x146_4_1_ld_cst_lr_grip_pos.AutoSize = true;
            this.lb_x146_4_1_ld_cst_lr_grip_pos.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x146_4_1_ld_cst_lr_grip_pos.ForeColor = System.Drawing.Color.White;
            this.lb_x146_4_1_ld_cst_lr_grip_pos.Location = new System.Drawing.Point(52, 319);
            this.lb_x146_4_1_ld_cst_lr_grip_pos.Name = "lb_x146_4_1_ld_cst_lr_grip_pos";
            this.lb_x146_4_1_ld_cst_lr_grip_pos.Size = new System.Drawing.Size(297, 13);
            this.lb_x146_4_1_ld_cst_lr_grip_pos.TabIndex = 141;
            this.lb_x146_4_1_ld_cst_lr_grip_pos.Text = "[ X146 ] 4-1 L/D Cassette 좌우 Grip POS";
            // 
            // pn_Loader_IN3_X146
            // 
            this.pn_Loader_IN3_X146.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN3_X146.Location = new System.Drawing.Point(6, 304);
            this.pn_Loader_IN3_X146.Name = "pn_Loader_IN3_X146";
            this.pn_Loader_IN3_X146.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN3_X146.TabIndex = 140;
            // 
            // lb_x145_3_ld_cst_d_ungrip_pos
            // 
            this.lb_x145_3_ld_cst_d_ungrip_pos.AutoSize = true;
            this.lb_x145_3_ld_cst_d_ungrip_pos.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x145_3_ld_cst_d_ungrip_pos.ForeColor = System.Drawing.Color.White;
            this.lb_x145_3_ld_cst_d_ungrip_pos.Location = new System.Drawing.Point(52, 271);
            this.lb_x145_3_ld_cst_d_ungrip_pos.Name = "lb_x145_3_ld_cst_d_ungrip_pos";
            this.lb_x145_3_ld_cst_d_ungrip_pos.Size = new System.Drawing.Size(298, 13);
            this.lb_x145_3_ld_cst_d_ungrip_pos.TabIndex = 139;
            this.lb_x145_3_ld_cst_d_ungrip_pos.Text = "[ X145 ] 3 L/D Cassette 하부 Ungrip POS";
            // 
            // pn_Loader_IN3_X145
            // 
            this.pn_Loader_IN3_X145.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN3_X145.Location = new System.Drawing.Point(6, 258);
            this.pn_Loader_IN3_X145.Name = "pn_Loader_IN3_X145";
            this.pn_Loader_IN3_X145.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN3_X145.TabIndex = 138;
            // 
            // lb_x144_3_ld_cst_d_grip_pos
            // 
            this.lb_x144_3_ld_cst_d_grip_pos.AutoSize = true;
            this.lb_x144_3_ld_cst_d_grip_pos.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x144_3_ld_cst_d_grip_pos.ForeColor = System.Drawing.Color.White;
            this.lb_x144_3_ld_cst_d_grip_pos.Location = new System.Drawing.Point(52, 226);
            this.lb_x144_3_ld_cst_d_grip_pos.Name = "lb_x144_3_ld_cst_d_grip_pos";
            this.lb_x144_3_ld_cst_d_grip_pos.Size = new System.Drawing.Size(281, 13);
            this.lb_x144_3_ld_cst_d_grip_pos.TabIndex = 137;
            this.lb_x144_3_ld_cst_d_grip_pos.Text = "[ X144 ] 3 L/D Cassette 하부 Grip POS";
            // 
            // pn_Loader_IN3_X144
            // 
            this.pn_Loader_IN3_X144.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN3_X144.Location = new System.Drawing.Point(6, 212);
            this.pn_Loader_IN3_X144.Name = "pn_Loader_IN3_X144";
            this.pn_Loader_IN3_X144.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN3_X144.TabIndex = 136;
            // 
            // lb_x143_3_2_ld_cst_lr_ungrip_pos
            // 
            this.lb_x143_3_2_ld_cst_lr_ungrip_pos.AutoSize = true;
            this.lb_x143_3_2_ld_cst_lr_ungrip_pos.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x143_3_2_ld_cst_lr_ungrip_pos.ForeColor = System.Drawing.Color.White;
            this.lb_x143_3_2_ld_cst_lr_ungrip_pos.Location = new System.Drawing.Point(52, 179);
            this.lb_x143_3_2_ld_cst_lr_ungrip_pos.Name = "lb_x143_3_2_ld_cst_lr_ungrip_pos";
            this.lb_x143_3_2_ld_cst_lr_ungrip_pos.Size = new System.Drawing.Size(311, 13);
            this.lb_x143_3_2_ld_cst_lr_ungrip_pos.TabIndex = 135;
            this.lb_x143_3_2_ld_cst_lr_ungrip_pos.Text = "[ X143 ] 3-2 L/D Cassette 좌우 Ungrip Pos";
            // 
            // pn_Loader_IN3_X143
            // 
            this.pn_Loader_IN3_X143.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN3_X143.Location = new System.Drawing.Point(6, 166);
            this.pn_Loader_IN3_X143.Name = "pn_Loader_IN3_X143";
            this.pn_Loader_IN3_X143.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN3_X143.TabIndex = 134;
            // 
            // lb_x142_3_2_ld_cst_lr_grip_pos
            // 
            this.lb_x142_3_2_ld_cst_lr_grip_pos.AutoSize = true;
            this.lb_x142_3_2_ld_cst_lr_grip_pos.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x142_3_2_ld_cst_lr_grip_pos.ForeColor = System.Drawing.Color.White;
            this.lb_x142_3_2_ld_cst_lr_grip_pos.Location = new System.Drawing.Point(52, 133);
            this.lb_x142_3_2_ld_cst_lr_grip_pos.Name = "lb_x142_3_2_ld_cst_lr_grip_pos";
            this.lb_x142_3_2_ld_cst_lr_grip_pos.Size = new System.Drawing.Size(294, 13);
            this.lb_x142_3_2_ld_cst_lr_grip_pos.TabIndex = 133;
            this.lb_x142_3_2_ld_cst_lr_grip_pos.Text = "[ X142 ] 3-2 L/D Cassette 좌우 Grip Pos";
            // 
            // pn_Loader_IN3_X142
            // 
            this.pn_Loader_IN3_X142.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN3_X142.Location = new System.Drawing.Point(6, 120);
            this.pn_Loader_IN3_X142.Name = "pn_Loader_IN3_X142";
            this.pn_Loader_IN3_X142.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN3_X142.TabIndex = 132;
            // 
            // lb_x141_3_1_ld_cst_lr_ungrip_pos
            // 
            this.lb_x141_3_1_ld_cst_lr_ungrip_pos.AutoSize = true;
            this.lb_x141_3_1_ld_cst_lr_ungrip_pos.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x141_3_1_ld_cst_lr_ungrip_pos.ForeColor = System.Drawing.Color.White;
            this.lb_x141_3_1_ld_cst_lr_ungrip_pos.Location = new System.Drawing.Point(52, 86);
            this.lb_x141_3_1_ld_cst_lr_ungrip_pos.Name = "lb_x141_3_1_ld_cst_lr_ungrip_pos";
            this.lb_x141_3_1_ld_cst_lr_ungrip_pos.Size = new System.Drawing.Size(314, 13);
            this.lb_x141_3_1_ld_cst_lr_ungrip_pos.TabIndex = 131;
            this.lb_x141_3_1_ld_cst_lr_ungrip_pos.Text = "[ X141 ] 3-1 L/D Cassette 좌우 Ungrip POS";
            // 
            // pn_Loader_IN3_X141
            // 
            this.pn_Loader_IN3_X141.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN3_X141.Location = new System.Drawing.Point(6, 74);
            this.pn_Loader_IN3_X141.Name = "pn_Loader_IN3_X141";
            this.pn_Loader_IN3_X141.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN3_X141.TabIndex = 130;
            // 
            // lb_x140_3_1_ld_cst_lr_grip_pos
            // 
            this.lb_x140_3_1_ld_cst_lr_grip_pos.AutoSize = true;
            this.lb_x140_3_1_ld_cst_lr_grip_pos.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x140_3_1_ld_cst_lr_grip_pos.ForeColor = System.Drawing.Color.White;
            this.lb_x140_3_1_ld_cst_lr_grip_pos.Location = new System.Drawing.Point(52, 42);
            this.lb_x140_3_1_ld_cst_lr_grip_pos.Name = "lb_x140_3_1_ld_cst_lr_grip_pos";
            this.lb_x140_3_1_ld_cst_lr_grip_pos.Size = new System.Drawing.Size(297, 13);
            this.lb_x140_3_1_ld_cst_lr_grip_pos.TabIndex = 129;
            this.lb_x140_3_1_ld_cst_lr_grip_pos.Text = "[ X140 ] 3-1 L/D Cassette 좌우 Grip POS";
            // 
            // pn_Loader_IN3_X140
            // 
            this.pn_Loader_IN3_X140.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN3_X140.Location = new System.Drawing.Point(6, 28);
            this.pn_Loader_IN3_X140.Name = "pn_Loader_IN3_X140";
            this.pn_Loader_IN3_X140.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN3_X140.TabIndex = 128;
            // 
            // tp_iostatus_ld_in4
            // 
            this.tp_iostatus_ld_in4.BackColor = System.Drawing.Color.DimGray;
            this.tp_iostatus_ld_in4.Controls.Add(this.lb_x17f_5_process_fan_stop_alarm);
            this.tp_iostatus_ld_in4.Controls.Add(this.lb_x16f_1_ld_light_curtain);
            this.tp_iostatus_ld_in4.Controls.Add(this.pn_Loader_IN4_X17F);
            this.tp_iostatus_ld_in4.Controls.Add(this.lb_x17e_4_process_fan_stop_alarm);
            this.tp_iostatus_ld_in4.Controls.Add(this.pn_Loader_IN4_X17E);
            this.tp_iostatus_ld_in4.Controls.Add(this.lb_x17d_3_process_fan_stop_alarm);
            this.tp_iostatus_ld_in4.Controls.Add(this.pn_Loader_IN4_X17D);
            this.tp_iostatus_ld_in4.Controls.Add(this.lb_x17c_2_process_fan_stop_alarm);
            this.tp_iostatus_ld_in4.Controls.Add(this.pn_Loader_IN4_X17C);
            this.tp_iostatus_ld_in4.Controls.Add(this.lb_x17b_1_process_fan_stop_alarm);
            this.tp_iostatus_ld_in4.Controls.Add(this.pn_Loader_IN4_X17B);
            this.tp_iostatus_ld_in4.Controls.Add(this.lb_x17a_laserhead_blow);
            this.tp_iostatus_ld_in4.Controls.Add(this.pn_Loader_IN4_X17A);
            this.tp_iostatus_ld_in4.Controls.Add(this.lb_x179_ld_exhaust_fan_run);
            this.tp_iostatus_ld_in4.Controls.Add(this.pn_Loader_IN4_X179);
            this.tp_iostatus_ld_in4.Controls.Add(this.lb_x178_ld_inspiration_fan_run);
            this.tp_iostatus_ld_in4.Controls.Add(this.pn_Loader_IN4_X178);
            this.tp_iostatus_ld_in4.Controls.Add(this.lb_x177_6_ld_fan_stop_alarm);
            this.tp_iostatus_ld_in4.Controls.Add(this.pn_Loader_IN4_X177);
            this.tp_iostatus_ld_in4.Controls.Add(this.lb_x176_5_ld_fan_stop_alarm);
            this.tp_iostatus_ld_in4.Controls.Add(this.pn_Loader_IN4_X176);
            this.tp_iostatus_ld_in4.Controls.Add(this.lb_x175_4_ld_fan_stop_alarm);
            this.tp_iostatus_ld_in4.Controls.Add(this.pn_Loader_IN4_X175);
            this.tp_iostatus_ld_in4.Controls.Add(this.lb_x174_3_ld_fan_stop_alarm);
            this.tp_iostatus_ld_in4.Controls.Add(this.pn_Loader_IN4_X174);
            this.tp_iostatus_ld_in4.Controls.Add(this.lb_x173_2_ld_fan_stop_alarm);
            this.tp_iostatus_ld_in4.Controls.Add(this.pn_Loader_IN4_X173);
            this.tp_iostatus_ld_in4.Controls.Add(this.lb_x172_1_ld_fan_stop_alarm);
            this.tp_iostatus_ld_in4.Controls.Add(this.pn_Loader_IN4_X172);
            this.tp_iostatus_ld_in4.Controls.Add(this.lb_x171_motion_off_timer);
            this.tp_iostatus_ld_in4.Controls.Add(this.pn_Loader_IN4_X171);
            this.tp_iostatus_ld_in4.Controls.Add(this.lb_x170_2_ld_light_curtain);
            this.tp_iostatus_ld_in4.Controls.Add(this.pn_Loader_IN4_X170);
            this.tp_iostatus_ld_in4.Controls.Add(this.pn_Loader_IN4_X16F);
            this.tp_iostatus_ld_in4.Controls.Add(this.lb_x16e_spare_cylinder_sensor);
            this.tp_iostatus_ld_in4.Controls.Add(this.pn_Loader_IN4_X16E);
            this.tp_iostatus_ld_in4.Controls.Add(this.lb_x16d_spare_cylinder_sensor);
            this.tp_iostatus_ld_in4.Controls.Add(this.pn_Loader_IN4_X16D);
            this.tp_iostatus_ld_in4.Controls.Add(this.lb_x16c_spare_cylinder_sensor);
            this.tp_iostatus_ld_in4.Controls.Add(this.pn_Loader_IN4_X16C);
            this.tp_iostatus_ld_in4.Controls.Add(this.lb_x16b_spare_cylinder_sensor);
            this.tp_iostatus_ld_in4.Controls.Add(this.pn_Loader_IN4_X16B);
            this.tp_iostatus_ld_in4.Controls.Add(this.lb_x16a_spare_cylinder_sensor);
            this.tp_iostatus_ld_in4.Controls.Add(this.pn_Loader_IN4_X16A);
            this.tp_iostatus_ld_in4.Controls.Add(this.lb_x169_spare_cylinder_sensor);
            this.tp_iostatus_ld_in4.Controls.Add(this.pn_Loader_IN4_X169);
            this.tp_iostatus_ld_in4.Controls.Add(this.lb_x168_spare_cylinder_sensor);
            this.tp_iostatus_ld_in4.Controls.Add(this.pn_Loader_IN4_X168);
            this.tp_iostatus_ld_in4.Controls.Add(this.lb_x167_spare_cylinder_sensor);
            this.tp_iostatus_ld_in4.Controls.Add(this.pn_Loader_IN4_X167);
            this.tp_iostatus_ld_in4.Controls.Add(this.lb_x166_3_main_pressure_sw);
            this.tp_iostatus_ld_in4.Controls.Add(this.pn_Loader_IN4_X166);
            this.tp_iostatus_ld_in4.Controls.Add(this.lb_x165_2_main_pressure_sw);
            this.tp_iostatus_ld_in4.Controls.Add(this.pn_Loader_IN4_X165);
            this.tp_iostatus_ld_in4.Controls.Add(this.lb_x164_1_main_pressure_sw);
            this.tp_iostatus_ld_in4.Controls.Add(this.pn_Loader_IN4_X164);
            this.tp_iostatus_ld_in4.Controls.Add(this.lb_x163_2_ld_trans_2ch_pressure_sw);
            this.tp_iostatus_ld_in4.Controls.Add(this.pn_Loader_IN4_X163);
            this.tp_iostatus_ld_in4.Controls.Add(this.lb_x162_2_ld_trans_1ch_pressure_sw);
            this.tp_iostatus_ld_in4.Controls.Add(this.pn_Loader_IN4_X162);
            this.tp_iostatus_ld_in4.Controls.Add(this.lb_x161_1_ld_trans_2ch_pressure_sw);
            this.tp_iostatus_ld_in4.Controls.Add(this.pn_Loader_IN4_X161);
            this.tp_iostatus_ld_in4.Controls.Add(this.lb_x160_1_ld_trans_1ch_pressure_sw);
            this.tp_iostatus_ld_in4.Controls.Add(this.pn_Loader_IN4_X160);
            this.tp_iostatus_ld_in4.Location = new System.Drawing.Point(4, 34);
            this.tp_iostatus_ld_in4.Name = "tp_iostatus_ld_in4";
            this.tp_iostatus_ld_in4.Padding = new System.Windows.Forms.Padding(3);
            this.tp_iostatus_ld_in4.Size = new System.Drawing.Size(846, 778);
            this.tp_iostatus_ld_in4.TabIndex = 3;
            this.tp_iostatus_ld_in4.Text = "IN - 4";
            // 
            // lb_x17f_5_process_fan_stop_alarm
            // 
            this.lb_x17f_5_process_fan_stop_alarm.AutoSize = true;
            this.lb_x17f_5_process_fan_stop_alarm.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x17f_5_process_fan_stop_alarm.ForeColor = System.Drawing.Color.White;
            this.lb_x17f_5_process_fan_stop_alarm.Location = new System.Drawing.Point(479, 731);
            this.lb_x17f_5_process_fan_stop_alarm.Name = "lb_x17f_5_process_fan_stop_alarm";
            this.lb_x17f_5_process_fan_stop_alarm.Size = new System.Drawing.Size(240, 13);
            this.lb_x17f_5_process_fan_stop_alarm.TabIndex = 254;
            this.lb_x17f_5_process_fan_stop_alarm.Text = "[ X17F ] 5 가공부 FAN Stop Alarm";
            // 
            // lb_x16f_1_ld_light_curtain
            // 
            this.lb_x16f_1_ld_light_curtain.AutoSize = true;
            this.lb_x16f_1_ld_light_curtain.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x16f_1_ld_light_curtain.ForeColor = System.Drawing.Color.White;
            this.lb_x16f_1_ld_light_curtain.Location = new System.Drawing.Point(52, 731);
            this.lb_x16f_1_ld_light_curtain.Name = "lb_x16f_1_ld_light_curtain";
            this.lb_x16f_1_ld_light_curtain.Size = new System.Drawing.Size(237, 13);
            this.lb_x16f_1_ld_light_curtain.TabIndex = 253;
            this.lb_x16f_1_ld_light_curtain.Text = "[ X16F ] 1 L/D Light Curtain 감지";
            // 
            // pn_Loader_IN4_X17F
            // 
            this.pn_Loader_IN4_X17F.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN4_X17F.Location = new System.Drawing.Point(433, 718);
            this.pn_Loader_IN4_X17F.Name = "pn_Loader_IN4_X17F";
            this.pn_Loader_IN4_X17F.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN4_X17F.TabIndex = 252;
            // 
            // lb_x17e_4_process_fan_stop_alarm
            // 
            this.lb_x17e_4_process_fan_stop_alarm.AutoSize = true;
            this.lb_x17e_4_process_fan_stop_alarm.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x17e_4_process_fan_stop_alarm.ForeColor = System.Drawing.Color.White;
            this.lb_x17e_4_process_fan_stop_alarm.Location = new System.Drawing.Point(479, 685);
            this.lb_x17e_4_process_fan_stop_alarm.Name = "lb_x17e_4_process_fan_stop_alarm";
            this.lb_x17e_4_process_fan_stop_alarm.Size = new System.Drawing.Size(241, 13);
            this.lb_x17e_4_process_fan_stop_alarm.TabIndex = 251;
            this.lb_x17e_4_process_fan_stop_alarm.Text = "[ X17E ] 4 가공부 FAN Stop Alarm";
            // 
            // pn_Loader_IN4_X17E
            // 
            this.pn_Loader_IN4_X17E.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN4_X17E.Location = new System.Drawing.Point(433, 672);
            this.pn_Loader_IN4_X17E.Name = "pn_Loader_IN4_X17E";
            this.pn_Loader_IN4_X17E.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN4_X17E.TabIndex = 250;
            // 
            // lb_x17d_3_process_fan_stop_alarm
            // 
            this.lb_x17d_3_process_fan_stop_alarm.AutoSize = true;
            this.lb_x17d_3_process_fan_stop_alarm.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x17d_3_process_fan_stop_alarm.ForeColor = System.Drawing.Color.White;
            this.lb_x17d_3_process_fan_stop_alarm.Location = new System.Drawing.Point(479, 640);
            this.lb_x17d_3_process_fan_stop_alarm.Name = "lb_x17d_3_process_fan_stop_alarm";
            this.lb_x17d_3_process_fan_stop_alarm.Size = new System.Drawing.Size(242, 13);
            this.lb_x17d_3_process_fan_stop_alarm.TabIndex = 249;
            this.lb_x17d_3_process_fan_stop_alarm.Text = "[ X17D ] 3 가공부 FAN Stop Alarm";
            // 
            // pn_Loader_IN4_X17D
            // 
            this.pn_Loader_IN4_X17D.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN4_X17D.Location = new System.Drawing.Point(433, 626);
            this.pn_Loader_IN4_X17D.Name = "pn_Loader_IN4_X17D";
            this.pn_Loader_IN4_X17D.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN4_X17D.TabIndex = 248;
            // 
            // lb_x17c_2_process_fan_stop_alarm
            // 
            this.lb_x17c_2_process_fan_stop_alarm.AutoSize = true;
            this.lb_x17c_2_process_fan_stop_alarm.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x17c_2_process_fan_stop_alarm.ForeColor = System.Drawing.Color.White;
            this.lb_x17c_2_process_fan_stop_alarm.Location = new System.Drawing.Point(479, 594);
            this.lb_x17c_2_process_fan_stop_alarm.Name = "lb_x17c_2_process_fan_stop_alarm";
            this.lb_x17c_2_process_fan_stop_alarm.Size = new System.Drawing.Size(242, 13);
            this.lb_x17c_2_process_fan_stop_alarm.TabIndex = 247;
            this.lb_x17c_2_process_fan_stop_alarm.Text = "[ X17C ] 2 가공부 FAN Stop Alarm";
            // 
            // pn_Loader_IN4_X17C
            // 
            this.pn_Loader_IN4_X17C.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN4_X17C.Location = new System.Drawing.Point(433, 580);
            this.pn_Loader_IN4_X17C.Name = "pn_Loader_IN4_X17C";
            this.pn_Loader_IN4_X17C.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN4_X17C.TabIndex = 246;
            // 
            // lb_x17b_1_process_fan_stop_alarm
            // 
            this.lb_x17b_1_process_fan_stop_alarm.AutoSize = true;
            this.lb_x17b_1_process_fan_stop_alarm.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x17b_1_process_fan_stop_alarm.ForeColor = System.Drawing.Color.White;
            this.lb_x17b_1_process_fan_stop_alarm.Location = new System.Drawing.Point(479, 548);
            this.lb_x17b_1_process_fan_stop_alarm.Name = "lb_x17b_1_process_fan_stop_alarm";
            this.lb_x17b_1_process_fan_stop_alarm.Size = new System.Drawing.Size(242, 13);
            this.lb_x17b_1_process_fan_stop_alarm.TabIndex = 245;
            this.lb_x17b_1_process_fan_stop_alarm.Text = "[ X17B ] 1 가공부 FAN Stop Alarm";
            // 
            // pn_Loader_IN4_X17B
            // 
            this.pn_Loader_IN4_X17B.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN4_X17B.Location = new System.Drawing.Point(433, 534);
            this.pn_Loader_IN4_X17B.Name = "pn_Loader_IN4_X17B";
            this.pn_Loader_IN4_X17B.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN4_X17B.TabIndex = 244;
            // 
            // lb_x17a_laserhead_blow
            // 
            this.lb_x17a_laserhead_blow.AutoSize = true;
            this.lb_x17a_laserhead_blow.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x17a_laserhead_blow.ForeColor = System.Drawing.Color.White;
            this.lb_x17a_laserhead_blow.Location = new System.Drawing.Point(479, 501);
            this.lb_x17a_laserhead_blow.Name = "lb_x17a_laserhead_blow";
            this.lb_x17a_laserhead_blow.Size = new System.Drawing.Size(189, 13);
            this.lb_x17a_laserhead_blow.TabIndex = 243;
            this.lb_x17a_laserhead_blow.Text = "[ X17A ] LaserHead Blow";
            // 
            // pn_Loader_IN4_X17A
            // 
            this.pn_Loader_IN4_X17A.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN4_X17A.Location = new System.Drawing.Point(433, 488);
            this.pn_Loader_IN4_X17A.Name = "pn_Loader_IN4_X17A";
            this.pn_Loader_IN4_X17A.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN4_X17A.TabIndex = 242;
            // 
            // lb_x179_ld_exhaust_fan_run
            // 
            this.lb_x179_ld_exhaust_fan_run.AutoSize = true;
            this.lb_x179_ld_exhaust_fan_run.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x179_ld_exhaust_fan_run.ForeColor = System.Drawing.Color.White;
            this.lb_x179_ld_exhaust_fan_run.Location = new System.Drawing.Point(479, 454);
            this.lb_x179_ld_exhaust_fan_run.Name = "lb_x179_ld_exhaust_fan_run";
            this.lb_x179_ld_exhaust_fan_run.Size = new System.Drawing.Size(194, 13);
            this.lb_x179_ld_exhaust_fan_run.TabIndex = 241;
            this.lb_x179_ld_exhaust_fan_run.Text = "[ X179 ] L/D 배기 FAN 구동";
            // 
            // pn_Loader_IN4_X179
            // 
            this.pn_Loader_IN4_X179.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN4_X179.Location = new System.Drawing.Point(433, 442);
            this.pn_Loader_IN4_X179.Name = "pn_Loader_IN4_X179";
            this.pn_Loader_IN4_X179.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN4_X179.TabIndex = 240;
            // 
            // lb_x178_ld_inspiration_fan_run
            // 
            this.lb_x178_ld_inspiration_fan_run.AutoSize = true;
            this.lb_x178_ld_inspiration_fan_run.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x178_ld_inspiration_fan_run.ForeColor = System.Drawing.Color.White;
            this.lb_x178_ld_inspiration_fan_run.Location = new System.Drawing.Point(479, 409);
            this.lb_x178_ld_inspiration_fan_run.Name = "lb_x178_ld_inspiration_fan_run";
            this.lb_x178_ld_inspiration_fan_run.Size = new System.Drawing.Size(194, 13);
            this.lb_x178_ld_inspiration_fan_run.TabIndex = 239;
            this.lb_x178_ld_inspiration_fan_run.Text = "[ X178 ] L/D 흡기 FAN 구동";
            // 
            // pn_Loader_IN4_X178
            // 
            this.pn_Loader_IN4_X178.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN4_X178.Location = new System.Drawing.Point(433, 396);
            this.pn_Loader_IN4_X178.Name = "pn_Loader_IN4_X178";
            this.pn_Loader_IN4_X178.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN4_X178.TabIndex = 238;
            // 
            // lb_x177_6_ld_fan_stop_alarm
            // 
            this.lb_x177_6_ld_fan_stop_alarm.AutoSize = true;
            this.lb_x177_6_ld_fan_stop_alarm.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x177_6_ld_fan_stop_alarm.ForeColor = System.Drawing.Color.White;
            this.lb_x177_6_ld_fan_stop_alarm.Location = new System.Drawing.Point(479, 364);
            this.lb_x177_6_ld_fan_stop_alarm.Name = "lb_x177_6_ld_fan_stop_alarm";
            this.lb_x177_6_ld_fan_stop_alarm.Size = new System.Drawing.Size(223, 13);
            this.lb_x177_6_ld_fan_stop_alarm.TabIndex = 237;
            this.lb_x177_6_ld_fan_stop_alarm.Text = "[ X177 ] 6 L/D FAN Stop Alarm";
            // 
            // pn_Loader_IN4_X177
            // 
            this.pn_Loader_IN4_X177.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN4_X177.Location = new System.Drawing.Point(433, 350);
            this.pn_Loader_IN4_X177.Name = "pn_Loader_IN4_X177";
            this.pn_Loader_IN4_X177.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN4_X177.TabIndex = 236;
            // 
            // lb_x176_5_ld_fan_stop_alarm
            // 
            this.lb_x176_5_ld_fan_stop_alarm.AutoSize = true;
            this.lb_x176_5_ld_fan_stop_alarm.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x176_5_ld_fan_stop_alarm.ForeColor = System.Drawing.Color.White;
            this.lb_x176_5_ld_fan_stop_alarm.Location = new System.Drawing.Point(479, 319);
            this.lb_x176_5_ld_fan_stop_alarm.Name = "lb_x176_5_ld_fan_stop_alarm";
            this.lb_x176_5_ld_fan_stop_alarm.Size = new System.Drawing.Size(223, 13);
            this.lb_x176_5_ld_fan_stop_alarm.TabIndex = 235;
            this.lb_x176_5_ld_fan_stop_alarm.Text = "[ X176 ] 5 L/D FAN Stop Alarm";
            // 
            // pn_Loader_IN4_X176
            // 
            this.pn_Loader_IN4_X176.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN4_X176.Location = new System.Drawing.Point(433, 304);
            this.pn_Loader_IN4_X176.Name = "pn_Loader_IN4_X176";
            this.pn_Loader_IN4_X176.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN4_X176.TabIndex = 234;
            // 
            // lb_x175_4_ld_fan_stop_alarm
            // 
            this.lb_x175_4_ld_fan_stop_alarm.AutoSize = true;
            this.lb_x175_4_ld_fan_stop_alarm.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x175_4_ld_fan_stop_alarm.ForeColor = System.Drawing.Color.White;
            this.lb_x175_4_ld_fan_stop_alarm.Location = new System.Drawing.Point(479, 271);
            this.lb_x175_4_ld_fan_stop_alarm.Name = "lb_x175_4_ld_fan_stop_alarm";
            this.lb_x175_4_ld_fan_stop_alarm.Size = new System.Drawing.Size(223, 13);
            this.lb_x175_4_ld_fan_stop_alarm.TabIndex = 233;
            this.lb_x175_4_ld_fan_stop_alarm.Text = "[ X175 ] 4 L/D FAN Stop Alarm";
            // 
            // pn_Loader_IN4_X175
            // 
            this.pn_Loader_IN4_X175.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN4_X175.Location = new System.Drawing.Point(433, 258);
            this.pn_Loader_IN4_X175.Name = "pn_Loader_IN4_X175";
            this.pn_Loader_IN4_X175.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN4_X175.TabIndex = 232;
            // 
            // lb_x174_3_ld_fan_stop_alarm
            // 
            this.lb_x174_3_ld_fan_stop_alarm.AutoSize = true;
            this.lb_x174_3_ld_fan_stop_alarm.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x174_3_ld_fan_stop_alarm.ForeColor = System.Drawing.Color.White;
            this.lb_x174_3_ld_fan_stop_alarm.Location = new System.Drawing.Point(479, 226);
            this.lb_x174_3_ld_fan_stop_alarm.Name = "lb_x174_3_ld_fan_stop_alarm";
            this.lb_x174_3_ld_fan_stop_alarm.Size = new System.Drawing.Size(223, 13);
            this.lb_x174_3_ld_fan_stop_alarm.TabIndex = 231;
            this.lb_x174_3_ld_fan_stop_alarm.Text = "[ X174 ] 3 L/D FAN Stop Alarm";
            // 
            // pn_Loader_IN4_X174
            // 
            this.pn_Loader_IN4_X174.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN4_X174.Location = new System.Drawing.Point(433, 212);
            this.pn_Loader_IN4_X174.Name = "pn_Loader_IN4_X174";
            this.pn_Loader_IN4_X174.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN4_X174.TabIndex = 230;
            // 
            // lb_x173_2_ld_fan_stop_alarm
            // 
            this.lb_x173_2_ld_fan_stop_alarm.AutoSize = true;
            this.lb_x173_2_ld_fan_stop_alarm.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x173_2_ld_fan_stop_alarm.ForeColor = System.Drawing.Color.White;
            this.lb_x173_2_ld_fan_stop_alarm.Location = new System.Drawing.Point(479, 179);
            this.lb_x173_2_ld_fan_stop_alarm.Name = "lb_x173_2_ld_fan_stop_alarm";
            this.lb_x173_2_ld_fan_stop_alarm.Size = new System.Drawing.Size(223, 13);
            this.lb_x173_2_ld_fan_stop_alarm.TabIndex = 229;
            this.lb_x173_2_ld_fan_stop_alarm.Text = "[ X173 ] 2 L/D FAN Stop Alarm";
            // 
            // pn_Loader_IN4_X173
            // 
            this.pn_Loader_IN4_X173.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN4_X173.Location = new System.Drawing.Point(433, 166);
            this.pn_Loader_IN4_X173.Name = "pn_Loader_IN4_X173";
            this.pn_Loader_IN4_X173.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN4_X173.TabIndex = 228;
            // 
            // lb_x172_1_ld_fan_stop_alarm
            // 
            this.lb_x172_1_ld_fan_stop_alarm.AutoSize = true;
            this.lb_x172_1_ld_fan_stop_alarm.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x172_1_ld_fan_stop_alarm.ForeColor = System.Drawing.Color.White;
            this.lb_x172_1_ld_fan_stop_alarm.Location = new System.Drawing.Point(479, 133);
            this.lb_x172_1_ld_fan_stop_alarm.Name = "lb_x172_1_ld_fan_stop_alarm";
            this.lb_x172_1_ld_fan_stop_alarm.Size = new System.Drawing.Size(223, 13);
            this.lb_x172_1_ld_fan_stop_alarm.TabIndex = 227;
            this.lb_x172_1_ld_fan_stop_alarm.Text = "[ X172 ] 1 L/D FAN Stop Alarm";
            // 
            // pn_Loader_IN4_X172
            // 
            this.pn_Loader_IN4_X172.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN4_X172.Location = new System.Drawing.Point(433, 120);
            this.pn_Loader_IN4_X172.Name = "pn_Loader_IN4_X172";
            this.pn_Loader_IN4_X172.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN4_X172.TabIndex = 226;
            // 
            // lb_x171_motion_off_timer
            // 
            this.lb_x171_motion_off_timer.AutoSize = true;
            this.lb_x171_motion_off_timer.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x171_motion_off_timer.ForeColor = System.Drawing.Color.White;
            this.lb_x171_motion_off_timer.Location = new System.Drawing.Point(479, 86);
            this.lb_x171_motion_off_timer.Name = "lb_x171_motion_off_timer";
            this.lb_x171_motion_off_timer.Size = new System.Drawing.Size(187, 13);
            this.lb_x171_motion_off_timer.TabIndex = 225;
            this.lb_x171_motion_off_timer.Text = "[ X171 ] Motion Off Timer";
            // 
            // pn_Loader_IN4_X171
            // 
            this.pn_Loader_IN4_X171.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN4_X171.Location = new System.Drawing.Point(433, 74);
            this.pn_Loader_IN4_X171.Name = "pn_Loader_IN4_X171";
            this.pn_Loader_IN4_X171.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN4_X171.TabIndex = 224;
            // 
            // lb_x170_2_ld_light_curtain
            // 
            this.lb_x170_2_ld_light_curtain.AutoSize = true;
            this.lb_x170_2_ld_light_curtain.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x170_2_ld_light_curtain.ForeColor = System.Drawing.Color.White;
            this.lb_x170_2_ld_light_curtain.Location = new System.Drawing.Point(479, 42);
            this.lb_x170_2_ld_light_curtain.Name = "lb_x170_2_ld_light_curtain";
            this.lb_x170_2_ld_light_curtain.Size = new System.Drawing.Size(237, 13);
            this.lb_x170_2_ld_light_curtain.TabIndex = 223;
            this.lb_x170_2_ld_light_curtain.Text = "[ X170 ] 2 L/D Light Curtain 감지";
            // 
            // pn_Loader_IN4_X170
            // 
            this.pn_Loader_IN4_X170.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN4_X170.Location = new System.Drawing.Point(433, 28);
            this.pn_Loader_IN4_X170.Name = "pn_Loader_IN4_X170";
            this.pn_Loader_IN4_X170.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN4_X170.TabIndex = 222;
            // 
            // pn_Loader_IN4_X16F
            // 
            this.pn_Loader_IN4_X16F.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN4_X16F.Location = new System.Drawing.Point(6, 718);
            this.pn_Loader_IN4_X16F.Name = "pn_Loader_IN4_X16F";
            this.pn_Loader_IN4_X16F.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN4_X16F.TabIndex = 221;
            // 
            // lb_x16e_spare_cylinder_sensor
            // 
            this.lb_x16e_spare_cylinder_sensor.AutoSize = true;
            this.lb_x16e_spare_cylinder_sensor.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x16e_spare_cylinder_sensor.ForeColor = System.Drawing.Color.White;
            this.lb_x16e_spare_cylinder_sensor.Location = new System.Drawing.Point(52, 685);
            this.lb_x16e_spare_cylinder_sensor.Name = "lb_x16e_spare_cylinder_sensor";
            this.lb_x16e_spare_cylinder_sensor.Size = new System.Drawing.Size(236, 13);
            this.lb_x16e_spare_cylinder_sensor.TabIndex = 220;
            this.lb_x16e_spare_cylinder_sensor.Text = "[ X16E ] Spare Cylinder Sensor";
            // 
            // pn_Loader_IN4_X16E
            // 
            this.pn_Loader_IN4_X16E.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN4_X16E.Location = new System.Drawing.Point(6, 672);
            this.pn_Loader_IN4_X16E.Name = "pn_Loader_IN4_X16E";
            this.pn_Loader_IN4_X16E.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN4_X16E.TabIndex = 219;
            // 
            // lb_x16d_spare_cylinder_sensor
            // 
            this.lb_x16d_spare_cylinder_sensor.AutoSize = true;
            this.lb_x16d_spare_cylinder_sensor.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x16d_spare_cylinder_sensor.ForeColor = System.Drawing.Color.White;
            this.lb_x16d_spare_cylinder_sensor.Location = new System.Drawing.Point(52, 640);
            this.lb_x16d_spare_cylinder_sensor.Name = "lb_x16d_spare_cylinder_sensor";
            this.lb_x16d_spare_cylinder_sensor.Size = new System.Drawing.Size(237, 13);
            this.lb_x16d_spare_cylinder_sensor.TabIndex = 218;
            this.lb_x16d_spare_cylinder_sensor.Text = "[ X16D ] Spare Cylinder Sensor";
            // 
            // pn_Loader_IN4_X16D
            // 
            this.pn_Loader_IN4_X16D.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN4_X16D.Location = new System.Drawing.Point(6, 626);
            this.pn_Loader_IN4_X16D.Name = "pn_Loader_IN4_X16D";
            this.pn_Loader_IN4_X16D.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN4_X16D.TabIndex = 217;
            // 
            // lb_x16c_spare_cylinder_sensor
            // 
            this.lb_x16c_spare_cylinder_sensor.AutoSize = true;
            this.lb_x16c_spare_cylinder_sensor.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x16c_spare_cylinder_sensor.ForeColor = System.Drawing.Color.White;
            this.lb_x16c_spare_cylinder_sensor.Location = new System.Drawing.Point(52, 594);
            this.lb_x16c_spare_cylinder_sensor.Name = "lb_x16c_spare_cylinder_sensor";
            this.lb_x16c_spare_cylinder_sensor.Size = new System.Drawing.Size(237, 13);
            this.lb_x16c_spare_cylinder_sensor.TabIndex = 216;
            this.lb_x16c_spare_cylinder_sensor.Text = "[ X16C ] Spare Cylinder Sensor";
            // 
            // pn_Loader_IN4_X16C
            // 
            this.pn_Loader_IN4_X16C.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN4_X16C.Location = new System.Drawing.Point(6, 580);
            this.pn_Loader_IN4_X16C.Name = "pn_Loader_IN4_X16C";
            this.pn_Loader_IN4_X16C.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN4_X16C.TabIndex = 215;
            // 
            // lb_x16b_spare_cylinder_sensor
            // 
            this.lb_x16b_spare_cylinder_sensor.AutoSize = true;
            this.lb_x16b_spare_cylinder_sensor.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x16b_spare_cylinder_sensor.ForeColor = System.Drawing.Color.White;
            this.lb_x16b_spare_cylinder_sensor.Location = new System.Drawing.Point(52, 548);
            this.lb_x16b_spare_cylinder_sensor.Name = "lb_x16b_spare_cylinder_sensor";
            this.lb_x16b_spare_cylinder_sensor.Size = new System.Drawing.Size(237, 13);
            this.lb_x16b_spare_cylinder_sensor.TabIndex = 214;
            this.lb_x16b_spare_cylinder_sensor.Text = "[ X16B ] Spare Cylinder Sensor";
            // 
            // pn_Loader_IN4_X16B
            // 
            this.pn_Loader_IN4_X16B.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN4_X16B.Location = new System.Drawing.Point(6, 534);
            this.pn_Loader_IN4_X16B.Name = "pn_Loader_IN4_X16B";
            this.pn_Loader_IN4_X16B.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN4_X16B.TabIndex = 213;
            // 
            // lb_x16a_spare_cylinder_sensor
            // 
            this.lb_x16a_spare_cylinder_sensor.AutoSize = true;
            this.lb_x16a_spare_cylinder_sensor.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x16a_spare_cylinder_sensor.ForeColor = System.Drawing.Color.White;
            this.lb_x16a_spare_cylinder_sensor.Location = new System.Drawing.Point(52, 501);
            this.lb_x16a_spare_cylinder_sensor.Name = "lb_x16a_spare_cylinder_sensor";
            this.lb_x16a_spare_cylinder_sensor.Size = new System.Drawing.Size(236, 13);
            this.lb_x16a_spare_cylinder_sensor.TabIndex = 212;
            this.lb_x16a_spare_cylinder_sensor.Text = "[ X16A ] Spare Cylinder Sensor";
            // 
            // pn_Loader_IN4_X16A
            // 
            this.pn_Loader_IN4_X16A.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN4_X16A.Location = new System.Drawing.Point(6, 488);
            this.pn_Loader_IN4_X16A.Name = "pn_Loader_IN4_X16A";
            this.pn_Loader_IN4_X16A.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN4_X16A.TabIndex = 211;
            // 
            // lb_x169_spare_cylinder_sensor
            // 
            this.lb_x169_spare_cylinder_sensor.AutoSize = true;
            this.lb_x169_spare_cylinder_sensor.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x169_spare_cylinder_sensor.ForeColor = System.Drawing.Color.White;
            this.lb_x169_spare_cylinder_sensor.Location = new System.Drawing.Point(52, 454);
            this.lb_x169_spare_cylinder_sensor.Name = "lb_x169_spare_cylinder_sensor";
            this.lb_x169_spare_cylinder_sensor.Size = new System.Drawing.Size(235, 13);
            this.lb_x169_spare_cylinder_sensor.TabIndex = 210;
            this.lb_x169_spare_cylinder_sensor.Text = "[ X169 ] Spare Cylinder Sensor";
            // 
            // pn_Loader_IN4_X169
            // 
            this.pn_Loader_IN4_X169.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN4_X169.Location = new System.Drawing.Point(6, 442);
            this.pn_Loader_IN4_X169.Name = "pn_Loader_IN4_X169";
            this.pn_Loader_IN4_X169.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN4_X169.TabIndex = 209;
            // 
            // lb_x168_spare_cylinder_sensor
            // 
            this.lb_x168_spare_cylinder_sensor.AutoSize = true;
            this.lb_x168_spare_cylinder_sensor.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x168_spare_cylinder_sensor.ForeColor = System.Drawing.Color.White;
            this.lb_x168_spare_cylinder_sensor.Location = new System.Drawing.Point(52, 409);
            this.lb_x168_spare_cylinder_sensor.Name = "lb_x168_spare_cylinder_sensor";
            this.lb_x168_spare_cylinder_sensor.Size = new System.Drawing.Size(235, 13);
            this.lb_x168_spare_cylinder_sensor.TabIndex = 208;
            this.lb_x168_spare_cylinder_sensor.Text = "[ X168 ] Spare Cylinder Sensor";
            // 
            // pn_Loader_IN4_X168
            // 
            this.pn_Loader_IN4_X168.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN4_X168.Location = new System.Drawing.Point(6, 396);
            this.pn_Loader_IN4_X168.Name = "pn_Loader_IN4_X168";
            this.pn_Loader_IN4_X168.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN4_X168.TabIndex = 207;
            // 
            // lb_x167_spare_cylinder_sensor
            // 
            this.lb_x167_spare_cylinder_sensor.AutoSize = true;
            this.lb_x167_spare_cylinder_sensor.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x167_spare_cylinder_sensor.ForeColor = System.Drawing.Color.White;
            this.lb_x167_spare_cylinder_sensor.Location = new System.Drawing.Point(52, 364);
            this.lb_x167_spare_cylinder_sensor.Name = "lb_x167_spare_cylinder_sensor";
            this.lb_x167_spare_cylinder_sensor.Size = new System.Drawing.Size(235, 13);
            this.lb_x167_spare_cylinder_sensor.TabIndex = 206;
            this.lb_x167_spare_cylinder_sensor.Text = "[ X167 ] Spare Cylinder Sensor";
            // 
            // pn_Loader_IN4_X167
            // 
            this.pn_Loader_IN4_X167.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN4_X167.Location = new System.Drawing.Point(6, 350);
            this.pn_Loader_IN4_X167.Name = "pn_Loader_IN4_X167";
            this.pn_Loader_IN4_X167.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN4_X167.TabIndex = 205;
            // 
            // lb_x166_3_main_pressure_sw
            // 
            this.lb_x166_3_main_pressure_sw.AutoSize = true;
            this.lb_x166_3_main_pressure_sw.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x166_3_main_pressure_sw.ForeColor = System.Drawing.Color.White;
            this.lb_x166_3_main_pressure_sw.Location = new System.Drawing.Point(52, 319);
            this.lb_x166_3_main_pressure_sw.Name = "lb_x166_3_main_pressure_sw";
            this.lb_x166_3_main_pressure_sw.Size = new System.Drawing.Size(218, 13);
            this.lb_x166_3_main_pressure_sw.TabIndex = 204;
            this.lb_x166_3_main_pressure_sw.Text = "[ X166 ] 3 Main Pressure SW";
            // 
            // pn_Loader_IN4_X166
            // 
            this.pn_Loader_IN4_X166.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN4_X166.Location = new System.Drawing.Point(6, 304);
            this.pn_Loader_IN4_X166.Name = "pn_Loader_IN4_X166";
            this.pn_Loader_IN4_X166.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN4_X166.TabIndex = 203;
            // 
            // lb_x165_2_main_pressure_sw
            // 
            this.lb_x165_2_main_pressure_sw.AutoSize = true;
            this.lb_x165_2_main_pressure_sw.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x165_2_main_pressure_sw.ForeColor = System.Drawing.Color.White;
            this.lb_x165_2_main_pressure_sw.Location = new System.Drawing.Point(52, 271);
            this.lb_x165_2_main_pressure_sw.Name = "lb_x165_2_main_pressure_sw";
            this.lb_x165_2_main_pressure_sw.Size = new System.Drawing.Size(218, 13);
            this.lb_x165_2_main_pressure_sw.TabIndex = 202;
            this.lb_x165_2_main_pressure_sw.Text = "[ X165 ] 2 Main Pressure SW";
            // 
            // pn_Loader_IN4_X165
            // 
            this.pn_Loader_IN4_X165.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN4_X165.Location = new System.Drawing.Point(6, 258);
            this.pn_Loader_IN4_X165.Name = "pn_Loader_IN4_X165";
            this.pn_Loader_IN4_X165.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN4_X165.TabIndex = 201;
            // 
            // lb_x164_1_main_pressure_sw
            // 
            this.lb_x164_1_main_pressure_sw.AutoSize = true;
            this.lb_x164_1_main_pressure_sw.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x164_1_main_pressure_sw.ForeColor = System.Drawing.Color.White;
            this.lb_x164_1_main_pressure_sw.Location = new System.Drawing.Point(52, 226);
            this.lb_x164_1_main_pressure_sw.Name = "lb_x164_1_main_pressure_sw";
            this.lb_x164_1_main_pressure_sw.Size = new System.Drawing.Size(218, 13);
            this.lb_x164_1_main_pressure_sw.TabIndex = 200;
            this.lb_x164_1_main_pressure_sw.Text = "[ X164 ] 1 Main Pressure SW";
            // 
            // pn_Loader_IN4_X164
            // 
            this.pn_Loader_IN4_X164.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN4_X164.Location = new System.Drawing.Point(6, 212);
            this.pn_Loader_IN4_X164.Name = "pn_Loader_IN4_X164";
            this.pn_Loader_IN4_X164.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN4_X164.TabIndex = 199;
            // 
            // lb_x163_2_ld_trans_2ch_pressure_sw
            // 
            this.lb_x163_2_ld_trans_2ch_pressure_sw.AutoSize = true;
            this.lb_x163_2_ld_trans_2ch_pressure_sw.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x163_2_ld_trans_2ch_pressure_sw.ForeColor = System.Drawing.Color.White;
            this.lb_x163_2_ld_trans_2ch_pressure_sw.Location = new System.Drawing.Point(52, 179);
            this.lb_x163_2_ld_trans_2ch_pressure_sw.Name = "lb_x163_2_ld_trans_2ch_pressure_sw";
            this.lb_x163_2_ld_trans_2ch_pressure_sw.Size = new System.Drawing.Size(287, 13);
            this.lb_x163_2_ld_trans_2ch_pressure_sw.TabIndex = 198;
            this.lb_x163_2_ld_trans_2ch_pressure_sw.Text = "[ X163 ] 2 L/D 이재기 2ch Pressure SW";
            // 
            // pn_Loader_IN4_X163
            // 
            this.pn_Loader_IN4_X163.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN4_X163.Location = new System.Drawing.Point(6, 166);
            this.pn_Loader_IN4_X163.Name = "pn_Loader_IN4_X163";
            this.pn_Loader_IN4_X163.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN4_X163.TabIndex = 197;
            // 
            // lb_x162_2_ld_trans_1ch_pressure_sw
            // 
            this.lb_x162_2_ld_trans_1ch_pressure_sw.AutoSize = true;
            this.lb_x162_2_ld_trans_1ch_pressure_sw.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x162_2_ld_trans_1ch_pressure_sw.ForeColor = System.Drawing.Color.White;
            this.lb_x162_2_ld_trans_1ch_pressure_sw.Location = new System.Drawing.Point(52, 133);
            this.lb_x162_2_ld_trans_1ch_pressure_sw.Name = "lb_x162_2_ld_trans_1ch_pressure_sw";
            this.lb_x162_2_ld_trans_1ch_pressure_sw.Size = new System.Drawing.Size(287, 13);
            this.lb_x162_2_ld_trans_1ch_pressure_sw.TabIndex = 196;
            this.lb_x162_2_ld_trans_1ch_pressure_sw.Text = "[ X162 ] 2 L/D 이재기 1ch Pressure SW";
            // 
            // pn_Loader_IN4_X162
            // 
            this.pn_Loader_IN4_X162.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN4_X162.Location = new System.Drawing.Point(6, 120);
            this.pn_Loader_IN4_X162.Name = "pn_Loader_IN4_X162";
            this.pn_Loader_IN4_X162.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN4_X162.TabIndex = 195;
            // 
            // lb_x161_1_ld_trans_2ch_pressure_sw
            // 
            this.lb_x161_1_ld_trans_2ch_pressure_sw.AutoSize = true;
            this.lb_x161_1_ld_trans_2ch_pressure_sw.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x161_1_ld_trans_2ch_pressure_sw.ForeColor = System.Drawing.Color.White;
            this.lb_x161_1_ld_trans_2ch_pressure_sw.Location = new System.Drawing.Point(52, 86);
            this.lb_x161_1_ld_trans_2ch_pressure_sw.Name = "lb_x161_1_ld_trans_2ch_pressure_sw";
            this.lb_x161_1_ld_trans_2ch_pressure_sw.Size = new System.Drawing.Size(287, 13);
            this.lb_x161_1_ld_trans_2ch_pressure_sw.TabIndex = 194;
            this.lb_x161_1_ld_trans_2ch_pressure_sw.Text = "[ X161 ] 1 L/D 이재기 2ch Pressure SW";
            // 
            // pn_Loader_IN4_X161
            // 
            this.pn_Loader_IN4_X161.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN4_X161.Location = new System.Drawing.Point(6, 74);
            this.pn_Loader_IN4_X161.Name = "pn_Loader_IN4_X161";
            this.pn_Loader_IN4_X161.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN4_X161.TabIndex = 193;
            // 
            // lb_x160_1_ld_trans_1ch_pressure_sw
            // 
            this.lb_x160_1_ld_trans_1ch_pressure_sw.AutoSize = true;
            this.lb_x160_1_ld_trans_1ch_pressure_sw.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x160_1_ld_trans_1ch_pressure_sw.ForeColor = System.Drawing.Color.White;
            this.lb_x160_1_ld_trans_1ch_pressure_sw.Location = new System.Drawing.Point(52, 42);
            this.lb_x160_1_ld_trans_1ch_pressure_sw.Name = "lb_x160_1_ld_trans_1ch_pressure_sw";
            this.lb_x160_1_ld_trans_1ch_pressure_sw.Size = new System.Drawing.Size(287, 13);
            this.lb_x160_1_ld_trans_1ch_pressure_sw.TabIndex = 192;
            this.lb_x160_1_ld_trans_1ch_pressure_sw.Text = "[ X160 ] 1 L/D 이재기 1ch Pressure SW";
            // 
            // pn_Loader_IN4_X160
            // 
            this.pn_Loader_IN4_X160.BackColor = System.Drawing.Color.Gray;
            this.pn_Loader_IN4_X160.Location = new System.Drawing.Point(6, 28);
            this.pn_Loader_IN4_X160.Name = "pn_Loader_IN4_X160";
            this.pn_Loader_IN4_X160.Size = new System.Drawing.Size(40, 40);
            this.pn_Loader_IN4_X160.TabIndex = 191;
            // 
            // tp_iostatus_process
            // 
            this.tp_iostatus_process.BackColor = System.Drawing.Color.DarkSlateGray;
            this.tp_iostatus_process.Controls.Add(this.tc_iostatus_process_out);
            this.tp_iostatus_process.Controls.Add(this.tc_iostatus_process_in);
            this.tp_iostatus_process.Location = new System.Drawing.Point(4, 34);
            this.tp_iostatus_process.Name = "tp_iostatus_process";
            this.tp_iostatus_process.Padding = new System.Windows.Forms.Padding(3);
            this.tp_iostatus_process.Size = new System.Drawing.Size(1726, 816);
            this.tp_iostatus_process.TabIndex = 1;
            this.tp_iostatus_process.Text = "프로세스";
            // 
            // tc_iostatus_process_out
            // 
            this.tc_iostatus_process_out.Controls.Add(this.tp_iostatus_process_out1);
            this.tc_iostatus_process_out.Controls.Add(this.tp_iostatus_process_out2);
            this.tc_iostatus_process_out.Controls.Add(this.tp_iostatus_process_out3);
            this.tc_iostatus_process_out.ItemSize = new System.Drawing.Size(283, 30);
            this.tc_iostatus_process_out.Location = new System.Drawing.Point(866, 6);
            this.tc_iostatus_process_out.Name = "tc_iostatus_process_out";
            this.tc_iostatus_process_out.SelectedIndex = 0;
            this.tc_iostatus_process_out.Size = new System.Drawing.Size(854, 816);
            this.tc_iostatus_process_out.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tc_iostatus_process_out.TabIndex = 2;
            // 
            // tp_iostatus_process_out1
            // 
            this.tp_iostatus_process_out1.BackColor = System.Drawing.Color.DimGray;
            this.tp_iostatus_process_out1.Controls.Add(this.label131);
            this.tp_iostatus_process_out1.Controls.Add(this.panel131);
            this.tp_iostatus_process_out1.Controls.Add(this.label132);
            this.tp_iostatus_process_out1.Controls.Add(this.panel132);
            this.tp_iostatus_process_out1.Controls.Add(this.label133);
            this.tp_iostatus_process_out1.Controls.Add(this.panel133);
            this.tp_iostatus_process_out1.Controls.Add(this.label134);
            this.tp_iostatus_process_out1.Controls.Add(this.panel134);
            this.tp_iostatus_process_out1.Controls.Add(this.label135);
            this.tp_iostatus_process_out1.Controls.Add(this.panel135);
            this.tp_iostatus_process_out1.Controls.Add(this.label136);
            this.tp_iostatus_process_out1.Controls.Add(this.panel136);
            this.tp_iostatus_process_out1.Controls.Add(this.label137);
            this.tp_iostatus_process_out1.Controls.Add(this.panel137);
            this.tp_iostatus_process_out1.Controls.Add(this.label138);
            this.tp_iostatus_process_out1.Controls.Add(this.panel138);
            this.tp_iostatus_process_out1.Controls.Add(this.label139);
            this.tp_iostatus_process_out1.Controls.Add(this.panel139);
            this.tp_iostatus_process_out1.Controls.Add(this.label140);
            this.tp_iostatus_process_out1.Controls.Add(this.panel140);
            this.tp_iostatus_process_out1.Controls.Add(this.label141);
            this.tp_iostatus_process_out1.Controls.Add(this.panel141);
            this.tp_iostatus_process_out1.Controls.Add(this.label142);
            this.tp_iostatus_process_out1.Controls.Add(this.panel142);
            this.tp_iostatus_process_out1.Controls.Add(this.label143);
            this.tp_iostatus_process_out1.Controls.Add(this.panel143);
            this.tp_iostatus_process_out1.Controls.Add(this.label144);
            this.tp_iostatus_process_out1.Controls.Add(this.panel144);
            this.tp_iostatus_process_out1.Controls.Add(this.label145);
            this.tp_iostatus_process_out1.Controls.Add(this.panel145);
            this.tp_iostatus_process_out1.Controls.Add(this.label146);
            this.tp_iostatus_process_out1.Controls.Add(this.panel146);
            this.tp_iostatus_process_out1.Controls.Add(this.label147);
            this.tp_iostatus_process_out1.Controls.Add(this.panel147);
            this.tp_iostatus_process_out1.Controls.Add(this.label148);
            this.tp_iostatus_process_out1.Controls.Add(this.panel148);
            this.tp_iostatus_process_out1.Controls.Add(this.label149);
            this.tp_iostatus_process_out1.Controls.Add(this.panel149);
            this.tp_iostatus_process_out1.Controls.Add(this.label150);
            this.tp_iostatus_process_out1.Controls.Add(this.panel150);
            this.tp_iostatus_process_out1.Controls.Add(this.label151);
            this.tp_iostatus_process_out1.Controls.Add(this.panel151);
            this.tp_iostatus_process_out1.Controls.Add(this.label152);
            this.tp_iostatus_process_out1.Controls.Add(this.panel152);
            this.tp_iostatus_process_out1.Controls.Add(this.label153);
            this.tp_iostatus_process_out1.Controls.Add(this.panel153);
            this.tp_iostatus_process_out1.Controls.Add(this.label154);
            this.tp_iostatus_process_out1.Controls.Add(this.panel154);
            this.tp_iostatus_process_out1.Controls.Add(this.label155);
            this.tp_iostatus_process_out1.Controls.Add(this.panel155);
            this.tp_iostatus_process_out1.Controls.Add(this.label156);
            this.tp_iostatus_process_out1.Controls.Add(this.panel156);
            this.tp_iostatus_process_out1.Controls.Add(this.label157);
            this.tp_iostatus_process_out1.Controls.Add(this.panel157);
            this.tp_iostatus_process_out1.Controls.Add(this.label158);
            this.tp_iostatus_process_out1.Controls.Add(this.panel158);
            this.tp_iostatus_process_out1.Controls.Add(this.label159);
            this.tp_iostatus_process_out1.Controls.Add(this.panel159);
            this.tp_iostatus_process_out1.Controls.Add(this.lb_y322_laser_emergency_stop);
            this.tp_iostatus_process_out1.Controls.Add(this.panel_y322_laser_emergency_stop);
            this.tp_iostatus_process_out1.Controls.Add(this.lb_y321_laser_stop_external_interlock);
            this.tp_iostatus_process_out1.Controls.Add(this.panel_y321_laser_stop_external_interlock);
            this.tp_iostatus_process_out1.Controls.Add(this.lb_y320_laser_stop_internal_interlock);
            this.tp_iostatus_process_out1.Controls.Add(this.panel_y320_laser_stop_internal_interlock);
            this.tp_iostatus_process_out1.Location = new System.Drawing.Point(4, 34);
            this.tp_iostatus_process_out1.Name = "tp_iostatus_process_out1";
            this.tp_iostatus_process_out1.Padding = new System.Windows.Forms.Padding(3);
            this.tp_iostatus_process_out1.Size = new System.Drawing.Size(846, 778);
            this.tp_iostatus_process_out1.TabIndex = 1;
            this.tp_iostatus_process_out1.Text = "OUT - 1";
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label131.ForeColor = System.Drawing.Color.White;
            this.label131.Location = new System.Drawing.Point(479, 730);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(240, 13);
            this.label131.TabIndex = 191;
            this.label131.Text = "[ Y1BF ] Spare 파기 AIR ON CMD";
            // 
            // panel131
            // 
            this.panel131.BackColor = System.Drawing.Color.Gray;
            this.panel131.Location = new System.Drawing.Point(433, 718);
            this.panel131.Name = "panel131";
            this.panel131.Size = new System.Drawing.Size(40, 40);
            this.panel131.TabIndex = 190;
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label132.ForeColor = System.Drawing.Color.White;
            this.label132.Location = new System.Drawing.Point(479, 685);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(241, 13);
            this.label132.TabIndex = 189;
            this.label132.Text = "[ Y1BE ] Spare 파기 AIR ON CMD";
            // 
            // panel132
            // 
            this.panel132.BackColor = System.Drawing.Color.Gray;
            this.panel132.Location = new System.Drawing.Point(433, 672);
            this.panel132.Name = "panel132";
            this.panel132.Size = new System.Drawing.Size(40, 40);
            this.panel132.TabIndex = 188;
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label133.ForeColor = System.Drawing.Color.White;
            this.label133.Location = new System.Drawing.Point(479, 640);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(318, 13);
            this.label133.TabIndex = 187;
            this.label133.Text = "[ Y1BD ] 2 Cassette 취출 이재기 파기 AIR ON";
            // 
            // panel133
            // 
            this.panel133.BackColor = System.Drawing.Color.Gray;
            this.panel133.Location = new System.Drawing.Point(433, 626);
            this.panel133.Name = "panel133";
            this.panel133.Size = new System.Drawing.Size(40, 40);
            this.panel133.TabIndex = 186;
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label134.ForeColor = System.Drawing.Color.White;
            this.label134.Location = new System.Drawing.Point(479, 594);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(295, 13);
            this.label134.TabIndex = 185;
            this.label134.Text = "[ Y1BC ] 2 Cassette 취출 이재기 Vac OFF";
            // 
            // panel134
            // 
            this.panel134.BackColor = System.Drawing.Color.Gray;
            this.panel134.Location = new System.Drawing.Point(433, 580);
            this.panel134.Name = "panel134";
            this.panel134.Size = new System.Drawing.Size(40, 40);
            this.panel134.TabIndex = 184;
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label135.ForeColor = System.Drawing.Color.White;
            this.label135.Location = new System.Drawing.Point(479, 548);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(289, 13);
            this.label135.TabIndex = 183;
            this.label135.Text = "[ Y1BB ] 2 Cassette 취출 이재기 Vac ON";
            // 
            // panel135
            // 
            this.panel135.BackColor = System.Drawing.Color.Gray;
            this.panel135.Location = new System.Drawing.Point(433, 534);
            this.panel135.Name = "panel135";
            this.panel135.Size = new System.Drawing.Size(40, 40);
            this.panel135.TabIndex = 182;
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label136.ForeColor = System.Drawing.Color.White;
            this.label136.Location = new System.Drawing.Point(479, 501);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(317, 13);
            this.label136.TabIndex = 181;
            this.label136.Text = "[ Y1BA ] 1 Cassette 취출 이재기 파기 AIR ON";
            // 
            // panel136
            // 
            this.panel136.BackColor = System.Drawing.Color.Gray;
            this.panel136.Location = new System.Drawing.Point(433, 488);
            this.panel136.Name = "panel136";
            this.panel136.Size = new System.Drawing.Size(40, 40);
            this.panel136.TabIndex = 180;
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label137.ForeColor = System.Drawing.Color.White;
            this.label137.Location = new System.Drawing.Point(479, 454);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(293, 13);
            this.label137.TabIndex = 179;
            this.label137.Text = "[ Y1B9 ] 1 Cassette 취출 이재기 Vac OFF";
            // 
            // panel137
            // 
            this.panel137.BackColor = System.Drawing.Color.Gray;
            this.panel137.Location = new System.Drawing.Point(433, 442);
            this.panel137.Name = "panel137";
            this.panel137.Size = new System.Drawing.Size(40, 40);
            this.panel137.TabIndex = 178;
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label138.ForeColor = System.Drawing.Color.White;
            this.label138.Location = new System.Drawing.Point(479, 409);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(287, 13);
            this.label138.TabIndex = 177;
            this.label138.Text = "[ Y1B8 ] 1 Cassette 취출 이재기 Vac ON";
            // 
            // panel138
            // 
            this.panel138.BackColor = System.Drawing.Color.Gray;
            this.panel138.Location = new System.Drawing.Point(433, 396);
            this.panel138.Name = "panel138";
            this.panel138.Size = new System.Drawing.Size(40, 40);
            this.panel138.TabIndex = 176;
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label139.ForeColor = System.Drawing.Color.White;
            this.label139.Location = new System.Drawing.Point(479, 364);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(323, 13);
            this.label139.TabIndex = 175;
            this.label139.Text = "[ Y1B7 ] 2 L/D Casset Lift Tilt Sensor DOWN";
            // 
            // panel139
            // 
            this.panel139.BackColor = System.Drawing.Color.Gray;
            this.panel139.Location = new System.Drawing.Point(433, 350);
            this.panel139.Name = "panel139";
            this.panel139.Size = new System.Drawing.Size(40, 40);
            this.panel139.TabIndex = 174;
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label140.ForeColor = System.Drawing.Color.White;
            this.label140.Location = new System.Drawing.Point(479, 319);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(299, 13);
            this.label140.TabIndex = 173;
            this.label140.Text = "[ Y1B6 ] 2 L/D Casset Lift Tilt Sensor UP";
            // 
            // panel140
            // 
            this.panel140.BackColor = System.Drawing.Color.Gray;
            this.panel140.Location = new System.Drawing.Point(433, 304);
            this.panel140.Name = "panel140";
            this.panel140.Size = new System.Drawing.Size(40, 40);
            this.panel140.TabIndex = 172;
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label141.ForeColor = System.Drawing.Color.White;
            this.label141.Location = new System.Drawing.Point(479, 271);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(276, 13);
            this.label141.TabIndex = 171;
            this.label141.Text = "[ Y1B5 ] 2 L/D 이재기 2ch 파기 AIR ON";
            // 
            // panel141
            // 
            this.panel141.BackColor = System.Drawing.Color.Gray;
            this.panel141.Location = new System.Drawing.Point(433, 258);
            this.panel141.Name = "panel141";
            this.panel141.Size = new System.Drawing.Size(40, 40);
            this.panel141.TabIndex = 170;
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label142.ForeColor = System.Drawing.Color.White;
            this.label142.Location = new System.Drawing.Point(479, 226);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(253, 13);
            this.label142.TabIndex = 169;
            this.label142.Text = "[ Y1B4 ] 2 L/D 이재기 2ch Vac OFF";
            // 
            // panel142
            // 
            this.panel142.BackColor = System.Drawing.Color.Gray;
            this.panel142.Location = new System.Drawing.Point(433, 212);
            this.panel142.Name = "panel142";
            this.panel142.Size = new System.Drawing.Size(40, 40);
            this.panel142.TabIndex = 168;
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label143.ForeColor = System.Drawing.Color.White;
            this.label143.Location = new System.Drawing.Point(479, 179);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(238, 13);
            this.label143.TabIndex = 167;
            this.label143.Text = "[ Y1B3 ] 2 L/D 이재기 2h Vac ON";
            // 
            // panel143
            // 
            this.panel143.BackColor = System.Drawing.Color.Gray;
            this.panel143.Location = new System.Drawing.Point(433, 166);
            this.panel143.Name = "panel143";
            this.panel143.Size = new System.Drawing.Size(40, 40);
            this.panel143.TabIndex = 166;
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label144.ForeColor = System.Drawing.Color.White;
            this.label144.Location = new System.Drawing.Point(479, 133);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(276, 13);
            this.label144.TabIndex = 165;
            this.label144.Text = "[ Y1B2 ] 2 L/D 이재기 1ch 파기 AIR ON";
            // 
            // panel144
            // 
            this.panel144.BackColor = System.Drawing.Color.Gray;
            this.panel144.Location = new System.Drawing.Point(433, 120);
            this.panel144.Name = "panel144";
            this.panel144.Size = new System.Drawing.Size(40, 40);
            this.panel144.TabIndex = 164;
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label145.ForeColor = System.Drawing.Color.White;
            this.label145.Location = new System.Drawing.Point(479, 86);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(253, 13);
            this.label145.TabIndex = 163;
            this.label145.Text = "[ Y1B1 ] 2 L/D 이재기 1ch Vac OFF";
            // 
            // panel145
            // 
            this.panel145.BackColor = System.Drawing.Color.Gray;
            this.panel145.Location = new System.Drawing.Point(433, 74);
            this.panel145.Name = "panel145";
            this.panel145.Size = new System.Drawing.Size(40, 40);
            this.panel145.TabIndex = 162;
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label146.ForeColor = System.Drawing.Color.White;
            this.label146.Location = new System.Drawing.Point(479, 42);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(247, 13);
            this.label146.TabIndex = 161;
            this.label146.Text = "[ Y1B0 ] 2 L/D 이재기 1ch Vac ON";
            // 
            // panel146
            // 
            this.panel146.BackColor = System.Drawing.Color.Gray;
            this.panel146.Location = new System.Drawing.Point(433, 28);
            this.panel146.Name = "panel146";
            this.panel146.Size = new System.Drawing.Size(40, 40);
            this.panel146.TabIndex = 160;
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label147.ForeColor = System.Drawing.Color.White;
            this.label147.Location = new System.Drawing.Point(52, 730);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(275, 13);
            this.label147.TabIndex = 159;
            this.label147.Text = "[ Y1AF ] 1 L/D 이재기 2ch 파기 AIR ON";
            // 
            // panel147
            // 
            this.panel147.BackColor = System.Drawing.Color.Gray;
            this.panel147.Location = new System.Drawing.Point(6, 718);
            this.panel147.Name = "panel147";
            this.panel147.Size = new System.Drawing.Size(40, 40);
            this.panel147.TabIndex = 158;
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label148.ForeColor = System.Drawing.Color.White;
            this.label148.Location = new System.Drawing.Point(52, 685);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(253, 13);
            this.label148.TabIndex = 157;
            this.label148.Text = "[ Y1AE ] 1 L/D 이재기 2ch Vac OFF";
            // 
            // panel148
            // 
            this.panel148.BackColor = System.Drawing.Color.Gray;
            this.panel148.Location = new System.Drawing.Point(6, 672);
            this.panel148.Name = "panel148";
            this.panel148.Size = new System.Drawing.Size(40, 40);
            this.panel148.TabIndex = 156;
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label149.ForeColor = System.Drawing.Color.White;
            this.label149.Location = new System.Drawing.Point(52, 640);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(248, 13);
            this.label149.TabIndex = 155;
            this.label149.Text = "[ Y1AD ] 1 L/D 이재기 2ch Vac ON";
            // 
            // panel149
            // 
            this.panel149.BackColor = System.Drawing.Color.Gray;
            this.panel149.Location = new System.Drawing.Point(6, 626);
            this.panel149.Name = "panel149";
            this.panel149.Size = new System.Drawing.Size(40, 40);
            this.panel149.TabIndex = 154;
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label150.ForeColor = System.Drawing.Color.White;
            this.label150.Location = new System.Drawing.Point(52, 594);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(277, 13);
            this.label150.TabIndex = 153;
            this.label150.Text = "[ Y1AC ] 1 L/D 이재기 1ch 파기 AIR ON";
            // 
            // panel150
            // 
            this.panel150.BackColor = System.Drawing.Color.Gray;
            this.panel150.Location = new System.Drawing.Point(6, 580);
            this.panel150.Name = "panel150";
            this.panel150.Size = new System.Drawing.Size(40, 40);
            this.panel150.TabIndex = 152;
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label151.ForeColor = System.Drawing.Color.White;
            this.label151.Location = new System.Drawing.Point(52, 548);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(254, 13);
            this.label151.TabIndex = 151;
            this.label151.Text = "[ Y1AB ] 1 L/D 이재기 1ch Vac OFF";
            // 
            // panel151
            // 
            this.panel151.BackColor = System.Drawing.Color.Gray;
            this.panel151.Location = new System.Drawing.Point(6, 534);
            this.panel151.Name = "panel151";
            this.panel151.Size = new System.Drawing.Size(40, 40);
            this.panel151.TabIndex = 150;
            // 
            // label152
            // 
            this.label152.AutoSize = true;
            this.label152.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label152.ForeColor = System.Drawing.Color.White;
            this.label152.Location = new System.Drawing.Point(52, 501);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(247, 13);
            this.label152.TabIndex = 149;
            this.label152.Text = "[ Y1AA ] 1 L/D 이재기 1ch Vac ON";
            // 
            // panel152
            // 
            this.panel152.BackColor = System.Drawing.Color.Gray;
            this.panel152.Location = new System.Drawing.Point(6, 488);
            this.panel152.Name = "panel152";
            this.panel152.Size = new System.Drawing.Size(40, 40);
            this.panel152.TabIndex = 148;
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label153.ForeColor = System.Drawing.Color.White;
            this.label153.Location = new System.Drawing.Point(52, 454);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(206, 13);
            this.label153.TabIndex = 147;
            this.label153.Text = "[ Y1A9 ] 2 L/D 이재기 DOWN";
            // 
            // panel153
            // 
            this.panel153.BackColor = System.Drawing.Color.Gray;
            this.panel153.Location = new System.Drawing.Point(6, 442);
            this.panel153.Name = "panel153";
            this.panel153.Size = new System.Drawing.Size(40, 40);
            this.panel153.TabIndex = 146;
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label154.ForeColor = System.Drawing.Color.White;
            this.label154.Location = new System.Drawing.Point(52, 409);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(182, 13);
            this.label154.TabIndex = 145;
            this.label154.Text = "[ Y1A8 ] 2 L/D 이재기 UP";
            // 
            // panel154
            // 
            this.panel154.BackColor = System.Drawing.Color.Gray;
            this.panel154.Location = new System.Drawing.Point(6, 396);
            this.panel154.Name = "panel154";
            this.panel154.Size = new System.Drawing.Size(40, 40);
            this.panel154.TabIndex = 144;
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label155.ForeColor = System.Drawing.Color.White;
            this.label155.Location = new System.Drawing.Point(52, 364);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(206, 13);
            this.label155.TabIndex = 143;
            this.label155.Text = "[ Y1A7 ] 1 L/D 이재기 DOWN";
            // 
            // panel155
            // 
            this.panel155.BackColor = System.Drawing.Color.Gray;
            this.panel155.Location = new System.Drawing.Point(6, 350);
            this.panel155.Name = "panel155";
            this.panel155.Size = new System.Drawing.Size(40, 40);
            this.panel155.TabIndex = 142;
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label156.ForeColor = System.Drawing.Color.White;
            this.label156.Location = new System.Drawing.Point(52, 319);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(182, 13);
            this.label156.TabIndex = 141;
            this.label156.Text = "[ Y1A6 ] 1 L/D 이재기 UP";
            // 
            // panel156
            // 
            this.panel156.BackColor = System.Drawing.Color.Gray;
            this.panel156.Location = new System.Drawing.Point(6, 304);
            this.panel156.Name = "panel156";
            this.panel156.Size = new System.Drawing.Size(40, 40);
            this.panel156.TabIndex = 140;
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label157.ForeColor = System.Drawing.Color.White;
            this.label157.Location = new System.Drawing.Point(52, 271);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(322, 13);
            this.label157.TabIndex = 139;
            this.label157.Text = "[ Y1A5 ] 1 L/D Casset Lift Tilt Sensor DOWN";
            // 
            // panel157
            // 
            this.panel157.BackColor = System.Drawing.Color.Gray;
            this.panel157.Location = new System.Drawing.Point(6, 258);
            this.panel157.Name = "panel157";
            this.panel157.Size = new System.Drawing.Size(40, 40);
            this.panel157.TabIndex = 138;
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label158.ForeColor = System.Drawing.Color.White;
            this.label158.Location = new System.Drawing.Point(52, 226);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(298, 13);
            this.label158.TabIndex = 137;
            this.label158.Text = "[ Y1A4 ] 1 L/D Casset Lift Tilt Sensor UP";
            // 
            // panel158
            // 
            this.panel158.BackColor = System.Drawing.Color.Gray;
            this.panel158.Location = new System.Drawing.Point(6, 212);
            this.panel158.Name = "panel158";
            this.panel158.Size = new System.Drawing.Size(40, 40);
            this.panel158.TabIndex = 136;
            // 
            // label159
            // 
            this.label159.AutoSize = true;
            this.label159.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label159.ForeColor = System.Drawing.Color.White;
            this.label159.Location = new System.Drawing.Point(52, 179);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(272, 13);
            this.label159.TabIndex = 135;
            this.label159.Text = "[ Y1A3 ] 2 L/D Cassette Lifter UnGrip";
            // 
            // panel159
            // 
            this.panel159.BackColor = System.Drawing.Color.Gray;
            this.panel159.Location = new System.Drawing.Point(6, 166);
            this.panel159.Name = "panel159";
            this.panel159.Size = new System.Drawing.Size(40, 40);
            this.panel159.TabIndex = 134;
            // 
            // lb_y322_laser_emergency_stop
            // 
            this.lb_y322_laser_emergency_stop.AutoSize = true;
            this.lb_y322_laser_emergency_stop.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y322_laser_emergency_stop.ForeColor = System.Drawing.Color.White;
            this.lb_y322_laser_emergency_stop.Location = new System.Drawing.Point(52, 133);
            this.lb_y322_laser_emergency_stop.Name = "lb_y322_laser_emergency_stop";
            this.lb_y322_laser_emergency_stop.Size = new System.Drawing.Size(234, 13);
            this.lb_y322_laser_emergency_stop.TabIndex = 133;
            this.lb_y322_laser_emergency_stop.Text = "[ Y322 ] Laser Emergency Stop";
            // 
            // panel_y322_laser_emergency_stop
            // 
            this.panel_y322_laser_emergency_stop.BackColor = System.Drawing.Color.Gray;
            this.panel_y322_laser_emergency_stop.Location = new System.Drawing.Point(6, 120);
            this.panel_y322_laser_emergency_stop.Name = "panel_y322_laser_emergency_stop";
            this.panel_y322_laser_emergency_stop.Size = new System.Drawing.Size(40, 40);
            this.panel_y322_laser_emergency_stop.TabIndex = 132;
            // 
            // lb_y321_laser_stop_external_interlock
            // 
            this.lb_y321_laser_stop_external_interlock.AutoSize = true;
            this.lb_y321_laser_stop_external_interlock.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y321_laser_stop_external_interlock.ForeColor = System.Drawing.Color.White;
            this.lb_y321_laser_stop_external_interlock.Location = new System.Drawing.Point(52, 86);
            this.lb_y321_laser_stop_external_interlock.Name = "lb_y321_laser_stop_external_interlock";
            this.lb_y321_laser_stop_external_interlock.Size = new System.Drawing.Size(288, 13);
            this.lb_y321_laser_stop_external_interlock.TabIndex = 131;
            this.lb_y321_laser_stop_external_interlock.Text = "[ Y321 ] Laser Stop(External_Interlock)";
            // 
            // panel_y321_laser_stop_external_interlock
            // 
            this.panel_y321_laser_stop_external_interlock.BackColor = System.Drawing.Color.Gray;
            this.panel_y321_laser_stop_external_interlock.Location = new System.Drawing.Point(6, 74);
            this.panel_y321_laser_stop_external_interlock.Name = "panel_y321_laser_stop_external_interlock";
            this.panel_y321_laser_stop_external_interlock.Size = new System.Drawing.Size(40, 40);
            this.panel_y321_laser_stop_external_interlock.TabIndex = 130;
            // 
            // lb_y320_laser_stop_internal_interlock
            // 
            this.lb_y320_laser_stop_internal_interlock.AutoSize = true;
            this.lb_y320_laser_stop_internal_interlock.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_y320_laser_stop_internal_interlock.ForeColor = System.Drawing.Color.White;
            this.lb_y320_laser_stop_internal_interlock.Location = new System.Drawing.Point(52, 42);
            this.lb_y320_laser_stop_internal_interlock.Name = "lb_y320_laser_stop_internal_interlock";
            this.lb_y320_laser_stop_internal_interlock.Size = new System.Drawing.Size(284, 13);
            this.lb_y320_laser_stop_internal_interlock.TabIndex = 129;
            this.lb_y320_laser_stop_internal_interlock.Text = "[ Y320 ] Laser Stop(Internal_Interlock)";
            // 
            // panel_y320_laser_stop_internal_interlock
            // 
            this.panel_y320_laser_stop_internal_interlock.BackColor = System.Drawing.Color.Gray;
            this.panel_y320_laser_stop_internal_interlock.Location = new System.Drawing.Point(6, 28);
            this.panel_y320_laser_stop_internal_interlock.Name = "panel_y320_laser_stop_internal_interlock";
            this.panel_y320_laser_stop_internal_interlock.Size = new System.Drawing.Size(40, 40);
            this.panel_y320_laser_stop_internal_interlock.TabIndex = 128;
            // 
            // tp_iostatus_process_out2
            // 
            this.tp_iostatus_process_out2.BackColor = System.Drawing.Color.DimGray;
            this.tp_iostatus_process_out2.Controls.Add(this.label163);
            this.tp_iostatus_process_out2.Controls.Add(this.panel163);
            this.tp_iostatus_process_out2.Controls.Add(this.label164);
            this.tp_iostatus_process_out2.Controls.Add(this.panel164);
            this.tp_iostatus_process_out2.Controls.Add(this.label165);
            this.tp_iostatus_process_out2.Controls.Add(this.panel165);
            this.tp_iostatus_process_out2.Controls.Add(this.label166);
            this.tp_iostatus_process_out2.Controls.Add(this.panel166);
            this.tp_iostatus_process_out2.Controls.Add(this.label167);
            this.tp_iostatus_process_out2.Controls.Add(this.panel167);
            this.tp_iostatus_process_out2.Controls.Add(this.label168);
            this.tp_iostatus_process_out2.Controls.Add(this.panel168);
            this.tp_iostatus_process_out2.Controls.Add(this.label169);
            this.tp_iostatus_process_out2.Controls.Add(this.panel169);
            this.tp_iostatus_process_out2.Controls.Add(this.label170);
            this.tp_iostatus_process_out2.Controls.Add(this.panel170);
            this.tp_iostatus_process_out2.Controls.Add(this.label171);
            this.tp_iostatus_process_out2.Controls.Add(this.panel171);
            this.tp_iostatus_process_out2.Controls.Add(this.label172);
            this.tp_iostatus_process_out2.Controls.Add(this.panel172);
            this.tp_iostatus_process_out2.Controls.Add(this.label173);
            this.tp_iostatus_process_out2.Controls.Add(this.panel173);
            this.tp_iostatus_process_out2.Controls.Add(this.label174);
            this.tp_iostatus_process_out2.Controls.Add(this.panel174);
            this.tp_iostatus_process_out2.Controls.Add(this.label175);
            this.tp_iostatus_process_out2.Controls.Add(this.panel175);
            this.tp_iostatus_process_out2.Controls.Add(this.label176);
            this.tp_iostatus_process_out2.Controls.Add(this.panel176);
            this.tp_iostatus_process_out2.Controls.Add(this.label177);
            this.tp_iostatus_process_out2.Controls.Add(this.panel177);
            this.tp_iostatus_process_out2.Controls.Add(this.label178);
            this.tp_iostatus_process_out2.Controls.Add(this.panel178);
            this.tp_iostatus_process_out2.Controls.Add(this.label179);
            this.tp_iostatus_process_out2.Controls.Add(this.panel179);
            this.tp_iostatus_process_out2.Controls.Add(this.label180);
            this.tp_iostatus_process_out2.Controls.Add(this.panel180);
            this.tp_iostatus_process_out2.Controls.Add(this.label181);
            this.tp_iostatus_process_out2.Controls.Add(this.panel181);
            this.tp_iostatus_process_out2.Controls.Add(this.label182);
            this.tp_iostatus_process_out2.Controls.Add(this.panel182);
            this.tp_iostatus_process_out2.Controls.Add(this.label183);
            this.tp_iostatus_process_out2.Controls.Add(this.panel183);
            this.tp_iostatus_process_out2.Controls.Add(this.label184);
            this.tp_iostatus_process_out2.Controls.Add(this.panel184);
            this.tp_iostatus_process_out2.Controls.Add(this.label185);
            this.tp_iostatus_process_out2.Controls.Add(this.panel185);
            this.tp_iostatus_process_out2.Controls.Add(this.label186);
            this.tp_iostatus_process_out2.Controls.Add(this.panel186);
            this.tp_iostatus_process_out2.Controls.Add(this.label187);
            this.tp_iostatus_process_out2.Controls.Add(this.panel187);
            this.tp_iostatus_process_out2.Controls.Add(this.label188);
            this.tp_iostatus_process_out2.Controls.Add(this.panel188);
            this.tp_iostatus_process_out2.Controls.Add(this.label189);
            this.tp_iostatus_process_out2.Controls.Add(this.panel189);
            this.tp_iostatus_process_out2.Controls.Add(this.label190);
            this.tp_iostatus_process_out2.Controls.Add(this.panel190);
            this.tp_iostatus_process_out2.Controls.Add(this.label191);
            this.tp_iostatus_process_out2.Controls.Add(this.panel191);
            this.tp_iostatus_process_out2.Controls.Add(this.label192);
            this.tp_iostatus_process_out2.Controls.Add(this.panel192);
            this.tp_iostatus_process_out2.Controls.Add(this.label193);
            this.tp_iostatus_process_out2.Controls.Add(this.panel193);
            this.tp_iostatus_process_out2.Controls.Add(this.label194);
            this.tp_iostatus_process_out2.Controls.Add(this.panel194);
            this.tp_iostatus_process_out2.Location = new System.Drawing.Point(4, 34);
            this.tp_iostatus_process_out2.Name = "tp_iostatus_process_out2";
            this.tp_iostatus_process_out2.Padding = new System.Windows.Forms.Padding(3);
            this.tp_iostatus_process_out2.Size = new System.Drawing.Size(846, 778);
            this.tp_iostatus_process_out2.TabIndex = 2;
            this.tp_iostatus_process_out2.Text = "OUT - 2";
            // 
            // label163
            // 
            this.label163.AutoSize = true;
            this.label163.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label163.ForeColor = System.Drawing.Color.White;
            this.label163.Location = new System.Drawing.Point(479, 730);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(116, 13);
            this.label163.TabIndex = 255;
            this.label163.Text = "[ Y1DF ] Spare";
            // 
            // panel163
            // 
            this.panel163.BackColor = System.Drawing.Color.Gray;
            this.panel163.Location = new System.Drawing.Point(433, 718);
            this.panel163.Name = "panel163";
            this.panel163.Size = new System.Drawing.Size(40, 40);
            this.panel163.TabIndex = 254;
            // 
            // label164
            // 
            this.label164.AutoSize = true;
            this.label164.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label164.ForeColor = System.Drawing.Color.White;
            this.label164.Location = new System.Drawing.Point(479, 685);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(117, 13);
            this.label164.TabIndex = 253;
            this.label164.Text = "[ Y1DE ] Spare";
            // 
            // panel164
            // 
            this.panel164.BackColor = System.Drawing.Color.Gray;
            this.panel164.Location = new System.Drawing.Point(433, 672);
            this.panel164.Name = "panel164";
            this.panel164.Size = new System.Drawing.Size(40, 40);
            this.panel164.TabIndex = 252;
            // 
            // label165
            // 
            this.label165.AutoSize = true;
            this.label165.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label165.ForeColor = System.Drawing.Color.White;
            this.label165.Location = new System.Drawing.Point(479, 640);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(118, 13);
            this.label165.TabIndex = 251;
            this.label165.Text = "[ Y1DD ] Spare";
            // 
            // panel165
            // 
            this.panel165.BackColor = System.Drawing.Color.Gray;
            this.panel165.Location = new System.Drawing.Point(433, 626);
            this.panel165.Name = "panel165";
            this.panel165.Size = new System.Drawing.Size(40, 40);
            this.panel165.TabIndex = 250;
            // 
            // label166
            // 
            this.label166.AutoSize = true;
            this.label166.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label166.ForeColor = System.Drawing.Color.White;
            this.label166.Location = new System.Drawing.Point(479, 594);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(118, 13);
            this.label166.TabIndex = 249;
            this.label166.Text = "[ Y1DC ] Spare";
            // 
            // panel166
            // 
            this.panel166.BackColor = System.Drawing.Color.Gray;
            this.panel166.Location = new System.Drawing.Point(433, 580);
            this.panel166.Name = "panel166";
            this.panel166.Size = new System.Drawing.Size(40, 40);
            this.panel166.TabIndex = 248;
            // 
            // label167
            // 
            this.label167.AutoSize = true;
            this.label167.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label167.ForeColor = System.Drawing.Color.White;
            this.label167.Location = new System.Drawing.Point(479, 548);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(118, 13);
            this.label167.TabIndex = 247;
            this.label167.Text = "[ Y1DB ] Spare";
            // 
            // panel167
            // 
            this.panel167.BackColor = System.Drawing.Color.Gray;
            this.panel167.Location = new System.Drawing.Point(433, 534);
            this.panel167.Name = "panel167";
            this.panel167.Size = new System.Drawing.Size(40, 40);
            this.panel167.TabIndex = 246;
            // 
            // label168
            // 
            this.label168.AutoSize = true;
            this.label168.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label168.ForeColor = System.Drawing.Color.White;
            this.label168.Location = new System.Drawing.Point(479, 501);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(117, 13);
            this.label168.TabIndex = 245;
            this.label168.Text = "[ Y1DA ] Spare";
            // 
            // panel168
            // 
            this.panel168.BackColor = System.Drawing.Color.Gray;
            this.panel168.Location = new System.Drawing.Point(433, 488);
            this.panel168.Name = "panel168";
            this.panel168.Size = new System.Drawing.Size(40, 40);
            this.panel168.TabIndex = 244;
            // 
            // label169
            // 
            this.label169.AutoSize = true;
            this.label169.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label169.ForeColor = System.Drawing.Color.White;
            this.label169.Location = new System.Drawing.Point(479, 454);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(162, 13);
            this.label169.TabIndex = 243;
            this.label169.Text = "[ Y1D9 ] 가공부 전광판";
            // 
            // panel169
            // 
            this.panel169.BackColor = System.Drawing.Color.Gray;
            this.panel169.Location = new System.Drawing.Point(433, 442);
            this.panel169.Name = "panel169";
            this.panel169.Size = new System.Drawing.Size(40, 40);
            this.panel169.TabIndex = 242;
            // 
            // label170
            // 
            this.label170.AutoSize = true;
            this.label170.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label170.ForeColor = System.Drawing.Color.White;
            this.label170.Location = new System.Drawing.Point(479, 409);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(116, 13);
            this.label170.TabIndex = 241;
            this.label170.Text = "[ Y1D8 ] Spare";
            // 
            // panel170
            // 
            this.panel170.BackColor = System.Drawing.Color.Gray;
            this.panel170.Location = new System.Drawing.Point(433, 396);
            this.panel170.Name = "panel170";
            this.panel170.Size = new System.Drawing.Size(40, 40);
            this.panel170.TabIndex = 240;
            // 
            // label171
            // 
            this.label171.AutoSize = true;
            this.label171.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label171.ForeColor = System.Drawing.Color.White;
            this.label171.Location = new System.Drawing.Point(479, 364);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(116, 13);
            this.label171.TabIndex = 239;
            this.label171.Text = "[ Y1D7 ] Spare";
            // 
            // panel171
            // 
            this.panel171.BackColor = System.Drawing.Color.Gray;
            this.panel171.Location = new System.Drawing.Point(433, 350);
            this.panel171.Name = "panel171";
            this.panel171.Size = new System.Drawing.Size(40, 40);
            this.panel171.TabIndex = 238;
            // 
            // label172
            // 
            this.label172.AutoSize = true;
            this.label172.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label172.ForeColor = System.Drawing.Color.White;
            this.label172.Location = new System.Drawing.Point(479, 319);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(116, 13);
            this.label172.TabIndex = 237;
            this.label172.Text = "[ Y1D6 ] Spare";
            // 
            // panel172
            // 
            this.panel172.BackColor = System.Drawing.Color.Gray;
            this.panel172.Location = new System.Drawing.Point(433, 304);
            this.panel172.Name = "panel172";
            this.panel172.Size = new System.Drawing.Size(40, 40);
            this.panel172.TabIndex = 236;
            // 
            // label173
            // 
            this.label173.AutoSize = true;
            this.label173.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label173.ForeColor = System.Drawing.Color.White;
            this.label173.Location = new System.Drawing.Point(479, 271);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(116, 13);
            this.label173.TabIndex = 235;
            this.label173.Text = "[ Y1D5 ] Spare";
            // 
            // panel173
            // 
            this.panel173.BackColor = System.Drawing.Color.Gray;
            this.panel173.Location = new System.Drawing.Point(433, 258);
            this.panel173.Name = "panel173";
            this.panel173.Size = new System.Drawing.Size(40, 40);
            this.panel173.TabIndex = 234;
            // 
            // label174
            // 
            this.label174.AutoSize = true;
            this.label174.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label174.ForeColor = System.Drawing.Color.White;
            this.label174.Location = new System.Drawing.Point(479, 226);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(116, 13);
            this.label174.TabIndex = 233;
            this.label174.Text = "[ Y1D4 ] Spare";
            // 
            // panel174
            // 
            this.panel174.BackColor = System.Drawing.Color.Gray;
            this.panel174.Location = new System.Drawing.Point(433, 212);
            this.panel174.Name = "panel174";
            this.panel174.Size = new System.Drawing.Size(40, 40);
            this.panel174.TabIndex = 232;
            // 
            // label175
            // 
            this.label175.AutoSize = true;
            this.label175.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label175.ForeColor = System.Drawing.Color.White;
            this.label175.Location = new System.Drawing.Point(479, 179);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(116, 13);
            this.label175.TabIndex = 231;
            this.label175.Text = "[ Y1D3 ] Spare";
            // 
            // panel175
            // 
            this.panel175.BackColor = System.Drawing.Color.Gray;
            this.panel175.Location = new System.Drawing.Point(433, 166);
            this.panel175.Name = "panel175";
            this.panel175.Size = new System.Drawing.Size(40, 40);
            this.panel175.TabIndex = 230;
            // 
            // label176
            // 
            this.label176.AutoSize = true;
            this.label176.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label176.ForeColor = System.Drawing.Color.White;
            this.label176.Location = new System.Drawing.Point(479, 133);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(254, 13);
            this.label176.TabIndex = 229;
            this.label176.Text = "[ Y1D2 ] 설비 내부 형광등 3 ON/OFF";
            // 
            // panel176
            // 
            this.panel176.BackColor = System.Drawing.Color.Gray;
            this.panel176.Location = new System.Drawing.Point(433, 120);
            this.panel176.Name = "panel176";
            this.panel176.Size = new System.Drawing.Size(40, 40);
            this.panel176.TabIndex = 228;
            // 
            // label177
            // 
            this.label177.AutoSize = true;
            this.label177.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label177.ForeColor = System.Drawing.Color.White;
            this.label177.Location = new System.Drawing.Point(479, 86);
            this.label177.Name = "label177";
            this.label177.Size = new System.Drawing.Size(254, 13);
            this.label177.TabIndex = 227;
            this.label177.Text = "[ Y1D1 ] 설비 내부 형광등 2 ON/OFF";
            // 
            // panel177
            // 
            this.panel177.BackColor = System.Drawing.Color.Gray;
            this.panel177.Location = new System.Drawing.Point(433, 74);
            this.panel177.Name = "panel177";
            this.panel177.Size = new System.Drawing.Size(40, 40);
            this.panel177.TabIndex = 226;
            // 
            // label178
            // 
            this.label178.AutoSize = true;
            this.label178.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label178.ForeColor = System.Drawing.Color.White;
            this.label178.Location = new System.Drawing.Point(479, 42);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(254, 13);
            this.label178.TabIndex = 225;
            this.label178.Text = "[ Y1D0 ] 설비 내부 형광등 1 ON/OFF";
            // 
            // panel178
            // 
            this.panel178.BackColor = System.Drawing.Color.Gray;
            this.panel178.Location = new System.Drawing.Point(433, 28);
            this.panel178.Name = "panel178";
            this.panel178.Size = new System.Drawing.Size(40, 40);
            this.panel178.TabIndex = 224;
            // 
            // label179
            // 
            this.label179.AutoSize = true;
            this.label179.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label179.ForeColor = System.Drawing.Color.White;
            this.label179.Location = new System.Drawing.Point(52, 730);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(240, 13);
            this.label179.TabIndex = 223;
            this.label179.Text = "[ Y1CF ] Spare 파기 AIR ON CMD";
            // 
            // panel179
            // 
            this.panel179.BackColor = System.Drawing.Color.Gray;
            this.panel179.Location = new System.Drawing.Point(6, 718);
            this.panel179.Name = "panel179";
            this.panel179.Size = new System.Drawing.Size(40, 40);
            this.panel179.TabIndex = 222;
            // 
            // label180
            // 
            this.label180.AutoSize = true;
            this.label180.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label180.ForeColor = System.Drawing.Color.White;
            this.label180.Location = new System.Drawing.Point(52, 685);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(241, 13);
            this.label180.TabIndex = 221;
            this.label180.Text = "[ Y1CE ] Spare 파기 AIR ON CMD";
            // 
            // panel180
            // 
            this.panel180.BackColor = System.Drawing.Color.Gray;
            this.panel180.Location = new System.Drawing.Point(6, 672);
            this.panel180.Name = "panel180";
            this.panel180.Size = new System.Drawing.Size(40, 40);
            this.panel180.TabIndex = 220;
            // 
            // label181
            // 
            this.label181.AutoSize = true;
            this.label181.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label181.ForeColor = System.Drawing.Color.White;
            this.label181.Location = new System.Drawing.Point(52, 640);
            this.label181.Name = "label181";
            this.label181.Size = new System.Drawing.Size(245, 13);
            this.label181.TabIndex = 219;
            this.label181.Text = "[ Y1CD ] Spare SOL V/V ON CMD";
            // 
            // panel181
            // 
            this.panel181.BackColor = System.Drawing.Color.Gray;
            this.panel181.Location = new System.Drawing.Point(6, 626);
            this.panel181.Name = "panel181";
            this.panel181.Size = new System.Drawing.Size(40, 40);
            this.panel181.TabIndex = 218;
            // 
            // label182
            // 
            this.label182.AutoSize = true;
            this.label182.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label182.ForeColor = System.Drawing.Color.White;
            this.label182.Location = new System.Drawing.Point(52, 594);
            this.label182.Name = "label182";
            this.label182.Size = new System.Drawing.Size(245, 13);
            this.label182.TabIndex = 217;
            this.label182.Text = "[ Y1CC ] Spare SOL V/V ON CMD";
            // 
            // panel182
            // 
            this.panel182.BackColor = System.Drawing.Color.Gray;
            this.panel182.Location = new System.Drawing.Point(6, 580);
            this.panel182.Name = "panel182";
            this.panel182.Size = new System.Drawing.Size(40, 40);
            this.panel182.TabIndex = 216;
            // 
            // label183
            // 
            this.label183.AutoSize = true;
            this.label183.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label183.ForeColor = System.Drawing.Color.White;
            this.label183.Location = new System.Drawing.Point(52, 548);
            this.label183.Name = "label183";
            this.label183.Size = new System.Drawing.Size(245, 13);
            this.label183.TabIndex = 215;
            this.label183.Text = "[ Y1CB ] Spare SOL V/V ON CMD";
            // 
            // panel183
            // 
            this.panel183.BackColor = System.Drawing.Color.Gray;
            this.panel183.Location = new System.Drawing.Point(6, 534);
            this.panel183.Name = "panel183";
            this.panel183.Size = new System.Drawing.Size(40, 40);
            this.panel183.TabIndex = 214;
            // 
            // label184
            // 
            this.label184.AutoSize = true;
            this.label184.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label184.ForeColor = System.Drawing.Color.White;
            this.label184.Location = new System.Drawing.Point(52, 501);
            this.label184.Name = "label184";
            this.label184.Size = new System.Drawing.Size(244, 13);
            this.label184.TabIndex = 213;
            this.label184.Text = "[ Y1CA ] Spare SOL V/V ON CMD";
            // 
            // panel184
            // 
            this.panel184.BackColor = System.Drawing.Color.Gray;
            this.panel184.Location = new System.Drawing.Point(6, 488);
            this.panel184.Name = "panel184";
            this.panel184.Size = new System.Drawing.Size(40, 40);
            this.panel184.TabIndex = 212;
            // 
            // label185
            // 
            this.label185.AutoSize = true;
            this.label185.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label185.ForeColor = System.Drawing.Color.White;
            this.label185.Location = new System.Drawing.Point(52, 454);
            this.label185.Name = "label185";
            this.label185.Size = new System.Drawing.Size(116, 13);
            this.label185.TabIndex = 211;
            this.label185.Text = "[ Y1C9 ] Spare";
            // 
            // panel185
            // 
            this.panel185.BackColor = System.Drawing.Color.Gray;
            this.panel185.Location = new System.Drawing.Point(6, 442);
            this.panel185.Name = "panel185";
            this.panel185.Size = new System.Drawing.Size(40, 40);
            this.panel185.TabIndex = 210;
            // 
            // label186
            // 
            this.label186.AutoSize = true;
            this.label186.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label186.ForeColor = System.Drawing.Color.White;
            this.label186.Location = new System.Drawing.Point(52, 409);
            this.label186.Name = "label186";
            this.label186.Size = new System.Drawing.Size(116, 13);
            this.label186.TabIndex = 209;
            this.label186.Text = "[ Y1C8 ] Spare";
            // 
            // panel186
            // 
            this.panel186.BackColor = System.Drawing.Color.Gray;
            this.panel186.Location = new System.Drawing.Point(6, 396);
            this.panel186.Name = "panel186";
            this.panel186.Size = new System.Drawing.Size(40, 40);
            this.panel186.TabIndex = 208;
            // 
            // label187
            // 
            this.label187.AutoSize = true;
            this.label187.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label187.ForeColor = System.Drawing.Color.White;
            this.label187.Location = new System.Drawing.Point(52, 364);
            this.label187.Name = "label187";
            this.label187.Size = new System.Drawing.Size(116, 13);
            this.label187.TabIndex = 207;
            this.label187.Text = "[ Y1C7 ] Spare";
            // 
            // panel187
            // 
            this.panel187.BackColor = System.Drawing.Color.Gray;
            this.panel187.Location = new System.Drawing.Point(6, 350);
            this.panel187.Name = "panel187";
            this.panel187.Size = new System.Drawing.Size(40, 40);
            this.panel187.TabIndex = 206;
            // 
            // label188
            // 
            this.label188.AutoSize = true;
            this.label188.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label188.ForeColor = System.Drawing.Color.White;
            this.label188.Location = new System.Drawing.Point(52, 319);
            this.label188.Name = "label188";
            this.label188.Size = new System.Drawing.Size(240, 13);
            this.label188.TabIndex = 205;
            this.label188.Text = "[ Y1C6 ] 7 Safety Door SW Open";
            // 
            // panel188
            // 
            this.panel188.BackColor = System.Drawing.Color.Gray;
            this.panel188.Location = new System.Drawing.Point(6, 304);
            this.panel188.Name = "panel188";
            this.panel188.Size = new System.Drawing.Size(40, 40);
            this.panel188.TabIndex = 204;
            // 
            // label189
            // 
            this.label189.AutoSize = true;
            this.label189.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label189.ForeColor = System.Drawing.Color.White;
            this.label189.Location = new System.Drawing.Point(52, 271);
            this.label189.Name = "label189";
            this.label189.Size = new System.Drawing.Size(240, 13);
            this.label189.TabIndex = 203;
            this.label189.Text = "[ Y1C5 ] 6 Safety Door SW Open";
            // 
            // panel189
            // 
            this.panel189.BackColor = System.Drawing.Color.Gray;
            this.panel189.Location = new System.Drawing.Point(6, 258);
            this.panel189.Name = "panel189";
            this.panel189.Size = new System.Drawing.Size(40, 40);
            this.panel189.TabIndex = 202;
            // 
            // label190
            // 
            this.label190.AutoSize = true;
            this.label190.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label190.ForeColor = System.Drawing.Color.White;
            this.label190.Location = new System.Drawing.Point(52, 226);
            this.label190.Name = "label190";
            this.label190.Size = new System.Drawing.Size(240, 13);
            this.label190.TabIndex = 201;
            this.label190.Text = "[ Y1C4 ] 5 Safety Door SW Open";
            // 
            // panel190
            // 
            this.panel190.BackColor = System.Drawing.Color.Gray;
            this.panel190.Location = new System.Drawing.Point(6, 212);
            this.panel190.Name = "panel190";
            this.panel190.Size = new System.Drawing.Size(40, 40);
            this.panel190.TabIndex = 200;
            // 
            // label191
            // 
            this.label191.AutoSize = true;
            this.label191.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label191.ForeColor = System.Drawing.Color.White;
            this.label191.Location = new System.Drawing.Point(52, 179);
            this.label191.Name = "label191";
            this.label191.Size = new System.Drawing.Size(240, 13);
            this.label191.TabIndex = 199;
            this.label191.Text = "[ Y1C3 ] 4 Safety Door SW Open";
            // 
            // panel191
            // 
            this.panel191.BackColor = System.Drawing.Color.Gray;
            this.panel191.Location = new System.Drawing.Point(6, 166);
            this.panel191.Name = "panel191";
            this.panel191.Size = new System.Drawing.Size(40, 40);
            this.panel191.TabIndex = 198;
            // 
            // label192
            // 
            this.label192.AutoSize = true;
            this.label192.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label192.ForeColor = System.Drawing.Color.White;
            this.label192.Location = new System.Drawing.Point(52, 133);
            this.label192.Name = "label192";
            this.label192.Size = new System.Drawing.Size(240, 13);
            this.label192.TabIndex = 197;
            this.label192.Text = "[ Y1C2 ] 3 Safety Door SW Open";
            // 
            // panel192
            // 
            this.panel192.BackColor = System.Drawing.Color.Gray;
            this.panel192.Location = new System.Drawing.Point(6, 120);
            this.panel192.Name = "panel192";
            this.panel192.Size = new System.Drawing.Size(40, 40);
            this.panel192.TabIndex = 196;
            // 
            // label193
            // 
            this.label193.AutoSize = true;
            this.label193.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label193.ForeColor = System.Drawing.Color.White;
            this.label193.Location = new System.Drawing.Point(52, 86);
            this.label193.Name = "label193";
            this.label193.Size = new System.Drawing.Size(240, 13);
            this.label193.TabIndex = 195;
            this.label193.Text = "[ Y1C1 ] 2 Safety Door SW Open";
            // 
            // panel193
            // 
            this.panel193.BackColor = System.Drawing.Color.Gray;
            this.panel193.Location = new System.Drawing.Point(6, 74);
            this.panel193.Name = "panel193";
            this.panel193.Size = new System.Drawing.Size(40, 40);
            this.panel193.TabIndex = 194;
            // 
            // label194
            // 
            this.label194.AutoSize = true;
            this.label194.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label194.ForeColor = System.Drawing.Color.White;
            this.label194.Location = new System.Drawing.Point(52, 42);
            this.label194.Name = "label194";
            this.label194.Size = new System.Drawing.Size(240, 13);
            this.label194.TabIndex = 193;
            this.label194.Text = "[ Y1C0 ] 1 Safety Door SW Open";
            // 
            // panel194
            // 
            this.panel194.BackColor = System.Drawing.Color.Gray;
            this.panel194.Location = new System.Drawing.Point(6, 28);
            this.panel194.Name = "panel194";
            this.panel194.Size = new System.Drawing.Size(40, 40);
            this.panel194.TabIndex = 192;
            // 
            // tp_iostatus_process_out3
            // 
            this.tp_iostatus_process_out3.BackColor = System.Drawing.Color.DimGray;
            this.tp_iostatus_process_out3.Controls.Add(this.label195);
            this.tp_iostatus_process_out3.Controls.Add(this.panel195);
            this.tp_iostatus_process_out3.Controls.Add(this.label196);
            this.tp_iostatus_process_out3.Controls.Add(this.panel196);
            this.tp_iostatus_process_out3.Controls.Add(this.label197);
            this.tp_iostatus_process_out3.Controls.Add(this.panel197);
            this.tp_iostatus_process_out3.Controls.Add(this.label198);
            this.tp_iostatus_process_out3.Controls.Add(this.panel198);
            this.tp_iostatus_process_out3.Controls.Add(this.label199);
            this.tp_iostatus_process_out3.Controls.Add(this.panel199);
            this.tp_iostatus_process_out3.Controls.Add(this.label200);
            this.tp_iostatus_process_out3.Controls.Add(this.panel200);
            this.tp_iostatus_process_out3.Controls.Add(this.label201);
            this.tp_iostatus_process_out3.Controls.Add(this.panel201);
            this.tp_iostatus_process_out3.Controls.Add(this.label202);
            this.tp_iostatus_process_out3.Controls.Add(this.panel202);
            this.tp_iostatus_process_out3.Controls.Add(this.label203);
            this.tp_iostatus_process_out3.Controls.Add(this.panel203);
            this.tp_iostatus_process_out3.Controls.Add(this.label204);
            this.tp_iostatus_process_out3.Controls.Add(this.panel204);
            this.tp_iostatus_process_out3.Controls.Add(this.label205);
            this.tp_iostatus_process_out3.Controls.Add(this.panel205);
            this.tp_iostatus_process_out3.Controls.Add(this.label206);
            this.tp_iostatus_process_out3.Controls.Add(this.panel206);
            this.tp_iostatus_process_out3.Controls.Add(this.label207);
            this.tp_iostatus_process_out3.Controls.Add(this.panel207);
            this.tp_iostatus_process_out3.Controls.Add(this.label208);
            this.tp_iostatus_process_out3.Controls.Add(this.panel208);
            this.tp_iostatus_process_out3.Controls.Add(this.label209);
            this.tp_iostatus_process_out3.Controls.Add(this.panel209);
            this.tp_iostatus_process_out3.Controls.Add(this.label210);
            this.tp_iostatus_process_out3.Controls.Add(this.panel210);
            this.tp_iostatus_process_out3.Controls.Add(this.label211);
            this.tp_iostatus_process_out3.Controls.Add(this.panel211);
            this.tp_iostatus_process_out3.Controls.Add(this.label212);
            this.tp_iostatus_process_out3.Controls.Add(this.panel212);
            this.tp_iostatus_process_out3.Controls.Add(this.label213);
            this.tp_iostatus_process_out3.Controls.Add(this.panel213);
            this.tp_iostatus_process_out3.Controls.Add(this.label214);
            this.tp_iostatus_process_out3.Controls.Add(this.panel214);
            this.tp_iostatus_process_out3.Controls.Add(this.label215);
            this.tp_iostatus_process_out3.Controls.Add(this.panel215);
            this.tp_iostatus_process_out3.Controls.Add(this.label216);
            this.tp_iostatus_process_out3.Controls.Add(this.panel216);
            this.tp_iostatus_process_out3.Controls.Add(this.label217);
            this.tp_iostatus_process_out3.Controls.Add(this.panel217);
            this.tp_iostatus_process_out3.Controls.Add(this.label218);
            this.tp_iostatus_process_out3.Controls.Add(this.panel218);
            this.tp_iostatus_process_out3.Controls.Add(this.label219);
            this.tp_iostatus_process_out3.Controls.Add(this.panel219);
            this.tp_iostatus_process_out3.Controls.Add(this.label220);
            this.tp_iostatus_process_out3.Controls.Add(this.panel220);
            this.tp_iostatus_process_out3.Controls.Add(this.label221);
            this.tp_iostatus_process_out3.Controls.Add(this.panel221);
            this.tp_iostatus_process_out3.Controls.Add(this.label222);
            this.tp_iostatus_process_out3.Controls.Add(this.panel222);
            this.tp_iostatus_process_out3.Controls.Add(this.label223);
            this.tp_iostatus_process_out3.Controls.Add(this.panel223);
            this.tp_iostatus_process_out3.Controls.Add(this.label224);
            this.tp_iostatus_process_out3.Controls.Add(this.panel224);
            this.tp_iostatus_process_out3.Controls.Add(this.label225);
            this.tp_iostatus_process_out3.Controls.Add(this.panel225);
            this.tp_iostatus_process_out3.Controls.Add(this.label226);
            this.tp_iostatus_process_out3.Controls.Add(this.panel226);
            this.tp_iostatus_process_out3.Location = new System.Drawing.Point(4, 34);
            this.tp_iostatus_process_out3.Name = "tp_iostatus_process_out3";
            this.tp_iostatus_process_out3.Padding = new System.Windows.Forms.Padding(3);
            this.tp_iostatus_process_out3.Size = new System.Drawing.Size(846, 778);
            this.tp_iostatus_process_out3.TabIndex = 3;
            this.tp_iostatus_process_out3.Text = "OUT - 3";
            // 
            // label195
            // 
            this.label195.AutoSize = true;
            this.label195.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label195.ForeColor = System.Drawing.Color.White;
            this.label195.Location = new System.Drawing.Point(479, 730);
            this.label195.Name = "label195";
            this.label195.Size = new System.Drawing.Size(114, 13);
            this.label195.TabIndex = 319;
            this.label195.Text = "[ Y1FF ] Spare";
            // 
            // panel195
            // 
            this.panel195.BackColor = System.Drawing.Color.Gray;
            this.panel195.Location = new System.Drawing.Point(433, 718);
            this.panel195.Name = "panel195";
            this.panel195.Size = new System.Drawing.Size(40, 40);
            this.panel195.TabIndex = 318;
            // 
            // label196
            // 
            this.label196.AutoSize = true;
            this.label196.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label196.ForeColor = System.Drawing.Color.White;
            this.label196.Location = new System.Drawing.Point(479, 685);
            this.label196.Name = "label196";
            this.label196.Size = new System.Drawing.Size(115, 13);
            this.label196.TabIndex = 317;
            this.label196.Text = "[ Y1FE ] Spare";
            // 
            // panel196
            // 
            this.panel196.BackColor = System.Drawing.Color.Gray;
            this.panel196.Location = new System.Drawing.Point(433, 672);
            this.panel196.Name = "panel196";
            this.panel196.Size = new System.Drawing.Size(40, 40);
            this.panel196.TabIndex = 316;
            // 
            // label197
            // 
            this.label197.AutoSize = true;
            this.label197.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label197.ForeColor = System.Drawing.Color.White;
            this.label197.Location = new System.Drawing.Point(479, 640);
            this.label197.Name = "label197";
            this.label197.Size = new System.Drawing.Size(116, 13);
            this.label197.TabIndex = 315;
            this.label197.Text = "[ Y1FD ] Spare";
            // 
            // panel197
            // 
            this.panel197.BackColor = System.Drawing.Color.Gray;
            this.panel197.Location = new System.Drawing.Point(433, 626);
            this.panel197.Name = "panel197";
            this.panel197.Size = new System.Drawing.Size(40, 40);
            this.panel197.TabIndex = 314;
            // 
            // label198
            // 
            this.label198.AutoSize = true;
            this.label198.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label198.ForeColor = System.Drawing.Color.White;
            this.label198.Location = new System.Drawing.Point(479, 594);
            this.label198.Name = "label198";
            this.label198.Size = new System.Drawing.Size(116, 13);
            this.label198.TabIndex = 313;
            this.label198.Text = "[ Y1FC ] Spare";
            // 
            // panel198
            // 
            this.panel198.BackColor = System.Drawing.Color.Gray;
            this.panel198.Location = new System.Drawing.Point(433, 580);
            this.panel198.Name = "panel198";
            this.panel198.Size = new System.Drawing.Size(40, 40);
            this.panel198.TabIndex = 312;
            // 
            // label199
            // 
            this.label199.AutoSize = true;
            this.label199.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label199.ForeColor = System.Drawing.Color.White;
            this.label199.Location = new System.Drawing.Point(479, 548);
            this.label199.Name = "label199";
            this.label199.Size = new System.Drawing.Size(116, 13);
            this.label199.TabIndex = 311;
            this.label199.Text = "[ Y1FB ] Spare";
            // 
            // panel199
            // 
            this.panel199.BackColor = System.Drawing.Color.Gray;
            this.panel199.Location = new System.Drawing.Point(433, 534);
            this.panel199.Name = "panel199";
            this.panel199.Size = new System.Drawing.Size(40, 40);
            this.panel199.TabIndex = 310;
            // 
            // label200
            // 
            this.label200.AutoSize = true;
            this.label200.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label200.ForeColor = System.Drawing.Color.White;
            this.label200.Location = new System.Drawing.Point(479, 501);
            this.label200.Name = "label200";
            this.label200.Size = new System.Drawing.Size(115, 13);
            this.label200.TabIndex = 309;
            this.label200.Text = "[ Y1FA ] Spare";
            // 
            // panel200
            // 
            this.panel200.BackColor = System.Drawing.Color.Gray;
            this.panel200.Location = new System.Drawing.Point(433, 488);
            this.panel200.Name = "panel200";
            this.panel200.Size = new System.Drawing.Size(40, 40);
            this.panel200.TabIndex = 308;
            // 
            // label201
            // 
            this.label201.AutoSize = true;
            this.label201.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label201.ForeColor = System.Drawing.Color.White;
            this.label201.Location = new System.Drawing.Point(479, 454);
            this.label201.Name = "label201";
            this.label201.Size = new System.Drawing.Size(114, 13);
            this.label201.TabIndex = 307;
            this.label201.Text = "[ Y1F9 ] Spare";
            // 
            // panel201
            // 
            this.panel201.BackColor = System.Drawing.Color.Gray;
            this.panel201.Location = new System.Drawing.Point(433, 442);
            this.panel201.Name = "panel201";
            this.panel201.Size = new System.Drawing.Size(40, 40);
            this.panel201.TabIndex = 306;
            // 
            // label202
            // 
            this.label202.AutoSize = true;
            this.label202.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label202.ForeColor = System.Drawing.Color.White;
            this.label202.Location = new System.Drawing.Point(479, 409);
            this.label202.Name = "label202";
            this.label202.Size = new System.Drawing.Size(114, 13);
            this.label202.TabIndex = 305;
            this.label202.Text = "[ Y1F8 ] Spare";
            // 
            // panel202
            // 
            this.panel202.BackColor = System.Drawing.Color.Gray;
            this.panel202.Location = new System.Drawing.Point(433, 396);
            this.panel202.Name = "panel202";
            this.panel202.Size = new System.Drawing.Size(40, 40);
            this.panel202.TabIndex = 304;
            // 
            // label203
            // 
            this.label203.AutoSize = true;
            this.label203.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label203.ForeColor = System.Drawing.Color.White;
            this.label203.Location = new System.Drawing.Point(479, 364);
            this.label203.Name = "label203";
            this.label203.Size = new System.Drawing.Size(114, 13);
            this.label203.TabIndex = 303;
            this.label203.Text = "[ Y1F7 ] Spare";
            // 
            // panel203
            // 
            this.panel203.BackColor = System.Drawing.Color.Gray;
            this.panel203.Location = new System.Drawing.Point(433, 350);
            this.panel203.Name = "panel203";
            this.panel203.Size = new System.Drawing.Size(40, 40);
            this.panel203.TabIndex = 302;
            // 
            // label204
            // 
            this.label204.AutoSize = true;
            this.label204.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label204.ForeColor = System.Drawing.Color.White;
            this.label204.Location = new System.Drawing.Point(479, 319);
            this.label204.Name = "label204";
            this.label204.Size = new System.Drawing.Size(114, 13);
            this.label204.TabIndex = 301;
            this.label204.Text = "[ Y1F6 ] Spare";
            // 
            // panel204
            // 
            this.panel204.BackColor = System.Drawing.Color.Gray;
            this.panel204.Location = new System.Drawing.Point(433, 304);
            this.panel204.Name = "panel204";
            this.panel204.Size = new System.Drawing.Size(40, 40);
            this.panel204.TabIndex = 300;
            // 
            // label205
            // 
            this.label205.AutoSize = true;
            this.label205.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label205.ForeColor = System.Drawing.Color.White;
            this.label205.Location = new System.Drawing.Point(479, 271);
            this.label205.Name = "label205";
            this.label205.Size = new System.Drawing.Size(114, 13);
            this.label205.TabIndex = 299;
            this.label205.Text = "[ Y1F5 ] Spare";
            // 
            // panel205
            // 
            this.panel205.BackColor = System.Drawing.Color.Gray;
            this.panel205.Location = new System.Drawing.Point(433, 258);
            this.panel205.Name = "panel205";
            this.panel205.Size = new System.Drawing.Size(40, 40);
            this.panel205.TabIndex = 298;
            // 
            // label206
            // 
            this.label206.AutoSize = true;
            this.label206.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label206.ForeColor = System.Drawing.Color.White;
            this.label206.Location = new System.Drawing.Point(479, 226);
            this.label206.Name = "label206";
            this.label206.Size = new System.Drawing.Size(114, 13);
            this.label206.TabIndex = 297;
            this.label206.Text = "[ Y1F4 ] Spare";
            // 
            // panel206
            // 
            this.panel206.BackColor = System.Drawing.Color.Gray;
            this.panel206.Location = new System.Drawing.Point(433, 212);
            this.panel206.Name = "panel206";
            this.panel206.Size = new System.Drawing.Size(40, 40);
            this.panel206.TabIndex = 296;
            // 
            // label207
            // 
            this.label207.AutoSize = true;
            this.label207.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label207.ForeColor = System.Drawing.Color.White;
            this.label207.Location = new System.Drawing.Point(479, 179);
            this.label207.Name = "label207";
            this.label207.Size = new System.Drawing.Size(114, 13);
            this.label207.TabIndex = 295;
            this.label207.Text = "[ Y1F3 ] Spare";
            // 
            // panel207
            // 
            this.panel207.BackColor = System.Drawing.Color.Gray;
            this.panel207.Location = new System.Drawing.Point(433, 166);
            this.panel207.Name = "panel207";
            this.panel207.Size = new System.Drawing.Size(40, 40);
            this.panel207.TabIndex = 294;
            // 
            // label208
            // 
            this.label208.AutoSize = true;
            this.label208.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label208.ForeColor = System.Drawing.Color.White;
            this.label208.Location = new System.Drawing.Point(479, 133);
            this.label208.Name = "label208";
            this.label208.Size = new System.Drawing.Size(114, 13);
            this.label208.TabIndex = 293;
            this.label208.Text = "[ Y1F2 ] Spare";
            // 
            // panel208
            // 
            this.panel208.BackColor = System.Drawing.Color.Gray;
            this.panel208.Location = new System.Drawing.Point(433, 120);
            this.panel208.Name = "panel208";
            this.panel208.Size = new System.Drawing.Size(40, 40);
            this.panel208.TabIndex = 292;
            // 
            // label209
            // 
            this.label209.AutoSize = true;
            this.label209.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label209.ForeColor = System.Drawing.Color.White;
            this.label209.Location = new System.Drawing.Point(479, 86);
            this.label209.Name = "label209";
            this.label209.Size = new System.Drawing.Size(114, 13);
            this.label209.TabIndex = 291;
            this.label209.Text = "[ Y1F1 ] Spare";
            // 
            // panel209
            // 
            this.panel209.BackColor = System.Drawing.Color.Gray;
            this.panel209.Location = new System.Drawing.Point(433, 74);
            this.panel209.Name = "panel209";
            this.panel209.Size = new System.Drawing.Size(40, 40);
            this.panel209.TabIndex = 290;
            // 
            // label210
            // 
            this.label210.AutoSize = true;
            this.label210.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label210.ForeColor = System.Drawing.Color.White;
            this.label210.Location = new System.Drawing.Point(479, 42);
            this.label210.Name = "label210";
            this.label210.Size = new System.Drawing.Size(114, 13);
            this.label210.TabIndex = 289;
            this.label210.Text = "[ Y1F0 ] Spare";
            // 
            // panel210
            // 
            this.panel210.BackColor = System.Drawing.Color.Gray;
            this.panel210.Location = new System.Drawing.Point(433, 28);
            this.panel210.Name = "panel210";
            this.panel210.Size = new System.Drawing.Size(40, 40);
            this.panel210.TabIndex = 288;
            // 
            // label211
            // 
            this.label211.AutoSize = true;
            this.label211.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label211.ForeColor = System.Drawing.Color.White;
            this.label211.Location = new System.Drawing.Point(52, 730);
            this.label211.Name = "label211";
            this.label211.Size = new System.Drawing.Size(115, 13);
            this.label211.TabIndex = 287;
            this.label211.Text = "[ Y1EF ] Spare";
            // 
            // panel211
            // 
            this.panel211.BackColor = System.Drawing.Color.Gray;
            this.panel211.Location = new System.Drawing.Point(6, 718);
            this.panel211.Name = "panel211";
            this.panel211.Size = new System.Drawing.Size(40, 40);
            this.panel211.TabIndex = 286;
            // 
            // label212
            // 
            this.label212.AutoSize = true;
            this.label212.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label212.ForeColor = System.Drawing.Color.White;
            this.label212.Location = new System.Drawing.Point(52, 685);
            this.label212.Name = "label212";
            this.label212.Size = new System.Drawing.Size(219, 13);
            this.label212.TabIndex = 285;
            this.label212.Text = "[ Y1EE ] load 뮤팅 down lamp";
            // 
            // panel212
            // 
            this.panel212.BackColor = System.Drawing.Color.Gray;
            this.panel212.Location = new System.Drawing.Point(6, 672);
            this.panel212.Name = "panel212";
            this.panel212.Size = new System.Drawing.Size(40, 40);
            this.panel212.TabIndex = 284;
            // 
            // label213
            // 
            this.label213.AutoSize = true;
            this.label213.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label213.ForeColor = System.Drawing.Color.White;
            this.label213.Location = new System.Drawing.Point(52, 640);
            this.label213.Name = "label213";
            this.label213.Size = new System.Drawing.Size(200, 13);
            this.label213.TabIndex = 283;
            this.label213.Text = "[ Y1ED ] load 뮤팅 up lamp";
            // 
            // panel213
            // 
            this.panel213.BackColor = System.Drawing.Color.Gray;
            this.panel213.Location = new System.Drawing.Point(6, 626);
            this.panel213.Name = "panel213";
            this.panel213.Size = new System.Drawing.Size(40, 40);
            this.panel213.TabIndex = 282;
            // 
            // label214
            // 
            this.label214.AutoSize = true;
            this.label214.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label214.ForeColor = System.Drawing.Color.White;
            this.label214.Location = new System.Drawing.Point(52, 594);
            this.label214.Name = "label214";
            this.label214.Size = new System.Drawing.Size(117, 13);
            this.label214.TabIndex = 281;
            this.label214.Text = "[ Y1EC ] Spare";
            // 
            // panel214
            // 
            this.panel214.BackColor = System.Drawing.Color.Gray;
            this.panel214.Location = new System.Drawing.Point(6, 580);
            this.panel214.Name = "panel214";
            this.panel214.Size = new System.Drawing.Size(40, 40);
            this.panel214.TabIndex = 280;
            // 
            // label215
            // 
            this.label215.AutoSize = true;
            this.label215.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label215.ForeColor = System.Drawing.Color.White;
            this.label215.Location = new System.Drawing.Point(52, 548);
            this.label215.Name = "label215";
            this.label215.Size = new System.Drawing.Size(271, 13);
            this.label215.TabIndex = 279;
            this.label215.Text = "[ Y1EB ] 4-2 L/D Lifter Muting On/Off";
            // 
            // panel215
            // 
            this.panel215.BackColor = System.Drawing.Color.Gray;
            this.panel215.Location = new System.Drawing.Point(6, 534);
            this.panel215.Name = "panel215";
            this.panel215.Size = new System.Drawing.Size(40, 40);
            this.panel215.TabIndex = 278;
            // 
            // label216
            // 
            this.label216.AutoSize = true;
            this.label216.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label216.ForeColor = System.Drawing.Color.White;
            this.label216.Location = new System.Drawing.Point(52, 501);
            this.label216.Name = "label216";
            this.label216.Size = new System.Drawing.Size(270, 13);
            this.label216.TabIndex = 277;
            this.label216.Text = "[ Y1EA ] 4-1 L/D Lifter Muting On/Off";
            // 
            // panel216
            // 
            this.panel216.BackColor = System.Drawing.Color.Gray;
            this.panel216.Location = new System.Drawing.Point(6, 488);
            this.panel216.Name = "panel216";
            this.panel216.Size = new System.Drawing.Size(40, 40);
            this.panel216.TabIndex = 276;
            // 
            // label217
            // 
            this.label217.AutoSize = true;
            this.label217.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label217.ForeColor = System.Drawing.Color.White;
            this.label217.Location = new System.Drawing.Point(52, 454);
            this.label217.Name = "label217";
            this.label217.Size = new System.Drawing.Size(269, 13);
            this.label217.TabIndex = 275;
            this.label217.Text = "[ Y1E9 ] 3-2 L/D Lifter Muting On/Off";
            // 
            // panel217
            // 
            this.panel217.BackColor = System.Drawing.Color.Gray;
            this.panel217.Location = new System.Drawing.Point(6, 442);
            this.panel217.Name = "panel217";
            this.panel217.Size = new System.Drawing.Size(40, 40);
            this.panel217.TabIndex = 274;
            // 
            // label218
            // 
            this.label218.AutoSize = true;
            this.label218.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label218.ForeColor = System.Drawing.Color.White;
            this.label218.Location = new System.Drawing.Point(52, 409);
            this.label218.Name = "label218";
            this.label218.Size = new System.Drawing.Size(269, 13);
            this.label218.TabIndex = 273;
            this.label218.Text = "[ Y1E8 ] 3-1 L/D Lifter Muting On/Off";
            // 
            // panel218
            // 
            this.panel218.BackColor = System.Drawing.Color.Gray;
            this.panel218.Location = new System.Drawing.Point(6, 396);
            this.panel218.Name = "panel218";
            this.panel218.Size = new System.Drawing.Size(40, 40);
            this.panel218.TabIndex = 272;
            // 
            // label219
            // 
            this.label219.AutoSize = true;
            this.label219.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label219.ForeColor = System.Drawing.Color.White;
            this.label219.Location = new System.Drawing.Point(52, 364);
            this.label219.Name = "label219";
            this.label219.Size = new System.Drawing.Size(269, 13);
            this.label219.TabIndex = 271;
            this.label219.Text = "[ Y1E7 ] 2-2 L/D Lifter Muting On/Off";
            // 
            // panel219
            // 
            this.panel219.BackColor = System.Drawing.Color.Gray;
            this.panel219.Location = new System.Drawing.Point(6, 350);
            this.panel219.Name = "panel219";
            this.panel219.Size = new System.Drawing.Size(40, 40);
            this.panel219.TabIndex = 270;
            // 
            // label220
            // 
            this.label220.AutoSize = true;
            this.label220.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label220.ForeColor = System.Drawing.Color.White;
            this.label220.Location = new System.Drawing.Point(52, 319);
            this.label220.Name = "label220";
            this.label220.Size = new System.Drawing.Size(269, 13);
            this.label220.TabIndex = 269;
            this.label220.Text = "[ Y1E6 ] 2-1 L/D Lifter Muting On/Off";
            // 
            // panel220
            // 
            this.panel220.BackColor = System.Drawing.Color.Gray;
            this.panel220.Location = new System.Drawing.Point(6, 304);
            this.panel220.Name = "panel220";
            this.panel220.Size = new System.Drawing.Size(40, 40);
            this.panel220.TabIndex = 268;
            // 
            // label221
            // 
            this.label221.AutoSize = true;
            this.label221.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label221.ForeColor = System.Drawing.Color.White;
            this.label221.Location = new System.Drawing.Point(52, 271);
            this.label221.Name = "label221";
            this.label221.Size = new System.Drawing.Size(269, 13);
            this.label221.TabIndex = 267;
            this.label221.Text = "[ Y1E5 ] 1-2 L/D Lifter Muting On/Off";
            // 
            // panel221
            // 
            this.panel221.BackColor = System.Drawing.Color.Gray;
            this.panel221.Location = new System.Drawing.Point(6, 258);
            this.panel221.Name = "panel221";
            this.panel221.Size = new System.Drawing.Size(40, 40);
            this.panel221.TabIndex = 266;
            // 
            // label222
            // 
            this.label222.AutoSize = true;
            this.label222.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label222.ForeColor = System.Drawing.Color.White;
            this.label222.Location = new System.Drawing.Point(52, 226);
            this.label222.Name = "label222";
            this.label222.Size = new System.Drawing.Size(269, 13);
            this.label222.TabIndex = 265;
            this.label222.Text = "[ Y1E4 ] 1-1 L/D Lifter Muting On/Off";
            // 
            // panel222
            // 
            this.panel222.BackColor = System.Drawing.Color.Gray;
            this.panel222.Location = new System.Drawing.Point(6, 212);
            this.panel222.Name = "panel222";
            this.panel222.Size = new System.Drawing.Size(40, 40);
            this.panel222.TabIndex = 264;
            // 
            // label223
            // 
            this.label223.AutoSize = true;
            this.label223.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label223.ForeColor = System.Drawing.Color.White;
            this.label223.Location = new System.Drawing.Point(52, 179);
            this.label223.Name = "label223";
            this.label223.Size = new System.Drawing.Size(262, 13);
            this.label223.TabIndex = 263;
            this.label223.Text = "[ Y1E3 ] 2-2 L/D 배출 Muting On/Off";
            // 
            // panel223
            // 
            this.panel223.BackColor = System.Drawing.Color.Gray;
            this.panel223.Location = new System.Drawing.Point(6, 166);
            this.panel223.Name = "panel223";
            this.panel223.Size = new System.Drawing.Size(40, 40);
            this.panel223.TabIndex = 262;
            // 
            // label224
            // 
            this.label224.AutoSize = true;
            this.label224.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label224.ForeColor = System.Drawing.Color.White;
            this.label224.Location = new System.Drawing.Point(52, 133);
            this.label224.Name = "label224";
            this.label224.Size = new System.Drawing.Size(262, 13);
            this.label224.TabIndex = 261;
            this.label224.Text = "[ Y1E2 ] 2-1 L/D 배출 Muting On/Off";
            // 
            // panel224
            // 
            this.panel224.BackColor = System.Drawing.Color.Gray;
            this.panel224.Location = new System.Drawing.Point(6, 120);
            this.panel224.Name = "panel224";
            this.panel224.Size = new System.Drawing.Size(40, 40);
            this.panel224.TabIndex = 260;
            // 
            // label225
            // 
            this.label225.AutoSize = true;
            this.label225.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label225.ForeColor = System.Drawing.Color.White;
            this.label225.Location = new System.Drawing.Point(52, 86);
            this.label225.Name = "label225";
            this.label225.Size = new System.Drawing.Size(262, 13);
            this.label225.TabIndex = 259;
            this.label225.Text = "[ Y1E1 ] 1-2 L/D 투입 Muting On/Off";
            // 
            // panel225
            // 
            this.panel225.BackColor = System.Drawing.Color.Gray;
            this.panel225.Location = new System.Drawing.Point(6, 74);
            this.panel225.Name = "panel225";
            this.panel225.Size = new System.Drawing.Size(40, 40);
            this.panel225.TabIndex = 258;
            // 
            // label226
            // 
            this.label226.AutoSize = true;
            this.label226.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label226.ForeColor = System.Drawing.Color.White;
            this.label226.Location = new System.Drawing.Point(52, 42);
            this.label226.Name = "label226";
            this.label226.Size = new System.Drawing.Size(262, 13);
            this.label226.TabIndex = 257;
            this.label226.Text = "[ Y1E0 ] 1-1 L/D 투입 Muting On/Off";
            // 
            // panel226
            // 
            this.panel226.BackColor = System.Drawing.Color.Gray;
            this.panel226.Location = new System.Drawing.Point(6, 28);
            this.panel226.Name = "panel226";
            this.panel226.Size = new System.Drawing.Size(40, 40);
            this.panel226.TabIndex = 256;
            // 
            // tc_iostatus_process_in
            // 
            this.tc_iostatus_process_in.Controls.Add(this.tp_iostatus_process_in1);
            this.tc_iostatus_process_in.Controls.Add(this.tp_iostatus_process_in2);
            this.tc_iostatus_process_in.Controls.Add(this.tp_iostatus_process_in3);
            this.tc_iostatus_process_in.ItemSize = new System.Drawing.Size(283, 30);
            this.tc_iostatus_process_in.Location = new System.Drawing.Point(6, 6);
            this.tc_iostatus_process_in.Name = "tc_iostatus_process_in";
            this.tc_iostatus_process_in.SelectedIndex = 0;
            this.tc_iostatus_process_in.Size = new System.Drawing.Size(854, 816);
            this.tc_iostatus_process_in.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tc_iostatus_process_in.TabIndex = 1;
            // 
            // tp_iostatus_process_in1
            // 
            this.tp_iostatus_process_in1.BackColor = System.Drawing.Color.DimGray;
            this.tp_iostatus_process_in1.Controls.Add(this.lb_x31f_1_breaking_stage_brush_down);
            this.tp_iostatus_process_in1.Controls.Add(this.pn_Process_IN1_X31F);
            this.tp_iostatus_process_in1.Controls.Add(this.lb_x31e_2_breaking_stage_brush_up);
            this.tp_iostatus_process_in1.Controls.Add(this.pn_Process_IN1_X31E);
            this.tp_iostatus_process_in1.Controls.Add(this.lb_x31d_1_breaking_stage_brush_down);
            this.tp_iostatus_process_in1.Controls.Add(this.pn_Process_IN1_X31D);
            this.tp_iostatus_process_in1.Controls.Add(this.lb_x31c_1_breaking_stage_brush_up);
            this.tp_iostatus_process_in1.Controls.Add(this.pn_Process_IN1_X31C);
            this.tp_iostatus_process_in1.Controls.Add(this.lb_x31b_2_after_ircut_trans_down);
            this.tp_iostatus_process_in1.Controls.Add(this.pn_Process_IN1_X31B);
            this.tp_iostatus_process_in1.Controls.Add(this.lb_x31a_2_after_ircut_trans_up);
            this.tp_iostatus_process_in1.Controls.Add(this.pn_Process_IN1_X31A);
            this.tp_iostatus_process_in1.Controls.Add(this.lb_x319_1_after_ircut_trans_down);
            this.tp_iostatus_process_in1.Controls.Add(this.pn_Process_IN1_X319);
            this.tp_iostatus_process_in1.Controls.Add(this.lb_x318_1_after_ircut_trans_up);
            this.tp_iostatus_process_in1.Controls.Add(this.pn_Process_IN1_X318);
            this.tp_iostatus_process_in1.Controls.Add(this.lb_x317_2_laser_stage_brush_down);
            this.tp_iostatus_process_in1.Controls.Add(this.pn_Process_IN1_X317);
            this.tp_iostatus_process_in1.Controls.Add(this.lb_x316_2_laser_stage_brush_up);
            this.tp_iostatus_process_in1.Controls.Add(this.pn_Process_IN1_X316);
            this.tp_iostatus_process_in1.Controls.Add(this.lb_x315_1_laser_stage_brush_down);
            this.tp_iostatus_process_in1.Controls.Add(this.pn_Process_IN1_X315);
            this.tp_iostatus_process_in1.Controls.Add(this.lb_x314_1_laser_stage_brush_up);
            this.tp_iostatus_process_in1.Controls.Add(this.pn_Process_IN1_X314);
            this.tp_iostatus_process_in1.Controls.Add(this.lb_x313_laser_shutter_close);
            this.tp_iostatus_process_in1.Controls.Add(this.pn_Process_IN1_X313);
            this.tp_iostatus_process_in1.Controls.Add(this.lb_x312_laser_shutter_open);
            this.tp_iostatus_process_in1.Controls.Add(this.pn_Process_IN1_X312);
            this.tp_iostatus_process_in1.Controls.Add(this.lb_x311_2_2_breaking_stage_cell_detect);
            this.tp_iostatus_process_in1.Controls.Add(this.pn_Process_IN1_X311);
            this.tp_iostatus_process_in1.Controls.Add(this.lb_x310_2_1_breaking_stage_cell_detect);
            this.tp_iostatus_process_in1.Controls.Add(this.pn_Process_IN1_X310);
            this.tp_iostatus_process_in1.Controls.Add(this.lb_x30f_1_2_breaking_stage_cell_detect);
            this.tp_iostatus_process_in1.Controls.Add(this.pn_Process_IN1_X30F);
            this.tp_iostatus_process_in1.Controls.Add(this.lb_x30e_1_1_breaking_stage_cell_detect);
            this.tp_iostatus_process_in1.Controls.Add(this.pn_Process_IN1_X30E);
            this.tp_iostatus_process_in1.Controls.Add(this.lb_x30d_2_2_laser_stage_cell_detect);
            this.tp_iostatus_process_in1.Controls.Add(this.pn_Process_IN1_X30D);
            this.tp_iostatus_process_in1.Controls.Add(this.lb_x30c_2_1_laser_stage_cell_detect);
            this.tp_iostatus_process_in1.Controls.Add(this.pn_Process_IN1_X30C);
            this.tp_iostatus_process_in1.Controls.Add(this.lb_x30b_1_2_laser_stage_cell_detect);
            this.tp_iostatus_process_in1.Controls.Add(this.pn_Process_IN1_X30B);
            this.tp_iostatus_process_in1.Controls.Add(this.lb_x30a_1_1_laser_stage_cell_detect);
            this.tp_iostatus_process_in1.Controls.Add(this.pn_Process_IN1_X30A);
            this.tp_iostatus_process_in1.Controls.Add(this.lb_x309_leak_sensor);
            this.tp_iostatus_process_in1.Controls.Add(this.pn_Process_IN1_X309);
            this.tp_iostatus_process_in1.Controls.Add(this.lb_x308_laser_cover_interlock);
            this.tp_iostatus_process_in1.Controls.Add(this.pn_Process_IN1_X308);
            this.tp_iostatus_process_in1.Controls.Add(this.lb_x307_laser_diodes_onoff_2);
            this.tp_iostatus_process_in1.Controls.Add(this.pn_Process_IN1_X307);
            this.tp_iostatus_process_in1.Controls.Add(this.lb_x306_laser_interlock_error_2);
            this.tp_iostatus_process_in1.Controls.Add(this.pn_Process_IN1_X306);
            this.tp_iostatus_process_in1.Controls.Add(this.lb_x305_laser_shutter_position_2);
            this.tp_iostatus_process_in1.Controls.Add(this.pn_Process_IN1_X305);
            this.tp_iostatus_process_in1.Controls.Add(this.lb_x304_laser_interlock_error_1);
            this.tp_iostatus_process_in1.Controls.Add(this.pn_Process_IN1_X304);
            this.tp_iostatus_process_in1.Controls.Add(this.lb__x303_laser_shutter_position_1);
            this.tp_iostatus_process_in1.Controls.Add(this.pn_Process_IN1_X303);
            this.tp_iostatus_process_in1.Controls.Add(this.lb_x302_laser_diodes_onoff_1);
            this.tp_iostatus_process_in1.Controls.Add(this.pn_Process_IN1_X302);
            this.tp_iostatus_process_in1.Controls.Add(this.lb_x301_laser_on_feedback);
            this.tp_iostatus_process_in1.Controls.Add(this.pn_Process_IN1_X301);
            this.tp_iostatus_process_in1.Controls.Add(this.lb_x300_laser_alarm);
            this.tp_iostatus_process_in1.Controls.Add(this.pn_Process_IN1_X300);
            this.tp_iostatus_process_in1.Location = new System.Drawing.Point(4, 34);
            this.tp_iostatus_process_in1.Name = "tp_iostatus_process_in1";
            this.tp_iostatus_process_in1.Padding = new System.Windows.Forms.Padding(3);
            this.tp_iostatus_process_in1.Size = new System.Drawing.Size(846, 778);
            this.tp_iostatus_process_in1.TabIndex = 0;
            this.tp_iostatus_process_in1.Text = "IN - 1";
            // 
            // lb_x31f_1_breaking_stage_brush_down
            // 
            this.lb_x31f_1_breaking_stage_brush_down.AutoSize = true;
            this.lb_x31f_1_breaking_stage_brush_down.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x31f_1_breaking_stage_brush_down.ForeColor = System.Drawing.Color.White;
            this.lb_x31f_1_breaking_stage_brush_down.Location = new System.Drawing.Point(479, 730);
            this.lb_x31f_1_breaking_stage_brush_down.Name = "lb_x31f_1_breaking_stage_brush_down";
            this.lb_x31f_1_breaking_stage_brush_down.Size = new System.Drawing.Size(290, 13);
            this.lb_x31f_1_breaking_stage_brush_down.TabIndex = 63;
            this.lb_x31f_1_breaking_stage_brush_down.Text = "[ X31F ] 2 Breaking Stage Brush DOWN";
            // 
            // pn_Process_IN1_X31F
            // 
            this.pn_Process_IN1_X31F.BackColor = System.Drawing.Color.Gray;
            this.pn_Process_IN1_X31F.Location = new System.Drawing.Point(433, 718);
            this.pn_Process_IN1_X31F.Name = "pn_Process_IN1_X31F";
            this.pn_Process_IN1_X31F.Size = new System.Drawing.Size(40, 40);
            this.pn_Process_IN1_X31F.TabIndex = 62;
            // 
            // lb_x31e_2_breaking_stage_brush_up
            // 
            this.lb_x31e_2_breaking_stage_brush_up.AutoSize = true;
            this.lb_x31e_2_breaking_stage_brush_up.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x31e_2_breaking_stage_brush_up.ForeColor = System.Drawing.Color.White;
            this.lb_x31e_2_breaking_stage_brush_up.Location = new System.Drawing.Point(479, 685);
            this.lb_x31e_2_breaking_stage_brush_up.Name = "lb_x31e_2_breaking_stage_brush_up";
            this.lb_x31e_2_breaking_stage_brush_up.Size = new System.Drawing.Size(267, 13);
            this.lb_x31e_2_breaking_stage_brush_up.TabIndex = 61;
            this.lb_x31e_2_breaking_stage_brush_up.Text = "[ X31E ] 2 Breaking Stage Brush UP";
            // 
            // pn_Process_IN1_X31E
            // 
            this.pn_Process_IN1_X31E.BackColor = System.Drawing.Color.Gray;
            this.pn_Process_IN1_X31E.Location = new System.Drawing.Point(433, 672);
            this.pn_Process_IN1_X31E.Name = "pn_Process_IN1_X31E";
            this.pn_Process_IN1_X31E.Size = new System.Drawing.Size(40, 40);
            this.pn_Process_IN1_X31E.TabIndex = 60;
            // 
            // lb_x31d_1_breaking_stage_brush_down
            // 
            this.lb_x31d_1_breaking_stage_brush_down.AutoSize = true;
            this.lb_x31d_1_breaking_stage_brush_down.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x31d_1_breaking_stage_brush_down.ForeColor = System.Drawing.Color.White;
            this.lb_x31d_1_breaking_stage_brush_down.Location = new System.Drawing.Point(479, 640);
            this.lb_x31d_1_breaking_stage_brush_down.Name = "lb_x31d_1_breaking_stage_brush_down";
            this.lb_x31d_1_breaking_stage_brush_down.Size = new System.Drawing.Size(292, 13);
            this.lb_x31d_1_breaking_stage_brush_down.TabIndex = 59;
            this.lb_x31d_1_breaking_stage_brush_down.Text = "[ X31D ] 1 Breaking Stage Brush DOWN";
            // 
            // pn_Process_IN1_X31D
            // 
            this.pn_Process_IN1_X31D.BackColor = System.Drawing.Color.Gray;
            this.pn_Process_IN1_X31D.Location = new System.Drawing.Point(433, 626);
            this.pn_Process_IN1_X31D.Name = "pn_Process_IN1_X31D";
            this.pn_Process_IN1_X31D.Size = new System.Drawing.Size(40, 40);
            this.pn_Process_IN1_X31D.TabIndex = 58;
            // 
            // lb_x31c_1_breaking_stage_brush_up
            // 
            this.lb_x31c_1_breaking_stage_brush_up.AutoSize = true;
            this.lb_x31c_1_breaking_stage_brush_up.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x31c_1_breaking_stage_brush_up.ForeColor = System.Drawing.Color.White;
            this.lb_x31c_1_breaking_stage_brush_up.Location = new System.Drawing.Point(479, 594);
            this.lb_x31c_1_breaking_stage_brush_up.Name = "lb_x31c_1_breaking_stage_brush_up";
            this.lb_x31c_1_breaking_stage_brush_up.Size = new System.Drawing.Size(268, 13);
            this.lb_x31c_1_breaking_stage_brush_up.TabIndex = 57;
            this.lb_x31c_1_breaking_stage_brush_up.Text = "[ X31C ] 1 Breaking Stage Brush UP";
            // 
            // pn_Process_IN1_X31C
            // 
            this.pn_Process_IN1_X31C.BackColor = System.Drawing.Color.Gray;
            this.pn_Process_IN1_X31C.Location = new System.Drawing.Point(433, 580);
            this.pn_Process_IN1_X31C.Name = "pn_Process_IN1_X31C";
            this.pn_Process_IN1_X31C.Size = new System.Drawing.Size(40, 40);
            this.pn_Process_IN1_X31C.TabIndex = 56;
            // 
            // lb_x31b_2_after_ircut_trans_down
            // 
            this.lb_x31b_2_after_ircut_trans_down.AutoSize = true;
            this.lb_x31b_2_after_ircut_trans_down.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x31b_2_after_ircut_trans_down.ForeColor = System.Drawing.Color.White;
            this.lb_x31b_2_after_ircut_trans_down.Location = new System.Drawing.Point(479, 548);
            this.lb_x31b_2_after_ircut_trans_down.Name = "lb_x31b_2_after_ircut_trans_down";
            this.lb_x31b_2_after_ircut_trans_down.Size = new System.Drawing.Size(261, 13);
            this.lb_x31b_2_after_ircut_trans_down.TabIndex = 55;
            this.lb_x31b_2_after_ircut_trans_down.Text = "[ X31B ] 2 After IR Cut 이재기 DOWN";
            // 
            // pn_Process_IN1_X31B
            // 
            this.pn_Process_IN1_X31B.BackColor = System.Drawing.Color.Gray;
            this.pn_Process_IN1_X31B.Location = new System.Drawing.Point(433, 534);
            this.pn_Process_IN1_X31B.Name = "pn_Process_IN1_X31B";
            this.pn_Process_IN1_X31B.Size = new System.Drawing.Size(40, 40);
            this.pn_Process_IN1_X31B.TabIndex = 54;
            // 
            // lb_x31a_2_after_ircut_trans_up
            // 
            this.lb_x31a_2_after_ircut_trans_up.AutoSize = true;
            this.lb_x31a_2_after_ircut_trans_up.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x31a_2_after_ircut_trans_up.ForeColor = System.Drawing.Color.White;
            this.lb_x31a_2_after_ircut_trans_up.Location = new System.Drawing.Point(479, 501);
            this.lb_x31a_2_after_ircut_trans_up.Name = "lb_x31a_2_after_ircut_trans_up";
            this.lb_x31a_2_after_ircut_trans_up.Size = new System.Drawing.Size(236, 13);
            this.lb_x31a_2_after_ircut_trans_up.TabIndex = 53;
            this.lb_x31a_2_after_ircut_trans_up.Text = "[ X31A ] 2 After IR Cut 이재기 UP";
            // 
            // pn_Process_IN1_X31A
            // 
            this.pn_Process_IN1_X31A.BackColor = System.Drawing.Color.Gray;
            this.pn_Process_IN1_X31A.Location = new System.Drawing.Point(433, 488);
            this.pn_Process_IN1_X31A.Name = "pn_Process_IN1_X31A";
            this.pn_Process_IN1_X31A.Size = new System.Drawing.Size(40, 40);
            this.pn_Process_IN1_X31A.TabIndex = 52;
            // 
            // lb_x319_1_after_ircut_trans_down
            // 
            this.lb_x319_1_after_ircut_trans_down.AutoSize = true;
            this.lb_x319_1_after_ircut_trans_down.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x319_1_after_ircut_trans_down.ForeColor = System.Drawing.Color.White;
            this.lb_x319_1_after_ircut_trans_down.Location = new System.Drawing.Point(479, 454);
            this.lb_x319_1_after_ircut_trans_down.Name = "lb_x319_1_after_ircut_trans_down";
            this.lb_x319_1_after_ircut_trans_down.Size = new System.Drawing.Size(259, 13);
            this.lb_x319_1_after_ircut_trans_down.TabIndex = 51;
            this.lb_x319_1_after_ircut_trans_down.Text = "[ X319 ] 1 After IR Cut 이재기 DOWN";
            // 
            // pn_Process_IN1_X319
            // 
            this.pn_Process_IN1_X319.BackColor = System.Drawing.Color.Gray;
            this.pn_Process_IN1_X319.Location = new System.Drawing.Point(433, 442);
            this.pn_Process_IN1_X319.Name = "pn_Process_IN1_X319";
            this.pn_Process_IN1_X319.Size = new System.Drawing.Size(40, 40);
            this.pn_Process_IN1_X319.TabIndex = 50;
            // 
            // lb_x318_1_after_ircut_trans_up
            // 
            this.lb_x318_1_after_ircut_trans_up.AutoSize = true;
            this.lb_x318_1_after_ircut_trans_up.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x318_1_after_ircut_trans_up.ForeColor = System.Drawing.Color.White;
            this.lb_x318_1_after_ircut_trans_up.Location = new System.Drawing.Point(479, 409);
            this.lb_x318_1_after_ircut_trans_up.Name = "lb_x318_1_after_ircut_trans_up";
            this.lb_x318_1_after_ircut_trans_up.Size = new System.Drawing.Size(235, 13);
            this.lb_x318_1_after_ircut_trans_up.TabIndex = 49;
            this.lb_x318_1_after_ircut_trans_up.Text = "[ X318 ] 1 After IR Cut 이재기 UP";
            // 
            // pn_Process_IN1_X318
            // 
            this.pn_Process_IN1_X318.BackColor = System.Drawing.Color.Gray;
            this.pn_Process_IN1_X318.Location = new System.Drawing.Point(433, 396);
            this.pn_Process_IN1_X318.Name = "pn_Process_IN1_X318";
            this.pn_Process_IN1_X318.Size = new System.Drawing.Size(40, 40);
            this.pn_Process_IN1_X318.TabIndex = 48;
            // 
            // lb_x317_2_laser_stage_brush_down
            // 
            this.lb_x317_2_laser_stage_brush_down.AutoSize = true;
            this.lb_x317_2_laser_stage_brush_down.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x317_2_laser_stage_brush_down.ForeColor = System.Drawing.Color.White;
            this.lb_x317_2_laser_stage_brush_down.Location = new System.Drawing.Point(479, 364);
            this.lb_x317_2_laser_stage_brush_down.Name = "lb_x317_2_laser_stage_brush_down";
            this.lb_x317_2_laser_stage_brush_down.Size = new System.Drawing.Size(255, 13);
            this.lb_x317_2_laser_stage_brush_down.TabIndex = 47;
            this.lb_x317_2_laser_stage_brush_down.Text = "[ X317 ] 2 가공 Stage Brush DOWN";
            // 
            // pn_Process_IN1_X317
            // 
            this.pn_Process_IN1_X317.BackColor = System.Drawing.Color.Gray;
            this.pn_Process_IN1_X317.Location = new System.Drawing.Point(433, 350);
            this.pn_Process_IN1_X317.Name = "pn_Process_IN1_X317";
            this.pn_Process_IN1_X317.Size = new System.Drawing.Size(40, 40);
            this.pn_Process_IN1_X317.TabIndex = 46;
            // 
            // lb_x316_2_laser_stage_brush_up
            // 
            this.lb_x316_2_laser_stage_brush_up.AutoSize = true;
            this.lb_x316_2_laser_stage_brush_up.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x316_2_laser_stage_brush_up.ForeColor = System.Drawing.Color.White;
            this.lb_x316_2_laser_stage_brush_up.Location = new System.Drawing.Point(479, 319);
            this.lb_x316_2_laser_stage_brush_up.Name = "lb_x316_2_laser_stage_brush_up";
            this.lb_x316_2_laser_stage_brush_up.Size = new System.Drawing.Size(231, 13);
            this.lb_x316_2_laser_stage_brush_up.TabIndex = 45;
            this.lb_x316_2_laser_stage_brush_up.Text = "[ X316 ] 2 가공 Stage Brush UP";
            // 
            // pn_Process_IN1_X316
            // 
            this.pn_Process_IN1_X316.BackColor = System.Drawing.Color.Gray;
            this.pn_Process_IN1_X316.Location = new System.Drawing.Point(433, 304);
            this.pn_Process_IN1_X316.Name = "pn_Process_IN1_X316";
            this.pn_Process_IN1_X316.Size = new System.Drawing.Size(40, 40);
            this.pn_Process_IN1_X316.TabIndex = 44;
            // 
            // lb_x315_1_laser_stage_brush_down
            // 
            this.lb_x315_1_laser_stage_brush_down.AutoSize = true;
            this.lb_x315_1_laser_stage_brush_down.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x315_1_laser_stage_brush_down.ForeColor = System.Drawing.Color.White;
            this.lb_x315_1_laser_stage_brush_down.Location = new System.Drawing.Point(479, 271);
            this.lb_x315_1_laser_stage_brush_down.Name = "lb_x315_1_laser_stage_brush_down";
            this.lb_x315_1_laser_stage_brush_down.Size = new System.Drawing.Size(255, 13);
            this.lb_x315_1_laser_stage_brush_down.TabIndex = 43;
            this.lb_x315_1_laser_stage_brush_down.Text = "[ X315 ] 1 가공 Stage Brush DOWN";
            // 
            // pn_Process_IN1_X315
            // 
            this.pn_Process_IN1_X315.BackColor = System.Drawing.Color.Gray;
            this.pn_Process_IN1_X315.Location = new System.Drawing.Point(433, 258);
            this.pn_Process_IN1_X315.Name = "pn_Process_IN1_X315";
            this.pn_Process_IN1_X315.Size = new System.Drawing.Size(40, 40);
            this.pn_Process_IN1_X315.TabIndex = 42;
            // 
            // lb_x314_1_laser_stage_brush_up
            // 
            this.lb_x314_1_laser_stage_brush_up.AutoSize = true;
            this.lb_x314_1_laser_stage_brush_up.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x314_1_laser_stage_brush_up.ForeColor = System.Drawing.Color.White;
            this.lb_x314_1_laser_stage_brush_up.Location = new System.Drawing.Point(479, 226);
            this.lb_x314_1_laser_stage_brush_up.Name = "lb_x314_1_laser_stage_brush_up";
            this.lb_x314_1_laser_stage_brush_up.Size = new System.Drawing.Size(231, 13);
            this.lb_x314_1_laser_stage_brush_up.TabIndex = 41;
            this.lb_x314_1_laser_stage_brush_up.Text = "[ X314 ] 1 가공 Stage Brush UP";
            // 
            // pn_Process_IN1_X314
            // 
            this.pn_Process_IN1_X314.BackColor = System.Drawing.Color.Gray;
            this.pn_Process_IN1_X314.Location = new System.Drawing.Point(433, 212);
            this.pn_Process_IN1_X314.Name = "pn_Process_IN1_X314";
            this.pn_Process_IN1_X314.Size = new System.Drawing.Size(40, 40);
            this.pn_Process_IN1_X314.TabIndex = 40;
            // 
            // lb_x313_laser_shutter_close
            // 
            this.lb_x313_laser_shutter_close.AutoSize = true;
            this.lb_x313_laser_shutter_close.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x313_laser_shutter_close.ForeColor = System.Drawing.Color.White;
            this.lb_x313_laser_shutter_close.Location = new System.Drawing.Point(479, 179);
            this.lb_x313_laser_shutter_close.Name = "lb_x313_laser_shutter_close";
            this.lb_x313_laser_shutter_close.Size = new System.Drawing.Size(221, 13);
            this.lb_x313_laser_shutter_close.TabIndex = 39;
            this.lb_x313_laser_shutter_close.Text = "[ X313 ] Laser Shutter CLOSE";
            // 
            // pn_Process_IN1_X313
            // 
            this.pn_Process_IN1_X313.BackColor = System.Drawing.Color.Gray;
            this.pn_Process_IN1_X313.Location = new System.Drawing.Point(433, 166);
            this.pn_Process_IN1_X313.Name = "pn_Process_IN1_X313";
            this.pn_Process_IN1_X313.Size = new System.Drawing.Size(40, 40);
            this.pn_Process_IN1_X313.TabIndex = 38;
            // 
            // lb_x312_laser_shutter_open
            // 
            this.lb_x312_laser_shutter_open.AutoSize = true;
            this.lb_x312_laser_shutter_open.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x312_laser_shutter_open.ForeColor = System.Drawing.Color.White;
            this.lb_x312_laser_shutter_open.Location = new System.Drawing.Point(479, 133);
            this.lb_x312_laser_shutter_open.Name = "lb_x312_laser_shutter_open";
            this.lb_x312_laser_shutter_open.Size = new System.Drawing.Size(213, 13);
            this.lb_x312_laser_shutter_open.TabIndex = 37;
            this.lb_x312_laser_shutter_open.Text = "[ X312 ] Laser Shutter OPEN";
            // 
            // pn_Process_IN1_X312
            // 
            this.pn_Process_IN1_X312.BackColor = System.Drawing.Color.Gray;
            this.pn_Process_IN1_X312.Location = new System.Drawing.Point(433, 120);
            this.pn_Process_IN1_X312.Name = "pn_Process_IN1_X312";
            this.pn_Process_IN1_X312.Size = new System.Drawing.Size(40, 40);
            this.pn_Process_IN1_X312.TabIndex = 36;
            // 
            // lb_x311_2_2_breaking_stage_cell_detect
            // 
            this.lb_x311_2_2_breaking_stage_cell_detect.AutoSize = true;
            this.lb_x311_2_2_breaking_stage_cell_detect.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x311_2_2_breaking_stage_cell_detect.ForeColor = System.Drawing.Color.White;
            this.lb_x311_2_2_breaking_stage_cell_detect.Location = new System.Drawing.Point(479, 86);
            this.lb_x311_2_2_breaking_stage_cell_detect.Name = "lb_x311_2_2_breaking_stage_cell_detect";
            this.lb_x311_2_2_breaking_stage_cell_detect.Size = new System.Drawing.Size(291, 13);
            this.lb_x311_2_2_breaking_stage_cell_detect.TabIndex = 35;
            this.lb_x311_2_2_breaking_stage_cell_detect.Text = "[ X311 ] 2-2 Breaking Stage Cell Detect";
            // 
            // pn_Process_IN1_X311
            // 
            this.pn_Process_IN1_X311.BackColor = System.Drawing.Color.Gray;
            this.pn_Process_IN1_X311.Location = new System.Drawing.Point(433, 74);
            this.pn_Process_IN1_X311.Name = "pn_Process_IN1_X311";
            this.pn_Process_IN1_X311.Size = new System.Drawing.Size(40, 40);
            this.pn_Process_IN1_X311.TabIndex = 34;
            // 
            // lb_x310_2_1_breaking_stage_cell_detect
            // 
            this.lb_x310_2_1_breaking_stage_cell_detect.AutoSize = true;
            this.lb_x310_2_1_breaking_stage_cell_detect.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x310_2_1_breaking_stage_cell_detect.ForeColor = System.Drawing.Color.White;
            this.lb_x310_2_1_breaking_stage_cell_detect.Location = new System.Drawing.Point(479, 42);
            this.lb_x310_2_1_breaking_stage_cell_detect.Name = "lb_x310_2_1_breaking_stage_cell_detect";
            this.lb_x310_2_1_breaking_stage_cell_detect.Size = new System.Drawing.Size(291, 13);
            this.lb_x310_2_1_breaking_stage_cell_detect.TabIndex = 33;
            this.lb_x310_2_1_breaking_stage_cell_detect.Text = "[ X310 ] 2-1 Breaking Stage Cell Detect";
            // 
            // pn_Process_IN1_X310
            // 
            this.pn_Process_IN1_X310.BackColor = System.Drawing.Color.Gray;
            this.pn_Process_IN1_X310.Location = new System.Drawing.Point(433, 28);
            this.pn_Process_IN1_X310.Name = "pn_Process_IN1_X310";
            this.pn_Process_IN1_X310.Size = new System.Drawing.Size(40, 40);
            this.pn_Process_IN1_X310.TabIndex = 32;
            // 
            // lb_x30f_1_2_breaking_stage_cell_detect
            // 
            this.lb_x30f_1_2_breaking_stage_cell_detect.AutoSize = true;
            this.lb_x30f_1_2_breaking_stage_cell_detect.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x30f_1_2_breaking_stage_cell_detect.ForeColor = System.Drawing.Color.White;
            this.lb_x30f_1_2_breaking_stage_cell_detect.Location = new System.Drawing.Point(52, 730);
            this.lb_x30f_1_2_breaking_stage_cell_detect.Name = "lb_x30f_1_2_breaking_stage_cell_detect";
            this.lb_x30f_1_2_breaking_stage_cell_detect.Size = new System.Drawing.Size(291, 13);
            this.lb_x30f_1_2_breaking_stage_cell_detect.TabIndex = 31;
            this.lb_x30f_1_2_breaking_stage_cell_detect.Text = "[ X30F ] 1-2 Breaking Stage Cell Detect";
            // 
            // pn_Process_IN1_X30F
            // 
            this.pn_Process_IN1_X30F.BackColor = System.Drawing.Color.Gray;
            this.pn_Process_IN1_X30F.Location = new System.Drawing.Point(6, 718);
            this.pn_Process_IN1_X30F.Name = "pn_Process_IN1_X30F";
            this.pn_Process_IN1_X30F.Size = new System.Drawing.Size(40, 40);
            this.pn_Process_IN1_X30F.TabIndex = 30;
            // 
            // lb_x30e_1_1_breaking_stage_cell_detect
            // 
            this.lb_x30e_1_1_breaking_stage_cell_detect.AutoSize = true;
            this.lb_x30e_1_1_breaking_stage_cell_detect.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x30e_1_1_breaking_stage_cell_detect.ForeColor = System.Drawing.Color.White;
            this.lb_x30e_1_1_breaking_stage_cell_detect.Location = new System.Drawing.Point(52, 685);
            this.lb_x30e_1_1_breaking_stage_cell_detect.Name = "lb_x30e_1_1_breaking_stage_cell_detect";
            this.lb_x30e_1_1_breaking_stage_cell_detect.Size = new System.Drawing.Size(292, 13);
            this.lb_x30e_1_1_breaking_stage_cell_detect.TabIndex = 29;
            this.lb_x30e_1_1_breaking_stage_cell_detect.Text = "[ X30E ] 1-1 Breaking Stage Cell Detect";
            // 
            // pn_Process_IN1_X30E
            // 
            this.pn_Process_IN1_X30E.BackColor = System.Drawing.Color.Gray;
            this.pn_Process_IN1_X30E.Location = new System.Drawing.Point(6, 672);
            this.pn_Process_IN1_X30E.Name = "pn_Process_IN1_X30E";
            this.pn_Process_IN1_X30E.Size = new System.Drawing.Size(40, 40);
            this.pn_Process_IN1_X30E.TabIndex = 28;
            // 
            // lb_x30d_2_2_laser_stage_cell_detect
            // 
            this.lb_x30d_2_2_laser_stage_cell_detect.AutoSize = true;
            this.lb_x30d_2_2_laser_stage_cell_detect.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x30d_2_2_laser_stage_cell_detect.ForeColor = System.Drawing.Color.White;
            this.lb_x30d_2_2_laser_stage_cell_detect.Location = new System.Drawing.Point(52, 640);
            this.lb_x30d_2_2_laser_stage_cell_detect.Name = "lb_x30d_2_2_laser_stage_cell_detect";
            this.lb_x30d_2_2_laser_stage_cell_detect.Size = new System.Drawing.Size(258, 13);
            this.lb_x30d_2_2_laser_stage_cell_detect.TabIndex = 27;
            this.lb_x30d_2_2_laser_stage_cell_detect.Text = "[ X30D ] 2-2 가공 Stage Cell Detect";
            // 
            // pn_Process_IN1_X30D
            // 
            this.pn_Process_IN1_X30D.BackColor = System.Drawing.Color.Gray;
            this.pn_Process_IN1_X30D.Location = new System.Drawing.Point(6, 626);
            this.pn_Process_IN1_X30D.Name = "pn_Process_IN1_X30D";
            this.pn_Process_IN1_X30D.Size = new System.Drawing.Size(40, 40);
            this.pn_Process_IN1_X30D.TabIndex = 26;
            // 
            // lb_x30c_2_1_laser_stage_cell_detect
            // 
            this.lb_x30c_2_1_laser_stage_cell_detect.AutoSize = true;
            this.lb_x30c_2_1_laser_stage_cell_detect.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x30c_2_1_laser_stage_cell_detect.ForeColor = System.Drawing.Color.White;
            this.lb_x30c_2_1_laser_stage_cell_detect.Location = new System.Drawing.Point(52, 594);
            this.lb_x30c_2_1_laser_stage_cell_detect.Name = "lb_x30c_2_1_laser_stage_cell_detect";
            this.lb_x30c_2_1_laser_stage_cell_detect.Size = new System.Drawing.Size(258, 13);
            this.lb_x30c_2_1_laser_stage_cell_detect.TabIndex = 25;
            this.lb_x30c_2_1_laser_stage_cell_detect.Text = "[ X30C ] 2-1 가공 Stage Cell Detect";
            // 
            // pn_Process_IN1_X30C
            // 
            this.pn_Process_IN1_X30C.BackColor = System.Drawing.Color.Gray;
            this.pn_Process_IN1_X30C.Location = new System.Drawing.Point(6, 580);
            this.pn_Process_IN1_X30C.Name = "pn_Process_IN1_X30C";
            this.pn_Process_IN1_X30C.Size = new System.Drawing.Size(40, 40);
            this.pn_Process_IN1_X30C.TabIndex = 24;
            // 
            // lb_x30b_1_2_laser_stage_cell_detect
            // 
            this.lb_x30b_1_2_laser_stage_cell_detect.AutoSize = true;
            this.lb_x30b_1_2_laser_stage_cell_detect.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x30b_1_2_laser_stage_cell_detect.ForeColor = System.Drawing.Color.White;
            this.lb_x30b_1_2_laser_stage_cell_detect.Location = new System.Drawing.Point(52, 548);
            this.lb_x30b_1_2_laser_stage_cell_detect.Name = "lb_x30b_1_2_laser_stage_cell_detect";
            this.lb_x30b_1_2_laser_stage_cell_detect.Size = new System.Drawing.Size(258, 13);
            this.lb_x30b_1_2_laser_stage_cell_detect.TabIndex = 23;
            this.lb_x30b_1_2_laser_stage_cell_detect.Text = "[ X30B ] 1-2 가공 Stage Cell Detect";
            // 
            // pn_Process_IN1_X30B
            // 
            this.pn_Process_IN1_X30B.BackColor = System.Drawing.Color.Gray;
            this.pn_Process_IN1_X30B.Location = new System.Drawing.Point(6, 534);
            this.pn_Process_IN1_X30B.Name = "pn_Process_IN1_X30B";
            this.pn_Process_IN1_X30B.Size = new System.Drawing.Size(40, 40);
            this.pn_Process_IN1_X30B.TabIndex = 22;
            // 
            // lb_x30a_1_1_laser_stage_cell_detect
            // 
            this.lb_x30a_1_1_laser_stage_cell_detect.AutoSize = true;
            this.lb_x30a_1_1_laser_stage_cell_detect.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x30a_1_1_laser_stage_cell_detect.ForeColor = System.Drawing.Color.White;
            this.lb_x30a_1_1_laser_stage_cell_detect.Location = new System.Drawing.Point(52, 501);
            this.lb_x30a_1_1_laser_stage_cell_detect.Name = "lb_x30a_1_1_laser_stage_cell_detect";
            this.lb_x30a_1_1_laser_stage_cell_detect.Size = new System.Drawing.Size(257, 13);
            this.lb_x30a_1_1_laser_stage_cell_detect.TabIndex = 21;
            this.lb_x30a_1_1_laser_stage_cell_detect.Text = "[ X30A ] 1-1 가공 Stage Cell Detect";
            // 
            // pn_Process_IN1_X30A
            // 
            this.pn_Process_IN1_X30A.BackColor = System.Drawing.Color.Gray;
            this.pn_Process_IN1_X30A.Location = new System.Drawing.Point(6, 488);
            this.pn_Process_IN1_X30A.Name = "pn_Process_IN1_X30A";
            this.pn_Process_IN1_X30A.Size = new System.Drawing.Size(40, 40);
            this.pn_Process_IN1_X30A.TabIndex = 20;
            // 
            // lb_x309_leak_sensor
            // 
            this.lb_x309_leak_sensor.AutoSize = true;
            this.lb_x309_leak_sensor.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x309_leak_sensor.ForeColor = System.Drawing.Color.White;
            this.lb_x309_leak_sensor.Location = new System.Drawing.Point(52, 454);
            this.lb_x309_leak_sensor.Name = "lb_x309_leak_sensor";
            this.lb_x309_leak_sensor.Size = new System.Drawing.Size(243, 13);
            this.lb_x309_leak_sensor.TabIndex = 19;
            this.lb_x309_leak_sensor.Text = "[ X309 ] Leak Sensor (원형 PMC)";
            // 
            // pn_Process_IN1_X309
            // 
            this.pn_Process_IN1_X309.BackColor = System.Drawing.Color.Gray;
            this.pn_Process_IN1_X309.Location = new System.Drawing.Point(6, 442);
            this.pn_Process_IN1_X309.Name = "pn_Process_IN1_X309";
            this.pn_Process_IN1_X309.Size = new System.Drawing.Size(40, 40);
            this.pn_Process_IN1_X309.TabIndex = 18;
            // 
            // lb_x308_laser_cover_interlock
            // 
            this.lb_x308_laser_cover_interlock.AutoSize = true;
            this.lb_x308_laser_cover_interlock.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x308_laser_cover_interlock.ForeColor = System.Drawing.Color.White;
            this.lb_x308_laser_cover_interlock.Location = new System.Drawing.Point(52, 409);
            this.lb_x308_laser_cover_interlock.Name = "lb_x308_laser_cover_interlock";
            this.lb_x308_laser_cover_interlock.Size = new System.Drawing.Size(224, 13);
            this.lb_x308_laser_cover_interlock.TabIndex = 17;
            this.lb_x308_laser_cover_interlock.Text = "[ X308 ] Laser Cover Interlock";
            // 
            // pn_Process_IN1_X308
            // 
            this.pn_Process_IN1_X308.BackColor = System.Drawing.Color.Gray;
            this.pn_Process_IN1_X308.Location = new System.Drawing.Point(6, 396);
            this.pn_Process_IN1_X308.Name = "pn_Process_IN1_X308";
            this.pn_Process_IN1_X308.Size = new System.Drawing.Size(40, 40);
            this.pn_Process_IN1_X308.TabIndex = 16;
            // 
            // lb_x307_laser_diodes_onoff_2
            // 
            this.lb_x307_laser_diodes_onoff_2.AutoSize = true;
            this.lb_x307_laser_diodes_onoff_2.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x307_laser_diodes_onoff_2.ForeColor = System.Drawing.Color.White;
            this.lb_x307_laser_diodes_onoff_2.Location = new System.Drawing.Point(52, 364);
            this.lb_x307_laser_diodes_onoff_2.Name = "lb_x307_laser_diodes_onoff_2";
            this.lb_x307_laser_diodes_onoff_2.Size = new System.Drawing.Size(302, 13);
            this.lb_x307_laser_diodes_onoff_2.TabIndex = 15;
            this.lb_x307_laser_diodes_onoff_2.Text = "[ X307 ] Laser Status_Diodes ON/OFF #2";
            // 
            // pn_Process_IN1_X307
            // 
            this.pn_Process_IN1_X307.BackColor = System.Drawing.Color.Gray;
            this.pn_Process_IN1_X307.Location = new System.Drawing.Point(6, 350);
            this.pn_Process_IN1_X307.Name = "pn_Process_IN1_X307";
            this.pn_Process_IN1_X307.Size = new System.Drawing.Size(40, 40);
            this.pn_Process_IN1_X307.TabIndex = 14;
            // 
            // lb_x306_laser_interlock_error_2
            // 
            this.lb_x306_laser_interlock_error_2.AutoSize = true;
            this.lb_x306_laser_interlock_error_2.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x306_laser_interlock_error_2.ForeColor = System.Drawing.Color.White;
            this.lb_x306_laser_interlock_error_2.Location = new System.Drawing.Point(52, 319);
            this.lb_x306_laser_interlock_error_2.Name = "lb_x306_laser_interlock_error_2";
            this.lb_x306_laser_interlock_error_2.Size = new System.Drawing.Size(294, 13);
            this.lb_x306_laser_interlock_error_2.TabIndex = 13;
            this.lb_x306_laser_interlock_error_2.Text = "[ X306 ] Laser Status_Interlock Error #2";
            // 
            // pn_Process_IN1_X306
            // 
            this.pn_Process_IN1_X306.BackColor = System.Drawing.Color.Gray;
            this.pn_Process_IN1_X306.Location = new System.Drawing.Point(6, 304);
            this.pn_Process_IN1_X306.Name = "pn_Process_IN1_X306";
            this.pn_Process_IN1_X306.Size = new System.Drawing.Size(40, 40);
            this.pn_Process_IN1_X306.TabIndex = 12;
            // 
            // lb_x305_laser_shutter_position_2
            // 
            this.lb_x305_laser_shutter_position_2.AutoSize = true;
            this.lb_x305_laser_shutter_position_2.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x305_laser_shutter_position_2.ForeColor = System.Drawing.Color.White;
            this.lb_x305_laser_shutter_position_2.Location = new System.Drawing.Point(52, 271);
            this.lb_x305_laser_shutter_position_2.Name = "lb_x305_laser_shutter_position_2";
            this.lb_x305_laser_shutter_position_2.Size = new System.Drawing.Size(306, 13);
            this.lb_x305_laser_shutter_position_2.TabIndex = 11;
            this.lb_x305_laser_shutter_position_2.Text = "[ X305 ] Laser Status_Shutter Position #2";
            // 
            // pn_Process_IN1_X305
            // 
            this.pn_Process_IN1_X305.BackColor = System.Drawing.Color.Gray;
            this.pn_Process_IN1_X305.Location = new System.Drawing.Point(6, 258);
            this.pn_Process_IN1_X305.Name = "pn_Process_IN1_X305";
            this.pn_Process_IN1_X305.Size = new System.Drawing.Size(40, 40);
            this.pn_Process_IN1_X305.TabIndex = 10;
            // 
            // lb_x304_laser_interlock_error_1
            // 
            this.lb_x304_laser_interlock_error_1.AutoSize = true;
            this.lb_x304_laser_interlock_error_1.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x304_laser_interlock_error_1.ForeColor = System.Drawing.Color.White;
            this.lb_x304_laser_interlock_error_1.Location = new System.Drawing.Point(52, 226);
            this.lb_x304_laser_interlock_error_1.Name = "lb_x304_laser_interlock_error_1";
            this.lb_x304_laser_interlock_error_1.Size = new System.Drawing.Size(294, 13);
            this.lb_x304_laser_interlock_error_1.TabIndex = 9;
            this.lb_x304_laser_interlock_error_1.Text = "[ X304 ] Laser Status_Interlock Error #1";
            // 
            // pn_Process_IN1_X304
            // 
            this.pn_Process_IN1_X304.BackColor = System.Drawing.Color.Gray;
            this.pn_Process_IN1_X304.Location = new System.Drawing.Point(6, 212);
            this.pn_Process_IN1_X304.Name = "pn_Process_IN1_X304";
            this.pn_Process_IN1_X304.Size = new System.Drawing.Size(40, 40);
            this.pn_Process_IN1_X304.TabIndex = 8;
            // 
            // lb__x303_laser_shutter_position_1
            // 
            this.lb__x303_laser_shutter_position_1.AutoSize = true;
            this.lb__x303_laser_shutter_position_1.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb__x303_laser_shutter_position_1.ForeColor = System.Drawing.Color.White;
            this.lb__x303_laser_shutter_position_1.Location = new System.Drawing.Point(52, 179);
            this.lb__x303_laser_shutter_position_1.Name = "lb__x303_laser_shutter_position_1";
            this.lb__x303_laser_shutter_position_1.Size = new System.Drawing.Size(306, 13);
            this.lb__x303_laser_shutter_position_1.TabIndex = 7;
            this.lb__x303_laser_shutter_position_1.Text = "[ X303 ] Laser Status_Shutter Position #1";
            // 
            // pn_Process_IN1_X303
            // 
            this.pn_Process_IN1_X303.BackColor = System.Drawing.Color.Gray;
            this.pn_Process_IN1_X303.Location = new System.Drawing.Point(6, 166);
            this.pn_Process_IN1_X303.Name = "pn_Process_IN1_X303";
            this.pn_Process_IN1_X303.Size = new System.Drawing.Size(40, 40);
            this.pn_Process_IN1_X303.TabIndex = 6;
            // 
            // lb_x302_laser_diodes_onoff_1
            // 
            this.lb_x302_laser_diodes_onoff_1.AutoSize = true;
            this.lb_x302_laser_diodes_onoff_1.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x302_laser_diodes_onoff_1.ForeColor = System.Drawing.Color.White;
            this.lb_x302_laser_diodes_onoff_1.Location = new System.Drawing.Point(52, 133);
            this.lb_x302_laser_diodes_onoff_1.Name = "lb_x302_laser_diodes_onoff_1";
            this.lb_x302_laser_diodes_onoff_1.Size = new System.Drawing.Size(302, 13);
            this.lb_x302_laser_diodes_onoff_1.TabIndex = 5;
            this.lb_x302_laser_diodes_onoff_1.Text = "[ X302 ] Laser Status_Diodes ON/OFF #1";
            // 
            // pn_Process_IN1_X302
            // 
            this.pn_Process_IN1_X302.BackColor = System.Drawing.Color.Gray;
            this.pn_Process_IN1_X302.Location = new System.Drawing.Point(6, 120);
            this.pn_Process_IN1_X302.Name = "pn_Process_IN1_X302";
            this.pn_Process_IN1_X302.Size = new System.Drawing.Size(40, 40);
            this.pn_Process_IN1_X302.TabIndex = 4;
            // 
            // lb_x301_laser_on_feedback
            // 
            this.lb_x301_laser_on_feedback.AutoSize = true;
            this.lb_x301_laser_on_feedback.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x301_laser_on_feedback.ForeColor = System.Drawing.Color.White;
            this.lb_x301_laser_on_feedback.Location = new System.Drawing.Point(52, 86);
            this.lb_x301_laser_on_feedback.Name = "lb_x301_laser_on_feedback";
            this.lb_x301_laser_on_feedback.Size = new System.Drawing.Size(209, 13);
            this.lb_x301_laser_on_feedback.TabIndex = 3;
            this.lb_x301_laser_on_feedback.Text = "[ X301 ] Laser on Feedback";
            // 
            // pn_Process_IN1_X301
            // 
            this.pn_Process_IN1_X301.BackColor = System.Drawing.Color.Gray;
            this.pn_Process_IN1_X301.Location = new System.Drawing.Point(6, 74);
            this.pn_Process_IN1_X301.Name = "pn_Process_IN1_X301";
            this.pn_Process_IN1_X301.Size = new System.Drawing.Size(40, 40);
            this.pn_Process_IN1_X301.TabIndex = 2;
            // 
            // lb_x300_laser_alarm
            // 
            this.lb_x300_laser_alarm.AutoSize = true;
            this.lb_x300_laser_alarm.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_x300_laser_alarm.ForeColor = System.Drawing.Color.White;
            this.lb_x300_laser_alarm.Location = new System.Drawing.Point(52, 42);
            this.lb_x300_laser_alarm.Name = "lb_x300_laser_alarm";
            this.lb_x300_laser_alarm.Size = new System.Drawing.Size(157, 13);
            this.lb_x300_laser_alarm.TabIndex = 1;
            this.lb_x300_laser_alarm.Text = "[ X300 ] Laser Alarm";
            // 
            // pn_Process_IN1_X300
            // 
            this.pn_Process_IN1_X300.BackColor = System.Drawing.Color.Gray;
            this.pn_Process_IN1_X300.Location = new System.Drawing.Point(6, 28);
            this.pn_Process_IN1_X300.Name = "pn_Process_IN1_X300";
            this.pn_Process_IN1_X300.Size = new System.Drawing.Size(40, 40);
            this.pn_Process_IN1_X300.TabIndex = 0;
            // 
            // tp_iostatus_process_in2
            // 
            this.tp_iostatus_process_in2.BackColor = System.Drawing.Color.DimGray;
            this.tp_iostatus_process_in2.Controls.Add(this.label35);
            this.tp_iostatus_process_in2.Controls.Add(this.panel35);
            this.tp_iostatus_process_in2.Controls.Add(this.label36);
            this.tp_iostatus_process_in2.Controls.Add(this.panel36);
            this.tp_iostatus_process_in2.Controls.Add(this.label37);
            this.tp_iostatus_process_in2.Controls.Add(this.panel37);
            this.tp_iostatus_process_in2.Controls.Add(this.label38);
            this.tp_iostatus_process_in2.Controls.Add(this.panel38);
            this.tp_iostatus_process_in2.Controls.Add(this.label39);
            this.tp_iostatus_process_in2.Controls.Add(this.panel39);
            this.tp_iostatus_process_in2.Controls.Add(this.label40);
            this.tp_iostatus_process_in2.Controls.Add(this.panel40);
            this.tp_iostatus_process_in2.Controls.Add(this.label41);
            this.tp_iostatus_process_in2.Controls.Add(this.panel41);
            this.tp_iostatus_process_in2.Controls.Add(this.label42);
            this.tp_iostatus_process_in2.Controls.Add(this.panel42);
            this.tp_iostatus_process_in2.Controls.Add(this.label43);
            this.tp_iostatus_process_in2.Controls.Add(this.panel43);
            this.tp_iostatus_process_in2.Controls.Add(this.label44);
            this.tp_iostatus_process_in2.Controls.Add(this.panel44);
            this.tp_iostatus_process_in2.Controls.Add(this.label45);
            this.tp_iostatus_process_in2.Controls.Add(this.panel45);
            this.tp_iostatus_process_in2.Controls.Add(this.label46);
            this.tp_iostatus_process_in2.Controls.Add(this.panel46);
            this.tp_iostatus_process_in2.Controls.Add(this.label47);
            this.tp_iostatus_process_in2.Controls.Add(this.panel47);
            this.tp_iostatus_process_in2.Controls.Add(this.label48);
            this.tp_iostatus_process_in2.Controls.Add(this.panel48);
            this.tp_iostatus_process_in2.Controls.Add(this.label49);
            this.tp_iostatus_process_in2.Controls.Add(this.panel49);
            this.tp_iostatus_process_in2.Controls.Add(this.label50);
            this.tp_iostatus_process_in2.Controls.Add(this.panel50);
            this.tp_iostatus_process_in2.Controls.Add(this.label51);
            this.tp_iostatus_process_in2.Controls.Add(this.panel51);
            this.tp_iostatus_process_in2.Controls.Add(this.label52);
            this.tp_iostatus_process_in2.Controls.Add(this.panel52);
            this.tp_iostatus_process_in2.Controls.Add(this.label53);
            this.tp_iostatus_process_in2.Controls.Add(this.panel53);
            this.tp_iostatus_process_in2.Controls.Add(this.label54);
            this.tp_iostatus_process_in2.Controls.Add(this.panel54);
            this.tp_iostatus_process_in2.Controls.Add(this.label55);
            this.tp_iostatus_process_in2.Controls.Add(this.panel55);
            this.tp_iostatus_process_in2.Controls.Add(this.label56);
            this.tp_iostatus_process_in2.Controls.Add(this.panel56);
            this.tp_iostatus_process_in2.Controls.Add(this.label57);
            this.tp_iostatus_process_in2.Controls.Add(this.panel57);
            this.tp_iostatus_process_in2.Controls.Add(this.label58);
            this.tp_iostatus_process_in2.Controls.Add(this.panel58);
            this.tp_iostatus_process_in2.Controls.Add(this.label59);
            this.tp_iostatus_process_in2.Controls.Add(this.panel59);
            this.tp_iostatus_process_in2.Controls.Add(this.label60);
            this.tp_iostatus_process_in2.Controls.Add(this.panel60);
            this.tp_iostatus_process_in2.Controls.Add(this.label61);
            this.tp_iostatus_process_in2.Controls.Add(this.panel61);
            this.tp_iostatus_process_in2.Controls.Add(this.label62);
            this.tp_iostatus_process_in2.Controls.Add(this.panel62);
            this.tp_iostatus_process_in2.Controls.Add(this.label63);
            this.tp_iostatus_process_in2.Controls.Add(this.panel63);
            this.tp_iostatus_process_in2.Controls.Add(this.label64);
            this.tp_iostatus_process_in2.Controls.Add(this.panel64);
            this.tp_iostatus_process_in2.Controls.Add(this.label65);
            this.tp_iostatus_process_in2.Controls.Add(this.panel65);
            this.tp_iostatus_process_in2.Controls.Add(this.label66);
            this.tp_iostatus_process_in2.Controls.Add(this.panel66);
            this.tp_iostatus_process_in2.Location = new System.Drawing.Point(4, 34);
            this.tp_iostatus_process_in2.Name = "tp_iostatus_process_in2";
            this.tp_iostatus_process_in2.Padding = new System.Windows.Forms.Padding(3);
            this.tp_iostatus_process_in2.Size = new System.Drawing.Size(846, 778);
            this.tp_iostatus_process_in2.TabIndex = 1;
            this.tp_iostatus_process_in2.Text = "IN - 2";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label35.ForeColor = System.Drawing.Color.White;
            this.label35.Location = new System.Drawing.Point(479, 730);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(211, 13);
            this.label35.TabIndex = 127;
            this.label35.Text = "[ X13F ] 가공부 배기 FAN 구동";
            // 
            // panel35
            // 
            this.panel35.BackColor = System.Drawing.Color.Gray;
            this.panel35.Location = new System.Drawing.Point(433, 718);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(40, 40);
            this.panel35.TabIndex = 126;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label36.ForeColor = System.Drawing.Color.White;
            this.label36.Location = new System.Drawing.Point(479, 685);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(212, 13);
            this.label36.TabIndex = 125;
            this.label36.Text = "[ X13E ] 가공부 흡기 FAN 구동";
            // 
            // panel36
            // 
            this.panel36.BackColor = System.Drawing.Color.Gray;
            this.panel36.Location = new System.Drawing.Point(433, 672);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(40, 40);
            this.panel36.TabIndex = 124;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label37.ForeColor = System.Drawing.Color.White;
            this.label37.Location = new System.Drawing.Point(479, 640);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(214, 13);
            this.label37.TabIndex = 123;
            this.label37.Text = "[ X13D ] 가공부 판넬 화재 알람";
            // 
            // panel37
            // 
            this.panel37.BackColor = System.Drawing.Color.Gray;
            this.panel37.Location = new System.Drawing.Point(433, 626);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(40, 40);
            this.panel37.TabIndex = 122;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label38.ForeColor = System.Drawing.Color.White;
            this.label38.Location = new System.Drawing.Point(479, 594);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(211, 13);
            this.label38.TabIndex = 121;
            this.label38.Text = "[ X13C ] L/D부 판넬 화재 알람";
            // 
            // panel38
            // 
            this.panel38.BackColor = System.Drawing.Color.Gray;
            this.panel38.Location = new System.Drawing.Point(433, 580);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(40, 40);
            this.panel38.TabIndex = 120;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label39.ForeColor = System.Drawing.Color.White;
            this.label39.Location = new System.Drawing.Point(479, 548);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(302, 13);
            this.label39.TabIndex = 119;
            this.label39.Text = "[ X13B ] 2 L/D Cassette 하부 UnGrip POS";
            // 
            // panel39
            // 
            this.panel39.BackColor = System.Drawing.Color.Gray;
            this.panel39.Location = new System.Drawing.Point(433, 534);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(40, 40);
            this.panel39.TabIndex = 118;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label40.ForeColor = System.Drawing.Color.White;
            this.label40.Location = new System.Drawing.Point(479, 501);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(282, 13);
            this.label40.TabIndex = 117;
            this.label40.Text = "[ X13A ] 2 L/D Cassette 하부 Grip POS";
            // 
            // panel40
            // 
            this.panel40.BackColor = System.Drawing.Color.Gray;
            this.panel40.Location = new System.Drawing.Point(433, 488);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(40, 40);
            this.panel40.TabIndex = 116;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label41.ForeColor = System.Drawing.Color.White;
            this.label41.Location = new System.Drawing.Point(479, 454);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(316, 13);
            this.label41.TabIndex = 115;
            this.label41.Text = "[ X139 ] 2-2 L/D Cassette 좌우 UnGrip POS";
            // 
            // panel41
            // 
            this.panel41.BackColor = System.Drawing.Color.Gray;
            this.panel41.Location = new System.Drawing.Point(433, 442);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(40, 40);
            this.panel41.TabIndex = 114;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label42.ForeColor = System.Drawing.Color.White;
            this.label42.Location = new System.Drawing.Point(479, 409);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(297, 13);
            this.label42.TabIndex = 113;
            this.label42.Text = "[ X138 ] 2-2 L/D Cassette 좌우 Grip POS";
            // 
            // panel42
            // 
            this.panel42.BackColor = System.Drawing.Color.Gray;
            this.panel42.Location = new System.Drawing.Point(433, 396);
            this.panel42.Name = "panel42";
            this.panel42.Size = new System.Drawing.Size(40, 40);
            this.panel42.TabIndex = 112;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label43.ForeColor = System.Drawing.Color.White;
            this.label43.Location = new System.Drawing.Point(479, 364);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(316, 13);
            this.label43.TabIndex = 111;
            this.label43.Text = "[ X137 ] 2-1 L/D Cassette 좌우 UnGrip POS";
            // 
            // panel43
            // 
            this.panel43.BackColor = System.Drawing.Color.Gray;
            this.panel43.Location = new System.Drawing.Point(433, 350);
            this.panel43.Name = "panel43";
            this.panel43.Size = new System.Drawing.Size(40, 40);
            this.panel43.TabIndex = 110;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label44.ForeColor = System.Drawing.Color.White;
            this.label44.Location = new System.Drawing.Point(479, 319);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(297, 13);
            this.label44.TabIndex = 109;
            this.label44.Text = "[ X136 ] 2-1 L/D Cassette 좌우 Grip POS";
            // 
            // panel44
            // 
            this.panel44.BackColor = System.Drawing.Color.Gray;
            this.panel44.Location = new System.Drawing.Point(433, 304);
            this.panel44.Name = "panel44";
            this.panel44.Size = new System.Drawing.Size(40, 40);
            this.panel44.TabIndex = 108;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label45.ForeColor = System.Drawing.Color.White;
            this.label45.Location = new System.Drawing.Point(479, 271);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(300, 13);
            this.label45.TabIndex = 107;
            this.label45.Text = "[ X135 ] 1 L/D Cassette 하부 UnGrip POS";
            // 
            // panel45
            // 
            this.panel45.BackColor = System.Drawing.Color.Gray;
            this.panel45.Location = new System.Drawing.Point(433, 258);
            this.panel45.Name = "panel45";
            this.panel45.Size = new System.Drawing.Size(40, 40);
            this.panel45.TabIndex = 106;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label46.ForeColor = System.Drawing.Color.White;
            this.label46.Location = new System.Drawing.Point(479, 226);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(281, 13);
            this.label46.TabIndex = 105;
            this.label46.Text = "[ X134 ] 1 L/D Cassette 하부 Grip POS";
            // 
            // panel46
            // 
            this.panel46.BackColor = System.Drawing.Color.Gray;
            this.panel46.Location = new System.Drawing.Point(433, 212);
            this.panel46.Name = "panel46";
            this.panel46.Size = new System.Drawing.Size(40, 40);
            this.panel46.TabIndex = 104;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label47.ForeColor = System.Drawing.Color.White;
            this.label47.Location = new System.Drawing.Point(479, 179);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(316, 13);
            this.label47.TabIndex = 103;
            this.label47.Text = "[ X133 ] 1-2 L/D Cassette 좌우 UnGrip POS";
            // 
            // panel47
            // 
            this.panel47.BackColor = System.Drawing.Color.Gray;
            this.panel47.Location = new System.Drawing.Point(433, 166);
            this.panel47.Name = "panel47";
            this.panel47.Size = new System.Drawing.Size(40, 40);
            this.panel47.TabIndex = 102;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label48.ForeColor = System.Drawing.Color.White;
            this.label48.Location = new System.Drawing.Point(479, 133);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(297, 13);
            this.label48.TabIndex = 101;
            this.label48.Text = "[ X132 ] 1-2 L/D Cassette 좌우 Grip POS";
            // 
            // panel48
            // 
            this.panel48.BackColor = System.Drawing.Color.Gray;
            this.panel48.Location = new System.Drawing.Point(433, 120);
            this.panel48.Name = "panel48";
            this.panel48.Size = new System.Drawing.Size(40, 40);
            this.panel48.TabIndex = 100;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label49.ForeColor = System.Drawing.Color.White;
            this.label49.Location = new System.Drawing.Point(479, 86);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(316, 13);
            this.label49.TabIndex = 99;
            this.label49.Text = "[ X131 ] 1-1 L/D Cassette 좌우 UnGrip POS";
            // 
            // panel49
            // 
            this.panel49.BackColor = System.Drawing.Color.Gray;
            this.panel49.Location = new System.Drawing.Point(433, 74);
            this.panel49.Name = "panel49";
            this.panel49.Size = new System.Drawing.Size(40, 40);
            this.panel49.TabIndex = 98;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label50.ForeColor = System.Drawing.Color.White;
            this.label50.Location = new System.Drawing.Point(479, 42);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(297, 13);
            this.label50.TabIndex = 97;
            this.label50.Text = "[ X130 ] 1-1 L/D Cassette 좌우 Grip POS";
            // 
            // panel50
            // 
            this.panel50.BackColor = System.Drawing.Color.Gray;
            this.panel50.Location = new System.Drawing.Point(433, 28);
            this.panel50.Name = "panel50";
            this.panel50.Size = new System.Drawing.Size(40, 40);
            this.panel50.TabIndex = 96;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label51.ForeColor = System.Drawing.Color.White;
            this.label51.Location = new System.Drawing.Point(52, 730);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(114, 13);
            this.label51.TabIndex = 95;
            this.label51.Text = "[ X12F ] Spare";
            // 
            // panel51
            // 
            this.panel51.BackColor = System.Drawing.Color.Gray;
            this.panel51.Location = new System.Drawing.Point(6, 718);
            this.panel51.Name = "panel51";
            this.panel51.Size = new System.Drawing.Size(40, 40);
            this.panel51.TabIndex = 94;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label52.ForeColor = System.Drawing.Color.White;
            this.label52.Location = new System.Drawing.Point(52, 685);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(244, 13);
            this.label52.TabIndex = 93;
            this.label52.Text = "[ X12E ] 2-3 L/D Cassette Detect";
            // 
            // panel52
            // 
            this.panel52.BackColor = System.Drawing.Color.Gray;
            this.panel52.Location = new System.Drawing.Point(6, 672);
            this.panel52.Name = "panel52";
            this.panel52.Size = new System.Drawing.Size(40, 40);
            this.panel52.TabIndex = 92;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label53.ForeColor = System.Drawing.Color.White;
            this.label53.Location = new System.Drawing.Point(52, 640);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(245, 13);
            this.label53.TabIndex = 91;
            this.label53.Text = "[ X12D ] 2-2 L/D Cassette Detect";
            // 
            // panel53
            // 
            this.panel53.BackColor = System.Drawing.Color.Gray;
            this.panel53.Location = new System.Drawing.Point(6, 626);
            this.panel53.Name = "panel53";
            this.panel53.Size = new System.Drawing.Size(40, 40);
            this.panel53.TabIndex = 90;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label54.ForeColor = System.Drawing.Color.White;
            this.label54.Location = new System.Drawing.Point(52, 594);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(245, 13);
            this.label54.TabIndex = 89;
            this.label54.Text = "[ X12C ] 2-1 L/D Cassette Detect";
            // 
            // panel54
            // 
            this.panel54.BackColor = System.Drawing.Color.Gray;
            this.panel54.Location = new System.Drawing.Point(6, 580);
            this.panel54.Name = "panel54";
            this.panel54.Size = new System.Drawing.Size(40, 40);
            this.panel54.TabIndex = 88;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label55.ForeColor = System.Drawing.Color.White;
            this.label55.Location = new System.Drawing.Point(52, 548);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(245, 13);
            this.label55.TabIndex = 87;
            this.label55.Text = "[ X12B ] 1-3 L/D Cassette Detect";
            // 
            // panel55
            // 
            this.panel55.BackColor = System.Drawing.Color.Gray;
            this.panel55.Location = new System.Drawing.Point(6, 534);
            this.panel55.Name = "panel55";
            this.panel55.Size = new System.Drawing.Size(40, 40);
            this.panel55.TabIndex = 86;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label56.ForeColor = System.Drawing.Color.White;
            this.label56.Location = new System.Drawing.Point(52, 501);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(244, 13);
            this.label56.TabIndex = 85;
            this.label56.Text = "[ X12A ] 1-2 L/D Cassette Detect";
            // 
            // panel56
            // 
            this.panel56.BackColor = System.Drawing.Color.Gray;
            this.panel56.Location = new System.Drawing.Point(6, 488);
            this.panel56.Name = "panel56";
            this.panel56.Size = new System.Drawing.Size(40, 40);
            this.panel56.TabIndex = 84;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label57.ForeColor = System.Drawing.Color.White;
            this.label57.Location = new System.Drawing.Point(52, 454);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(243, 13);
            this.label57.TabIndex = 83;
            this.label57.Text = "[ X129 ] 1-1 L/D Cassette Detect";
            // 
            // panel57
            // 
            this.panel57.BackColor = System.Drawing.Color.Gray;
            this.panel57.Location = new System.Drawing.Point(6, 442);
            this.panel57.Name = "panel57";
            this.panel57.Size = new System.Drawing.Size(40, 40);
            this.panel57.TabIndex = 82;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label58.ForeColor = System.Drawing.Color.White;
            this.label58.Location = new System.Drawing.Point(52, 409);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(202, 13);
            this.label58.TabIndex = 81;
            this.label58.Text = "[ X128 ] Control Select L/D";
            // 
            // panel58
            // 
            this.panel58.BackColor = System.Drawing.Color.Gray;
            this.panel58.Location = new System.Drawing.Point(6, 396);
            this.panel58.Name = "panel58";
            this.panel58.Size = new System.Drawing.Size(40, 40);
            this.panel58.TabIndex = 80;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label59.ForeColor = System.Drawing.Color.White;
            this.label59.Location = new System.Drawing.Point(52, 364);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(114, 13);
            this.label59.TabIndex = 79;
            this.label59.Text = "[ X127 ] Spare";
            // 
            // panel59
            // 
            this.panel59.BackColor = System.Drawing.Color.Gray;
            this.panel59.Location = new System.Drawing.Point(6, 350);
            this.panel59.Name = "panel59";
            this.panel59.Size = new System.Drawing.Size(40, 40);
            this.panel59.TabIndex = 78;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label60.ForeColor = System.Drawing.Color.White;
            this.label60.Location = new System.Drawing.Point(52, 319);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(114, 13);
            this.label60.TabIndex = 77;
            this.label60.Text = "[ X126 ] Spare";
            // 
            // panel60
            // 
            this.panel60.BackColor = System.Drawing.Color.Gray;
            this.panel60.Location = new System.Drawing.Point(6, 304);
            this.panel60.Name = "panel60";
            this.panel60.Size = new System.Drawing.Size(40, 40);
            this.panel60.TabIndex = 76;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label61.ForeColor = System.Drawing.Color.White;
            this.label61.Location = new System.Drawing.Point(52, 271);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(114, 13);
            this.label61.TabIndex = 75;
            this.label61.Text = "[ X125 ] Spare";
            // 
            // panel61
            // 
            this.panel61.BackColor = System.Drawing.Color.Gray;
            this.panel61.Location = new System.Drawing.Point(6, 258);
            this.panel61.Name = "panel61";
            this.panel61.Size = new System.Drawing.Size(40, 40);
            this.panel61.TabIndex = 74;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label62.ForeColor = System.Drawing.Color.White;
            this.label62.Location = new System.Drawing.Point(52, 226);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(143, 13);
            this.label62.TabIndex = 73;
            this.label62.Text = "[ X124 ] EFU Alarm";
            // 
            // panel62
            // 
            this.panel62.BackColor = System.Drawing.Color.Gray;
            this.panel62.Location = new System.Drawing.Point(6, 212);
            this.panel62.Name = "panel62";
            this.panel62.Size = new System.Drawing.Size(40, 40);
            this.panel62.TabIndex = 72;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label63.ForeColor = System.Drawing.Color.White;
            this.label63.Location = new System.Drawing.Point(52, 179);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(166, 13);
            this.label63.TabIndex = 71;
            this.label63.Text = "[ X123 ] 가공부 MC ON";
            // 
            // panel63
            // 
            this.panel63.BackColor = System.Drawing.Color.Gray;
            this.panel63.Location = new System.Drawing.Point(6, 166);
            this.panel63.Name = "panel63";
            this.panel63.Size = new System.Drawing.Size(40, 40);
            this.panel63.TabIndex = 70;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label64.ForeColor = System.Drawing.Color.White;
            this.label64.Location = new System.Drawing.Point(52, 133);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(166, 13);
            this.label64.TabIndex = 69;
            this.label64.Text = "[ X122 ] 로더부 MC ON";
            // 
            // panel64
            // 
            this.panel64.BackColor = System.Drawing.Color.Gray;
            this.panel64.Location = new System.Drawing.Point(6, 120);
            this.panel64.Name = "panel64";
            this.panel64.Size = new System.Drawing.Size(40, 40);
            this.panel64.TabIndex = 68;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label65.ForeColor = System.Drawing.Color.White;
            this.label65.Location = new System.Drawing.Point(52, 86);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(251, 13);
            this.label65.TabIndex = 67;
            this.label65.Text = "[ X121 ] Mode Select SW - Teach";
            // 
            // panel65
            // 
            this.panel65.BackColor = System.Drawing.Color.Gray;
            this.panel65.Location = new System.Drawing.Point(6, 74);
            this.panel65.Name = "panel65";
            this.panel65.Size = new System.Drawing.Size(40, 40);
            this.panel65.TabIndex = 66;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label66.ForeColor = System.Drawing.Color.White;
            this.label66.Location = new System.Drawing.Point(52, 42);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(237, 13);
            this.label66.TabIndex = 65;
            this.label66.Text = "[ X120 ] Mode Select SW - Auto";
            // 
            // panel66
            // 
            this.panel66.BackColor = System.Drawing.Color.Gray;
            this.panel66.Location = new System.Drawing.Point(6, 28);
            this.panel66.Name = "panel66";
            this.panel66.Size = new System.Drawing.Size(40, 40);
            this.panel66.TabIndex = 64;
            // 
            // tp_iostatus_process_in3
            // 
            this.tp_iostatus_process_in3.BackColor = System.Drawing.Color.DimGray;
            this.tp_iostatus_process_in3.Controls.Add(this.label67);
            this.tp_iostatus_process_in3.Controls.Add(this.panel67);
            this.tp_iostatus_process_in3.Controls.Add(this.label68);
            this.tp_iostatus_process_in3.Controls.Add(this.panel68);
            this.tp_iostatus_process_in3.Controls.Add(this.label69);
            this.tp_iostatus_process_in3.Controls.Add(this.panel69);
            this.tp_iostatus_process_in3.Controls.Add(this.label70);
            this.tp_iostatus_process_in3.Controls.Add(this.panel70);
            this.tp_iostatus_process_in3.Controls.Add(this.label71);
            this.tp_iostatus_process_in3.Controls.Add(this.panel71);
            this.tp_iostatus_process_in3.Controls.Add(this.label72);
            this.tp_iostatus_process_in3.Controls.Add(this.panel72);
            this.tp_iostatus_process_in3.Controls.Add(this.label73);
            this.tp_iostatus_process_in3.Controls.Add(this.panel73);
            this.tp_iostatus_process_in3.Controls.Add(this.label74);
            this.tp_iostatus_process_in3.Controls.Add(this.panel74);
            this.tp_iostatus_process_in3.Controls.Add(this.label75);
            this.tp_iostatus_process_in3.Controls.Add(this.panel75);
            this.tp_iostatus_process_in3.Controls.Add(this.label76);
            this.tp_iostatus_process_in3.Controls.Add(this.panel76);
            this.tp_iostatus_process_in3.Controls.Add(this.label77);
            this.tp_iostatus_process_in3.Controls.Add(this.panel77);
            this.tp_iostatus_process_in3.Controls.Add(this.label78);
            this.tp_iostatus_process_in3.Controls.Add(this.panel78);
            this.tp_iostatus_process_in3.Controls.Add(this.label79);
            this.tp_iostatus_process_in3.Controls.Add(this.panel79);
            this.tp_iostatus_process_in3.Controls.Add(this.label80);
            this.tp_iostatus_process_in3.Controls.Add(this.panel80);
            this.tp_iostatus_process_in3.Controls.Add(this.label81);
            this.tp_iostatus_process_in3.Controls.Add(this.panel81);
            this.tp_iostatus_process_in3.Controls.Add(this.label82);
            this.tp_iostatus_process_in3.Controls.Add(this.panel82);
            this.tp_iostatus_process_in3.Controls.Add(this.label83);
            this.tp_iostatus_process_in3.Controls.Add(this.panel83);
            this.tp_iostatus_process_in3.Controls.Add(this.label84);
            this.tp_iostatus_process_in3.Controls.Add(this.panel84);
            this.tp_iostatus_process_in3.Controls.Add(this.label85);
            this.tp_iostatus_process_in3.Controls.Add(this.panel85);
            this.tp_iostatus_process_in3.Controls.Add(this.label86);
            this.tp_iostatus_process_in3.Controls.Add(this.panel86);
            this.tp_iostatus_process_in3.Controls.Add(this.label87);
            this.tp_iostatus_process_in3.Controls.Add(this.panel87);
            this.tp_iostatus_process_in3.Controls.Add(this.label88);
            this.tp_iostatus_process_in3.Controls.Add(this.panel88);
            this.tp_iostatus_process_in3.Controls.Add(this.label89);
            this.tp_iostatus_process_in3.Controls.Add(this.panel89);
            this.tp_iostatus_process_in3.Controls.Add(this.label90);
            this.tp_iostatus_process_in3.Controls.Add(this.panel90);
            this.tp_iostatus_process_in3.Controls.Add(this.label91);
            this.tp_iostatus_process_in3.Controls.Add(this.panel91);
            this.tp_iostatus_process_in3.Controls.Add(this.label92);
            this.tp_iostatus_process_in3.Controls.Add(this.panel92);
            this.tp_iostatus_process_in3.Controls.Add(this.label93);
            this.tp_iostatus_process_in3.Controls.Add(this.panel93);
            this.tp_iostatus_process_in3.Controls.Add(this.label94);
            this.tp_iostatus_process_in3.Controls.Add(this.panel94);
            this.tp_iostatus_process_in3.Controls.Add(this.label95);
            this.tp_iostatus_process_in3.Controls.Add(this.panel95);
            this.tp_iostatus_process_in3.Controls.Add(this.label96);
            this.tp_iostatus_process_in3.Controls.Add(this.panel96);
            this.tp_iostatus_process_in3.Controls.Add(this.label97);
            this.tp_iostatus_process_in3.Controls.Add(this.panel97);
            this.tp_iostatus_process_in3.Controls.Add(this.label98);
            this.tp_iostatus_process_in3.Controls.Add(this.panel98);
            this.tp_iostatus_process_in3.Location = new System.Drawing.Point(4, 34);
            this.tp_iostatus_process_in3.Name = "tp_iostatus_process_in3";
            this.tp_iostatus_process_in3.Padding = new System.Windows.Forms.Padding(3);
            this.tp_iostatus_process_in3.Size = new System.Drawing.Size(846, 778);
            this.tp_iostatus_process_in3.TabIndex = 2;
            this.tp_iostatus_process_in3.Text = "IN - 3";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label67.ForeColor = System.Drawing.Color.White;
            this.label67.Location = new System.Drawing.Point(479, 730);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(114, 13);
            this.label67.TabIndex = 191;
            this.label67.Text = "[ X15F ] Spare";
            // 
            // panel67
            // 
            this.panel67.BackColor = System.Drawing.Color.Gray;
            this.panel67.Location = new System.Drawing.Point(433, 718);
            this.panel67.Name = "panel67";
            this.panel67.Size = new System.Drawing.Size(40, 40);
            this.panel67.TabIndex = 190;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label68.ForeColor = System.Drawing.Color.White;
            this.label68.Location = new System.Drawing.Point(479, 685);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(241, 13);
            this.label68.TabIndex = 189;
            this.label68.Text = "[ X15E ] 6 가공부 FAN Stop Alarm";
            // 
            // panel68
            // 
            this.panel68.BackColor = System.Drawing.Color.Gray;
            this.panel68.Location = new System.Drawing.Point(433, 672);
            this.panel68.Name = "panel68";
            this.panel68.Size = new System.Drawing.Size(40, 40);
            this.panel68.TabIndex = 188;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label69.ForeColor = System.Drawing.Color.White;
            this.label69.Location = new System.Drawing.Point(479, 640);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(207, 13);
            this.label69.TabIndex = 187;
            this.label69.Text = "[ X15D ] 2 L/D 이재기 DOWN";
            // 
            // panel69
            // 
            this.panel69.BackColor = System.Drawing.Color.Gray;
            this.panel69.Location = new System.Drawing.Point(433, 626);
            this.panel69.Name = "panel69";
            this.panel69.Size = new System.Drawing.Size(40, 40);
            this.panel69.TabIndex = 186;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label70.ForeColor = System.Drawing.Color.White;
            this.label70.Location = new System.Drawing.Point(479, 594);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(183, 13);
            this.label70.TabIndex = 185;
            this.label70.Text = "[ X15C ] 2 L/D 이재기 UP";
            // 
            // panel70
            // 
            this.panel70.BackColor = System.Drawing.Color.Gray;
            this.panel70.Location = new System.Drawing.Point(433, 580);
            this.panel70.Name = "panel70";
            this.panel70.Size = new System.Drawing.Size(40, 40);
            this.panel70.TabIndex = 184;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label71.ForeColor = System.Drawing.Color.White;
            this.label71.Location = new System.Drawing.Point(479, 548);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(207, 13);
            this.label71.TabIndex = 183;
            this.label71.Text = "[ X15B ] 1 L/D 이재기 DOWN";
            // 
            // panel71
            // 
            this.panel71.BackColor = System.Drawing.Color.Gray;
            this.panel71.Location = new System.Drawing.Point(433, 534);
            this.panel71.Name = "panel71";
            this.panel71.Size = new System.Drawing.Size(40, 40);
            this.panel71.TabIndex = 182;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label72.ForeColor = System.Drawing.Color.White;
            this.label72.Location = new System.Drawing.Point(479, 501);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(182, 13);
            this.label72.TabIndex = 181;
            this.label72.Text = "[ X15A ] 1 L/D 이재기 UP";
            // 
            // panel72
            // 
            this.panel72.BackColor = System.Drawing.Color.Gray;
            this.panel72.Location = new System.Drawing.Point(433, 488);
            this.panel72.Name = "panel72";
            this.panel72.Size = new System.Drawing.Size(40, 40);
            this.panel72.TabIndex = 180;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label73.ForeColor = System.Drawing.Color.White;
            this.label73.Location = new System.Drawing.Point(479, 454);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(356, 13);
            this.label73.TabIndex = 179;
            this.label73.Text = "[ X159 ] 2 L/D Cassette Lift Tilting Sensor DOWN";
            // 
            // panel73
            // 
            this.panel73.BackColor = System.Drawing.Color.Gray;
            this.panel73.Location = new System.Drawing.Point(433, 442);
            this.panel73.Name = "panel73";
            this.panel73.Size = new System.Drawing.Size(40, 40);
            this.panel73.TabIndex = 178;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label74.ForeColor = System.Drawing.Color.White;
            this.label74.Location = new System.Drawing.Point(479, 409);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(332, 13);
            this.label74.TabIndex = 177;
            this.label74.Text = "[ X158 ] 2 L/D Cassette Lift Tilting Sensor UP";
            // 
            // panel74
            // 
            this.panel74.BackColor = System.Drawing.Color.Gray;
            this.panel74.Location = new System.Drawing.Point(433, 396);
            this.panel74.Name = "panel74";
            this.panel74.Size = new System.Drawing.Size(40, 40);
            this.panel74.TabIndex = 176;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label75.ForeColor = System.Drawing.Color.White;
            this.label75.Location = new System.Drawing.Point(479, 364);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(327, 13);
            this.label75.TabIndex = 175;
            this.label75.Text = "[ X157 ] 2 Cassette 취출 이재기 Pressure SW";
            // 
            // panel75
            // 
            this.panel75.BackColor = System.Drawing.Color.Gray;
            this.panel75.Location = new System.Drawing.Point(433, 350);
            this.panel75.Name = "panel75";
            this.panel75.Size = new System.Drawing.Size(40, 40);
            this.panel75.TabIndex = 174;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label76.ForeColor = System.Drawing.Color.White;
            this.label76.Location = new System.Drawing.Point(479, 319);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(327, 13);
            this.label76.TabIndex = 173;
            this.label76.Text = "[ X156 ] 1 Cassette 취출 이재기 Pressure SW";
            // 
            // panel76
            // 
            this.panel76.BackColor = System.Drawing.Color.Gray;
            this.panel76.Location = new System.Drawing.Point(433, 304);
            this.panel76.Name = "panel76";
            this.panel76.Size = new System.Drawing.Size(40, 40);
            this.panel76.TabIndex = 172;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label77.ForeColor = System.Drawing.Color.White;
            this.label77.Location = new System.Drawing.Point(479, 271);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(356, 13);
            this.label77.TabIndex = 171;
            this.label77.Text = "[ X155 ] 1 L/D Cassette Lift Tilting Sensor DOWN";
            // 
            // panel77
            // 
            this.panel77.BackColor = System.Drawing.Color.Gray;
            this.panel77.Location = new System.Drawing.Point(433, 258);
            this.panel77.Name = "panel77";
            this.panel77.Size = new System.Drawing.Size(40, 40);
            this.panel77.TabIndex = 170;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label78.ForeColor = System.Drawing.Color.White;
            this.label78.Location = new System.Drawing.Point(479, 226);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(332, 13);
            this.label78.TabIndex = 169;
            this.label78.Text = "[ X154 ] 1 L/D Cassette Lift Tilting Sensor UP";
            // 
            // panel78
            // 
            this.panel78.BackColor = System.Drawing.Color.Gray;
            this.panel78.Location = new System.Drawing.Point(433, 212);
            this.panel78.Name = "panel78";
            this.panel78.Size = new System.Drawing.Size(40, 40);
            this.panel78.TabIndex = 168;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label79.ForeColor = System.Drawing.Color.White;
            this.label79.Location = new System.Drawing.Point(479, 179);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(321, 13);
            this.label79.TabIndex = 167;
            this.label79.Text = "[ X153 ] 2-2 L/D Cassette Lifter Ungrip POS";
            // 
            // panel79
            // 
            this.panel79.BackColor = System.Drawing.Color.Gray;
            this.panel79.Location = new System.Drawing.Point(433, 166);
            this.panel79.Name = "panel79";
            this.panel79.Size = new System.Drawing.Size(40, 40);
            this.panel79.TabIndex = 166;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label80.ForeColor = System.Drawing.Color.White;
            this.label80.Location = new System.Drawing.Point(479, 133);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(304, 13);
            this.label80.TabIndex = 165;
            this.label80.Text = "[ X152 ] 2-2 L/D Cassette Lifter Grip POS";
            // 
            // panel80
            // 
            this.panel80.BackColor = System.Drawing.Color.Gray;
            this.panel80.Location = new System.Drawing.Point(433, 120);
            this.panel80.Name = "panel80";
            this.panel80.Size = new System.Drawing.Size(40, 40);
            this.panel80.TabIndex = 164;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label81.ForeColor = System.Drawing.Color.White;
            this.label81.Location = new System.Drawing.Point(479, 86);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(321, 13);
            this.label81.TabIndex = 163;
            this.label81.Text = "[ X151 ] 2-1 L/D Cassette Lifter Ungrip POS";
            // 
            // panel81
            // 
            this.panel81.BackColor = System.Drawing.Color.Gray;
            this.panel81.Location = new System.Drawing.Point(433, 74);
            this.panel81.Name = "panel81";
            this.panel81.Size = new System.Drawing.Size(40, 40);
            this.panel81.TabIndex = 162;
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label82.ForeColor = System.Drawing.Color.White;
            this.label82.Location = new System.Drawing.Point(479, 42);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(304, 13);
            this.label82.TabIndex = 161;
            this.label82.Text = "[ X150 ] 2-1 L/D Cassette Lifter Grip POS";
            // 
            // panel82
            // 
            this.panel82.BackColor = System.Drawing.Color.Gray;
            this.panel82.Location = new System.Drawing.Point(433, 28);
            this.panel82.Name = "panel82";
            this.panel82.Size = new System.Drawing.Size(40, 40);
            this.panel82.TabIndex = 160;
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label83.ForeColor = System.Drawing.Color.White;
            this.label83.Location = new System.Drawing.Point(52, 730);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(321, 13);
            this.label83.TabIndex = 159;
            this.label83.Text = "[ X14F ] 1-2 L/D Cassette Lifter Ungrip POS";
            // 
            // panel83
            // 
            this.panel83.BackColor = System.Drawing.Color.Gray;
            this.panel83.Location = new System.Drawing.Point(6, 718);
            this.panel83.Name = "panel83";
            this.panel83.Size = new System.Drawing.Size(40, 40);
            this.panel83.TabIndex = 158;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label84.ForeColor = System.Drawing.Color.White;
            this.label84.Location = new System.Drawing.Point(52, 685);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(305, 13);
            this.label84.TabIndex = 157;
            this.label84.Text = "[ X14E ] 1-2 L/D Cassette Lifter Grip POS";
            // 
            // panel84
            // 
            this.panel84.BackColor = System.Drawing.Color.Gray;
            this.panel84.Location = new System.Drawing.Point(6, 672);
            this.panel84.Name = "panel84";
            this.panel84.Size = new System.Drawing.Size(40, 40);
            this.panel84.TabIndex = 156;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label85.ForeColor = System.Drawing.Color.White;
            this.label85.Location = new System.Drawing.Point(52, 640);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(323, 13);
            this.label85.TabIndex = 155;
            this.label85.Text = "[ X14D ] 1-1 L/D Cassette Lifter Ungrip POS";
            // 
            // panel85
            // 
            this.panel85.BackColor = System.Drawing.Color.Gray;
            this.panel85.Location = new System.Drawing.Point(6, 626);
            this.panel85.Name = "panel85";
            this.panel85.Size = new System.Drawing.Size(40, 40);
            this.panel85.TabIndex = 154;
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label86.ForeColor = System.Drawing.Color.White;
            this.label86.Location = new System.Drawing.Point(52, 594);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(306, 13);
            this.label86.TabIndex = 153;
            this.label86.Text = "[ X14C ] 1-1 L/D Cassette Lifter Grip POS";
            // 
            // panel86
            // 
            this.panel86.BackColor = System.Drawing.Color.Gray;
            this.panel86.Location = new System.Drawing.Point(6, 580);
            this.panel86.Name = "panel86";
            this.panel86.Size = new System.Drawing.Size(40, 40);
            this.panel86.TabIndex = 152;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label87.ForeColor = System.Drawing.Color.White;
            this.label87.Location = new System.Drawing.Point(52, 548);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(300, 13);
            this.label87.TabIndex = 151;
            this.label87.Text = "[ X14B ] 4 L/D Cassette 하부 Ungrip POS";
            // 
            // panel87
            // 
            this.panel87.BackColor = System.Drawing.Color.Gray;
            this.panel87.Location = new System.Drawing.Point(6, 534);
            this.panel87.Name = "panel87";
            this.panel87.Size = new System.Drawing.Size(40, 40);
            this.panel87.TabIndex = 150;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label88.ForeColor = System.Drawing.Color.White;
            this.label88.Location = new System.Drawing.Point(52, 501);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(282, 13);
            this.label88.TabIndex = 149;
            this.label88.Text = "[ X14A ] 4 L/D Cassette 하부 Grip POS";
            // 
            // panel88
            // 
            this.panel88.BackColor = System.Drawing.Color.Gray;
            this.panel88.Location = new System.Drawing.Point(6, 488);
            this.panel88.Name = "panel88";
            this.panel88.Size = new System.Drawing.Size(40, 40);
            this.panel88.TabIndex = 148;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label89.ForeColor = System.Drawing.Color.White;
            this.label89.Location = new System.Drawing.Point(52, 454);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(314, 13);
            this.label89.TabIndex = 147;
            this.label89.Text = "[ X149 ] 4-2 L/D Cassette 좌우 Ungrip POS";
            // 
            // panel89
            // 
            this.panel89.BackColor = System.Drawing.Color.Gray;
            this.panel89.Location = new System.Drawing.Point(6, 442);
            this.panel89.Name = "panel89";
            this.panel89.Size = new System.Drawing.Size(40, 40);
            this.panel89.TabIndex = 146;
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label90.ForeColor = System.Drawing.Color.White;
            this.label90.Location = new System.Drawing.Point(52, 409);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(297, 13);
            this.label90.TabIndex = 145;
            this.label90.Text = "[ X148 ] 4-2 L/D Cassette 좌우 Grip POS";
            // 
            // panel90
            // 
            this.panel90.BackColor = System.Drawing.Color.Gray;
            this.panel90.Location = new System.Drawing.Point(6, 396);
            this.panel90.Name = "panel90";
            this.panel90.Size = new System.Drawing.Size(40, 40);
            this.panel90.TabIndex = 144;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label91.ForeColor = System.Drawing.Color.White;
            this.label91.Location = new System.Drawing.Point(52, 364);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(314, 13);
            this.label91.TabIndex = 143;
            this.label91.Text = "[ X147 ] 4-1 L/D Cassette 좌우 Ungrip POS";
            // 
            // panel91
            // 
            this.panel91.BackColor = System.Drawing.Color.Gray;
            this.panel91.Location = new System.Drawing.Point(6, 350);
            this.panel91.Name = "panel91";
            this.panel91.Size = new System.Drawing.Size(40, 40);
            this.panel91.TabIndex = 142;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label92.ForeColor = System.Drawing.Color.White;
            this.label92.Location = new System.Drawing.Point(52, 319);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(297, 13);
            this.label92.TabIndex = 141;
            this.label92.Text = "[ X146 ] 4-1 L/D Cassette 좌우 Grip POS";
            // 
            // panel92
            // 
            this.panel92.BackColor = System.Drawing.Color.Gray;
            this.panel92.Location = new System.Drawing.Point(6, 304);
            this.panel92.Name = "panel92";
            this.panel92.Size = new System.Drawing.Size(40, 40);
            this.panel92.TabIndex = 140;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label93.ForeColor = System.Drawing.Color.White;
            this.label93.Location = new System.Drawing.Point(52, 271);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(298, 13);
            this.label93.TabIndex = 139;
            this.label93.Text = "[ X145 ] 3 L/D Cassette 하부 Ungrip POS";
            // 
            // panel93
            // 
            this.panel93.BackColor = System.Drawing.Color.Gray;
            this.panel93.Location = new System.Drawing.Point(6, 258);
            this.panel93.Name = "panel93";
            this.panel93.Size = new System.Drawing.Size(40, 40);
            this.panel93.TabIndex = 138;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label94.ForeColor = System.Drawing.Color.White;
            this.label94.Location = new System.Drawing.Point(52, 226);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(281, 13);
            this.label94.TabIndex = 137;
            this.label94.Text = "[ X144 ] 3 L/D Cassette 하부 Grip POS";
            // 
            // panel94
            // 
            this.panel94.BackColor = System.Drawing.Color.Gray;
            this.panel94.Location = new System.Drawing.Point(6, 212);
            this.panel94.Name = "panel94";
            this.panel94.Size = new System.Drawing.Size(40, 40);
            this.panel94.TabIndex = 136;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label95.ForeColor = System.Drawing.Color.White;
            this.label95.Location = new System.Drawing.Point(52, 179);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(311, 13);
            this.label95.TabIndex = 135;
            this.label95.Text = "[ X143 ] 3-2 L/D Cassette 좌우 Ungrip Pos";
            // 
            // panel95
            // 
            this.panel95.BackColor = System.Drawing.Color.Gray;
            this.panel95.Location = new System.Drawing.Point(6, 166);
            this.panel95.Name = "panel95";
            this.panel95.Size = new System.Drawing.Size(40, 40);
            this.panel95.TabIndex = 134;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label96.ForeColor = System.Drawing.Color.White;
            this.label96.Location = new System.Drawing.Point(52, 133);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(294, 13);
            this.label96.TabIndex = 133;
            this.label96.Text = "[ X142 ] 3-2 L/D Cassette 좌우 Grip Pos";
            // 
            // panel96
            // 
            this.panel96.BackColor = System.Drawing.Color.Gray;
            this.panel96.Location = new System.Drawing.Point(6, 120);
            this.panel96.Name = "panel96";
            this.panel96.Size = new System.Drawing.Size(40, 40);
            this.panel96.TabIndex = 132;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label97.ForeColor = System.Drawing.Color.White;
            this.label97.Location = new System.Drawing.Point(52, 86);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(314, 13);
            this.label97.TabIndex = 131;
            this.label97.Text = "[ X141 ] 3-1 L/D Cassette 좌우 Ungrip POS";
            // 
            // panel97
            // 
            this.panel97.BackColor = System.Drawing.Color.Gray;
            this.panel97.Location = new System.Drawing.Point(6, 74);
            this.panel97.Name = "panel97";
            this.panel97.Size = new System.Drawing.Size(40, 40);
            this.panel97.TabIndex = 130;
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label98.ForeColor = System.Drawing.Color.White;
            this.label98.Location = new System.Drawing.Point(52, 42);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(297, 13);
            this.label98.TabIndex = 129;
            this.label98.Text = "[ X140 ] 3-1 L/D Cassette 좌우 Grip POS";
            // 
            // panel98
            // 
            this.panel98.BackColor = System.Drawing.Color.Gray;
            this.panel98.Location = new System.Drawing.Point(6, 28);
            this.panel98.Name = "panel98";
            this.panel98.Size = new System.Drawing.Size(40, 40);
            this.panel98.TabIndex = 128;
            // 
            // tp_iostatus_uld
            // 
            this.tp_iostatus_uld.BackColor = System.Drawing.Color.DarkSlateGray;
            this.tp_iostatus_uld.Controls.Add(this.tabControl1);
            this.tp_iostatus_uld.Controls.Add(this.tabControl2);
            this.tp_iostatus_uld.Location = new System.Drawing.Point(4, 34);
            this.tp_iostatus_uld.Name = "tp_iostatus_uld";
            this.tp_iostatus_uld.Padding = new System.Windows.Forms.Padding(3);
            this.tp_iostatus_uld.Size = new System.Drawing.Size(1726, 816);
            this.tp_iostatus_uld.TabIndex = 2;
            this.tp_iostatus_uld.Text = "언로더";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.ItemSize = new System.Drawing.Size(283, 30);
            this.tabControl1.Location = new System.Drawing.Point(6, 6);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(854, 816);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 4;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.DimGray;
            this.tabPage1.Controls.Add(this.inOutTotalControl1);
            this.tabPage1.Location = new System.Drawing.Point(4, 34);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(846, 778);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "IN - 1";
            // 
            // inOutTotalControl1
            // 
            this.inOutTotalControl1.Location = new System.Drawing.Point(-2, -1);
            this.inOutTotalControl1.Name = "inOutTotalControl1";
            this.inOutTotalControl1.Size = new System.Drawing.Size(851, 798);
            this.inOutTotalControl1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.DimGray;
            this.tabPage2.Location = new System.Drawing.Point(4, 34);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(846, 778);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "IN - 2";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.DimGray;
            this.tabPage3.Location = new System.Drawing.Point(4, 34);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(846, 778);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "IN - 3";
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Controls.Add(this.tabPage5);
            this.tabControl2.Controls.Add(this.tabPage6);
            this.tabControl2.ItemSize = new System.Drawing.Size(283, 30);
            this.tabControl2.Location = new System.Drawing.Point(866, 6);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(854, 816);
            this.tabControl2.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl2.TabIndex = 3;
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.DimGray;
            this.tabPage4.Controls.Add(this.label256);
            this.tabPage4.Controls.Add(this.panel257);
            this.tabPage4.Controls.Add(this.label257);
            this.tabPage4.Controls.Add(this.panel258);
            this.tabPage4.Controls.Add(this.label258);
            this.tabPage4.Controls.Add(this.panel259);
            this.tabPage4.Controls.Add(this.label259);
            this.tabPage4.Controls.Add(this.panel260);
            this.tabPage4.Controls.Add(this.label260);
            this.tabPage4.Controls.Add(this.panel261);
            this.tabPage4.Controls.Add(this.label261);
            this.tabPage4.Controls.Add(this.panel262);
            this.tabPage4.Controls.Add(this.label262);
            this.tabPage4.Controls.Add(this.panel263);
            this.tabPage4.Controls.Add(this.label263);
            this.tabPage4.Controls.Add(this.panel264);
            this.tabPage4.Controls.Add(this.label264);
            this.tabPage4.Controls.Add(this.panel265);
            this.tabPage4.Controls.Add(this.label265);
            this.tabPage4.Controls.Add(this.panel266);
            this.tabPage4.Controls.Add(this.label266);
            this.tabPage4.Controls.Add(this.panel267);
            this.tabPage4.Controls.Add(this.label267);
            this.tabPage4.Controls.Add(this.panel268);
            this.tabPage4.Controls.Add(this.label268);
            this.tabPage4.Controls.Add(this.panel269);
            this.tabPage4.Controls.Add(this.label269);
            this.tabPage4.Controls.Add(this.panel270);
            this.tabPage4.Controls.Add(this.label270);
            this.tabPage4.Controls.Add(this.panel271);
            this.tabPage4.Controls.Add(this.label271);
            this.tabPage4.Controls.Add(this.panel272);
            this.tabPage4.Controls.Add(this.label272);
            this.tabPage4.Controls.Add(this.panel273);
            this.tabPage4.Controls.Add(this.label273);
            this.tabPage4.Controls.Add(this.panel274);
            this.tabPage4.Controls.Add(this.label274);
            this.tabPage4.Controls.Add(this.panel275);
            this.tabPage4.Controls.Add(this.label275);
            this.tabPage4.Controls.Add(this.panel276);
            this.tabPage4.Controls.Add(this.label276);
            this.tabPage4.Controls.Add(this.panel277);
            this.tabPage4.Controls.Add(this.label277);
            this.tabPage4.Controls.Add(this.panel278);
            this.tabPage4.Controls.Add(this.label278);
            this.tabPage4.Controls.Add(this.panel279);
            this.tabPage4.Controls.Add(this.label279);
            this.tabPage4.Controls.Add(this.panel280);
            this.tabPage4.Controls.Add(this.label280);
            this.tabPage4.Controls.Add(this.panel281);
            this.tabPage4.Controls.Add(this.label281);
            this.tabPage4.Controls.Add(this.panel282);
            this.tabPage4.Controls.Add(this.label282);
            this.tabPage4.Controls.Add(this.panel283);
            this.tabPage4.Controls.Add(this.label283);
            this.tabPage4.Controls.Add(this.panel284);
            this.tabPage4.Controls.Add(this.label284);
            this.tabPage4.Controls.Add(this.panel285);
            this.tabPage4.Controls.Add(this.label285);
            this.tabPage4.Controls.Add(this.panel286);
            this.tabPage4.Controls.Add(this.label286);
            this.tabPage4.Controls.Add(this.panel287);
            this.tabPage4.Controls.Add(this.label287);
            this.tabPage4.Controls.Add(this.panel288);
            this.tabPage4.Location = new System.Drawing.Point(4, 34);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(846, 778);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "OUT - 1";
            // 
            // label256
            // 
            this.label256.AutoSize = true;
            this.label256.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label256.ForeColor = System.Drawing.Color.White;
            this.label256.Location = new System.Drawing.Point(479, 730);
            this.label256.Name = "label256";
            this.label256.Size = new System.Drawing.Size(240, 13);
            this.label256.TabIndex = 191;
            this.label256.Text = "[ Y1BF ] Spare 파기 AIR ON CMD";
            // 
            // panel257
            // 
            this.panel257.BackColor = System.Drawing.Color.Gray;
            this.panel257.Location = new System.Drawing.Point(433, 718);
            this.panel257.Name = "panel257";
            this.panel257.Size = new System.Drawing.Size(40, 40);
            this.panel257.TabIndex = 190;
            // 
            // label257
            // 
            this.label257.AutoSize = true;
            this.label257.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label257.ForeColor = System.Drawing.Color.White;
            this.label257.Location = new System.Drawing.Point(479, 685);
            this.label257.Name = "label257";
            this.label257.Size = new System.Drawing.Size(241, 13);
            this.label257.TabIndex = 189;
            this.label257.Text = "[ Y1BE ] Spare 파기 AIR ON CMD";
            // 
            // panel258
            // 
            this.panel258.BackColor = System.Drawing.Color.Gray;
            this.panel258.Location = new System.Drawing.Point(433, 672);
            this.panel258.Name = "panel258";
            this.panel258.Size = new System.Drawing.Size(40, 40);
            this.panel258.TabIndex = 188;
            // 
            // label258
            // 
            this.label258.AutoSize = true;
            this.label258.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label258.ForeColor = System.Drawing.Color.White;
            this.label258.Location = new System.Drawing.Point(479, 640);
            this.label258.Name = "label258";
            this.label258.Size = new System.Drawing.Size(318, 13);
            this.label258.TabIndex = 187;
            this.label258.Text = "[ Y1BD ] 2 Cassette 취출 이재기 파기 AIR ON";
            // 
            // panel259
            // 
            this.panel259.BackColor = System.Drawing.Color.Gray;
            this.panel259.Location = new System.Drawing.Point(433, 626);
            this.panel259.Name = "panel259";
            this.panel259.Size = new System.Drawing.Size(40, 40);
            this.panel259.TabIndex = 186;
            // 
            // label259
            // 
            this.label259.AutoSize = true;
            this.label259.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label259.ForeColor = System.Drawing.Color.White;
            this.label259.Location = new System.Drawing.Point(479, 594);
            this.label259.Name = "label259";
            this.label259.Size = new System.Drawing.Size(295, 13);
            this.label259.TabIndex = 185;
            this.label259.Text = "[ Y1BC ] 2 Cassette 취출 이재기 Vac OFF";
            // 
            // panel260
            // 
            this.panel260.BackColor = System.Drawing.Color.Gray;
            this.panel260.Location = new System.Drawing.Point(433, 580);
            this.panel260.Name = "panel260";
            this.panel260.Size = new System.Drawing.Size(40, 40);
            this.panel260.TabIndex = 184;
            // 
            // label260
            // 
            this.label260.AutoSize = true;
            this.label260.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label260.ForeColor = System.Drawing.Color.White;
            this.label260.Location = new System.Drawing.Point(479, 548);
            this.label260.Name = "label260";
            this.label260.Size = new System.Drawing.Size(289, 13);
            this.label260.TabIndex = 183;
            this.label260.Text = "[ Y1BB ] 2 Cassette 취출 이재기 Vac ON";
            // 
            // panel261
            // 
            this.panel261.BackColor = System.Drawing.Color.Gray;
            this.panel261.Location = new System.Drawing.Point(433, 534);
            this.panel261.Name = "panel261";
            this.panel261.Size = new System.Drawing.Size(40, 40);
            this.panel261.TabIndex = 182;
            // 
            // label261
            // 
            this.label261.AutoSize = true;
            this.label261.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label261.ForeColor = System.Drawing.Color.White;
            this.label261.Location = new System.Drawing.Point(479, 501);
            this.label261.Name = "label261";
            this.label261.Size = new System.Drawing.Size(317, 13);
            this.label261.TabIndex = 181;
            this.label261.Text = "[ Y1BA ] 1 Cassette 취출 이재기 파기 AIR ON";
            // 
            // panel262
            // 
            this.panel262.BackColor = System.Drawing.Color.Gray;
            this.panel262.Location = new System.Drawing.Point(433, 488);
            this.panel262.Name = "panel262";
            this.panel262.Size = new System.Drawing.Size(40, 40);
            this.panel262.TabIndex = 180;
            // 
            // label262
            // 
            this.label262.AutoSize = true;
            this.label262.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label262.ForeColor = System.Drawing.Color.White;
            this.label262.Location = new System.Drawing.Point(479, 454);
            this.label262.Name = "label262";
            this.label262.Size = new System.Drawing.Size(293, 13);
            this.label262.TabIndex = 179;
            this.label262.Text = "[ Y1B9 ] 1 Cassette 취출 이재기 Vac OFF";
            // 
            // panel263
            // 
            this.panel263.BackColor = System.Drawing.Color.Gray;
            this.panel263.Location = new System.Drawing.Point(433, 442);
            this.panel263.Name = "panel263";
            this.panel263.Size = new System.Drawing.Size(40, 40);
            this.panel263.TabIndex = 178;
            // 
            // label263
            // 
            this.label263.AutoSize = true;
            this.label263.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label263.ForeColor = System.Drawing.Color.White;
            this.label263.Location = new System.Drawing.Point(479, 409);
            this.label263.Name = "label263";
            this.label263.Size = new System.Drawing.Size(287, 13);
            this.label263.TabIndex = 177;
            this.label263.Text = "[ Y1B8 ] 1 Cassette 취출 이재기 Vac ON";
            // 
            // panel264
            // 
            this.panel264.BackColor = System.Drawing.Color.Gray;
            this.panel264.Location = new System.Drawing.Point(433, 396);
            this.panel264.Name = "panel264";
            this.panel264.Size = new System.Drawing.Size(40, 40);
            this.panel264.TabIndex = 176;
            // 
            // label264
            // 
            this.label264.AutoSize = true;
            this.label264.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label264.ForeColor = System.Drawing.Color.White;
            this.label264.Location = new System.Drawing.Point(479, 364);
            this.label264.Name = "label264";
            this.label264.Size = new System.Drawing.Size(323, 13);
            this.label264.TabIndex = 175;
            this.label264.Text = "[ Y1B7 ] 2 L/D Casset Lift Tilt Sensor DOWN";
            // 
            // panel265
            // 
            this.panel265.BackColor = System.Drawing.Color.Gray;
            this.panel265.Location = new System.Drawing.Point(433, 350);
            this.panel265.Name = "panel265";
            this.panel265.Size = new System.Drawing.Size(40, 40);
            this.panel265.TabIndex = 174;
            // 
            // label265
            // 
            this.label265.AutoSize = true;
            this.label265.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label265.ForeColor = System.Drawing.Color.White;
            this.label265.Location = new System.Drawing.Point(479, 319);
            this.label265.Name = "label265";
            this.label265.Size = new System.Drawing.Size(299, 13);
            this.label265.TabIndex = 173;
            this.label265.Text = "[ Y1B6 ] 2 L/D Casset Lift Tilt Sensor UP";
            // 
            // panel266
            // 
            this.panel266.BackColor = System.Drawing.Color.Gray;
            this.panel266.Location = new System.Drawing.Point(433, 304);
            this.panel266.Name = "panel266";
            this.panel266.Size = new System.Drawing.Size(40, 40);
            this.panel266.TabIndex = 172;
            // 
            // label266
            // 
            this.label266.AutoSize = true;
            this.label266.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label266.ForeColor = System.Drawing.Color.White;
            this.label266.Location = new System.Drawing.Point(479, 271);
            this.label266.Name = "label266";
            this.label266.Size = new System.Drawing.Size(276, 13);
            this.label266.TabIndex = 171;
            this.label266.Text = "[ Y1B5 ] 2 L/D 이재기 2ch 파기 AIR ON";
            // 
            // panel267
            // 
            this.panel267.BackColor = System.Drawing.Color.Gray;
            this.panel267.Location = new System.Drawing.Point(433, 258);
            this.panel267.Name = "panel267";
            this.panel267.Size = new System.Drawing.Size(40, 40);
            this.panel267.TabIndex = 170;
            // 
            // label267
            // 
            this.label267.AutoSize = true;
            this.label267.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label267.ForeColor = System.Drawing.Color.White;
            this.label267.Location = new System.Drawing.Point(479, 226);
            this.label267.Name = "label267";
            this.label267.Size = new System.Drawing.Size(253, 13);
            this.label267.TabIndex = 169;
            this.label267.Text = "[ Y1B4 ] 2 L/D 이재기 2ch Vac OFF";
            // 
            // panel268
            // 
            this.panel268.BackColor = System.Drawing.Color.Gray;
            this.panel268.Location = new System.Drawing.Point(433, 212);
            this.panel268.Name = "panel268";
            this.panel268.Size = new System.Drawing.Size(40, 40);
            this.panel268.TabIndex = 168;
            // 
            // label268
            // 
            this.label268.AutoSize = true;
            this.label268.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label268.ForeColor = System.Drawing.Color.White;
            this.label268.Location = new System.Drawing.Point(479, 179);
            this.label268.Name = "label268";
            this.label268.Size = new System.Drawing.Size(238, 13);
            this.label268.TabIndex = 167;
            this.label268.Text = "[ Y1B3 ] 2 L/D 이재기 2h Vac ON";
            // 
            // panel269
            // 
            this.panel269.BackColor = System.Drawing.Color.Gray;
            this.panel269.Location = new System.Drawing.Point(433, 166);
            this.panel269.Name = "panel269";
            this.panel269.Size = new System.Drawing.Size(40, 40);
            this.panel269.TabIndex = 166;
            // 
            // label269
            // 
            this.label269.AutoSize = true;
            this.label269.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label269.ForeColor = System.Drawing.Color.White;
            this.label269.Location = new System.Drawing.Point(479, 133);
            this.label269.Name = "label269";
            this.label269.Size = new System.Drawing.Size(276, 13);
            this.label269.TabIndex = 165;
            this.label269.Text = "[ Y1B2 ] 2 L/D 이재기 1ch 파기 AIR ON";
            // 
            // panel270
            // 
            this.panel270.BackColor = System.Drawing.Color.Gray;
            this.panel270.Location = new System.Drawing.Point(433, 120);
            this.panel270.Name = "panel270";
            this.panel270.Size = new System.Drawing.Size(40, 40);
            this.panel270.TabIndex = 164;
            // 
            // label270
            // 
            this.label270.AutoSize = true;
            this.label270.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label270.ForeColor = System.Drawing.Color.White;
            this.label270.Location = new System.Drawing.Point(479, 86);
            this.label270.Name = "label270";
            this.label270.Size = new System.Drawing.Size(253, 13);
            this.label270.TabIndex = 163;
            this.label270.Text = "[ Y1B1 ] 2 L/D 이재기 1ch Vac OFF";
            // 
            // panel271
            // 
            this.panel271.BackColor = System.Drawing.Color.Gray;
            this.panel271.Location = new System.Drawing.Point(433, 74);
            this.panel271.Name = "panel271";
            this.panel271.Size = new System.Drawing.Size(40, 40);
            this.panel271.TabIndex = 162;
            // 
            // label271
            // 
            this.label271.AutoSize = true;
            this.label271.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label271.ForeColor = System.Drawing.Color.White;
            this.label271.Location = new System.Drawing.Point(479, 42);
            this.label271.Name = "label271";
            this.label271.Size = new System.Drawing.Size(247, 13);
            this.label271.TabIndex = 161;
            this.label271.Text = "[ Y1B0 ] 2 L/D 이재기 1ch Vac ON";
            // 
            // panel272
            // 
            this.panel272.BackColor = System.Drawing.Color.Gray;
            this.panel272.Location = new System.Drawing.Point(433, 28);
            this.panel272.Name = "panel272";
            this.panel272.Size = new System.Drawing.Size(40, 40);
            this.panel272.TabIndex = 160;
            // 
            // label272
            // 
            this.label272.AutoSize = true;
            this.label272.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label272.ForeColor = System.Drawing.Color.White;
            this.label272.Location = new System.Drawing.Point(52, 730);
            this.label272.Name = "label272";
            this.label272.Size = new System.Drawing.Size(275, 13);
            this.label272.TabIndex = 159;
            this.label272.Text = "[ Y1AF ] 1 L/D 이재기 2ch 파기 AIR ON";
            // 
            // panel273
            // 
            this.panel273.BackColor = System.Drawing.Color.Gray;
            this.panel273.Location = new System.Drawing.Point(6, 718);
            this.panel273.Name = "panel273";
            this.panel273.Size = new System.Drawing.Size(40, 40);
            this.panel273.TabIndex = 158;
            // 
            // label273
            // 
            this.label273.AutoSize = true;
            this.label273.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label273.ForeColor = System.Drawing.Color.White;
            this.label273.Location = new System.Drawing.Point(52, 685);
            this.label273.Name = "label273";
            this.label273.Size = new System.Drawing.Size(253, 13);
            this.label273.TabIndex = 157;
            this.label273.Text = "[ Y1AE ] 1 L/D 이재기 2ch Vac OFF";
            // 
            // panel274
            // 
            this.panel274.BackColor = System.Drawing.Color.Gray;
            this.panel274.Location = new System.Drawing.Point(6, 672);
            this.panel274.Name = "panel274";
            this.panel274.Size = new System.Drawing.Size(40, 40);
            this.panel274.TabIndex = 156;
            // 
            // label274
            // 
            this.label274.AutoSize = true;
            this.label274.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label274.ForeColor = System.Drawing.Color.White;
            this.label274.Location = new System.Drawing.Point(52, 640);
            this.label274.Name = "label274";
            this.label274.Size = new System.Drawing.Size(248, 13);
            this.label274.TabIndex = 155;
            this.label274.Text = "[ Y1AD ] 1 L/D 이재기 2ch Vac ON";
            // 
            // panel275
            // 
            this.panel275.BackColor = System.Drawing.Color.Gray;
            this.panel275.Location = new System.Drawing.Point(6, 626);
            this.panel275.Name = "panel275";
            this.panel275.Size = new System.Drawing.Size(40, 40);
            this.panel275.TabIndex = 154;
            // 
            // label275
            // 
            this.label275.AutoSize = true;
            this.label275.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label275.ForeColor = System.Drawing.Color.White;
            this.label275.Location = new System.Drawing.Point(52, 594);
            this.label275.Name = "label275";
            this.label275.Size = new System.Drawing.Size(277, 13);
            this.label275.TabIndex = 153;
            this.label275.Text = "[ Y1AC ] 1 L/D 이재기 1ch 파기 AIR ON";
            // 
            // panel276
            // 
            this.panel276.BackColor = System.Drawing.Color.Gray;
            this.panel276.Location = new System.Drawing.Point(6, 580);
            this.panel276.Name = "panel276";
            this.panel276.Size = new System.Drawing.Size(40, 40);
            this.panel276.TabIndex = 152;
            // 
            // label276
            // 
            this.label276.AutoSize = true;
            this.label276.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label276.ForeColor = System.Drawing.Color.White;
            this.label276.Location = new System.Drawing.Point(52, 548);
            this.label276.Name = "label276";
            this.label276.Size = new System.Drawing.Size(254, 13);
            this.label276.TabIndex = 151;
            this.label276.Text = "[ Y1AB ] 1 L/D 이재기 1ch Vac OFF";
            // 
            // panel277
            // 
            this.panel277.BackColor = System.Drawing.Color.Gray;
            this.panel277.Location = new System.Drawing.Point(6, 534);
            this.panel277.Name = "panel277";
            this.panel277.Size = new System.Drawing.Size(40, 40);
            this.panel277.TabIndex = 150;
            // 
            // label277
            // 
            this.label277.AutoSize = true;
            this.label277.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label277.ForeColor = System.Drawing.Color.White;
            this.label277.Location = new System.Drawing.Point(52, 501);
            this.label277.Name = "label277";
            this.label277.Size = new System.Drawing.Size(247, 13);
            this.label277.TabIndex = 149;
            this.label277.Text = "[ Y1AA ] 1 L/D 이재기 1ch Vac ON";
            // 
            // panel278
            // 
            this.panel278.BackColor = System.Drawing.Color.Gray;
            this.panel278.Location = new System.Drawing.Point(6, 488);
            this.panel278.Name = "panel278";
            this.panel278.Size = new System.Drawing.Size(40, 40);
            this.panel278.TabIndex = 148;
            // 
            // label278
            // 
            this.label278.AutoSize = true;
            this.label278.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label278.ForeColor = System.Drawing.Color.White;
            this.label278.Location = new System.Drawing.Point(52, 454);
            this.label278.Name = "label278";
            this.label278.Size = new System.Drawing.Size(206, 13);
            this.label278.TabIndex = 147;
            this.label278.Text = "[ Y1A9 ] 2 L/D 이재기 DOWN";
            // 
            // panel279
            // 
            this.panel279.BackColor = System.Drawing.Color.Gray;
            this.panel279.Location = new System.Drawing.Point(6, 442);
            this.panel279.Name = "panel279";
            this.panel279.Size = new System.Drawing.Size(40, 40);
            this.panel279.TabIndex = 146;
            // 
            // label279
            // 
            this.label279.AutoSize = true;
            this.label279.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label279.ForeColor = System.Drawing.Color.White;
            this.label279.Location = new System.Drawing.Point(52, 409);
            this.label279.Name = "label279";
            this.label279.Size = new System.Drawing.Size(182, 13);
            this.label279.TabIndex = 145;
            this.label279.Text = "[ Y1A8 ] 2 L/D 이재기 UP";
            // 
            // panel280
            // 
            this.panel280.BackColor = System.Drawing.Color.Gray;
            this.panel280.Location = new System.Drawing.Point(6, 396);
            this.panel280.Name = "panel280";
            this.panel280.Size = new System.Drawing.Size(40, 40);
            this.panel280.TabIndex = 144;
            // 
            // label280
            // 
            this.label280.AutoSize = true;
            this.label280.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label280.ForeColor = System.Drawing.Color.White;
            this.label280.Location = new System.Drawing.Point(52, 364);
            this.label280.Name = "label280";
            this.label280.Size = new System.Drawing.Size(206, 13);
            this.label280.TabIndex = 143;
            this.label280.Text = "[ Y1A7 ] 1 L/D 이재기 DOWN";
            // 
            // panel281
            // 
            this.panel281.BackColor = System.Drawing.Color.Gray;
            this.panel281.Location = new System.Drawing.Point(6, 350);
            this.panel281.Name = "panel281";
            this.panel281.Size = new System.Drawing.Size(40, 40);
            this.panel281.TabIndex = 142;
            // 
            // label281
            // 
            this.label281.AutoSize = true;
            this.label281.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label281.ForeColor = System.Drawing.Color.White;
            this.label281.Location = new System.Drawing.Point(52, 319);
            this.label281.Name = "label281";
            this.label281.Size = new System.Drawing.Size(182, 13);
            this.label281.TabIndex = 141;
            this.label281.Text = "[ Y1A6 ] 1 L/D 이재기 UP";
            // 
            // panel282
            // 
            this.panel282.BackColor = System.Drawing.Color.Gray;
            this.panel282.Location = new System.Drawing.Point(6, 304);
            this.panel282.Name = "panel282";
            this.panel282.Size = new System.Drawing.Size(40, 40);
            this.panel282.TabIndex = 140;
            // 
            // label282
            // 
            this.label282.AutoSize = true;
            this.label282.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label282.ForeColor = System.Drawing.Color.White;
            this.label282.Location = new System.Drawing.Point(52, 271);
            this.label282.Name = "label282";
            this.label282.Size = new System.Drawing.Size(322, 13);
            this.label282.TabIndex = 139;
            this.label282.Text = "[ Y1A5 ] 1 L/D Casset Lift Tilt Sensor DOWN";
            // 
            // panel283
            // 
            this.panel283.BackColor = System.Drawing.Color.Gray;
            this.panel283.Location = new System.Drawing.Point(6, 258);
            this.panel283.Name = "panel283";
            this.panel283.Size = new System.Drawing.Size(40, 40);
            this.panel283.TabIndex = 138;
            // 
            // label283
            // 
            this.label283.AutoSize = true;
            this.label283.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label283.ForeColor = System.Drawing.Color.White;
            this.label283.Location = new System.Drawing.Point(52, 226);
            this.label283.Name = "label283";
            this.label283.Size = new System.Drawing.Size(298, 13);
            this.label283.TabIndex = 137;
            this.label283.Text = "[ Y1A4 ] 1 L/D Casset Lift Tilt Sensor UP";
            // 
            // panel284
            // 
            this.panel284.BackColor = System.Drawing.Color.Gray;
            this.panel284.Location = new System.Drawing.Point(6, 212);
            this.panel284.Name = "panel284";
            this.panel284.Size = new System.Drawing.Size(40, 40);
            this.panel284.TabIndex = 136;
            // 
            // label284
            // 
            this.label284.AutoSize = true;
            this.label284.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label284.ForeColor = System.Drawing.Color.White;
            this.label284.Location = new System.Drawing.Point(52, 179);
            this.label284.Name = "label284";
            this.label284.Size = new System.Drawing.Size(272, 13);
            this.label284.TabIndex = 135;
            this.label284.Text = "[ Y1A3 ] 2 L/D Cassette Lifter UnGrip";
            // 
            // panel285
            // 
            this.panel285.BackColor = System.Drawing.Color.Gray;
            this.panel285.Location = new System.Drawing.Point(6, 166);
            this.panel285.Name = "panel285";
            this.panel285.Size = new System.Drawing.Size(40, 40);
            this.panel285.TabIndex = 134;
            // 
            // label285
            // 
            this.label285.AutoSize = true;
            this.label285.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label285.ForeColor = System.Drawing.Color.White;
            this.label285.Location = new System.Drawing.Point(52, 133);
            this.label285.Name = "label285";
            this.label285.Size = new System.Drawing.Size(234, 13);
            this.label285.TabIndex = 133;
            this.label285.Text = "[ Y322 ] Laser Emergency Stop";
            // 
            // panel286
            // 
            this.panel286.BackColor = System.Drawing.Color.Gray;
            this.panel286.Location = new System.Drawing.Point(6, 120);
            this.panel286.Name = "panel286";
            this.panel286.Size = new System.Drawing.Size(40, 40);
            this.panel286.TabIndex = 132;
            // 
            // label286
            // 
            this.label286.AutoSize = true;
            this.label286.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label286.ForeColor = System.Drawing.Color.White;
            this.label286.Location = new System.Drawing.Point(52, 86);
            this.label286.Name = "label286";
            this.label286.Size = new System.Drawing.Size(288, 13);
            this.label286.TabIndex = 131;
            this.label286.Text = "[ Y321 ] Laser Stop(External_Interlock)";
            // 
            // panel287
            // 
            this.panel287.BackColor = System.Drawing.Color.Gray;
            this.panel287.Location = new System.Drawing.Point(6, 74);
            this.panel287.Name = "panel287";
            this.panel287.Size = new System.Drawing.Size(40, 40);
            this.panel287.TabIndex = 130;
            // 
            // label287
            // 
            this.label287.AutoSize = true;
            this.label287.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label287.ForeColor = System.Drawing.Color.White;
            this.label287.Location = new System.Drawing.Point(52, 42);
            this.label287.Name = "label287";
            this.label287.Size = new System.Drawing.Size(284, 13);
            this.label287.TabIndex = 129;
            this.label287.Text = "[ Y320 ] Laser Stop(Internal_Interlock)";
            // 
            // panel288
            // 
            this.panel288.BackColor = System.Drawing.Color.Gray;
            this.panel288.Location = new System.Drawing.Point(6, 28);
            this.panel288.Name = "panel288";
            this.panel288.Size = new System.Drawing.Size(40, 40);
            this.panel288.TabIndex = 128;
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.Color.DimGray;
            this.tabPage5.Controls.Add(this.label288);
            this.tabPage5.Controls.Add(this.panel289);
            this.tabPage5.Controls.Add(this.label289);
            this.tabPage5.Controls.Add(this.panel290);
            this.tabPage5.Controls.Add(this.label290);
            this.tabPage5.Controls.Add(this.panel291);
            this.tabPage5.Controls.Add(this.label291);
            this.tabPage5.Controls.Add(this.panel292);
            this.tabPage5.Controls.Add(this.label292);
            this.tabPage5.Controls.Add(this.panel293);
            this.tabPage5.Controls.Add(this.label293);
            this.tabPage5.Controls.Add(this.panel294);
            this.tabPage5.Controls.Add(this.label294);
            this.tabPage5.Controls.Add(this.panel295);
            this.tabPage5.Controls.Add(this.label295);
            this.tabPage5.Controls.Add(this.panel296);
            this.tabPage5.Controls.Add(this.label296);
            this.tabPage5.Controls.Add(this.panel297);
            this.tabPage5.Controls.Add(this.label297);
            this.tabPage5.Controls.Add(this.panel298);
            this.tabPage5.Controls.Add(this.label298);
            this.tabPage5.Controls.Add(this.panel299);
            this.tabPage5.Controls.Add(this.label299);
            this.tabPage5.Controls.Add(this.panel300);
            this.tabPage5.Controls.Add(this.label300);
            this.tabPage5.Controls.Add(this.panel301);
            this.tabPage5.Controls.Add(this.label301);
            this.tabPage5.Controls.Add(this.panel302);
            this.tabPage5.Controls.Add(this.label302);
            this.tabPage5.Controls.Add(this.panel303);
            this.tabPage5.Controls.Add(this.label303);
            this.tabPage5.Controls.Add(this.panel304);
            this.tabPage5.Controls.Add(this.label304);
            this.tabPage5.Controls.Add(this.panel305);
            this.tabPage5.Controls.Add(this.label305);
            this.tabPage5.Controls.Add(this.panel306);
            this.tabPage5.Controls.Add(this.label306);
            this.tabPage5.Controls.Add(this.panel307);
            this.tabPage5.Controls.Add(this.label307);
            this.tabPage5.Controls.Add(this.panel308);
            this.tabPage5.Controls.Add(this.label308);
            this.tabPage5.Controls.Add(this.panel309);
            this.tabPage5.Controls.Add(this.label309);
            this.tabPage5.Controls.Add(this.panel310);
            this.tabPage5.Controls.Add(this.label310);
            this.tabPage5.Controls.Add(this.panel311);
            this.tabPage5.Controls.Add(this.label311);
            this.tabPage5.Controls.Add(this.panel312);
            this.tabPage5.Controls.Add(this.label312);
            this.tabPage5.Controls.Add(this.panel313);
            this.tabPage5.Controls.Add(this.label313);
            this.tabPage5.Controls.Add(this.panel314);
            this.tabPage5.Controls.Add(this.label314);
            this.tabPage5.Controls.Add(this.panel315);
            this.tabPage5.Controls.Add(this.label315);
            this.tabPage5.Controls.Add(this.panel316);
            this.tabPage5.Controls.Add(this.label316);
            this.tabPage5.Controls.Add(this.panel317);
            this.tabPage5.Controls.Add(this.label317);
            this.tabPage5.Controls.Add(this.panel318);
            this.tabPage5.Controls.Add(this.label318);
            this.tabPage5.Controls.Add(this.panel319);
            this.tabPage5.Controls.Add(this.label319);
            this.tabPage5.Controls.Add(this.panel320);
            this.tabPage5.Location = new System.Drawing.Point(4, 34);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(846, 778);
            this.tabPage5.TabIndex = 2;
            this.tabPage5.Text = "OUT - 2";
            // 
            // label288
            // 
            this.label288.AutoSize = true;
            this.label288.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label288.ForeColor = System.Drawing.Color.White;
            this.label288.Location = new System.Drawing.Point(479, 730);
            this.label288.Name = "label288";
            this.label288.Size = new System.Drawing.Size(116, 13);
            this.label288.TabIndex = 255;
            this.label288.Text = "[ Y1DF ] Spare";
            // 
            // panel289
            // 
            this.panel289.BackColor = System.Drawing.Color.Gray;
            this.panel289.Location = new System.Drawing.Point(433, 718);
            this.panel289.Name = "panel289";
            this.panel289.Size = new System.Drawing.Size(40, 40);
            this.panel289.TabIndex = 254;
            // 
            // label289
            // 
            this.label289.AutoSize = true;
            this.label289.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label289.ForeColor = System.Drawing.Color.White;
            this.label289.Location = new System.Drawing.Point(479, 685);
            this.label289.Name = "label289";
            this.label289.Size = new System.Drawing.Size(117, 13);
            this.label289.TabIndex = 253;
            this.label289.Text = "[ Y1DE ] Spare";
            // 
            // panel290
            // 
            this.panel290.BackColor = System.Drawing.Color.Gray;
            this.panel290.Location = new System.Drawing.Point(433, 672);
            this.panel290.Name = "panel290";
            this.panel290.Size = new System.Drawing.Size(40, 40);
            this.panel290.TabIndex = 252;
            // 
            // label290
            // 
            this.label290.AutoSize = true;
            this.label290.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label290.ForeColor = System.Drawing.Color.White;
            this.label290.Location = new System.Drawing.Point(479, 640);
            this.label290.Name = "label290";
            this.label290.Size = new System.Drawing.Size(118, 13);
            this.label290.TabIndex = 251;
            this.label290.Text = "[ Y1DD ] Spare";
            // 
            // panel291
            // 
            this.panel291.BackColor = System.Drawing.Color.Gray;
            this.panel291.Location = new System.Drawing.Point(433, 626);
            this.panel291.Name = "panel291";
            this.panel291.Size = new System.Drawing.Size(40, 40);
            this.panel291.TabIndex = 250;
            // 
            // label291
            // 
            this.label291.AutoSize = true;
            this.label291.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label291.ForeColor = System.Drawing.Color.White;
            this.label291.Location = new System.Drawing.Point(479, 594);
            this.label291.Name = "label291";
            this.label291.Size = new System.Drawing.Size(118, 13);
            this.label291.TabIndex = 249;
            this.label291.Text = "[ Y1DC ] Spare";
            // 
            // panel292
            // 
            this.panel292.BackColor = System.Drawing.Color.Gray;
            this.panel292.Location = new System.Drawing.Point(433, 580);
            this.panel292.Name = "panel292";
            this.panel292.Size = new System.Drawing.Size(40, 40);
            this.panel292.TabIndex = 248;
            // 
            // label292
            // 
            this.label292.AutoSize = true;
            this.label292.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label292.ForeColor = System.Drawing.Color.White;
            this.label292.Location = new System.Drawing.Point(479, 548);
            this.label292.Name = "label292";
            this.label292.Size = new System.Drawing.Size(118, 13);
            this.label292.TabIndex = 247;
            this.label292.Text = "[ Y1DB ] Spare";
            // 
            // panel293
            // 
            this.panel293.BackColor = System.Drawing.Color.Gray;
            this.panel293.Location = new System.Drawing.Point(433, 534);
            this.panel293.Name = "panel293";
            this.panel293.Size = new System.Drawing.Size(40, 40);
            this.panel293.TabIndex = 246;
            // 
            // label293
            // 
            this.label293.AutoSize = true;
            this.label293.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label293.ForeColor = System.Drawing.Color.White;
            this.label293.Location = new System.Drawing.Point(479, 501);
            this.label293.Name = "label293";
            this.label293.Size = new System.Drawing.Size(117, 13);
            this.label293.TabIndex = 245;
            this.label293.Text = "[ Y1DA ] Spare";
            // 
            // panel294
            // 
            this.panel294.BackColor = System.Drawing.Color.Gray;
            this.panel294.Location = new System.Drawing.Point(433, 488);
            this.panel294.Name = "panel294";
            this.panel294.Size = new System.Drawing.Size(40, 40);
            this.panel294.TabIndex = 244;
            // 
            // label294
            // 
            this.label294.AutoSize = true;
            this.label294.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label294.ForeColor = System.Drawing.Color.White;
            this.label294.Location = new System.Drawing.Point(479, 454);
            this.label294.Name = "label294";
            this.label294.Size = new System.Drawing.Size(162, 13);
            this.label294.TabIndex = 243;
            this.label294.Text = "[ Y1D9 ] 가공부 전광판";
            // 
            // panel295
            // 
            this.panel295.BackColor = System.Drawing.Color.Gray;
            this.panel295.Location = new System.Drawing.Point(433, 442);
            this.panel295.Name = "panel295";
            this.panel295.Size = new System.Drawing.Size(40, 40);
            this.panel295.TabIndex = 242;
            // 
            // label295
            // 
            this.label295.AutoSize = true;
            this.label295.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label295.ForeColor = System.Drawing.Color.White;
            this.label295.Location = new System.Drawing.Point(479, 409);
            this.label295.Name = "label295";
            this.label295.Size = new System.Drawing.Size(116, 13);
            this.label295.TabIndex = 241;
            this.label295.Text = "[ Y1D8 ] Spare";
            // 
            // panel296
            // 
            this.panel296.BackColor = System.Drawing.Color.Gray;
            this.panel296.Location = new System.Drawing.Point(433, 396);
            this.panel296.Name = "panel296";
            this.panel296.Size = new System.Drawing.Size(40, 40);
            this.panel296.TabIndex = 240;
            // 
            // label296
            // 
            this.label296.AutoSize = true;
            this.label296.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label296.ForeColor = System.Drawing.Color.White;
            this.label296.Location = new System.Drawing.Point(479, 364);
            this.label296.Name = "label296";
            this.label296.Size = new System.Drawing.Size(116, 13);
            this.label296.TabIndex = 239;
            this.label296.Text = "[ Y1D7 ] Spare";
            // 
            // panel297
            // 
            this.panel297.BackColor = System.Drawing.Color.Gray;
            this.panel297.Location = new System.Drawing.Point(433, 350);
            this.panel297.Name = "panel297";
            this.panel297.Size = new System.Drawing.Size(40, 40);
            this.panel297.TabIndex = 238;
            // 
            // label297
            // 
            this.label297.AutoSize = true;
            this.label297.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label297.ForeColor = System.Drawing.Color.White;
            this.label297.Location = new System.Drawing.Point(479, 319);
            this.label297.Name = "label297";
            this.label297.Size = new System.Drawing.Size(116, 13);
            this.label297.TabIndex = 237;
            this.label297.Text = "[ Y1D6 ] Spare";
            // 
            // panel298
            // 
            this.panel298.BackColor = System.Drawing.Color.Gray;
            this.panel298.Location = new System.Drawing.Point(433, 304);
            this.panel298.Name = "panel298";
            this.panel298.Size = new System.Drawing.Size(40, 40);
            this.panel298.TabIndex = 236;
            // 
            // label298
            // 
            this.label298.AutoSize = true;
            this.label298.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label298.ForeColor = System.Drawing.Color.White;
            this.label298.Location = new System.Drawing.Point(479, 271);
            this.label298.Name = "label298";
            this.label298.Size = new System.Drawing.Size(116, 13);
            this.label298.TabIndex = 235;
            this.label298.Text = "[ Y1D5 ] Spare";
            // 
            // panel299
            // 
            this.panel299.BackColor = System.Drawing.Color.Gray;
            this.panel299.Location = new System.Drawing.Point(433, 258);
            this.panel299.Name = "panel299";
            this.panel299.Size = new System.Drawing.Size(40, 40);
            this.panel299.TabIndex = 234;
            // 
            // label299
            // 
            this.label299.AutoSize = true;
            this.label299.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label299.ForeColor = System.Drawing.Color.White;
            this.label299.Location = new System.Drawing.Point(479, 226);
            this.label299.Name = "label299";
            this.label299.Size = new System.Drawing.Size(116, 13);
            this.label299.TabIndex = 233;
            this.label299.Text = "[ Y1D4 ] Spare";
            // 
            // panel300
            // 
            this.panel300.BackColor = System.Drawing.Color.Gray;
            this.panel300.Location = new System.Drawing.Point(433, 212);
            this.panel300.Name = "panel300";
            this.panel300.Size = new System.Drawing.Size(40, 40);
            this.panel300.TabIndex = 232;
            // 
            // label300
            // 
            this.label300.AutoSize = true;
            this.label300.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label300.ForeColor = System.Drawing.Color.White;
            this.label300.Location = new System.Drawing.Point(479, 179);
            this.label300.Name = "label300";
            this.label300.Size = new System.Drawing.Size(116, 13);
            this.label300.TabIndex = 231;
            this.label300.Text = "[ Y1D3 ] Spare";
            // 
            // panel301
            // 
            this.panel301.BackColor = System.Drawing.Color.Gray;
            this.panel301.Location = new System.Drawing.Point(433, 166);
            this.panel301.Name = "panel301";
            this.panel301.Size = new System.Drawing.Size(40, 40);
            this.panel301.TabIndex = 230;
            // 
            // label301
            // 
            this.label301.AutoSize = true;
            this.label301.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label301.ForeColor = System.Drawing.Color.White;
            this.label301.Location = new System.Drawing.Point(479, 133);
            this.label301.Name = "label301";
            this.label301.Size = new System.Drawing.Size(254, 13);
            this.label301.TabIndex = 229;
            this.label301.Text = "[ Y1D2 ] 설비 내부 형광등 3 ON/OFF";
            // 
            // panel302
            // 
            this.panel302.BackColor = System.Drawing.Color.Gray;
            this.panel302.Location = new System.Drawing.Point(433, 120);
            this.panel302.Name = "panel302";
            this.panel302.Size = new System.Drawing.Size(40, 40);
            this.panel302.TabIndex = 228;
            // 
            // label302
            // 
            this.label302.AutoSize = true;
            this.label302.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label302.ForeColor = System.Drawing.Color.White;
            this.label302.Location = new System.Drawing.Point(479, 86);
            this.label302.Name = "label302";
            this.label302.Size = new System.Drawing.Size(254, 13);
            this.label302.TabIndex = 227;
            this.label302.Text = "[ Y1D1 ] 설비 내부 형광등 2 ON/OFF";
            // 
            // panel303
            // 
            this.panel303.BackColor = System.Drawing.Color.Gray;
            this.panel303.Location = new System.Drawing.Point(433, 74);
            this.panel303.Name = "panel303";
            this.panel303.Size = new System.Drawing.Size(40, 40);
            this.panel303.TabIndex = 226;
            // 
            // label303
            // 
            this.label303.AutoSize = true;
            this.label303.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label303.ForeColor = System.Drawing.Color.White;
            this.label303.Location = new System.Drawing.Point(479, 42);
            this.label303.Name = "label303";
            this.label303.Size = new System.Drawing.Size(254, 13);
            this.label303.TabIndex = 225;
            this.label303.Text = "[ Y1D0 ] 설비 내부 형광등 1 ON/OFF";
            // 
            // panel304
            // 
            this.panel304.BackColor = System.Drawing.Color.Gray;
            this.panel304.Location = new System.Drawing.Point(433, 28);
            this.panel304.Name = "panel304";
            this.panel304.Size = new System.Drawing.Size(40, 40);
            this.panel304.TabIndex = 224;
            // 
            // label304
            // 
            this.label304.AutoSize = true;
            this.label304.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label304.ForeColor = System.Drawing.Color.White;
            this.label304.Location = new System.Drawing.Point(52, 730);
            this.label304.Name = "label304";
            this.label304.Size = new System.Drawing.Size(240, 13);
            this.label304.TabIndex = 223;
            this.label304.Text = "[ Y1CF ] Spare 파기 AIR ON CMD";
            // 
            // panel305
            // 
            this.panel305.BackColor = System.Drawing.Color.Gray;
            this.panel305.Location = new System.Drawing.Point(6, 718);
            this.panel305.Name = "panel305";
            this.panel305.Size = new System.Drawing.Size(40, 40);
            this.panel305.TabIndex = 222;
            // 
            // label305
            // 
            this.label305.AutoSize = true;
            this.label305.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label305.ForeColor = System.Drawing.Color.White;
            this.label305.Location = new System.Drawing.Point(52, 685);
            this.label305.Name = "label305";
            this.label305.Size = new System.Drawing.Size(241, 13);
            this.label305.TabIndex = 221;
            this.label305.Text = "[ Y1CE ] Spare 파기 AIR ON CMD";
            // 
            // panel306
            // 
            this.panel306.BackColor = System.Drawing.Color.Gray;
            this.panel306.Location = new System.Drawing.Point(6, 672);
            this.panel306.Name = "panel306";
            this.panel306.Size = new System.Drawing.Size(40, 40);
            this.panel306.TabIndex = 220;
            // 
            // label306
            // 
            this.label306.AutoSize = true;
            this.label306.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label306.ForeColor = System.Drawing.Color.White;
            this.label306.Location = new System.Drawing.Point(52, 640);
            this.label306.Name = "label306";
            this.label306.Size = new System.Drawing.Size(245, 13);
            this.label306.TabIndex = 219;
            this.label306.Text = "[ Y1CD ] Spare SOL V/V ON CMD";
            // 
            // panel307
            // 
            this.panel307.BackColor = System.Drawing.Color.Gray;
            this.panel307.Location = new System.Drawing.Point(6, 626);
            this.panel307.Name = "panel307";
            this.panel307.Size = new System.Drawing.Size(40, 40);
            this.panel307.TabIndex = 218;
            // 
            // label307
            // 
            this.label307.AutoSize = true;
            this.label307.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label307.ForeColor = System.Drawing.Color.White;
            this.label307.Location = new System.Drawing.Point(52, 594);
            this.label307.Name = "label307";
            this.label307.Size = new System.Drawing.Size(245, 13);
            this.label307.TabIndex = 217;
            this.label307.Text = "[ Y1CC ] Spare SOL V/V ON CMD";
            // 
            // panel308
            // 
            this.panel308.BackColor = System.Drawing.Color.Gray;
            this.panel308.Location = new System.Drawing.Point(6, 580);
            this.panel308.Name = "panel308";
            this.panel308.Size = new System.Drawing.Size(40, 40);
            this.panel308.TabIndex = 216;
            // 
            // label308
            // 
            this.label308.AutoSize = true;
            this.label308.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label308.ForeColor = System.Drawing.Color.White;
            this.label308.Location = new System.Drawing.Point(52, 548);
            this.label308.Name = "label308";
            this.label308.Size = new System.Drawing.Size(245, 13);
            this.label308.TabIndex = 215;
            this.label308.Text = "[ Y1CB ] Spare SOL V/V ON CMD";
            // 
            // panel309
            // 
            this.panel309.BackColor = System.Drawing.Color.Gray;
            this.panel309.Location = new System.Drawing.Point(6, 534);
            this.panel309.Name = "panel309";
            this.panel309.Size = new System.Drawing.Size(40, 40);
            this.panel309.TabIndex = 214;
            // 
            // label309
            // 
            this.label309.AutoSize = true;
            this.label309.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label309.ForeColor = System.Drawing.Color.White;
            this.label309.Location = new System.Drawing.Point(52, 501);
            this.label309.Name = "label309";
            this.label309.Size = new System.Drawing.Size(244, 13);
            this.label309.TabIndex = 213;
            this.label309.Text = "[ Y1CA ] Spare SOL V/V ON CMD";
            // 
            // panel310
            // 
            this.panel310.BackColor = System.Drawing.Color.Gray;
            this.panel310.Location = new System.Drawing.Point(6, 488);
            this.panel310.Name = "panel310";
            this.panel310.Size = new System.Drawing.Size(40, 40);
            this.panel310.TabIndex = 212;
            // 
            // label310
            // 
            this.label310.AutoSize = true;
            this.label310.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label310.ForeColor = System.Drawing.Color.White;
            this.label310.Location = new System.Drawing.Point(52, 454);
            this.label310.Name = "label310";
            this.label310.Size = new System.Drawing.Size(116, 13);
            this.label310.TabIndex = 211;
            this.label310.Text = "[ Y1C9 ] Spare";
            // 
            // panel311
            // 
            this.panel311.BackColor = System.Drawing.Color.Gray;
            this.panel311.Location = new System.Drawing.Point(6, 442);
            this.panel311.Name = "panel311";
            this.panel311.Size = new System.Drawing.Size(40, 40);
            this.panel311.TabIndex = 210;
            // 
            // label311
            // 
            this.label311.AutoSize = true;
            this.label311.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label311.ForeColor = System.Drawing.Color.White;
            this.label311.Location = new System.Drawing.Point(52, 409);
            this.label311.Name = "label311";
            this.label311.Size = new System.Drawing.Size(116, 13);
            this.label311.TabIndex = 209;
            this.label311.Text = "[ Y1C8 ] Spare";
            // 
            // panel312
            // 
            this.panel312.BackColor = System.Drawing.Color.Gray;
            this.panel312.Location = new System.Drawing.Point(6, 396);
            this.panel312.Name = "panel312";
            this.panel312.Size = new System.Drawing.Size(40, 40);
            this.panel312.TabIndex = 208;
            // 
            // label312
            // 
            this.label312.AutoSize = true;
            this.label312.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label312.ForeColor = System.Drawing.Color.White;
            this.label312.Location = new System.Drawing.Point(52, 364);
            this.label312.Name = "label312";
            this.label312.Size = new System.Drawing.Size(116, 13);
            this.label312.TabIndex = 207;
            this.label312.Text = "[ Y1C7 ] Spare";
            // 
            // panel313
            // 
            this.panel313.BackColor = System.Drawing.Color.Gray;
            this.panel313.Location = new System.Drawing.Point(6, 350);
            this.panel313.Name = "panel313";
            this.panel313.Size = new System.Drawing.Size(40, 40);
            this.panel313.TabIndex = 206;
            // 
            // label313
            // 
            this.label313.AutoSize = true;
            this.label313.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label313.ForeColor = System.Drawing.Color.White;
            this.label313.Location = new System.Drawing.Point(52, 319);
            this.label313.Name = "label313";
            this.label313.Size = new System.Drawing.Size(240, 13);
            this.label313.TabIndex = 205;
            this.label313.Text = "[ Y1C6 ] 7 Safety Door SW Open";
            // 
            // panel314
            // 
            this.panel314.BackColor = System.Drawing.Color.Gray;
            this.panel314.Location = new System.Drawing.Point(6, 304);
            this.panel314.Name = "panel314";
            this.panel314.Size = new System.Drawing.Size(40, 40);
            this.panel314.TabIndex = 204;
            // 
            // label314
            // 
            this.label314.AutoSize = true;
            this.label314.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label314.ForeColor = System.Drawing.Color.White;
            this.label314.Location = new System.Drawing.Point(52, 271);
            this.label314.Name = "label314";
            this.label314.Size = new System.Drawing.Size(240, 13);
            this.label314.TabIndex = 203;
            this.label314.Text = "[ Y1C5 ] 6 Safety Door SW Open";
            // 
            // panel315
            // 
            this.panel315.BackColor = System.Drawing.Color.Gray;
            this.panel315.Location = new System.Drawing.Point(6, 258);
            this.panel315.Name = "panel315";
            this.panel315.Size = new System.Drawing.Size(40, 40);
            this.panel315.TabIndex = 202;
            // 
            // label315
            // 
            this.label315.AutoSize = true;
            this.label315.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label315.ForeColor = System.Drawing.Color.White;
            this.label315.Location = new System.Drawing.Point(52, 226);
            this.label315.Name = "label315";
            this.label315.Size = new System.Drawing.Size(240, 13);
            this.label315.TabIndex = 201;
            this.label315.Text = "[ Y1C4 ] 5 Safety Door SW Open";
            // 
            // panel316
            // 
            this.panel316.BackColor = System.Drawing.Color.Gray;
            this.panel316.Location = new System.Drawing.Point(6, 212);
            this.panel316.Name = "panel316";
            this.panel316.Size = new System.Drawing.Size(40, 40);
            this.panel316.TabIndex = 200;
            // 
            // label316
            // 
            this.label316.AutoSize = true;
            this.label316.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label316.ForeColor = System.Drawing.Color.White;
            this.label316.Location = new System.Drawing.Point(52, 179);
            this.label316.Name = "label316";
            this.label316.Size = new System.Drawing.Size(240, 13);
            this.label316.TabIndex = 199;
            this.label316.Text = "[ Y1C3 ] 4 Safety Door SW Open";
            // 
            // panel317
            // 
            this.panel317.BackColor = System.Drawing.Color.Gray;
            this.panel317.Location = new System.Drawing.Point(6, 166);
            this.panel317.Name = "panel317";
            this.panel317.Size = new System.Drawing.Size(40, 40);
            this.panel317.TabIndex = 198;
            // 
            // label317
            // 
            this.label317.AutoSize = true;
            this.label317.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label317.ForeColor = System.Drawing.Color.White;
            this.label317.Location = new System.Drawing.Point(52, 133);
            this.label317.Name = "label317";
            this.label317.Size = new System.Drawing.Size(240, 13);
            this.label317.TabIndex = 197;
            this.label317.Text = "[ Y1C2 ] 3 Safety Door SW Open";
            // 
            // panel318
            // 
            this.panel318.BackColor = System.Drawing.Color.Gray;
            this.panel318.Location = new System.Drawing.Point(6, 120);
            this.panel318.Name = "panel318";
            this.panel318.Size = new System.Drawing.Size(40, 40);
            this.panel318.TabIndex = 196;
            // 
            // label318
            // 
            this.label318.AutoSize = true;
            this.label318.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label318.ForeColor = System.Drawing.Color.White;
            this.label318.Location = new System.Drawing.Point(52, 86);
            this.label318.Name = "label318";
            this.label318.Size = new System.Drawing.Size(240, 13);
            this.label318.TabIndex = 195;
            this.label318.Text = "[ Y1C1 ] 2 Safety Door SW Open";
            // 
            // panel319
            // 
            this.panel319.BackColor = System.Drawing.Color.Gray;
            this.panel319.Location = new System.Drawing.Point(6, 74);
            this.panel319.Name = "panel319";
            this.panel319.Size = new System.Drawing.Size(40, 40);
            this.panel319.TabIndex = 194;
            // 
            // label319
            // 
            this.label319.AutoSize = true;
            this.label319.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label319.ForeColor = System.Drawing.Color.White;
            this.label319.Location = new System.Drawing.Point(52, 42);
            this.label319.Name = "label319";
            this.label319.Size = new System.Drawing.Size(240, 13);
            this.label319.TabIndex = 193;
            this.label319.Text = "[ Y1C0 ] 1 Safety Door SW Open";
            // 
            // panel320
            // 
            this.panel320.BackColor = System.Drawing.Color.Gray;
            this.panel320.Location = new System.Drawing.Point(6, 28);
            this.panel320.Name = "panel320";
            this.panel320.Size = new System.Drawing.Size(40, 40);
            this.panel320.TabIndex = 192;
            // 
            // tabPage6
            // 
            this.tabPage6.BackColor = System.Drawing.Color.DimGray;
            this.tabPage6.Controls.Add(this.label320);
            this.tabPage6.Controls.Add(this.panel321);
            this.tabPage6.Controls.Add(this.label321);
            this.tabPage6.Controls.Add(this.panel322);
            this.tabPage6.Controls.Add(this.label322);
            this.tabPage6.Controls.Add(this.panel323);
            this.tabPage6.Controls.Add(this.label323);
            this.tabPage6.Controls.Add(this.panel324);
            this.tabPage6.Controls.Add(this.label324);
            this.tabPage6.Controls.Add(this.panel325);
            this.tabPage6.Controls.Add(this.label325);
            this.tabPage6.Controls.Add(this.panel326);
            this.tabPage6.Controls.Add(this.label326);
            this.tabPage6.Controls.Add(this.panel327);
            this.tabPage6.Controls.Add(this.label327);
            this.tabPage6.Controls.Add(this.panel328);
            this.tabPage6.Controls.Add(this.label328);
            this.tabPage6.Controls.Add(this.panel329);
            this.tabPage6.Controls.Add(this.label329);
            this.tabPage6.Controls.Add(this.panel330);
            this.tabPage6.Controls.Add(this.label330);
            this.tabPage6.Controls.Add(this.panel331);
            this.tabPage6.Controls.Add(this.label331);
            this.tabPage6.Controls.Add(this.panel332);
            this.tabPage6.Controls.Add(this.label332);
            this.tabPage6.Controls.Add(this.panel333);
            this.tabPage6.Controls.Add(this.label333);
            this.tabPage6.Controls.Add(this.panel334);
            this.tabPage6.Controls.Add(this.label334);
            this.tabPage6.Controls.Add(this.panel335);
            this.tabPage6.Controls.Add(this.label335);
            this.tabPage6.Controls.Add(this.panel336);
            this.tabPage6.Controls.Add(this.label336);
            this.tabPage6.Controls.Add(this.panel337);
            this.tabPage6.Controls.Add(this.label337);
            this.tabPage6.Controls.Add(this.panel338);
            this.tabPage6.Controls.Add(this.label338);
            this.tabPage6.Controls.Add(this.panel339);
            this.tabPage6.Controls.Add(this.label339);
            this.tabPage6.Controls.Add(this.panel340);
            this.tabPage6.Controls.Add(this.label340);
            this.tabPage6.Controls.Add(this.panel341);
            this.tabPage6.Controls.Add(this.label341);
            this.tabPage6.Controls.Add(this.panel342);
            this.tabPage6.Controls.Add(this.label342);
            this.tabPage6.Controls.Add(this.panel343);
            this.tabPage6.Controls.Add(this.label343);
            this.tabPage6.Controls.Add(this.panel344);
            this.tabPage6.Controls.Add(this.label344);
            this.tabPage6.Controls.Add(this.panel345);
            this.tabPage6.Controls.Add(this.label345);
            this.tabPage6.Controls.Add(this.panel346);
            this.tabPage6.Controls.Add(this.label346);
            this.tabPage6.Controls.Add(this.panel347);
            this.tabPage6.Controls.Add(this.label347);
            this.tabPage6.Controls.Add(this.panel348);
            this.tabPage6.Controls.Add(this.label348);
            this.tabPage6.Controls.Add(this.panel349);
            this.tabPage6.Controls.Add(this.label349);
            this.tabPage6.Controls.Add(this.panel350);
            this.tabPage6.Controls.Add(this.label350);
            this.tabPage6.Controls.Add(this.panel351);
            this.tabPage6.Controls.Add(this.label351);
            this.tabPage6.Controls.Add(this.panel352);
            this.tabPage6.Location = new System.Drawing.Point(4, 34);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(846, 778);
            this.tabPage6.TabIndex = 3;
            this.tabPage6.Text = "OUT - 3";
            // 
            // label320
            // 
            this.label320.AutoSize = true;
            this.label320.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label320.ForeColor = System.Drawing.Color.White;
            this.label320.Location = new System.Drawing.Point(479, 730);
            this.label320.Name = "label320";
            this.label320.Size = new System.Drawing.Size(114, 13);
            this.label320.TabIndex = 319;
            this.label320.Text = "[ Y1FF ] Spare";
            // 
            // panel321
            // 
            this.panel321.BackColor = System.Drawing.Color.Gray;
            this.panel321.Location = new System.Drawing.Point(433, 718);
            this.panel321.Name = "panel321";
            this.panel321.Size = new System.Drawing.Size(40, 40);
            this.panel321.TabIndex = 318;
            // 
            // label321
            // 
            this.label321.AutoSize = true;
            this.label321.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label321.ForeColor = System.Drawing.Color.White;
            this.label321.Location = new System.Drawing.Point(479, 685);
            this.label321.Name = "label321";
            this.label321.Size = new System.Drawing.Size(115, 13);
            this.label321.TabIndex = 317;
            this.label321.Text = "[ Y1FE ] Spare";
            // 
            // panel322
            // 
            this.panel322.BackColor = System.Drawing.Color.Gray;
            this.panel322.Location = new System.Drawing.Point(433, 672);
            this.panel322.Name = "panel322";
            this.panel322.Size = new System.Drawing.Size(40, 40);
            this.panel322.TabIndex = 316;
            // 
            // label322
            // 
            this.label322.AutoSize = true;
            this.label322.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label322.ForeColor = System.Drawing.Color.White;
            this.label322.Location = new System.Drawing.Point(479, 640);
            this.label322.Name = "label322";
            this.label322.Size = new System.Drawing.Size(116, 13);
            this.label322.TabIndex = 315;
            this.label322.Text = "[ Y1FD ] Spare";
            // 
            // panel323
            // 
            this.panel323.BackColor = System.Drawing.Color.Gray;
            this.panel323.Location = new System.Drawing.Point(433, 626);
            this.panel323.Name = "panel323";
            this.panel323.Size = new System.Drawing.Size(40, 40);
            this.panel323.TabIndex = 314;
            // 
            // label323
            // 
            this.label323.AutoSize = true;
            this.label323.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label323.ForeColor = System.Drawing.Color.White;
            this.label323.Location = new System.Drawing.Point(479, 594);
            this.label323.Name = "label323";
            this.label323.Size = new System.Drawing.Size(116, 13);
            this.label323.TabIndex = 313;
            this.label323.Text = "[ Y1FC ] Spare";
            // 
            // panel324
            // 
            this.panel324.BackColor = System.Drawing.Color.Gray;
            this.panel324.Location = new System.Drawing.Point(433, 580);
            this.panel324.Name = "panel324";
            this.panel324.Size = new System.Drawing.Size(40, 40);
            this.panel324.TabIndex = 312;
            // 
            // label324
            // 
            this.label324.AutoSize = true;
            this.label324.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label324.ForeColor = System.Drawing.Color.White;
            this.label324.Location = new System.Drawing.Point(479, 548);
            this.label324.Name = "label324";
            this.label324.Size = new System.Drawing.Size(116, 13);
            this.label324.TabIndex = 311;
            this.label324.Text = "[ Y1FB ] Spare";
            // 
            // panel325
            // 
            this.panel325.BackColor = System.Drawing.Color.Gray;
            this.panel325.Location = new System.Drawing.Point(433, 534);
            this.panel325.Name = "panel325";
            this.panel325.Size = new System.Drawing.Size(40, 40);
            this.panel325.TabIndex = 310;
            // 
            // label325
            // 
            this.label325.AutoSize = true;
            this.label325.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label325.ForeColor = System.Drawing.Color.White;
            this.label325.Location = new System.Drawing.Point(479, 501);
            this.label325.Name = "label325";
            this.label325.Size = new System.Drawing.Size(115, 13);
            this.label325.TabIndex = 309;
            this.label325.Text = "[ Y1FA ] Spare";
            // 
            // panel326
            // 
            this.panel326.BackColor = System.Drawing.Color.Gray;
            this.panel326.Location = new System.Drawing.Point(433, 488);
            this.panel326.Name = "panel326";
            this.panel326.Size = new System.Drawing.Size(40, 40);
            this.panel326.TabIndex = 308;
            // 
            // label326
            // 
            this.label326.AutoSize = true;
            this.label326.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label326.ForeColor = System.Drawing.Color.White;
            this.label326.Location = new System.Drawing.Point(479, 454);
            this.label326.Name = "label326";
            this.label326.Size = new System.Drawing.Size(114, 13);
            this.label326.TabIndex = 307;
            this.label326.Text = "[ Y1F9 ] Spare";
            // 
            // panel327
            // 
            this.panel327.BackColor = System.Drawing.Color.Gray;
            this.panel327.Location = new System.Drawing.Point(433, 442);
            this.panel327.Name = "panel327";
            this.panel327.Size = new System.Drawing.Size(40, 40);
            this.panel327.TabIndex = 306;
            // 
            // label327
            // 
            this.label327.AutoSize = true;
            this.label327.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label327.ForeColor = System.Drawing.Color.White;
            this.label327.Location = new System.Drawing.Point(479, 409);
            this.label327.Name = "label327";
            this.label327.Size = new System.Drawing.Size(114, 13);
            this.label327.TabIndex = 305;
            this.label327.Text = "[ Y1F8 ] Spare";
            // 
            // panel328
            // 
            this.panel328.BackColor = System.Drawing.Color.Gray;
            this.panel328.Location = new System.Drawing.Point(433, 396);
            this.panel328.Name = "panel328";
            this.panel328.Size = new System.Drawing.Size(40, 40);
            this.panel328.TabIndex = 304;
            // 
            // label328
            // 
            this.label328.AutoSize = true;
            this.label328.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label328.ForeColor = System.Drawing.Color.White;
            this.label328.Location = new System.Drawing.Point(479, 364);
            this.label328.Name = "label328";
            this.label328.Size = new System.Drawing.Size(114, 13);
            this.label328.TabIndex = 303;
            this.label328.Text = "[ Y1F7 ] Spare";
            // 
            // panel329
            // 
            this.panel329.BackColor = System.Drawing.Color.Gray;
            this.panel329.Location = new System.Drawing.Point(433, 350);
            this.panel329.Name = "panel329";
            this.panel329.Size = new System.Drawing.Size(40, 40);
            this.panel329.TabIndex = 302;
            // 
            // label329
            // 
            this.label329.AutoSize = true;
            this.label329.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label329.ForeColor = System.Drawing.Color.White;
            this.label329.Location = new System.Drawing.Point(479, 319);
            this.label329.Name = "label329";
            this.label329.Size = new System.Drawing.Size(114, 13);
            this.label329.TabIndex = 301;
            this.label329.Text = "[ Y1F6 ] Spare";
            // 
            // panel330
            // 
            this.panel330.BackColor = System.Drawing.Color.Gray;
            this.panel330.Location = new System.Drawing.Point(433, 304);
            this.panel330.Name = "panel330";
            this.panel330.Size = new System.Drawing.Size(40, 40);
            this.panel330.TabIndex = 300;
            // 
            // label330
            // 
            this.label330.AutoSize = true;
            this.label330.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label330.ForeColor = System.Drawing.Color.White;
            this.label330.Location = new System.Drawing.Point(479, 271);
            this.label330.Name = "label330";
            this.label330.Size = new System.Drawing.Size(114, 13);
            this.label330.TabIndex = 299;
            this.label330.Text = "[ Y1F5 ] Spare";
            // 
            // panel331
            // 
            this.panel331.BackColor = System.Drawing.Color.Gray;
            this.panel331.Location = new System.Drawing.Point(433, 258);
            this.panel331.Name = "panel331";
            this.panel331.Size = new System.Drawing.Size(40, 40);
            this.panel331.TabIndex = 298;
            // 
            // label331
            // 
            this.label331.AutoSize = true;
            this.label331.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label331.ForeColor = System.Drawing.Color.White;
            this.label331.Location = new System.Drawing.Point(479, 226);
            this.label331.Name = "label331";
            this.label331.Size = new System.Drawing.Size(114, 13);
            this.label331.TabIndex = 297;
            this.label331.Text = "[ Y1F4 ] Spare";
            // 
            // panel332
            // 
            this.panel332.BackColor = System.Drawing.Color.Gray;
            this.panel332.Location = new System.Drawing.Point(433, 212);
            this.panel332.Name = "panel332";
            this.panel332.Size = new System.Drawing.Size(40, 40);
            this.panel332.TabIndex = 296;
            // 
            // label332
            // 
            this.label332.AutoSize = true;
            this.label332.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label332.ForeColor = System.Drawing.Color.White;
            this.label332.Location = new System.Drawing.Point(479, 179);
            this.label332.Name = "label332";
            this.label332.Size = new System.Drawing.Size(114, 13);
            this.label332.TabIndex = 295;
            this.label332.Text = "[ Y1F3 ] Spare";
            // 
            // panel333
            // 
            this.panel333.BackColor = System.Drawing.Color.Gray;
            this.panel333.Location = new System.Drawing.Point(433, 166);
            this.panel333.Name = "panel333";
            this.panel333.Size = new System.Drawing.Size(40, 40);
            this.panel333.TabIndex = 294;
            // 
            // label333
            // 
            this.label333.AutoSize = true;
            this.label333.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label333.ForeColor = System.Drawing.Color.White;
            this.label333.Location = new System.Drawing.Point(479, 133);
            this.label333.Name = "label333";
            this.label333.Size = new System.Drawing.Size(114, 13);
            this.label333.TabIndex = 293;
            this.label333.Text = "[ Y1F2 ] Spare";
            // 
            // panel334
            // 
            this.panel334.BackColor = System.Drawing.Color.Gray;
            this.panel334.Location = new System.Drawing.Point(433, 120);
            this.panel334.Name = "panel334";
            this.panel334.Size = new System.Drawing.Size(40, 40);
            this.panel334.TabIndex = 292;
            // 
            // label334
            // 
            this.label334.AutoSize = true;
            this.label334.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label334.ForeColor = System.Drawing.Color.White;
            this.label334.Location = new System.Drawing.Point(479, 86);
            this.label334.Name = "label334";
            this.label334.Size = new System.Drawing.Size(114, 13);
            this.label334.TabIndex = 291;
            this.label334.Text = "[ Y1F1 ] Spare";
            // 
            // panel335
            // 
            this.panel335.BackColor = System.Drawing.Color.Gray;
            this.panel335.Location = new System.Drawing.Point(433, 74);
            this.panel335.Name = "panel335";
            this.panel335.Size = new System.Drawing.Size(40, 40);
            this.panel335.TabIndex = 290;
            // 
            // label335
            // 
            this.label335.AutoSize = true;
            this.label335.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label335.ForeColor = System.Drawing.Color.White;
            this.label335.Location = new System.Drawing.Point(479, 42);
            this.label335.Name = "label335";
            this.label335.Size = new System.Drawing.Size(114, 13);
            this.label335.TabIndex = 289;
            this.label335.Text = "[ Y1F0 ] Spare";
            // 
            // panel336
            // 
            this.panel336.BackColor = System.Drawing.Color.Gray;
            this.panel336.Location = new System.Drawing.Point(433, 28);
            this.panel336.Name = "panel336";
            this.panel336.Size = new System.Drawing.Size(40, 40);
            this.panel336.TabIndex = 288;
            // 
            // label336
            // 
            this.label336.AutoSize = true;
            this.label336.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label336.ForeColor = System.Drawing.Color.White;
            this.label336.Location = new System.Drawing.Point(52, 730);
            this.label336.Name = "label336";
            this.label336.Size = new System.Drawing.Size(115, 13);
            this.label336.TabIndex = 287;
            this.label336.Text = "[ Y1EF ] Spare";
            // 
            // panel337
            // 
            this.panel337.BackColor = System.Drawing.Color.Gray;
            this.panel337.Location = new System.Drawing.Point(6, 718);
            this.panel337.Name = "panel337";
            this.panel337.Size = new System.Drawing.Size(40, 40);
            this.panel337.TabIndex = 286;
            // 
            // label337
            // 
            this.label337.AutoSize = true;
            this.label337.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label337.ForeColor = System.Drawing.Color.White;
            this.label337.Location = new System.Drawing.Point(52, 685);
            this.label337.Name = "label337";
            this.label337.Size = new System.Drawing.Size(219, 13);
            this.label337.TabIndex = 285;
            this.label337.Text = "[ Y1EE ] load 뮤팅 down lamp";
            // 
            // panel338
            // 
            this.panel338.BackColor = System.Drawing.Color.Gray;
            this.panel338.Location = new System.Drawing.Point(6, 672);
            this.panel338.Name = "panel338";
            this.panel338.Size = new System.Drawing.Size(40, 40);
            this.panel338.TabIndex = 284;
            // 
            // label338
            // 
            this.label338.AutoSize = true;
            this.label338.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label338.ForeColor = System.Drawing.Color.White;
            this.label338.Location = new System.Drawing.Point(52, 640);
            this.label338.Name = "label338";
            this.label338.Size = new System.Drawing.Size(200, 13);
            this.label338.TabIndex = 283;
            this.label338.Text = "[ Y1ED ] load 뮤팅 up lamp";
            // 
            // panel339
            // 
            this.panel339.BackColor = System.Drawing.Color.Gray;
            this.panel339.Location = new System.Drawing.Point(6, 626);
            this.panel339.Name = "panel339";
            this.panel339.Size = new System.Drawing.Size(40, 40);
            this.panel339.TabIndex = 282;
            // 
            // label339
            // 
            this.label339.AutoSize = true;
            this.label339.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label339.ForeColor = System.Drawing.Color.White;
            this.label339.Location = new System.Drawing.Point(52, 594);
            this.label339.Name = "label339";
            this.label339.Size = new System.Drawing.Size(117, 13);
            this.label339.TabIndex = 281;
            this.label339.Text = "[ Y1EC ] Spare";
            // 
            // panel340
            // 
            this.panel340.BackColor = System.Drawing.Color.Gray;
            this.panel340.Location = new System.Drawing.Point(6, 580);
            this.panel340.Name = "panel340";
            this.panel340.Size = new System.Drawing.Size(40, 40);
            this.panel340.TabIndex = 280;
            // 
            // label340
            // 
            this.label340.AutoSize = true;
            this.label340.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label340.ForeColor = System.Drawing.Color.White;
            this.label340.Location = new System.Drawing.Point(52, 548);
            this.label340.Name = "label340";
            this.label340.Size = new System.Drawing.Size(271, 13);
            this.label340.TabIndex = 279;
            this.label340.Text = "[ Y1EB ] 4-2 L/D Lifter Muting On/Off";
            // 
            // panel341
            // 
            this.panel341.BackColor = System.Drawing.Color.Gray;
            this.panel341.Location = new System.Drawing.Point(6, 534);
            this.panel341.Name = "panel341";
            this.panel341.Size = new System.Drawing.Size(40, 40);
            this.panel341.TabIndex = 278;
            // 
            // label341
            // 
            this.label341.AutoSize = true;
            this.label341.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label341.ForeColor = System.Drawing.Color.White;
            this.label341.Location = new System.Drawing.Point(52, 501);
            this.label341.Name = "label341";
            this.label341.Size = new System.Drawing.Size(270, 13);
            this.label341.TabIndex = 277;
            this.label341.Text = "[ Y1EA ] 4-1 L/D Lifter Muting On/Off";
            // 
            // panel342
            // 
            this.panel342.BackColor = System.Drawing.Color.Gray;
            this.panel342.Location = new System.Drawing.Point(6, 488);
            this.panel342.Name = "panel342";
            this.panel342.Size = new System.Drawing.Size(40, 40);
            this.panel342.TabIndex = 276;
            // 
            // label342
            // 
            this.label342.AutoSize = true;
            this.label342.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label342.ForeColor = System.Drawing.Color.White;
            this.label342.Location = new System.Drawing.Point(52, 454);
            this.label342.Name = "label342";
            this.label342.Size = new System.Drawing.Size(269, 13);
            this.label342.TabIndex = 275;
            this.label342.Text = "[ Y1E9 ] 3-2 L/D Lifter Muting On/Off";
            // 
            // panel343
            // 
            this.panel343.BackColor = System.Drawing.Color.Gray;
            this.panel343.Location = new System.Drawing.Point(6, 442);
            this.panel343.Name = "panel343";
            this.panel343.Size = new System.Drawing.Size(40, 40);
            this.panel343.TabIndex = 274;
            // 
            // label343
            // 
            this.label343.AutoSize = true;
            this.label343.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label343.ForeColor = System.Drawing.Color.White;
            this.label343.Location = new System.Drawing.Point(52, 409);
            this.label343.Name = "label343";
            this.label343.Size = new System.Drawing.Size(269, 13);
            this.label343.TabIndex = 273;
            this.label343.Text = "[ Y1E8 ] 3-1 L/D Lifter Muting On/Off";
            // 
            // panel344
            // 
            this.panel344.BackColor = System.Drawing.Color.Gray;
            this.panel344.Location = new System.Drawing.Point(6, 396);
            this.panel344.Name = "panel344";
            this.panel344.Size = new System.Drawing.Size(40, 40);
            this.panel344.TabIndex = 272;
            // 
            // label344
            // 
            this.label344.AutoSize = true;
            this.label344.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label344.ForeColor = System.Drawing.Color.White;
            this.label344.Location = new System.Drawing.Point(52, 364);
            this.label344.Name = "label344";
            this.label344.Size = new System.Drawing.Size(269, 13);
            this.label344.TabIndex = 271;
            this.label344.Text = "[ Y1E7 ] 2-2 L/D Lifter Muting On/Off";
            // 
            // panel345
            // 
            this.panel345.BackColor = System.Drawing.Color.Gray;
            this.panel345.Location = new System.Drawing.Point(6, 350);
            this.panel345.Name = "panel345";
            this.panel345.Size = new System.Drawing.Size(40, 40);
            this.panel345.TabIndex = 270;
            // 
            // label345
            // 
            this.label345.AutoSize = true;
            this.label345.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label345.ForeColor = System.Drawing.Color.White;
            this.label345.Location = new System.Drawing.Point(52, 319);
            this.label345.Name = "label345";
            this.label345.Size = new System.Drawing.Size(269, 13);
            this.label345.TabIndex = 269;
            this.label345.Text = "[ Y1E6 ] 2-1 L/D Lifter Muting On/Off";
            // 
            // panel346
            // 
            this.panel346.BackColor = System.Drawing.Color.Gray;
            this.panel346.Location = new System.Drawing.Point(6, 304);
            this.panel346.Name = "panel346";
            this.panel346.Size = new System.Drawing.Size(40, 40);
            this.panel346.TabIndex = 268;
            // 
            // label346
            // 
            this.label346.AutoSize = true;
            this.label346.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label346.ForeColor = System.Drawing.Color.White;
            this.label346.Location = new System.Drawing.Point(52, 271);
            this.label346.Name = "label346";
            this.label346.Size = new System.Drawing.Size(269, 13);
            this.label346.TabIndex = 267;
            this.label346.Text = "[ Y1E5 ] 1-2 L/D Lifter Muting On/Off";
            // 
            // panel347
            // 
            this.panel347.BackColor = System.Drawing.Color.Gray;
            this.panel347.Location = new System.Drawing.Point(6, 258);
            this.panel347.Name = "panel347";
            this.panel347.Size = new System.Drawing.Size(40, 40);
            this.panel347.TabIndex = 266;
            // 
            // label347
            // 
            this.label347.AutoSize = true;
            this.label347.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label347.ForeColor = System.Drawing.Color.White;
            this.label347.Location = new System.Drawing.Point(52, 226);
            this.label347.Name = "label347";
            this.label347.Size = new System.Drawing.Size(269, 13);
            this.label347.TabIndex = 265;
            this.label347.Text = "[ Y1E4 ] 1-1 L/D Lifter Muting On/Off";
            // 
            // panel348
            // 
            this.panel348.BackColor = System.Drawing.Color.Gray;
            this.panel348.Location = new System.Drawing.Point(6, 212);
            this.panel348.Name = "panel348";
            this.panel348.Size = new System.Drawing.Size(40, 40);
            this.panel348.TabIndex = 264;
            // 
            // label348
            // 
            this.label348.AutoSize = true;
            this.label348.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label348.ForeColor = System.Drawing.Color.White;
            this.label348.Location = new System.Drawing.Point(52, 179);
            this.label348.Name = "label348";
            this.label348.Size = new System.Drawing.Size(262, 13);
            this.label348.TabIndex = 263;
            this.label348.Text = "[ Y1E3 ] 2-2 L/D 배출 Muting On/Off";
            // 
            // panel349
            // 
            this.panel349.BackColor = System.Drawing.Color.Gray;
            this.panel349.Location = new System.Drawing.Point(6, 166);
            this.panel349.Name = "panel349";
            this.panel349.Size = new System.Drawing.Size(40, 40);
            this.panel349.TabIndex = 262;
            // 
            // label349
            // 
            this.label349.AutoSize = true;
            this.label349.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label349.ForeColor = System.Drawing.Color.White;
            this.label349.Location = new System.Drawing.Point(52, 133);
            this.label349.Name = "label349";
            this.label349.Size = new System.Drawing.Size(262, 13);
            this.label349.TabIndex = 261;
            this.label349.Text = "[ Y1E2 ] 2-1 L/D 배출 Muting On/Off";
            // 
            // panel350
            // 
            this.panel350.BackColor = System.Drawing.Color.Gray;
            this.panel350.Location = new System.Drawing.Point(6, 120);
            this.panel350.Name = "panel350";
            this.panel350.Size = new System.Drawing.Size(40, 40);
            this.panel350.TabIndex = 260;
            // 
            // label350
            // 
            this.label350.AutoSize = true;
            this.label350.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label350.ForeColor = System.Drawing.Color.White;
            this.label350.Location = new System.Drawing.Point(52, 86);
            this.label350.Name = "label350";
            this.label350.Size = new System.Drawing.Size(262, 13);
            this.label350.TabIndex = 259;
            this.label350.Text = "[ Y1E1 ] 1-2 L/D 투입 Muting On/Off";
            // 
            // panel351
            // 
            this.panel351.BackColor = System.Drawing.Color.Gray;
            this.panel351.Location = new System.Drawing.Point(6, 74);
            this.panel351.Name = "panel351";
            this.panel351.Size = new System.Drawing.Size(40, 40);
            this.panel351.TabIndex = 258;
            // 
            // label351
            // 
            this.label351.AutoSize = true;
            this.label351.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label351.ForeColor = System.Drawing.Color.White;
            this.label351.Location = new System.Drawing.Point(52, 42);
            this.label351.Name = "label351";
            this.label351.Size = new System.Drawing.Size(262, 13);
            this.label351.TabIndex = 257;
            this.label351.Text = "[ Y1E0 ] 1-1 L/D 투입 Muting On/Off";
            // 
            // panel352
            // 
            this.panel352.BackColor = System.Drawing.Color.Gray;
            this.panel352.Location = new System.Drawing.Point(6, 28);
            this.panel352.Name = "panel352";
            this.panel352.Size = new System.Drawing.Size(40, 40);
            this.panel352.TabIndex = 256;
            // 
            // Manager_IOStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.Controls.Add(this.tc_iostatus_info);
            this.Name = "Manager_IOStatus";
            this.Size = new System.Drawing.Size(1740, 860);
            this.tc_iostatus_info.ResumeLayout(false);
            this.tp_iostatus_ld.ResumeLayout(false);
            this.tc_iostatus_ld_out.ResumeLayout(false);
            this.tp_iostatus_ld_out1.ResumeLayout(false);
            this.tp_iostatus_ld_out1.PerformLayout();
            this.tp_iostatus_ld_out2.ResumeLayout(false);
            this.tp_iostatus_ld_out2.PerformLayout();
            this.tp_iostatus_ld_out3.ResumeLayout(false);
            this.tp_iostatus_ld_out3.PerformLayout();
            this.tp_iostatus_ld_out4.ResumeLayout(false);
            this.tp_iostatus_ld_out4.PerformLayout();
            this.tc_iostatus_ld_in.ResumeLayout(false);
            this.tp_iostatus_ld_in1.ResumeLayout(false);
            this.tp_iostatus_ld_in1.PerformLayout();
            this.tp_iostatus_ld_in2.ResumeLayout(false);
            this.tp_iostatus_ld_in2.PerformLayout();
            this.tp_iostatus_ld_in3.ResumeLayout(false);
            this.tp_iostatus_ld_in3.PerformLayout();
            this.tp_iostatus_ld_in4.ResumeLayout(false);
            this.tp_iostatus_ld_in4.PerformLayout();
            this.tp_iostatus_process.ResumeLayout(false);
            this.tc_iostatus_process_out.ResumeLayout(false);
            this.tp_iostatus_process_out1.ResumeLayout(false);
            this.tp_iostatus_process_out1.PerformLayout();
            this.tp_iostatus_process_out2.ResumeLayout(false);
            this.tp_iostatus_process_out2.PerformLayout();
            this.tp_iostatus_process_out3.ResumeLayout(false);
            this.tp_iostatus_process_out3.PerformLayout();
            this.tc_iostatus_process_in.ResumeLayout(false);
            this.tp_iostatus_process_in1.ResumeLayout(false);
            this.tp_iostatus_process_in1.PerformLayout();
            this.tp_iostatus_process_in2.ResumeLayout(false);
            this.tp_iostatus_process_in2.PerformLayout();
            this.tp_iostatus_process_in3.ResumeLayout(false);
            this.tp_iostatus_process_in3.PerformLayout();
            this.tp_iostatus_uld.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tc_iostatus_info;
        private System.Windows.Forms.TabPage tp_iostatus_ld;
        private System.Windows.Forms.TabPage tp_iostatus_process;
        private System.Windows.Forms.TabPage tp_iostatus_uld;
        private System.Windows.Forms.TabControl tc_iostatus_ld_in;
        private System.Windows.Forms.TabPage tp_iostatus_ld_in1;
        private System.Windows.Forms.TabPage tp_iostatus_ld_in2;
        private System.Windows.Forms.TabPage tp_iostatus_ld_in3;
        private System.Windows.Forms.TabPage tp_iostatus_ld_in4;
        private System.Windows.Forms.TabControl tc_iostatus_ld_out;
        private System.Windows.Forms.TabPage tp_iostatus_ld_out1;
        private System.Windows.Forms.TabPage tp_iostatus_ld_out2;
        private System.Windows.Forms.TabPage tp_iostatus_ld_out3;
        private System.Windows.Forms.TabPage tp_iostatus_ld_out4;
        private System.Windows.Forms.Panel pn_Loader_IN1_X100;
        private System.Windows.Forms.Label lb_ld_in_muting_switch_on_off;
        private System.Windows.Forms.Label lb_x101_ld_out_muting_switch_on_off;
        private System.Windows.Forms.Panel pn_Loader_IN1_X101;
        private System.Windows.Forms.Label lb_x103_control_reset_switch;
        private System.Windows.Forms.Panel pn_Loader_IN1_X103;
        private System.Windows.Forms.Label lb_x102_ld_reset_switch;
        private System.Windows.Forms.Panel pn_Loader_IN1_X102;
        private System.Windows.Forms.Label lb_x105_ld_out_muting_on;
        private System.Windows.Forms.Panel pn_Loader_IN1_X105;
        private System.Windows.Forms.Label lb_x104_ld_in_muting_on;
        private System.Windows.Forms.Panel pn_Loader_IN1_X104;
        private System.Windows.Forms.Label lb_x106_ld_lifter_muting_on;
        private System.Windows.Forms.Panel pn_Loader_IN1_X106;
        private System.Windows.Forms.Label lb_x107_ld_lifter_muting_on;
        private System.Windows.Forms.Panel pn_Loader_IN1_X107;
        private System.Windows.Forms.Label lb_x109_ld_lifter_muting_on;
        private System.Windows.Forms.Panel pn_Loader_IN1_X109;
        private System.Windows.Forms.Label lb_x108_ld_lifter_muting_on;
        private System.Windows.Forms.Panel pn_Loader_IN1_X108;
        private System.Windows.Forms.Label lb_x10b_2_emergency_stop;
        private System.Windows.Forms.Panel pn_Loader_IN1_X10B;
        private System.Windows.Forms.Label lb_x10a_1_emergency_stop;
        private System.Windows.Forms.Panel pn_Loader_IN1_X10A;
        private System.Windows.Forms.Label lb_x10c_3_emergency_stop;
        private System.Windows.Forms.Panel pn_Loader_IN1_X10C;
        private System.Windows.Forms.Label lb_x10d_1_emergency_stop_enable_grip_sw;
        private System.Windows.Forms.Panel pn_Loader_IN1_X10D;
        private System.Windows.Forms.Label lb_x10f_3_emergency_stop_enable_grip_sw;
        private System.Windows.Forms.Panel pn_Loader_IN1_X10F;
        private System.Windows.Forms.Label lb_x10e_2_emergency_stop_enable_grip_sw;
        private System.Windows.Forms.Panel pn_Loader_IN1_X10E;
        private System.Windows.Forms.Label lb_x11f_spare;
        private System.Windows.Forms.Panel pn_Loader_IN1_X11F;
        private System.Windows.Forms.Label lb_x11e_spare;
        private System.Windows.Forms.Panel pn_Loader_IN1_X11E;
        private System.Windows.Forms.Label lb_x11d_cell_trans_crush_sensor;
        private System.Windows.Forms.Panel pn_Loader_IN1_X11D;
        private System.Windows.Forms.Label lb_x11c_spare;
        private System.Windows.Forms.Panel pn_Loader_IN1_X11C;
        private System.Windows.Forms.Label lb_x11b_spare;
        private System.Windows.Forms.Panel pn_Loader_IN1_X11B;
        private System.Windows.Forms.Label lb_x11a_spare;
        private System.Windows.Forms.Panel pn_Loader_IN1_X11A;
        private System.Windows.Forms.Label lb_x119_7_safety_door_sw_open;
        private System.Windows.Forms.Panel pn_Loader_IN1_X119;
        private System.Windows.Forms.Label lb_x118_6_safety_door_sw_open;
        private System.Windows.Forms.Panel pn_Loader_IN1_X118;
        private System.Windows.Forms.Label lb_x117_5_safety_door_sw_open;
        private System.Windows.Forms.Panel pn_Loader_IN1_X117;
        private System.Windows.Forms.Label lb_x116_4_safety_door_sw_open;
        private System.Windows.Forms.Panel pn_Loader_IN1_X116;
        private System.Windows.Forms.Label lb_x115_3_safety_door_sw_open;
        private System.Windows.Forms.Panel pn_Loader_IN1_X115;
        private System.Windows.Forms.Label lb_x114_2_safety_door_sw_open;
        private System.Windows.Forms.Panel pn_Loader_IN1_X114;
        private System.Windows.Forms.Label lb_x113_1_safety_door_sw_open;
        private System.Windows.Forms.Panel pn_Loader_IN1_X113;
        private System.Windows.Forms.Label lb_x112_3_safety_enable_grip_sw_on;
        private System.Windows.Forms.Panel pn_Loader_IN1_X112;
        private System.Windows.Forms.Label lb_x111_2_safety_enable_grip_sw_on;
        private System.Windows.Forms.Panel pn_Loader_IN1_X111;
        private System.Windows.Forms.Label lb_x110_1_safety_enable_grip_sw_on;
        private System.Windows.Forms.Panel pn_Loader_IN1_X110;
        private System.Windows.Forms.Label lb_y19f_4_ld_cst_d_ungrip;
        private System.Windows.Forms.Panel pn_Loader_OUT1_Y19F;
        private System.Windows.Forms.Label lb_y19e_4_ld_cst_d_grip;
        private System.Windows.Forms.Panel pn_Loader_OUT1_Y19E;
        private System.Windows.Forms.Label lb_y19d_4_ld_cst_lr_ungrip;
        private System.Windows.Forms.Panel pn_Loader_OUT1_Y19D;
        private System.Windows.Forms.Label lb_y19c_4_ld_cst_lr_grip;
        private System.Windows.Forms.Panel pn_Loader_OUT1_Y19C;
        private System.Windows.Forms.Label lb_y19b_3_ld_cst_d_ungrip;
        private System.Windows.Forms.Panel pn_Loader_OUT1_Y19B;
        private System.Windows.Forms.Label lb_y19a_3_ld_cst_d_grip;
        private System.Windows.Forms.Panel pn_Loader_OUT1_Y19A;
        private System.Windows.Forms.Label lb_y199_3_ld_cst_lr_ungrip;
        private System.Windows.Forms.Panel pn_Loader_OUT1_Y199;
        private System.Windows.Forms.Label lb_y198_3_ld_cst_lr_grip;
        private System.Windows.Forms.Panel pn_Loader_OUT1_Y198;
        private System.Windows.Forms.Label lb_y197_2_ld_cst_d_ungrip;
        private System.Windows.Forms.Panel pn_Loader_OUT1_Y197;
        private System.Windows.Forms.Label lb_y196_2_ld_cst_d_grip;
        private System.Windows.Forms.Panel pn_Loader_OUT1_Y196;
        private System.Windows.Forms.Label lb_y195_2_ld_cst_lr_ungrip;
        private System.Windows.Forms.Panel pn_Loader_OUT1_Y195;
        private System.Windows.Forms.Label lb_y194_2_ld_cst_lr_grip;
        private System.Windows.Forms.Panel pn_Loader_OUT1_Y194;
        private System.Windows.Forms.Label lb_y193_1_ld_cst_d_ungrip;
        private System.Windows.Forms.Panel pn_Loader_OUT1_Y193;
        private System.Windows.Forms.Label lb_y192_1_ld_cst_d_grip;
        private System.Windows.Forms.Panel pn_Loader_OUT1_Y192;
        private System.Windows.Forms.Label lb_y191_1_ld_cst_lr_ungrip;
        private System.Windows.Forms.Panel pn_Loader_OUT1_Y191;
        private System.Windows.Forms.Label lb_y190_1_ld_cst_lr_grip;
        private System.Windows.Forms.Panel pn_Loader_OUT1_Y190;
        private System.Windows.Forms.Label lb_y18f_mode_select_sw;
        private System.Windows.Forms.Panel pn_Loader_OUT1_Y18F;
        private System.Windows.Forms.Label lb_y18e_ld_reset;
        private System.Windows.Forms.Panel pn_Loader_OUT1_Y18E;
        private System.Windows.Forms.Label lb_y18d_cst_out_ready_buzzer;
        private System.Windows.Forms.Panel pn_Loader_OUT1_Y18D;
        private System.Windows.Forms.Label lb_y18c_cst_in_ready_buzzer;
        private System.Windows.Forms.Panel pn_Loader_OUT1_Y18C;
        private System.Windows.Forms.Label lb_y18b_control_select_ld_feedback;
        private System.Windows.Forms.Panel pn_Loader_OUT1_Y18B;
        private System.Windows.Forms.Label lb_y18a_buzzer;
        private System.Windows.Forms.Panel pn_Loader_OUT1_Y18A;
        private System.Windows.Forms.Label lb_y189_1_tower_lamp_g;
        private System.Windows.Forms.Panel pn_Loader_OUT1_Y189;
        private System.Windows.Forms.Label lb_y188_1_tower_lamp_y;
        private System.Windows.Forms.Panel pn_Loader_OUT1_Y188;
        private System.Windows.Forms.Label lb_y187_1_tower_lamp_r;
        private System.Windows.Forms.Panel pn_Loader_OUT1_Y187;
        private System.Windows.Forms.Label lb_y186_safty_reset;
        private System.Windows.Forms.Panel pn_Loader_OUT1_Y186;
        private System.Windows.Forms.Label lb_y185_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT1_Y185;
        private System.Windows.Forms.Label lb_y184_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT1_Y184;
        private System.Windows.Forms.Label lb_y183_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT1_Y183;
        private System.Windows.Forms.Label lb_y182_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT1_Y182;
        private System.Windows.Forms.Label lb_y181_laser_ld_motor_control_on_cmd;
        private System.Windows.Forms.Panel pn_Loader_OUT1_Y181;
        private System.Windows.Forms.Label lb_y180_ld_motor_control_on_cmd;
        private System.Windows.Forms.Panel pn_Loader_OUT1_Y180;
        private System.Windows.Forms.Label lb_x13f_laser_exhaust_fan_run;
        private System.Windows.Forms.Panel pn_Loader_IN2_X13F;
        private System.Windows.Forms.Label lb_x13e_laser_inspiration_fan_run;
        private System.Windows.Forms.Panel pn_Loader_IN2_X13E;
        private System.Windows.Forms.Label label_x13d_laser_panel_critical_alarm;
        private System.Windows.Forms.Panel pn_Loader_IN2_X13D;
        private System.Windows.Forms.Label lb_x13c_ld_panel_critical_alarm;
        private System.Windows.Forms.Panel pn_Loader_IN2_X13C;
        private System.Windows.Forms.Label lb_x13b_2_ld_cst_d_ungrip_pos;
        private System.Windows.Forms.Panel pn_Loader_IN2_X13B;
        private System.Windows.Forms.Label lb_x13a_2_ld_cst_d_grip_pos;
        private System.Windows.Forms.Panel pn_Loader_IN2_X13A;
        private System.Windows.Forms.Label lb_x139_2_2_ld_cst_lr_ungrip_pos;
        private System.Windows.Forms.Panel pn_Loader_IN2_X139;
        private System.Windows.Forms.Label lb_x138_2_2_ld_cst_lr_grip_pos;
        private System.Windows.Forms.Panel pn_Loader_IN2_X138;
        private System.Windows.Forms.Label lb_x137_2_1_ld_cst_lr_ungrip_pos;
        private System.Windows.Forms.Panel pn_Loader_IN2_X137;
        private System.Windows.Forms.Label lb_x136_2_1_ld_cst_lr_grip_pos;
        private System.Windows.Forms.Panel pn_Loader_IN2_X136;
        private System.Windows.Forms.Label lb_x135_1_ld_cst_d_ungrip_pos;
        private System.Windows.Forms.Panel pn_Loader_IN2_X135;
        private System.Windows.Forms.Label lb_x134_1_ld_cst_d_grip_pos;
        private System.Windows.Forms.Panel pn_Loader_IN2_X134;
        private System.Windows.Forms.Label lb_x133_1_2_ld_cst_lr_ungrip_pos;
        private System.Windows.Forms.Panel pn_Loader_IN2_X133;
        private System.Windows.Forms.Label lb_x132_1_2_ld_cst_lr_grip_pos;
        private System.Windows.Forms.Panel pn_Loader_IN2_X132;
        private System.Windows.Forms.Label lb_x131_1_1_ld_cst_lr_ungrip_pos;
        private System.Windows.Forms.Panel pn_Loader_IN2_X131;
        private System.Windows.Forms.Label lb_x130_1_1_ld_cst_lr_grip_pos;
        private System.Windows.Forms.Panel pn_Loader_IN2_X130;
        private System.Windows.Forms.Label lb_x12f_spare;
        private System.Windows.Forms.Panel pn_Loader_IN2_X12F;
        private System.Windows.Forms.Label lb_x12e_2_3_ld_cst_dettect;
        private System.Windows.Forms.Panel pn_Loader_IN2_X1E;
        private System.Windows.Forms.Label lb_x12d_2_2_ld_cst_dettect;
        private System.Windows.Forms.Panel pn_Loader_IN2_X12D;
        private System.Windows.Forms.Label lb_x12c_2_1_ld_cst_dettect;
        private System.Windows.Forms.Panel pn_Loader_IN2_X12C;
        private System.Windows.Forms.Label lb_x12b_1_3_ld_cst_dettect;
        private System.Windows.Forms.Panel pn_Loader_IN2_X12B;
        private System.Windows.Forms.Label lb_x12a_1_2_ld_cst_dettect;
        private System.Windows.Forms.Panel pn_Loader_IN2_X12A;
        private System.Windows.Forms.Label lb_x129_1_1_ld_cst_dettect;
        private System.Windows.Forms.Panel pn_Loader_IN2_X129;
        private System.Windows.Forms.Label lb_x128_control_select_ld;
        private System.Windows.Forms.Panel pn_Loader_IN2_X128;
        private System.Windows.Forms.Label lb_x127_spare;
        private System.Windows.Forms.Panel pn_Loader_IN2_X127;
        private System.Windows.Forms.Label lb_x126_spare;
        private System.Windows.Forms.Panel pn_Loader_IN2_X126;
        private System.Windows.Forms.Label lb_x125_spare;
        private System.Windows.Forms.Panel pn_Loader_IN2_X125;
        private System.Windows.Forms.Label lb_x124_efu_alarm;
        private System.Windows.Forms.Panel pn_Loader_IN2_X124;
        private System.Windows.Forms.Label lb_x123_laser_mc_on;
        private System.Windows.Forms.Panel pn_Loader_IN2_X123;
        private System.Windows.Forms.Label lb_x122_ld_mc_on;
        private System.Windows.Forms.Panel pn_Loader_IN2_X122;
        private System.Windows.Forms.Label lb_x121_mode_select_sw_teach;
        private System.Windows.Forms.Panel pn_Loader_IN2_X121;
        private System.Windows.Forms.Label lb_x120_mode_select_sw_auto;
        private System.Windows.Forms.Panel pn_Loader_IN2_X120;
        private System.Windows.Forms.Label lb_y1bf_spare_air_on_cmd;
        private System.Windows.Forms.Panel pn_Loader_OUT2_Y1BF;
        private System.Windows.Forms.Label lb_y1be_spare_air_on_cmd;
        private System.Windows.Forms.Panel pn_Loader_OUT2_Y1BE;
        private System.Windows.Forms.Label lb_y1bd_2_cst_trans_air_on;
        private System.Windows.Forms.Panel pn_Loader_OUT2_Y1BD;
        private System.Windows.Forms.Label lb_y1bc_2_cst_trans_vacuum_off;
        private System.Windows.Forms.Panel pn_Loader_OUT2_Y1BC;
        private System.Windows.Forms.Label lb_y1bb_2_cst_trans_vacuum_on;
        private System.Windows.Forms.Panel pn_Loader_OUT2_Y1BB;
        private System.Windows.Forms.Label lb_y1ba_1_cst_trans_air_on;
        private System.Windows.Forms.Panel pn_Loader_OUT2_Y1BA;
        private System.Windows.Forms.Label lb_y1b9_1_cst_trans_vacuum_off;
        private System.Windows.Forms.Panel pn_Loader_OUT2_Y1B9;
        private System.Windows.Forms.Label lb_y1b8_1_cst_trans_vacuum_on;
        private System.Windows.Forms.Panel pn_Loader_OUT2_Y1B8;
        private System.Windows.Forms.Label lb_y1b6_2_ld_cst_lift_tiltsensor_down;
        private System.Windows.Forms.Panel pn_Loader_OUT2_Y1B7;
        private System.Windows.Forms.Label lb_y1b6_2_ld_cst_lift_tiltsensor_up;
        private System.Windows.Forms.Panel pn_Loader_OUT2_Y1B6;
        private System.Windows.Forms.Label lb_y1b5_2_ld_trans_2ch_air_on;
        private System.Windows.Forms.Panel pn_Loader_OUT2_Y1B5;
        private System.Windows.Forms.Label lb_y1b4_2_ld_trans_2ch_vacuum_off;
        private System.Windows.Forms.Panel pn_Loader_OUT2_Y1B4;
        private System.Windows.Forms.Label lb_y1b3_2_ld_trans_2ch_vacuum_on;
        private System.Windows.Forms.Panel pn_Loader_OUT2_Y1B3;
        private System.Windows.Forms.Label lb_y1b2_2_ld_trans_1ch_air_on;
        private System.Windows.Forms.Panel pn_Loader_OUT2_Y1B2;
        private System.Windows.Forms.Label lb_y1b1_2_ld_trans_1ch_vacuum_off;
        private System.Windows.Forms.Panel pn_Loader_OUT2_Y1B1;
        private System.Windows.Forms.Label lb_y1b0_2_lld_trans_1ch_vacuum_on;
        private System.Windows.Forms.Panel pn_Loader_OUT2_Y1B0;
        private System.Windows.Forms.Label lb_y1af_1_ld_trans_2ch_air_on;
        private System.Windows.Forms.Panel pn_Loader_OUT2_Y1AF;
        private System.Windows.Forms.Label lb_y1ae_1_ld_trans_2ch_vacuum_off;
        private System.Windows.Forms.Panel pn_Loader_OUT2_Y1AE;
        private System.Windows.Forms.Label lb_y1ad_1_ld_trans_2ch_vacuum_on;
        private System.Windows.Forms.Panel pn_Loader_OUT2_Y1AD;
        private System.Windows.Forms.Label lb_y1ac_1_ld_trans_1ch_air_on;
        private System.Windows.Forms.Panel pn_Loader_OUT2_Y1AC;
        private System.Windows.Forms.Label lb_y1ab_1_ld_trans_1ch_vacuum_off;
        private System.Windows.Forms.Panel pn_Loader_OUT2_Y1AB;
        private System.Windows.Forms.Label lb_y1aa_1_ld_trans_1ch_vacuum_on;
        private System.Windows.Forms.Panel pn_Loader_OUT2_Y1AA;
        private System.Windows.Forms.Label lb_y1a9_2_ld_trans_down;
        private System.Windows.Forms.Panel pn_Loader_OUT2_Y1A9;
        private System.Windows.Forms.Label lb_y1a8_2_ld_trans_up;
        private System.Windows.Forms.Panel pn_Loader_OUT2_Y1A8;
        private System.Windows.Forms.Label lb_y1a7_1_ld_trans_down;
        private System.Windows.Forms.Panel pn_Loader_OUT2_Y1A7;
        private System.Windows.Forms.Label lb_y1a6_1_ld_trans_up;
        private System.Windows.Forms.Panel pn_Loader_OUT2_Y1A6;
        private System.Windows.Forms.Label lb_y1a5_1_ld_cst_lift_tiltsensor_down;
        private System.Windows.Forms.Panel pn_Loader_OUT2_Y1A5;
        private System.Windows.Forms.Label lb_y1a4_1_ld_cst_lift_tiltsensor_up;
        private System.Windows.Forms.Panel pn_Loader_OUT2_Y1A4;
        private System.Windows.Forms.Label lb_y1a3_2_ld_cst_lifter_ungrip;
        private System.Windows.Forms.Panel pn_Loader_OUT2_Y1A3;
        private System.Windows.Forms.Label lb_y1a2_2_ld_cst_lifter_grip;
        private System.Windows.Forms.Panel pn_Loader_OUT2_Y1A2;
        private System.Windows.Forms.Label lb_y1a1_1_ld_cst_lifter_ungrip;
        private System.Windows.Forms.Panel pn_Loader_OUT2_Y1A1;
        private System.Windows.Forms.Label lb_y1a0_1_ld_cst_lifter_grip;
        private System.Windows.Forms.Panel pn_Loader_OUT2_Y1A0;
        private System.Windows.Forms.Label lb_x15f_spare;
        private System.Windows.Forms.Panel pn_Loader_IN3_X15F;
        private System.Windows.Forms.Label lb_x15e_6_laser_fan_stop_alarm;
        private System.Windows.Forms.Panel pn_Loader_IN3_X15E;
        private System.Windows.Forms.Label lb_x15d_2_ld_trans_down;
        private System.Windows.Forms.Panel pn_Loader_IN3_X15D;
        private System.Windows.Forms.Label lb_x15c_2_ld_trans_up;
        private System.Windows.Forms.Panel pn_Loader_IN3_X15C;
        private System.Windows.Forms.Label lb_x15b_1_ld_trans_down;
        private System.Windows.Forms.Panel pn_Loader_IN3_X15B;
        private System.Windows.Forms.Label lb_x15a_1_ld_trans_up;
        private System.Windows.Forms.Panel pn_Loader_IN3_X15A;
        private System.Windows.Forms.Label lb_x159_2_ld_cst_lift_tiltsensor_down;
        private System.Windows.Forms.Panel pn_Loader_IN3_X159;
        private System.Windows.Forms.Label lb_x158_2_ld_cst_lift_tiltsensor_up;
        private System.Windows.Forms.Panel pn_Loader_IN3_X158;
        private System.Windows.Forms.Label lb_x157_2_cst_trans_pressure_sw;
        private System.Windows.Forms.Panel pn_Loader_IN3_X157;
        private System.Windows.Forms.Label lb_x156_1_cst_trans_pressure_sw;
        private System.Windows.Forms.Panel pn_Loader_IN3_X156;
        private System.Windows.Forms.Label lb_x155_1_ld_cst_lift_tiltsensor_down;
        private System.Windows.Forms.Panel pn_Loader_IN3_X155;
        private System.Windows.Forms.Label lb_x154_1_ld_cst_lift_tiltsensor_up;
        private System.Windows.Forms.Panel pn_Loader_IN3_X154;
        private System.Windows.Forms.Label lb_x153_2_2_ld_cst_lifter_ungrip_pos;
        private System.Windows.Forms.Panel pn_Loader_IN3_X153;
        private System.Windows.Forms.Label lb_x152_2_2_ld_cst_lifter_grip_pos;
        private System.Windows.Forms.Panel pn_Loader_IN3_X152;
        private System.Windows.Forms.Label lb_x151_2_1_ld_cst_lifter_ungrip_pos;
        private System.Windows.Forms.Panel pn_Loader_IN3_X151;
        private System.Windows.Forms.Label lb_x150_2_1_ld_cst_lifter_grip_pos;
        private System.Windows.Forms.Panel pn_Loader_IN3_X150;
        private System.Windows.Forms.Label lb_x14f_1_2_ld_cst_lifter_ungrip_pos;
        private System.Windows.Forms.Panel pn_Loader_IN3_X14F;
        private System.Windows.Forms.Label lb_x14e_1_2_ld_cst_lifter_grip_pos;
        private System.Windows.Forms.Panel pn_Loader_IN3_X14E;
        private System.Windows.Forms.Label lb_x14d_1_1_ld_cst_lifter_ungrip_pos;
        private System.Windows.Forms.Panel pn_Loader_IN3_X14D;
        private System.Windows.Forms.Label lb_x14c_1_1_ld_cst_lifter_grip_pos;
        private System.Windows.Forms.Panel pn_Loader_IN3_X14C;
        private System.Windows.Forms.Label lb_x14b_4_ld_cst_d_ungrip_pos;
        private System.Windows.Forms.Panel pn_Loader_IN3_X14B;
        private System.Windows.Forms.Label lb_x14a_4_ld_cst_d_grip_pos;
        private System.Windows.Forms.Panel pn_Loader_IN3_X14A;
        private System.Windows.Forms.Label lb_x149_4_2_ld_cst_lr_ungrip_pos;
        private System.Windows.Forms.Panel pn_Loader_IN3_X149;
        private System.Windows.Forms.Label lb_x148_4_2_ld_cst_lr_grip_pos;
        private System.Windows.Forms.Panel pn_Loader_IN3_X148;
        private System.Windows.Forms.Label lb_x147_4_1_ld_cst_lr_ungrip_pos;
        private System.Windows.Forms.Panel pn_Loader_IN3_X147;
        private System.Windows.Forms.Label lb_x146_4_1_ld_cst_lr_grip_pos;
        private System.Windows.Forms.Panel pn_Loader_IN3_X146;
        private System.Windows.Forms.Label lb_x145_3_ld_cst_d_ungrip_pos;
        private System.Windows.Forms.Panel pn_Loader_IN3_X145;
        private System.Windows.Forms.Label lb_x144_3_ld_cst_d_grip_pos;
        private System.Windows.Forms.Panel pn_Loader_IN3_X144;
        private System.Windows.Forms.Label lb_x143_3_2_ld_cst_lr_ungrip_pos;
        private System.Windows.Forms.Panel pn_Loader_IN3_X143;
        private System.Windows.Forms.Label lb_x142_3_2_ld_cst_lr_grip_pos;
        private System.Windows.Forms.Panel pn_Loader_IN3_X142;
        private System.Windows.Forms.Label lb_x141_3_1_ld_cst_lr_ungrip_pos;
        private System.Windows.Forms.Panel pn_Loader_IN3_X141;
        private System.Windows.Forms.Label lb_x140_3_1_ld_cst_lr_grip_pos;
        private System.Windows.Forms.Panel pn_Loader_IN3_X140;
        private System.Windows.Forms.Label lb_y1df_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT3_Y1DF;
        private System.Windows.Forms.Label lb_y1de_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT3_Y1DE;
        private System.Windows.Forms.Label lb_y1dd_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT3_Y1DD;
        private System.Windows.Forms.Label lb_y1dc_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT3_Y1DC;
        private System.Windows.Forms.Label lb_y1db_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT3_Y1DB;
        private System.Windows.Forms.Label lb_y1da_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT3_Y1DA;
        private System.Windows.Forms.Label lb_y1d9_laser_led;
        private System.Windows.Forms.Panel pn_Loader_OUT3_Y1D9;
        private System.Windows.Forms.Label lb_y1d8_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT3_Y1D8;
        private System.Windows.Forms.Label lb_y1d7_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT3_Y1D7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel pn_Loader_OUT3_Y1D6;
        private System.Windows.Forms.Label lb_y1d5_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT3_Y1D5;
        private System.Windows.Forms.Label lb_y1d4_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT3_Y1D4;
        private System.Windows.Forms.Label lb_y1d3_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT3_Y1D3;
        private System.Windows.Forms.Label lb_y1d2_light_3_onoff;
        private System.Windows.Forms.Panel pn_Loader_OUT3_Y1D2;
        private System.Windows.Forms.Label lb_y1d1_light_2_onoff;
        private System.Windows.Forms.Panel pn_Loader_OUT3_Y1D1;
        private System.Windows.Forms.Label lb_y1d0_light_1_onoff;
        private System.Windows.Forms.Panel pn_Loader_OUT3_Y1D0;
        private System.Windows.Forms.Label lb_y1cf_spare_air_on_cmd;
        private System.Windows.Forms.Panel pn_Loader_OUT3_Y1CF;
        private System.Windows.Forms.Label lb_y1ce_spare_air_on_cmd;
        private System.Windows.Forms.Panel pn_Loader_OUT3_Y1CE;
        private System.Windows.Forms.Label lb_y1cd_spare_sol_on_cmd;
        private System.Windows.Forms.Panel pn_Loader_OUT3_Y1CD;
        private System.Windows.Forms.Label lb_y1cc_spare_sol_on_cmd;
        private System.Windows.Forms.Panel pn_Loader_OUT3_Y1CC;
        private System.Windows.Forms.Label lb_y1cb_spare_sol_on_cmd;
        private System.Windows.Forms.Panel pn_Loader_OUT3_Y1CB;
        private System.Windows.Forms.Label lb_y1ca_spare_sol_on_cmd;
        private System.Windows.Forms.Panel pn_Loader_OUT3_Y1CA;
        private System.Windows.Forms.Label lb_y1c9_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT3_Y1C9;
        private System.Windows.Forms.Label lb_y1c8_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT3_Y1C8;
        private System.Windows.Forms.Label lb_y1c7_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT3_Y1C7;
        private System.Windows.Forms.Label lb_y1c6_7_safety_door_sw_open;
        private System.Windows.Forms.Panel pn_Loader_OUT3_Y1C6;
        private System.Windows.Forms.Label lb_y1c5_6_safety_door_sw_open;
        private System.Windows.Forms.Panel pn_Loader_OUT3_Y1C5;
        private System.Windows.Forms.Label lb_y1c4_5_safety_door_sw_open;
        private System.Windows.Forms.Panel pn_Loader_OUT3_Y1C4;
        private System.Windows.Forms.Label lb_y1c3_4_safety_door_sw_open;
        private System.Windows.Forms.Panel pn_Loader_OUT3_Y1C3;
        private System.Windows.Forms.Label lb_y1c2_3_safety_door_sw_open;
        private System.Windows.Forms.Panel pn_Loader_OUT3_Y1C2;
        private System.Windows.Forms.Label lb_y1c1_2_safety_door_sw_open;
        private System.Windows.Forms.Panel pn_Loader_OUT3_Y1C1;
        private System.Windows.Forms.Label lb_y1c0_1_safety_door_sw_open;
        private System.Windows.Forms.Panel pn_Loader_OUT3_Y1C0;
        private System.Windows.Forms.Panel pn_Loader_IN4_X17F;
        private System.Windows.Forms.Label lb_x17e_4_process_fan_stop_alarm;
        private System.Windows.Forms.Panel pn_Loader_IN4_X17E;
        private System.Windows.Forms.Label lb_x17d_3_process_fan_stop_alarm;
        private System.Windows.Forms.Panel pn_Loader_IN4_X17D;
        private System.Windows.Forms.Label lb_x17c_2_process_fan_stop_alarm;
        private System.Windows.Forms.Panel pn_Loader_IN4_X17C;
        private System.Windows.Forms.Label lb_x17b_1_process_fan_stop_alarm;
        private System.Windows.Forms.Panel pn_Loader_IN4_X17B;
        private System.Windows.Forms.Label lb_x17a_laserhead_blow;
        private System.Windows.Forms.Panel pn_Loader_IN4_X17A;
        private System.Windows.Forms.Label lb_x179_ld_exhaust_fan_run;
        private System.Windows.Forms.Panel pn_Loader_IN4_X179;
        private System.Windows.Forms.Label lb_x178_ld_inspiration_fan_run;
        private System.Windows.Forms.Panel pn_Loader_IN4_X178;
        private System.Windows.Forms.Label lb_x177_6_ld_fan_stop_alarm;
        private System.Windows.Forms.Panel pn_Loader_IN4_X177;
        private System.Windows.Forms.Label lb_x176_5_ld_fan_stop_alarm;
        private System.Windows.Forms.Panel pn_Loader_IN4_X176;
        private System.Windows.Forms.Label lb_x175_4_ld_fan_stop_alarm;
        private System.Windows.Forms.Panel pn_Loader_IN4_X175;
        private System.Windows.Forms.Label lb_x174_3_ld_fan_stop_alarm;
        private System.Windows.Forms.Panel pn_Loader_IN4_X174;
        private System.Windows.Forms.Label lb_x173_2_ld_fan_stop_alarm;
        private System.Windows.Forms.Panel pn_Loader_IN4_X173;
        private System.Windows.Forms.Label lb_x172_1_ld_fan_stop_alarm;
        private System.Windows.Forms.Panel pn_Loader_IN4_X172;
        private System.Windows.Forms.Label lb_x171_motion_off_timer;
        private System.Windows.Forms.Panel pn_Loader_IN4_X171;
        private System.Windows.Forms.Label lb_x170_2_ld_light_curtain;
        private System.Windows.Forms.Panel pn_Loader_IN4_X170;
        private System.Windows.Forms.Panel pn_Loader_IN4_X16F;
        private System.Windows.Forms.Label lb_x16e_spare_cylinder_sensor;
        private System.Windows.Forms.Panel pn_Loader_IN4_X16E;
        private System.Windows.Forms.Label lb_x16d_spare_cylinder_sensor;
        private System.Windows.Forms.Panel pn_Loader_IN4_X16D;
        private System.Windows.Forms.Label lb_x16c_spare_cylinder_sensor;
        private System.Windows.Forms.Panel pn_Loader_IN4_X16C;
        private System.Windows.Forms.Label lb_x16b_spare_cylinder_sensor;
        private System.Windows.Forms.Panel pn_Loader_IN4_X16B;
        private System.Windows.Forms.Label lb_x16a_spare_cylinder_sensor;
        private System.Windows.Forms.Panel pn_Loader_IN4_X16A;
        private System.Windows.Forms.Label lb_x169_spare_cylinder_sensor;
        private System.Windows.Forms.Panel pn_Loader_IN4_X169;
        private System.Windows.Forms.Label lb_x168_spare_cylinder_sensor;
        private System.Windows.Forms.Panel pn_Loader_IN4_X168;
        private System.Windows.Forms.Label lb_x167_spare_cylinder_sensor;
        private System.Windows.Forms.Panel pn_Loader_IN4_X167;
        private System.Windows.Forms.Label lb_x166_3_main_pressure_sw;
        private System.Windows.Forms.Panel pn_Loader_IN4_X166;
        private System.Windows.Forms.Label lb_x165_2_main_pressure_sw;
        private System.Windows.Forms.Panel pn_Loader_IN4_X165;
        private System.Windows.Forms.Label lb_x164_1_main_pressure_sw;
        private System.Windows.Forms.Panel pn_Loader_IN4_X164;
        private System.Windows.Forms.Label lb_x163_2_ld_trans_2ch_pressure_sw;
        private System.Windows.Forms.Panel pn_Loader_IN4_X163;
        private System.Windows.Forms.Label lb_x162_2_ld_trans_1ch_pressure_sw;
        private System.Windows.Forms.Panel pn_Loader_IN4_X162;
        private System.Windows.Forms.Label lb_x161_1_ld_trans_2ch_pressure_sw;
        private System.Windows.Forms.Panel pn_Loader_IN4_X161;
        private System.Windows.Forms.Label lb_x160_1_ld_trans_1ch_pressure_sw;
        private System.Windows.Forms.Panel pn_Loader_IN4_X160;
        private System.Windows.Forms.Label lb_x17f_5_process_fan_stop_alarm;
        private System.Windows.Forms.Label lb_x16f_1_ld_light_curtain;
        private System.Windows.Forms.Label lb_y1ff_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT4_Y1FF;
        private System.Windows.Forms.Label lb_y1fe_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT4_Y1FE;
        private System.Windows.Forms.Label lb_y1fd_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT4_Y1FD;
        private System.Windows.Forms.Label lb_y1fc_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT4_Y1FC;
        private System.Windows.Forms.Label lb_y1fb_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT4_Y1FB;
        private System.Windows.Forms.Label lb_y1fa_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT4_Y1FA;
        private System.Windows.Forms.Label lb_y1f9_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT4_Y1F9;
        private System.Windows.Forms.Label lb_y1f8_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT4_Y1F8;
        private System.Windows.Forms.Label lb_y1f7_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT4_Y1F7;
        private System.Windows.Forms.Label lb_y1f6_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT4_Y1F6;
        private System.Windows.Forms.Label lb_y1f5_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT4_Y1F5;
        private System.Windows.Forms.Label lb_y1f4_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT4_Y1F4;
        private System.Windows.Forms.Label lb_y1f3_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT4_Y1F3;
        private System.Windows.Forms.Label lb_y1f2_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT4_Y1F2;
        private System.Windows.Forms.Label lb_y1f1_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT4_Y1F1;
        private System.Windows.Forms.Label lb_y1f0_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT4_Y1F0;
        private System.Windows.Forms.Label lb_y1ef_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT4_Y1EF;
        private System.Windows.Forms.Label lb_y1ee_load_muting_down;
        private System.Windows.Forms.Panel pn_Loader_OUT4_Y1EE;
        private System.Windows.Forms.Label lb_y1ed_load_muting_up;
        private System.Windows.Forms.Panel pn_Loader_OUT4_Y1ED;
        private System.Windows.Forms.Label lb_y1ec_spare;
        private System.Windows.Forms.Panel pn_Loader_OUT4_Y1EC;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Panel pn_Loader_OUT4_Y1EB;
        private System.Windows.Forms.Label lb_y1ea_4_1_ld_lifter_muting_onoff;
        private System.Windows.Forms.Panel pn_Loader_OUT4_Y1EA;
        private System.Windows.Forms.Label lb_y1e9_3_2_ld_lifter_muting_onoff;
        private System.Windows.Forms.Panel pn_Loader_OUT4_Y1E9;
        private System.Windows.Forms.Label lb_y1e8_3_1_ld_lifter_muting_onoff;
        private System.Windows.Forms.Panel pn_Loader_OUT4_Y1E8;
        private System.Windows.Forms.Label lb_y1e7_2_2_ld_lifter_muting_onoff;
        private System.Windows.Forms.Panel pn_Loader_OUT4_Y1E7;
        private System.Windows.Forms.Label lb_y1e6_2_1_ld_lifter_muting_onoff;
        private System.Windows.Forms.Panel pn_Loader_OUT4_Y1E6;
        private System.Windows.Forms.Label lb_y1e5_1_2_ld_lifter_muting_onoff;
        private System.Windows.Forms.Panel pn_Loader_OUT4_Y1E5;
        private System.Windows.Forms.Label lb_y1e4_1_1_ld_lifter_muting_onoff;
        private System.Windows.Forms.Panel pn_Loader_OUT4_Y1E4;
        private System.Windows.Forms.Label lb_y1e3_2_2_ld_out_muting_onoff;
        private System.Windows.Forms.Panel pn_Loader_OUT4_Y1E3;
        private System.Windows.Forms.Label lb_y1e2_2_1_ld_out_muting_onoff;
        private System.Windows.Forms.Panel pn_Loader_OUT4_Y1E2;
        private System.Windows.Forms.Label lb_y1e1_1_2_ld_in_muting_onoff;
        private System.Windows.Forms.Panel pn_Loader_OUT4_Y1E1;
        private System.Windows.Forms.Label lb_y1e0_1_1_ld_in_muting_onoff;
        private System.Windows.Forms.Panel pn_Loader_OUT4_Y1E0;
        private System.Windows.Forms.TabControl tc_iostatus_process_out;
        private System.Windows.Forms.TabPage tp_iostatus_process_out1;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Panel panel131;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.Panel panel132;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.Panel panel133;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.Panel panel134;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.Panel panel135;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.Panel panel136;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.Panel panel137;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.Panel panel138;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.Panel panel139;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.Panel panel140;
        private System.Windows.Forms.Label label141;
        private System.Windows.Forms.Panel panel141;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.Panel panel142;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.Panel panel143;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.Panel panel144;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.Panel panel145;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.Panel panel146;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.Panel panel147;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.Panel panel148;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.Panel panel149;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.Panel panel150;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.Panel panel151;
        private System.Windows.Forms.Label label152;
        private System.Windows.Forms.Panel panel152;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.Panel panel153;
        private System.Windows.Forms.Label label154;
        private System.Windows.Forms.Panel panel154;
        private System.Windows.Forms.Label label155;
        private System.Windows.Forms.Panel panel155;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.Panel panel156;
        private System.Windows.Forms.Label label157;
        private System.Windows.Forms.Panel panel157;
        private System.Windows.Forms.Label label158;
        private System.Windows.Forms.Panel panel158;
        private System.Windows.Forms.Label label159;
        private System.Windows.Forms.Panel panel159;
        private System.Windows.Forms.Label lb_y322_laser_emergency_stop;
        private System.Windows.Forms.Panel panel_y322_laser_emergency_stop;
        private System.Windows.Forms.Label lb_y321_laser_stop_external_interlock;
        private System.Windows.Forms.Panel panel_y321_laser_stop_external_interlock;
        private System.Windows.Forms.Label lb_y320_laser_stop_internal_interlock;
        private System.Windows.Forms.Panel panel_y320_laser_stop_internal_interlock;
        private System.Windows.Forms.TabPage tp_iostatus_process_out2;
        private System.Windows.Forms.Label label163;
        private System.Windows.Forms.Panel panel163;
        private System.Windows.Forms.Label label164;
        private System.Windows.Forms.Panel panel164;
        private System.Windows.Forms.Label label165;
        private System.Windows.Forms.Panel panel165;
        private System.Windows.Forms.Label label166;
        private System.Windows.Forms.Panel panel166;
        private System.Windows.Forms.Label label167;
        private System.Windows.Forms.Panel panel167;
        private System.Windows.Forms.Label label168;
        private System.Windows.Forms.Panel panel168;
        private System.Windows.Forms.Label label169;
        private System.Windows.Forms.Panel panel169;
        private System.Windows.Forms.Label label170;
        private System.Windows.Forms.Panel panel170;
        private System.Windows.Forms.Label label171;
        private System.Windows.Forms.Panel panel171;
        private System.Windows.Forms.Label label172;
        private System.Windows.Forms.Panel panel172;
        private System.Windows.Forms.Label label173;
        private System.Windows.Forms.Panel panel173;
        private System.Windows.Forms.Label label174;
        private System.Windows.Forms.Panel panel174;
        private System.Windows.Forms.Label label175;
        private System.Windows.Forms.Panel panel175;
        private System.Windows.Forms.Label label176;
        private System.Windows.Forms.Panel panel176;
        private System.Windows.Forms.Label label177;
        private System.Windows.Forms.Panel panel177;
        private System.Windows.Forms.Label label178;
        private System.Windows.Forms.Panel panel178;
        private System.Windows.Forms.Label label179;
        private System.Windows.Forms.Panel panel179;
        private System.Windows.Forms.Label label180;
        private System.Windows.Forms.Panel panel180;
        private System.Windows.Forms.Label label181;
        private System.Windows.Forms.Panel panel181;
        private System.Windows.Forms.Label label182;
        private System.Windows.Forms.Panel panel182;
        private System.Windows.Forms.Label label183;
        private System.Windows.Forms.Panel panel183;
        private System.Windows.Forms.Label label184;
        private System.Windows.Forms.Panel panel184;
        private System.Windows.Forms.Label label185;
        private System.Windows.Forms.Panel panel185;
        private System.Windows.Forms.Label label186;
        private System.Windows.Forms.Panel panel186;
        private System.Windows.Forms.Label label187;
        private System.Windows.Forms.Panel panel187;
        private System.Windows.Forms.Label label188;
        private System.Windows.Forms.Panel panel188;
        private System.Windows.Forms.Label label189;
        private System.Windows.Forms.Panel panel189;
        private System.Windows.Forms.Label label190;
        private System.Windows.Forms.Panel panel190;
        private System.Windows.Forms.Label label191;
        private System.Windows.Forms.Panel panel191;
        private System.Windows.Forms.Label label192;
        private System.Windows.Forms.Panel panel192;
        private System.Windows.Forms.Label label193;
        private System.Windows.Forms.Panel panel193;
        private System.Windows.Forms.Label label194;
        private System.Windows.Forms.Panel panel194;
        private System.Windows.Forms.TabPage tp_iostatus_process_out3;
        private System.Windows.Forms.Label label195;
        private System.Windows.Forms.Panel panel195;
        private System.Windows.Forms.Label label196;
        private System.Windows.Forms.Panel panel196;
        private System.Windows.Forms.Label label197;
        private System.Windows.Forms.Panel panel197;
        private System.Windows.Forms.Label label198;
        private System.Windows.Forms.Panel panel198;
        private System.Windows.Forms.Label label199;
        private System.Windows.Forms.Panel panel199;
        private System.Windows.Forms.Label label200;
        private System.Windows.Forms.Panel panel200;
        private System.Windows.Forms.Label label201;
        private System.Windows.Forms.Panel panel201;
        private System.Windows.Forms.Label label202;
        private System.Windows.Forms.Panel panel202;
        private System.Windows.Forms.Label label203;
        private System.Windows.Forms.Panel panel203;
        private System.Windows.Forms.Label label204;
        private System.Windows.Forms.Panel panel204;
        private System.Windows.Forms.Label label205;
        private System.Windows.Forms.Panel panel205;
        private System.Windows.Forms.Label label206;
        private System.Windows.Forms.Panel panel206;
        private System.Windows.Forms.Label label207;
        private System.Windows.Forms.Panel panel207;
        private System.Windows.Forms.Label label208;
        private System.Windows.Forms.Panel panel208;
        private System.Windows.Forms.Label label209;
        private System.Windows.Forms.Panel panel209;
        private System.Windows.Forms.Label label210;
        private System.Windows.Forms.Panel panel210;
        private System.Windows.Forms.Label label211;
        private System.Windows.Forms.Panel panel211;
        private System.Windows.Forms.Label label212;
        private System.Windows.Forms.Panel panel212;
        private System.Windows.Forms.Label label213;
        private System.Windows.Forms.Panel panel213;
        private System.Windows.Forms.Label label214;
        private System.Windows.Forms.Panel panel214;
        private System.Windows.Forms.Label label215;
        private System.Windows.Forms.Panel panel215;
        private System.Windows.Forms.Label label216;
        private System.Windows.Forms.Panel panel216;
        private System.Windows.Forms.Label label217;
        private System.Windows.Forms.Panel panel217;
        private System.Windows.Forms.Label label218;
        private System.Windows.Forms.Panel panel218;
        private System.Windows.Forms.Label label219;
        private System.Windows.Forms.Panel panel219;
        private System.Windows.Forms.Label label220;
        private System.Windows.Forms.Panel panel220;
        private System.Windows.Forms.Label label221;
        private System.Windows.Forms.Panel panel221;
        private System.Windows.Forms.Label label222;
        private System.Windows.Forms.Panel panel222;
        private System.Windows.Forms.Label label223;
        private System.Windows.Forms.Panel panel223;
        private System.Windows.Forms.Label label224;
        private System.Windows.Forms.Panel panel224;
        private System.Windows.Forms.Label label225;
        private System.Windows.Forms.Panel panel225;
        private System.Windows.Forms.Label label226;
        private System.Windows.Forms.Panel panel226;
        private System.Windows.Forms.TabControl tc_iostatus_process_in;
        private System.Windows.Forms.TabPage tp_iostatus_process_in1;
        private System.Windows.Forms.Label lb_x31f_1_breaking_stage_brush_down;
        private System.Windows.Forms.Panel pn_Process_IN1_X31F;
        private System.Windows.Forms.Label lb_x31e_2_breaking_stage_brush_up;
        private System.Windows.Forms.Panel pn_Process_IN1_X31E;
        private System.Windows.Forms.Label lb_x31d_1_breaking_stage_brush_down;
        private System.Windows.Forms.Panel pn_Process_IN1_X31D;
        private System.Windows.Forms.Label lb_x31c_1_breaking_stage_brush_up;
        private System.Windows.Forms.Panel pn_Process_IN1_X31C;
        private System.Windows.Forms.Label lb_x31b_2_after_ircut_trans_down;
        private System.Windows.Forms.Panel pn_Process_IN1_X31B;
        private System.Windows.Forms.Label lb_x31a_2_after_ircut_trans_up;
        private System.Windows.Forms.Panel pn_Process_IN1_X31A;
        private System.Windows.Forms.Label lb_x319_1_after_ircut_trans_down;
        private System.Windows.Forms.Panel pn_Process_IN1_X319;
        private System.Windows.Forms.Label lb_x318_1_after_ircut_trans_up;
        private System.Windows.Forms.Panel pn_Process_IN1_X318;
        private System.Windows.Forms.Label lb_x317_2_laser_stage_brush_down;
        private System.Windows.Forms.Panel pn_Process_IN1_X317;
        private System.Windows.Forms.Label lb_x316_2_laser_stage_brush_up;
        private System.Windows.Forms.Panel pn_Process_IN1_X316;
        private System.Windows.Forms.Label lb_x315_1_laser_stage_brush_down;
        private System.Windows.Forms.Panel pn_Process_IN1_X315;
        private System.Windows.Forms.Label lb_x314_1_laser_stage_brush_up;
        private System.Windows.Forms.Panel pn_Process_IN1_X314;
        private System.Windows.Forms.Label lb_x313_laser_shutter_close;
        private System.Windows.Forms.Panel pn_Process_IN1_X313;
        private System.Windows.Forms.Label lb_x312_laser_shutter_open;
        private System.Windows.Forms.Panel pn_Process_IN1_X312;
        private System.Windows.Forms.Label lb_x311_2_2_breaking_stage_cell_detect;
        private System.Windows.Forms.Panel pn_Process_IN1_X311;
        private System.Windows.Forms.Label lb_x310_2_1_breaking_stage_cell_detect;
        private System.Windows.Forms.Panel pn_Process_IN1_X310;
        private System.Windows.Forms.Label lb_x30f_1_2_breaking_stage_cell_detect;
        private System.Windows.Forms.Panel pn_Process_IN1_X30F;
        private System.Windows.Forms.Label lb_x30e_1_1_breaking_stage_cell_detect;
        private System.Windows.Forms.Panel pn_Process_IN1_X30E;
        private System.Windows.Forms.Label lb_x30d_2_2_laser_stage_cell_detect;
        private System.Windows.Forms.Panel pn_Process_IN1_X30D;
        private System.Windows.Forms.Label lb_x30c_2_1_laser_stage_cell_detect;
        private System.Windows.Forms.Panel pn_Process_IN1_X30C;
        private System.Windows.Forms.Label lb_x30b_1_2_laser_stage_cell_detect;
        private System.Windows.Forms.Panel pn_Process_IN1_X30B;
        private System.Windows.Forms.Label lb_x30a_1_1_laser_stage_cell_detect;
        private System.Windows.Forms.Panel pn_Process_IN1_X30A;
        private System.Windows.Forms.Label lb_x309_leak_sensor;
        private System.Windows.Forms.Panel pn_Process_IN1_X309;
        private System.Windows.Forms.Label lb_x308_laser_cover_interlock;
        private System.Windows.Forms.Panel pn_Process_IN1_X308;
        private System.Windows.Forms.Label lb_x307_laser_diodes_onoff_2;
        private System.Windows.Forms.Panel pn_Process_IN1_X307;
        private System.Windows.Forms.Label lb_x306_laser_interlock_error_2;
        private System.Windows.Forms.Panel pn_Process_IN1_X306;
        private System.Windows.Forms.Label lb_x305_laser_shutter_position_2;
        private System.Windows.Forms.Panel pn_Process_IN1_X305;
        private System.Windows.Forms.Label lb_x304_laser_interlock_error_1;
        private System.Windows.Forms.Panel pn_Process_IN1_X304;
        private System.Windows.Forms.Label lb__x303_laser_shutter_position_1;
        private System.Windows.Forms.Panel pn_Process_IN1_X303;
        private System.Windows.Forms.Label lb_x302_laser_diodes_onoff_1;
        private System.Windows.Forms.Panel pn_Process_IN1_X302;
        private System.Windows.Forms.Label lb_x301_laser_on_feedback;
        private System.Windows.Forms.Panel pn_Process_IN1_X301;
        private System.Windows.Forms.Label lb_x300_laser_alarm;
        private System.Windows.Forms.Panel pn_Process_IN1_X300;
        private System.Windows.Forms.TabPage tp_iostatus_process_in2;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Panel panel38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Panel panel39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Panel panel40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Panel panel41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Panel panel42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Panel panel43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Panel panel44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Panel panel45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Panel panel46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Panel panel47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Panel panel48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Panel panel49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Panel panel50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Panel panel51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Panel panel52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Panel panel53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Panel panel54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Panel panel55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Panel panel56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Panel panel57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Panel panel58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Panel panel59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Panel panel60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Panel panel61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Panel panel62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Panel panel63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Panel panel64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Panel panel65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Panel panel66;
        private System.Windows.Forms.TabPage tp_iostatus_process_in3;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Panel panel67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Panel panel68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Panel panel69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Panel panel70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Panel panel71;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Panel panel72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Panel panel73;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Panel panel74;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Panel panel75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Panel panel76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Panel panel77;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Panel panel78;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Panel panel79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Panel panel80;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Panel panel81;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Panel panel82;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Panel panel83;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Panel panel84;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Panel panel85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Panel panel86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Panel panel87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Panel panel88;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Panel panel89;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Panel panel90;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Panel panel91;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Panel panel92;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Panel panel93;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Panel panel94;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Panel panel95;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Panel panel96;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Panel panel97;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Panel panel98;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label label256;
        private System.Windows.Forms.Panel panel257;
        private System.Windows.Forms.Label label257;
        private System.Windows.Forms.Panel panel258;
        private System.Windows.Forms.Label label258;
        private System.Windows.Forms.Panel panel259;
        private System.Windows.Forms.Label label259;
        private System.Windows.Forms.Panel panel260;
        private System.Windows.Forms.Label label260;
        private System.Windows.Forms.Panel panel261;
        private System.Windows.Forms.Label label261;
        private System.Windows.Forms.Panel panel262;
        private System.Windows.Forms.Label label262;
        private System.Windows.Forms.Panel panel263;
        private System.Windows.Forms.Label label263;
        private System.Windows.Forms.Panel panel264;
        private System.Windows.Forms.Label label264;
        private System.Windows.Forms.Panel panel265;
        private System.Windows.Forms.Label label265;
        private System.Windows.Forms.Panel panel266;
        private System.Windows.Forms.Label label266;
        private System.Windows.Forms.Panel panel267;
        private System.Windows.Forms.Label label267;
        private System.Windows.Forms.Panel panel268;
        private System.Windows.Forms.Label label268;
        private System.Windows.Forms.Panel panel269;
        private System.Windows.Forms.Label label269;
        private System.Windows.Forms.Panel panel270;
        private System.Windows.Forms.Label label270;
        private System.Windows.Forms.Panel panel271;
        private System.Windows.Forms.Label label271;
        private System.Windows.Forms.Panel panel272;
        private System.Windows.Forms.Label label272;
        private System.Windows.Forms.Panel panel273;
        private System.Windows.Forms.Label label273;
        private System.Windows.Forms.Panel panel274;
        private System.Windows.Forms.Label label274;
        private System.Windows.Forms.Panel panel275;
        private System.Windows.Forms.Label label275;
        private System.Windows.Forms.Panel panel276;
        private System.Windows.Forms.Label label276;
        private System.Windows.Forms.Panel panel277;
        private System.Windows.Forms.Label label277;
        private System.Windows.Forms.Panel panel278;
        private System.Windows.Forms.Label label278;
        private System.Windows.Forms.Panel panel279;
        private System.Windows.Forms.Label label279;
        private System.Windows.Forms.Panel panel280;
        private System.Windows.Forms.Label label280;
        private System.Windows.Forms.Panel panel281;
        private System.Windows.Forms.Label label281;
        private System.Windows.Forms.Panel panel282;
        private System.Windows.Forms.Label label282;
        private System.Windows.Forms.Panel panel283;
        private System.Windows.Forms.Label label283;
        private System.Windows.Forms.Panel panel284;
        private System.Windows.Forms.Label label284;
        private System.Windows.Forms.Panel panel285;
        private System.Windows.Forms.Label label285;
        private System.Windows.Forms.Panel panel286;
        private System.Windows.Forms.Label label286;
        private System.Windows.Forms.Panel panel287;
        private System.Windows.Forms.Label label287;
        private System.Windows.Forms.Panel panel288;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Label label288;
        private System.Windows.Forms.Panel panel289;
        private System.Windows.Forms.Label label289;
        private System.Windows.Forms.Panel panel290;
        private System.Windows.Forms.Label label290;
        private System.Windows.Forms.Panel panel291;
        private System.Windows.Forms.Label label291;
        private System.Windows.Forms.Panel panel292;
        private System.Windows.Forms.Label label292;
        private System.Windows.Forms.Panel panel293;
        private System.Windows.Forms.Label label293;
        private System.Windows.Forms.Panel panel294;
        private System.Windows.Forms.Label label294;
        private System.Windows.Forms.Panel panel295;
        private System.Windows.Forms.Label label295;
        private System.Windows.Forms.Panel panel296;
        private System.Windows.Forms.Label label296;
        private System.Windows.Forms.Panel panel297;
        private System.Windows.Forms.Label label297;
        private System.Windows.Forms.Panel panel298;
        private System.Windows.Forms.Label label298;
        private System.Windows.Forms.Panel panel299;
        private System.Windows.Forms.Label label299;
        private System.Windows.Forms.Panel panel300;
        private System.Windows.Forms.Label label300;
        private System.Windows.Forms.Panel panel301;
        private System.Windows.Forms.Label label301;
        private System.Windows.Forms.Panel panel302;
        private System.Windows.Forms.Label label302;
        private System.Windows.Forms.Panel panel303;
        private System.Windows.Forms.Label label303;
        private System.Windows.Forms.Panel panel304;
        private System.Windows.Forms.Label label304;
        private System.Windows.Forms.Panel panel305;
        private System.Windows.Forms.Label label305;
        private System.Windows.Forms.Panel panel306;
        private System.Windows.Forms.Label label306;
        private System.Windows.Forms.Panel panel307;
        private System.Windows.Forms.Label label307;
        private System.Windows.Forms.Panel panel308;
        private System.Windows.Forms.Label label308;
        private System.Windows.Forms.Panel panel309;
        private System.Windows.Forms.Label label309;
        private System.Windows.Forms.Panel panel310;
        private System.Windows.Forms.Label label310;
        private System.Windows.Forms.Panel panel311;
        private System.Windows.Forms.Label label311;
        private System.Windows.Forms.Panel panel312;
        private System.Windows.Forms.Label label312;
        private System.Windows.Forms.Panel panel313;
        private System.Windows.Forms.Label label313;
        private System.Windows.Forms.Panel panel314;
        private System.Windows.Forms.Label label314;
        private System.Windows.Forms.Panel panel315;
        private System.Windows.Forms.Label label315;
        private System.Windows.Forms.Panel panel316;
        private System.Windows.Forms.Label label316;
        private System.Windows.Forms.Panel panel317;
        private System.Windows.Forms.Label label317;
        private System.Windows.Forms.Panel panel318;
        private System.Windows.Forms.Label label318;
        private System.Windows.Forms.Panel panel319;
        private System.Windows.Forms.Label label319;
        private System.Windows.Forms.Panel panel320;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.Label label320;
        private System.Windows.Forms.Panel panel321;
        private System.Windows.Forms.Label label321;
        private System.Windows.Forms.Panel panel322;
        private System.Windows.Forms.Label label322;
        private System.Windows.Forms.Panel panel323;
        private System.Windows.Forms.Label label323;
        private System.Windows.Forms.Panel panel324;
        private System.Windows.Forms.Label label324;
        private System.Windows.Forms.Panel panel325;
        private System.Windows.Forms.Label label325;
        private System.Windows.Forms.Panel panel326;
        private System.Windows.Forms.Label label326;
        private System.Windows.Forms.Panel panel327;
        private System.Windows.Forms.Label label327;
        private System.Windows.Forms.Panel panel328;
        private System.Windows.Forms.Label label328;
        private System.Windows.Forms.Panel panel329;
        private System.Windows.Forms.Label label329;
        private System.Windows.Forms.Panel panel330;
        private System.Windows.Forms.Label label330;
        private System.Windows.Forms.Panel panel331;
        private System.Windows.Forms.Label label331;
        private System.Windows.Forms.Panel panel332;
        private System.Windows.Forms.Label label332;
        private System.Windows.Forms.Panel panel333;
        private System.Windows.Forms.Label label333;
        private System.Windows.Forms.Panel panel334;
        private System.Windows.Forms.Label label334;
        private System.Windows.Forms.Panel panel335;
        private System.Windows.Forms.Label label335;
        private System.Windows.Forms.Panel panel336;
        private System.Windows.Forms.Label label336;
        private System.Windows.Forms.Panel panel337;
        private System.Windows.Forms.Label label337;
        private System.Windows.Forms.Panel panel338;
        private System.Windows.Forms.Label label338;
        private System.Windows.Forms.Panel panel339;
        private System.Windows.Forms.Label label339;
        private System.Windows.Forms.Panel panel340;
        private System.Windows.Forms.Label label340;
        private System.Windows.Forms.Panel panel341;
        private System.Windows.Forms.Label label341;
        private System.Windows.Forms.Panel panel342;
        private System.Windows.Forms.Label label342;
        private System.Windows.Forms.Panel panel343;
        private System.Windows.Forms.Label label343;
        private System.Windows.Forms.Panel panel344;
        private System.Windows.Forms.Label label344;
        private System.Windows.Forms.Panel panel345;
        private System.Windows.Forms.Label label345;
        private System.Windows.Forms.Panel panel346;
        private System.Windows.Forms.Label label346;
        private System.Windows.Forms.Panel panel347;
        private System.Windows.Forms.Label label347;
        private System.Windows.Forms.Panel panel348;
        private System.Windows.Forms.Label label348;
        private System.Windows.Forms.Panel panel349;
        private System.Windows.Forms.Label label349;
        private System.Windows.Forms.Panel panel350;
        private System.Windows.Forms.Label label350;
        private System.Windows.Forms.Panel panel351;
        private System.Windows.Forms.Label label351;
        private System.Windows.Forms.Panel panel352;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private Moter_In_Out.InOutTotalControl inOutTotalControl1;
    }
}
