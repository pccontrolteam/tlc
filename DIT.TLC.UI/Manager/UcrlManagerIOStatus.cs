﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DIT.TLC.UI
{
    public partial class Manager_IOStatus : UserControl
    {
        public string LD_IN01 = "";
        public string LD_IN02 = "";
        public string LD_IN03 = "";
        public string LD_IN04 = "";

        public string LD_OUT01 = "";
        public string LD_OUT02 = "";
        public string LD_OUT03 = "";
        public string LD_OUT04 = "";


        public string PROC_IN01 = "";
        public string PROC_IN02 = "";
        public string PROC_IN03 = "";
        public string PROC_IN04 = "";

        public string PROC_OUT01 = "";
        public string PROC_OUT02 = "";
        public string PROC_OUT03 = "";
        public string PROC_OUT04 = "";

        public string ULD_IN01 = "";
        public string ULD_IN02 = "";
        public string ULD_IN03 = "";
        public string ULD_IN04 = "";

        public string ULD_OUT01 = "";
        public string ULD_OUT02 = "";
        public string ULD_OUT03 = "";
        public string ULD_OUT04 = "";



        public Manager_IOStatus()
        {
            InitializeComponent();

        }
        private void btnMonitorIO_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            btnMonitorLoaderIO.BackColor = btnMonitorLoaderIO == btn ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            btnMonitorProcessIO.BackColor = btnMonitorProcessIO == btn ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            btnMonitorUnloaderIO.BackColor = btnMonitorUnloaderIO == btn ? UiGlobal.SET_C : UiGlobal.UNSET_C;


            btnInIO01_Click(btnInIO01, null);
            btnOutIO01_Click(btnOutIO01, null);


        }

        private void btnInIO01_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            btnInIO01.BackColor = btnInIO01 == btn ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            btnInIO02.BackColor = btnInIO02 == btn ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            btnInIO03.BackColor = btnInIO03 == btn ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            btnInIO04.BackColor = btnInIO04 == btn ? UiGlobal.SET_C : UiGlobal.UNSET_C;

            string address = string.Empty;

            if (btnMonitorLoaderIO.BackColor == UiGlobal.SET_C)
            {
                address = btnInIO01 == btn ? LD_IN01 :
                          btnInIO02 == btn ? LD_IN02 :
                          btnInIO03 == btn ? LD_IN03 :
                          btnInIO04 == btn ? LD_IN04 : "";

            }
            else if (btnMonitorProcessIO.BackColor == UiGlobal.SET_C)
            {
                address = btnInIO01 == btn ? PROC_IN01 :
                          btnInIO02 == btn ? PROC_IN02 :
                          btnInIO03 == btn ? PROC_IN03 :
                          btnInIO04 == btn ? PROC_IN04 : "";
            }
            else if (btnMonitorUnloaderIO.BackColor == UiGlobal.SET_C)
            {
                address = btnInIO01 == btn ? ULD_IN01 :
                          btnInIO02 == btn ? ULD_IN02 :
                          btnInIO03 == btn ? ULD_IN03 :
                          btnInIO04 == btn ? ULD_IN04 : "";
            }
            inOutTotalControlIn.SetAddress(address);
        }

        private void btnOutIO01_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            btnOutIO01.BackColor = btnOutIO01 == btn ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            btnOutIO02.BackColor = btnOutIO02 == btn ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            btnOutIO03.BackColor = btnOutIO03 == btn ? UiGlobal.SET_C : UiGlobal.UNSET_C;
            btnOutIO04.BackColor = btnOutIO04 == btn ? UiGlobal.SET_C : UiGlobal.UNSET_C;

            string address = string.Empty;

            if (btnMonitorLoaderIO.BackColor == UiGlobal.SET_C)
            {
                address = btnOutIO01 == btn ? LD_OUT01 :
                          btnOutIO02 == btn ? LD_OUT02 :
                          btnOutIO03 == btn ? LD_OUT03 :
                          btnOutIO04 == btn ? LD_OUT04 : "";

            }
            else if (btnMonitorProcessIO.BackColor == UiGlobal.SET_C)
            {
                address = btnOutIO01 == btn ? PROC_OUT01 :
                          btnOutIO02 == btn ? PROC_OUT02 :
                          btnOutIO03 == btn ? PROC_OUT03 :
                          btnOutIO04 == btn ? PROC_OUT04 : "";
            }
            else if (btnMonitorUnloaderIO.BackColor == UiGlobal.SET_C)
            {
                address = btnOutIO01 == btn ? ULD_OUT01 :
                          btnOutIO02 == btn ? ULD_OUT02 :
                          btnOutIO03 == btn ? ULD_OUT03 :
                          btnOutIO04 == btn ? ULD_OUT04 : "";
            }
            inOutTotalControlOut.SetAddress(address);
        }

        public void UpdateUI()
        {
            inOutTotalControlIn.UpdateUi();
            inOutTotalControlOut.UpdateUi();
        }
    }
}