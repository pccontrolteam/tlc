﻿namespace DIT.TLC.UI
{
    partial class Manager_TactTime
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listTactView05 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView04 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView03 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView02 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView01 = new DIT.TLC.UI.UctrlLstTactView();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnMonitorLoaderIO = new System.Windows.Forms.Button();
            this.btnMonitorProcessIO = new System.Windows.Forms.Button();
            this.btnMonitorUnloaderIO = new System.Windows.Forms.Button();
            this.listTactView07 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView08 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView06 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView09 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView10 = new DIT.TLC.UI.UctrlLstTactView();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listTactView05
            // 
            this.listTactView05.BackColor = System.Drawing.Color.DimGray;
            this.listTactView05.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView05.Location = new System.Drawing.Point(1389, 46);
            this.listTactView05.Name = "listTactView05";
            this.listTactView05.Size = new System.Drawing.Size(343, 420);
            this.listTactView05.TabIndex = 15;
            // 
            // listTactView04
            // 
            this.listTactView04.BackColor = System.Drawing.Color.DimGray;
            this.listTactView04.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView04.Location = new System.Drawing.Point(1041, 46);
            this.listTactView04.Name = "listTactView04";
            this.listTactView04.Size = new System.Drawing.Size(343, 420);
            this.listTactView04.TabIndex = 14;
            // 
            // listTactView03
            // 
            this.listTactView03.BackColor = System.Drawing.Color.DimGray;
            this.listTactView03.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView03.Location = new System.Drawing.Point(693, 46);
            this.listTactView03.Name = "listTactView03";
            this.listTactView03.Size = new System.Drawing.Size(343, 420);
            this.listTactView03.TabIndex = 13;
            // 
            // listTactView02
            // 
            this.listTactView02.BackColor = System.Drawing.Color.DimGray;
            this.listTactView02.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView02.Location = new System.Drawing.Point(344, 46);
            this.listTactView02.Name = "listTactView02";
            this.listTactView02.Size = new System.Drawing.Size(343, 420);
            this.listTactView02.TabIndex = 12;
            // 
            // listTactView01
            // 
            this.listTactView01.BackColor = System.Drawing.Color.DimGray;
            this.listTactView01.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView01.Location = new System.Drawing.Point(3, 46);
            this.listTactView01.Name = "listTactView01";
            this.listTactView01.Size = new System.Drawing.Size(343, 420);
            this.listTactView01.TabIndex = 11;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnMonitorLoaderIO);
            this.flowLayoutPanel1.Controls.Add(this.btnMonitorProcessIO);
            this.flowLayoutPanel1.Controls.Add(this.btnMonitorUnloaderIO);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1740, 47);
            this.flowLayoutPanel1.TabIndex = 3;
            // 
            // btnMonitorLoaderIO
            // 
            this.btnMonitorLoaderIO.Location = new System.Drawing.Point(3, 3);
            this.btnMonitorLoaderIO.Name = "btnMonitorLoaderIO";
            this.btnMonitorLoaderIO.Size = new System.Drawing.Size(244, 41);
            this.btnMonitorLoaderIO.TabIndex = 0;
            this.btnMonitorLoaderIO.Text = "로더";
            this.btnMonitorLoaderIO.UseVisualStyleBackColor = true;
            this.btnMonitorLoaderIO.Click += new System.EventHandler(this.btnMonitorLoaderIO_Click);
            // 
            // btnMonitorProcessIO
            // 
            this.btnMonitorProcessIO.Location = new System.Drawing.Point(253, 3);
            this.btnMonitorProcessIO.Name = "btnMonitorProcessIO";
            this.btnMonitorProcessIO.Size = new System.Drawing.Size(244, 41);
            this.btnMonitorProcessIO.TabIndex = 1;
            this.btnMonitorProcessIO.Text = "프로세스";
            this.btnMonitorProcessIO.UseVisualStyleBackColor = true;
            this.btnMonitorProcessIO.Click += new System.EventHandler(this.btnMonitorProcessIO_Click);
            // 
            // btnMonitorUnloaderIO
            // 
            this.btnMonitorUnloaderIO.Location = new System.Drawing.Point(503, 3);
            this.btnMonitorUnloaderIO.Name = "btnMonitorUnloaderIO";
            this.btnMonitorUnloaderIO.Size = new System.Drawing.Size(244, 41);
            this.btnMonitorUnloaderIO.TabIndex = 2;
            this.btnMonitorUnloaderIO.Text = "언로드";
            this.btnMonitorUnloaderIO.UseVisualStyleBackColor = true;
            this.btnMonitorUnloaderIO.Click += new System.EventHandler(this.btnMonitorUnloaderIO_Click);
            // 
            // listTactView07
            // 
            this.listTactView07.BackColor = System.Drawing.Color.DimGray;
            this.listTactView07.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView07.Location = new System.Drawing.Point(353, 454);
            this.listTactView07.Name = "listTactView07";
            this.listTactView07.Size = new System.Drawing.Size(343, 420);
            this.listTactView07.TabIndex = 12;
            // 
            // listTactView08
            // 
            this.listTactView08.BackColor = System.Drawing.Color.DimGray;
            this.listTactView08.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView08.Location = new System.Drawing.Point(702, 454);
            this.listTactView08.Name = "listTactView08";
            this.listTactView08.Size = new System.Drawing.Size(343, 420);
            this.listTactView08.TabIndex = 13;
            // 
            // listTactView06
            // 
            this.listTactView06.BackColor = System.Drawing.Color.DimGray;
            this.listTactView06.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView06.Location = new System.Drawing.Point(12, 454);
            this.listTactView06.Name = "listTactView06";
            this.listTactView06.Size = new System.Drawing.Size(343, 420);
            this.listTactView06.TabIndex = 11;
            // 
            // listTactView09
            // 
            this.listTactView09.BackColor = System.Drawing.Color.DimGray;
            this.listTactView09.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView09.Location = new System.Drawing.Point(1050, 454);
            this.listTactView09.Name = "listTactView09";
            this.listTactView09.Size = new System.Drawing.Size(343, 420);
            this.listTactView09.TabIndex = 14;
            // 
            // listTactView10
            // 
            this.listTactView10.BackColor = System.Drawing.Color.DimGray;
            this.listTactView10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView10.Location = new System.Drawing.Point(1398, 454);
            this.listTactView10.Name = "listTactView10";
            this.listTactView10.Size = new System.Drawing.Size(343, 420);
            this.listTactView10.TabIndex = 15;
            // 
            // Manager_TactTime
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.listTactView10);
            this.Controls.Add(this.listTactView05);
            this.Controls.Add(this.listTactView09);
            this.Controls.Add(this.listTactView04);
            this.Controls.Add(this.listTactView06);
            this.Controls.Add(this.listTactView08);
            this.Controls.Add(this.listTactView01);
            this.Controls.Add(this.listTactView07);
            this.Controls.Add(this.listTactView03);
            this.Controls.Add(this.listTactView02);
            this.Name = "Manager_TactTime";
            this.Size = new System.Drawing.Size(1740, 860);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private UctrlLstTactView listTactView05;
        private UctrlLstTactView listTactView04;
        private UctrlLstTactView listTactView03;
        private UctrlLstTactView listTactView02;
        private UctrlLstTactView listTactView01;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnMonitorLoaderIO;
        private System.Windows.Forms.Button btnMonitorProcessIO;
        private System.Windows.Forms.Button btnMonitorUnloaderIO;
        private UctrlLstTactView listTactView07;
        private UctrlLstTactView listTactView08;
        private UctrlLstTactView listTactView06;
        private UctrlLstTactView listTactView09;
        private UctrlLstTactView listTactView10;
    }
}
