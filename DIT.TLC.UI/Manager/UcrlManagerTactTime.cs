﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DIT.TLC.CTRL;

namespace DIT.TLC.UI
{
    public partial class Manager_TactTime : UserControl
    {
        public Manager_TactTime()
        {
            InitializeComponent();
        }

        private void btnMonitorLoaderIO_Click(object sender, EventArgs e)
        {
            listTactView01.Unit = GG.Equip.LD.TopCstLoader;
            listTactView02.Unit = GG.Equip.LD.BotCstLoader;
            listTactView03.Unit = GG.Equip.LD.Loader;
            listTactView04.Unit = GG.Equip.LD.TopLoaderTransfer;
            listTactView05.Unit = GG.Equip.LD.BotLoaderTransfer;
            listTactView06.Unit = null;
            listTactView07.Unit = null;
            listTactView08.Unit = null;
            listTactView09.Unit = null;
            listTactView10.Unit = null;

        }

        private void btnMonitorUnloaderIO_Click(object sender, EventArgs e)
        {
            listTactView01.Unit = GG.Equip.LD.TopCstLoader;
            listTactView02.Unit = GG.Equip.LD.BotCstLoader;
            listTactView03.Unit = GG.Equip.LD.Loader;
            listTactView04.Unit = GG.Equip.LD.TopLoaderTransfer;
            listTactView05.Unit = GG.Equip.LD.BotLoaderTransfer;
            listTactView06.Unit = null;
            listTactView07.Unit = null;
            listTactView08.Unit = null;
            listTactView09.Unit = null;
            listTactView10.Unit = null;
        }
        private void btnMonitorProcessIO_Click(object sender, EventArgs e)
        {
            listTactView01.Unit = GG.Equip.PROC.TopProcessStage;
            listTactView02.Unit = GG.Equip.PROC.BotProcessStage;
            listTactView03.Unit = GG.Equip.PROC.BreakAlign;
            listTactView04.Unit = GG.Equip.PROC.TopBreakTransfer;
            listTactView05.Unit = GG.Equip.PROC.BotBreakTransfer;
            listTactView06.Unit = GG.Equip.PROC.TopBreakAlign;
            listTactView07.Unit = GG.Equip.PROC.BotBreakAlign;
            listTactView08.Unit = GG.Equip.PROC.TopBreakStage;
            listTactView09.Unit = GG.Equip.PROC.BotBreakStage;
            listTactView10.Unit = null;
        }

        public void UpdateUI()
        {
            listTactView01.UpdateUI();
            listTactView02.UpdateUI();
            listTactView03.UpdateUI();
            listTactView04.UpdateUI();
            listTactView05.UpdateUI();
            listTactView06.UpdateUI();
            listTactView07.UpdateUI();
            listTactView08.UpdateUI();
            listTactView09.UpdateUI();
            listTactView10.UpdateUI();
        }
    }
}
