﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DIT.TLC.CTRL;

namespace DIT.TLC.UI
{
    public partial class UcrlManagerServoManualCtrl : UserControl
    {
        private UcrlAjinServoCtrl[] _lstUcrlAjinServoCtrl = null;
        public UcrlManagerServoManualCtrl()
        {
            InitializeComponent();

            _lstUcrlAjinServoCtrl = new UcrlAjinServoCtrl[]
            {
            ucrlAjinServoCtrl01, ucrlAjinServoCtrl02,
            ucrlAjinServoCtrl03, ucrlAjinServoCtrl04,
            ucrlAjinServoCtrl05, ucrlAjinServoCtrl06,
            ucrlAjinServoCtrl07, ucrlAjinServoCtrl08,
            };
        }


        public void InitalizeUserControlUI(Equipment equip)
        {
            ucrlAjinServoCtrl01.Servo = equip.LD.Loader.X1Axis;
            ucrlAjinServoCtrl02.Servo = equip.LD.Loader.X2Axis;
            ucrlAjinServoCtrl03.Servo = equip.LD.Loader.Y1Axis;
            ucrlAjinServoCtrl04.Servo = equip.LD.Loader.Y2Axis;
        }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnCstLoad_Click(object sender, EventArgs e)
        {
            btnCstLoad.BackColor = UiGlobal.SET_C;
            btnCellOutTransfer.BackColor = UiGlobal.SET_C;
            btnCellLoadTransfer.BackColor = UiGlobal.SET_C;
            btnIRCutProcess.BackColor = UiGlobal.SET_C;
            btnBreakTransfer.BackColor = UiGlobal.SET_C;
            btnBreakUnitXZ.BackColor = UiGlobal.SET_C;
            btnBreakUnitTY.BackColor = UiGlobal.SET_C;
            btnUnloaderTransfer.BackColor = UiGlobal.SET_C;
            btnCameraUnit.BackColor = UiGlobal.SET_C;
            btnCellInTransfer.BackColor = UiGlobal.SET_C;
            btnCstUnloader.BackColor = UiGlobal.SET_C;


            if (((Button)sender) == btnCstLoad)
            {
                btnCstLoad.BackColor = UiGlobal.SET_C;
                FillServoUi(
                    new ServoMotorControl[] {
                        GG.Equip.LD.TopCstLoader.RotationAxis, GG.Equip.LD.BotCstLoader.RotationAxis,
                        GG.Equip.LD.TopCstLoader.CstUpDownAxis, GG.Equip.LD.BotCstLoader.CstUpDownAxis,
                        null, null,
                        null, null
                    });
            }
            else if (((Button)sender) == btnCellOutTransfer)
            {
                btnCellOutTransfer.BackColor = UiGlobal.SET_C;
                FillServoUi(
                    new ServoMotorControl[] {
                        GG.Equip.LD.Loader.X1Axis, GG.Equip.LD.Loader.X2Axis,
                        GG.Equip.LD.Loader.Y1Axis, GG.Equip.LD.Loader.Y2Axis,
                        null, null,
                        null, null
                    });
            }
            else if (((Button)sender) == btnCellLoadTransfer)
            {
                btnCellLoadTransfer.BackColor = UiGlobal.SET_C;
                FillServoUi(
                    new ServoMotorControl[] {
                        null, null,
                        GG.Equip.LD.TopLoaderTransfer.XAxis,  GG.Equip.LD.BotLoaderTransfer.XAxis,
                        GG.Equip.LD.TopLoaderTransfer.YAxis,  GG.Equip.LD.BotLoaderTransfer.YAxis,
                        GG.Equip.LD.TopLoaderTransfer.TAxis,  GG.Equip.LD.BotLoaderTransfer.TAxis,
                    });

            }
            else if (((Button)sender) == btnIRCutProcess)
            {
                btnIRCutProcess.BackColor = UiGlobal.SET_C;
                FillServoUi(
                    new ServoMotorControl[] {
                        GG.Equip.PROC.TopProcessStage.YAxis,  GG.Equip.PROC.BotProcessStage.YAxis,
                        null, null,
                        null, null,
                        null, null,
                        //GG.Equip.LD.TopLoaderTransfer.YAxis,  GG.Equip.LD.BotLoaderTransfer.YAxis, 
                        //GG.Equip.LD.TopLoaderTransfer.TAxis,  GG.Equip.LD.BotLoaderTransfer.TAxis, 
                    });
            }
            else if (((Button)sender) == btnBreakTransfer)
            {
                btnBreakTransfer.BackColor = UiGlobal.SET_C;
                FillServoUi(
                    new ServoMotorControl[] {
                        GG.Equip.PROC.TopBreakTransfer.XAxis,  GG.Equip.PROC.BotBreakTransfer.XAxis,
                        null, null,
                        null, null,
                        null, null,
                    });
            }
            else if (((Button)sender) == btnBreakUnitXZ)
            {
                btnBreakUnitXZ.BackColor = UiGlobal.SET_C;
                FillServoUi(
                    new ServoMotorControl[] {
                        GG.Equip.PROC.TopBreakStage.X1AxisHeader,  GG.Equip.PROC.BotBreakStage.X1AxisHeader,
                        GG.Equip.PROC.TopBreakStage.X2AxisHeader,  GG.Equip.PROC.BotBreakStage.X2AxisHeader,
                        GG.Equip.PROC.TopBreakStage.Z1AxisHeader,  GG.Equip.PROC.BotBreakStage.Z1AxisHeader,
                        GG.Equip.PROC.TopBreakStage.Z2AxisHeader,  GG.Equip.PROC.BotBreakStage.Z2AxisHeader,
                    });
            }
            else if (((Button)sender) == btnBreakUnitTY)
            {
                btnBreakUnitXZ.BackColor = UiGlobal.SET_C;
                FillServoUi(
                    new ServoMotorControl[] {
                        GG.Equip.PROC.TopBreakStage.YAxis,  GG.Equip.PROC.BotBreakStage.YAxis,
                        GG.Equip.PROC.Masure.X1Axis, null,
                        GG.Equip.PROC.TopBreakStage.T1AxisHeader,  GG.Equip.PROC.BotBreakStage.T1AxisHeader,
                        GG.Equip.PROC.TopBreakStage.T2AxisHeader,  GG.Equip.PROC.BotBreakStage.T2AxisHeader,
                    });
            }
            else if (((Button)sender) == btnUnloaderTransfer)
            {
                btnUnloaderTransfer.BackColor = UiGlobal.SET_C;
                FillServoUi(
                    new ServoMotorControl[] {
                        GG.Equip.UD.TopUnloaderTransfer.YAxis,   GG.Equip.UD.BotUnloaderTransfer.YAxis,
                        GG.Equip.UD.TopUnloaderTransfer.T1Axis,  GG.Equip.UD.BotUnloaderTransfer.T1Axis,
                        GG.Equip.UD.TopUnloaderTransfer.T2Axis,  GG.Equip.UD.BotUnloaderTransfer.T2Axis,
                        null, null,
                    });
            }
            else if (((Button)sender) == btnCameraUnit)
            {
                btnCameraUnit.BackColor = UiGlobal.SET_C;
                FillServoUi(
                    new ServoMotorControl[] {
                        GG.Equip.UD.Camera.X1Axis,  GG.Equip.UD.Camera.Z1Axis,
                        null, null,
                        null, null,
                    });
            }
            else if (((Button)sender) == btnCellInTransfer)
            {
                btnCellInTransfer.BackColor = UiGlobal.SET_C;
                FillServoUi(
                    new ServoMotorControl[] {
                        GG.Equip.UD.Unloader.X1Axis,  GG.Equip.UD.Unloader.X2Axis,
                        GG.Equip.UD.Unloader.Y1Axis,  GG.Equip.UD.Unloader.Y2Axis,
                        null, null,
                        null, null,
                    });

            }
            else if (((Button)sender) == btnCstUnloader)
            {
                btnCellInTransfer.BackColor = UiGlobal.SET_C;
                FillServoUi(
                    new ServoMotorControl[] {
                        GG.Equip.UD.TopCstUnloader.RotationUpAxis, GG.Equip.UD.BotCstUnloader.RotationUpAxis,
                        GG.Equip.UD.TopCstUnloader.RotationDownAxis, GG.Equip.UD.BotCstUnloader.RotationDownAxis,
                        GG.Equip.UD.TopCstUnloader.ZAxis, GG.Equip.UD.BotCstUnloader.ZAxis,
                        null, null
                    });
            }
        }

        private void FillServoUi(ServoMotorControl[] lstServo)
        {
            for (int iPos = 0; iPos < 8; iPos++)
            {
                if (iPos < lstServo.Length)
                {
                    _lstUcrlAjinServoCtrl[iPos].Visible = true;
                    _lstUcrlAjinServoCtrl[iPos].Servo = lstServo[iPos];
                }
                else
                {
                    _lstUcrlAjinServoCtrl[iPos].Visible = false;
                    _lstUcrlAjinServoCtrl[iPos].Servo = null;
                }
            }
        }

    }
}
