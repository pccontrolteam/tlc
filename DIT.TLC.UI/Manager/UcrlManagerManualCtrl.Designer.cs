﻿namespace DIT.TLC.UI
{
    partial class UcrlManagerServoManualCtrl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ucrlAjinServoCtrl01 = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.ucrlAjinServoCtrl02 = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnCstLoad = new System.Windows.Forms.Button();
            this.btnCellOutTransfer = new System.Windows.Forms.Button();
            this.btnCellLoadTransfer = new System.Windows.Forms.Button();
            this.btnIRCutProcess = new System.Windows.Forms.Button();
            this.btnBreakTransfer = new System.Windows.Forms.Button();
            this.btnBreakUnitXZ = new System.Windows.Forms.Button();
            this.btnBreakUnitTY = new System.Windows.Forms.Button();
            this.btnUnloaderTransfer = new System.Windows.Forms.Button();
            this.btnCameraUnit = new System.Windows.Forms.Button();
            this.btnCellInTransfer = new System.Windows.Forms.Button();
            this.btnCstUnloader = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ucrlAjinServoCtrl07 = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.ucrlAjinServoCtrl08 = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.ucrlAjinServoCtrl05 = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.ucrlAjinServoCtrl06 = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.ucrlAjinServoCtrl03 = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.ucrlAjinServoCtrl04 = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ucrlAjinServoCtrl01
            // 
            this.ucrlAjinServoCtrl01.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucrlAjinServoCtrl01.Location = new System.Drawing.Point(3, 3);
            this.ucrlAjinServoCtrl01.Name = "ucrlAjinServoCtrl01";
            this.ucrlAjinServoCtrl01.Servo = null;
            this.ucrlAjinServoCtrl01.Size = new System.Drawing.Size(848, 183);
            this.ucrlAjinServoCtrl01.TabIndex = 8;
            // 
            // ucrlAjinServoCtrl02
            // 
            this.ucrlAjinServoCtrl02.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucrlAjinServoCtrl02.Location = new System.Drawing.Point(862, 3);
            this.ucrlAjinServoCtrl02.Name = "ucrlAjinServoCtrl02";
            this.ucrlAjinServoCtrl02.Servo = null;
            this.ucrlAjinServoCtrl02.Size = new System.Drawing.Size(848, 183);
            this.ucrlAjinServoCtrl02.TabIndex = 6;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnCstLoad);
            this.flowLayoutPanel1.Controls.Add(this.btnCellOutTransfer);
            this.flowLayoutPanel1.Controls.Add(this.btnCellLoadTransfer);
            this.flowLayoutPanel1.Controls.Add(this.btnIRCutProcess);
            this.flowLayoutPanel1.Controls.Add(this.btnBreakTransfer);
            this.flowLayoutPanel1.Controls.Add(this.btnBreakUnitXZ);
            this.flowLayoutPanel1.Controls.Add(this.btnBreakUnitTY);
            this.flowLayoutPanel1.Controls.Add(this.btnUnloaderTransfer);
            this.flowLayoutPanel1.Controls.Add(this.btnCameraUnit);
            this.flowLayoutPanel1.Controls.Add(this.btnCellInTransfer);
            this.flowLayoutPanel1.Controls.Add(this.btnCstUnloader);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1740, 47);
            this.flowLayoutPanel1.TabIndex = 3;
            // 
            // btnCstLoad
            // 
            this.btnCstLoad.Location = new System.Drawing.Point(3, 3);
            this.btnCstLoad.Name = "btnCstLoad";
            this.btnCstLoad.Size = new System.Drawing.Size(150, 41);
            this.btnCstLoad.TabIndex = 0;
            this.btnCstLoad.Text = "카셋트 로드";
            this.btnCstLoad.UseVisualStyleBackColor = true;
            this.btnCstLoad.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnCellOutTransfer
            // 
            this.btnCellOutTransfer.Location = new System.Drawing.Point(159, 3);
            this.btnCellOutTransfer.Name = "btnCellOutTransfer";
            this.btnCellOutTransfer.Size = new System.Drawing.Size(150, 41);
            this.btnCellOutTransfer.TabIndex = 1;
            this.btnCellOutTransfer.Text = "설 취출 이재기";
            this.btnCellOutTransfer.UseVisualStyleBackColor = true;
            this.btnCellOutTransfer.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnCellLoadTransfer
            // 
            this.btnCellLoadTransfer.Location = new System.Drawing.Point(315, 3);
            this.btnCellLoadTransfer.Name = "btnCellLoadTransfer";
            this.btnCellLoadTransfer.Size = new System.Drawing.Size(150, 41);
            this.btnCellLoadTransfer.TabIndex = 2;
            this.btnCellLoadTransfer.Text = "셀 로드 이재기";
            this.btnCellLoadTransfer.UseVisualStyleBackColor = true;
            this.btnCellLoadTransfer.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnIRCutProcess
            // 
            this.btnIRCutProcess.Location = new System.Drawing.Point(471, 3);
            this.btnIRCutProcess.Name = "btnIRCutProcess";
            this.btnIRCutProcess.Size = new System.Drawing.Size(150, 41);
            this.btnIRCutProcess.TabIndex = 3;
            this.btnIRCutProcess.Text = "IR Cut 프로세스";
            this.btnIRCutProcess.UseVisualStyleBackColor = true;
            this.btnIRCutProcess.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnBreakTransfer
            // 
            this.btnBreakTransfer.Location = new System.Drawing.Point(627, 3);
            this.btnBreakTransfer.Name = "btnBreakTransfer";
            this.btnBreakTransfer.Size = new System.Drawing.Size(150, 41);
            this.btnBreakTransfer.TabIndex = 4;
            this.btnBreakTransfer.Text = "Break 트랜스퍼";
            this.btnBreakTransfer.UseVisualStyleBackColor = true;
            this.btnBreakTransfer.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnBreakUnitXZ
            // 
            this.btnBreakUnitXZ.Location = new System.Drawing.Point(783, 3);
            this.btnBreakUnitXZ.Name = "btnBreakUnitXZ";
            this.btnBreakUnitXZ.Size = new System.Drawing.Size(150, 41);
            this.btnBreakUnitXZ.TabIndex = 5;
            this.btnBreakUnitXZ.Text = "Break 유닛(X축 Z축)";
            this.btnBreakUnitXZ.UseVisualStyleBackColor = true;
            this.btnBreakUnitXZ.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnBreakUnitTY
            // 
            this.btnBreakUnitTY.Location = new System.Drawing.Point(939, 3);
            this.btnBreakUnitTY.Name = "btnBreakUnitTY";
            this.btnBreakUnitTY.Size = new System.Drawing.Size(150, 41);
            this.btnBreakUnitTY.TabIndex = 6;
            this.btnBreakUnitTY.Text = "Break 유닛(T축 Y축)";
            this.btnBreakUnitTY.UseVisualStyleBackColor = true;
            this.btnBreakUnitTY.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnUnloaderTransfer
            // 
            this.btnUnloaderTransfer.Location = new System.Drawing.Point(1095, 3);
            this.btnUnloaderTransfer.Name = "btnUnloaderTransfer";
            this.btnUnloaderTransfer.Size = new System.Drawing.Size(150, 41);
            this.btnUnloaderTransfer.TabIndex = 7;
            this.btnUnloaderTransfer.Text = "언로더 트랜스퍼";
            this.btnUnloaderTransfer.UseVisualStyleBackColor = true;
            this.btnUnloaderTransfer.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnCameraUnit
            // 
            this.btnCameraUnit.Location = new System.Drawing.Point(1251, 3);
            this.btnCameraUnit.Name = "btnCameraUnit";
            this.btnCameraUnit.Size = new System.Drawing.Size(150, 41);
            this.btnCameraUnit.TabIndex = 8;
            this.btnCameraUnit.Text = "카메라 유닛";
            this.btnCameraUnit.UseVisualStyleBackColor = true;
            this.btnCameraUnit.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnCellInTransfer
            // 
            this.btnCellInTransfer.Location = new System.Drawing.Point(1407, 3);
            this.btnCellInTransfer.Name = "btnCellInTransfer";
            this.btnCellInTransfer.Size = new System.Drawing.Size(150, 41);
            this.btnCellInTransfer.TabIndex = 9;
            this.btnCellInTransfer.Text = "셀 투입 이재기";
            this.btnCellInTransfer.UseVisualStyleBackColor = true;
            this.btnCellInTransfer.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // btnCstUnloader
            // 
            this.btnCstUnloader.Location = new System.Drawing.Point(1563, 3);
            this.btnCstUnloader.Name = "btnCstUnloader";
            this.btnCstUnloader.Size = new System.Drawing.Size(150, 41);
            this.btnCstUnloader.TabIndex = 10;
            this.btnCstUnloader.Text = "카셋트 언로드";
            this.btnCstUnloader.UseVisualStyleBackColor = true;
            this.btnCstUnloader.Click += new System.EventHandler(this.btnCstLoad_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel1.Controls.Add(this.ucrlAjinServoCtrl07);
            this.panel1.Controls.Add(this.ucrlAjinServoCtrl08);
            this.panel1.Controls.Add(this.ucrlAjinServoCtrl05);
            this.panel1.Controls.Add(this.ucrlAjinServoCtrl06);
            this.panel1.Controls.Add(this.ucrlAjinServoCtrl03);
            this.panel1.Controls.Add(this.ucrlAjinServoCtrl04);
            this.panel1.Controls.Add(this.ucrlAjinServoCtrl01);
            this.panel1.Controls.Add(this.ucrlAjinServoCtrl02);
            this.panel1.Location = new System.Drawing.Point(3, 50);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1737, 807);
            this.panel1.TabIndex = 4;
            // 
            // ucrlAjinServoCtrl07
            // 
            this.ucrlAjinServoCtrl07.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucrlAjinServoCtrl07.Location = new System.Drawing.Point(3, 570);
            this.ucrlAjinServoCtrl07.Name = "ucrlAjinServoCtrl07";
            this.ucrlAjinServoCtrl07.Servo = null;
            this.ucrlAjinServoCtrl07.Size = new System.Drawing.Size(848, 183);
            this.ucrlAjinServoCtrl07.TabIndex = 14;
            // 
            // ucrlAjinServoCtrl08
            // 
            this.ucrlAjinServoCtrl08.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucrlAjinServoCtrl08.Location = new System.Drawing.Point(862, 570);
            this.ucrlAjinServoCtrl08.Name = "ucrlAjinServoCtrl08";
            this.ucrlAjinServoCtrl08.Servo = null;
            this.ucrlAjinServoCtrl08.Size = new System.Drawing.Size(848, 183);
            this.ucrlAjinServoCtrl08.TabIndex = 13;
            // 
            // ucrlAjinServoCtrl05
            // 
            this.ucrlAjinServoCtrl05.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucrlAjinServoCtrl05.Location = new System.Drawing.Point(3, 381);
            this.ucrlAjinServoCtrl05.Name = "ucrlAjinServoCtrl05";
            this.ucrlAjinServoCtrl05.Servo = null;
            this.ucrlAjinServoCtrl05.Size = new System.Drawing.Size(848, 183);
            this.ucrlAjinServoCtrl05.TabIndex = 12;
            // 
            // ucrlAjinServoCtrl06
            // 
            this.ucrlAjinServoCtrl06.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucrlAjinServoCtrl06.Location = new System.Drawing.Point(862, 381);
            this.ucrlAjinServoCtrl06.Name = "ucrlAjinServoCtrl06";
            this.ucrlAjinServoCtrl06.Servo = null;
            this.ucrlAjinServoCtrl06.Size = new System.Drawing.Size(848, 183);
            this.ucrlAjinServoCtrl06.TabIndex = 11;
            // 
            // ucrlAjinServoCtrl03
            // 
            this.ucrlAjinServoCtrl03.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucrlAjinServoCtrl03.Location = new System.Drawing.Point(3, 192);
            this.ucrlAjinServoCtrl03.Name = "ucrlAjinServoCtrl03";
            this.ucrlAjinServoCtrl03.Servo = null;
            this.ucrlAjinServoCtrl03.Size = new System.Drawing.Size(848, 183);
            this.ucrlAjinServoCtrl03.TabIndex = 10;
            // 
            // ucrlAjinServoCtrl04
            // 
            this.ucrlAjinServoCtrl04.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucrlAjinServoCtrl04.Location = new System.Drawing.Point(862, 192);
            this.ucrlAjinServoCtrl04.Name = "ucrlAjinServoCtrl04";
            this.ucrlAjinServoCtrl04.Servo = null;
            this.ucrlAjinServoCtrl04.Size = new System.Drawing.Size(848, 183);
            this.ucrlAjinServoCtrl04.TabIndex = 9;
            // 
            // UcrlManagerServoManualCtrl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "UcrlManagerServoManualCtrl";
            this.Size = new System.Drawing.Size(1740, 860);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private UcrlAjinServoCtrl ucrlAjinServoCtrl01;
        private UcrlAjinServoCtrl ucrlAjinServoCtrl02;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnCstLoad;
        private System.Windows.Forms.Button btnCellOutTransfer;
        private System.Windows.Forms.Button btnCellLoadTransfer;
        private System.Windows.Forms.Button btnIRCutProcess;
        private System.Windows.Forms.Button btnBreakTransfer;
        private System.Windows.Forms.Button btnBreakUnitXZ;
        private System.Windows.Forms.Button btnBreakUnitTY;
        private System.Windows.Forms.Button btnUnloaderTransfer;
        private System.Windows.Forms.Button btnCameraUnit;
        private System.Windows.Forms.Button btnCellInTransfer;
        private System.Windows.Forms.Button btnCstUnloader;
        private System.Windows.Forms.Panel panel1;
        private UcrlAjinServoCtrl ucrlAjinServoCtrl07;
        private UcrlAjinServoCtrl ucrlAjinServoCtrl08;
        private UcrlAjinServoCtrl ucrlAjinServoCtrl05;
        private UcrlAjinServoCtrl ucrlAjinServoCtrl06;
        private UcrlAjinServoCtrl ucrlAjinServoCtrl03;
        private UcrlAjinServoCtrl ucrlAjinServoCtrl04;
    }
}
