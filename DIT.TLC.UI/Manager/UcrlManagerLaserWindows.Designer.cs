﻿namespace DIT.TLC.UI
{
    partial class Manager_Laser
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_Main_Lock = new System.Windows.Forms.Button();
            this.txtMain_Lock = new System.Windows.Forms.TextBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.txtMain_PD7Value_PD7Power_ReadOnly = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtMain_PD7Value_OutputPower_ReadOnly = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btn_Main_Shutter_Close = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMain_Shutter_light = new System.Windows.Forms.TextBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtMain_ModulatorPulseEnergyControl_External_light = new System.Windows.Forms.TextBox();
            this.txtMain_ModulatorPulseEnergyControl_Internal_light = new System.Windows.Forms.TextBox();
            this.txtMain_ModulatorPulseEnergyControl_ReadOnly = new System.Windows.Forms.TextBox();
            this.btn_Main_ModulatorPulseEnergyControl_Set = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.txtMain_ModulatorPulseEnergyControl = new System.Windows.Forms.TextBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.txtMain_AttenuatorPowerControl_AttenuatorPower_ReadOnly = new System.Windows.Forms.TextBox();
            this.btn_Main_AttenuatorPowerControl_AttenuatorPower_Set = new System.Windows.Forms.Button();
            this.txtMain_AttenuatorPowerControl_AttenuatorPower = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtMain_AttenuatorPowerControl_ClosedLoop_light = new System.Windows.Forms.TextBox();
            this.txtMain_AttenuatorPowerControl_Transmission_ReadOnly = new System.Windows.Forms.TextBox();
            this.btn_Main_AttenuatorPowerControl_Transmission_Set = new System.Windows.Forms.Button();
            this.txtMain_AttenuatorPowerControl_Transmission = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMain_AttenuatorPowerControl_OpenLoop_light = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtMain_TimingAndFrequency_Burst_ReadOnly = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtMain_TimingAndFrequency_Burst = new System.Windows.Forms.TextBox();
            this.txtMain_TimingAndFrequency_PulseMode_ReadOnly = new System.Windows.Forms.TextBox();
            this.cbxtMain_TimingAndFrequency_PulseMode = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cbxtMain_TimingAndFrequency_Amplifier = new System.Windows.Forms.ComboBox();
            this.txtMain_TimingAndFrequency_Output_ReadOnly = new System.Windows.Forms.TextBox();
            this.txtMain_TimingAndFrequency_Divider_ReadOnly = new System.Windows.Forms.TextBox();
            this.txtMain_TimingAndFrequency_Divider = new System.Windows.Forms.TextBox();
            this.txtMain_TimingAndFrequency_Amplifier_ReadOnly = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btn_Main_TimingAndFrequency_PulseMode_Set = new System.Windows.Forms.Button();
            this.btn_Main_TimingAndFrequency_Burst_Set = new System.Windows.Forms.Button();
            this.btn_Main_TimingAndFrequency_Divider_Set = new System.Windows.Forms.Button();
            this.btn_Main_TimingAndFrequency_Amplifier_Set = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btn_Main_SystemStatus_Stop = new System.Windows.Forms.Button();
            this.btn_Main_SystemStatus_Start = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.txtMain_SystemStatus_light = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btn_Main_SystemFaults_Clear = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtMain_SystemFaults_light = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMain_KeySwitch_light = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_Main_ConnectionToLaser_DisConnection = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMain_ConnectionToLaser_light = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.txtMonitoring_H3_Dewpoint = new System.Windows.Forms.TextBox();
            this.txtMonitoring_H3_AmbientAir_light = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtMonitoring_H3_Humidity = new System.Windows.Forms.TextBox();
            this.txtMonitoring_H3_Temp = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.txtMonitoring_H2_Dewpoint = new System.Windows.Forms.TextBox();
            this.txtMonitoring_H2_HarmonicModuleAir_light = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtMonitoring_H2_Humidity = new System.Windows.Forms.TextBox();
            this.txtMonitoring_H2_Temp = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.txtMonitoring_H1_Amplifier_Dewpoint = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtMonitoring_H1_Amplifier_Temp = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtMonitoring_H1_Amplifier_Humidity = new System.Windows.Forms.TextBox();
            this.txtMonitoring_H1_Amplifier_light = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.txtMonitoring_HousingTemperature2_Temp = new System.Windows.Forms.TextBox();
            this.txtMonitoring_HousingTemperature2_light = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.txtMonitoring_HousingTemperature1_Temp = new System.Windows.Forms.TextBox();
            this.txtMonitoring_HousingTemperature1_light = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.txtMonitoring_WaterMonitor_Flow = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtMonitoring_WaterMonitor_Temp = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtMonitoring_WaterMonitor_light = new System.Windows.Forms.TextBox();
            this.panel25 = new System.Windows.Forms.Panel();
            this.label33 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label34 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel25);
            this.panel1.Controls.Add(this.groupBox9);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox8);
            this.panel1.Controls.Add(this.groupBox7);
            this.panel1.Controls.Add(this.groupBox6);
            this.panel1.Controls.Add(this.groupBox5);
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(865, 869);
            this.panel1.TabIndex = 0;
            // 
            // btn_Main_Lock
            // 
            this.btn_Main_Lock.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btn_Main_Lock.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btn_Main_Lock.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Main_Lock.ForeColor = System.Drawing.Color.White;
            this.btn_Main_Lock.Location = new System.Drawing.Point(161, 7);
            this.btn_Main_Lock.Name = "btn_Main_Lock";
            this.btn_Main_Lock.Size = new System.Drawing.Size(77, 25);
            this.btn_Main_Lock.TabIndex = 28;
            this.btn_Main_Lock.Text = "LOCK";
            this.btn_Main_Lock.UseVisualStyleBackColor = false;
            // 
            // txtMain_Lock
            // 
            this.txtMain_Lock.Font = new System.Drawing.Font("맑은 고딕", 7F, System.Drawing.FontStyle.Bold);
            this.txtMain_Lock.Location = new System.Drawing.Point(3, 9);
            this.txtMain_Lock.Name = "txtMain_Lock";
            this.txtMain_Lock.Size = new System.Drawing.Size(152, 20);
            this.txtMain_Lock.TabIndex = 28;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.txtMain_PD7Value_PD7Power_ReadOnly);
            this.groupBox9.Controls.Add(this.label19);
            this.groupBox9.Controls.Add(this.txtMain_PD7Value_OutputPower_ReadOnly);
            this.groupBox9.Controls.Add(this.label18);
            this.groupBox9.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox9.ForeColor = System.Drawing.Color.White;
            this.groupBox9.Location = new System.Drawing.Point(16, 783);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(830, 55);
            this.groupBox9.TabIndex = 5;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "PD7 Value";
            // 
            // txtMain_PD7Value_PD7Power_ReadOnly
            // 
            this.txtMain_PD7Value_PD7Power_ReadOnly.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMain_PD7Value_PD7Power_ReadOnly.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.txtMain_PD7Value_PD7Power_ReadOnly.Location = new System.Drawing.Point(631, 20);
            this.txtMain_PD7Value_PD7Power_ReadOnly.Name = "txtMain_PD7Value_PD7Power_ReadOnly";
            this.txtMain_PD7Value_PD7Power_ReadOnly.Size = new System.Drawing.Size(121, 25);
            this.txtMain_PD7Value_PD7Power_ReadOnly.TabIndex = 45;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(454, 24);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(95, 21);
            this.label19.TabIndex = 44;
            this.label19.Text = "PD7 Power";
            // 
            // txtMain_PD7Value_OutputPower_ReadOnly
            // 
            this.txtMain_PD7Value_OutputPower_ReadOnly.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMain_PD7Value_OutputPower_ReadOnly.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.txtMain_PD7Value_OutputPower_ReadOnly.Location = new System.Drawing.Point(249, 23);
            this.txtMain_PD7Value_OutputPower_ReadOnly.Name = "txtMain_PD7Value_OutputPower_ReadOnly";
            this.txtMain_PD7Value_OutputPower_ReadOnly.Size = new System.Drawing.Size(121, 25);
            this.txtMain_PD7Value_OutputPower_ReadOnly.TabIndex = 43;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(14, 25);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(152, 21);
            this.label18.TabIndex = 42;
            this.label18.Text = "Output Power (W)";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btn_Main_Shutter_Close);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.txtMain_Shutter_light);
            this.groupBox3.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox3.ForeColor = System.Drawing.Color.White;
            this.groupBox3.Location = new System.Drawing.Point(287, 109);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(559, 55);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Shutter";
            // 
            // btn_Main_Shutter_Close
            // 
            this.btn_Main_Shutter_Close.BackColor = System.Drawing.Color.DimGray;
            this.btn_Main_Shutter_Close.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Main_Shutter_Close.Location = new System.Drawing.Point(432, 19);
            this.btn_Main_Shutter_Close.Name = "btn_Main_Shutter_Close";
            this.btn_Main_Shutter_Close.Size = new System.Drawing.Size(121, 25);
            this.btn_Main_Shutter_Close.TabIndex = 3;
            this.btn_Main_Shutter_Close.Text = "Close";
            this.btn_Main_Shutter_Close.UseVisualStyleBackColor = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(73, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 21);
            this.label3.TabIndex = 5;
            this.label3.Text = "Open";
            // 
            // txtMain_Shutter_light
            // 
            this.txtMain_Shutter_light.BackColor = System.Drawing.Color.Lime;
            this.txtMain_Shutter_light.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.txtMain_Shutter_light.Location = new System.Drawing.Point(18, 22);
            this.txtMain_Shutter_light.Name = "txtMain_Shutter_light";
            this.txtMain_Shutter_light.Size = new System.Drawing.Size(37, 25);
            this.txtMain_Shutter_light.TabIndex = 4;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label16);
            this.groupBox8.Controls.Add(this.label7);
            this.groupBox8.Controls.Add(this.txtMain_ModulatorPulseEnergyControl_External_light);
            this.groupBox8.Controls.Add(this.txtMain_ModulatorPulseEnergyControl_Internal_light);
            this.groupBox8.Controls.Add(this.txtMain_ModulatorPulseEnergyControl_ReadOnly);
            this.groupBox8.Controls.Add(this.btn_Main_ModulatorPulseEnergyControl_Set);
            this.groupBox8.Controls.Add(this.label17);
            this.groupBox8.Controls.Add(this.txtMain_ModulatorPulseEnergyControl);
            this.groupBox8.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox8.ForeColor = System.Drawing.Color.White;
            this.groupBox8.Location = new System.Drawing.Point(17, 641);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(830, 132);
            this.groupBox8.TabIndex = 7;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Modulator Pulse Energy Control";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(503, 32);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(72, 21);
            this.label16.TabIndex = 41;
            this.label16.Text = "External";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(72, 30);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 21);
            this.label7.TabIndex = 13;
            this.label7.Text = "Internal";
            // 
            // txtMain_ModulatorPulseEnergyControl_External_light
            // 
            this.txtMain_ModulatorPulseEnergyControl_External_light.BackColor = System.Drawing.Color.Black;
            this.txtMain_ModulatorPulseEnergyControl_External_light.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.txtMain_ModulatorPulseEnergyControl_External_light.Location = new System.Drawing.Point(448, 30);
            this.txtMain_ModulatorPulseEnergyControl_External_light.Name = "txtMain_ModulatorPulseEnergyControl_External_light";
            this.txtMain_ModulatorPulseEnergyControl_External_light.Size = new System.Drawing.Size(37, 25);
            this.txtMain_ModulatorPulseEnergyControl_External_light.TabIndex = 40;
            // 
            // txtMain_ModulatorPulseEnergyControl_Internal_light
            // 
            this.txtMain_ModulatorPulseEnergyControl_Internal_light.BackColor = System.Drawing.Color.Lime;
            this.txtMain_ModulatorPulseEnergyControl_Internal_light.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.txtMain_ModulatorPulseEnergyControl_Internal_light.Location = new System.Drawing.Point(17, 28);
            this.txtMain_ModulatorPulseEnergyControl_Internal_light.Name = "txtMain_ModulatorPulseEnergyControl_Internal_light";
            this.txtMain_ModulatorPulseEnergyControl_Internal_light.Size = new System.Drawing.Size(37, 25);
            this.txtMain_ModulatorPulseEnergyControl_Internal_light.TabIndex = 12;
            // 
            // txtMain_ModulatorPulseEnergyControl_ReadOnly
            // 
            this.txtMain_ModulatorPulseEnergyControl_ReadOnly.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMain_ModulatorPulseEnergyControl_ReadOnly.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.txtMain_ModulatorPulseEnergyControl_ReadOnly.Location = new System.Drawing.Point(147, 69);
            this.txtMain_ModulatorPulseEnergyControl_ReadOnly.Name = "txtMain_ModulatorPulseEnergyControl_ReadOnly";
            this.txtMain_ModulatorPulseEnergyControl_ReadOnly.Size = new System.Drawing.Size(121, 25);
            this.txtMain_ModulatorPulseEnergyControl_ReadOnly.TabIndex = 39;
            // 
            // btn_Main_ModulatorPulseEnergyControl_Set
            // 
            this.btn_Main_ModulatorPulseEnergyControl_Set.BackColor = System.Drawing.Color.DimGray;
            this.btn_Main_ModulatorPulseEnergyControl_Set.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Main_ModulatorPulseEnergyControl_Set.Location = new System.Drawing.Point(281, 100);
            this.btn_Main_ModulatorPulseEnergyControl_Set.Name = "btn_Main_ModulatorPulseEnergyControl_Set";
            this.btn_Main_ModulatorPulseEnergyControl_Set.Size = new System.Drawing.Size(121, 25);
            this.btn_Main_ModulatorPulseEnergyControl_Set.TabIndex = 36;
            this.btn_Main_ModulatorPulseEnergyControl_Set.Text = "Set";
            this.btn_Main_ModulatorPulseEnergyControl_Set.UseVisualStyleBackColor = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(13, 70);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(85, 21);
            this.label17.TabIndex = 38;
            this.label17.Text = "% of max";
            // 
            // txtMain_ModulatorPulseEnergyControl
            // 
            this.txtMain_ModulatorPulseEnergyControl.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.txtMain_ModulatorPulseEnergyControl.Location = new System.Drawing.Point(147, 100);
            this.txtMain_ModulatorPulseEnergyControl.Name = "txtMain_ModulatorPulseEnergyControl";
            this.txtMain_ModulatorPulseEnergyControl.Size = new System.Drawing.Size(121, 25);
            this.txtMain_ModulatorPulseEnergyControl.TabIndex = 37;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.txtMain_AttenuatorPowerControl_AttenuatorPower_ReadOnly);
            this.groupBox7.Controls.Add(this.btn_Main_AttenuatorPowerControl_AttenuatorPower_Set);
            this.groupBox7.Controls.Add(this.txtMain_AttenuatorPowerControl_AttenuatorPower);
            this.groupBox7.Controls.Add(this.label15);
            this.groupBox7.Controls.Add(this.label14);
            this.groupBox7.Controls.Add(this.txtMain_AttenuatorPowerControl_ClosedLoop_light);
            this.groupBox7.Controls.Add(this.txtMain_AttenuatorPowerControl_Transmission_ReadOnly);
            this.groupBox7.Controls.Add(this.btn_Main_AttenuatorPowerControl_Transmission_Set);
            this.groupBox7.Controls.Add(this.txtMain_AttenuatorPowerControl_Transmission);
            this.groupBox7.Controls.Add(this.label13);
            this.groupBox7.Controls.Add(this.label6);
            this.groupBox7.Controls.Add(this.txtMain_AttenuatorPowerControl_OpenLoop_light);
            this.groupBox7.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox7.ForeColor = System.Drawing.Color.White;
            this.groupBox7.Location = new System.Drawing.Point(16, 499);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(830, 132);
            this.groupBox7.TabIndex = 6;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Attenuator Power Control";
            // 
            // txtMain_AttenuatorPowerControl_AttenuatorPower_ReadOnly
            // 
            this.txtMain_AttenuatorPowerControl_AttenuatorPower_ReadOnly.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMain_AttenuatorPowerControl_AttenuatorPower_ReadOnly.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.txtMain_AttenuatorPowerControl_AttenuatorPower_ReadOnly.Location = new System.Drawing.Point(703, 69);
            this.txtMain_AttenuatorPowerControl_AttenuatorPower_ReadOnly.Name = "txtMain_AttenuatorPowerControl_AttenuatorPower_ReadOnly";
            this.txtMain_AttenuatorPowerControl_AttenuatorPower_ReadOnly.Size = new System.Drawing.Size(121, 25);
            this.txtMain_AttenuatorPowerControl_AttenuatorPower_ReadOnly.TabIndex = 35;
            // 
            // btn_Main_AttenuatorPowerControl_AttenuatorPower_Set
            // 
            this.btn_Main_AttenuatorPowerControl_AttenuatorPower_Set.BackColor = System.Drawing.Color.DimGray;
            this.btn_Main_AttenuatorPowerControl_AttenuatorPower_Set.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Main_AttenuatorPowerControl_AttenuatorPower_Set.Location = new System.Drawing.Point(703, 100);
            this.btn_Main_AttenuatorPowerControl_AttenuatorPower_Set.Name = "btn_Main_AttenuatorPowerControl_AttenuatorPower_Set";
            this.btn_Main_AttenuatorPowerControl_AttenuatorPower_Set.Size = new System.Drawing.Size(121, 25);
            this.btn_Main_AttenuatorPowerControl_AttenuatorPower_Set.TabIndex = 33;
            this.btn_Main_AttenuatorPowerControl_AttenuatorPower_Set.Text = "Set";
            this.btn_Main_AttenuatorPowerControl_AttenuatorPower_Set.UseVisualStyleBackColor = false;
            // 
            // txtMain_AttenuatorPowerControl_AttenuatorPower
            // 
            this.txtMain_AttenuatorPowerControl_AttenuatorPower.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMain_AttenuatorPowerControl_AttenuatorPower.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.txtMain_AttenuatorPowerControl_AttenuatorPower.Location = new System.Drawing.Point(569, 100);
            this.txtMain_AttenuatorPowerControl_AttenuatorPower.Name = "txtMain_AttenuatorPowerControl_AttenuatorPower";
            this.txtMain_AttenuatorPowerControl_AttenuatorPower.Size = new System.Drawing.Size(121, 25);
            this.txtMain_AttenuatorPowerControl_AttenuatorPower.TabIndex = 34;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(445, 69);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(181, 21);
            this.label15.TabIndex = 32;
            this.label15.Text = "Attenuator Power (W)";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(504, 30);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(104, 21);
            this.label14.TabIndex = 31;
            this.label14.Text = "Closed Loop";
            // 
            // txtMain_AttenuatorPowerControl_ClosedLoop_light
            // 
            this.txtMain_AttenuatorPowerControl_ClosedLoop_light.BackColor = System.Drawing.Color.Black;
            this.txtMain_AttenuatorPowerControl_ClosedLoop_light.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.txtMain_AttenuatorPowerControl_ClosedLoop_light.Location = new System.Drawing.Point(449, 28);
            this.txtMain_AttenuatorPowerControl_ClosedLoop_light.Name = "txtMain_AttenuatorPowerControl_ClosedLoop_light";
            this.txtMain_AttenuatorPowerControl_ClosedLoop_light.Size = new System.Drawing.Size(37, 25);
            this.txtMain_AttenuatorPowerControl_ClosedLoop_light.TabIndex = 30;
            // 
            // txtMain_AttenuatorPowerControl_Transmission_ReadOnly
            // 
            this.txtMain_AttenuatorPowerControl_Transmission_ReadOnly.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMain_AttenuatorPowerControl_Transmission_ReadOnly.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.txtMain_AttenuatorPowerControl_Transmission_ReadOnly.Location = new System.Drawing.Point(282, 70);
            this.txtMain_AttenuatorPowerControl_Transmission_ReadOnly.Name = "txtMain_AttenuatorPowerControl_Transmission_ReadOnly";
            this.txtMain_AttenuatorPowerControl_Transmission_ReadOnly.Size = new System.Drawing.Size(121, 25);
            this.txtMain_AttenuatorPowerControl_Transmission_ReadOnly.TabIndex = 29;
            // 
            // btn_Main_AttenuatorPowerControl_Transmission_Set
            // 
            this.btn_Main_AttenuatorPowerControl_Transmission_Set.BackColor = System.Drawing.Color.DimGray;
            this.btn_Main_AttenuatorPowerControl_Transmission_Set.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Main_AttenuatorPowerControl_Transmission_Set.Location = new System.Drawing.Point(282, 101);
            this.btn_Main_AttenuatorPowerControl_Transmission_Set.Name = "btn_Main_AttenuatorPowerControl_Transmission_Set";
            this.btn_Main_AttenuatorPowerControl_Transmission_Set.Size = new System.Drawing.Size(121, 25);
            this.btn_Main_AttenuatorPowerControl_Transmission_Set.TabIndex = 28;
            this.btn_Main_AttenuatorPowerControl_Transmission_Set.Text = "Set";
            this.btn_Main_AttenuatorPowerControl_Transmission_Set.UseVisualStyleBackColor = false;
            // 
            // txtMain_AttenuatorPowerControl_Transmission
            // 
            this.txtMain_AttenuatorPowerControl_Transmission.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.txtMain_AttenuatorPowerControl_Transmission.Location = new System.Drawing.Point(148, 101);
            this.txtMain_AttenuatorPowerControl_Transmission.Name = "txtMain_AttenuatorPowerControl_Transmission";
            this.txtMain_AttenuatorPowerControl_Transmission.Size = new System.Drawing.Size(121, 25);
            this.txtMain_AttenuatorPowerControl_Transmission.TabIndex = 28;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(14, 71);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(140, 21);
            this.label13.TabIndex = 28;
            this.label13.Text = "Transmission (%)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(73, 30);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 21);
            this.label6.TabIndex = 11;
            this.label6.Text = "Open Loop";
            // 
            // txtMain_AttenuatorPowerControl_OpenLoop_light
            // 
            this.txtMain_AttenuatorPowerControl_OpenLoop_light.BackColor = System.Drawing.Color.Lime;
            this.txtMain_AttenuatorPowerControl_OpenLoop_light.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.txtMain_AttenuatorPowerControl_OpenLoop_light.Location = new System.Drawing.Point(18, 28);
            this.txtMain_AttenuatorPowerControl_OpenLoop_light.Name = "txtMain_AttenuatorPowerControl_OpenLoop_light";
            this.txtMain_AttenuatorPowerControl_OpenLoop_light.Size = new System.Drawing.Size(37, 25);
            this.txtMain_AttenuatorPowerControl_OpenLoop_light.TabIndex = 10;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.txtMain_TimingAndFrequency_Burst_ReadOnly);
            this.groupBox6.Controls.Add(this.label12);
            this.groupBox6.Controls.Add(this.txtMain_TimingAndFrequency_Burst);
            this.groupBox6.Controls.Add(this.txtMain_TimingAndFrequency_PulseMode_ReadOnly);
            this.groupBox6.Controls.Add(this.cbxtMain_TimingAndFrequency_PulseMode);
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Controls.Add(this.cbxtMain_TimingAndFrequency_Amplifier);
            this.groupBox6.Controls.Add(this.txtMain_TimingAndFrequency_Output_ReadOnly);
            this.groupBox6.Controls.Add(this.txtMain_TimingAndFrequency_Divider_ReadOnly);
            this.groupBox6.Controls.Add(this.txtMain_TimingAndFrequency_Divider);
            this.groupBox6.Controls.Add(this.txtMain_TimingAndFrequency_Amplifier_ReadOnly);
            this.groupBox6.Controls.Add(this.label10);
            this.groupBox6.Controls.Add(this.label9);
            this.groupBox6.Controls.Add(this.label8);
            this.groupBox6.Controls.Add(this.btn_Main_TimingAndFrequency_PulseMode_Set);
            this.groupBox6.Controls.Add(this.btn_Main_TimingAndFrequency_Burst_Set);
            this.groupBox6.Controls.Add(this.btn_Main_TimingAndFrequency_Divider_Set);
            this.groupBox6.Controls.Add(this.btn_Main_TimingAndFrequency_Amplifier_Set);
            this.groupBox6.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox6.ForeColor = System.Drawing.Color.White;
            this.groupBox6.Location = new System.Drawing.Point(16, 304);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(830, 185);
            this.groupBox6.TabIndex = 5;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Timing and Frequency";
            // 
            // txtMain_TimingAndFrequency_Burst_ReadOnly
            // 
            this.txtMain_TimingAndFrequency_Burst_ReadOnly.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMain_TimingAndFrequency_Burst_ReadOnly.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.txtMain_TimingAndFrequency_Burst_ReadOnly.Location = new System.Drawing.Point(576, 153);
            this.txtMain_TimingAndFrequency_Burst_ReadOnly.Name = "txtMain_TimingAndFrequency_Burst_ReadOnly";
            this.txtMain_TimingAndFrequency_Burst_ReadOnly.Size = new System.Drawing.Size(121, 25);
            this.txtMain_TimingAndFrequency_Burst_ReadOnly.TabIndex = 27;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(572, 121);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(49, 21);
            this.label12.TabIndex = 26;
            this.label12.Text = "Burst";
            // 
            // txtMain_TimingAndFrequency_Burst
            // 
            this.txtMain_TimingAndFrequency_Burst.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.txtMain_TimingAndFrequency_Burst.Location = new System.Drawing.Point(703, 121);
            this.txtMain_TimingAndFrequency_Burst.Name = "txtMain_TimingAndFrequency_Burst";
            this.txtMain_TimingAndFrequency_Burst.Size = new System.Drawing.Size(121, 25);
            this.txtMain_TimingAndFrequency_Burst.TabIndex = 25;
            // 
            // txtMain_TimingAndFrequency_PulseMode_ReadOnly
            // 
            this.txtMain_TimingAndFrequency_PulseMode_ReadOnly.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMain_TimingAndFrequency_PulseMode_ReadOnly.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.txtMain_TimingAndFrequency_PulseMode_ReadOnly.Location = new System.Drawing.Point(576, 88);
            this.txtMain_TimingAndFrequency_PulseMode_ReadOnly.Name = "txtMain_TimingAndFrequency_PulseMode_ReadOnly";
            this.txtMain_TimingAndFrequency_PulseMode_ReadOnly.Size = new System.Drawing.Size(248, 25);
            this.txtMain_TimingAndFrequency_PulseMode_ReadOnly.TabIndex = 24;
            // 
            // cbxtMain_TimingAndFrequency_PulseMode
            // 
            this.cbxtMain_TimingAndFrequency_PulseMode.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.cbxtMain_TimingAndFrequency_PulseMode.FormattingEnabled = true;
            this.cbxtMain_TimingAndFrequency_PulseMode.Location = new System.Drawing.Point(576, 57);
            this.cbxtMain_TimingAndFrequency_PulseMode.Name = "cbxtMain_TimingAndFrequency_PulseMode";
            this.cbxtMain_TimingAndFrequency_PulseMode.Size = new System.Drawing.Size(248, 25);
            this.cbxtMain_TimingAndFrequency_PulseMode.TabIndex = 23;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(572, 27);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(100, 21);
            this.label11.TabIndex = 22;
            this.label11.Text = "Pulse Mode";
            // 
            // cbxtMain_TimingAndFrequency_Amplifier
            // 
            this.cbxtMain_TimingAndFrequency_Amplifier.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.cbxtMain_TimingAndFrequency_Amplifier.FormattingEnabled = true;
            this.cbxtMain_TimingAndFrequency_Amplifier.Location = new System.Drawing.Point(148, 28);
            this.cbxtMain_TimingAndFrequency_Amplifier.Name = "cbxtMain_TimingAndFrequency_Amplifier";
            this.cbxtMain_TimingAndFrequency_Amplifier.Size = new System.Drawing.Size(121, 25);
            this.cbxtMain_TimingAndFrequency_Amplifier.TabIndex = 21;
            // 
            // txtMain_TimingAndFrequency_Output_ReadOnly
            // 
            this.txtMain_TimingAndFrequency_Output_ReadOnly.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMain_TimingAndFrequency_Output_ReadOnly.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.txtMain_TimingAndFrequency_Output_ReadOnly.Location = new System.Drawing.Point(148, 153);
            this.txtMain_TimingAndFrequency_Output_ReadOnly.Name = "txtMain_TimingAndFrequency_Output_ReadOnly";
            this.txtMain_TimingAndFrequency_Output_ReadOnly.Size = new System.Drawing.Size(121, 25);
            this.txtMain_TimingAndFrequency_Output_ReadOnly.TabIndex = 20;
            // 
            // txtMain_TimingAndFrequency_Divider_ReadOnly
            // 
            this.txtMain_TimingAndFrequency_Divider_ReadOnly.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMain_TimingAndFrequency_Divider_ReadOnly.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.txtMain_TimingAndFrequency_Divider_ReadOnly.Location = new System.Drawing.Point(148, 121);
            this.txtMain_TimingAndFrequency_Divider_ReadOnly.Name = "txtMain_TimingAndFrequency_Divider_ReadOnly";
            this.txtMain_TimingAndFrequency_Divider_ReadOnly.Size = new System.Drawing.Size(121, 25);
            this.txtMain_TimingAndFrequency_Divider_ReadOnly.TabIndex = 19;
            // 
            // txtMain_TimingAndFrequency_Divider
            // 
            this.txtMain_TimingAndFrequency_Divider.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.txtMain_TimingAndFrequency_Divider.Location = new System.Drawing.Point(148, 89);
            this.txtMain_TimingAndFrequency_Divider.Name = "txtMain_TimingAndFrequency_Divider";
            this.txtMain_TimingAndFrequency_Divider.Size = new System.Drawing.Size(121, 25);
            this.txtMain_TimingAndFrequency_Divider.TabIndex = 18;
            // 
            // txtMain_TimingAndFrequency_Amplifier_ReadOnly
            // 
            this.txtMain_TimingAndFrequency_Amplifier_ReadOnly.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMain_TimingAndFrequency_Amplifier_ReadOnly.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.txtMain_TimingAndFrequency_Amplifier_ReadOnly.Location = new System.Drawing.Point(148, 57);
            this.txtMain_TimingAndFrequency_Amplifier_ReadOnly.Name = "txtMain_TimingAndFrequency_Amplifier_ReadOnly";
            this.txtMain_TimingAndFrequency_Amplifier_ReadOnly.Size = new System.Drawing.Size(121, 25);
            this.txtMain_TimingAndFrequency_Amplifier_ReadOnly.TabIndex = 17;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(14, 156);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(112, 21);
            this.label10.TabIndex = 16;
            this.label10.Text = "OutPut (KHz)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(14, 93);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 21);
            this.label9.TabIndex = 15;
            this.label9.Text = "Divider";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 30);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(121, 21);
            this.label8.TabIndex = 11;
            this.label8.Text = "Amplifier(KHz)";
            // 
            // btn_Main_TimingAndFrequency_PulseMode_Set
            // 
            this.btn_Main_TimingAndFrequency_PulseMode_Set.BackColor = System.Drawing.Color.DimGray;
            this.btn_Main_TimingAndFrequency_PulseMode_Set.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Main_TimingAndFrequency_PulseMode_Set.Location = new System.Drawing.Point(703, 23);
            this.btn_Main_TimingAndFrequency_PulseMode_Set.Name = "btn_Main_TimingAndFrequency_PulseMode_Set";
            this.btn_Main_TimingAndFrequency_PulseMode_Set.Size = new System.Drawing.Size(121, 25);
            this.btn_Main_TimingAndFrequency_PulseMode_Set.TabIndex = 14;
            this.btn_Main_TimingAndFrequency_PulseMode_Set.Text = "Set";
            this.btn_Main_TimingAndFrequency_PulseMode_Set.UseVisualStyleBackColor = false;
            // 
            // btn_Main_TimingAndFrequency_Burst_Set
            // 
            this.btn_Main_TimingAndFrequency_Burst_Set.BackColor = System.Drawing.Color.DimGray;
            this.btn_Main_TimingAndFrequency_Burst_Set.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Main_TimingAndFrequency_Burst_Set.Location = new System.Drawing.Point(703, 154);
            this.btn_Main_TimingAndFrequency_Burst_Set.Name = "btn_Main_TimingAndFrequency_Burst_Set";
            this.btn_Main_TimingAndFrequency_Burst_Set.Size = new System.Drawing.Size(121, 25);
            this.btn_Main_TimingAndFrequency_Burst_Set.TabIndex = 13;
            this.btn_Main_TimingAndFrequency_Burst_Set.Text = "Set";
            this.btn_Main_TimingAndFrequency_Burst_Set.UseVisualStyleBackColor = false;
            // 
            // btn_Main_TimingAndFrequency_Divider_Set
            // 
            this.btn_Main_TimingAndFrequency_Divider_Set.BackColor = System.Drawing.Color.DimGray;
            this.btn_Main_TimingAndFrequency_Divider_Set.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Main_TimingAndFrequency_Divider_Set.Location = new System.Drawing.Point(282, 91);
            this.btn_Main_TimingAndFrequency_Divider_Set.Name = "btn_Main_TimingAndFrequency_Divider_Set";
            this.btn_Main_TimingAndFrequency_Divider_Set.Size = new System.Drawing.Size(121, 25);
            this.btn_Main_TimingAndFrequency_Divider_Set.TabIndex = 12;
            this.btn_Main_TimingAndFrequency_Divider_Set.Text = "Set";
            this.btn_Main_TimingAndFrequency_Divider_Set.UseVisualStyleBackColor = false;
            // 
            // btn_Main_TimingAndFrequency_Amplifier_Set
            // 
            this.btn_Main_TimingAndFrequency_Amplifier_Set.BackColor = System.Drawing.Color.DimGray;
            this.btn_Main_TimingAndFrequency_Amplifier_Set.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Main_TimingAndFrequency_Amplifier_Set.Location = new System.Drawing.Point(282, 28);
            this.btn_Main_TimingAndFrequency_Amplifier_Set.Name = "btn_Main_TimingAndFrequency_Amplifier_Set";
            this.btn_Main_TimingAndFrequency_Amplifier_Set.Size = new System.Drawing.Size(121, 25);
            this.btn_Main_TimingAndFrequency_Amplifier_Set.TabIndex = 11;
            this.btn_Main_TimingAndFrequency_Amplifier_Set.Text = "Set";
            this.btn_Main_TimingAndFrequency_Amplifier_Set.UseVisualStyleBackColor = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.btn_Main_SystemStatus_Stop);
            this.groupBox5.Controls.Add(this.btn_Main_SystemStatus_Start);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.txtMain_SystemStatus_light);
            this.groupBox5.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox5.ForeColor = System.Drawing.Color.White;
            this.groupBox5.Location = new System.Drawing.Point(16, 239);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(830, 55);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "System Status";
            // 
            // btn_Main_SystemStatus_Stop
            // 
            this.btn_Main_SystemStatus_Stop.BackColor = System.Drawing.Color.DimGray;
            this.btn_Main_SystemStatus_Stop.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Main_SystemStatus_Stop.Location = new System.Drawing.Point(576, 21);
            this.btn_Main_SystemStatus_Stop.Name = "btn_Main_SystemStatus_Stop";
            this.btn_Main_SystemStatus_Stop.Size = new System.Drawing.Size(121, 25);
            this.btn_Main_SystemStatus_Stop.TabIndex = 10;
            this.btn_Main_SystemStatus_Stop.Text = "Stop";
            this.btn_Main_SystemStatus_Stop.UseVisualStyleBackColor = false;
            // 
            // btn_Main_SystemStatus_Start
            // 
            this.btn_Main_SystemStatus_Start.BackColor = System.Drawing.Color.DimGray;
            this.btn_Main_SystemStatus_Start.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Main_SystemStatus_Start.Location = new System.Drawing.Point(703, 21);
            this.btn_Main_SystemStatus_Start.Name = "btn_Main_SystemStatus_Start";
            this.btn_Main_SystemStatus_Start.Size = new System.Drawing.Size(121, 25);
            this.btn_Main_SystemStatus_Start.TabIndex = 8;
            this.btn_Main_SystemStatus_Start.Text = "Start";
            this.btn_Main_SystemStatus_Start.UseVisualStyleBackColor = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(73, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 21);
            this.label5.TabIndex = 9;
            this.label5.Text = "On";
            // 
            // txtMain_SystemStatus_light
            // 
            this.txtMain_SystemStatus_light.BackColor = System.Drawing.Color.Lime;
            this.txtMain_SystemStatus_light.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.txtMain_SystemStatus_light.Location = new System.Drawing.Point(18, 24);
            this.txtMain_SystemStatus_light.Name = "txtMain_SystemStatus_light";
            this.txtMain_SystemStatus_light.Size = new System.Drawing.Size(37, 25);
            this.txtMain_SystemStatus_light.TabIndex = 8;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btn_Main_SystemFaults_Clear);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.txtMain_SystemFaults_light);
            this.groupBox4.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox4.ForeColor = System.Drawing.Color.White;
            this.groupBox4.Location = new System.Drawing.Point(16, 174);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(830, 55);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "System Faults";
            // 
            // btn_Main_SystemFaults_Clear
            // 
            this.btn_Main_SystemFaults_Clear.BackColor = System.Drawing.Color.DimGray;
            this.btn_Main_SystemFaults_Clear.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Main_SystemFaults_Clear.Location = new System.Drawing.Point(703, 21);
            this.btn_Main_SystemFaults_Clear.Name = "btn_Main_SystemFaults_Clear";
            this.btn_Main_SystemFaults_Clear.Size = new System.Drawing.Size(121, 25);
            this.btn_Main_SystemFaults_Clear.TabIndex = 6;
            this.btn_Main_SystemFaults_Clear.Text = "Clear";
            this.btn_Main_SystemFaults_Clear.UseVisualStyleBackColor = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(73, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 21);
            this.label4.TabIndex = 7;
            this.label4.Text = "SYSTEM OK";
            // 
            // txtMain_SystemFaults_light
            // 
            this.txtMain_SystemFaults_light.BackColor = System.Drawing.Color.Lime;
            this.txtMain_SystemFaults_light.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.txtMain_SystemFaults_light.Location = new System.Drawing.Point(18, 24);
            this.txtMain_SystemFaults_light.Name = "txtMain_SystemFaults_light";
            this.txtMain_SystemFaults_light.Size = new System.Drawing.Size(37, 25);
            this.txtMain_SystemFaults_light.TabIndex = 6;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtMain_KeySwitch_light);
            this.groupBox2.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(16, 109);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(245, 55);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Key-Switch";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(73, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 21);
            this.label2.TabIndex = 3;
            this.label2.Text = "Key on";
            // 
            // txtMain_KeySwitch_light
            // 
            this.txtMain_KeySwitch_light.BackColor = System.Drawing.Color.Lime;
            this.txtMain_KeySwitch_light.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.txtMain_KeySwitch_light.Location = new System.Drawing.Point(18, 24);
            this.txtMain_KeySwitch_light.Name = "txtMain_KeySwitch_light";
            this.txtMain_KeySwitch_light.Size = new System.Drawing.Size(37, 25);
            this.txtMain_KeySwitch_light.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.DimGray;
            this.groupBox1.Controls.Add(this.btn_Main_ConnectionToLaser_DisConnection);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtMain_ConnectionToLaser_light);
            this.groupBox1.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(16, 44);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(830, 55);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Connection to Laser";
            // 
            // btn_Main_ConnectionToLaser_DisConnection
            // 
            this.btn_Main_ConnectionToLaser_DisConnection.BackColor = System.Drawing.Color.DimGray;
            this.btn_Main_ConnectionToLaser_DisConnection.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Main_ConnectionToLaser_DisConnection.Location = new System.Drawing.Point(703, 20);
            this.btn_Main_ConnectionToLaser_DisConnection.Name = "btn_Main_ConnectionToLaser_DisConnection";
            this.btn_Main_ConnectionToLaser_DisConnection.Size = new System.Drawing.Size(121, 25);
            this.btn_Main_ConnectionToLaser_DisConnection.TabIndex = 2;
            this.btn_Main_ConnectionToLaser_DisConnection.Text = "DisConnection";
            this.btn_Main_ConnectionToLaser_DisConnection.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(73, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(155, 21);
            this.label1.TabIndex = 1;
            this.label1.Text = "HyperNX 1064-100";
            // 
            // txtMain_ConnectionToLaser_light
            // 
            this.txtMain_ConnectionToLaser_light.BackColor = System.Drawing.Color.Lime;
            this.txtMain_ConnectionToLaser_light.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.txtMain_ConnectionToLaser_light.Location = new System.Drawing.Point(18, 23);
            this.txtMain_ConnectionToLaser_light.Name = "txtMain_ConnectionToLaser_light";
            this.txtMain_ConnectionToLaser_light.Size = new System.Drawing.Size(37, 25);
            this.txtMain_ConnectionToLaser_light.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.groupBox15);
            this.panel2.Controls.Add(this.groupBox14);
            this.panel2.Controls.Add(this.groupBox13);
            this.panel2.Controls.Add(this.groupBox12);
            this.panel2.Controls.Add(this.groupBox11);
            this.panel2.Controls.Add(this.groupBox10);
            this.panel2.Location = new System.Drawing.Point(873, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(864, 869);
            this.panel2.TabIndex = 1;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.txtMonitoring_H3_Dewpoint);
            this.groupBox15.Controls.Add(this.txtMonitoring_H3_AmbientAir_light);
            this.groupBox15.Controls.Add(this.label30);
            this.groupBox15.Controls.Add(this.txtMonitoring_H3_Humidity);
            this.groupBox15.Controls.Add(this.txtMonitoring_H3_Temp);
            this.groupBox15.Controls.Add(this.label32);
            this.groupBox15.Controls.Add(this.label31);
            this.groupBox15.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox15.ForeColor = System.Drawing.Color.White;
            this.groupBox15.Location = new System.Drawing.Point(17, 709);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(830, 120);
            this.groupBox15.TabIndex = 9;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "H3 - Ambient Air";
            // 
            // txtMonitoring_H3_Dewpoint
            // 
            this.txtMonitoring_H3_Dewpoint.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMonitoring_H3_Dewpoint.Font = new System.Drawing.Font("맑은 고딕", 14F, System.Drawing.FontStyle.Bold);
            this.txtMonitoring_H3_Dewpoint.Location = new System.Drawing.Point(646, 75);
            this.txtMonitoring_H3_Dewpoint.Name = "txtMonitoring_H3_Dewpoint";
            this.txtMonitoring_H3_Dewpoint.Size = new System.Drawing.Size(121, 32);
            this.txtMonitoring_H3_Dewpoint.TabIndex = 44;
            // 
            // txtMonitoring_H3_AmbientAir_light
            // 
            this.txtMonitoring_H3_AmbientAir_light.BackColor = System.Drawing.Color.Red;
            this.txtMonitoring_H3_AmbientAir_light.Font = new System.Drawing.Font("맑은 고딕", 15F, System.Drawing.FontStyle.Bold);
            this.txtMonitoring_H3_AmbientAir_light.Location = new System.Drawing.Point(24, 50);
            this.txtMonitoring_H3_AmbientAir_light.Name = "txtMonitoring_H3_AmbientAir_light";
            this.txtMonitoring_H3_AmbientAir_light.Size = new System.Drawing.Size(37, 34);
            this.txtMonitoring_H3_AmbientAir_light.TabIndex = 6;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(545, 79);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(84, 21);
            this.label30.TabIndex = 43;
            this.label30.Text = "Dewpoint";
            // 
            // txtMonitoring_H3_Humidity
            // 
            this.txtMonitoring_H3_Humidity.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMonitoring_H3_Humidity.Font = new System.Drawing.Font("맑은 고딕", 14F, System.Drawing.FontStyle.Bold);
            this.txtMonitoring_H3_Humidity.Location = new System.Drawing.Point(270, 28);
            this.txtMonitoring_H3_Humidity.Name = "txtMonitoring_H3_Humidity";
            this.txtMonitoring_H3_Humidity.Size = new System.Drawing.Size(121, 32);
            this.txtMonitoring_H3_Humidity.TabIndex = 40;
            // 
            // txtMonitoring_H3_Temp
            // 
            this.txtMonitoring_H3_Temp.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMonitoring_H3_Temp.Font = new System.Drawing.Font("맑은 고딕", 14F, System.Drawing.FontStyle.Bold);
            this.txtMonitoring_H3_Temp.Location = new System.Drawing.Point(270, 75);
            this.txtMonitoring_H3_Temp.Name = "txtMonitoring_H3_Temp";
            this.txtMonitoring_H3_Temp.Size = new System.Drawing.Size(121, 32);
            this.txtMonitoring_H3_Temp.TabIndex = 42;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(169, 32);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(80, 21);
            this.label32.TabIndex = 39;
            this.label32.Text = "Humidity";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(169, 79);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(53, 21);
            this.label31.TabIndex = 41;
            this.label31.Text = "Temp";
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.txtMonitoring_H2_Dewpoint);
            this.groupBox14.Controls.Add(this.txtMonitoring_H2_HarmonicModuleAir_light);
            this.groupBox14.Controls.Add(this.label27);
            this.groupBox14.Controls.Add(this.txtMonitoring_H2_Humidity);
            this.groupBox14.Controls.Add(this.txtMonitoring_H2_Temp);
            this.groupBox14.Controls.Add(this.label29);
            this.groupBox14.Controls.Add(this.label28);
            this.groupBox14.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox14.ForeColor = System.Drawing.Color.White;
            this.groupBox14.Location = new System.Drawing.Point(17, 576);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(830, 120);
            this.groupBox14.TabIndex = 9;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "H2 - Harmonic Module Air";
            // 
            // txtMonitoring_H2_Dewpoint
            // 
            this.txtMonitoring_H2_Dewpoint.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMonitoring_H2_Dewpoint.Enabled = false;
            this.txtMonitoring_H2_Dewpoint.Font = new System.Drawing.Font("맑은 고딕", 14F, System.Drawing.FontStyle.Bold);
            this.txtMonitoring_H2_Dewpoint.Location = new System.Drawing.Point(646, 75);
            this.txtMonitoring_H2_Dewpoint.Name = "txtMonitoring_H2_Dewpoint";
            this.txtMonitoring_H2_Dewpoint.Size = new System.Drawing.Size(121, 32);
            this.txtMonitoring_H2_Dewpoint.TabIndex = 38;
            // 
            // txtMonitoring_H2_HarmonicModuleAir_light
            // 
            this.txtMonitoring_H2_HarmonicModuleAir_light.BackColor = System.Drawing.Color.Red;
            this.txtMonitoring_H2_HarmonicModuleAir_light.Font = new System.Drawing.Font("맑은 고딕", 15F, System.Drawing.FontStyle.Bold);
            this.txtMonitoring_H2_HarmonicModuleAir_light.Location = new System.Drawing.Point(24, 51);
            this.txtMonitoring_H2_HarmonicModuleAir_light.Name = "txtMonitoring_H2_HarmonicModuleAir_light";
            this.txtMonitoring_H2_HarmonicModuleAir_light.Size = new System.Drawing.Size(37, 34);
            this.txtMonitoring_H2_HarmonicModuleAir_light.TabIndex = 5;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(545, 79);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(84, 21);
            this.label27.TabIndex = 37;
            this.label27.Text = "Dewpoint";
            // 
            // txtMonitoring_H2_Humidity
            // 
            this.txtMonitoring_H2_Humidity.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMonitoring_H2_Humidity.Enabled = false;
            this.txtMonitoring_H2_Humidity.Font = new System.Drawing.Font("맑은 고딕", 14F, System.Drawing.FontStyle.Bold);
            this.txtMonitoring_H2_Humidity.Location = new System.Drawing.Point(270, 28);
            this.txtMonitoring_H2_Humidity.Name = "txtMonitoring_H2_Humidity";
            this.txtMonitoring_H2_Humidity.Size = new System.Drawing.Size(121, 32);
            this.txtMonitoring_H2_Humidity.TabIndex = 34;
            // 
            // txtMonitoring_H2_Temp
            // 
            this.txtMonitoring_H2_Temp.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMonitoring_H2_Temp.Enabled = false;
            this.txtMonitoring_H2_Temp.Font = new System.Drawing.Font("맑은 고딕", 14F, System.Drawing.FontStyle.Bold);
            this.txtMonitoring_H2_Temp.Location = new System.Drawing.Point(270, 75);
            this.txtMonitoring_H2_Temp.Name = "txtMonitoring_H2_Temp";
            this.txtMonitoring_H2_Temp.Size = new System.Drawing.Size(121, 32);
            this.txtMonitoring_H2_Temp.TabIndex = 36;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(169, 32);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(80, 21);
            this.label29.TabIndex = 33;
            this.label29.Text = "Humidity";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(169, 79);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(53, 21);
            this.label28.TabIndex = 35;
            this.label28.Text = "Temp";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.txtMonitoring_H1_Amplifier_Dewpoint);
            this.groupBox13.Controls.Add(this.label26);
            this.groupBox13.Controls.Add(this.txtMonitoring_H1_Amplifier_Temp);
            this.groupBox13.Controls.Add(this.label25);
            this.groupBox13.Controls.Add(this.txtMonitoring_H1_Amplifier_Humidity);
            this.groupBox13.Controls.Add(this.txtMonitoring_H1_Amplifier_light);
            this.groupBox13.Controls.Add(this.label24);
            this.groupBox13.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox13.ForeColor = System.Drawing.Color.White;
            this.groupBox13.Location = new System.Drawing.Point(17, 443);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(830, 120);
            this.groupBox13.TabIndex = 9;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "H1 - Amplifier";
            // 
            // txtMonitoring_H1_Amplifier_Dewpoint
            // 
            this.txtMonitoring_H1_Amplifier_Dewpoint.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMonitoring_H1_Amplifier_Dewpoint.Enabled = false;
            this.txtMonitoring_H1_Amplifier_Dewpoint.Font = new System.Drawing.Font("맑은 고딕", 14F, System.Drawing.FontStyle.Bold);
            this.txtMonitoring_H1_Amplifier_Dewpoint.Location = new System.Drawing.Point(646, 77);
            this.txtMonitoring_H1_Amplifier_Dewpoint.Name = "txtMonitoring_H1_Amplifier_Dewpoint";
            this.txtMonitoring_H1_Amplifier_Dewpoint.Size = new System.Drawing.Size(121, 32);
            this.txtMonitoring_H1_Amplifier_Dewpoint.TabIndex = 32;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(545, 80);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(84, 21);
            this.label26.TabIndex = 31;
            this.label26.Text = "Dewpoint";
            // 
            // txtMonitoring_H1_Amplifier_Temp
            // 
            this.txtMonitoring_H1_Amplifier_Temp.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMonitoring_H1_Amplifier_Temp.Enabled = false;
            this.txtMonitoring_H1_Amplifier_Temp.Font = new System.Drawing.Font("맑은 고딕", 14F, System.Drawing.FontStyle.Bold);
            this.txtMonitoring_H1_Amplifier_Temp.Location = new System.Drawing.Point(270, 77);
            this.txtMonitoring_H1_Amplifier_Temp.Name = "txtMonitoring_H1_Amplifier_Temp";
            this.txtMonitoring_H1_Amplifier_Temp.Size = new System.Drawing.Size(121, 32);
            this.txtMonitoring_H1_Amplifier_Temp.TabIndex = 30;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(169, 81);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(53, 21);
            this.label25.TabIndex = 29;
            this.label25.Text = "Temp";
            // 
            // txtMonitoring_H1_Amplifier_Humidity
            // 
            this.txtMonitoring_H1_Amplifier_Humidity.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMonitoring_H1_Amplifier_Humidity.Enabled = false;
            this.txtMonitoring_H1_Amplifier_Humidity.Font = new System.Drawing.Font("맑은 고딕", 14F, System.Drawing.FontStyle.Bold);
            this.txtMonitoring_H1_Amplifier_Humidity.Location = new System.Drawing.Point(270, 30);
            this.txtMonitoring_H1_Amplifier_Humidity.Name = "txtMonitoring_H1_Amplifier_Humidity";
            this.txtMonitoring_H1_Amplifier_Humidity.Size = new System.Drawing.Size(121, 32);
            this.txtMonitoring_H1_Amplifier_Humidity.TabIndex = 28;
            // 
            // txtMonitoring_H1_Amplifier_light
            // 
            this.txtMonitoring_H1_Amplifier_light.BackColor = System.Drawing.Color.Red;
            this.txtMonitoring_H1_Amplifier_light.Font = new System.Drawing.Font("맑은 고딕", 15F, System.Drawing.FontStyle.Bold);
            this.txtMonitoring_H1_Amplifier_light.Location = new System.Drawing.Point(24, 50);
            this.txtMonitoring_H1_Amplifier_light.Name = "txtMonitoring_H1_Amplifier_light";
            this.txtMonitoring_H1_Amplifier_light.Size = new System.Drawing.Size(37, 34);
            this.txtMonitoring_H1_Amplifier_light.TabIndex = 4;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(169, 34);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(80, 21);
            this.label24.TabIndex = 27;
            this.label24.Text = "Humidity";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.txtMonitoring_HousingTemperature2_Temp);
            this.groupBox12.Controls.Add(this.txtMonitoring_HousingTemperature2_light);
            this.groupBox12.Controls.Add(this.label23);
            this.groupBox12.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox12.ForeColor = System.Drawing.Color.White;
            this.groupBox12.Location = new System.Drawing.Point(17, 310);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(830, 120);
            this.groupBox12.TabIndex = 9;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Housing Temperature 2";
            // 
            // txtMonitoring_HousingTemperature2_Temp
            // 
            this.txtMonitoring_HousingTemperature2_Temp.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMonitoring_HousingTemperature2_Temp.Enabled = false;
            this.txtMonitoring_HousingTemperature2_Temp.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.txtMonitoring_HousingTemperature2_Temp.Location = new System.Drawing.Point(270, 59);
            this.txtMonitoring_HousingTemperature2_Temp.Name = "txtMonitoring_HousingTemperature2_Temp";
            this.txtMonitoring_HousingTemperature2_Temp.Size = new System.Drawing.Size(121, 25);
            this.txtMonitoring_HousingTemperature2_Temp.TabIndex = 26;
            // 
            // txtMonitoring_HousingTemperature2_light
            // 
            this.txtMonitoring_HousingTemperature2_light.BackColor = System.Drawing.Color.Red;
            this.txtMonitoring_HousingTemperature2_light.Font = new System.Drawing.Font("맑은 고딕", 15F, System.Drawing.FontStyle.Bold);
            this.txtMonitoring_HousingTemperature2_light.Location = new System.Drawing.Point(24, 53);
            this.txtMonitoring_HousingTemperature2_light.Name = "txtMonitoring_HousingTemperature2_light";
            this.txtMonitoring_HousingTemperature2_light.Size = new System.Drawing.Size(37, 34);
            this.txtMonitoring_HousingTemperature2_light.TabIndex = 3;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(169, 57);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(53, 21);
            this.label23.TabIndex = 25;
            this.label23.Text = "Temp";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.txtMonitoring_HousingTemperature1_Temp);
            this.groupBox11.Controls.Add(this.txtMonitoring_HousingTemperature1_light);
            this.groupBox11.Controls.Add(this.label22);
            this.groupBox11.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox11.ForeColor = System.Drawing.Color.White;
            this.groupBox11.Location = new System.Drawing.Point(17, 177);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(830, 120);
            this.groupBox11.TabIndex = 9;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Housing Temperature 1";
            // 
            // txtMonitoring_HousingTemperature1_Temp
            // 
            this.txtMonitoring_HousingTemperature1_Temp.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMonitoring_HousingTemperature1_Temp.Enabled = false;
            this.txtMonitoring_HousingTemperature1_Temp.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.txtMonitoring_HousingTemperature1_Temp.Location = new System.Drawing.Point(270, 59);
            this.txtMonitoring_HousingTemperature1_Temp.Name = "txtMonitoring_HousingTemperature1_Temp";
            this.txtMonitoring_HousingTemperature1_Temp.Size = new System.Drawing.Size(121, 25);
            this.txtMonitoring_HousingTemperature1_Temp.TabIndex = 24;
            // 
            // txtMonitoring_HousingTemperature1_light
            // 
            this.txtMonitoring_HousingTemperature1_light.BackColor = System.Drawing.Color.Red;
            this.txtMonitoring_HousingTemperature1_light.Font = new System.Drawing.Font("맑은 고딕", 15F, System.Drawing.FontStyle.Bold);
            this.txtMonitoring_HousingTemperature1_light.Location = new System.Drawing.Point(21, 50);
            this.txtMonitoring_HousingTemperature1_light.Name = "txtMonitoring_HousingTemperature1_light";
            this.txtMonitoring_HousingTemperature1_light.Size = new System.Drawing.Size(37, 34);
            this.txtMonitoring_HousingTemperature1_light.TabIndex = 2;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(169, 57);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(53, 21);
            this.label22.TabIndex = 23;
            this.label22.Text = "Temp";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.txtMonitoring_WaterMonitor_Flow);
            this.groupBox10.Controls.Add(this.label21);
            this.groupBox10.Controls.Add(this.txtMonitoring_WaterMonitor_Temp);
            this.groupBox10.Controls.Add(this.label20);
            this.groupBox10.Controls.Add(this.txtMonitoring_WaterMonitor_light);
            this.groupBox10.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox10.ForeColor = System.Drawing.Color.White;
            this.groupBox10.Location = new System.Drawing.Point(17, 44);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(830, 120);
            this.groupBox10.TabIndex = 8;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Water Monitor";
            // 
            // txtMonitoring_WaterMonitor_Flow
            // 
            this.txtMonitoring_WaterMonitor_Flow.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMonitoring_WaterMonitor_Flow.Enabled = false;
            this.txtMonitoring_WaterMonitor_Flow.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.txtMonitoring_WaterMonitor_Flow.Location = new System.Drawing.Point(646, 58);
            this.txtMonitoring_WaterMonitor_Flow.Name = "txtMonitoring_WaterMonitor_Flow";
            this.txtMonitoring_WaterMonitor_Flow.Size = new System.Drawing.Size(121, 25);
            this.txtMonitoring_WaterMonitor_Flow.TabIndex = 22;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(545, 58);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(45, 21);
            this.label21.TabIndex = 21;
            this.label21.Text = "Flow";
            // 
            // txtMonitoring_WaterMonitor_Temp
            // 
            this.txtMonitoring_WaterMonitor_Temp.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtMonitoring_WaterMonitor_Temp.Enabled = false;
            this.txtMonitoring_WaterMonitor_Temp.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.txtMonitoring_WaterMonitor_Temp.Location = new System.Drawing.Point(270, 56);
            this.txtMonitoring_WaterMonitor_Temp.Name = "txtMonitoring_WaterMonitor_Temp";
            this.txtMonitoring_WaterMonitor_Temp.Size = new System.Drawing.Size(121, 25);
            this.txtMonitoring_WaterMonitor_Temp.TabIndex = 20;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(169, 54);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(53, 21);
            this.label20.TabIndex = 19;
            this.label20.Text = "Temp";
            // 
            // txtMonitoring_WaterMonitor_light
            // 
            this.txtMonitoring_WaterMonitor_light.BackColor = System.Drawing.Color.Red;
            this.txtMonitoring_WaterMonitor_light.Font = new System.Drawing.Font("맑은 고딕", 15F, System.Drawing.FontStyle.Bold);
            this.txtMonitoring_WaterMonitor_light.Location = new System.Drawing.Point(24, 51);
            this.txtMonitoring_WaterMonitor_light.Name = "txtMonitoring_WaterMonitor_light";
            this.txtMonitoring_WaterMonitor_light.Size = new System.Drawing.Size(37, 34);
            this.txtMonitoring_WaterMonitor_light.TabIndex = 1;
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel25.Controls.Add(this.label33);
            this.panel25.Controls.Add(this.btn_Main_Lock);
            this.panel25.Controls.Add(this.txtMain_Lock);
            this.panel25.ForeColor = System.Drawing.Color.White;
            this.panel25.Location = new System.Drawing.Point(3, 4);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(859, 38);
            this.panel25.TabIndex = 60;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold);
            this.label33.ForeColor = System.Drawing.Color.White;
            this.label33.Location = new System.Drawing.Point(392, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(73, 32);
            this.label33.TabIndex = 53;
            this.label33.Text = "Main";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel3.Controls.Add(this.label34);
            this.panel3.ForeColor = System.Drawing.Color.White;
            this.panel3.Location = new System.Drawing.Point(3, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(858, 38);
            this.panel3.TabIndex = 60;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold);
            this.label34.ForeColor = System.Drawing.Color.White;
            this.label34.Location = new System.Drawing.Point(356, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(145, 32);
            this.label34.TabIndex = 53;
            this.label34.Text = "Monitoring";
            // 
            // Manager_Laser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Manager_Laser";
            this.Size = new System.Drawing.Size(1740, 875);
            this.panel1.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.panel25.ResumeLayout(false);
            this.panel25.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.TextBox txtMain_PD7Value_PD7Power_ReadOnly;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtMain_PD7Value_OutputPower_ReadOnly;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btn_Main_Shutter_Close;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtMain_Shutter_light;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtMain_ModulatorPulseEnergyControl_External_light;
        private System.Windows.Forms.TextBox txtMain_ModulatorPulseEnergyControl_Internal_light;
        private System.Windows.Forms.TextBox txtMain_ModulatorPulseEnergyControl_ReadOnly;
        private System.Windows.Forms.Button btn_Main_ModulatorPulseEnergyControl_Set;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtMain_ModulatorPulseEnergyControl;
        private System.Windows.Forms.TextBox txtMain_AttenuatorPowerControl_AttenuatorPower_ReadOnly;
        private System.Windows.Forms.Button btn_Main_AttenuatorPowerControl_AttenuatorPower_Set;
        private System.Windows.Forms.TextBox txtMain_AttenuatorPowerControl_AttenuatorPower;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtMain_AttenuatorPowerControl_ClosedLoop_light;
        private System.Windows.Forms.TextBox txtMain_AttenuatorPowerControl_Transmission_ReadOnly;
        private System.Windows.Forms.Button btn_Main_AttenuatorPowerControl_Transmission_Set;
        private System.Windows.Forms.TextBox txtMain_AttenuatorPowerControl_Transmission;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtMain_AttenuatorPowerControl_OpenLoop_light;
        private System.Windows.Forms.TextBox txtMain_TimingAndFrequency_Burst_ReadOnly;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtMain_TimingAndFrequency_Burst;
        private System.Windows.Forms.TextBox txtMain_TimingAndFrequency_PulseMode_ReadOnly;
        private System.Windows.Forms.ComboBox cbxtMain_TimingAndFrequency_PulseMode;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbxtMain_TimingAndFrequency_Amplifier;
        private System.Windows.Forms.TextBox txtMain_TimingAndFrequency_Output_ReadOnly;
        private System.Windows.Forms.TextBox txtMain_TimingAndFrequency_Divider_ReadOnly;
        private System.Windows.Forms.TextBox txtMain_TimingAndFrequency_Divider;
        private System.Windows.Forms.TextBox txtMain_TimingAndFrequency_Amplifier_ReadOnly;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btn_Main_TimingAndFrequency_PulseMode_Set;
        private System.Windows.Forms.Button btn_Main_TimingAndFrequency_Burst_Set;
        private System.Windows.Forms.Button btn_Main_TimingAndFrequency_Divider_Set;
        private System.Windows.Forms.Button btn_Main_TimingAndFrequency_Amplifier_Set;
        private System.Windows.Forms.Button btn_Main_SystemStatus_Stop;
        private System.Windows.Forms.Button btn_Main_SystemStatus_Start;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtMain_SystemStatus_light;
        private System.Windows.Forms.Button btn_Main_SystemFaults_Clear;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtMain_SystemFaults_light;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtMain_KeySwitch_light;
        private System.Windows.Forms.Button btn_Main_ConnectionToLaser_DisConnection;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtMain_ConnectionToLaser_light;
        private System.Windows.Forms.TextBox txtMonitoring_H3_Dewpoint;
        private System.Windows.Forms.TextBox txtMonitoring_H3_AmbientAir_light;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtMonitoring_H3_Humidity;
        private System.Windows.Forms.TextBox txtMonitoring_H3_Temp;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtMonitoring_H2_Dewpoint;
        private System.Windows.Forms.TextBox txtMonitoring_H2_HarmonicModuleAir_light;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtMonitoring_H2_Humidity;
        private System.Windows.Forms.TextBox txtMonitoring_H2_Temp;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtMonitoring_H1_Amplifier_Dewpoint;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtMonitoring_H1_Amplifier_Temp;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtMonitoring_H1_Amplifier_Humidity;
        private System.Windows.Forms.TextBox txtMonitoring_H1_Amplifier_light;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtMonitoring_HousingTemperature2_Temp;
        private System.Windows.Forms.TextBox txtMonitoring_HousingTemperature2_light;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtMonitoring_HousingTemperature1_Temp;
        private System.Windows.Forms.TextBox txtMonitoring_HousingTemperature1_light;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtMonitoring_WaterMonitor_Flow;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtMonitoring_WaterMonitor_Temp;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtMonitoring_WaterMonitor_light;
        private System.Windows.Forms.Button btn_Main_Lock;
        private System.Windows.Forms.TextBox txtMain_Lock;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label34;
    }
}
