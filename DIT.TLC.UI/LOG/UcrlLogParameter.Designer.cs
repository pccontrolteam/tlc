﻿namespace DIT.TLC.UI.LOG
{
    partial class Log_Parameter
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvParameter = new System.Windows.Forms.ListView();
            this.colParameterDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colParameterTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colParameterChangeContents = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colParameterChangeValue = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colParameterChangeUser = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.calendarParameter = new DIT.TLC.UI.LOG.calendar();
            this.SuspendLayout();
            // 
            // lvParameter
            // 
            this.lvParameter.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colParameterDate,
            this.colParameterTime,
            this.colParameterChangeContents,
            this.colParameterChangeValue,
            this.colParameterChangeUser});
            this.lvParameter.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lvParameter.GridLines = true;
            this.lvParameter.Location = new System.Drawing.Point(325, 21);
            this.lvParameter.Name = "lvParameter";
            this.lvParameter.Size = new System.Drawing.Size(1407, 648);
            this.lvParameter.TabIndex = 3;
            this.lvParameter.UseCompatibleStateImageBehavior = false;
            this.lvParameter.View = System.Windows.Forms.View.Details;
            // 
            // colParameterDate
            // 
            this.colParameterDate.Text = "날짜";
            this.colParameterDate.Width = 150;
            // 
            // colParameterTime
            // 
            this.colParameterTime.Text = "시간";
            this.colParameterTime.Width = 150;
            // 
            // colParameterChangeContents
            // 
            this.colParameterChangeContents.Text = "변경한 항목";
            this.colParameterChangeContents.Width = 477;
            // 
            // colParameterChangeValue
            // 
            this.colParameterChangeValue.Text = "변경한 값";
            this.colParameterChangeValue.Width = 398;
            // 
            // colParameterChangeUser
            // 
            this.colParameterChangeUser.Text = "변경한 사용자";
            this.colParameterChangeUser.Width = 219;
            // 
            // calendarParameter
            // 
            this.calendarParameter.BackColor = System.Drawing.Color.DimGray;
            this.calendarParameter.Location = new System.Drawing.Point(9, 2);
            this.calendarParameter.Name = "calendarParameter";
            this.calendarParameter.Size = new System.Drawing.Size(320, 870);
            this.calendarParameter.TabIndex = 2;
            // 
            // Log_Parameter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.Controls.Add(this.lvParameter);
            this.Controls.Add(this.calendarParameter);
            this.Name = "Log_Parameter";
            this.Size = new System.Drawing.Size(1740, 875);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lvParameter;
        private System.Windows.Forms.ColumnHeader colParameterDate;
        private System.Windows.Forms.ColumnHeader colParameterTime;
        private System.Windows.Forms.ColumnHeader colParameterChangeContents;
        private System.Windows.Forms.ColumnHeader colParameterChangeValue;
        private System.Windows.Forms.ColumnHeader colParameterChangeUser;
        private calendar calendarParameter;
    }
}
