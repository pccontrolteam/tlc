﻿namespace DIT.TLC.UI.LOG
{
    partial class LogOpcall
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvOpcall = new System.Windows.Forms.ListView();
            this.colOpcallDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colOpcallTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colOpcallMessage = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.calendarOpcall = new DIT.TLC.UI.LOG.calendar();
            this.SuspendLayout();
            // 
            // lvOpcall
            // 
            this.lvOpcall.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colOpcallDate,
            this.colOpcallTime,
            this.colOpcallMessage});
            this.lvOpcall.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lvOpcall.GridLines = true;
            this.lvOpcall.Location = new System.Drawing.Point(325, 21);
            this.lvOpcall.Name = "lvOpcall";
            this.lvOpcall.Size = new System.Drawing.Size(1407, 648);
            this.lvOpcall.TabIndex = 3;
            this.lvOpcall.UseCompatibleStateImageBehavior = false;
            this.lvOpcall.View = System.Windows.Forms.View.Details;
            // 
            // colOpcallDate
            // 
            this.colOpcallDate.Text = "날짜";
            this.colOpcallDate.Width = 150;
            // 
            // colOpcallTime
            // 
            this.colOpcallTime.Text = "시간";
            this.colOpcallTime.Width = 150;
            // 
            // colOpcallMessage
            // 
            this.colOpcallMessage.Text = "메시지";
            this.colOpcallMessage.Width = 1090;
            // 
            // calendarOpcall
            // 
            this.calendarOpcall.BackColor = System.Drawing.Color.DimGray;
            this.calendarOpcall.Location = new System.Drawing.Point(9, 2);
            this.calendarOpcall.Name = "calendarOpcall";
            this.calendarOpcall.Size = new System.Drawing.Size(320, 870);
            this.calendarOpcall.TabIndex = 2;
            // 
            // LogOpcall
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.Controls.Add(this.lvOpcall);
            this.Controls.Add(this.calendarOpcall);
            this.Name = "LogOpcall";
            this.Size = new System.Drawing.Size(1740, 875);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lvOpcall;
        private System.Windows.Forms.ColumnHeader colOpcallDate;
        private System.Windows.Forms.ColumnHeader colOpcallTime;
        private System.Windows.Forms.ColumnHeader colOpcallMessage;
        private calendar calendarOpcall;
    }
}
