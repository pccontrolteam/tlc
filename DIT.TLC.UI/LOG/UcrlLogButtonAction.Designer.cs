﻿namespace DIT.TLC.UI.LOG
{
    partial class LogButtonAction
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvButtonAction = new System.Windows.Forms.ListView();
            this.colButtonActionDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colButtonActionTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colButtonActionPushPosition = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colButtonActionPushBtn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colButtonActionUser = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.calendarButtonAction = new DIT.TLC.UI.LOG.calendar();
            this.SuspendLayout();
            // 
            // lvButtonAction
            // 
            this.lvButtonAction.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colButtonActionDate,
            this.colButtonActionTime,
            this.colButtonActionPushPosition,
            this.colButtonActionPushBtn,
            this.colButtonActionUser});
            this.lvButtonAction.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lvButtonAction.GridLines = true;
            this.lvButtonAction.Location = new System.Drawing.Point(325, 21);
            this.lvButtonAction.Name = "lvButtonAction";
            this.lvButtonAction.Size = new System.Drawing.Size(1407, 648);
            this.lvButtonAction.TabIndex = 3;
            this.lvButtonAction.UseCompatibleStateImageBehavior = false;
            this.lvButtonAction.View = System.Windows.Forms.View.Details;
            // 
            // colButtonActionDate
            // 
            this.colButtonActionDate.Text = "날짜";
            this.colButtonActionDate.Width = 150;
            // 
            // colButtonActionTime
            // 
            this.colButtonActionTime.Text = "시간";
            this.colButtonActionTime.Width = 150;
            // 
            // colButtonActionPushPosition
            // 
            this.colButtonActionPushPosition.Text = "누른 위치";
            this.colButtonActionPushPosition.Width = 495;
            // 
            // colButtonActionPushBtn
            // 
            this.colButtonActionPushBtn.Text = "누른 버튼";
            this.colButtonActionPushBtn.Width = 377;
            // 
            // colButtonActionUser
            // 
            this.colButtonActionUser.Text = "누른 사용자";
            this.colButtonActionUser.Width = 210;
            // 
            // calendarButtonAction
            // 
            this.calendarButtonAction.BackColor = System.Drawing.Color.DimGray;
            this.calendarButtonAction.Location = new System.Drawing.Point(9, 2);
            this.calendarButtonAction.Name = "calendarButtonAction";
            this.calendarButtonAction.Size = new System.Drawing.Size(320, 870);
            this.calendarButtonAction.TabIndex = 2;
            // 
            // LogButtonAction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.Controls.Add(this.lvButtonAction);
            this.Controls.Add(this.calendarButtonAction);
            this.Name = "LogButtonAction";
            this.Size = new System.Drawing.Size(1740, 875);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lvButtonAction;
        private System.Windows.Forms.ColumnHeader colButtonActionDate;
        private System.Windows.Forms.ColumnHeader colButtonActionTime;
        private System.Windows.Forms.ColumnHeader colButtonActionPushPosition;
        private System.Windows.Forms.ColumnHeader colButtonActionPushBtn;
        private System.Windows.Forms.ColumnHeader colButtonActionUser;
        private calendar calendarButtonAction;
    }
}
