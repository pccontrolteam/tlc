﻿namespace DIT.TLC.UI.LOG
{
    partial class LogMeasure
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvMeasure = new System.Windows.Forms.ListView();
            this.colMeasureDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colMeasureTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colMeasurePD7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colMeasurePower = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colMeasureAvg = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.calendarMeasure = new DIT.TLC.UI.LOG.calendar();
            this.SuspendLayout();
            // 
            // lvMeasure
            // 
            this.lvMeasure.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colMeasureDate,
            this.colMeasureTime,
            this.colMeasurePD7,
            this.colMeasurePower,
            this.colMeasureAvg});
            this.lvMeasure.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lvMeasure.GridLines = true;
            this.lvMeasure.Location = new System.Drawing.Point(325, 21);
            this.lvMeasure.Name = "lvMeasure";
            this.lvMeasure.Size = new System.Drawing.Size(1407, 648);
            this.lvMeasure.TabIndex = 3;
            this.lvMeasure.UseCompatibleStateImageBehavior = false;
            this.lvMeasure.View = System.Windows.Forms.View.Details;
            // 
            // colMeasureDate
            // 
            this.colMeasureDate.Text = "날짜";
            this.colMeasureDate.Width = 150;
            // 
            // colMeasureTime
            // 
            this.colMeasureTime.Text = "시간";
            this.colMeasureTime.Width = 150;
            // 
            // colMeasurePD7
            // 
            this.colMeasurePD7.Text = "PD 7";
            this.colMeasurePD7.Width = 226;
            // 
            // colMeasurePower
            // 
            this.colMeasurePower.Text = "출력 파워(%)";
            this.colMeasurePower.Width = 274;
            // 
            // colMeasureAvg
            // 
            this.colMeasureAvg.Text = "Avg";
            this.colMeasureAvg.Width = 288;
            // 
            // calendarMeasure
            // 
            this.calendarMeasure.BackColor = System.Drawing.Color.DimGray;
            this.calendarMeasure.Location = new System.Drawing.Point(9, 2);
            this.calendarMeasure.Name = "calendarMeasure";
            this.calendarMeasure.Size = new System.Drawing.Size(320, 870);
            this.calendarMeasure.TabIndex = 2;
            // 
            // LogMeasure
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.Controls.Add(this.lvMeasure);
            this.Controls.Add(this.calendarMeasure);
            this.Name = "LogMeasure";
            this.Size = new System.Drawing.Size(1740, 875);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lvMeasure;
        private System.Windows.Forms.ColumnHeader colMeasureDate;
        private System.Windows.Forms.ColumnHeader colMeasureTime;
        private System.Windows.Forms.ColumnHeader colMeasurePD7;
        private System.Windows.Forms.ColumnHeader colMeasurePower;
        private System.Windows.Forms.ColumnHeader colMeasureAvg;
        private calendar calendarMeasure;
    }
}
