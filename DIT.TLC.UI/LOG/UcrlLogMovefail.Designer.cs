﻿namespace DIT.TLC.UI.LOG
{
    partial class LogMovefail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvMovefail = new System.Windows.Forms.ListView();
            this.colMovefailDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colMovefailTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colMovefailNumber = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colMovefailTargetPosition = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colMovefailCurrentPosition = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colMovefailInpositionValue = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.calendarMovefail = new DIT.TLC.UI.LOG.calendar();
            this.SuspendLayout();
            // 
            // lvMovefail
            // 
            this.lvMovefail.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colMovefailDate,
            this.colMovefailTime,
            this.colMovefailNumber,
            this.colMovefailTargetPosition,
            this.colMovefailCurrentPosition,
            this.colMovefailInpositionValue});
            this.lvMovefail.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lvMovefail.GridLines = true;
            this.lvMovefail.Location = new System.Drawing.Point(325, 21);
            this.lvMovefail.Name = "lvMovefail";
            this.lvMovefail.Size = new System.Drawing.Size(1407, 648);
            this.lvMovefail.TabIndex = 3;
            this.lvMovefail.UseCompatibleStateImageBehavior = false;
            this.lvMovefail.View = System.Windows.Forms.View.Details;
            // 
            // colMovefailDate
            // 
            this.colMovefailDate.Text = "날짜";
            this.colMovefailDate.Width = 150;
            // 
            // colMovefailTime
            // 
            this.colMovefailTime.Text = "시간";
            this.colMovefailTime.Width = 150;
            // 
            // colMovefailNumber
            // 
            this.colMovefailNumber.Text = "축 번호";
            this.colMovefailNumber.Width = 150;
            // 
            // colMovefailTargetPosition
            // 
            this.colMovefailTargetPosition.Text = "타겟 위치";
            this.colMovefailTargetPosition.Width = 338;
            // 
            // colMovefailCurrentPosition
            // 
            this.colMovefailCurrentPosition.Text = "현재 위치";
            this.colMovefailCurrentPosition.Width = 296;
            // 
            // colMovefailInpositionValue
            // 
            this.colMovefailInpositionValue.Text = "인포지션 값";
            this.colMovefailInpositionValue.Width = 306;
            // 
            // calendarMovefail
            // 
            this.calendarMovefail.BackColor = System.Drawing.Color.DimGray;
            this.calendarMovefail.Location = new System.Drawing.Point(9, 2);
            this.calendarMovefail.Name = "calendarMovefail";
            this.calendarMovefail.Size = new System.Drawing.Size(320, 870);
            this.calendarMovefail.TabIndex = 2;
            // 
            // LogMovefail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.Controls.Add(this.lvMovefail);
            this.Controls.Add(this.calendarMovefail);
            this.Name = "LogMovefail";
            this.Size = new System.Drawing.Size(1740, 875);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lvMovefail;
        private System.Windows.Forms.ColumnHeader colMovefailDate;
        private System.Windows.Forms.ColumnHeader colMovefailTime;
        private System.Windows.Forms.ColumnHeader colMovefailNumber;
        private System.Windows.Forms.ColumnHeader colMovefailTargetPosition;
        private System.Windows.Forms.ColumnHeader colMovefailCurrentPosition;
        private System.Windows.Forms.ColumnHeader colMovefailInpositionValue;
        private calendar calendarMovefail;
    }
}
