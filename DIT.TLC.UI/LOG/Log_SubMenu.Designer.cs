﻿namespace DIT.TLC.UI.LOG
{
    partial class Log_SubMenu
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btn_Measure = new System.Windows.Forms.Button();
            this.btn_Movefail = new System.Windows.Forms.Button();
            this.btn_Opcall = new System.Windows.Forms.Button();
            this.btn_Parameter = new System.Windows.Forms.Button();
            this.btn_Alarm = new System.Windows.Forms.Button();
            this.btn_ButtonAction = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.DarkSlateGray;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.DarkSlateGray;
            this.splitContainer1.Panel2.Controls.Add(this.btn_Measure);
            this.splitContainer1.Panel2.Controls.Add(this.btn_Movefail);
            this.splitContainer1.Panel2.Controls.Add(this.btn_Opcall);
            this.splitContainer1.Panel2.Controls.Add(this.btn_Parameter);
            this.splitContainer1.Panel2.Controls.Add(this.btn_Alarm);
            this.splitContainer1.Panel2.Controls.Add(this.btn_ButtonAction);
            this.splitContainer1.Size = new System.Drawing.Size(1880, 860);
            this.splitContainer1.SplitterDistance = 1740;
            this.splitContainer1.TabIndex = 1;
            // 
            // btn_Measure
            // 
            this.btn_Measure.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_Measure.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Bold);
            this.btn_Measure.ForeColor = System.Drawing.Color.White;
            this.btn_Measure.Location = new System.Drawing.Point(2, 303);
            this.btn_Measure.Name = "btn_Measure";
            this.btn_Measure.Size = new System.Drawing.Size(132, 50);
            this.btn_Measure.TabIndex = 45;
            this.btn_Measure.Text = "측정 로그";
            this.btn_Measure.UseVisualStyleBackColor = false;
            this.btn_Measure.Click += new System.EventHandler(this.btn_Measure_Click);
            // 
            // btn_Movefail
            // 
            this.btn_Movefail.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_Movefail.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Bold);
            this.btn_Movefail.ForeColor = System.Drawing.Color.White;
            this.btn_Movefail.Location = new System.Drawing.Point(2, 247);
            this.btn_Movefail.Name = "btn_Movefail";
            this.btn_Movefail.Size = new System.Drawing.Size(132, 50);
            this.btn_Movefail.TabIndex = 44;
            this.btn_Movefail.Text = "이동실패 로그";
            this.btn_Movefail.UseVisualStyleBackColor = false;
            this.btn_Movefail.Click += new System.EventHandler(this.btn_Movefail_Click);
            // 
            // btn_Opcall
            // 
            this.btn_Opcall.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_Opcall.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Bold);
            this.btn_Opcall.ForeColor = System.Drawing.Color.White;
            this.btn_Opcall.Location = new System.Drawing.Point(2, 191);
            this.btn_Opcall.Name = "btn_Opcall";
            this.btn_Opcall.Size = new System.Drawing.Size(132, 50);
            this.btn_Opcall.TabIndex = 43;
            this.btn_Opcall.Text = "오피콜 로그";
            this.btn_Opcall.UseVisualStyleBackColor = false;
            this.btn_Opcall.Click += new System.EventHandler(this.btn_Opcall_Click);
            // 
            // btn_Parameter
            // 
            this.btn_Parameter.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_Parameter.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Bold);
            this.btn_Parameter.ForeColor = System.Drawing.Color.White;
            this.btn_Parameter.Location = new System.Drawing.Point(2, 135);
            this.btn_Parameter.Name = "btn_Parameter";
            this.btn_Parameter.Size = new System.Drawing.Size(132, 50);
            this.btn_Parameter.TabIndex = 42;
            this.btn_Parameter.Text = "파라미터 로그";
            this.btn_Parameter.UseVisualStyleBackColor = false;
            this.btn_Parameter.Click += new System.EventHandler(this.btn_Parameter_Click);
            // 
            // btn_Alarm
            // 
            this.btn_Alarm.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_Alarm.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Bold);
            this.btn_Alarm.ForeColor = System.Drawing.Color.White;
            this.btn_Alarm.Location = new System.Drawing.Point(2, 23);
            this.btn_Alarm.Name = "btn_Alarm";
            this.btn_Alarm.Size = new System.Drawing.Size(132, 50);
            this.btn_Alarm.TabIndex = 40;
            this.btn_Alarm.Text = "알람 로그";
            this.btn_Alarm.UseVisualStyleBackColor = false;
            this.btn_Alarm.Click += new System.EventHandler(this.btn_Alarm_Click);
            // 
            // btn_ButtonAction
            // 
            this.btn_ButtonAction.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_ButtonAction.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Bold);
            this.btn_ButtonAction.ForeColor = System.Drawing.Color.White;
            this.btn_ButtonAction.Location = new System.Drawing.Point(2, 79);
            this.btn_ButtonAction.Name = "btn_ButtonAction";
            this.btn_ButtonAction.Size = new System.Drawing.Size(132, 50);
            this.btn_ButtonAction.TabIndex = 41;
            this.btn_ButtonAction.Text = "버튼액션 로그";
            this.btn_ButtonAction.UseVisualStyleBackColor = false;
            this.btn_ButtonAction.Click += new System.EventHandler(this.btn_ButtonAction_Click);
            // 
            // Log_SubMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "Log_SubMenu";
            this.Size = new System.Drawing.Size(1880, 860);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btn_Measure;
        private System.Windows.Forms.Button btn_Movefail;
        private System.Windows.Forms.Button btn_Opcall;
        private System.Windows.Forms.Button btn_Parameter;
        private System.Windows.Forms.Button btn_Alarm;
        private System.Windows.Forms.Button btn_ButtonAction;
    }
}
