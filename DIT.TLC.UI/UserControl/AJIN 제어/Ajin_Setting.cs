﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DIT.TLC.CTRL;

namespace DIT.TLC.UI.AJIN_제어
{
    public partial class Ajin_Setting : UserControl
    {
        private ServoMotorControl Servo { get; set; }
        public Ajin_Setting()
        {
            InitializeComponent();
        }
        public void Update()
        {

        }

        private void btnMoveJogMinus_MouseUp(object sender, MouseEventArgs e)
        {
            Servo.JogMove(GG.Equip, EM_SERVO_JOG.JOG_STOP, 0);
        }
        private void btnMoveJogMinus_MouseDown(object sender, MouseEventArgs e)
        {
            float speed = ((float)nuJobSpeed.Value > (float)Servo.SoftJogSpeedLimit * 1000.0f ? (float)Servo.SoftJogSpeedLimit : (float)nuJobSpeed.Value / 1000.0f);
            Servo.JogMove(GG.Equip, EM_SERVO_JOG.JOG_MINUS, speed);
        }
        private void btnMoveJogPlus_MouseDown(object sender, MouseEventArgs e)
        {
            float speed = ((float)nuJobSpeed.Value > (float)Servo.SoftJogSpeedLimit * 1000.0f ? (float)Servo.SoftJogSpeedLimit : (float)nuJobSpeed.Value / 1000.0f);
            Servo.JogMove(GG.Equip, EM_SERVO_JOG.JOG_PLUS, speed);
        }
        private void btnMoveJogPlus_MouseUp(object sender, MouseEventArgs e)
        {
            Servo.JogMove(GG.Equip, EM_SERVO_JOG.JOG_STOP, 0);
        }
    }
}
