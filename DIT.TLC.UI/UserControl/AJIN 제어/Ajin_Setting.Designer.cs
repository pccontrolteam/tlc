﻿namespace DIT.TLC.UI.AJIN_제어
{
    partial class Ajin_Setting
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.btnMoveJogMinus = new System.Windows.Forms.Button();
            this.btnMoveJogPlus = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.nuJobSpeed = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.lblPosition = new System.Windows.Forms.Label();
            this.lblJogSpeed = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nuJobSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(17, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "현재 위치";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("맑은 고딕", 7F);
            this.button1.Location = new System.Drawing.Point(378, 35);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(59, 45);
            this.button1.TabIndex = 2;
            this.button1.Text = "SERVO ON";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("맑은 고딕", 9.5F, System.Drawing.FontStyle.Bold);
            this.button2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button2.Location = new System.Drawing.Point(332, 98);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(58, 62);
            this.button2.TabIndex = 3;
            this.button2.Text = "절대 이동";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.Lime;
            this.textBox2.Font = new System.Drawing.Font("굴림", 11F, System.Drawing.FontStyle.Bold);
            this.textBox2.ForeColor = System.Drawing.SystemColors.InfoText;
            this.textBox2.Location = new System.Drawing.Point(1, 3);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(94, 24);
            this.textBox2.TabIndex = 4;
            this.textBox2.Text = "AJIN";
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.SystemColors.Highlight;
            this.textBox3.Font = new System.Drawing.Font("굴림", 11F, System.Drawing.FontStyle.Bold);
            this.textBox3.ForeColor = System.Drawing.SystemColors.InfoText;
            this.textBox3.Location = new System.Drawing.Point(96, 3);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(748, 24);
            this.textBox3.TabIndex = 5;
            this.textBox3.Text = "AJIN";
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 8.25F);
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Location = new System.Drawing.Point(135, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "(mm)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("맑은 고딕", 8.25F);
            this.label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label3.Location = new System.Drawing.Point(187, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "현재 속도";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("맑은 고딕", 8.25F);
            this.label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.Location = new System.Drawing.Point(306, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "(mm/sec)";
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("맑은 고딕", 7F);
            this.button3.Location = new System.Drawing.Point(437, 35);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(59, 45);
            this.button3.TabIndex = 10;
            this.button3.Text = "SERVO OFF";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("맑은 고딕", 7F);
            this.button5.Location = new System.Drawing.Point(509, 35);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(59, 45);
            this.button5.TabIndex = 11;
            this.button5.Text = "SERVO ON";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("맑은 고딕", 7F);
            this.button4.Location = new System.Drawing.Point(567, 35);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(54, 45);
            this.button4.TabIndex = 12;
            this.button4.Text = "Home";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("맑은 고딕", 7F);
            this.button7.Location = new System.Drawing.Point(620, 35);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(54, 45);
            this.button7.TabIndex = 13;
            this.button7.Text = "Alarm";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("맑은 고딕", 7F);
            this.button6.Location = new System.Drawing.Point(673, 35);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(54, 45);
            this.button6.TabIndex = 14;
            this.button6.Text = "+Limit";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.Font = new System.Drawing.Font("맑은 고딕", 7F);
            this.button9.Location = new System.Drawing.Point(726, 35);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(54, 45);
            this.button9.TabIndex = 15;
            this.button9.Text = "-Limit";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("맑은 고딕", 7F);
            this.button8.Location = new System.Drawing.Point(779, 35);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(54, 45);
            this.button8.TabIndex = 16;
            this.button8.Text = "InPos";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label5.Location = new System.Drawing.Point(38, 92);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 21);
            this.label5.TabIndex = 17;
            this.label5.Text = "위치 설정";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label6.Location = new System.Drawing.Point(38, 138);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 21);
            this.label6.TabIndex = 18;
            this.label6.Text = "속도 설정";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("맑은 고딕", 8.25F);
            this.label7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label7.Location = new System.Drawing.Point(234, 99);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "(mm)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("맑은 고딕", 8.25F);
            this.label8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label8.Location = new System.Drawing.Point(235, 143);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 22;
            this.label8.Text = "(mm/sec)";
            // 
            // button10
            // 
            this.button10.Font = new System.Drawing.Font("맑은 고딕", 9.5F, System.Drawing.FontStyle.Bold);
            this.button10.Location = new System.Drawing.Point(411, 98);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(58, 62);
            this.button10.TabIndex = 23;
            this.button10.Text = "  -   상대 이동";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // button11
            // 
            this.button11.Font = new System.Drawing.Font("맑은 고딕", 9.5F, System.Drawing.FontStyle.Bold);
            this.button11.Location = new System.Drawing.Point(471, 98);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(58, 62);
            this.button11.TabIndex = 24;
            this.button11.Text = "  +   상대 이동";
            this.button11.UseVisualStyleBackColor = true;
            // 
            // btnMoveJogMinus
            // 
            this.btnMoveJogMinus.Font = new System.Drawing.Font("맑은 고딕", 9.5F, System.Drawing.FontStyle.Bold);
            this.btnMoveJogMinus.Location = new System.Drawing.Point(548, 98);
            this.btnMoveJogMinus.Name = "btnMoveJogMinus";
            this.btnMoveJogMinus.Size = new System.Drawing.Size(58, 62);
            this.btnMoveJogMinus.TabIndex = 25;
            this.btnMoveJogMinus.Text = "  -   조그 이동";
            this.btnMoveJogMinus.UseVisualStyleBackColor = true;
            this.btnMoveJogMinus.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnMoveJogMinus_MouseDown);
            this.btnMoveJogMinus.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnMoveJogMinus_MouseUp);
            // 
            // btnMoveJogPlus
            // 
            this.btnMoveJogPlus.Font = new System.Drawing.Font("맑은 고딕", 9.5F, System.Drawing.FontStyle.Bold);
            this.btnMoveJogPlus.Location = new System.Drawing.Point(608, 98);
            this.btnMoveJogPlus.Name = "btnMoveJogPlus";
            this.btnMoveJogPlus.Size = new System.Drawing.Size(58, 62);
            this.btnMoveJogPlus.TabIndex = 26;
            this.btnMoveJogPlus.Text = "  +   조그 이동";
            this.btnMoveJogPlus.UseVisualStyleBackColor = true;
            this.btnMoveJogPlus.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnMoveJogPlus_MouseDown);
            this.btnMoveJogPlus.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnMoveJogPlus_MouseUp);
            // 
            // button14
            // 
            this.button14.Font = new System.Drawing.Font("맑은 고딕", 9.5F, System.Drawing.FontStyle.Bold);
            this.button14.Location = new System.Drawing.Point(687, 98);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(58, 62);
            this.button14.TabIndex = 27;
            this.button14.Text = "Home";
            this.button14.UseVisualStyleBackColor = true;
            // 
            // button15
            // 
            this.button15.Font = new System.Drawing.Font("맑은 고딕", 9.5F, System.Drawing.FontStyle.Bold);
            this.button15.Location = new System.Drawing.Point(761, 98);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(58, 62);
            this.button15.TabIndex = 28;
            this.button15.Text = "구동 정지";
            this.button15.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblPosition);
            this.panel1.Controls.Add(this.lblJogSpeed);
            this.panel1.Controls.Add(this.numericUpDown1);
            this.panel1.Controls.Add(this.nuJobSpeed);
            this.panel1.Controls.Add(this.button15);
            this.panel1.Controls.Add(this.button14);
            this.panel1.Controls.Add(this.btnMoveJogPlus);
            this.panel1.Controls.Add(this.btnMoveJogMinus);
            this.panel1.Controls.Add(this.button11);
            this.panel1.Controls.Add(this.button10);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.button8);
            this.panel1.Controls.Add(this.button9);
            this.panel1.Controls.Add(this.button6);
            this.panel1.Controls.Add(this.button7);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.button5);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.textBox3);
            this.panel1.Controls.Add(this.textBox2);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(4, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(844, 176);
            this.panel1.TabIndex = 0;
            // 
            // nuJobSpeed
            // 
            this.nuJobSpeed.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.nuJobSpeed.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nuJobSpeed.Location = new System.Drawing.Point(139, 141);
            this.nuJobSpeed.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nuJobSpeed.Name = "nuJobSpeed";
            this.nuJobSpeed.Size = new System.Drawing.Size(82, 21);
            this.nuJobSpeed.TabIndex = 95;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.numericUpDown1.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown1.Location = new System.Drawing.Point(139, 97);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(82, 21);
            this.numericUpDown1.TabIndex = 96;
            // 
            // lblPosition
            // 
            this.lblPosition.AutoEllipsis = true;
            this.lblPosition.BackColor = System.Drawing.Color.Silver;
            this.lblPosition.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPosition.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPosition.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblPosition.Location = new System.Drawing.Point(78, 44);
            this.lblPosition.Name = "lblPosition";
            this.lblPosition.Size = new System.Drawing.Size(56, 21);
            this.lblPosition.TabIndex = 98;
            this.lblPosition.Text = "0";
            this.lblPosition.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblJogSpeed
            // 
            this.lblJogSpeed.AutoEllipsis = true;
            this.lblJogSpeed.BackColor = System.Drawing.Color.Silver;
            this.lblJogSpeed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJogSpeed.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJogSpeed.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblJogSpeed.Location = new System.Drawing.Point(238, 46);
            this.lblJogSpeed.Name = "lblJogSpeed";
            this.lblJogSpeed.Size = new System.Drawing.Size(62, 21);
            this.lblJogSpeed.TabIndex = 97;
            this.lblJogSpeed.Text = "0";
            this.lblJogSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Ajin_Setting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.panel1);
            this.Name = "Ajin_Setting";
            this.Size = new System.Drawing.Size(851, 183);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nuJobSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button btnMoveJogMinus;
        private System.Windows.Forms.Button btnMoveJogPlus;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.NumericUpDown nuJobSpeed;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        internal System.Windows.Forms.Label lblPosition;
        internal System.Windows.Forms.Label lblJogSpeed;
    }
}
