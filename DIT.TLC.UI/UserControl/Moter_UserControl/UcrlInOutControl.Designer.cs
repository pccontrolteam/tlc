﻿namespace DIT.TLC.UI.Moter_In_Out
{
    partial class InOutControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnXY = new System.Windows.Forms.Button();
            this.lblXYName = new System.Windows.Forms.Label();
            this.lblXY = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnXY
            // 
            this.btnXY.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnXY.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnXY.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnXY.Location = new System.Drawing.Point(12, 2);
            this.btnXY.Margin = new System.Windows.Forms.Padding(0);
            this.btnXY.Name = "btnXY";
            this.btnXY.Size = new System.Drawing.Size(38, 38);
            this.btnXY.TabIndex = 138;
            this.btnXY.UseVisualStyleBackColor = false;
            // 
            // lblXYName
            // 
            this.lblXYName.AutoSize = true;
            this.lblXYName.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblXYName.ForeColor = System.Drawing.Color.White;
            this.lblXYName.Location = new System.Drawing.Point(129, 14);
            this.lblXYName.Name = "lblXYName";
            this.lblXYName.Size = new System.Drawing.Size(15, 13);
            this.lblXYName.TabIndex = 137;
            this.lblXYName.Text = "-";
            // 
            // lblXY
            // 
            this.lblXY.AutoSize = true;
            this.lblXY.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblXY.ForeColor = System.Drawing.Color.White;
            this.lblXY.Location = new System.Drawing.Point(57, 14);
            this.lblXY.Name = "lblXY";
            this.lblXY.Size = new System.Drawing.Size(66, 13);
            this.lblXY.TabIndex = 136;
            this.lblXY.Text = "[ X000 ]";
            // 
            // InOutControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnXY);
            this.Controls.Add(this.lblXYName);
            this.Controls.Add(this.lblXY);
            this.Name = "InOutControl";
            this.Size = new System.Drawing.Size(416, 41);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnXY;
        private System.Windows.Forms.Label lblXYName;
        private System.Windows.Forms.Label lblXY;
    }
}
