﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dit.Framework.PLC;

namespace DIT.TLC.UI.Moter_In_Out
{
    public partial class InOutControl : UserControl
    {
        public PlcAddr _plcAddr = null;
        public InOutControl()
        {
            InitializeComponent();
        }
        public void SetPlcAddress(PlcAddr plcAddr)
        {
            _plcAddr = plcAddr;

            lblXYName.Text = _plcAddr.Desc;
            lblXYName.BackColor = _plcAddr.vBit ? UiGlobal.SET_C : UiGlobal.UNSET_C;
        }
        public void UpdateUi()
        {
            lblXYName.BackColor = _plcAddr.vBit ? UiGlobal.SET_C : UiGlobal.UNSET_C;
        }
    }
}
