﻿namespace DIT.TLC.UI.Moter_In_Out
{
    partial class InOutTotalControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.inControl32 = new DIT.TLC.UI.Moter_In_Out.InOutControl();
            this.inControl31 = new DIT.TLC.UI.Moter_In_Out.InOutControl();
            this.inControl30 = new DIT.TLC.UI.Moter_In_Out.InOutControl();
            this.inControl29 = new DIT.TLC.UI.Moter_In_Out.InOutControl();
            this.inControl28 = new DIT.TLC.UI.Moter_In_Out.InOutControl();
            this.inControl27 = new DIT.TLC.UI.Moter_In_Out.InOutControl();
            this.inControl26 = new DIT.TLC.UI.Moter_In_Out.InOutControl();
            this.inControl25 = new DIT.TLC.UI.Moter_In_Out.InOutControl();
            this.inControl24 = new DIT.TLC.UI.Moter_In_Out.InOutControl();
            this.inControl23 = new DIT.TLC.UI.Moter_In_Out.InOutControl();
            this.inControl22 = new DIT.TLC.UI.Moter_In_Out.InOutControl();
            this.inControl21 = new DIT.TLC.UI.Moter_In_Out.InOutControl();
            this.inControl20 = new DIT.TLC.UI.Moter_In_Out.InOutControl();
            this.inControl19 = new DIT.TLC.UI.Moter_In_Out.InOutControl();
            this.inControl18 = new DIT.TLC.UI.Moter_In_Out.InOutControl();
            this.inControl17 = new DIT.TLC.UI.Moter_In_Out.InOutControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.inControl16 = new DIT.TLC.UI.Moter_In_Out.InOutControl();
            this.inControl15 = new DIT.TLC.UI.Moter_In_Out.InOutControl();
            this.inControl14 = new DIT.TLC.UI.Moter_In_Out.InOutControl();
            this.inControl13 = new DIT.TLC.UI.Moter_In_Out.InOutControl();
            this.inControl12 = new DIT.TLC.UI.Moter_In_Out.InOutControl();
            this.inControl11 = new DIT.TLC.UI.Moter_In_Out.InOutControl();
            this.inControl10 = new DIT.TLC.UI.Moter_In_Out.InOutControl();
            this.inControl9 = new DIT.TLC.UI.Moter_In_Out.InOutControl();
            this.inControl8 = new DIT.TLC.UI.Moter_In_Out.InOutControl();
            this.inControl7 = new DIT.TLC.UI.Moter_In_Out.InOutControl();
            this.inControl6 = new DIT.TLC.UI.Moter_In_Out.InOutControl();
            this.inControl5 = new DIT.TLC.UI.Moter_In_Out.InOutControl();
            this.inControl4 = new DIT.TLC.UI.Moter_In_Out.InOutControl();
            this.inControl3 = new DIT.TLC.UI.Moter_In_Out.InOutControl();
            this.inControl2 = new DIT.TLC.UI.Moter_In_Out.InOutControl();
            this.inControl1 = new DIT.TLC.UI.Moter_In_Out.InOutControl();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.inControl32);
            this.panel2.Controls.Add(this.inControl31);
            this.panel2.Controls.Add(this.inControl30);
            this.panel2.Controls.Add(this.inControl29);
            this.panel2.Controls.Add(this.inControl28);
            this.panel2.Controls.Add(this.inControl27);
            this.panel2.Controls.Add(this.inControl26);
            this.panel2.Controls.Add(this.inControl25);
            this.panel2.Controls.Add(this.inControl24);
            this.panel2.Controls.Add(this.inControl23);
            this.panel2.Controls.Add(this.inControl22);
            this.panel2.Controls.Add(this.inControl21);
            this.panel2.Controls.Add(this.inControl20);
            this.panel2.Controls.Add(this.inControl19);
            this.panel2.Controls.Add(this.inControl18);
            this.panel2.Controls.Add(this.inControl17);
            this.panel2.Location = new System.Drawing.Point(426, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(420, 771);
            this.panel2.TabIndex = 19;
            // 
            // inControl32
            // 
            this.inControl32.Location = new System.Drawing.Point(3, 723);
            this.inControl32.Name = "inControl32";
            this.inControl32.Size = new System.Drawing.Size(415, 43);
            this.inControl32.TabIndex = 15;
            // 
            // inControl31
            // 
            this.inControl31.Location = new System.Drawing.Point(3, 675);
            this.inControl31.Name = "inControl31";
            this.inControl31.Size = new System.Drawing.Size(415, 43);
            this.inControl31.TabIndex = 14;
            // 
            // inControl30
            // 
            this.inControl30.Location = new System.Drawing.Point(3, 627);
            this.inControl30.Name = "inControl30";
            this.inControl30.Size = new System.Drawing.Size(415, 43);
            this.inControl30.TabIndex = 13;
            // 
            // inControl29
            // 
            this.inControl29.Location = new System.Drawing.Point(3, 579);
            this.inControl29.Name = "inControl29";
            this.inControl29.Size = new System.Drawing.Size(415, 43);
            this.inControl29.TabIndex = 12;
            // 
            // inControl28
            // 
            this.inControl28.Location = new System.Drawing.Point(3, 531);
            this.inControl28.Name = "inControl28";
            this.inControl28.Size = new System.Drawing.Size(415, 43);
            this.inControl28.TabIndex = 11;
            // 
            // inControl27
            // 
            this.inControl27.Location = new System.Drawing.Point(3, 482);
            this.inControl27.Name = "inControl27";
            this.inControl27.Size = new System.Drawing.Size(415, 43);
            this.inControl27.TabIndex = 10;
            // 
            // inControl26
            // 
            this.inControl26.Location = new System.Drawing.Point(3, 435);
            this.inControl26.Name = "inControl26";
            this.inControl26.Size = new System.Drawing.Size(415, 43);
            this.inControl26.TabIndex = 9;
            // 
            // inControl25
            // 
            this.inControl25.Location = new System.Drawing.Point(3, 387);
            this.inControl25.Name = "inControl25";
            this.inControl25.Size = new System.Drawing.Size(415, 43);
            this.inControl25.TabIndex = 8;
            // 
            // inControl24
            // 
            this.inControl24.Location = new System.Drawing.Point(3, 339);
            this.inControl24.Name = "inControl24";
            this.inControl24.Size = new System.Drawing.Size(415, 43);
            this.inControl24.TabIndex = 7;
            // 
            // inControl23
            // 
            this.inControl23.Location = new System.Drawing.Point(3, 291);
            this.inControl23.Name = "inControl23";
            this.inControl23.Size = new System.Drawing.Size(415, 43);
            this.inControl23.TabIndex = 6;
            // 
            // inControl22
            // 
            this.inControl22.Location = new System.Drawing.Point(3, 243);
            this.inControl22.Name = "inControl22";
            this.inControl22.Size = new System.Drawing.Size(415, 43);
            this.inControl22.TabIndex = 5;
            // 
            // inControl21
            // 
            this.inControl21.Location = new System.Drawing.Point(3, 195);
            this.inControl21.Name = "inControl21";
            this.inControl21.Size = new System.Drawing.Size(415, 43);
            this.inControl21.TabIndex = 4;
            // 
            // inControl20
            // 
            this.inControl20.Location = new System.Drawing.Point(3, 147);
            this.inControl20.Name = "inControl20";
            this.inControl20.Size = new System.Drawing.Size(415, 43);
            this.inControl20.TabIndex = 3;
            // 
            // inControl19
            // 
            this.inControl19.Location = new System.Drawing.Point(3, 99);
            this.inControl19.Name = "inControl19";
            this.inControl19.Size = new System.Drawing.Size(415, 43);
            this.inControl19.TabIndex = 2;
            // 
            // inControl18
            // 
            this.inControl18.Location = new System.Drawing.Point(3, 51);
            this.inControl18.Name = "inControl18";
            this.inControl18.Size = new System.Drawing.Size(415, 43);
            this.inControl18.TabIndex = 1;
            // 
            // inControl17
            // 
            this.inControl17.Location = new System.Drawing.Point(3, 3);
            this.inControl17.Name = "inControl17";
            this.inControl17.Size = new System.Drawing.Size(415, 43);
            this.inControl17.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.inControl16);
            this.panel1.Controls.Add(this.inControl15);
            this.panel1.Controls.Add(this.inControl14);
            this.panel1.Controls.Add(this.inControl13);
            this.panel1.Controls.Add(this.inControl12);
            this.panel1.Controls.Add(this.inControl11);
            this.panel1.Controls.Add(this.inControl10);
            this.panel1.Controls.Add(this.inControl9);
            this.panel1.Controls.Add(this.inControl8);
            this.panel1.Controls.Add(this.inControl7);
            this.panel1.Controls.Add(this.inControl6);
            this.panel1.Controls.Add(this.inControl5);
            this.panel1.Controls.Add(this.inControl4);
            this.panel1.Controls.Add(this.inControl3);
            this.panel1.Controls.Add(this.inControl2);
            this.panel1.Controls.Add(this.inControl1);
            this.panel1.Location = new System.Drawing.Point(4, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(420, 771);
            this.panel1.TabIndex = 18;
            // 
            // inControl16
            // 
            this.inControl16.Location = new System.Drawing.Point(3, 723);
            this.inControl16.Name = "inControl16";
            this.inControl16.Size = new System.Drawing.Size(415, 43);
            this.inControl16.TabIndex = 15;
            // 
            // inControl15
            // 
            this.inControl15.Location = new System.Drawing.Point(3, 675);
            this.inControl15.Name = "inControl15";
            this.inControl15.Size = new System.Drawing.Size(415, 43);
            this.inControl15.TabIndex = 14;
            // 
            // inControl14
            // 
            this.inControl14.Location = new System.Drawing.Point(3, 627);
            this.inControl14.Name = "inControl14";
            this.inControl14.Size = new System.Drawing.Size(415, 43);
            this.inControl14.TabIndex = 13;
            // 
            // inControl13
            // 
            this.inControl13.Location = new System.Drawing.Point(3, 579);
            this.inControl13.Name = "inControl13";
            this.inControl13.Size = new System.Drawing.Size(415, 43);
            this.inControl13.TabIndex = 12;
            // 
            // inControl12
            // 
            this.inControl12.Location = new System.Drawing.Point(3, 531);
            this.inControl12.Name = "inControl12";
            this.inControl12.Size = new System.Drawing.Size(415, 43);
            this.inControl12.TabIndex = 11;
            // 
            // inControl11
            // 
            this.inControl11.Location = new System.Drawing.Point(3, 482);
            this.inControl11.Name = "inControl11";
            this.inControl11.Size = new System.Drawing.Size(415, 43);
            this.inControl11.TabIndex = 10;
            // 
            // inControl10
            // 
            this.inControl10.Location = new System.Drawing.Point(3, 435);
            this.inControl10.Name = "inControl10";
            this.inControl10.Size = new System.Drawing.Size(415, 43);
            this.inControl10.TabIndex = 9;
            // 
            // inControl9
            // 
            this.inControl9.Location = new System.Drawing.Point(3, 387);
            this.inControl9.Name = "inControl9";
            this.inControl9.Size = new System.Drawing.Size(415, 43);
            this.inControl9.TabIndex = 8;
            // 
            // inControl8
            // 
            this.inControl8.Location = new System.Drawing.Point(3, 339);
            this.inControl8.Name = "inControl8";
            this.inControl8.Size = new System.Drawing.Size(415, 43);
            this.inControl8.TabIndex = 7;
            // 
            // inControl7
            // 
            this.inControl7.Location = new System.Drawing.Point(3, 291);
            this.inControl7.Name = "inControl7";
            this.inControl7.Size = new System.Drawing.Size(415, 43);
            this.inControl7.TabIndex = 6;
            // 
            // inControl6
            // 
            this.inControl6.Location = new System.Drawing.Point(3, 243);
            this.inControl6.Name = "inControl6";
            this.inControl6.Size = new System.Drawing.Size(415, 43);
            this.inControl6.TabIndex = 5;
            // 
            // inControl5
            // 
            this.inControl5.Location = new System.Drawing.Point(3, 195);
            this.inControl5.Name = "inControl5";
            this.inControl5.Size = new System.Drawing.Size(415, 43);
            this.inControl5.TabIndex = 4;
            // 
            // inControl4
            // 
            this.inControl4.Location = new System.Drawing.Point(3, 147);
            this.inControl4.Name = "inControl4";
            this.inControl4.Size = new System.Drawing.Size(415, 43);
            this.inControl4.TabIndex = 3;
            // 
            // inControl3
            // 
            this.inControl3.Location = new System.Drawing.Point(3, 99);
            this.inControl3.Name = "inControl3";
            this.inControl3.Size = new System.Drawing.Size(415, 43);
            this.inControl3.TabIndex = 2;
            // 
            // inControl2
            // 
            this.inControl2.Location = new System.Drawing.Point(3, 51);
            this.inControl2.Name = "inControl2";
            this.inControl2.Size = new System.Drawing.Size(415, 43);
            this.inControl2.TabIndex = 1;
            // 
            // inControl1
            // 
            this.inControl1.Location = new System.Drawing.Point(3, 3);
            this.inControl1.Name = "inControl1";
            this.inControl1.Size = new System.Drawing.Size(415, 43);
            this.inControl1.TabIndex = 0;
            // 
            // InOutTotalControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "InOutTotalControl";
            this.Size = new System.Drawing.Size(850, 780);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private InOutControl inControl32;
        private InOutControl inControl31;
        private InOutControl inControl30;
        private InOutControl inControl29;
        private InOutControl inControl28;
        private InOutControl inControl27;
        private InOutControl inControl26;
        private InOutControl inControl25;
        private InOutControl inControl24;
        private InOutControl inControl23;
        private InOutControl inControl22;
        private InOutControl inControl21;
        private InOutControl inControl20;
        private InOutControl inControl19;
        private InOutControl inControl18;
        private InOutControl inControl17;
        private System.Windows.Forms.Panel panel1;
        private InOutControl inControl16;
        private InOutControl inControl15;
        private InOutControl inControl14;
        private InOutControl inControl13;
        private InOutControl inControl12;
        private InOutControl inControl11;
        private InOutControl inControl10;
        private InOutControl inControl9;
        private InOutControl inControl8;
        private InOutControl inControl7;
        private InOutControl inControl6;
        private InOutControl inControl5;
        private InOutControl inControl4;
        private InOutControl inControl3;
        private InOutControl inControl2;
        private InOutControl inControl1;
    }
}
