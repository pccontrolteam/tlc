﻿namespace DIT.TLC.UI
{
    partial class Parameter_UserManager
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbox_UserManager = new System.Windows.Forms.GroupBox();
            this.lbl_UserInformation = new System.Windows.Forms.Label();
            this.lv_Information = new System.Windows.Forms.ListView();
            this.btn_Add = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_Modify = new System.Windows.Forms.Button();
            this.btn_Delete = new System.Windows.Forms.Button();
            this.col_No = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_ID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_Grade = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.gbox_UserManager.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbox_UserManager
            // 
            this.gbox_UserManager.Controls.Add(this.tableLayoutPanel1);
            this.gbox_UserManager.Controls.Add(this.lv_Information);
            this.gbox_UserManager.Controls.Add(this.lbl_UserInformation);
            this.gbox_UserManager.Location = new System.Drawing.Point(3, 0);
            this.gbox_UserManager.Name = "gbox_UserManager";
            this.gbox_UserManager.Size = new System.Drawing.Size(436, 848);
            this.gbox_UserManager.TabIndex = 0;
            this.gbox_UserManager.TabStop = false;
            // 
            // lbl_UserInformation
            // 
            this.lbl_UserInformation.AutoSize = true;
            this.lbl_UserInformation.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_UserInformation.ForeColor = System.Drawing.Color.White;
            this.lbl_UserInformation.Location = new System.Drawing.Point(22, 17);
            this.lbl_UserInformation.Name = "lbl_UserInformation";
            this.lbl_UserInformation.Size = new System.Drawing.Size(184, 30);
            this.lbl_UserInformation.TabIndex = 1;
            this.lbl_UserInformation.Text = "User Information";
            // 
            // lv_Information
            // 
            this.lv_Information.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lv_Information.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.col_No,
            this.col_ID,
            this.col_Grade});
            this.lv_Information.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lv_Information.GridLines = true;
            this.lv_Information.Location = new System.Drawing.Point(6, 50);
            this.lv_Information.Name = "lv_Information";
            this.lv_Information.Size = new System.Drawing.Size(424, 645);
            this.lv_Information.TabIndex = 2;
            this.lv_Information.UseCompatibleStateImageBehavior = false;
            this.lv_Information.View = System.Windows.Forms.View.Details;
            // 
            // btn_Add
            // 
            this.btn_Add.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btn_Add.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold);
            this.btn_Add.ForeColor = System.Drawing.Color.White;
            this.btn_Add.Location = new System.Drawing.Point(3, 3);
            this.btn_Add.Name = "btn_Add";
            this.btn_Add.Size = new System.Drawing.Size(136, 135);
            this.btn_Add.TabIndex = 3;
            this.btn_Add.Text = "Add";
            this.btn_Add.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.btn_Delete, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.btn_Modify, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.btn_Add, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(4, 701);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(429, 142);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // btn_Modify
            // 
            this.btn_Modify.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btn_Modify.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold);
            this.btn_Modify.ForeColor = System.Drawing.Color.White;
            this.btn_Modify.Location = new System.Drawing.Point(145, 3);
            this.btn_Modify.Name = "btn_Modify";
            this.btn_Modify.Size = new System.Drawing.Size(136, 135);
            this.btn_Modify.TabIndex = 4;
            this.btn_Modify.Text = "Modify";
            this.btn_Modify.UseVisualStyleBackColor = false;
            // 
            // btn_Delete
            // 
            this.btn_Delete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btn_Delete.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold);
            this.btn_Delete.ForeColor = System.Drawing.Color.White;
            this.btn_Delete.Location = new System.Drawing.Point(287, 3);
            this.btn_Delete.Name = "btn_Delete";
            this.btn_Delete.Size = new System.Drawing.Size(136, 135);
            this.btn_Delete.TabIndex = 5;
            this.btn_Delete.Text = "Delete";
            this.btn_Delete.UseVisualStyleBackColor = false;
            // 
            // col_No
            // 
            this.col_No.Text = "No.";
            // 
            // col_ID
            // 
            this.col_ID.Text = "ID";
            this.col_ID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_ID.Width = 213;
            // 
            // col_Grade
            // 
            this.col_Grade.Text = "col_Grade";
            this.col_Grade.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_Grade.Width = 140;
            // 
            // Parameter_UserManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.gbox_UserManager);
            this.Name = "Parameter_UserManager";
            this.Size = new System.Drawing.Size(1738, 873);
            this.gbox_UserManager.ResumeLayout(false);
            this.gbox_UserManager.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbox_UserManager;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btn_Add;
        private System.Windows.Forms.ListView lv_Information;
        private System.Windows.Forms.Label lbl_UserInformation;
        private System.Windows.Forms.Button btn_Delete;
        private System.Windows.Forms.Button btn_Modify;
        private System.Windows.Forms.ColumnHeader col_No;
        private System.Windows.Forms.ColumnHeader col_ID;
        private System.Windows.Forms.ColumnHeader col_Grade;
    }
}
