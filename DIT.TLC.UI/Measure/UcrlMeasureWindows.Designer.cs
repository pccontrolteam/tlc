﻿namespace DIT.TLC.UI.Measure
{
    partial class Measure
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tciostatus_info = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel35 = new System.Windows.Forms.Panel();
            this.label64 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel36 = new System.Windows.Forms.Panel();
            this.label65 = new System.Windows.Forms.Label();
            this.btnMcrBClose = new System.Windows.Forms.Button();
            this.btnMcrBOpen = new System.Windows.Forms.Button();
            this.panel10 = new System.Windows.Forms.Panel();
            this.btnMcrBGet = new System.Windows.Forms.Button();
            this.tboxMcrB = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnMcrAClose = new System.Windows.Forms.Button();
            this.btnMcrAOpen = new System.Windows.Forms.Button();
            this.panel9 = new System.Windows.Forms.Panel();
            this.btnMcrAGet = new System.Windows.Forms.Button();
            this.tboxMcrA = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel34 = new System.Windows.Forms.Panel();
            this.label63 = new System.Windows.Forms.Label();
            this.btnTiltULdClose = new System.Windows.Forms.Button();
            this.btnTiltULdOpen = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.tboxULdCheck4 = new System.Windows.Forms.TextBox();
            this.tboxULdCheck3 = new System.Windows.Forms.TextBox();
            this.tboxULdCheck2 = new System.Windows.Forms.TextBox();
            this.tboxULdCheck1 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tboxULdPosition4 = new System.Windows.Forms.TextBox();
            this.tboxULdPosition3 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tboxULdPosition2 = new System.Windows.Forms.TextBox();
            this.tboxULdPosition1 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.btnTiltLdClose = new System.Windows.Forms.Button();
            this.btnTiltLdOpen = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.tboxLdCheck4 = new System.Windows.Forms.TextBox();
            this.tboxLdCheck3 = new System.Windows.Forms.TextBox();
            this.tboxLdCheck2 = new System.Windows.Forms.TextBox();
            this.tboxLdCheck1 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tboxLdPosition4 = new System.Windows.Forms.TextBox();
            this.tboxLdPosition3 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tboxLdPosition2 = new System.Windows.Forms.TextBox();
            this.tboxLdPosition1 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel33 = new System.Windows.Forms.Panel();
            this.label62 = new System.Windows.Forms.Label();
            this.btnControllerSave = new System.Windows.Forms.Button();
            this.btnControllerClose = new System.Windows.Forms.Button();
            this.btnControllerOpen = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btn2chSet = new System.Windows.Forms.Button();
            this.btn1chSet = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbox2chprbrightset = new System.Windows.Forms.TextBox();
            this.tbox1chprbrightset = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbox2chbrightvalue = new System.Windows.Forms.TextBox();
            this.tbox1chbrightvalue = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel45 = new System.Windows.Forms.Panel();
            this.label74 = new System.Windows.Forms.Label();
            this.btnULdHandyBacordClose = new System.Windows.Forms.Button();
            this.btnULdHandyBacordOpen = new System.Windows.Forms.Button();
            this.panel18 = new System.Windows.Forms.Panel();
            this.btnULdHandyBacordGet = new System.Windows.Forms.Button();
            this.tboxULdHandyBarcode = new System.Windows.Forms.TextBox();
            this.panel19 = new System.Windows.Forms.Panel();
            this.panel46 = new System.Windows.Forms.Panel();
            this.label75 = new System.Windows.Forms.Label();
            this.btnULdBarcodeBClose = new System.Windows.Forms.Button();
            this.btnULdBarcodeBOpen = new System.Windows.Forms.Button();
            this.panel20 = new System.Windows.Forms.Panel();
            this.btnULdBarcodeBGet = new System.Windows.Forms.Button();
            this.tboxULdBarcodeB = new System.Windows.Forms.TextBox();
            this.panel21 = new System.Windows.Forms.Panel();
            this.panel47 = new System.Windows.Forms.Panel();
            this.label76 = new System.Windows.Forms.Label();
            this.btnLdBarcodeBClose = new System.Windows.Forms.Button();
            this.btnLdBarcodeBOpen = new System.Windows.Forms.Button();
            this.panel22 = new System.Windows.Forms.Panel();
            this.btnLdBarcodeBGet = new System.Windows.Forms.Button();
            this.tboxLdBarcodeB = new System.Windows.Forms.TextBox();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel44 = new System.Windows.Forms.Panel();
            this.label73 = new System.Windows.Forms.Label();
            this.btnLdHandyBacordClose = new System.Windows.Forms.Button();
            this.btnLdHandyBacordOpen = new System.Windows.Forms.Button();
            this.panel16 = new System.Windows.Forms.Panel();
            this.btnLdHandyBacordGet = new System.Windows.Forms.Button();
            this.tboxLdHandyBarcode = new System.Windows.Forms.TextBox();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel43 = new System.Windows.Forms.Panel();
            this.label72 = new System.Windows.Forms.Label();
            this.btnULdBarcodeAClose = new System.Windows.Forms.Button();
            this.btnULdBarcodeAOpen = new System.Windows.Forms.Button();
            this.panel14 = new System.Windows.Forms.Panel();
            this.btnULdBarcodeAGet = new System.Windows.Forms.Button();
            this.tboxULdBarcodeA = new System.Windows.Forms.TextBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel42 = new System.Windows.Forms.Panel();
            this.label71 = new System.Windows.Forms.Label();
            this.btnLdBarcodeAClose = new System.Windows.Forms.Button();
            this.btnLdBarcodeAOpen = new System.Windows.Forms.Button();
            this.panel12 = new System.Windows.Forms.Panel();
            this.btnLdBarcodeAGet = new System.Windows.Forms.Button();
            this.tboxLdBarcodeA = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel23 = new System.Windows.Forms.Panel();
            this.panel40 = new System.Windows.Forms.Panel();
            this.label69 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.tboxLDSBreakTableIdx = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.tboxLDSBreakMeasure = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.btnLDSBreakSave = new System.Windows.Forms.Button();
            this.tboxLDSBreakStandby = new System.Windows.Forms.TextBox();
            this.btnLDSBreakStop = new System.Windows.Forms.Button();
            this.btnLDSBreakStart = new System.Windows.Forms.Button();
            this.panel24 = new System.Windows.Forms.Panel();
            this.lvLDSBreak = new System.Windows.Forms.ListView();
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label50 = new System.Windows.Forms.Label();
            this.tboxLDSBreakYInterval = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.tboxLSDBreakValue9 = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.tboxLSDBreakValue6 = new System.Windows.Forms.TextBox();
            this.tboxLSDBreakValue3 = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.tboxLSDBreakValue8 = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.tboxLSDBreakValue5 = new System.Windows.Forms.TextBox();
            this.tboxLSDBreakValue2 = new System.Windows.Forms.TextBox();
            this.tboxLSDBreakCurrentValue = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.tboxLDSBreakXInterval = new System.Windows.Forms.TextBox();
            this.tboxLSDBreakValue7 = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.tboxLSDBreakValue4 = new System.Windows.Forms.TextBox();
            this.tboxLSDBreakValue1 = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.panel25 = new System.Windows.Forms.Panel();
            this.panel39 = new System.Windows.Forms.Panel();
            this.label68 = new System.Windows.Forms.Label();
            this.btnPowerClose = new System.Windows.Forms.Button();
            this.btnPowerOpen = new System.Windows.Forms.Button();
            this.panel26 = new System.Windows.Forms.Panel();
            this.btnPowerGet = new System.Windows.Forms.Button();
            this.tboxPowerGet = new System.Windows.Forms.TextBox();
            this.panel27 = new System.Windows.Forms.Panel();
            this.panel41 = new System.Windows.Forms.Panel();
            this.label70 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.tboxLDSProTableIdx = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.tboxLDSProMeasure = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.btnLDSProcessSave = new System.Windows.Forms.Button();
            this.tboxLDSproStandby = new System.Windows.Forms.TextBox();
            this.btnLDSProcessStop = new System.Windows.Forms.Button();
            this.btnLDSProcessStart = new System.Windows.Forms.Button();
            this.panel29 = new System.Windows.Forms.Panel();
            this.lvLDSProcess = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label41 = new System.Windows.Forms.Label();
            this.tboxLDSProcessYInterval = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.tboxLDSProcessValue9 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.tboxLDSProcessValue6 = new System.Windows.Forms.TextBox();
            this.tboxLDSProcessValue3 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tboxLDSProcessValue8 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.tboxLDSProcessValue5 = new System.Windows.Forms.TextBox();
            this.tboxLDSProcessValue2 = new System.Windows.Forms.TextBox();
            this.tboxLDSProcessCurrentValue = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.tboxLDSProcessXInterval = new System.Windows.Forms.TextBox();
            this.tboxLDSProcessValue7 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.tboxLDSProcessValue4 = new System.Windows.Forms.TextBox();
            this.tboxLDSProcessValue1 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.panel30 = new System.Windows.Forms.Panel();
            this.panel38 = new System.Windows.Forms.Panel();
            this.label67 = new System.Windows.Forms.Label();
            this.btnBeamClose = new System.Windows.Forms.Button();
            this.btnBeamOpen = new System.Windows.Forms.Button();
            this.panel31 = new System.Windows.Forms.Panel();
            this.label39 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.tboxErrorPos = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.tboxTeachingY = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.tboxErrorPower = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.tboxTeachingX = new System.Windows.Forms.TextBox();
            this.tboxTeachingPower = new System.Windows.Forms.TextBox();
            this.btnBeamSave = new System.Windows.Forms.Button();
            this.panel32 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tboxBeamPosYMedianValue = new System.Windows.Forms.TextBox();
            this.tboxBeamPosYMaxValue = new System.Windows.Forms.TextBox();
            this.tboxBeamPosXMedianValue = new System.Windows.Forms.TextBox();
            this.tboxBeamPosXMaxValue = new System.Windows.Forms.TextBox();
            this.tboxBeamPowerMedianValue = new System.Windows.Forms.TextBox();
            this.tboxBeamPowerMaxValue = new System.Windows.Forms.TextBox();
            this.tboxBeamPosYCurrenValue = new System.Windows.Forms.TextBox();
            this.tboxBeamPosYMiniValue = new System.Windows.Forms.TextBox();
            this.tboxBeamPowerCurrenValue = new System.Windows.Forms.TextBox();
            this.tboxBeamPowerMiniValue = new System.Windows.Forms.TextBox();
            this.tboxBeamPosXCurrenValue = new System.Windows.Forms.TextBox();
            this.tboxBeamPosXMiniValue = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.panel28 = new System.Windows.Forms.Panel();
            this.panel37 = new System.Windows.Forms.Panel();
            this.label66 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCutLineColBR2Table = new System.Windows.Forms.Button();
            this.btnCutLineColBR1Table = new System.Windows.Forms.Button();
            this.btnCutLineColAL2Table = new System.Windows.Forms.Button();
            this.btnCutLineColAL1Table = new System.Windows.Forms.Button();
            this.tciostatus_info.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel35.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel36.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel34.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel33.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel45.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel46.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel47.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel44.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel43.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel42.SuspendLayout();
            this.panel12.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel40.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel39.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel41.SuspendLayout();
            this.panel29.SuspendLayout();
            this.panel30.SuspendLayout();
            this.panel38.SuspendLayout();
            this.panel31.SuspendLayout();
            this.panel32.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.panel28.SuspendLayout();
            this.panel37.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tciostatus_info
            // 
            this.tciostatus_info.Controls.Add(this.tabPage1);
            this.tciostatus_info.Controls.Add(this.tabPage2);
            this.tciostatus_info.Controls.Add(this.tabPage3);
            this.tciostatus_info.Controls.Add(this.tabPage4);
            this.tciostatus_info.Font = new System.Drawing.Font("굴림", 9F);
            this.tciostatus_info.ItemSize = new System.Drawing.Size(466, 40);
            this.tciostatus_info.Location = new System.Drawing.Point(8, 8);
            this.tciostatus_info.Name = "tciostatus_info";
            this.tciostatus_info.SelectedIndex = 0;
            this.tciostatus_info.Size = new System.Drawing.Size(1869, 875);
            this.tciostatus_info.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tciostatus_info.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.DimGray;
            this.tabPage1.Controls.Add(this.panel35);
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.panel4);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Font = new System.Drawing.Font("굴림", 20F);
            this.tabPage1.Location = new System.Drawing.Point(4, 44);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(1861, 827);
            this.tabPage1.TabIndex = 3;
            this.tabPage1.Text = "조면 컨트롤 / MCR / BeamPosition";
            // 
            // panel35
            // 
            this.panel35.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel35.Controls.Add(this.label64);
            this.panel35.Location = new System.Drawing.Point(943, 6);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(909, 39);
            this.panel35.TabIndex = 32;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold);
            this.label64.ForeColor = System.Drawing.Color.White;
            this.label64.Location = new System.Drawing.Point(419, 0);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(94, 32);
            this.label64.TabIndex = 24;
            this.label64.Text = "MCR A";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel36);
            this.panel3.Controls.Add(this.btnMcrBClose);
            this.panel3.Controls.Add(this.btnMcrBOpen);
            this.panel3.Controls.Add(this.panel10);
            this.panel3.Location = new System.Drawing.Point(940, 412);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(915, 400);
            this.panel3.TabIndex = 7;
            // 
            // panel36
            // 
            this.panel36.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel36.Controls.Add(this.label65);
            this.panel36.Location = new System.Drawing.Point(3, 5);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(909, 39);
            this.panel36.TabIndex = 32;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold);
            this.label65.ForeColor = System.Drawing.Color.White;
            this.label65.Location = new System.Drawing.Point(421, 1);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(92, 32);
            this.label65.TabIndex = 24;
            this.label65.Text = "MCR B";
            // 
            // btnMcrBClose
            // 
            this.btnMcrBClose.BackColor = System.Drawing.Color.DimGray;
            this.btnMcrBClose.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnMcrBClose.ForeColor = System.Drawing.Color.White;
            this.btnMcrBClose.Location = new System.Drawing.Point(106, 58);
            this.btnMcrBClose.Name = "btnMcrBClose";
            this.btnMcrBClose.Size = new System.Drawing.Size(93, 40);
            this.btnMcrBClose.TabIndex = 13;
            this.btnMcrBClose.Text = "Close";
            this.btnMcrBClose.UseVisualStyleBackColor = false;
            // 
            // btnMcrBOpen
            // 
            this.btnMcrBOpen.BackColor = System.Drawing.Color.DimGray;
            this.btnMcrBOpen.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnMcrBOpen.ForeColor = System.Drawing.Color.White;
            this.btnMcrBOpen.Location = new System.Drawing.Point(9, 58);
            this.btnMcrBOpen.Name = "btnMcrBOpen";
            this.btnMcrBOpen.Size = new System.Drawing.Size(93, 40);
            this.btnMcrBOpen.TabIndex = 12;
            this.btnMcrBOpen.Text = "Open";
            this.btnMcrBOpen.UseVisualStyleBackColor = false;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.btnMcrBGet);
            this.panel10.Controls.Add(this.tboxMcrB);
            this.panel10.Location = new System.Drawing.Point(9, 114);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(903, 283);
            this.panel10.TabIndex = 11;
            // 
            // btnMcrBGet
            // 
            this.btnMcrBGet.BackColor = System.Drawing.Color.DimGray;
            this.btnMcrBGet.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnMcrBGet.ForeColor = System.Drawing.Color.White;
            this.btnMcrBGet.Location = new System.Drawing.Point(42, 106);
            this.btnMcrBGet.Name = "btnMcrBGet";
            this.btnMcrBGet.Size = new System.Drawing.Size(166, 81);
            this.btnMcrBGet.TabIndex = 29;
            this.btnMcrBGet.Text = "Get";
            this.btnMcrBGet.UseVisualStyleBackColor = false;
            // 
            // tboxMcrB
            // 
            this.tboxMcrB.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxMcrB.Font = new System.Drawing.Font("맑은 고딕", 40F, System.Drawing.FontStyle.Bold);
            this.tboxMcrB.Location = new System.Drawing.Point(314, 105);
            this.tboxMcrB.Name = "tboxMcrB";
            this.tboxMcrB.Size = new System.Drawing.Size(451, 78);
            this.tboxMcrB.TabIndex = 25;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnMcrAClose);
            this.panel2.Controls.Add(this.btnMcrAOpen);
            this.panel2.Controls.Add(this.panel9);
            this.panel2.Location = new System.Drawing.Point(940, 6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(915, 400);
            this.panel2.TabIndex = 5;
            // 
            // btnMcrAClose
            // 
            this.btnMcrAClose.BackColor = System.Drawing.Color.DimGray;
            this.btnMcrAClose.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnMcrAClose.ForeColor = System.Drawing.Color.White;
            this.btnMcrAClose.Location = new System.Drawing.Point(106, 48);
            this.btnMcrAClose.Name = "btnMcrAClose";
            this.btnMcrAClose.Size = new System.Drawing.Size(93, 40);
            this.btnMcrAClose.TabIndex = 10;
            this.btnMcrAClose.Text = "Close";
            this.btnMcrAClose.UseVisualStyleBackColor = false;
            // 
            // btnMcrAOpen
            // 
            this.btnMcrAOpen.BackColor = System.Drawing.Color.DimGray;
            this.btnMcrAOpen.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnMcrAOpen.ForeColor = System.Drawing.Color.White;
            this.btnMcrAOpen.Location = new System.Drawing.Point(9, 48);
            this.btnMcrAOpen.Name = "btnMcrAOpen";
            this.btnMcrAOpen.Size = new System.Drawing.Size(93, 40);
            this.btnMcrAOpen.TabIndex = 9;
            this.btnMcrAOpen.Text = "Open";
            this.btnMcrAOpen.UseVisualStyleBackColor = false;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.btnMcrAGet);
            this.panel9.Controls.Add(this.tboxMcrA);
            this.panel9.Location = new System.Drawing.Point(9, 104);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(903, 283);
            this.panel9.TabIndex = 5;
            // 
            // btnMcrAGet
            // 
            this.btnMcrAGet.BackColor = System.Drawing.Color.DimGray;
            this.btnMcrAGet.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnMcrAGet.ForeColor = System.Drawing.Color.White;
            this.btnMcrAGet.Location = new System.Drawing.Point(42, 106);
            this.btnMcrAGet.Name = "btnMcrAGet";
            this.btnMcrAGet.Size = new System.Drawing.Size(166, 81);
            this.btnMcrAGet.TabIndex = 29;
            this.btnMcrAGet.Text = "Get";
            this.btnMcrAGet.UseVisualStyleBackColor = false;
            // 
            // tboxMcrA
            // 
            this.tboxMcrA.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxMcrA.Font = new System.Drawing.Font("맑은 고딕", 40F, System.Drawing.FontStyle.Bold);
            this.tboxMcrA.Location = new System.Drawing.Point(314, 106);
            this.tboxMcrA.Name = "tboxMcrA";
            this.tboxMcrA.Size = new System.Drawing.Size(451, 78);
            this.tboxMcrA.TabIndex = 25;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel34);
            this.panel4.Controls.Add(this.btnTiltULdClose);
            this.panel4.Controls.Add(this.btnTiltULdOpen);
            this.panel4.Controls.Add(this.panel8);
            this.panel4.Controls.Add(this.btnTiltLdClose);
            this.panel4.Controls.Add(this.btnTiltLdOpen);
            this.panel4.Controls.Add(this.panel7);
            this.panel4.Location = new System.Drawing.Point(4, 412);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(915, 400);
            this.panel4.TabIndex = 6;
            // 
            // panel34
            // 
            this.panel34.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel34.Controls.Add(this.label63);
            this.panel34.Location = new System.Drawing.Point(3, 3);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(909, 39);
            this.panel34.TabIndex = 32;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold);
            this.label63.ForeColor = System.Drawing.Color.White;
            this.label63.Location = new System.Drawing.Point(399, 3);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(137, 32);
            this.label63.TabIndex = 24;
            this.label63.Text = "Tilt Sensor";
            // 
            // btnTiltULdClose
            // 
            this.btnTiltULdClose.BackColor = System.Drawing.Color.DimGray;
            this.btnTiltULdClose.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnTiltULdClose.ForeColor = System.Drawing.Color.White;
            this.btnTiltULdClose.Location = new System.Drawing.Point(581, 58);
            this.btnTiltULdClose.Name = "btnTiltULdClose";
            this.btnTiltULdClose.Size = new System.Drawing.Size(93, 40);
            this.btnTiltULdClose.TabIndex = 14;
            this.btnTiltULdClose.Text = "Close";
            this.btnTiltULdClose.UseVisualStyleBackColor = false;
            // 
            // btnTiltULdOpen
            // 
            this.btnTiltULdOpen.BackColor = System.Drawing.Color.DimGray;
            this.btnTiltULdOpen.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnTiltULdOpen.ForeColor = System.Drawing.Color.White;
            this.btnTiltULdOpen.Location = new System.Drawing.Point(484, 58);
            this.btnTiltULdOpen.Name = "btnTiltULdOpen";
            this.btnTiltULdOpen.Size = new System.Drawing.Size(93, 40);
            this.btnTiltULdOpen.TabIndex = 13;
            this.btnTiltULdOpen.Text = "Open";
            this.btnTiltULdOpen.UseVisualStyleBackColor = false;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.tboxULdCheck4);
            this.panel8.Controls.Add(this.tboxULdCheck3);
            this.panel8.Controls.Add(this.tboxULdCheck2);
            this.panel8.Controls.Add(this.tboxULdCheck1);
            this.panel8.Controls.Add(this.label10);
            this.panel8.Controls.Add(this.label11);
            this.panel8.Controls.Add(this.tboxULdPosition4);
            this.panel8.Controls.Add(this.tboxULdPosition3);
            this.panel8.Controls.Add(this.label15);
            this.panel8.Controls.Add(this.label16);
            this.panel8.Controls.Add(this.tboxULdPosition2);
            this.panel8.Controls.Add(this.tboxULdPosition1);
            this.panel8.Controls.Add(this.label12);
            this.panel8.Location = new System.Drawing.Point(487, 114);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(409, 254);
            this.panel8.TabIndex = 12;
            // 
            // tboxULdCheck4
            // 
            this.tboxULdCheck4.BackColor = System.Drawing.Color.Red;
            this.tboxULdCheck4.Font = new System.Drawing.Font("맑은 고딕", 15F, System.Drawing.FontStyle.Bold);
            this.tboxULdCheck4.ForeColor = System.Drawing.Color.White;
            this.tboxULdCheck4.Location = new System.Drawing.Point(317, 201);
            this.tboxULdCheck4.Name = "tboxULdCheck4";
            this.tboxULdCheck4.Size = new System.Drawing.Size(62, 34);
            this.tboxULdCheck4.TabIndex = 42;
            this.tboxULdCheck4.Text = "NG";
            this.tboxULdCheck4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tboxULdCheck3
            // 
            this.tboxULdCheck3.BackColor = System.Drawing.Color.Red;
            this.tboxULdCheck3.Font = new System.Drawing.Font("맑은 고딕", 15F, System.Drawing.FontStyle.Bold);
            this.tboxULdCheck3.ForeColor = System.Drawing.Color.White;
            this.tboxULdCheck3.Location = new System.Drawing.Point(317, 153);
            this.tboxULdCheck3.Name = "tboxULdCheck3";
            this.tboxULdCheck3.Size = new System.Drawing.Size(62, 34);
            this.tboxULdCheck3.TabIndex = 41;
            this.tboxULdCheck3.Text = "NG";
            this.tboxULdCheck3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tboxULdCheck2
            // 
            this.tboxULdCheck2.BackColor = System.Drawing.Color.Red;
            this.tboxULdCheck2.Font = new System.Drawing.Font("맑은 고딕", 15F, System.Drawing.FontStyle.Bold);
            this.tboxULdCheck2.ForeColor = System.Drawing.Color.White;
            this.tboxULdCheck2.Location = new System.Drawing.Point(317, 107);
            this.tboxULdCheck2.Name = "tboxULdCheck2";
            this.tboxULdCheck2.Size = new System.Drawing.Size(62, 34);
            this.tboxULdCheck2.TabIndex = 40;
            this.tboxULdCheck2.Text = "NG";
            this.tboxULdCheck2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tboxULdCheck1
            // 
            this.tboxULdCheck1.BackColor = System.Drawing.Color.Red;
            this.tboxULdCheck1.Font = new System.Drawing.Font("맑은 고딕", 15F, System.Drawing.FontStyle.Bold);
            this.tboxULdCheck1.ForeColor = System.Drawing.Color.White;
            this.tboxULdCheck1.Location = new System.Drawing.Point(317, 59);
            this.tboxULdCheck1.Name = "tboxULdCheck1";
            this.tboxULdCheck1.Size = new System.Drawing.Size(62, 34);
            this.tboxULdCheck1.TabIndex = 39;
            this.tboxULdCheck1.Text = "NG";
            this.tboxULdCheck1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(19, 210);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(118, 20);
            this.label10.TabIndex = 38;
            this.label10.Text = "Position 4 [mm]";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(19, 163);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(118, 20);
            this.label11.TabIndex = 37;
            this.label11.Text = "Position 3 [mm]";
            // 
            // tboxULdPosition4
            // 
            this.tboxULdPosition4.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxULdPosition4.Font = new System.Drawing.Font("맑은 고딕", 15F, System.Drawing.FontStyle.Bold);
            this.tboxULdPosition4.Location = new System.Drawing.Point(167, 201);
            this.tboxULdPosition4.Name = "tboxULdPosition4";
            this.tboxULdPosition4.Size = new System.Drawing.Size(131, 34);
            this.tboxULdPosition4.TabIndex = 36;
            // 
            // tboxULdPosition3
            // 
            this.tboxULdPosition3.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxULdPosition3.Font = new System.Drawing.Font("맑은 고딕", 15F, System.Drawing.FontStyle.Bold);
            this.tboxULdPosition3.Location = new System.Drawing.Point(167, 153);
            this.tboxULdPosition3.Name = "tboxULdPosition3";
            this.tboxULdPosition3.Size = new System.Drawing.Size(131, 34);
            this.tboxULdPosition3.TabIndex = 35;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(19, 116);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(118, 20);
            this.label15.TabIndex = 34;
            this.label15.Text = "Position 2 [mm]";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(19, 69);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(118, 20);
            this.label16.TabIndex = 33;
            this.label16.Text = "Position 1 [mm]";
            // 
            // tboxULdPosition2
            // 
            this.tboxULdPosition2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxULdPosition2.Font = new System.Drawing.Font("맑은 고딕", 15F, System.Drawing.FontStyle.Bold);
            this.tboxULdPosition2.Location = new System.Drawing.Point(167, 107);
            this.tboxULdPosition2.Name = "tboxULdPosition2";
            this.tboxULdPosition2.Size = new System.Drawing.Size(131, 34);
            this.tboxULdPosition2.TabIndex = 32;
            // 
            // tboxULdPosition1
            // 
            this.tboxULdPosition1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxULdPosition1.Font = new System.Drawing.Font("맑은 고딕", 15F, System.Drawing.FontStyle.Bold);
            this.tboxULdPosition1.Location = new System.Drawing.Point(167, 59);
            this.tboxULdPosition1.Name = "tboxULdPosition1";
            this.tboxULdPosition1.Size = new System.Drawing.Size(131, 34);
            this.tboxULdPosition1.TabIndex = 31;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("맑은 고딕", 13F, System.Drawing.FontStyle.Bold);
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(17, 18);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(165, 25);
            this.label12.TabIndex = 18;
            this.label12.Text = "Module UnLoader";
            // 
            // btnTiltLdClose
            // 
            this.btnTiltLdClose.BackColor = System.Drawing.Color.DimGray;
            this.btnTiltLdClose.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnTiltLdClose.ForeColor = System.Drawing.Color.White;
            this.btnTiltLdClose.Location = new System.Drawing.Point(116, 58);
            this.btnTiltLdClose.Name = "btnTiltLdClose";
            this.btnTiltLdClose.Size = new System.Drawing.Size(93, 40);
            this.btnTiltLdClose.TabIndex = 11;
            this.btnTiltLdClose.Text = "Close";
            this.btnTiltLdClose.UseVisualStyleBackColor = false;
            // 
            // btnTiltLdOpen
            // 
            this.btnTiltLdOpen.BackColor = System.Drawing.Color.DimGray;
            this.btnTiltLdOpen.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnTiltLdOpen.ForeColor = System.Drawing.Color.White;
            this.btnTiltLdOpen.Location = new System.Drawing.Point(19, 58);
            this.btnTiltLdOpen.Name = "btnTiltLdOpen";
            this.btnTiltLdOpen.Size = new System.Drawing.Size(93, 40);
            this.btnTiltLdOpen.TabIndex = 10;
            this.btnTiltLdOpen.Text = "Open";
            this.btnTiltLdOpen.UseVisualStyleBackColor = false;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.tboxLdCheck4);
            this.panel7.Controls.Add(this.tboxLdCheck3);
            this.panel7.Controls.Add(this.tboxLdCheck2);
            this.panel7.Controls.Add(this.tboxLdCheck1);
            this.panel7.Controls.Add(this.label13);
            this.panel7.Controls.Add(this.label14);
            this.panel7.Controls.Add(this.tboxLdPosition4);
            this.panel7.Controls.Add(this.tboxLdPosition3);
            this.panel7.Controls.Add(this.label6);
            this.panel7.Controls.Add(this.label7);
            this.panel7.Controls.Add(this.tboxLdPosition2);
            this.panel7.Controls.Add(this.tboxLdPosition1);
            this.panel7.Controls.Add(this.label9);
            this.panel7.Location = new System.Drawing.Point(22, 114);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(409, 254);
            this.panel7.TabIndex = 9;
            // 
            // tboxLdCheck4
            // 
            this.tboxLdCheck4.BackColor = System.Drawing.Color.Red;
            this.tboxLdCheck4.Font = new System.Drawing.Font("맑은 고딕", 15F, System.Drawing.FontStyle.Bold);
            this.tboxLdCheck4.ForeColor = System.Drawing.Color.White;
            this.tboxLdCheck4.Location = new System.Drawing.Point(316, 201);
            this.tboxLdCheck4.Name = "tboxLdCheck4";
            this.tboxLdCheck4.Size = new System.Drawing.Size(62, 34);
            this.tboxLdCheck4.TabIndex = 30;
            this.tboxLdCheck4.Text = "NG";
            this.tboxLdCheck4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tboxLdCheck3
            // 
            this.tboxLdCheck3.BackColor = System.Drawing.Color.Red;
            this.tboxLdCheck3.Font = new System.Drawing.Font("맑은 고딕", 15F, System.Drawing.FontStyle.Bold);
            this.tboxLdCheck3.ForeColor = System.Drawing.Color.White;
            this.tboxLdCheck3.Location = new System.Drawing.Point(316, 153);
            this.tboxLdCheck3.Name = "tboxLdCheck3";
            this.tboxLdCheck3.Size = new System.Drawing.Size(62, 34);
            this.tboxLdCheck3.TabIndex = 29;
            this.tboxLdCheck3.Text = "NG";
            this.tboxLdCheck3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tboxLdCheck2
            // 
            this.tboxLdCheck2.BackColor = System.Drawing.Color.Red;
            this.tboxLdCheck2.Font = new System.Drawing.Font("맑은 고딕", 15F, System.Drawing.FontStyle.Bold);
            this.tboxLdCheck2.ForeColor = System.Drawing.Color.White;
            this.tboxLdCheck2.Location = new System.Drawing.Point(316, 107);
            this.tboxLdCheck2.Name = "tboxLdCheck2";
            this.tboxLdCheck2.Size = new System.Drawing.Size(62, 34);
            this.tboxLdCheck2.TabIndex = 28;
            this.tboxLdCheck2.Text = "NG";
            this.tboxLdCheck2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tboxLdCheck1
            // 
            this.tboxLdCheck1.BackColor = System.Drawing.Color.Red;
            this.tboxLdCheck1.Font = new System.Drawing.Font("맑은 고딕", 15F, System.Drawing.FontStyle.Bold);
            this.tboxLdCheck1.ForeColor = System.Drawing.Color.White;
            this.tboxLdCheck1.Location = new System.Drawing.Point(316, 59);
            this.tboxLdCheck1.Name = "tboxLdCheck1";
            this.tboxLdCheck1.Size = new System.Drawing.Size(62, 34);
            this.tboxLdCheck1.TabIndex = 27;
            this.tboxLdCheck1.Text = "NG";
            this.tboxLdCheck1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(18, 210);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(118, 20);
            this.label13.TabIndex = 26;
            this.label13.Text = "Position 4 [mm]";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(18, 163);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(118, 20);
            this.label14.TabIndex = 25;
            this.label14.Text = "Position 3 [mm]";
            // 
            // tboxLdPosition4
            // 
            this.tboxLdPosition4.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxLdPosition4.Font = new System.Drawing.Font("맑은 고딕", 15F, System.Drawing.FontStyle.Bold);
            this.tboxLdPosition4.Location = new System.Drawing.Point(166, 201);
            this.tboxLdPosition4.Name = "tboxLdPosition4";
            this.tboxLdPosition4.Size = new System.Drawing.Size(131, 34);
            this.tboxLdPosition4.TabIndex = 24;
            // 
            // tboxLdPosition3
            // 
            this.tboxLdPosition3.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxLdPosition3.Font = new System.Drawing.Font("맑은 고딕", 15F, System.Drawing.FontStyle.Bold);
            this.tboxLdPosition3.Location = new System.Drawing.Point(166, 153);
            this.tboxLdPosition3.Name = "tboxLdPosition3";
            this.tboxLdPosition3.Size = new System.Drawing.Size(131, 34);
            this.tboxLdPosition3.TabIndex = 23;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(18, 116);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(118, 20);
            this.label6.TabIndex = 22;
            this.label6.Text = "Position 2 [mm]";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(18, 69);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(118, 20);
            this.label7.TabIndex = 21;
            this.label7.Text = "Position 1 [mm]";
            // 
            // tboxLdPosition2
            // 
            this.tboxLdPosition2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxLdPosition2.Font = new System.Drawing.Font("맑은 고딕", 15F, System.Drawing.FontStyle.Bold);
            this.tboxLdPosition2.Location = new System.Drawing.Point(166, 107);
            this.tboxLdPosition2.Name = "tboxLdPosition2";
            this.tboxLdPosition2.Size = new System.Drawing.Size(131, 34);
            this.tboxLdPosition2.TabIndex = 20;
            // 
            // tboxLdPosition1
            // 
            this.tboxLdPosition1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxLdPosition1.Font = new System.Drawing.Font("맑은 고딕", 15F, System.Drawing.FontStyle.Bold);
            this.tboxLdPosition1.Location = new System.Drawing.Point(166, 59);
            this.tboxLdPosition1.Name = "tboxLdPosition1";
            this.tboxLdPosition1.Size = new System.Drawing.Size(131, 34);
            this.tboxLdPosition1.TabIndex = 19;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("맑은 고딕", 13F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(17, 18);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(141, 25);
            this.label9.TabIndex = 18;
            this.label9.Text = "Module Loader";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel33);
            this.panel1.Controls.Add(this.btnControllerSave);
            this.panel1.Controls.Add(this.btnControllerClose);
            this.panel1.Controls.Add(this.btnControllerOpen);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Location = new System.Drawing.Point(4, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(915, 400);
            this.panel1.TabIndex = 4;
            // 
            // panel33
            // 
            this.panel33.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel33.Controls.Add(this.label62);
            this.panel33.Location = new System.Drawing.Point(3, 0);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(909, 39);
            this.panel33.TabIndex = 31;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold);
            this.label62.ForeColor = System.Drawing.Color.White;
            this.label62.Location = new System.Drawing.Point(363, 3);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(211, 32);
            this.label62.TabIndex = 24;
            this.label62.Text = "ERI 조명 컨트롤러";
            // 
            // btnControllerSave
            // 
            this.btnControllerSave.BackColor = System.Drawing.Color.DimGray;
            this.btnControllerSave.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnControllerSave.ForeColor = System.Drawing.Color.White;
            this.btnControllerSave.Location = new System.Drawing.Point(760, 344);
            this.btnControllerSave.Name = "btnControllerSave";
            this.btnControllerSave.Size = new System.Drawing.Size(132, 43);
            this.btnControllerSave.TabIndex = 30;
            this.btnControllerSave.Text = "저장";
            this.btnControllerSave.UseVisualStyleBackColor = false;
            // 
            // btnControllerClose
            // 
            this.btnControllerClose.BackColor = System.Drawing.Color.DimGray;
            this.btnControllerClose.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnControllerClose.ForeColor = System.Drawing.Color.White;
            this.btnControllerClose.Location = new System.Drawing.Point(102, 48);
            this.btnControllerClose.Name = "btnControllerClose";
            this.btnControllerClose.Size = new System.Drawing.Size(93, 40);
            this.btnControllerClose.TabIndex = 8;
            this.btnControllerClose.Text = "Close";
            this.btnControllerClose.UseVisualStyleBackColor = false;
            // 
            // btnControllerOpen
            // 
            this.btnControllerOpen.BackColor = System.Drawing.Color.DimGray;
            this.btnControllerOpen.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnControllerOpen.ForeColor = System.Drawing.Color.White;
            this.btnControllerOpen.Location = new System.Drawing.Point(5, 48);
            this.btnControllerOpen.Name = "btnControllerOpen";
            this.btnControllerOpen.Size = new System.Drawing.Size(93, 40);
            this.btnControllerOpen.TabIndex = 7;
            this.btnControllerOpen.Text = "Open";
            this.btnControllerOpen.UseVisualStyleBackColor = false;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.btn2chSet);
            this.panel6.Controls.Add(this.btn1chSet);
            this.panel6.Controls.Add(this.label3);
            this.panel6.Controls.Add(this.label4);
            this.panel6.Controls.Add(this.tbox2chprbrightset);
            this.panel6.Controls.Add(this.tbox1chprbrightset);
            this.panel6.Controls.Add(this.label5);
            this.panel6.Location = new System.Drawing.Point(385, 104);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(511, 201);
            this.panel6.TabIndex = 4;
            // 
            // btn2chSet
            // 
            this.btn2chSet.BackColor = System.Drawing.Color.DimGray;
            this.btn2chSet.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn2chSet.ForeColor = System.Drawing.Color.White;
            this.btn2chSet.Location = new System.Drawing.Point(369, 147);
            this.btn2chSet.Name = "btn2chSet";
            this.btn2chSet.Size = new System.Drawing.Size(115, 43);
            this.btn2chSet.TabIndex = 29;
            this.btn2chSet.Text = "2채널 설정";
            this.btn2chSet.UseVisualStyleBackColor = false;
            // 
            // btn1chSet
            // 
            this.btn1chSet.BackColor = System.Drawing.Color.DimGray;
            this.btn1chSet.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn1chSet.ForeColor = System.Drawing.Color.White;
            this.btn1chSet.Location = new System.Drawing.Point(369, 82);
            this.btn1chSet.Name = "btn1chSet";
            this.btn1chSet.Size = new System.Drawing.Size(115, 43);
            this.btn1chSet.TabIndex = 28;
            this.btn1chSet.Text = "1채널 설정";
            this.btn1chSet.UseVisualStyleBackColor = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(40, 153);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(152, 20);
            this.label3.TabIndex = 27;
            this.label3.Text = "2채널 현재 밝기 설정";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(40, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(152, 20);
            this.label4.TabIndex = 26;
            this.label4.Text = "1채널 현재 밝기 설정";
            // 
            // tbox2chprbrightset
            // 
            this.tbox2chprbrightset.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox2chprbrightset.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.tbox2chprbrightset.Location = new System.Drawing.Point(208, 144);
            this.tbox2chprbrightset.Name = "tbox2chprbrightset";
            this.tbox2chprbrightset.Size = new System.Drawing.Size(131, 43);
            this.tbox2chprbrightset.TabIndex = 25;
            // 
            // tbox1chprbrightset
            // 
            this.tbox1chprbrightset.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox1chprbrightset.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.tbox1chprbrightset.Location = new System.Drawing.Point(208, 82);
            this.tbox1chprbrightset.Name = "tbox1chprbrightset";
            this.tbox1chprbrightset.Size = new System.Drawing.Size(131, 43);
            this.tbox1chprbrightset.TabIndex = 24;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("맑은 고딕", 13F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(46, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(108, 25);
            this.label5.TabIndex = 23;
            this.label5.Text = "밝기값 설정";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Controls.Add(this.tbox2chbrightvalue);
            this.panel5.Controls.Add(this.tbox1chbrightvalue);
            this.panel5.Controls.Add(this.label8);
            this.panel5.Location = new System.Drawing.Point(8, 104);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(371, 201);
            this.panel5.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(24, 153);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 20);
            this.label2.TabIndex = 22;
            this.label2.Text = "2채널 현재 밝기값";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(24, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 20);
            this.label1.TabIndex = 21;
            this.label1.Text = "1채널 현재 밝기값";
            // 
            // tbox2chbrightvalue
            // 
            this.tbox2chbrightvalue.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox2chbrightvalue.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.tbox2chbrightvalue.Location = new System.Drawing.Point(205, 144);
            this.tbox2chbrightvalue.Name = "tbox2chbrightvalue";
            this.tbox2chbrightvalue.Size = new System.Drawing.Size(147, 43);
            this.tbox2chbrightvalue.TabIndex = 20;
            // 
            // tbox1chbrightvalue
            // 
            this.tbox1chbrightvalue.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox1chbrightvalue.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.tbox1chbrightvalue.Location = new System.Drawing.Point(205, 82);
            this.tbox1chbrightvalue.Name = "tbox1chbrightvalue";
            this.tbox1chbrightvalue.Size = new System.Drawing.Size(147, 43);
            this.tbox1chbrightvalue.TabIndex = 19;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("맑은 고딕", 13F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(30, 31);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(108, 25);
            this.label8.TabIndex = 18;
            this.label8.Text = "현재 밝기값";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.DimGray;
            this.tabPage2.Controls.Add(this.panel17);
            this.tabPage2.Controls.Add(this.panel19);
            this.tabPage2.Controls.Add(this.panel21);
            this.tabPage2.Controls.Add(this.panel15);
            this.tabPage2.Controls.Add(this.panel13);
            this.tabPage2.Controls.Add(this.panel11);
            this.tabPage2.Font = new System.Drawing.Font("굴림", 20F);
            this.tabPage2.Location = new System.Drawing.Point(4, 44);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(1861, 827);
            this.tabPage2.TabIndex = 4;
            this.tabPage2.Text = "바코드 리더";
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.panel45);
            this.panel17.Controls.Add(this.btnULdHandyBacordClose);
            this.panel17.Controls.Add(this.btnULdHandyBacordOpen);
            this.panel17.Controls.Add(this.panel18);
            this.panel17.Location = new System.Drawing.Point(942, 534);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(915, 255);
            this.panel17.TabIndex = 21;
            // 
            // panel45
            // 
            this.panel45.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel45.Controls.Add(this.label74);
            this.panel45.Location = new System.Drawing.Point(3, 3);
            this.panel45.Name = "panel45";
            this.panel45.Size = new System.Drawing.Size(909, 39);
            this.panel45.TabIndex = 33;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold);
            this.label74.ForeColor = System.Drawing.Color.White;
            this.label74.Location = new System.Drawing.Point(358, 3);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(199, 32);
            this.label74.TabIndex = 24;
            this.label74.Text = "핸디 바코드 리더";
            // 
            // btnULdHandyBacordClose
            // 
            this.btnULdHandyBacordClose.BackColor = System.Drawing.Color.DimGray;
            this.btnULdHandyBacordClose.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnULdHandyBacordClose.ForeColor = System.Drawing.Color.White;
            this.btnULdHandyBacordClose.Location = new System.Drawing.Point(106, 48);
            this.btnULdHandyBacordClose.Name = "btnULdHandyBacordClose";
            this.btnULdHandyBacordClose.Size = new System.Drawing.Size(93, 40);
            this.btnULdHandyBacordClose.TabIndex = 10;
            this.btnULdHandyBacordClose.Text = "Close";
            this.btnULdHandyBacordClose.UseVisualStyleBackColor = false;
            // 
            // btnULdHandyBacordOpen
            // 
            this.btnULdHandyBacordOpen.BackColor = System.Drawing.Color.DimGray;
            this.btnULdHandyBacordOpen.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnULdHandyBacordOpen.ForeColor = System.Drawing.Color.White;
            this.btnULdHandyBacordOpen.Location = new System.Drawing.Point(9, 48);
            this.btnULdHandyBacordOpen.Name = "btnULdHandyBacordOpen";
            this.btnULdHandyBacordOpen.Size = new System.Drawing.Size(93, 40);
            this.btnULdHandyBacordOpen.TabIndex = 9;
            this.btnULdHandyBacordOpen.Text = "Open";
            this.btnULdHandyBacordOpen.UseVisualStyleBackColor = false;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.btnULdHandyBacordGet);
            this.panel18.Controls.Add(this.tboxULdHandyBarcode);
            this.panel18.Location = new System.Drawing.Point(9, 94);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(903, 152);
            this.panel18.TabIndex = 5;
            // 
            // btnULdHandyBacordGet
            // 
            this.btnULdHandyBacordGet.BackColor = System.Drawing.Color.DimGray;
            this.btnULdHandyBacordGet.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnULdHandyBacordGet.ForeColor = System.Drawing.Color.White;
            this.btnULdHandyBacordGet.Location = new System.Drawing.Point(42, 47);
            this.btnULdHandyBacordGet.Name = "btnULdHandyBacordGet";
            this.btnULdHandyBacordGet.Size = new System.Drawing.Size(166, 66);
            this.btnULdHandyBacordGet.TabIndex = 29;
            this.btnULdHandyBacordGet.Text = "Get";
            this.btnULdHandyBacordGet.UseVisualStyleBackColor = false;
            // 
            // tboxULdHandyBarcode
            // 
            this.tboxULdHandyBarcode.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxULdHandyBarcode.Font = new System.Drawing.Font("맑은 고딕", 30F, System.Drawing.FontStyle.Bold);
            this.tboxULdHandyBarcode.Location = new System.Drawing.Point(289, 49);
            this.tboxULdHandyBarcode.Name = "tboxULdHandyBarcode";
            this.tboxULdHandyBarcode.Size = new System.Drawing.Size(417, 61);
            this.tboxULdHandyBarcode.TabIndex = 25;
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.panel46);
            this.panel19.Controls.Add(this.btnULdBarcodeBClose);
            this.panel19.Controls.Add(this.btnULdBarcodeBOpen);
            this.panel19.Controls.Add(this.panel20);
            this.panel19.Location = new System.Drawing.Point(942, 270);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(915, 255);
            this.panel19.TabIndex = 20;
            // 
            // panel46
            // 
            this.panel46.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel46.Controls.Add(this.label75);
            this.panel46.Location = new System.Drawing.Point(3, 3);
            this.panel46.Name = "panel46";
            this.panel46.Size = new System.Drawing.Size(909, 39);
            this.panel46.TabIndex = 33;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold);
            this.label75.ForeColor = System.Drawing.Color.White;
            this.label75.Location = new System.Drawing.Point(279, 4);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(350, 32);
            this.label75.TabIndex = 24;
            this.label75.Text = "언로더부 고정식 바코드 리더 B";
            // 
            // btnULdBarcodeBClose
            // 
            this.btnULdBarcodeBClose.BackColor = System.Drawing.Color.DimGray;
            this.btnULdBarcodeBClose.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnULdBarcodeBClose.ForeColor = System.Drawing.Color.White;
            this.btnULdBarcodeBClose.Location = new System.Drawing.Point(106, 48);
            this.btnULdBarcodeBClose.Name = "btnULdBarcodeBClose";
            this.btnULdBarcodeBClose.Size = new System.Drawing.Size(93, 40);
            this.btnULdBarcodeBClose.TabIndex = 10;
            this.btnULdBarcodeBClose.Text = "Close";
            this.btnULdBarcodeBClose.UseVisualStyleBackColor = false;
            // 
            // btnULdBarcodeBOpen
            // 
            this.btnULdBarcodeBOpen.BackColor = System.Drawing.Color.DimGray;
            this.btnULdBarcodeBOpen.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnULdBarcodeBOpen.ForeColor = System.Drawing.Color.White;
            this.btnULdBarcodeBOpen.Location = new System.Drawing.Point(9, 48);
            this.btnULdBarcodeBOpen.Name = "btnULdBarcodeBOpen";
            this.btnULdBarcodeBOpen.Size = new System.Drawing.Size(93, 40);
            this.btnULdBarcodeBOpen.TabIndex = 9;
            this.btnULdBarcodeBOpen.Text = "Open";
            this.btnULdBarcodeBOpen.UseVisualStyleBackColor = false;
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.btnULdBarcodeBGet);
            this.panel20.Controls.Add(this.tboxULdBarcodeB);
            this.panel20.Location = new System.Drawing.Point(9, 94);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(903, 152);
            this.panel20.TabIndex = 5;
            // 
            // btnULdBarcodeBGet
            // 
            this.btnULdBarcodeBGet.BackColor = System.Drawing.Color.DimGray;
            this.btnULdBarcodeBGet.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnULdBarcodeBGet.ForeColor = System.Drawing.Color.White;
            this.btnULdBarcodeBGet.Location = new System.Drawing.Point(42, 47);
            this.btnULdBarcodeBGet.Name = "btnULdBarcodeBGet";
            this.btnULdBarcodeBGet.Size = new System.Drawing.Size(166, 66);
            this.btnULdBarcodeBGet.TabIndex = 29;
            this.btnULdBarcodeBGet.Text = "Get";
            this.btnULdBarcodeBGet.UseVisualStyleBackColor = false;
            // 
            // tboxULdBarcodeB
            // 
            this.tboxULdBarcodeB.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxULdBarcodeB.Font = new System.Drawing.Font("맑은 고딕", 30F, System.Drawing.FontStyle.Bold);
            this.tboxULdBarcodeB.Location = new System.Drawing.Point(289, 49);
            this.tboxULdBarcodeB.Name = "tboxULdBarcodeB";
            this.tboxULdBarcodeB.Size = new System.Drawing.Size(417, 61);
            this.tboxULdBarcodeB.TabIndex = 25;
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.panel47);
            this.panel21.Controls.Add(this.btnLdBarcodeBClose);
            this.panel21.Controls.Add(this.btnLdBarcodeBOpen);
            this.panel21.Controls.Add(this.panel22);
            this.panel21.Location = new System.Drawing.Point(942, 7);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(915, 255);
            this.panel21.TabIndex = 19;
            // 
            // panel47
            // 
            this.panel47.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel47.Controls.Add(this.label76);
            this.panel47.Location = new System.Drawing.Point(3, 3);
            this.panel47.Name = "panel47";
            this.panel47.Size = new System.Drawing.Size(909, 39);
            this.panel47.TabIndex = 33;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold);
            this.label76.ForeColor = System.Drawing.Color.White;
            this.label76.Location = new System.Drawing.Point(310, 3);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(326, 32);
            this.label76.TabIndex = 24;
            this.label76.Text = "로더부 고정식 바코드 리더 B";
            // 
            // btnLdBarcodeBClose
            // 
            this.btnLdBarcodeBClose.BackColor = System.Drawing.Color.DimGray;
            this.btnLdBarcodeBClose.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnLdBarcodeBClose.ForeColor = System.Drawing.Color.White;
            this.btnLdBarcodeBClose.Location = new System.Drawing.Point(106, 48);
            this.btnLdBarcodeBClose.Name = "btnLdBarcodeBClose";
            this.btnLdBarcodeBClose.Size = new System.Drawing.Size(93, 40);
            this.btnLdBarcodeBClose.TabIndex = 10;
            this.btnLdBarcodeBClose.Text = "Close";
            this.btnLdBarcodeBClose.UseVisualStyleBackColor = false;
            // 
            // btnLdBarcodeBOpen
            // 
            this.btnLdBarcodeBOpen.BackColor = System.Drawing.Color.DimGray;
            this.btnLdBarcodeBOpen.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnLdBarcodeBOpen.ForeColor = System.Drawing.Color.White;
            this.btnLdBarcodeBOpen.Location = new System.Drawing.Point(9, 48);
            this.btnLdBarcodeBOpen.Name = "btnLdBarcodeBOpen";
            this.btnLdBarcodeBOpen.Size = new System.Drawing.Size(93, 40);
            this.btnLdBarcodeBOpen.TabIndex = 9;
            this.btnLdBarcodeBOpen.Text = "Open";
            this.btnLdBarcodeBOpen.UseVisualStyleBackColor = false;
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.btnLdBarcodeBGet);
            this.panel22.Controls.Add(this.tboxLdBarcodeB);
            this.panel22.Location = new System.Drawing.Point(9, 94);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(903, 152);
            this.panel22.TabIndex = 5;
            // 
            // btnLdBarcodeBGet
            // 
            this.btnLdBarcodeBGet.BackColor = System.Drawing.Color.DimGray;
            this.btnLdBarcodeBGet.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnLdBarcodeBGet.ForeColor = System.Drawing.Color.White;
            this.btnLdBarcodeBGet.Location = new System.Drawing.Point(42, 47);
            this.btnLdBarcodeBGet.Name = "btnLdBarcodeBGet";
            this.btnLdBarcodeBGet.Size = new System.Drawing.Size(166, 66);
            this.btnLdBarcodeBGet.TabIndex = 29;
            this.btnLdBarcodeBGet.Text = "Get";
            this.btnLdBarcodeBGet.UseVisualStyleBackColor = false;
            // 
            // tboxLdBarcodeB
            // 
            this.tboxLdBarcodeB.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxLdBarcodeB.Font = new System.Drawing.Font("맑은 고딕", 30F, System.Drawing.FontStyle.Bold);
            this.tboxLdBarcodeB.Location = new System.Drawing.Point(289, 49);
            this.tboxLdBarcodeB.Name = "tboxLdBarcodeB";
            this.tboxLdBarcodeB.Size = new System.Drawing.Size(417, 61);
            this.tboxLdBarcodeB.TabIndex = 25;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.panel44);
            this.panel15.Controls.Add(this.btnLdHandyBacordClose);
            this.panel15.Controls.Add(this.btnLdHandyBacordOpen);
            this.panel15.Controls.Add(this.panel16);
            this.panel15.Location = new System.Drawing.Point(6, 534);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(915, 255);
            this.panel15.TabIndex = 18;
            // 
            // panel44
            // 
            this.panel44.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel44.Controls.Add(this.label73);
            this.panel44.Location = new System.Drawing.Point(3, 3);
            this.panel44.Name = "panel44";
            this.panel44.Size = new System.Drawing.Size(909, 39);
            this.panel44.TabIndex = 32;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold);
            this.label73.ForeColor = System.Drawing.Color.White;
            this.label73.Location = new System.Drawing.Point(358, 3);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(199, 32);
            this.label73.TabIndex = 24;
            this.label73.Text = "핸디 바코드 리더";
            // 
            // btnLdHandyBacordClose
            // 
            this.btnLdHandyBacordClose.BackColor = System.Drawing.Color.DimGray;
            this.btnLdHandyBacordClose.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnLdHandyBacordClose.ForeColor = System.Drawing.Color.White;
            this.btnLdHandyBacordClose.Location = new System.Drawing.Point(106, 48);
            this.btnLdHandyBacordClose.Name = "btnLdHandyBacordClose";
            this.btnLdHandyBacordClose.Size = new System.Drawing.Size(93, 40);
            this.btnLdHandyBacordClose.TabIndex = 10;
            this.btnLdHandyBacordClose.Text = "Close";
            this.btnLdHandyBacordClose.UseVisualStyleBackColor = false;
            // 
            // btnLdHandyBacordOpen
            // 
            this.btnLdHandyBacordOpen.BackColor = System.Drawing.Color.DimGray;
            this.btnLdHandyBacordOpen.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnLdHandyBacordOpen.ForeColor = System.Drawing.Color.White;
            this.btnLdHandyBacordOpen.Location = new System.Drawing.Point(9, 48);
            this.btnLdHandyBacordOpen.Name = "btnLdHandyBacordOpen";
            this.btnLdHandyBacordOpen.Size = new System.Drawing.Size(93, 40);
            this.btnLdHandyBacordOpen.TabIndex = 9;
            this.btnLdHandyBacordOpen.Text = "Open";
            this.btnLdHandyBacordOpen.UseVisualStyleBackColor = false;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.btnLdHandyBacordGet);
            this.panel16.Controls.Add(this.tboxLdHandyBarcode);
            this.panel16.Location = new System.Drawing.Point(9, 94);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(895, 152);
            this.panel16.TabIndex = 5;
            // 
            // btnLdHandyBacordGet
            // 
            this.btnLdHandyBacordGet.BackColor = System.Drawing.Color.DimGray;
            this.btnLdHandyBacordGet.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnLdHandyBacordGet.ForeColor = System.Drawing.Color.White;
            this.btnLdHandyBacordGet.Location = new System.Drawing.Point(42, 47);
            this.btnLdHandyBacordGet.Name = "btnLdHandyBacordGet";
            this.btnLdHandyBacordGet.Size = new System.Drawing.Size(166, 66);
            this.btnLdHandyBacordGet.TabIndex = 29;
            this.btnLdHandyBacordGet.Text = "Get";
            this.btnLdHandyBacordGet.UseVisualStyleBackColor = false;
            // 
            // tboxLdHandyBarcode
            // 
            this.tboxLdHandyBarcode.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxLdHandyBarcode.Font = new System.Drawing.Font("맑은 고딕", 30F, System.Drawing.FontStyle.Bold);
            this.tboxLdHandyBarcode.Location = new System.Drawing.Point(289, 49);
            this.tboxLdHandyBarcode.Name = "tboxLdHandyBarcode";
            this.tboxLdHandyBarcode.Size = new System.Drawing.Size(417, 61);
            this.tboxLdHandyBarcode.TabIndex = 25;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.panel43);
            this.panel13.Controls.Add(this.btnULdBarcodeAClose);
            this.panel13.Controls.Add(this.btnULdBarcodeAOpen);
            this.panel13.Controls.Add(this.panel14);
            this.panel13.Location = new System.Drawing.Point(6, 270);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(915, 255);
            this.panel13.TabIndex = 17;
            // 
            // panel43
            // 
            this.panel43.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel43.Controls.Add(this.label72);
            this.panel43.Location = new System.Drawing.Point(3, 3);
            this.panel43.Name = "panel43";
            this.panel43.Size = new System.Drawing.Size(909, 39);
            this.panel43.TabIndex = 32;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold);
            this.label72.ForeColor = System.Drawing.Color.White;
            this.label72.Location = new System.Drawing.Point(279, 4);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(352, 32);
            this.label72.TabIndex = 24;
            this.label72.Text = "언로더부 고정식 바코드 리더 A";
            // 
            // btnULdBarcodeAClose
            // 
            this.btnULdBarcodeAClose.BackColor = System.Drawing.Color.DimGray;
            this.btnULdBarcodeAClose.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnULdBarcodeAClose.ForeColor = System.Drawing.Color.White;
            this.btnULdBarcodeAClose.Location = new System.Drawing.Point(106, 48);
            this.btnULdBarcodeAClose.Name = "btnULdBarcodeAClose";
            this.btnULdBarcodeAClose.Size = new System.Drawing.Size(93, 40);
            this.btnULdBarcodeAClose.TabIndex = 10;
            this.btnULdBarcodeAClose.Text = "Close";
            this.btnULdBarcodeAClose.UseVisualStyleBackColor = false;
            // 
            // btnULdBarcodeAOpen
            // 
            this.btnULdBarcodeAOpen.BackColor = System.Drawing.Color.DimGray;
            this.btnULdBarcodeAOpen.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnULdBarcodeAOpen.ForeColor = System.Drawing.Color.White;
            this.btnULdBarcodeAOpen.Location = new System.Drawing.Point(9, 48);
            this.btnULdBarcodeAOpen.Name = "btnULdBarcodeAOpen";
            this.btnULdBarcodeAOpen.Size = new System.Drawing.Size(93, 40);
            this.btnULdBarcodeAOpen.TabIndex = 9;
            this.btnULdBarcodeAOpen.Text = "Open";
            this.btnULdBarcodeAOpen.UseVisualStyleBackColor = false;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.btnULdBarcodeAGet);
            this.panel14.Controls.Add(this.tboxULdBarcodeA);
            this.panel14.Location = new System.Drawing.Point(9, 94);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(895, 152);
            this.panel14.TabIndex = 5;
            // 
            // btnULdBarcodeAGet
            // 
            this.btnULdBarcodeAGet.BackColor = System.Drawing.Color.DimGray;
            this.btnULdBarcodeAGet.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnULdBarcodeAGet.ForeColor = System.Drawing.Color.White;
            this.btnULdBarcodeAGet.Location = new System.Drawing.Point(42, 47);
            this.btnULdBarcodeAGet.Name = "btnULdBarcodeAGet";
            this.btnULdBarcodeAGet.Size = new System.Drawing.Size(166, 66);
            this.btnULdBarcodeAGet.TabIndex = 29;
            this.btnULdBarcodeAGet.Text = "Get";
            this.btnULdBarcodeAGet.UseVisualStyleBackColor = false;
            // 
            // tboxULdBarcodeA
            // 
            this.tboxULdBarcodeA.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxULdBarcodeA.Font = new System.Drawing.Font("맑은 고딕", 30F, System.Drawing.FontStyle.Bold);
            this.tboxULdBarcodeA.Location = new System.Drawing.Point(289, 49);
            this.tboxULdBarcodeA.Name = "tboxULdBarcodeA";
            this.tboxULdBarcodeA.Size = new System.Drawing.Size(417, 61);
            this.tboxULdBarcodeA.TabIndex = 25;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.panel42);
            this.panel11.Controls.Add(this.btnLdBarcodeAClose);
            this.panel11.Controls.Add(this.btnLdBarcodeAOpen);
            this.panel11.Controls.Add(this.panel12);
            this.panel11.Location = new System.Drawing.Point(6, 7);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(915, 255);
            this.panel11.TabIndex = 16;
            // 
            // panel42
            // 
            this.panel42.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel42.Controls.Add(this.label71);
            this.panel42.Location = new System.Drawing.Point(3, 3);
            this.panel42.Name = "panel42";
            this.panel42.Size = new System.Drawing.Size(909, 39);
            this.panel42.TabIndex = 32;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold);
            this.label71.ForeColor = System.Drawing.Color.White;
            this.label71.Location = new System.Drawing.Point(310, 3);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(328, 32);
            this.label71.TabIndex = 24;
            this.label71.Text = "로더부 고정식 바코드 리더 A";
            // 
            // btnLdBarcodeAClose
            // 
            this.btnLdBarcodeAClose.BackColor = System.Drawing.Color.DimGray;
            this.btnLdBarcodeAClose.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnLdBarcodeAClose.ForeColor = System.Drawing.Color.White;
            this.btnLdBarcodeAClose.Location = new System.Drawing.Point(106, 48);
            this.btnLdBarcodeAClose.Name = "btnLdBarcodeAClose";
            this.btnLdBarcodeAClose.Size = new System.Drawing.Size(93, 40);
            this.btnLdBarcodeAClose.TabIndex = 10;
            this.btnLdBarcodeAClose.Text = "Close";
            this.btnLdBarcodeAClose.UseVisualStyleBackColor = false;
            // 
            // btnLdBarcodeAOpen
            // 
            this.btnLdBarcodeAOpen.BackColor = System.Drawing.Color.DimGray;
            this.btnLdBarcodeAOpen.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnLdBarcodeAOpen.ForeColor = System.Drawing.Color.White;
            this.btnLdBarcodeAOpen.Location = new System.Drawing.Point(9, 48);
            this.btnLdBarcodeAOpen.Name = "btnLdBarcodeAOpen";
            this.btnLdBarcodeAOpen.Size = new System.Drawing.Size(93, 40);
            this.btnLdBarcodeAOpen.TabIndex = 9;
            this.btnLdBarcodeAOpen.Text = "Open";
            this.btnLdBarcodeAOpen.UseVisualStyleBackColor = false;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.btnLdBarcodeAGet);
            this.panel12.Controls.Add(this.tboxLdBarcodeA);
            this.panel12.Location = new System.Drawing.Point(9, 94);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(895, 152);
            this.panel12.TabIndex = 5;
            // 
            // btnLdBarcodeAGet
            // 
            this.btnLdBarcodeAGet.BackColor = System.Drawing.Color.DimGray;
            this.btnLdBarcodeAGet.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnLdBarcodeAGet.ForeColor = System.Drawing.Color.White;
            this.btnLdBarcodeAGet.Location = new System.Drawing.Point(42, 47);
            this.btnLdBarcodeAGet.Name = "btnLdBarcodeAGet";
            this.btnLdBarcodeAGet.Size = new System.Drawing.Size(166, 66);
            this.btnLdBarcodeAGet.TabIndex = 29;
            this.btnLdBarcodeAGet.Text = "Get";
            this.btnLdBarcodeAGet.UseVisualStyleBackColor = false;
            // 
            // tboxLdBarcodeA
            // 
            this.tboxLdBarcodeA.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxLdBarcodeA.Font = new System.Drawing.Font("맑은 고딕", 30F, System.Drawing.FontStyle.Bold);
            this.tboxLdBarcodeA.Location = new System.Drawing.Point(289, 49);
            this.tboxLdBarcodeA.Name = "tboxLdBarcodeA";
            this.tboxLdBarcodeA.Size = new System.Drawing.Size(417, 61);
            this.tboxLdBarcodeA.TabIndex = 25;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.DimGray;
            this.tabPage3.Controls.Add(this.panel23);
            this.tabPage3.Controls.Add(this.panel25);
            this.tabPage3.Controls.Add(this.panel27);
            this.tabPage3.Controls.Add(this.panel30);
            this.tabPage3.Font = new System.Drawing.Font("굴림", 20F);
            this.tabPage3.Location = new System.Drawing.Point(4, 44);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1861, 827);
            this.tabPage3.TabIndex = 5;
            this.tabPage3.Text = "틸트 센서 / 파워미터 / LDS";
            // 
            // panel23
            // 
            this.panel23.Controls.Add(this.panel40);
            this.panel23.Controls.Add(this.label46);
            this.panel23.Controls.Add(this.label47);
            this.panel23.Controls.Add(this.tboxLDSBreakTableIdx);
            this.panel23.Controls.Add(this.label48);
            this.panel23.Controls.Add(this.tboxLDSBreakMeasure);
            this.panel23.Controls.Add(this.label49);
            this.panel23.Controls.Add(this.btnLDSBreakSave);
            this.panel23.Controls.Add(this.tboxLDSBreakStandby);
            this.panel23.Controls.Add(this.btnLDSBreakStop);
            this.panel23.Controls.Add(this.btnLDSBreakStart);
            this.panel23.Controls.Add(this.panel24);
            this.panel23.Location = new System.Drawing.Point(944, 412);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(915, 400);
            this.panel23.TabIndex = 58;
            // 
            // panel40
            // 
            this.panel40.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel40.Controls.Add(this.label69);
            this.panel40.Location = new System.Drawing.Point(3, 3);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(909, 39);
            this.panel40.TabIndex = 47;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.BackColor = System.Drawing.SystemColors.HotTrack;
            this.label69.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold);
            this.label69.ForeColor = System.Drawing.Color.White;
            this.label69.Location = new System.Drawing.Point(363, 3);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(143, 32);
            this.label69.TabIndex = 24;
            this.label69.Text = "LDS BREAK";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("맑은 고딕", 10F);
            this.label46.ForeColor = System.Drawing.Color.White;
            this.label46.Location = new System.Drawing.Point(597, 63);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(51, 19);
            this.label46.TabIndex = 53;
            this.label46.Text = "대기중";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label47.ForeColor = System.Drawing.Color.White;
            this.label47.Location = new System.Drawing.Point(392, 63);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(90, 15);
            this.label47.TabIndex = 52;
            this.label47.Text = "테이블 인덱스 :";
            // 
            // tboxLDSBreakTableIdx
            // 
            this.tboxLDSBreakTableIdx.BackColor = System.Drawing.Color.White;
            this.tboxLDSBreakTableIdx.Font = new System.Drawing.Font("맑은 고딕", 15F);
            this.tboxLDSBreakTableIdx.ForeColor = System.Drawing.Color.White;
            this.tboxLDSBreakTableIdx.Location = new System.Drawing.Point(503, 55);
            this.tboxLDSBreakTableIdx.Name = "tboxLDSBreakTableIdx";
            this.tboxLDSBreakTableIdx.Size = new System.Drawing.Size(64, 34);
            this.tboxLDSBreakTableIdx.TabIndex = 51;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label48.ForeColor = System.Drawing.Color.White;
            this.label48.Location = new System.Drawing.Point(214, 63);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(72, 15);
            this.label48.TabIndex = 50;
            this.label48.Text = "측정 시간[s]";
            // 
            // tboxLDSBreakMeasure
            // 
            this.tboxLDSBreakMeasure.BackColor = System.Drawing.Color.White;
            this.tboxLDSBreakMeasure.Font = new System.Drawing.Font("맑은 고딕", 15F);
            this.tboxLDSBreakMeasure.ForeColor = System.Drawing.Color.White;
            this.tboxLDSBreakMeasure.Location = new System.Drawing.Point(307, 55);
            this.tboxLDSBreakMeasure.Name = "tboxLDSBreakMeasure";
            this.tboxLDSBreakMeasure.Size = new System.Drawing.Size(64, 34);
            this.tboxLDSBreakMeasure.TabIndex = 49;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label49.ForeColor = System.Drawing.Color.White;
            this.label49.Location = new System.Drawing.Point(36, 63);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(72, 15);
            this.label49.TabIndex = 48;
            this.label49.Text = "대기 시간[s]";
            // 
            // btnLDSBreakSave
            // 
            this.btnLDSBreakSave.BackColor = System.Drawing.Color.DimGray;
            this.btnLDSBreakSave.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnLDSBreakSave.ForeColor = System.Drawing.Color.White;
            this.btnLDSBreakSave.Location = new System.Drawing.Point(789, 359);
            this.btnLDSBreakSave.Name = "btnLDSBreakSave";
            this.btnLDSBreakSave.Size = new System.Drawing.Size(114, 32);
            this.btnLDSBreakSave.TabIndex = 30;
            this.btnLDSBreakSave.Text = "Save";
            this.btnLDSBreakSave.UseVisualStyleBackColor = false;
            // 
            // tboxLDSBreakStandby
            // 
            this.tboxLDSBreakStandby.BackColor = System.Drawing.Color.White;
            this.tboxLDSBreakStandby.Font = new System.Drawing.Font("맑은 고딕", 15F);
            this.tboxLDSBreakStandby.ForeColor = System.Drawing.Color.White;
            this.tboxLDSBreakStandby.Location = new System.Drawing.Point(129, 55);
            this.tboxLDSBreakStandby.Name = "tboxLDSBreakStandby";
            this.tboxLDSBreakStandby.Size = new System.Drawing.Size(64, 34);
            this.tboxLDSBreakStandby.TabIndex = 47;
            // 
            // btnLDSBreakStop
            // 
            this.btnLDSBreakStop.BackColor = System.Drawing.Color.DimGray;
            this.btnLDSBreakStop.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnLDSBreakStop.ForeColor = System.Drawing.Color.White;
            this.btnLDSBreakStop.Location = new System.Drawing.Point(767, 49);
            this.btnLDSBreakStop.Name = "btnLDSBreakStop";
            this.btnLDSBreakStop.Size = new System.Drawing.Size(93, 40);
            this.btnLDSBreakStop.TabIndex = 14;
            this.btnLDSBreakStop.Text = "측정 정지";
            this.btnLDSBreakStop.UseVisualStyleBackColor = false;
            // 
            // btnLDSBreakStart
            // 
            this.btnLDSBreakStart.BackColor = System.Drawing.Color.DimGray;
            this.btnLDSBreakStart.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnLDSBreakStart.ForeColor = System.Drawing.Color.White;
            this.btnLDSBreakStart.Location = new System.Drawing.Point(670, 49);
            this.btnLDSBreakStart.Name = "btnLDSBreakStart";
            this.btnLDSBreakStart.Size = new System.Drawing.Size(93, 40);
            this.btnLDSBreakStart.TabIndex = 13;
            this.btnLDSBreakStart.Text = "측정 시작";
            this.btnLDSBreakStart.UseVisualStyleBackColor = false;
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.lvLDSBreak);
            this.panel24.Controls.Add(this.label50);
            this.panel24.Controls.Add(this.tboxLDSBreakYInterval);
            this.panel24.Controls.Add(this.label51);
            this.panel24.Controls.Add(this.tboxLSDBreakValue9);
            this.panel24.Controls.Add(this.label52);
            this.panel24.Controls.Add(this.label53);
            this.panel24.Controls.Add(this.tboxLSDBreakValue6);
            this.panel24.Controls.Add(this.tboxLSDBreakValue3);
            this.panel24.Controls.Add(this.label54);
            this.panel24.Controls.Add(this.tboxLSDBreakValue8);
            this.panel24.Controls.Add(this.label55);
            this.panel24.Controls.Add(this.label56);
            this.panel24.Controls.Add(this.tboxLSDBreakValue5);
            this.panel24.Controls.Add(this.tboxLSDBreakValue2);
            this.panel24.Controls.Add(this.tboxLSDBreakCurrentValue);
            this.panel24.Controls.Add(this.label57);
            this.panel24.Controls.Add(this.label58);
            this.panel24.Controls.Add(this.tboxLDSBreakXInterval);
            this.panel24.Controls.Add(this.tboxLSDBreakValue7);
            this.panel24.Controls.Add(this.label59);
            this.panel24.Controls.Add(this.label60);
            this.panel24.Controls.Add(this.tboxLSDBreakValue4);
            this.panel24.Controls.Add(this.tboxLSDBreakValue1);
            this.panel24.Controls.Add(this.label61);
            this.panel24.Location = new System.Drawing.Point(8, 99);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(895, 254);
            this.panel24.TabIndex = 9;
            // 
            // lvLDSBreak
            // 
            this.lvLDSBreak.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6});
            this.lvLDSBreak.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lvLDSBreak.GridLines = true;
            this.lvLDSBreak.Location = new System.Drawing.Point(418, 29);
            this.lvLDSBreak.Name = "lvLDSBreak";
            this.lvLDSBreak.Size = new System.Drawing.Size(463, 222);
            this.lvLDSBreak.TabIndex = 46;
            this.lvLDSBreak.UseCompatibleStateImageBehavior = false;
            this.lvLDSBreak.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "인덱스";
            this.columnHeader4.Width = 134;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "측정값";
            this.columnHeader5.Width = 121;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "차이값";
            this.columnHeader6.Width = 130;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label50.ForeColor = System.Drawing.Color.White;
            this.label50.Location = new System.Drawing.Point(209, 215);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(100, 15);
            this.label50.TabIndex = 45;
            this.label50.Text = "Y 이동간격 (mm)";
            // 
            // tboxLDSBreakYInterval
            // 
            this.tboxLDSBreakYInterval.BackColor = System.Drawing.Color.White;
            this.tboxLDSBreakYInterval.Font = new System.Drawing.Font("맑은 고딕", 15F);
            this.tboxLDSBreakYInterval.ForeColor = System.Drawing.Color.White;
            this.tboxLDSBreakYInterval.Location = new System.Drawing.Point(314, 203);
            this.tboxLDSBreakYInterval.Name = "tboxLDSBreakYInterval";
            this.tboxLDSBreakYInterval.Size = new System.Drawing.Size(64, 34);
            this.tboxLDSBreakYInterval.TabIndex = 44;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label51.ForeColor = System.Drawing.Color.White;
            this.label51.Location = new System.Drawing.Point(277, 162);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(17, 20);
            this.label51.TabIndex = 43;
            this.label51.Text = "9";
            // 
            // tboxLSDBreakValue9
            // 
            this.tboxLSDBreakValue9.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxLSDBreakValue9.Font = new System.Drawing.Font("맑은 고딕", 15F);
            this.tboxLSDBreakValue9.ForeColor = System.Drawing.Color.White;
            this.tboxLSDBreakValue9.Location = new System.Drawing.Point(314, 152);
            this.tboxLSDBreakValue9.Name = "tboxLSDBreakValue9";
            this.tboxLSDBreakValue9.Size = new System.Drawing.Size(64, 34);
            this.tboxLSDBreakValue9.TabIndex = 42;
            this.tboxLSDBreakValue9.Text = "미측정";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label52.ForeColor = System.Drawing.Color.White;
            this.label52.Location = new System.Drawing.Point(277, 115);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(17, 20);
            this.label52.TabIndex = 41;
            this.label52.Text = "6";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label53.ForeColor = System.Drawing.Color.White;
            this.label53.Location = new System.Drawing.Point(277, 68);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(17, 20);
            this.label53.TabIndex = 40;
            this.label53.Text = "3";
            // 
            // tboxLSDBreakValue6
            // 
            this.tboxLSDBreakValue6.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxLSDBreakValue6.Font = new System.Drawing.Font("맑은 고딕", 15F);
            this.tboxLSDBreakValue6.ForeColor = System.Drawing.Color.White;
            this.tboxLSDBreakValue6.Location = new System.Drawing.Point(314, 106);
            this.tboxLSDBreakValue6.Name = "tboxLSDBreakValue6";
            this.tboxLSDBreakValue6.Size = new System.Drawing.Size(64, 34);
            this.tboxLSDBreakValue6.TabIndex = 39;
            this.tboxLSDBreakValue6.Text = "미측정";
            // 
            // tboxLSDBreakValue3
            // 
            this.tboxLSDBreakValue3.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxLSDBreakValue3.Font = new System.Drawing.Font("맑은 고딕", 15F);
            this.tboxLSDBreakValue3.ForeColor = System.Drawing.Color.White;
            this.tboxLSDBreakValue3.Location = new System.Drawing.Point(314, 58);
            this.tboxLSDBreakValue3.Name = "tboxLSDBreakValue3";
            this.tboxLSDBreakValue3.Size = new System.Drawing.Size(64, 34);
            this.tboxLSDBreakValue3.TabIndex = 38;
            this.tboxLSDBreakValue3.Text = "미측정";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label54.ForeColor = System.Drawing.Color.White;
            this.label54.Location = new System.Drawing.Point(151, 163);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(17, 20);
            this.label54.TabIndex = 37;
            this.label54.Text = "8";
            // 
            // tboxLSDBreakValue8
            // 
            this.tboxLSDBreakValue8.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxLSDBreakValue8.Font = new System.Drawing.Font("맑은 고딕", 15F);
            this.tboxLSDBreakValue8.ForeColor = System.Drawing.Color.White;
            this.tboxLSDBreakValue8.Location = new System.Drawing.Point(188, 153);
            this.tboxLSDBreakValue8.Name = "tboxLSDBreakValue8";
            this.tboxLSDBreakValue8.Size = new System.Drawing.Size(64, 34);
            this.tboxLSDBreakValue8.TabIndex = 36;
            this.tboxLSDBreakValue8.Text = "미측정";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label55.ForeColor = System.Drawing.Color.White;
            this.label55.Location = new System.Drawing.Point(151, 116);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(17, 20);
            this.label55.TabIndex = 35;
            this.label55.Text = "5";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label56.ForeColor = System.Drawing.Color.White;
            this.label56.Location = new System.Drawing.Point(151, 69);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(17, 20);
            this.label56.TabIndex = 34;
            this.label56.Text = "2";
            // 
            // tboxLSDBreakValue5
            // 
            this.tboxLSDBreakValue5.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxLSDBreakValue5.Font = new System.Drawing.Font("맑은 고딕", 15F);
            this.tboxLSDBreakValue5.ForeColor = System.Drawing.Color.White;
            this.tboxLSDBreakValue5.Location = new System.Drawing.Point(188, 107);
            this.tboxLSDBreakValue5.Name = "tboxLSDBreakValue5";
            this.tboxLSDBreakValue5.Size = new System.Drawing.Size(64, 34);
            this.tboxLSDBreakValue5.TabIndex = 33;
            this.tboxLSDBreakValue5.Text = "미측정";
            // 
            // tboxLSDBreakValue2
            // 
            this.tboxLSDBreakValue2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxLSDBreakValue2.Font = new System.Drawing.Font("맑은 고딕", 15F);
            this.tboxLSDBreakValue2.ForeColor = System.Drawing.Color.White;
            this.tboxLSDBreakValue2.Location = new System.Drawing.Point(188, 59);
            this.tboxLSDBreakValue2.Name = "tboxLSDBreakValue2";
            this.tboxLSDBreakValue2.Size = new System.Drawing.Size(64, 34);
            this.tboxLSDBreakValue2.TabIndex = 32;
            this.tboxLSDBreakValue2.Text = "미측정";
            // 
            // tboxLSDBreakCurrentValue
            // 
            this.tboxLSDBreakCurrentValue.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxLSDBreakCurrentValue.Font = new System.Drawing.Font("맑은 고딕", 15F, System.Drawing.FontStyle.Bold);
            this.tboxLSDBreakCurrentValue.Location = new System.Drawing.Point(166, 8);
            this.tboxLSDBreakCurrentValue.Name = "tboxLSDBreakCurrentValue";
            this.tboxLSDBreakCurrentValue.Size = new System.Drawing.Size(212, 34);
            this.tboxLSDBreakCurrentValue.TabIndex = 31;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label57.ForeColor = System.Drawing.Color.White;
            this.label57.Location = new System.Drawing.Point(18, 213);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(100, 15);
            this.label57.TabIndex = 26;
            this.label57.Text = "X 이동간격 (mm)";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label58.ForeColor = System.Drawing.Color.White;
            this.label58.Location = new System.Drawing.Point(23, 163);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(17, 20);
            this.label58.TabIndex = 25;
            this.label58.Text = "7";
            // 
            // tboxLDSBreakXInterval
            // 
            this.tboxLDSBreakXInterval.BackColor = System.Drawing.Color.White;
            this.tboxLDSBreakXInterval.Font = new System.Drawing.Font("맑은 고딕", 15F);
            this.tboxLDSBreakXInterval.ForeColor = System.Drawing.Color.White;
            this.tboxLDSBreakXInterval.Location = new System.Drawing.Point(134, 203);
            this.tboxLDSBreakXInterval.Name = "tboxLDSBreakXInterval";
            this.tboxLDSBreakXInterval.Size = new System.Drawing.Size(64, 34);
            this.tboxLDSBreakXInterval.TabIndex = 24;
            // 
            // tboxLSDBreakValue7
            // 
            this.tboxLSDBreakValue7.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxLSDBreakValue7.Font = new System.Drawing.Font("맑은 고딕", 15F);
            this.tboxLSDBreakValue7.ForeColor = System.Drawing.Color.White;
            this.tboxLSDBreakValue7.Location = new System.Drawing.Point(60, 153);
            this.tboxLSDBreakValue7.Name = "tboxLSDBreakValue7";
            this.tboxLSDBreakValue7.Size = new System.Drawing.Size(64, 34);
            this.tboxLSDBreakValue7.TabIndex = 23;
            this.tboxLSDBreakValue7.Text = "미측정";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label59.ForeColor = System.Drawing.Color.White;
            this.label59.Location = new System.Drawing.Point(23, 116);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(17, 20);
            this.label59.TabIndex = 22;
            this.label59.Text = "4";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label60.ForeColor = System.Drawing.Color.White;
            this.label60.Location = new System.Drawing.Point(23, 69);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(17, 20);
            this.label60.TabIndex = 21;
            this.label60.Text = "1";
            // 
            // tboxLSDBreakValue4
            // 
            this.tboxLSDBreakValue4.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxLSDBreakValue4.Font = new System.Drawing.Font("맑은 고딕", 15F);
            this.tboxLSDBreakValue4.ForeColor = System.Drawing.Color.White;
            this.tboxLSDBreakValue4.Location = new System.Drawing.Point(60, 107);
            this.tboxLSDBreakValue4.Name = "tboxLSDBreakValue4";
            this.tboxLSDBreakValue4.Size = new System.Drawing.Size(64, 34);
            this.tboxLSDBreakValue4.TabIndex = 20;
            this.tboxLSDBreakValue4.Text = "미측정";
            // 
            // tboxLSDBreakValue1
            // 
            this.tboxLSDBreakValue1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxLSDBreakValue1.Font = new System.Drawing.Font("맑은 고딕", 15F);
            this.tboxLSDBreakValue1.ForeColor = System.Drawing.Color.White;
            this.tboxLSDBreakValue1.Location = new System.Drawing.Point(60, 59);
            this.tboxLSDBreakValue1.Name = "tboxLSDBreakValue1";
            this.tboxLSDBreakValue1.Size = new System.Drawing.Size(64, 34);
            this.tboxLSDBreakValue1.TabIndex = 19;
            this.tboxLSDBreakValue1.Text = "미측정";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.label61.ForeColor = System.Drawing.Color.White;
            this.label61.Location = new System.Drawing.Point(17, 18);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(138, 20);
            this.label61.TabIndex = 18;
            this.label61.Text = "LDS 현재 값(mm) :";
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.panel39);
            this.panel25.Controls.Add(this.btnPowerClose);
            this.panel25.Controls.Add(this.btnPowerOpen);
            this.panel25.Controls.Add(this.panel26);
            this.panel25.Location = new System.Drawing.Point(944, 6);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(915, 400);
            this.panel25.TabIndex = 56;
            // 
            // panel39
            // 
            this.panel39.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel39.Controls.Add(this.label68);
            this.panel39.Location = new System.Drawing.Point(3, 3);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(909, 39);
            this.panel39.TabIndex = 32;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold);
            this.label68.ForeColor = System.Drawing.Color.White;
            this.label68.Location = new System.Drawing.Point(363, 3);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(153, 32);
            this.label68.TabIndex = 24;
            this.label68.Text = "PowerMeter";
            // 
            // btnPowerClose
            // 
            this.btnPowerClose.BackColor = System.Drawing.Color.DimGray;
            this.btnPowerClose.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnPowerClose.ForeColor = System.Drawing.Color.White;
            this.btnPowerClose.Location = new System.Drawing.Point(106, 48);
            this.btnPowerClose.Name = "btnPowerClose";
            this.btnPowerClose.Size = new System.Drawing.Size(93, 40);
            this.btnPowerClose.TabIndex = 10;
            this.btnPowerClose.Text = "Close";
            this.btnPowerClose.UseVisualStyleBackColor = false;
            // 
            // btnPowerOpen
            // 
            this.btnPowerOpen.BackColor = System.Drawing.Color.DimGray;
            this.btnPowerOpen.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnPowerOpen.ForeColor = System.Drawing.Color.White;
            this.btnPowerOpen.Location = new System.Drawing.Point(9, 48);
            this.btnPowerOpen.Name = "btnPowerOpen";
            this.btnPowerOpen.Size = new System.Drawing.Size(93, 40);
            this.btnPowerOpen.TabIndex = 9;
            this.btnPowerOpen.Text = "Open";
            this.btnPowerOpen.UseVisualStyleBackColor = false;
            // 
            // panel26
            // 
            this.panel26.Controls.Add(this.btnPowerGet);
            this.panel26.Controls.Add(this.tboxPowerGet);
            this.panel26.Location = new System.Drawing.Point(9, 104);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(894, 283);
            this.panel26.TabIndex = 5;
            // 
            // btnPowerGet
            // 
            this.btnPowerGet.BackColor = System.Drawing.Color.DimGray;
            this.btnPowerGet.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnPowerGet.ForeColor = System.Drawing.Color.White;
            this.btnPowerGet.Location = new System.Drawing.Point(32, 106);
            this.btnPowerGet.Name = "btnPowerGet";
            this.btnPowerGet.Size = new System.Drawing.Size(166, 81);
            this.btnPowerGet.TabIndex = 29;
            this.btnPowerGet.Text = "Get";
            this.btnPowerGet.UseVisualStyleBackColor = false;
            // 
            // tboxPowerGet
            // 
            this.tboxPowerGet.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxPowerGet.Font = new System.Drawing.Font("맑은 고딕", 40F, System.Drawing.FontStyle.Bold);
            this.tboxPowerGet.Location = new System.Drawing.Point(289, 106);
            this.tboxPowerGet.Name = "tboxPowerGet";
            this.tboxPowerGet.Size = new System.Drawing.Size(514, 78);
            this.tboxPowerGet.TabIndex = 25;
            // 
            // panel27
            // 
            this.panel27.Controls.Add(this.panel41);
            this.panel27.Controls.Add(this.label45);
            this.panel27.Controls.Add(this.label44);
            this.panel27.Controls.Add(this.tboxLDSProTableIdx);
            this.panel27.Controls.Add(this.label43);
            this.panel27.Controls.Add(this.tboxLDSProMeasure);
            this.panel27.Controls.Add(this.label42);
            this.panel27.Controls.Add(this.btnLDSProcessSave);
            this.panel27.Controls.Add(this.tboxLDSproStandby);
            this.panel27.Controls.Add(this.btnLDSProcessStop);
            this.panel27.Controls.Add(this.btnLDSProcessStart);
            this.panel27.Controls.Add(this.panel29);
            this.panel27.Location = new System.Drawing.Point(6, 412);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(915, 400);
            this.panel27.TabIndex = 57;
            // 
            // panel41
            // 
            this.panel41.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel41.Controls.Add(this.label70);
            this.panel41.Location = new System.Drawing.Point(3, 3);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(909, 39);
            this.panel41.TabIndex = 54;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold);
            this.label70.ForeColor = System.Drawing.Color.White;
            this.label70.Location = new System.Drawing.Point(363, 3);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(173, 32);
            this.label70.TabIndex = 24;
            this.label70.Text = "LDS PROCESS";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("맑은 고딕", 10F);
            this.label45.ForeColor = System.Drawing.Color.White;
            this.label45.Location = new System.Drawing.Point(597, 63);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(51, 19);
            this.label45.TabIndex = 53;
            this.label45.Text = "대기중";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label44.ForeColor = System.Drawing.Color.White;
            this.label44.Location = new System.Drawing.Point(392, 63);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(90, 15);
            this.label44.TabIndex = 52;
            this.label44.Text = "테이블 인덱스 :";
            // 
            // tboxLDSProTableIdx
            // 
            this.tboxLDSProTableIdx.BackColor = System.Drawing.Color.White;
            this.tboxLDSProTableIdx.Font = new System.Drawing.Font("맑은 고딕", 15F);
            this.tboxLDSProTableIdx.ForeColor = System.Drawing.Color.White;
            this.tboxLDSProTableIdx.Location = new System.Drawing.Point(503, 55);
            this.tboxLDSProTableIdx.Name = "tboxLDSProTableIdx";
            this.tboxLDSProTableIdx.Size = new System.Drawing.Size(64, 34);
            this.tboxLDSProTableIdx.TabIndex = 51;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label43.ForeColor = System.Drawing.Color.White;
            this.label43.Location = new System.Drawing.Point(214, 63);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(72, 15);
            this.label43.TabIndex = 50;
            this.label43.Text = "측정 시간[s]";
            // 
            // tboxLDSProMeasure
            // 
            this.tboxLDSProMeasure.BackColor = System.Drawing.Color.White;
            this.tboxLDSProMeasure.Font = new System.Drawing.Font("맑은 고딕", 15F);
            this.tboxLDSProMeasure.ForeColor = System.Drawing.Color.White;
            this.tboxLDSProMeasure.Location = new System.Drawing.Point(307, 55);
            this.tboxLDSProMeasure.Name = "tboxLDSProMeasure";
            this.tboxLDSProMeasure.Size = new System.Drawing.Size(64, 34);
            this.tboxLDSProMeasure.TabIndex = 49;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label42.ForeColor = System.Drawing.Color.White;
            this.label42.Location = new System.Drawing.Point(36, 63);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(72, 15);
            this.label42.TabIndex = 48;
            this.label42.Text = "대기 시간[s]";
            // 
            // btnLDSProcessSave
            // 
            this.btnLDSProcessSave.BackColor = System.Drawing.Color.DimGray;
            this.btnLDSProcessSave.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnLDSProcessSave.ForeColor = System.Drawing.Color.White;
            this.btnLDSProcessSave.Location = new System.Drawing.Point(791, 359);
            this.btnLDSProcessSave.Name = "btnLDSProcessSave";
            this.btnLDSProcessSave.Size = new System.Drawing.Size(114, 32);
            this.btnLDSProcessSave.TabIndex = 30;
            this.btnLDSProcessSave.Text = "Save";
            this.btnLDSProcessSave.UseVisualStyleBackColor = false;
            // 
            // tboxLDSproStandby
            // 
            this.tboxLDSproStandby.BackColor = System.Drawing.Color.White;
            this.tboxLDSproStandby.Font = new System.Drawing.Font("맑은 고딕", 15F);
            this.tboxLDSproStandby.ForeColor = System.Drawing.Color.White;
            this.tboxLDSproStandby.Location = new System.Drawing.Point(129, 55);
            this.tboxLDSproStandby.Name = "tboxLDSproStandby";
            this.tboxLDSproStandby.Size = new System.Drawing.Size(64, 34);
            this.tboxLDSproStandby.TabIndex = 47;
            // 
            // btnLDSProcessStop
            // 
            this.btnLDSProcessStop.BackColor = System.Drawing.Color.DimGray;
            this.btnLDSProcessStop.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnLDSProcessStop.ForeColor = System.Drawing.Color.White;
            this.btnLDSProcessStop.Location = new System.Drawing.Point(767, 49);
            this.btnLDSProcessStop.Name = "btnLDSProcessStop";
            this.btnLDSProcessStop.Size = new System.Drawing.Size(93, 40);
            this.btnLDSProcessStop.TabIndex = 14;
            this.btnLDSProcessStop.Text = "측정 정지";
            this.btnLDSProcessStop.UseVisualStyleBackColor = false;
            // 
            // btnLDSProcessStart
            // 
            this.btnLDSProcessStart.BackColor = System.Drawing.Color.DimGray;
            this.btnLDSProcessStart.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnLDSProcessStart.ForeColor = System.Drawing.Color.White;
            this.btnLDSProcessStart.Location = new System.Drawing.Point(670, 49);
            this.btnLDSProcessStart.Name = "btnLDSProcessStart";
            this.btnLDSProcessStart.Size = new System.Drawing.Size(93, 40);
            this.btnLDSProcessStart.TabIndex = 13;
            this.btnLDSProcessStart.Text = "측정 시작";
            this.btnLDSProcessStart.UseVisualStyleBackColor = false;
            // 
            // panel29
            // 
            this.panel29.Controls.Add(this.lvLDSProcess);
            this.panel29.Controls.Add(this.label41);
            this.panel29.Controls.Add(this.tboxLDSProcessYInterval);
            this.panel29.Controls.Add(this.label20);
            this.panel29.Controls.Add(this.tboxLDSProcessValue9);
            this.panel29.Controls.Add(this.label21);
            this.panel29.Controls.Add(this.label40);
            this.panel29.Controls.Add(this.tboxLDSProcessValue6);
            this.panel29.Controls.Add(this.tboxLDSProcessValue3);
            this.panel29.Controls.Add(this.label17);
            this.panel29.Controls.Add(this.tboxLDSProcessValue8);
            this.panel29.Controls.Add(this.label18);
            this.panel29.Controls.Add(this.label19);
            this.panel29.Controls.Add(this.tboxLDSProcessValue5);
            this.panel29.Controls.Add(this.tboxLDSProcessValue2);
            this.panel29.Controls.Add(this.tboxLDSProcessCurrentValue);
            this.panel29.Controls.Add(this.label22);
            this.panel29.Controls.Add(this.label23);
            this.panel29.Controls.Add(this.tboxLDSProcessXInterval);
            this.panel29.Controls.Add(this.tboxLDSProcessValue7);
            this.panel29.Controls.Add(this.label24);
            this.panel29.Controls.Add(this.label25);
            this.panel29.Controls.Add(this.tboxLDSProcessValue4);
            this.panel29.Controls.Add(this.tboxLDSProcessValue1);
            this.panel29.Controls.Add(this.label26);
            this.panel29.Location = new System.Drawing.Point(8, 99);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(897, 254);
            this.panel29.TabIndex = 9;
            // 
            // lvLDSProcess
            // 
            this.lvLDSProcess.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.lvLDSProcess.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lvLDSProcess.GridLines = true;
            this.lvLDSProcess.Location = new System.Drawing.Point(420, 29);
            this.lvLDSProcess.Name = "lvLDSProcess";
            this.lvLDSProcess.Size = new System.Drawing.Size(463, 222);
            this.lvLDSProcess.TabIndex = 46;
            this.lvLDSProcess.UseCompatibleStateImageBehavior = false;
            this.lvLDSProcess.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "인덱스";
            this.columnHeader1.Width = 134;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "측정값";
            this.columnHeader2.Width = 121;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "차이값";
            this.columnHeader3.Width = 130;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label41.ForeColor = System.Drawing.Color.White;
            this.label41.Location = new System.Drawing.Point(209, 215);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(100, 15);
            this.label41.TabIndex = 45;
            this.label41.Text = "Y 이동간격 (mm)";
            // 
            // tboxLDSProcessYInterval
            // 
            this.tboxLDSProcessYInterval.BackColor = System.Drawing.Color.White;
            this.tboxLDSProcessYInterval.Font = new System.Drawing.Font("맑은 고딕", 15F);
            this.tboxLDSProcessYInterval.ForeColor = System.Drawing.Color.White;
            this.tboxLDSProcessYInterval.Location = new System.Drawing.Point(314, 203);
            this.tboxLDSProcessYInterval.Name = "tboxLDSProcessYInterval";
            this.tboxLDSProcessYInterval.Size = new System.Drawing.Size(64, 34);
            this.tboxLDSProcessYInterval.TabIndex = 44;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(277, 162);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(17, 20);
            this.label20.TabIndex = 43;
            this.label20.Text = "9";
            // 
            // tboxLDSProcessValue9
            // 
            this.tboxLDSProcessValue9.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxLDSProcessValue9.Font = new System.Drawing.Font("맑은 고딕", 15F);
            this.tboxLDSProcessValue9.ForeColor = System.Drawing.Color.White;
            this.tboxLDSProcessValue9.Location = new System.Drawing.Point(314, 152);
            this.tboxLDSProcessValue9.Name = "tboxLDSProcessValue9";
            this.tboxLDSProcessValue9.Size = new System.Drawing.Size(64, 34);
            this.tboxLDSProcessValue9.TabIndex = 42;
            this.tboxLDSProcessValue9.Text = "미측정";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(277, 115);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(17, 20);
            this.label21.TabIndex = 41;
            this.label21.Text = "6";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label40.ForeColor = System.Drawing.Color.White;
            this.label40.Location = new System.Drawing.Point(277, 68);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(17, 20);
            this.label40.TabIndex = 40;
            this.label40.Text = "3";
            // 
            // tboxLDSProcessValue6
            // 
            this.tboxLDSProcessValue6.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxLDSProcessValue6.Font = new System.Drawing.Font("맑은 고딕", 15F);
            this.tboxLDSProcessValue6.ForeColor = System.Drawing.Color.White;
            this.tboxLDSProcessValue6.Location = new System.Drawing.Point(314, 106);
            this.tboxLDSProcessValue6.Name = "tboxLDSProcessValue6";
            this.tboxLDSProcessValue6.Size = new System.Drawing.Size(64, 34);
            this.tboxLDSProcessValue6.TabIndex = 39;
            this.tboxLDSProcessValue6.Text = "미측정";
            // 
            // tboxLDSProcessValue3
            // 
            this.tboxLDSProcessValue3.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxLDSProcessValue3.Font = new System.Drawing.Font("맑은 고딕", 15F);
            this.tboxLDSProcessValue3.ForeColor = System.Drawing.Color.White;
            this.tboxLDSProcessValue3.Location = new System.Drawing.Point(314, 58);
            this.tboxLDSProcessValue3.Name = "tboxLDSProcessValue3";
            this.tboxLDSProcessValue3.Size = new System.Drawing.Size(64, 34);
            this.tboxLDSProcessValue3.TabIndex = 38;
            this.tboxLDSProcessValue3.Text = "미측정";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(151, 163);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(17, 20);
            this.label17.TabIndex = 37;
            this.label17.Text = "8";
            // 
            // tboxLDSProcessValue8
            // 
            this.tboxLDSProcessValue8.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxLDSProcessValue8.Font = new System.Drawing.Font("맑은 고딕", 15F);
            this.tboxLDSProcessValue8.ForeColor = System.Drawing.Color.White;
            this.tboxLDSProcessValue8.Location = new System.Drawing.Point(188, 153);
            this.tboxLDSProcessValue8.Name = "tboxLDSProcessValue8";
            this.tboxLDSProcessValue8.Size = new System.Drawing.Size(64, 34);
            this.tboxLDSProcessValue8.TabIndex = 36;
            this.tboxLDSProcessValue8.Text = "미측정";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(151, 116);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(17, 20);
            this.label18.TabIndex = 35;
            this.label18.Text = "5";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(151, 69);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(17, 20);
            this.label19.TabIndex = 34;
            this.label19.Text = "2";
            // 
            // tboxLDSProcessValue5
            // 
            this.tboxLDSProcessValue5.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxLDSProcessValue5.Font = new System.Drawing.Font("맑은 고딕", 15F);
            this.tboxLDSProcessValue5.ForeColor = System.Drawing.Color.White;
            this.tboxLDSProcessValue5.Location = new System.Drawing.Point(188, 107);
            this.tboxLDSProcessValue5.Name = "tboxLDSProcessValue5";
            this.tboxLDSProcessValue5.Size = new System.Drawing.Size(64, 34);
            this.tboxLDSProcessValue5.TabIndex = 33;
            this.tboxLDSProcessValue5.Text = "미측정";
            // 
            // tboxLDSProcessValue2
            // 
            this.tboxLDSProcessValue2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxLDSProcessValue2.Font = new System.Drawing.Font("맑은 고딕", 15F);
            this.tboxLDSProcessValue2.ForeColor = System.Drawing.Color.White;
            this.tboxLDSProcessValue2.Location = new System.Drawing.Point(188, 59);
            this.tboxLDSProcessValue2.Name = "tboxLDSProcessValue2";
            this.tboxLDSProcessValue2.Size = new System.Drawing.Size(64, 34);
            this.tboxLDSProcessValue2.TabIndex = 32;
            this.tboxLDSProcessValue2.Text = "미측정";
            // 
            // tboxLDSProcessCurrentValue
            // 
            this.tboxLDSProcessCurrentValue.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxLDSProcessCurrentValue.Font = new System.Drawing.Font("맑은 고딕", 15F, System.Drawing.FontStyle.Bold);
            this.tboxLDSProcessCurrentValue.Location = new System.Drawing.Point(167, 8);
            this.tboxLDSProcessCurrentValue.Name = "tboxLDSProcessCurrentValue";
            this.tboxLDSProcessCurrentValue.Size = new System.Drawing.Size(212, 34);
            this.tboxLDSProcessCurrentValue.TabIndex = 31;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.label22.ForeColor = System.Drawing.Color.White;
            this.label22.Location = new System.Drawing.Point(18, 213);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(100, 15);
            this.label22.TabIndex = 26;
            this.label22.Text = "X 이동간격 (mm)";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label23.ForeColor = System.Drawing.Color.White;
            this.label23.Location = new System.Drawing.Point(23, 163);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(17, 20);
            this.label23.TabIndex = 25;
            this.label23.Text = "7";
            // 
            // tboxLDSProcessXInterval
            // 
            this.tboxLDSProcessXInterval.BackColor = System.Drawing.Color.White;
            this.tboxLDSProcessXInterval.Font = new System.Drawing.Font("맑은 고딕", 15F);
            this.tboxLDSProcessXInterval.ForeColor = System.Drawing.Color.White;
            this.tboxLDSProcessXInterval.Location = new System.Drawing.Point(134, 203);
            this.tboxLDSProcessXInterval.Name = "tboxLDSProcessXInterval";
            this.tboxLDSProcessXInterval.Size = new System.Drawing.Size(64, 34);
            this.tboxLDSProcessXInterval.TabIndex = 24;
            // 
            // tboxLDSProcessValue7
            // 
            this.tboxLDSProcessValue7.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxLDSProcessValue7.Font = new System.Drawing.Font("맑은 고딕", 15F);
            this.tboxLDSProcessValue7.ForeColor = System.Drawing.Color.White;
            this.tboxLDSProcessValue7.Location = new System.Drawing.Point(60, 153);
            this.tboxLDSProcessValue7.Name = "tboxLDSProcessValue7";
            this.tboxLDSProcessValue7.Size = new System.Drawing.Size(64, 34);
            this.tboxLDSProcessValue7.TabIndex = 23;
            this.tboxLDSProcessValue7.Text = "미측정";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label24.ForeColor = System.Drawing.Color.White;
            this.label24.Location = new System.Drawing.Point(23, 116);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(17, 20);
            this.label24.TabIndex = 22;
            this.label24.Text = "4";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label25.ForeColor = System.Drawing.Color.White;
            this.label25.Location = new System.Drawing.Point(23, 69);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(17, 20);
            this.label25.TabIndex = 21;
            this.label25.Text = "1";
            // 
            // tboxLDSProcessValue4
            // 
            this.tboxLDSProcessValue4.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxLDSProcessValue4.Font = new System.Drawing.Font("맑은 고딕", 15F);
            this.tboxLDSProcessValue4.ForeColor = System.Drawing.Color.White;
            this.tboxLDSProcessValue4.Location = new System.Drawing.Point(60, 107);
            this.tboxLDSProcessValue4.Name = "tboxLDSProcessValue4";
            this.tboxLDSProcessValue4.Size = new System.Drawing.Size(64, 34);
            this.tboxLDSProcessValue4.TabIndex = 20;
            this.tboxLDSProcessValue4.Text = "미측정";
            // 
            // tboxLDSProcessValue1
            // 
            this.tboxLDSProcessValue1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxLDSProcessValue1.Font = new System.Drawing.Font("맑은 고딕", 15F);
            this.tboxLDSProcessValue1.ForeColor = System.Drawing.Color.White;
            this.tboxLDSProcessValue1.Location = new System.Drawing.Point(60, 59);
            this.tboxLDSProcessValue1.Name = "tboxLDSProcessValue1";
            this.tboxLDSProcessValue1.Size = new System.Drawing.Size(64, 34);
            this.tboxLDSProcessValue1.TabIndex = 19;
            this.tboxLDSProcessValue1.Text = "미측정";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.label26.ForeColor = System.Drawing.Color.White;
            this.label26.Location = new System.Drawing.Point(17, 18);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(138, 20);
            this.label26.TabIndex = 18;
            this.label26.Text = "LDS 현재 값(mm) :";
            // 
            // panel30
            // 
            this.panel30.Controls.Add(this.panel38);
            this.panel30.Controls.Add(this.btnBeamClose);
            this.panel30.Controls.Add(this.btnBeamOpen);
            this.panel30.Controls.Add(this.panel31);
            this.panel30.Controls.Add(this.panel32);
            this.panel30.Location = new System.Drawing.Point(6, 6);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(915, 400);
            this.panel30.TabIndex = 55;
            // 
            // panel38
            // 
            this.panel38.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel38.Controls.Add(this.label67);
            this.panel38.Location = new System.Drawing.Point(3, 3);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(909, 39);
            this.panel38.TabIndex = 32;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold);
            this.label67.ForeColor = System.Drawing.Color.White;
            this.label67.Location = new System.Drawing.Point(363, 3);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(209, 32);
            this.label67.TabIndex = 24;
            this.label67.Text = "BeamPositionner";
            // 
            // btnBeamClose
            // 
            this.btnBeamClose.BackColor = System.Drawing.Color.DimGray;
            this.btnBeamClose.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnBeamClose.ForeColor = System.Drawing.Color.White;
            this.btnBeamClose.Location = new System.Drawing.Point(102, 48);
            this.btnBeamClose.Name = "btnBeamClose";
            this.btnBeamClose.Size = new System.Drawing.Size(93, 40);
            this.btnBeamClose.TabIndex = 8;
            this.btnBeamClose.Text = "Close";
            this.btnBeamClose.UseVisualStyleBackColor = false;
            // 
            // btnBeamOpen
            // 
            this.btnBeamOpen.BackColor = System.Drawing.Color.DimGray;
            this.btnBeamOpen.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnBeamOpen.ForeColor = System.Drawing.Color.White;
            this.btnBeamOpen.Location = new System.Drawing.Point(5, 48);
            this.btnBeamOpen.Name = "btnBeamOpen";
            this.btnBeamOpen.Size = new System.Drawing.Size(93, 40);
            this.btnBeamOpen.TabIndex = 7;
            this.btnBeamOpen.Text = "Open";
            this.btnBeamOpen.UseVisualStyleBackColor = false;
            // 
            // panel31
            // 
            this.panel31.Controls.Add(this.label39);
            this.panel31.Controls.Add(this.label37);
            this.panel31.Controls.Add(this.tboxErrorPos);
            this.panel31.Controls.Add(this.label38);
            this.panel31.Controls.Add(this.tboxTeachingY);
            this.panel31.Controls.Add(this.label27);
            this.panel31.Controls.Add(this.tboxErrorPower);
            this.panel31.Controls.Add(this.label28);
            this.panel31.Controls.Add(this.label29);
            this.panel31.Controls.Add(this.tboxTeachingX);
            this.panel31.Controls.Add(this.tboxTeachingPower);
            this.panel31.Controls.Add(this.btnBeamSave);
            this.panel31.Location = new System.Drawing.Point(466, 105);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(439, 283);
            this.panel31.TabIndex = 4;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label39.ForeColor = System.Drawing.Color.White;
            this.label39.Location = new System.Drawing.Point(39, 23);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(104, 20);
            this.label39.TabIndex = 44;
            this.label39.Text = "Limit Val 설정";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("맑은 고딕", 10F);
            this.label37.ForeColor = System.Drawing.Color.White;
            this.label37.Location = new System.Drawing.Point(239, 165);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(105, 19);
            this.label37.TabIndex = 43;
            this.label37.Text = "에러 범위 POS:";
            // 
            // tboxErrorPos
            // 
            this.tboxErrorPos.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxErrorPos.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.tboxErrorPos.Location = new System.Drawing.Point(346, 155);
            this.tboxErrorPos.Name = "tboxErrorPos";
            this.tboxErrorPos.Size = new System.Drawing.Size(70, 43);
            this.tboxErrorPos.TabIndex = 42;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("맑은 고딕", 10F);
            this.label38.ForeColor = System.Drawing.Color.White;
            this.label38.Location = new System.Drawing.Point(239, 116);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(53, 19);
            this.label38.TabIndex = 41;
            this.label38.Text = "티칭 Y:";
            // 
            // tboxTeachingY
            // 
            this.tboxTeachingY.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxTeachingY.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.tboxTeachingY.Location = new System.Drawing.Point(346, 106);
            this.tboxTeachingY.Name = "tboxTeachingY";
            this.tboxTeachingY.Size = new System.Drawing.Size(70, 43);
            this.tboxTeachingY.TabIndex = 40;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("맑은 고딕", 10F);
            this.label27.ForeColor = System.Drawing.Color.White;
            this.label27.Location = new System.Drawing.Point(18, 164);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(123, 19);
            this.label27.TabIndex = 39;
            this.label27.Text = "에러 범위 POWER";
            // 
            // tboxErrorPower
            // 
            this.tboxErrorPower.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxErrorPower.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.tboxErrorPower.Location = new System.Drawing.Point(159, 154);
            this.tboxErrorPower.Name = "tboxErrorPower";
            this.tboxErrorPower.Size = new System.Drawing.Size(70, 43);
            this.tboxErrorPower.TabIndex = 38;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("맑은 고딕", 10F);
            this.label28.ForeColor = System.Drawing.Color.White;
            this.label28.Location = new System.Drawing.Point(18, 115);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(53, 19);
            this.label28.TabIndex = 37;
            this.label28.Text = "티칭 X:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("맑은 고딕", 10F);
            this.label29.ForeColor = System.Drawing.Color.White;
            this.label29.Location = new System.Drawing.Point(18, 64);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(83, 19);
            this.label29.TabIndex = 36;
            this.label29.Text = "티칭 Power:";
            // 
            // tboxTeachingX
            // 
            this.tboxTeachingX.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxTeachingX.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.tboxTeachingX.Location = new System.Drawing.Point(159, 105);
            this.tboxTeachingX.Name = "tboxTeachingX";
            this.tboxTeachingX.Size = new System.Drawing.Size(70, 43);
            this.tboxTeachingX.TabIndex = 35;
            // 
            // tboxTeachingPower
            // 
            this.tboxTeachingPower.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxTeachingPower.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.tboxTeachingPower.Location = new System.Drawing.Point(159, 57);
            this.tboxTeachingPower.Name = "tboxTeachingPower";
            this.tboxTeachingPower.Size = new System.Drawing.Size(70, 43);
            this.tboxTeachingPower.TabIndex = 34;
            // 
            // btnBeamSave
            // 
            this.btnBeamSave.BackColor = System.Drawing.Color.DimGray;
            this.btnBeamSave.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnBeamSave.ForeColor = System.Drawing.Color.White;
            this.btnBeamSave.Location = new System.Drawing.Point(316, 219);
            this.btnBeamSave.Name = "btnBeamSave";
            this.btnBeamSave.Size = new System.Drawing.Size(99, 43);
            this.btnBeamSave.TabIndex = 29;
            this.btnBeamSave.Text = "Save";
            this.btnBeamSave.UseVisualStyleBackColor = false;
            // 
            // panel32
            // 
            this.panel32.Controls.Add(this.tableLayoutPanel2);
            this.panel32.Controls.Add(this.label36);
            this.panel32.Controls.Add(this.label35);
            this.panel32.Controls.Add(this.label34);
            this.panel32.Controls.Add(this.label30);
            this.panel32.Controls.Add(this.label31);
            this.panel32.Controls.Add(this.label32);
            this.panel32.Controls.Add(this.label33);
            this.panel32.Location = new System.Drawing.Point(8, 104);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(442, 283);
            this.panel32.TabIndex = 3;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Controls.Add(this.tboxBeamPosYMedianValue, 3, 2);
            this.tableLayoutPanel2.Controls.Add(this.tboxBeamPosYMaxValue, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.tboxBeamPosXMedianValue, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.tboxBeamPosXMaxValue, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.tboxBeamPowerMedianValue, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.tboxBeamPowerMaxValue, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.tboxBeamPosYCurrenValue, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.tboxBeamPosYMiniValue, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.tboxBeamPowerCurrenValue, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tboxBeamPowerMiniValue, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.tboxBeamPosXCurrenValue, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.tboxBeamPosXMiniValue, 1, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(119, 67);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(289, 134);
            this.tableLayoutPanel2.TabIndex = 47;
            // 
            // tboxBeamPosYMedianValue
            // 
            this.tboxBeamPosYMedianValue.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxBeamPosYMedianValue.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.tboxBeamPosYMedianValue.Location = new System.Drawing.Point(219, 91);
            this.tboxBeamPosYMedianValue.Name = "tboxBeamPosYMedianValue";
            this.tboxBeamPosYMedianValue.Size = new System.Drawing.Size(66, 43);
            this.tboxBeamPosYMedianValue.TabIndex = 40;
            // 
            // tboxBeamPosYMaxValue
            // 
            this.tboxBeamPosYMaxValue.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxBeamPosYMaxValue.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.tboxBeamPosYMaxValue.Location = new System.Drawing.Point(147, 91);
            this.tboxBeamPosYMaxValue.Name = "tboxBeamPosYMaxValue";
            this.tboxBeamPosYMaxValue.Size = new System.Drawing.Size(66, 43);
            this.tboxBeamPosYMaxValue.TabIndex = 39;
            // 
            // tboxBeamPosXMedianValue
            // 
            this.tboxBeamPosXMedianValue.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxBeamPosXMedianValue.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.tboxBeamPosXMedianValue.Location = new System.Drawing.Point(219, 47);
            this.tboxBeamPosXMedianValue.Name = "tboxBeamPosXMedianValue";
            this.tboxBeamPosXMedianValue.Size = new System.Drawing.Size(66, 43);
            this.tboxBeamPosXMedianValue.TabIndex = 38;
            // 
            // tboxBeamPosXMaxValue
            // 
            this.tboxBeamPosXMaxValue.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxBeamPosXMaxValue.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.tboxBeamPosXMaxValue.Location = new System.Drawing.Point(147, 47);
            this.tboxBeamPosXMaxValue.Name = "tboxBeamPosXMaxValue";
            this.tboxBeamPosXMaxValue.Size = new System.Drawing.Size(66, 43);
            this.tboxBeamPosXMaxValue.TabIndex = 37;
            // 
            // tboxBeamPowerMedianValue
            // 
            this.tboxBeamPowerMedianValue.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxBeamPowerMedianValue.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.tboxBeamPowerMedianValue.Location = new System.Drawing.Point(219, 3);
            this.tboxBeamPowerMedianValue.Name = "tboxBeamPowerMedianValue";
            this.tboxBeamPowerMedianValue.Size = new System.Drawing.Size(66, 43);
            this.tboxBeamPowerMedianValue.TabIndex = 36;
            // 
            // tboxBeamPowerMaxValue
            // 
            this.tboxBeamPowerMaxValue.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxBeamPowerMaxValue.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.tboxBeamPowerMaxValue.Location = new System.Drawing.Point(147, 3);
            this.tboxBeamPowerMaxValue.Name = "tboxBeamPowerMaxValue";
            this.tboxBeamPowerMaxValue.Size = new System.Drawing.Size(66, 43);
            this.tboxBeamPowerMaxValue.TabIndex = 35;
            // 
            // tboxBeamPosYCurrenValue
            // 
            this.tboxBeamPosYCurrenValue.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxBeamPosYCurrenValue.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.tboxBeamPosYCurrenValue.Location = new System.Drawing.Point(3, 91);
            this.tboxBeamPosYCurrenValue.Name = "tboxBeamPosYCurrenValue";
            this.tboxBeamPosYCurrenValue.Size = new System.Drawing.Size(66, 43);
            this.tboxBeamPosYCurrenValue.TabIndex = 30;
            // 
            // tboxBeamPosYMiniValue
            // 
            this.tboxBeamPosYMiniValue.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxBeamPosYMiniValue.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.tboxBeamPosYMiniValue.Location = new System.Drawing.Point(75, 91);
            this.tboxBeamPosYMiniValue.Name = "tboxBeamPosYMiniValue";
            this.tboxBeamPosYMiniValue.Size = new System.Drawing.Size(66, 43);
            this.tboxBeamPosYMiniValue.TabIndex = 29;
            // 
            // tboxBeamPowerCurrenValue
            // 
            this.tboxBeamPowerCurrenValue.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxBeamPowerCurrenValue.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.tboxBeamPowerCurrenValue.Location = new System.Drawing.Point(3, 3);
            this.tboxBeamPowerCurrenValue.Name = "tboxBeamPowerCurrenValue";
            this.tboxBeamPowerCurrenValue.Size = new System.Drawing.Size(66, 43);
            this.tboxBeamPowerCurrenValue.TabIndex = 28;
            // 
            // tboxBeamPowerMiniValue
            // 
            this.tboxBeamPowerMiniValue.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxBeamPowerMiniValue.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.tboxBeamPowerMiniValue.Location = new System.Drawing.Point(75, 3);
            this.tboxBeamPowerMiniValue.Name = "tboxBeamPowerMiniValue";
            this.tboxBeamPowerMiniValue.Size = new System.Drawing.Size(66, 43);
            this.tboxBeamPowerMiniValue.TabIndex = 27;
            // 
            // tboxBeamPosXCurrenValue
            // 
            this.tboxBeamPosXCurrenValue.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxBeamPosXCurrenValue.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.tboxBeamPosXCurrenValue.Location = new System.Drawing.Point(3, 47);
            this.tboxBeamPosXCurrenValue.Name = "tboxBeamPosXCurrenValue";
            this.tboxBeamPosXCurrenValue.Size = new System.Drawing.Size(66, 43);
            this.tboxBeamPosXCurrenValue.TabIndex = 31;
            // 
            // tboxBeamPosXMiniValue
            // 
            this.tboxBeamPosXMiniValue.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tboxBeamPosXMiniValue.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.tboxBeamPosXMiniValue.Location = new System.Drawing.Point(75, 47);
            this.tboxBeamPosXMiniValue.Name = "tboxBeamPosXMiniValue";
            this.tboxBeamPosXMiniValue.Size = new System.Drawing.Size(66, 43);
            this.tboxBeamPosXMiniValue.TabIndex = 34;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label36.ForeColor = System.Drawing.Color.White;
            this.label36.Location = new System.Drawing.Point(354, 26);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(54, 20);
            this.label36.TabIndex = 46;
            this.label36.Text = "중간값";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label35.ForeColor = System.Drawing.Color.White;
            this.label35.Location = new System.Drawing.Point(276, 26);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(54, 20);
            this.label35.TabIndex = 45;
            this.label35.Text = "최대값";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label34.ForeColor = System.Drawing.Color.White;
            this.label34.Location = new System.Drawing.Point(199, 26);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(54, 20);
            this.label34.TabIndex = 44;
            this.label34.Text = "최소값";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label30.ForeColor = System.Drawing.Color.White;
            this.label30.Location = new System.Drawing.Point(121, 26);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(54, 20);
            this.label30.TabIndex = 43;
            this.label30.Text = "현재값";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label31.ForeColor = System.Drawing.Color.White;
            this.label31.Location = new System.Drawing.Point(26, 167);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(73, 20);
            this.label31.TabIndex = 33;
            this.label31.Text = "PosY(um)";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label32.ForeColor = System.Drawing.Color.White;
            this.label32.Location = new System.Drawing.Point(26, 118);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(74, 20);
            this.label32.TabIndex = 30;
            this.label32.Text = "PosX(um)";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.label33.ForeColor = System.Drawing.Color.White;
            this.label33.Location = new System.Drawing.Point(26, 67);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(85, 20);
            this.label33.TabIndex = 29;
            this.label33.Text = "power(mw)";
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.DimGray;
            this.tabPage4.Controls.Add(this.panel28);
            this.tabPage4.Location = new System.Drawing.Point(4, 44);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1861, 827);
            this.tabPage4.TabIndex = 6;
            this.tabPage4.Text = "컷라인 계측";
            // 
            // panel28
            // 
            this.panel28.Controls.Add(this.panel37);
            this.panel28.Controls.Add(this.tableLayoutPanel1);
            this.panel28.Location = new System.Drawing.Point(6, 6);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(915, 814);
            this.panel28.TabIndex = 6;
            // 
            // panel37
            // 
            this.panel37.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel37.Controls.Add(this.label66);
            this.panel37.Location = new System.Drawing.Point(3, 0);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(909, 39);
            this.panel37.TabIndex = 32;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold);
            this.label66.ForeColor = System.Drawing.Color.White;
            this.label66.Location = new System.Drawing.Point(399, 2);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(143, 32);
            this.label66.TabIndex = 24;
            this.label66.Text = "컷라인 계측";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Controls.Add(this.btnCutLineColBR2Table, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnCutLineColBR1Table, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnCutLineColAL2Table, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnCutLineColAL1Table, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(13, 48);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(899, 126);
            this.tableLayoutPanel1.TabIndex = 30;
            // 
            // btnCutLineColBR2Table
            // 
            this.btnCutLineColBR2Table.BackColor = System.Drawing.Color.DimGray;
            this.btnCutLineColBR2Table.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnCutLineColBR2Table.ForeColor = System.Drawing.Color.White;
            this.btnCutLineColBR2Table.Location = new System.Drawing.Point(675, 3);
            this.btnCutLineColBR2Table.Name = "btnCutLineColBR2Table";
            this.btnCutLineColBR2Table.Size = new System.Drawing.Size(221, 120);
            this.btnCutLineColBR2Table.TabIndex = 32;
            this.btnCutLineColBR2Table.Text = "B열 R2테이블";
            this.btnCutLineColBR2Table.UseVisualStyleBackColor = false;
            // 
            // btnCutLineColBR1Table
            // 
            this.btnCutLineColBR1Table.BackColor = System.Drawing.Color.DimGray;
            this.btnCutLineColBR1Table.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnCutLineColBR1Table.ForeColor = System.Drawing.Color.White;
            this.btnCutLineColBR1Table.Location = new System.Drawing.Point(451, 3);
            this.btnCutLineColBR1Table.Name = "btnCutLineColBR1Table";
            this.btnCutLineColBR1Table.Size = new System.Drawing.Size(218, 120);
            this.btnCutLineColBR1Table.TabIndex = 31;
            this.btnCutLineColBR1Table.Text = "B열 R1테이블";
            this.btnCutLineColBR1Table.UseVisualStyleBackColor = false;
            // 
            // btnCutLineColAL2Table
            // 
            this.btnCutLineColAL2Table.BackColor = System.Drawing.Color.DimGray;
            this.btnCutLineColAL2Table.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnCutLineColAL2Table.ForeColor = System.Drawing.Color.White;
            this.btnCutLineColAL2Table.Location = new System.Drawing.Point(227, 3);
            this.btnCutLineColAL2Table.Name = "btnCutLineColAL2Table";
            this.btnCutLineColAL2Table.Size = new System.Drawing.Size(218, 120);
            this.btnCutLineColAL2Table.TabIndex = 30;
            this.btnCutLineColAL2Table.Text = "A열 L2테이블";
            this.btnCutLineColAL2Table.UseVisualStyleBackColor = false;
            // 
            // btnCutLineColAL1Table
            // 
            this.btnCutLineColAL1Table.BackColor = System.Drawing.Color.DimGray;
            this.btnCutLineColAL1Table.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnCutLineColAL1Table.ForeColor = System.Drawing.Color.White;
            this.btnCutLineColAL1Table.Location = new System.Drawing.Point(3, 3);
            this.btnCutLineColAL1Table.Name = "btnCutLineColAL1Table";
            this.btnCutLineColAL1Table.Size = new System.Drawing.Size(218, 120);
            this.btnCutLineColAL1Table.TabIndex = 29;
            this.btnCutLineColAL1Table.Text = "A열 L1테이블";
            this.btnCutLineColAL1Table.UseVisualStyleBackColor = false;
            // 
            // Measure
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.Controls.Add(this.tciostatus_info);
            this.Name = "Measure";
            this.Size = new System.Drawing.Size(1880, 875);
            this.tciostatus_info.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel35.ResumeLayout(false);
            this.panel35.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel36.ResumeLayout(false);
            this.panel36.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel34.ResumeLayout(false);
            this.panel34.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel33.ResumeLayout(false);
            this.panel33.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel45.ResumeLayout(false);
            this.panel45.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.panel19.ResumeLayout(false);
            this.panel46.ResumeLayout(false);
            this.panel46.PerformLayout();
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            this.panel21.ResumeLayout(false);
            this.panel47.ResumeLayout(false);
            this.panel47.PerformLayout();
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel44.ResumeLayout(false);
            this.panel44.PerformLayout();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel43.ResumeLayout(false);
            this.panel43.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel42.ResumeLayout(false);
            this.panel42.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            this.panel40.ResumeLayout(false);
            this.panel40.PerformLayout();
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            this.panel25.ResumeLayout(false);
            this.panel39.ResumeLayout(false);
            this.panel39.PerformLayout();
            this.panel26.ResumeLayout(false);
            this.panel26.PerformLayout();
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            this.panel41.ResumeLayout(false);
            this.panel41.PerformLayout();
            this.panel29.ResumeLayout(false);
            this.panel29.PerformLayout();
            this.panel30.ResumeLayout(false);
            this.panel38.ResumeLayout(false);
            this.panel38.PerformLayout();
            this.panel31.ResumeLayout(false);
            this.panel31.PerformLayout();
            this.panel32.ResumeLayout(false);
            this.panel32.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.panel28.ResumeLayout(false);
            this.panel37.ResumeLayout(false);
            this.panel37.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tciostatus_info;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btnCutLineColBR2Table;
        private System.Windows.Forms.Button btnCutLineColBR1Table;
        private System.Windows.Forms.Button btnCutLineColAL2Table;
        private System.Windows.Forms.Button btnCutLineColAL1Table;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnMcrBClose;
        private System.Windows.Forms.Button btnMcrBOpen;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Button btnMcrBGet;
        private System.Windows.Forms.TextBox tboxMcrB;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnMcrAClose;
        private System.Windows.Forms.Button btnMcrAOpen;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Button btnMcrAGet;
        private System.Windows.Forms.TextBox tboxMcrA;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnTiltULdClose;
        private System.Windows.Forms.Button btnTiltULdOpen;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.TextBox tboxULdCheck4;
        private System.Windows.Forms.TextBox tboxULdCheck3;
        private System.Windows.Forms.TextBox tboxULdCheck2;
        private System.Windows.Forms.TextBox tboxULdCheck1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tboxULdPosition4;
        private System.Windows.Forms.TextBox tboxULdPosition3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tboxULdPosition2;
        private System.Windows.Forms.TextBox tboxULdPosition1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnTiltLdClose;
        private System.Windows.Forms.Button btnTiltLdOpen;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TextBox tboxLdCheck4;
        private System.Windows.Forms.TextBox tboxLdCheck3;
        private System.Windows.Forms.TextBox tboxLdCheck2;
        private System.Windows.Forms.TextBox tboxLdCheck1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tboxLdPosition4;
        private System.Windows.Forms.TextBox tboxLdPosition3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tboxLdPosition2;
        private System.Windows.Forms.TextBox tboxLdPosition1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnControllerSave;
        private System.Windows.Forms.Button btnControllerClose;
        private System.Windows.Forms.Button btnControllerOpen;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btn2chSet;
        private System.Windows.Forms.Button btn1chSet;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbox2chprbrightset;
        private System.Windows.Forms.TextBox tbox1chprbrightset;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbox2chbrightvalue;
        private System.Windows.Forms.TextBox tbox1chbrightvalue;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Button btnULdHandyBacordClose;
        private System.Windows.Forms.Button btnULdHandyBacordOpen;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Button btnULdHandyBacordGet;
        private System.Windows.Forms.TextBox tboxULdHandyBarcode;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Button btnULdBarcodeBClose;
        private System.Windows.Forms.Button btnULdBarcodeBOpen;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Button btnULdBarcodeBGet;
        private System.Windows.Forms.TextBox tboxULdBarcodeB;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Button btnLdBarcodeBClose;
        private System.Windows.Forms.Button btnLdBarcodeBOpen;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Button btnLdBarcodeBGet;
        private System.Windows.Forms.TextBox tboxLdBarcodeB;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Button btnLdHandyBacordClose;
        private System.Windows.Forms.Button btnLdHandyBacordOpen;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Button btnLdHandyBacordGet;
        private System.Windows.Forms.TextBox tboxLdHandyBarcode;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Button btnULdBarcodeAClose;
        private System.Windows.Forms.Button btnULdBarcodeAOpen;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Button btnULdBarcodeAGet;
        private System.Windows.Forms.TextBox tboxULdBarcodeA;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Button btnLdBarcodeAClose;
        private System.Windows.Forms.Button btnLdBarcodeAOpen;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Button btnLdBarcodeAGet;
        private System.Windows.Forms.TextBox tboxLdBarcodeA;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox tboxLDSBreakTableIdx;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox tboxLDSBreakMeasure;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Button btnLDSBreakSave;
        private System.Windows.Forms.TextBox tboxLDSBreakStandby;
        private System.Windows.Forms.Button btnLDSBreakStop;
        private System.Windows.Forms.Button btnLDSBreakStart;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.ListView lvLDSBreak;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox tboxLDSBreakYInterval;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox tboxLSDBreakValue9;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox tboxLSDBreakValue6;
        private System.Windows.Forms.TextBox tboxLSDBreakValue3;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox tboxLSDBreakValue8;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox tboxLSDBreakValue5;
        private System.Windows.Forms.TextBox tboxLSDBreakValue2;
        private System.Windows.Forms.TextBox tboxLSDBreakCurrentValue;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox tboxLDSBreakXInterval;
        private System.Windows.Forms.TextBox tboxLSDBreakValue7;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.TextBox tboxLSDBreakValue4;
        private System.Windows.Forms.TextBox tboxLSDBreakValue1;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Button btnPowerClose;
        private System.Windows.Forms.Button btnPowerOpen;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Button btnPowerGet;
        private System.Windows.Forms.TextBox tboxPowerGet;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox tboxLDSProTableIdx;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox tboxLDSProMeasure;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Button btnLDSProcessSave;
        private System.Windows.Forms.TextBox tboxLDSproStandby;
        private System.Windows.Forms.Button btnLDSProcessStop;
        private System.Windows.Forms.Button btnLDSProcessStart;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.ListView lvLDSProcess;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox tboxLDSProcessYInterval;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox tboxLDSProcessValue9;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox tboxLDSProcessValue6;
        private System.Windows.Forms.TextBox tboxLDSProcessValue3;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tboxLDSProcessValue8;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox tboxLDSProcessValue5;
        private System.Windows.Forms.TextBox tboxLDSProcessValue2;
        private System.Windows.Forms.TextBox tboxLDSProcessCurrentValue;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox tboxLDSProcessXInterval;
        private System.Windows.Forms.TextBox tboxLDSProcessValue7;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox tboxLDSProcessValue4;
        private System.Windows.Forms.TextBox tboxLDSProcessValue1;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Button btnBeamClose;
        private System.Windows.Forms.Button btnBeamOpen;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox tboxErrorPos;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox tboxTeachingY;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox tboxErrorPower;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox tboxTeachingX;
        private System.Windows.Forms.TextBox tboxTeachingPower;
        private System.Windows.Forms.Button btnBeamSave;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TextBox tboxBeamPosYMedianValue;
        private System.Windows.Forms.TextBox tboxBeamPosYMaxValue;
        private System.Windows.Forms.TextBox tboxBeamPosXMedianValue;
        private System.Windows.Forms.TextBox tboxBeamPosXMaxValue;
        private System.Windows.Forms.TextBox tboxBeamPowerMedianValue;
        private System.Windows.Forms.TextBox tboxBeamPowerMaxValue;
        private System.Windows.Forms.TextBox tboxBeamPosYCurrenValue;
        private System.Windows.Forms.TextBox tboxBeamPosYMiniValue;
        private System.Windows.Forms.TextBox tboxBeamPowerCurrenValue;
        private System.Windows.Forms.TextBox tboxBeamPowerMiniValue;
        private System.Windows.Forms.TextBox tboxBeamPosXCurrenValue;
        private System.Windows.Forms.TextBox tboxBeamPosXMiniValue;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Panel panel40;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Panel panel39;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Panel panel41;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Panel panel38;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Panel panel42;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Panel panel45;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Panel panel46;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Panel panel47;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Panel panel44;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Panel panel43;
        private System.Windows.Forms.Label label72;
    }
}
