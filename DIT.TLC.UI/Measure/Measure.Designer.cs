﻿namespace DIT.TLC.UI.Measure
{
    partial class Measure
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tc_iostatus_info = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.panel28 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.button49 = new System.Windows.Forms.Button();
            this.button43 = new System.Windows.Forms.Button();
            this.button42 = new System.Windows.Forms.Button();
            this.button48 = new System.Windows.Forms.Button();
            this.textBox92 = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btn_Mcr_B_Close = new System.Windows.Forms.Button();
            this.btn_Mcr_B_Open = new System.Windows.Forms.Button();
            this.panel10 = new System.Windows.Forms.Panel();
            this.btn_Mcr_B_Get = new System.Windows.Forms.Button();
            this.tbox_Mcr_B = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btn_Mcr_A_Close = new System.Windows.Forms.Button();
            this.btn_Mcr_A_Open = new System.Windows.Forms.Button();
            this.panel9 = new System.Windows.Forms.Panel();
            this.btn_Mcr_A_Get = new System.Windows.Forms.Button();
            this.tbox_Mcr_A = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btn_Tilt_ULd_Close = new System.Windows.Forms.Button();
            this.btn_Tilt_ULd_Open = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.tbox_ULd_Check4 = new System.Windows.Forms.TextBox();
            this.tbox_ULd_Check3 = new System.Windows.Forms.TextBox();
            this.tbox_ULd_Check2 = new System.Windows.Forms.TextBox();
            this.tbox_ULd_Check1 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tbox_ULd_Position4 = new System.Windows.Forms.TextBox();
            this.tbox_ULd_Position3 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tbox_ULd_Position2 = new System.Windows.Forms.TextBox();
            this.tbox_ULd_Position1 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.btn_Tilt_Ld_Close = new System.Windows.Forms.Button();
            this.btn_Tilt_Ld_Open = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.tbox_Ld_Check4 = new System.Windows.Forms.TextBox();
            this.tbox_Ld_Check3 = new System.Windows.Forms.TextBox();
            this.tbox_Ld_Check2 = new System.Windows.Forms.TextBox();
            this.tbox_Ld_Check1 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tbox_Ld_Position4 = new System.Windows.Forms.TextBox();
            this.tbox_Ld_Position3 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tbox_Ld_Position2 = new System.Windows.Forms.TextBox();
            this.tbox_Ld_Position1 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_Controller_Save = new System.Windows.Forms.Button();
            this.btn_Controller_Close = new System.Windows.Forms.Button();
            this.btn_Controller_Open = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btn_2ch_Set = new System.Windows.Forms.Button();
            this.btn_1ch_Set = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbox_2ch_pr_bright_set = new System.Windows.Forms.TextBox();
            this.tbox_1ch_pr_bright_set = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbox_2ch_bright_value = new System.Windows.Forms.TextBox();
            this.tbox_1ch_bright_value = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.panel17 = new System.Windows.Forms.Panel();
            this.btn_ULd_HandyBacord_Close = new System.Windows.Forms.Button();
            this.btn_ULd_HandyBacord_Open = new System.Windows.Forms.Button();
            this.panel18 = new System.Windows.Forms.Panel();
            this.btn_ULd_Handy_Get = new System.Windows.Forms.Button();
            this.tbox_ULd_HandyBarcode = new System.Windows.Forms.TextBox();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.panel19 = new System.Windows.Forms.Panel();
            this.btn_ULd_Barcode_B_Close = new System.Windows.Forms.Button();
            this.btn_ULd_Barcode_B_Open = new System.Windows.Forms.Button();
            this.panel20 = new System.Windows.Forms.Panel();
            this.button30 = new System.Windows.Forms.Button();
            this.tbox_ULd_Barcode_B = new System.Windows.Forms.TextBox();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.panel21 = new System.Windows.Forms.Panel();
            this.btn_Ld_Barcode_B_Close = new System.Windows.Forms.Button();
            this.btn_Ld_Barcode_B_Open = new System.Windows.Forms.Button();
            this.panel22 = new System.Windows.Forms.Panel();
            this.button33 = new System.Windows.Forms.Button();
            this.tbox_Ld_Barcode_B = new System.Windows.Forms.TextBox();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.panel15 = new System.Windows.Forms.Panel();
            this.btn_Ld_HandyBacord_Close = new System.Windows.Forms.Button();
            this.btn_Ld_HandyBacord_Open = new System.Windows.Forms.Button();
            this.panel16 = new System.Windows.Forms.Panel();
            this.btn_Ld_Handy_Get = new System.Windows.Forms.Button();
            this.tbox_Ld_HandyBarcode = new System.Windows.Forms.TextBox();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.panel13 = new System.Windows.Forms.Panel();
            this.btn_ULd_Barcode_A_Close = new System.Windows.Forms.Button();
            this.btn_ULd_Barcode_A_Open = new System.Windows.Forms.Button();
            this.panel14 = new System.Windows.Forms.Panel();
            this.button21 = new System.Windows.Forms.Button();
            this.tbox_ULd_Barcode_A = new System.Windows.Forms.TextBox();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.btn_Ld_Barcode_A_Close = new System.Windows.Forms.Button();
            this.btn_Ld_Barcode_A_Open = new System.Windows.Forms.Button();
            this.panel12 = new System.Windows.Forms.Panel();
            this.button18 = new System.Windows.Forms.Button();
            this.tbox_Ld_Barcode_A = new System.Windows.Forms.TextBox();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.panel23 = new System.Windows.Forms.Panel();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.tbox_LDS_Break_TableIdx = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.tbox_LDS_Break_Measure = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.button34 = new System.Windows.Forms.Button();
            this.tbox_LDS_Break_Standby = new System.Windows.Forms.TextBox();
            this.btn_LDS_Break_Stop = new System.Windows.Forms.Button();
            this.btn_LDS_Break_Start = new System.Windows.Forms.Button();
            this.panel24 = new System.Windows.Forms.Panel();
            this.listView2 = new System.Windows.Forms.ListView();
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label50 = new System.Windows.Forms.Label();
            this.textBox78 = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.textBox79 = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.textBox80 = new System.Windows.Forms.TextBox();
            this.textBox81 = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.textBox82 = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.textBox83 = new System.Windows.Forms.TextBox();
            this.textBox84 = new System.Windows.Forms.TextBox();
            this.textBox85 = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.textBox86 = new System.Windows.Forms.TextBox();
            this.textBox87 = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.textBox88 = new System.Windows.Forms.TextBox();
            this.textBox89 = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.textBox90 = new System.Windows.Forms.TextBox();
            this.panel25 = new System.Windows.Forms.Panel();
            this.btn_Power_Close = new System.Windows.Forms.Button();
            this.btn_Power_Open = new System.Windows.Forms.Button();
            this.panel26 = new System.Windows.Forms.Panel();
            this.btn_Power_Get = new System.Windows.Forms.Button();
            this.tbox_Power_Get = new System.Windows.Forms.TextBox();
            this.textBox42 = new System.Windows.Forms.TextBox();
            this.panel27 = new System.Windows.Forms.Panel();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.tbox_LDS_Pro_TableIdx = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.tbox_LDS_Pro_Measure = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.button44 = new System.Windows.Forms.Button();
            this.tbox_LDS_pro_Standby = new System.Windows.Forms.TextBox();
            this.btn_LDS_Process_Stop = new System.Windows.Forms.Button();
            this.btn_LDS_Process_Start = new System.Windows.Forms.Button();
            this.panel29 = new System.Windows.Forms.Panel();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label41 = new System.Windows.Forms.Label();
            this.textBox50 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBox47 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.textBox48 = new System.Windows.Forms.TextBox();
            this.textBox49 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.textBox44 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox45 = new System.Windows.Forms.TextBox();
            this.textBox46 = new System.Windows.Forms.TextBox();
            this.tbox_LDS_CurrendValue = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.tbox_ = new System.Windows.Forms.TextBox();
            this.textBox56 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.textBox57 = new System.Windows.Forms.TextBox();
            this.textBox58 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.textBox59 = new System.Windows.Forms.TextBox();
            this.panel30 = new System.Windows.Forms.Panel();
            this.btn_Beam_Close = new System.Windows.Forms.Button();
            this.btn_Beam_Open = new System.Windows.Forms.Button();
            this.panel31 = new System.Windows.Forms.Panel();
            this.label39 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.tbox_Error_Pos = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.tbox_Teaching_Y = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.tbox_Error_Power = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.tbox_Teaching_X = new System.Windows.Forms.TextBox();
            this.tbox_Teaching_Power = new System.Windows.Forms.TextBox();
            this.btn_Beam_Save = new System.Windows.Forms.Button();
            this.panel32 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tbox_Beam_PosY_MedianValue = new System.Windows.Forms.TextBox();
            this.tbox_Beam_PosY_MaxValue = new System.Windows.Forms.TextBox();
            this.tbox_Beam_PosX_MedianValue = new System.Windows.Forms.TextBox();
            this.tbox_Beam_PosX_MaxValue = new System.Windows.Forms.TextBox();
            this.tbox_Beam_Power_MedianValue = new System.Windows.Forms.TextBox();
            this.tbox_Beam_Power_MaxValue = new System.Windows.Forms.TextBox();
            this.tbox_Beam_PosY_CurrenValue = new System.Windows.Forms.TextBox();
            this.tbox_Beam_PosY_MiniValue = new System.Windows.Forms.TextBox();
            this.tbox_Beam_Power_CurrenValue = new System.Windows.Forms.TextBox();
            this.tbox_Beam_Power_MiniValue = new System.Windows.Forms.TextBox();
            this.tbox_Beam_PosX_CurrenValue = new System.Windows.Forms.TextBox();
            this.tbox_Beam_PosX_MiniValue = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.textBox64 = new System.Windows.Forms.TextBox();
            this.tc_iostatus_info.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.panel28.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel29.SuspendLayout();
            this.panel30.SuspendLayout();
            this.panel31.SuspendLayout();
            this.panel32.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tc_iostatus_info
            // 
            this.tc_iostatus_info.Controls.Add(this.tabPage1);
            this.tc_iostatus_info.Controls.Add(this.tabPage2);
            this.tc_iostatus_info.Controls.Add(this.tabPage3);
            this.tc_iostatus_info.Controls.Add(this.tabPage4);
            this.tc_iostatus_info.ItemSize = new System.Drawing.Size(466, 40);
            this.tc_iostatus_info.Location = new System.Drawing.Point(8, 8);
            this.tc_iostatus_info.Name = "tc_iostatus_info";
            this.tc_iostatus_info.SelectedIndex = 0;
            this.tc_iostatus_info.Size = new System.Drawing.Size(1869, 875);
            this.tc_iostatus_info.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tc_iostatus_info.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.DimGray;
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.panel4);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Font = new System.Drawing.Font("Gulim", 20F);
            this.tabPage1.Location = new System.Drawing.Point(4, 44);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(1861, 827);
            this.tabPage1.TabIndex = 3;
            this.tabPage1.Text = "조면 컨트롤 / MCR / BeamPosition";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.DimGray;
            this.tabPage2.Controls.Add(this.panel17);
            this.tabPage2.Controls.Add(this.panel19);
            this.tabPage2.Controls.Add(this.panel21);
            this.tabPage2.Controls.Add(this.panel15);
            this.tabPage2.Controls.Add(this.panel13);
            this.tabPage2.Controls.Add(this.panel11);
            this.tabPage2.Font = new System.Drawing.Font("Gulim", 20F);
            this.tabPage2.Location = new System.Drawing.Point(4, 44);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(1861, 827);
            this.tabPage2.TabIndex = 4;
            this.tabPage2.Text = "바코드 리더";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.DimGray;
            this.tabPage3.Controls.Add(this.panel23);
            this.tabPage3.Controls.Add(this.panel25);
            this.tabPage3.Controls.Add(this.panel27);
            this.tabPage3.Controls.Add(this.panel30);
            this.tabPage3.Font = new System.Drawing.Font("Gulim", 20F);
            this.tabPage3.Location = new System.Drawing.Point(4, 44);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1861, 827);
            this.tabPage3.TabIndex = 5;
            this.tabPage3.Text = "틸트 센서 / 파워미터 / LDS";
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.DimGray;
            this.tabPage4.Controls.Add(this.panel28);
            this.tabPage4.Location = new System.Drawing.Point(4, 44);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1861, 827);
            this.tabPage4.TabIndex = 6;
            this.tabPage4.Text = "컷라인 계측";
            // 
            // panel28
            // 
            this.panel28.Controls.Add(this.tableLayoutPanel1);
            this.panel28.Controls.Add(this.textBox92);
            this.panel28.Location = new System.Drawing.Point(6, 6);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(915, 814);
            this.panel28.TabIndex = 6;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Controls.Add(this.button49, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.button43, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.button42, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.button48, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(13, 48);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(899, 126);
            this.tableLayoutPanel1.TabIndex = 30;
            // 
            // button49
            // 
            this.button49.BackColor = System.Drawing.Color.DimGray;
            this.button49.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.button49.ForeColor = System.Drawing.Color.White;
            this.button49.Location = new System.Drawing.Point(675, 3);
            this.button49.Name = "button49";
            this.button49.Size = new System.Drawing.Size(221, 120);
            this.button49.TabIndex = 32;
            this.button49.Text = "B열 R2테이블";
            this.button49.UseVisualStyleBackColor = false;
            // 
            // button43
            // 
            this.button43.BackColor = System.Drawing.Color.DimGray;
            this.button43.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.button43.ForeColor = System.Drawing.Color.White;
            this.button43.Location = new System.Drawing.Point(451, 3);
            this.button43.Name = "button43";
            this.button43.Size = new System.Drawing.Size(218, 120);
            this.button43.TabIndex = 31;
            this.button43.Text = "B열 R1테이블";
            this.button43.UseVisualStyleBackColor = false;
            // 
            // button42
            // 
            this.button42.BackColor = System.Drawing.Color.DimGray;
            this.button42.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.button42.ForeColor = System.Drawing.Color.White;
            this.button42.Location = new System.Drawing.Point(227, 3);
            this.button42.Name = "button42";
            this.button42.Size = new System.Drawing.Size(218, 120);
            this.button42.TabIndex = 30;
            this.button42.Text = "A열 L2테이블";
            this.button42.UseVisualStyleBackColor = false;
            // 
            // button48
            // 
            this.button48.BackColor = System.Drawing.Color.DimGray;
            this.button48.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.button48.ForeColor = System.Drawing.Color.White;
            this.button48.Location = new System.Drawing.Point(3, 3);
            this.button48.Name = "button48";
            this.button48.Size = new System.Drawing.Size(218, 120);
            this.button48.TabIndex = 29;
            this.button48.Text = "A열 L1테이블";
            this.button48.UseVisualStyleBackColor = false;
            // 
            // textBox92
            // 
            this.textBox92.BackColor = System.Drawing.SystemColors.HotTrack;
            this.textBox92.Enabled = false;
            this.textBox92.Font = new System.Drawing.Font("Malgun Gothic", 18F, System.Drawing.FontStyle.Bold);
            this.textBox92.ForeColor = System.Drawing.Color.White;
            this.textBox92.Location = new System.Drawing.Point(3, 3);
            this.textBox92.Name = "textBox92";
            this.textBox92.Size = new System.Drawing.Size(909, 39);
            this.textBox92.TabIndex = 3;
            this.textBox92.Text = "컷라인 계측";
            this.textBox92.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btn_Mcr_B_Close);
            this.panel3.Controls.Add(this.btn_Mcr_B_Open);
            this.panel3.Controls.Add(this.panel10);
            this.panel3.Controls.Add(this.textBox4);
            this.panel3.Location = new System.Drawing.Point(940, 412);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(915, 400);
            this.panel3.TabIndex = 7;
            // 
            // btn_Mcr_B_Close
            // 
            this.btn_Mcr_B_Close.BackColor = System.Drawing.Color.DimGray;
            this.btn_Mcr_B_Close.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Mcr_B_Close.ForeColor = System.Drawing.Color.White;
            this.btn_Mcr_B_Close.Location = new System.Drawing.Point(106, 58);
            this.btn_Mcr_B_Close.Name = "btn_Mcr_B_Close";
            this.btn_Mcr_B_Close.Size = new System.Drawing.Size(93, 40);
            this.btn_Mcr_B_Close.TabIndex = 13;
            this.btn_Mcr_B_Close.Text = "Close";
            this.btn_Mcr_B_Close.UseVisualStyleBackColor = false;
            // 
            // btn_Mcr_B_Open
            // 
            this.btn_Mcr_B_Open.BackColor = System.Drawing.Color.DimGray;
            this.btn_Mcr_B_Open.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Mcr_B_Open.ForeColor = System.Drawing.Color.White;
            this.btn_Mcr_B_Open.Location = new System.Drawing.Point(9, 58);
            this.btn_Mcr_B_Open.Name = "btn_Mcr_B_Open";
            this.btn_Mcr_B_Open.Size = new System.Drawing.Size(93, 40);
            this.btn_Mcr_B_Open.TabIndex = 12;
            this.btn_Mcr_B_Open.Text = "Open";
            this.btn_Mcr_B_Open.UseVisualStyleBackColor = false;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.btn_Mcr_B_Get);
            this.panel10.Controls.Add(this.tbox_Mcr_B);
            this.panel10.Location = new System.Drawing.Point(9, 114);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(903, 283);
            this.panel10.TabIndex = 11;
            // 
            // btn_Mcr_B_Get
            // 
            this.btn_Mcr_B_Get.BackColor = System.Drawing.Color.DimGray;
            this.btn_Mcr_B_Get.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Mcr_B_Get.ForeColor = System.Drawing.Color.White;
            this.btn_Mcr_B_Get.Location = new System.Drawing.Point(42, 106);
            this.btn_Mcr_B_Get.Name = "btn_Mcr_B_Get";
            this.btn_Mcr_B_Get.Size = new System.Drawing.Size(166, 81);
            this.btn_Mcr_B_Get.TabIndex = 29;
            this.btn_Mcr_B_Get.Text = "Get";
            this.btn_Mcr_B_Get.UseVisualStyleBackColor = false;
            // 
            // tbox_Mcr_B
            // 
            this.tbox_Mcr_B.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_Mcr_B.Font = new System.Drawing.Font("Malgun Gothic", 40F, System.Drawing.FontStyle.Bold);
            this.tbox_Mcr_B.Location = new System.Drawing.Point(314, 105);
            this.tbox_Mcr_B.Name = "tbox_Mcr_B";
            this.tbox_Mcr_B.Size = new System.Drawing.Size(451, 78);
            this.tbox_Mcr_B.TabIndex = 25;
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.SystemColors.HotTrack;
            this.textBox4.Enabled = false;
            this.textBox4.Font = new System.Drawing.Font("Malgun Gothic", 18F, System.Drawing.FontStyle.Bold);
            this.textBox4.ForeColor = System.Drawing.Color.White;
            this.textBox4.Location = new System.Drawing.Point(3, 3);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(909, 39);
            this.textBox4.TabIndex = 3;
            this.textBox4.Text = "MCR B";
            this.textBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btn_Mcr_A_Close);
            this.panel2.Controls.Add(this.btn_Mcr_A_Open);
            this.panel2.Controls.Add(this.panel9);
            this.panel2.Controls.Add(this.textBox1);
            this.panel2.Location = new System.Drawing.Point(940, 6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(915, 400);
            this.panel2.TabIndex = 5;
            // 
            // btn_Mcr_A_Close
            // 
            this.btn_Mcr_A_Close.BackColor = System.Drawing.Color.DimGray;
            this.btn_Mcr_A_Close.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Mcr_A_Close.ForeColor = System.Drawing.Color.White;
            this.btn_Mcr_A_Close.Location = new System.Drawing.Point(106, 48);
            this.btn_Mcr_A_Close.Name = "btn_Mcr_A_Close";
            this.btn_Mcr_A_Close.Size = new System.Drawing.Size(93, 40);
            this.btn_Mcr_A_Close.TabIndex = 10;
            this.btn_Mcr_A_Close.Text = "Close";
            this.btn_Mcr_A_Close.UseVisualStyleBackColor = false;
            // 
            // btn_Mcr_A_Open
            // 
            this.btn_Mcr_A_Open.BackColor = System.Drawing.Color.DimGray;
            this.btn_Mcr_A_Open.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Mcr_A_Open.ForeColor = System.Drawing.Color.White;
            this.btn_Mcr_A_Open.Location = new System.Drawing.Point(9, 48);
            this.btn_Mcr_A_Open.Name = "btn_Mcr_A_Open";
            this.btn_Mcr_A_Open.Size = new System.Drawing.Size(93, 40);
            this.btn_Mcr_A_Open.TabIndex = 9;
            this.btn_Mcr_A_Open.Text = "Open";
            this.btn_Mcr_A_Open.UseVisualStyleBackColor = false;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.btn_Mcr_A_Get);
            this.panel9.Controls.Add(this.tbox_Mcr_A);
            this.panel9.Location = new System.Drawing.Point(9, 104);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(903, 283);
            this.panel9.TabIndex = 5;
            // 
            // btn_Mcr_A_Get
            // 
            this.btn_Mcr_A_Get.BackColor = System.Drawing.Color.DimGray;
            this.btn_Mcr_A_Get.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Mcr_A_Get.ForeColor = System.Drawing.Color.White;
            this.btn_Mcr_A_Get.Location = new System.Drawing.Point(42, 106);
            this.btn_Mcr_A_Get.Name = "btn_Mcr_A_Get";
            this.btn_Mcr_A_Get.Size = new System.Drawing.Size(166, 81);
            this.btn_Mcr_A_Get.TabIndex = 29;
            this.btn_Mcr_A_Get.Text = "Get";
            this.btn_Mcr_A_Get.UseVisualStyleBackColor = false;
            // 
            // tbox_Mcr_A
            // 
            this.tbox_Mcr_A.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_Mcr_A.Font = new System.Drawing.Font("Malgun Gothic", 40F, System.Drawing.FontStyle.Bold);
            this.tbox_Mcr_A.Location = new System.Drawing.Point(314, 106);
            this.tbox_Mcr_A.Name = "tbox_Mcr_A";
            this.tbox_Mcr_A.Size = new System.Drawing.Size(451, 78);
            this.tbox_Mcr_A.TabIndex = 25;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.HotTrack;
            this.textBox1.Enabled = false;
            this.textBox1.Font = new System.Drawing.Font("Malgun Gothic", 18F, System.Drawing.FontStyle.Bold);
            this.textBox1.ForeColor = System.Drawing.Color.White;
            this.textBox1.Location = new System.Drawing.Point(3, 3);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(909, 39);
            this.textBox1.TabIndex = 3;
            this.textBox1.Text = "MCR A";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btn_Tilt_ULd_Close);
            this.panel4.Controls.Add(this.btn_Tilt_ULd_Open);
            this.panel4.Controls.Add(this.panel8);
            this.panel4.Controls.Add(this.btn_Tilt_Ld_Close);
            this.panel4.Controls.Add(this.btn_Tilt_Ld_Open);
            this.panel4.Controls.Add(this.panel7);
            this.panel4.Controls.Add(this.textBox3);
            this.panel4.Location = new System.Drawing.Point(4, 412);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(915, 400);
            this.panel4.TabIndex = 6;
            // 
            // btn_Tilt_ULd_Close
            // 
            this.btn_Tilt_ULd_Close.BackColor = System.Drawing.Color.DimGray;
            this.btn_Tilt_ULd_Close.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Tilt_ULd_Close.ForeColor = System.Drawing.Color.White;
            this.btn_Tilt_ULd_Close.Location = new System.Drawing.Point(581, 58);
            this.btn_Tilt_ULd_Close.Name = "btn_Tilt_ULd_Close";
            this.btn_Tilt_ULd_Close.Size = new System.Drawing.Size(93, 40);
            this.btn_Tilt_ULd_Close.TabIndex = 14;
            this.btn_Tilt_ULd_Close.Text = "Close";
            this.btn_Tilt_ULd_Close.UseVisualStyleBackColor = false;
            // 
            // btn_Tilt_ULd_Open
            // 
            this.btn_Tilt_ULd_Open.BackColor = System.Drawing.Color.DimGray;
            this.btn_Tilt_ULd_Open.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Tilt_ULd_Open.ForeColor = System.Drawing.Color.White;
            this.btn_Tilt_ULd_Open.Location = new System.Drawing.Point(484, 58);
            this.btn_Tilt_ULd_Open.Name = "btn_Tilt_ULd_Open";
            this.btn_Tilt_ULd_Open.Size = new System.Drawing.Size(93, 40);
            this.btn_Tilt_ULd_Open.TabIndex = 13;
            this.btn_Tilt_ULd_Open.Text = "Open";
            this.btn_Tilt_ULd_Open.UseVisualStyleBackColor = false;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.tbox_ULd_Check4);
            this.panel8.Controls.Add(this.tbox_ULd_Check3);
            this.panel8.Controls.Add(this.tbox_ULd_Check2);
            this.panel8.Controls.Add(this.tbox_ULd_Check1);
            this.panel8.Controls.Add(this.label10);
            this.panel8.Controls.Add(this.label11);
            this.panel8.Controls.Add(this.tbox_ULd_Position4);
            this.panel8.Controls.Add(this.tbox_ULd_Position3);
            this.panel8.Controls.Add(this.label15);
            this.panel8.Controls.Add(this.label16);
            this.panel8.Controls.Add(this.tbox_ULd_Position2);
            this.panel8.Controls.Add(this.tbox_ULd_Position1);
            this.panel8.Controls.Add(this.label12);
            this.panel8.Location = new System.Drawing.Point(487, 114);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(409, 254);
            this.panel8.TabIndex = 12;
            // 
            // tbox_ULd_Check4
            // 
            this.tbox_ULd_Check4.BackColor = System.Drawing.Color.Red;
            this.tbox_ULd_Check4.Font = new System.Drawing.Font("Malgun Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.tbox_ULd_Check4.ForeColor = System.Drawing.Color.White;
            this.tbox_ULd_Check4.Location = new System.Drawing.Point(317, 201);
            this.tbox_ULd_Check4.Name = "tbox_ULd_Check4";
            this.tbox_ULd_Check4.Size = new System.Drawing.Size(62, 34);
            this.tbox_ULd_Check4.TabIndex = 42;
            this.tbox_ULd_Check4.Text = "NG";
            this.tbox_ULd_Check4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbox_ULd_Check3
            // 
            this.tbox_ULd_Check3.BackColor = System.Drawing.Color.Red;
            this.tbox_ULd_Check3.Font = new System.Drawing.Font("Malgun Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.tbox_ULd_Check3.ForeColor = System.Drawing.Color.White;
            this.tbox_ULd_Check3.Location = new System.Drawing.Point(317, 153);
            this.tbox_ULd_Check3.Name = "tbox_ULd_Check3";
            this.tbox_ULd_Check3.Size = new System.Drawing.Size(62, 34);
            this.tbox_ULd_Check3.TabIndex = 41;
            this.tbox_ULd_Check3.Text = "NG";
            this.tbox_ULd_Check3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbox_ULd_Check2
            // 
            this.tbox_ULd_Check2.BackColor = System.Drawing.Color.Red;
            this.tbox_ULd_Check2.Font = new System.Drawing.Font("Malgun Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.tbox_ULd_Check2.ForeColor = System.Drawing.Color.White;
            this.tbox_ULd_Check2.Location = new System.Drawing.Point(317, 107);
            this.tbox_ULd_Check2.Name = "tbox_ULd_Check2";
            this.tbox_ULd_Check2.Size = new System.Drawing.Size(62, 34);
            this.tbox_ULd_Check2.TabIndex = 40;
            this.tbox_ULd_Check2.Text = "NG";
            this.tbox_ULd_Check2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbox_ULd_Check1
            // 
            this.tbox_ULd_Check1.BackColor = System.Drawing.Color.Red;
            this.tbox_ULd_Check1.Font = new System.Drawing.Font("Malgun Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.tbox_ULd_Check1.ForeColor = System.Drawing.Color.White;
            this.tbox_ULd_Check1.Location = new System.Drawing.Point(317, 59);
            this.tbox_ULd_Check1.Name = "tbox_ULd_Check1";
            this.tbox_ULd_Check1.Size = new System.Drawing.Size(62, 34);
            this.tbox_ULd_Check1.TabIndex = 39;
            this.tbox_ULd_Check1.Text = "NG";
            this.tbox_ULd_Check1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(19, 210);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(118, 20);
            this.label10.TabIndex = 38;
            this.label10.Text = "Position 4 [mm]";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(19, 163);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(118, 20);
            this.label11.TabIndex = 37;
            this.label11.Text = "Position 3 [mm]";
            // 
            // tbox_ULd_Position4
            // 
            this.tbox_ULd_Position4.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_ULd_Position4.Font = new System.Drawing.Font("Malgun Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.tbox_ULd_Position4.Location = new System.Drawing.Point(167, 201);
            this.tbox_ULd_Position4.Name = "tbox_ULd_Position4";
            this.tbox_ULd_Position4.Size = new System.Drawing.Size(131, 34);
            this.tbox_ULd_Position4.TabIndex = 36;
            // 
            // tbox_ULd_Position3
            // 
            this.tbox_ULd_Position3.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_ULd_Position3.Font = new System.Drawing.Font("Malgun Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.tbox_ULd_Position3.Location = new System.Drawing.Point(167, 153);
            this.tbox_ULd_Position3.Name = "tbox_ULd_Position3";
            this.tbox_ULd_Position3.Size = new System.Drawing.Size(131, 34);
            this.tbox_ULd_Position3.TabIndex = 35;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(19, 116);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(118, 20);
            this.label15.TabIndex = 34;
            this.label15.Text = "Position 2 [mm]";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(19, 69);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(118, 20);
            this.label16.TabIndex = 33;
            this.label16.Text = "Position 1 [mm]";
            // 
            // tbox_ULd_Position2
            // 
            this.tbox_ULd_Position2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_ULd_Position2.Font = new System.Drawing.Font("Malgun Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.tbox_ULd_Position2.Location = new System.Drawing.Point(167, 107);
            this.tbox_ULd_Position2.Name = "tbox_ULd_Position2";
            this.tbox_ULd_Position2.Size = new System.Drawing.Size(131, 34);
            this.tbox_ULd_Position2.TabIndex = 32;
            // 
            // tbox_ULd_Position1
            // 
            this.tbox_ULd_Position1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_ULd_Position1.Font = new System.Drawing.Font("Malgun Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.tbox_ULd_Position1.Location = new System.Drawing.Point(167, 59);
            this.tbox_ULd_Position1.Name = "tbox_ULd_Position1";
            this.tbox_ULd_Position1.Size = new System.Drawing.Size(131, 34);
            this.tbox_ULd_Position1.TabIndex = 31;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Malgun Gothic", 13F, System.Drawing.FontStyle.Bold);
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(17, 18);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(165, 25);
            this.label12.TabIndex = 18;
            this.label12.Text = "Module UnLoader";
            // 
            // btn_Tilt_Ld_Close
            // 
            this.btn_Tilt_Ld_Close.BackColor = System.Drawing.Color.DimGray;
            this.btn_Tilt_Ld_Close.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Tilt_Ld_Close.ForeColor = System.Drawing.Color.White;
            this.btn_Tilt_Ld_Close.Location = new System.Drawing.Point(116, 58);
            this.btn_Tilt_Ld_Close.Name = "btn_Tilt_Ld_Close";
            this.btn_Tilt_Ld_Close.Size = new System.Drawing.Size(93, 40);
            this.btn_Tilt_Ld_Close.TabIndex = 11;
            this.btn_Tilt_Ld_Close.Text = "Close";
            this.btn_Tilt_Ld_Close.UseVisualStyleBackColor = false;
            // 
            // btn_Tilt_Ld_Open
            // 
            this.btn_Tilt_Ld_Open.BackColor = System.Drawing.Color.DimGray;
            this.btn_Tilt_Ld_Open.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Tilt_Ld_Open.ForeColor = System.Drawing.Color.White;
            this.btn_Tilt_Ld_Open.Location = new System.Drawing.Point(19, 58);
            this.btn_Tilt_Ld_Open.Name = "btn_Tilt_Ld_Open";
            this.btn_Tilt_Ld_Open.Size = new System.Drawing.Size(93, 40);
            this.btn_Tilt_Ld_Open.TabIndex = 10;
            this.btn_Tilt_Ld_Open.Text = "Open";
            this.btn_Tilt_Ld_Open.UseVisualStyleBackColor = false;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.tbox_Ld_Check4);
            this.panel7.Controls.Add(this.tbox_Ld_Check3);
            this.panel7.Controls.Add(this.tbox_Ld_Check2);
            this.panel7.Controls.Add(this.tbox_Ld_Check1);
            this.panel7.Controls.Add(this.label13);
            this.panel7.Controls.Add(this.label14);
            this.panel7.Controls.Add(this.tbox_Ld_Position4);
            this.panel7.Controls.Add(this.tbox_Ld_Position3);
            this.panel7.Controls.Add(this.label6);
            this.panel7.Controls.Add(this.label7);
            this.panel7.Controls.Add(this.tbox_Ld_Position2);
            this.panel7.Controls.Add(this.tbox_Ld_Position1);
            this.panel7.Controls.Add(this.label9);
            this.panel7.Location = new System.Drawing.Point(22, 114);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(409, 254);
            this.panel7.TabIndex = 9;
            // 
            // tbox_Ld_Check4
            // 
            this.tbox_Ld_Check4.BackColor = System.Drawing.Color.Red;
            this.tbox_Ld_Check4.Font = new System.Drawing.Font("Malgun Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.tbox_Ld_Check4.ForeColor = System.Drawing.Color.White;
            this.tbox_Ld_Check4.Location = new System.Drawing.Point(316, 201);
            this.tbox_Ld_Check4.Name = "tbox_Ld_Check4";
            this.tbox_Ld_Check4.Size = new System.Drawing.Size(62, 34);
            this.tbox_Ld_Check4.TabIndex = 30;
            this.tbox_Ld_Check4.Text = "NG";
            this.tbox_Ld_Check4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbox_Ld_Check3
            // 
            this.tbox_Ld_Check3.BackColor = System.Drawing.Color.Red;
            this.tbox_Ld_Check3.Font = new System.Drawing.Font("Malgun Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.tbox_Ld_Check3.ForeColor = System.Drawing.Color.White;
            this.tbox_Ld_Check3.Location = new System.Drawing.Point(316, 153);
            this.tbox_Ld_Check3.Name = "tbox_Ld_Check3";
            this.tbox_Ld_Check3.Size = new System.Drawing.Size(62, 34);
            this.tbox_Ld_Check3.TabIndex = 29;
            this.tbox_Ld_Check3.Text = "NG";
            this.tbox_Ld_Check3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbox_Ld_Check2
            // 
            this.tbox_Ld_Check2.BackColor = System.Drawing.Color.Red;
            this.tbox_Ld_Check2.Font = new System.Drawing.Font("Malgun Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.tbox_Ld_Check2.ForeColor = System.Drawing.Color.White;
            this.tbox_Ld_Check2.Location = new System.Drawing.Point(316, 107);
            this.tbox_Ld_Check2.Name = "tbox_Ld_Check2";
            this.tbox_Ld_Check2.Size = new System.Drawing.Size(62, 34);
            this.tbox_Ld_Check2.TabIndex = 28;
            this.tbox_Ld_Check2.Text = "NG";
            this.tbox_Ld_Check2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbox_Ld_Check1
            // 
            this.tbox_Ld_Check1.BackColor = System.Drawing.Color.Red;
            this.tbox_Ld_Check1.Font = new System.Drawing.Font("Malgun Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.tbox_Ld_Check1.ForeColor = System.Drawing.Color.White;
            this.tbox_Ld_Check1.Location = new System.Drawing.Point(316, 59);
            this.tbox_Ld_Check1.Name = "tbox_Ld_Check1";
            this.tbox_Ld_Check1.Size = new System.Drawing.Size(62, 34);
            this.tbox_Ld_Check1.TabIndex = 27;
            this.tbox_Ld_Check1.Text = "NG";
            this.tbox_Ld_Check1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(18, 210);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(118, 20);
            this.label13.TabIndex = 26;
            this.label13.Text = "Position 4 [mm]";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(18, 163);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(118, 20);
            this.label14.TabIndex = 25;
            this.label14.Text = "Position 3 [mm]";
            // 
            // tbox_Ld_Position4
            // 
            this.tbox_Ld_Position4.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_Ld_Position4.Font = new System.Drawing.Font("Malgun Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.tbox_Ld_Position4.Location = new System.Drawing.Point(166, 201);
            this.tbox_Ld_Position4.Name = "tbox_Ld_Position4";
            this.tbox_Ld_Position4.Size = new System.Drawing.Size(131, 34);
            this.tbox_Ld_Position4.TabIndex = 24;
            // 
            // tbox_Ld_Position3
            // 
            this.tbox_Ld_Position3.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_Ld_Position3.Font = new System.Drawing.Font("Malgun Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.tbox_Ld_Position3.Location = new System.Drawing.Point(166, 153);
            this.tbox_Ld_Position3.Name = "tbox_Ld_Position3";
            this.tbox_Ld_Position3.Size = new System.Drawing.Size(131, 34);
            this.tbox_Ld_Position3.TabIndex = 23;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(18, 116);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(118, 20);
            this.label6.TabIndex = 22;
            this.label6.Text = "Position 2 [mm]";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(18, 69);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(118, 20);
            this.label7.TabIndex = 21;
            this.label7.Text = "Position 1 [mm]";
            // 
            // tbox_Ld_Position2
            // 
            this.tbox_Ld_Position2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_Ld_Position2.Font = new System.Drawing.Font("Malgun Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.tbox_Ld_Position2.Location = new System.Drawing.Point(166, 107);
            this.tbox_Ld_Position2.Name = "tbox_Ld_Position2";
            this.tbox_Ld_Position2.Size = new System.Drawing.Size(131, 34);
            this.tbox_Ld_Position2.TabIndex = 20;
            // 
            // tbox_Ld_Position1
            // 
            this.tbox_Ld_Position1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_Ld_Position1.Font = new System.Drawing.Font("Malgun Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.tbox_Ld_Position1.Location = new System.Drawing.Point(166, 59);
            this.tbox_Ld_Position1.Name = "tbox_Ld_Position1";
            this.tbox_Ld_Position1.Size = new System.Drawing.Size(131, 34);
            this.tbox_Ld_Position1.TabIndex = 19;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Malgun Gothic", 13F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(17, 18);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(141, 25);
            this.label9.TabIndex = 18;
            this.label9.Text = "Module Loader";
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.SystemColors.HotTrack;
            this.textBox3.Enabled = false;
            this.textBox3.Font = new System.Drawing.Font("Malgun Gothic", 18F, System.Drawing.FontStyle.Bold);
            this.textBox3.ForeColor = System.Drawing.Color.White;
            this.textBox3.Location = new System.Drawing.Point(3, 3);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(909, 39);
            this.textBox3.TabIndex = 3;
            this.textBox3.Text = "Tilt Sensor";
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_Controller_Save);
            this.panel1.Controls.Add(this.btn_Controller_Close);
            this.panel1.Controls.Add(this.btn_Controller_Open);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.textBox2);
            this.panel1.Location = new System.Drawing.Point(4, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(915, 400);
            this.panel1.TabIndex = 4;
            // 
            // btn_Controller_Save
            // 
            this.btn_Controller_Save.BackColor = System.Drawing.Color.DimGray;
            this.btn_Controller_Save.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Controller_Save.ForeColor = System.Drawing.Color.White;
            this.btn_Controller_Save.Location = new System.Drawing.Point(760, 344);
            this.btn_Controller_Save.Name = "btn_Controller_Save";
            this.btn_Controller_Save.Size = new System.Drawing.Size(132, 43);
            this.btn_Controller_Save.TabIndex = 30;
            this.btn_Controller_Save.Text = "저장";
            this.btn_Controller_Save.UseVisualStyleBackColor = false;
            // 
            // btn_Controller_Close
            // 
            this.btn_Controller_Close.BackColor = System.Drawing.Color.DimGray;
            this.btn_Controller_Close.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Controller_Close.ForeColor = System.Drawing.Color.White;
            this.btn_Controller_Close.Location = new System.Drawing.Point(102, 48);
            this.btn_Controller_Close.Name = "btn_Controller_Close";
            this.btn_Controller_Close.Size = new System.Drawing.Size(93, 40);
            this.btn_Controller_Close.TabIndex = 8;
            this.btn_Controller_Close.Text = "Close";
            this.btn_Controller_Close.UseVisualStyleBackColor = false;
            // 
            // btn_Controller_Open
            // 
            this.btn_Controller_Open.BackColor = System.Drawing.Color.DimGray;
            this.btn_Controller_Open.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Controller_Open.ForeColor = System.Drawing.Color.White;
            this.btn_Controller_Open.Location = new System.Drawing.Point(5, 48);
            this.btn_Controller_Open.Name = "btn_Controller_Open";
            this.btn_Controller_Open.Size = new System.Drawing.Size(93, 40);
            this.btn_Controller_Open.TabIndex = 7;
            this.btn_Controller_Open.Text = "Open";
            this.btn_Controller_Open.UseVisualStyleBackColor = false;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.btn_2ch_Set);
            this.panel6.Controls.Add(this.btn_1ch_Set);
            this.panel6.Controls.Add(this.label3);
            this.panel6.Controls.Add(this.label4);
            this.panel6.Controls.Add(this.tbox_2ch_pr_bright_set);
            this.panel6.Controls.Add(this.tbox_1ch_pr_bright_set);
            this.panel6.Controls.Add(this.label5);
            this.panel6.Location = new System.Drawing.Point(385, 104);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(511, 201);
            this.panel6.TabIndex = 4;
            // 
            // btn_2ch_Set
            // 
            this.btn_2ch_Set.BackColor = System.Drawing.Color.DimGray;
            this.btn_2ch_Set.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_2ch_Set.ForeColor = System.Drawing.Color.White;
            this.btn_2ch_Set.Location = new System.Drawing.Point(369, 147);
            this.btn_2ch_Set.Name = "btn_2ch_Set";
            this.btn_2ch_Set.Size = new System.Drawing.Size(115, 43);
            this.btn_2ch_Set.TabIndex = 29;
            this.btn_2ch_Set.Text = "2채널 설정";
            this.btn_2ch_Set.UseVisualStyleBackColor = false;
            // 
            // btn_1ch_Set
            // 
            this.btn_1ch_Set.BackColor = System.Drawing.Color.DimGray;
            this.btn_1ch_Set.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_1ch_Set.ForeColor = System.Drawing.Color.White;
            this.btn_1ch_Set.Location = new System.Drawing.Point(369, 82);
            this.btn_1ch_Set.Name = "btn_1ch_Set";
            this.btn_1ch_Set.Size = new System.Drawing.Size(115, 43);
            this.btn_1ch_Set.TabIndex = 28;
            this.btn_1ch_Set.Text = "1채널 설정";
            this.btn_1ch_Set.UseVisualStyleBackColor = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(40, 153);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(152, 20);
            this.label3.TabIndex = 27;
            this.label3.Text = "2채널 현재 밝기 설정";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(40, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(152, 20);
            this.label4.TabIndex = 26;
            this.label4.Text = "1채널 현재 밝기 설정";
            // 
            // tbox_2ch_pr_bright_set
            // 
            this.tbox_2ch_pr_bright_set.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_2ch_pr_bright_set.Font = new System.Drawing.Font("Malgun Gothic", 20F, System.Drawing.FontStyle.Bold);
            this.tbox_2ch_pr_bright_set.Location = new System.Drawing.Point(208, 144);
            this.tbox_2ch_pr_bright_set.Name = "tbox_2ch_pr_bright_set";
            this.tbox_2ch_pr_bright_set.Size = new System.Drawing.Size(131, 43);
            this.tbox_2ch_pr_bright_set.TabIndex = 25;
            // 
            // tbox_1ch_pr_bright_set
            // 
            this.tbox_1ch_pr_bright_set.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_1ch_pr_bright_set.Font = new System.Drawing.Font("Malgun Gothic", 20F, System.Drawing.FontStyle.Bold);
            this.tbox_1ch_pr_bright_set.Location = new System.Drawing.Point(208, 82);
            this.tbox_1ch_pr_bright_set.Name = "tbox_1ch_pr_bright_set";
            this.tbox_1ch_pr_bright_set.Size = new System.Drawing.Size(131, 43);
            this.tbox_1ch_pr_bright_set.TabIndex = 24;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Malgun Gothic", 13F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(46, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(108, 25);
            this.label5.TabIndex = 23;
            this.label5.Text = "밝기값 설정";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Controls.Add(this.tbox_2ch_bright_value);
            this.panel5.Controls.Add(this.tbox_1ch_bright_value);
            this.panel5.Controls.Add(this.label8);
            this.panel5.Location = new System.Drawing.Point(8, 104);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(371, 201);
            this.panel5.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(24, 153);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 20);
            this.label2.TabIndex = 22;
            this.label2.Text = "2채널 현재 밝기값";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(24, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 20);
            this.label1.TabIndex = 21;
            this.label1.Text = "1채널 현재 밝기값";
            // 
            // tbox_2ch_bright_value
            // 
            this.tbox_2ch_bright_value.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_2ch_bright_value.Font = new System.Drawing.Font("Malgun Gothic", 20F, System.Drawing.FontStyle.Bold);
            this.tbox_2ch_bright_value.Location = new System.Drawing.Point(205, 144);
            this.tbox_2ch_bright_value.Name = "tbox_2ch_bright_value";
            this.tbox_2ch_bright_value.Size = new System.Drawing.Size(147, 43);
            this.tbox_2ch_bright_value.TabIndex = 20;
            // 
            // tbox_1ch_bright_value
            // 
            this.tbox_1ch_bright_value.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_1ch_bright_value.Font = new System.Drawing.Font("Malgun Gothic", 20F, System.Drawing.FontStyle.Bold);
            this.tbox_1ch_bright_value.Location = new System.Drawing.Point(205, 82);
            this.tbox_1ch_bright_value.Name = "tbox_1ch_bright_value";
            this.tbox_1ch_bright_value.Size = new System.Drawing.Size(147, 43);
            this.tbox_1ch_bright_value.TabIndex = 19;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Malgun Gothic", 13F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(30, 31);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(108, 25);
            this.label8.TabIndex = 18;
            this.label8.Text = "현재 밝기값";
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.HotTrack;
            this.textBox2.Enabled = false;
            this.textBox2.Font = new System.Drawing.Font("Malgun Gothic", 18F, System.Drawing.FontStyle.Bold);
            this.textBox2.ForeColor = System.Drawing.Color.White;
            this.textBox2.Location = new System.Drawing.Point(3, 3);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(909, 39);
            this.textBox2.TabIndex = 2;
            this.textBox2.Text = "ERI 조명 컨트롤러";
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.btn_ULd_HandyBacord_Close);
            this.panel17.Controls.Add(this.btn_ULd_HandyBacord_Open);
            this.panel17.Controls.Add(this.panel18);
            this.panel17.Controls.Add(this.textBox34);
            this.panel17.Location = new System.Drawing.Point(942, 534);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(915, 255);
            this.panel17.TabIndex = 21;
            // 
            // btn_ULd_HandyBacord_Close
            // 
            this.btn_ULd_HandyBacord_Close.BackColor = System.Drawing.Color.DimGray;
            this.btn_ULd_HandyBacord_Close.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_ULd_HandyBacord_Close.ForeColor = System.Drawing.Color.White;
            this.btn_ULd_HandyBacord_Close.Location = new System.Drawing.Point(106, 48);
            this.btn_ULd_HandyBacord_Close.Name = "btn_ULd_HandyBacord_Close";
            this.btn_ULd_HandyBacord_Close.Size = new System.Drawing.Size(93, 40);
            this.btn_ULd_HandyBacord_Close.TabIndex = 10;
            this.btn_ULd_HandyBacord_Close.Text = "Close";
            this.btn_ULd_HandyBacord_Close.UseVisualStyleBackColor = false;
            // 
            // btn_ULd_HandyBacord_Open
            // 
            this.btn_ULd_HandyBacord_Open.BackColor = System.Drawing.Color.DimGray;
            this.btn_ULd_HandyBacord_Open.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_ULd_HandyBacord_Open.ForeColor = System.Drawing.Color.White;
            this.btn_ULd_HandyBacord_Open.Location = new System.Drawing.Point(9, 48);
            this.btn_ULd_HandyBacord_Open.Name = "btn_ULd_HandyBacord_Open";
            this.btn_ULd_HandyBacord_Open.Size = new System.Drawing.Size(93, 40);
            this.btn_ULd_HandyBacord_Open.TabIndex = 9;
            this.btn_ULd_HandyBacord_Open.Text = "Open";
            this.btn_ULd_HandyBacord_Open.UseVisualStyleBackColor = false;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.btn_ULd_Handy_Get);
            this.panel18.Controls.Add(this.tbox_ULd_HandyBarcode);
            this.panel18.Location = new System.Drawing.Point(9, 94);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(903, 152);
            this.panel18.TabIndex = 5;
            // 
            // btn_ULd_Handy_Get
            // 
            this.btn_ULd_Handy_Get.BackColor = System.Drawing.Color.DimGray;
            this.btn_ULd_Handy_Get.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_ULd_Handy_Get.ForeColor = System.Drawing.Color.White;
            this.btn_ULd_Handy_Get.Location = new System.Drawing.Point(42, 47);
            this.btn_ULd_Handy_Get.Name = "btn_ULd_Handy_Get";
            this.btn_ULd_Handy_Get.Size = new System.Drawing.Size(166, 66);
            this.btn_ULd_Handy_Get.TabIndex = 29;
            this.btn_ULd_Handy_Get.Text = "Get";
            this.btn_ULd_Handy_Get.UseVisualStyleBackColor = false;
            // 
            // tbox_ULd_HandyBarcode
            // 
            this.tbox_ULd_HandyBarcode.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_ULd_HandyBarcode.Font = new System.Drawing.Font("Malgun Gothic", 30F, System.Drawing.FontStyle.Bold);
            this.tbox_ULd_HandyBarcode.Location = new System.Drawing.Point(289, 49);
            this.tbox_ULd_HandyBarcode.Name = "tbox_ULd_HandyBarcode";
            this.tbox_ULd_HandyBarcode.Size = new System.Drawing.Size(417, 61);
            this.tbox_ULd_HandyBarcode.TabIndex = 25;
            // 
            // textBox34
            // 
            this.textBox34.BackColor = System.Drawing.SystemColors.HotTrack;
            this.textBox34.Enabled = false;
            this.textBox34.Font = new System.Drawing.Font("Malgun Gothic", 18F, System.Drawing.FontStyle.Bold);
            this.textBox34.ForeColor = System.Drawing.Color.White;
            this.textBox34.Location = new System.Drawing.Point(3, 3);
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(909, 39);
            this.textBox34.TabIndex = 3;
            this.textBox34.Text = "핸디 바코드 리더";
            this.textBox34.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.btn_ULd_Barcode_B_Close);
            this.panel19.Controls.Add(this.btn_ULd_Barcode_B_Open);
            this.panel19.Controls.Add(this.panel20);
            this.panel19.Controls.Add(this.textBox36);
            this.panel19.Location = new System.Drawing.Point(942, 270);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(915, 255);
            this.panel19.TabIndex = 20;
            // 
            // btn_ULd_Barcode_B_Close
            // 
            this.btn_ULd_Barcode_B_Close.BackColor = System.Drawing.Color.DimGray;
            this.btn_ULd_Barcode_B_Close.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_ULd_Barcode_B_Close.ForeColor = System.Drawing.Color.White;
            this.btn_ULd_Barcode_B_Close.Location = new System.Drawing.Point(106, 48);
            this.btn_ULd_Barcode_B_Close.Name = "btn_ULd_Barcode_B_Close";
            this.btn_ULd_Barcode_B_Close.Size = new System.Drawing.Size(93, 40);
            this.btn_ULd_Barcode_B_Close.TabIndex = 10;
            this.btn_ULd_Barcode_B_Close.Text = "Close";
            this.btn_ULd_Barcode_B_Close.UseVisualStyleBackColor = false;
            // 
            // btn_ULd_Barcode_B_Open
            // 
            this.btn_ULd_Barcode_B_Open.BackColor = System.Drawing.Color.DimGray;
            this.btn_ULd_Barcode_B_Open.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_ULd_Barcode_B_Open.ForeColor = System.Drawing.Color.White;
            this.btn_ULd_Barcode_B_Open.Location = new System.Drawing.Point(9, 48);
            this.btn_ULd_Barcode_B_Open.Name = "btn_ULd_Barcode_B_Open";
            this.btn_ULd_Barcode_B_Open.Size = new System.Drawing.Size(93, 40);
            this.btn_ULd_Barcode_B_Open.TabIndex = 9;
            this.btn_ULd_Barcode_B_Open.Text = "Open";
            this.btn_ULd_Barcode_B_Open.UseVisualStyleBackColor = false;
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.button30);
            this.panel20.Controls.Add(this.tbox_ULd_Barcode_B);
            this.panel20.Location = new System.Drawing.Point(9, 94);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(903, 152);
            this.panel20.TabIndex = 5;
            // 
            // button30
            // 
            this.button30.BackColor = System.Drawing.Color.DimGray;
            this.button30.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.button30.ForeColor = System.Drawing.Color.White;
            this.button30.Location = new System.Drawing.Point(42, 47);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(166, 66);
            this.button30.TabIndex = 29;
            this.button30.Text = "Get";
            this.button30.UseVisualStyleBackColor = false;
            // 
            // tbox_ULd_Barcode_B
            // 
            this.tbox_ULd_Barcode_B.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_ULd_Barcode_B.Font = new System.Drawing.Font("Malgun Gothic", 30F, System.Drawing.FontStyle.Bold);
            this.tbox_ULd_Barcode_B.Location = new System.Drawing.Point(289, 49);
            this.tbox_ULd_Barcode_B.Name = "tbox_ULd_Barcode_B";
            this.tbox_ULd_Barcode_B.Size = new System.Drawing.Size(417, 61);
            this.tbox_ULd_Barcode_B.TabIndex = 25;
            // 
            // textBox36
            // 
            this.textBox36.BackColor = System.Drawing.SystemColors.HotTrack;
            this.textBox36.Enabled = false;
            this.textBox36.Font = new System.Drawing.Font("Malgun Gothic", 18F, System.Drawing.FontStyle.Bold);
            this.textBox36.ForeColor = System.Drawing.Color.White;
            this.textBox36.Location = new System.Drawing.Point(3, 3);
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new System.Drawing.Size(909, 39);
            this.textBox36.TabIndex = 3;
            this.textBox36.Text = "언로더부 고정식 바코드 리더 B";
            this.textBox36.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.btn_Ld_Barcode_B_Close);
            this.panel21.Controls.Add(this.btn_Ld_Barcode_B_Open);
            this.panel21.Controls.Add(this.panel22);
            this.panel21.Controls.Add(this.textBox38);
            this.panel21.Location = new System.Drawing.Point(942, 7);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(915, 255);
            this.panel21.TabIndex = 19;
            // 
            // btn_Ld_Barcode_B_Close
            // 
            this.btn_Ld_Barcode_B_Close.BackColor = System.Drawing.Color.DimGray;
            this.btn_Ld_Barcode_B_Close.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Ld_Barcode_B_Close.ForeColor = System.Drawing.Color.White;
            this.btn_Ld_Barcode_B_Close.Location = new System.Drawing.Point(106, 48);
            this.btn_Ld_Barcode_B_Close.Name = "btn_Ld_Barcode_B_Close";
            this.btn_Ld_Barcode_B_Close.Size = new System.Drawing.Size(93, 40);
            this.btn_Ld_Barcode_B_Close.TabIndex = 10;
            this.btn_Ld_Barcode_B_Close.Text = "Close";
            this.btn_Ld_Barcode_B_Close.UseVisualStyleBackColor = false;
            // 
            // btn_Ld_Barcode_B_Open
            // 
            this.btn_Ld_Barcode_B_Open.BackColor = System.Drawing.Color.DimGray;
            this.btn_Ld_Barcode_B_Open.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Ld_Barcode_B_Open.ForeColor = System.Drawing.Color.White;
            this.btn_Ld_Barcode_B_Open.Location = new System.Drawing.Point(9, 48);
            this.btn_Ld_Barcode_B_Open.Name = "btn_Ld_Barcode_B_Open";
            this.btn_Ld_Barcode_B_Open.Size = new System.Drawing.Size(93, 40);
            this.btn_Ld_Barcode_B_Open.TabIndex = 9;
            this.btn_Ld_Barcode_B_Open.Text = "Open";
            this.btn_Ld_Barcode_B_Open.UseVisualStyleBackColor = false;
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.button33);
            this.panel22.Controls.Add(this.tbox_Ld_Barcode_B);
            this.panel22.Location = new System.Drawing.Point(9, 94);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(903, 152);
            this.panel22.TabIndex = 5;
            // 
            // button33
            // 
            this.button33.BackColor = System.Drawing.Color.DimGray;
            this.button33.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.button33.ForeColor = System.Drawing.Color.White;
            this.button33.Location = new System.Drawing.Point(42, 47);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(166, 66);
            this.button33.TabIndex = 29;
            this.button33.Text = "Get";
            this.button33.UseVisualStyleBackColor = false;
            // 
            // tbox_Ld_Barcode_B
            // 
            this.tbox_Ld_Barcode_B.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_Ld_Barcode_B.Font = new System.Drawing.Font("Malgun Gothic", 30F, System.Drawing.FontStyle.Bold);
            this.tbox_Ld_Barcode_B.Location = new System.Drawing.Point(289, 49);
            this.tbox_Ld_Barcode_B.Name = "tbox_Ld_Barcode_B";
            this.tbox_Ld_Barcode_B.Size = new System.Drawing.Size(417, 61);
            this.tbox_Ld_Barcode_B.TabIndex = 25;
            // 
            // textBox38
            // 
            this.textBox38.BackColor = System.Drawing.SystemColors.HotTrack;
            this.textBox38.Enabled = false;
            this.textBox38.Font = new System.Drawing.Font("Malgun Gothic", 18F, System.Drawing.FontStyle.Bold);
            this.textBox38.ForeColor = System.Drawing.Color.White;
            this.textBox38.Location = new System.Drawing.Point(3, 3);
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new System.Drawing.Size(909, 39);
            this.textBox38.TabIndex = 3;
            this.textBox38.Text = "로더부 고정식 바코드 리더  B";
            this.textBox38.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.btn_Ld_HandyBacord_Close);
            this.panel15.Controls.Add(this.btn_Ld_HandyBacord_Open);
            this.panel15.Controls.Add(this.panel16);
            this.panel15.Controls.Add(this.textBox32);
            this.panel15.Location = new System.Drawing.Point(6, 534);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(915, 255);
            this.panel15.TabIndex = 18;
            // 
            // btn_Ld_HandyBacord_Close
            // 
            this.btn_Ld_HandyBacord_Close.BackColor = System.Drawing.Color.DimGray;
            this.btn_Ld_HandyBacord_Close.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Ld_HandyBacord_Close.ForeColor = System.Drawing.Color.White;
            this.btn_Ld_HandyBacord_Close.Location = new System.Drawing.Point(106, 48);
            this.btn_Ld_HandyBacord_Close.Name = "btn_Ld_HandyBacord_Close";
            this.btn_Ld_HandyBacord_Close.Size = new System.Drawing.Size(93, 40);
            this.btn_Ld_HandyBacord_Close.TabIndex = 10;
            this.btn_Ld_HandyBacord_Close.Text = "Close";
            this.btn_Ld_HandyBacord_Close.UseVisualStyleBackColor = false;
            // 
            // btn_Ld_HandyBacord_Open
            // 
            this.btn_Ld_HandyBacord_Open.BackColor = System.Drawing.Color.DimGray;
            this.btn_Ld_HandyBacord_Open.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Ld_HandyBacord_Open.ForeColor = System.Drawing.Color.White;
            this.btn_Ld_HandyBacord_Open.Location = new System.Drawing.Point(9, 48);
            this.btn_Ld_HandyBacord_Open.Name = "btn_Ld_HandyBacord_Open";
            this.btn_Ld_HandyBacord_Open.Size = new System.Drawing.Size(93, 40);
            this.btn_Ld_HandyBacord_Open.TabIndex = 9;
            this.btn_Ld_HandyBacord_Open.Text = "Open";
            this.btn_Ld_HandyBacord_Open.UseVisualStyleBackColor = false;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.btn_Ld_Handy_Get);
            this.panel16.Controls.Add(this.tbox_Ld_HandyBarcode);
            this.panel16.Location = new System.Drawing.Point(9, 94);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(895, 152);
            this.panel16.TabIndex = 5;
            // 
            // btn_Ld_Handy_Get
            // 
            this.btn_Ld_Handy_Get.BackColor = System.Drawing.Color.DimGray;
            this.btn_Ld_Handy_Get.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Ld_Handy_Get.ForeColor = System.Drawing.Color.White;
            this.btn_Ld_Handy_Get.Location = new System.Drawing.Point(42, 47);
            this.btn_Ld_Handy_Get.Name = "btn_Ld_Handy_Get";
            this.btn_Ld_Handy_Get.Size = new System.Drawing.Size(166, 66);
            this.btn_Ld_Handy_Get.TabIndex = 29;
            this.btn_Ld_Handy_Get.Text = "Get";
            this.btn_Ld_Handy_Get.UseVisualStyleBackColor = false;
            // 
            // tbox_Ld_HandyBarcode
            // 
            this.tbox_Ld_HandyBarcode.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_Ld_HandyBarcode.Font = new System.Drawing.Font("Malgun Gothic", 30F, System.Drawing.FontStyle.Bold);
            this.tbox_Ld_HandyBarcode.Location = new System.Drawing.Point(289, 49);
            this.tbox_Ld_HandyBarcode.Name = "tbox_Ld_HandyBarcode";
            this.tbox_Ld_HandyBarcode.Size = new System.Drawing.Size(417, 61);
            this.tbox_Ld_HandyBarcode.TabIndex = 25;
            // 
            // textBox32
            // 
            this.textBox32.BackColor = System.Drawing.SystemColors.HotTrack;
            this.textBox32.Enabled = false;
            this.textBox32.Font = new System.Drawing.Font("Malgun Gothic", 18F, System.Drawing.FontStyle.Bold);
            this.textBox32.ForeColor = System.Drawing.Color.White;
            this.textBox32.Location = new System.Drawing.Point(3, 3);
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new System.Drawing.Size(909, 39);
            this.textBox32.TabIndex = 3;
            this.textBox32.Text = "핸디 바코드 리더";
            this.textBox32.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.btn_ULd_Barcode_A_Close);
            this.panel13.Controls.Add(this.btn_ULd_Barcode_A_Open);
            this.panel13.Controls.Add(this.panel14);
            this.panel13.Controls.Add(this.textBox30);
            this.panel13.Location = new System.Drawing.Point(6, 270);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(915, 255);
            this.panel13.TabIndex = 17;
            // 
            // btn_ULd_Barcode_A_Close
            // 
            this.btn_ULd_Barcode_A_Close.BackColor = System.Drawing.Color.DimGray;
            this.btn_ULd_Barcode_A_Close.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_ULd_Barcode_A_Close.ForeColor = System.Drawing.Color.White;
            this.btn_ULd_Barcode_A_Close.Location = new System.Drawing.Point(106, 48);
            this.btn_ULd_Barcode_A_Close.Name = "btn_ULd_Barcode_A_Close";
            this.btn_ULd_Barcode_A_Close.Size = new System.Drawing.Size(93, 40);
            this.btn_ULd_Barcode_A_Close.TabIndex = 10;
            this.btn_ULd_Barcode_A_Close.Text = "Close";
            this.btn_ULd_Barcode_A_Close.UseVisualStyleBackColor = false;
            // 
            // btn_ULd_Barcode_A_Open
            // 
            this.btn_ULd_Barcode_A_Open.BackColor = System.Drawing.Color.DimGray;
            this.btn_ULd_Barcode_A_Open.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_ULd_Barcode_A_Open.ForeColor = System.Drawing.Color.White;
            this.btn_ULd_Barcode_A_Open.Location = new System.Drawing.Point(9, 48);
            this.btn_ULd_Barcode_A_Open.Name = "btn_ULd_Barcode_A_Open";
            this.btn_ULd_Barcode_A_Open.Size = new System.Drawing.Size(93, 40);
            this.btn_ULd_Barcode_A_Open.TabIndex = 9;
            this.btn_ULd_Barcode_A_Open.Text = "Open";
            this.btn_ULd_Barcode_A_Open.UseVisualStyleBackColor = false;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.button21);
            this.panel14.Controls.Add(this.tbox_ULd_Barcode_A);
            this.panel14.Location = new System.Drawing.Point(9, 94);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(895, 152);
            this.panel14.TabIndex = 5;
            // 
            // button21
            // 
            this.button21.BackColor = System.Drawing.Color.DimGray;
            this.button21.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.button21.ForeColor = System.Drawing.Color.White;
            this.button21.Location = new System.Drawing.Point(42, 47);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(166, 66);
            this.button21.TabIndex = 29;
            this.button21.Text = "Get";
            this.button21.UseVisualStyleBackColor = false;
            // 
            // tbox_ULd_Barcode_A
            // 
            this.tbox_ULd_Barcode_A.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_ULd_Barcode_A.Font = new System.Drawing.Font("Malgun Gothic", 30F, System.Drawing.FontStyle.Bold);
            this.tbox_ULd_Barcode_A.Location = new System.Drawing.Point(289, 49);
            this.tbox_ULd_Barcode_A.Name = "tbox_ULd_Barcode_A";
            this.tbox_ULd_Barcode_A.Size = new System.Drawing.Size(417, 61);
            this.tbox_ULd_Barcode_A.TabIndex = 25;
            // 
            // textBox30
            // 
            this.textBox30.BackColor = System.Drawing.SystemColors.HotTrack;
            this.textBox30.Enabled = false;
            this.textBox30.Font = new System.Drawing.Font("Malgun Gothic", 18F, System.Drawing.FontStyle.Bold);
            this.textBox30.ForeColor = System.Drawing.Color.White;
            this.textBox30.Location = new System.Drawing.Point(3, 3);
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new System.Drawing.Size(909, 39);
            this.textBox30.TabIndex = 3;
            this.textBox30.Text = "언로더부 고정식 바코드 리더 A";
            this.textBox30.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.btn_Ld_Barcode_A_Close);
            this.panel11.Controls.Add(this.btn_Ld_Barcode_A_Open);
            this.panel11.Controls.Add(this.panel12);
            this.panel11.Controls.Add(this.textBox28);
            this.panel11.Location = new System.Drawing.Point(6, 7);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(915, 255);
            this.panel11.TabIndex = 16;
            // 
            // btn_Ld_Barcode_A_Close
            // 
            this.btn_Ld_Barcode_A_Close.BackColor = System.Drawing.Color.DimGray;
            this.btn_Ld_Barcode_A_Close.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Ld_Barcode_A_Close.ForeColor = System.Drawing.Color.White;
            this.btn_Ld_Barcode_A_Close.Location = new System.Drawing.Point(106, 48);
            this.btn_Ld_Barcode_A_Close.Name = "btn_Ld_Barcode_A_Close";
            this.btn_Ld_Barcode_A_Close.Size = new System.Drawing.Size(93, 40);
            this.btn_Ld_Barcode_A_Close.TabIndex = 10;
            this.btn_Ld_Barcode_A_Close.Text = "Close";
            this.btn_Ld_Barcode_A_Close.UseVisualStyleBackColor = false;
            // 
            // btn_Ld_Barcode_A_Open
            // 
            this.btn_Ld_Barcode_A_Open.BackColor = System.Drawing.Color.DimGray;
            this.btn_Ld_Barcode_A_Open.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Ld_Barcode_A_Open.ForeColor = System.Drawing.Color.White;
            this.btn_Ld_Barcode_A_Open.Location = new System.Drawing.Point(9, 48);
            this.btn_Ld_Barcode_A_Open.Name = "btn_Ld_Barcode_A_Open";
            this.btn_Ld_Barcode_A_Open.Size = new System.Drawing.Size(93, 40);
            this.btn_Ld_Barcode_A_Open.TabIndex = 9;
            this.btn_Ld_Barcode_A_Open.Text = "Open";
            this.btn_Ld_Barcode_A_Open.UseVisualStyleBackColor = false;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.button18);
            this.panel12.Controls.Add(this.tbox_Ld_Barcode_A);
            this.panel12.Location = new System.Drawing.Point(9, 94);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(895, 152);
            this.panel12.TabIndex = 5;
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.Color.DimGray;
            this.button18.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.button18.ForeColor = System.Drawing.Color.White;
            this.button18.Location = new System.Drawing.Point(42, 47);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(166, 66);
            this.button18.TabIndex = 29;
            this.button18.Text = "Get";
            this.button18.UseVisualStyleBackColor = false;
            // 
            // tbox_Ld_Barcode_A
            // 
            this.tbox_Ld_Barcode_A.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_Ld_Barcode_A.Font = new System.Drawing.Font("Malgun Gothic", 30F, System.Drawing.FontStyle.Bold);
            this.tbox_Ld_Barcode_A.Location = new System.Drawing.Point(289, 49);
            this.tbox_Ld_Barcode_A.Name = "tbox_Ld_Barcode_A";
            this.tbox_Ld_Barcode_A.Size = new System.Drawing.Size(417, 61);
            this.tbox_Ld_Barcode_A.TabIndex = 25;
            // 
            // textBox28
            // 
            this.textBox28.BackColor = System.Drawing.SystemColors.HotTrack;
            this.textBox28.Enabled = false;
            this.textBox28.Font = new System.Drawing.Font("Malgun Gothic", 18F, System.Drawing.FontStyle.Bold);
            this.textBox28.ForeColor = System.Drawing.Color.White;
            this.textBox28.Location = new System.Drawing.Point(3, 3);
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new System.Drawing.Size(909, 39);
            this.textBox28.TabIndex = 3;
            this.textBox28.Text = "로더부 고정식 바코드 리더 A";
            this.textBox28.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel23
            // 
            this.panel23.Controls.Add(this.label46);
            this.panel23.Controls.Add(this.label47);
            this.panel23.Controls.Add(this.tbox_LDS_Break_TableIdx);
            this.panel23.Controls.Add(this.label48);
            this.panel23.Controls.Add(this.tbox_LDS_Break_Measure);
            this.panel23.Controls.Add(this.label49);
            this.panel23.Controls.Add(this.button34);
            this.panel23.Controls.Add(this.tbox_LDS_Break_Standby);
            this.panel23.Controls.Add(this.btn_LDS_Break_Stop);
            this.panel23.Controls.Add(this.btn_LDS_Break_Start);
            this.panel23.Controls.Add(this.panel24);
            this.panel23.Controls.Add(this.textBox90);
            this.panel23.Location = new System.Drawing.Point(944, 412);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(915, 400);
            this.panel23.TabIndex = 58;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Malgun Gothic", 10F);
            this.label46.ForeColor = System.Drawing.Color.White;
            this.label46.Location = new System.Drawing.Point(597, 63);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(51, 19);
            this.label46.TabIndex = 53;
            this.label46.Text = "대기중";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.label47.ForeColor = System.Drawing.Color.White;
            this.label47.Location = new System.Drawing.Point(392, 63);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(90, 15);
            this.label47.TabIndex = 52;
            this.label47.Text = "테이블 인덱스 :";
            // 
            // tbox_LDS_Break_TableIdx
            // 
            this.tbox_LDS_Break_TableIdx.BackColor = System.Drawing.Color.White;
            this.tbox_LDS_Break_TableIdx.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.tbox_LDS_Break_TableIdx.ForeColor = System.Drawing.Color.White;
            this.tbox_LDS_Break_TableIdx.Location = new System.Drawing.Point(503, 55);
            this.tbox_LDS_Break_TableIdx.Name = "tbox_LDS_Break_TableIdx";
            this.tbox_LDS_Break_TableIdx.Size = new System.Drawing.Size(64, 34);
            this.tbox_LDS_Break_TableIdx.TabIndex = 51;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.label48.ForeColor = System.Drawing.Color.White;
            this.label48.Location = new System.Drawing.Point(214, 63);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(72, 15);
            this.label48.TabIndex = 50;
            this.label48.Text = "측정 시간[s]";
            // 
            // tbox_LDS_Break_Measure
            // 
            this.tbox_LDS_Break_Measure.BackColor = System.Drawing.Color.White;
            this.tbox_LDS_Break_Measure.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.tbox_LDS_Break_Measure.ForeColor = System.Drawing.Color.White;
            this.tbox_LDS_Break_Measure.Location = new System.Drawing.Point(307, 55);
            this.tbox_LDS_Break_Measure.Name = "tbox_LDS_Break_Measure";
            this.tbox_LDS_Break_Measure.Size = new System.Drawing.Size(64, 34);
            this.tbox_LDS_Break_Measure.TabIndex = 49;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.label49.ForeColor = System.Drawing.Color.White;
            this.label49.Location = new System.Drawing.Point(36, 63);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(72, 15);
            this.label49.TabIndex = 48;
            this.label49.Text = "대기 시간[s]";
            // 
            // button34
            // 
            this.button34.BackColor = System.Drawing.Color.DimGray;
            this.button34.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.button34.ForeColor = System.Drawing.Color.White;
            this.button34.Location = new System.Drawing.Point(789, 359);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(114, 32);
            this.button34.TabIndex = 30;
            this.button34.Text = "Save";
            this.button34.UseVisualStyleBackColor = false;
            // 
            // tbox_LDS_Break_Standby
            // 
            this.tbox_LDS_Break_Standby.BackColor = System.Drawing.Color.White;
            this.tbox_LDS_Break_Standby.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.tbox_LDS_Break_Standby.ForeColor = System.Drawing.Color.White;
            this.tbox_LDS_Break_Standby.Location = new System.Drawing.Point(129, 55);
            this.tbox_LDS_Break_Standby.Name = "tbox_LDS_Break_Standby";
            this.tbox_LDS_Break_Standby.Size = new System.Drawing.Size(64, 34);
            this.tbox_LDS_Break_Standby.TabIndex = 47;
            // 
            // btn_LDS_Break_Stop
            // 
            this.btn_LDS_Break_Stop.BackColor = System.Drawing.Color.DimGray;
            this.btn_LDS_Break_Stop.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_LDS_Break_Stop.ForeColor = System.Drawing.Color.White;
            this.btn_LDS_Break_Stop.Location = new System.Drawing.Point(767, 49);
            this.btn_LDS_Break_Stop.Name = "btn_LDS_Break_Stop";
            this.btn_LDS_Break_Stop.Size = new System.Drawing.Size(93, 40);
            this.btn_LDS_Break_Stop.TabIndex = 14;
            this.btn_LDS_Break_Stop.Text = "측정 정지";
            this.btn_LDS_Break_Stop.UseVisualStyleBackColor = false;
            // 
            // btn_LDS_Break_Start
            // 
            this.btn_LDS_Break_Start.BackColor = System.Drawing.Color.DimGray;
            this.btn_LDS_Break_Start.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_LDS_Break_Start.ForeColor = System.Drawing.Color.White;
            this.btn_LDS_Break_Start.Location = new System.Drawing.Point(670, 49);
            this.btn_LDS_Break_Start.Name = "btn_LDS_Break_Start";
            this.btn_LDS_Break_Start.Size = new System.Drawing.Size(93, 40);
            this.btn_LDS_Break_Start.TabIndex = 13;
            this.btn_LDS_Break_Start.Text = "측정 시작";
            this.btn_LDS_Break_Start.UseVisualStyleBackColor = false;
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.listView2);
            this.panel24.Controls.Add(this.label50);
            this.panel24.Controls.Add(this.textBox78);
            this.panel24.Controls.Add(this.label51);
            this.panel24.Controls.Add(this.textBox79);
            this.panel24.Controls.Add(this.label52);
            this.panel24.Controls.Add(this.label53);
            this.panel24.Controls.Add(this.textBox80);
            this.panel24.Controls.Add(this.textBox81);
            this.panel24.Controls.Add(this.label54);
            this.panel24.Controls.Add(this.textBox82);
            this.panel24.Controls.Add(this.label55);
            this.panel24.Controls.Add(this.label56);
            this.panel24.Controls.Add(this.textBox83);
            this.panel24.Controls.Add(this.textBox84);
            this.panel24.Controls.Add(this.textBox85);
            this.panel24.Controls.Add(this.label57);
            this.panel24.Controls.Add(this.label58);
            this.panel24.Controls.Add(this.textBox86);
            this.panel24.Controls.Add(this.textBox87);
            this.panel24.Controls.Add(this.label59);
            this.panel24.Controls.Add(this.label60);
            this.panel24.Controls.Add(this.textBox88);
            this.panel24.Controls.Add(this.textBox89);
            this.panel24.Controls.Add(this.label61);
            this.panel24.Location = new System.Drawing.Point(8, 99);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(895, 254);
            this.panel24.TabIndex = 9;
            // 
            // listView2
            // 
            this.listView2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6});
            this.listView2.Font = new System.Drawing.Font("Malgun Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.listView2.Location = new System.Drawing.Point(418, 29);
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(463, 222);
            this.listView2.TabIndex = 46;
            this.listView2.UseCompatibleStateImageBehavior = false;
            this.listView2.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "인덱스";
            this.columnHeader4.Width = 134;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "측정값";
            this.columnHeader5.Width = 121;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "차이값";
            this.columnHeader6.Width = 130;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.label50.ForeColor = System.Drawing.Color.White;
            this.label50.Location = new System.Drawing.Point(209, 215);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(100, 15);
            this.label50.TabIndex = 45;
            this.label50.Text = "Y 이동간격 (mm)";
            // 
            // textBox78
            // 
            this.textBox78.BackColor = System.Drawing.Color.White;
            this.textBox78.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.textBox78.ForeColor = System.Drawing.Color.White;
            this.textBox78.Location = new System.Drawing.Point(314, 203);
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new System.Drawing.Size(64, 34);
            this.textBox78.TabIndex = 44;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label51.ForeColor = System.Drawing.Color.White;
            this.label51.Location = new System.Drawing.Point(277, 162);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(17, 20);
            this.label51.TabIndex = 43;
            this.label51.Text = "9";
            // 
            // textBox79
            // 
            this.textBox79.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBox79.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.textBox79.ForeColor = System.Drawing.Color.White;
            this.textBox79.Location = new System.Drawing.Point(314, 152);
            this.textBox79.Name = "textBox79";
            this.textBox79.Size = new System.Drawing.Size(64, 34);
            this.textBox79.TabIndex = 42;
            this.textBox79.Text = "미측정";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label52.ForeColor = System.Drawing.Color.White;
            this.label52.Location = new System.Drawing.Point(277, 115);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(17, 20);
            this.label52.TabIndex = 41;
            this.label52.Text = "6";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label53.ForeColor = System.Drawing.Color.White;
            this.label53.Location = new System.Drawing.Point(277, 68);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(17, 20);
            this.label53.TabIndex = 40;
            this.label53.Text = "3";
            // 
            // textBox80
            // 
            this.textBox80.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBox80.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.textBox80.ForeColor = System.Drawing.Color.White;
            this.textBox80.Location = new System.Drawing.Point(314, 106);
            this.textBox80.Name = "textBox80";
            this.textBox80.Size = new System.Drawing.Size(64, 34);
            this.textBox80.TabIndex = 39;
            this.textBox80.Text = "미측정";
            // 
            // textBox81
            // 
            this.textBox81.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBox81.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.textBox81.ForeColor = System.Drawing.Color.White;
            this.textBox81.Location = new System.Drawing.Point(314, 58);
            this.textBox81.Name = "textBox81";
            this.textBox81.Size = new System.Drawing.Size(64, 34);
            this.textBox81.TabIndex = 38;
            this.textBox81.Text = "미측정";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label54.ForeColor = System.Drawing.Color.White;
            this.label54.Location = new System.Drawing.Point(151, 163);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(17, 20);
            this.label54.TabIndex = 37;
            this.label54.Text = "8";
            // 
            // textBox82
            // 
            this.textBox82.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBox82.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.textBox82.ForeColor = System.Drawing.Color.White;
            this.textBox82.Location = new System.Drawing.Point(188, 153);
            this.textBox82.Name = "textBox82";
            this.textBox82.Size = new System.Drawing.Size(64, 34);
            this.textBox82.TabIndex = 36;
            this.textBox82.Text = "미측정";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label55.ForeColor = System.Drawing.Color.White;
            this.label55.Location = new System.Drawing.Point(151, 116);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(17, 20);
            this.label55.TabIndex = 35;
            this.label55.Text = "5";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label56.ForeColor = System.Drawing.Color.White;
            this.label56.Location = new System.Drawing.Point(151, 69);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(17, 20);
            this.label56.TabIndex = 34;
            this.label56.Text = "2";
            // 
            // textBox83
            // 
            this.textBox83.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBox83.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.textBox83.ForeColor = System.Drawing.Color.White;
            this.textBox83.Location = new System.Drawing.Point(188, 107);
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new System.Drawing.Size(64, 34);
            this.textBox83.TabIndex = 33;
            this.textBox83.Text = "미측정";
            // 
            // textBox84
            // 
            this.textBox84.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBox84.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.textBox84.ForeColor = System.Drawing.Color.White;
            this.textBox84.Location = new System.Drawing.Point(188, 59);
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new System.Drawing.Size(64, 34);
            this.textBox84.TabIndex = 32;
            this.textBox84.Text = "미측정";
            // 
            // textBox85
            // 
            this.textBox85.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBox85.Font = new System.Drawing.Font("Malgun Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.textBox85.Location = new System.Drawing.Point(166, 8);
            this.textBox85.Name = "textBox85";
            this.textBox85.Size = new System.Drawing.Size(212, 34);
            this.textBox85.TabIndex = 31;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.label57.ForeColor = System.Drawing.Color.White;
            this.label57.Location = new System.Drawing.Point(18, 213);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(100, 15);
            this.label57.TabIndex = 26;
            this.label57.Text = "X 이동간격 (mm)";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label58.ForeColor = System.Drawing.Color.White;
            this.label58.Location = new System.Drawing.Point(23, 163);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(17, 20);
            this.label58.TabIndex = 25;
            this.label58.Text = "7";
            // 
            // textBox86
            // 
            this.textBox86.BackColor = System.Drawing.Color.White;
            this.textBox86.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.textBox86.ForeColor = System.Drawing.Color.White;
            this.textBox86.Location = new System.Drawing.Point(134, 203);
            this.textBox86.Name = "textBox86";
            this.textBox86.Size = new System.Drawing.Size(64, 34);
            this.textBox86.TabIndex = 24;
            // 
            // textBox87
            // 
            this.textBox87.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBox87.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.textBox87.ForeColor = System.Drawing.Color.White;
            this.textBox87.Location = new System.Drawing.Point(60, 153);
            this.textBox87.Name = "textBox87";
            this.textBox87.Size = new System.Drawing.Size(64, 34);
            this.textBox87.TabIndex = 23;
            this.textBox87.Text = "미측정";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label59.ForeColor = System.Drawing.Color.White;
            this.label59.Location = new System.Drawing.Point(23, 116);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(17, 20);
            this.label59.TabIndex = 22;
            this.label59.Text = "4";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label60.ForeColor = System.Drawing.Color.White;
            this.label60.Location = new System.Drawing.Point(23, 69);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(17, 20);
            this.label60.TabIndex = 21;
            this.label60.Text = "1";
            // 
            // textBox88
            // 
            this.textBox88.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBox88.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.textBox88.ForeColor = System.Drawing.Color.White;
            this.textBox88.Location = new System.Drawing.Point(60, 107);
            this.textBox88.Name = "textBox88";
            this.textBox88.Size = new System.Drawing.Size(64, 34);
            this.textBox88.TabIndex = 20;
            this.textBox88.Text = "미측정";
            // 
            // textBox89
            // 
            this.textBox89.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBox89.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.textBox89.ForeColor = System.Drawing.Color.White;
            this.textBox89.Location = new System.Drawing.Point(60, 59);
            this.textBox89.Name = "textBox89";
            this.textBox89.Size = new System.Drawing.Size(64, 34);
            this.textBox89.TabIndex = 19;
            this.textBox89.Text = "미측정";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Malgun Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.label61.ForeColor = System.Drawing.Color.White;
            this.label61.Location = new System.Drawing.Point(17, 18);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(138, 20);
            this.label61.TabIndex = 18;
            this.label61.Text = "LDS 현재 값(mm) :";
            // 
            // textBox90
            // 
            this.textBox90.BackColor = System.Drawing.SystemColors.HotTrack;
            this.textBox90.Enabled = false;
            this.textBox90.Font = new System.Drawing.Font("Malgun Gothic", 18F, System.Drawing.FontStyle.Bold);
            this.textBox90.ForeColor = System.Drawing.Color.White;
            this.textBox90.Location = new System.Drawing.Point(3, 3);
            this.textBox90.Name = "textBox90";
            this.textBox90.Size = new System.Drawing.Size(909, 39);
            this.textBox90.TabIndex = 3;
            this.textBox90.Text = "LDS BREAK";
            this.textBox90.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.btn_Power_Close);
            this.panel25.Controls.Add(this.btn_Power_Open);
            this.panel25.Controls.Add(this.panel26);
            this.panel25.Controls.Add(this.textBox42);
            this.panel25.Location = new System.Drawing.Point(944, 6);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(915, 400);
            this.panel25.TabIndex = 56;
            // 
            // btn_Power_Close
            // 
            this.btn_Power_Close.BackColor = System.Drawing.Color.DimGray;
            this.btn_Power_Close.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Power_Close.ForeColor = System.Drawing.Color.White;
            this.btn_Power_Close.Location = new System.Drawing.Point(106, 48);
            this.btn_Power_Close.Name = "btn_Power_Close";
            this.btn_Power_Close.Size = new System.Drawing.Size(93, 40);
            this.btn_Power_Close.TabIndex = 10;
            this.btn_Power_Close.Text = "Close";
            this.btn_Power_Close.UseVisualStyleBackColor = false;
            // 
            // btn_Power_Open
            // 
            this.btn_Power_Open.BackColor = System.Drawing.Color.DimGray;
            this.btn_Power_Open.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Power_Open.ForeColor = System.Drawing.Color.White;
            this.btn_Power_Open.Location = new System.Drawing.Point(9, 48);
            this.btn_Power_Open.Name = "btn_Power_Open";
            this.btn_Power_Open.Size = new System.Drawing.Size(93, 40);
            this.btn_Power_Open.TabIndex = 9;
            this.btn_Power_Open.Text = "Open";
            this.btn_Power_Open.UseVisualStyleBackColor = false;
            // 
            // panel26
            // 
            this.panel26.Controls.Add(this.btn_Power_Get);
            this.panel26.Controls.Add(this.tbox_Power_Get);
            this.panel26.Location = new System.Drawing.Point(9, 104);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(894, 283);
            this.panel26.TabIndex = 5;
            // 
            // btn_Power_Get
            // 
            this.btn_Power_Get.BackColor = System.Drawing.Color.DimGray;
            this.btn_Power_Get.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Power_Get.ForeColor = System.Drawing.Color.White;
            this.btn_Power_Get.Location = new System.Drawing.Point(32, 106);
            this.btn_Power_Get.Name = "btn_Power_Get";
            this.btn_Power_Get.Size = new System.Drawing.Size(166, 81);
            this.btn_Power_Get.TabIndex = 29;
            this.btn_Power_Get.Text = "Get";
            this.btn_Power_Get.UseVisualStyleBackColor = false;
            // 
            // tbox_Power_Get
            // 
            this.tbox_Power_Get.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_Power_Get.Font = new System.Drawing.Font("Malgun Gothic", 40F, System.Drawing.FontStyle.Bold);
            this.tbox_Power_Get.Location = new System.Drawing.Point(289, 106);
            this.tbox_Power_Get.Name = "tbox_Power_Get";
            this.tbox_Power_Get.Size = new System.Drawing.Size(514, 78);
            this.tbox_Power_Get.TabIndex = 25;
            // 
            // textBox42
            // 
            this.textBox42.BackColor = System.Drawing.SystemColors.HotTrack;
            this.textBox42.Enabled = false;
            this.textBox42.Font = new System.Drawing.Font("Malgun Gothic", 18F, System.Drawing.FontStyle.Bold);
            this.textBox42.ForeColor = System.Drawing.Color.White;
            this.textBox42.Location = new System.Drawing.Point(3, 3);
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new System.Drawing.Size(909, 39);
            this.textBox42.TabIndex = 3;
            this.textBox42.Text = "PowerMeter";
            this.textBox42.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel27
            // 
            this.panel27.Controls.Add(this.label45);
            this.panel27.Controls.Add(this.label44);
            this.panel27.Controls.Add(this.tbox_LDS_Pro_TableIdx);
            this.panel27.Controls.Add(this.label43);
            this.panel27.Controls.Add(this.tbox_LDS_Pro_Measure);
            this.panel27.Controls.Add(this.label42);
            this.panel27.Controls.Add(this.button44);
            this.panel27.Controls.Add(this.tbox_LDS_pro_Standby);
            this.panel27.Controls.Add(this.btn_LDS_Process_Stop);
            this.panel27.Controls.Add(this.btn_LDS_Process_Start);
            this.panel27.Controls.Add(this.panel29);
            this.panel27.Controls.Add(this.textBox59);
            this.panel27.Location = new System.Drawing.Point(6, 412);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(915, 400);
            this.panel27.TabIndex = 57;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Malgun Gothic", 10F);
            this.label45.ForeColor = System.Drawing.Color.White;
            this.label45.Location = new System.Drawing.Point(597, 63);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(51, 19);
            this.label45.TabIndex = 53;
            this.label45.Text = "대기중";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.label44.ForeColor = System.Drawing.Color.White;
            this.label44.Location = new System.Drawing.Point(392, 63);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(90, 15);
            this.label44.TabIndex = 52;
            this.label44.Text = "테이블 인덱스 :";
            // 
            // tbox_LDS_Pro_TableIdx
            // 
            this.tbox_LDS_Pro_TableIdx.BackColor = System.Drawing.Color.White;
            this.tbox_LDS_Pro_TableIdx.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.tbox_LDS_Pro_TableIdx.ForeColor = System.Drawing.Color.White;
            this.tbox_LDS_Pro_TableIdx.Location = new System.Drawing.Point(503, 55);
            this.tbox_LDS_Pro_TableIdx.Name = "tbox_LDS_Pro_TableIdx";
            this.tbox_LDS_Pro_TableIdx.Size = new System.Drawing.Size(64, 34);
            this.tbox_LDS_Pro_TableIdx.TabIndex = 51;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.label43.ForeColor = System.Drawing.Color.White;
            this.label43.Location = new System.Drawing.Point(214, 63);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(72, 15);
            this.label43.TabIndex = 50;
            this.label43.Text = "측정 시간[s]";
            // 
            // tbox_LDS_Pro_Measure
            // 
            this.tbox_LDS_Pro_Measure.BackColor = System.Drawing.Color.White;
            this.tbox_LDS_Pro_Measure.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.tbox_LDS_Pro_Measure.ForeColor = System.Drawing.Color.White;
            this.tbox_LDS_Pro_Measure.Location = new System.Drawing.Point(307, 55);
            this.tbox_LDS_Pro_Measure.Name = "tbox_LDS_Pro_Measure";
            this.tbox_LDS_Pro_Measure.Size = new System.Drawing.Size(64, 34);
            this.tbox_LDS_Pro_Measure.TabIndex = 49;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.label42.ForeColor = System.Drawing.Color.White;
            this.label42.Location = new System.Drawing.Point(36, 63);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(72, 15);
            this.label42.TabIndex = 48;
            this.label42.Text = "대기 시간[s]";
            // 
            // button44
            // 
            this.button44.BackColor = System.Drawing.Color.DimGray;
            this.button44.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.button44.ForeColor = System.Drawing.Color.White;
            this.button44.Location = new System.Drawing.Point(791, 359);
            this.button44.Name = "button44";
            this.button44.Size = new System.Drawing.Size(114, 32);
            this.button44.TabIndex = 30;
            this.button44.Text = "Save";
            this.button44.UseVisualStyleBackColor = false;
            // 
            // tbox_LDS_pro_Standby
            // 
            this.tbox_LDS_pro_Standby.BackColor = System.Drawing.Color.White;
            this.tbox_LDS_pro_Standby.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.tbox_LDS_pro_Standby.ForeColor = System.Drawing.Color.White;
            this.tbox_LDS_pro_Standby.Location = new System.Drawing.Point(129, 55);
            this.tbox_LDS_pro_Standby.Name = "tbox_LDS_pro_Standby";
            this.tbox_LDS_pro_Standby.Size = new System.Drawing.Size(64, 34);
            this.tbox_LDS_pro_Standby.TabIndex = 47;
            // 
            // btn_LDS_Process_Stop
            // 
            this.btn_LDS_Process_Stop.BackColor = System.Drawing.Color.DimGray;
            this.btn_LDS_Process_Stop.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_LDS_Process_Stop.ForeColor = System.Drawing.Color.White;
            this.btn_LDS_Process_Stop.Location = new System.Drawing.Point(767, 49);
            this.btn_LDS_Process_Stop.Name = "btn_LDS_Process_Stop";
            this.btn_LDS_Process_Stop.Size = new System.Drawing.Size(93, 40);
            this.btn_LDS_Process_Stop.TabIndex = 14;
            this.btn_LDS_Process_Stop.Text = "측정 정지";
            this.btn_LDS_Process_Stop.UseVisualStyleBackColor = false;
            // 
            // btn_LDS_Process_Start
            // 
            this.btn_LDS_Process_Start.BackColor = System.Drawing.Color.DimGray;
            this.btn_LDS_Process_Start.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_LDS_Process_Start.ForeColor = System.Drawing.Color.White;
            this.btn_LDS_Process_Start.Location = new System.Drawing.Point(670, 49);
            this.btn_LDS_Process_Start.Name = "btn_LDS_Process_Start";
            this.btn_LDS_Process_Start.Size = new System.Drawing.Size(93, 40);
            this.btn_LDS_Process_Start.TabIndex = 13;
            this.btn_LDS_Process_Start.Text = "측정 시작";
            this.btn_LDS_Process_Start.UseVisualStyleBackColor = false;
            // 
            // panel29
            // 
            this.panel29.Controls.Add(this.listView1);
            this.panel29.Controls.Add(this.label41);
            this.panel29.Controls.Add(this.textBox50);
            this.panel29.Controls.Add(this.label20);
            this.panel29.Controls.Add(this.textBox47);
            this.panel29.Controls.Add(this.label21);
            this.panel29.Controls.Add(this.label40);
            this.panel29.Controls.Add(this.textBox48);
            this.panel29.Controls.Add(this.textBox49);
            this.panel29.Controls.Add(this.label17);
            this.panel29.Controls.Add(this.textBox44);
            this.panel29.Controls.Add(this.label18);
            this.panel29.Controls.Add(this.label19);
            this.panel29.Controls.Add(this.textBox45);
            this.panel29.Controls.Add(this.textBox46);
            this.panel29.Controls.Add(this.tbox_LDS_CurrendValue);
            this.panel29.Controls.Add(this.label22);
            this.panel29.Controls.Add(this.label23);
            this.panel29.Controls.Add(this.tbox_);
            this.panel29.Controls.Add(this.textBox56);
            this.panel29.Controls.Add(this.label24);
            this.panel29.Controls.Add(this.label25);
            this.panel29.Controls.Add(this.textBox57);
            this.panel29.Controls.Add(this.textBox58);
            this.panel29.Controls.Add(this.label26);
            this.panel29.Location = new System.Drawing.Point(8, 99);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(897, 254);
            this.panel29.TabIndex = 9;
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.listView1.Font = new System.Drawing.Font("Malgun Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.listView1.Location = new System.Drawing.Point(420, 29);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(463, 222);
            this.listView1.TabIndex = 46;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "인덱스";
            this.columnHeader1.Width = 134;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "측정값";
            this.columnHeader2.Width = 121;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "차이값";
            this.columnHeader3.Width = 130;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.label41.ForeColor = System.Drawing.Color.White;
            this.label41.Location = new System.Drawing.Point(209, 215);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(100, 15);
            this.label41.TabIndex = 45;
            this.label41.Text = "Y 이동간격 (mm)";
            // 
            // textBox50
            // 
            this.textBox50.BackColor = System.Drawing.Color.White;
            this.textBox50.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.textBox50.ForeColor = System.Drawing.Color.White;
            this.textBox50.Location = new System.Drawing.Point(314, 203);
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new System.Drawing.Size(64, 34);
            this.textBox50.TabIndex = 44;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(277, 162);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(17, 20);
            this.label20.TabIndex = 43;
            this.label20.Text = "9";
            // 
            // textBox47
            // 
            this.textBox47.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBox47.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.textBox47.ForeColor = System.Drawing.Color.White;
            this.textBox47.Location = new System.Drawing.Point(314, 152);
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new System.Drawing.Size(64, 34);
            this.textBox47.TabIndex = 42;
            this.textBox47.Text = "미측정";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(277, 115);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(17, 20);
            this.label21.TabIndex = 41;
            this.label21.Text = "6";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label40.ForeColor = System.Drawing.Color.White;
            this.label40.Location = new System.Drawing.Point(277, 68);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(17, 20);
            this.label40.TabIndex = 40;
            this.label40.Text = "3";
            // 
            // textBox48
            // 
            this.textBox48.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBox48.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.textBox48.ForeColor = System.Drawing.Color.White;
            this.textBox48.Location = new System.Drawing.Point(314, 106);
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new System.Drawing.Size(64, 34);
            this.textBox48.TabIndex = 39;
            this.textBox48.Text = "미측정";
            // 
            // textBox49
            // 
            this.textBox49.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBox49.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.textBox49.ForeColor = System.Drawing.Color.White;
            this.textBox49.Location = new System.Drawing.Point(314, 58);
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new System.Drawing.Size(64, 34);
            this.textBox49.TabIndex = 38;
            this.textBox49.Text = "미측정";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(151, 163);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(17, 20);
            this.label17.TabIndex = 37;
            this.label17.Text = "8";
            // 
            // textBox44
            // 
            this.textBox44.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBox44.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.textBox44.ForeColor = System.Drawing.Color.White;
            this.textBox44.Location = new System.Drawing.Point(188, 153);
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new System.Drawing.Size(64, 34);
            this.textBox44.TabIndex = 36;
            this.textBox44.Text = "미측정";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(151, 116);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(17, 20);
            this.label18.TabIndex = 35;
            this.label18.Text = "5";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(151, 69);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(17, 20);
            this.label19.TabIndex = 34;
            this.label19.Text = "2";
            // 
            // textBox45
            // 
            this.textBox45.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBox45.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.textBox45.ForeColor = System.Drawing.Color.White;
            this.textBox45.Location = new System.Drawing.Point(188, 107);
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new System.Drawing.Size(64, 34);
            this.textBox45.TabIndex = 33;
            this.textBox45.Text = "미측정";
            // 
            // textBox46
            // 
            this.textBox46.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBox46.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.textBox46.ForeColor = System.Drawing.Color.White;
            this.textBox46.Location = new System.Drawing.Point(188, 59);
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new System.Drawing.Size(64, 34);
            this.textBox46.TabIndex = 32;
            this.textBox46.Text = "미측정";
            // 
            // tbox_LDS_CurrendValue
            // 
            this.tbox_LDS_CurrendValue.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_LDS_CurrendValue.Font = new System.Drawing.Font("Malgun Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.tbox_LDS_CurrendValue.Location = new System.Drawing.Point(166, 8);
            this.tbox_LDS_CurrendValue.Name = "tbox_LDS_CurrendValue";
            this.tbox_LDS_CurrendValue.Size = new System.Drawing.Size(212, 34);
            this.tbox_LDS_CurrendValue.TabIndex = 31;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.label22.ForeColor = System.Drawing.Color.White;
            this.label22.Location = new System.Drawing.Point(18, 213);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(100, 15);
            this.label22.TabIndex = 26;
            this.label22.Text = "X 이동간격 (mm)";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label23.ForeColor = System.Drawing.Color.White;
            this.label23.Location = new System.Drawing.Point(23, 163);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(17, 20);
            this.label23.TabIndex = 25;
            this.label23.Text = "7";
            // 
            // tbox_
            // 
            this.tbox_.BackColor = System.Drawing.Color.White;
            this.tbox_.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.tbox_.ForeColor = System.Drawing.Color.White;
            this.tbox_.Location = new System.Drawing.Point(134, 203);
            this.tbox_.Name = "tbox_";
            this.tbox_.Size = new System.Drawing.Size(64, 34);
            this.tbox_.TabIndex = 24;
            // 
            // textBox56
            // 
            this.textBox56.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBox56.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.textBox56.ForeColor = System.Drawing.Color.White;
            this.textBox56.Location = new System.Drawing.Point(60, 153);
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new System.Drawing.Size(64, 34);
            this.textBox56.TabIndex = 23;
            this.textBox56.Text = "미측정";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label24.ForeColor = System.Drawing.Color.White;
            this.label24.Location = new System.Drawing.Point(23, 116);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(17, 20);
            this.label24.TabIndex = 22;
            this.label24.Text = "4";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label25.ForeColor = System.Drawing.Color.White;
            this.label25.Location = new System.Drawing.Point(23, 69);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(17, 20);
            this.label25.TabIndex = 21;
            this.label25.Text = "1";
            // 
            // textBox57
            // 
            this.textBox57.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBox57.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.textBox57.ForeColor = System.Drawing.Color.White;
            this.textBox57.Location = new System.Drawing.Point(60, 107);
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new System.Drawing.Size(64, 34);
            this.textBox57.TabIndex = 20;
            this.textBox57.Text = "미측정";
            // 
            // textBox58
            // 
            this.textBox58.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBox58.Font = new System.Drawing.Font("Malgun Gothic", 15F);
            this.textBox58.ForeColor = System.Drawing.Color.White;
            this.textBox58.Location = new System.Drawing.Point(60, 59);
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new System.Drawing.Size(64, 34);
            this.textBox58.TabIndex = 19;
            this.textBox58.Text = "미측정";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Malgun Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.label26.ForeColor = System.Drawing.Color.White;
            this.label26.Location = new System.Drawing.Point(17, 18);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(138, 20);
            this.label26.TabIndex = 18;
            this.label26.Text = "LDS 현재 값(mm) :";
            // 
            // textBox59
            // 
            this.textBox59.BackColor = System.Drawing.SystemColors.HotTrack;
            this.textBox59.Enabled = false;
            this.textBox59.Font = new System.Drawing.Font("Malgun Gothic", 18F, System.Drawing.FontStyle.Bold);
            this.textBox59.ForeColor = System.Drawing.Color.White;
            this.textBox59.Location = new System.Drawing.Point(3, 3);
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new System.Drawing.Size(909, 39);
            this.textBox59.TabIndex = 3;
            this.textBox59.Text = "LDS PROCESS";
            this.textBox59.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel30
            // 
            this.panel30.Controls.Add(this.btn_Beam_Close);
            this.panel30.Controls.Add(this.btn_Beam_Open);
            this.panel30.Controls.Add(this.panel31);
            this.panel30.Controls.Add(this.panel32);
            this.panel30.Controls.Add(this.textBox64);
            this.panel30.Location = new System.Drawing.Point(6, 6);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(915, 400);
            this.panel30.TabIndex = 55;
            // 
            // btn_Beam_Close
            // 
            this.btn_Beam_Close.BackColor = System.Drawing.Color.DimGray;
            this.btn_Beam_Close.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Beam_Close.ForeColor = System.Drawing.Color.White;
            this.btn_Beam_Close.Location = new System.Drawing.Point(102, 48);
            this.btn_Beam_Close.Name = "btn_Beam_Close";
            this.btn_Beam_Close.Size = new System.Drawing.Size(93, 40);
            this.btn_Beam_Close.TabIndex = 8;
            this.btn_Beam_Close.Text = "Close";
            this.btn_Beam_Close.UseVisualStyleBackColor = false;
            // 
            // btn_Beam_Open
            // 
            this.btn_Beam_Open.BackColor = System.Drawing.Color.DimGray;
            this.btn_Beam_Open.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Beam_Open.ForeColor = System.Drawing.Color.White;
            this.btn_Beam_Open.Location = new System.Drawing.Point(5, 48);
            this.btn_Beam_Open.Name = "btn_Beam_Open";
            this.btn_Beam_Open.Size = new System.Drawing.Size(93, 40);
            this.btn_Beam_Open.TabIndex = 7;
            this.btn_Beam_Open.Text = "Open";
            this.btn_Beam_Open.UseVisualStyleBackColor = false;
            // 
            // panel31
            // 
            this.panel31.Controls.Add(this.label39);
            this.panel31.Controls.Add(this.label37);
            this.panel31.Controls.Add(this.tbox_Error_Pos);
            this.panel31.Controls.Add(this.label38);
            this.panel31.Controls.Add(this.tbox_Teaching_Y);
            this.panel31.Controls.Add(this.label27);
            this.panel31.Controls.Add(this.tbox_Error_Power);
            this.panel31.Controls.Add(this.label28);
            this.panel31.Controls.Add(this.label29);
            this.panel31.Controls.Add(this.tbox_Teaching_X);
            this.panel31.Controls.Add(this.tbox_Teaching_Power);
            this.panel31.Controls.Add(this.btn_Beam_Save);
            this.panel31.Location = new System.Drawing.Point(466, 105);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(439, 283);
            this.panel31.TabIndex = 4;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label39.ForeColor = System.Drawing.Color.White;
            this.label39.Location = new System.Drawing.Point(39, 23);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(104, 20);
            this.label39.TabIndex = 44;
            this.label39.Text = "Limit Val 설정";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Malgun Gothic", 10F);
            this.label37.ForeColor = System.Drawing.Color.White;
            this.label37.Location = new System.Drawing.Point(239, 165);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(105, 19);
            this.label37.TabIndex = 43;
            this.label37.Text = "에러 범위 POS:";
            // 
            // tbox_Error_Pos
            // 
            this.tbox_Error_Pos.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_Error_Pos.Font = new System.Drawing.Font("Malgun Gothic", 20F, System.Drawing.FontStyle.Bold);
            this.tbox_Error_Pos.Location = new System.Drawing.Point(346, 155);
            this.tbox_Error_Pos.Name = "tbox_Error_Pos";
            this.tbox_Error_Pos.Size = new System.Drawing.Size(70, 43);
            this.tbox_Error_Pos.TabIndex = 42;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Malgun Gothic", 10F);
            this.label38.ForeColor = System.Drawing.Color.White;
            this.label38.Location = new System.Drawing.Point(239, 116);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(53, 19);
            this.label38.TabIndex = 41;
            this.label38.Text = "티칭 Y:";
            // 
            // tbox_Teaching_Y
            // 
            this.tbox_Teaching_Y.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_Teaching_Y.Font = new System.Drawing.Font("Malgun Gothic", 20F, System.Drawing.FontStyle.Bold);
            this.tbox_Teaching_Y.Location = new System.Drawing.Point(346, 106);
            this.tbox_Teaching_Y.Name = "tbox_Teaching_Y";
            this.tbox_Teaching_Y.Size = new System.Drawing.Size(70, 43);
            this.tbox_Teaching_Y.TabIndex = 40;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Malgun Gothic", 10F);
            this.label27.ForeColor = System.Drawing.Color.White;
            this.label27.Location = new System.Drawing.Point(18, 164);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(123, 19);
            this.label27.TabIndex = 39;
            this.label27.Text = "에러 범위 POWER";
            // 
            // tbox_Error_Power
            // 
            this.tbox_Error_Power.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_Error_Power.Font = new System.Drawing.Font("Malgun Gothic", 20F, System.Drawing.FontStyle.Bold);
            this.tbox_Error_Power.Location = new System.Drawing.Point(159, 154);
            this.tbox_Error_Power.Name = "tbox_Error_Power";
            this.tbox_Error_Power.Size = new System.Drawing.Size(70, 43);
            this.tbox_Error_Power.TabIndex = 38;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Malgun Gothic", 10F);
            this.label28.ForeColor = System.Drawing.Color.White;
            this.label28.Location = new System.Drawing.Point(18, 115);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(53, 19);
            this.label28.TabIndex = 37;
            this.label28.Text = "티칭 X:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Malgun Gothic", 10F);
            this.label29.ForeColor = System.Drawing.Color.White;
            this.label29.Location = new System.Drawing.Point(18, 64);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(83, 19);
            this.label29.TabIndex = 36;
            this.label29.Text = "티칭 Power:";
            // 
            // tbox_Teaching_X
            // 
            this.tbox_Teaching_X.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_Teaching_X.Font = new System.Drawing.Font("Malgun Gothic", 20F, System.Drawing.FontStyle.Bold);
            this.tbox_Teaching_X.Location = new System.Drawing.Point(159, 105);
            this.tbox_Teaching_X.Name = "tbox_Teaching_X";
            this.tbox_Teaching_X.Size = new System.Drawing.Size(70, 43);
            this.tbox_Teaching_X.TabIndex = 35;
            // 
            // tbox_Teaching_Power
            // 
            this.tbox_Teaching_Power.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_Teaching_Power.Font = new System.Drawing.Font("Malgun Gothic", 20F, System.Drawing.FontStyle.Bold);
            this.tbox_Teaching_Power.Location = new System.Drawing.Point(159, 57);
            this.tbox_Teaching_Power.Name = "tbox_Teaching_Power";
            this.tbox_Teaching_Power.Size = new System.Drawing.Size(70, 43);
            this.tbox_Teaching_Power.TabIndex = 34;
            // 
            // btn_Beam_Save
            // 
            this.btn_Beam_Save.BackColor = System.Drawing.Color.DimGray;
            this.btn_Beam_Save.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Beam_Save.ForeColor = System.Drawing.Color.White;
            this.btn_Beam_Save.Location = new System.Drawing.Point(316, 219);
            this.btn_Beam_Save.Name = "btn_Beam_Save";
            this.btn_Beam_Save.Size = new System.Drawing.Size(99, 43);
            this.btn_Beam_Save.TabIndex = 29;
            this.btn_Beam_Save.Text = "Save";
            this.btn_Beam_Save.UseVisualStyleBackColor = false;
            // 
            // panel32
            // 
            this.panel32.Controls.Add(this.tableLayoutPanel2);
            this.panel32.Controls.Add(this.label36);
            this.panel32.Controls.Add(this.label35);
            this.panel32.Controls.Add(this.label34);
            this.panel32.Controls.Add(this.label30);
            this.panel32.Controls.Add(this.label31);
            this.panel32.Controls.Add(this.label32);
            this.panel32.Controls.Add(this.label33);
            this.panel32.Location = new System.Drawing.Point(8, 104);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(442, 283);
            this.panel32.TabIndex = 3;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Controls.Add(this.tbox_Beam_PosY_MedianValue, 3, 2);
            this.tableLayoutPanel2.Controls.Add(this.tbox_Beam_PosY_MaxValue, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.tbox_Beam_PosX_MedianValue, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.tbox_Beam_PosX_MaxValue, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.tbox_Beam_Power_MedianValue, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.tbox_Beam_Power_MaxValue, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.tbox_Beam_PosY_CurrenValue, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.tbox_Beam_PosY_MiniValue, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.tbox_Beam_Power_CurrenValue, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tbox_Beam_Power_MiniValue, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.tbox_Beam_PosX_CurrenValue, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.tbox_Beam_PosX_MiniValue, 1, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(119, 67);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(289, 134);
            this.tableLayoutPanel2.TabIndex = 47;
            // 
            // tbox_Beam_PosY_MedianValue
            // 
            this.tbox_Beam_PosY_MedianValue.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_Beam_PosY_MedianValue.Font = new System.Drawing.Font("Malgun Gothic", 20F, System.Drawing.FontStyle.Bold);
            this.tbox_Beam_PosY_MedianValue.Location = new System.Drawing.Point(219, 91);
            this.tbox_Beam_PosY_MedianValue.Name = "tbox_Beam_PosY_MedianValue";
            this.tbox_Beam_PosY_MedianValue.Size = new System.Drawing.Size(66, 43);
            this.tbox_Beam_PosY_MedianValue.TabIndex = 40;
            // 
            // tbox_Beam_PosY_MaxValue
            // 
            this.tbox_Beam_PosY_MaxValue.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_Beam_PosY_MaxValue.Font = new System.Drawing.Font("Malgun Gothic", 20F, System.Drawing.FontStyle.Bold);
            this.tbox_Beam_PosY_MaxValue.Location = new System.Drawing.Point(147, 91);
            this.tbox_Beam_PosY_MaxValue.Name = "tbox_Beam_PosY_MaxValue";
            this.tbox_Beam_PosY_MaxValue.Size = new System.Drawing.Size(66, 43);
            this.tbox_Beam_PosY_MaxValue.TabIndex = 39;
            // 
            // tbox_Beam_PosX_MedianValue
            // 
            this.tbox_Beam_PosX_MedianValue.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_Beam_PosX_MedianValue.Font = new System.Drawing.Font("Malgun Gothic", 20F, System.Drawing.FontStyle.Bold);
            this.tbox_Beam_PosX_MedianValue.Location = new System.Drawing.Point(219, 47);
            this.tbox_Beam_PosX_MedianValue.Name = "tbox_Beam_PosX_MedianValue";
            this.tbox_Beam_PosX_MedianValue.Size = new System.Drawing.Size(66, 43);
            this.tbox_Beam_PosX_MedianValue.TabIndex = 38;
            // 
            // tbox_Beam_PosX_MaxValue
            // 
            this.tbox_Beam_PosX_MaxValue.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_Beam_PosX_MaxValue.Font = new System.Drawing.Font("Malgun Gothic", 20F, System.Drawing.FontStyle.Bold);
            this.tbox_Beam_PosX_MaxValue.Location = new System.Drawing.Point(147, 47);
            this.tbox_Beam_PosX_MaxValue.Name = "tbox_Beam_PosX_MaxValue";
            this.tbox_Beam_PosX_MaxValue.Size = new System.Drawing.Size(66, 43);
            this.tbox_Beam_PosX_MaxValue.TabIndex = 37;
            // 
            // tbox_Beam_Power_MedianValue
            // 
            this.tbox_Beam_Power_MedianValue.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_Beam_Power_MedianValue.Font = new System.Drawing.Font("Malgun Gothic", 20F, System.Drawing.FontStyle.Bold);
            this.tbox_Beam_Power_MedianValue.Location = new System.Drawing.Point(219, 3);
            this.tbox_Beam_Power_MedianValue.Name = "tbox_Beam_Power_MedianValue";
            this.tbox_Beam_Power_MedianValue.Size = new System.Drawing.Size(66, 43);
            this.tbox_Beam_Power_MedianValue.TabIndex = 36;
            // 
            // tbox_Beam_Power_MaxValue
            // 
            this.tbox_Beam_Power_MaxValue.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_Beam_Power_MaxValue.Font = new System.Drawing.Font("Malgun Gothic", 20F, System.Drawing.FontStyle.Bold);
            this.tbox_Beam_Power_MaxValue.Location = new System.Drawing.Point(147, 3);
            this.tbox_Beam_Power_MaxValue.Name = "tbox_Beam_Power_MaxValue";
            this.tbox_Beam_Power_MaxValue.Size = new System.Drawing.Size(66, 43);
            this.tbox_Beam_Power_MaxValue.TabIndex = 35;
            // 
            // tbox_Beam_PosY_CurrenValue
            // 
            this.tbox_Beam_PosY_CurrenValue.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_Beam_PosY_CurrenValue.Font = new System.Drawing.Font("Malgun Gothic", 20F, System.Drawing.FontStyle.Bold);
            this.tbox_Beam_PosY_CurrenValue.Location = new System.Drawing.Point(3, 91);
            this.tbox_Beam_PosY_CurrenValue.Name = "tbox_Beam_PosY_CurrenValue";
            this.tbox_Beam_PosY_CurrenValue.Size = new System.Drawing.Size(66, 43);
            this.tbox_Beam_PosY_CurrenValue.TabIndex = 30;
            // 
            // tbox_Beam_PosY_MiniValue
            // 
            this.tbox_Beam_PosY_MiniValue.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_Beam_PosY_MiniValue.Font = new System.Drawing.Font("Malgun Gothic", 20F, System.Drawing.FontStyle.Bold);
            this.tbox_Beam_PosY_MiniValue.Location = new System.Drawing.Point(75, 91);
            this.tbox_Beam_PosY_MiniValue.Name = "tbox_Beam_PosY_MiniValue";
            this.tbox_Beam_PosY_MiniValue.Size = new System.Drawing.Size(66, 43);
            this.tbox_Beam_PosY_MiniValue.TabIndex = 29;
            // 
            // tbox_Beam_Power_CurrenValue
            // 
            this.tbox_Beam_Power_CurrenValue.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_Beam_Power_CurrenValue.Font = new System.Drawing.Font("Malgun Gothic", 20F, System.Drawing.FontStyle.Bold);
            this.tbox_Beam_Power_CurrenValue.Location = new System.Drawing.Point(3, 3);
            this.tbox_Beam_Power_CurrenValue.Name = "tbox_Beam_Power_CurrenValue";
            this.tbox_Beam_Power_CurrenValue.Size = new System.Drawing.Size(66, 43);
            this.tbox_Beam_Power_CurrenValue.TabIndex = 28;
            // 
            // tbox_Beam_Power_MiniValue
            // 
            this.tbox_Beam_Power_MiniValue.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_Beam_Power_MiniValue.Font = new System.Drawing.Font("Malgun Gothic", 20F, System.Drawing.FontStyle.Bold);
            this.tbox_Beam_Power_MiniValue.Location = new System.Drawing.Point(75, 3);
            this.tbox_Beam_Power_MiniValue.Name = "tbox_Beam_Power_MiniValue";
            this.tbox_Beam_Power_MiniValue.Size = new System.Drawing.Size(66, 43);
            this.tbox_Beam_Power_MiniValue.TabIndex = 27;
            // 
            // tbox_Beam_PosX_CurrenValue
            // 
            this.tbox_Beam_PosX_CurrenValue.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_Beam_PosX_CurrenValue.Font = new System.Drawing.Font("Malgun Gothic", 20F, System.Drawing.FontStyle.Bold);
            this.tbox_Beam_PosX_CurrenValue.Location = new System.Drawing.Point(3, 47);
            this.tbox_Beam_PosX_CurrenValue.Name = "tbox_Beam_PosX_CurrenValue";
            this.tbox_Beam_PosX_CurrenValue.Size = new System.Drawing.Size(66, 43);
            this.tbox_Beam_PosX_CurrenValue.TabIndex = 31;
            // 
            // tbox_Beam_PosX_MiniValue
            // 
            this.tbox_Beam_PosX_MiniValue.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tbox_Beam_PosX_MiniValue.Font = new System.Drawing.Font("Malgun Gothic", 20F, System.Drawing.FontStyle.Bold);
            this.tbox_Beam_PosX_MiniValue.Location = new System.Drawing.Point(75, 47);
            this.tbox_Beam_PosX_MiniValue.Name = "tbox_Beam_PosX_MiniValue";
            this.tbox_Beam_PosX_MiniValue.Size = new System.Drawing.Size(66, 43);
            this.tbox_Beam_PosX_MiniValue.TabIndex = 34;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label36.ForeColor = System.Drawing.Color.White;
            this.label36.Location = new System.Drawing.Point(354, 26);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(54, 20);
            this.label36.TabIndex = 46;
            this.label36.Text = "중간값";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label35.ForeColor = System.Drawing.Color.White;
            this.label35.Location = new System.Drawing.Point(276, 26);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(54, 20);
            this.label35.TabIndex = 45;
            this.label35.Text = "최대값";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label34.ForeColor = System.Drawing.Color.White;
            this.label34.Location = new System.Drawing.Point(199, 26);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(54, 20);
            this.label34.TabIndex = 44;
            this.label34.Text = "최소값";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label30.ForeColor = System.Drawing.Color.White;
            this.label30.Location = new System.Drawing.Point(121, 26);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(54, 20);
            this.label30.TabIndex = 43;
            this.label30.Text = "현재값";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label31.ForeColor = System.Drawing.Color.White;
            this.label31.Location = new System.Drawing.Point(26, 167);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(73, 20);
            this.label31.TabIndex = 33;
            this.label31.Text = "PosY(um)";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label32.ForeColor = System.Drawing.Color.White;
            this.label32.Location = new System.Drawing.Point(26, 118);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(74, 20);
            this.label32.TabIndex = 30;
            this.label32.Text = "PosX(um)";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Malgun Gothic", 11F);
            this.label33.ForeColor = System.Drawing.Color.White;
            this.label33.Location = new System.Drawing.Point(26, 67);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(85, 20);
            this.label33.TabIndex = 29;
            this.label33.Text = "power(mw)";
            // 
            // textBox64
            // 
            this.textBox64.BackColor = System.Drawing.SystemColors.HotTrack;
            this.textBox64.Enabled = false;
            this.textBox64.Font = new System.Drawing.Font("Malgun Gothic", 18F, System.Drawing.FontStyle.Bold);
            this.textBox64.ForeColor = System.Drawing.Color.White;
            this.textBox64.Location = new System.Drawing.Point(3, 3);
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new System.Drawing.Size(909, 39);
            this.textBox64.TabIndex = 2;
            this.textBox64.Text = "BeamPositionner";
            this.textBox64.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Measure
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.Controls.Add(this.tc_iostatus_info);
            this.Name = "Measure";
            this.Size = new System.Drawing.Size(1880, 875);
            this.tc_iostatus_info.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.panel28.ResumeLayout(false);
            this.panel28.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            this.panel25.ResumeLayout(false);
            this.panel25.PerformLayout();
            this.panel26.ResumeLayout(false);
            this.panel26.PerformLayout();
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            this.panel29.ResumeLayout(false);
            this.panel29.PerformLayout();
            this.panel30.ResumeLayout(false);
            this.panel30.PerformLayout();
            this.panel31.ResumeLayout(false);
            this.panel31.PerformLayout();
            this.panel32.ResumeLayout(false);
            this.panel32.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tc_iostatus_info;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button button49;
        private System.Windows.Forms.Button button43;
        private System.Windows.Forms.Button button42;
        private System.Windows.Forms.Button button48;
        private System.Windows.Forms.TextBox textBox92;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btn_Mcr_B_Close;
        private System.Windows.Forms.Button btn_Mcr_B_Open;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Button btn_Mcr_B_Get;
        private System.Windows.Forms.TextBox tbox_Mcr_B;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btn_Mcr_A_Close;
        private System.Windows.Forms.Button btn_Mcr_A_Open;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Button btn_Mcr_A_Get;
        private System.Windows.Forms.TextBox tbox_Mcr_A;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btn_Tilt_ULd_Close;
        private System.Windows.Forms.Button btn_Tilt_ULd_Open;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.TextBox tbox_ULd_Check4;
        private System.Windows.Forms.TextBox tbox_ULd_Check3;
        private System.Windows.Forms.TextBox tbox_ULd_Check2;
        private System.Windows.Forms.TextBox tbox_ULd_Check1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbox_ULd_Position4;
        private System.Windows.Forms.TextBox tbox_ULd_Position3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tbox_ULd_Position2;
        private System.Windows.Forms.TextBox tbox_ULd_Position1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btn_Tilt_Ld_Close;
        private System.Windows.Forms.Button btn_Tilt_Ld_Open;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TextBox tbox_Ld_Check4;
        private System.Windows.Forms.TextBox tbox_Ld_Check3;
        private System.Windows.Forms.TextBox tbox_Ld_Check2;
        private System.Windows.Forms.TextBox tbox_Ld_Check1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tbox_Ld_Position4;
        private System.Windows.Forms.TextBox tbox_Ld_Position3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbox_Ld_Position2;
        private System.Windows.Forms.TextBox tbox_Ld_Position1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_Controller_Save;
        private System.Windows.Forms.Button btn_Controller_Close;
        private System.Windows.Forms.Button btn_Controller_Open;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btn_2ch_Set;
        private System.Windows.Forms.Button btn_1ch_Set;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbox_2ch_pr_bright_set;
        private System.Windows.Forms.TextBox tbox_1ch_pr_bright_set;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbox_2ch_bright_value;
        private System.Windows.Forms.TextBox tbox_1ch_bright_value;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Button btn_ULd_HandyBacord_Close;
        private System.Windows.Forms.Button btn_ULd_HandyBacord_Open;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Button btn_ULd_Handy_Get;
        private System.Windows.Forms.TextBox tbox_ULd_HandyBarcode;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Button btn_ULd_Barcode_B_Close;
        private System.Windows.Forms.Button btn_ULd_Barcode_B_Open;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.TextBox tbox_ULd_Barcode_B;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Button btn_Ld_Barcode_B_Close;
        private System.Windows.Forms.Button btn_Ld_Barcode_B_Open;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.TextBox tbox_Ld_Barcode_B;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Button btn_Ld_HandyBacord_Close;
        private System.Windows.Forms.Button btn_Ld_HandyBacord_Open;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Button btn_Ld_Handy_Get;
        private System.Windows.Forms.TextBox tbox_Ld_HandyBarcode;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Button btn_ULd_Barcode_A_Close;
        private System.Windows.Forms.Button btn_ULd_Barcode_A_Open;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.TextBox tbox_ULd_Barcode_A;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Button btn_Ld_Barcode_A_Close;
        private System.Windows.Forms.Button btn_Ld_Barcode_A_Open;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.TextBox tbox_Ld_Barcode_A;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox tbox_LDS_Break_TableIdx;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox tbox_LDS_Break_Measure;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.TextBox tbox_LDS_Break_Standby;
        private System.Windows.Forms.Button btn_LDS_Break_Stop;
        private System.Windows.Forms.Button btn_LDS_Break_Start;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox textBox78;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox textBox79;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox textBox80;
        private System.Windows.Forms.TextBox textBox81;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox textBox82;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox textBox83;
        private System.Windows.Forms.TextBox textBox84;
        private System.Windows.Forms.TextBox textBox85;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox textBox86;
        private System.Windows.Forms.TextBox textBox87;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.TextBox textBox88;
        private System.Windows.Forms.TextBox textBox89;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox textBox90;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Button btn_Power_Close;
        private System.Windows.Forms.Button btn_Power_Open;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Button btn_Power_Get;
        private System.Windows.Forms.TextBox tbox_Power_Get;
        private System.Windows.Forms.TextBox textBox42;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox tbox_LDS_Pro_TableIdx;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox tbox_LDS_Pro_Measure;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Button button44;
        private System.Windows.Forms.TextBox tbox_LDS_pro_Standby;
        private System.Windows.Forms.Button btn_LDS_Process_Stop;
        private System.Windows.Forms.Button btn_LDS_Process_Start;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox textBox50;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBox47;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox textBox48;
        private System.Windows.Forms.TextBox textBox49;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox44;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBox45;
        private System.Windows.Forms.TextBox textBox46;
        private System.Windows.Forms.TextBox tbox_LDS_CurrendValue;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox tbox_;
        private System.Windows.Forms.TextBox textBox56;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textBox57;
        private System.Windows.Forms.TextBox textBox58;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox textBox59;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Button btn_Beam_Close;
        private System.Windows.Forms.Button btn_Beam_Open;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox tbox_Error_Pos;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox tbox_Teaching_Y;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox tbox_Error_Power;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox tbox_Teaching_X;
        private System.Windows.Forms.TextBox tbox_Teaching_Power;
        private System.Windows.Forms.Button btn_Beam_Save;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TextBox tbox_Beam_PosY_MedianValue;
        private System.Windows.Forms.TextBox tbox_Beam_PosY_MaxValue;
        private System.Windows.Forms.TextBox tbox_Beam_PosX_MedianValue;
        private System.Windows.Forms.TextBox tbox_Beam_PosX_MaxValue;
        private System.Windows.Forms.TextBox tbox_Beam_Power_MedianValue;
        private System.Windows.Forms.TextBox tbox_Beam_Power_MaxValue;
        private System.Windows.Forms.TextBox tbox_Beam_PosY_CurrenValue;
        private System.Windows.Forms.TextBox tbox_Beam_PosY_MiniValue;
        private System.Windows.Forms.TextBox tbox_Beam_Power_CurrenValue;
        private System.Windows.Forms.TextBox tbox_Beam_Power_MiniValue;
        private System.Windows.Forms.TextBox tbox_Beam_PosX_CurrenValue;
        private System.Windows.Forms.TextBox tbox_Beam_PosX_MiniValue;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox textBox64;
    }
}
