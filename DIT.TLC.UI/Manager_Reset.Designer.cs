﻿namespace DIT.TLC.UI
{
    partial class Manager_Reset
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel_all_reset = new System.Windows.Forms.Panel();
            this.lb_all_reset = new System.Windows.Forms.Label();
            this.panel_uld_reset = new System.Windows.Forms.Panel();
            this.lb_uld_reset = new System.Windows.Forms.Label();
            this.panel_process_reset = new System.Windows.Forms.Panel();
            this.lb_process_reset = new System.Windows.Forms.Label();
            this.panel_ld_reset = new System.Windows.Forms.Panel();
            this.lb_ld_reset = new System.Windows.Forms.Label();
            this.panel_ld_percent = new System.Windows.Forms.Panel();
            this.lb_ld_percent_info2 = new System.Windows.Forms.Label();
            this.lb_ld_percent = new System.Windows.Forms.Label();
            this.lb_ld_percent_info = new System.Windows.Forms.Label();
            this.pbar_ld_percent = new System.Windows.Forms.ProgressBar();
            this.pbar_process_percent = new System.Windows.Forms.ProgressBar();
            this.panel_process_percent = new System.Windows.Forms.Panel();
            this.lb_process_percent_info2 = new System.Windows.Forms.Label();
            this.lb_process_percent = new System.Windows.Forms.Label();
            this.lb_process_percent_info = new System.Windows.Forms.Label();
            this.pbar_uld_percent = new System.Windows.Forms.ProgressBar();
            this.panel_uld_percent = new System.Windows.Forms.Panel();
            this.lb_uld_percent_info2 = new System.Windows.Forms.Label();
            this.lb_uld_percent = new System.Windows.Forms.Label();
            this.lb_uld_percent_info = new System.Windows.Forms.Label();
            this.panel_all_percent = new System.Windows.Forms.Panel();
            this.lb_all_percent_info2 = new System.Windows.Forms.Label();
            this.lb_all_percent = new System.Windows.Forms.Label();
            this.lb_all_percent_info = new System.Windows.Forms.Label();
            this.pbar_all_percent = new System.Windows.Forms.ProgressBar();
            this.panel_sscnet_reset = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.panel_data_init = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.gbox_ld = new System.Windows.Forms.GroupBox();
            this.panel_14_cell_trans_y_4 = new System.Windows.Forms.Panel();
            this.lb_14_cell_trans_y_4 = new System.Windows.Forms.Label();
            this.panel_10_cell_y_1 = new System.Windows.Forms.Panel();
            this.lb_10_cell_y_1 = new System.Windows.Forms.Label();
            this.panel_15_cell_trans_t_2 = new System.Windows.Forms.Panel();
            this.lb_15_cell_trans_t_2 = new System.Windows.Forms.Label();
            this.panel_13_cell_y_2 = new System.Windows.Forms.Panel();
            this.lb_13_cell_y_2 = new System.Windows.Forms.Label();
            this.panel_06_cst_rotation_up_right = new System.Windows.Forms.Panel();
            this.lb_06_cst_rotation_up_right = new System.Windows.Forms.Label();
            this.panel_12_cell_trans_t_1 = new System.Windows.Forms.Panel();
            this.lb_12_cell_trans_t_1 = new System.Windows.Forms.Label();
            this.panel_11_cell_trans_y_3 = new System.Windows.Forms.Panel();
            this.lb_11_cell_trans_y_3 = new System.Windows.Forms.Label();
            this.panel_09_cst_electricty_z_right = new System.Windows.Forms.Panel();
            this.lb_09_cst_electricty_z_right = new System.Windows.Forms.Label();
            this.panel_35_cell_trans_x_1 = new System.Windows.Forms.Panel();
            this.lb_35_cell_trans_x_1 = new System.Windows.Forms.Label();
            this.panel_08_cst_electricty_z_left = new System.Windows.Forms.Panel();
            this.lb_08_cst_electricty_z_left = new System.Windows.Forms.Label();
            this.panel_07_cst_rotation_down_right = new System.Windows.Forms.Panel();
            this.lb_07_cst_rotation_down_right = new System.Windows.Forms.Label();
            this.panel_05_cst_rotation_down_left = new System.Windows.Forms.Panel();
            this.lb_05_cst_rotation_down_left = new System.Windows.Forms.Label();
            this.panel_33_cell_trans_y_1 = new System.Windows.Forms.Panel();
            this.lb_33_cell_trans_y_1 = new System.Windows.Forms.Label();
            this.panel_04_cst_rotation_up_left = new System.Windows.Forms.Panel();
            this.lb_04_cst_rotation_up_left = new System.Windows.Forms.Label();
            this.panel_36_cell_trans_x_2 = new System.Windows.Forms.Panel();
            this.lb_36_cell_trans_x_2 = new System.Windows.Forms.Label();
            this.panel_32_cell_x_2 = new System.Windows.Forms.Panel();
            this.lb_32_cell_x_2 = new System.Windows.Forms.Label();
            this.panel_31_cell_x_1 = new System.Windows.Forms.Panel();
            this.lb_31_cell_x_1 = new System.Windows.Forms.Label();
            this.gbox_process = new System.Windows.Forms.GroupBox();
            this.panel_18_break_z_1 = new System.Windows.Forms.Panel();
            this.lb_18_break_z_1 = new System.Windows.Forms.Label();
            this.panel_21_break_z_4 = new System.Windows.Forms.Panel();
            this.lb_21_break_z_4 = new System.Windows.Forms.Label();
            this.panel_11_break_x_1 = new System.Windows.Forms.Panel();
            this.lb_11_break_x_1 = new System.Windows.Forms.Label();
            this.panel_20_break_z_3 = new System.Windows.Forms.Panel();
            this.lb_20_break_z_3 = new System.Windows.Forms.Label();
            this.panel_19_break_z_2 = new System.Windows.Forms.Panel();
            this.lb_19_break_z_2 = new System.Windows.Forms.Label();
            this.panel_14_break_x_4 = new System.Windows.Forms.Panel();
            this.lb_14_break_x_4 = new System.Windows.Forms.Label();
            this.panel_07_cell_trans_y_1 = new System.Windows.Forms.Panel();
            this.lb_07_cell_trans_y_1 = new System.Windows.Forms.Label();
            this.panel_13_break_x_3 = new System.Windows.Forms.Panel();
            this.lb_13_break_x_3 = new System.Windows.Forms.Label();
            this.panel_12_break_x_2 = new System.Windows.Forms.Panel();
            this.lb_12_break_x_2 = new System.Windows.Forms.Label();
            this.panel_10_break_table_y_1 = new System.Windows.Forms.Panel();
            this.lb_10_break_table_y_1 = new System.Windows.Forms.Label();
            this.panel_17_head_z_1 = new System.Windows.Forms.Panel();
            this.lb_17_head_z_1 = new System.Windows.Forms.Label();
            this.panel_09_break_table_y_1 = new System.Windows.Forms.Panel();
            this.lb_09_break_table_y_1 = new System.Windows.Forms.Label();
            this.panel_22_masure_x_1 = new System.Windows.Forms.Panel();
            this.lb_22_masure_x_1 = new System.Windows.Forms.Label();
            this.panel_06_cell_trans_x_2 = new System.Windows.Forms.Panel();
            this.lb_06_cell_trans_x_2 = new System.Windows.Forms.Label();
            this.panel_03_head_x_1 = new System.Windows.Forms.Panel();
            this.lb_03_head_x_1 = new System.Windows.Forms.Label();
            this.panel_05_cell_trans_x_1 = new System.Windows.Forms.Panel();
            this.lb_05_cell_trans_x_1 = new System.Windows.Forms.Label();
            this.panel_04_align_x_1 = new System.Windows.Forms.Panel();
            this.lb_04_align_x_1 = new System.Windows.Forms.Label();
            this.panel_02_table_y_2 = new System.Windows.Forms.Panel();
            this.lb_02_table_y_2 = new System.Windows.Forms.Label();
            this.panel_01_table_y_1 = new System.Windows.Forms.Panel();
            this.lb_01_table_y_1 = new System.Windows.Forms.Label();
            this.gbox_uld = new System.Windows.Forms.GroupBox();
            this.panel_4_break_table_t_4 = new System.Windows.Forms.Panel();
            this.lb_4_break_table_t_4 = new System.Windows.Forms.Label();
            this.panel_1_break_table_t_2 = new System.Windows.Forms.Panel();
            this.lb_1_break_table_t_2 = new System.Windows.Forms.Label();
            this.panel_2_break_table_t_3 = new System.Windows.Forms.Panel();
            this.lb_2_break_table_t_3 = new System.Windows.Forms.Label();
            this.panel_29_cell_y_2 = new System.Windows.Forms.Panel();
            this.lb_29_cell_y_2 = new System.Windows.Forms.Label();
            this.panel_0_break_table_t_1 = new System.Windows.Forms.Panel();
            this.lb_0_break_table_t_1 = new System.Windows.Forms.Label();
            this.panel_23_cell_trans_t_3 = new System.Windows.Forms.Panel();
            this.lb_23_cell_trans_t_3 = new System.Windows.Forms.Label();
            this.panel_30_cst_rotation_down_right = new System.Windows.Forms.Panel();
            this.lb_30_cst_rotation_down_right = new System.Windows.Forms.Label();
            this.panel_29_cst_rotation_up_right = new System.Windows.Forms.Panel();
            this.lb_29_cst_rotation_up_right = new System.Windows.Forms.Label();
            this.panel_27_insp_y_1 = new System.Windows.Forms.Panel();
            this.lb_27_insp_y_1 = new System.Windows.Forms.Label();
            this.panel_18_cst_electricty_z_left = new System.Windows.Forms.Panel();
            this.lb_18_cst_electricty_z_left = new System.Windows.Forms.Label();
            this.panel_26_insp_z_1 = new System.Windows.Forms.Panel();
            this.lb_26_insp_z_1 = new System.Windows.Forms.Label();
            this.panel_24_cell_trans_t_4 = new System.Windows.Forms.Panel();
            this.lb_24_cell_trans_t_4 = new System.Windows.Forms.Label();
            this.panel_21_cell_trans_t_2 = new System.Windows.Forms.Panel();
            this.lb_21_cell_trans_t_2 = new System.Windows.Forms.Label();
            this.panel_40_cell_x_1 = new System.Windows.Forms.Panel();
            this.label65 = new System.Windows.Forms.Label();
            this.panel_20_cell_trans_t_1 = new System.Windows.Forms.Panel();
            this.lb_20_cell_trans_t_1 = new System.Windows.Forms.Label();
            this.panel_19_cst_electricty_z_right = new System.Windows.Forms.Panel();
            this.lb_19_cst_electricty_z_right = new System.Windows.Forms.Label();
            this.panel_17_cst_rotation_down_left = new System.Windows.Forms.Panel();
            this.lb_17_cst_rotation_down_left = new System.Windows.Forms.Label();
            this.panel_39_insp_x_1 = new System.Windows.Forms.Panel();
            this.lb_39_insp_x_1 = new System.Windows.Forms.Label();
            this.panel_16_cst_rotation_up_left = new System.Windows.Forms.Panel();
            this.lb_16_cst_rotation_up_left = new System.Windows.Forms.Label();
            this.panel_41_cell_x_2 = new System.Windows.Forms.Panel();
            this.lb_41_cell_x_2 = new System.Windows.Forms.Label();
            this.panel_38_cell_trans_y_2 = new System.Windows.Forms.Panel();
            this.lb_38_cell_trans_y_2 = new System.Windows.Forms.Label();
            this.panel_37_cell_trans_y_1 = new System.Windows.Forms.Panel();
            this.lb_37_cell_trans_y_1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel_all_reset.SuspendLayout();
            this.panel_uld_reset.SuspendLayout();
            this.panel_process_reset.SuspendLayout();
            this.panel_ld_reset.SuspendLayout();
            this.panel_ld_percent.SuspendLayout();
            this.panel_process_percent.SuspendLayout();
            this.panel_uld_percent.SuspendLayout();
            this.panel_all_percent.SuspendLayout();
            this.panel_sscnet_reset.SuspendLayout();
            this.panel_data_init.SuspendLayout();
            this.gbox_ld.SuspendLayout();
            this.panel_14_cell_trans_y_4.SuspendLayout();
            this.panel_10_cell_y_1.SuspendLayout();
            this.panel_15_cell_trans_t_2.SuspendLayout();
            this.panel_13_cell_y_2.SuspendLayout();
            this.panel_06_cst_rotation_up_right.SuspendLayout();
            this.panel_12_cell_trans_t_1.SuspendLayout();
            this.panel_11_cell_trans_y_3.SuspendLayout();
            this.panel_09_cst_electricty_z_right.SuspendLayout();
            this.panel_35_cell_trans_x_1.SuspendLayout();
            this.panel_08_cst_electricty_z_left.SuspendLayout();
            this.panel_07_cst_rotation_down_right.SuspendLayout();
            this.panel_05_cst_rotation_down_left.SuspendLayout();
            this.panel_33_cell_trans_y_1.SuspendLayout();
            this.panel_04_cst_rotation_up_left.SuspendLayout();
            this.panel_36_cell_trans_x_2.SuspendLayout();
            this.panel_32_cell_x_2.SuspendLayout();
            this.panel_31_cell_x_1.SuspendLayout();
            this.gbox_process.SuspendLayout();
            this.panel_18_break_z_1.SuspendLayout();
            this.panel_21_break_z_4.SuspendLayout();
            this.panel_11_break_x_1.SuspendLayout();
            this.panel_20_break_z_3.SuspendLayout();
            this.panel_19_break_z_2.SuspendLayout();
            this.panel_14_break_x_4.SuspendLayout();
            this.panel_07_cell_trans_y_1.SuspendLayout();
            this.panel_13_break_x_3.SuspendLayout();
            this.panel_12_break_x_2.SuspendLayout();
            this.panel_10_break_table_y_1.SuspendLayout();
            this.panel_17_head_z_1.SuspendLayout();
            this.panel_09_break_table_y_1.SuspendLayout();
            this.panel_22_masure_x_1.SuspendLayout();
            this.panel_06_cell_trans_x_2.SuspendLayout();
            this.panel_03_head_x_1.SuspendLayout();
            this.panel_05_cell_trans_x_1.SuspendLayout();
            this.panel_04_align_x_1.SuspendLayout();
            this.panel_02_table_y_2.SuspendLayout();
            this.panel_01_table_y_1.SuspendLayout();
            this.gbox_uld.SuspendLayout();
            this.panel_4_break_table_t_4.SuspendLayout();
            this.panel_1_break_table_t_2.SuspendLayout();
            this.panel_2_break_table_t_3.SuspendLayout();
            this.panel_29_cell_y_2.SuspendLayout();
            this.panel_0_break_table_t_1.SuspendLayout();
            this.panel_23_cell_trans_t_3.SuspendLayout();
            this.panel_30_cst_rotation_down_right.SuspendLayout();
            this.panel_29_cst_rotation_up_right.SuspendLayout();
            this.panel_27_insp_y_1.SuspendLayout();
            this.panel_18_cst_electricty_z_left.SuspendLayout();
            this.panel_26_insp_z_1.SuspendLayout();
            this.panel_24_cell_trans_t_4.SuspendLayout();
            this.panel_21_cell_trans_t_2.SuspendLayout();
            this.panel_40_cell_x_1.SuspendLayout();
            this.panel_20_cell_trans_t_1.SuspendLayout();
            this.panel_19_cst_electricty_z_right.SuspendLayout();
            this.panel_17_cst_rotation_down_left.SuspendLayout();
            this.panel_39_insp_x_1.SuspendLayout();
            this.panel_16_cst_rotation_up_left.SuspendLayout();
            this.panel_41_cell_x_2.SuspendLayout();
            this.panel_38_cell_trans_y_2.SuspendLayout();
            this.panel_37_cell_trans_y_1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.panel_all_reset);
            this.panel1.Controls.Add(this.panel_uld_reset);
            this.panel1.Controls.Add(this.panel_process_reset);
            this.panel1.Controls.Add(this.panel_ld_reset);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1734, 300);
            this.panel1.TabIndex = 0;
            // 
            // panel_all_reset
            // 
            this.panel_all_reset.BackColor = System.Drawing.Color.DimGray;
            this.panel_all_reset.Controls.Add(this.lb_all_reset);
            this.panel_all_reset.Location = new System.Drawing.Point(3, 152);
            this.panel_all_reset.Name = "panel_all_reset";
            this.panel_all_reset.Size = new System.Drawing.Size(1726, 143);
            this.panel_all_reset.TabIndex = 3;
            // 
            // lb_all_reset
            // 
            this.lb_all_reset.Font = new System.Drawing.Font("굴림", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_all_reset.ForeColor = System.Drawing.Color.White;
            this.lb_all_reset.Location = new System.Drawing.Point(768, 1);
            this.lb_all_reset.Name = "lb_all_reset";
            this.lb_all_reset.Size = new System.Drawing.Size(196, 137);
            this.lb_all_reset.TabIndex = 0;
            this.lb_all_reset.Text = " 전체  초기화";
            this.lb_all_reset.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_uld_reset
            // 
            this.panel_uld_reset.BackColor = System.Drawing.Color.DimGray;
            this.panel_uld_reset.Controls.Add(this.lb_uld_reset);
            this.panel_uld_reset.Location = new System.Drawing.Point(1159, 3);
            this.panel_uld_reset.Name = "panel_uld_reset";
            this.panel_uld_reset.Size = new System.Drawing.Size(570, 143);
            this.panel_uld_reset.TabIndex = 2;
            // 
            // lb_uld_reset
            // 
            this.lb_uld_reset.Font = new System.Drawing.Font("굴림", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld_reset.ForeColor = System.Drawing.Color.White;
            this.lb_uld_reset.Location = new System.Drawing.Point(164, 3);
            this.lb_uld_reset.Name = "lb_uld_reset";
            this.lb_uld_reset.Size = new System.Drawing.Size(247, 137);
            this.lb_uld_reset.TabIndex = 0;
            this.lb_uld_reset.Text = "언로더부초기화";
            this.lb_uld_reset.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_process_reset
            // 
            this.panel_process_reset.BackColor = System.Drawing.Color.DimGray;
            this.panel_process_reset.Controls.Add(this.lb_process_reset);
            this.panel_process_reset.Location = new System.Drawing.Point(581, 3);
            this.panel_process_reset.Name = "panel_process_reset";
            this.panel_process_reset.Size = new System.Drawing.Size(570, 143);
            this.panel_process_reset.TabIndex = 1;
            // 
            // lb_process_reset
            // 
            this.lb_process_reset.Font = new System.Drawing.Font("굴림", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_reset.ForeColor = System.Drawing.Color.White;
            this.lb_process_reset.Location = new System.Drawing.Point(149, 3);
            this.lb_process_reset.Name = "lb_process_reset";
            this.lb_process_reset.Size = new System.Drawing.Size(289, 137);
            this.lb_process_reset.TabIndex = 0;
            this.lb_process_reset.Text = "프로세스부초기화";
            this.lb_process_reset.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_ld_reset
            // 
            this.panel_ld_reset.BackColor = System.Drawing.Color.DimGray;
            this.panel_ld_reset.Controls.Add(this.lb_ld_reset);
            this.panel_ld_reset.Location = new System.Drawing.Point(3, 3);
            this.panel_ld_reset.Name = "panel_ld_reset";
            this.panel_ld_reset.Size = new System.Drawing.Size(570, 143);
            this.panel_ld_reset.TabIndex = 0;
            // 
            // lb_ld_reset
            // 
            this.lb_ld_reset.Font = new System.Drawing.Font("굴림", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld_reset.ForeColor = System.Drawing.Color.White;
            this.lb_ld_reset.Location = new System.Drawing.Point(165, 3);
            this.lb_ld_reset.Name = "lb_ld_reset";
            this.lb_ld_reset.Size = new System.Drawing.Size(223, 137);
            this.lb_ld_reset.TabIndex = 0;
            this.lb_ld_reset.Text = "로더부초기화";
            this.lb_ld_reset.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_ld_percent
            // 
            this.panel_ld_percent.BackColor = System.Drawing.Color.Gray;
            this.panel_ld_percent.Controls.Add(this.lb_ld_percent_info2);
            this.panel_ld_percent.Controls.Add(this.lb_ld_percent);
            this.panel_ld_percent.Controls.Add(this.lb_ld_percent_info);
            this.panel_ld_percent.Location = new System.Drawing.Point(7, 309);
            this.panel_ld_percent.Name = "panel_ld_percent";
            this.panel_ld_percent.Size = new System.Drawing.Size(188, 44);
            this.panel_ld_percent.TabIndex = 4;
            // 
            // lb_ld_percent_info2
            // 
            this.lb_ld_percent_info2.AutoSize = true;
            this.lb_ld_percent_info2.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld_percent_info2.ForeColor = System.Drawing.Color.White;
            this.lb_ld_percent_info2.Location = new System.Drawing.Point(95, 24);
            this.lb_ld_percent_info2.Name = "lb_ld_percent_info2";
            this.lb_ld_percent_info2.Size = new System.Drawing.Size(19, 15);
            this.lb_ld_percent_info2.TabIndex = 2;
            this.lb_ld_percent_info2.Text = "%";
            // 
            // lb_ld_percent
            // 
            this.lb_ld_percent.AutoSize = true;
            this.lb_ld_percent.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld_percent.ForeColor = System.Drawing.Color.White;
            this.lb_ld_percent.Location = new System.Drawing.Point(80, 24);
            this.lb_ld_percent.Name = "lb_ld_percent";
            this.lb_ld_percent.Size = new System.Drawing.Size(16, 15);
            this.lb_ld_percent.TabIndex = 1;
            this.lb_ld_percent.Text = "0";
            // 
            // lb_ld_percent_info
            // 
            this.lb_ld_percent_info.AutoSize = true;
            this.lb_ld_percent_info.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld_percent_info.ForeColor = System.Drawing.Color.White;
            this.lb_ld_percent_info.Location = new System.Drawing.Point(42, 5);
            this.lb_ld_percent_info.Name = "lb_ld_percent_info";
            this.lb_ld_percent_info.Size = new System.Drawing.Size(109, 15);
            this.lb_ld_percent_info.TabIndex = 0;
            this.lb_ld_percent_info.Text = "로더부 진행률";
            // 
            // pbar_ld_percent
            // 
            this.pbar_ld_percent.Location = new System.Drawing.Point(201, 309);
            this.pbar_ld_percent.Name = "pbar_ld_percent";
            this.pbar_ld_percent.Size = new System.Drawing.Size(376, 44);
            this.pbar_ld_percent.TabIndex = 5;
            // 
            // pbar_process_percent
            // 
            this.pbar_process_percent.Location = new System.Drawing.Point(779, 309);
            this.pbar_process_percent.Name = "pbar_process_percent";
            this.pbar_process_percent.Size = new System.Drawing.Size(376, 44);
            this.pbar_process_percent.TabIndex = 7;
            // 
            // panel_process_percent
            // 
            this.panel_process_percent.BackColor = System.Drawing.Color.Gray;
            this.panel_process_percent.Controls.Add(this.lb_process_percent_info2);
            this.panel_process_percent.Controls.Add(this.lb_process_percent);
            this.panel_process_percent.Controls.Add(this.lb_process_percent_info);
            this.panel_process_percent.Location = new System.Drawing.Point(585, 309);
            this.panel_process_percent.Name = "panel_process_percent";
            this.panel_process_percent.Size = new System.Drawing.Size(188, 44);
            this.panel_process_percent.TabIndex = 6;
            // 
            // lb_process_percent_info2
            // 
            this.lb_process_percent_info2.AutoSize = true;
            this.lb_process_percent_info2.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_percent_info2.ForeColor = System.Drawing.Color.White;
            this.lb_process_percent_info2.Location = new System.Drawing.Point(95, 24);
            this.lb_process_percent_info2.Name = "lb_process_percent_info2";
            this.lb_process_percent_info2.Size = new System.Drawing.Size(19, 15);
            this.lb_process_percent_info2.TabIndex = 2;
            this.lb_process_percent_info2.Text = "%";
            // 
            // lb_process_percent
            // 
            this.lb_process_percent.AutoSize = true;
            this.lb_process_percent.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_percent.ForeColor = System.Drawing.Color.White;
            this.lb_process_percent.Location = new System.Drawing.Point(80, 24);
            this.lb_process_percent.Name = "lb_process_percent";
            this.lb_process_percent.Size = new System.Drawing.Size(16, 15);
            this.lb_process_percent.TabIndex = 1;
            this.lb_process_percent.Text = "0";
            // 
            // lb_process_percent_info
            // 
            this.lb_process_percent_info.AutoSize = true;
            this.lb_process_percent_info.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_percent_info.ForeColor = System.Drawing.Color.White;
            this.lb_process_percent_info.Location = new System.Drawing.Point(26, 5);
            this.lb_process_percent_info.Name = "lb_process_percent_info";
            this.lb_process_percent_info.Size = new System.Drawing.Size(141, 15);
            this.lb_process_percent_info.TabIndex = 0;
            this.lb_process_percent_info.Text = "프로세스부 진행률";
            // 
            // pbar_uld_percent
            // 
            this.pbar_uld_percent.Location = new System.Drawing.Point(1357, 309);
            this.pbar_uld_percent.Name = "pbar_uld_percent";
            this.pbar_uld_percent.Size = new System.Drawing.Size(376, 44);
            this.pbar_uld_percent.TabIndex = 9;
            // 
            // panel_uld_percent
            // 
            this.panel_uld_percent.BackColor = System.Drawing.Color.Gray;
            this.panel_uld_percent.Controls.Add(this.lb_uld_percent_info2);
            this.panel_uld_percent.Controls.Add(this.lb_uld_percent);
            this.panel_uld_percent.Controls.Add(this.lb_uld_percent_info);
            this.panel_uld_percent.Location = new System.Drawing.Point(1163, 309);
            this.panel_uld_percent.Name = "panel_uld_percent";
            this.panel_uld_percent.Size = new System.Drawing.Size(188, 44);
            this.panel_uld_percent.TabIndex = 8;
            // 
            // lb_uld_percent_info2
            // 
            this.lb_uld_percent_info2.AutoSize = true;
            this.lb_uld_percent_info2.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld_percent_info2.ForeColor = System.Drawing.Color.White;
            this.lb_uld_percent_info2.Location = new System.Drawing.Point(95, 24);
            this.lb_uld_percent_info2.Name = "lb_uld_percent_info2";
            this.lb_uld_percent_info2.Size = new System.Drawing.Size(19, 15);
            this.lb_uld_percent_info2.TabIndex = 2;
            this.lb_uld_percent_info2.Text = "%";
            // 
            // lb_uld_percent
            // 
            this.lb_uld_percent.AutoSize = true;
            this.lb_uld_percent.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld_percent.ForeColor = System.Drawing.Color.White;
            this.lb_uld_percent.Location = new System.Drawing.Point(80, 24);
            this.lb_uld_percent.Name = "lb_uld_percent";
            this.lb_uld_percent.Size = new System.Drawing.Size(16, 15);
            this.lb_uld_percent.TabIndex = 1;
            this.lb_uld_percent.Text = "0";
            // 
            // lb_uld_percent_info
            // 
            this.lb_uld_percent_info.AutoSize = true;
            this.lb_uld_percent_info.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld_percent_info.ForeColor = System.Drawing.Color.White;
            this.lb_uld_percent_info.Location = new System.Drawing.Point(32, 5);
            this.lb_uld_percent_info.Name = "lb_uld_percent_info";
            this.lb_uld_percent_info.Size = new System.Drawing.Size(125, 15);
            this.lb_uld_percent_info.TabIndex = 0;
            this.lb_uld_percent_info.Text = "언로더부 진행률";
            // 
            // panel_all_percent
            // 
            this.panel_all_percent.BackColor = System.Drawing.Color.Gray;
            this.panel_all_percent.Controls.Add(this.lb_all_percent_info2);
            this.panel_all_percent.Controls.Add(this.lb_all_percent);
            this.panel_all_percent.Controls.Add(this.lb_all_percent_info);
            this.panel_all_percent.Location = new System.Drawing.Point(7, 359);
            this.panel_all_percent.Name = "panel_all_percent";
            this.panel_all_percent.Size = new System.Drawing.Size(188, 44);
            this.panel_all_percent.TabIndex = 5;
            // 
            // lb_all_percent_info2
            // 
            this.lb_all_percent_info2.AutoSize = true;
            this.lb_all_percent_info2.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_all_percent_info2.ForeColor = System.Drawing.Color.White;
            this.lb_all_percent_info2.Location = new System.Drawing.Point(95, 24);
            this.lb_all_percent_info2.Name = "lb_all_percent_info2";
            this.lb_all_percent_info2.Size = new System.Drawing.Size(19, 15);
            this.lb_all_percent_info2.TabIndex = 2;
            this.lb_all_percent_info2.Text = "%";
            // 
            // lb_all_percent
            // 
            this.lb_all_percent.AutoSize = true;
            this.lb_all_percent.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_all_percent.ForeColor = System.Drawing.Color.White;
            this.lb_all_percent.Location = new System.Drawing.Point(80, 24);
            this.lb_all_percent.Name = "lb_all_percent";
            this.lb_all_percent.Size = new System.Drawing.Size(16, 15);
            this.lb_all_percent.TabIndex = 1;
            this.lb_all_percent.Text = "0";
            // 
            // lb_all_percent_info
            // 
            this.lb_all_percent_info.AutoSize = true;
            this.lb_all_percent_info.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_all_percent_info.ForeColor = System.Drawing.Color.White;
            this.lb_all_percent_info.Location = new System.Drawing.Point(47, 5);
            this.lb_all_percent_info.Name = "lb_all_percent_info";
            this.lb_all_percent_info.Size = new System.Drawing.Size(93, 15);
            this.lb_all_percent_info.TabIndex = 0;
            this.lb_all_percent_info.Text = "전체 진행률";
            // 
            // pbar_all_percent
            // 
            this.pbar_all_percent.Location = new System.Drawing.Point(201, 359);
            this.pbar_all_percent.Name = "pbar_all_percent";
            this.pbar_all_percent.Size = new System.Drawing.Size(1047, 44);
            this.pbar_all_percent.TabIndex = 10;
            // 
            // panel_sscnet_reset
            // 
            this.panel_sscnet_reset.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_sscnet_reset.Controls.Add(this.label17);
            this.panel_sscnet_reset.Location = new System.Drawing.Point(1254, 359);
            this.panel_sscnet_reset.Name = "panel_sscnet_reset";
            this.panel_sscnet_reset.Size = new System.Drawing.Size(331, 44);
            this.panel_sscnet_reset.TabIndex = 11;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(75, 14);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(180, 15);
            this.label17.TabIndex = 1;
            this.label17.Text = "미쯔비시 SSC넷 재설정";
            // 
            // panel_data_init
            // 
            this.panel_data_init.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_data_init.Controls.Add(this.label18);
            this.panel_data_init.Location = new System.Drawing.Point(1591, 359);
            this.panel_data_init.Name = "panel_data_init";
            this.panel_data_init.Size = new System.Drawing.Size(142, 44);
            this.panel_data_init.TabIndex = 12;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(38, 14);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(69, 15);
            this.label18.TabIndex = 1;
            this.label18.Text = "Data Init";
            // 
            // gbox_ld
            // 
            this.gbox_ld.Controls.Add(this.panel_14_cell_trans_y_4);
            this.gbox_ld.Controls.Add(this.panel_10_cell_y_1);
            this.gbox_ld.Controls.Add(this.panel_15_cell_trans_t_2);
            this.gbox_ld.Controls.Add(this.panel_13_cell_y_2);
            this.gbox_ld.Controls.Add(this.panel_06_cst_rotation_up_right);
            this.gbox_ld.Controls.Add(this.panel_12_cell_trans_t_1);
            this.gbox_ld.Controls.Add(this.panel_11_cell_trans_y_3);
            this.gbox_ld.Controls.Add(this.panel_09_cst_electricty_z_right);
            this.gbox_ld.Controls.Add(this.panel_35_cell_trans_x_1);
            this.gbox_ld.Controls.Add(this.panel_08_cst_electricty_z_left);
            this.gbox_ld.Controls.Add(this.panel_07_cst_rotation_down_right);
            this.gbox_ld.Controls.Add(this.panel_05_cst_rotation_down_left);
            this.gbox_ld.Controls.Add(this.panel_33_cell_trans_y_1);
            this.gbox_ld.Controls.Add(this.panel_04_cst_rotation_up_left);
            this.gbox_ld.Controls.Add(this.panel_36_cell_trans_x_2);
            this.gbox_ld.Controls.Add(this.panel_32_cell_x_2);
            this.gbox_ld.Controls.Add(this.panel_31_cell_x_1);
            this.gbox_ld.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_ld.ForeColor = System.Drawing.Color.White;
            this.gbox_ld.Location = new System.Drawing.Point(7, 409);
            this.gbox_ld.Name = "gbox_ld";
            this.gbox_ld.Size = new System.Drawing.Size(570, 448);
            this.gbox_ld.TabIndex = 14;
            this.gbox_ld.TabStop = false;
            this.gbox_ld.Text = "로더";
            // 
            // panel_14_cell_trans_y_4
            // 
            this.panel_14_cell_trans_y_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_14_cell_trans_y_4.Controls.Add(this.lb_14_cell_trans_y_4);
            this.panel_14_cell_trans_y_4.Location = new System.Drawing.Point(289, 320);
            this.panel_14_cell_trans_y_4.Name = "panel_14_cell_trans_y_4";
            this.panel_14_cell_trans_y_4.Size = new System.Drawing.Size(275, 35);
            this.panel_14_cell_trans_y_4.TabIndex = 7;
            // 
            // lb_14_cell_trans_y_4
            // 
            this.lb_14_cell_trans_y_4.AutoSize = true;
            this.lb_14_cell_trans_y_4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_14_cell_trans_y_4.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_14_cell_trans_y_4.Location = new System.Drawing.Point(52, 10);
            this.lb_14_cell_trans_y_4.Name = "lb_14_cell_trans_y_4";
            this.lb_14_cell_trans_y_4.Size = new System.Drawing.Size(168, 12);
            this.lb_14_cell_trans_y_4.TabIndex = 2;
            this.lb_14_cell_trans_y_4.Text = "14.CELL_TRANSFER_Y_4";
            // 
            // panel_10_cell_y_1
            // 
            this.panel_10_cell_y_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_10_cell_y_1.Controls.Add(this.lb_10_cell_y_1);
            this.panel_10_cell_y_1.Location = new System.Drawing.Point(289, 238);
            this.panel_10_cell_y_1.Name = "panel_10_cell_y_1";
            this.panel_10_cell_y_1.Size = new System.Drawing.Size(275, 35);
            this.panel_10_cell_y_1.TabIndex = 7;
            // 
            // lb_10_cell_y_1
            // 
            this.lb_10_cell_y_1.AutoSize = true;
            this.lb_10_cell_y_1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_10_cell_y_1.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_10_cell_y_1.Location = new System.Drawing.Point(92, 10);
            this.lb_10_cell_y_1.Name = "lb_10_cell_y_1";
            this.lb_10_cell_y_1.Size = new System.Drawing.Size(89, 12);
            this.lb_10_cell_y_1.TabIndex = 1;
            this.lb_10_cell_y_1.Text = "10.CELL_Y_1";
            // 
            // panel_15_cell_trans_t_2
            // 
            this.panel_15_cell_trans_t_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_15_cell_trans_t_2.Controls.Add(this.lb_15_cell_trans_t_2);
            this.panel_15_cell_trans_t_2.Location = new System.Drawing.Point(6, 361);
            this.panel_15_cell_trans_t_2.Name = "panel_15_cell_trans_t_2";
            this.panel_15_cell_trans_t_2.Size = new System.Drawing.Size(275, 35);
            this.panel_15_cell_trans_t_2.TabIndex = 4;
            // 
            // lb_15_cell_trans_t_2
            // 
            this.lb_15_cell_trans_t_2.AutoSize = true;
            this.lb_15_cell_trans_t_2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_15_cell_trans_t_2.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_15_cell_trans_t_2.Location = new System.Drawing.Point(52, 10);
            this.lb_15_cell_trans_t_2.Name = "lb_15_cell_trans_t_2";
            this.lb_15_cell_trans_t_2.Size = new System.Drawing.Size(168, 12);
            this.lb_15_cell_trans_t_2.TabIndex = 2;
            this.lb_15_cell_trans_t_2.Text = "15.CELL_TRANSFER_T_2";
            // 
            // panel_13_cell_y_2
            // 
            this.panel_13_cell_y_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_13_cell_y_2.Controls.Add(this.lb_13_cell_y_2);
            this.panel_13_cell_y_2.Location = new System.Drawing.Point(6, 320);
            this.panel_13_cell_y_2.Name = "panel_13_cell_y_2";
            this.panel_13_cell_y_2.Size = new System.Drawing.Size(275, 35);
            this.panel_13_cell_y_2.TabIndex = 6;
            // 
            // lb_13_cell_y_2
            // 
            this.lb_13_cell_y_2.AutoSize = true;
            this.lb_13_cell_y_2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_13_cell_y_2.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_13_cell_y_2.Location = new System.Drawing.Point(92, 10);
            this.lb_13_cell_y_2.Name = "lb_13_cell_y_2";
            this.lb_13_cell_y_2.Size = new System.Drawing.Size(89, 12);
            this.lb_13_cell_y_2.TabIndex = 2;
            this.lb_13_cell_y_2.Text = "13.CELL_Y_2";
            // 
            // panel_06_cst_rotation_up_right
            // 
            this.panel_06_cst_rotation_up_right.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_06_cst_rotation_up_right.Controls.Add(this.lb_06_cst_rotation_up_right);
            this.panel_06_cst_rotation_up_right.Location = new System.Drawing.Point(289, 156);
            this.panel_06_cst_rotation_up_right.Name = "panel_06_cst_rotation_up_right";
            this.panel_06_cst_rotation_up_right.Size = new System.Drawing.Size(275, 35);
            this.panel_06_cst_rotation_up_right.TabIndex = 7;
            // 
            // lb_06_cst_rotation_up_right
            // 
            this.lb_06_cst_rotation_up_right.AutoSize = true;
            this.lb_06_cst_rotation_up_right.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_06_cst_rotation_up_right.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_06_cst_rotation_up_right.Location = new System.Drawing.Point(13, 11);
            this.lb_06_cst_rotation_up_right.Name = "lb_06_cst_rotation_up_right";
            this.lb_06_cst_rotation_up_right.Size = new System.Drawing.Size(247, 12);
            this.lb_06_cst_rotation_up_right.TabIndex = 6;
            this.lb_06_cst_rotation_up_right.Text = "06.CASSETTE_ROTATION_UP_RIGHT";
            // 
            // panel_12_cell_trans_t_1
            // 
            this.panel_12_cell_trans_t_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_12_cell_trans_t_1.Controls.Add(this.lb_12_cell_trans_t_1);
            this.panel_12_cell_trans_t_1.Location = new System.Drawing.Point(289, 279);
            this.panel_12_cell_trans_t_1.Name = "panel_12_cell_trans_t_1";
            this.panel_12_cell_trans_t_1.Size = new System.Drawing.Size(275, 35);
            this.panel_12_cell_trans_t_1.TabIndex = 5;
            // 
            // lb_12_cell_trans_t_1
            // 
            this.lb_12_cell_trans_t_1.AutoSize = true;
            this.lb_12_cell_trans_t_1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_12_cell_trans_t_1.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_12_cell_trans_t_1.Location = new System.Drawing.Point(52, 10);
            this.lb_12_cell_trans_t_1.Name = "lb_12_cell_trans_t_1";
            this.lb_12_cell_trans_t_1.Size = new System.Drawing.Size(168, 12);
            this.lb_12_cell_trans_t_1.TabIndex = 2;
            this.lb_12_cell_trans_t_1.Text = "12.CELL_TRANSFER_T_1";
            // 
            // panel_11_cell_trans_y_3
            // 
            this.panel_11_cell_trans_y_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_11_cell_trans_y_3.Controls.Add(this.lb_11_cell_trans_y_3);
            this.panel_11_cell_trans_y_3.Location = new System.Drawing.Point(6, 279);
            this.panel_11_cell_trans_y_3.Name = "panel_11_cell_trans_y_3";
            this.panel_11_cell_trans_y_3.Size = new System.Drawing.Size(275, 35);
            this.panel_11_cell_trans_y_3.TabIndex = 4;
            // 
            // lb_11_cell_trans_y_3
            // 
            this.lb_11_cell_trans_y_3.AutoSize = true;
            this.lb_11_cell_trans_y_3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_11_cell_trans_y_3.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_11_cell_trans_y_3.Location = new System.Drawing.Point(56, 11);
            this.lb_11_cell_trans_y_3.Name = "lb_11_cell_trans_y_3";
            this.lb_11_cell_trans_y_3.Size = new System.Drawing.Size(168, 12);
            this.lb_11_cell_trans_y_3.TabIndex = 1;
            this.lb_11_cell_trans_y_3.Text = "11.CELL_TRANSFER_Y_3";
            // 
            // panel_09_cst_electricty_z_right
            // 
            this.panel_09_cst_electricty_z_right.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_09_cst_electricty_z_right.Controls.Add(this.lb_09_cst_electricty_z_right);
            this.panel_09_cst_electricty_z_right.Location = new System.Drawing.Point(6, 238);
            this.panel_09_cst_electricty_z_right.Name = "panel_09_cst_electricty_z_right";
            this.panel_09_cst_electricty_z_right.Size = new System.Drawing.Size(275, 35);
            this.panel_09_cst_electricty_z_right.TabIndex = 6;
            // 
            // lb_09_cst_electricty_z_right
            // 
            this.lb_09_cst_electricty_z_right.AutoSize = true;
            this.lb_09_cst_electricty_z_right.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_09_cst_electricty_z_right.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_09_cst_electricty_z_right.Location = new System.Drawing.Point(9, 10);
            this.lb_09_cst_electricty_z_right.Name = "lb_09_cst_electricty_z_right";
            this.lb_09_cst_electricty_z_right.Size = new System.Drawing.Size(254, 12);
            this.lb_09_cst_electricty_z_right.TabIndex = 8;
            this.lb_09_cst_electricty_z_right.Text = "09.CASSETTE_ELECTRICTY_Z_RIGHT";
            // 
            // panel_35_cell_trans_x_1
            // 
            this.panel_35_cell_trans_x_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_35_cell_trans_x_1.Controls.Add(this.lb_35_cell_trans_x_1);
            this.panel_35_cell_trans_x_1.Location = new System.Drawing.Point(289, 74);
            this.panel_35_cell_trans_x_1.Name = "panel_35_cell_trans_x_1";
            this.panel_35_cell_trans_x_1.Size = new System.Drawing.Size(275, 35);
            this.panel_35_cell_trans_x_1.TabIndex = 3;
            // 
            // lb_35_cell_trans_x_1
            // 
            this.lb_35_cell_trans_x_1.AutoSize = true;
            this.lb_35_cell_trans_x_1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_35_cell_trans_x_1.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_35_cell_trans_x_1.Location = new System.Drawing.Point(57, 12);
            this.lb_35_cell_trans_x_1.Name = "lb_35_cell_trans_x_1";
            this.lb_35_cell_trans_x_1.Size = new System.Drawing.Size(168, 12);
            this.lb_35_cell_trans_x_1.TabIndex = 2;
            this.lb_35_cell_trans_x_1.Text = "35.CELL_TRANSFER_X_1";
            // 
            // panel_08_cst_electricty_z_left
            // 
            this.panel_08_cst_electricty_z_left.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_08_cst_electricty_z_left.Controls.Add(this.lb_08_cst_electricty_z_left);
            this.panel_08_cst_electricty_z_left.Location = new System.Drawing.Point(289, 197);
            this.panel_08_cst_electricty_z_left.Name = "panel_08_cst_electricty_z_left";
            this.panel_08_cst_electricty_z_left.Size = new System.Drawing.Size(275, 35);
            this.panel_08_cst_electricty_z_left.TabIndex = 5;
            // 
            // lb_08_cst_electricty_z_left
            // 
            this.lb_08_cst_electricty_z_left.AutoSize = true;
            this.lb_08_cst_electricty_z_left.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_08_cst_electricty_z_left.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_08_cst_electricty_z_left.Location = new System.Drawing.Point(13, 10);
            this.lb_08_cst_electricty_z_left.Name = "lb_08_cst_electricty_z_left";
            this.lb_08_cst_electricty_z_left.Size = new System.Drawing.Size(247, 12);
            this.lb_08_cst_electricty_z_left.TabIndex = 7;
            this.lb_08_cst_electricty_z_left.Text = "08.CASSETTE_ELECTRICTY_Z_LEFT";
            // 
            // panel_07_cst_rotation_down_right
            // 
            this.panel_07_cst_rotation_down_right.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_07_cst_rotation_down_right.Controls.Add(this.lb_07_cst_rotation_down_right);
            this.panel_07_cst_rotation_down_right.Location = new System.Drawing.Point(6, 197);
            this.panel_07_cst_rotation_down_right.Name = "panel_07_cst_rotation_down_right";
            this.panel_07_cst_rotation_down_right.Size = new System.Drawing.Size(275, 35);
            this.panel_07_cst_rotation_down_right.TabIndex = 4;
            // 
            // lb_07_cst_rotation_down_right
            // 
            this.lb_07_cst_rotation_down_right.AutoSize = true;
            this.lb_07_cst_rotation_down_right.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_07_cst_rotation_down_right.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_07_cst_rotation_down_right.Location = new System.Drawing.Point(3, 10);
            this.lb_07_cst_rotation_down_right.Name = "lb_07_cst_rotation_down_right";
            this.lb_07_cst_rotation_down_right.Size = new System.Drawing.Size(269, 12);
            this.lb_07_cst_rotation_down_right.TabIndex = 6;
            this.lb_07_cst_rotation_down_right.Text = "07.CASSETTE_ROTATION_DOWN_RIGHT";
            // 
            // panel_05_cst_rotation_down_left
            // 
            this.panel_05_cst_rotation_down_left.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_05_cst_rotation_down_left.Controls.Add(this.lb_05_cst_rotation_down_left);
            this.panel_05_cst_rotation_down_left.Location = new System.Drawing.Point(6, 156);
            this.panel_05_cst_rotation_down_left.Name = "panel_05_cst_rotation_down_left";
            this.panel_05_cst_rotation_down_left.Size = new System.Drawing.Size(275, 35);
            this.panel_05_cst_rotation_down_left.TabIndex = 6;
            // 
            // lb_05_cst_rotation_down_left
            // 
            this.lb_05_cst_rotation_down_left.AutoSize = true;
            this.lb_05_cst_rotation_down_left.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_05_cst_rotation_down_left.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_05_cst_rotation_down_left.Location = new System.Drawing.Point(6, 11);
            this.lb_05_cst_rotation_down_left.Name = "lb_05_cst_rotation_down_left";
            this.lb_05_cst_rotation_down_left.Size = new System.Drawing.Size(262, 12);
            this.lb_05_cst_rotation_down_left.TabIndex = 5;
            this.lb_05_cst_rotation_down_left.Text = "05.CASSETTE_ROTATION_DOWN_LEFT";
            // 
            // panel_33_cell_trans_y_1
            // 
            this.panel_33_cell_trans_y_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_33_cell_trans_y_1.Controls.Add(this.lb_33_cell_trans_y_1);
            this.panel_33_cell_trans_y_1.Location = new System.Drawing.Point(6, 74);
            this.panel_33_cell_trans_y_1.Name = "panel_33_cell_trans_y_1";
            this.panel_33_cell_trans_y_1.Size = new System.Drawing.Size(275, 35);
            this.panel_33_cell_trans_y_1.TabIndex = 2;
            // 
            // lb_33_cell_trans_y_1
            // 
            this.lb_33_cell_trans_y_1.AutoSize = true;
            this.lb_33_cell_trans_y_1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_33_cell_trans_y_1.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_33_cell_trans_y_1.Location = new System.Drawing.Point(57, 13);
            this.lb_33_cell_trans_y_1.Name = "lb_33_cell_trans_y_1";
            this.lb_33_cell_trans_y_1.Size = new System.Drawing.Size(168, 12);
            this.lb_33_cell_trans_y_1.TabIndex = 1;
            this.lb_33_cell_trans_y_1.Text = "33.CELL_TRANSFER_Y_1";
            // 
            // panel_04_cst_rotation_up_left
            // 
            this.panel_04_cst_rotation_up_left.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_04_cst_rotation_up_left.Controls.Add(this.lb_04_cst_rotation_up_left);
            this.panel_04_cst_rotation_up_left.Location = new System.Drawing.Point(289, 115);
            this.panel_04_cst_rotation_up_left.Name = "panel_04_cst_rotation_up_left";
            this.panel_04_cst_rotation_up_left.Size = new System.Drawing.Size(275, 35);
            this.panel_04_cst_rotation_up_left.TabIndex = 5;
            // 
            // lb_04_cst_rotation_up_left
            // 
            this.lb_04_cst_rotation_up_left.AutoSize = true;
            this.lb_04_cst_rotation_up_left.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_04_cst_rotation_up_left.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_04_cst_rotation_up_left.Location = new System.Drawing.Point(19, 12);
            this.lb_04_cst_rotation_up_left.Name = "lb_04_cst_rotation_up_left";
            this.lb_04_cst_rotation_up_left.Size = new System.Drawing.Size(240, 12);
            this.lb_04_cst_rotation_up_left.TabIndex = 4;
            this.lb_04_cst_rotation_up_left.Text = "04.CASSETTE_ROTATION_UP_LEFT";
            // 
            // panel_36_cell_trans_x_2
            // 
            this.panel_36_cell_trans_x_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_36_cell_trans_x_2.Controls.Add(this.lb_36_cell_trans_x_2);
            this.panel_36_cell_trans_x_2.Location = new System.Drawing.Point(6, 115);
            this.panel_36_cell_trans_x_2.Name = "panel_36_cell_trans_x_2";
            this.panel_36_cell_trans_x_2.Size = new System.Drawing.Size(275, 35);
            this.panel_36_cell_trans_x_2.TabIndex = 4;
            // 
            // lb_36_cell_trans_x_2
            // 
            this.lb_36_cell_trans_x_2.AutoSize = true;
            this.lb_36_cell_trans_x_2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_36_cell_trans_x_2.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_36_cell_trans_x_2.Location = new System.Drawing.Point(56, 12);
            this.lb_36_cell_trans_x_2.Name = "lb_36_cell_trans_x_2";
            this.lb_36_cell_trans_x_2.Size = new System.Drawing.Size(168, 12);
            this.lb_36_cell_trans_x_2.TabIndex = 3;
            this.lb_36_cell_trans_x_2.Text = "36.CELL_TRANSFER_X_2";
            // 
            // panel_32_cell_x_2
            // 
            this.panel_32_cell_x_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_32_cell_x_2.Controls.Add(this.lb_32_cell_x_2);
            this.panel_32_cell_x_2.Location = new System.Drawing.Point(289, 33);
            this.panel_32_cell_x_2.Name = "panel_32_cell_x_2";
            this.panel_32_cell_x_2.Size = new System.Drawing.Size(275, 35);
            this.panel_32_cell_x_2.TabIndex = 1;
            // 
            // lb_32_cell_x_2
            // 
            this.lb_32_cell_x_2.AutoSize = true;
            this.lb_32_cell_x_2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_32_cell_x_2.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_32_cell_x_2.Location = new System.Drawing.Point(92, 11);
            this.lb_32_cell_x_2.Name = "lb_32_cell_x_2";
            this.lb_32_cell_x_2.Size = new System.Drawing.Size(89, 12);
            this.lb_32_cell_x_2.TabIndex = 1;
            this.lb_32_cell_x_2.Text = "32.CELL_X_2";
            // 
            // panel_31_cell_x_1
            // 
            this.panel_31_cell_x_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_31_cell_x_1.Controls.Add(this.lb_31_cell_x_1);
            this.panel_31_cell_x_1.Location = new System.Drawing.Point(6, 33);
            this.panel_31_cell_x_1.Name = "panel_31_cell_x_1";
            this.panel_31_cell_x_1.Size = new System.Drawing.Size(275, 35);
            this.panel_31_cell_x_1.TabIndex = 0;
            // 
            // lb_31_cell_x_1
            // 
            this.lb_31_cell_x_1.AutoSize = true;
            this.lb_31_cell_x_1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_31_cell_x_1.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_31_cell_x_1.Location = new System.Drawing.Point(92, 11);
            this.lb_31_cell_x_1.Name = "lb_31_cell_x_1";
            this.lb_31_cell_x_1.Size = new System.Drawing.Size(89, 12);
            this.lb_31_cell_x_1.TabIndex = 0;
            this.lb_31_cell_x_1.Text = "31.CELL_X_1";
            // 
            // gbox_process
            // 
            this.gbox_process.Controls.Add(this.panel_18_break_z_1);
            this.gbox_process.Controls.Add(this.panel_21_break_z_4);
            this.gbox_process.Controls.Add(this.panel_11_break_x_1);
            this.gbox_process.Controls.Add(this.panel_20_break_z_3);
            this.gbox_process.Controls.Add(this.panel_19_break_z_2);
            this.gbox_process.Controls.Add(this.panel_14_break_x_4);
            this.gbox_process.Controls.Add(this.panel_07_cell_trans_y_1);
            this.gbox_process.Controls.Add(this.panel_13_break_x_3);
            this.gbox_process.Controls.Add(this.panel_12_break_x_2);
            this.gbox_process.Controls.Add(this.panel_10_break_table_y_1);
            this.gbox_process.Controls.Add(this.panel_17_head_z_1);
            this.gbox_process.Controls.Add(this.panel_09_break_table_y_1);
            this.gbox_process.Controls.Add(this.panel_22_masure_x_1);
            this.gbox_process.Controls.Add(this.panel_06_cell_trans_x_2);
            this.gbox_process.Controls.Add(this.panel_03_head_x_1);
            this.gbox_process.Controls.Add(this.panel_05_cell_trans_x_1);
            this.gbox_process.Controls.Add(this.panel_04_align_x_1);
            this.gbox_process.Controls.Add(this.panel_02_table_y_2);
            this.gbox_process.Controls.Add(this.panel_01_table_y_1);
            this.gbox_process.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_process.ForeColor = System.Drawing.Color.White;
            this.gbox_process.Location = new System.Drawing.Point(585, 409);
            this.gbox_process.Name = "gbox_process";
            this.gbox_process.Size = new System.Drawing.Size(570, 448);
            this.gbox_process.TabIndex = 15;
            this.gbox_process.TabStop = false;
            this.gbox_process.Text = "프로세스";
            // 
            // panel_18_break_z_1
            // 
            this.panel_18_break_z_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_18_break_z_1.Controls.Add(this.lb_18_break_z_1);
            this.panel_18_break_z_1.Location = new System.Drawing.Point(289, 320);
            this.panel_18_break_z_1.Name = "panel_18_break_z_1";
            this.panel_18_break_z_1.Size = new System.Drawing.Size(275, 35);
            this.panel_18_break_z_1.TabIndex = 26;
            // 
            // lb_18_break_z_1
            // 
            this.lb_18_break_z_1.AutoSize = true;
            this.lb_18_break_z_1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_18_break_z_1.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_18_break_z_1.Location = new System.Drawing.Point(87, 10);
            this.lb_18_break_z_1.Name = "lb_18_break_z_1";
            this.lb_18_break_z_1.Size = new System.Drawing.Size(99, 12);
            this.lb_18_break_z_1.TabIndex = 4;
            this.lb_18_break_z_1.Text = "18.BREAK_Z_1";
            // 
            // panel_21_break_z_4
            // 
            this.panel_21_break_z_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_21_break_z_4.Controls.Add(this.lb_21_break_z_4);
            this.panel_21_break_z_4.Location = new System.Drawing.Point(6, 402);
            this.panel_21_break_z_4.Name = "panel_21_break_z_4";
            this.panel_21_break_z_4.Size = new System.Drawing.Size(275, 35);
            this.panel_21_break_z_4.TabIndex = 22;
            // 
            // lb_21_break_z_4
            // 
            this.lb_21_break_z_4.AutoSize = true;
            this.lb_21_break_z_4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_21_break_z_4.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_21_break_z_4.Location = new System.Drawing.Point(87, 10);
            this.lb_21_break_z_4.Name = "lb_21_break_z_4";
            this.lb_21_break_z_4.Size = new System.Drawing.Size(99, 12);
            this.lb_21_break_z_4.TabIndex = 5;
            this.lb_21_break_z_4.Text = "21.BREAK_Z_4";
            // 
            // panel_11_break_x_1
            // 
            this.panel_11_break_x_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_11_break_x_1.Controls.Add(this.lb_11_break_x_1);
            this.panel_11_break_x_1.Location = new System.Drawing.Point(289, 238);
            this.panel_11_break_x_1.Name = "panel_11_break_x_1";
            this.panel_11_break_x_1.Size = new System.Drawing.Size(275, 35);
            this.panel_11_break_x_1.TabIndex = 25;
            // 
            // lb_11_break_x_1
            // 
            this.lb_11_break_x_1.AutoSize = true;
            this.lb_11_break_x_1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_11_break_x_1.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_11_break_x_1.Location = new System.Drawing.Point(87, 10);
            this.lb_11_break_x_1.Name = "lb_11_break_x_1";
            this.lb_11_break_x_1.Size = new System.Drawing.Size(99, 12);
            this.lb_11_break_x_1.TabIndex = 2;
            this.lb_11_break_x_1.Text = "11.BREAK_X_1";
            // 
            // panel_20_break_z_3
            // 
            this.panel_20_break_z_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_20_break_z_3.Controls.Add(this.lb_20_break_z_3);
            this.panel_20_break_z_3.Location = new System.Drawing.Point(289, 361);
            this.panel_20_break_z_3.Name = "panel_20_break_z_3";
            this.panel_20_break_z_3.Size = new System.Drawing.Size(275, 35);
            this.panel_20_break_z_3.TabIndex = 17;
            // 
            // lb_20_break_z_3
            // 
            this.lb_20_break_z_3.AutoSize = true;
            this.lb_20_break_z_3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_20_break_z_3.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_20_break_z_3.Location = new System.Drawing.Point(87, 10);
            this.lb_20_break_z_3.Name = "lb_20_break_z_3";
            this.lb_20_break_z_3.Size = new System.Drawing.Size(99, 12);
            this.lb_20_break_z_3.TabIndex = 5;
            this.lb_20_break_z_3.Text = "20.BREAK_Z_3";
            // 
            // panel_19_break_z_2
            // 
            this.panel_19_break_z_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_19_break_z_2.Controls.Add(this.lb_19_break_z_2);
            this.panel_19_break_z_2.Location = new System.Drawing.Point(6, 361);
            this.panel_19_break_z_2.Name = "panel_19_break_z_2";
            this.panel_19_break_z_2.Size = new System.Drawing.Size(275, 35);
            this.panel_19_break_z_2.TabIndex = 15;
            // 
            // lb_19_break_z_2
            // 
            this.lb_19_break_z_2.AutoSize = true;
            this.lb_19_break_z_2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_19_break_z_2.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_19_break_z_2.Location = new System.Drawing.Point(87, 10);
            this.lb_19_break_z_2.Name = "lb_19_break_z_2";
            this.lb_19_break_z_2.Size = new System.Drawing.Size(99, 12);
            this.lb_19_break_z_2.TabIndex = 5;
            this.lb_19_break_z_2.Text = "19.BREAK_Z_2";
            // 
            // panel_14_break_x_4
            // 
            this.panel_14_break_x_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_14_break_x_4.Controls.Add(this.lb_14_break_x_4);
            this.panel_14_break_x_4.Location = new System.Drawing.Point(6, 320);
            this.panel_14_break_x_4.Name = "panel_14_break_x_4";
            this.panel_14_break_x_4.Size = new System.Drawing.Size(275, 35);
            this.panel_14_break_x_4.TabIndex = 23;
            // 
            // lb_14_break_x_4
            // 
            this.lb_14_break_x_4.AutoSize = true;
            this.lb_14_break_x_4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_14_break_x_4.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_14_break_x_4.Location = new System.Drawing.Point(87, 10);
            this.lb_14_break_x_4.Name = "lb_14_break_x_4";
            this.lb_14_break_x_4.Size = new System.Drawing.Size(99, 12);
            this.lb_14_break_x_4.TabIndex = 4;
            this.lb_14_break_x_4.Text = "14.BREAK_X_4";
            // 
            // panel_07_cell_trans_y_1
            // 
            this.panel_07_cell_trans_y_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_07_cell_trans_y_1.Controls.Add(this.lb_07_cell_trans_y_1);
            this.panel_07_cell_trans_y_1.Location = new System.Drawing.Point(289, 156);
            this.panel_07_cell_trans_y_1.Name = "panel_07_cell_trans_y_1";
            this.panel_07_cell_trans_y_1.Size = new System.Drawing.Size(275, 35);
            this.panel_07_cell_trans_y_1.TabIndex = 24;
            // 
            // lb_07_cell_trans_y_1
            // 
            this.lb_07_cell_trans_y_1.AutoSize = true;
            this.lb_07_cell_trans_y_1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_07_cell_trans_y_1.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_07_cell_trans_y_1.Location = new System.Drawing.Point(52, 10);
            this.lb_07_cell_trans_y_1.Name = "lb_07_cell_trans_y_1";
            this.lb_07_cell_trans_y_1.Size = new System.Drawing.Size(168, 12);
            this.lb_07_cell_trans_y_1.TabIndex = 5;
            this.lb_07_cell_trans_y_1.Text = "07.CELL_TRANSFER_Y_1";
            // 
            // panel_13_break_x_3
            // 
            this.panel_13_break_x_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_13_break_x_3.Controls.Add(this.lb_13_break_x_3);
            this.panel_13_break_x_3.Location = new System.Drawing.Point(289, 279);
            this.panel_13_break_x_3.Name = "panel_13_break_x_3";
            this.panel_13_break_x_3.Size = new System.Drawing.Size(275, 35);
            this.panel_13_break_x_3.TabIndex = 16;
            // 
            // lb_13_break_x_3
            // 
            this.lb_13_break_x_3.AutoSize = true;
            this.lb_13_break_x_3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_13_break_x_3.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_13_break_x_3.Location = new System.Drawing.Point(87, 10);
            this.lb_13_break_x_3.Name = "lb_13_break_x_3";
            this.lb_13_break_x_3.Size = new System.Drawing.Size(99, 12);
            this.lb_13_break_x_3.TabIndex = 3;
            this.lb_13_break_x_3.Text = "13.BREAK_X_3";
            // 
            // panel_12_break_x_2
            // 
            this.panel_12_break_x_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_12_break_x_2.Controls.Add(this.lb_12_break_x_2);
            this.panel_12_break_x_2.Location = new System.Drawing.Point(6, 279);
            this.panel_12_break_x_2.Name = "panel_12_break_x_2";
            this.panel_12_break_x_2.Size = new System.Drawing.Size(275, 35);
            this.panel_12_break_x_2.TabIndex = 14;
            // 
            // lb_12_break_x_2
            // 
            this.lb_12_break_x_2.AutoSize = true;
            this.lb_12_break_x_2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_12_break_x_2.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_12_break_x_2.Location = new System.Drawing.Point(87, 10);
            this.lb_12_break_x_2.Name = "lb_12_break_x_2";
            this.lb_12_break_x_2.Size = new System.Drawing.Size(99, 12);
            this.lb_12_break_x_2.TabIndex = 3;
            this.lb_12_break_x_2.Text = "12.BREAK_X_2";
            // 
            // panel_10_break_table_y_1
            // 
            this.panel_10_break_table_y_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_10_break_table_y_1.Controls.Add(this.lb_10_break_table_y_1);
            this.panel_10_break_table_y_1.Location = new System.Drawing.Point(6, 238);
            this.panel_10_break_table_y_1.Name = "panel_10_break_table_y_1";
            this.panel_10_break_table_y_1.Size = new System.Drawing.Size(275, 35);
            this.panel_10_break_table_y_1.TabIndex = 20;
            // 
            // lb_10_break_table_y_1
            // 
            this.lb_10_break_table_y_1.AutoSize = true;
            this.lb_10_break_table_y_1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_10_break_table_y_1.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_10_break_table_y_1.Location = new System.Drawing.Point(61, 10);
            this.lb_10_break_table_y_1.Name = "lb_10_break_table_y_1";
            this.lb_10_break_table_y_1.Size = new System.Drawing.Size(150, 12);
            this.lb_10_break_table_y_1.TabIndex = 7;
            this.lb_10_break_table_y_1.Text = "10.BREAK_TABLE_Y_1";
            // 
            // panel_17_head_z_1
            // 
            this.panel_17_head_z_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_17_head_z_1.Controls.Add(this.lb_17_head_z_1);
            this.panel_17_head_z_1.Location = new System.Drawing.Point(289, 74);
            this.panel_17_head_z_1.Name = "panel_17_head_z_1";
            this.panel_17_head_z_1.Size = new System.Drawing.Size(275, 35);
            this.panel_17_head_z_1.TabIndex = 11;
            // 
            // lb_17_head_z_1
            // 
            this.lb_17_head_z_1.AutoSize = true;
            this.lb_17_head_z_1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_17_head_z_1.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_17_head_z_1.Location = new System.Drawing.Point(91, 10);
            this.lb_17_head_z_1.Name = "lb_17_head_z_1";
            this.lb_17_head_z_1.Size = new System.Drawing.Size(90, 12);
            this.lb_17_head_z_1.TabIndex = 2;
            this.lb_17_head_z_1.Text = "17.HEAD_Z_1";
            // 
            // panel_09_break_table_y_1
            // 
            this.panel_09_break_table_y_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_09_break_table_y_1.Controls.Add(this.lb_09_break_table_y_1);
            this.panel_09_break_table_y_1.Location = new System.Drawing.Point(289, 197);
            this.panel_09_break_table_y_1.Name = "panel_09_break_table_y_1";
            this.panel_09_break_table_y_1.Size = new System.Drawing.Size(275, 35);
            this.panel_09_break_table_y_1.TabIndex = 18;
            // 
            // lb_09_break_table_y_1
            // 
            this.lb_09_break_table_y_1.AutoSize = true;
            this.lb_09_break_table_y_1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_09_break_table_y_1.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_09_break_table_y_1.Location = new System.Drawing.Point(61, 10);
            this.lb_09_break_table_y_1.Name = "lb_09_break_table_y_1";
            this.lb_09_break_table_y_1.Size = new System.Drawing.Size(150, 12);
            this.lb_09_break_table_y_1.TabIndex = 6;
            this.lb_09_break_table_y_1.Text = "09.BREAK_TABLE_Y_1";
            // 
            // panel_22_masure_x_1
            // 
            this.panel_22_masure_x_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_22_masure_x_1.Controls.Add(this.lb_22_masure_x_1);
            this.panel_22_masure_x_1.Location = new System.Drawing.Point(6, 197);
            this.panel_22_masure_x_1.Name = "panel_22_masure_x_1";
            this.panel_22_masure_x_1.Size = new System.Drawing.Size(275, 35);
            this.panel_22_masure_x_1.TabIndex = 13;
            // 
            // lb_22_masure_x_1
            // 
            this.lb_22_masure_x_1.AutoSize = true;
            this.lb_22_masure_x_1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_22_masure_x_1.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_22_masure_x_1.Location = new System.Drawing.Point(84, 10);
            this.lb_22_masure_x_1.Name = "lb_22_masure_x_1";
            this.lb_22_masure_x_1.Size = new System.Drawing.Size(111, 12);
            this.lb_22_masure_x_1.TabIndex = 3;
            this.lb_22_masure_x_1.Text = "22.MASURE_X_1";
            // 
            // panel_06_cell_trans_x_2
            // 
            this.panel_06_cell_trans_x_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_06_cell_trans_x_2.Controls.Add(this.lb_06_cell_trans_x_2);
            this.panel_06_cell_trans_x_2.Location = new System.Drawing.Point(6, 156);
            this.panel_06_cell_trans_x_2.Name = "panel_06_cell_trans_x_2";
            this.panel_06_cell_trans_x_2.Size = new System.Drawing.Size(275, 35);
            this.panel_06_cell_trans_x_2.TabIndex = 21;
            // 
            // lb_06_cell_trans_x_2
            // 
            this.lb_06_cell_trans_x_2.AutoSize = true;
            this.lb_06_cell_trans_x_2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_06_cell_trans_x_2.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_06_cell_trans_x_2.Location = new System.Drawing.Point(52, 10);
            this.lb_06_cell_trans_x_2.Name = "lb_06_cell_trans_x_2";
            this.lb_06_cell_trans_x_2.Size = new System.Drawing.Size(168, 12);
            this.lb_06_cell_trans_x_2.TabIndex = 4;
            this.lb_06_cell_trans_x_2.Text = "06.CELL_TRANSFER_X_2";
            // 
            // panel_03_head_x_1
            // 
            this.panel_03_head_x_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_03_head_x_1.Controls.Add(this.lb_03_head_x_1);
            this.panel_03_head_x_1.Location = new System.Drawing.Point(6, 74);
            this.panel_03_head_x_1.Name = "panel_03_head_x_1";
            this.panel_03_head_x_1.Size = new System.Drawing.Size(275, 35);
            this.panel_03_head_x_1.TabIndex = 10;
            // 
            // lb_03_head_x_1
            // 
            this.lb_03_head_x_1.AutoSize = true;
            this.lb_03_head_x_1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_03_head_x_1.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_03_head_x_1.Location = new System.Drawing.Point(92, 12);
            this.lb_03_head_x_1.Name = "lb_03_head_x_1";
            this.lb_03_head_x_1.Size = new System.Drawing.Size(90, 12);
            this.lb_03_head_x_1.TabIndex = 1;
            this.lb_03_head_x_1.Text = "03.HEAD_X_1";
            // 
            // panel_05_cell_trans_x_1
            // 
            this.panel_05_cell_trans_x_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_05_cell_trans_x_1.Controls.Add(this.lb_05_cell_trans_x_1);
            this.panel_05_cell_trans_x_1.Location = new System.Drawing.Point(289, 115);
            this.panel_05_cell_trans_x_1.Name = "panel_05_cell_trans_x_1";
            this.panel_05_cell_trans_x_1.Size = new System.Drawing.Size(275, 35);
            this.panel_05_cell_trans_x_1.TabIndex = 19;
            // 
            // lb_05_cell_trans_x_1
            // 
            this.lb_05_cell_trans_x_1.AutoSize = true;
            this.lb_05_cell_trans_x_1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_05_cell_trans_x_1.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_05_cell_trans_x_1.Location = new System.Drawing.Point(52, 10);
            this.lb_05_cell_trans_x_1.Name = "lb_05_cell_trans_x_1";
            this.lb_05_cell_trans_x_1.Size = new System.Drawing.Size(168, 12);
            this.lb_05_cell_trans_x_1.TabIndex = 3;
            this.lb_05_cell_trans_x_1.Text = "05.CELL_TRANSFER_X_1";
            // 
            // panel_04_align_x_1
            // 
            this.panel_04_align_x_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_04_align_x_1.Controls.Add(this.lb_04_align_x_1);
            this.panel_04_align_x_1.Location = new System.Drawing.Point(6, 115);
            this.panel_04_align_x_1.Name = "panel_04_align_x_1";
            this.panel_04_align_x_1.Size = new System.Drawing.Size(275, 35);
            this.panel_04_align_x_1.TabIndex = 12;
            // 
            // lb_04_align_x_1
            // 
            this.lb_04_align_x_1.AutoSize = true;
            this.lb_04_align_x_1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_04_align_x_1.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_04_align_x_1.Location = new System.Drawing.Point(91, 10);
            this.lb_04_align_x_1.Name = "lb_04_align_x_1";
            this.lb_04_align_x_1.Size = new System.Drawing.Size(95, 12);
            this.lb_04_align_x_1.TabIndex = 2;
            this.lb_04_align_x_1.Text = "04.ALIGN_X_1";
            // 
            // panel_02_table_y_2
            // 
            this.panel_02_table_y_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_02_table_y_2.Controls.Add(this.lb_02_table_y_2);
            this.panel_02_table_y_2.Location = new System.Drawing.Point(289, 33);
            this.panel_02_table_y_2.Name = "panel_02_table_y_2";
            this.panel_02_table_y_2.Size = new System.Drawing.Size(275, 35);
            this.panel_02_table_y_2.TabIndex = 9;
            // 
            // lb_02_table_y_2
            // 
            this.lb_02_table_y_2.AutoSize = true;
            this.lb_02_table_y_2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_02_table_y_2.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_02_table_y_2.Location = new System.Drawing.Point(90, 11);
            this.lb_02_table_y_2.Name = "lb_02_table_y_2";
            this.lb_02_table_y_2.Size = new System.Drawing.Size(98, 12);
            this.lb_02_table_y_2.TabIndex = 1;
            this.lb_02_table_y_2.Text = "02.TABLE_Y_2";
            // 
            // panel_01_table_y_1
            // 
            this.panel_01_table_y_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_01_table_y_1.Controls.Add(this.lb_01_table_y_1);
            this.panel_01_table_y_1.Location = new System.Drawing.Point(6, 33);
            this.panel_01_table_y_1.Name = "panel_01_table_y_1";
            this.panel_01_table_y_1.Size = new System.Drawing.Size(275, 35);
            this.panel_01_table_y_1.TabIndex = 8;
            // 
            // lb_01_table_y_1
            // 
            this.lb_01_table_y_1.AutoSize = true;
            this.lb_01_table_y_1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_01_table_y_1.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_01_table_y_1.Location = new System.Drawing.Point(89, 11);
            this.lb_01_table_y_1.Name = "lb_01_table_y_1";
            this.lb_01_table_y_1.Size = new System.Drawing.Size(98, 12);
            this.lb_01_table_y_1.TabIndex = 0;
            this.lb_01_table_y_1.Text = "01.TABLE_Y_1";
            // 
            // gbox_uld
            // 
            this.gbox_uld.Controls.Add(this.panel_4_break_table_t_4);
            this.gbox_uld.Controls.Add(this.panel_1_break_table_t_2);
            this.gbox_uld.Controls.Add(this.panel_2_break_table_t_3);
            this.gbox_uld.Controls.Add(this.panel_29_cell_y_2);
            this.gbox_uld.Controls.Add(this.panel_0_break_table_t_1);
            this.gbox_uld.Controls.Add(this.panel_23_cell_trans_t_3);
            this.gbox_uld.Controls.Add(this.panel_30_cst_rotation_down_right);
            this.gbox_uld.Controls.Add(this.panel_29_cst_rotation_up_right);
            this.gbox_uld.Controls.Add(this.panel_27_insp_y_1);
            this.gbox_uld.Controls.Add(this.panel_18_cst_electricty_z_left);
            this.gbox_uld.Controls.Add(this.panel_26_insp_z_1);
            this.gbox_uld.Controls.Add(this.panel_24_cell_trans_t_4);
            this.gbox_uld.Controls.Add(this.panel_21_cell_trans_t_2);
            this.gbox_uld.Controls.Add(this.panel_40_cell_x_1);
            this.gbox_uld.Controls.Add(this.panel_20_cell_trans_t_1);
            this.gbox_uld.Controls.Add(this.panel_19_cst_electricty_z_right);
            this.gbox_uld.Controls.Add(this.panel_17_cst_rotation_down_left);
            this.gbox_uld.Controls.Add(this.panel_39_insp_x_1);
            this.gbox_uld.Controls.Add(this.panel_16_cst_rotation_up_left);
            this.gbox_uld.Controls.Add(this.panel_41_cell_x_2);
            this.gbox_uld.Controls.Add(this.panel_38_cell_trans_y_2);
            this.gbox_uld.Controls.Add(this.panel_37_cell_trans_y_1);
            this.gbox_uld.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_uld.ForeColor = System.Drawing.Color.White;
            this.gbox_uld.Location = new System.Drawing.Point(1163, 409);
            this.gbox_uld.Name = "gbox_uld";
            this.gbox_uld.Size = new System.Drawing.Size(570, 448);
            this.gbox_uld.TabIndex = 16;
            this.gbox_uld.TabStop = false;
            this.gbox_uld.Text = "언로더";
            // 
            // panel_4_break_table_t_4
            // 
            this.panel_4_break_table_t_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_4_break_table_t_4.Controls.Add(this.lb_4_break_table_t_4);
            this.panel_4_break_table_t_4.Location = new System.Drawing.Point(289, 393);
            this.panel_4_break_table_t_4.Name = "panel_4_break_table_t_4";
            this.panel_4_break_table_t_4.Size = new System.Drawing.Size(275, 30);
            this.panel_4_break_table_t_4.TabIndex = 48;
            // 
            // lb_4_break_table_t_4
            // 
            this.lb_4_break_table_t_4.AutoSize = true;
            this.lb_4_break_table_t_4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_4_break_table_t_4.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_4_break_table_t_4.Location = new System.Drawing.Point(65, 8);
            this.lb_4_break_table_t_4.Name = "lb_4_break_table_t_4";
            this.lb_4_break_table_t_4.Size = new System.Drawing.Size(143, 12);
            this.lb_4_break_table_t_4.TabIndex = 8;
            this.lb_4_break_table_t_4.Text = "4.BREAK_TABLE_T_4";
            // 
            // panel_1_break_table_t_2
            // 
            this.panel_1_break_table_t_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_1_break_table_t_2.Controls.Add(this.lb_1_break_table_t_2);
            this.panel_1_break_table_t_2.Location = new System.Drawing.Point(289, 357);
            this.panel_1_break_table_t_2.Name = "panel_1_break_table_t_2";
            this.panel_1_break_table_t_2.Size = new System.Drawing.Size(275, 30);
            this.panel_1_break_table_t_2.TabIndex = 46;
            // 
            // lb_1_break_table_t_2
            // 
            this.lb_1_break_table_t_2.AutoSize = true;
            this.lb_1_break_table_t_2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_1_break_table_t_2.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_1_break_table_t_2.Location = new System.Drawing.Point(65, 8);
            this.lb_1_break_table_t_2.Name = "lb_1_break_table_t_2";
            this.lb_1_break_table_t_2.Size = new System.Drawing.Size(143, 12);
            this.lb_1_break_table_t_2.TabIndex = 6;
            this.lb_1_break_table_t_2.Text = "1.BREAK_TABLE_T_2";
            // 
            // panel_2_break_table_t_3
            // 
            this.panel_2_break_table_t_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_2_break_table_t_3.Controls.Add(this.lb_2_break_table_t_3);
            this.panel_2_break_table_t_3.Location = new System.Drawing.Point(6, 393);
            this.panel_2_break_table_t_3.Name = "panel_2_break_table_t_3";
            this.panel_2_break_table_t_3.Size = new System.Drawing.Size(275, 30);
            this.panel_2_break_table_t_3.TabIndex = 47;
            // 
            // lb_2_break_table_t_3
            // 
            this.lb_2_break_table_t_3.AutoSize = true;
            this.lb_2_break_table_t_3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_2_break_table_t_3.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_2_break_table_t_3.Location = new System.Drawing.Point(70, 8);
            this.lb_2_break_table_t_3.Name = "lb_2_break_table_t_3";
            this.lb_2_break_table_t_3.Size = new System.Drawing.Size(143, 12);
            this.lb_2_break_table_t_3.TabIndex = 7;
            this.lb_2_break_table_t_3.Text = "2.BREAK_TABLE_T_3";
            // 
            // panel_29_cell_y_2
            // 
            this.panel_29_cell_y_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_29_cell_y_2.Controls.Add(this.lb_29_cell_y_2);
            this.panel_29_cell_y_2.Location = new System.Drawing.Point(289, 285);
            this.panel_29_cell_y_2.Name = "panel_29_cell_y_2";
            this.panel_29_cell_y_2.Size = new System.Drawing.Size(275, 30);
            this.panel_29_cell_y_2.TabIndex = 45;
            // 
            // lb_29_cell_y_2
            // 
            this.lb_29_cell_y_2.AutoSize = true;
            this.lb_29_cell_y_2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_29_cell_y_2.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_29_cell_y_2.Location = new System.Drawing.Point(92, 8);
            this.lb_29_cell_y_2.Name = "lb_29_cell_y_2";
            this.lb_29_cell_y_2.Size = new System.Drawing.Size(89, 12);
            this.lb_29_cell_y_2.TabIndex = 5;
            this.lb_29_cell_y_2.Text = "28.CELL_Y_2";
            // 
            // panel_0_break_table_t_1
            // 
            this.panel_0_break_table_t_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_0_break_table_t_1.Controls.Add(this.lb_0_break_table_t_1);
            this.panel_0_break_table_t_1.Location = new System.Drawing.Point(6, 357);
            this.panel_0_break_table_t_1.Name = "panel_0_break_table_t_1";
            this.panel_0_break_table_t_1.Size = new System.Drawing.Size(275, 30);
            this.panel_0_break_table_t_1.TabIndex = 41;
            // 
            // lb_0_break_table_t_1
            // 
            this.lb_0_break_table_t_1.AutoSize = true;
            this.lb_0_break_table_t_1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_0_break_table_t_1.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_0_break_table_t_1.Location = new System.Drawing.Point(70, 8);
            this.lb_0_break_table_t_1.Name = "lb_0_break_table_t_1";
            this.lb_0_break_table_t_1.Size = new System.Drawing.Size(143, 12);
            this.lb_0_break_table_t_1.TabIndex = 5;
            this.lb_0_break_table_t_1.Text = "0.BREAK_TABLE_T_1";
            // 
            // panel_23_cell_trans_t_3
            // 
            this.panel_23_cell_trans_t_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_23_cell_trans_t_3.Controls.Add(this.lb_23_cell_trans_t_3);
            this.panel_23_cell_trans_t_3.Location = new System.Drawing.Point(289, 213);
            this.panel_23_cell_trans_t_3.Name = "panel_23_cell_trans_t_3";
            this.panel_23_cell_trans_t_3.Size = new System.Drawing.Size(275, 30);
            this.panel_23_cell_trans_t_3.TabIndex = 44;
            // 
            // lb_23_cell_trans_t_3
            // 
            this.lb_23_cell_trans_t_3.AutoSize = true;
            this.lb_23_cell_trans_t_3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_23_cell_trans_t_3.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_23_cell_trans_t_3.Location = new System.Drawing.Point(52, 8);
            this.lb_23_cell_trans_t_3.Name = "lb_23_cell_trans_t_3";
            this.lb_23_cell_trans_t_3.Size = new System.Drawing.Size(168, 12);
            this.lb_23_cell_trans_t_3.TabIndex = 4;
            this.lb_23_cell_trans_t_3.Text = "23.CELL_TRANSFER_T_3";
            // 
            // panel_30_cst_rotation_down_right
            // 
            this.panel_30_cst_rotation_down_right.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_30_cst_rotation_down_right.Controls.Add(this.lb_30_cst_rotation_down_right);
            this.panel_30_cst_rotation_down_right.Location = new System.Drawing.Point(289, 321);
            this.panel_30_cst_rotation_down_right.Name = "panel_30_cst_rotation_down_right";
            this.panel_30_cst_rotation_down_right.Size = new System.Drawing.Size(275, 30);
            this.panel_30_cst_rotation_down_right.TabIndex = 36;
            // 
            // lb_30_cst_rotation_down_right
            // 
            this.lb_30_cst_rotation_down_right.AutoSize = true;
            this.lb_30_cst_rotation_down_right.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_30_cst_rotation_down_right.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_30_cst_rotation_down_right.Location = new System.Drawing.Point(2, 8);
            this.lb_30_cst_rotation_down_right.Name = "lb_30_cst_rotation_down_right";
            this.lb_30_cst_rotation_down_right.Size = new System.Drawing.Size(269, 12);
            this.lb_30_cst_rotation_down_right.TabIndex = 7;
            this.lb_30_cst_rotation_down_right.Text = "30.CASSETTE_ROTATION_DOWN_RIGHT";
            // 
            // panel_29_cst_rotation_up_right
            // 
            this.panel_29_cst_rotation_up_right.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_29_cst_rotation_up_right.Controls.Add(this.lb_29_cst_rotation_up_right);
            this.panel_29_cst_rotation_up_right.Location = new System.Drawing.Point(6, 321);
            this.panel_29_cst_rotation_up_right.Name = "panel_29_cst_rotation_up_right";
            this.panel_29_cst_rotation_up_right.Size = new System.Drawing.Size(275, 30);
            this.panel_29_cst_rotation_up_right.TabIndex = 34;
            // 
            // lb_29_cst_rotation_up_right
            // 
            this.lb_29_cst_rotation_up_right.AutoSize = true;
            this.lb_29_cst_rotation_up_right.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_29_cst_rotation_up_right.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_29_cst_rotation_up_right.Location = new System.Drawing.Point(13, 8);
            this.lb_29_cst_rotation_up_right.Name = "lb_29_cst_rotation_up_right";
            this.lb_29_cst_rotation_up_right.Size = new System.Drawing.Size(247, 12);
            this.lb_29_cst_rotation_up_right.TabIndex = 7;
            this.lb_29_cst_rotation_up_right.Text = "29.CASSETTE_ROTATION_UP_RIGHT";
            // 
            // panel_27_insp_y_1
            // 
            this.panel_27_insp_y_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_27_insp_y_1.Controls.Add(this.lb_27_insp_y_1);
            this.panel_27_insp_y_1.Location = new System.Drawing.Point(6, 285);
            this.panel_27_insp_y_1.Name = "panel_27_insp_y_1";
            this.panel_27_insp_y_1.Size = new System.Drawing.Size(275, 30);
            this.panel_27_insp_y_1.TabIndex = 42;
            // 
            // lb_27_insp_y_1
            // 
            this.lb_27_insp_y_1.AutoSize = true;
            this.lb_27_insp_y_1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_27_insp_y_1.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_27_insp_y_1.Location = new System.Drawing.Point(93, 8);
            this.lb_27_insp_y_1.Name = "lb_27_insp_y_1";
            this.lb_27_insp_y_1.Size = new System.Drawing.Size(89, 12);
            this.lb_27_insp_y_1.TabIndex = 4;
            this.lb_27_insp_y_1.Text = "27.CELL_Y_1";
            // 
            // panel_18_cst_electricty_z_left
            // 
            this.panel_18_cst_electricty_z_left.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_18_cst_electricty_z_left.Controls.Add(this.lb_18_cst_electricty_z_left);
            this.panel_18_cst_electricty_z_left.Location = new System.Drawing.Point(289, 141);
            this.panel_18_cst_electricty_z_left.Name = "panel_18_cst_electricty_z_left";
            this.panel_18_cst_electricty_z_left.Size = new System.Drawing.Size(275, 30);
            this.panel_18_cst_electricty_z_left.TabIndex = 43;
            // 
            // lb_18_cst_electricty_z_left
            // 
            this.lb_18_cst_electricty_z_left.AutoSize = true;
            this.lb_18_cst_electricty_z_left.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_18_cst_electricty_z_left.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_18_cst_electricty_z_left.Location = new System.Drawing.Point(13, 8);
            this.lb_18_cst_electricty_z_left.Name = "lb_18_cst_electricty_z_left";
            this.lb_18_cst_electricty_z_left.Size = new System.Drawing.Size(247, 12);
            this.lb_18_cst_electricty_z_left.TabIndex = 8;
            this.lb_18_cst_electricty_z_left.Text = "18.CASSETTE_ELECTRICTY_Z_LEFT";
            // 
            // panel_26_insp_z_1
            // 
            this.panel_26_insp_z_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_26_insp_z_1.Controls.Add(this.lb_26_insp_z_1);
            this.panel_26_insp_z_1.Location = new System.Drawing.Point(289, 249);
            this.panel_26_insp_z_1.Name = "panel_26_insp_z_1";
            this.panel_26_insp_z_1.Size = new System.Drawing.Size(275, 30);
            this.panel_26_insp_z_1.TabIndex = 35;
            // 
            // lb_26_insp_z_1
            // 
            this.lb_26_insp_z_1.AutoSize = true;
            this.lb_26_insp_z_1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_26_insp_z_1.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_26_insp_z_1.Location = new System.Drawing.Point(95, 8);
            this.lb_26_insp_z_1.Name = "lb_26_insp_z_1";
            this.lb_26_insp_z_1.Size = new System.Drawing.Size(86, 12);
            this.lb_26_insp_z_1.TabIndex = 3;
            this.lb_26_insp_z_1.Text = "26.INSP_Z_1";
            // 
            // panel_24_cell_trans_t_4
            // 
            this.panel_24_cell_trans_t_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_24_cell_trans_t_4.Controls.Add(this.lb_24_cell_trans_t_4);
            this.panel_24_cell_trans_t_4.Location = new System.Drawing.Point(6, 249);
            this.panel_24_cell_trans_t_4.Name = "panel_24_cell_trans_t_4";
            this.panel_24_cell_trans_t_4.Size = new System.Drawing.Size(275, 30);
            this.panel_24_cell_trans_t_4.TabIndex = 33;
            // 
            // lb_24_cell_trans_t_4
            // 
            this.lb_24_cell_trans_t_4.AutoSize = true;
            this.lb_24_cell_trans_t_4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_24_cell_trans_t_4.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_24_cell_trans_t_4.Location = new System.Drawing.Point(52, 8);
            this.lb_24_cell_trans_t_4.Name = "lb_24_cell_trans_t_4";
            this.lb_24_cell_trans_t_4.Size = new System.Drawing.Size(168, 12);
            this.lb_24_cell_trans_t_4.TabIndex = 5;
            this.lb_24_cell_trans_t_4.Text = "24.CELL_TRANSFER_T_4";
            // 
            // panel_21_cell_trans_t_2
            // 
            this.panel_21_cell_trans_t_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_21_cell_trans_t_2.Controls.Add(this.lb_21_cell_trans_t_2);
            this.panel_21_cell_trans_t_2.Location = new System.Drawing.Point(6, 213);
            this.panel_21_cell_trans_t_2.Name = "panel_21_cell_trans_t_2";
            this.panel_21_cell_trans_t_2.Size = new System.Drawing.Size(275, 30);
            this.panel_21_cell_trans_t_2.TabIndex = 39;
            // 
            // lb_21_cell_trans_t_2
            // 
            this.lb_21_cell_trans_t_2.AutoSize = true;
            this.lb_21_cell_trans_t_2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_21_cell_trans_t_2.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_21_cell_trans_t_2.Location = new System.Drawing.Point(52, 8);
            this.lb_21_cell_trans_t_2.Name = "lb_21_cell_trans_t_2";
            this.lb_21_cell_trans_t_2.Size = new System.Drawing.Size(168, 12);
            this.lb_21_cell_trans_t_2.TabIndex = 4;
            this.lb_21_cell_trans_t_2.Text = "21.CELL_TRANSFER_T_2";
            // 
            // panel_40_cell_x_1
            // 
            this.panel_40_cell_x_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_40_cell_x_1.Controls.Add(this.label65);
            this.panel_40_cell_x_1.Location = new System.Drawing.Point(289, 69);
            this.panel_40_cell_x_1.Name = "panel_40_cell_x_1";
            this.panel_40_cell_x_1.Size = new System.Drawing.Size(275, 30);
            this.panel_40_cell_x_1.TabIndex = 30;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label65.ForeColor = System.Drawing.Color.LimeGreen;
            this.label65.Location = new System.Drawing.Point(93, 9);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(89, 12);
            this.label65.TabIndex = 2;
            this.label65.Text = "40.CELL_X_1";
            // 
            // panel_20_cell_trans_t_1
            // 
            this.panel_20_cell_trans_t_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_20_cell_trans_t_1.Controls.Add(this.lb_20_cell_trans_t_1);
            this.panel_20_cell_trans_t_1.Location = new System.Drawing.Point(289, 177);
            this.panel_20_cell_trans_t_1.Name = "panel_20_cell_trans_t_1";
            this.panel_20_cell_trans_t_1.Size = new System.Drawing.Size(275, 30);
            this.panel_20_cell_trans_t_1.TabIndex = 37;
            // 
            // lb_20_cell_trans_t_1
            // 
            this.lb_20_cell_trans_t_1.AutoSize = true;
            this.lb_20_cell_trans_t_1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_20_cell_trans_t_1.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_20_cell_trans_t_1.Location = new System.Drawing.Point(52, 8);
            this.lb_20_cell_trans_t_1.Name = "lb_20_cell_trans_t_1";
            this.lb_20_cell_trans_t_1.Size = new System.Drawing.Size(168, 12);
            this.lb_20_cell_trans_t_1.TabIndex = 3;
            this.lb_20_cell_trans_t_1.Text = "20.CELL_TRANSFER_T_1";
            // 
            // panel_19_cst_electricty_z_right
            // 
            this.panel_19_cst_electricty_z_right.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_19_cst_electricty_z_right.Controls.Add(this.lb_19_cst_electricty_z_right);
            this.panel_19_cst_electricty_z_right.Location = new System.Drawing.Point(6, 177);
            this.panel_19_cst_electricty_z_right.Name = "panel_19_cst_electricty_z_right";
            this.panel_19_cst_electricty_z_right.Size = new System.Drawing.Size(275, 30);
            this.panel_19_cst_electricty_z_right.TabIndex = 32;
            // 
            // lb_19_cst_electricty_z_right
            // 
            this.lb_19_cst_electricty_z_right.AutoSize = true;
            this.lb_19_cst_electricty_z_right.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_19_cst_electricty_z_right.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_19_cst_electricty_z_right.Location = new System.Drawing.Point(9, 8);
            this.lb_19_cst_electricty_z_right.Name = "lb_19_cst_electricty_z_right";
            this.lb_19_cst_electricty_z_right.Size = new System.Drawing.Size(254, 12);
            this.lb_19_cst_electricty_z_right.TabIndex = 9;
            this.lb_19_cst_electricty_z_right.Text = "19.CASSETTE_ELECTRICTY_Z_RIGHT";
            // 
            // panel_17_cst_rotation_down_left
            // 
            this.panel_17_cst_rotation_down_left.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_17_cst_rotation_down_left.Controls.Add(this.lb_17_cst_rotation_down_left);
            this.panel_17_cst_rotation_down_left.Location = new System.Drawing.Point(6, 141);
            this.panel_17_cst_rotation_down_left.Name = "panel_17_cst_rotation_down_left";
            this.panel_17_cst_rotation_down_left.Size = new System.Drawing.Size(275, 30);
            this.panel_17_cst_rotation_down_left.TabIndex = 40;
            // 
            // lb_17_cst_rotation_down_left
            // 
            this.lb_17_cst_rotation_down_left.AutoSize = true;
            this.lb_17_cst_rotation_down_left.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_17_cst_rotation_down_left.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_17_cst_rotation_down_left.Location = new System.Drawing.Point(5, 8);
            this.lb_17_cst_rotation_down_left.Name = "lb_17_cst_rotation_down_left";
            this.lb_17_cst_rotation_down_left.Size = new System.Drawing.Size(262, 12);
            this.lb_17_cst_rotation_down_left.TabIndex = 6;
            this.lb_17_cst_rotation_down_left.Text = "17.CASSETTE_ROTATION_DOWN_LEFT";
            // 
            // panel_39_insp_x_1
            // 
            this.panel_39_insp_x_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_39_insp_x_1.Controls.Add(this.lb_39_insp_x_1);
            this.panel_39_insp_x_1.Location = new System.Drawing.Point(6, 69);
            this.panel_39_insp_x_1.Name = "panel_39_insp_x_1";
            this.panel_39_insp_x_1.Size = new System.Drawing.Size(275, 30);
            this.panel_39_insp_x_1.TabIndex = 29;
            // 
            // lb_39_insp_x_1
            // 
            this.lb_39_insp_x_1.AutoSize = true;
            this.lb_39_insp_x_1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_39_insp_x_1.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_39_insp_x_1.Location = new System.Drawing.Point(92, 9);
            this.lb_39_insp_x_1.Name = "lb_39_insp_x_1";
            this.lb_39_insp_x_1.Size = new System.Drawing.Size(86, 12);
            this.lb_39_insp_x_1.TabIndex = 1;
            this.lb_39_insp_x_1.Text = "39.INSP_X_1";
            // 
            // panel_16_cst_rotation_up_left
            // 
            this.panel_16_cst_rotation_up_left.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_16_cst_rotation_up_left.Controls.Add(this.lb_16_cst_rotation_up_left);
            this.panel_16_cst_rotation_up_left.Location = new System.Drawing.Point(289, 105);
            this.panel_16_cst_rotation_up_left.Name = "panel_16_cst_rotation_up_left";
            this.panel_16_cst_rotation_up_left.Size = new System.Drawing.Size(275, 30);
            this.panel_16_cst_rotation_up_left.TabIndex = 38;
            // 
            // lb_16_cst_rotation_up_left
            // 
            this.lb_16_cst_rotation_up_left.AutoSize = true;
            this.lb_16_cst_rotation_up_left.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_16_cst_rotation_up_left.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_16_cst_rotation_up_left.Location = new System.Drawing.Point(16, 8);
            this.lb_16_cst_rotation_up_left.Name = "lb_16_cst_rotation_up_left";
            this.lb_16_cst_rotation_up_left.Size = new System.Drawing.Size(240, 12);
            this.lb_16_cst_rotation_up_left.TabIndex = 5;
            this.lb_16_cst_rotation_up_left.Text = "16.CASSETTE_ROTATION_UP_LEFT";
            // 
            // panel_41_cell_x_2
            // 
            this.panel_41_cell_x_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_41_cell_x_2.Controls.Add(this.lb_41_cell_x_2);
            this.panel_41_cell_x_2.Location = new System.Drawing.Point(6, 105);
            this.panel_41_cell_x_2.Name = "panel_41_cell_x_2";
            this.panel_41_cell_x_2.Size = new System.Drawing.Size(275, 30);
            this.panel_41_cell_x_2.TabIndex = 31;
            // 
            // lb_41_cell_x_2
            // 
            this.lb_41_cell_x_2.AutoSize = true;
            this.lb_41_cell_x_2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_41_cell_x_2.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_41_cell_x_2.Location = new System.Drawing.Point(92, 8);
            this.lb_41_cell_x_2.Name = "lb_41_cell_x_2";
            this.lb_41_cell_x_2.Size = new System.Drawing.Size(89, 12);
            this.lb_41_cell_x_2.TabIndex = 3;
            this.lb_41_cell_x_2.Text = "41.CELL_X_2";
            // 
            // panel_38_cell_trans_y_2
            // 
            this.panel_38_cell_trans_y_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_38_cell_trans_y_2.Controls.Add(this.lb_38_cell_trans_y_2);
            this.panel_38_cell_trans_y_2.Location = new System.Drawing.Point(289, 33);
            this.panel_38_cell_trans_y_2.Name = "panel_38_cell_trans_y_2";
            this.panel_38_cell_trans_y_2.Size = new System.Drawing.Size(275, 30);
            this.panel_38_cell_trans_y_2.TabIndex = 28;
            // 
            // lb_38_cell_trans_y_2
            // 
            this.lb_38_cell_trans_y_2.AutoSize = true;
            this.lb_38_cell_trans_y_2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_38_cell_trans_y_2.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_38_cell_trans_y_2.Location = new System.Drawing.Point(52, 7);
            this.lb_38_cell_trans_y_2.Name = "lb_38_cell_trans_y_2";
            this.lb_38_cell_trans_y_2.Size = new System.Drawing.Size(168, 12);
            this.lb_38_cell_trans_y_2.TabIndex = 7;
            this.lb_38_cell_trans_y_2.Text = "38.CELL_TRANSFER_Y_2";
            // 
            // panel_37_cell_trans_y_1
            // 
            this.panel_37_cell_trans_y_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_37_cell_trans_y_1.Controls.Add(this.lb_37_cell_trans_y_1);
            this.panel_37_cell_trans_y_1.Location = new System.Drawing.Point(6, 33);
            this.panel_37_cell_trans_y_1.Name = "panel_37_cell_trans_y_1";
            this.panel_37_cell_trans_y_1.Size = new System.Drawing.Size(275, 30);
            this.panel_37_cell_trans_y_1.TabIndex = 27;
            // 
            // lb_37_cell_trans_y_1
            // 
            this.lb_37_cell_trans_y_1.AutoSize = true;
            this.lb_37_cell_trans_y_1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_37_cell_trans_y_1.ForeColor = System.Drawing.Color.LimeGreen;
            this.lb_37_cell_trans_y_1.Location = new System.Drawing.Point(52, 7);
            this.lb_37_cell_trans_y_1.Name = "lb_37_cell_trans_y_1";
            this.lb_37_cell_trans_y_1.Size = new System.Drawing.Size(168, 12);
            this.lb_37_cell_trans_y_1.TabIndex = 6;
            this.lb_37_cell_trans_y_1.Text = "37.CELL_TRANSFER_Y_1";
            // 
            // Manager_Reset
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.Controls.Add(this.gbox_uld);
            this.Controls.Add(this.gbox_process);
            this.Controls.Add(this.gbox_ld);
            this.Controls.Add(this.panel_data_init);
            this.Controls.Add(this.panel_sscnet_reset);
            this.Controls.Add(this.pbar_all_percent);
            this.Controls.Add(this.panel_all_percent);
            this.Controls.Add(this.pbar_uld_percent);
            this.Controls.Add(this.panel_uld_percent);
            this.Controls.Add(this.pbar_process_percent);
            this.Controls.Add(this.panel_process_percent);
            this.Controls.Add(this.pbar_ld_percent);
            this.Controls.Add(this.panel_ld_percent);
            this.Controls.Add(this.panel1);
            this.Name = "Manager_Reset";
            this.Size = new System.Drawing.Size(1740, 860);
            this.panel1.ResumeLayout(false);
            this.panel_all_reset.ResumeLayout(false);
            this.panel_uld_reset.ResumeLayout(false);
            this.panel_process_reset.ResumeLayout(false);
            this.panel_ld_reset.ResumeLayout(false);
            this.panel_ld_percent.ResumeLayout(false);
            this.panel_ld_percent.PerformLayout();
            this.panel_process_percent.ResumeLayout(false);
            this.panel_process_percent.PerformLayout();
            this.panel_uld_percent.ResumeLayout(false);
            this.panel_uld_percent.PerformLayout();
            this.panel_all_percent.ResumeLayout(false);
            this.panel_all_percent.PerformLayout();
            this.panel_sscnet_reset.ResumeLayout(false);
            this.panel_sscnet_reset.PerformLayout();
            this.panel_data_init.ResumeLayout(false);
            this.panel_data_init.PerformLayout();
            this.gbox_ld.ResumeLayout(false);
            this.panel_14_cell_trans_y_4.ResumeLayout(false);
            this.panel_14_cell_trans_y_4.PerformLayout();
            this.panel_10_cell_y_1.ResumeLayout(false);
            this.panel_10_cell_y_1.PerformLayout();
            this.panel_15_cell_trans_t_2.ResumeLayout(false);
            this.panel_15_cell_trans_t_2.PerformLayout();
            this.panel_13_cell_y_2.ResumeLayout(false);
            this.panel_13_cell_y_2.PerformLayout();
            this.panel_06_cst_rotation_up_right.ResumeLayout(false);
            this.panel_06_cst_rotation_up_right.PerformLayout();
            this.panel_12_cell_trans_t_1.ResumeLayout(false);
            this.panel_12_cell_trans_t_1.PerformLayout();
            this.panel_11_cell_trans_y_3.ResumeLayout(false);
            this.panel_11_cell_trans_y_3.PerformLayout();
            this.panel_09_cst_electricty_z_right.ResumeLayout(false);
            this.panel_09_cst_electricty_z_right.PerformLayout();
            this.panel_35_cell_trans_x_1.ResumeLayout(false);
            this.panel_35_cell_trans_x_1.PerformLayout();
            this.panel_08_cst_electricty_z_left.ResumeLayout(false);
            this.panel_08_cst_electricty_z_left.PerformLayout();
            this.panel_07_cst_rotation_down_right.ResumeLayout(false);
            this.panel_07_cst_rotation_down_right.PerformLayout();
            this.panel_05_cst_rotation_down_left.ResumeLayout(false);
            this.panel_05_cst_rotation_down_left.PerformLayout();
            this.panel_33_cell_trans_y_1.ResumeLayout(false);
            this.panel_33_cell_trans_y_1.PerformLayout();
            this.panel_04_cst_rotation_up_left.ResumeLayout(false);
            this.panel_04_cst_rotation_up_left.PerformLayout();
            this.panel_36_cell_trans_x_2.ResumeLayout(false);
            this.panel_36_cell_trans_x_2.PerformLayout();
            this.panel_32_cell_x_2.ResumeLayout(false);
            this.panel_32_cell_x_2.PerformLayout();
            this.panel_31_cell_x_1.ResumeLayout(false);
            this.panel_31_cell_x_1.PerformLayout();
            this.gbox_process.ResumeLayout(false);
            this.panel_18_break_z_1.ResumeLayout(false);
            this.panel_18_break_z_1.PerformLayout();
            this.panel_21_break_z_4.ResumeLayout(false);
            this.panel_21_break_z_4.PerformLayout();
            this.panel_11_break_x_1.ResumeLayout(false);
            this.panel_11_break_x_1.PerformLayout();
            this.panel_20_break_z_3.ResumeLayout(false);
            this.panel_20_break_z_3.PerformLayout();
            this.panel_19_break_z_2.ResumeLayout(false);
            this.panel_19_break_z_2.PerformLayout();
            this.panel_14_break_x_4.ResumeLayout(false);
            this.panel_14_break_x_4.PerformLayout();
            this.panel_07_cell_trans_y_1.ResumeLayout(false);
            this.panel_07_cell_trans_y_1.PerformLayout();
            this.panel_13_break_x_3.ResumeLayout(false);
            this.panel_13_break_x_3.PerformLayout();
            this.panel_12_break_x_2.ResumeLayout(false);
            this.panel_12_break_x_2.PerformLayout();
            this.panel_10_break_table_y_1.ResumeLayout(false);
            this.panel_10_break_table_y_1.PerformLayout();
            this.panel_17_head_z_1.ResumeLayout(false);
            this.panel_17_head_z_1.PerformLayout();
            this.panel_09_break_table_y_1.ResumeLayout(false);
            this.panel_09_break_table_y_1.PerformLayout();
            this.panel_22_masure_x_1.ResumeLayout(false);
            this.panel_22_masure_x_1.PerformLayout();
            this.panel_06_cell_trans_x_2.ResumeLayout(false);
            this.panel_06_cell_trans_x_2.PerformLayout();
            this.panel_03_head_x_1.ResumeLayout(false);
            this.panel_03_head_x_1.PerformLayout();
            this.panel_05_cell_trans_x_1.ResumeLayout(false);
            this.panel_05_cell_trans_x_1.PerformLayout();
            this.panel_04_align_x_1.ResumeLayout(false);
            this.panel_04_align_x_1.PerformLayout();
            this.panel_02_table_y_2.ResumeLayout(false);
            this.panel_02_table_y_2.PerformLayout();
            this.panel_01_table_y_1.ResumeLayout(false);
            this.panel_01_table_y_1.PerformLayout();
            this.gbox_uld.ResumeLayout(false);
            this.panel_4_break_table_t_4.ResumeLayout(false);
            this.panel_4_break_table_t_4.PerformLayout();
            this.panel_1_break_table_t_2.ResumeLayout(false);
            this.panel_1_break_table_t_2.PerformLayout();
            this.panel_2_break_table_t_3.ResumeLayout(false);
            this.panel_2_break_table_t_3.PerformLayout();
            this.panel_29_cell_y_2.ResumeLayout(false);
            this.panel_29_cell_y_2.PerformLayout();
            this.panel_0_break_table_t_1.ResumeLayout(false);
            this.panel_0_break_table_t_1.PerformLayout();
            this.panel_23_cell_trans_t_3.ResumeLayout(false);
            this.panel_23_cell_trans_t_3.PerformLayout();
            this.panel_30_cst_rotation_down_right.ResumeLayout(false);
            this.panel_30_cst_rotation_down_right.PerformLayout();
            this.panel_29_cst_rotation_up_right.ResumeLayout(false);
            this.panel_29_cst_rotation_up_right.PerformLayout();
            this.panel_27_insp_y_1.ResumeLayout(false);
            this.panel_27_insp_y_1.PerformLayout();
            this.panel_18_cst_electricty_z_left.ResumeLayout(false);
            this.panel_18_cst_electricty_z_left.PerformLayout();
            this.panel_26_insp_z_1.ResumeLayout(false);
            this.panel_26_insp_z_1.PerformLayout();
            this.panel_24_cell_trans_t_4.ResumeLayout(false);
            this.panel_24_cell_trans_t_4.PerformLayout();
            this.panel_21_cell_trans_t_2.ResumeLayout(false);
            this.panel_21_cell_trans_t_2.PerformLayout();
            this.panel_40_cell_x_1.ResumeLayout(false);
            this.panel_40_cell_x_1.PerformLayout();
            this.panel_20_cell_trans_t_1.ResumeLayout(false);
            this.panel_20_cell_trans_t_1.PerformLayout();
            this.panel_19_cst_electricty_z_right.ResumeLayout(false);
            this.panel_19_cst_electricty_z_right.PerformLayout();
            this.panel_17_cst_rotation_down_left.ResumeLayout(false);
            this.panel_17_cst_rotation_down_left.PerformLayout();
            this.panel_39_insp_x_1.ResumeLayout(false);
            this.panel_39_insp_x_1.PerformLayout();
            this.panel_16_cst_rotation_up_left.ResumeLayout(false);
            this.panel_16_cst_rotation_up_left.PerformLayout();
            this.panel_41_cell_x_2.ResumeLayout(false);
            this.panel_41_cell_x_2.PerformLayout();
            this.panel_38_cell_trans_y_2.ResumeLayout(false);
            this.panel_38_cell_trans_y_2.PerformLayout();
            this.panel_37_cell_trans_y_1.ResumeLayout(false);
            this.panel_37_cell_trans_y_1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel_ld_reset;
        private System.Windows.Forms.Label lb_ld_reset;
        private System.Windows.Forms.Panel panel_uld_reset;
        private System.Windows.Forms.Label lb_uld_reset;
        private System.Windows.Forms.Panel panel_process_reset;
        private System.Windows.Forms.Label lb_process_reset;
        private System.Windows.Forms.Panel panel_all_reset;
        private System.Windows.Forms.Label lb_all_reset;
        private System.Windows.Forms.Panel panel_ld_percent;
        private System.Windows.Forms.Label lb_ld_percent_info2;
        private System.Windows.Forms.Label lb_ld_percent;
        private System.Windows.Forms.Label lb_ld_percent_info;
        private System.Windows.Forms.ProgressBar pbar_ld_percent;
        private System.Windows.Forms.ProgressBar pbar_process_percent;
        private System.Windows.Forms.Panel panel_process_percent;
        private System.Windows.Forms.Label lb_process_percent_info2;
        private System.Windows.Forms.Label lb_process_percent;
        private System.Windows.Forms.Label lb_process_percent_info;
        private System.Windows.Forms.ProgressBar pbar_uld_percent;
        private System.Windows.Forms.Panel panel_uld_percent;
        private System.Windows.Forms.Label lb_uld_percent_info2;
        private System.Windows.Forms.Label lb_uld_percent;
        private System.Windows.Forms.Label lb_uld_percent_info;
        private System.Windows.Forms.Panel panel_all_percent;
        private System.Windows.Forms.Label lb_all_percent_info2;
        private System.Windows.Forms.Label lb_all_percent;
        private System.Windows.Forms.Label lb_all_percent_info;
        private System.Windows.Forms.ProgressBar pbar_all_percent;
        private System.Windows.Forms.Panel panel_sscnet_reset;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panel_data_init;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox gbox_ld;
        private System.Windows.Forms.GroupBox gbox_process;
        private System.Windows.Forms.GroupBox gbox_uld;
        private System.Windows.Forms.Panel panel_14_cell_trans_y_4;
        private System.Windows.Forms.Panel panel_10_cell_y_1;
        private System.Windows.Forms.Panel panel_15_cell_trans_t_2;
        private System.Windows.Forms.Panel panel_13_cell_y_2;
        private System.Windows.Forms.Panel panel_06_cst_rotation_up_right;
        private System.Windows.Forms.Panel panel_12_cell_trans_t_1;
        private System.Windows.Forms.Panel panel_11_cell_trans_y_3;
        private System.Windows.Forms.Panel panel_09_cst_electricty_z_right;
        private System.Windows.Forms.Panel panel_35_cell_trans_x_1;
        private System.Windows.Forms.Panel panel_08_cst_electricty_z_left;
        private System.Windows.Forms.Panel panel_07_cst_rotation_down_right;
        private System.Windows.Forms.Panel panel_05_cst_rotation_down_left;
        private System.Windows.Forms.Panel panel_33_cell_trans_y_1;
        private System.Windows.Forms.Panel panel_04_cst_rotation_up_left;
        private System.Windows.Forms.Panel panel_36_cell_trans_x_2;
        private System.Windows.Forms.Panel panel_32_cell_x_2;
        private System.Windows.Forms.Panel panel_31_cell_x_1;
        private System.Windows.Forms.Label lb_31_cell_x_1;
        private System.Windows.Forms.Label lb_33_cell_trans_y_1;
        private System.Windows.Forms.Label lb_32_cell_x_2;
        private System.Windows.Forms.Label lb_35_cell_trans_x_1;
        private System.Windows.Forms.Label lb_05_cst_rotation_down_left;
        private System.Windows.Forms.Label lb_04_cst_rotation_up_left;
        private System.Windows.Forms.Label lb_36_cell_trans_x_2;
        private System.Windows.Forms.Label lb_10_cell_y_1;
        private System.Windows.Forms.Label lb_06_cst_rotation_up_right;
        private System.Windows.Forms.Label lb_09_cst_electricty_z_right;
        private System.Windows.Forms.Label lb_08_cst_electricty_z_left;
        private System.Windows.Forms.Label lb_07_cst_rotation_down_right;
        private System.Windows.Forms.Label lb_14_cell_trans_y_4;
        private System.Windows.Forms.Label lb_15_cell_trans_t_2;
        private System.Windows.Forms.Label lb_13_cell_y_2;
        private System.Windows.Forms.Label lb_12_cell_trans_t_1;
        private System.Windows.Forms.Label lb_11_cell_trans_y_3;
        private System.Windows.Forms.Panel panel_21_break_z_4;
        private System.Windows.Forms.Panel panel_11_break_x_1;
        private System.Windows.Forms.Panel panel_20_break_z_3;
        private System.Windows.Forms.Panel panel_19_break_z_2;
        private System.Windows.Forms.Panel panel_14_break_x_4;
        private System.Windows.Forms.Panel panel_07_cell_trans_y_1;
        private System.Windows.Forms.Panel panel_13_break_x_3;
        private System.Windows.Forms.Panel panel_12_break_x_2;
        private System.Windows.Forms.Panel panel_10_break_table_y_1;
        private System.Windows.Forms.Panel panel_17_head_z_1;
        private System.Windows.Forms.Label lb_17_head_z_1;
        private System.Windows.Forms.Panel panel_09_break_table_y_1;
        private System.Windows.Forms.Panel panel_22_masure_x_1;
        private System.Windows.Forms.Panel panel_03_head_x_1;
        private System.Windows.Forms.Label lb_03_head_x_1;
        private System.Windows.Forms.Panel panel_05_cell_trans_x_1;
        private System.Windows.Forms.Panel panel_04_align_x_1;
        private System.Windows.Forms.Panel panel_02_table_y_2;
        private System.Windows.Forms.Label lb_02_table_y_2;
        private System.Windows.Forms.Panel panel_01_table_y_1;
        private System.Windows.Forms.Label lb_01_table_y_1;
        private System.Windows.Forms.Label lb_07_cell_trans_y_1;
        private System.Windows.Forms.Panel panel_06_cell_trans_x_2;
        private System.Windows.Forms.Label lb_06_cell_trans_x_2;
        private System.Windows.Forms.Label lb_05_cell_trans_x_1;
        private System.Windows.Forms.Label lb_04_align_x_1;
        private System.Windows.Forms.Panel panel_18_break_z_1;
        private System.Windows.Forms.Label lb_18_break_z_1;
        private System.Windows.Forms.Label lb_21_break_z_4;
        private System.Windows.Forms.Label lb_11_break_x_1;
        private System.Windows.Forms.Label lb_20_break_z_3;
        private System.Windows.Forms.Label lb_19_break_z_2;
        private System.Windows.Forms.Label lb_14_break_x_4;
        private System.Windows.Forms.Label lb_13_break_x_3;
        private System.Windows.Forms.Label lb_12_break_x_2;
        private System.Windows.Forms.Label lb_10_break_table_y_1;
        private System.Windows.Forms.Label lb_09_break_table_y_1;
        private System.Windows.Forms.Label lb_22_masure_x_1;
        private System.Windows.Forms.Panel panel_29_cell_y_2;
        private System.Windows.Forms.Panel panel_0_break_table_t_1;
        private System.Windows.Forms.Label lb_0_break_table_t_1;
        private System.Windows.Forms.Panel panel_23_cell_trans_t_3;
        private System.Windows.Forms.Panel panel_30_cst_rotation_down_right;
        private System.Windows.Forms.Panel panel_29_cst_rotation_up_right;
        private System.Windows.Forms.Panel panel_27_insp_y_1;
        private System.Windows.Forms.Panel panel_18_cst_electricty_z_left;
        private System.Windows.Forms.Panel panel_26_insp_z_1;
        private System.Windows.Forms.Label lb_26_insp_z_1;
        private System.Windows.Forms.Panel panel_24_cell_trans_t_4;
        private System.Windows.Forms.Panel panel_21_cell_trans_t_2;
        private System.Windows.Forms.Panel panel_40_cell_x_1;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Panel panel_20_cell_trans_t_1;
        private System.Windows.Forms.Panel panel_19_cst_electricty_z_right;
        private System.Windows.Forms.Panel panel_17_cst_rotation_down_left;
        private System.Windows.Forms.Panel panel_39_insp_x_1;
        private System.Windows.Forms.Label lb_39_insp_x_1;
        private System.Windows.Forms.Panel panel_16_cst_rotation_up_left;
        private System.Windows.Forms.Panel panel_41_cell_x_2;
        private System.Windows.Forms.Panel panel_38_cell_trans_y_2;
        private System.Windows.Forms.Label lb_38_cell_trans_y_2;
        private System.Windows.Forms.Panel panel_37_cell_trans_y_1;
        private System.Windows.Forms.Label lb_37_cell_trans_y_1;
        private System.Windows.Forms.Panel panel_4_break_table_t_4;
        private System.Windows.Forms.Label lb_4_break_table_t_4;
        private System.Windows.Forms.Panel panel_1_break_table_t_2;
        private System.Windows.Forms.Label lb_1_break_table_t_2;
        private System.Windows.Forms.Panel panel_2_break_table_t_3;
        private System.Windows.Forms.Label lb_2_break_table_t_3;
        private System.Windows.Forms.Label lb_29_cell_y_2;
        private System.Windows.Forms.Label lb_23_cell_trans_t_3;
        private System.Windows.Forms.Label lb_30_cst_rotation_down_right;
        private System.Windows.Forms.Label lb_29_cst_rotation_up_right;
        private System.Windows.Forms.Label lb_27_insp_y_1;
        private System.Windows.Forms.Label lb_18_cst_electricty_z_left;
        private System.Windows.Forms.Label lb_24_cell_trans_t_4;
        private System.Windows.Forms.Label lb_21_cell_trans_t_2;
        private System.Windows.Forms.Label lb_20_cell_trans_t_1;
        private System.Windows.Forms.Label lb_19_cst_electricty_z_right;
        private System.Windows.Forms.Label lb_17_cst_rotation_down_left;
        private System.Windows.Forms.Label lb_16_cst_rotation_up_left;
        private System.Windows.Forms.Label lb_41_cell_x_2;
    }
}
