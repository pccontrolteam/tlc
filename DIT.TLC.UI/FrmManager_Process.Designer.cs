﻿namespace DIT.TLC.UI
{
    partial class FrmManager_Process
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.btn_Process_Vision = new System.Windows.Forms.Button();
            this.btn_Process_Current = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btn_Process_B_R2Blow = new System.Windows.Forms.Button();
            this.btn_Process_B_R2Vac = new System.Windows.Forms.Button();
            this.btn_Process_B_R1Blow = new System.Windows.Forms.Button();
            this.btn_Process_B_R1Vac = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btn_Process_A_L2Blow = new System.Windows.Forms.Button();
            this.btn_Process_A_L2Vac = new System.Windows.Forms.Button();
            this.btn_Process_A_L1Blow = new System.Windows.Forms.Button();
            this.btn_Process_A_L1Vac = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.btn_Process_B_ProcessR3 = new System.Windows.Forms.Button();
            this.btn_Process_B_ProcessR1 = new System.Windows.Forms.Button();
            this.btn_Process_B_ProcessR2 = new System.Windows.Forms.Button();
            this.panel13 = new System.Windows.Forms.Panel();
            this.btn_Process_A_ProcessL3 = new System.Windows.Forms.Button();
            this.btn_Process_A_ProcessL2 = new System.Windows.Forms.Button();
            this.btn_Process_A_ProcessL1 = new System.Windows.Forms.Button();
            this.panel10 = new System.Windows.Forms.Panel();
            this.btn_Process_B_AlignR3 = new System.Windows.Forms.Button();
            this.btn_Process_B_AlignR1 = new System.Windows.Forms.Button();
            this.btn_Process_B_AlignR2 = new System.Windows.Forms.Button();
            this.panel11 = new System.Windows.Forms.Panel();
            this.btn_Process_A_AlignL3 = new System.Windows.Forms.Button();
            this.btn_Process_A_AlignL2 = new System.Windows.Forms.Button();
            this.btn_Process_A_AlignL1 = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btn_Break_A_L2Blow = new System.Windows.Forms.Button();
            this.btn_Break_A_L2Vac = new System.Windows.Forms.Button();
            this.btn_Break_A_L1Blow = new System.Windows.Forms.Button();
            this.btn_Break_A_L1Vac = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btn_Break_B_R2Blow = new System.Windows.Forms.Button();
            this.btn_Break_B_R2Vac = new System.Windows.Forms.Button();
            this.btn_Break_B_R1Blow = new System.Windows.Forms.Button();
            this.btn_Break_B_R1Vac = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.btn_Break_B_AlignR3 = new System.Windows.Forms.Button();
            this.btn_Break_B_AlignR1 = new System.Windows.Forms.Button();
            this.btn_Break_B_AlignR2 = new System.Windows.Forms.Button();
            this.panel15 = new System.Windows.Forms.Panel();
            this.btn_Break_A_AlignL3 = new System.Windows.Forms.Button();
            this.btn_Break_A_AlignL2 = new System.Windows.Forms.Button();
            this.btn_Break_A_AlignL1 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.btn_Break_B_ProcessR3 = new System.Windows.Forms.Button();
            this.btn_Break_B_ProcessR1 = new System.Windows.Forms.Button();
            this.btn_Break_B_ProcessR2 = new System.Windows.Forms.Button();
            this.panel17 = new System.Windows.Forms.Panel();
            this.btn_Break_A_ProcessL3 = new System.Windows.Forms.Button();
            this.btn_Break_A_ProcessL2 = new System.Windows.Forms.Button();
            this.btn_Break_A_ProcessL1 = new System.Windows.Forms.Button();
            this.panel19 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.btn_Move_BreakTable_RightUnload = new System.Windows.Forms.Button();
            this.btn_Move_BreakTable_LeftUnload = new System.Windows.Forms.Button();
            this.btn_Move_BreakTable_RightLoad = new System.Windows.Forms.Button();
            this.btn_Move_BreakTable_LeftLoad = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btn_Move_BreakTransfer_RightUnload = new System.Windows.Forms.Button();
            this.btn_Move_BreakTransfer_LeftUnload = new System.Windows.Forms.Button();
            this.btn_Move_BreakTransfer_RightLoad = new System.Windows.Forms.Button();
            this.btn_Move_BreakTransfer_LeftLoad = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btn_Move_Process_RightUnload = new System.Windows.Forms.Button();
            this.btn_Move_Process_LeftUnload = new System.Windows.Forms.Button();
            this.btn_Move_Process_RightLoad = new System.Windows.Forms.Button();
            this.btn_Move_Process_LeftLoad = new System.Windows.Forms.Button();
            this.panel21 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.tbox_Etc_TimeShot = new System.Windows.Forms.TextBox();
            this.btn_Etc_TimeShot = new System.Windows.Forms.Button();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.btn_Etc_Shutter_Close = new System.Windows.Forms.Button();
            this.btn_Etc_Shutter_Open = new System.Windows.Forms.Button();
            this.btn_Etc_OneProcess = new System.Windows.Forms.Button();
            this.btn_Etc_RepeatMove = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.btn_Close = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel22 = new System.Windows.Forms.Panel();
            this.btn_Align_IR2 = new System.Windows.Forms.Button();
            this.btn_Align_IR1 = new System.Windows.Forms.Button();
            this.btn_Align_IL2 = new System.Windows.Forms.Button();
            this.btn_Align_IL1 = new System.Windows.Forms.Button();
            this.btn_Align_Idle = new System.Windows.Forms.Button();
            this.btn_Align_TestInsp = new System.Windows.Forms.Button();
            this.btn_Align_PreAlign = new System.Windows.Forms.Button();
            this.tbox_Align_TotalInspCnt = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.tbox_Align_InspCount = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.panel23 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.panel24 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.panel25 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.panel26 = new System.Windows.Forms.Panel();
            this.label22 = new System.Windows.Forms.Label();
            this.panel27 = new System.Windows.Forms.Panel();
            this.label23 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel20.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel22.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel27.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox5);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(398, 684);
            this.panel1.TabIndex = 3;
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.CornflowerBlue;
            this.panel18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel18.Controls.Add(this.btn_Process_Vision);
            this.panel18.Controls.Add(this.btn_Process_Current);
            this.panel18.Location = new System.Drawing.Point(1, 459);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(386, 50);
            this.panel18.TabIndex = 51;
            // 
            // btn_Process_Vision
            // 
            this.btn_Process_Vision.BackColor = System.Drawing.Color.DimGray;
            this.btn_Process_Vision.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Process_Vision.ForeColor = System.Drawing.Color.White;
            this.btn_Process_Vision.Location = new System.Drawing.Point(203, 7);
            this.btn_Process_Vision.Name = "btn_Process_Vision";
            this.btn_Process_Vision.Size = new System.Drawing.Size(174, 33);
            this.btn_Process_Vision.TabIndex = 47;
            this.btn_Process_Vision.Text = "Vision";
            this.btn_Process_Vision.UseVisualStyleBackColor = false;
            // 
            // btn_Process_Current
            // 
            this.btn_Process_Current.BackColor = System.Drawing.Color.DimGray;
            this.btn_Process_Current.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Process_Current.ForeColor = System.Drawing.Color.White;
            this.btn_Process_Current.Location = new System.Drawing.Point(8, 7);
            this.btn_Process_Current.Name = "btn_Process_Current";
            this.btn_Process_Current.Size = new System.Drawing.Size(174, 33);
            this.btn_Process_Current.TabIndex = 46;
            this.btn_Process_Current.Text = "Current";
            this.btn_Process_Current.UseVisualStyleBackColor = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(80, -10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(26, 30);
            this.label5.TabIndex = 56;
            this.label5.Text = "B";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(80, -10);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(28, 30);
            this.label6.TabIndex = 55;
            this.label6.Text = "A";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(80, -9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 30);
            this.label2.TabIndex = 52;
            this.label2.Text = "B";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(80, -9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 30);
            this.label1.TabIndex = 15;
            this.label1.Text = "A";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Purple;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btn_Process_B_R2Blow);
            this.panel2.Controls.Add(this.btn_Process_B_R2Vac);
            this.panel2.Controls.Add(this.btn_Process_B_R1Blow);
            this.panel2.Controls.Add(this.btn_Process_B_R1Vac);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(197, 56);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(190, 225);
            this.panel2.TabIndex = 10;
            // 
            // btn_Process_B_R2Blow
            // 
            this.btn_Process_B_R2Blow.BackColor = System.Drawing.Color.DimGray;
            this.btn_Process_B_R2Blow.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Process_B_R2Blow.ForeColor = System.Drawing.Color.White;
            this.btn_Process_B_R2Blow.Location = new System.Drawing.Point(95, 140);
            this.btn_Process_B_R2Blow.Name = "btn_Process_B_R2Blow";
            this.btn_Process_B_R2Blow.Size = new System.Drawing.Size(85, 67);
            this.btn_Process_B_R2Blow.TabIndex = 42;
            this.btn_Process_B_R2Blow.Text = "R2 Blow";
            this.btn_Process_B_R2Blow.UseVisualStyleBackColor = false;
            // 
            // btn_Process_B_R2Vac
            // 
            this.btn_Process_B_R2Vac.BackColor = System.Drawing.Color.DimGray;
            this.btn_Process_B_R2Vac.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Process_B_R2Vac.ForeColor = System.Drawing.Color.White;
            this.btn_Process_B_R2Vac.Location = new System.Drawing.Point(6, 140);
            this.btn_Process_B_R2Vac.Name = "btn_Process_B_R2Vac";
            this.btn_Process_B_R2Vac.Size = new System.Drawing.Size(85, 67);
            this.btn_Process_B_R2Vac.TabIndex = 41;
            this.btn_Process_B_R2Vac.Text = "R2 Vac";
            this.btn_Process_B_R2Vac.UseVisualStyleBackColor = false;
            // 
            // btn_Process_B_R1Blow
            // 
            this.btn_Process_B_R1Blow.BackColor = System.Drawing.Color.DimGray;
            this.btn_Process_B_R1Blow.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Process_B_R1Blow.ForeColor = System.Drawing.Color.White;
            this.btn_Process_B_R1Blow.Location = new System.Drawing.Point(96, 40);
            this.btn_Process_B_R1Blow.Name = "btn_Process_B_R1Blow";
            this.btn_Process_B_R1Blow.Size = new System.Drawing.Size(85, 67);
            this.btn_Process_B_R1Blow.TabIndex = 40;
            this.btn_Process_B_R1Blow.Text = "R1 Blow";
            this.btn_Process_B_R1Blow.UseVisualStyleBackColor = false;
            // 
            // btn_Process_B_R1Vac
            // 
            this.btn_Process_B_R1Vac.BackColor = System.Drawing.Color.DimGray;
            this.btn_Process_B_R1Vac.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Process_B_R1Vac.ForeColor = System.Drawing.Color.White;
            this.btn_Process_B_R1Vac.Location = new System.Drawing.Point(7, 40);
            this.btn_Process_B_R1Vac.Name = "btn_Process_B_R1Vac";
            this.btn_Process_B_R1Vac.Size = new System.Drawing.Size(85, 67);
            this.btn_Process_B_R1Vac.TabIndex = 39;
            this.btn_Process_B_R1Vac.Text = "R1 Vac";
            this.btn_Process_B_R1Vac.UseVisualStyleBackColor = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.SlateGray;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.btn_Process_A_L2Blow);
            this.panel3.Controls.Add(this.btn_Process_A_L2Vac);
            this.panel3.Controls.Add(this.btn_Process_A_L1Blow);
            this.panel3.Controls.Add(this.btn_Process_A_L1Vac);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Location = new System.Drawing.Point(1, 56);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(190, 225);
            this.panel3.TabIndex = 1;
            // 
            // btn_Process_A_L2Blow
            // 
            this.btn_Process_A_L2Blow.BackColor = System.Drawing.Color.DimGray;
            this.btn_Process_A_L2Blow.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Process_A_L2Blow.ForeColor = System.Drawing.Color.White;
            this.btn_Process_A_L2Blow.Location = new System.Drawing.Point(95, 140);
            this.btn_Process_A_L2Blow.Name = "btn_Process_A_L2Blow";
            this.btn_Process_A_L2Blow.Size = new System.Drawing.Size(85, 67);
            this.btn_Process_A_L2Blow.TabIndex = 46;
            this.btn_Process_A_L2Blow.Text = "L2 Blow";
            this.btn_Process_A_L2Blow.UseVisualStyleBackColor = false;
            // 
            // btn_Process_A_L2Vac
            // 
            this.btn_Process_A_L2Vac.BackColor = System.Drawing.Color.DimGray;
            this.btn_Process_A_L2Vac.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Process_A_L2Vac.ForeColor = System.Drawing.Color.White;
            this.btn_Process_A_L2Vac.Location = new System.Drawing.Point(6, 140);
            this.btn_Process_A_L2Vac.Name = "btn_Process_A_L2Vac";
            this.btn_Process_A_L2Vac.Size = new System.Drawing.Size(85, 67);
            this.btn_Process_A_L2Vac.TabIndex = 45;
            this.btn_Process_A_L2Vac.Text = "L2 Vac";
            this.btn_Process_A_L2Vac.UseVisualStyleBackColor = false;
            // 
            // btn_Process_A_L1Blow
            // 
            this.btn_Process_A_L1Blow.BackColor = System.Drawing.Color.DimGray;
            this.btn_Process_A_L1Blow.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Process_A_L1Blow.ForeColor = System.Drawing.Color.White;
            this.btn_Process_A_L1Blow.Location = new System.Drawing.Point(96, 40);
            this.btn_Process_A_L1Blow.Name = "btn_Process_A_L1Blow";
            this.btn_Process_A_L1Blow.Size = new System.Drawing.Size(85, 67);
            this.btn_Process_A_L1Blow.TabIndex = 44;
            this.btn_Process_A_L1Blow.Text = "L1 Blow";
            this.btn_Process_A_L1Blow.UseVisualStyleBackColor = false;
            // 
            // btn_Process_A_L1Vac
            // 
            this.btn_Process_A_L1Vac.BackColor = System.Drawing.Color.DimGray;
            this.btn_Process_A_L1Vac.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Process_A_L1Vac.ForeColor = System.Drawing.Color.White;
            this.btn_Process_A_L1Vac.Location = new System.Drawing.Point(7, 40);
            this.btn_Process_A_L1Vac.Name = "btn_Process_A_L1Vac";
            this.btn_Process_A_L1Vac.Size = new System.Drawing.Size(85, 67);
            this.btn_Process_A_L1Vac.TabIndex = 43;
            this.btn_Process_A_L1Vac.Text = "L1 Vac";
            this.btn_Process_A_L1Vac.UseVisualStyleBackColor = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(80, -8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 30);
            this.label3.TabIndex = 54;
            this.label3.Text = "B";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(80, -8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 30);
            this.label4.TabIndex = 53;
            this.label4.Text = "A";
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.Purple;
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Controls.Add(this.btn_Process_B_ProcessR3);
            this.panel12.Controls.Add(this.label5);
            this.panel12.Controls.Add(this.btn_Process_B_ProcessR1);
            this.panel12.Controls.Add(this.btn_Process_B_ProcessR2);
            this.panel12.Location = new System.Drawing.Point(197, 515);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(190, 165);
            this.panel12.TabIndex = 50;
            // 
            // btn_Process_B_ProcessR3
            // 
            this.btn_Process_B_ProcessR3.BackColor = System.Drawing.Color.DimGray;
            this.btn_Process_B_ProcessR3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Process_B_ProcessR3.ForeColor = System.Drawing.Color.White;
            this.btn_Process_B_ProcessR3.Location = new System.Drawing.Point(7, 113);
            this.btn_Process_B_ProcessR3.Name = "btn_Process_B_ProcessR3";
            this.btn_Process_B_ProcessR3.Size = new System.Drawing.Size(174, 42);
            this.btn_Process_B_ProcessR3.TabIndex = 54;
            this.btn_Process_B_ProcessR3.Text = "Process R3";
            this.btn_Process_B_ProcessR3.UseVisualStyleBackColor = false;
            // 
            // btn_Process_B_ProcessR1
            // 
            this.btn_Process_B_ProcessR1.BackColor = System.Drawing.Color.DimGray;
            this.btn_Process_B_ProcessR1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Process_B_ProcessR1.ForeColor = System.Drawing.Color.White;
            this.btn_Process_B_ProcessR1.Location = new System.Drawing.Point(7, 21);
            this.btn_Process_B_ProcessR1.Name = "btn_Process_B_ProcessR1";
            this.btn_Process_B_ProcessR1.Size = new System.Drawing.Size(174, 42);
            this.btn_Process_B_ProcessR1.TabIndex = 52;
            this.btn_Process_B_ProcessR1.Text = "Process R1";
            this.btn_Process_B_ProcessR1.UseVisualStyleBackColor = false;
            // 
            // btn_Process_B_ProcessR2
            // 
            this.btn_Process_B_ProcessR2.BackColor = System.Drawing.Color.DimGray;
            this.btn_Process_B_ProcessR2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Process_B_ProcessR2.ForeColor = System.Drawing.Color.White;
            this.btn_Process_B_ProcessR2.Location = new System.Drawing.Point(7, 67);
            this.btn_Process_B_ProcessR2.Name = "btn_Process_B_ProcessR2";
            this.btn_Process_B_ProcessR2.Size = new System.Drawing.Size(174, 42);
            this.btn_Process_B_ProcessR2.TabIndex = 53;
            this.btn_Process_B_ProcessR2.Text = "Process R2";
            this.btn_Process_B_ProcessR2.UseVisualStyleBackColor = false;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.SlateGray;
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel13.Controls.Add(this.btn_Process_A_ProcessL3);
            this.panel13.Controls.Add(this.btn_Process_A_ProcessL2);
            this.panel13.Controls.Add(this.label6);
            this.panel13.Controls.Add(this.btn_Process_A_ProcessL1);
            this.panel13.Location = new System.Drawing.Point(1, 515);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(190, 165);
            this.panel13.TabIndex = 49;
            // 
            // btn_Process_A_ProcessL3
            // 
            this.btn_Process_A_ProcessL3.BackColor = System.Drawing.Color.DimGray;
            this.btn_Process_A_ProcessL3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Process_A_ProcessL3.ForeColor = System.Drawing.Color.White;
            this.btn_Process_A_ProcessL3.Location = new System.Drawing.Point(7, 113);
            this.btn_Process_A_ProcessL3.Name = "btn_Process_A_ProcessL3";
            this.btn_Process_A_ProcessL3.Size = new System.Drawing.Size(174, 42);
            this.btn_Process_A_ProcessL3.TabIndex = 48;
            this.btn_Process_A_ProcessL3.Text = "Process L3";
            this.btn_Process_A_ProcessL3.UseVisualStyleBackColor = false;
            // 
            // btn_Process_A_ProcessL2
            // 
            this.btn_Process_A_ProcessL2.BackColor = System.Drawing.Color.DimGray;
            this.btn_Process_A_ProcessL2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Process_A_ProcessL2.ForeColor = System.Drawing.Color.White;
            this.btn_Process_A_ProcessL2.Location = new System.Drawing.Point(7, 67);
            this.btn_Process_A_ProcessL2.Name = "btn_Process_A_ProcessL2";
            this.btn_Process_A_ProcessL2.Size = new System.Drawing.Size(174, 42);
            this.btn_Process_A_ProcessL2.TabIndex = 47;
            this.btn_Process_A_ProcessL2.Text = "Process L2";
            this.btn_Process_A_ProcessL2.UseVisualStyleBackColor = false;
            // 
            // btn_Process_A_ProcessL1
            // 
            this.btn_Process_A_ProcessL1.BackColor = System.Drawing.Color.DimGray;
            this.btn_Process_A_ProcessL1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Process_A_ProcessL1.ForeColor = System.Drawing.Color.White;
            this.btn_Process_A_ProcessL1.Location = new System.Drawing.Point(7, 21);
            this.btn_Process_A_ProcessL1.Name = "btn_Process_A_ProcessL1";
            this.btn_Process_A_ProcessL1.Size = new System.Drawing.Size(174, 42);
            this.btn_Process_A_ProcessL1.TabIndex = 46;
            this.btn_Process_A_ProcessL1.Text = "Process L1";
            this.btn_Process_A_ProcessL1.UseVisualStyleBackColor = false;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.Purple;
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Controls.Add(this.btn_Process_B_AlignR3);
            this.panel10.Controls.Add(this.btn_Process_B_AlignR1);
            this.panel10.Controls.Add(this.btn_Process_B_AlignR2);
            this.panel10.Controls.Add(this.label3);
            this.panel10.Location = new System.Drawing.Point(197, 287);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(190, 165);
            this.panel10.TabIndex = 48;
            // 
            // btn_Process_B_AlignR3
            // 
            this.btn_Process_B_AlignR3.BackColor = System.Drawing.Color.DimGray;
            this.btn_Process_B_AlignR3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Process_B_AlignR3.ForeColor = System.Drawing.Color.White;
            this.btn_Process_B_AlignR3.Location = new System.Drawing.Point(7, 115);
            this.btn_Process_B_AlignR3.Name = "btn_Process_B_AlignR3";
            this.btn_Process_B_AlignR3.Size = new System.Drawing.Size(174, 42);
            this.btn_Process_B_AlignR3.TabIndex = 48;
            this.btn_Process_B_AlignR3.Text = "Align R3";
            this.btn_Process_B_AlignR3.UseVisualStyleBackColor = false;
            // 
            // btn_Process_B_AlignR1
            // 
            this.btn_Process_B_AlignR1.BackColor = System.Drawing.Color.DimGray;
            this.btn_Process_B_AlignR1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Process_B_AlignR1.ForeColor = System.Drawing.Color.White;
            this.btn_Process_B_AlignR1.Location = new System.Drawing.Point(7, 23);
            this.btn_Process_B_AlignR1.Name = "btn_Process_B_AlignR1";
            this.btn_Process_B_AlignR1.Size = new System.Drawing.Size(174, 42);
            this.btn_Process_B_AlignR1.TabIndex = 46;
            this.btn_Process_B_AlignR1.Text = "Align R1";
            this.btn_Process_B_AlignR1.UseVisualStyleBackColor = false;
            // 
            // btn_Process_B_AlignR2
            // 
            this.btn_Process_B_AlignR2.BackColor = System.Drawing.Color.DimGray;
            this.btn_Process_B_AlignR2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Process_B_AlignR2.ForeColor = System.Drawing.Color.White;
            this.btn_Process_B_AlignR2.Location = new System.Drawing.Point(7, 69);
            this.btn_Process_B_AlignR2.Name = "btn_Process_B_AlignR2";
            this.btn_Process_B_AlignR2.Size = new System.Drawing.Size(174, 42);
            this.btn_Process_B_AlignR2.TabIndex = 47;
            this.btn_Process_B_AlignR2.Text = "Align R2";
            this.btn_Process_B_AlignR2.UseVisualStyleBackColor = false;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.SlateGray;
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.btn_Process_A_AlignL3);
            this.panel11.Controls.Add(this.btn_Process_A_AlignL2);
            this.panel11.Controls.Add(this.btn_Process_A_AlignL1);
            this.panel11.Controls.Add(this.label4);
            this.panel11.Location = new System.Drawing.Point(1, 287);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(190, 165);
            this.panel11.TabIndex = 47;
            // 
            // btn_Process_A_AlignL3
            // 
            this.btn_Process_A_AlignL3.BackColor = System.Drawing.Color.DimGray;
            this.btn_Process_A_AlignL3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Process_A_AlignL3.ForeColor = System.Drawing.Color.White;
            this.btn_Process_A_AlignL3.Location = new System.Drawing.Point(7, 115);
            this.btn_Process_A_AlignL3.Name = "btn_Process_A_AlignL3";
            this.btn_Process_A_AlignL3.Size = new System.Drawing.Size(174, 42);
            this.btn_Process_A_AlignL3.TabIndex = 45;
            this.btn_Process_A_AlignL3.Text = "Align L3";
            this.btn_Process_A_AlignL3.UseVisualStyleBackColor = false;
            // 
            // btn_Process_A_AlignL2
            // 
            this.btn_Process_A_AlignL2.BackColor = System.Drawing.Color.DimGray;
            this.btn_Process_A_AlignL2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Process_A_AlignL2.ForeColor = System.Drawing.Color.White;
            this.btn_Process_A_AlignL2.Location = new System.Drawing.Point(7, 69);
            this.btn_Process_A_AlignL2.Name = "btn_Process_A_AlignL2";
            this.btn_Process_A_AlignL2.Size = new System.Drawing.Size(174, 42);
            this.btn_Process_A_AlignL2.TabIndex = 44;
            this.btn_Process_A_AlignL2.Text = "Align L2";
            this.btn_Process_A_AlignL2.UseVisualStyleBackColor = false;
            // 
            // btn_Process_A_AlignL1
            // 
            this.btn_Process_A_AlignL1.BackColor = System.Drawing.Color.DimGray;
            this.btn_Process_A_AlignL1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Process_A_AlignL1.ForeColor = System.Drawing.Color.White;
            this.btn_Process_A_AlignL1.Location = new System.Drawing.Point(7, 23);
            this.btn_Process_A_AlignL1.Name = "btn_Process_A_AlignL1";
            this.btn_Process_A_AlignL1.Size = new System.Drawing.Size(174, 42);
            this.btn_Process_A_AlignL1.TabIndex = 43;
            this.btn_Process_A_AlignL1.Text = "Align L1";
            this.btn_Process_A_AlignL1.UseVisualStyleBackColor = false;
            // 
            // panel4
            // 
            this.panel4.Location = new System.Drawing.Point(416, 12);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(398, 684);
            this.panel4.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(82, -7);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(28, 30);
            this.label8.TabIndex = 57;
            this.label8.Text = "A";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.SlateGray;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.btn_Break_A_L2Blow);
            this.panel6.Controls.Add(this.label8);
            this.panel6.Controls.Add(this.btn_Break_A_L2Vac);
            this.panel6.Controls.Add(this.btn_Break_A_L1Blow);
            this.panel6.Controls.Add(this.btn_Break_A_L1Vac);
            this.panel6.Location = new System.Drawing.Point(2, 54);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(190, 266);
            this.panel6.TabIndex = 1;
            // 
            // btn_Break_A_L2Blow
            // 
            this.btn_Break_A_L2Blow.BackColor = System.Drawing.Color.DimGray;
            this.btn_Break_A_L2Blow.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Break_A_L2Blow.ForeColor = System.Drawing.Color.White;
            this.btn_Break_A_L2Blow.Location = new System.Drawing.Point(96, 149);
            this.btn_Break_A_L2Blow.Name = "btn_Break_A_L2Blow";
            this.btn_Break_A_L2Blow.Size = new System.Drawing.Size(85, 67);
            this.btn_Break_A_L2Blow.TabIndex = 50;
            this.btn_Break_A_L2Blow.Text = "L2 Blow";
            this.btn_Break_A_L2Blow.UseVisualStyleBackColor = false;
            // 
            // btn_Break_A_L2Vac
            // 
            this.btn_Break_A_L2Vac.BackColor = System.Drawing.Color.DimGray;
            this.btn_Break_A_L2Vac.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Break_A_L2Vac.ForeColor = System.Drawing.Color.White;
            this.btn_Break_A_L2Vac.Location = new System.Drawing.Point(7, 149);
            this.btn_Break_A_L2Vac.Name = "btn_Break_A_L2Vac";
            this.btn_Break_A_L2Vac.Size = new System.Drawing.Size(85, 67);
            this.btn_Break_A_L2Vac.TabIndex = 49;
            this.btn_Break_A_L2Vac.Text = "L2 Vac";
            this.btn_Break_A_L2Vac.UseVisualStyleBackColor = false;
            // 
            // btn_Break_A_L1Blow
            // 
            this.btn_Break_A_L1Blow.BackColor = System.Drawing.Color.DimGray;
            this.btn_Break_A_L1Blow.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Break_A_L1Blow.ForeColor = System.Drawing.Color.White;
            this.btn_Break_A_L1Blow.Location = new System.Drawing.Point(97, 49);
            this.btn_Break_A_L1Blow.Name = "btn_Break_A_L1Blow";
            this.btn_Break_A_L1Blow.Size = new System.Drawing.Size(85, 67);
            this.btn_Break_A_L1Blow.TabIndex = 48;
            this.btn_Break_A_L1Blow.Text = "L1 Blow";
            this.btn_Break_A_L1Blow.UseVisualStyleBackColor = false;
            // 
            // btn_Break_A_L1Vac
            // 
            this.btn_Break_A_L1Vac.BackColor = System.Drawing.Color.DimGray;
            this.btn_Break_A_L1Vac.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Break_A_L1Vac.ForeColor = System.Drawing.Color.White;
            this.btn_Break_A_L1Vac.Location = new System.Drawing.Point(8, 49);
            this.btn_Break_A_L1Vac.Name = "btn_Break_A_L1Vac";
            this.btn_Break_A_L1Vac.Size = new System.Drawing.Size(85, 67);
            this.btn_Break_A_L1Vac.TabIndex = 47;
            this.btn_Break_A_L1Vac.Text = "L1 Vac";
            this.btn_Break_A_L1Vac.UseVisualStyleBackColor = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(82, -7);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(26, 30);
            this.label7.TabIndex = 58;
            this.label7.Text = "B";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Purple;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.btn_Break_B_R2Blow);
            this.panel5.Controls.Add(this.btn_Break_B_R2Vac);
            this.panel5.Controls.Add(this.label7);
            this.panel5.Controls.Add(this.btn_Break_B_R1Blow);
            this.panel5.Controls.Add(this.btn_Break_B_R1Vac);
            this.panel5.Location = new System.Drawing.Point(198, 54);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(190, 266);
            this.panel5.TabIndex = 10;
            // 
            // btn_Break_B_R2Blow
            // 
            this.btn_Break_B_R2Blow.BackColor = System.Drawing.Color.DimGray;
            this.btn_Break_B_R2Blow.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Break_B_R2Blow.ForeColor = System.Drawing.Color.White;
            this.btn_Break_B_R2Blow.Location = new System.Drawing.Point(96, 149);
            this.btn_Break_B_R2Blow.Name = "btn_Break_B_R2Blow";
            this.btn_Break_B_R2Blow.Size = new System.Drawing.Size(85, 67);
            this.btn_Break_B_R2Blow.TabIndex = 46;
            this.btn_Break_B_R2Blow.Text = "R2 Blow";
            this.btn_Break_B_R2Blow.UseVisualStyleBackColor = false;
            // 
            // btn_Break_B_R2Vac
            // 
            this.btn_Break_B_R2Vac.BackColor = System.Drawing.Color.DimGray;
            this.btn_Break_B_R2Vac.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Break_B_R2Vac.ForeColor = System.Drawing.Color.White;
            this.btn_Break_B_R2Vac.Location = new System.Drawing.Point(7, 149);
            this.btn_Break_B_R2Vac.Name = "btn_Break_B_R2Vac";
            this.btn_Break_B_R2Vac.Size = new System.Drawing.Size(85, 67);
            this.btn_Break_B_R2Vac.TabIndex = 45;
            this.btn_Break_B_R2Vac.Text = "R2 Vac";
            this.btn_Break_B_R2Vac.UseVisualStyleBackColor = false;
            // 
            // btn_Break_B_R1Blow
            // 
            this.btn_Break_B_R1Blow.BackColor = System.Drawing.Color.DimGray;
            this.btn_Break_B_R1Blow.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Break_B_R1Blow.ForeColor = System.Drawing.Color.White;
            this.btn_Break_B_R1Blow.Location = new System.Drawing.Point(97, 49);
            this.btn_Break_B_R1Blow.Name = "btn_Break_B_R1Blow";
            this.btn_Break_B_R1Blow.Size = new System.Drawing.Size(85, 67);
            this.btn_Break_B_R1Blow.TabIndex = 44;
            this.btn_Break_B_R1Blow.Text = "R1 Blow";
            this.btn_Break_B_R1Blow.UseVisualStyleBackColor = false;
            // 
            // btn_Break_B_R1Vac
            // 
            this.btn_Break_B_R1Vac.BackColor = System.Drawing.Color.DimGray;
            this.btn_Break_B_R1Vac.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Break_B_R1Vac.ForeColor = System.Drawing.Color.White;
            this.btn_Break_B_R1Vac.Location = new System.Drawing.Point(8, 49);
            this.btn_Break_B_R1Vac.Name = "btn_Break_B_R1Vac";
            this.btn_Break_B_R1Vac.Size = new System.Drawing.Size(85, 67);
            this.btn_Break_B_R1Vac.TabIndex = 43;
            this.btn_Break_B_R1Vac.Text = "R1 Vac";
            this.btn_Break_B_R1Vac.UseVisualStyleBackColor = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(82, -8);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(26, 30);
            this.label9.TabIndex = 60;
            this.label9.Text = "B";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(82, -9);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(28, 30);
            this.label10.TabIndex = 59;
            this.label10.Text = "A";
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.Purple;
            this.panel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel14.Controls.Add(this.btn_Break_B_AlignR3);
            this.panel14.Controls.Add(this.btn_Break_B_AlignR1);
            this.panel14.Controls.Add(this.btn_Break_B_AlignR2);
            this.panel14.Controls.Add(this.label9);
            this.panel14.Location = new System.Drawing.Point(198, 335);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(190, 165);
            this.panel14.TabIndex = 50;
            // 
            // btn_Break_B_AlignR3
            // 
            this.btn_Break_B_AlignR3.BackColor = System.Drawing.Color.DimGray;
            this.btn_Break_B_AlignR3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Break_B_AlignR3.ForeColor = System.Drawing.Color.White;
            this.btn_Break_B_AlignR3.Location = new System.Drawing.Point(8, 111);
            this.btn_Break_B_AlignR3.Name = "btn_Break_B_AlignR3";
            this.btn_Break_B_AlignR3.Size = new System.Drawing.Size(174, 42);
            this.btn_Break_B_AlignR3.TabIndex = 51;
            this.btn_Break_B_AlignR3.Text = "Align R3";
            this.btn_Break_B_AlignR3.UseVisualStyleBackColor = false;
            // 
            // btn_Break_B_AlignR1
            // 
            this.btn_Break_B_AlignR1.BackColor = System.Drawing.Color.DimGray;
            this.btn_Break_B_AlignR1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Break_B_AlignR1.ForeColor = System.Drawing.Color.White;
            this.btn_Break_B_AlignR1.Location = new System.Drawing.Point(8, 19);
            this.btn_Break_B_AlignR1.Name = "btn_Break_B_AlignR1";
            this.btn_Break_B_AlignR1.Size = new System.Drawing.Size(174, 42);
            this.btn_Break_B_AlignR1.TabIndex = 49;
            this.btn_Break_B_AlignR1.Text = "Align R1";
            this.btn_Break_B_AlignR1.UseVisualStyleBackColor = false;
            // 
            // btn_Break_B_AlignR2
            // 
            this.btn_Break_B_AlignR2.BackColor = System.Drawing.Color.DimGray;
            this.btn_Break_B_AlignR2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Break_B_AlignR2.ForeColor = System.Drawing.Color.White;
            this.btn_Break_B_AlignR2.Location = new System.Drawing.Point(8, 65);
            this.btn_Break_B_AlignR2.Name = "btn_Break_B_AlignR2";
            this.btn_Break_B_AlignR2.Size = new System.Drawing.Size(174, 42);
            this.btn_Break_B_AlignR2.TabIndex = 50;
            this.btn_Break_B_AlignR2.Text = "Align R2";
            this.btn_Break_B_AlignR2.UseVisualStyleBackColor = false;
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.SlateGray;
            this.panel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel15.Controls.Add(this.btn_Break_A_AlignL3);
            this.panel15.Controls.Add(this.btn_Break_A_AlignL2);
            this.panel15.Controls.Add(this.btn_Break_A_AlignL1);
            this.panel15.Controls.Add(this.label10);
            this.panel15.Location = new System.Drawing.Point(2, 335);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(190, 165);
            this.panel15.TabIndex = 49;
            // 
            // btn_Break_A_AlignL3
            // 
            this.btn_Break_A_AlignL3.BackColor = System.Drawing.Color.DimGray;
            this.btn_Break_A_AlignL3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Break_A_AlignL3.ForeColor = System.Drawing.Color.White;
            this.btn_Break_A_AlignL3.Location = new System.Drawing.Point(8, 111);
            this.btn_Break_A_AlignL3.Name = "btn_Break_A_AlignL3";
            this.btn_Break_A_AlignL3.Size = new System.Drawing.Size(174, 42);
            this.btn_Break_A_AlignL3.TabIndex = 48;
            this.btn_Break_A_AlignL3.Text = "Align L3";
            this.btn_Break_A_AlignL3.UseVisualStyleBackColor = false;
            // 
            // btn_Break_A_AlignL2
            // 
            this.btn_Break_A_AlignL2.BackColor = System.Drawing.Color.DimGray;
            this.btn_Break_A_AlignL2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Break_A_AlignL2.ForeColor = System.Drawing.Color.White;
            this.btn_Break_A_AlignL2.Location = new System.Drawing.Point(8, 65);
            this.btn_Break_A_AlignL2.Name = "btn_Break_A_AlignL2";
            this.btn_Break_A_AlignL2.Size = new System.Drawing.Size(174, 42);
            this.btn_Break_A_AlignL2.TabIndex = 47;
            this.btn_Break_A_AlignL2.Text = "Align L2";
            this.btn_Break_A_AlignL2.UseVisualStyleBackColor = false;
            // 
            // btn_Break_A_AlignL1
            // 
            this.btn_Break_A_AlignL1.BackColor = System.Drawing.Color.DimGray;
            this.btn_Break_A_AlignL1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Break_A_AlignL1.ForeColor = System.Drawing.Color.White;
            this.btn_Break_A_AlignL1.Location = new System.Drawing.Point(8, 19);
            this.btn_Break_A_AlignL1.Name = "btn_Break_A_AlignL1";
            this.btn_Break_A_AlignL1.Size = new System.Drawing.Size(174, 42);
            this.btn_Break_A_AlignL1.TabIndex = 46;
            this.btn_Break_A_AlignL1.Text = "Align L1";
            this.btn_Break_A_AlignL1.UseVisualStyleBackColor = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(82, -8);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(26, 30);
            this.label11.TabIndex = 62;
            this.label11.Text = "B";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(82, -9);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(28, 30);
            this.label12.TabIndex = 61;
            this.label12.Text = "A";
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.Purple;
            this.panel16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel16.Controls.Add(this.btn_Break_B_ProcessR3);
            this.panel16.Controls.Add(this.btn_Break_B_ProcessR1);
            this.panel16.Controls.Add(this.btn_Break_B_ProcessR2);
            this.panel16.Controls.Add(this.label11);
            this.panel16.Location = new System.Drawing.Point(198, 516);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(190, 165);
            this.panel16.TabIndex = 52;
            // 
            // btn_Break_B_ProcessR3
            // 
            this.btn_Break_B_ProcessR3.BackColor = System.Drawing.Color.DimGray;
            this.btn_Break_B_ProcessR3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Break_B_ProcessR3.ForeColor = System.Drawing.Color.White;
            this.btn_Break_B_ProcessR3.Location = new System.Drawing.Point(8, 113);
            this.btn_Break_B_ProcessR3.Name = "btn_Break_B_ProcessR3";
            this.btn_Break_B_ProcessR3.Size = new System.Drawing.Size(174, 42);
            this.btn_Break_B_ProcessR3.TabIndex = 57;
            this.btn_Break_B_ProcessR3.Text = "Process R3";
            this.btn_Break_B_ProcessR3.UseVisualStyleBackColor = false;
            // 
            // btn_Break_B_ProcessR1
            // 
            this.btn_Break_B_ProcessR1.BackColor = System.Drawing.Color.DimGray;
            this.btn_Break_B_ProcessR1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Break_B_ProcessR1.ForeColor = System.Drawing.Color.White;
            this.btn_Break_B_ProcessR1.Location = new System.Drawing.Point(8, 21);
            this.btn_Break_B_ProcessR1.Name = "btn_Break_B_ProcessR1";
            this.btn_Break_B_ProcessR1.Size = new System.Drawing.Size(174, 42);
            this.btn_Break_B_ProcessR1.TabIndex = 55;
            this.btn_Break_B_ProcessR1.Text = "Process R1";
            this.btn_Break_B_ProcessR1.UseVisualStyleBackColor = false;
            // 
            // btn_Break_B_ProcessR2
            // 
            this.btn_Break_B_ProcessR2.BackColor = System.Drawing.Color.DimGray;
            this.btn_Break_B_ProcessR2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Break_B_ProcessR2.ForeColor = System.Drawing.Color.White;
            this.btn_Break_B_ProcessR2.Location = new System.Drawing.Point(8, 67);
            this.btn_Break_B_ProcessR2.Name = "btn_Break_B_ProcessR2";
            this.btn_Break_B_ProcessR2.Size = new System.Drawing.Size(174, 42);
            this.btn_Break_B_ProcessR2.TabIndex = 56;
            this.btn_Break_B_ProcessR2.Text = "Process R2";
            this.btn_Break_B_ProcessR2.UseVisualStyleBackColor = false;
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.SlateGray;
            this.panel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel17.Controls.Add(this.btn_Break_A_ProcessL3);
            this.panel17.Controls.Add(this.btn_Break_A_ProcessL2);
            this.panel17.Controls.Add(this.btn_Break_A_ProcessL1);
            this.panel17.Controls.Add(this.label12);
            this.panel17.Location = new System.Drawing.Point(2, 516);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(190, 165);
            this.panel17.TabIndex = 51;
            // 
            // btn_Break_A_ProcessL3
            // 
            this.btn_Break_A_ProcessL3.BackColor = System.Drawing.Color.DimGray;
            this.btn_Break_A_ProcessL3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Break_A_ProcessL3.ForeColor = System.Drawing.Color.White;
            this.btn_Break_A_ProcessL3.Location = new System.Drawing.Point(8, 113);
            this.btn_Break_A_ProcessL3.Name = "btn_Break_A_ProcessL3";
            this.btn_Break_A_ProcessL3.Size = new System.Drawing.Size(174, 42);
            this.btn_Break_A_ProcessL3.TabIndex = 51;
            this.btn_Break_A_ProcessL3.Text = "Process L3";
            this.btn_Break_A_ProcessL3.UseVisualStyleBackColor = false;
            // 
            // btn_Break_A_ProcessL2
            // 
            this.btn_Break_A_ProcessL2.BackColor = System.Drawing.Color.DimGray;
            this.btn_Break_A_ProcessL2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Break_A_ProcessL2.ForeColor = System.Drawing.Color.White;
            this.btn_Break_A_ProcessL2.Location = new System.Drawing.Point(8, 67);
            this.btn_Break_A_ProcessL2.Name = "btn_Break_A_ProcessL2";
            this.btn_Break_A_ProcessL2.Size = new System.Drawing.Size(174, 42);
            this.btn_Break_A_ProcessL2.TabIndex = 50;
            this.btn_Break_A_ProcessL2.Text = "Process L2";
            this.btn_Break_A_ProcessL2.UseVisualStyleBackColor = false;
            // 
            // btn_Break_A_ProcessL1
            // 
            this.btn_Break_A_ProcessL1.BackColor = System.Drawing.Color.DimGray;
            this.btn_Break_A_ProcessL1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Break_A_ProcessL1.ForeColor = System.Drawing.Color.White;
            this.btn_Break_A_ProcessL1.Location = new System.Drawing.Point(8, 21);
            this.btn_Break_A_ProcessL1.Name = "btn_Break_A_ProcessL1";
            this.btn_Break_A_ProcessL1.Size = new System.Drawing.Size(174, 42);
            this.btn_Break_A_ProcessL1.TabIndex = 49;
            this.btn_Break_A_ProcessL1.Text = "Process L1";
            this.btn_Break_A_ProcessL1.UseVisualStyleBackColor = false;
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.panel25);
            this.panel19.Controls.Add(this.groupBox3);
            this.panel19.Location = new System.Drawing.Point(820, 12);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(520, 300);
            this.panel19.TabIndex = 15;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.LightSteelBlue;
            this.label15.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(50, -8);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(69, 60);
            this.label15.TabIndex = 60;
            this.label15.Text = "Break\r\ntable";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Purple;
            this.label14.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(35, -6);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(91, 60);
            this.label14.TabIndex = 59;
            this.label14.Text = "Break\r\ntransfer";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.SlateGray;
            this.label13.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(46, 53);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(89, 30);
            this.label13.TabIndex = 58;
            this.label13.Text = "Process";
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.LightSteelBlue;
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.label15);
            this.panel9.Controls.Add(this.btn_Move_BreakTable_RightUnload);
            this.panel9.Controls.Add(this.btn_Move_BreakTable_LeftUnload);
            this.panel9.Controls.Add(this.btn_Move_BreakTable_RightLoad);
            this.panel9.Controls.Add(this.btn_Move_BreakTable_LeftLoad);
            this.panel9.Location = new System.Drawing.Point(347, 52);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(165, 238);
            this.panel9.TabIndex = 17;
            // 
            // btn_Move_BreakTable_RightUnload
            // 
            this.btn_Move_BreakTable_RightUnload.BackColor = System.Drawing.Color.DimGray;
            this.btn_Move_BreakTable_RightUnload.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Move_BreakTable_RightUnload.ForeColor = System.Drawing.Color.White;
            this.btn_Move_BreakTable_RightUnload.Location = new System.Drawing.Point(86, 141);
            this.btn_Move_BreakTable_RightUnload.Name = "btn_Move_BreakTable_RightUnload";
            this.btn_Move_BreakTable_RightUnload.Size = new System.Drawing.Size(71, 50);
            this.btn_Move_BreakTable_RightUnload.TabIndex = 58;
            this.btn_Move_BreakTable_RightUnload.Text = "Right Unload";
            this.btn_Move_BreakTable_RightUnload.UseVisualStyleBackColor = false;
            // 
            // btn_Move_BreakTable_LeftUnload
            // 
            this.btn_Move_BreakTable_LeftUnload.BackColor = System.Drawing.Color.DimGray;
            this.btn_Move_BreakTable_LeftUnload.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Move_BreakTable_LeftUnload.ForeColor = System.Drawing.Color.White;
            this.btn_Move_BreakTable_LeftUnload.Location = new System.Drawing.Point(9, 141);
            this.btn_Move_BreakTable_LeftUnload.Name = "btn_Move_BreakTable_LeftUnload";
            this.btn_Move_BreakTable_LeftUnload.Size = new System.Drawing.Size(71, 50);
            this.btn_Move_BreakTable_LeftUnload.TabIndex = 57;
            this.btn_Move_BreakTable_LeftUnload.Text = "Left Unload";
            this.btn_Move_BreakTable_LeftUnload.UseVisualStyleBackColor = false;
            // 
            // btn_Move_BreakTable_RightLoad
            // 
            this.btn_Move_BreakTable_RightLoad.BackColor = System.Drawing.Color.DimGray;
            this.btn_Move_BreakTable_RightLoad.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Move_BreakTable_RightLoad.ForeColor = System.Drawing.Color.White;
            this.btn_Move_BreakTable_RightLoad.Location = new System.Drawing.Point(86, 67);
            this.btn_Move_BreakTable_RightLoad.Name = "btn_Move_BreakTable_RightLoad";
            this.btn_Move_BreakTable_RightLoad.Size = new System.Drawing.Size(71, 50);
            this.btn_Move_BreakTable_RightLoad.TabIndex = 56;
            this.btn_Move_BreakTable_RightLoad.Text = "Right Load";
            this.btn_Move_BreakTable_RightLoad.UseVisualStyleBackColor = false;
            // 
            // btn_Move_BreakTable_LeftLoad
            // 
            this.btn_Move_BreakTable_LeftLoad.BackColor = System.Drawing.Color.DimGray;
            this.btn_Move_BreakTable_LeftLoad.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Move_BreakTable_LeftLoad.ForeColor = System.Drawing.Color.White;
            this.btn_Move_BreakTable_LeftLoad.Location = new System.Drawing.Point(9, 67);
            this.btn_Move_BreakTable_LeftLoad.Name = "btn_Move_BreakTable_LeftLoad";
            this.btn_Move_BreakTable_LeftLoad.Size = new System.Drawing.Size(71, 50);
            this.btn_Move_BreakTable_LeftLoad.TabIndex = 55;
            this.btn_Move_BreakTable_LeftLoad.Text = "  Left    Load";
            this.btn_Move_BreakTable_LeftLoad.UseVisualStyleBackColor = false;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Purple;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.btn_Move_BreakTransfer_RightUnload);
            this.panel8.Controls.Add(this.label14);
            this.panel8.Controls.Add(this.btn_Move_BreakTransfer_LeftUnload);
            this.panel8.Controls.Add(this.btn_Move_BreakTransfer_RightLoad);
            this.panel8.Controls.Add(this.btn_Move_BreakTransfer_LeftLoad);
            this.panel8.Location = new System.Drawing.Point(177, 52);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(165, 238);
            this.panel8.TabIndex = 16;
            // 
            // btn_Move_BreakTransfer_RightUnload
            // 
            this.btn_Move_BreakTransfer_RightUnload.BackColor = System.Drawing.Color.DimGray;
            this.btn_Move_BreakTransfer_RightUnload.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Move_BreakTransfer_RightUnload.ForeColor = System.Drawing.Color.White;
            this.btn_Move_BreakTransfer_RightUnload.Location = new System.Drawing.Point(85, 141);
            this.btn_Move_BreakTransfer_RightUnload.Name = "btn_Move_BreakTransfer_RightUnload";
            this.btn_Move_BreakTransfer_RightUnload.Size = new System.Drawing.Size(71, 50);
            this.btn_Move_BreakTransfer_RightUnload.TabIndex = 58;
            this.btn_Move_BreakTransfer_RightUnload.Text = "Right Unload";
            this.btn_Move_BreakTransfer_RightUnload.UseVisualStyleBackColor = false;
            // 
            // btn_Move_BreakTransfer_LeftUnload
            // 
            this.btn_Move_BreakTransfer_LeftUnload.BackColor = System.Drawing.Color.DimGray;
            this.btn_Move_BreakTransfer_LeftUnload.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Move_BreakTransfer_LeftUnload.ForeColor = System.Drawing.Color.White;
            this.btn_Move_BreakTransfer_LeftUnload.Location = new System.Drawing.Point(8, 141);
            this.btn_Move_BreakTransfer_LeftUnload.Name = "btn_Move_BreakTransfer_LeftUnload";
            this.btn_Move_BreakTransfer_LeftUnload.Size = new System.Drawing.Size(71, 50);
            this.btn_Move_BreakTransfer_LeftUnload.TabIndex = 57;
            this.btn_Move_BreakTransfer_LeftUnload.Text = "Left Unload";
            this.btn_Move_BreakTransfer_LeftUnload.UseVisualStyleBackColor = false;
            // 
            // btn_Move_BreakTransfer_RightLoad
            // 
            this.btn_Move_BreakTransfer_RightLoad.BackColor = System.Drawing.Color.DimGray;
            this.btn_Move_BreakTransfer_RightLoad.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Move_BreakTransfer_RightLoad.ForeColor = System.Drawing.Color.White;
            this.btn_Move_BreakTransfer_RightLoad.Location = new System.Drawing.Point(85, 67);
            this.btn_Move_BreakTransfer_RightLoad.Name = "btn_Move_BreakTransfer_RightLoad";
            this.btn_Move_BreakTransfer_RightLoad.Size = new System.Drawing.Size(71, 50);
            this.btn_Move_BreakTransfer_RightLoad.TabIndex = 56;
            this.btn_Move_BreakTransfer_RightLoad.Text = "Right Load";
            this.btn_Move_BreakTransfer_RightLoad.UseVisualStyleBackColor = false;
            // 
            // btn_Move_BreakTransfer_LeftLoad
            // 
            this.btn_Move_BreakTransfer_LeftLoad.BackColor = System.Drawing.Color.DimGray;
            this.btn_Move_BreakTransfer_LeftLoad.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Move_BreakTransfer_LeftLoad.ForeColor = System.Drawing.Color.White;
            this.btn_Move_BreakTransfer_LeftLoad.Location = new System.Drawing.Point(8, 67);
            this.btn_Move_BreakTransfer_LeftLoad.Name = "btn_Move_BreakTransfer_LeftLoad";
            this.btn_Move_BreakTransfer_LeftLoad.Size = new System.Drawing.Size(71, 50);
            this.btn_Move_BreakTransfer_LeftLoad.TabIndex = 55;
            this.btn_Move_BreakTransfer_LeftLoad.Text = "  Left    Load";
            this.btn_Move_BreakTransfer_LeftLoad.UseVisualStyleBackColor = false;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.SlateGray;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.btn_Move_Process_RightUnload);
            this.panel7.Controls.Add(this.btn_Move_Process_LeftUnload);
            this.panel7.Controls.Add(this.btn_Move_Process_RightLoad);
            this.panel7.Controls.Add(this.btn_Move_Process_LeftLoad);
            this.panel7.Location = new System.Drawing.Point(6, 52);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(165, 238);
            this.panel7.TabIndex = 15;
            // 
            // btn_Move_Process_RightUnload
            // 
            this.btn_Move_Process_RightUnload.BackColor = System.Drawing.Color.DimGray;
            this.btn_Move_Process_RightUnload.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Move_Process_RightUnload.ForeColor = System.Drawing.Color.White;
            this.btn_Move_Process_RightUnload.Location = new System.Drawing.Point(85, 141);
            this.btn_Move_Process_RightUnload.Name = "btn_Move_Process_RightUnload";
            this.btn_Move_Process_RightUnload.Size = new System.Drawing.Size(71, 50);
            this.btn_Move_Process_RightUnload.TabIndex = 54;
            this.btn_Move_Process_RightUnload.Text = "Right Unload";
            this.btn_Move_Process_RightUnload.UseVisualStyleBackColor = false;
            // 
            // btn_Move_Process_LeftUnload
            // 
            this.btn_Move_Process_LeftUnload.BackColor = System.Drawing.Color.DimGray;
            this.btn_Move_Process_LeftUnload.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Move_Process_LeftUnload.ForeColor = System.Drawing.Color.White;
            this.btn_Move_Process_LeftUnload.Location = new System.Drawing.Point(8, 141);
            this.btn_Move_Process_LeftUnload.Name = "btn_Move_Process_LeftUnload";
            this.btn_Move_Process_LeftUnload.Size = new System.Drawing.Size(71, 50);
            this.btn_Move_Process_LeftUnload.TabIndex = 53;
            this.btn_Move_Process_LeftUnload.Text = "Left Unload";
            this.btn_Move_Process_LeftUnload.UseVisualStyleBackColor = false;
            // 
            // btn_Move_Process_RightLoad
            // 
            this.btn_Move_Process_RightLoad.BackColor = System.Drawing.Color.DimGray;
            this.btn_Move_Process_RightLoad.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Move_Process_RightLoad.ForeColor = System.Drawing.Color.White;
            this.btn_Move_Process_RightLoad.Location = new System.Drawing.Point(85, 67);
            this.btn_Move_Process_RightLoad.Name = "btn_Move_Process_RightLoad";
            this.btn_Move_Process_RightLoad.Size = new System.Drawing.Size(71, 50);
            this.btn_Move_Process_RightLoad.TabIndex = 52;
            this.btn_Move_Process_RightLoad.Text = "Right Load";
            this.btn_Move_Process_RightLoad.UseVisualStyleBackColor = false;
            // 
            // btn_Move_Process_LeftLoad
            // 
            this.btn_Move_Process_LeftLoad.BackColor = System.Drawing.Color.DimGray;
            this.btn_Move_Process_LeftLoad.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Move_Process_LeftLoad.ForeColor = System.Drawing.Color.White;
            this.btn_Move_Process_LeftLoad.Location = new System.Drawing.Point(8, 67);
            this.btn_Move_Process_LeftLoad.Name = "btn_Move_Process_LeftLoad";
            this.btn_Move_Process_LeftLoad.Size = new System.Drawing.Size(71, 50);
            this.btn_Move_Process_LeftLoad.TabIndex = 51;
            this.btn_Move_Process_LeftLoad.Text = "  Left    Load";
            this.btn_Move_Process_LeftLoad.UseVisualStyleBackColor = false;
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.groupBox1);
            this.panel21.Location = new System.Drawing.Point(820, 511);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(520, 169);
            this.panel21.TabIndex = 62;
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.groupBox2);
            this.panel20.Location = new System.Drawing.Point(820, 320);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(520, 185);
            this.panel20.TabIndex = 63;
            // 
            // tbox_Etc_TimeShot
            // 
            this.tbox_Etc_TimeShot.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.tbox_Etc_TimeShot.Location = new System.Drawing.Point(149, 73);
            this.tbox_Etc_TimeShot.Name = "tbox_Etc_TimeShot";
            this.tbox_Etc_TimeShot.Size = new System.Drawing.Size(76, 25);
            this.tbox_Etc_TimeShot.TabIndex = 63;
            // 
            // btn_Etc_TimeShot
            // 
            this.btn_Etc_TimeShot.BackColor = System.Drawing.Color.DimGray;
            this.btn_Etc_TimeShot.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_Etc_TimeShot.ForeColor = System.Drawing.Color.White;
            this.btn_Etc_TimeShot.Location = new System.Drawing.Point(230, 66);
            this.btn_Etc_TimeShot.Name = "btn_Etc_TimeShot";
            this.btn_Etc_TimeShot.Size = new System.Drawing.Size(64, 36);
            this.btn_Etc_TimeShot.TabIndex = 62;
            this.btn_Etc_TimeShot.Text = "Shot";
            this.btn_Etc_TimeShot.UseVisualStyleBackColor = false;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.btn_Etc_Shutter_Close);
            this.groupBox9.Controls.Add(this.btn_Etc_Shutter_Open);
            this.groupBox9.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox9.ForeColor = System.Drawing.Color.White;
            this.groupBox9.Location = new System.Drawing.Point(305, 47);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(206, 125);
            this.groupBox9.TabIndex = 61;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Shutter";
            // 
            // btn_Etc_Shutter_Close
            // 
            this.btn_Etc_Shutter_Close.BackColor = System.Drawing.Color.DimGray;
            this.btn_Etc_Shutter_Close.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Etc_Shutter_Close.ForeColor = System.Drawing.Color.White;
            this.btn_Etc_Shutter_Close.Location = new System.Drawing.Point(111, 37);
            this.btn_Etc_Shutter_Close.Name = "btn_Etc_Shutter_Close";
            this.btn_Etc_Shutter_Close.Size = new System.Drawing.Size(80, 66);
            this.btn_Etc_Shutter_Close.TabIndex = 60;
            this.btn_Etc_Shutter_Close.Text = "Shutter Close";
            this.btn_Etc_Shutter_Close.UseVisualStyleBackColor = false;
            // 
            // btn_Etc_Shutter_Open
            // 
            this.btn_Etc_Shutter_Open.BackColor = System.Drawing.Color.DimGray;
            this.btn_Etc_Shutter_Open.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Etc_Shutter_Open.ForeColor = System.Drawing.Color.White;
            this.btn_Etc_Shutter_Open.Location = new System.Drawing.Point(18, 37);
            this.btn_Etc_Shutter_Open.Name = "btn_Etc_Shutter_Open";
            this.btn_Etc_Shutter_Open.Size = new System.Drawing.Size(80, 66);
            this.btn_Etc_Shutter_Open.TabIndex = 59;
            this.btn_Etc_Shutter_Open.Text = "Shutter Open";
            this.btn_Etc_Shutter_Open.UseVisualStyleBackColor = false;
            // 
            // btn_Etc_OneProcess
            // 
            this.btn_Etc_OneProcess.BackColor = System.Drawing.Color.DimGray;
            this.btn_Etc_OneProcess.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Etc_OneProcess.ForeColor = System.Drawing.Color.White;
            this.btn_Etc_OneProcess.Location = new System.Drawing.Point(161, 120);
            this.btn_Etc_OneProcess.Name = "btn_Etc_OneProcess";
            this.btn_Etc_OneProcess.Size = new System.Drawing.Size(128, 41);
            this.btn_Etc_OneProcess.TabIndex = 60;
            this.btn_Etc_OneProcess.Text = "One Process";
            this.btn_Etc_OneProcess.UseVisualStyleBackColor = false;
            // 
            // btn_Etc_RepeatMove
            // 
            this.btn_Etc_RepeatMove.BackColor = System.Drawing.Color.DimGray;
            this.btn_Etc_RepeatMove.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Etc_RepeatMove.ForeColor = System.Drawing.Color.White;
            this.btn_Etc_RepeatMove.Location = new System.Drawing.Point(15, 121);
            this.btn_Etc_RepeatMove.Name = "btn_Etc_RepeatMove";
            this.btn_Etc_RepeatMove.Size = new System.Drawing.Size(128, 41);
            this.btn_Etc_RepeatMove.TabIndex = 59;
            this.btn_Etc_RepeatMove.Text = "Repeat Move";
            this.btn_Etc_RepeatMove.UseVisualStyleBackColor = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(15, 72);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(107, 21);
            this.label17.TabIndex = 58;
            this.label17.Text = "Time Shot[s]";
            // 
            // btn_Close
            // 
            this.btn_Close.BackColor = System.Drawing.Color.DimGray;
            this.btn_Close.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Close.ForeColor = System.Drawing.Color.White;
            this.btn_Close.Location = new System.Drawing.Point(1207, 689);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(128, 31);
            this.btn_Close.TabIndex = 70;
            this.btn_Close.Text = "Close";
            this.btn_Close.UseVisualStyleBackColor = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel27);
            this.groupBox1.Controls.Add(this.panel22);
            this.groupBox1.Controls.Add(this.btn_Align_Idle);
            this.groupBox1.Controls.Add(this.btn_Align_TestInsp);
            this.groupBox1.Controls.Add(this.btn_Align_PreAlign);
            this.groupBox1.Controls.Add(this.tbox_Align_TotalInspCnt);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.tbox_Align_InspCount);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Location = new System.Drawing.Point(3, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(517, 166);
            this.groupBox1.TabIndex = 63;
            this.groupBox1.TabStop = false;
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.btn_Align_IR2);
            this.panel22.Controls.Add(this.btn_Align_IR1);
            this.panel22.Controls.Add(this.btn_Align_IL2);
            this.panel22.Controls.Add(this.btn_Align_IL1);
            this.panel22.Location = new System.Drawing.Point(158, 96);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(200, 65);
            this.panel22.TabIndex = 78;
            // 
            // btn_Align_IR2
            // 
            this.btn_Align_IR2.BackColor = System.Drawing.Color.DimGray;
            this.btn_Align_IR2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Align_IR2.ForeColor = System.Drawing.Color.White;
            this.btn_Align_IR2.Location = new System.Drawing.Point(156, 15);
            this.btn_Align_IR2.Name = "btn_Align_IR2";
            this.btn_Align_IR2.Size = new System.Drawing.Size(38, 41);
            this.btn_Align_IR2.TabIndex = 71;
            this.btn_Align_IR2.Text = "I   R2";
            this.btn_Align_IR2.UseVisualStyleBackColor = false;
            // 
            // btn_Align_IR1
            // 
            this.btn_Align_IR1.BackColor = System.Drawing.Color.DimGray;
            this.btn_Align_IR1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Align_IR1.ForeColor = System.Drawing.Color.White;
            this.btn_Align_IR1.Location = new System.Drawing.Point(106, 15);
            this.btn_Align_IR1.Name = "btn_Align_IR1";
            this.btn_Align_IR1.Size = new System.Drawing.Size(38, 41);
            this.btn_Align_IR1.TabIndex = 70;
            this.btn_Align_IR1.Text = "I   R1";
            this.btn_Align_IR1.UseVisualStyleBackColor = false;
            // 
            // btn_Align_IL2
            // 
            this.btn_Align_IL2.BackColor = System.Drawing.Color.DimGray;
            this.btn_Align_IL2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Align_IL2.ForeColor = System.Drawing.Color.White;
            this.btn_Align_IL2.Location = new System.Drawing.Point(56, 15);
            this.btn_Align_IL2.Name = "btn_Align_IL2";
            this.btn_Align_IL2.Size = new System.Drawing.Size(38, 41);
            this.btn_Align_IL2.TabIndex = 69;
            this.btn_Align_IL2.Text = "I   L2";
            this.btn_Align_IL2.UseVisualStyleBackColor = false;
            // 
            // btn_Align_IL1
            // 
            this.btn_Align_IL1.BackColor = System.Drawing.Color.DimGray;
            this.btn_Align_IL1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Align_IL1.ForeColor = System.Drawing.Color.White;
            this.btn_Align_IL1.Location = new System.Drawing.Point(6, 15);
            this.btn_Align_IL1.Name = "btn_Align_IL1";
            this.btn_Align_IL1.Size = new System.Drawing.Size(38, 41);
            this.btn_Align_IL1.TabIndex = 68;
            this.btn_Align_IL1.Text = "I   L1";
            this.btn_Align_IL1.UseVisualStyleBackColor = false;
            // 
            // btn_Align_Idle
            // 
            this.btn_Align_Idle.BackColor = System.Drawing.Color.DimGray;
            this.btn_Align_Idle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Align_Idle.ForeColor = System.Drawing.Color.White;
            this.btn_Align_Idle.Location = new System.Drawing.Point(11, 55);
            this.btn_Align_Idle.Name = "btn_Align_Idle";
            this.btn_Align_Idle.Size = new System.Drawing.Size(128, 41);
            this.btn_Align_Idle.TabIndex = 77;
            this.btn_Align_Idle.Text = "Idle";
            this.btn_Align_Idle.UseVisualStyleBackColor = false;
            // 
            // btn_Align_TestInsp
            // 
            this.btn_Align_TestInsp.BackColor = System.Drawing.Color.DimGray;
            this.btn_Align_TestInsp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Align_TestInsp.ForeColor = System.Drawing.Color.White;
            this.btn_Align_TestInsp.Location = new System.Drawing.Point(367, 111);
            this.btn_Align_TestInsp.Name = "btn_Align_TestInsp";
            this.btn_Align_TestInsp.Size = new System.Drawing.Size(128, 41);
            this.btn_Align_TestInsp.TabIndex = 76;
            this.btn_Align_TestInsp.Text = "TestInsp";
            this.btn_Align_TestInsp.UseVisualStyleBackColor = false;
            // 
            // btn_Align_PreAlign
            // 
            this.btn_Align_PreAlign.BackColor = System.Drawing.Color.DimGray;
            this.btn_Align_PreAlign.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Align_PreAlign.ForeColor = System.Drawing.Color.White;
            this.btn_Align_PreAlign.Location = new System.Drawing.Point(11, 111);
            this.btn_Align_PreAlign.Name = "btn_Align_PreAlign";
            this.btn_Align_PreAlign.Size = new System.Drawing.Size(128, 41);
            this.btn_Align_PreAlign.TabIndex = 75;
            this.btn_Align_PreAlign.Text = "PreAlign";
            this.btn_Align_PreAlign.UseVisualStyleBackColor = false;
            // 
            // tbox_Align_TotalInspCnt
            // 
            this.tbox_Align_TotalInspCnt.Font = new System.Drawing.Font("맑은 고딕", 15F, System.Drawing.FontStyle.Bold);
            this.tbox_Align_TotalInspCnt.Location = new System.Drawing.Point(450, 52);
            this.tbox_Align_TotalInspCnt.Name = "tbox_Align_TotalInspCnt";
            this.tbox_Align_TotalInspCnt.Size = new System.Drawing.Size(46, 34);
            this.tbox_Align_TotalInspCnt.TabIndex = 74;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(344, 59);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(106, 21);
            this.label18.TabIndex = 73;
            this.label18.Text = "TotalInspCnt";
            // 
            // tbox_Align_InspCount
            // 
            this.tbox_Align_InspCount.Font = new System.Drawing.Font("맑은 고딕", 15F, System.Drawing.FontStyle.Bold);
            this.tbox_Align_InspCount.Location = new System.Drawing.Point(254, 52);
            this.tbox_Align_InspCount.Name = "tbox_Align_InspCount";
            this.tbox_Align_InspCount.Size = new System.Drawing.Size(76, 34);
            this.tbox_Align_InspCount.TabIndex = 72;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(160, 59);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(88, 21);
            this.label16.TabIndex = 71;
            this.label16.Text = "InspCount";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.panel26);
            this.groupBox2.Controls.Add(this.tbox_Etc_TimeShot);
            this.groupBox2.Controls.Add(this.btn_Etc_TimeShot);
            this.groupBox2.Controls.Add(this.groupBox9);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.btn_Etc_OneProcess);
            this.groupBox2.Controls.Add(this.btn_Etc_RepeatMove);
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(517, 179);
            this.groupBox2.TabIndex = 71;
            this.groupBox2.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.panel9);
            this.groupBox3.Controls.Add(this.panel7);
            this.groupBox3.Controls.Add(this.panel8);
            this.groupBox3.Location = new System.Drawing.Point(3, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(517, 302);
            this.groupBox3.TabIndex = 71;
            this.groupBox3.TabStop = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.panel24);
            this.groupBox4.Controls.Add(this.panel6);
            this.groupBox4.Controls.Add(this.panel17);
            this.groupBox4.Controls.Add(this.panel5);
            this.groupBox4.Controls.Add(this.panel16);
            this.groupBox4.Controls.Add(this.panel14);
            this.groupBox4.Controls.Add(this.panel15);
            this.groupBox4.Location = new System.Drawing.Point(420, 10);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(390, 686);
            this.groupBox4.TabIndex = 72;
            this.groupBox4.TabStop = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.panel23);
            this.groupBox5.Controls.Add(this.panel18);
            this.groupBox5.Controls.Add(this.panel2);
            this.groupBox5.Controls.Add(this.panel11);
            this.groupBox5.Controls.Add(this.panel10);
            this.groupBox5.Controls.Add(this.panel3);
            this.groupBox5.Controls.Add(this.panel13);
            this.groupBox5.Controls.Add(this.panel12);
            this.groupBox5.Location = new System.Drawing.Point(7, -2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(390, 686);
            this.groupBox5.TabIndex = 73;
            this.groupBox5.TabStop = false;
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel23.Controls.Add(this.label19);
            this.panel23.ForeColor = System.Drawing.Color.White;
            this.panel23.Location = new System.Drawing.Point(2, 8);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(387, 38);
            this.panel23.TabIndex = 53;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold);
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(143, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(101, 32);
            this.label19.TabIndex = 53;
            this.label19.Text = "Process";
            // 
            // panel24
            // 
            this.panel24.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel24.Controls.Add(this.label20);
            this.panel24.ForeColor = System.Drawing.Color.White;
            this.panel24.Location = new System.Drawing.Point(1, 8);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(387, 38);
            this.panel24.TabIndex = 58;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold);
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(153, 1);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(78, 32);
            this.label20.TabIndex = 53;
            this.label20.Text = "Break";
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel25.Controls.Add(this.label21);
            this.panel25.ForeColor = System.Drawing.Color.White;
            this.panel25.Location = new System.Drawing.Point(4, 8);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(514, 38);
            this.panel25.TabIndex = 59;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold);
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(214, 3);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(79, 32);
            this.label21.TabIndex = 53;
            this.label21.Text = "Move";
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel26.Controls.Add(this.label22);
            this.panel26.ForeColor = System.Drawing.Color.White;
            this.panel26.Location = new System.Drawing.Point(2, 8);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(514, 38);
            this.panel26.TabIndex = 59;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold);
            this.label22.ForeColor = System.Drawing.Color.White;
            this.label22.Location = new System.Drawing.Point(227, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(49, 32);
            this.label22.TabIndex = 53;
            this.label22.Text = "Etc";
            // 
            // panel27
            // 
            this.panel27.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel27.Controls.Add(this.label23);
            this.panel27.ForeColor = System.Drawing.Color.White;
            this.panel27.Location = new System.Drawing.Point(1, 8);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(514, 38);
            this.panel27.TabIndex = 59;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold);
            this.label23.ForeColor = System.Drawing.Color.White;
            this.label23.Location = new System.Drawing.Point(217, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(76, 32);
            this.label23.TabIndex = 53;
            this.label23.Text = "Align";
            // 
            // FrmManager_Process
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.ClientSize = new System.Drawing.Size(1344, 729);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.btn_Close);
            this.Controls.Add(this.panel20);
            this.Controls.Add(this.panel21);
            this.Controls.Add(this.panel19);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel1);
            this.Name = "FrmManager_Process";
            this.Text = "FrmManager_Process";
            this.panel1.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.panel19.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel22.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            this.panel25.ResumeLayout(false);
            this.panel25.PerformLayout();
            this.panel26.ResumeLayout(false);
            this.panel26.PerformLayout();
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Button btn_Process_Vision;
        private System.Windows.Forms.Button btn_Process_Current;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Button btn_Process_B_ProcessR3;
        private System.Windows.Forms.Button btn_Process_B_ProcessR1;
        private System.Windows.Forms.Button btn_Process_B_ProcessR2;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Button btn_Process_A_ProcessL3;
        private System.Windows.Forms.Button btn_Process_A_ProcessL2;
        private System.Windows.Forms.Button btn_Process_A_ProcessL1;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Button btn_Process_B_AlignR3;
        private System.Windows.Forms.Button btn_Process_B_AlignR1;
        private System.Windows.Forms.Button btn_Process_B_AlignR2;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Button btn_Process_A_AlignL3;
        private System.Windows.Forms.Button btn_Process_A_AlignL2;
        private System.Windows.Forms.Button btn_Process_A_AlignL1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btn_Process_B_R2Blow;
        private System.Windows.Forms.Button btn_Process_B_R2Vac;
        private System.Windows.Forms.Button btn_Process_B_R1Blow;
        private System.Windows.Forms.Button btn_Process_B_R1Vac;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btn_Process_A_L2Blow;
        private System.Windows.Forms.Button btn_Process_A_L2Vac;
        private System.Windows.Forms.Button btn_Process_A_L1Blow;
        private System.Windows.Forms.Button btn_Process_A_L1Vac;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Button btn_Break_B_ProcessR3;
        private System.Windows.Forms.Button btn_Break_B_ProcessR1;
        private System.Windows.Forms.Button btn_Break_B_ProcessR2;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Button btn_Break_A_ProcessL3;
        private System.Windows.Forms.Button btn_Break_A_ProcessL2;
        private System.Windows.Forms.Button btn_Break_A_ProcessL1;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Button btn_Break_B_AlignR3;
        private System.Windows.Forms.Button btn_Break_B_AlignR1;
        private System.Windows.Forms.Button btn_Break_B_AlignR2;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Button btn_Break_A_AlignL3;
        private System.Windows.Forms.Button btn_Break_A_AlignL2;
        private System.Windows.Forms.Button btn_Break_A_AlignL1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btn_Break_B_R2Blow;
        private System.Windows.Forms.Button btn_Break_B_R2Vac;
        private System.Windows.Forms.Button btn_Break_B_R1Blow;
        private System.Windows.Forms.Button btn_Break_B_R1Vac;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btn_Break_A_L2Blow;
        private System.Windows.Forms.Button btn_Break_A_L2Vac;
        private System.Windows.Forms.Button btn_Break_A_L1Blow;
        private System.Windows.Forms.Button btn_Break_A_L1Vac;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btn_Move_BreakTable_RightUnload;
        private System.Windows.Forms.Button btn_Move_BreakTable_LeftUnload;
        private System.Windows.Forms.Button btn_Move_BreakTable_RightLoad;
        private System.Windows.Forms.Button btn_Move_BreakTable_LeftLoad;
        private System.Windows.Forms.Button btn_Move_BreakTransfer_RightUnload;
        private System.Windows.Forms.Button btn_Move_BreakTransfer_LeftUnload;
        private System.Windows.Forms.Button btn_Move_BreakTransfer_RightLoad;
        private System.Windows.Forms.Button btn_Move_BreakTransfer_LeftLoad;
        private System.Windows.Forms.Button btn_Move_Process_RightUnload;
        private System.Windows.Forms.Button btn_Move_Process_LeftUnload;
        private System.Windows.Forms.Button btn_Move_Process_RightLoad;
        private System.Windows.Forms.Button btn_Move_Process_LeftLoad;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Button btn_Etc_OneProcess;
        private System.Windows.Forms.Button btn_Etc_RepeatMove;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TextBox tbox_Etc_TimeShot;
        private System.Windows.Forms.Button btn_Etc_TimeShot;
        private System.Windows.Forms.Button btn_Etc_Shutter_Close;
        private System.Windows.Forms.Button btn_Etc_Shutter_Open;
        private System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Button btn_Align_IR2;
        private System.Windows.Forms.Button btn_Align_IR1;
        private System.Windows.Forms.Button btn_Align_IL2;
        private System.Windows.Forms.Button btn_Align_IL1;
        private System.Windows.Forms.Button btn_Align_Idle;
        private System.Windows.Forms.Button btn_Align_TestInsp;
        private System.Windows.Forms.Button btn_Align_PreAlign;
        private System.Windows.Forms.TextBox tbox_Align_TotalInspCnt;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tbox_Align_InspCount;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Label label20;
    }
}