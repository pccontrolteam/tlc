﻿namespace DIT.TLC.UI
{
    partial class MainWindow
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        } 

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.btn_start = new System.Windows.Forms.Button();
            this.btn_pause = new System.Windows.Forms.Button();
            this.btn_stop = new System.Windows.Forms.Button();
            this.btn_keyswitch = new System.Windows.Forms.Button();
            this.btn_pm = new System.Windows.Forms.Button();
            this.btn_dooropen = new System.Windows.Forms.Button();
            this.btn_light = new System.Windows.Forms.Button();
            this.lb_process_connect_info = new System.Windows.Forms.Label();
            this.panel_cim_connect_info = new System.Windows.Forms.Panel();
            this.panel_ajin_connect_info = new System.Windows.Forms.Panel();
            this.lb_cim_connect_info = new System.Windows.Forms.Label();
            this.lb_ajin_connect_info = new System.Windows.Forms.Label();
            this.panel_umac_connect_info = new System.Windows.Forms.Panel();
            this.lb_umac_connect_info = new System.Windows.Forms.Label();
            this.panel_laser_connect_info = new System.Windows.Forms.Panel();
            this.panel_sequence_connect_info = new System.Windows.Forms.Panel();
            this.lb_laser_connect_info = new System.Windows.Forms.Label();
            this.panel_insp1_connect_info = new System.Windows.Forms.Panel();
            this.lb_sequence_connect_info = new System.Windows.Forms.Label();
            this.lb_insp1_connect_info = new System.Windows.Forms.Label();
            this.panel_serial_connect_info = new System.Windows.Forms.Panel();
            this.panel_insp2_connect_info = new System.Windows.Forms.Panel();
            this.lb_serial_connect_info = new System.Windows.Forms.Label();
            this.lb_insp2_connect_info = new System.Windows.Forms.Label();
            this.panel_runinfo = new System.Windows.Forms.Panel();
            this.tbox_tact_time = new System.Windows.Forms.TextBox();
            this.tbox_run_count = new System.Windows.Forms.TextBox();
            this.tbox_run_time = new System.Windows.Forms.TextBox();
            this.tbox_start_time = new System.Windows.Forms.TextBox();
            this.tbox_cell_size = new System.Windows.Forms.TextBox();
            this.tbox_process = new System.Windows.Forms.TextBox();
            this.tbox_run_name = new System.Windows.Forms.TextBox();
            this.tbox_recipe_name = new System.Windows.Forms.TextBox();
            this.lb_tact_time = new System.Windows.Forms.Label();
            this.lb_run_count = new System.Windows.Forms.Label();
            this.lb_run_time = new System.Windows.Forms.Label();
            this.lb_start_time = new System.Windows.Forms.Label();
            this.lb_cell_size = new System.Windows.Forms.Label();
            this.lb_process = new System.Windows.Forms.Label();
            this.lb_recipe_name = new System.Windows.Forms.Label();
            this.lb_run_name = new System.Windows.Forms.Label();
            this.lb_run_info = new System.Windows.Forms.Label();
            this.gbox_laser_info = new System.Windows.Forms.GroupBox();
            this.lb_shutter = new System.Windows.Forms.Label();
            this.lb_shutter_info = new System.Windows.Forms.Label();
            this.lb_power = new System.Windows.Forms.Label();
            this.lb_power_info = new System.Windows.Forms.Label();
            this.lb_divider = new System.Windows.Forms.Label();
            this.lb_divider_info = new System.Windows.Forms.Label();
            this.lb_pd7 = new System.Windows.Forms.Label();
            this.lb_pd7power_info = new System.Windows.Forms.Label();
            this.lb_outamplifier = new System.Windows.Forms.Label();
            this.lb_amplifier = new System.Windows.Forms.Label();
            this.lb_burst = new System.Windows.Forms.Label();
            this.lb_pulse_mode = new System.Windows.Forms.Label();
            this.lb_outamplifier_info = new System.Windows.Forms.Label();
            this.lb_amplifier_info = new System.Windows.Forms.Label();
            this.lb_burst_info = new System.Windows.Forms.Label();
            this.lb_pulsemode_info = new System.Windows.Forms.Label();
            this.gbox_mcr_count = new System.Windows.Forms.GroupBox();
            this.panel_cstinfo = new System.Windows.Forms.Panel();
            this.lb_cst_total_count = new System.Windows.Forms.Label();
            this.lb_cst_total_count_info = new System.Windows.Forms.Label();
            this.lb_cst_read_count = new System.Windows.Forms.Label();
            this.lb_cst_read_count_info = new System.Windows.Forms.Label();
            this.lb_cst = new System.Windows.Forms.Label();
            this.panel_day_info = new System.Windows.Forms.Panel();
            this.lb_day_total_count = new System.Windows.Forms.Label();
            this.lb_day_total_count_info = new System.Windows.Forms.Label();
            this.lb_day_read_count = new System.Windows.Forms.Label();
            this.lb_day_read_count_info = new System.Windows.Forms.Label();
            this.lb_day = new System.Windows.Forms.Label();
            this.gbox_uld = new System.Windows.Forms.GroupBox();
            this.panel_uld_in_muting = new System.Windows.Forms.Panel();
            this.lb_uld_in_muting_info = new System.Windows.Forms.Label();
            this.panel_uld_out_muting = new System.Windows.Forms.Panel();
            this.lb_uld_out_muting_info = new System.Windows.Forms.Label();
            this.gbox_ld = new System.Windows.Forms.GroupBox();
            this.panel_ld_out_muting = new System.Windows.Forms.Panel();
            this.lb_ld_out_muting_info = new System.Windows.Forms.Label();
            this.panel_ld_in_muting = new System.Windows.Forms.Panel();
            this.lb_ld_in_muting_info = new System.Windows.Forms.Label();
            this.btn_safety_reset = new System.Windows.Forms.Button();
            this.panel_run = new System.Windows.Forms.Panel();
            this.panel_lot_b_info = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.lb_lot_b_info = new System.Windows.Forms.Label();
            this.panel_lot_a_info = new System.Windows.Forms.Panel();
            this.lb_lot_a = new System.Windows.Forms.Label();
            this.lb_lot_a_info = new System.Windows.Forms.Label();
            this.panel_ld_muting_out = new System.Windows.Forms.Panel();
            this.lb_ld_muting_out = new System.Windows.Forms.Label();
            this.panel_uld_muting_in = new System.Windows.Forms.Panel();
            this.lb_uld_muting_in = new System.Windows.Forms.Label();
            this.panel_ld_muting_in = new System.Windows.Forms.Panel();
            this.lb_ld_muting_in = new System.Windows.Forms.Label();
            this.panel_uld_muting_out = new System.Windows.Forms.Panel();
            this.lb_uld_muting_out = new System.Windows.Forms.Label();
            this.panel_door2 = new System.Windows.Forms.Panel();
            this.lb_door2 = new System.Windows.Forms.Label();
            this.panel38 = new System.Windows.Forms.Panel();
            this.label51 = new System.Windows.Forms.Label();
            this.panel_door3 = new System.Windows.Forms.Panel();
            this.lb_door3 = new System.Windows.Forms.Label();
            this.panel_door5 = new System.Windows.Forms.Panel();
            this.lb_door5 = new System.Windows.Forms.Label();
            this.panel_door4 = new System.Windows.Forms.Panel();
            this.lb_door4 = new System.Windows.Forms.Label();
            this.panel_door6 = new System.Windows.Forms.Panel();
            this.lb_door6 = new System.Windows.Forms.Label();
            this.panel_door8 = new System.Windows.Forms.Panel();
            this.lb_door8 = new System.Windows.Forms.Label();
            this.panel_door7 = new System.Windows.Forms.Panel();
            this.lb_door7 = new System.Windows.Forms.Label();
            this.panel_door9 = new System.Windows.Forms.Panel();
            this.lb_door9 = new System.Windows.Forms.Label();
            this.panel_b_cst_skip = new System.Windows.Forms.Panel();
            this.lb_b_cst_skip = new System.Windows.Forms.Label();
            this.panel_a_cst_skip = new System.Windows.Forms.Panel();
            this.lb_a_cst_skip = new System.Windows.Forms.Label();
            this.panel_ld_box = new System.Windows.Forms.Panel();
            this.lb_ld_box = new System.Windows.Forms.Label();
            this.panel_ems3 = new System.Windows.Forms.Panel();
            this.lb_ems3 = new System.Windows.Forms.Label();
            this.panel_process_box = new System.Windows.Forms.Panel();
            this.lb_process_box = new System.Windows.Forms.Label();
            this.panel_ems5 = new System.Windows.Forms.Panel();
            this.lb_ems5 = new System.Windows.Forms.Label();
            this.panel_uld_box = new System.Windows.Forms.Panel();
            this.lb_uld_box = new System.Windows.Forms.Label();
            this.panel_run2 = new System.Windows.Forms.Panel();
            this.panel_ld_light = new System.Windows.Forms.Panel();
            this.lb_ld_light = new System.Windows.Forms.Label();
            this.panel_process_light = new System.Windows.Forms.Panel();
            this.lb_process_light = new System.Windows.Forms.Label();
            this.panel_uld_light = new System.Windows.Forms.Panel();
            this.lb_uld_light = new System.Windows.Forms.Label();
            this.panel_ems1 = new System.Windows.Forms.Panel();
            this.lb_ems1 = new System.Windows.Forms.Label();
            this.panel_target_avgpower = new System.Windows.Forms.Panel();
            this.lb_avgpower = new System.Windows.Forms.Label();
            this.lb_avgpower_info = new System.Windows.Forms.Label();
            this.lb_tartget = new System.Windows.Forms.Label();
            this.lb_target_info = new System.Windows.Forms.Label();
            this.panel_zpos_gap = new System.Windows.Forms.Panel();
            this.lb_zpos_gap = new System.Windows.Forms.Label();
            this.lb_zpos_gap_info = new System.Windows.Forms.Label();
            this.panel_ems4 = new System.Windows.Forms.Panel();
            this.lb_ems4 = new System.Windows.Forms.Label();
            this.panel_ld_out_b = new System.Windows.Forms.Panel();
            this.lb_ld_out_b = new System.Windows.Forms.Label();
            this.panel_ld_in_b = new System.Windows.Forms.Panel();
            this.lb_ld_in_b = new System.Windows.Forms.Label();
            this.panel_ld_out_a = new System.Windows.Forms.Panel();
            this.lb_ld_out_a = new System.Windows.Forms.Label();
            this.panel_ld_in_a = new System.Windows.Forms.Panel();
            this.lb_ld_in_a = new System.Windows.Forms.Label();
            this.panel_uld_in_b = new System.Windows.Forms.Panel();
            this.lb_uld_in_b = new System.Windows.Forms.Label();
            this.panel_uld_out_b = new System.Windows.Forms.Panel();
            this.lb_uld_out_b = new System.Windows.Forms.Label();
            this.panel_uld_in_a = new System.Windows.Forms.Panel();
            this.lb_uld_in_a = new System.Windows.Forms.Label();
            this.panel_uld_out_a = new System.Windows.Forms.Panel();
            this.lb_uld_out_a = new System.Windows.Forms.Label();
            this.panel_uld1_out2 = new System.Windows.Forms.Panel();
            this.panel_uld2_out2 = new System.Windows.Forms.Panel();
            this.panel_uld1_table = new System.Windows.Forms.Panel();
            this.panel_uld2_table = new System.Windows.Forms.Panel();
            this.panel_uld1_out1 = new System.Windows.Forms.Panel();
            this.panel_uld2_out1 = new System.Windows.Forms.Panel();
            this.panel_uld1 = new System.Windows.Forms.Panel();
            this.lb_uld1 = new System.Windows.Forms.Label();
            this.panel_uld2 = new System.Windows.Forms.Panel();
            this.lb_uld2 = new System.Windows.Forms.Label();
            this.panel_uld_trans_a1 = new System.Windows.Forms.Panel();
            this.lb_uld_trans_a1 = new System.Windows.Forms.Label();
            this.panel_uld_trans_b1 = new System.Windows.Forms.Panel();
            this.lb_uld_trans_b1 = new System.Windows.Forms.Label();
            this.panel_uld_trans_a2 = new System.Windows.Forms.Panel();
            this.lb_uld_trans_a2 = new System.Windows.Forms.Label();
            this.panel_uld_trans_b2 = new System.Windows.Forms.Panel();
            this.lb_uld_trans_b2 = new System.Windows.Forms.Label();
            this.panel_break_table_a1 = new System.Windows.Forms.Panel();
            this.lb_break_table_a1 = new System.Windows.Forms.Label();
            this.panel_break_table_b1 = new System.Windows.Forms.Panel();
            this.lb_break_table_b1 = new System.Windows.Forms.Label();
            this.panel_break_trans1 = new System.Windows.Forms.Panel();
            this.lb_break_trans1 = new System.Windows.Forms.Label();
            this.panel_break_trans2 = new System.Windows.Forms.Panel();
            this.lb_break_trans2 = new System.Windows.Forms.Label();
            this.panel_break_table_a2 = new System.Windows.Forms.Panel();
            this.lb_break_table_a2 = new System.Windows.Forms.Label();
            this.panel_break_table_b2 = new System.Windows.Forms.Panel();
            this.lb_break_table_b2 = new System.Windows.Forms.Label();
            this.panel_process_table_a1 = new System.Windows.Forms.Panel();
            this.lb_process_table_a1 = new System.Windows.Forms.Label();
            this.panel_process_table_b1 = new System.Windows.Forms.Panel();
            this.lb_process_table_b1 = new System.Windows.Forms.Label();
            this.panel_process_table_a2 = new System.Windows.Forms.Panel();
            this.lb_process_table_a2 = new System.Windows.Forms.Label();
            this.panel_ld_trans1 = new System.Windows.Forms.Panel();
            this.lb_ld_trans1 = new System.Windows.Forms.Label();
            this.panel_process_table_b2 = new System.Windows.Forms.Panel();
            this.lb_process_table_b2 = new System.Windows.Forms.Label();
            this.panel_ld1 = new System.Windows.Forms.Panel();
            this.lb_ld1 = new System.Windows.Forms.Label();
            this.panel_ld_trans2 = new System.Windows.Forms.Panel();
            this.lb_ld_trans2 = new System.Windows.Forms.Label();
            this.panel_ld1_table = new System.Windows.Forms.Panel();
            this.panel_ld2 = new System.Windows.Forms.Panel();
            this.lb_ld2 = new System.Windows.Forms.Label();
            this.panel_ld1_in2 = new System.Windows.Forms.Panel();
            this.panel_ld2_table = new System.Windows.Forms.Panel();
            this.panel_ld1_in1 = new System.Windows.Forms.Panel();
            this.panel_ld2_in2 = new System.Windows.Forms.Panel();
            this.panel_ld2_in1 = new System.Windows.Forms.Panel();
            this.lb_ld1_o_time = new System.Windows.Forms.Label();
            this.lb_ld1_o = new System.Windows.Forms.Label();
            this.lb_ld1_i_time = new System.Windows.Forms.Label();
            this.lb_ld1_i = new System.Windows.Forms.Label();
            this.lb_ld2_o_time = new System.Windows.Forms.Label();
            this.lb_ld2_o = new System.Windows.Forms.Label();
            this.lb_ld2_i_time = new System.Windows.Forms.Label();
            this.lb_ld2_i = new System.Windows.Forms.Label();
            this.lb_uld1_i_time = new System.Windows.Forms.Label();
            this.lb_uld1_i = new System.Windows.Forms.Label();
            this.lb_uld1_o_time = new System.Windows.Forms.Label();
            this.lb_uld1_o = new System.Windows.Forms.Label();
            this.lb_uld2_i_time = new System.Windows.Forms.Label();
            this.lb_uld2_i = new System.Windows.Forms.Label();
            this.lb_uld2_o_time = new System.Windows.Forms.Label();
            this.lb_uld2_o = new System.Windows.Forms.Label();
            this.panel_shutter_open = new System.Windows.Forms.Panel();
            this.lb_shutter_open = new System.Windows.Forms.Label();
            this.panel_lock = new System.Windows.Forms.Panel();
            this.lb_lock = new System.Windows.Forms.Label();
            this.panel_laser_cover = new System.Windows.Forms.Panel();
            this.lb_laser_cover = new System.Windows.Forms.Label();
            this.panel_ems2 = new System.Windows.Forms.Panel();
            this.lb_ems2 = new System.Windows.Forms.Label();
            this.panel_ems6 = new System.Windows.Forms.Panel();
            this.lb_ems6 = new System.Windows.Forms.Label();
            this.panel_grab_ems1 = new System.Windows.Forms.Panel();
            this.lb_grab_ems1 = new System.Windows.Forms.Label();
            this.panel_grab_ems2 = new System.Windows.Forms.Panel();
            this.lb_grab_ems2 = new System.Windows.Forms.Label();
            this.panel_grab_switch1 = new System.Windows.Forms.Panel();
            this.lb_grab_switch1 = new System.Windows.Forms.Label();
            this.panel_grab_switch2 = new System.Windows.Forms.Panel();
            this.lb_grab_switch2 = new System.Windows.Forms.Label();
            this.panel_grab_ems3 = new System.Windows.Forms.Panel();
            this.lb_grab_ems3 = new System.Windows.Forms.Label();
            this.panel_grab_switch3 = new System.Windows.Forms.Panel();
            this.lb_grab_switch3 = new System.Windows.Forms.Label();
            this.panel_door10 = new System.Windows.Forms.Panel();
            this.lb_door10 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label30 = new System.Windows.Forms.Label();
            this.panel_camera_l_info = new System.Windows.Forms.Panel();
            this.lb_camera_l = new System.Windows.Forms.Label();
            this.panel_camera_l = new System.Windows.Forms.Panel();
            this.panel_camera_r_info = new System.Windows.Forms.Panel();
            this.lb_camera_r = new System.Windows.Forms.Label();
            this.panel_camera_r = new System.Windows.Forms.Panel();
            this.panel_align = new System.Windows.Forms.Panel();
            this.tbox_align_break4_offset_a = new System.Windows.Forms.TextBox();
            this.tbox_align_break3_offset_a = new System.Windows.Forms.TextBox();
            this.tbox_align_pre2_offset_a = new System.Windows.Forms.TextBox();
            this.lb_align_offset_a2 = new System.Windows.Forms.Label();
            this.tbox_align_break4_offset_y = new System.Windows.Forms.TextBox();
            this.tbox_align_break3_offset_y = new System.Windows.Forms.TextBox();
            this.tbox_align_pre2_offset_y = new System.Windows.Forms.TextBox();
            this.lb_align_offset_y2 = new System.Windows.Forms.Label();
            this.tbox_align_break4_offset_x = new System.Windows.Forms.TextBox();
            this.tbox_align_break3_offset_x = new System.Windows.Forms.TextBox();
            this.tbox_align_pre2_offset_x = new System.Windows.Forms.TextBox();
            this.lb_align_offset_x2 = new System.Windows.Forms.Label();
            this.tbox_align_break4_result = new System.Windows.Forms.TextBox();
            this.tbox_align_break3_result = new System.Windows.Forms.TextBox();
            this.tbox_align_pre2_result = new System.Windows.Forms.TextBox();
            this.lb_align_result2 = new System.Windows.Forms.Label();
            this.lb_align_break4 = new System.Windows.Forms.Label();
            this.lb_align_break3 = new System.Windows.Forms.Label();
            this.lb_align_pre2 = new System.Windows.Forms.Label();
            this.lb_align_name2 = new System.Windows.Forms.Label();
            this.tbox_align_break2_offset_a = new System.Windows.Forms.TextBox();
            this.tbox_align_break1_offset_a = new System.Windows.Forms.TextBox();
            this.tbox_align_pre1_offset_a = new System.Windows.Forms.TextBox();
            this.lb_align_offset_a1 = new System.Windows.Forms.Label();
            this.tbox_align_break2_offset_y = new System.Windows.Forms.TextBox();
            this.tbox_align_break1_offset_y = new System.Windows.Forms.TextBox();
            this.tbox_align_pre1_offset_y = new System.Windows.Forms.TextBox();
            this.lb_align_offset_y1 = new System.Windows.Forms.Label();
            this.tbox_align_break2_offset_x = new System.Windows.Forms.TextBox();
            this.tbox_align_break1_offset_x = new System.Windows.Forms.TextBox();
            this.tbox_align_pre1_offset_x = new System.Windows.Forms.TextBox();
            this.lb_align_offset_x1 = new System.Windows.Forms.Label();
            this.tbox_align_break2_result = new System.Windows.Forms.TextBox();
            this.tbox_align_break1_result = new System.Windows.Forms.TextBox();
            this.tbox_align_pre1_result = new System.Windows.Forms.TextBox();
            this.lb_align_result1 = new System.Windows.Forms.Label();
            this.lb_align_break2 = new System.Windows.Forms.Label();
            this.lb_align_break1 = new System.Windows.Forms.Label();
            this.lb_align_pre1 = new System.Windows.Forms.Label();
            this.lb_align_name1 = new System.Windows.Forms.Label();
            this.gbox_breaking_count = new System.Windows.Forms.GroupBox();
            this.panel_breaking_count_clear = new System.Windows.Forms.Panel();
            this.lb_breaking_count_clear = new System.Windows.Forms.Label();
            this.lb_breaking_count = new System.Windows.Forms.Label();
            this.lb_breaking_count_info = new System.Windows.Forms.Label();
            this.gbox_dummy_tank = new System.Windows.Forms.GroupBox();
            this.panel_dummy_tank_set = new System.Windows.Forms.Panel();
            this.lb_dummy_tank_set = new System.Windows.Forms.Label();
            this.panel_dummy_tank_get = new System.Windows.Forms.Panel();
            this.lb_dummy_tank_get = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.detailTactView21 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView17 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView16 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView15 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView10 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView9 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView8 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView7 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView6 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView23 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView22 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView20 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView19 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView18 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView14 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView13 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView12 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView5 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView4 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView3 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView11 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView2 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView1 = new DIT.TLC.UI.Main.DetailTactView();
            this.cstCellCount3 = new DIT.TLC.UI.Main.CstCellCount();
            this.cstCellCount4 = new DIT.TLC.UI.Main.CstCellCount();
            this.cstCellCount2 = new DIT.TLC.UI.Main.CstCellCount();
            this.cstCellCount1 = new DIT.TLC.UI.Main.CstCellCount();
            this.panel_runinfo.SuspendLayout();
            this.gbox_laser_info.SuspendLayout();
            this.gbox_mcr_count.SuspendLayout();
            this.panel_cstinfo.SuspendLayout();
            this.panel_day_info.SuspendLayout();
            this.gbox_uld.SuspendLayout();
            this.panel_uld_in_muting.SuspendLayout();
            this.panel_uld_out_muting.SuspendLayout();
            this.gbox_ld.SuspendLayout();
            this.panel_ld_out_muting.SuspendLayout();
            this.panel_ld_in_muting.SuspendLayout();
            this.panel_run.SuspendLayout();
            this.panel_lot_b_info.SuspendLayout();
            this.panel_lot_a_info.SuspendLayout();
            this.panel_ld_muting_out.SuspendLayout();
            this.panel_uld_muting_in.SuspendLayout();
            this.panel_ld_muting_in.SuspendLayout();
            this.panel_uld_muting_out.SuspendLayout();
            this.panel_door2.SuspendLayout();
            this.panel38.SuspendLayout();
            this.panel_door3.SuspendLayout();
            this.panel_door5.SuspendLayout();
            this.panel_door4.SuspendLayout();
            this.panel_door6.SuspendLayout();
            this.panel_door8.SuspendLayout();
            this.panel_door7.SuspendLayout();
            this.panel_door9.SuspendLayout();
            this.panel_b_cst_skip.SuspendLayout();
            this.panel_a_cst_skip.SuspendLayout();
            this.panel_ld_box.SuspendLayout();
            this.panel_ems3.SuspendLayout();
            this.panel_process_box.SuspendLayout();
            this.panel_ems5.SuspendLayout();
            this.panel_uld_box.SuspendLayout();
            this.panel_run2.SuspendLayout();
            this.panel_ld_light.SuspendLayout();
            this.panel_process_light.SuspendLayout();
            this.panel_uld_light.SuspendLayout();
            this.panel_ems1.SuspendLayout();
            this.panel_target_avgpower.SuspendLayout();
            this.panel_zpos_gap.SuspendLayout();
            this.panel_ems4.SuspendLayout();
            this.panel_ld_out_b.SuspendLayout();
            this.panel_ld_in_b.SuspendLayout();
            this.panel_ld_out_a.SuspendLayout();
            this.panel_ld_in_a.SuspendLayout();
            this.panel_uld_in_b.SuspendLayout();
            this.panel_uld_out_b.SuspendLayout();
            this.panel_uld_in_a.SuspendLayout();
            this.panel_uld_out_a.SuspendLayout();
            this.panel_uld1.SuspendLayout();
            this.panel_uld2.SuspendLayout();
            this.panel_uld_trans_a1.SuspendLayout();
            this.panel_uld_trans_b1.SuspendLayout();
            this.panel_uld_trans_a2.SuspendLayout();
            this.panel_uld_trans_b2.SuspendLayout();
            this.panel_break_table_a1.SuspendLayout();
            this.panel_break_table_b1.SuspendLayout();
            this.panel_break_trans1.SuspendLayout();
            this.panel_break_trans2.SuspendLayout();
            this.panel_break_table_a2.SuspendLayout();
            this.panel_break_table_b2.SuspendLayout();
            this.panel_process_table_a1.SuspendLayout();
            this.panel_process_table_b1.SuspendLayout();
            this.panel_process_table_a2.SuspendLayout();
            this.panel_ld_trans1.SuspendLayout();
            this.panel_process_table_b2.SuspendLayout();
            this.panel_ld1.SuspendLayout();
            this.panel_ld_trans2.SuspendLayout();
            this.panel_ld2.SuspendLayout();
            this.panel_shutter_open.SuspendLayout();
            this.panel_lock.SuspendLayout();
            this.panel_laser_cover.SuspendLayout();
            this.panel_ems2.SuspendLayout();
            this.panel_ems6.SuspendLayout();
            this.panel_grab_ems1.SuspendLayout();
            this.panel_grab_ems2.SuspendLayout();
            this.panel_grab_switch1.SuspendLayout();
            this.panel_grab_switch2.SuspendLayout();
            this.panel_grab_ems3.SuspendLayout();
            this.panel_grab_switch3.SuspendLayout();
            this.panel_door10.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel_camera_l_info.SuspendLayout();
            this.panel_camera_r_info.SuspendLayout();
            this.panel_align.SuspendLayout();
            this.gbox_breaking_count.SuspendLayout();
            this.panel_breaking_count_clear.SuspendLayout();
            this.gbox_dummy_tank.SuspendLayout();
            this.panel_dummy_tank_set.SuspendLayout();
            this.panel_dummy_tank_get.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_start
            // 
            this.btn_start.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_start.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_start.ForeColor = System.Drawing.Color.White;
            this.btn_start.Image = ((System.Drawing.Image)(resources.GetObject("btn_start.Image")));
            this.btn_start.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_start.Location = new System.Drawing.Point(3, 23);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(132, 50);
            this.btn_start.TabIndex = 32;
            this.btn_start.Text = "시작";
            this.btn_start.UseVisualStyleBackColor = false;
            this.btn_start.Click += new System.EventHandler(this.btn_start_Click);
            // 
            // btn_pause
            // 
            this.btn_pause.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_pause.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_pause.ForeColor = System.Drawing.Color.White;
            this.btn_pause.Image = ((System.Drawing.Image)(resources.GetObject("btn_pause.Image")));
            this.btn_pause.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_pause.Location = new System.Drawing.Point(3, 79);
            this.btn_pause.Name = "btn_pause";
            this.btn_pause.Size = new System.Drawing.Size(132, 50);
            this.btn_pause.TabIndex = 34;
            this.btn_pause.Text = "일시정지";
            this.btn_pause.UseVisualStyleBackColor = false;
            this.btn_pause.Click += new System.EventHandler(this.btn_pause_Click);
            // 
            // btn_stop
            // 
            this.btn_stop.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_stop.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_stop.ForeColor = System.Drawing.Color.White;
            this.btn_stop.Image = ((System.Drawing.Image)(resources.GetObject("btn_stop.Image")));
            this.btn_stop.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_stop.Location = new System.Drawing.Point(3, 135);
            this.btn_stop.Name = "btn_stop";
            this.btn_stop.Size = new System.Drawing.Size(132, 50);
            this.btn_stop.TabIndex = 35;
            this.btn_stop.Text = "작업종료";
            this.btn_stop.UseVisualStyleBackColor = false;
            this.btn_stop.Click += new System.EventHandler(this.btn_stop_Click);
            // 
            // btn_keyswitch
            // 
            this.btn_keyswitch.BackColor = System.Drawing.Color.DimGray;
            this.btn_keyswitch.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold);
            this.btn_keyswitch.ForeColor = System.Drawing.Color.White;
            this.btn_keyswitch.Location = new System.Drawing.Point(3, 230);
            this.btn_keyswitch.Name = "btn_keyswitch";
            this.btn_keyswitch.Size = new System.Drawing.Size(132, 50);
            this.btn_keyswitch.TabIndex = 36;
            this.btn_keyswitch.Text = "키 스위치";
            this.btn_keyswitch.UseVisualStyleBackColor = false;
            // 
            // btn_pm
            // 
            this.btn_pm.BackColor = System.Drawing.Color.DimGray;
            this.btn_pm.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold);
            this.btn_pm.ForeColor = System.Drawing.Color.White;
            this.btn_pm.Location = new System.Drawing.Point(2, 286);
            this.btn_pm.Name = "btn_pm";
            this.btn_pm.Size = new System.Drawing.Size(132, 50);
            this.btn_pm.TabIndex = 37;
            this.btn_pm.Text = "PM";
            this.btn_pm.UseVisualStyleBackColor = false;
            // 
            // btn_dooropen
            // 
            this.btn_dooropen.BackColor = System.Drawing.Color.DimGray;
            this.btn_dooropen.Font = new System.Drawing.Font("맑은 고딕", 15F, System.Drawing.FontStyle.Bold);
            this.btn_dooropen.ForeColor = System.Drawing.Color.White;
            this.btn_dooropen.Location = new System.Drawing.Point(3, 342);
            this.btn_dooropen.Name = "btn_dooropen";
            this.btn_dooropen.Size = new System.Drawing.Size(132, 50);
            this.btn_dooropen.TabIndex = 38;
            this.btn_dooropen.Text = "Door-Open";
            this.btn_dooropen.UseVisualStyleBackColor = false;
            // 
            // btn_light
            // 
            this.btn_light.BackColor = System.Drawing.Color.DimGray;
            this.btn_light.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold);
            this.btn_light.ForeColor = System.Drawing.Color.White;
            this.btn_light.Location = new System.Drawing.Point(3, 398);
            this.btn_light.Name = "btn_light";
            this.btn_light.Size = new System.Drawing.Size(132, 50);
            this.btn_light.TabIndex = 39;
            this.btn_light.Text = "설비조명";
            this.btn_light.UseVisualStyleBackColor = false;
            // 
            // lb_process_connect_info
            // 
            this.lb_process_connect_info.AutoSize = true;
            this.lb_process_connect_info.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lb_process_connect_info.ForeColor = System.Drawing.Color.White;
            this.lb_process_connect_info.Location = new System.Drawing.Point(8, 560);
            this.lb_process_connect_info.Name = "lb_process_connect_info";
            this.lb_process_connect_info.Size = new System.Drawing.Size(126, 19);
            this.lb_process_connect_info.TabIndex = 29;
            this.lb_process_connect_info.Text = "프로세스 연결상태";
            // 
            // panel_cim_connect_info
            // 
            this.panel_cim_connect_info.BackColor = System.Drawing.Color.Red;
            this.panel_cim_connect_info.ForeColor = System.Drawing.Color.White;
            this.panel_cim_connect_info.Location = new System.Drawing.Point(11, 590);
            this.panel_cim_connect_info.Name = "panel_cim_connect_info";
            this.panel_cim_connect_info.Size = new System.Drawing.Size(25, 25);
            this.panel_cim_connect_info.TabIndex = 30;
            // 
            // panel_ajin_connect_info
            // 
            this.panel_ajin_connect_info.BackColor = System.Drawing.Color.Red;
            this.panel_ajin_connect_info.ForeColor = System.Drawing.Color.White;
            this.panel_ajin_connect_info.Location = new System.Drawing.Point(11, 623);
            this.panel_ajin_connect_info.Name = "panel_ajin_connect_info";
            this.panel_ajin_connect_info.Size = new System.Drawing.Size(25, 25);
            this.panel_ajin_connect_info.TabIndex = 31;
            // 
            // lb_cim_connect_info
            // 
            this.lb_cim_connect_info.AutoSize = true;
            this.lb_cim_connect_info.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_cim_connect_info.ForeColor = System.Drawing.Color.White;
            this.lb_cim_connect_info.Location = new System.Drawing.Point(42, 594);
            this.lb_cim_connect_info.Name = "lb_cim_connect_info";
            this.lb_cim_connect_info.Size = new System.Drawing.Size(54, 17);
            this.lb_cim_connect_info.TabIndex = 33;
            this.lb_cim_connect_info.Text = "CIM-PC";
            // 
            // lb_ajin_connect_info
            // 
            this.lb_ajin_connect_info.AutoSize = true;
            this.lb_ajin_connect_info.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_ajin_connect_info.ForeColor = System.Drawing.Color.White;
            this.lb_ajin_connect_info.Location = new System.Drawing.Point(42, 627);
            this.lb_ajin_connect_info.Name = "lb_ajin_connect_info";
            this.lb_ajin_connect_info.Size = new System.Drawing.Size(65, 17);
            this.lb_ajin_connect_info.TabIndex = 40;
            this.lb_ajin_connect_info.Text = "AJIN-PLC";
            // 
            // panel_umac_connect_info
            // 
            this.panel_umac_connect_info.BackColor = System.Drawing.Color.Red;
            this.panel_umac_connect_info.ForeColor = System.Drawing.Color.White;
            this.panel_umac_connect_info.Location = new System.Drawing.Point(11, 654);
            this.panel_umac_connect_info.Name = "panel_umac_connect_info";
            this.panel_umac_connect_info.Size = new System.Drawing.Size(25, 25);
            this.panel_umac_connect_info.TabIndex = 41;
            // 
            // lb_umac_connect_info
            // 
            this.lb_umac_connect_info.AutoSize = true;
            this.lb_umac_connect_info.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_umac_connect_info.ForeColor = System.Drawing.Color.White;
            this.lb_umac_connect_info.Location = new System.Drawing.Point(42, 658);
            this.lb_umac_connect_info.Name = "lb_umac_connect_info";
            this.lb_umac_connect_info.Size = new System.Drawing.Size(76, 17);
            this.lb_umac_connect_info.TabIndex = 42;
            this.lb_umac_connect_info.Text = "UMAC-PLC";
            // 
            // panel_laser_connect_info
            // 
            this.panel_laser_connect_info.BackColor = System.Drawing.Color.Red;
            this.panel_laser_connect_info.ForeColor = System.Drawing.Color.White;
            this.panel_laser_connect_info.Location = new System.Drawing.Point(11, 686);
            this.panel_laser_connect_info.Name = "panel_laser_connect_info";
            this.panel_laser_connect_info.Size = new System.Drawing.Size(25, 25);
            this.panel_laser_connect_info.TabIndex = 43;
            // 
            // panel_sequence_connect_info
            // 
            this.panel_sequence_connect_info.BackColor = System.Drawing.Color.Red;
            this.panel_sequence_connect_info.ForeColor = System.Drawing.Color.White;
            this.panel_sequence_connect_info.Location = new System.Drawing.Point(11, 719);
            this.panel_sequence_connect_info.Name = "panel_sequence_connect_info";
            this.panel_sequence_connect_info.Size = new System.Drawing.Size(25, 25);
            this.panel_sequence_connect_info.TabIndex = 44;
            // 
            // lb_laser_connect_info
            // 
            this.lb_laser_connect_info.AutoSize = true;
            this.lb_laser_connect_info.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_laser_connect_info.ForeColor = System.Drawing.Color.White;
            this.lb_laser_connect_info.Location = new System.Drawing.Point(42, 690);
            this.lb_laser_connect_info.Name = "lb_laser_connect_info";
            this.lb_laser_connect_info.Size = new System.Drawing.Size(47, 17);
            this.lb_laser_connect_info.TabIndex = 45;
            this.lb_laser_connect_info.Text = "레이저";
            // 
            // panel_insp1_connect_info
            // 
            this.panel_insp1_connect_info.BackColor = System.Drawing.Color.Red;
            this.panel_insp1_connect_info.ForeColor = System.Drawing.Color.White;
            this.panel_insp1_connect_info.Location = new System.Drawing.Point(11, 781);
            this.panel_insp1_connect_info.Name = "panel_insp1_connect_info";
            this.panel_insp1_connect_info.Size = new System.Drawing.Size(25, 25);
            this.panel_insp1_connect_info.TabIndex = 49;
            // 
            // lb_sequence_connect_info
            // 
            this.lb_sequence_connect_info.AutoSize = true;
            this.lb_sequence_connect_info.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_sequence_connect_info.ForeColor = System.Drawing.Color.White;
            this.lb_sequence_connect_info.Location = new System.Drawing.Point(42, 723);
            this.lb_sequence_connect_info.Name = "lb_sequence_connect_info";
            this.lb_sequence_connect_info.Size = new System.Drawing.Size(47, 17);
            this.lb_sequence_connect_info.TabIndex = 46;
            this.lb_sequence_connect_info.Text = "시퀀스";
            // 
            // lb_insp1_connect_info
            // 
            this.lb_insp1_connect_info.AutoSize = true;
            this.lb_insp1_connect_info.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_insp1_connect_info.ForeColor = System.Drawing.Color.White;
            this.lb_insp1_connect_info.Location = new System.Drawing.Point(42, 785);
            this.lb_insp1_connect_info.Name = "lb_insp1_connect_info";
            this.lb_insp1_connect_info.Size = new System.Drawing.Size(60, 17);
            this.lb_insp1_connect_info.TabIndex = 50;
            this.lb_insp1_connect_info.Text = "검사기 1";
            // 
            // panel_serial_connect_info
            // 
            this.panel_serial_connect_info.BackColor = System.Drawing.Color.Red;
            this.panel_serial_connect_info.ForeColor = System.Drawing.Color.White;
            this.panel_serial_connect_info.Location = new System.Drawing.Point(11, 750);
            this.panel_serial_connect_info.Name = "panel_serial_connect_info";
            this.panel_serial_connect_info.Size = new System.Drawing.Size(25, 25);
            this.panel_serial_connect_info.TabIndex = 47;
            // 
            // panel_insp2_connect_info
            // 
            this.panel_insp2_connect_info.BackColor = System.Drawing.Color.Red;
            this.panel_insp2_connect_info.ForeColor = System.Drawing.Color.White;
            this.panel_insp2_connect_info.Location = new System.Drawing.Point(11, 812);
            this.panel_insp2_connect_info.Name = "panel_insp2_connect_info";
            this.panel_insp2_connect_info.Size = new System.Drawing.Size(25, 25);
            this.panel_insp2_connect_info.TabIndex = 51;
            // 
            // lb_serial_connect_info
            // 
            this.lb_serial_connect_info.AutoSize = true;
            this.lb_serial_connect_info.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_serial_connect_info.ForeColor = System.Drawing.Color.White;
            this.lb_serial_connect_info.Location = new System.Drawing.Point(42, 754);
            this.lb_serial_connect_info.Name = "lb_serial_connect_info";
            this.lb_serial_connect_info.Size = new System.Drawing.Size(73, 17);
            this.lb_serial_connect_info.TabIndex = 48;
            this.lb_serial_connect_info.Text = "Serial 통신";
            // 
            // lb_insp2_connect_info
            // 
            this.lb_insp2_connect_info.AutoSize = true;
            this.lb_insp2_connect_info.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_insp2_connect_info.ForeColor = System.Drawing.Color.White;
            this.lb_insp2_connect_info.Location = new System.Drawing.Point(42, 816);
            this.lb_insp2_connect_info.Name = "lb_insp2_connect_info";
            this.lb_insp2_connect_info.Size = new System.Drawing.Size(60, 17);
            this.lb_insp2_connect_info.TabIndex = 52;
            this.lb_insp2_connect_info.Text = "검사기 2";
            // 
            // panel_runinfo
            // 
            this.panel_runinfo.BackColor = System.Drawing.Color.DimGray;
            this.panel_runinfo.Controls.Add(this.tbox_tact_time);
            this.panel_runinfo.Controls.Add(this.tbox_run_count);
            this.panel_runinfo.Controls.Add(this.tbox_run_time);
            this.panel_runinfo.Controls.Add(this.tbox_start_time);
            this.panel_runinfo.Controls.Add(this.tbox_cell_size);
            this.panel_runinfo.Controls.Add(this.tbox_process);
            this.panel_runinfo.Controls.Add(this.tbox_run_name);
            this.panel_runinfo.Controls.Add(this.tbox_recipe_name);
            this.panel_runinfo.Controls.Add(this.lb_tact_time);
            this.panel_runinfo.Controls.Add(this.lb_run_count);
            this.panel_runinfo.Controls.Add(this.lb_run_time);
            this.panel_runinfo.Controls.Add(this.lb_start_time);
            this.panel_runinfo.Controls.Add(this.lb_cell_size);
            this.panel_runinfo.Controls.Add(this.lb_process);
            this.panel_runinfo.Controls.Add(this.lb_recipe_name);
            this.panel_runinfo.Controls.Add(this.lb_run_name);
            this.panel_runinfo.Controls.Add(this.lb_run_info);
            this.panel_runinfo.Location = new System.Drawing.Point(4, 3);
            this.panel_runinfo.Name = "panel_runinfo";
            this.panel_runinfo.Size = new System.Drawing.Size(253, 246);
            this.panel_runinfo.TabIndex = 26;
            // 
            // tbox_tact_time
            // 
            this.tbox_tact_time.BackColor = System.Drawing.Color.DarkSlateGray;
            this.tbox_tact_time.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbox_tact_time.Location = new System.Drawing.Point(83, 219);
            this.tbox_tact_time.Name = "tbox_tact_time";
            this.tbox_tact_time.ReadOnly = true;
            this.tbox_tact_time.Size = new System.Drawing.Size(164, 21);
            this.tbox_tact_time.TabIndex = 19;
            // 
            // tbox_run_count
            // 
            this.tbox_run_count.BackColor = System.Drawing.Color.DarkSlateGray;
            this.tbox_run_count.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbox_run_count.Location = new System.Drawing.Point(83, 192);
            this.tbox_run_count.Name = "tbox_run_count";
            this.tbox_run_count.ReadOnly = true;
            this.tbox_run_count.Size = new System.Drawing.Size(164, 21);
            this.tbox_run_count.TabIndex = 18;
            // 
            // tbox_run_time
            // 
            this.tbox_run_time.BackColor = System.Drawing.Color.DarkSlateGray;
            this.tbox_run_time.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbox_run_time.Location = new System.Drawing.Point(83, 165);
            this.tbox_run_time.Name = "tbox_run_time";
            this.tbox_run_time.ReadOnly = true;
            this.tbox_run_time.Size = new System.Drawing.Size(164, 21);
            this.tbox_run_time.TabIndex = 17;
            // 
            // tbox_start_time
            // 
            this.tbox_start_time.BackColor = System.Drawing.Color.DarkSlateGray;
            this.tbox_start_time.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbox_start_time.Location = new System.Drawing.Point(83, 138);
            this.tbox_start_time.Name = "tbox_start_time";
            this.tbox_start_time.ReadOnly = true;
            this.tbox_start_time.Size = new System.Drawing.Size(164, 21);
            this.tbox_start_time.TabIndex = 16;
            // 
            // tbox_cell_size
            // 
            this.tbox_cell_size.BackColor = System.Drawing.Color.DarkSlateGray;
            this.tbox_cell_size.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbox_cell_size.Location = new System.Drawing.Point(83, 111);
            this.tbox_cell_size.Name = "tbox_cell_size";
            this.tbox_cell_size.ReadOnly = true;
            this.tbox_cell_size.Size = new System.Drawing.Size(164, 21);
            this.tbox_cell_size.TabIndex = 15;
            // 
            // tbox_process
            // 
            this.tbox_process.BackColor = System.Drawing.Color.DarkSlateGray;
            this.tbox_process.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbox_process.Location = new System.Drawing.Point(83, 85);
            this.tbox_process.Name = "tbox_process";
            this.tbox_process.ReadOnly = true;
            this.tbox_process.Size = new System.Drawing.Size(164, 21);
            this.tbox_process.TabIndex = 14;
            // 
            // tbox_run_name
            // 
            this.tbox_run_name.BackColor = System.Drawing.Color.DarkSlateGray;
            this.tbox_run_name.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbox_run_name.Location = new System.Drawing.Point(83, 31);
            this.tbox_run_name.Name = "tbox_run_name";
            this.tbox_run_name.ReadOnly = true;
            this.tbox_run_name.Size = new System.Drawing.Size(164, 21);
            this.tbox_run_name.TabIndex = 12;
            // 
            // tbox_recipe_name
            // 
            this.tbox_recipe_name.BackColor = System.Drawing.Color.DarkSlateGray;
            this.tbox_recipe_name.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbox_recipe_name.Location = new System.Drawing.Point(83, 58);
            this.tbox_recipe_name.Name = "tbox_recipe_name";
            this.tbox_recipe_name.ReadOnly = true;
            this.tbox_recipe_name.Size = new System.Drawing.Size(164, 21);
            this.tbox_recipe_name.TabIndex = 13;
            // 
            // lb_tact_time
            // 
            this.lb_tact_time.AutoSize = true;
            this.lb_tact_time.BackColor = System.Drawing.Color.Transparent;
            this.lb_tact_time.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_tact_time.ForeColor = System.Drawing.Color.White;
            this.lb_tact_time.Location = new System.Drawing.Point(3, 222);
            this.lb_tact_time.Name = "lb_tact_time";
            this.lb_tact_time.Size = new System.Drawing.Size(80, 20);
            this.lb_tact_time.TabIndex = 8;
            this.lb_tact_time.Text = "Tact-Time";
            // 
            // lb_run_count
            // 
            this.lb_run_count.AutoSize = true;
            this.lb_run_count.BackColor = System.Drawing.Color.Transparent;
            this.lb_run_count.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_run_count.ForeColor = System.Drawing.Color.White;
            this.lb_run_count.Location = new System.Drawing.Point(3, 196);
            this.lb_run_count.Name = "lb_run_count";
            this.lb_run_count.Size = new System.Drawing.Size(74, 20);
            this.lb_run_count.TabIndex = 7;
            this.lb_run_count.Text = "작업 수량";
            // 
            // lb_run_time
            // 
            this.lb_run_time.AutoSize = true;
            this.lb_run_time.BackColor = System.Drawing.Color.Transparent;
            this.lb_run_time.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_run_time.ForeColor = System.Drawing.Color.White;
            this.lb_run_time.Location = new System.Drawing.Point(3, 170);
            this.lb_run_time.Name = "lb_run_time";
            this.lb_run_time.Size = new System.Drawing.Size(74, 20);
            this.lb_run_time.TabIndex = 6;
            this.lb_run_time.Text = "작업 시간";
            // 
            // lb_start_time
            // 
            this.lb_start_time.AutoSize = true;
            this.lb_start_time.BackColor = System.Drawing.Color.Transparent;
            this.lb_start_time.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_start_time.ForeColor = System.Drawing.Color.White;
            this.lb_start_time.Location = new System.Drawing.Point(3, 143);
            this.lb_start_time.Name = "lb_start_time";
            this.lb_start_time.Size = new System.Drawing.Size(74, 20);
            this.lb_start_time.TabIndex = 5;
            this.lb_start_time.Text = "시작 시간";
            // 
            // lb_cell_size
            // 
            this.lb_cell_size.AutoSize = true;
            this.lb_cell_size.BackColor = System.Drawing.Color.Transparent;
            this.lb_cell_size.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_cell_size.ForeColor = System.Drawing.Color.White;
            this.lb_cell_size.Location = new System.Drawing.Point(3, 113);
            this.lb_cell_size.Name = "lb_cell_size";
            this.lb_cell_size.Size = new System.Drawing.Size(74, 20);
            this.lb_cell_size.TabIndex = 4;
            this.lb_cell_size.Text = "셀 사이즈";
            // 
            // lb_process
            // 
            this.lb_process.AutoSize = true;
            this.lb_process.BackColor = System.Drawing.Color.Transparent;
            this.lb_process.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_process.ForeColor = System.Drawing.Color.White;
            this.lb_process.Location = new System.Drawing.Point(3, 86);
            this.lb_process.Name = "lb_process";
            this.lb_process.Size = new System.Drawing.Size(69, 20);
            this.lb_process.TabIndex = 3;
            this.lb_process.Text = "프로세스";
            // 
            // lb_recipe_name
            // 
            this.lb_recipe_name.AutoSize = true;
            this.lb_recipe_name.BackColor = System.Drawing.Color.Transparent;
            this.lb_recipe_name.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_recipe_name.ForeColor = System.Drawing.Color.White;
            this.lb_recipe_name.Location = new System.Drawing.Point(3, 61);
            this.lb_recipe_name.Name = "lb_recipe_name";
            this.lb_recipe_name.Size = new System.Drawing.Size(69, 20);
            this.lb_recipe_name.TabIndex = 2;
            this.lb_recipe_name.Text = "레시피명";
            // 
            // lb_run_name
            // 
            this.lb_run_name.AutoSize = true;
            this.lb_run_name.BackColor = System.Drawing.Color.Transparent;
            this.lb_run_name.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_run_name.ForeColor = System.Drawing.Color.White;
            this.lb_run_name.Location = new System.Drawing.Point(3, 33);
            this.lb_run_name.Name = "lb_run_name";
            this.lb_run_name.Size = new System.Drawing.Size(69, 20);
            this.lb_run_name.TabIndex = 1;
            this.lb_run_name.Text = "작업자명";
            // 
            // lb_run_info
            // 
            this.lb_run_info.AutoSize = true;
            this.lb_run_info.BackColor = System.Drawing.Color.Transparent;
            this.lb_run_info.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lb_run_info.ForeColor = System.Drawing.Color.White;
            this.lb_run_info.Location = new System.Drawing.Point(3, 7);
            this.lb_run_info.Name = "lb_run_info";
            this.lb_run_info.Size = new System.Drawing.Size(74, 21);
            this.lb_run_info.TabIndex = 0;
            this.lb_run_info.Text = "작업정보";
            // 
            // gbox_laser_info
            // 
            this.gbox_laser_info.Controls.Add(this.lb_shutter);
            this.gbox_laser_info.Controls.Add(this.lb_shutter_info);
            this.gbox_laser_info.Controls.Add(this.lb_power);
            this.gbox_laser_info.Controls.Add(this.lb_power_info);
            this.gbox_laser_info.Controls.Add(this.lb_divider);
            this.gbox_laser_info.Controls.Add(this.lb_divider_info);
            this.gbox_laser_info.Controls.Add(this.lb_pd7);
            this.gbox_laser_info.Controls.Add(this.lb_pd7power_info);
            this.gbox_laser_info.Controls.Add(this.lb_outamplifier);
            this.gbox_laser_info.Controls.Add(this.lb_amplifier);
            this.gbox_laser_info.Controls.Add(this.lb_burst);
            this.gbox_laser_info.Controls.Add(this.lb_pulse_mode);
            this.gbox_laser_info.Controls.Add(this.lb_outamplifier_info);
            this.gbox_laser_info.Controls.Add(this.lb_amplifier_info);
            this.gbox_laser_info.Controls.Add(this.lb_burst_info);
            this.gbox_laser_info.Controls.Add(this.lb_pulsemode_info);
            this.gbox_laser_info.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.gbox_laser_info.ForeColor = System.Drawing.Color.White;
            this.gbox_laser_info.Location = new System.Drawing.Point(4, 255);
            this.gbox_laser_info.Name = "gbox_laser_info";
            this.gbox_laser_info.Size = new System.Drawing.Size(253, 130);
            this.gbox_laser_info.TabIndex = 27;
            this.gbox_laser_info.TabStop = false;
            this.gbox_laser_info.Text = "레이저 정보";
            // 
            // lb_shutter
            // 
            this.lb_shutter.AutoSize = true;
            this.lb_shutter.BackColor = System.Drawing.Color.Transparent;
            this.lb_shutter.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_shutter.ForeColor = System.Drawing.Color.White;
            this.lb_shutter.Location = new System.Drawing.Point(210, 107);
            this.lb_shutter.Name = "lb_shutter";
            this.lb_shutter.Size = new System.Drawing.Size(13, 13);
            this.lb_shutter.TabIndex = 23;
            this.lb_shutter.Text = "0";
            // 
            // lb_shutter_info
            // 
            this.lb_shutter_info.AutoSize = true;
            this.lb_shutter_info.BackColor = System.Drawing.Color.Transparent;
            this.lb_shutter_info.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_shutter_info.ForeColor = System.Drawing.Color.White;
            this.lb_shutter_info.Location = new System.Drawing.Point(142, 107);
            this.lb_shutter_info.Name = "lb_shutter_info";
            this.lb_shutter_info.Size = new System.Drawing.Size(52, 13);
            this.lb_shutter_info.TabIndex = 22;
            this.lb_shutter_info.Text = "Shutter :";
            // 
            // lb_power
            // 
            this.lb_power.AutoSize = true;
            this.lb_power.BackColor = System.Drawing.Color.Transparent;
            this.lb_power.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_power.ForeColor = System.Drawing.Color.White;
            this.lb_power.Location = new System.Drawing.Point(223, 79);
            this.lb_power.Name = "lb_power";
            this.lb_power.Size = new System.Drawing.Size(13, 13);
            this.lb_power.TabIndex = 21;
            this.lb_power.Text = "0";
            // 
            // lb_power_info
            // 
            this.lb_power_info.AutoSize = true;
            this.lb_power_info.BackColor = System.Drawing.Color.Transparent;
            this.lb_power_info.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_power_info.ForeColor = System.Drawing.Color.White;
            this.lb_power_info.Location = new System.Drawing.Point(142, 79);
            this.lb_power_info.Name = "lb_power_info";
            this.lb_power_info.Size = new System.Drawing.Size(65, 13);
            this.lb_power_info.TabIndex = 20;
            this.lb_power_info.Text = "Power(%) :";
            // 
            // lb_divider
            // 
            this.lb_divider.AutoSize = true;
            this.lb_divider.BackColor = System.Drawing.Color.Transparent;
            this.lb_divider.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_divider.ForeColor = System.Drawing.Color.White;
            this.lb_divider.Location = new System.Drawing.Point(208, 51);
            this.lb_divider.Name = "lb_divider";
            this.lb_divider.Size = new System.Drawing.Size(13, 13);
            this.lb_divider.TabIndex = 19;
            this.lb_divider.Text = "0";
            // 
            // lb_divider_info
            // 
            this.lb_divider_info.AutoSize = true;
            this.lb_divider_info.BackColor = System.Drawing.Color.Transparent;
            this.lb_divider_info.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_divider_info.ForeColor = System.Drawing.Color.White;
            this.lb_divider_info.Location = new System.Drawing.Point(142, 51);
            this.lb_divider_info.Name = "lb_divider_info";
            this.lb_divider_info.Size = new System.Drawing.Size(51, 13);
            this.lb_divider_info.TabIndex = 18;
            this.lb_divider_info.Text = "Divider :";
            // 
            // lb_pd7
            // 
            this.lb_pd7.AutoSize = true;
            this.lb_pd7.BackColor = System.Drawing.Color.Transparent;
            this.lb_pd7.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_pd7.ForeColor = System.Drawing.Color.White;
            this.lb_pd7.Location = new System.Drawing.Point(227, 25);
            this.lb_pd7.Name = "lb_pd7";
            this.lb_pd7.Size = new System.Drawing.Size(13, 13);
            this.lb_pd7.TabIndex = 17;
            this.lb_pd7.Text = "0";
            // 
            // lb_pd7power_info
            // 
            this.lb_pd7power_info.AutoSize = true;
            this.lb_pd7power_info.BackColor = System.Drawing.Color.Transparent;
            this.lb_pd7power_info.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_pd7power_info.ForeColor = System.Drawing.Color.White;
            this.lb_pd7power_info.Location = new System.Drawing.Point(142, 25);
            this.lb_pd7power_info.Name = "lb_pd7power_info";
            this.lb_pd7power_info.Size = new System.Drawing.Size(68, 13);
            this.lb_pd7power_info.TabIndex = 16;
            this.lb_pd7power_info.Text = "PD7Power :";
            // 
            // lb_outamplifier
            // 
            this.lb_outamplifier.AutoSize = true;
            this.lb_outamplifier.BackColor = System.Drawing.Color.Transparent;
            this.lb_outamplifier.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_outamplifier.ForeColor = System.Drawing.Color.White;
            this.lb_outamplifier.Location = new System.Drawing.Point(113, 107);
            this.lb_outamplifier.Name = "lb_outamplifier";
            this.lb_outamplifier.Size = new System.Drawing.Size(13, 13);
            this.lb_outamplifier.TabIndex = 15;
            this.lb_outamplifier.Text = "0";
            // 
            // lb_amplifier
            // 
            this.lb_amplifier.AutoSize = true;
            this.lb_amplifier.BackColor = System.Drawing.Color.Transparent;
            this.lb_amplifier.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_amplifier.ForeColor = System.Drawing.Color.White;
            this.lb_amplifier.Location = new System.Drawing.Point(83, 79);
            this.lb_amplifier.Name = "lb_amplifier";
            this.lb_amplifier.Size = new System.Drawing.Size(13, 13);
            this.lb_amplifier.TabIndex = 14;
            this.lb_amplifier.Text = "0";
            // 
            // lb_burst
            // 
            this.lb_burst.AutoSize = true;
            this.lb_burst.BackColor = System.Drawing.Color.Transparent;
            this.lb_burst.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_burst.ForeColor = System.Drawing.Color.White;
            this.lb_burst.Location = new System.Drawing.Point(58, 51);
            this.lb_burst.Name = "lb_burst";
            this.lb_burst.Size = new System.Drawing.Size(13, 13);
            this.lb_burst.TabIndex = 13;
            this.lb_burst.Text = "0";
            // 
            // lb_pulse_mode
            // 
            this.lb_pulse_mode.AutoSize = true;
            this.lb_pulse_mode.BackColor = System.Drawing.Color.Transparent;
            this.lb_pulse_mode.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_pulse_mode.ForeColor = System.Drawing.Color.White;
            this.lb_pulse_mode.Location = new System.Drawing.Point(100, 25);
            this.lb_pulse_mode.Name = "lb_pulse_mode";
            this.lb_pulse_mode.Size = new System.Drawing.Size(13, 13);
            this.lb_pulse_mode.TabIndex = 12;
            this.lb_pulse_mode.Text = "0";
            // 
            // lb_outamplifier_info
            // 
            this.lb_outamplifier_info.AutoSize = true;
            this.lb_outamplifier_info.BackColor = System.Drawing.Color.Transparent;
            this.lb_outamplifier_info.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_outamplifier_info.ForeColor = System.Drawing.Color.White;
            this.lb_outamplifier_info.Location = new System.Drawing.Point(8, 107);
            this.lb_outamplifier_info.Name = "lb_outamplifier_info";
            this.lb_outamplifier_info.Size = new System.Drawing.Size(86, 13);
            this.lb_outamplifier_info.TabIndex = 11;
            this.lb_outamplifier_info.Text = "Out Amplifier :";
            // 
            // lb_amplifier_info
            // 
            this.lb_amplifier_info.AutoSize = true;
            this.lb_amplifier_info.BackColor = System.Drawing.Color.Transparent;
            this.lb_amplifier_info.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_amplifier_info.ForeColor = System.Drawing.Color.White;
            this.lb_amplifier_info.Location = new System.Drawing.Point(6, 79);
            this.lb_amplifier_info.Name = "lb_amplifier_info";
            this.lb_amplifier_info.Size = new System.Drawing.Size(62, 13);
            this.lb_amplifier_info.TabIndex = 10;
            this.lb_amplifier_info.Text = "Amplifier :";
            // 
            // lb_burst_info
            // 
            this.lb_burst_info.AutoSize = true;
            this.lb_burst_info.BackColor = System.Drawing.Color.Transparent;
            this.lb_burst_info.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_burst_info.ForeColor = System.Drawing.Color.White;
            this.lb_burst_info.Location = new System.Drawing.Point(4, 51);
            this.lb_burst_info.Name = "lb_burst_info";
            this.lb_burst_info.Size = new System.Drawing.Size(41, 13);
            this.lb_burst_info.TabIndex = 9;
            this.lb_burst_info.Text = "Burst :";
            // 
            // lb_pulsemode_info
            // 
            this.lb_pulsemode_info.AutoSize = true;
            this.lb_pulsemode_info.BackColor = System.Drawing.Color.Transparent;
            this.lb_pulsemode_info.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_pulsemode_info.ForeColor = System.Drawing.Color.White;
            this.lb_pulsemode_info.Location = new System.Drawing.Point(4, 25);
            this.lb_pulsemode_info.Name = "lb_pulsemode_info";
            this.lb_pulsemode_info.Size = new System.Drawing.Size(77, 13);
            this.lb_pulsemode_info.TabIndex = 8;
            this.lb_pulsemode_info.Text = "Pulse Mode :";
            // 
            // gbox_mcr_count
            // 
            this.gbox_mcr_count.BackColor = System.Drawing.Color.Transparent;
            this.gbox_mcr_count.Controls.Add(this.panel_cstinfo);
            this.gbox_mcr_count.Controls.Add(this.panel_day_info);
            this.gbox_mcr_count.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_mcr_count.ForeColor = System.Drawing.Color.White;
            this.gbox_mcr_count.Location = new System.Drawing.Point(3, 627);
            this.gbox_mcr_count.Name = "gbox_mcr_count";
            this.gbox_mcr_count.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gbox_mcr_count.Size = new System.Drawing.Size(343, 106);
            this.gbox_mcr_count.TabIndex = 31;
            this.gbox_mcr_count.TabStop = false;
            this.gbox_mcr_count.Text = "MCR 카운트";
            // 
            // panel_cstinfo
            // 
            this.panel_cstinfo.BackColor = System.Drawing.Color.DimGray;
            this.panel_cstinfo.Controls.Add(this.lb_cst_total_count);
            this.panel_cstinfo.Controls.Add(this.lb_cst_total_count_info);
            this.panel_cstinfo.Controls.Add(this.lb_cst_read_count);
            this.panel_cstinfo.Controls.Add(this.lb_cst_read_count_info);
            this.panel_cstinfo.Controls.Add(this.lb_cst);
            this.panel_cstinfo.Location = new System.Drawing.Point(4, 63);
            this.panel_cstinfo.Name = "panel_cstinfo";
            this.panel_cstinfo.Size = new System.Drawing.Size(334, 28);
            this.panel_cstinfo.TabIndex = 5;
            // 
            // lb_cst_total_count
            // 
            this.lb_cst_total_count.AutoSize = true;
            this.lb_cst_total_count.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_cst_total_count.Location = new System.Drawing.Point(289, 5);
            this.lb_cst_total_count.Name = "lb_cst_total_count";
            this.lb_cst_total_count.Size = new System.Drawing.Size(17, 18);
            this.lb_cst_total_count.TabIndex = 4;
            this.lb_cst_total_count.Text = "0";
            // 
            // lb_cst_total_count_info
            // 
            this.lb_cst_total_count_info.AutoSize = true;
            this.lb_cst_total_count_info.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_cst_total_count_info.Location = new System.Drawing.Point(192, 5);
            this.lb_cst_total_count_info.Name = "lb_cst_total_count_info";
            this.lb_cst_total_count_info.Size = new System.Drawing.Size(96, 20);
            this.lb_cst_total_count_info.TabIndex = 3;
            this.lb_cst_total_count_info.Text = "total count :";
            // 
            // lb_cst_read_count
            // 
            this.lb_cst_read_count.AutoSize = true;
            this.lb_cst_read_count.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_cst_read_count.Location = new System.Drawing.Point(148, 5);
            this.lb_cst_read_count.Name = "lb_cst_read_count";
            this.lb_cst_read_count.Size = new System.Drawing.Size(17, 18);
            this.lb_cst_read_count.TabIndex = 2;
            this.lb_cst_read_count.Text = "0";
            // 
            // lb_cst_read_count_info
            // 
            this.lb_cst_read_count_info.AutoSize = true;
            this.lb_cst_read_count_info.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_cst_read_count_info.Location = new System.Drawing.Point(52, 5);
            this.lb_cst_read_count_info.Name = "lb_cst_read_count_info";
            this.lb_cst_read_count_info.Size = new System.Drawing.Size(94, 20);
            this.lb_cst_read_count_info.TabIndex = 1;
            this.lb_cst_read_count_info.Text = "read count :";
            // 
            // lb_cst
            // 
            this.lb_cst.AutoSize = true;
            this.lb_cst.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_cst.Location = new System.Drawing.Point(6, 5);
            this.lb_cst.Name = "lb_cst";
            this.lb_cst.Size = new System.Drawing.Size(36, 20);
            this.lb_cst.TabIndex = 0;
            this.lb_cst.Text = "CST";
            // 
            // panel_day_info
            // 
            this.panel_day_info.BackColor = System.Drawing.Color.DimGray;
            this.panel_day_info.Controls.Add(this.lb_day_total_count);
            this.panel_day_info.Controls.Add(this.lb_day_total_count_info);
            this.panel_day_info.Controls.Add(this.lb_day_read_count);
            this.panel_day_info.Controls.Add(this.lb_day_read_count_info);
            this.panel_day_info.Controls.Add(this.lb_day);
            this.panel_day_info.Location = new System.Drawing.Point(4, 28);
            this.panel_day_info.Name = "panel_day_info";
            this.panel_day_info.Size = new System.Drawing.Size(334, 28);
            this.panel_day_info.TabIndex = 4;
            // 
            // lb_day_total_count
            // 
            this.lb_day_total_count.AutoSize = true;
            this.lb_day_total_count.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_day_total_count.Location = new System.Drawing.Point(289, 5);
            this.lb_day_total_count.Name = "lb_day_total_count";
            this.lb_day_total_count.Size = new System.Drawing.Size(17, 18);
            this.lb_day_total_count.TabIndex = 4;
            this.lb_day_total_count.Text = "0";
            // 
            // lb_day_total_count_info
            // 
            this.lb_day_total_count_info.AutoSize = true;
            this.lb_day_total_count_info.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_day_total_count_info.Location = new System.Drawing.Point(192, 5);
            this.lb_day_total_count_info.Name = "lb_day_total_count_info";
            this.lb_day_total_count_info.Size = new System.Drawing.Size(96, 20);
            this.lb_day_total_count_info.TabIndex = 3;
            this.lb_day_total_count_info.Text = "total count :";
            // 
            // lb_day_read_count
            // 
            this.lb_day_read_count.AutoSize = true;
            this.lb_day_read_count.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_day_read_count.Location = new System.Drawing.Point(148, 5);
            this.lb_day_read_count.Name = "lb_day_read_count";
            this.lb_day_read_count.Size = new System.Drawing.Size(17, 18);
            this.lb_day_read_count.TabIndex = 2;
            this.lb_day_read_count.Text = "0";
            // 
            // lb_day_read_count_info
            // 
            this.lb_day_read_count_info.AutoSize = true;
            this.lb_day_read_count_info.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_day_read_count_info.Location = new System.Drawing.Point(52, 5);
            this.lb_day_read_count_info.Name = "lb_day_read_count_info";
            this.lb_day_read_count_info.Size = new System.Drawing.Size(94, 20);
            this.lb_day_read_count_info.TabIndex = 1;
            this.lb_day_read_count_info.Text = "read count :";
            // 
            // lb_day
            // 
            this.lb_day.AutoSize = true;
            this.lb_day.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_day.Location = new System.Drawing.Point(6, 5);
            this.lb_day.Name = "lb_day";
            this.lb_day.Size = new System.Drawing.Size(40, 20);
            this.lb_day.TabIndex = 0;
            this.lb_day.Text = "DAY";
            // 
            // gbox_uld
            // 
            this.gbox_uld.BackColor = System.Drawing.Color.Transparent;
            this.gbox_uld.Controls.Add(this.panel_uld_in_muting);
            this.gbox_uld.Controls.Add(this.panel_uld_out_muting);
            this.gbox_uld.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.gbox_uld.ForeColor = System.Drawing.Color.White;
            this.gbox_uld.Location = new System.Drawing.Point(4, 739);
            this.gbox_uld.Name = "gbox_uld";
            this.gbox_uld.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gbox_uld.Size = new System.Drawing.Size(168, 117);
            this.gbox_uld.TabIndex = 32;
            this.gbox_uld.TabStop = false;
            this.gbox_uld.Text = "언로딩";
            // 
            // panel_uld_in_muting
            // 
            this.panel_uld_in_muting.BackColor = System.Drawing.Color.DimGray;
            this.panel_uld_in_muting.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_uld_in_muting.Controls.Add(this.lb_uld_in_muting_info);
            this.panel_uld_in_muting.Location = new System.Drawing.Point(6, 69);
            this.panel_uld_in_muting.Name = "panel_uld_in_muting";
            this.panel_uld_in_muting.Size = new System.Drawing.Size(156, 35);
            this.panel_uld_in_muting.TabIndex = 1;
            // 
            // lb_uld_in_muting_info
            // 
            this.lb_uld_in_muting_info.AutoSize = true;
            this.lb_uld_in_muting_info.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_uld_in_muting_info.Location = new System.Drawing.Point(42, 7);
            this.lb_uld_in_muting_info.Name = "lb_uld_in_muting_info";
            this.lb_uld_in_muting_info.Size = new System.Drawing.Size(74, 20);
            this.lb_uld_in_muting_info.TabIndex = 0;
            this.lb_uld_in_muting_info.Text = "투입 뮤팅";
            // 
            // panel_uld_out_muting
            // 
            this.panel_uld_out_muting.BackColor = System.Drawing.Color.DimGray;
            this.panel_uld_out_muting.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_uld_out_muting.Controls.Add(this.lb_uld_out_muting_info);
            this.panel_uld_out_muting.Location = new System.Drawing.Point(6, 25);
            this.panel_uld_out_muting.Name = "panel_uld_out_muting";
            this.panel_uld_out_muting.Size = new System.Drawing.Size(156, 35);
            this.panel_uld_out_muting.TabIndex = 0;
            // 
            // lb_uld_out_muting_info
            // 
            this.lb_uld_out_muting_info.AutoSize = true;
            this.lb_uld_out_muting_info.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_uld_out_muting_info.Location = new System.Drawing.Point(43, 7);
            this.lb_uld_out_muting_info.Name = "lb_uld_out_muting_info";
            this.lb_uld_out_muting_info.Size = new System.Drawing.Size(74, 20);
            this.lb_uld_out_muting_info.TabIndex = 0;
            this.lb_uld_out_muting_info.Text = "배출 뮤팅";
            // 
            // gbox_ld
            // 
            this.gbox_ld.BackColor = System.Drawing.Color.Transparent;
            this.gbox_ld.Controls.Add(this.panel_ld_out_muting);
            this.gbox_ld.Controls.Add(this.panel_ld_in_muting);
            this.gbox_ld.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.gbox_ld.ForeColor = System.Drawing.Color.White;
            this.gbox_ld.Location = new System.Drawing.Point(178, 739);
            this.gbox_ld.Name = "gbox_ld";
            this.gbox_ld.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gbox_ld.Size = new System.Drawing.Size(168, 117);
            this.gbox_ld.TabIndex = 33;
            this.gbox_ld.TabStop = false;
            this.gbox_ld.Text = "로딩";
            // 
            // panel_ld_out_muting
            // 
            this.panel_ld_out_muting.BackColor = System.Drawing.Color.DimGray;
            this.panel_ld_out_muting.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_ld_out_muting.Controls.Add(this.lb_ld_out_muting_info);
            this.panel_ld_out_muting.Location = new System.Drawing.Point(6, 69);
            this.panel_ld_out_muting.Name = "panel_ld_out_muting";
            this.panel_ld_out_muting.Size = new System.Drawing.Size(156, 35);
            this.panel_ld_out_muting.TabIndex = 3;
            // 
            // lb_ld_out_muting_info
            // 
            this.lb_ld_out_muting_info.AutoSize = true;
            this.lb_ld_out_muting_info.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_ld_out_muting_info.Location = new System.Drawing.Point(42, 6);
            this.lb_ld_out_muting_info.Name = "lb_ld_out_muting_info";
            this.lb_ld_out_muting_info.Size = new System.Drawing.Size(74, 20);
            this.lb_ld_out_muting_info.TabIndex = 0;
            this.lb_ld_out_muting_info.Text = "배출 뮤팅";
            // 
            // panel_ld_in_muting
            // 
            this.panel_ld_in_muting.BackColor = System.Drawing.Color.DimGray;
            this.panel_ld_in_muting.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_ld_in_muting.Controls.Add(this.lb_ld_in_muting_info);
            this.panel_ld_in_muting.Location = new System.Drawing.Point(6, 25);
            this.panel_ld_in_muting.Name = "panel_ld_in_muting";
            this.panel_ld_in_muting.Size = new System.Drawing.Size(156, 35);
            this.panel_ld_in_muting.TabIndex = 2;
            // 
            // lb_ld_in_muting_info
            // 
            this.lb_ld_in_muting_info.AutoSize = true;
            this.lb_ld_in_muting_info.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_ld_in_muting_info.Location = new System.Drawing.Point(42, 6);
            this.lb_ld_in_muting_info.Name = "lb_ld_in_muting_info";
            this.lb_ld_in_muting_info.Size = new System.Drawing.Size(74, 20);
            this.lb_ld_in_muting_info.TabIndex = 0;
            this.lb_ld_in_muting_info.Text = "투입 뮤팅";
            // 
            // btn_safety_reset
            // 
            this.btn_safety_reset.BackColor = System.Drawing.Color.DimGray;
            this.btn_safety_reset.Font = new System.Drawing.Font("맑은 고딕", 17F, System.Drawing.FontStyle.Bold);
            this.btn_safety_reset.ForeColor = System.Drawing.Color.White;
            this.btn_safety_reset.Location = new System.Drawing.Point(352, 745);
            this.btn_safety_reset.Name = "btn_safety_reset";
            this.btn_safety_reset.Size = new System.Drawing.Size(168, 111);
            this.btn_safety_reset.TabIndex = 28;
            this.btn_safety_reset.Text = "세이프티 리셋";
            this.btn_safety_reset.UseVisualStyleBackColor = false;
            // 
            // panel_run
            // 
            this.panel_run.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_run.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_run.Controls.Add(this.panel_lot_b_info);
            this.panel_run.Controls.Add(this.panel_lot_a_info);
            this.panel_run.Controls.Add(this.panel_ld_muting_out);
            this.panel_run.Controls.Add(this.panel_uld_muting_in);
            this.panel_run.Controls.Add(this.panel_ld_muting_in);
            this.panel_run.Controls.Add(this.panel_uld_muting_out);
            this.panel_run.Controls.Add(this.panel_door2);
            this.panel_run.Controls.Add(this.panel38);
            this.panel_run.Controls.Add(this.panel_door3);
            this.panel_run.Controls.Add(this.panel_door5);
            this.panel_run.Controls.Add(this.panel_door4);
            this.panel_run.Controls.Add(this.panel_door6);
            this.panel_run.Controls.Add(this.panel_door8);
            this.panel_run.Controls.Add(this.panel_door7);
            this.panel_run.Controls.Add(this.panel_door9);
            this.panel_run.Controls.Add(this.panel_b_cst_skip);
            this.panel_run.Controls.Add(this.panel_a_cst_skip);
            this.panel_run.Controls.Add(this.panel_ld_box);
            this.panel_run.Controls.Add(this.panel_ems3);
            this.panel_run.Controls.Add(this.panel_process_box);
            this.panel_run.Controls.Add(this.panel_ems5);
            this.panel_run.Controls.Add(this.panel_uld_box);
            this.panel_run.Controls.Add(this.panel_run2);
            this.panel_run.Controls.Add(this.panel_door10);
            this.panel_run.Controls.Add(this.panel2);
            this.panel_run.Controls.Add(this.panel_camera_l_info);
            this.panel_run.Location = new System.Drawing.Point(471, 3);
            this.panel_run.Name = "panel_run";
            this.panel_run.Size = new System.Drawing.Size(1056, 382);
            this.panel_run.TabIndex = 60;
            // 
            // panel_lot_b_info
            // 
            this.panel_lot_b_info.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_lot_b_info.Controls.Add(this.label5);
            this.panel_lot_b_info.Controls.Add(this.lb_lot_b_info);
            this.panel_lot_b_info.Location = new System.Drawing.Point(887, 156);
            this.panel_lot_b_info.Name = "panel_lot_b_info";
            this.panel_lot_b_info.Size = new System.Drawing.Size(168, 33);
            this.panel_lot_b_info.TabIndex = 24;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.ForeColor = System.Drawing.Color.YellowGreen;
            this.label5.Location = new System.Drawing.Point(10, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 12);
            this.label5.TabIndex = 1;
            this.label5.Text = "ABCDEFGHIJK";
            // 
            // lb_lot_b_info
            // 
            this.lb_lot_b_info.AutoSize = true;
            this.lb_lot_b_info.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_lot_b_info.ForeColor = System.Drawing.Color.YellowGreen;
            this.lb_lot_b_info.Location = new System.Drawing.Point(5, 2);
            this.lb_lot_b_info.Name = "lb_lot_b_info";
            this.lb_lot_b_info.Size = new System.Drawing.Size(64, 12);
            this.lb_lot_b_info.TabIndex = 0;
            this.lb_lot_b_info.Text = "LOT ID B";
            // 
            // panel_lot_a_info
            // 
            this.panel_lot_a_info.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_lot_a_info.Controls.Add(this.lb_lot_a);
            this.panel_lot_a_info.Controls.Add(this.lb_lot_a_info);
            this.panel_lot_a_info.Location = new System.Drawing.Point(887, 191);
            this.panel_lot_a_info.Name = "panel_lot_a_info";
            this.panel_lot_a_info.Size = new System.Drawing.Size(168, 33);
            this.panel_lot_a_info.TabIndex = 23;
            // 
            // lb_lot_a
            // 
            this.lb_lot_a.AutoSize = true;
            this.lb_lot_a.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_lot_a.ForeColor = System.Drawing.Color.YellowGreen;
            this.lb_lot_a.Location = new System.Drawing.Point(10, 16);
            this.lb_lot_a.Name = "lb_lot_a";
            this.lb_lot_a.Size = new System.Drawing.Size(98, 12);
            this.lb_lot_a.TabIndex = 1;
            this.lb_lot_a.Text = "ABCDEFGHIJK";
            // 
            // lb_lot_a_info
            // 
            this.lb_lot_a_info.AutoSize = true;
            this.lb_lot_a_info.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_lot_a_info.ForeColor = System.Drawing.Color.YellowGreen;
            this.lb_lot_a_info.Location = new System.Drawing.Point(5, 2);
            this.lb_lot_a_info.Name = "lb_lot_a_info";
            this.lb_lot_a_info.Size = new System.Drawing.Size(64, 12);
            this.lb_lot_a_info.TabIndex = 0;
            this.lb_lot_a_info.Text = "LOT ID A";
            // 
            // panel_ld_muting_out
            // 
            this.panel_ld_muting_out.BackColor = System.Drawing.Color.DimGray;
            this.panel_ld_muting_out.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_ld_muting_out.Controls.Add(this.lb_ld_muting_out);
            this.panel_ld_muting_out.ForeColor = System.Drawing.Color.White;
            this.panel_ld_muting_out.Location = new System.Drawing.Point(844, 195);
            this.panel_ld_muting_out.Name = "panel_ld_muting_out";
            this.panel_ld_muting_out.Size = new System.Drawing.Size(37, 39);
            this.panel_ld_muting_out.TabIndex = 19;
            // 
            // lb_ld_muting_out
            // 
            this.lb_ld_muting_out.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld_muting_out.ForeColor = System.Drawing.Color.White;
            this.lb_ld_muting_out.Location = new System.Drawing.Point(3, 3);
            this.lb_ld_muting_out.Name = "lb_ld_muting_out";
            this.lb_ld_muting_out.Size = new System.Drawing.Size(32, 33);
            this.lb_ld_muting_out.TabIndex = 1;
            this.lb_ld_muting_out.Text = "뮤팅아웃";
            this.lb_ld_muting_out.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_uld_muting_in
            // 
            this.panel_uld_muting_in.BackColor = System.Drawing.Color.DimGray;
            this.panel_uld_muting_in.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_uld_muting_in.Controls.Add(this.lb_uld_muting_in);
            this.panel_uld_muting_in.ForeColor = System.Drawing.Color.White;
            this.panel_uld_muting_in.Location = new System.Drawing.Point(4, 175);
            this.panel_uld_muting_in.Name = "panel_uld_muting_in";
            this.panel_uld_muting_in.Size = new System.Drawing.Size(37, 39);
            this.panel_uld_muting_in.TabIndex = 15;
            // 
            // lb_uld_muting_in
            // 
            this.lb_uld_muting_in.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld_muting_in.ForeColor = System.Drawing.Color.White;
            this.lb_uld_muting_in.Location = new System.Drawing.Point(3, 3);
            this.lb_uld_muting_in.Name = "lb_uld_muting_in";
            this.lb_uld_muting_in.Size = new System.Drawing.Size(32, 33);
            this.lb_uld_muting_in.TabIndex = 1;
            this.lb_uld_muting_in.Text = "뮤팅인";
            this.lb_uld_muting_in.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_ld_muting_in
            // 
            this.panel_ld_muting_in.BackColor = System.Drawing.Color.DimGray;
            this.panel_ld_muting_in.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_ld_muting_in.Controls.Add(this.lb_ld_muting_in);
            this.panel_ld_muting_in.ForeColor = System.Drawing.Color.White;
            this.panel_ld_muting_in.Location = new System.Drawing.Point(844, 150);
            this.panel_ld_muting_in.Name = "panel_ld_muting_in";
            this.panel_ld_muting_in.Size = new System.Drawing.Size(37, 39);
            this.panel_ld_muting_in.TabIndex = 18;
            // 
            // lb_ld_muting_in
            // 
            this.lb_ld_muting_in.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld_muting_in.ForeColor = System.Drawing.Color.White;
            this.lb_ld_muting_in.Location = new System.Drawing.Point(3, 3);
            this.lb_ld_muting_in.Name = "lb_ld_muting_in";
            this.lb_ld_muting_in.Size = new System.Drawing.Size(32, 33);
            this.lb_ld_muting_in.TabIndex = 1;
            this.lb_ld_muting_in.Text = "뮤팅인";
            this.lb_ld_muting_in.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_uld_muting_out
            // 
            this.panel_uld_muting_out.BackColor = System.Drawing.Color.DimGray;
            this.panel_uld_muting_out.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_uld_muting_out.Controls.Add(this.lb_uld_muting_out);
            this.panel_uld_muting_out.ForeColor = System.Drawing.Color.White;
            this.panel_uld_muting_out.Location = new System.Drawing.Point(4, 130);
            this.panel_uld_muting_out.Name = "panel_uld_muting_out";
            this.panel_uld_muting_out.Size = new System.Drawing.Size(37, 39);
            this.panel_uld_muting_out.TabIndex = 14;
            // 
            // lb_uld_muting_out
            // 
            this.lb_uld_muting_out.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld_muting_out.ForeColor = System.Drawing.Color.White;
            this.lb_uld_muting_out.Location = new System.Drawing.Point(3, 3);
            this.lb_uld_muting_out.Name = "lb_uld_muting_out";
            this.lb_uld_muting_out.Size = new System.Drawing.Size(32, 33);
            this.lb_uld_muting_out.TabIndex = 1;
            this.lb_uld_muting_out.Text = "뮤팅아웃";
            this.lb_uld_muting_out.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_door2
            // 
            this.panel_door2.BackColor = System.Drawing.Color.DimGray;
            this.panel_door2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_door2.Controls.Add(this.lb_door2);
            this.panel_door2.ForeColor = System.Drawing.Color.White;
            this.panel_door2.Location = new System.Drawing.Point(844, 85);
            this.panel_door2.Name = "panel_door2";
            this.panel_door2.Size = new System.Drawing.Size(37, 59);
            this.panel_door2.TabIndex = 16;
            // 
            // lb_door2
            // 
            this.lb_door2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_door2.ForeColor = System.Drawing.Color.White;
            this.lb_door2.Location = new System.Drawing.Point(10, 3);
            this.lb_door2.Name = "lb_door2";
            this.lb_door2.Size = new System.Drawing.Size(15, 51);
            this.lb_door2.TabIndex = 1;
            this.lb_door2.Text = "도어2";
            this.lb_door2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel38
            // 
            this.panel38.BackColor = System.Drawing.Color.DimGray;
            this.panel38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel38.Controls.Add(this.label51);
            this.panel38.ForeColor = System.Drawing.Color.White;
            this.panel38.Location = new System.Drawing.Point(845, 86);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(37, 59);
            this.panel38.TabIndex = 17;
            // 
            // label51
            // 
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label51.ForeColor = System.Drawing.Color.White;
            this.label51.Location = new System.Drawing.Point(4, 3);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(27, 51);
            this.label51.TabIndex = 1;
            this.label51.Text = "도어10";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_door3
            // 
            this.panel_door3.BackColor = System.Drawing.Color.DimGray;
            this.panel_door3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_door3.Controls.Add(this.lb_door3);
            this.panel_door3.ForeColor = System.Drawing.Color.White;
            this.panel_door3.Location = new System.Drawing.Point(692, 344);
            this.panel_door3.Name = "panel_door3";
            this.panel_door3.Size = new System.Drawing.Size(67, 33);
            this.panel_door3.TabIndex = 19;
            // 
            // lb_door3
            // 
            this.lb_door3.AutoSize = true;
            this.lb_door3.BackColor = System.Drawing.Color.Transparent;
            this.lb_door3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_door3.ForeColor = System.Drawing.Color.White;
            this.lb_door3.Location = new System.Drawing.Point(11, 9);
            this.lb_door3.Name = "lb_door3";
            this.lb_door3.Size = new System.Drawing.Size(43, 12);
            this.lb_door3.TabIndex = 4;
            this.lb_door3.Text = "도어 3";
            // 
            // panel_door5
            // 
            this.panel_door5.BackColor = System.Drawing.Color.DimGray;
            this.panel_door5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_door5.Controls.Add(this.lb_door5);
            this.panel_door5.ForeColor = System.Drawing.Color.White;
            this.panel_door5.Location = new System.Drawing.Point(495, 344);
            this.panel_door5.Name = "panel_door5";
            this.panel_door5.Size = new System.Drawing.Size(67, 33);
            this.panel_door5.TabIndex = 20;
            // 
            // lb_door5
            // 
            this.lb_door5.AutoSize = true;
            this.lb_door5.BackColor = System.Drawing.Color.Transparent;
            this.lb_door5.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_door5.ForeColor = System.Drawing.Color.White;
            this.lb_door5.Location = new System.Drawing.Point(11, 9);
            this.lb_door5.Name = "lb_door5";
            this.lb_door5.Size = new System.Drawing.Size(43, 12);
            this.lb_door5.TabIndex = 4;
            this.lb_door5.Text = "도어 5";
            // 
            // panel_door4
            // 
            this.panel_door4.BackColor = System.Drawing.Color.DimGray;
            this.panel_door4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_door4.Controls.Add(this.lb_door4);
            this.panel_door4.ForeColor = System.Drawing.Color.White;
            this.panel_door4.Location = new System.Drawing.Point(619, 344);
            this.panel_door4.Name = "panel_door4";
            this.panel_door4.Size = new System.Drawing.Size(67, 33);
            this.panel_door4.TabIndex = 18;
            // 
            // lb_door4
            // 
            this.lb_door4.AutoSize = true;
            this.lb_door4.BackColor = System.Drawing.Color.Transparent;
            this.lb_door4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_door4.ForeColor = System.Drawing.Color.White;
            this.lb_door4.Location = new System.Drawing.Point(11, 9);
            this.lb_door4.Name = "lb_door4";
            this.lb_door4.Size = new System.Drawing.Size(43, 12);
            this.lb_door4.TabIndex = 4;
            this.lb_door4.Text = "도어 4";
            // 
            // panel_door6
            // 
            this.panel_door6.BackColor = System.Drawing.Color.DimGray;
            this.panel_door6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_door6.Controls.Add(this.lb_door6);
            this.panel_door6.ForeColor = System.Drawing.Color.White;
            this.panel_door6.Location = new System.Drawing.Point(422, 344);
            this.panel_door6.Name = "panel_door6";
            this.panel_door6.Size = new System.Drawing.Size(67, 33);
            this.panel_door6.TabIndex = 19;
            // 
            // lb_door6
            // 
            this.lb_door6.AutoSize = true;
            this.lb_door6.BackColor = System.Drawing.Color.Transparent;
            this.lb_door6.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_door6.ForeColor = System.Drawing.Color.White;
            this.lb_door6.Location = new System.Drawing.Point(11, 9);
            this.lb_door6.Name = "lb_door6";
            this.lb_door6.Size = new System.Drawing.Size(43, 12);
            this.lb_door6.TabIndex = 4;
            this.lb_door6.Text = "도어 6";
            // 
            // panel_door8
            // 
            this.panel_door8.BackColor = System.Drawing.Color.DimGray;
            this.panel_door8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_door8.Controls.Add(this.lb_door8);
            this.panel_door8.ForeColor = System.Drawing.Color.White;
            this.panel_door8.Location = new System.Drawing.Point(223, 344);
            this.panel_door8.Name = "panel_door8";
            this.panel_door8.Size = new System.Drawing.Size(67, 33);
            this.panel_door8.TabIndex = 17;
            // 
            // lb_door8
            // 
            this.lb_door8.AutoSize = true;
            this.lb_door8.BackColor = System.Drawing.Color.Transparent;
            this.lb_door8.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_door8.ForeColor = System.Drawing.Color.White;
            this.lb_door8.Location = new System.Drawing.Point(11, 9);
            this.lb_door8.Name = "lb_door8";
            this.lb_door8.Size = new System.Drawing.Size(43, 12);
            this.lb_door8.TabIndex = 4;
            this.lb_door8.Text = "도어 8";
            // 
            // panel_door7
            // 
            this.panel_door7.BackColor = System.Drawing.Color.DimGray;
            this.panel_door7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_door7.Controls.Add(this.lb_door7);
            this.panel_door7.ForeColor = System.Drawing.Color.White;
            this.panel_door7.Location = new System.Drawing.Point(349, 344);
            this.panel_door7.Name = "panel_door7";
            this.panel_door7.Size = new System.Drawing.Size(67, 33);
            this.panel_door7.TabIndex = 18;
            // 
            // lb_door7
            // 
            this.lb_door7.AutoSize = true;
            this.lb_door7.BackColor = System.Drawing.Color.Transparent;
            this.lb_door7.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_door7.ForeColor = System.Drawing.Color.White;
            this.lb_door7.Location = new System.Drawing.Point(11, 9);
            this.lb_door7.Name = "lb_door7";
            this.lb_door7.Size = new System.Drawing.Size(43, 12);
            this.lb_door7.TabIndex = 4;
            this.lb_door7.Text = "도어 7";
            // 
            // panel_door9
            // 
            this.panel_door9.BackColor = System.Drawing.Color.DimGray;
            this.panel_door9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_door9.Controls.Add(this.lb_door9);
            this.panel_door9.ForeColor = System.Drawing.Color.White;
            this.panel_door9.Location = new System.Drawing.Point(150, 344);
            this.panel_door9.Name = "panel_door9";
            this.panel_door9.Size = new System.Drawing.Size(67, 33);
            this.panel_door9.TabIndex = 16;
            // 
            // lb_door9
            // 
            this.lb_door9.AutoSize = true;
            this.lb_door9.BackColor = System.Drawing.Color.Transparent;
            this.lb_door9.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_door9.ForeColor = System.Drawing.Color.White;
            this.lb_door9.Location = new System.Drawing.Point(11, 9);
            this.lb_door9.Name = "lb_door9";
            this.lb_door9.Size = new System.Drawing.Size(43, 12);
            this.lb_door9.TabIndex = 4;
            this.lb_door9.Text = "도어 9";
            // 
            // panel_b_cst_skip
            // 
            this.panel_b_cst_skip.BackColor = System.Drawing.Color.Gray;
            this.panel_b_cst_skip.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_b_cst_skip.Controls.Add(this.lb_b_cst_skip);
            this.panel_b_cst_skip.ForeColor = System.Drawing.Color.White;
            this.panel_b_cst_skip.Location = new System.Drawing.Point(768, 3);
            this.panel_b_cst_skip.Name = "panel_b_cst_skip";
            this.panel_b_cst_skip.Size = new System.Drawing.Size(64, 33);
            this.panel_b_cst_skip.TabIndex = 17;
            // 
            // lb_b_cst_skip
            // 
            this.lb_b_cst_skip.BackColor = System.Drawing.Color.Transparent;
            this.lb_b_cst_skip.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_b_cst_skip.ForeColor = System.Drawing.Color.White;
            this.lb_b_cst_skip.Location = new System.Drawing.Point(4, 2);
            this.lb_b_cst_skip.Name = "lb_b_cst_skip";
            this.lb_b_cst_skip.Size = new System.Drawing.Size(55, 26);
            this.lb_b_cst_skip.TabIndex = 5;
            this.lb_b_cst_skip.Text = "B-CST Skip";
            this.lb_b_cst_skip.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_a_cst_skip
            // 
            this.panel_a_cst_skip.BackColor = System.Drawing.Color.Gray;
            this.panel_a_cst_skip.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_a_cst_skip.Controls.Add(this.lb_a_cst_skip);
            this.panel_a_cst_skip.ForeColor = System.Drawing.Color.White;
            this.panel_a_cst_skip.Location = new System.Drawing.Point(695, 3);
            this.panel_a_cst_skip.Name = "panel_a_cst_skip";
            this.panel_a_cst_skip.Size = new System.Drawing.Size(64, 33);
            this.panel_a_cst_skip.TabIndex = 16;
            // 
            // lb_a_cst_skip
            // 
            this.lb_a_cst_skip.BackColor = System.Drawing.Color.Transparent;
            this.lb_a_cst_skip.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_a_cst_skip.ForeColor = System.Drawing.Color.White;
            this.lb_a_cst_skip.Location = new System.Drawing.Point(6, 2);
            this.lb_a_cst_skip.Name = "lb_a_cst_skip";
            this.lb_a_cst_skip.Size = new System.Drawing.Size(55, 26);
            this.lb_a_cst_skip.TabIndex = 4;
            this.lb_a_cst_skip.Text = "A-CST Skip";
            this.lb_a_cst_skip.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_ld_box
            // 
            this.panel_ld_box.BackColor = System.Drawing.Color.DimGray;
            this.panel_ld_box.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_ld_box.Controls.Add(this.lb_ld_box);
            this.panel_ld_box.ForeColor = System.Drawing.Color.White;
            this.panel_ld_box.Location = new System.Drawing.Point(564, 3);
            this.panel_ld_box.Name = "panel_ld_box";
            this.panel_ld_box.Size = new System.Drawing.Size(125, 33);
            this.panel_ld_box.TabIndex = 16;
            // 
            // lb_ld_box
            // 
            this.lb_ld_box.AutoSize = true;
            this.lb_ld_box.BackColor = System.Drawing.Color.Transparent;
            this.lb_ld_box.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld_box.ForeColor = System.Drawing.Color.White;
            this.lb_ld_box.Location = new System.Drawing.Point(18, 9);
            this.lb_ld_box.Name = "lb_ld_box";
            this.lb_ld_box.Size = new System.Drawing.Size(88, 12);
            this.lb_ld_box.TabIndex = 4;
            this.lb_ld_box.Text = "로더 전장박스";
            // 
            // panel_ems3
            // 
            this.panel_ems3.BackColor = System.Drawing.Color.DimGray;
            this.panel_ems3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_ems3.Controls.Add(this.lb_ems3);
            this.panel_ems3.ForeColor = System.Drawing.Color.White;
            this.panel_ems3.Location = new System.Drawing.Point(488, 3);
            this.panel_ems3.Name = "panel_ems3";
            this.panel_ems3.Size = new System.Drawing.Size(55, 33);
            this.panel_ems3.TabIndex = 16;
            // 
            // lb_ems3
            // 
            this.lb_ems3.AutoSize = true;
            this.lb_ems3.BackColor = System.Drawing.Color.Transparent;
            this.lb_ems3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ems3.ForeColor = System.Drawing.Color.White;
            this.lb_ems3.Location = new System.Drawing.Point(4, 9);
            this.lb_ems3.Name = "lb_ems3";
            this.lb_ems3.Size = new System.Drawing.Size(47, 12);
            this.lb_ems3.TabIndex = 4;
            this.lb_ems3.Text = "EMS 3";
            // 
            // panel_process_box
            // 
            this.panel_process_box.BackColor = System.Drawing.Color.DimGray;
            this.panel_process_box.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_process_box.Controls.Add(this.lb_process_box);
            this.panel_process_box.ForeColor = System.Drawing.Color.White;
            this.panel_process_box.Location = new System.Drawing.Point(341, 3);
            this.panel_process_box.Name = "panel_process_box";
            this.panel_process_box.Size = new System.Drawing.Size(125, 33);
            this.panel_process_box.TabIndex = 15;
            // 
            // lb_process_box
            // 
            this.lb_process_box.AutoSize = true;
            this.lb_process_box.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_box.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_box.ForeColor = System.Drawing.Color.White;
            this.lb_process_box.Location = new System.Drawing.Point(6, 9);
            this.lb_process_box.Name = "lb_process_box";
            this.lb_process_box.Size = new System.Drawing.Size(114, 12);
            this.lb_process_box.TabIndex = 4;
            this.lb_process_box.Text = "프로세스 전장박스";
            // 
            // panel_ems5
            // 
            this.panel_ems5.BackColor = System.Drawing.Color.DimGray;
            this.panel_ems5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_ems5.Controls.Add(this.lb_ems5);
            this.panel_ems5.ForeColor = System.Drawing.Color.White;
            this.panel_ems5.Location = new System.Drawing.Point(263, 3);
            this.panel_ems5.Name = "panel_ems5";
            this.panel_ems5.Size = new System.Drawing.Size(55, 33);
            this.panel_ems5.TabIndex = 15;
            // 
            // lb_ems5
            // 
            this.lb_ems5.AutoSize = true;
            this.lb_ems5.BackColor = System.Drawing.Color.Transparent;
            this.lb_ems5.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ems5.ForeColor = System.Drawing.Color.White;
            this.lb_ems5.Location = new System.Drawing.Point(3, 9);
            this.lb_ems5.Name = "lb_ems5";
            this.lb_ems5.Size = new System.Drawing.Size(47, 12);
            this.lb_ems5.TabIndex = 4;
            this.lb_ems5.Text = "EMS 5";
            // 
            // panel_uld_box
            // 
            this.panel_uld_box.BackColor = System.Drawing.Color.DimGray;
            this.panel_uld_box.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_uld_box.Controls.Add(this.lb_uld_box);
            this.panel_uld_box.ForeColor = System.Drawing.Color.White;
            this.panel_uld_box.Location = new System.Drawing.Point(111, 3);
            this.panel_uld_box.Name = "panel_uld_box";
            this.panel_uld_box.Size = new System.Drawing.Size(125, 33);
            this.panel_uld_box.TabIndex = 14;
            // 
            // lb_uld_box
            // 
            this.lb_uld_box.AutoSize = true;
            this.lb_uld_box.BackColor = System.Drawing.Color.Transparent;
            this.lb_uld_box.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld_box.ForeColor = System.Drawing.Color.White;
            this.lb_uld_box.Location = new System.Drawing.Point(12, 9);
            this.lb_uld_box.Name = "lb_uld_box";
            this.lb_uld_box.Size = new System.Drawing.Size(101, 12);
            this.lb_uld_box.TabIndex = 4;
            this.lb_uld_box.Text = "언로더 전장박스";
            // 
            // panel_run2
            // 
            this.panel_run2.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_run2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_run2.Controls.Add(this.panel_ld_light);
            this.panel_run2.Controls.Add(this.panel_process_light);
            this.panel_run2.Controls.Add(this.panel_uld_light);
            this.panel_run2.Controls.Add(this.panel_ems1);
            this.panel_run2.Controls.Add(this.panel_target_avgpower);
            this.panel_run2.Controls.Add(this.panel_zpos_gap);
            this.panel_run2.Controls.Add(this.panel_ems4);
            this.panel_run2.Controls.Add(this.panel_ld_out_b);
            this.panel_run2.Controls.Add(this.panel_ld_in_b);
            this.panel_run2.Controls.Add(this.panel_ld_out_a);
            this.panel_run2.Controls.Add(this.panel_ld_in_a);
            this.panel_run2.Controls.Add(this.panel_uld_in_b);
            this.panel_run2.Controls.Add(this.panel_uld_out_b);
            this.panel_run2.Controls.Add(this.panel_uld_in_a);
            this.panel_run2.Controls.Add(this.panel_uld_out_a);
            this.panel_run2.Controls.Add(this.panel_uld1_out2);
            this.panel_run2.Controls.Add(this.panel_uld2_out2);
            this.panel_run2.Controls.Add(this.panel_uld1_table);
            this.panel_run2.Controls.Add(this.panel_uld2_table);
            this.panel_run2.Controls.Add(this.panel_uld1_out1);
            this.panel_run2.Controls.Add(this.panel_uld2_out1);
            this.panel_run2.Controls.Add(this.panel_uld1);
            this.panel_run2.Controls.Add(this.panel_uld2);
            this.panel_run2.Controls.Add(this.panel_uld_trans_a1);
            this.panel_run2.Controls.Add(this.panel_uld_trans_b1);
            this.panel_run2.Controls.Add(this.panel_uld_trans_a2);
            this.panel_run2.Controls.Add(this.panel_uld_trans_b2);
            this.panel_run2.Controls.Add(this.panel_break_table_a1);
            this.panel_run2.Controls.Add(this.panel_break_table_b1);
            this.panel_run2.Controls.Add(this.panel_break_trans1);
            this.panel_run2.Controls.Add(this.panel_break_trans2);
            this.panel_run2.Controls.Add(this.panel_break_table_a2);
            this.panel_run2.Controls.Add(this.panel_break_table_b2);
            this.panel_run2.Controls.Add(this.panel_process_table_a1);
            this.panel_run2.Controls.Add(this.panel_process_table_b1);
            this.panel_run2.Controls.Add(this.panel_process_table_a2);
            this.panel_run2.Controls.Add(this.panel_ld_trans1);
            this.panel_run2.Controls.Add(this.panel_process_table_b2);
            this.panel_run2.Controls.Add(this.panel_ld1);
            this.panel_run2.Controls.Add(this.panel_ld_trans2);
            this.panel_run2.Controls.Add(this.panel_ld1_table);
            this.panel_run2.Controls.Add(this.panel_ld2);
            this.panel_run2.Controls.Add(this.panel_ld1_in2);
            this.panel_run2.Controls.Add(this.panel_ld2_table);
            this.panel_run2.Controls.Add(this.panel_ld1_in1);
            this.panel_run2.Controls.Add(this.panel_ld2_in2);
            this.panel_run2.Controls.Add(this.panel_ld2_in1);
            this.panel_run2.Controls.Add(this.lb_ld1_o_time);
            this.panel_run2.Controls.Add(this.lb_ld1_o);
            this.panel_run2.Controls.Add(this.lb_ld1_i_time);
            this.panel_run2.Controls.Add(this.lb_ld1_i);
            this.panel_run2.Controls.Add(this.lb_ld2_o_time);
            this.panel_run2.Controls.Add(this.lb_ld2_o);
            this.panel_run2.Controls.Add(this.lb_ld2_i_time);
            this.panel_run2.Controls.Add(this.lb_ld2_i);
            this.panel_run2.Controls.Add(this.lb_uld1_i_time);
            this.panel_run2.Controls.Add(this.lb_uld1_i);
            this.panel_run2.Controls.Add(this.lb_uld1_o_time);
            this.panel_run2.Controls.Add(this.lb_uld1_o);
            this.panel_run2.Controls.Add(this.lb_uld2_i_time);
            this.panel_run2.Controls.Add(this.lb_uld2_i);
            this.panel_run2.Controls.Add(this.lb_uld2_o_time);
            this.panel_run2.Controls.Add(this.lb_uld2_o);
            this.panel_run2.Controls.Add(this.panel_shutter_open);
            this.panel_run2.Controls.Add(this.panel_lock);
            this.panel_run2.Controls.Add(this.panel_laser_cover);
            this.panel_run2.Controls.Add(this.panel_ems2);
            this.panel_run2.Controls.Add(this.panel_ems6);
            this.panel_run2.Controls.Add(this.panel_grab_ems1);
            this.panel_run2.Controls.Add(this.panel_grab_ems2);
            this.panel_run2.Controls.Add(this.panel_grab_switch1);
            this.panel_run2.Controls.Add(this.panel_grab_switch2);
            this.panel_run2.Controls.Add(this.panel_grab_ems3);
            this.panel_run2.Controls.Add(this.panel_grab_switch3);
            this.panel_run2.Location = new System.Drawing.Point(46, 41);
            this.panel_run2.Name = "panel_run2";
            this.panel_run2.Size = new System.Drawing.Size(793, 300);
            this.panel_run2.TabIndex = 22;
            // 
            // panel_ld_light
            // 
            this.panel_ld_light.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_ld_light.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_ld_light.Controls.Add(this.lb_ld_light);
            this.panel_ld_light.ForeColor = System.Drawing.Color.White;
            this.panel_ld_light.Location = new System.Drawing.Point(529, 219);
            this.panel_ld_light.Name = "panel_ld_light";
            this.panel_ld_light.Size = new System.Drawing.Size(125, 36);
            this.panel_ld_light.TabIndex = 17;
            // 
            // lb_ld_light
            // 
            this.lb_ld_light.AutoSize = true;
            this.lb_ld_light.BackColor = System.Drawing.Color.Transparent;
            this.lb_ld_light.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld_light.ForeColor = System.Drawing.Color.White;
            this.lb_ld_light.Location = new System.Drawing.Point(16, 9);
            this.lb_ld_light.Name = "lb_ld_light";
            this.lb_ld_light.Size = new System.Drawing.Size(93, 15);
            this.lb_ld_light.TabIndex = 4;
            this.lb_ld_light.Text = "로더부 조명";
            // 
            // panel_process_light
            // 
            this.panel_process_light.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_process_light.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_process_light.Controls.Add(this.lb_process_light);
            this.panel_process_light.ForeColor = System.Drawing.Color.White;
            this.panel_process_light.Location = new System.Drawing.Point(337, 219);
            this.panel_process_light.Name = "panel_process_light";
            this.panel_process_light.Size = new System.Drawing.Size(125, 36);
            this.panel_process_light.TabIndex = 17;
            // 
            // lb_process_light
            // 
            this.lb_process_light.AutoSize = true;
            this.lb_process_light.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_light.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_light.ForeColor = System.Drawing.Color.White;
            this.lb_process_light.Location = new System.Drawing.Point(17, 9);
            this.lb_process_light.Name = "lb_process_light";
            this.lb_process_light.Size = new System.Drawing.Size(93, 15);
            this.lb_process_light.TabIndex = 4;
            this.lb_process_light.Text = "가공부 조명";
            // 
            // panel_uld_light
            // 
            this.panel_uld_light.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_uld_light.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_uld_light.Controls.Add(this.lb_uld_light);
            this.panel_uld_light.ForeColor = System.Drawing.Color.White;
            this.panel_uld_light.Location = new System.Drawing.Point(134, 219);
            this.panel_uld_light.Name = "panel_uld_light";
            this.panel_uld_light.Size = new System.Drawing.Size(125, 36);
            this.panel_uld_light.TabIndex = 16;
            // 
            // lb_uld_light
            // 
            this.lb_uld_light.AutoSize = true;
            this.lb_uld_light.BackColor = System.Drawing.Color.Transparent;
            this.lb_uld_light.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld_light.ForeColor = System.Drawing.Color.White;
            this.lb_uld_light.Location = new System.Drawing.Point(8, 9);
            this.lb_uld_light.Name = "lb_uld_light";
            this.lb_uld_light.Size = new System.Drawing.Size(109, 15);
            this.lb_uld_light.TabIndex = 4;
            this.lb_uld_light.Text = "언로더부 조명";
            // 
            // panel_ems1
            // 
            this.panel_ems1.BackColor = System.Drawing.Color.DimGray;
            this.panel_ems1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_ems1.Controls.Add(this.lb_ems1);
            this.panel_ems1.ForeColor = System.Drawing.Color.White;
            this.panel_ems1.Location = new System.Drawing.Point(528, 260);
            this.panel_ems1.Name = "panel_ems1";
            this.panel_ems1.Size = new System.Drawing.Size(55, 35);
            this.panel_ems1.TabIndex = 18;
            // 
            // lb_ems1
            // 
            this.lb_ems1.AutoSize = true;
            this.lb_ems1.BackColor = System.Drawing.Color.Transparent;
            this.lb_ems1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ems1.ForeColor = System.Drawing.Color.White;
            this.lb_ems1.Location = new System.Drawing.Point(3, 10);
            this.lb_ems1.Name = "lb_ems1";
            this.lb_ems1.Size = new System.Drawing.Size(47, 12);
            this.lb_ems1.TabIndex = 4;
            this.lb_ems1.Text = "EMS 1";
            // 
            // panel_target_avgpower
            // 
            this.panel_target_avgpower.BackColor = System.Drawing.Color.DimGray;
            this.panel_target_avgpower.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_target_avgpower.Controls.Add(this.lb_avgpower);
            this.panel_target_avgpower.Controls.Add(this.lb_avgpower_info);
            this.panel_target_avgpower.Controls.Add(this.lb_tartget);
            this.panel_target_avgpower.Controls.Add(this.lb_target_info);
            this.panel_target_avgpower.ForeColor = System.Drawing.Color.White;
            this.panel_target_avgpower.Location = new System.Drawing.Point(397, 260);
            this.panel_target_avgpower.Name = "panel_target_avgpower";
            this.panel_target_avgpower.Size = new System.Drawing.Size(125, 36);
            this.panel_target_avgpower.TabIndex = 16;
            // 
            // lb_avgpower
            // 
            this.lb_avgpower.AutoSize = true;
            this.lb_avgpower.BackColor = System.Drawing.Color.Transparent;
            this.lb_avgpower.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_avgpower.ForeColor = System.Drawing.Color.White;
            this.lb_avgpower.Location = new System.Drawing.Point(99, 19);
            this.lb_avgpower.Name = "lb_avgpower";
            this.lb_avgpower.Size = new System.Drawing.Size(12, 12);
            this.lb_avgpower.TabIndex = 7;
            this.lb_avgpower.Text = "0";
            // 
            // lb_avgpower_info
            // 
            this.lb_avgpower_info.AutoSize = true;
            this.lb_avgpower_info.BackColor = System.Drawing.Color.Transparent;
            this.lb_avgpower_info.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_avgpower_info.ForeColor = System.Drawing.Color.White;
            this.lb_avgpower_info.Location = new System.Drawing.Point(0, 19);
            this.lb_avgpower_info.Name = "lb_avgpower_info";
            this.lb_avgpower_info.Size = new System.Drawing.Size(100, 12);
            this.lb_avgpower_info.TabIndex = 6;
            this.lb_avgpower_info.Text = "AvgPower[w]:";
            // 
            // lb_tartget
            // 
            this.lb_tartget.AutoSize = true;
            this.lb_tartget.BackColor = System.Drawing.Color.Transparent;
            this.lb_tartget.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_tartget.ForeColor = System.Drawing.Color.White;
            this.lb_tartget.Location = new System.Drawing.Point(74, 4);
            this.lb_tartget.Name = "lb_tartget";
            this.lb_tartget.Size = new System.Drawing.Size(12, 12);
            this.lb_tartget.TabIndex = 5;
            this.lb_tartget.Text = "0";
            // 
            // lb_target_info
            // 
            this.lb_target_info.AutoSize = true;
            this.lb_target_info.BackColor = System.Drawing.Color.Transparent;
            this.lb_target_info.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_target_info.ForeColor = System.Drawing.Color.White;
            this.lb_target_info.Location = new System.Drawing.Point(-1, 4);
            this.lb_target_info.Name = "lb_target_info";
            this.lb_target_info.Size = new System.Drawing.Size(77, 12);
            this.lb_target_info.TabIndex = 4;
            this.lb_target_info.Text = "Target[w]:";
            // 
            // panel_zpos_gap
            // 
            this.panel_zpos_gap.BackColor = System.Drawing.Color.DimGray;
            this.panel_zpos_gap.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_zpos_gap.Controls.Add(this.lb_zpos_gap);
            this.panel_zpos_gap.Controls.Add(this.lb_zpos_gap_info);
            this.panel_zpos_gap.ForeColor = System.Drawing.Color.White;
            this.panel_zpos_gap.Location = new System.Drawing.Point(266, 260);
            this.panel_zpos_gap.Name = "panel_zpos_gap";
            this.panel_zpos_gap.Size = new System.Drawing.Size(125, 36);
            this.panel_zpos_gap.TabIndex = 15;
            // 
            // lb_zpos_gap
            // 
            this.lb_zpos_gap.AutoSize = true;
            this.lb_zpos_gap.BackColor = System.Drawing.Color.Transparent;
            this.lb_zpos_gap.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_zpos_gap.ForeColor = System.Drawing.Color.White;
            this.lb_zpos_gap.Location = new System.Drawing.Point(80, 10);
            this.lb_zpos_gap.Name = "lb_zpos_gap";
            this.lb_zpos_gap.Size = new System.Drawing.Size(12, 12);
            this.lb_zpos_gap.TabIndex = 5;
            this.lb_zpos_gap.Text = "0";
            // 
            // lb_zpos_gap_info
            // 
            this.lb_zpos_gap_info.AutoSize = true;
            this.lb_zpos_gap_info.BackColor = System.Drawing.Color.Transparent;
            this.lb_zpos_gap_info.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_zpos_gap_info.ForeColor = System.Drawing.Color.White;
            this.lb_zpos_gap_info.Location = new System.Drawing.Point(-1, 10);
            this.lb_zpos_gap_info.Name = "lb_zpos_gap_info";
            this.lb_zpos_gap_info.Size = new System.Drawing.Size(80, 12);
            this.lb_zpos_gap_info.TabIndex = 4;
            this.lb_zpos_gap_info.Text = "ZPos Gap :";
            // 
            // panel_ems4
            // 
            this.panel_ems4.BackColor = System.Drawing.Color.DimGray;
            this.panel_ems4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_ems4.Controls.Add(this.lb_ems4);
            this.panel_ems4.ForeColor = System.Drawing.Color.White;
            this.panel_ems4.Location = new System.Drawing.Point(205, 260);
            this.panel_ems4.Name = "panel_ems4";
            this.panel_ems4.Size = new System.Drawing.Size(55, 35);
            this.panel_ems4.TabIndex = 17;
            // 
            // lb_ems4
            // 
            this.lb_ems4.AutoSize = true;
            this.lb_ems4.BackColor = System.Drawing.Color.Transparent;
            this.lb_ems4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ems4.ForeColor = System.Drawing.Color.White;
            this.lb_ems4.Location = new System.Drawing.Point(3, 10);
            this.lb_ems4.Name = "lb_ems4";
            this.lb_ems4.Size = new System.Drawing.Size(47, 12);
            this.lb_ems4.TabIndex = 4;
            this.lb_ems4.Text = "EMS 4";
            // 
            // panel_ld_out_b
            // 
            this.panel_ld_out_b.BackColor = System.Drawing.Color.DimGray;
            this.panel_ld_out_b.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_ld_out_b.Controls.Add(this.lb_ld_out_b);
            this.panel_ld_out_b.ForeColor = System.Drawing.Color.White;
            this.panel_ld_out_b.Location = new System.Drawing.Point(753, 260);
            this.panel_ld_out_b.Name = "panel_ld_out_b";
            this.panel_ld_out_b.Size = new System.Drawing.Size(35, 35);
            this.panel_ld_out_b.TabIndex = 56;
            // 
            // lb_ld_out_b
            // 
            this.lb_ld_out_b.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld_out_b.ForeColor = System.Drawing.Color.White;
            this.lb_ld_out_b.Location = new System.Drawing.Point(2, 1);
            this.lb_ld_out_b.Name = "lb_ld_out_b";
            this.lb_ld_out_b.Size = new System.Drawing.Size(32, 33);
            this.lb_ld_out_b.TabIndex = 1;
            this.lb_ld_out_b.Text = "배출B";
            this.lb_ld_out_b.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_ld_in_b
            // 
            this.panel_ld_in_b.BackColor = System.Drawing.Color.DimGray;
            this.panel_ld_in_b.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_ld_in_b.Controls.Add(this.lb_ld_in_b);
            this.panel_ld_in_b.ForeColor = System.Drawing.Color.White;
            this.panel_ld_in_b.Location = new System.Drawing.Point(753, 222);
            this.panel_ld_in_b.Name = "panel_ld_in_b";
            this.panel_ld_in_b.Size = new System.Drawing.Size(35, 35);
            this.panel_ld_in_b.TabIndex = 54;
            // 
            // lb_ld_in_b
            // 
            this.lb_ld_in_b.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld_in_b.ForeColor = System.Drawing.Color.White;
            this.lb_ld_in_b.Location = new System.Drawing.Point(2, 1);
            this.lb_ld_in_b.Name = "lb_ld_in_b";
            this.lb_ld_in_b.Size = new System.Drawing.Size(32, 33);
            this.lb_ld_in_b.TabIndex = 1;
            this.lb_ld_in_b.Text = "투입B";
            this.lb_ld_in_b.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_ld_out_a
            // 
            this.panel_ld_out_a.BackColor = System.Drawing.Color.DimGray;
            this.panel_ld_out_a.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_ld_out_a.Controls.Add(this.lb_ld_out_a);
            this.panel_ld_out_a.ForeColor = System.Drawing.Color.White;
            this.panel_ld_out_a.Location = new System.Drawing.Point(715, 260);
            this.panel_ld_out_a.Name = "panel_ld_out_a";
            this.panel_ld_out_a.Size = new System.Drawing.Size(35, 35);
            this.panel_ld_out_a.TabIndex = 55;
            // 
            // lb_ld_out_a
            // 
            this.lb_ld_out_a.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld_out_a.ForeColor = System.Drawing.Color.White;
            this.lb_ld_out_a.Location = new System.Drawing.Point(2, 1);
            this.lb_ld_out_a.Name = "lb_ld_out_a";
            this.lb_ld_out_a.Size = new System.Drawing.Size(32, 33);
            this.lb_ld_out_a.TabIndex = 1;
            this.lb_ld_out_a.Text = "배출A";
            this.lb_ld_out_a.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_ld_in_a
            // 
            this.panel_ld_in_a.BackColor = System.Drawing.Color.DimGray;
            this.panel_ld_in_a.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_ld_in_a.Controls.Add(this.lb_ld_in_a);
            this.panel_ld_in_a.ForeColor = System.Drawing.Color.White;
            this.panel_ld_in_a.Location = new System.Drawing.Point(715, 222);
            this.panel_ld_in_a.Name = "panel_ld_in_a";
            this.panel_ld_in_a.Size = new System.Drawing.Size(35, 35);
            this.panel_ld_in_a.TabIndex = 53;
            // 
            // lb_ld_in_a
            // 
            this.lb_ld_in_a.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld_in_a.ForeColor = System.Drawing.Color.White;
            this.lb_ld_in_a.Location = new System.Drawing.Point(2, 1);
            this.lb_ld_in_a.Name = "lb_ld_in_a";
            this.lb_ld_in_a.Size = new System.Drawing.Size(32, 33);
            this.lb_ld_in_a.TabIndex = 1;
            this.lb_ld_in_a.Text = "투입A";
            this.lb_ld_in_a.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_uld_in_b
            // 
            this.panel_uld_in_b.BackColor = System.Drawing.Color.DimGray;
            this.panel_uld_in_b.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_uld_in_b.Controls.Add(this.lb_uld_in_b);
            this.panel_uld_in_b.ForeColor = System.Drawing.Color.White;
            this.panel_uld_in_b.Location = new System.Drawing.Point(41, 260);
            this.panel_uld_in_b.Name = "panel_uld_in_b";
            this.panel_uld_in_b.Size = new System.Drawing.Size(35, 35);
            this.panel_uld_in_b.TabIndex = 19;
            // 
            // lb_uld_in_b
            // 
            this.lb_uld_in_b.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld_in_b.ForeColor = System.Drawing.Color.White;
            this.lb_uld_in_b.Location = new System.Drawing.Point(2, 1);
            this.lb_uld_in_b.Name = "lb_uld_in_b";
            this.lb_uld_in_b.Size = new System.Drawing.Size(32, 33);
            this.lb_uld_in_b.TabIndex = 1;
            this.lb_uld_in_b.Text = "투입B";
            this.lb_uld_in_b.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_uld_out_b
            // 
            this.panel_uld_out_b.BackColor = System.Drawing.Color.DimGray;
            this.panel_uld_out_b.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_uld_out_b.Controls.Add(this.lb_uld_out_b);
            this.panel_uld_out_b.ForeColor = System.Drawing.Color.White;
            this.panel_uld_out_b.Location = new System.Drawing.Point(41, 222);
            this.panel_uld_out_b.Name = "panel_uld_out_b";
            this.panel_uld_out_b.Size = new System.Drawing.Size(35, 35);
            this.panel_uld_out_b.TabIndex = 17;
            // 
            // lb_uld_out_b
            // 
            this.lb_uld_out_b.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld_out_b.ForeColor = System.Drawing.Color.White;
            this.lb_uld_out_b.Location = new System.Drawing.Point(2, 1);
            this.lb_uld_out_b.Name = "lb_uld_out_b";
            this.lb_uld_out_b.Size = new System.Drawing.Size(32, 33);
            this.lb_uld_out_b.TabIndex = 1;
            this.lb_uld_out_b.Text = "배출B";
            this.lb_uld_out_b.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_uld_in_a
            // 
            this.panel_uld_in_a.BackColor = System.Drawing.Color.DimGray;
            this.panel_uld_in_a.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_uld_in_a.Controls.Add(this.lb_uld_in_a);
            this.panel_uld_in_a.ForeColor = System.Drawing.Color.White;
            this.panel_uld_in_a.Location = new System.Drawing.Point(3, 260);
            this.panel_uld_in_a.Name = "panel_uld_in_a";
            this.panel_uld_in_a.Size = new System.Drawing.Size(35, 35);
            this.panel_uld_in_a.TabIndex = 18;
            // 
            // lb_uld_in_a
            // 
            this.lb_uld_in_a.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld_in_a.ForeColor = System.Drawing.Color.White;
            this.lb_uld_in_a.Location = new System.Drawing.Point(2, 1);
            this.lb_uld_in_a.Name = "lb_uld_in_a";
            this.lb_uld_in_a.Size = new System.Drawing.Size(32, 33);
            this.lb_uld_in_a.TabIndex = 1;
            this.lb_uld_in_a.Text = "투입A";
            this.lb_uld_in_a.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_uld_out_a
            // 
            this.panel_uld_out_a.BackColor = System.Drawing.Color.DimGray;
            this.panel_uld_out_a.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_uld_out_a.Controls.Add(this.lb_uld_out_a);
            this.panel_uld_out_a.ForeColor = System.Drawing.Color.White;
            this.panel_uld_out_a.Location = new System.Drawing.Point(3, 222);
            this.panel_uld_out_a.Name = "panel_uld_out_a";
            this.panel_uld_out_a.Size = new System.Drawing.Size(35, 35);
            this.panel_uld_out_a.TabIndex = 16;
            // 
            // lb_uld_out_a
            // 
            this.lb_uld_out_a.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld_out_a.ForeColor = System.Drawing.Color.White;
            this.lb_uld_out_a.Location = new System.Drawing.Point(2, 1);
            this.lb_uld_out_a.Name = "lb_uld_out_a";
            this.lb_uld_out_a.Size = new System.Drawing.Size(32, 33);
            this.lb_uld_out_a.TabIndex = 1;
            this.lb_uld_out_a.Text = "배출A";
            this.lb_uld_out_a.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_uld1_out2
            // 
            this.panel_uld1_out2.BackColor = System.Drawing.Color.DimGray;
            this.panel_uld1_out2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_uld1_out2.ForeColor = System.Drawing.Color.White;
            this.panel_uld1_out2.Location = new System.Drawing.Point(96, 174);
            this.panel_uld1_out2.Name = "panel_uld1_out2";
            this.panel_uld1_out2.Size = new System.Drawing.Size(20, 20);
            this.panel_uld1_out2.TabIndex = 48;
            // 
            // panel_uld2_out2
            // 
            this.panel_uld2_out2.BackColor = System.Drawing.Color.DimGray;
            this.panel_uld2_out2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_uld2_out2.ForeColor = System.Drawing.Color.White;
            this.panel_uld2_out2.Location = new System.Drawing.Point(96, 98);
            this.panel_uld2_out2.Name = "panel_uld2_out2";
            this.panel_uld2_out2.Size = new System.Drawing.Size(20, 20);
            this.panel_uld2_out2.TabIndex = 23;
            // 
            // panel_uld1_table
            // 
            this.panel_uld1_table.BackColor = System.Drawing.Color.DimGray;
            this.panel_uld1_table.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_uld1_table.ForeColor = System.Drawing.Color.White;
            this.panel_uld1_table.Location = new System.Drawing.Point(121, 148);
            this.panel_uld1_table.Name = "panel_uld1_table";
            this.panel_uld1_table.Size = new System.Drawing.Size(15, 47);
            this.panel_uld1_table.TabIndex = 44;
            // 
            // panel_uld2_table
            // 
            this.panel_uld2_table.BackColor = System.Drawing.Color.DimGray;
            this.panel_uld2_table.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_uld2_table.ForeColor = System.Drawing.Color.White;
            this.panel_uld2_table.Location = new System.Drawing.Point(121, 72);
            this.panel_uld2_table.Name = "panel_uld2_table";
            this.panel_uld2_table.Size = new System.Drawing.Size(15, 47);
            this.panel_uld2_table.TabIndex = 22;
            // 
            // panel_uld1_out1
            // 
            this.panel_uld1_out1.BackColor = System.Drawing.Color.DimGray;
            this.panel_uld1_out1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_uld1_out1.ForeColor = System.Drawing.Color.White;
            this.panel_uld1_out1.Location = new System.Drawing.Point(96, 148);
            this.panel_uld1_out1.Name = "panel_uld1_out1";
            this.panel_uld1_out1.Size = new System.Drawing.Size(20, 20);
            this.panel_uld1_out1.TabIndex = 45;
            // 
            // panel_uld2_out1
            // 
            this.panel_uld2_out1.BackColor = System.Drawing.Color.DimGray;
            this.panel_uld2_out1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_uld2_out1.ForeColor = System.Drawing.Color.White;
            this.panel_uld2_out1.Location = new System.Drawing.Point(96, 72);
            this.panel_uld2_out1.Name = "panel_uld2_out1";
            this.panel_uld2_out1.Size = new System.Drawing.Size(20, 20);
            this.panel_uld2_out1.TabIndex = 22;
            // 
            // panel_uld1
            // 
            this.panel_uld1.BackColor = System.Drawing.Color.DimGray;
            this.panel_uld1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_uld1.Controls.Add(this.lb_uld1);
            this.panel_uld1.ForeColor = System.Drawing.Color.White;
            this.panel_uld1.Location = new System.Drawing.Point(141, 154);
            this.panel_uld1.Name = "panel_uld1";
            this.panel_uld1.Size = new System.Drawing.Size(67, 35);
            this.panel_uld1.TabIndex = 46;
            // 
            // lb_uld1
            // 
            this.lb_uld1.AutoSize = true;
            this.lb_uld1.BackColor = System.Drawing.Color.Transparent;
            this.lb_uld1.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld1.ForeColor = System.Drawing.Color.White;
            this.lb_uld1.Location = new System.Drawing.Point(6, 11);
            this.lb_uld1.Name = "lb_uld1";
            this.lb_uld1.Size = new System.Drawing.Size(53, 11);
            this.lb_uld1.TabIndex = 5;
            this.lb_uld1.Text = "언로더 1";
            // 
            // panel_uld2
            // 
            this.panel_uld2.BackColor = System.Drawing.Color.DimGray;
            this.panel_uld2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_uld2.Controls.Add(this.lb_uld2);
            this.panel_uld2.ForeColor = System.Drawing.Color.White;
            this.panel_uld2.Location = new System.Drawing.Point(141, 78);
            this.panel_uld2.Name = "panel_uld2";
            this.panel_uld2.Size = new System.Drawing.Size(67, 35);
            this.panel_uld2.TabIndex = 22;
            // 
            // lb_uld2
            // 
            this.lb_uld2.AutoSize = true;
            this.lb_uld2.BackColor = System.Drawing.Color.Transparent;
            this.lb_uld2.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld2.ForeColor = System.Drawing.Color.White;
            this.lb_uld2.Location = new System.Drawing.Point(6, 11);
            this.lb_uld2.Name = "lb_uld2";
            this.lb_uld2.Size = new System.Drawing.Size(53, 11);
            this.lb_uld2.TabIndex = 5;
            this.lb_uld2.Text = "언로더 2";
            // 
            // panel_uld_trans_a1
            // 
            this.panel_uld_trans_a1.BackColor = System.Drawing.Color.DimGray;
            this.panel_uld_trans_a1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_uld_trans_a1.Controls.Add(this.lb_uld_trans_a1);
            this.panel_uld_trans_a1.ForeColor = System.Drawing.Color.White;
            this.panel_uld_trans_a1.Location = new System.Drawing.Point(214, 178);
            this.panel_uld_trans_a1.Name = "panel_uld_trans_a1";
            this.panel_uld_trans_a1.Size = new System.Drawing.Size(67, 35);
            this.panel_uld_trans_a1.TabIndex = 52;
            // 
            // lb_uld_trans_a1
            // 
            this.lb_uld_trans_a1.BackColor = System.Drawing.Color.Transparent;
            this.lb_uld_trans_a1.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld_trans_a1.ForeColor = System.Drawing.Color.White;
            this.lb_uld_trans_a1.Location = new System.Drawing.Point(-5, -1);
            this.lb_uld_trans_a1.Name = "lb_uld_trans_a1";
            this.lb_uld_trans_a1.Size = new System.Drawing.Size(76, 36);
            this.lb_uld_trans_a1.TabIndex = 4;
            this.lb_uld_trans_a1.Text = "  언로더   트랜스퍼A1";
            this.lb_uld_trans_a1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_uld_trans_b1
            // 
            this.panel_uld_trans_b1.BackColor = System.Drawing.Color.DimGray;
            this.panel_uld_trans_b1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_uld_trans_b1.Controls.Add(this.lb_uld_trans_b1);
            this.panel_uld_trans_b1.ForeColor = System.Drawing.Color.White;
            this.panel_uld_trans_b1.Location = new System.Drawing.Point(214, 102);
            this.panel_uld_trans_b1.Name = "panel_uld_trans_b1";
            this.panel_uld_trans_b1.Size = new System.Drawing.Size(67, 35);
            this.panel_uld_trans_b1.TabIndex = 36;
            // 
            // lb_uld_trans_b1
            // 
            this.lb_uld_trans_b1.BackColor = System.Drawing.Color.Transparent;
            this.lb_uld_trans_b1.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld_trans_b1.ForeColor = System.Drawing.Color.White;
            this.lb_uld_trans_b1.Location = new System.Drawing.Point(-5, -1);
            this.lb_uld_trans_b1.Name = "lb_uld_trans_b1";
            this.lb_uld_trans_b1.Size = new System.Drawing.Size(76, 36);
            this.lb_uld_trans_b1.TabIndex = 4;
            this.lb_uld_trans_b1.Text = "  언로더   트랜스퍼B1";
            this.lb_uld_trans_b1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_uld_trans_a2
            // 
            this.panel_uld_trans_a2.BackColor = System.Drawing.Color.DimGray;
            this.panel_uld_trans_a2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_uld_trans_a2.Controls.Add(this.lb_uld_trans_a2);
            this.panel_uld_trans_a2.ForeColor = System.Drawing.Color.White;
            this.panel_uld_trans_a2.Location = new System.Drawing.Point(214, 140);
            this.panel_uld_trans_a2.Name = "panel_uld_trans_a2";
            this.panel_uld_trans_a2.Size = new System.Drawing.Size(67, 35);
            this.panel_uld_trans_a2.TabIndex = 51;
            // 
            // lb_uld_trans_a2
            // 
            this.lb_uld_trans_a2.BackColor = System.Drawing.Color.Transparent;
            this.lb_uld_trans_a2.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld_trans_a2.ForeColor = System.Drawing.Color.White;
            this.lb_uld_trans_a2.Location = new System.Drawing.Point(-5, -1);
            this.lb_uld_trans_a2.Name = "lb_uld_trans_a2";
            this.lb_uld_trans_a2.Size = new System.Drawing.Size(76, 36);
            this.lb_uld_trans_a2.TabIndex = 4;
            this.lb_uld_trans_a2.Text = "  언로더   트랜스퍼A2";
            this.lb_uld_trans_a2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_uld_trans_b2
            // 
            this.panel_uld_trans_b2.BackColor = System.Drawing.Color.DimGray;
            this.panel_uld_trans_b2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_uld_trans_b2.Controls.Add(this.lb_uld_trans_b2);
            this.panel_uld_trans_b2.ForeColor = System.Drawing.Color.White;
            this.panel_uld_trans_b2.Location = new System.Drawing.Point(214, 64);
            this.panel_uld_trans_b2.Name = "panel_uld_trans_b2";
            this.panel_uld_trans_b2.Size = new System.Drawing.Size(67, 35);
            this.panel_uld_trans_b2.TabIndex = 35;
            // 
            // lb_uld_trans_b2
            // 
            this.lb_uld_trans_b2.BackColor = System.Drawing.Color.Transparent;
            this.lb_uld_trans_b2.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld_trans_b2.ForeColor = System.Drawing.Color.White;
            this.lb_uld_trans_b2.Location = new System.Drawing.Point(-5, -1);
            this.lb_uld_trans_b2.Name = "lb_uld_trans_b2";
            this.lb_uld_trans_b2.Size = new System.Drawing.Size(76, 36);
            this.lb_uld_trans_b2.TabIndex = 4;
            this.lb_uld_trans_b2.Text = "  언로더   트랜스퍼B2";
            this.lb_uld_trans_b2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_break_table_a1
            // 
            this.panel_break_table_a1.BackColor = System.Drawing.Color.DimGray;
            this.panel_break_table_a1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_break_table_a1.Controls.Add(this.lb_break_table_a1);
            this.panel_break_table_a1.ForeColor = System.Drawing.Color.White;
            this.panel_break_table_a1.Location = new System.Drawing.Point(287, 178);
            this.panel_break_table_a1.Name = "panel_break_table_a1";
            this.panel_break_table_a1.Size = new System.Drawing.Size(67, 35);
            this.panel_break_table_a1.TabIndex = 50;
            // 
            // lb_break_table_a1
            // 
            this.lb_break_table_a1.BackColor = System.Drawing.Color.Transparent;
            this.lb_break_table_a1.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_break_table_a1.ForeColor = System.Drawing.Color.White;
            this.lb_break_table_a1.Location = new System.Drawing.Point(-5, -1);
            this.lb_break_table_a1.Name = "lb_break_table_a1";
            this.lb_break_table_a1.Size = new System.Drawing.Size(76, 36);
            this.lb_break_table_a1.TabIndex = 4;
            this.lb_break_table_a1.Text = " 브레이크  테이블 A1";
            this.lb_break_table_a1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_break_table_b1
            // 
            this.panel_break_table_b1.BackColor = System.Drawing.Color.DimGray;
            this.panel_break_table_b1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_break_table_b1.Controls.Add(this.lb_break_table_b1);
            this.panel_break_table_b1.ForeColor = System.Drawing.Color.White;
            this.panel_break_table_b1.Location = new System.Drawing.Point(287, 102);
            this.panel_break_table_b1.Name = "panel_break_table_b1";
            this.panel_break_table_b1.Size = new System.Drawing.Size(67, 35);
            this.panel_break_table_b1.TabIndex = 24;
            // 
            // lb_break_table_b1
            // 
            this.lb_break_table_b1.BackColor = System.Drawing.Color.Transparent;
            this.lb_break_table_b1.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_break_table_b1.ForeColor = System.Drawing.Color.White;
            this.lb_break_table_b1.Location = new System.Drawing.Point(-5, -1);
            this.lb_break_table_b1.Name = "lb_break_table_b1";
            this.lb_break_table_b1.Size = new System.Drawing.Size(76, 36);
            this.lb_break_table_b1.TabIndex = 4;
            this.lb_break_table_b1.Text = " 브레이크  테이블 B1";
            this.lb_break_table_b1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_break_trans1
            // 
            this.panel_break_trans1.BackColor = System.Drawing.Color.DimGray;
            this.panel_break_trans1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_break_trans1.Controls.Add(this.lb_break_trans1);
            this.panel_break_trans1.ForeColor = System.Drawing.Color.White;
            this.panel_break_trans1.Location = new System.Drawing.Point(359, 154);
            this.panel_break_trans1.Name = "panel_break_trans1";
            this.panel_break_trans1.Size = new System.Drawing.Size(67, 35);
            this.panel_break_trans1.TabIndex = 40;
            // 
            // lb_break_trans1
            // 
            this.lb_break_trans1.BackColor = System.Drawing.Color.Transparent;
            this.lb_break_trans1.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_break_trans1.ForeColor = System.Drawing.Color.White;
            this.lb_break_trans1.Location = new System.Drawing.Point(-6, -1);
            this.lb_break_trans1.Name = "lb_break_trans1";
            this.lb_break_trans1.Size = new System.Drawing.Size(76, 36);
            this.lb_break_trans1.TabIndex = 4;
            this.lb_break_trans1.Text = " 브레이크  트랜스퍼 1";
            this.lb_break_trans1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_break_trans2
            // 
            this.panel_break_trans2.BackColor = System.Drawing.Color.DimGray;
            this.panel_break_trans2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_break_trans2.Controls.Add(this.lb_break_trans2);
            this.panel_break_trans2.ForeColor = System.Drawing.Color.White;
            this.panel_break_trans2.Location = new System.Drawing.Point(359, 78);
            this.panel_break_trans2.Name = "panel_break_trans2";
            this.panel_break_trans2.Size = new System.Drawing.Size(67, 35);
            this.panel_break_trans2.TabIndex = 21;
            // 
            // lb_break_trans2
            // 
            this.lb_break_trans2.BackColor = System.Drawing.Color.Transparent;
            this.lb_break_trans2.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_break_trans2.ForeColor = System.Drawing.Color.White;
            this.lb_break_trans2.Location = new System.Drawing.Point(-6, -1);
            this.lb_break_trans2.Name = "lb_break_trans2";
            this.lb_break_trans2.Size = new System.Drawing.Size(76, 36);
            this.lb_break_trans2.TabIndex = 4;
            this.lb_break_trans2.Text = " 브레이크  트랜스퍼 2";
            this.lb_break_trans2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_break_table_a2
            // 
            this.panel_break_table_a2.BackColor = System.Drawing.Color.DimGray;
            this.panel_break_table_a2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_break_table_a2.Controls.Add(this.lb_break_table_a2);
            this.panel_break_table_a2.ForeColor = System.Drawing.Color.White;
            this.panel_break_table_a2.Location = new System.Drawing.Point(287, 140);
            this.panel_break_table_a2.Name = "panel_break_table_a2";
            this.panel_break_table_a2.Size = new System.Drawing.Size(67, 35);
            this.panel_break_table_a2.TabIndex = 49;
            // 
            // lb_break_table_a2
            // 
            this.lb_break_table_a2.BackColor = System.Drawing.Color.Transparent;
            this.lb_break_table_a2.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_break_table_a2.ForeColor = System.Drawing.Color.White;
            this.lb_break_table_a2.Location = new System.Drawing.Point(-5, -1);
            this.lb_break_table_a2.Name = "lb_break_table_a2";
            this.lb_break_table_a2.Size = new System.Drawing.Size(76, 36);
            this.lb_break_table_a2.TabIndex = 4;
            this.lb_break_table_a2.Text = " 브레이크  테이블 A2";
            this.lb_break_table_a2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_break_table_b2
            // 
            this.panel_break_table_b2.BackColor = System.Drawing.Color.DimGray;
            this.panel_break_table_b2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_break_table_b2.Controls.Add(this.lb_break_table_b2);
            this.panel_break_table_b2.ForeColor = System.Drawing.Color.White;
            this.panel_break_table_b2.Location = new System.Drawing.Point(287, 64);
            this.panel_break_table_b2.Name = "panel_break_table_b2";
            this.panel_break_table_b2.Size = new System.Drawing.Size(67, 35);
            this.panel_break_table_b2.TabIndex = 23;
            // 
            // lb_break_table_b2
            // 
            this.lb_break_table_b2.BackColor = System.Drawing.Color.Transparent;
            this.lb_break_table_b2.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_break_table_b2.ForeColor = System.Drawing.Color.White;
            this.lb_break_table_b2.Location = new System.Drawing.Point(-5, -1);
            this.lb_break_table_b2.Name = "lb_break_table_b2";
            this.lb_break_table_b2.Size = new System.Drawing.Size(76, 36);
            this.lb_break_table_b2.TabIndex = 4;
            this.lb_break_table_b2.Text = " 브레이크  테이블 B2";
            this.lb_break_table_b2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_process_table_a1
            // 
            this.panel_process_table_a1.BackColor = System.Drawing.Color.DimGray;
            this.panel_process_table_a1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_process_table_a1.Controls.Add(this.lb_process_table_a1);
            this.panel_process_table_a1.ForeColor = System.Drawing.Color.White;
            this.panel_process_table_a1.Location = new System.Drawing.Point(431, 174);
            this.panel_process_table_a1.Name = "panel_process_table_a1";
            this.panel_process_table_a1.Size = new System.Drawing.Size(67, 35);
            this.panel_process_table_a1.TabIndex = 47;
            // 
            // lb_process_table_a1
            // 
            this.lb_process_table_a1.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_table_a1.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_table_a1.ForeColor = System.Drawing.Color.White;
            this.lb_process_table_a1.Location = new System.Drawing.Point(-5, -1);
            this.lb_process_table_a1.Name = "lb_process_table_a1";
            this.lb_process_table_a1.Size = new System.Drawing.Size(76, 36);
            this.lb_process_table_a1.TabIndex = 4;
            this.lb_process_table_a1.Text = "   가공     테이블 A1";
            this.lb_process_table_a1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_process_table_b1
            // 
            this.panel_process_table_b1.BackColor = System.Drawing.Color.DimGray;
            this.panel_process_table_b1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_process_table_b1.Controls.Add(this.lb_process_table_b1);
            this.panel_process_table_b1.ForeColor = System.Drawing.Color.White;
            this.panel_process_table_b1.Location = new System.Drawing.Point(431, 98);
            this.panel_process_table_b1.Name = "panel_process_table_b1";
            this.panel_process_table_b1.Size = new System.Drawing.Size(67, 35);
            this.panel_process_table_b1.TabIndex = 22;
            // 
            // lb_process_table_b1
            // 
            this.lb_process_table_b1.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_table_b1.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_table_b1.ForeColor = System.Drawing.Color.White;
            this.lb_process_table_b1.Location = new System.Drawing.Point(-5, -1);
            this.lb_process_table_b1.Name = "lb_process_table_b1";
            this.lb_process_table_b1.Size = new System.Drawing.Size(76, 36);
            this.lb_process_table_b1.TabIndex = 4;
            this.lb_process_table_b1.Text = "   가공     테이블 B1";
            this.lb_process_table_b1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_process_table_a2
            // 
            this.panel_process_table_a2.BackColor = System.Drawing.Color.DimGray;
            this.panel_process_table_a2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_process_table_a2.Controls.Add(this.lb_process_table_a2);
            this.panel_process_table_a2.ForeColor = System.Drawing.Color.White;
            this.panel_process_table_a2.Location = new System.Drawing.Point(431, 136);
            this.panel_process_table_a2.Name = "panel_process_table_a2";
            this.panel_process_table_a2.Size = new System.Drawing.Size(67, 35);
            this.panel_process_table_a2.TabIndex = 41;
            // 
            // lb_process_table_a2
            // 
            this.lb_process_table_a2.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_table_a2.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_table_a2.ForeColor = System.Drawing.Color.White;
            this.lb_process_table_a2.Location = new System.Drawing.Point(-5, -1);
            this.lb_process_table_a2.Name = "lb_process_table_a2";
            this.lb_process_table_a2.Size = new System.Drawing.Size(76, 36);
            this.lb_process_table_a2.TabIndex = 4;
            this.lb_process_table_a2.Text = "   가공     테이블 A2";
            this.lb_process_table_a2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_ld_trans1
            // 
            this.panel_ld_trans1.BackColor = System.Drawing.Color.DimGray;
            this.panel_ld_trans1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_ld_trans1.Controls.Add(this.lb_ld_trans1);
            this.panel_ld_trans1.ForeColor = System.Drawing.Color.White;
            this.panel_ld_trans1.Location = new System.Drawing.Point(502, 154);
            this.panel_ld_trans1.Name = "panel_ld_trans1";
            this.panel_ld_trans1.Size = new System.Drawing.Size(67, 35);
            this.panel_ld_trans1.TabIndex = 38;
            // 
            // lb_ld_trans1
            // 
            this.lb_ld_trans1.BackColor = System.Drawing.Color.Transparent;
            this.lb_ld_trans1.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld_trans1.ForeColor = System.Drawing.Color.White;
            this.lb_ld_trans1.Location = new System.Drawing.Point(-5, -1);
            this.lb_ld_trans1.Name = "lb_ld_trans1";
            this.lb_ld_trans1.Size = new System.Drawing.Size(76, 36);
            this.lb_ld_trans1.TabIndex = 4;
            this.lb_ld_trans1.Text = "   로더     트랜스퍼 1";
            this.lb_ld_trans1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_process_table_b2
            // 
            this.panel_process_table_b2.BackColor = System.Drawing.Color.DimGray;
            this.panel_process_table_b2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_process_table_b2.Controls.Add(this.lb_process_table_b2);
            this.panel_process_table_b2.ForeColor = System.Drawing.Color.White;
            this.panel_process_table_b2.Location = new System.Drawing.Point(431, 60);
            this.panel_process_table_b2.Name = "panel_process_table_b2";
            this.panel_process_table_b2.Size = new System.Drawing.Size(67, 35);
            this.panel_process_table_b2.TabIndex = 21;
            // 
            // lb_process_table_b2
            // 
            this.lb_process_table_b2.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_table_b2.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_table_b2.ForeColor = System.Drawing.Color.White;
            this.lb_process_table_b2.Location = new System.Drawing.Point(-5, -1);
            this.lb_process_table_b2.Name = "lb_process_table_b2";
            this.lb_process_table_b2.Size = new System.Drawing.Size(76, 36);
            this.lb_process_table_b2.TabIndex = 4;
            this.lb_process_table_b2.Text = "   가공     테이블 B2";
            this.lb_process_table_b2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_ld1
            // 
            this.panel_ld1.BackColor = System.Drawing.Color.DimGray;
            this.panel_ld1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_ld1.Controls.Add(this.lb_ld1);
            this.panel_ld1.ForeColor = System.Drawing.Color.White;
            this.panel_ld1.Location = new System.Drawing.Point(573, 154);
            this.panel_ld1.Name = "panel_ld1";
            this.panel_ld1.Size = new System.Drawing.Size(67, 35);
            this.panel_ld1.TabIndex = 37;
            // 
            // lb_ld1
            // 
            this.lb_ld1.AutoSize = true;
            this.lb_ld1.BackColor = System.Drawing.Color.Transparent;
            this.lb_ld1.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld1.ForeColor = System.Drawing.Color.White;
            this.lb_ld1.Location = new System.Drawing.Point(11, 11);
            this.lb_ld1.Name = "lb_ld1";
            this.lb_ld1.Size = new System.Drawing.Size(41, 11);
            this.lb_ld1.TabIndex = 4;
            this.lb_ld1.Text = "로더 1";
            // 
            // panel_ld_trans2
            // 
            this.panel_ld_trans2.BackColor = System.Drawing.Color.DimGray;
            this.panel_ld_trans2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_ld_trans2.Controls.Add(this.lb_ld_trans2);
            this.panel_ld_trans2.ForeColor = System.Drawing.Color.White;
            this.panel_ld_trans2.Location = new System.Drawing.Point(502, 78);
            this.panel_ld_trans2.Name = "panel_ld_trans2";
            this.panel_ld_trans2.Size = new System.Drawing.Size(67, 35);
            this.panel_ld_trans2.TabIndex = 20;
            // 
            // lb_ld_trans2
            // 
            this.lb_ld_trans2.BackColor = System.Drawing.Color.Transparent;
            this.lb_ld_trans2.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld_trans2.ForeColor = System.Drawing.Color.White;
            this.lb_ld_trans2.Location = new System.Drawing.Point(-5, -1);
            this.lb_ld_trans2.Name = "lb_ld_trans2";
            this.lb_ld_trans2.Size = new System.Drawing.Size(76, 36);
            this.lb_ld_trans2.TabIndex = 4;
            this.lb_ld_trans2.Text = "   로더     트랜스퍼 2";
            this.lb_ld_trans2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_ld1_table
            // 
            this.panel_ld1_table.BackColor = System.Drawing.Color.DimGray;
            this.panel_ld1_table.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_ld1_table.ForeColor = System.Drawing.Color.White;
            this.panel_ld1_table.Location = new System.Drawing.Point(644, 148);
            this.panel_ld1_table.Name = "panel_ld1_table";
            this.panel_ld1_table.Size = new System.Drawing.Size(15, 47);
            this.panel_ld1_table.TabIndex = 42;
            // 
            // panel_ld2
            // 
            this.panel_ld2.BackColor = System.Drawing.Color.DimGray;
            this.panel_ld2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_ld2.Controls.Add(this.lb_ld2);
            this.panel_ld2.ForeColor = System.Drawing.Color.White;
            this.panel_ld2.Location = new System.Drawing.Point(573, 78);
            this.panel_ld2.Name = "panel_ld2";
            this.panel_ld2.Size = new System.Drawing.Size(67, 35);
            this.panel_ld2.TabIndex = 19;
            // 
            // lb_ld2
            // 
            this.lb_ld2.AutoSize = true;
            this.lb_ld2.BackColor = System.Drawing.Color.Transparent;
            this.lb_ld2.Font = new System.Drawing.Font("굴림", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld2.ForeColor = System.Drawing.Color.White;
            this.lb_ld2.Location = new System.Drawing.Point(11, 11);
            this.lb_ld2.Name = "lb_ld2";
            this.lb_ld2.Size = new System.Drawing.Size(41, 11);
            this.lb_ld2.TabIndex = 4;
            this.lb_ld2.Text = "로더 2";
            // 
            // panel_ld1_in2
            // 
            this.panel_ld1_in2.BackColor = System.Drawing.Color.DimGray;
            this.panel_ld1_in2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_ld1_in2.ForeColor = System.Drawing.Color.White;
            this.panel_ld1_in2.Location = new System.Drawing.Point(663, 174);
            this.panel_ld1_in2.Name = "panel_ld1_in2";
            this.panel_ld1_in2.Size = new System.Drawing.Size(20, 20);
            this.panel_ld1_in2.TabIndex = 43;
            // 
            // panel_ld2_table
            // 
            this.panel_ld2_table.BackColor = System.Drawing.Color.DimGray;
            this.panel_ld2_table.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_ld2_table.ForeColor = System.Drawing.Color.White;
            this.panel_ld2_table.Location = new System.Drawing.Point(644, 72);
            this.panel_ld2_table.Name = "panel_ld2_table";
            this.panel_ld2_table.Size = new System.Drawing.Size(15, 47);
            this.panel_ld2_table.TabIndex = 21;
            // 
            // panel_ld1_in1
            // 
            this.panel_ld1_in1.BackColor = System.Drawing.Color.DimGray;
            this.panel_ld1_in1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_ld1_in1.ForeColor = System.Drawing.Color.White;
            this.panel_ld1_in1.Location = new System.Drawing.Point(663, 148);
            this.panel_ld1_in1.Name = "panel_ld1_in1";
            this.panel_ld1_in1.Size = new System.Drawing.Size(20, 20);
            this.panel_ld1_in1.TabIndex = 39;
            // 
            // panel_ld2_in2
            // 
            this.panel_ld2_in2.BackColor = System.Drawing.Color.DimGray;
            this.panel_ld2_in2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_ld2_in2.ForeColor = System.Drawing.Color.White;
            this.panel_ld2_in2.Location = new System.Drawing.Point(663, 98);
            this.panel_ld2_in2.Name = "panel_ld2_in2";
            this.panel_ld2_in2.Size = new System.Drawing.Size(20, 20);
            this.panel_ld2_in2.TabIndex = 21;
            // 
            // panel_ld2_in1
            // 
            this.panel_ld2_in1.BackColor = System.Drawing.Color.DimGray;
            this.panel_ld2_in1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_ld2_in1.ForeColor = System.Drawing.Color.White;
            this.panel_ld2_in1.Location = new System.Drawing.Point(663, 72);
            this.panel_ld2_in1.Name = "panel_ld2_in1";
            this.panel_ld2_in1.Size = new System.Drawing.Size(20, 20);
            this.panel_ld2_in1.TabIndex = 20;
            // 
            // lb_ld1_o_time
            // 
            this.lb_ld1_o_time.AutoSize = true;
            this.lb_ld1_o_time.BackColor = System.Drawing.Color.Transparent;
            this.lb_ld1_o_time.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld1_o_time.ForeColor = System.Drawing.Color.White;
            this.lb_ld1_o_time.Location = new System.Drawing.Point(716, 179);
            this.lb_ld1_o_time.Name = "lb_ld1_o_time";
            this.lb_ld1_o_time.Size = new System.Drawing.Size(57, 12);
            this.lb_ld1_o_time.TabIndex = 34;
            this.lb_ld1_o_time.Text = "00시 00분";
            // 
            // lb_ld1_o
            // 
            this.lb_ld1_o.AutoSize = true;
            this.lb_ld1_o.BackColor = System.Drawing.Color.Transparent;
            this.lb_ld1_o.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld1_o.ForeColor = System.Drawing.Color.White;
            this.lb_ld1_o.Location = new System.Drawing.Point(689, 179);
            this.lb_ld1_o.Name = "lb_ld1_o";
            this.lb_ld1_o.Size = new System.Drawing.Size(22, 12);
            this.lb_ld1_o.TabIndex = 33;
            this.lb_ld1_o.Text = "O :";
            // 
            // lb_ld1_i_time
            // 
            this.lb_ld1_i_time.AutoSize = true;
            this.lb_ld1_i_time.BackColor = System.Drawing.Color.Transparent;
            this.lb_ld1_i_time.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld1_i_time.ForeColor = System.Drawing.Color.White;
            this.lb_ld1_i_time.Location = new System.Drawing.Point(716, 152);
            this.lb_ld1_i_time.Name = "lb_ld1_i_time";
            this.lb_ld1_i_time.Size = new System.Drawing.Size(57, 12);
            this.lb_ld1_i_time.TabIndex = 32;
            this.lb_ld1_i_time.Text = "00시 00분";
            // 
            // lb_ld1_i
            // 
            this.lb_ld1_i.AutoSize = true;
            this.lb_ld1_i.BackColor = System.Drawing.Color.Transparent;
            this.lb_ld1_i.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld1_i.ForeColor = System.Drawing.Color.White;
            this.lb_ld1_i.Location = new System.Drawing.Point(689, 152);
            this.lb_ld1_i.Name = "lb_ld1_i";
            this.lb_ld1_i.Size = new System.Drawing.Size(16, 12);
            this.lb_ld1_i.TabIndex = 31;
            this.lb_ld1_i.Text = "I :";
            // 
            // lb_ld2_o_time
            // 
            this.lb_ld2_o_time.AutoSize = true;
            this.lb_ld2_o_time.BackColor = System.Drawing.Color.Transparent;
            this.lb_ld2_o_time.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld2_o_time.ForeColor = System.Drawing.Color.White;
            this.lb_ld2_o_time.Location = new System.Drawing.Point(716, 102);
            this.lb_ld2_o_time.Name = "lb_ld2_o_time";
            this.lb_ld2_o_time.Size = new System.Drawing.Size(57, 12);
            this.lb_ld2_o_time.TabIndex = 30;
            this.lb_ld2_o_time.Text = "00시 00분";
            // 
            // lb_ld2_o
            // 
            this.lb_ld2_o.AutoSize = true;
            this.lb_ld2_o.BackColor = System.Drawing.Color.Transparent;
            this.lb_ld2_o.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld2_o.ForeColor = System.Drawing.Color.White;
            this.lb_ld2_o.Location = new System.Drawing.Point(689, 102);
            this.lb_ld2_o.Name = "lb_ld2_o";
            this.lb_ld2_o.Size = new System.Drawing.Size(22, 12);
            this.lb_ld2_o.TabIndex = 29;
            this.lb_ld2_o.Text = "O :";
            // 
            // lb_ld2_i_time
            // 
            this.lb_ld2_i_time.AutoSize = true;
            this.lb_ld2_i_time.BackColor = System.Drawing.Color.Transparent;
            this.lb_ld2_i_time.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld2_i_time.ForeColor = System.Drawing.Color.White;
            this.lb_ld2_i_time.Location = new System.Drawing.Point(716, 76);
            this.lb_ld2_i_time.Name = "lb_ld2_i_time";
            this.lb_ld2_i_time.Size = new System.Drawing.Size(57, 12);
            this.lb_ld2_i_time.TabIndex = 28;
            this.lb_ld2_i_time.Text = "00시 00분";
            // 
            // lb_ld2_i
            // 
            this.lb_ld2_i.AutoSize = true;
            this.lb_ld2_i.BackColor = System.Drawing.Color.Transparent;
            this.lb_ld2_i.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld2_i.ForeColor = System.Drawing.Color.White;
            this.lb_ld2_i.Location = new System.Drawing.Point(689, 76);
            this.lb_ld2_i.Name = "lb_ld2_i";
            this.lb_ld2_i.Size = new System.Drawing.Size(16, 12);
            this.lb_ld2_i.TabIndex = 27;
            this.lb_ld2_i.Text = "I :";
            // 
            // lb_uld1_i_time
            // 
            this.lb_uld1_i_time.AutoSize = true;
            this.lb_uld1_i_time.BackColor = System.Drawing.Color.Transparent;
            this.lb_uld1_i_time.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld1_i_time.ForeColor = System.Drawing.Color.White;
            this.lb_uld1_i_time.Location = new System.Drawing.Point(33, 179);
            this.lb_uld1_i_time.Name = "lb_uld1_i_time";
            this.lb_uld1_i_time.Size = new System.Drawing.Size(57, 12);
            this.lb_uld1_i_time.TabIndex = 26;
            this.lb_uld1_i_time.Text = "00시 00분";
            // 
            // lb_uld1_i
            // 
            this.lb_uld1_i.AutoSize = true;
            this.lb_uld1_i.BackColor = System.Drawing.Color.Transparent;
            this.lb_uld1_i.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld1_i.ForeColor = System.Drawing.Color.White;
            this.lb_uld1_i.Location = new System.Drawing.Point(6, 179);
            this.lb_uld1_i.Name = "lb_uld1_i";
            this.lb_uld1_i.Size = new System.Drawing.Size(16, 12);
            this.lb_uld1_i.TabIndex = 25;
            this.lb_uld1_i.Text = "I :";
            // 
            // lb_uld1_o_time
            // 
            this.lb_uld1_o_time.AutoSize = true;
            this.lb_uld1_o_time.BackColor = System.Drawing.Color.Transparent;
            this.lb_uld1_o_time.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld1_o_time.ForeColor = System.Drawing.Color.White;
            this.lb_uld1_o_time.Location = new System.Drawing.Point(33, 152);
            this.lb_uld1_o_time.Name = "lb_uld1_o_time";
            this.lb_uld1_o_time.Size = new System.Drawing.Size(57, 12);
            this.lb_uld1_o_time.TabIndex = 24;
            this.lb_uld1_o_time.Text = "00시 00분";
            // 
            // lb_uld1_o
            // 
            this.lb_uld1_o.AutoSize = true;
            this.lb_uld1_o.BackColor = System.Drawing.Color.Transparent;
            this.lb_uld1_o.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld1_o.ForeColor = System.Drawing.Color.White;
            this.lb_uld1_o.Location = new System.Drawing.Point(6, 152);
            this.lb_uld1_o.Name = "lb_uld1_o";
            this.lb_uld1_o.Size = new System.Drawing.Size(22, 12);
            this.lb_uld1_o.TabIndex = 23;
            this.lb_uld1_o.Text = "O :";
            // 
            // lb_uld2_i_time
            // 
            this.lb_uld2_i_time.AutoSize = true;
            this.lb_uld2_i_time.BackColor = System.Drawing.Color.Transparent;
            this.lb_uld2_i_time.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld2_i_time.ForeColor = System.Drawing.Color.White;
            this.lb_uld2_i_time.Location = new System.Drawing.Point(33, 103);
            this.lb_uld2_i_time.Name = "lb_uld2_i_time";
            this.lb_uld2_i_time.Size = new System.Drawing.Size(57, 12);
            this.lb_uld2_i_time.TabIndex = 22;
            this.lb_uld2_i_time.Text = "00시 00분";
            // 
            // lb_uld2_i
            // 
            this.lb_uld2_i.AutoSize = true;
            this.lb_uld2_i.BackColor = System.Drawing.Color.Transparent;
            this.lb_uld2_i.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld2_i.ForeColor = System.Drawing.Color.White;
            this.lb_uld2_i.Location = new System.Drawing.Point(6, 103);
            this.lb_uld2_i.Name = "lb_uld2_i";
            this.lb_uld2_i.Size = new System.Drawing.Size(16, 12);
            this.lb_uld2_i.TabIndex = 21;
            this.lb_uld2_i.Text = "I :";
            // 
            // lb_uld2_o_time
            // 
            this.lb_uld2_o_time.AutoSize = true;
            this.lb_uld2_o_time.BackColor = System.Drawing.Color.Transparent;
            this.lb_uld2_o_time.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld2_o_time.ForeColor = System.Drawing.Color.White;
            this.lb_uld2_o_time.Location = new System.Drawing.Point(33, 76);
            this.lb_uld2_o_time.Name = "lb_uld2_o_time";
            this.lb_uld2_o_time.Size = new System.Drawing.Size(57, 12);
            this.lb_uld2_o_time.TabIndex = 6;
            this.lb_uld2_o_time.Text = "00시 00분";
            // 
            // lb_uld2_o
            // 
            this.lb_uld2_o.AutoSize = true;
            this.lb_uld2_o.BackColor = System.Drawing.Color.Transparent;
            this.lb_uld2_o.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld2_o.ForeColor = System.Drawing.Color.White;
            this.lb_uld2_o.Location = new System.Drawing.Point(6, 76);
            this.lb_uld2_o.Name = "lb_uld2_o";
            this.lb_uld2_o.Size = new System.Drawing.Size(22, 12);
            this.lb_uld2_o.TabIndex = 5;
            this.lb_uld2_o.Text = "O :";
            // 
            // panel_shutter_open
            // 
            this.panel_shutter_open.BackColor = System.Drawing.Color.DimGray;
            this.panel_shutter_open.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_shutter_open.Controls.Add(this.lb_shutter_open);
            this.panel_shutter_open.ForeColor = System.Drawing.Color.White;
            this.panel_shutter_open.Location = new System.Drawing.Point(606, 23);
            this.panel_shutter_open.Name = "panel_shutter_open";
            this.panel_shutter_open.Size = new System.Drawing.Size(65, 30);
            this.panel_shutter_open.TabIndex = 20;
            // 
            // lb_shutter_open
            // 
            this.lb_shutter_open.BackColor = System.Drawing.Color.Transparent;
            this.lb_shutter_open.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_shutter_open.ForeColor = System.Drawing.Color.White;
            this.lb_shutter_open.Location = new System.Drawing.Point(3, 3);
            this.lb_shutter_open.Name = "lb_shutter_open";
            this.lb_shutter_open.Size = new System.Drawing.Size(57, 25);
            this.lb_shutter_open.TabIndex = 4;
            this.lb_shutter_open.Text = "Shutter Open";
            this.lb_shutter_open.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_lock
            // 
            this.panel_lock.BackColor = System.Drawing.Color.DimGray;
            this.panel_lock.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_lock.Controls.Add(this.lb_lock);
            this.panel_lock.ForeColor = System.Drawing.Color.White;
            this.panel_lock.Location = new System.Drawing.Point(535, 23);
            this.panel_lock.Name = "panel_lock";
            this.panel_lock.Size = new System.Drawing.Size(65, 30);
            this.panel_lock.TabIndex = 19;
            // 
            // lb_lock
            // 
            this.lb_lock.AutoSize = true;
            this.lb_lock.BackColor = System.Drawing.Color.Transparent;
            this.lb_lock.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_lock.ForeColor = System.Drawing.Color.White;
            this.lb_lock.Location = new System.Drawing.Point(14, 9);
            this.lb_lock.Name = "lb_lock";
            this.lb_lock.Size = new System.Drawing.Size(36, 12);
            this.lb_lock.TabIndex = 4;
            this.lb_lock.Text = "Lock";
            // 
            // panel_laser_cover
            // 
            this.panel_laser_cover.BackColor = System.Drawing.Color.DimGray;
            this.panel_laser_cover.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_laser_cover.Controls.Add(this.lb_laser_cover);
            this.panel_laser_cover.ForeColor = System.Drawing.Color.White;
            this.panel_laser_cover.Location = new System.Drawing.Point(447, 23);
            this.panel_laser_cover.Name = "panel_laser_cover";
            this.panel_laser_cover.Size = new System.Drawing.Size(82, 30);
            this.panel_laser_cover.TabIndex = 18;
            // 
            // lb_laser_cover
            // 
            this.lb_laser_cover.AutoSize = true;
            this.lb_laser_cover.BackColor = System.Drawing.Color.Transparent;
            this.lb_laser_cover.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_laser_cover.ForeColor = System.Drawing.Color.White;
            this.lb_laser_cover.Location = new System.Drawing.Point(3, 9);
            this.lb_laser_cover.Name = "lb_laser_cover";
            this.lb_laser_cover.Size = new System.Drawing.Size(75, 12);
            this.lb_laser_cover.TabIndex = 4;
            this.lb_laser_cover.Text = "레이저 커버";
            // 
            // panel_ems2
            // 
            this.panel_ems2.BackColor = System.Drawing.Color.DimGray;
            this.panel_ems2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_ems2.Controls.Add(this.lb_ems2);
            this.panel_ems2.ForeColor = System.Drawing.Color.White;
            this.panel_ems2.Location = new System.Drawing.Point(727, 23);
            this.panel_ems2.Name = "panel_ems2";
            this.panel_ems2.Size = new System.Drawing.Size(55, 30);
            this.panel_ems2.TabIndex = 17;
            // 
            // lb_ems2
            // 
            this.lb_ems2.AutoSize = true;
            this.lb_ems2.BackColor = System.Drawing.Color.Transparent;
            this.lb_ems2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ems2.ForeColor = System.Drawing.Color.White;
            this.lb_ems2.Location = new System.Drawing.Point(3, 9);
            this.lb_ems2.Name = "lb_ems2";
            this.lb_ems2.Size = new System.Drawing.Size(47, 12);
            this.lb_ems2.TabIndex = 4;
            this.lb_ems2.Text = "EMS 2";
            // 
            // panel_ems6
            // 
            this.panel_ems6.BackColor = System.Drawing.Color.DimGray;
            this.panel_ems6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_ems6.Controls.Add(this.lb_ems6);
            this.panel_ems6.ForeColor = System.Drawing.Color.White;
            this.panel_ems6.Location = new System.Drawing.Point(8, 23);
            this.panel_ems6.Name = "panel_ems6";
            this.panel_ems6.Size = new System.Drawing.Size(55, 30);
            this.panel_ems6.TabIndex = 16;
            // 
            // lb_ems6
            // 
            this.lb_ems6.AutoSize = true;
            this.lb_ems6.BackColor = System.Drawing.Color.Transparent;
            this.lb_ems6.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ems6.ForeColor = System.Drawing.Color.White;
            this.lb_ems6.Location = new System.Drawing.Point(3, 9);
            this.lb_ems6.Name = "lb_ems6";
            this.lb_ems6.Size = new System.Drawing.Size(47, 12);
            this.lb_ems6.TabIndex = 4;
            this.lb_ems6.Text = "EMS 6";
            // 
            // panel_grab_ems1
            // 
            this.panel_grab_ems1.BackColor = System.Drawing.Color.DimGray;
            this.panel_grab_ems1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_grab_ems1.Controls.Add(this.lb_grab_ems1);
            this.panel_grab_ems1.ForeColor = System.Drawing.Color.White;
            this.panel_grab_ems1.Location = new System.Drawing.Point(619, 3);
            this.panel_grab_ems1.Name = "panel_grab_ems1";
            this.panel_grab_ems1.Size = new System.Drawing.Size(94, 14);
            this.panel_grab_ems1.TabIndex = 19;
            // 
            // lb_grab_ems1
            // 
            this.lb_grab_ems1.AutoSize = true;
            this.lb_grab_ems1.BackColor = System.Drawing.Color.Transparent;
            this.lb_grab_ems1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_grab_ems1.ForeColor = System.Drawing.Color.White;
            this.lb_grab_ems1.Location = new System.Drawing.Point(5, 1);
            this.lb_grab_ems1.Name = "lb_grab_ems1";
            this.lb_grab_ems1.Size = new System.Drawing.Size(83, 12);
            this.lb_grab_ems1.TabIndex = 4;
            this.lb_grab_ems1.Text = "Grab EMS 1";
            // 
            // panel_grab_ems2
            // 
            this.panel_grab_ems2.BackColor = System.Drawing.Color.DimGray;
            this.panel_grab_ems2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_grab_ems2.Controls.Add(this.lb_grab_ems2);
            this.panel_grab_ems2.ForeColor = System.Drawing.Color.White;
            this.panel_grab_ems2.Location = new System.Drawing.Point(405, 3);
            this.panel_grab_ems2.Name = "panel_grab_ems2";
            this.panel_grab_ems2.Size = new System.Drawing.Size(94, 14);
            this.panel_grab_ems2.TabIndex = 17;
            // 
            // lb_grab_ems2
            // 
            this.lb_grab_ems2.AutoSize = true;
            this.lb_grab_ems2.BackColor = System.Drawing.Color.Transparent;
            this.lb_grab_ems2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_grab_ems2.ForeColor = System.Drawing.Color.White;
            this.lb_grab_ems2.Location = new System.Drawing.Point(5, 1);
            this.lb_grab_ems2.Name = "lb_grab_ems2";
            this.lb_grab_ems2.Size = new System.Drawing.Size(83, 12);
            this.lb_grab_ems2.TabIndex = 4;
            this.lb_grab_ems2.Text = "Grab EMS 2";
            // 
            // panel_grab_switch1
            // 
            this.panel_grab_switch1.BackColor = System.Drawing.Color.DimGray;
            this.panel_grab_switch1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_grab_switch1.Controls.Add(this.lb_grab_switch1);
            this.panel_grab_switch1.ForeColor = System.Drawing.Color.White;
            this.panel_grab_switch1.Location = new System.Drawing.Point(505, 3);
            this.panel_grab_switch1.Name = "panel_grab_switch1";
            this.panel_grab_switch1.Size = new System.Drawing.Size(108, 14);
            this.panel_grab_switch1.TabIndex = 18;
            // 
            // lb_grab_switch1
            // 
            this.lb_grab_switch1.AutoSize = true;
            this.lb_grab_switch1.BackColor = System.Drawing.Color.Transparent;
            this.lb_grab_switch1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_grab_switch1.ForeColor = System.Drawing.Color.White;
            this.lb_grab_switch1.Location = new System.Drawing.Point(5, 1);
            this.lb_grab_switch1.Name = "lb_grab_switch1";
            this.lb_grab_switch1.Size = new System.Drawing.Size(97, 12);
            this.lb_grab_switch1.TabIndex = 4;
            this.lb_grab_switch1.Text = "Grab Switch 1";
            // 
            // panel_grab_switch2
            // 
            this.panel_grab_switch2.BackColor = System.Drawing.Color.DimGray;
            this.panel_grab_switch2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_grab_switch2.Controls.Add(this.lb_grab_switch2);
            this.panel_grab_switch2.ForeColor = System.Drawing.Color.White;
            this.panel_grab_switch2.Location = new System.Drawing.Point(291, 3);
            this.panel_grab_switch2.Name = "panel_grab_switch2";
            this.panel_grab_switch2.Size = new System.Drawing.Size(108, 14);
            this.panel_grab_switch2.TabIndex = 16;
            // 
            // lb_grab_switch2
            // 
            this.lb_grab_switch2.AutoSize = true;
            this.lb_grab_switch2.BackColor = System.Drawing.Color.Transparent;
            this.lb_grab_switch2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_grab_switch2.ForeColor = System.Drawing.Color.White;
            this.lb_grab_switch2.Location = new System.Drawing.Point(5, 1);
            this.lb_grab_switch2.Name = "lb_grab_switch2";
            this.lb_grab_switch2.Size = new System.Drawing.Size(97, 12);
            this.lb_grab_switch2.TabIndex = 4;
            this.lb_grab_switch2.Text = "Grab Switch 2";
            // 
            // panel_grab_ems3
            // 
            this.panel_grab_ems3.BackColor = System.Drawing.Color.DimGray;
            this.panel_grab_ems3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_grab_ems3.Controls.Add(this.lb_grab_ems3);
            this.panel_grab_ems3.ForeColor = System.Drawing.Color.White;
            this.panel_grab_ems3.Location = new System.Drawing.Point(191, 3);
            this.panel_grab_ems3.Name = "panel_grab_ems3";
            this.panel_grab_ems3.Size = new System.Drawing.Size(94, 14);
            this.panel_grab_ems3.TabIndex = 16;
            // 
            // lb_grab_ems3
            // 
            this.lb_grab_ems3.AutoSize = true;
            this.lb_grab_ems3.BackColor = System.Drawing.Color.Transparent;
            this.lb_grab_ems3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_grab_ems3.ForeColor = System.Drawing.Color.White;
            this.lb_grab_ems3.Location = new System.Drawing.Point(5, 1);
            this.lb_grab_ems3.Name = "lb_grab_ems3";
            this.lb_grab_ems3.Size = new System.Drawing.Size(83, 12);
            this.lb_grab_ems3.TabIndex = 4;
            this.lb_grab_ems3.Text = "Grab EMS 3";
            // 
            // panel_grab_switch3
            // 
            this.panel_grab_switch3.BackColor = System.Drawing.Color.DimGray;
            this.panel_grab_switch3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_grab_switch3.Controls.Add(this.lb_grab_switch3);
            this.panel_grab_switch3.ForeColor = System.Drawing.Color.White;
            this.panel_grab_switch3.Location = new System.Drawing.Point(77, 3);
            this.panel_grab_switch3.Name = "panel_grab_switch3";
            this.panel_grab_switch3.Size = new System.Drawing.Size(108, 14);
            this.panel_grab_switch3.TabIndex = 15;
            // 
            // lb_grab_switch3
            // 
            this.lb_grab_switch3.AutoSize = true;
            this.lb_grab_switch3.BackColor = System.Drawing.Color.Transparent;
            this.lb_grab_switch3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_grab_switch3.ForeColor = System.Drawing.Color.White;
            this.lb_grab_switch3.Location = new System.Drawing.Point(5, 1);
            this.lb_grab_switch3.Name = "lb_grab_switch3";
            this.lb_grab_switch3.Size = new System.Drawing.Size(97, 12);
            this.lb_grab_switch3.TabIndex = 4;
            this.lb_grab_switch3.Text = "Grab Switch 3";
            // 
            // panel_door10
            // 
            this.panel_door10.BackColor = System.Drawing.Color.DimGray;
            this.panel_door10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_door10.Controls.Add(this.lb_door10);
            this.panel_door10.ForeColor = System.Drawing.Color.White;
            this.panel_door10.Location = new System.Drawing.Point(4, 65);
            this.panel_door10.Name = "panel_door10";
            this.panel_door10.Size = new System.Drawing.Size(37, 59);
            this.panel_door10.TabIndex = 13;
            // 
            // lb_door10
            // 
            this.lb_door10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_door10.ForeColor = System.Drawing.Color.White;
            this.lb_door10.Location = new System.Drawing.Point(4, 3);
            this.lb_door10.Name = "lb_door10";
            this.lb_door10.Size = new System.Drawing.Size(27, 51);
            this.lb_door10.TabIndex = 1;
            this.lb_door10.Text = "도어10";
            this.lb_door10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DimGray;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label30);
            this.panel2.ForeColor = System.Drawing.Color.White;
            this.panel2.Location = new System.Drawing.Point(5, 66);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(37, 59);
            this.panel2.TabIndex = 13;
            // 
            // label30
            // 
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label30.ForeColor = System.Drawing.Color.White;
            this.label30.Location = new System.Drawing.Point(4, 3);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(27, 51);
            this.label30.TabIndex = 1;
            this.label30.Text = "도어10";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_camera_l_info
            // 
            this.panel_camera_l_info.BackColor = System.Drawing.Color.DimGray;
            this.panel_camera_l_info.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_camera_l_info.Controls.Add(this.lb_camera_l);
            this.panel_camera_l_info.Controls.Add(this.panel_camera_l);
            this.panel_camera_l_info.ForeColor = System.Drawing.Color.White;
            this.panel_camera_l_info.Location = new System.Drawing.Point(887, 225);
            this.panel_camera_l_info.Name = "panel_camera_l_info";
            this.panel_camera_l_info.Size = new System.Drawing.Size(168, 156);
            this.panel_camera_l_info.TabIndex = 13;
            // 
            // lb_camera_l
            // 
            this.lb_camera_l.AutoSize = true;
            this.lb_camera_l.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_camera_l.Location = new System.Drawing.Point(3, 4);
            this.lb_camera_l.Name = "lb_camera_l";
            this.lb_camera_l.Size = new System.Drawing.Size(91, 24);
            this.lb_camera_l.TabIndex = 0;
            this.lb_camera_l.Text = "좌측 카메라";
            // 
            // panel_camera_l
            // 
            this.panel_camera_l.BackColor = System.Drawing.Color.White;
            this.panel_camera_l.Location = new System.Drawing.Point(3, 28);
            this.panel_camera_l.Name = "panel_camera_l";
            this.panel_camera_l.Size = new System.Drawing.Size(162, 123);
            this.panel_camera_l.TabIndex = 3;
            // 
            // panel_camera_r_info
            // 
            this.panel_camera_r_info.BackColor = System.Drawing.Color.DimGray;
            this.panel_camera_r_info.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_camera_r_info.Controls.Add(this.lb_camera_r);
            this.panel_camera_r_info.Controls.Add(this.panel_camera_r);
            this.panel_camera_r_info.ForeColor = System.Drawing.Color.White;
            this.panel_camera_r_info.Location = new System.Drawing.Point(1359, 3);
            this.panel_camera_r_info.Name = "panel_camera_r_info";
            this.panel_camera_r_info.Size = new System.Drawing.Size(168, 156);
            this.panel_camera_r_info.TabIndex = 39;
            // 
            // lb_camera_r
            // 
            this.lb_camera_r.AutoSize = true;
            this.lb_camera_r.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_camera_r.Location = new System.Drawing.Point(3, 4);
            this.lb_camera_r.Name = "lb_camera_r";
            this.lb_camera_r.Size = new System.Drawing.Size(91, 24);
            this.lb_camera_r.TabIndex = 0;
            this.lb_camera_r.Text = "우측 카메라";
            // 
            // panel_camera_r
            // 
            this.panel_camera_r.BackColor = System.Drawing.Color.White;
            this.panel_camera_r.Location = new System.Drawing.Point(3, 28);
            this.panel_camera_r.Name = "panel_camera_r";
            this.panel_camera_r.Size = new System.Drawing.Size(162, 123);
            this.panel_camera_r.TabIndex = 3;
            // 
            // panel_align
            // 
            this.panel_align.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_align.Controls.Add(this.tbox_align_break4_offset_a);
            this.panel_align.Controls.Add(this.tbox_align_break3_offset_a);
            this.panel_align.Controls.Add(this.tbox_align_pre2_offset_a);
            this.panel_align.Controls.Add(this.lb_align_offset_a2);
            this.panel_align.Controls.Add(this.tbox_align_break4_offset_y);
            this.panel_align.Controls.Add(this.tbox_align_break3_offset_y);
            this.panel_align.Controls.Add(this.tbox_align_pre2_offset_y);
            this.panel_align.Controls.Add(this.lb_align_offset_y2);
            this.panel_align.Controls.Add(this.tbox_align_break4_offset_x);
            this.panel_align.Controls.Add(this.tbox_align_break3_offset_x);
            this.panel_align.Controls.Add(this.tbox_align_pre2_offset_x);
            this.panel_align.Controls.Add(this.lb_align_offset_x2);
            this.panel_align.Controls.Add(this.tbox_align_break4_result);
            this.panel_align.Controls.Add(this.tbox_align_break3_result);
            this.panel_align.Controls.Add(this.tbox_align_pre2_result);
            this.panel_align.Controls.Add(this.lb_align_result2);
            this.panel_align.Controls.Add(this.lb_align_break4);
            this.panel_align.Controls.Add(this.lb_align_break3);
            this.panel_align.Controls.Add(this.lb_align_pre2);
            this.panel_align.Controls.Add(this.lb_align_name2);
            this.panel_align.Controls.Add(this.tbox_align_break2_offset_a);
            this.panel_align.Controls.Add(this.tbox_align_break1_offset_a);
            this.panel_align.Controls.Add(this.tbox_align_pre1_offset_a);
            this.panel_align.Controls.Add(this.lb_align_offset_a1);
            this.panel_align.Controls.Add(this.tbox_align_break2_offset_y);
            this.panel_align.Controls.Add(this.tbox_align_break1_offset_y);
            this.panel_align.Controls.Add(this.tbox_align_pre1_offset_y);
            this.panel_align.Controls.Add(this.lb_align_offset_y1);
            this.panel_align.Controls.Add(this.tbox_align_break2_offset_x);
            this.panel_align.Controls.Add(this.tbox_align_break1_offset_x);
            this.panel_align.Controls.Add(this.tbox_align_pre1_offset_x);
            this.panel_align.Controls.Add(this.lb_align_offset_x1);
            this.panel_align.Controls.Add(this.tbox_align_break2_result);
            this.panel_align.Controls.Add(this.tbox_align_break1_result);
            this.panel_align.Controls.Add(this.tbox_align_pre1_result);
            this.panel_align.Controls.Add(this.lb_align_result1);
            this.panel_align.Controls.Add(this.lb_align_break2);
            this.panel_align.Controls.Add(this.lb_align_break1);
            this.panel_align.Controls.Add(this.lb_align_pre1);
            this.panel_align.Controls.Add(this.lb_align_name1);
            this.panel_align.ForeColor = System.Drawing.Color.White;
            this.panel_align.Location = new System.Drawing.Point(874, 745);
            this.panel_align.Name = "panel_align";
            this.panel_align.Size = new System.Drawing.Size(864, 112);
            this.panel_align.TabIndex = 54;
            // 
            // tbox_align_break4_offset_a
            // 
            this.tbox_align_break4_offset_a.BackColor = System.Drawing.Color.DimGray;
            this.tbox_align_break4_offset_a.Location = new System.Drawing.Point(779, 82);
            this.tbox_align_break4_offset_a.Name = "tbox_align_break4_offset_a";
            this.tbox_align_break4_offset_a.ReadOnly = true;
            this.tbox_align_break4_offset_a.Size = new System.Drawing.Size(73, 21);
            this.tbox_align_break4_offset_a.TabIndex = 49;
            // 
            // tbox_align_break3_offset_a
            // 
            this.tbox_align_break3_offset_a.BackColor = System.Drawing.Color.DimGray;
            this.tbox_align_break3_offset_a.Location = new System.Drawing.Point(779, 53);
            this.tbox_align_break3_offset_a.Name = "tbox_align_break3_offset_a";
            this.tbox_align_break3_offset_a.ReadOnly = true;
            this.tbox_align_break3_offset_a.Size = new System.Drawing.Size(73, 21);
            this.tbox_align_break3_offset_a.TabIndex = 48;
            // 
            // tbox_align_pre2_offset_a
            // 
            this.tbox_align_pre2_offset_a.BackColor = System.Drawing.Color.DimGray;
            this.tbox_align_pre2_offset_a.Location = new System.Drawing.Point(779, 26);
            this.tbox_align_pre2_offset_a.Name = "tbox_align_pre2_offset_a";
            this.tbox_align_pre2_offset_a.ReadOnly = true;
            this.tbox_align_pre2_offset_a.Size = new System.Drawing.Size(73, 21);
            this.tbox_align_pre2_offset_a.TabIndex = 46;
            // 
            // lb_align_offset_a2
            // 
            this.lb_align_offset_a2.AutoSize = true;
            this.lb_align_offset_a2.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_align_offset_a2.Location = new System.Drawing.Point(780, 3);
            this.lb_align_offset_a2.Name = "lb_align_offset_a2";
            this.lb_align_offset_a2.Size = new System.Drawing.Size(78, 20);
            this.lb_align_offset_a2.TabIndex = 47;
            this.lb_align_offset_a2.Text = "OFFSET A";
            // 
            // tbox_align_break4_offset_y
            // 
            this.tbox_align_break4_offset_y.BackColor = System.Drawing.Color.DimGray;
            this.tbox_align_break4_offset_y.Location = new System.Drawing.Point(702, 82);
            this.tbox_align_break4_offset_y.Name = "tbox_align_break4_offset_y";
            this.tbox_align_break4_offset_y.ReadOnly = true;
            this.tbox_align_break4_offset_y.Size = new System.Drawing.Size(73, 21);
            this.tbox_align_break4_offset_y.TabIndex = 45;
            // 
            // tbox_align_break3_offset_y
            // 
            this.tbox_align_break3_offset_y.BackColor = System.Drawing.Color.DimGray;
            this.tbox_align_break3_offset_y.Location = new System.Drawing.Point(702, 53);
            this.tbox_align_break3_offset_y.Name = "tbox_align_break3_offset_y";
            this.tbox_align_break3_offset_y.ReadOnly = true;
            this.tbox_align_break3_offset_y.Size = new System.Drawing.Size(73, 21);
            this.tbox_align_break3_offset_y.TabIndex = 44;
            // 
            // tbox_align_pre2_offset_y
            // 
            this.tbox_align_pre2_offset_y.BackColor = System.Drawing.Color.DimGray;
            this.tbox_align_pre2_offset_y.Location = new System.Drawing.Point(702, 26);
            this.tbox_align_pre2_offset_y.Name = "tbox_align_pre2_offset_y";
            this.tbox_align_pre2_offset_y.ReadOnly = true;
            this.tbox_align_pre2_offset_y.Size = new System.Drawing.Size(73, 21);
            this.tbox_align_pre2_offset_y.TabIndex = 42;
            // 
            // lb_align_offset_y2
            // 
            this.lb_align_offset_y2.AutoSize = true;
            this.lb_align_offset_y2.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_align_offset_y2.Location = new System.Drawing.Point(703, 3);
            this.lb_align_offset_y2.Name = "lb_align_offset_y2";
            this.lb_align_offset_y2.Size = new System.Drawing.Size(76, 20);
            this.lb_align_offset_y2.TabIndex = 43;
            this.lb_align_offset_y2.Text = "OFFSET Y";
            // 
            // tbox_align_break4_offset_x
            // 
            this.tbox_align_break4_offset_x.BackColor = System.Drawing.Color.DimGray;
            this.tbox_align_break4_offset_x.Location = new System.Drawing.Point(623, 82);
            this.tbox_align_break4_offset_x.Name = "tbox_align_break4_offset_x";
            this.tbox_align_break4_offset_x.ReadOnly = true;
            this.tbox_align_break4_offset_x.Size = new System.Drawing.Size(73, 21);
            this.tbox_align_break4_offset_x.TabIndex = 41;
            // 
            // tbox_align_break3_offset_x
            // 
            this.tbox_align_break3_offset_x.BackColor = System.Drawing.Color.DimGray;
            this.tbox_align_break3_offset_x.Location = new System.Drawing.Point(623, 53);
            this.tbox_align_break3_offset_x.Name = "tbox_align_break3_offset_x";
            this.tbox_align_break3_offset_x.ReadOnly = true;
            this.tbox_align_break3_offset_x.Size = new System.Drawing.Size(73, 21);
            this.tbox_align_break3_offset_x.TabIndex = 40;
            // 
            // tbox_align_pre2_offset_x
            // 
            this.tbox_align_pre2_offset_x.BackColor = System.Drawing.Color.DimGray;
            this.tbox_align_pre2_offset_x.Location = new System.Drawing.Point(623, 26);
            this.tbox_align_pre2_offset_x.Name = "tbox_align_pre2_offset_x";
            this.tbox_align_pre2_offset_x.ReadOnly = true;
            this.tbox_align_pre2_offset_x.Size = new System.Drawing.Size(73, 21);
            this.tbox_align_pre2_offset_x.TabIndex = 38;
            // 
            // lb_align_offset_x2
            // 
            this.lb_align_offset_x2.AutoSize = true;
            this.lb_align_offset_x2.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_align_offset_x2.Location = new System.Drawing.Point(624, 3);
            this.lb_align_offset_x2.Name = "lb_align_offset_x2";
            this.lb_align_offset_x2.Size = new System.Drawing.Size(77, 20);
            this.lb_align_offset_x2.TabIndex = 39;
            this.lb_align_offset_x2.Text = "OFFSET X";
            // 
            // tbox_align_break4_result
            // 
            this.tbox_align_break4_result.BackColor = System.Drawing.Color.DimGray;
            this.tbox_align_break4_result.Location = new System.Drawing.Point(544, 82);
            this.tbox_align_break4_result.Name = "tbox_align_break4_result";
            this.tbox_align_break4_result.ReadOnly = true;
            this.tbox_align_break4_result.Size = new System.Drawing.Size(73, 21);
            this.tbox_align_break4_result.TabIndex = 37;
            // 
            // tbox_align_break3_result
            // 
            this.tbox_align_break3_result.BackColor = System.Drawing.Color.DimGray;
            this.tbox_align_break3_result.Location = new System.Drawing.Point(544, 53);
            this.tbox_align_break3_result.Name = "tbox_align_break3_result";
            this.tbox_align_break3_result.ReadOnly = true;
            this.tbox_align_break3_result.Size = new System.Drawing.Size(73, 21);
            this.tbox_align_break3_result.TabIndex = 36;
            // 
            // tbox_align_pre2_result
            // 
            this.tbox_align_pre2_result.BackColor = System.Drawing.Color.DimGray;
            this.tbox_align_pre2_result.Location = new System.Drawing.Point(544, 26);
            this.tbox_align_pre2_result.Name = "tbox_align_pre2_result";
            this.tbox_align_pre2_result.ReadOnly = true;
            this.tbox_align_pre2_result.Size = new System.Drawing.Size(73, 21);
            this.tbox_align_pre2_result.TabIndex = 31;
            // 
            // lb_align_result2
            // 
            this.lb_align_result2.AutoSize = true;
            this.lb_align_result2.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_align_result2.Location = new System.Drawing.Point(551, 3);
            this.lb_align_result2.Name = "lb_align_result2";
            this.lb_align_result2.Size = new System.Drawing.Size(63, 20);
            this.lb_align_result2.TabIndex = 35;
            this.lb_align_result2.Text = "RESULT";
            // 
            // lb_align_break4
            // 
            this.lb_align_break4.AutoSize = true;
            this.lb_align_break4.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_align_break4.Location = new System.Drawing.Point(453, 87);
            this.lb_align_break4.Name = "lb_align_break4";
            this.lb_align_break4.Size = new System.Drawing.Size(61, 17);
            this.lb_align_break4.TabIndex = 34;
            this.lb_align_break4.Text = "BREAK 4";
            // 
            // lb_align_break3
            // 
            this.lb_align_break3.AutoSize = true;
            this.lb_align_break3.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_align_break3.Location = new System.Drawing.Point(453, 58);
            this.lb_align_break3.Name = "lb_align_break3";
            this.lb_align_break3.Size = new System.Drawing.Size(61, 17);
            this.lb_align_break3.TabIndex = 33;
            this.lb_align_break3.Text = "BREAK 3";
            // 
            // lb_align_pre2
            // 
            this.lb_align_pre2.AutoSize = true;
            this.lb_align_pre2.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_align_pre2.Location = new System.Drawing.Point(460, 30);
            this.lb_align_pre2.Name = "lb_align_pre2";
            this.lb_align_pre2.Size = new System.Drawing.Size(44, 17);
            this.lb_align_pre2.TabIndex = 32;
            this.lb_align_pre2.Text = "PRE 2";
            // 
            // lb_align_name2
            // 
            this.lb_align_name2.AutoSize = true;
            this.lb_align_name2.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_align_name2.Location = new System.Drawing.Point(435, 3);
            this.lb_align_name2.Name = "lb_align_name2";
            this.lb_align_name2.Size = new System.Drawing.Size(106, 20);
            this.lb_align_name2.TabIndex = 30;
            this.lb_align_name2.Text = "ALIGN NAME";
            // 
            // tbox_align_break2_offset_a
            // 
            this.tbox_align_break2_offset_a.BackColor = System.Drawing.Color.DimGray;
            this.tbox_align_break2_offset_a.Location = new System.Drawing.Point(341, 82);
            this.tbox_align_break2_offset_a.Name = "tbox_align_break2_offset_a";
            this.tbox_align_break2_offset_a.ReadOnly = true;
            this.tbox_align_break2_offset_a.Size = new System.Drawing.Size(73, 21);
            this.tbox_align_break2_offset_a.TabIndex = 29;
            // 
            // tbox_align_break1_offset_a
            // 
            this.tbox_align_break1_offset_a.BackColor = System.Drawing.Color.DimGray;
            this.tbox_align_break1_offset_a.Location = new System.Drawing.Point(341, 53);
            this.tbox_align_break1_offset_a.Name = "tbox_align_break1_offset_a";
            this.tbox_align_break1_offset_a.ReadOnly = true;
            this.tbox_align_break1_offset_a.Size = new System.Drawing.Size(73, 21);
            this.tbox_align_break1_offset_a.TabIndex = 28;
            // 
            // tbox_align_pre1_offset_a
            // 
            this.tbox_align_pre1_offset_a.BackColor = System.Drawing.Color.DimGray;
            this.tbox_align_pre1_offset_a.Location = new System.Drawing.Point(341, 26);
            this.tbox_align_pre1_offset_a.Name = "tbox_align_pre1_offset_a";
            this.tbox_align_pre1_offset_a.ReadOnly = true;
            this.tbox_align_pre1_offset_a.Size = new System.Drawing.Size(73, 21);
            this.tbox_align_pre1_offset_a.TabIndex = 26;
            // 
            // lb_align_offset_a1
            // 
            this.lb_align_offset_a1.AutoSize = true;
            this.lb_align_offset_a1.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_align_offset_a1.Location = new System.Drawing.Point(342, 3);
            this.lb_align_offset_a1.Name = "lb_align_offset_a1";
            this.lb_align_offset_a1.Size = new System.Drawing.Size(78, 20);
            this.lb_align_offset_a1.TabIndex = 27;
            this.lb_align_offset_a1.Text = "OFFSET A";
            // 
            // tbox_align_break2_offset_y
            // 
            this.tbox_align_break2_offset_y.BackColor = System.Drawing.Color.DimGray;
            this.tbox_align_break2_offset_y.Location = new System.Drawing.Point(264, 82);
            this.tbox_align_break2_offset_y.Name = "tbox_align_break2_offset_y";
            this.tbox_align_break2_offset_y.ReadOnly = true;
            this.tbox_align_break2_offset_y.Size = new System.Drawing.Size(73, 21);
            this.tbox_align_break2_offset_y.TabIndex = 25;
            // 
            // tbox_align_break1_offset_y
            // 
            this.tbox_align_break1_offset_y.BackColor = System.Drawing.Color.DimGray;
            this.tbox_align_break1_offset_y.Location = new System.Drawing.Point(264, 53);
            this.tbox_align_break1_offset_y.Name = "tbox_align_break1_offset_y";
            this.tbox_align_break1_offset_y.ReadOnly = true;
            this.tbox_align_break1_offset_y.Size = new System.Drawing.Size(73, 21);
            this.tbox_align_break1_offset_y.TabIndex = 24;
            // 
            // tbox_align_pre1_offset_y
            // 
            this.tbox_align_pre1_offset_y.BackColor = System.Drawing.Color.DimGray;
            this.tbox_align_pre1_offset_y.Location = new System.Drawing.Point(264, 26);
            this.tbox_align_pre1_offset_y.Name = "tbox_align_pre1_offset_y";
            this.tbox_align_pre1_offset_y.ReadOnly = true;
            this.tbox_align_pre1_offset_y.Size = new System.Drawing.Size(73, 21);
            this.tbox_align_pre1_offset_y.TabIndex = 21;
            // 
            // lb_align_offset_y1
            // 
            this.lb_align_offset_y1.AutoSize = true;
            this.lb_align_offset_y1.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_align_offset_y1.Location = new System.Drawing.Point(265, 3);
            this.lb_align_offset_y1.Name = "lb_align_offset_y1";
            this.lb_align_offset_y1.Size = new System.Drawing.Size(76, 20);
            this.lb_align_offset_y1.TabIndex = 23;
            this.lb_align_offset_y1.Text = "OFFSET Y";
            // 
            // tbox_align_break2_offset_x
            // 
            this.tbox_align_break2_offset_x.BackColor = System.Drawing.Color.DimGray;
            this.tbox_align_break2_offset_x.Location = new System.Drawing.Point(185, 82);
            this.tbox_align_break2_offset_x.Name = "tbox_align_break2_offset_x";
            this.tbox_align_break2_offset_x.ReadOnly = true;
            this.tbox_align_break2_offset_x.Size = new System.Drawing.Size(73, 21);
            this.tbox_align_break2_offset_x.TabIndex = 20;
            // 
            // tbox_align_break1_offset_x
            // 
            this.tbox_align_break1_offset_x.BackColor = System.Drawing.Color.DimGray;
            this.tbox_align_break1_offset_x.Location = new System.Drawing.Point(185, 53);
            this.tbox_align_break1_offset_x.Name = "tbox_align_break1_offset_x";
            this.tbox_align_break1_offset_x.ReadOnly = true;
            this.tbox_align_break1_offset_x.Size = new System.Drawing.Size(73, 21);
            this.tbox_align_break1_offset_x.TabIndex = 19;
            // 
            // tbox_align_pre1_offset_x
            // 
            this.tbox_align_pre1_offset_x.BackColor = System.Drawing.Color.DimGray;
            this.tbox_align_pre1_offset_x.Location = new System.Drawing.Point(185, 26);
            this.tbox_align_pre1_offset_x.Name = "tbox_align_pre1_offset_x";
            this.tbox_align_pre1_offset_x.ReadOnly = true;
            this.tbox_align_pre1_offset_x.Size = new System.Drawing.Size(73, 21);
            this.tbox_align_pre1_offset_x.TabIndex = 16;
            // 
            // lb_align_offset_x1
            // 
            this.lb_align_offset_x1.AutoSize = true;
            this.lb_align_offset_x1.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_align_offset_x1.Location = new System.Drawing.Point(186, 3);
            this.lb_align_offset_x1.Name = "lb_align_offset_x1";
            this.lb_align_offset_x1.Size = new System.Drawing.Size(77, 20);
            this.lb_align_offset_x1.TabIndex = 18;
            this.lb_align_offset_x1.Text = "OFFSET X";
            // 
            // tbox_align_break2_result
            // 
            this.tbox_align_break2_result.BackColor = System.Drawing.Color.DimGray;
            this.tbox_align_break2_result.Location = new System.Drawing.Point(106, 82);
            this.tbox_align_break2_result.Name = "tbox_align_break2_result";
            this.tbox_align_break2_result.ReadOnly = true;
            this.tbox_align_break2_result.Size = new System.Drawing.Size(73, 21);
            this.tbox_align_break2_result.TabIndex = 15;
            // 
            // tbox_align_break1_result
            // 
            this.tbox_align_break1_result.BackColor = System.Drawing.Color.DimGray;
            this.tbox_align_break1_result.Location = new System.Drawing.Point(106, 53);
            this.tbox_align_break1_result.Name = "tbox_align_break1_result";
            this.tbox_align_break1_result.ReadOnly = true;
            this.tbox_align_break1_result.Size = new System.Drawing.Size(73, 21);
            this.tbox_align_break1_result.TabIndex = 14;
            // 
            // tbox_align_pre1_result
            // 
            this.tbox_align_pre1_result.BackColor = System.Drawing.Color.DimGray;
            this.tbox_align_pre1_result.Location = new System.Drawing.Point(106, 26);
            this.tbox_align_pre1_result.Name = "tbox_align_pre1_result";
            this.tbox_align_pre1_result.ReadOnly = true;
            this.tbox_align_pre1_result.Size = new System.Drawing.Size(73, 21);
            this.tbox_align_pre1_result.TabIndex = 9;
            // 
            // lb_align_result1
            // 
            this.lb_align_result1.AutoSize = true;
            this.lb_align_result1.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_align_result1.Location = new System.Drawing.Point(113, 3);
            this.lb_align_result1.Name = "lb_align_result1";
            this.lb_align_result1.Size = new System.Drawing.Size(63, 20);
            this.lb_align_result1.TabIndex = 13;
            this.lb_align_result1.Text = "RESULT";
            // 
            // lb_align_break2
            // 
            this.lb_align_break2.AutoSize = true;
            this.lb_align_break2.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_align_break2.Location = new System.Drawing.Point(20, 87);
            this.lb_align_break2.Name = "lb_align_break2";
            this.lb_align_break2.Size = new System.Drawing.Size(61, 17);
            this.lb_align_break2.TabIndex = 12;
            this.lb_align_break2.Text = "BREAK 2";
            // 
            // lb_align_break1
            // 
            this.lb_align_break1.AutoSize = true;
            this.lb_align_break1.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_align_break1.Location = new System.Drawing.Point(20, 58);
            this.lb_align_break1.Name = "lb_align_break1";
            this.lb_align_break1.Size = new System.Drawing.Size(61, 17);
            this.lb_align_break1.TabIndex = 11;
            this.lb_align_break1.Text = "BREAK 1";
            // 
            // lb_align_pre1
            // 
            this.lb_align_pre1.AutoSize = true;
            this.lb_align_pre1.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_align_pre1.Location = new System.Drawing.Point(29, 30);
            this.lb_align_pre1.Name = "lb_align_pre1";
            this.lb_align_pre1.Size = new System.Drawing.Size(44, 17);
            this.lb_align_pre1.TabIndex = 10;
            this.lb_align_pre1.Text = "PRE 1";
            // 
            // lb_align_name1
            // 
            this.lb_align_name1.AutoSize = true;
            this.lb_align_name1.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_align_name1.Location = new System.Drawing.Point(4, 3);
            this.lb_align_name1.Name = "lb_align_name1";
            this.lb_align_name1.Size = new System.Drawing.Size(106, 20);
            this.lb_align_name1.TabIndex = 9;
            this.lb_align_name1.Text = "ALIGN NAME";
            // 
            // gbox_breaking_count
            // 
            this.gbox_breaking_count.BackColor = System.Drawing.Color.Transparent;
            this.gbox_breaking_count.Controls.Add(this.panel_breaking_count_clear);
            this.gbox_breaking_count.Controls.Add(this.lb_breaking_count);
            this.gbox_breaking_count.Controls.Add(this.lb_breaking_count_info);
            this.gbox_breaking_count.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.gbox_breaking_count.ForeColor = System.Drawing.Color.White;
            this.gbox_breaking_count.Location = new System.Drawing.Point(1280, 655);
            this.gbox_breaking_count.Name = "gbox_breaking_count";
            this.gbox_breaking_count.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gbox_breaking_count.Size = new System.Drawing.Size(274, 76);
            this.gbox_breaking_count.TabIndex = 62;
            this.gbox_breaking_count.TabStop = false;
            this.gbox_breaking_count.Text = "브레이킹 COUNT";
            // 
            // panel_breaking_count_clear
            // 
            this.panel_breaking_count_clear.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_breaking_count_clear.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_breaking_count_clear.Controls.Add(this.lb_breaking_count_clear);
            this.panel_breaking_count_clear.Location = new System.Drawing.Point(181, 28);
            this.panel_breaking_count_clear.Name = "panel_breaking_count_clear";
            this.panel_breaking_count_clear.Size = new System.Drawing.Size(87, 35);
            this.panel_breaking_count_clear.TabIndex = 3;
            // 
            // lb_breaking_count_clear
            // 
            this.lb_breaking_count_clear.AutoSize = true;
            this.lb_breaking_count_clear.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_count_clear.Location = new System.Drawing.Point(22, 6);
            this.lb_breaking_count_clear.Name = "lb_breaking_count_clear";
            this.lb_breaking_count_clear.Size = new System.Drawing.Size(48, 18);
            this.lb_breaking_count_clear.TabIndex = 0;
            this.lb_breaking_count_clear.Text = "Clear";
            // 
            // lb_breaking_count
            // 
            this.lb_breaking_count.AutoSize = true;
            this.lb_breaking_count.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_count.Location = new System.Drawing.Point(135, 37);
            this.lb_breaking_count.Name = "lb_breaking_count";
            this.lb_breaking_count.Size = new System.Drawing.Size(17, 18);
            this.lb_breaking_count.TabIndex = 2;
            this.lb_breaking_count.Text = "0";
            // 
            // lb_breaking_count_info
            // 
            this.lb_breaking_count_info.AutoSize = true;
            this.lb_breaking_count_info.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_count_info.Location = new System.Drawing.Point(6, 37);
            this.lb_breaking_count_info.Name = "lb_breaking_count_info";
            this.lb_breaking_count_info.Size = new System.Drawing.Size(134, 18);
            this.lb_breaking_count_info.TabIndex = 1;
            this.lb_breaking_count_info.Text = "Breaking Count :";
            // 
            // gbox_dummy_tank
            // 
            this.gbox_dummy_tank.BackColor = System.Drawing.Color.Transparent;
            this.gbox_dummy_tank.Controls.Add(this.panel_dummy_tank_set);
            this.gbox_dummy_tank.Controls.Add(this.panel_dummy_tank_get);
            this.gbox_dummy_tank.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_dummy_tank.ForeColor = System.Drawing.Color.White;
            this.gbox_dummy_tank.Location = new System.Drawing.Point(1570, 655);
            this.gbox_dummy_tank.Name = "gbox_dummy_tank";
            this.gbox_dummy_tank.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gbox_dummy_tank.Size = new System.Drawing.Size(166, 76);
            this.gbox_dummy_tank.TabIndex = 63;
            this.gbox_dummy_tank.TabStop = false;
            this.gbox_dummy_tank.Text = "Dummy Tank";
            // 
            // panel_dummy_tank_set
            // 
            this.panel_dummy_tank_set.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_dummy_tank_set.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_dummy_tank_set.Controls.Add(this.lb_dummy_tank_set);
            this.panel_dummy_tank_set.Location = new System.Drawing.Point(92, 22);
            this.panel_dummy_tank_set.Name = "panel_dummy_tank_set";
            this.panel_dummy_tank_set.Size = new System.Drawing.Size(63, 45);
            this.panel_dummy_tank_set.TabIndex = 4;
            // 
            // lb_dummy_tank_set
            // 
            this.lb_dummy_tank_set.AutoSize = true;
            this.lb_dummy_tank_set.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_dummy_tank_set.Location = new System.Drawing.Point(13, 12);
            this.lb_dummy_tank_set.Name = "lb_dummy_tank_set";
            this.lb_dummy_tank_set.Size = new System.Drawing.Size(34, 18);
            this.lb_dummy_tank_set.TabIndex = 0;
            this.lb_dummy_tank_set.Text = "배출";
            // 
            // panel_dummy_tank_get
            // 
            this.panel_dummy_tank_get.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_dummy_tank_get.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_dummy_tank_get.Controls.Add(this.lb_dummy_tank_get);
            this.panel_dummy_tank_get.Location = new System.Drawing.Point(20, 22);
            this.panel_dummy_tank_get.Name = "panel_dummy_tank_get";
            this.panel_dummy_tank_get.Size = new System.Drawing.Size(63, 45);
            this.panel_dummy_tank_get.TabIndex = 3;
            // 
            // lb_dummy_tank_get
            // 
            this.lb_dummy_tank_get.AutoSize = true;
            this.lb_dummy_tank_get.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_dummy_tank_get.Location = new System.Drawing.Point(13, 12);
            this.lb_dummy_tank_get.Name = "lb_dummy_tank_get";
            this.lb_dummy_tank_get.Size = new System.Drawing.Size(34, 18);
            this.lb_dummy_tank_get.TabIndex = 0;
            this.lb_dummy_tank_get.Text = "투입";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView21);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView17);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView16);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView15);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView10);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView9);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView8);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView7);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView6);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView23);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView22);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView20);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView19);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView18);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView14);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView13);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView12);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView5);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView4);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView3);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView11);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView2);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView1);
            this.splitContainer1.Panel1.Controls.Add(this.cstCellCount3);
            this.splitContainer1.Panel1.Controls.Add(this.cstCellCount4);
            this.splitContainer1.Panel1.Controls.Add(this.cstCellCount2);
            this.splitContainer1.Panel1.Controls.Add(this.cstCellCount1);
            this.splitContainer1.Panel1.Controls.Add(this.gbox_dummy_tank);
            this.splitContainer1.Panel1.Controls.Add(this.gbox_breaking_count);
            this.splitContainer1.Panel1.Controls.Add(this.panel_align);
            this.splitContainer1.Panel1.Controls.Add(this.panel_camera_r_info);
            this.splitContainer1.Panel1.Controls.Add(this.panel_run);
            this.splitContainer1.Panel1.Controls.Add(this.btn_safety_reset);
            this.splitContainer1.Panel1.Controls.Add(this.gbox_ld);
            this.splitContainer1.Panel1.Controls.Add(this.gbox_uld);
            this.splitContainer1.Panel1.Controls.Add(this.gbox_mcr_count);
            this.splitContainer1.Panel1.Controls.Add(this.gbox_laser_info);
            this.splitContainer1.Panel1.Controls.Add(this.panel_runinfo);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.DarkSlateGray;
            this.splitContainer1.Panel2.Controls.Add(this.lb_insp2_connect_info);
            this.splitContainer1.Panel2.Controls.Add(this.lb_serial_connect_info);
            this.splitContainer1.Panel2.Controls.Add(this.panel_insp2_connect_info);
            this.splitContainer1.Panel2.Controls.Add(this.panel_serial_connect_info);
            this.splitContainer1.Panel2.Controls.Add(this.lb_insp1_connect_info);
            this.splitContainer1.Panel2.Controls.Add(this.lb_sequence_connect_info);
            this.splitContainer1.Panel2.Controls.Add(this.panel_insp1_connect_info);
            this.splitContainer1.Panel2.Controls.Add(this.lb_laser_connect_info);
            this.splitContainer1.Panel2.Controls.Add(this.panel_sequence_connect_info);
            this.splitContainer1.Panel2.Controls.Add(this.panel_laser_connect_info);
            this.splitContainer1.Panel2.Controls.Add(this.lb_umac_connect_info);
            this.splitContainer1.Panel2.Controls.Add(this.panel_umac_connect_info);
            this.splitContainer1.Panel2.Controls.Add(this.lb_ajin_connect_info);
            this.splitContainer1.Panel2.Controls.Add(this.lb_cim_connect_info);
            this.splitContainer1.Panel2.Controls.Add(this.panel_ajin_connect_info);
            this.splitContainer1.Panel2.Controls.Add(this.panel_cim_connect_info);
            this.splitContainer1.Panel2.Controls.Add(this.lb_process_connect_info);
            this.splitContainer1.Panel2.Controls.Add(this.btn_light);
            this.splitContainer1.Panel2.Controls.Add(this.btn_dooropen);
            this.splitContainer1.Panel2.Controls.Add(this.btn_pm);
            this.splitContainer1.Panel2.Controls.Add(this.btn_keyswitch);
            this.splitContainer1.Panel2.Controls.Add(this.btn_stop);
            this.splitContainer1.Panel2.Controls.Add(this.btn_pause);
            this.splitContainer1.Panel2.Controls.Add(this.btn_start);
            this.splitContainer1.Size = new System.Drawing.Size(1880, 860);
            this.splitContainer1.SplitterDistance = 1740;
            this.splitContainer1.TabIndex = 0;
            // 
            // detailTactView21
            // 
            this.detailTactView21.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView21.Location = new System.Drawing.Point(1049, 626);
            this.detailTactView21.Name = "detailTactView21";
            this.detailTactView21.Size = new System.Drawing.Size(166, 110);
            this.detailTactView21.TabIndex = 90;
            // 
            // detailTactView17
            // 
            this.detailTactView17.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView17.Location = new System.Drawing.Point(1568, 507);
            this.detailTactView17.Name = "detailTactView17";
            this.detailTactView17.Size = new System.Drawing.Size(166, 110);
            this.detailTactView17.TabIndex = 89;
            // 
            // detailTactView16
            // 
            this.detailTactView16.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView16.Location = new System.Drawing.Point(1221, 508);
            this.detailTactView16.Name = "detailTactView16";
            this.detailTactView16.Size = new System.Drawing.Size(166, 110);
            this.detailTactView16.TabIndex = 88;
            // 
            // detailTactView15
            // 
            this.detailTactView15.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView15.Location = new System.Drawing.Point(1049, 508);
            this.detailTactView15.Name = "detailTactView15";
            this.detailTactView15.Size = new System.Drawing.Size(166, 110);
            this.detailTactView15.TabIndex = 87;
            // 
            // detailTactView10
            // 
            this.detailTactView10.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView10.Location = new System.Drawing.Point(1569, 391);
            this.detailTactView10.Name = "detailTactView10";
            this.detailTactView10.Size = new System.Drawing.Size(166, 110);
            this.detailTactView10.TabIndex = 86;
            // 
            // detailTactView9
            // 
            this.detailTactView9.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView9.Location = new System.Drawing.Point(1395, 391);
            this.detailTactView9.Name = "detailTactView9";
            this.detailTactView9.Size = new System.Drawing.Size(166, 110);
            this.detailTactView9.TabIndex = 85;
            // 
            // detailTactView8
            // 
            this.detailTactView8.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView8.Location = new System.Drawing.Point(1221, 391);
            this.detailTactView8.Name = "detailTactView8";
            this.detailTactView8.Size = new System.Drawing.Size(166, 110);
            this.detailTactView8.TabIndex = 84;
            // 
            // detailTactView7
            // 
            this.detailTactView7.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView7.Location = new System.Drawing.Point(1049, 391);
            this.detailTactView7.Name = "detailTactView7";
            this.detailTactView7.Size = new System.Drawing.Size(166, 110);
            this.detailTactView7.TabIndex = 83;
            // 
            // detailTactView6
            // 
            this.detailTactView6.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView6.Location = new System.Drawing.Point(875, 391);
            this.detailTactView6.Name = "detailTactView6";
            this.detailTactView6.Size = new System.Drawing.Size(166, 110);
            this.detailTactView6.TabIndex = 82;
            // 
            // detailTactView23
            // 
            this.detailTactView23.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView23.Location = new System.Drawing.Point(700, 742);
            this.detailTactView23.Name = "detailTactView23";
            this.detailTactView23.Size = new System.Drawing.Size(166, 110);
            this.detailTactView23.TabIndex = 81;
            // 
            // detailTactView22
            // 
            this.detailTactView22.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView22.Location = new System.Drawing.Point(526, 742);
            this.detailTactView22.Name = "detailTactView22";
            this.detailTactView22.Size = new System.Drawing.Size(166, 110);
            this.detailTactView22.TabIndex = 80;
            // 
            // detailTactView20
            // 
            this.detailTactView20.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView20.Location = new System.Drawing.Point(700, 626);
            this.detailTactView20.Name = "detailTactView20";
            this.detailTactView20.Size = new System.Drawing.Size(166, 110);
            this.detailTactView20.TabIndex = 79;
            // 
            // detailTactView19
            // 
            this.detailTactView19.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView19.Location = new System.Drawing.Point(526, 626);
            this.detailTactView19.Name = "detailTactView19";
            this.detailTactView19.Size = new System.Drawing.Size(166, 110);
            this.detailTactView19.TabIndex = 78;
            // 
            // detailTactView18
            // 
            this.detailTactView18.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView18.Location = new System.Drawing.Point(352, 626);
            this.detailTactView18.Name = "detailTactView18";
            this.detailTactView18.Size = new System.Drawing.Size(166, 110);
            this.detailTactView18.TabIndex = 77;
            // 
            // detailTactView14
            // 
            this.detailTactView14.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView14.Location = new System.Drawing.Point(700, 509);
            this.detailTactView14.Name = "detailTactView14";
            this.detailTactView14.Size = new System.Drawing.Size(166, 110);
            this.detailTactView14.TabIndex = 76;
            // 
            // detailTactView13
            // 
            this.detailTactView13.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView13.Location = new System.Drawing.Point(526, 509);
            this.detailTactView13.Name = "detailTactView13";
            this.detailTactView13.Size = new System.Drawing.Size(166, 110);
            this.detailTactView13.TabIndex = 75;
            // 
            // detailTactView12
            // 
            this.detailTactView12.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView12.Location = new System.Drawing.Point(352, 509);
            this.detailTactView12.Name = "detailTactView12";
            this.detailTactView12.Size = new System.Drawing.Size(166, 110);
            this.detailTactView12.TabIndex = 74;
            // 
            // detailTactView5
            // 
            this.detailTactView5.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView5.Location = new System.Drawing.Point(700, 391);
            this.detailTactView5.Name = "detailTactView5";
            this.detailTactView5.Size = new System.Drawing.Size(166, 110);
            this.detailTactView5.TabIndex = 73;
            // 
            // detailTactView4
            // 
            this.detailTactView4.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView4.Location = new System.Drawing.Point(526, 391);
            this.detailTactView4.Name = "detailTactView4";
            this.detailTactView4.Size = new System.Drawing.Size(166, 110);
            this.detailTactView4.TabIndex = 72;
            // 
            // detailTactView3
            // 
            this.detailTactView3.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView3.Location = new System.Drawing.Point(352, 391);
            this.detailTactView3.Name = "detailTactView3";
            this.detailTactView3.Size = new System.Drawing.Size(166, 110);
            this.detailTactView3.TabIndex = 71;
            // 
            // detailTactView11
            // 
            this.detailTactView11.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView11.Location = new System.Drawing.Point(7, 511);
            this.detailTactView11.Name = "detailTactView11";
            this.detailTactView11.Size = new System.Drawing.Size(166, 110);
            this.detailTactView11.TabIndex = 70;
            // 
            // detailTactView2
            // 
            this.detailTactView2.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView2.Location = new System.Drawing.Point(180, 391);
            this.detailTactView2.Name = "detailTactView2";
            this.detailTactView2.Size = new System.Drawing.Size(166, 110);
            this.detailTactView2.TabIndex = 69;
            // 
            // detailTactView1
            // 
            this.detailTactView1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView1.Location = new System.Drawing.Point(6, 391);
            this.detailTactView1.Name = "detailTactView1";
            this.detailTactView1.Size = new System.Drawing.Size(166, 110);
            this.detailTactView1.TabIndex = 68;
            // 
            // cstCellCount3
            // 
            this.cstCellCount3.BackColor = System.Drawing.Color.DimGray;
            this.cstCellCount3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cstCellCount3.Location = new System.Drawing.Point(1638, 5);
            this.cstCellCount3.Name = "cstCellCount3";
            this.cstCellCount3.Size = new System.Drawing.Size(96, 380);
            this.cstCellCount3.TabIndex = 67;
            // 
            // cstCellCount4
            // 
            this.cstCellCount4.BackColor = System.Drawing.Color.DimGray;
            this.cstCellCount4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cstCellCount4.Location = new System.Drawing.Point(1533, 5);
            this.cstCellCount4.Name = "cstCellCount4";
            this.cstCellCount4.Size = new System.Drawing.Size(96, 380);
            this.cstCellCount4.TabIndex = 66;
            // 
            // cstCellCount2
            // 
            this.cstCellCount2.BackColor = System.Drawing.Color.DimGray;
            this.cstCellCount2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cstCellCount2.Location = new System.Drawing.Point(368, 5);
            this.cstCellCount2.Name = "cstCellCount2";
            this.cstCellCount2.Size = new System.Drawing.Size(96, 380);
            this.cstCellCount2.TabIndex = 65;
            // 
            // cstCellCount1
            // 
            this.cstCellCount1.BackColor = System.Drawing.Color.DimGray;
            this.cstCellCount1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cstCellCount1.Location = new System.Drawing.Point(263, 5);
            this.cstCellCount1.Name = "cstCellCount1";
            this.cstCellCount1.Size = new System.Drawing.Size(96, 380);
            this.cstCellCount1.TabIndex = 64;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.Controls.Add(this.splitContainer1);
            this.Name = "MainWindow";
            this.Size = new System.Drawing.Size(1880, 860);
            this.panel_runinfo.ResumeLayout(false);
            this.panel_runinfo.PerformLayout();
            this.gbox_laser_info.ResumeLayout(false);
            this.gbox_laser_info.PerformLayout();
            this.gbox_mcr_count.ResumeLayout(false);
            this.panel_cstinfo.ResumeLayout(false);
            this.panel_cstinfo.PerformLayout();
            this.panel_day_info.ResumeLayout(false);
            this.panel_day_info.PerformLayout();
            this.gbox_uld.ResumeLayout(false);
            this.panel_uld_in_muting.ResumeLayout(false);
            this.panel_uld_in_muting.PerformLayout();
            this.panel_uld_out_muting.ResumeLayout(false);
            this.panel_uld_out_muting.PerformLayout();
            this.gbox_ld.ResumeLayout(false);
            this.panel_ld_out_muting.ResumeLayout(false);
            this.panel_ld_out_muting.PerformLayout();
            this.panel_ld_in_muting.ResumeLayout(false);
            this.panel_ld_in_muting.PerformLayout();
            this.panel_run.ResumeLayout(false);
            this.panel_lot_b_info.ResumeLayout(false);
            this.panel_lot_b_info.PerformLayout();
            this.panel_lot_a_info.ResumeLayout(false);
            this.panel_lot_a_info.PerformLayout();
            this.panel_ld_muting_out.ResumeLayout(false);
            this.panel_uld_muting_in.ResumeLayout(false);
            this.panel_ld_muting_in.ResumeLayout(false);
            this.panel_uld_muting_out.ResumeLayout(false);
            this.panel_door2.ResumeLayout(false);
            this.panel38.ResumeLayout(false);
            this.panel_door3.ResumeLayout(false);
            this.panel_door3.PerformLayout();
            this.panel_door5.ResumeLayout(false);
            this.panel_door5.PerformLayout();
            this.panel_door4.ResumeLayout(false);
            this.panel_door4.PerformLayout();
            this.panel_door6.ResumeLayout(false);
            this.panel_door6.PerformLayout();
            this.panel_door8.ResumeLayout(false);
            this.panel_door8.PerformLayout();
            this.panel_door7.ResumeLayout(false);
            this.panel_door7.PerformLayout();
            this.panel_door9.ResumeLayout(false);
            this.panel_door9.PerformLayout();
            this.panel_b_cst_skip.ResumeLayout(false);
            this.panel_a_cst_skip.ResumeLayout(false);
            this.panel_ld_box.ResumeLayout(false);
            this.panel_ld_box.PerformLayout();
            this.panel_ems3.ResumeLayout(false);
            this.panel_ems3.PerformLayout();
            this.panel_process_box.ResumeLayout(false);
            this.panel_process_box.PerformLayout();
            this.panel_ems5.ResumeLayout(false);
            this.panel_ems5.PerformLayout();
            this.panel_uld_box.ResumeLayout(false);
            this.panel_uld_box.PerformLayout();
            this.panel_run2.ResumeLayout(false);
            this.panel_run2.PerformLayout();
            this.panel_ld_light.ResumeLayout(false);
            this.panel_ld_light.PerformLayout();
            this.panel_process_light.ResumeLayout(false);
            this.panel_process_light.PerformLayout();
            this.panel_uld_light.ResumeLayout(false);
            this.panel_uld_light.PerformLayout();
            this.panel_ems1.ResumeLayout(false);
            this.panel_ems1.PerformLayout();
            this.panel_target_avgpower.ResumeLayout(false);
            this.panel_target_avgpower.PerformLayout();
            this.panel_zpos_gap.ResumeLayout(false);
            this.panel_zpos_gap.PerformLayout();
            this.panel_ems4.ResumeLayout(false);
            this.panel_ems4.PerformLayout();
            this.panel_ld_out_b.ResumeLayout(false);
            this.panel_ld_in_b.ResumeLayout(false);
            this.panel_ld_out_a.ResumeLayout(false);
            this.panel_ld_in_a.ResumeLayout(false);
            this.panel_uld_in_b.ResumeLayout(false);
            this.panel_uld_out_b.ResumeLayout(false);
            this.panel_uld_in_a.ResumeLayout(false);
            this.panel_uld_out_a.ResumeLayout(false);
            this.panel_uld1.ResumeLayout(false);
            this.panel_uld1.PerformLayout();
            this.panel_uld2.ResumeLayout(false);
            this.panel_uld2.PerformLayout();
            this.panel_uld_trans_a1.ResumeLayout(false);
            this.panel_uld_trans_b1.ResumeLayout(false);
            this.panel_uld_trans_a2.ResumeLayout(false);
            this.panel_uld_trans_b2.ResumeLayout(false);
            this.panel_break_table_a1.ResumeLayout(false);
            this.panel_break_table_b1.ResumeLayout(false);
            this.panel_break_trans1.ResumeLayout(false);
            this.panel_break_trans2.ResumeLayout(false);
            this.panel_break_table_a2.ResumeLayout(false);
            this.panel_break_table_b2.ResumeLayout(false);
            this.panel_process_table_a1.ResumeLayout(false);
            this.panel_process_table_b1.ResumeLayout(false);
            this.panel_process_table_a2.ResumeLayout(false);
            this.panel_ld_trans1.ResumeLayout(false);
            this.panel_process_table_b2.ResumeLayout(false);
            this.panel_ld1.ResumeLayout(false);
            this.panel_ld1.PerformLayout();
            this.panel_ld_trans2.ResumeLayout(false);
            this.panel_ld2.ResumeLayout(false);
            this.panel_ld2.PerformLayout();
            this.panel_shutter_open.ResumeLayout(false);
            this.panel_lock.ResumeLayout(false);
            this.panel_lock.PerformLayout();
            this.panel_laser_cover.ResumeLayout(false);
            this.panel_laser_cover.PerformLayout();
            this.panel_ems2.ResumeLayout(false);
            this.panel_ems2.PerformLayout();
            this.panel_ems6.ResumeLayout(false);
            this.panel_ems6.PerformLayout();
            this.panel_grab_ems1.ResumeLayout(false);
            this.panel_grab_ems1.PerformLayout();
            this.panel_grab_ems2.ResumeLayout(false);
            this.panel_grab_ems2.PerformLayout();
            this.panel_grab_switch1.ResumeLayout(false);
            this.panel_grab_switch1.PerformLayout();
            this.panel_grab_switch2.ResumeLayout(false);
            this.panel_grab_switch2.PerformLayout();
            this.panel_grab_ems3.ResumeLayout(false);
            this.panel_grab_ems3.PerformLayout();
            this.panel_grab_switch3.ResumeLayout(false);
            this.panel_grab_switch3.PerformLayout();
            this.panel_door10.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel_camera_l_info.ResumeLayout(false);
            this.panel_camera_l_info.PerformLayout();
            this.panel_camera_r_info.ResumeLayout(false);
            this.panel_camera_r_info.PerformLayout();
            this.panel_align.ResumeLayout(false);
            this.panel_align.PerformLayout();
            this.gbox_breaking_count.ResumeLayout(false);
            this.gbox_breaking_count.PerformLayout();
            this.panel_breaking_count_clear.ResumeLayout(false);
            this.panel_breaking_count_clear.PerformLayout();
            this.gbox_dummy_tank.ResumeLayout(false);
            this.panel_dummy_tank_set.ResumeLayout(false);
            this.panel_dummy_tank_set.PerformLayout();
            this.panel_dummy_tank_get.ResumeLayout(false);
            this.panel_dummy_tank_get.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_start;
        private System.Windows.Forms.Button btn_pause;
        private System.Windows.Forms.Button btn_stop;
        private System.Windows.Forms.Button btn_keyswitch;
        private System.Windows.Forms.Button btn_pm;
        private System.Windows.Forms.Button btn_dooropen;
        private System.Windows.Forms.Button btn_light;
        private System.Windows.Forms.Label lb_process_connect_info;
        private System.Windows.Forms.Panel panel_cim_connect_info;
        private System.Windows.Forms.Panel panel_ajin_connect_info;
        private System.Windows.Forms.Label lb_cim_connect_info;
        private System.Windows.Forms.Label lb_ajin_connect_info;
        private System.Windows.Forms.Panel panel_umac_connect_info;
        private System.Windows.Forms.Label lb_umac_connect_info;
        private System.Windows.Forms.Panel panel_laser_connect_info;
        private System.Windows.Forms.Panel panel_sequence_connect_info;
        private System.Windows.Forms.Label lb_laser_connect_info;
        private System.Windows.Forms.Panel panel_insp1_connect_info;
        private System.Windows.Forms.Label lb_sequence_connect_info;
        private System.Windows.Forms.Label lb_insp1_connect_info;
        private System.Windows.Forms.Panel panel_serial_connect_info;
        private System.Windows.Forms.Panel panel_insp2_connect_info;
        private System.Windows.Forms.Label lb_serial_connect_info;
        private System.Windows.Forms.Label lb_insp2_connect_info;
        private System.Windows.Forms.Panel panel_runinfo;
        private System.Windows.Forms.TextBox tbox_tact_time;
        private System.Windows.Forms.TextBox tbox_run_count;
        private System.Windows.Forms.TextBox tbox_run_time;
        private System.Windows.Forms.TextBox tbox_start_time;
        private System.Windows.Forms.TextBox tbox_cell_size;
        private System.Windows.Forms.TextBox tbox_process;
        private System.Windows.Forms.TextBox tbox_run_name;
        private System.Windows.Forms.TextBox tbox_recipe_name;
        private System.Windows.Forms.Label lb_tact_time;
        private System.Windows.Forms.Label lb_run_count;
        private System.Windows.Forms.Label lb_run_time;
        private System.Windows.Forms.Label lb_start_time;
        private System.Windows.Forms.Label lb_cell_size;
        private System.Windows.Forms.Label lb_process;
        private System.Windows.Forms.Label lb_recipe_name;
        private System.Windows.Forms.Label lb_run_name;
        private System.Windows.Forms.Label lb_run_info;
        private System.Windows.Forms.GroupBox gbox_laser_info;
        private System.Windows.Forms.Label lb_shutter;
        private System.Windows.Forms.Label lb_shutter_info;
        private System.Windows.Forms.Label lb_power;
        private System.Windows.Forms.Label lb_power_info;
        private System.Windows.Forms.Label lb_divider;
        private System.Windows.Forms.Label lb_divider_info;
        private System.Windows.Forms.Label lb_pd7;
        private System.Windows.Forms.Label lb_pd7power_info;
        private System.Windows.Forms.Label lb_outamplifier;
        private System.Windows.Forms.Label lb_amplifier;
        private System.Windows.Forms.Label lb_burst;
        private System.Windows.Forms.Label lb_pulse_mode;
        private System.Windows.Forms.Label lb_outamplifier_info;
        private System.Windows.Forms.Label lb_amplifier_info;
        private System.Windows.Forms.Label lb_burst_info;
        private System.Windows.Forms.Label lb_pulsemode_info;
        private System.Windows.Forms.GroupBox gbox_mcr_count;
        private System.Windows.Forms.Panel panel_cstinfo;
        private System.Windows.Forms.Label lb_cst_total_count;
        private System.Windows.Forms.Label lb_cst_total_count_info;
        private System.Windows.Forms.Label lb_cst_read_count;
        private System.Windows.Forms.Label lb_cst_read_count_info;
        private System.Windows.Forms.Label lb_cst;
        private System.Windows.Forms.Panel panel_day_info;
        private System.Windows.Forms.Label lb_day_total_count;
        private System.Windows.Forms.Label lb_day_total_count_info;
        private System.Windows.Forms.Label lb_day_read_count;
        private System.Windows.Forms.Label lb_day_read_count_info;
        private System.Windows.Forms.Label lb_day;
        private System.Windows.Forms.GroupBox gbox_uld;
        private System.Windows.Forms.Panel panel_uld_in_muting;
        private System.Windows.Forms.Label lb_uld_in_muting_info;
        private System.Windows.Forms.Panel panel_uld_out_muting;
        private System.Windows.Forms.Label lb_uld_out_muting_info;
        private System.Windows.Forms.GroupBox gbox_ld;
        private System.Windows.Forms.Panel panel_ld_out_muting;
        private System.Windows.Forms.Label lb_ld_out_muting_info;
        private System.Windows.Forms.Panel panel_ld_in_muting;
        private System.Windows.Forms.Label lb_ld_in_muting_info;
        private System.Windows.Forms.Button btn_safety_reset;
        private System.Windows.Forms.Panel panel_run;
        private System.Windows.Forms.Panel panel_ld_muting_out;
        private System.Windows.Forms.Label lb_ld_muting_out;
        private System.Windows.Forms.Panel panel_uld_muting_in;
        private System.Windows.Forms.Label lb_uld_muting_in;
        private System.Windows.Forms.Panel panel_ld_muting_in;
        private System.Windows.Forms.Label lb_ld_muting_in;
        private System.Windows.Forms.Panel panel_uld_muting_out;
        private System.Windows.Forms.Label lb_uld_muting_out;
        private System.Windows.Forms.Panel panel_door2;
        private System.Windows.Forms.Label lb_door2;
        private System.Windows.Forms.Panel panel38;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Panel panel_door3;
        private System.Windows.Forms.Label lb_door3;
        private System.Windows.Forms.Panel panel_door5;
        private System.Windows.Forms.Label lb_door5;
        private System.Windows.Forms.Panel panel_door4;
        private System.Windows.Forms.Label lb_door4;
        private System.Windows.Forms.Panel panel_door6;
        private System.Windows.Forms.Label lb_door6;
        private System.Windows.Forms.Panel panel_door8;
        private System.Windows.Forms.Label lb_door8;
        private System.Windows.Forms.Panel panel_door7;
        private System.Windows.Forms.Label lb_door7;
        private System.Windows.Forms.Panel panel_door9;
        private System.Windows.Forms.Label lb_door9;
        private System.Windows.Forms.Panel panel_b_cst_skip;
        private System.Windows.Forms.Label lb_b_cst_skip;
        private System.Windows.Forms.Panel panel_a_cst_skip;
        private System.Windows.Forms.Label lb_a_cst_skip;
        private System.Windows.Forms.Panel panel_ld_box;
        private System.Windows.Forms.Label lb_ld_box;
        private System.Windows.Forms.Panel panel_ems3;
        private System.Windows.Forms.Label lb_ems3;
        private System.Windows.Forms.Panel panel_process_box;
        private System.Windows.Forms.Label lb_process_box;
        private System.Windows.Forms.Panel panel_ems5;
        private System.Windows.Forms.Label lb_ems5;
        private System.Windows.Forms.Panel panel_uld_box;
        private System.Windows.Forms.Label lb_uld_box;
        private System.Windows.Forms.Panel panel_run2;
        private System.Windows.Forms.Panel panel_ld_light;
        private System.Windows.Forms.Label lb_ld_light;
        private System.Windows.Forms.Panel panel_process_light;
        private System.Windows.Forms.Label lb_process_light;
        private System.Windows.Forms.Panel panel_uld_light;
        private System.Windows.Forms.Label lb_uld_light;
        private System.Windows.Forms.Panel panel_ems1;
        private System.Windows.Forms.Label lb_ems1;
        private System.Windows.Forms.Panel panel_target_avgpower;
        private System.Windows.Forms.Label lb_avgpower;
        private System.Windows.Forms.Label lb_avgpower_info;
        private System.Windows.Forms.Label lb_tartget;
        private System.Windows.Forms.Label lb_target_info;
        private System.Windows.Forms.Panel panel_zpos_gap;
        private System.Windows.Forms.Label lb_zpos_gap;
        private System.Windows.Forms.Label lb_zpos_gap_info;
        private System.Windows.Forms.Panel panel_ems4;
        private System.Windows.Forms.Label lb_ems4;
        private System.Windows.Forms.Panel panel_ld_out_b;
        private System.Windows.Forms.Label lb_ld_out_b;
        private System.Windows.Forms.Panel panel_ld_in_b;
        private System.Windows.Forms.Label lb_ld_in_b;
        private System.Windows.Forms.Panel panel_ld_out_a;
        private System.Windows.Forms.Label lb_ld_out_a;
        private System.Windows.Forms.Panel panel_ld_in_a;
        private System.Windows.Forms.Label lb_ld_in_a;
        private System.Windows.Forms.Panel panel_uld_in_b;
        private System.Windows.Forms.Label lb_uld_in_b;
        private System.Windows.Forms.Panel panel_uld_out_b;
        private System.Windows.Forms.Label lb_uld_out_b;
        private System.Windows.Forms.Panel panel_uld_in_a;
        private System.Windows.Forms.Label lb_uld_in_a;
        private System.Windows.Forms.Panel panel_uld_out_a;
        private System.Windows.Forms.Label lb_uld_out_a;
        private System.Windows.Forms.Panel panel_uld1_out2;
        private System.Windows.Forms.Panel panel_uld2_out2;
        private System.Windows.Forms.Panel panel_uld1_table;
        private System.Windows.Forms.Panel panel_uld2_table;
        private System.Windows.Forms.Panel panel_uld1_out1;
        private System.Windows.Forms.Panel panel_uld2_out1;
        private System.Windows.Forms.Panel panel_uld1;
        private System.Windows.Forms.Label lb_uld1;
        private System.Windows.Forms.Panel panel_uld2;
        private System.Windows.Forms.Label lb_uld2;
        private System.Windows.Forms.Panel panel_uld_trans_a1;
        private System.Windows.Forms.Label lb_uld_trans_a1;
        private System.Windows.Forms.Panel panel_uld_trans_b1;
        private System.Windows.Forms.Label lb_uld_trans_b1;
        private System.Windows.Forms.Panel panel_uld_trans_a2;
        private System.Windows.Forms.Label lb_uld_trans_a2;
        private System.Windows.Forms.Panel panel_uld_trans_b2;
        private System.Windows.Forms.Label lb_uld_trans_b2;
        private System.Windows.Forms.Panel panel_break_table_a1;
        private System.Windows.Forms.Label lb_break_table_a1;
        private System.Windows.Forms.Panel panel_break_table_b1;
        private System.Windows.Forms.Label lb_break_table_b1;
        private System.Windows.Forms.Panel panel_break_trans1;
        private System.Windows.Forms.Label lb_break_trans1;
        private System.Windows.Forms.Panel panel_break_trans2;
        private System.Windows.Forms.Label lb_break_trans2;
        private System.Windows.Forms.Panel panel_break_table_a2;
        private System.Windows.Forms.Label lb_break_table_a2;
        private System.Windows.Forms.Panel panel_break_table_b2;
        private System.Windows.Forms.Label lb_break_table_b2;
        private System.Windows.Forms.Panel panel_process_table_a1;
        private System.Windows.Forms.Label lb_process_table_a1;
        private System.Windows.Forms.Panel panel_process_table_b1;
        private System.Windows.Forms.Label lb_process_table_b1;
        private System.Windows.Forms.Panel panel_process_table_a2;
        private System.Windows.Forms.Label lb_process_table_a2;
        private System.Windows.Forms.Panel panel_ld_trans1;
        private System.Windows.Forms.Label lb_ld_trans1;
        private System.Windows.Forms.Panel panel_process_table_b2;
        private System.Windows.Forms.Label lb_process_table_b2;
        private System.Windows.Forms.Panel panel_ld1;
        private System.Windows.Forms.Label lb_ld1;
        private System.Windows.Forms.Panel panel_ld_trans2;
        private System.Windows.Forms.Label lb_ld_trans2;
        private System.Windows.Forms.Panel panel_ld1_table;
        private System.Windows.Forms.Panel panel_ld2;
        private System.Windows.Forms.Label lb_ld2;
        private System.Windows.Forms.Panel panel_ld1_in2;
        private System.Windows.Forms.Panel panel_ld2_table;
        private System.Windows.Forms.Panel panel_ld1_in1;
        private System.Windows.Forms.Panel panel_ld2_in2;
        private System.Windows.Forms.Panel panel_ld2_in1;
        private System.Windows.Forms.Label lb_ld1_o_time;
        private System.Windows.Forms.Label lb_ld1_o;
        private System.Windows.Forms.Label lb_ld1_i_time;
        private System.Windows.Forms.Label lb_ld1_i;
        private System.Windows.Forms.Label lb_ld2_o_time;
        private System.Windows.Forms.Label lb_ld2_o;
        private System.Windows.Forms.Label lb_ld2_i_time;
        private System.Windows.Forms.Label lb_ld2_i;
        private System.Windows.Forms.Label lb_uld1_i_time;
        private System.Windows.Forms.Label lb_uld1_i;
        private System.Windows.Forms.Label lb_uld1_o_time;
        private System.Windows.Forms.Label lb_uld1_o;
        private System.Windows.Forms.Label lb_uld2_i_time;
        private System.Windows.Forms.Label lb_uld2_i;
        private System.Windows.Forms.Label lb_uld2_o_time;
        private System.Windows.Forms.Label lb_uld2_o;
        private System.Windows.Forms.Panel panel_shutter_open;
        private System.Windows.Forms.Label lb_shutter_open;
        private System.Windows.Forms.Panel panel_lock;
        private System.Windows.Forms.Label lb_lock;
        private System.Windows.Forms.Panel panel_laser_cover;
        private System.Windows.Forms.Label lb_laser_cover;
        private System.Windows.Forms.Panel panel_ems2;
        private System.Windows.Forms.Label lb_ems2;
        private System.Windows.Forms.Panel panel_ems6;
        private System.Windows.Forms.Label lb_ems6;
        private System.Windows.Forms.Panel panel_grab_ems1;
        private System.Windows.Forms.Label lb_grab_ems1;
        private System.Windows.Forms.Panel panel_grab_ems2;
        private System.Windows.Forms.Label lb_grab_ems2;
        private System.Windows.Forms.Panel panel_grab_switch1;
        private System.Windows.Forms.Label lb_grab_switch1;
        private System.Windows.Forms.Panel panel_grab_switch2;
        private System.Windows.Forms.Label lb_grab_switch2;
        private System.Windows.Forms.Panel panel_grab_ems3;
        private System.Windows.Forms.Label lb_grab_ems3;
        private System.Windows.Forms.Panel panel_grab_switch3;
        private System.Windows.Forms.Label lb_grab_switch3;
        private System.Windows.Forms.Panel panel_door10;
        private System.Windows.Forms.Label lb_door10;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Panel panel_camera_l_info;
        private System.Windows.Forms.Label lb_camera_l;
        private System.Windows.Forms.Panel panel_camera_l;
        private System.Windows.Forms.Panel panel_camera_r_info;
        private System.Windows.Forms.Label lb_camera_r;
        private System.Windows.Forms.Panel panel_camera_r;
        private System.Windows.Forms.Panel panel_align;
        private System.Windows.Forms.TextBox tbox_align_break4_offset_a;
        private System.Windows.Forms.TextBox tbox_align_break3_offset_a;
        private System.Windows.Forms.TextBox tbox_align_pre2_offset_a;
        private System.Windows.Forms.Label lb_align_offset_a2;
        private System.Windows.Forms.TextBox tbox_align_break4_offset_y;
        private System.Windows.Forms.TextBox tbox_align_break3_offset_y;
        private System.Windows.Forms.TextBox tbox_align_pre2_offset_y;
        private System.Windows.Forms.Label lb_align_offset_y2;
        private System.Windows.Forms.TextBox tbox_align_break4_offset_x;
        private System.Windows.Forms.TextBox tbox_align_break3_offset_x;
        private System.Windows.Forms.TextBox tbox_align_pre2_offset_x;
        private System.Windows.Forms.Label lb_align_offset_x2;
        private System.Windows.Forms.TextBox tbox_align_break4_result;
        private System.Windows.Forms.TextBox tbox_align_break3_result;
        private System.Windows.Forms.TextBox tbox_align_pre2_result;
        private System.Windows.Forms.Label lb_align_result2;
        private System.Windows.Forms.Label lb_align_break4;
        private System.Windows.Forms.Label lb_align_break3;
        private System.Windows.Forms.Label lb_align_pre2;
        private System.Windows.Forms.Label lb_align_name2;
        private System.Windows.Forms.TextBox tbox_align_break2_offset_a;
        private System.Windows.Forms.TextBox tbox_align_break1_offset_a;
        private System.Windows.Forms.TextBox tbox_align_pre1_offset_a;
        private System.Windows.Forms.Label lb_align_offset_a1;
        private System.Windows.Forms.TextBox tbox_align_break2_offset_y;
        private System.Windows.Forms.TextBox tbox_align_break1_offset_y;
        private System.Windows.Forms.TextBox tbox_align_pre1_offset_y;
        private System.Windows.Forms.Label lb_align_offset_y1;
        private System.Windows.Forms.TextBox tbox_align_break2_offset_x;
        private System.Windows.Forms.TextBox tbox_align_break1_offset_x;
        private System.Windows.Forms.TextBox tbox_align_pre1_offset_x;
        private System.Windows.Forms.Label lb_align_offset_x1;
        private System.Windows.Forms.TextBox tbox_align_break2_result;
        private System.Windows.Forms.TextBox tbox_align_break1_result;
        private System.Windows.Forms.TextBox tbox_align_pre1_result;
        private System.Windows.Forms.Label lb_align_result1;
        private System.Windows.Forms.Label lb_align_break2;
        private System.Windows.Forms.Label lb_align_break1;
        private System.Windows.Forms.Label lb_align_pre1;
        private System.Windows.Forms.Label lb_align_name1;
        private System.Windows.Forms.GroupBox gbox_breaking_count;
        private System.Windows.Forms.Panel panel_breaking_count_clear;
        private System.Windows.Forms.Label lb_breaking_count_clear;
        private System.Windows.Forms.Label lb_breaking_count;
        private System.Windows.Forms.Label lb_breaking_count_info;
        private System.Windows.Forms.GroupBox gbox_dummy_tank;
        private System.Windows.Forms.Panel panel_dummy_tank_set;
        private System.Windows.Forms.Label lb_dummy_tank_set;
        private System.Windows.Forms.Panel panel_dummy_tank_get;
        private System.Windows.Forms.Label lb_dummy_tank_get;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel_lot_a_info;
        private System.Windows.Forms.Label lb_lot_a;
        private System.Windows.Forms.Label lb_lot_a_info;
        private System.Windows.Forms.Panel panel_lot_b_info;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lb_lot_b_info;
        private Main.DetailTactView detailTactView1;
        private Main.CstCellCount cstCellCount3;
        private Main.CstCellCount cstCellCount4;
        private Main.CstCellCount cstCellCount2;
        private Main.CstCellCount cstCellCount1;
        private Main.DetailTactView detailTactView21;
        private Main.DetailTactView detailTactView17;
        private Main.DetailTactView detailTactView16;
        private Main.DetailTactView detailTactView15;
        private Main.DetailTactView detailTactView10;
        private Main.DetailTactView detailTactView9;
        private Main.DetailTactView detailTactView8;
        private Main.DetailTactView detailTactView7;
        private Main.DetailTactView detailTactView6;
        private Main.DetailTactView detailTactView23;
        private Main.DetailTactView detailTactView22;
        private Main.DetailTactView detailTactView20;
        private Main.DetailTactView detailTactView19;
        private Main.DetailTactView detailTactView18;
        private Main.DetailTactView detailTactView14;
        private Main.DetailTactView detailTactView13;
        private Main.DetailTactView detailTactView12;
        private Main.DetailTactView detailTactView5;
        private Main.DetailTactView detailTactView4;
        private Main.DetailTactView detailTactView3;
        private Main.DetailTactView detailTactView11;
        private Main.DetailTactView detailTactView2;
    }
}
