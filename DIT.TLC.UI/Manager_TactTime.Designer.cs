﻿namespace DIT.TLC.UI
{
    partial class Manager_TactTime
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tc_iostatus_info = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.listTactView_Loader_5 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView_Loader_4 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView_Loader_3 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView_Loader_2 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView_Loader_1 = new DIT.TLC.UI.UctrlLstTactView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.listTactView_Process_10 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView_Process_9 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView_Process_8 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView_Process_7 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView_Process_6 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView_Process_5 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView_Process_4 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView_Process_3 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView_Process_2 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView_Process_1 = new DIT.TLC.UI.UctrlLstTactView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.listTactView_Unloader_6 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView_Unloader_5 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView_Unloader_4 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView_Unloader_3 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView_Unloader_2 = new DIT.TLC.UI.UctrlLstTactView();
            this.listTactView_Unloader_1 = new DIT.TLC.UI.UctrlLstTactView();
            this.tc_iostatus_info.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tc_iostatus_info
            // 
            this.tc_iostatus_info.Controls.Add(this.tabPage1);
            this.tc_iostatus_info.Controls.Add(this.tabPage2);
            this.tc_iostatus_info.Controls.Add(this.tabPage3);
            this.tc_iostatus_info.ItemSize = new System.Drawing.Size(579, 30);
            this.tc_iostatus_info.Location = new System.Drawing.Point(4, 0);
            this.tc_iostatus_info.Name = "tc_iostatus_info";
            this.tc_iostatus_info.SelectedIndex = 0;
            this.tc_iostatus_info.Size = new System.Drawing.Size(1740, 875);
            this.tc_iostatus_info.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tc_iostatus_info.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.DimGray;
            this.tabPage1.Controls.Add(this.listTactView_Loader_5);
            this.tabPage1.Controls.Add(this.listTactView_Loader_4);
            this.tabPage1.Controls.Add(this.listTactView_Loader_3);
            this.tabPage1.Controls.Add(this.listTactView_Loader_2);
            this.tabPage1.Controls.Add(this.listTactView_Loader_1);
            this.tabPage1.Location = new System.Drawing.Point(4, 34);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(1732, 837);
            this.tabPage1.TabIndex = 3;
            this.tabPage1.Text = "로더";
            // 
            // listTactView_Loader_5
            // 
            this.listTactView_Loader_5.BackColor = System.Drawing.Color.DimGray;
            this.listTactView_Loader_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView_Loader_5.Location = new System.Drawing.Point(1386, 0);
            this.listTactView_Loader_5.Name = "listTactView_Loader_5";
            this.listTactView_Loader_5.Size = new System.Drawing.Size(343, 420);
            this.listTactView_Loader_5.TabIndex = 4;
            // 
            // listTactView_Loader_4
            // 
            this.listTactView_Loader_4.BackColor = System.Drawing.Color.DimGray;
            this.listTactView_Loader_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView_Loader_4.Location = new System.Drawing.Point(1038, 0);
            this.listTactView_Loader_4.Name = "listTactView_Loader_4";
            this.listTactView_Loader_4.Size = new System.Drawing.Size(343, 420);
            this.listTactView_Loader_4.TabIndex = 3;
            // 
            // listTactView_Loader_3
            // 
            this.listTactView_Loader_3.BackColor = System.Drawing.Color.DimGray;
            this.listTactView_Loader_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView_Loader_3.Location = new System.Drawing.Point(689, 0);
            this.listTactView_Loader_3.Name = "listTactView_Loader_3";
            this.listTactView_Loader_3.Size = new System.Drawing.Size(343, 420);
            this.listTactView_Loader_3.TabIndex = 2;
            // 
            // listTactView_Loader_2
            // 
            this.listTactView_Loader_2.BackColor = System.Drawing.Color.DimGray;
            this.listTactView_Loader_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView_Loader_2.Location = new System.Drawing.Point(340, 0);
            this.listTactView_Loader_2.Name = "listTactView_Loader_2";
            this.listTactView_Loader_2.Size = new System.Drawing.Size(343, 420);
            this.listTactView_Loader_2.TabIndex = 1;
            // 
            // listTactView_Loader_1
            // 
            this.listTactView_Loader_1.BackColor = System.Drawing.Color.DimGray;
            this.listTactView_Loader_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView_Loader_1.Location = new System.Drawing.Point(3, 0);
            this.listTactView_Loader_1.Name = "listTactView_Loader_1";
            this.listTactView_Loader_1.Size = new System.Drawing.Size(343, 420);
            this.listTactView_Loader_1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.DimGray;
            this.tabPage2.Controls.Add(this.listTactView_Process_10);
            this.tabPage2.Controls.Add(this.listTactView_Process_9);
            this.tabPage2.Controls.Add(this.listTactView_Process_8);
            this.tabPage2.Controls.Add(this.listTactView_Process_7);
            this.tabPage2.Controls.Add(this.listTactView_Process_6);
            this.tabPage2.Controls.Add(this.listTactView_Process_5);
            this.tabPage2.Controls.Add(this.listTactView_Process_4);
            this.tabPage2.Controls.Add(this.listTactView_Process_3);
            this.tabPage2.Controls.Add(this.listTactView_Process_2);
            this.tabPage2.Controls.Add(this.listTactView_Process_1);
            this.tabPage2.Location = new System.Drawing.Point(4, 34);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(1732, 837);
            this.tabPage2.TabIndex = 4;
            this.tabPage2.Text = "프로세스";
            // 
            // listTactView_Process_10
            // 
            this.listTactView_Process_10.BackColor = System.Drawing.Color.DimGray;
            this.listTactView_Process_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView_Process_10.Location = new System.Drawing.Point(1387, 412);
            this.listTactView_Process_10.Name = "listTactView_Process_10";
            this.listTactView_Process_10.Size = new System.Drawing.Size(343, 420);
            this.listTactView_Process_10.TabIndex = 14;
            // 
            // listTactView_Process_9
            // 
            this.listTactView_Process_9.BackColor = System.Drawing.Color.DimGray;
            this.listTactView_Process_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView_Process_9.Location = new System.Drawing.Point(1039, 412);
            this.listTactView_Process_9.Name = "listTactView_Process_9";
            this.listTactView_Process_9.Size = new System.Drawing.Size(343, 420);
            this.listTactView_Process_9.TabIndex = 13;
            // 
            // listTactView_Process_8
            // 
            this.listTactView_Process_8.BackColor = System.Drawing.Color.DimGray;
            this.listTactView_Process_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView_Process_8.Location = new System.Drawing.Point(690, 412);
            this.listTactView_Process_8.Name = "listTactView_Process_8";
            this.listTactView_Process_8.Size = new System.Drawing.Size(343, 420);
            this.listTactView_Process_8.TabIndex = 12;
            // 
            // listTactView_Process_7
            // 
            this.listTactView_Process_7.BackColor = System.Drawing.Color.DimGray;
            this.listTactView_Process_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView_Process_7.Location = new System.Drawing.Point(341, 412);
            this.listTactView_Process_7.Name = "listTactView_Process_7";
            this.listTactView_Process_7.Size = new System.Drawing.Size(343, 420);
            this.listTactView_Process_7.TabIndex = 11;
            // 
            // listTactView_Process_6
            // 
            this.listTactView_Process_6.BackColor = System.Drawing.Color.DimGray;
            this.listTactView_Process_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView_Process_6.Location = new System.Drawing.Point(4, 412);
            this.listTactView_Process_6.Name = "listTactView_Process_6";
            this.listTactView_Process_6.Size = new System.Drawing.Size(343, 420);
            this.listTactView_Process_6.TabIndex = 10;
            // 
            // listTactView_Process_5
            // 
            this.listTactView_Process_5.BackColor = System.Drawing.Color.DimGray;
            this.listTactView_Process_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView_Process_5.Location = new System.Drawing.Point(1386, 0);
            this.listTactView_Process_5.Name = "listTactView_Process_5";
            this.listTactView_Process_5.Size = new System.Drawing.Size(343, 420);
            this.listTactView_Process_5.TabIndex = 9;
            // 
            // listTactView_Process_4
            // 
            this.listTactView_Process_4.BackColor = System.Drawing.Color.DimGray;
            this.listTactView_Process_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView_Process_4.Location = new System.Drawing.Point(1038, 0);
            this.listTactView_Process_4.Name = "listTactView_Process_4";
            this.listTactView_Process_4.Size = new System.Drawing.Size(343, 420);
            this.listTactView_Process_4.TabIndex = 8;
            // 
            // listTactView_Process_3
            // 
            this.listTactView_Process_3.BackColor = System.Drawing.Color.DimGray;
            this.listTactView_Process_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView_Process_3.Location = new System.Drawing.Point(690, 0);
            this.listTactView_Process_3.Name = "listTactView_Process_3";
            this.listTactView_Process_3.Size = new System.Drawing.Size(343, 420);
            this.listTactView_Process_3.TabIndex = 7;
            // 
            // listTactView_Process_2
            // 
            this.listTactView_Process_2.BackColor = System.Drawing.Color.DimGray;
            this.listTactView_Process_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView_Process_2.Location = new System.Drawing.Point(341, 0);
            this.listTactView_Process_2.Name = "listTactView_Process_2";
            this.listTactView_Process_2.Size = new System.Drawing.Size(343, 420);
            this.listTactView_Process_2.TabIndex = 6;
            // 
            // listTactView_Process_1
            // 
            this.listTactView_Process_1.BackColor = System.Drawing.Color.DimGray;
            this.listTactView_Process_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView_Process_1.Location = new System.Drawing.Point(0, 0);
            this.listTactView_Process_1.Name = "listTactView_Process_1";
            this.listTactView_Process_1.Size = new System.Drawing.Size(343, 420);
            this.listTactView_Process_1.TabIndex = 5;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.DimGray;
            this.tabPage3.Controls.Add(this.listTactView_Unloader_6);
            this.tabPage3.Controls.Add(this.listTactView_Unloader_5);
            this.tabPage3.Controls.Add(this.listTactView_Unloader_4);
            this.tabPage3.Controls.Add(this.listTactView_Unloader_3);
            this.tabPage3.Controls.Add(this.listTactView_Unloader_2);
            this.tabPage3.Controls.Add(this.listTactView_Unloader_1);
            this.tabPage3.Location = new System.Drawing.Point(4, 34);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1732, 837);
            this.tabPage3.TabIndex = 5;
            this.tabPage3.Text = "언로더";
            // 
            // listTactView_Unloader_6
            // 
            this.listTactView_Unloader_6.BackColor = System.Drawing.Color.DimGray;
            this.listTactView_Unloader_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView_Unloader_6.Location = new System.Drawing.Point(5, 414);
            this.listTactView_Unloader_6.Name = "listTactView_Unloader_6";
            this.listTactView_Unloader_6.Size = new System.Drawing.Size(343, 420);
            this.listTactView_Unloader_6.TabIndex = 16;
            // 
            // listTactView_Unloader_5
            // 
            this.listTactView_Unloader_5.BackColor = System.Drawing.Color.DimGray;
            this.listTactView_Unloader_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView_Unloader_5.Location = new System.Drawing.Point(1387, 2);
            this.listTactView_Unloader_5.Name = "listTactView_Unloader_5";
            this.listTactView_Unloader_5.Size = new System.Drawing.Size(343, 420);
            this.listTactView_Unloader_5.TabIndex = 15;
            // 
            // listTactView_Unloader_4
            // 
            this.listTactView_Unloader_4.BackColor = System.Drawing.Color.DimGray;
            this.listTactView_Unloader_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView_Unloader_4.Location = new System.Drawing.Point(1039, 2);
            this.listTactView_Unloader_4.Name = "listTactView_Unloader_4";
            this.listTactView_Unloader_4.Size = new System.Drawing.Size(343, 420);
            this.listTactView_Unloader_4.TabIndex = 14;
            // 
            // listTactView_Unloader_3
            // 
            this.listTactView_Unloader_3.BackColor = System.Drawing.Color.DimGray;
            this.listTactView_Unloader_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView_Unloader_3.Location = new System.Drawing.Point(691, 2);
            this.listTactView_Unloader_3.Name = "listTactView_Unloader_3";
            this.listTactView_Unloader_3.Size = new System.Drawing.Size(343, 420);
            this.listTactView_Unloader_3.TabIndex = 13;
            // 
            // listTactView_Unloader_2
            // 
            this.listTactView_Unloader_2.BackColor = System.Drawing.Color.DimGray;
            this.listTactView_Unloader_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView_Unloader_2.Location = new System.Drawing.Point(342, 2);
            this.listTactView_Unloader_2.Name = "listTactView_Unloader_2";
            this.listTactView_Unloader_2.Size = new System.Drawing.Size(343, 420);
            this.listTactView_Unloader_2.TabIndex = 12;
            // 
            // listTactView_Unloader_1
            // 
            this.listTactView_Unloader_1.BackColor = System.Drawing.Color.DimGray;
            this.listTactView_Unloader_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listTactView_Unloader_1.Location = new System.Drawing.Point(1, 2);
            this.listTactView_Unloader_1.Name = "listTactView_Unloader_1";
            this.listTactView_Unloader_1.Size = new System.Drawing.Size(343, 420);
            this.listTactView_Unloader_1.TabIndex = 11;
            // 
            // Manager_TactTime
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tc_iostatus_info);
            this.Name = "Manager_TactTime";
            this.Size = new System.Drawing.Size(1740, 860);
            this.tc_iostatus_info.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tc_iostatus_info;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private UctrlLstTactView listTactView_Loader_1;
        private UctrlLstTactView listTactView_Loader_5;
        private UctrlLstTactView listTactView_Loader_4;
        private UctrlLstTactView listTactView_Loader_3;
        private UctrlLstTactView listTactView_Loader_2;
        private UctrlLstTactView listTactView_Process_10;
        private UctrlLstTactView listTactView_Process_9;
        private UctrlLstTactView listTactView_Process_8;
        private UctrlLstTactView listTactView_Process_7;
        private UctrlLstTactView listTactView_Process_6;
        private UctrlLstTactView listTactView_Process_5;
        private UctrlLstTactView listTactView_Process_4;
        private UctrlLstTactView listTactView_Process_3;
        private UctrlLstTactView listTactView_Process_2;
        private UctrlLstTactView listTactView_Process_1;
        private UctrlLstTactView listTactView_Unloader_6;
        private UctrlLstTactView listTactView_Unloader_5;
        private UctrlLstTactView listTactView_Unloader_4;
        private UctrlLstTactView listTactView_Unloader_3;
        private UctrlLstTactView listTactView_Unloader_2;
        private UctrlLstTactView listTactView_Unloader_1;
    }
}
