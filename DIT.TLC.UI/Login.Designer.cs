﻿namespace DIT.TLC.UI
{
    partial class Login
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel_login = new System.Windows.Forms.Panel();
            this.btn_Cancle = new System.Windows.Forms.Button();
            this.btn_OK = new System.Windows.Forms.Button();
            this.tbox_user_password = new System.Windows.Forms.TextBox();
            this.tbox_user_id = new System.Windows.Forms.TextBox();
            this.lb_user_password = new System.Windows.Forms.Label();
            this.lb_user_id = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel_login.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_login
            // 
            this.panel_login.BackColor = System.Drawing.Color.DimGray;
            this.panel_login.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_login.Controls.Add(this.btn_Cancle);
            this.panel_login.Controls.Add(this.btn_OK);
            this.panel_login.Controls.Add(this.tbox_user_password);
            this.panel_login.Controls.Add(this.tbox_user_id);
            this.panel_login.Controls.Add(this.lb_user_password);
            this.panel_login.Controls.Add(this.lb_user_id);
            this.panel_login.ForeColor = System.Drawing.Color.White;
            this.panel_login.Location = new System.Drawing.Point(2, 9);
            this.panel_login.Name = "panel_login";
            this.panel_login.Size = new System.Drawing.Size(600, 130);
            this.panel_login.TabIndex = 18;
            // 
            // btn_Cancle
            // 
            this.btn_Cancle.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_Cancle.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Cancle.Location = new System.Drawing.Point(487, 31);
            this.btn_Cancle.Name = "btn_Cancle";
            this.btn_Cancle.Size = new System.Drawing.Size(67, 67);
            this.btn_Cancle.TabIndex = 5;
            this.btn_Cancle.Text = "취소";
            this.btn_Cancle.UseVisualStyleBackColor = false;
            // 
            // btn_OK
            // 
            this.btn_OK.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_OK.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_OK.Location = new System.Drawing.Point(414, 31);
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.Size = new System.Drawing.Size(67, 67);
            this.btn_OK.TabIndex = 2;
            this.btn_OK.Text = "확인";
            this.btn_OK.UseVisualStyleBackColor = false;
            // 
            // tbox_user_password
            // 
            this.tbox_user_password.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbox_user_password.Location = new System.Drawing.Point(170, 70);
            this.tbox_user_password.Name = "tbox_user_password";
            this.tbox_user_password.PasswordChar = '*';
            this.tbox_user_password.Size = new System.Drawing.Size(235, 29);
            this.tbox_user_password.TabIndex = 4;
            this.tbox_user_password.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbox_user_id
            // 
            this.tbox_user_id.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbox_user_id.Location = new System.Drawing.Point(170, 31);
            this.tbox_user_id.Name = "tbox_user_id";
            this.tbox_user_id.Size = new System.Drawing.Size(235, 29);
            this.tbox_user_id.TabIndex = 3;
            this.tbox_user_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lb_user_password
            // 
            this.lb_user_password.AutoSize = true;
            this.lb_user_password.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_user_password.Location = new System.Drawing.Point(46, 73);
            this.lb_user_password.Name = "lb_user_password";
            this.lb_user_password.Size = new System.Drawing.Size(99, 20);
            this.lb_user_password.TabIndex = 2;
            this.lb_user_password.Text = "사용자 비밀번호";
            // 
            // lb_user_id
            // 
            this.lb_user_id.AutoSize = true;
            this.lb_user_id.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_user_id.Location = new System.Drawing.Point(46, 35);
            this.lb_user_id.Name = "lb_user_id";
            this.lb_user_id.Size = new System.Drawing.Size(87, 20);
            this.lb_user_id.TabIndex = 1;
            this.lb_user_id.Text = "사용자 아이디";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel_login);
            this.groupBox1.Location = new System.Drawing.Point(640, 360);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(604, 142);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.Controls.Add(this.groupBox1);
            this.Name = "Login";
            this.Size = new System.Drawing.Size(1880, 860);
            this.panel_login.ResumeLayout(false);
            this.panel_login.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_login;
        private System.Windows.Forms.TextBox tbox_user_password;
        private System.Windows.Forms.TextBox tbox_user_id;
        private System.Windows.Forms.Label lb_user_password;
        private System.Windows.Forms.Label lb_user_id;
        private System.Windows.Forms.Button btn_Cancle;
        private System.Windows.Forms.Button btn_OK;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}
