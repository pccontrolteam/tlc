﻿namespace DIT.TLC.UI
{
    partial class Parameter_SubMenu
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btn_UserManager = new System.Windows.Forms.Button();
            this.btn_System = new System.Windows.Forms.Button();
            this.btn_Axis = new System.Windows.Forms.Button();
            this.btn_Location = new System.Windows.Forms.Button();
            this.btn_SequenceOffset = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.DarkSlateGray;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.DarkSlateGray;
            this.splitContainer1.Panel2.Controls.Add(this.btn_UserManager);
            this.splitContainer1.Panel2.Controls.Add(this.btn_System);
            this.splitContainer1.Panel2.Controls.Add(this.btn_Axis);
            this.splitContainer1.Panel2.Controls.Add(this.btn_Location);
            this.splitContainer1.Panel2.Controls.Add(this.btn_SequenceOffset);
            this.splitContainer1.Size = new System.Drawing.Size(1880, 860);
            this.splitContainer1.SplitterDistance = 1740;
            this.splitContainer1.TabIndex = 1;
            // 
            // btn_UserManager
            // 
            this.btn_UserManager.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_UserManager.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Bold);
            this.btn_UserManager.ForeColor = System.Drawing.Color.White;
            this.btn_UserManager.Location = new System.Drawing.Point(2, 247);
            this.btn_UserManager.Name = "btn_UserManager";
            this.btn_UserManager.Size = new System.Drawing.Size(132, 50);
            this.btn_UserManager.TabIndex = 44;
            this.btn_UserManager.Text = "사용자 매니저";
            this.btn_UserManager.UseVisualStyleBackColor = false;
            this.btn_UserManager.Click += new System.EventHandler(this.btn_UserManager_Click);
            // 
            // btn_System
            // 
            this.btn_System.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_System.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.btn_System.ForeColor = System.Drawing.Color.White;
            this.btn_System.Location = new System.Drawing.Point(2, 191);
            this.btn_System.Name = "btn_System";
            this.btn_System.Size = new System.Drawing.Size(132, 50);
            this.btn_System.TabIndex = 43;
            this.btn_System.Text = "시스템 파라미터";
            this.btn_System.UseVisualStyleBackColor = false;
            this.btn_System.Click += new System.EventHandler(this.btn_System_Click);
            // 
            // btn_Axis
            // 
            this.btn_Axis.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_Axis.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Bold);
            this.btn_Axis.ForeColor = System.Drawing.Color.White;
            this.btn_Axis.Location = new System.Drawing.Point(2, 135);
            this.btn_Axis.Name = "btn_Axis";
            this.btn_Axis.Size = new System.Drawing.Size(132, 50);
            this.btn_Axis.TabIndex = 42;
            this.btn_Axis.Text = "축 파라미터";
            this.btn_Axis.UseVisualStyleBackColor = false;
            this.btn_Axis.Click += new System.EventHandler(this.btn_Axis_Click);
            // 
            // btn_Location
            // 
            this.btn_Location.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_Location.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Bold);
            this.btn_Location.ForeColor = System.Drawing.Color.White;
            this.btn_Location.Location = new System.Drawing.Point(2, 23);
            this.btn_Location.Name = "btn_Location";
            this.btn_Location.Size = new System.Drawing.Size(132, 50);
            this.btn_Location.TabIndex = 40;
            this.btn_Location.Text = "위치 파라미터";
            this.btn_Location.UseVisualStyleBackColor = false;
            this.btn_Location.Click += new System.EventHandler(this.btn_Location_Click);
            // 
            // btn_SequenceOffset
            // 
            this.btn_SequenceOffset.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_SequenceOffset.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Bold);
            this.btn_SequenceOffset.ForeColor = System.Drawing.Color.White;
            this.btn_SequenceOffset.Location = new System.Drawing.Point(2, 79);
            this.btn_SequenceOffset.Name = "btn_SequenceOffset";
            this.btn_SequenceOffset.Size = new System.Drawing.Size(132, 50);
            this.btn_SequenceOffset.TabIndex = 41;
            this.btn_SequenceOffset.Text = "시퀀스 Offset";
            this.btn_SequenceOffset.UseVisualStyleBackColor = false;
            this.btn_SequenceOffset.Click += new System.EventHandler(this.btn_SequenceOffset_Click);
            // 
            // Parameter_SubMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "Parameter_SubMenu";
            this.Size = new System.Drawing.Size(1880, 860);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btn_UserManager;
        private System.Windows.Forms.Button btn_System;
        private System.Windows.Forms.Button btn_Axis;
        private System.Windows.Forms.Button btn_Location;
        private System.Windows.Forms.Button btn_SequenceOffset;
    }
}
