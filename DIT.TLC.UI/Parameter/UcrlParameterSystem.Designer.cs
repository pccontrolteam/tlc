﻿namespace DIT.TLC.UI
{
    partial class Parameter_System
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_Mode = new System.Windows.Forms.Label();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.gxtMode_DistributionMode = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.btnModeDistributionModeDontUse = new System.Windows.Forms.Button();
            this.txtMode_DistributionMode = new System.Windows.Forms.TextBox();
            this.btnModeDistributionModeUse = new System.Windows.Forms.Button();
            this.gxtSimulationMode = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.btnSimulationModeDontUse = new System.Windows.Forms.Button();
            this.txtSimulationMode = new System.Windows.Forms.TextBox();
            this.btnSimulationModeUse = new System.Windows.Forms.Button();
            this.gxtSkipMode = new System.Windows.Forms.GroupBox();
            this.gxtDoorSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel31 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_DoorSkip_DontUSe = new System.Windows.Forms.Button();
            this.txtDoorSkip = new System.Windows.Forms.TextBox();
            this.btn_DoorSkip_Use = new System.Windows.Forms.Button();
            this.gxtGrabSwitchSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel30 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_GrabSwitchSkip_DontUse = new System.Windows.Forms.Button();
            this.txtGrabSwitchSkip = new System.Windows.Forms.TextBox();
            this.btn_GrabSwitchSkip_Use = new System.Windows.Forms.Button();
            this.gxtMcDownSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel29 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_McDownSkip_DontUse = new System.Windows.Forms.Button();
            this.txtMcDownSkip = new System.Windows.Forms.TextBox();
            this.btn_McDownSkip_Use = new System.Windows.Forms.Button();
            this.gxtTeachSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel28 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_TeachSkip_DontUse = new System.Windows.Forms.Button();
            this.txtTeachSkip = new System.Windows.Forms.TextBox();
            this.btn_TeachSkip_Use = new System.Windows.Forms.Button();
            this.gxtMutingSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel27 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_MutingSkip_DontUse = new System.Windows.Forms.Button();
            this.txtMutingSkip = new System.Windows.Forms.TextBox();
            this.btn_MutingSkip_Use = new System.Windows.Forms.Button();
            this.gxtLaserMeasureSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel26 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_LaserMeasureSkip_DontUse = new System.Windows.Forms.Button();
            this.txtLaserMeasureSkip = new System.Windows.Forms.TextBox();
            this.btn_LaserMeasureSkip_Use = new System.Windows.Forms.Button();
            this.gxtUnloaderInputSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel21 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel24 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_UnloaderInputSkipB_DontUse = new System.Windows.Forms.Button();
            this.txtUnloaderInputSkipB = new System.Windows.Forms.TextBox();
            this.btn_UnloaderInputSkipB_Use = new System.Windows.Forms.Button();
            this.tableLayoutPanel25 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_UnloaderInputSkipA_DontUse = new System.Windows.Forms.Button();
            this.txtUnloaderInputSkipA = new System.Windows.Forms.TextBox();
            this.btn_UnloaderInputSkipA_Use = new System.Windows.Forms.Button();
            this.gxtLoaderInputSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel20 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel23 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_LoaderInputSkipB_DontUse = new System.Windows.Forms.Button();
            this.txtLoaderInputSkipB = new System.Windows.Forms.TextBox();
            this.btn_LoaderInputSkipB_Use = new System.Windows.Forms.Button();
            this.tableLayoutPanel22 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_LoaderInputSkipA_DontUse = new System.Windows.Forms.Button();
            this.txtLoaderInputSkipA = new System.Windows.Forms.TextBox();
            this.btn_LoaderInputSkipA_Use = new System.Windows.Forms.Button();
            this.gxtBufferPickerSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel19 = new System.Windows.Forms.TableLayoutPanel();
            this.btnBufferPickerSkipDontUse = new System.Windows.Forms.Button();
            this.txtBufferPickerSkip = new System.Windows.Forms.TextBox();
            this.btnBufferPickerSkipUse = new System.Windows.Forms.Button();
            this.gxtTiltMeasureSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel18 = new System.Windows.Forms.TableLayoutPanel();
            this.btnTiltMeasureSkipDontUse = new System.Windows.Forms.Button();
            this.txtTiltMeasureSkip = new System.Windows.Forms.TextBox();
            this.btnTiltMeasureSkipUse = new System.Windows.Forms.Button();
            this.gxtBreakingAlignSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel17 = new System.Windows.Forms.TableLayoutPanel();
            this.btnBreakingAlignSkipDontUse = new System.Windows.Forms.Button();
            this.txtBreakingAlignSkip = new System.Windows.Forms.TextBox();
            this.btnBreakingAlignSkipUse = new System.Windows.Forms.Button();
            this.gxtBcrSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.btnBcrSkipDontUse = new System.Windows.Forms.Button();
            this.txtBcrSkip = new System.Windows.Forms.TextBox();
            this.btnBcrSkipUse = new System.Windows.Forms.Button();
            this.gxtMcrSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.btnMcrSkipDontUse = new System.Windows.Forms.Button();
            this.txtMcrSkip = new System.Windows.Forms.TextBox();
            this.btnMcrSkipUse = new System.Windows.Forms.Button();
            this.gxtInspectionSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.btnInspectionSkipDontUse = new System.Windows.Forms.Button();
            this.txtInspectionSkip = new System.Windows.Forms.TextBox();
            this.btninspectionSkipUse = new System.Windows.Forms.Button();
            this.gxtBreakingMotionSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.btnBreakingMotionSkipDontUse = new System.Windows.Forms.Button();
            this.txtBreakingMotionSkip = new System.Windows.Forms.TextBox();
            this.btnBreakingMotionSkipUse = new System.Windows.Forms.Button();
            this.gxtLaserSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.btnLaserSkipDontUse = new System.Windows.Forms.Button();
            this.txtLaserSkip = new System.Windows.Forms.TextBox();
            this.btnLaserSkipUse = new System.Windows.Forms.Button();
            this.gxtBreakingBSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.btnBreakingBSkipDontUse = new System.Windows.Forms.Button();
            this.txtBreakingBSkip = new System.Windows.Forms.TextBox();
            this.btnBreakingBSkipUse = new System.Windows.Forms.Button();
            this.gxtBreakingASkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.btnBreakingASkipDontUse = new System.Windows.Forms.Button();
            this.txtBreakingASkip = new System.Windows.Forms.TextBox();
            this.btnBreakingASkipUse = new System.Windows.Forms.Button();
            this.gxtProcessBSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.btnProcessBSkipDontUse = new System.Windows.Forms.Button();
            this.txtProcessBSkip = new System.Windows.Forms.TextBox();
            this.btnProcessBSkipUse = new System.Windows.Forms.Button();
            this.gxtProcessASkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.btnProcessASkipDontUse = new System.Windows.Forms.Button();
            this.txtProcessASkip = new System.Windows.Forms.TextBox();
            this.btnProcessASkipUse = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel32 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_Setting = new System.Windows.Forms.Label();
            this.tableLayoutPanel33 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel34 = new System.Windows.Forms.TableLayoutPanel();
            this.gxtAutoDeleteCycle = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel40 = new System.Windows.Forms.TableLayoutPanel();
            this.txtAutoDeleteCycle = new System.Windows.Forms.TextBox();
            this.lbl_AutoDeleteCycle = new System.Windows.Forms.Label();
            this.gxtBreakingCount = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel39 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_CriticalAlarm = new System.Windows.Forms.Label();
            this.txtLightAlarm = new System.Windows.Forms.TextBox();
            this.lbl_LightAlarm = new System.Windows.Forms.Label();
            this.txtCriticalAlarm = new System.Windows.Forms.TextBox();
            this.gxtDoorOpenSpeed = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel38 = new System.Windows.Forms.TableLayoutPanel();
            this.txtDoorOpenSpeed = new System.Windows.Forms.TextBox();
            this.lbl_DoorOpenSpped = new System.Windows.Forms.Label();
            this.gxtCountError = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel37 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_INSP = new System.Windows.Forms.Label();
            this.txtMCR = new System.Windows.Forms.TextBox();
            this.lbl_MCR = new System.Windows.Forms.Label();
            this.txtINSP = new System.Windows.Forms.TextBox();
            this.gxtTimeout = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel36 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_MutingTimeout = new System.Windows.Forms.Label();
            this.txtVacuumTimeout = new System.Windows.Forms.TextBox();
            this.lbl_VacuumTimeout = new System.Windows.Forms.Label();
            this.txtCylinderTimeout = new System.Windows.Forms.TextBox();
            this.txtPreAlignTimeout = new System.Windows.Forms.TextBox();
            this.txtBreakingAlignTimeout = new System.Windows.Forms.TextBox();
            this.txtInspectiongTimeout = new System.Windows.Forms.TextBox();
            this.txtSequenceMoveTimeout = new System.Windows.Forms.TextBox();
            this.txtMutingTimeout = new System.Windows.Forms.TextBox();
            this.lbl_CylinderTimeout = new System.Windows.Forms.Label();
            this.lbl_PreAlignTimeout = new System.Windows.Forms.Label();
            this.lbl_BreakingAlignTimeout = new System.Windows.Forms.Label();
            this.lbl_InspectionTimeout = new System.Windows.Forms.Label();
            this.lbl_SequenceMoveTimeout = new System.Windows.Forms.Label();
            this.gxtSetting_DistributionMode = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel35 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_BreakingAlignSpeed = new System.Windows.Forms.Label();
            this.lbl_FineAlignSpeed = new System.Windows.Forms.Label();
            this.txtPreAlignSpeed = new System.Windows.Forms.TextBox();
            this.lbl_PreAlignSpeed = new System.Windows.Forms.Label();
            this.txtFineAlignSpeed = new System.Windows.Forms.TextBox();
            this.txtBreakingAlignSpeed = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel41 = new System.Windows.Forms.TableLayoutPanel();
            this.gxtLDS = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel48 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_ErrorRange = new System.Windows.Forms.Label();
            this.lbl_WaitingTime = new System.Windows.Forms.Label();
            this.txtLDS_MeasureCycle = new System.Windows.Forms.TextBox();
            this.lbl_LDS_MeasureCycle = new System.Windows.Forms.Label();
            this.txtWaitingTime = new System.Windows.Forms.TextBox();
            this.lbl_LDS_MeasureTime = new System.Windows.Forms.Label();
            this.txtLDS_MeasureTime = new System.Windows.Forms.TextBox();
            this.txtErrorRange = new System.Windows.Forms.TextBox();
            this.gxtTemperatureAlarm = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel42 = new System.Windows.Forms.TableLayoutPanel();
            this.txtTemperatureLightAlarm = new System.Windows.Forms.TextBox();
            this.txtTemperatureCriticalAlarm = new System.Windows.Forms.TextBox();
            this.lbl_TemperatureAlarm = new System.Windows.Forms.Label();
            this.gxtMeasureCycle = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel43 = new System.Windows.Forms.TableLayoutPanel();
            this.txtCutLineMeasure = new System.Windows.Forms.TextBox();
            this.lbl_CutLineMeasure = new System.Windows.Forms.Label();
            this.gxtBlowingCheck = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel44 = new System.Windows.Forms.TableLayoutPanel();
            this.txtBlowingCheck = new System.Windows.Forms.TextBox();
            this.lbl_BlowingCheck = new System.Windows.Forms.Label();
            this.gxtRetryCount = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel45 = new System.Windows.Forms.TableLayoutPanel();
            this.txtRetryCount = new System.Windows.Forms.TextBox();
            this.lbl_RetryCount = new System.Windows.Forms.Label();
            this.gxtLaserPD7Error = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel46 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_LaserPD7ErrorRange = new System.Windows.Forms.Label();
            this.txtLaserPD7Power = new System.Windows.Forms.TextBox();
            this.lbl_LaserPD7Power = new System.Windows.Forms.Label();
            this.txtLaserPD7ErrorRange = new System.Windows.Forms.TextBox();
            this.gxtLaserPowerMeasure = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel47 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_TargetErrorRange = new System.Windows.Forms.Label();
            this.txtMeasureCycle = new System.Windows.Forms.TextBox();
            this.lbl_LaserPowerMeasure_MeasureCycle = new System.Windows.Forms.Label();
            this.txtTargetPEC = new System.Windows.Forms.TextBox();
            this.txtTargetPower = new System.Windows.Forms.TextBox();
            this.txtWarmUpTime = new System.Windows.Forms.TextBox();
            this.txtMeasureTime = new System.Windows.Forms.TextBox();
            this.txtCorrectionErrorRange = new System.Windows.Forms.TextBox();
            this.txtTargetErrorRange = new System.Windows.Forms.TextBox();
            this.lbl_TargetPEC = new System.Windows.Forms.Label();
            this.lbl_TargetPower = new System.Windows.Forms.Label();
            this.lbl_WarmUpTime = new System.Windows.Forms.Label();
            this.lbl_MeasureTime = new System.Windows.Forms.Label();
            this.lbl_CorrectionErrorRange = new System.Windows.Forms.Label();
            this.btn_Save = new System.Windows.Forms.Button();
            this.btn_AlarmClear = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.gxtMode_DistributionMode.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.gxtSimulationMode.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.gxtSkipMode.SuspendLayout();
            this.gxtDoorSkip.SuspendLayout();
            this.tableLayoutPanel31.SuspendLayout();
            this.gxtGrabSwitchSkip.SuspendLayout();
            this.tableLayoutPanel30.SuspendLayout();
            this.gxtMcDownSkip.SuspendLayout();
            this.tableLayoutPanel29.SuspendLayout();
            this.gxtTeachSkip.SuspendLayout();
            this.tableLayoutPanel28.SuspendLayout();
            this.gxtMutingSkip.SuspendLayout();
            this.tableLayoutPanel27.SuspendLayout();
            this.gxtLaserMeasureSkip.SuspendLayout();
            this.tableLayoutPanel26.SuspendLayout();
            this.gxtUnloaderInputSkip.SuspendLayout();
            this.tableLayoutPanel21.SuspendLayout();
            this.tableLayoutPanel24.SuspendLayout();
            this.tableLayoutPanel25.SuspendLayout();
            this.gxtLoaderInputSkip.SuspendLayout();
            this.tableLayoutPanel20.SuspendLayout();
            this.tableLayoutPanel23.SuspendLayout();
            this.tableLayoutPanel22.SuspendLayout();
            this.gxtBufferPickerSkip.SuspendLayout();
            this.tableLayoutPanel19.SuspendLayout();
            this.gxtTiltMeasureSkip.SuspendLayout();
            this.tableLayoutPanel18.SuspendLayout();
            this.gxtBreakingAlignSkip.SuspendLayout();
            this.tableLayoutPanel17.SuspendLayout();
            this.gxtBcrSkip.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            this.gxtMcrSkip.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.gxtInspectionSkip.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.gxtBreakingMotionSkip.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.gxtLaserSkip.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.gxtBreakingBSkip.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.gxtBreakingASkip.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.gxtProcessBSkip.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.gxtProcessASkip.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel32.SuspendLayout();
            this.tableLayoutPanel33.SuspendLayout();
            this.tableLayoutPanel34.SuspendLayout();
            this.gxtAutoDeleteCycle.SuspendLayout();
            this.tableLayoutPanel40.SuspendLayout();
            this.gxtBreakingCount.SuspendLayout();
            this.tableLayoutPanel39.SuspendLayout();
            this.gxtDoorOpenSpeed.SuspendLayout();
            this.tableLayoutPanel38.SuspendLayout();
            this.gxtCountError.SuspendLayout();
            this.tableLayoutPanel37.SuspendLayout();
            this.gxtTimeout.SuspendLayout();
            this.tableLayoutPanel36.SuspendLayout();
            this.gxtSetting_DistributionMode.SuspendLayout();
            this.tableLayoutPanel35.SuspendLayout();
            this.tableLayoutPanel41.SuspendLayout();
            this.gxtLDS.SuspendLayout();
            this.tableLayoutPanel48.SuspendLayout();
            this.gxtTemperatureAlarm.SuspendLayout();
            this.tableLayoutPanel42.SuspendLayout();
            this.gxtMeasureCycle.SuspendLayout();
            this.tableLayoutPanel43.SuspendLayout();
            this.gxtBlowingCheck.SuspendLayout();
            this.tableLayoutPanel44.SuspendLayout();
            this.gxtRetryCount.SuspendLayout();
            this.tableLayoutPanel45.SuspendLayout();
            this.gxtLaserPD7Error.SuspendLayout();
            this.tableLayoutPanel46.SuspendLayout();
            this.gxtLaserPowerMeasure.SuspendLayout();
            this.tableLayoutPanel47.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 0, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.513872F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 93.48613F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(865, 821);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.SystemColors.Highlight;
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.5F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.01746F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.49127F));
            this.tableLayoutPanel3.Controls.Add(this.lbl_Mode, 1, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(859, 47);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // lbl_Mode
            // 
            this.lbl_Mode.AutoSize = true;
            this.lbl_Mode.Font = new System.Drawing.Font("맑은 고딕", 14F, System.Drawing.FontStyle.Bold);
            this.lbl_Mode.Location = new System.Drawing.Point(400, 10);
            this.lbl_Mode.Margin = new System.Windows.Forms.Padding(35, 10, 3, 0);
            this.lbl_Mode.Name = "lbl_Mode";
            this.lbl_Mode.Size = new System.Drawing.Size(64, 25);
            this.lbl_Mode.TabIndex = 1;
            this.lbl_Mode.Text = "Mode";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel5, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.gxtSkipMode, 0, 1);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 56);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.259259F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90.74074F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(859, 762);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.gxtMode_DistributionMode, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.gxtSimulationMode, 0, 0);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(853, 64);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // gxtMode_DistributionMode
            // 
            this.gxtMode_DistributionMode.Controls.Add(this.tableLayoutPanel7);
            this.gxtMode_DistributionMode.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtMode_DistributionMode.ForeColor = System.Drawing.Color.White;
            this.gxtMode_DistributionMode.Location = new System.Drawing.Point(429, 3);
            this.gxtMode_DistributionMode.Name = "gxtMode_DistributionMode";
            this.gxtMode_DistributionMode.Size = new System.Drawing.Size(420, 58);
            this.gxtMode_DistributionMode.TabIndex = 53;
            this.gxtMode_DistributionMode.TabStop = false;
            this.gxtMode_DistributionMode.Text = "     물류 모드    ";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 3;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel7.Controls.Add(this.btnModeDistributionModeDontUse, 2, 0);
            this.tableLayoutPanel7.Controls.Add(this.txtMode_DistributionMode, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.btnModeDistributionModeUse, 1, 0);
            this.tableLayoutPanel7.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(408, 38);
            this.tableLayoutPanel7.TabIndex = 1;
            // 
            // btnModeDistributionModeDontUse
            // 
            this.btnModeDistributionModeDontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnModeDistributionModeDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnModeDistributionModeDontUse.ForeColor = System.Drawing.Color.White;
            this.btnModeDistributionModeDontUse.Location = new System.Drawing.Point(298, 3);
            this.btnModeDistributionModeDontUse.Name = "btnModeDistributionModeDontUse";
            this.btnModeDistributionModeDontUse.Size = new System.Drawing.Size(106, 32);
            this.btnModeDistributionModeDontUse.TabIndex = 47;
            this.btnModeDistributionModeDontUse.Text = "사용안함";
            this.btnModeDistributionModeDontUse.UseVisualStyleBackColor = false;
            // 
            // txtMode_DistributionMode
            // 
            this.txtMode_DistributionMode.Location = new System.Drawing.Point(3, 3);
            this.txtMode_DistributionMode.Name = "txtMode_DistributionMode";
            this.txtMode_DistributionMode.Size = new System.Drawing.Size(177, 29);
            this.txtMode_DistributionMode.TabIndex = 0;
            // 
            // btnModeDistributionModeUse
            // 
            this.btnModeDistributionModeUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnModeDistributionModeUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnModeDistributionModeUse.ForeColor = System.Drawing.Color.White;
            this.btnModeDistributionModeUse.Location = new System.Drawing.Point(186, 3);
            this.btnModeDistributionModeUse.Name = "btnModeDistributionModeUse";
            this.btnModeDistributionModeUse.Size = new System.Drawing.Size(106, 32);
            this.btnModeDistributionModeUse.TabIndex = 46;
            this.btnModeDistributionModeUse.Text = "사용함";
            this.btnModeDistributionModeUse.UseVisualStyleBackColor = false;
            // 
            // gxtSimulationMode
            // 
            this.gxtSimulationMode.Controls.Add(this.tableLayoutPanel6);
            this.gxtSimulationMode.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtSimulationMode.ForeColor = System.Drawing.Color.White;
            this.gxtSimulationMode.Location = new System.Drawing.Point(3, 3);
            this.gxtSimulationMode.Name = "gxtSimulationMode";
            this.gxtSimulationMode.Size = new System.Drawing.Size(420, 58);
            this.gxtSimulationMode.TabIndex = 52;
            this.gxtSimulationMode.TabStop = false;
            this.gxtSimulationMode.Text = "     시뮬레이션 모드    ";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 3;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel6.Controls.Add(this.btnSimulationModeDontUse, 2, 0);
            this.tableLayoutPanel6.Controls.Add(this.txtSimulationMode, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.btnSimulationModeUse, 1, 0);
            this.tableLayoutPanel6.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(408, 38);
            this.tableLayoutPanel6.TabIndex = 1;
            // 
            // btnSimulationModeDontUse
            // 
            this.btnSimulationModeDontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnSimulationModeDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnSimulationModeDontUse.ForeColor = System.Drawing.Color.White;
            this.btnSimulationModeDontUse.Location = new System.Drawing.Point(298, 3);
            this.btnSimulationModeDontUse.Name = "btnSimulationModeDontUse";
            this.btnSimulationModeDontUse.Size = new System.Drawing.Size(106, 32);
            this.btnSimulationModeDontUse.TabIndex = 47;
            this.btnSimulationModeDontUse.Text = "사용안함";
            this.btnSimulationModeDontUse.UseVisualStyleBackColor = false;
            // 
            // txtSimulationMode
            // 
            this.txtSimulationMode.Location = new System.Drawing.Point(3, 3);
            this.txtSimulationMode.Name = "txtSimulationMode";
            this.txtSimulationMode.Size = new System.Drawing.Size(177, 29);
            this.txtSimulationMode.TabIndex = 0;
            // 
            // btnSimulationModeUse
            // 
            this.btnSimulationModeUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnSimulationModeUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnSimulationModeUse.ForeColor = System.Drawing.Color.White;
            this.btnSimulationModeUse.Location = new System.Drawing.Point(186, 3);
            this.btnSimulationModeUse.Name = "btnSimulationModeUse";
            this.btnSimulationModeUse.Size = new System.Drawing.Size(106, 32);
            this.btnSimulationModeUse.TabIndex = 46;
            this.btnSimulationModeUse.Text = "사용함";
            this.btnSimulationModeUse.UseVisualStyleBackColor = false;
            // 
            // gxtSkipMode
            // 
            this.gxtSkipMode.Controls.Add(this.gxtDoorSkip);
            this.gxtSkipMode.Controls.Add(this.gxtGrabSwitchSkip);
            this.gxtSkipMode.Controls.Add(this.gxtMcDownSkip);
            this.gxtSkipMode.Controls.Add(this.gxtTeachSkip);
            this.gxtSkipMode.Controls.Add(this.gxtMutingSkip);
            this.gxtSkipMode.Controls.Add(this.gxtLaserMeasureSkip);
            this.gxtSkipMode.Controls.Add(this.gxtUnloaderInputSkip);
            this.gxtSkipMode.Controls.Add(this.gxtLoaderInputSkip);
            this.gxtSkipMode.Controls.Add(this.gxtBufferPickerSkip);
            this.gxtSkipMode.Controls.Add(this.gxtTiltMeasureSkip);
            this.gxtSkipMode.Controls.Add(this.gxtBreakingAlignSkip);
            this.gxtSkipMode.Controls.Add(this.gxtBcrSkip);
            this.gxtSkipMode.Controls.Add(this.gxtMcrSkip);
            this.gxtSkipMode.Controls.Add(this.gxtInspectionSkip);
            this.gxtSkipMode.Controls.Add(this.gxtBreakingMotionSkip);
            this.gxtSkipMode.Controls.Add(this.gxtLaserSkip);
            this.gxtSkipMode.Controls.Add(this.gxtBreakingBSkip);
            this.gxtSkipMode.Controls.Add(this.gxtBreakingASkip);
            this.gxtSkipMode.Controls.Add(this.gxtProcessBSkip);
            this.gxtSkipMode.Controls.Add(this.gxtProcessASkip);
            this.gxtSkipMode.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtSkipMode.ForeColor = System.Drawing.Color.White;
            this.gxtSkipMode.Location = new System.Drawing.Point(3, 73);
            this.gxtSkipMode.Name = "gxtSkipMode";
            this.gxtSkipMode.Size = new System.Drawing.Size(849, 686);
            this.gxtSkipMode.TabIndex = 53;
            this.gxtSkipMode.TabStop = false;
            this.gxtSkipMode.Text = "     스킵 모드    ";
            // 
            // gxtDoorSkip
            // 
            this.gxtDoorSkip.Controls.Add(this.tableLayoutPanel31);
            this.gxtDoorSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtDoorSkip.ForeColor = System.Drawing.Color.White;
            this.gxtDoorSkip.Location = new System.Drawing.Point(428, 552);
            this.gxtDoorSkip.Name = "gxtDoorSkip";
            this.gxtDoorSkip.Size = new System.Drawing.Size(419, 57);
            this.gxtDoorSkip.TabIndex = 56;
            this.gxtDoorSkip.TabStop = false;
            this.gxtDoorSkip.Text = "     도어 스킵     ";
            // 
            // tableLayoutPanel31
            // 
            this.tableLayoutPanel31.ColumnCount = 3;
            this.tableLayoutPanel31.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel31.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel31.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel31.Controls.Add(this.btn_DoorSkip_DontUSe, 2, 0);
            this.tableLayoutPanel31.Controls.Add(this.txtDoorSkip, 0, 0);
            this.tableLayoutPanel31.Controls.Add(this.btn_DoorSkip_Use, 1, 0);
            this.tableLayoutPanel31.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel31.Name = "tableLayoutPanel31";
            this.tableLayoutPanel31.RowCount = 1;
            this.tableLayoutPanel31.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel31.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel31.TabIndex = 1;
            // 
            // btn_DoorSkip_DontUSe
            // 
            this.btn_DoorSkip_DontUSe.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_DoorSkip_DontUSe.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_DoorSkip_DontUSe.ForeColor = System.Drawing.Color.White;
            this.btn_DoorSkip_DontUSe.Location = new System.Drawing.Point(298, 3);
            this.btn_DoorSkip_DontUSe.Name = "btn_DoorSkip_DontUSe";
            this.btn_DoorSkip_DontUSe.Size = new System.Drawing.Size(106, 26);
            this.btn_DoorSkip_DontUSe.TabIndex = 47;
            this.btn_DoorSkip_DontUSe.Text = "사용안함";
            this.btn_DoorSkip_DontUSe.UseVisualStyleBackColor = false;
            // 
            // txtDoorSkip
            // 
            this.txtDoorSkip.Location = new System.Drawing.Point(3, 3);
            this.txtDoorSkip.Name = "txtDoorSkip";
            this.txtDoorSkip.Size = new System.Drawing.Size(177, 29);
            this.txtDoorSkip.TabIndex = 0;
            // 
            // btn_DoorSkip_Use
            // 
            this.btn_DoorSkip_Use.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_DoorSkip_Use.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_DoorSkip_Use.ForeColor = System.Drawing.Color.White;
            this.btn_DoorSkip_Use.Location = new System.Drawing.Point(186, 3);
            this.btn_DoorSkip_Use.Name = "btn_DoorSkip_Use";
            this.btn_DoorSkip_Use.Size = new System.Drawing.Size(106, 26);
            this.btn_DoorSkip_Use.TabIndex = 46;
            this.btn_DoorSkip_Use.Text = "사용함";
            this.btn_DoorSkip_Use.UseVisualStyleBackColor = false;
            // 
            // gxtGrabSwitchSkip
            // 
            this.gxtGrabSwitchSkip.Controls.Add(this.tableLayoutPanel30);
            this.gxtGrabSwitchSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtGrabSwitchSkip.ForeColor = System.Drawing.Color.White;
            this.gxtGrabSwitchSkip.Location = new System.Drawing.Point(428, 496);
            this.gxtGrabSwitchSkip.Name = "gxtGrabSwitchSkip";
            this.gxtGrabSwitchSkip.Size = new System.Drawing.Size(419, 57);
            this.gxtGrabSwitchSkip.TabIndex = 64;
            this.gxtGrabSwitchSkip.TabStop = false;
            this.gxtGrabSwitchSkip.Text = "     그랩 스위치 스킵     ";
            // 
            // tableLayoutPanel30
            // 
            this.tableLayoutPanel30.ColumnCount = 3;
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel30.Controls.Add(this.btn_GrabSwitchSkip_DontUse, 2, 0);
            this.tableLayoutPanel30.Controls.Add(this.txtGrabSwitchSkip, 0, 0);
            this.tableLayoutPanel30.Controls.Add(this.btn_GrabSwitchSkip_Use, 1, 0);
            this.tableLayoutPanel30.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel30.Name = "tableLayoutPanel30";
            this.tableLayoutPanel30.RowCount = 1;
            this.tableLayoutPanel30.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel30.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel30.TabIndex = 1;
            // 
            // btn_GrabSwitchSkip_DontUse
            // 
            this.btn_GrabSwitchSkip_DontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_GrabSwitchSkip_DontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_GrabSwitchSkip_DontUse.ForeColor = System.Drawing.Color.White;
            this.btn_GrabSwitchSkip_DontUse.Location = new System.Drawing.Point(298, 3);
            this.btn_GrabSwitchSkip_DontUse.Name = "btn_GrabSwitchSkip_DontUse";
            this.btn_GrabSwitchSkip_DontUse.Size = new System.Drawing.Size(106, 26);
            this.btn_GrabSwitchSkip_DontUse.TabIndex = 47;
            this.btn_GrabSwitchSkip_DontUse.Text = "사용안함";
            this.btn_GrabSwitchSkip_DontUse.UseVisualStyleBackColor = false;
            // 
            // txtGrabSwitchSkip
            // 
            this.txtGrabSwitchSkip.Location = new System.Drawing.Point(3, 3);
            this.txtGrabSwitchSkip.Name = "txtGrabSwitchSkip";
            this.txtGrabSwitchSkip.Size = new System.Drawing.Size(177, 29);
            this.txtGrabSwitchSkip.TabIndex = 0;
            // 
            // btn_GrabSwitchSkip_Use
            // 
            this.btn_GrabSwitchSkip_Use.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_GrabSwitchSkip_Use.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_GrabSwitchSkip_Use.ForeColor = System.Drawing.Color.White;
            this.btn_GrabSwitchSkip_Use.Location = new System.Drawing.Point(186, 3);
            this.btn_GrabSwitchSkip_Use.Name = "btn_GrabSwitchSkip_Use";
            this.btn_GrabSwitchSkip_Use.Size = new System.Drawing.Size(106, 26);
            this.btn_GrabSwitchSkip_Use.TabIndex = 46;
            this.btn_GrabSwitchSkip_Use.Text = "사용함";
            this.btn_GrabSwitchSkip_Use.UseVisualStyleBackColor = false;
            // 
            // gxtMcDownSkip
            // 
            this.gxtMcDownSkip.Controls.Add(this.tableLayoutPanel29);
            this.gxtMcDownSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtMcDownSkip.ForeColor = System.Drawing.Color.White;
            this.gxtMcDownSkip.Location = new System.Drawing.Point(4, 608);
            this.gxtMcDownSkip.Name = "gxtMcDownSkip";
            this.gxtMcDownSkip.Size = new System.Drawing.Size(419, 57);
            this.gxtMcDownSkip.TabIndex = 63;
            this.gxtMcDownSkip.TabStop = false;
            this.gxtMcDownSkip.Text = "     MC 다운 스킵     ";
            // 
            // tableLayoutPanel29
            // 
            this.tableLayoutPanel29.ColumnCount = 3;
            this.tableLayoutPanel29.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel29.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel29.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel29.Controls.Add(this.btn_McDownSkip_DontUse, 2, 0);
            this.tableLayoutPanel29.Controls.Add(this.txtMcDownSkip, 0, 0);
            this.tableLayoutPanel29.Controls.Add(this.btn_McDownSkip_Use, 1, 0);
            this.tableLayoutPanel29.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel29.Name = "tableLayoutPanel29";
            this.tableLayoutPanel29.RowCount = 1;
            this.tableLayoutPanel29.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel29.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel29.TabIndex = 1;
            // 
            // btn_McDownSkip_DontUse
            // 
            this.btn_McDownSkip_DontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_McDownSkip_DontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_McDownSkip_DontUse.ForeColor = System.Drawing.Color.White;
            this.btn_McDownSkip_DontUse.Location = new System.Drawing.Point(298, 3);
            this.btn_McDownSkip_DontUse.Name = "btn_McDownSkip_DontUse";
            this.btn_McDownSkip_DontUse.Size = new System.Drawing.Size(106, 26);
            this.btn_McDownSkip_DontUse.TabIndex = 47;
            this.btn_McDownSkip_DontUse.Text = "사용안함";
            this.btn_McDownSkip_DontUse.UseVisualStyleBackColor = false;
            // 
            // txtMcDownSkip
            // 
            this.txtMcDownSkip.Location = new System.Drawing.Point(3, 3);
            this.txtMcDownSkip.Name = "txtMcDownSkip";
            this.txtMcDownSkip.Size = new System.Drawing.Size(177, 29);
            this.txtMcDownSkip.TabIndex = 0;
            // 
            // btn_McDownSkip_Use
            // 
            this.btn_McDownSkip_Use.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_McDownSkip_Use.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_McDownSkip_Use.ForeColor = System.Drawing.Color.White;
            this.btn_McDownSkip_Use.Location = new System.Drawing.Point(186, 3);
            this.btn_McDownSkip_Use.Name = "btn_McDownSkip_Use";
            this.btn_McDownSkip_Use.Size = new System.Drawing.Size(106, 26);
            this.btn_McDownSkip_Use.TabIndex = 46;
            this.btn_McDownSkip_Use.Text = "사용함";
            this.btn_McDownSkip_Use.UseVisualStyleBackColor = false;
            // 
            // gxtTeachSkip
            // 
            this.gxtTeachSkip.Controls.Add(this.tableLayoutPanel28);
            this.gxtTeachSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtTeachSkip.ForeColor = System.Drawing.Color.White;
            this.gxtTeachSkip.Location = new System.Drawing.Point(3, 552);
            this.gxtTeachSkip.Name = "gxtTeachSkip";
            this.gxtTeachSkip.Size = new System.Drawing.Size(419, 57);
            this.gxtTeachSkip.TabIndex = 55;
            this.gxtTeachSkip.TabStop = false;
            this.gxtTeachSkip.Text = "     티치 스킵     ";
            // 
            // tableLayoutPanel28
            // 
            this.tableLayoutPanel28.ColumnCount = 3;
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel28.Controls.Add(this.btn_TeachSkip_DontUse, 2, 0);
            this.tableLayoutPanel28.Controls.Add(this.txtTeachSkip, 0, 0);
            this.tableLayoutPanel28.Controls.Add(this.btn_TeachSkip_Use, 1, 0);
            this.tableLayoutPanel28.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel28.Name = "tableLayoutPanel28";
            this.tableLayoutPanel28.RowCount = 1;
            this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel28.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel28.TabIndex = 1;
            // 
            // btn_TeachSkip_DontUse
            // 
            this.btn_TeachSkip_DontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_TeachSkip_DontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_TeachSkip_DontUse.ForeColor = System.Drawing.Color.White;
            this.btn_TeachSkip_DontUse.Location = new System.Drawing.Point(298, 3);
            this.btn_TeachSkip_DontUse.Name = "btn_TeachSkip_DontUse";
            this.btn_TeachSkip_DontUse.Size = new System.Drawing.Size(106, 26);
            this.btn_TeachSkip_DontUse.TabIndex = 47;
            this.btn_TeachSkip_DontUse.Text = "사용안함";
            this.btn_TeachSkip_DontUse.UseVisualStyleBackColor = false;
            // 
            // txtTeachSkip
            // 
            this.txtTeachSkip.Location = new System.Drawing.Point(3, 3);
            this.txtTeachSkip.Name = "txtTeachSkip";
            this.txtTeachSkip.Size = new System.Drawing.Size(177, 29);
            this.txtTeachSkip.TabIndex = 0;
            // 
            // btn_TeachSkip_Use
            // 
            this.btn_TeachSkip_Use.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_TeachSkip_Use.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_TeachSkip_Use.ForeColor = System.Drawing.Color.White;
            this.btn_TeachSkip_Use.Location = new System.Drawing.Point(186, 3);
            this.btn_TeachSkip_Use.Name = "btn_TeachSkip_Use";
            this.btn_TeachSkip_Use.Size = new System.Drawing.Size(106, 26);
            this.btn_TeachSkip_Use.TabIndex = 46;
            this.btn_TeachSkip_Use.Text = "사용함";
            this.btn_TeachSkip_Use.UseVisualStyleBackColor = false;
            // 
            // gxtMutingSkip
            // 
            this.gxtMutingSkip.Controls.Add(this.tableLayoutPanel27);
            this.gxtMutingSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtMutingSkip.ForeColor = System.Drawing.Color.White;
            this.gxtMutingSkip.Location = new System.Drawing.Point(3, 496);
            this.gxtMutingSkip.Name = "gxtMutingSkip";
            this.gxtMutingSkip.Size = new System.Drawing.Size(419, 57);
            this.gxtMutingSkip.TabIndex = 62;
            this.gxtMutingSkip.TabStop = false;
            this.gxtMutingSkip.Text = "     뮤팅 스킵     ";
            // 
            // tableLayoutPanel27
            // 
            this.tableLayoutPanel27.ColumnCount = 3;
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel27.Controls.Add(this.btn_MutingSkip_DontUse, 2, 0);
            this.tableLayoutPanel27.Controls.Add(this.txtMutingSkip, 0, 0);
            this.tableLayoutPanel27.Controls.Add(this.btn_MutingSkip_Use, 1, 0);
            this.tableLayoutPanel27.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel27.Name = "tableLayoutPanel27";
            this.tableLayoutPanel27.RowCount = 1;
            this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel27.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel27.TabIndex = 1;
            // 
            // btn_MutingSkip_DontUse
            // 
            this.btn_MutingSkip_DontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_MutingSkip_DontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_MutingSkip_DontUse.ForeColor = System.Drawing.Color.White;
            this.btn_MutingSkip_DontUse.Location = new System.Drawing.Point(298, 3);
            this.btn_MutingSkip_DontUse.Name = "btn_MutingSkip_DontUse";
            this.btn_MutingSkip_DontUse.Size = new System.Drawing.Size(106, 26);
            this.btn_MutingSkip_DontUse.TabIndex = 47;
            this.btn_MutingSkip_DontUse.Text = "사용안함";
            this.btn_MutingSkip_DontUse.UseVisualStyleBackColor = false;
            // 
            // txtMutingSkip
            // 
            this.txtMutingSkip.Location = new System.Drawing.Point(3, 3);
            this.txtMutingSkip.Name = "txtMutingSkip";
            this.txtMutingSkip.Size = new System.Drawing.Size(177, 29);
            this.txtMutingSkip.TabIndex = 0;
            // 
            // btn_MutingSkip_Use
            // 
            this.btn_MutingSkip_Use.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_MutingSkip_Use.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_MutingSkip_Use.ForeColor = System.Drawing.Color.White;
            this.btn_MutingSkip_Use.Location = new System.Drawing.Point(186, 3);
            this.btn_MutingSkip_Use.Name = "btn_MutingSkip_Use";
            this.btn_MutingSkip_Use.Size = new System.Drawing.Size(106, 26);
            this.btn_MutingSkip_Use.TabIndex = 46;
            this.btn_MutingSkip_Use.Text = "사용함";
            this.btn_MutingSkip_Use.UseVisualStyleBackColor = false;
            // 
            // gxtLaserMeasureSkip
            // 
            this.gxtLaserMeasureSkip.Controls.Add(this.tableLayoutPanel26);
            this.gxtLaserMeasureSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtLaserMeasureSkip.ForeColor = System.Drawing.Color.White;
            this.gxtLaserMeasureSkip.Location = new System.Drawing.Point(3, 421);
            this.gxtLaserMeasureSkip.Name = "gxtLaserMeasureSkip";
            this.gxtLaserMeasureSkip.Size = new System.Drawing.Size(419, 57);
            this.gxtLaserMeasureSkip.TabIndex = 61;
            this.gxtLaserMeasureSkip.TabStop = false;
            this.gxtLaserMeasureSkip.Text = "     레이져 측정 스킵     ";
            // 
            // tableLayoutPanel26
            // 
            this.tableLayoutPanel26.ColumnCount = 3;
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel26.Controls.Add(this.btn_LaserMeasureSkip_DontUse, 2, 0);
            this.tableLayoutPanel26.Controls.Add(this.txtLaserMeasureSkip, 0, 0);
            this.tableLayoutPanel26.Controls.Add(this.btn_LaserMeasureSkip_Use, 1, 0);
            this.tableLayoutPanel26.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel26.Name = "tableLayoutPanel26";
            this.tableLayoutPanel26.RowCount = 1;
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel26.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel26.TabIndex = 1;
            // 
            // btn_LaserMeasureSkip_DontUse
            // 
            this.btn_LaserMeasureSkip_DontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_LaserMeasureSkip_DontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_LaserMeasureSkip_DontUse.ForeColor = System.Drawing.Color.White;
            this.btn_LaserMeasureSkip_DontUse.Location = new System.Drawing.Point(298, 3);
            this.btn_LaserMeasureSkip_DontUse.Name = "btn_LaserMeasureSkip_DontUse";
            this.btn_LaserMeasureSkip_DontUse.Size = new System.Drawing.Size(106, 26);
            this.btn_LaserMeasureSkip_DontUse.TabIndex = 47;
            this.btn_LaserMeasureSkip_DontUse.Text = "사용안함";
            this.btn_LaserMeasureSkip_DontUse.UseVisualStyleBackColor = false;
            // 
            // txtLaserMeasureSkip
            // 
            this.txtLaserMeasureSkip.Location = new System.Drawing.Point(3, 3);
            this.txtLaserMeasureSkip.Name = "txtLaserMeasureSkip";
            this.txtLaserMeasureSkip.Size = new System.Drawing.Size(177, 29);
            this.txtLaserMeasureSkip.TabIndex = 0;
            // 
            // btn_LaserMeasureSkip_Use
            // 
            this.btn_LaserMeasureSkip_Use.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_LaserMeasureSkip_Use.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_LaserMeasureSkip_Use.ForeColor = System.Drawing.Color.White;
            this.btn_LaserMeasureSkip_Use.Location = new System.Drawing.Point(186, 3);
            this.btn_LaserMeasureSkip_Use.Name = "btn_LaserMeasureSkip_Use";
            this.btn_LaserMeasureSkip_Use.Size = new System.Drawing.Size(106, 26);
            this.btn_LaserMeasureSkip_Use.TabIndex = 46;
            this.btn_LaserMeasureSkip_Use.Text = "사용함";
            this.btn_LaserMeasureSkip_Use.UseVisualStyleBackColor = false;
            // 
            // gxtUnloaderInputSkip
            // 
            this.gxtUnloaderInputSkip.Controls.Add(this.tableLayoutPanel21);
            this.gxtUnloaderInputSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtUnloaderInputSkip.ForeColor = System.Drawing.Color.White;
            this.gxtUnloaderInputSkip.Location = new System.Drawing.Point(428, 362);
            this.gxtUnloaderInputSkip.Name = "gxtUnloaderInputSkip";
            this.gxtUnloaderInputSkip.Size = new System.Drawing.Size(419, 60);
            this.gxtUnloaderInputSkip.TabIndex = 60;
            this.gxtUnloaderInputSkip.TabStop = false;
            this.gxtUnloaderInputSkip.Text = "     언로더부 카세트 A / B 투입 스킵     ";
            // 
            // tableLayoutPanel21
            // 
            this.tableLayoutPanel21.ColumnCount = 2;
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel21.Controls.Add(this.tableLayoutPanel24, 0, 0);
            this.tableLayoutPanel21.Controls.Add(this.tableLayoutPanel25, 0, 0);
            this.tableLayoutPanel21.Location = new System.Drawing.Point(3, 18);
            this.tableLayoutPanel21.Name = "tableLayoutPanel21";
            this.tableLayoutPanel21.RowCount = 1;
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel21.Size = new System.Drawing.Size(415, 39);
            this.tableLayoutPanel21.TabIndex = 0;
            // 
            // tableLayoutPanel24
            // 
            this.tableLayoutPanel24.ColumnCount = 3;
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.5F));
            this.tableLayoutPanel24.Controls.Add(this.btn_UnloaderInputSkipB_DontUse, 2, 0);
            this.tableLayoutPanel24.Controls.Add(this.txtUnloaderInputSkipB, 0, 0);
            this.tableLayoutPanel24.Controls.Add(this.btn_UnloaderInputSkipB_Use, 1, 0);
            this.tableLayoutPanel24.Location = new System.Drawing.Point(210, 3);
            this.tableLayoutPanel24.Name = "tableLayoutPanel24";
            this.tableLayoutPanel24.RowCount = 1;
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel24.Size = new System.Drawing.Size(201, 33);
            this.tableLayoutPanel24.TabIndex = 3;
            // 
            // btn_UnloaderInputSkipB_DontUse
            // 
            this.btn_UnloaderInputSkipB_DontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderInputSkipB_DontUse.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_UnloaderInputSkipB_DontUse.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderInputSkipB_DontUse.Location = new System.Drawing.Point(138, 3);
            this.btn_UnloaderInputSkipB_DontUse.Name = "btn_UnloaderInputSkipB_DontUse";
            this.btn_UnloaderInputSkipB_DontUse.Size = new System.Drawing.Size(60, 27);
            this.btn_UnloaderInputSkipB_DontUse.TabIndex = 47;
            this.btn_UnloaderInputSkipB_DontUse.Text = "사용안함";
            this.btn_UnloaderInputSkipB_DontUse.UseVisualStyleBackColor = false;
            // 
            // txtUnloaderInputSkipB
            // 
            this.txtUnloaderInputSkipB.Location = new System.Drawing.Point(3, 3);
            this.txtUnloaderInputSkipB.Name = "txtUnloaderInputSkipB";
            this.txtUnloaderInputSkipB.Size = new System.Drawing.Size(74, 29);
            this.txtUnloaderInputSkipB.TabIndex = 0;
            // 
            // btn_UnloaderInputSkipB_Use
            // 
            this.btn_UnloaderInputSkipB_Use.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderInputSkipB_Use.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_UnloaderInputSkipB_Use.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderInputSkipB_Use.Location = new System.Drawing.Point(83, 3);
            this.btn_UnloaderInputSkipB_Use.Name = "btn_UnloaderInputSkipB_Use";
            this.btn_UnloaderInputSkipB_Use.Size = new System.Drawing.Size(49, 27);
            this.btn_UnloaderInputSkipB_Use.TabIndex = 46;
            this.btn_UnloaderInputSkipB_Use.Text = "사용함";
            this.btn_UnloaderInputSkipB_Use.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel25
            // 
            this.tableLayoutPanel25.ColumnCount = 3;
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.5F));
            this.tableLayoutPanel25.Controls.Add(this.btn_UnloaderInputSkipA_DontUse, 2, 0);
            this.tableLayoutPanel25.Controls.Add(this.txtUnloaderInputSkipA, 0, 0);
            this.tableLayoutPanel25.Controls.Add(this.btn_UnloaderInputSkipA_Use, 1, 0);
            this.tableLayoutPanel25.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel25.Name = "tableLayoutPanel25";
            this.tableLayoutPanel25.RowCount = 1;
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel25.Size = new System.Drawing.Size(201, 33);
            this.tableLayoutPanel25.TabIndex = 2;
            // 
            // btn_UnloaderInputSkipA_DontUse
            // 
            this.btn_UnloaderInputSkipA_DontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderInputSkipA_DontUse.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_UnloaderInputSkipA_DontUse.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderInputSkipA_DontUse.Location = new System.Drawing.Point(138, 3);
            this.btn_UnloaderInputSkipA_DontUse.Name = "btn_UnloaderInputSkipA_DontUse";
            this.btn_UnloaderInputSkipA_DontUse.Size = new System.Drawing.Size(60, 27);
            this.btn_UnloaderInputSkipA_DontUse.TabIndex = 47;
            this.btn_UnloaderInputSkipA_DontUse.Text = "사용안함";
            this.btn_UnloaderInputSkipA_DontUse.UseVisualStyleBackColor = false;
            // 
            // txtUnloaderInputSkipA
            // 
            this.txtUnloaderInputSkipA.Location = new System.Drawing.Point(3, 3);
            this.txtUnloaderInputSkipA.Name = "txtUnloaderInputSkipA";
            this.txtUnloaderInputSkipA.Size = new System.Drawing.Size(74, 29);
            this.txtUnloaderInputSkipA.TabIndex = 0;
            // 
            // btn_UnloaderInputSkipA_Use
            // 
            this.btn_UnloaderInputSkipA_Use.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderInputSkipA_Use.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_UnloaderInputSkipA_Use.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderInputSkipA_Use.Location = new System.Drawing.Point(83, 3);
            this.btn_UnloaderInputSkipA_Use.Name = "btn_UnloaderInputSkipA_Use";
            this.btn_UnloaderInputSkipA_Use.Size = new System.Drawing.Size(49, 27);
            this.btn_UnloaderInputSkipA_Use.TabIndex = 46;
            this.btn_UnloaderInputSkipA_Use.Text = "사용함";
            this.btn_UnloaderInputSkipA_Use.UseVisualStyleBackColor = false;
            // 
            // gxtLoaderInputSkip
            // 
            this.gxtLoaderInputSkip.Controls.Add(this.tableLayoutPanel20);
            this.gxtLoaderInputSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtLoaderInputSkip.ForeColor = System.Drawing.Color.White;
            this.gxtLoaderInputSkip.Location = new System.Drawing.Point(3, 362);
            this.gxtLoaderInputSkip.Name = "gxtLoaderInputSkip";
            this.gxtLoaderInputSkip.Size = new System.Drawing.Size(419, 60);
            this.gxtLoaderInputSkip.TabIndex = 59;
            this.gxtLoaderInputSkip.TabStop = false;
            this.gxtLoaderInputSkip.Text = "     로더부 카세트 A / B 투입 스킵     ";
            // 
            // tableLayoutPanel20
            // 
            this.tableLayoutPanel20.ColumnCount = 2;
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel20.Controls.Add(this.tableLayoutPanel23, 0, 0);
            this.tableLayoutPanel20.Controls.Add(this.tableLayoutPanel22, 0, 0);
            this.tableLayoutPanel20.Location = new System.Drawing.Point(3, 18);
            this.tableLayoutPanel20.Name = "tableLayoutPanel20";
            this.tableLayoutPanel20.RowCount = 1;
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel20.Size = new System.Drawing.Size(415, 39);
            this.tableLayoutPanel20.TabIndex = 0;
            // 
            // tableLayoutPanel23
            // 
            this.tableLayoutPanel23.ColumnCount = 3;
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.5F));
            this.tableLayoutPanel23.Controls.Add(this.btn_LoaderInputSkipB_DontUse, 2, 0);
            this.tableLayoutPanel23.Controls.Add(this.txtLoaderInputSkipB, 0, 0);
            this.tableLayoutPanel23.Controls.Add(this.btn_LoaderInputSkipB_Use, 1, 0);
            this.tableLayoutPanel23.Location = new System.Drawing.Point(210, 3);
            this.tableLayoutPanel23.Name = "tableLayoutPanel23";
            this.tableLayoutPanel23.RowCount = 1;
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel23.Size = new System.Drawing.Size(201, 33);
            this.tableLayoutPanel23.TabIndex = 3;
            // 
            // btn_LoaderInputSkipB_DontUse
            // 
            this.btn_LoaderInputSkipB_DontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_LoaderInputSkipB_DontUse.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_LoaderInputSkipB_DontUse.ForeColor = System.Drawing.Color.White;
            this.btn_LoaderInputSkipB_DontUse.Location = new System.Drawing.Point(138, 3);
            this.btn_LoaderInputSkipB_DontUse.Name = "btn_LoaderInputSkipB_DontUse";
            this.btn_LoaderInputSkipB_DontUse.Size = new System.Drawing.Size(60, 27);
            this.btn_LoaderInputSkipB_DontUse.TabIndex = 47;
            this.btn_LoaderInputSkipB_DontUse.Text = "사용안함";
            this.btn_LoaderInputSkipB_DontUse.UseVisualStyleBackColor = false;
            // 
            // txtLoaderInputSkipB
            // 
            this.txtLoaderInputSkipB.Location = new System.Drawing.Point(3, 3);
            this.txtLoaderInputSkipB.Name = "txtLoaderInputSkipB";
            this.txtLoaderInputSkipB.Size = new System.Drawing.Size(74, 29);
            this.txtLoaderInputSkipB.TabIndex = 0;
            // 
            // btn_LoaderInputSkipB_Use
            // 
            this.btn_LoaderInputSkipB_Use.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_LoaderInputSkipB_Use.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_LoaderInputSkipB_Use.ForeColor = System.Drawing.Color.White;
            this.btn_LoaderInputSkipB_Use.Location = new System.Drawing.Point(83, 3);
            this.btn_LoaderInputSkipB_Use.Name = "btn_LoaderInputSkipB_Use";
            this.btn_LoaderInputSkipB_Use.Size = new System.Drawing.Size(49, 27);
            this.btn_LoaderInputSkipB_Use.TabIndex = 46;
            this.btn_LoaderInputSkipB_Use.Text = "사용함";
            this.btn_LoaderInputSkipB_Use.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel22
            // 
            this.tableLayoutPanel22.ColumnCount = 3;
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.5F));
            this.tableLayoutPanel22.Controls.Add(this.btn_LoaderInputSkipA_DontUse, 2, 0);
            this.tableLayoutPanel22.Controls.Add(this.txtLoaderInputSkipA, 0, 0);
            this.tableLayoutPanel22.Controls.Add(this.btn_LoaderInputSkipA_Use, 1, 0);
            this.tableLayoutPanel22.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel22.Name = "tableLayoutPanel22";
            this.tableLayoutPanel22.RowCount = 1;
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel22.Size = new System.Drawing.Size(201, 33);
            this.tableLayoutPanel22.TabIndex = 2;
            // 
            // btn_LoaderInputSkipA_DontUse
            // 
            this.btn_LoaderInputSkipA_DontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_LoaderInputSkipA_DontUse.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_LoaderInputSkipA_DontUse.ForeColor = System.Drawing.Color.White;
            this.btn_LoaderInputSkipA_DontUse.Location = new System.Drawing.Point(138, 3);
            this.btn_LoaderInputSkipA_DontUse.Name = "btn_LoaderInputSkipA_DontUse";
            this.btn_LoaderInputSkipA_DontUse.Size = new System.Drawing.Size(60, 27);
            this.btn_LoaderInputSkipA_DontUse.TabIndex = 47;
            this.btn_LoaderInputSkipA_DontUse.Text = "사용안함";
            this.btn_LoaderInputSkipA_DontUse.UseVisualStyleBackColor = false;
            // 
            // txtLoaderInputSkipA
            // 
            this.txtLoaderInputSkipA.Location = new System.Drawing.Point(3, 3);
            this.txtLoaderInputSkipA.Name = "txtLoaderInputSkipA";
            this.txtLoaderInputSkipA.Size = new System.Drawing.Size(74, 29);
            this.txtLoaderInputSkipA.TabIndex = 0;
            // 
            // btn_LoaderInputSkipA_Use
            // 
            this.btn_LoaderInputSkipA_Use.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_LoaderInputSkipA_Use.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_LoaderInputSkipA_Use.ForeColor = System.Drawing.Color.White;
            this.btn_LoaderInputSkipA_Use.Location = new System.Drawing.Point(83, 3);
            this.btn_LoaderInputSkipA_Use.Name = "btn_LoaderInputSkipA_Use";
            this.btn_LoaderInputSkipA_Use.Size = new System.Drawing.Size(49, 27);
            this.btn_LoaderInputSkipA_Use.TabIndex = 46;
            this.btn_LoaderInputSkipA_Use.Text = "사용함";
            this.btn_LoaderInputSkipA_Use.UseVisualStyleBackColor = false;
            // 
            // gxtBufferPickerSkip
            // 
            this.gxtBufferPickerSkip.Controls.Add(this.tableLayoutPanel19);
            this.gxtBufferPickerSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtBufferPickerSkip.ForeColor = System.Drawing.Color.White;
            this.gxtBufferPickerSkip.Location = new System.Drawing.Point(428, 306);
            this.gxtBufferPickerSkip.Name = "gxtBufferPickerSkip";
            this.gxtBufferPickerSkip.Size = new System.Drawing.Size(419, 57);
            this.gxtBufferPickerSkip.TabIndex = 58;
            this.gxtBufferPickerSkip.TabStop = false;
            this.gxtBufferPickerSkip.Text = "     버퍼 피커 스킵     ";
            // 
            // tableLayoutPanel19
            // 
            this.tableLayoutPanel19.ColumnCount = 3;
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel19.Controls.Add(this.btnBufferPickerSkipDontUse, 2, 0);
            this.tableLayoutPanel19.Controls.Add(this.txtBufferPickerSkip, 0, 0);
            this.tableLayoutPanel19.Controls.Add(this.btnBufferPickerSkipUse, 1, 0);
            this.tableLayoutPanel19.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel19.Name = "tableLayoutPanel19";
            this.tableLayoutPanel19.RowCount = 1;
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel19.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel19.TabIndex = 1;
            // 
            // btnBufferPickerSkipDontUse
            // 
            this.btnBufferPickerSkipDontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBufferPickerSkipDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBufferPickerSkipDontUse.ForeColor = System.Drawing.Color.White;
            this.btnBufferPickerSkipDontUse.Location = new System.Drawing.Point(298, 3);
            this.btnBufferPickerSkipDontUse.Name = "btnBufferPickerSkipDontUse";
            this.btnBufferPickerSkipDontUse.Size = new System.Drawing.Size(106, 26);
            this.btnBufferPickerSkipDontUse.TabIndex = 47;
            this.btnBufferPickerSkipDontUse.Text = "사용안함";
            this.btnBufferPickerSkipDontUse.UseVisualStyleBackColor = false;
            // 
            // txtBufferPickerSkip
            // 
            this.txtBufferPickerSkip.Location = new System.Drawing.Point(3, 3);
            this.txtBufferPickerSkip.Name = "txtBufferPickerSkip";
            this.txtBufferPickerSkip.Size = new System.Drawing.Size(177, 29);
            this.txtBufferPickerSkip.TabIndex = 0;
            // 
            // btnBufferPickerSkipUse
            // 
            this.btnBufferPickerSkipUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBufferPickerSkipUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBufferPickerSkipUse.ForeColor = System.Drawing.Color.White;
            this.btnBufferPickerSkipUse.Location = new System.Drawing.Point(186, 3);
            this.btnBufferPickerSkipUse.Name = "btnBufferPickerSkipUse";
            this.btnBufferPickerSkipUse.Size = new System.Drawing.Size(106, 26);
            this.btnBufferPickerSkipUse.TabIndex = 46;
            this.btnBufferPickerSkipUse.Text = "사용함";
            this.btnBufferPickerSkipUse.UseVisualStyleBackColor = false;
            // 
            // gxtTiltMeasureSkip
            // 
            this.gxtTiltMeasureSkip.Controls.Add(this.tableLayoutPanel18);
            this.gxtTiltMeasureSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtTiltMeasureSkip.ForeColor = System.Drawing.Color.White;
            this.gxtTiltMeasureSkip.Location = new System.Drawing.Point(428, 250);
            this.gxtTiltMeasureSkip.Name = "gxtTiltMeasureSkip";
            this.gxtTiltMeasureSkip.Size = new System.Drawing.Size(419, 57);
            this.gxtTiltMeasureSkip.TabIndex = 54;
            this.gxtTiltMeasureSkip.TabStop = false;
            this.gxtTiltMeasureSkip.Text = "     틸트 측정 스킵";
            // 
            // tableLayoutPanel18
            // 
            this.tableLayoutPanel18.ColumnCount = 3;
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel18.Controls.Add(this.btnTiltMeasureSkipDontUse, 2, 0);
            this.tableLayoutPanel18.Controls.Add(this.txtTiltMeasureSkip, 0, 0);
            this.tableLayoutPanel18.Controls.Add(this.btnTiltMeasureSkipUse, 1, 0);
            this.tableLayoutPanel18.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel18.Name = "tableLayoutPanel18";
            this.tableLayoutPanel18.RowCount = 1;
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel18.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel18.TabIndex = 1;
            // 
            // btnTiltMeasureSkipDontUse
            // 
            this.btnTiltMeasureSkipDontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnTiltMeasureSkipDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnTiltMeasureSkipDontUse.ForeColor = System.Drawing.Color.White;
            this.btnTiltMeasureSkipDontUse.Location = new System.Drawing.Point(298, 3);
            this.btnTiltMeasureSkipDontUse.Name = "btnTiltMeasureSkipDontUse";
            this.btnTiltMeasureSkipDontUse.Size = new System.Drawing.Size(106, 26);
            this.btnTiltMeasureSkipDontUse.TabIndex = 47;
            this.btnTiltMeasureSkipDontUse.Text = "사용안함";
            this.btnTiltMeasureSkipDontUse.UseVisualStyleBackColor = false;
            // 
            // txtTiltMeasureSkip
            // 
            this.txtTiltMeasureSkip.Location = new System.Drawing.Point(3, 3);
            this.txtTiltMeasureSkip.Name = "txtTiltMeasureSkip";
            this.txtTiltMeasureSkip.Size = new System.Drawing.Size(177, 29);
            this.txtTiltMeasureSkip.TabIndex = 0;
            // 
            // btnTiltMeasureSkipUse
            // 
            this.btnTiltMeasureSkipUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnTiltMeasureSkipUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnTiltMeasureSkipUse.ForeColor = System.Drawing.Color.White;
            this.btnTiltMeasureSkipUse.Location = new System.Drawing.Point(186, 3);
            this.btnTiltMeasureSkipUse.Name = "btnTiltMeasureSkipUse";
            this.btnTiltMeasureSkipUse.Size = new System.Drawing.Size(106, 26);
            this.btnTiltMeasureSkipUse.TabIndex = 46;
            this.btnTiltMeasureSkipUse.Text = "사용함";
            this.btnTiltMeasureSkipUse.UseVisualStyleBackColor = false;
            // 
            // gxtBreakingAlignSkip
            // 
            this.gxtBreakingAlignSkip.Controls.Add(this.tableLayoutPanel17);
            this.gxtBreakingAlignSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtBreakingAlignSkip.ForeColor = System.Drawing.Color.White;
            this.gxtBreakingAlignSkip.Location = new System.Drawing.Point(428, 194);
            this.gxtBreakingAlignSkip.Name = "gxtBreakingAlignSkip";
            this.gxtBreakingAlignSkip.Size = new System.Drawing.Size(419, 57);
            this.gxtBreakingAlignSkip.TabIndex = 57;
            this.gxtBreakingAlignSkip.TabStop = false;
            this.gxtBreakingAlignSkip.Text = "     브레이킹 얼라인 스킵     ";
            // 
            // tableLayoutPanel17
            // 
            this.tableLayoutPanel17.ColumnCount = 3;
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel17.Controls.Add(this.btnBreakingAlignSkipDontUse, 2, 0);
            this.tableLayoutPanel17.Controls.Add(this.txtBreakingAlignSkip, 0, 0);
            this.tableLayoutPanel17.Controls.Add(this.btnBreakingAlignSkipUse, 1, 0);
            this.tableLayoutPanel17.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel17.Name = "tableLayoutPanel17";
            this.tableLayoutPanel17.RowCount = 1;
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel17.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel17.TabIndex = 1;
            // 
            // btnBreakingAlignSkipDontUse
            // 
            this.btnBreakingAlignSkipDontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakingAlignSkipDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakingAlignSkipDontUse.ForeColor = System.Drawing.Color.White;
            this.btnBreakingAlignSkipDontUse.Location = new System.Drawing.Point(298, 3);
            this.btnBreakingAlignSkipDontUse.Name = "btnBreakingAlignSkipDontUse";
            this.btnBreakingAlignSkipDontUse.Size = new System.Drawing.Size(106, 26);
            this.btnBreakingAlignSkipDontUse.TabIndex = 47;
            this.btnBreakingAlignSkipDontUse.Text = "사용안함";
            this.btnBreakingAlignSkipDontUse.UseVisualStyleBackColor = false;
            // 
            // txtBreakingAlignSkip
            // 
            this.txtBreakingAlignSkip.Location = new System.Drawing.Point(3, 3);
            this.txtBreakingAlignSkip.Name = "txtBreakingAlignSkip";
            this.txtBreakingAlignSkip.Size = new System.Drawing.Size(177, 29);
            this.txtBreakingAlignSkip.TabIndex = 0;
            // 
            // btnBreakingAlignSkipUse
            // 
            this.btnBreakingAlignSkipUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakingAlignSkipUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakingAlignSkipUse.ForeColor = System.Drawing.Color.White;
            this.btnBreakingAlignSkipUse.Location = new System.Drawing.Point(186, 3);
            this.btnBreakingAlignSkipUse.Name = "btnBreakingAlignSkipUse";
            this.btnBreakingAlignSkipUse.Size = new System.Drawing.Size(106, 26);
            this.btnBreakingAlignSkipUse.TabIndex = 46;
            this.btnBreakingAlignSkipUse.Text = "사용함";
            this.btnBreakingAlignSkipUse.UseVisualStyleBackColor = false;
            // 
            // gxtBcrSkip
            // 
            this.gxtBcrSkip.Controls.Add(this.tableLayoutPanel16);
            this.gxtBcrSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtBcrSkip.ForeColor = System.Drawing.Color.White;
            this.gxtBcrSkip.Location = new System.Drawing.Point(3, 306);
            this.gxtBcrSkip.Name = "gxtBcrSkip";
            this.gxtBcrSkip.Size = new System.Drawing.Size(419, 57);
            this.gxtBcrSkip.TabIndex = 54;
            this.gxtBcrSkip.TabStop = false;
            this.gxtBcrSkip.Text = "     BCR 스킵     ";
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.ColumnCount = 3;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel16.Controls.Add(this.btnBcrSkipDontUse, 2, 0);
            this.tableLayoutPanel16.Controls.Add(this.txtBcrSkip, 0, 0);
            this.tableLayoutPanel16.Controls.Add(this.btnBcrSkipUse, 1, 0);
            this.tableLayoutPanel16.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 1;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel16.TabIndex = 1;
            // 
            // btnBcrSkipDontUse
            // 
            this.btnBcrSkipDontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBcrSkipDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBcrSkipDontUse.ForeColor = System.Drawing.Color.White;
            this.btnBcrSkipDontUse.Location = new System.Drawing.Point(298, 3);
            this.btnBcrSkipDontUse.Name = "btnBcrSkipDontUse";
            this.btnBcrSkipDontUse.Size = new System.Drawing.Size(106, 26);
            this.btnBcrSkipDontUse.TabIndex = 47;
            this.btnBcrSkipDontUse.Text = "사용안함";
            this.btnBcrSkipDontUse.UseVisualStyleBackColor = false;
            // 
            // txtBcrSkip
            // 
            this.txtBcrSkip.Location = new System.Drawing.Point(3, 3);
            this.txtBcrSkip.Name = "txtBcrSkip";
            this.txtBcrSkip.Size = new System.Drawing.Size(177, 29);
            this.txtBcrSkip.TabIndex = 0;
            // 
            // btnBcrSkipUse
            // 
            this.btnBcrSkipUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBcrSkipUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBcrSkipUse.ForeColor = System.Drawing.Color.White;
            this.btnBcrSkipUse.Location = new System.Drawing.Point(186, 3);
            this.btnBcrSkipUse.Name = "btnBcrSkipUse";
            this.btnBcrSkipUse.Size = new System.Drawing.Size(106, 26);
            this.btnBcrSkipUse.TabIndex = 46;
            this.btnBcrSkipUse.Text = "사용함";
            this.btnBcrSkipUse.UseVisualStyleBackColor = false;
            // 
            // gxtMcrSkip
            // 
            this.gxtMcrSkip.Controls.Add(this.tableLayoutPanel15);
            this.gxtMcrSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtMcrSkip.ForeColor = System.Drawing.Color.White;
            this.gxtMcrSkip.Location = new System.Drawing.Point(3, 250);
            this.gxtMcrSkip.Name = "gxtMcrSkip";
            this.gxtMcrSkip.Size = new System.Drawing.Size(419, 57);
            this.gxtMcrSkip.TabIndex = 54;
            this.gxtMcrSkip.TabStop = false;
            this.gxtMcrSkip.Text = "     MCR 스킵    ";
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 3;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel15.Controls.Add(this.btnMcrSkipDontUse, 2, 0);
            this.tableLayoutPanel15.Controls.Add(this.txtMcrSkip, 0, 0);
            this.tableLayoutPanel15.Controls.Add(this.btnMcrSkipUse, 1, 0);
            this.tableLayoutPanel15.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 1;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel15.TabIndex = 1;
            // 
            // btnMcrSkipDontUse
            // 
            this.btnMcrSkipDontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnMcrSkipDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMcrSkipDontUse.ForeColor = System.Drawing.Color.White;
            this.btnMcrSkipDontUse.Location = new System.Drawing.Point(298, 3);
            this.btnMcrSkipDontUse.Name = "btnMcrSkipDontUse";
            this.btnMcrSkipDontUse.Size = new System.Drawing.Size(106, 26);
            this.btnMcrSkipDontUse.TabIndex = 47;
            this.btnMcrSkipDontUse.Text = "사용안함";
            this.btnMcrSkipDontUse.UseVisualStyleBackColor = false;
            // 
            // txtMcrSkip
            // 
            this.txtMcrSkip.Location = new System.Drawing.Point(3, 3);
            this.txtMcrSkip.Name = "txtMcrSkip";
            this.txtMcrSkip.Size = new System.Drawing.Size(177, 29);
            this.txtMcrSkip.TabIndex = 0;
            // 
            // btnMcrSkipUse
            // 
            this.btnMcrSkipUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnMcrSkipUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMcrSkipUse.ForeColor = System.Drawing.Color.White;
            this.btnMcrSkipUse.Location = new System.Drawing.Point(186, 3);
            this.btnMcrSkipUse.Name = "btnMcrSkipUse";
            this.btnMcrSkipUse.Size = new System.Drawing.Size(106, 26);
            this.btnMcrSkipUse.TabIndex = 46;
            this.btnMcrSkipUse.Text = "사용함";
            this.btnMcrSkipUse.UseVisualStyleBackColor = false;
            // 
            // gxtInspectionSkip
            // 
            this.gxtInspectionSkip.Controls.Add(this.tableLayoutPanel14);
            this.gxtInspectionSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtInspectionSkip.ForeColor = System.Drawing.Color.White;
            this.gxtInspectionSkip.Location = new System.Drawing.Point(3, 194);
            this.gxtInspectionSkip.Name = "gxtInspectionSkip";
            this.gxtInspectionSkip.Size = new System.Drawing.Size(419, 57);
            this.gxtInspectionSkip.TabIndex = 56;
            this.gxtInspectionSkip.TabStop = false;
            this.gxtInspectionSkip.Text = "     인스펙션 스킵     ";
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 3;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel14.Controls.Add(this.btnInspectionSkipDontUse, 2, 0);
            this.tableLayoutPanel14.Controls.Add(this.txtInspectionSkip, 0, 0);
            this.tableLayoutPanel14.Controls.Add(this.btninspectionSkipUse, 1, 0);
            this.tableLayoutPanel14.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 1;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel14.TabIndex = 1;
            // 
            // btnInspectionSkipDontUse
            // 
            this.btnInspectionSkipDontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnInspectionSkipDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnInspectionSkipDontUse.ForeColor = System.Drawing.Color.White;
            this.btnInspectionSkipDontUse.Location = new System.Drawing.Point(298, 3);
            this.btnInspectionSkipDontUse.Name = "btnInspectionSkipDontUse";
            this.btnInspectionSkipDontUse.Size = new System.Drawing.Size(106, 26);
            this.btnInspectionSkipDontUse.TabIndex = 47;
            this.btnInspectionSkipDontUse.Text = "사용안함";
            this.btnInspectionSkipDontUse.UseVisualStyleBackColor = false;
            // 
            // txtInspectionSkip
            // 
            this.txtInspectionSkip.Location = new System.Drawing.Point(3, 3);
            this.txtInspectionSkip.Name = "txtInspectionSkip";
            this.txtInspectionSkip.Size = new System.Drawing.Size(177, 29);
            this.txtInspectionSkip.TabIndex = 0;
            // 
            // btninspectionSkipUse
            // 
            this.btninspectionSkipUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btninspectionSkipUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btninspectionSkipUse.ForeColor = System.Drawing.Color.White;
            this.btninspectionSkipUse.Location = new System.Drawing.Point(186, 3);
            this.btninspectionSkipUse.Name = "btninspectionSkipUse";
            this.btninspectionSkipUse.Size = new System.Drawing.Size(106, 26);
            this.btninspectionSkipUse.TabIndex = 46;
            this.btninspectionSkipUse.Text = "사용함";
            this.btninspectionSkipUse.UseVisualStyleBackColor = false;
            // 
            // gxtBreakingMotionSkip
            // 
            this.gxtBreakingMotionSkip.Controls.Add(this.tableLayoutPanel13);
            this.gxtBreakingMotionSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtBreakingMotionSkip.ForeColor = System.Drawing.Color.White;
            this.gxtBreakingMotionSkip.Location = new System.Drawing.Point(428, 138);
            this.gxtBreakingMotionSkip.Name = "gxtBreakingMotionSkip";
            this.gxtBreakingMotionSkip.Size = new System.Drawing.Size(419, 57);
            this.gxtBreakingMotionSkip.TabIndex = 54;
            this.gxtBreakingMotionSkip.TabStop = false;
            this.gxtBreakingMotionSkip.Text = "     브레이킹 모션 스킵     ";
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 3;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel13.Controls.Add(this.btnBreakingMotionSkipDontUse, 2, 0);
            this.tableLayoutPanel13.Controls.Add(this.txtBreakingMotionSkip, 0, 0);
            this.tableLayoutPanel13.Controls.Add(this.btnBreakingMotionSkipUse, 1, 0);
            this.tableLayoutPanel13.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 1;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel13.TabIndex = 1;
            // 
            // btnBreakingMotionSkipDontUse
            // 
            this.btnBreakingMotionSkipDontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakingMotionSkipDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakingMotionSkipDontUse.ForeColor = System.Drawing.Color.White;
            this.btnBreakingMotionSkipDontUse.Location = new System.Drawing.Point(298, 3);
            this.btnBreakingMotionSkipDontUse.Name = "btnBreakingMotionSkipDontUse";
            this.btnBreakingMotionSkipDontUse.Size = new System.Drawing.Size(106, 26);
            this.btnBreakingMotionSkipDontUse.TabIndex = 47;
            this.btnBreakingMotionSkipDontUse.Text = "사용안함";
            this.btnBreakingMotionSkipDontUse.UseVisualStyleBackColor = false;
            // 
            // txtBreakingMotionSkip
            // 
            this.txtBreakingMotionSkip.Location = new System.Drawing.Point(3, 3);
            this.txtBreakingMotionSkip.Name = "txtBreakingMotionSkip";
            this.txtBreakingMotionSkip.Size = new System.Drawing.Size(177, 29);
            this.txtBreakingMotionSkip.TabIndex = 0;
            // 
            // btnBreakingMotionSkipUse
            // 
            this.btnBreakingMotionSkipUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakingMotionSkipUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakingMotionSkipUse.ForeColor = System.Drawing.Color.White;
            this.btnBreakingMotionSkipUse.Location = new System.Drawing.Point(186, 3);
            this.btnBreakingMotionSkipUse.Name = "btnBreakingMotionSkipUse";
            this.btnBreakingMotionSkipUse.Size = new System.Drawing.Size(106, 26);
            this.btnBreakingMotionSkipUse.TabIndex = 46;
            this.btnBreakingMotionSkipUse.Text = "사용함";
            this.btnBreakingMotionSkipUse.UseVisualStyleBackColor = false;
            // 
            // gxtLaserSkip
            // 
            this.gxtLaserSkip.Controls.Add(this.tableLayoutPanel12);
            this.gxtLaserSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtLaserSkip.ForeColor = System.Drawing.Color.White;
            this.gxtLaserSkip.Location = new System.Drawing.Point(3, 138);
            this.gxtLaserSkip.Name = "gxtLaserSkip";
            this.gxtLaserSkip.Size = new System.Drawing.Size(419, 57);
            this.gxtLaserSkip.TabIndex = 55;
            this.gxtLaserSkip.TabStop = false;
            this.gxtLaserSkip.Text = "     Laser 스킵     ";
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 3;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel12.Controls.Add(this.btnLaserSkipDontUse, 2, 0);
            this.tableLayoutPanel12.Controls.Add(this.txtLaserSkip, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.btnLaserSkipUse, 1, 0);
            this.tableLayoutPanel12.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 1;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel12.TabIndex = 1;
            // 
            // btnLaserSkipDontUse
            // 
            this.btnLaserSkipDontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnLaserSkipDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLaserSkipDontUse.ForeColor = System.Drawing.Color.White;
            this.btnLaserSkipDontUse.Location = new System.Drawing.Point(298, 3);
            this.btnLaserSkipDontUse.Name = "btnLaserSkipDontUse";
            this.btnLaserSkipDontUse.Size = new System.Drawing.Size(106, 26);
            this.btnLaserSkipDontUse.TabIndex = 47;
            this.btnLaserSkipDontUse.Text = "사용안함";
            this.btnLaserSkipDontUse.UseVisualStyleBackColor = false;
            // 
            // txtLaserSkip
            // 
            this.txtLaserSkip.Location = new System.Drawing.Point(3, 3);
            this.txtLaserSkip.Name = "txtLaserSkip";
            this.txtLaserSkip.Size = new System.Drawing.Size(177, 29);
            this.txtLaserSkip.TabIndex = 0;
            // 
            // btnLaserSkipUse
            // 
            this.btnLaserSkipUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnLaserSkipUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnLaserSkipUse.ForeColor = System.Drawing.Color.White;
            this.btnLaserSkipUse.Location = new System.Drawing.Point(186, 3);
            this.btnLaserSkipUse.Name = "btnLaserSkipUse";
            this.btnLaserSkipUse.Size = new System.Drawing.Size(106, 26);
            this.btnLaserSkipUse.TabIndex = 46;
            this.btnLaserSkipUse.Text = "사용함";
            this.btnLaserSkipUse.UseVisualStyleBackColor = false;
            // 
            // gxtBreakingBSkip
            // 
            this.gxtBreakingBSkip.Controls.Add(this.tableLayoutPanel11);
            this.gxtBreakingBSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtBreakingBSkip.ForeColor = System.Drawing.Color.White;
            this.gxtBreakingBSkip.Location = new System.Drawing.Point(428, 82);
            this.gxtBreakingBSkip.Name = "gxtBreakingBSkip";
            this.gxtBreakingBSkip.Size = new System.Drawing.Size(419, 57);
            this.gxtBreakingBSkip.TabIndex = 54;
            this.gxtBreakingBSkip.TabStop = false;
            this.gxtBreakingBSkip.Text = "     브레이킹 B 물류 스킵     ";
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 3;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel11.Controls.Add(this.btnBreakingBSkipDontUse, 2, 0);
            this.tableLayoutPanel11.Controls.Add(this.txtBreakingBSkip, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this.btnBreakingBSkipUse, 1, 0);
            this.tableLayoutPanel11.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 1;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel11.TabIndex = 1;
            // 
            // btnBreakingBSkipDontUse
            // 
            this.btnBreakingBSkipDontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakingBSkipDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakingBSkipDontUse.ForeColor = System.Drawing.Color.White;
            this.btnBreakingBSkipDontUse.Location = new System.Drawing.Point(298, 3);
            this.btnBreakingBSkipDontUse.Name = "btnBreakingBSkipDontUse";
            this.btnBreakingBSkipDontUse.Size = new System.Drawing.Size(106, 26);
            this.btnBreakingBSkipDontUse.TabIndex = 47;
            this.btnBreakingBSkipDontUse.Text = "사용안함";
            this.btnBreakingBSkipDontUse.UseVisualStyleBackColor = false;
            // 
            // txtBreakingBSkip
            // 
            this.txtBreakingBSkip.Location = new System.Drawing.Point(3, 3);
            this.txtBreakingBSkip.Name = "txtBreakingBSkip";
            this.txtBreakingBSkip.Size = new System.Drawing.Size(177, 29);
            this.txtBreakingBSkip.TabIndex = 0;
            // 
            // btnBreakingBSkipUse
            // 
            this.btnBreakingBSkipUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakingBSkipUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakingBSkipUse.ForeColor = System.Drawing.Color.White;
            this.btnBreakingBSkipUse.Location = new System.Drawing.Point(186, 3);
            this.btnBreakingBSkipUse.Name = "btnBreakingBSkipUse";
            this.btnBreakingBSkipUse.Size = new System.Drawing.Size(106, 26);
            this.btnBreakingBSkipUse.TabIndex = 46;
            this.btnBreakingBSkipUse.Text = "사용함";
            this.btnBreakingBSkipUse.UseVisualStyleBackColor = false;
            // 
            // gxtBreakingASkip
            // 
            this.gxtBreakingASkip.Controls.Add(this.tableLayoutPanel10);
            this.gxtBreakingASkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtBreakingASkip.ForeColor = System.Drawing.Color.White;
            this.gxtBreakingASkip.Location = new System.Drawing.Point(3, 82);
            this.gxtBreakingASkip.Name = "gxtBreakingASkip";
            this.gxtBreakingASkip.Size = new System.Drawing.Size(419, 57);
            this.gxtBreakingASkip.TabIndex = 54;
            this.gxtBreakingASkip.TabStop = false;
            this.gxtBreakingASkip.Text = "     브레이킹 A 물류 스킵     ";
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 3;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel10.Controls.Add(this.btnBreakingASkipDontUse, 2, 0);
            this.tableLayoutPanel10.Controls.Add(this.txtBreakingASkip, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.btnBreakingASkipUse, 1, 0);
            this.tableLayoutPanel10.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 1;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel10.TabIndex = 1;
            // 
            // btnBreakingASkipDontUse
            // 
            this.btnBreakingASkipDontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakingASkipDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakingASkipDontUse.ForeColor = System.Drawing.Color.White;
            this.btnBreakingASkipDontUse.Location = new System.Drawing.Point(298, 3);
            this.btnBreakingASkipDontUse.Name = "btnBreakingASkipDontUse";
            this.btnBreakingASkipDontUse.Size = new System.Drawing.Size(106, 26);
            this.btnBreakingASkipDontUse.TabIndex = 47;
            this.btnBreakingASkipDontUse.Text = "사용안함";
            this.btnBreakingASkipDontUse.UseVisualStyleBackColor = false;
            // 
            // txtBreakingASkip
            // 
            this.txtBreakingASkip.Location = new System.Drawing.Point(3, 3);
            this.txtBreakingASkip.Name = "txtBreakingASkip";
            this.txtBreakingASkip.Size = new System.Drawing.Size(177, 29);
            this.txtBreakingASkip.TabIndex = 0;
            // 
            // btnBreakingASkipUse
            // 
            this.btnBreakingASkipUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakingASkipUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakingASkipUse.ForeColor = System.Drawing.Color.White;
            this.btnBreakingASkipUse.Location = new System.Drawing.Point(186, 3);
            this.btnBreakingASkipUse.Name = "btnBreakingASkipUse";
            this.btnBreakingASkipUse.Size = new System.Drawing.Size(106, 26);
            this.btnBreakingASkipUse.TabIndex = 46;
            this.btnBreakingASkipUse.Text = "사용함";
            this.btnBreakingASkipUse.UseVisualStyleBackColor = false;
            // 
            // gxtProcessBSkip
            // 
            this.gxtProcessBSkip.Controls.Add(this.tableLayoutPanel9);
            this.gxtProcessBSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtProcessBSkip.ForeColor = System.Drawing.Color.White;
            this.gxtProcessBSkip.Location = new System.Drawing.Point(428, 26);
            this.gxtProcessBSkip.Name = "gxtProcessBSkip";
            this.gxtProcessBSkip.Size = new System.Drawing.Size(419, 57);
            this.gxtProcessBSkip.TabIndex = 53;
            this.gxtProcessBSkip.TabStop = false;
            this.gxtProcessBSkip.Text = "     프로세스 B 물류 스킵     ";
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 3;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel9.Controls.Add(this.btnProcessBSkipDontUse, 2, 0);
            this.tableLayoutPanel9.Controls.Add(this.txtProcessBSkip, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.btnProcessBSkipUse, 1, 0);
            this.tableLayoutPanel9.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel9.TabIndex = 1;
            // 
            // btnProcessBSkipDontUse
            // 
            this.btnProcessBSkipDontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnProcessBSkipDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnProcessBSkipDontUse.ForeColor = System.Drawing.Color.White;
            this.btnProcessBSkipDontUse.Location = new System.Drawing.Point(298, 3);
            this.btnProcessBSkipDontUse.Name = "btnProcessBSkipDontUse";
            this.btnProcessBSkipDontUse.Size = new System.Drawing.Size(106, 26);
            this.btnProcessBSkipDontUse.TabIndex = 47;
            this.btnProcessBSkipDontUse.Text = "사용안함";
            this.btnProcessBSkipDontUse.UseVisualStyleBackColor = false;
            // 
            // txtProcessBSkip
            // 
            this.txtProcessBSkip.Location = new System.Drawing.Point(3, 3);
            this.txtProcessBSkip.Name = "txtProcessBSkip";
            this.txtProcessBSkip.Size = new System.Drawing.Size(177, 29);
            this.txtProcessBSkip.TabIndex = 0;
            // 
            // btnProcessBSkipUse
            // 
            this.btnProcessBSkipUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnProcessBSkipUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnProcessBSkipUse.ForeColor = System.Drawing.Color.White;
            this.btnProcessBSkipUse.Location = new System.Drawing.Point(186, 3);
            this.btnProcessBSkipUse.Name = "btnProcessBSkipUse";
            this.btnProcessBSkipUse.Size = new System.Drawing.Size(106, 26);
            this.btnProcessBSkipUse.TabIndex = 46;
            this.btnProcessBSkipUse.Text = "사용함";
            this.btnProcessBSkipUse.UseVisualStyleBackColor = false;
            // 
            // gxtProcessASkip
            // 
            this.gxtProcessASkip.Controls.Add(this.tableLayoutPanel8);
            this.gxtProcessASkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtProcessASkip.ForeColor = System.Drawing.Color.White;
            this.gxtProcessASkip.Location = new System.Drawing.Point(3, 26);
            this.gxtProcessASkip.Name = "gxtProcessASkip";
            this.gxtProcessASkip.Size = new System.Drawing.Size(419, 57);
            this.gxtProcessASkip.TabIndex = 53;
            this.gxtProcessASkip.TabStop = false;
            this.gxtProcessASkip.Text = "     프로세스 A 물류 스킵     ";
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 3;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel8.Controls.Add(this.btnProcessASkipDontUse, 2, 0);
            this.tableLayoutPanel8.Controls.Add(this.txtProcessASkip, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.btnProcessASkipUse, 1, 0);
            this.tableLayoutPanel8.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel8.TabIndex = 1;
            // 
            // btnProcessASkipDontUse
            // 
            this.btnProcessASkipDontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnProcessASkipDontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnProcessASkipDontUse.ForeColor = System.Drawing.Color.White;
            this.btnProcessASkipDontUse.Location = new System.Drawing.Point(298, 3);
            this.btnProcessASkipDontUse.Name = "btnProcessASkipDontUse";
            this.btnProcessASkipDontUse.Size = new System.Drawing.Size(106, 26);
            this.btnProcessASkipDontUse.TabIndex = 47;
            this.btnProcessASkipDontUse.Text = "사용안함";
            this.btnProcessASkipDontUse.UseVisualStyleBackColor = false;
            // 
            // txtProcessASkip
            // 
            this.txtProcessASkip.Location = new System.Drawing.Point(3, 3);
            this.txtProcessASkip.Name = "txtProcessASkip";
            this.txtProcessASkip.Size = new System.Drawing.Size(177, 29);
            this.txtProcessASkip.TabIndex = 0;
            // 
            // btnProcessASkipUse
            // 
            this.btnProcessASkipUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnProcessASkipUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnProcessASkipUse.ForeColor = System.Drawing.Color.White;
            this.btnProcessASkipUse.Location = new System.Drawing.Point(186, 3);
            this.btnProcessASkipUse.Name = "btnProcessASkipUse";
            this.btnProcessASkipUse.Size = new System.Drawing.Size(106, 26);
            this.btnProcessASkipUse.TabIndex = 46;
            this.btnProcessASkipUse.Text = "사용함";
            this.btnProcessASkipUse.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel32, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel33, 0, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(875, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.513872F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 93.48613F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(862, 821);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // tableLayoutPanel32
            // 
            this.tableLayoutPanel32.BackColor = System.Drawing.SystemColors.Highlight;
            this.tableLayoutPanel32.ColumnCount = 3;
            this.tableLayoutPanel32.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.5F));
            this.tableLayoutPanel32.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.01746F));
            this.tableLayoutPanel32.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.49127F));
            this.tableLayoutPanel32.Controls.Add(this.lbl_Setting, 1, 0);
            this.tableLayoutPanel32.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel32.Name = "tableLayoutPanel32";
            this.tableLayoutPanel32.RowCount = 1;
            this.tableLayoutPanel32.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel32.Size = new System.Drawing.Size(856, 47);
            this.tableLayoutPanel32.TabIndex = 1;
            // 
            // lbl_Setting
            // 
            this.lbl_Setting.AutoSize = true;
            this.lbl_Setting.Font = new System.Drawing.Font("맑은 고딕", 14F, System.Drawing.FontStyle.Bold);
            this.lbl_Setting.Location = new System.Drawing.Point(398, 10);
            this.lbl_Setting.Margin = new System.Windows.Forms.Padding(35, 10, 3, 0);
            this.lbl_Setting.Name = "lbl_Setting";
            this.lbl_Setting.Size = new System.Drawing.Size(76, 25);
            this.lbl_Setting.TabIndex = 1;
            this.lbl_Setting.Text = "Setting";
            // 
            // tableLayoutPanel33
            // 
            this.tableLayoutPanel33.ColumnCount = 2;
            this.tableLayoutPanel33.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel33.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel33.Controls.Add(this.tableLayoutPanel34, 0, 0);
            this.tableLayoutPanel33.Controls.Add(this.tableLayoutPanel41, 1, 0);
            this.tableLayoutPanel33.Location = new System.Drawing.Point(3, 56);
            this.tableLayoutPanel33.Name = "tableLayoutPanel33";
            this.tableLayoutPanel33.RowCount = 1;
            this.tableLayoutPanel33.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel33.Size = new System.Drawing.Size(856, 762);
            this.tableLayoutPanel33.TabIndex = 2;
            // 
            // tableLayoutPanel34
            // 
            this.tableLayoutPanel34.ColumnCount = 1;
            this.tableLayoutPanel34.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel34.Controls.Add(this.gxtAutoDeleteCycle, 0, 5);
            this.tableLayoutPanel34.Controls.Add(this.gxtBreakingCount, 0, 4);
            this.tableLayoutPanel34.Controls.Add(this.gxtDoorOpenSpeed, 0, 3);
            this.tableLayoutPanel34.Controls.Add(this.gxtCountError, 0, 2);
            this.tableLayoutPanel34.Controls.Add(this.gxtTimeout, 0, 1);
            this.tableLayoutPanel34.Controls.Add(this.gxtSetting_DistributionMode, 0, 0);
            this.tableLayoutPanel34.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel34.Name = "tableLayoutPanel34";
            this.tableLayoutPanel34.RowCount = 6;
            this.tableLayoutPanel34.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 129F));
            this.tableLayoutPanel34.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 269F));
            this.tableLayoutPanel34.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 101F));
            this.tableLayoutPanel34.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 66F));
            this.tableLayoutPanel34.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 106F));
            this.tableLayoutPanel34.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 17F));
            this.tableLayoutPanel34.Size = new System.Drawing.Size(422, 735);
            this.tableLayoutPanel34.TabIndex = 3;
            // 
            // gxtAutoDeleteCycle
            // 
            this.gxtAutoDeleteCycle.Controls.Add(this.tableLayoutPanel40);
            this.gxtAutoDeleteCycle.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtAutoDeleteCycle.ForeColor = System.Drawing.Color.White;
            this.gxtAutoDeleteCycle.Location = new System.Drawing.Point(3, 674);
            this.gxtAutoDeleteCycle.Name = "gxtAutoDeleteCycle";
            this.gxtAutoDeleteCycle.Size = new System.Drawing.Size(416, 58);
            this.gxtAutoDeleteCycle.TabIndex = 59;
            this.gxtAutoDeleteCycle.TabStop = false;
            this.gxtAutoDeleteCycle.Text = "     Auto Delete 주기    ";
            // 
            // tableLayoutPanel40
            // 
            this.tableLayoutPanel40.ColumnCount = 2;
            this.tableLayoutPanel40.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel40.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel40.Controls.Add(this.txtAutoDeleteCycle, 1, 0);
            this.tableLayoutPanel40.Controls.Add(this.lbl_AutoDeleteCycle, 0, 0);
            this.tableLayoutPanel40.Location = new System.Drawing.Point(6, 22);
            this.tableLayoutPanel40.Name = "tableLayoutPanel40";
            this.tableLayoutPanel40.RowCount = 1;
            this.tableLayoutPanel40.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel40.Size = new System.Drawing.Size(404, 30);
            this.tableLayoutPanel40.TabIndex = 1;
            // 
            // txtAutoDeleteCycle
            // 
            this.txtAutoDeleteCycle.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAutoDeleteCycle.Location = new System.Drawing.Point(205, 3);
            this.txtAutoDeleteCycle.Name = "txtAutoDeleteCycle";
            this.txtAutoDeleteCycle.Size = new System.Drawing.Size(196, 22);
            this.txtAutoDeleteCycle.TabIndex = 0;
            // 
            // lbl_AutoDeleteCycle
            // 
            this.lbl_AutoDeleteCycle.AutoSize = true;
            this.lbl_AutoDeleteCycle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_AutoDeleteCycle.Location = new System.Drawing.Point(3, 6);
            this.lbl_AutoDeleteCycle.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_AutoDeleteCycle.Name = "lbl_AutoDeleteCycle";
            this.lbl_AutoDeleteCycle.Size = new System.Drawing.Size(145, 15);
            this.lbl_AutoDeleteCycle.TabIndex = 2;
            this.lbl_AutoDeleteCycle.Text = "Auto Delete 주기[Day] : ";
            // 
            // gxtBreakingCount
            // 
            this.gxtBreakingCount.Controls.Add(this.tableLayoutPanel39);
            this.gxtBreakingCount.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtBreakingCount.ForeColor = System.Drawing.Color.White;
            this.gxtBreakingCount.Location = new System.Drawing.Point(3, 568);
            this.gxtBreakingCount.Name = "gxtBreakingCount";
            this.gxtBreakingCount.Size = new System.Drawing.Size(416, 95);
            this.gxtBreakingCount.TabIndex = 58;
            this.gxtBreakingCount.TabStop = false;
            this.gxtBreakingCount.Text = "     브레이킹 횟수    ";
            // 
            // tableLayoutPanel39
            // 
            this.tableLayoutPanel39.ColumnCount = 2;
            this.tableLayoutPanel39.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel39.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel39.Controls.Add(this.lbl_CriticalAlarm, 0, 1);
            this.tableLayoutPanel39.Controls.Add(this.txtLightAlarm, 1, 0);
            this.tableLayoutPanel39.Controls.Add(this.lbl_LightAlarm, 0, 0);
            this.tableLayoutPanel39.Controls.Add(this.txtCriticalAlarm, 1, 1);
            this.tableLayoutPanel39.Location = new System.Drawing.Point(6, 22);
            this.tableLayoutPanel39.Name = "tableLayoutPanel39";
            this.tableLayoutPanel39.RowCount = 2;
            this.tableLayoutPanel39.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel39.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel39.Size = new System.Drawing.Size(404, 67);
            this.tableLayoutPanel39.TabIndex = 1;
            // 
            // lbl_CriticalAlarm
            // 
            this.lbl_CriticalAlarm.AutoSize = true;
            this.lbl_CriticalAlarm.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_CriticalAlarm.Location = new System.Drawing.Point(3, 39);
            this.lbl_CriticalAlarm.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_CriticalAlarm.Name = "lbl_CriticalAlarm";
            this.lbl_CriticalAlarm.Size = new System.Drawing.Size(90, 15);
            this.lbl_CriticalAlarm.TabIndex = 3;
            this.lbl_CriticalAlarm.Text = "중알림[횟수] :  ";
            // 
            // txtLightAlarm
            // 
            this.txtLightAlarm.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLightAlarm.Location = new System.Drawing.Point(205, 3);
            this.txtLightAlarm.Name = "txtLightAlarm";
            this.txtLightAlarm.Size = new System.Drawing.Size(196, 22);
            this.txtLightAlarm.TabIndex = 0;
            // 
            // lbl_LightAlarm
            // 
            this.lbl_LightAlarm.AutoSize = true;
            this.lbl_LightAlarm.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_LightAlarm.Location = new System.Drawing.Point(3, 6);
            this.lbl_LightAlarm.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_LightAlarm.Name = "lbl_LightAlarm";
            this.lbl_LightAlarm.Size = new System.Drawing.Size(86, 15);
            this.lbl_LightAlarm.TabIndex = 2;
            this.lbl_LightAlarm.Text = "경알림[횟수] : ";
            // 
            // txtCriticalAlarm
            // 
            this.txtCriticalAlarm.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCriticalAlarm.Location = new System.Drawing.Point(205, 36);
            this.txtCriticalAlarm.Name = "txtCriticalAlarm";
            this.txtCriticalAlarm.Size = new System.Drawing.Size(196, 22);
            this.txtCriticalAlarm.TabIndex = 1;
            // 
            // gxtDoorOpenSpeed
            // 
            this.gxtDoorOpenSpeed.Controls.Add(this.tableLayoutPanel38);
            this.gxtDoorOpenSpeed.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtDoorOpenSpeed.ForeColor = System.Drawing.Color.White;
            this.gxtDoorOpenSpeed.Location = new System.Drawing.Point(3, 502);
            this.gxtDoorOpenSpeed.Name = "gxtDoorOpenSpeed";
            this.gxtDoorOpenSpeed.Size = new System.Drawing.Size(416, 60);
            this.gxtDoorOpenSpeed.TabIndex = 57;
            this.gxtDoorOpenSpeed.TabStop = false;
            this.gxtDoorOpenSpeed.Text = "     도어 오픈 스피드    ";
            // 
            // tableLayoutPanel38
            // 
            this.tableLayoutPanel38.ColumnCount = 2;
            this.tableLayoutPanel38.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel38.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel38.Controls.Add(this.txtDoorOpenSpeed, 1, 0);
            this.tableLayoutPanel38.Controls.Add(this.lbl_DoorOpenSpped, 0, 0);
            this.tableLayoutPanel38.Location = new System.Drawing.Point(6, 22);
            this.tableLayoutPanel38.Name = "tableLayoutPanel38";
            this.tableLayoutPanel38.RowCount = 1;
            this.tableLayoutPanel38.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel38.Size = new System.Drawing.Size(404, 30);
            this.tableLayoutPanel38.TabIndex = 1;
            // 
            // txtDoorOpenSpeed
            // 
            this.txtDoorOpenSpeed.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDoorOpenSpeed.Location = new System.Drawing.Point(205, 3);
            this.txtDoorOpenSpeed.Name = "txtDoorOpenSpeed";
            this.txtDoorOpenSpeed.Size = new System.Drawing.Size(196, 22);
            this.txtDoorOpenSpeed.TabIndex = 0;
            // 
            // lbl_DoorOpenSpped
            // 
            this.lbl_DoorOpenSpped.AutoSize = true;
            this.lbl_DoorOpenSpped.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_DoorOpenSpped.Location = new System.Drawing.Point(3, 6);
            this.lbl_DoorOpenSpped.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_DoorOpenSpped.Name = "lbl_DoorOpenSpped";
            this.lbl_DoorOpenSpped.Size = new System.Drawing.Size(176, 15);
            this.lbl_DoorOpenSpped.TabIndex = 2;
            this.lbl_DoorOpenSpped.Text = "Door Open Speed[mm/sec] : ";
            // 
            // gxtCountError
            // 
            this.gxtCountError.Controls.Add(this.tableLayoutPanel37);
            this.gxtCountError.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCountError.ForeColor = System.Drawing.Color.White;
            this.gxtCountError.Location = new System.Drawing.Point(3, 401);
            this.gxtCountError.Name = "gxtCountError";
            this.gxtCountError.Size = new System.Drawing.Size(416, 95);
            this.gxtCountError.TabIndex = 56;
            this.gxtCountError.TabStop = false;
            this.gxtCountError.Text = "     Count Error    ";
            // 
            // tableLayoutPanel37
            // 
            this.tableLayoutPanel37.ColumnCount = 2;
            this.tableLayoutPanel37.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel37.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel37.Controls.Add(this.lbl_INSP, 0, 1);
            this.tableLayoutPanel37.Controls.Add(this.txtMCR, 1, 0);
            this.tableLayoutPanel37.Controls.Add(this.lbl_MCR, 0, 0);
            this.tableLayoutPanel37.Controls.Add(this.txtINSP, 1, 1);
            this.tableLayoutPanel37.Location = new System.Drawing.Point(6, 22);
            this.tableLayoutPanel37.Name = "tableLayoutPanel37";
            this.tableLayoutPanel37.RowCount = 2;
            this.tableLayoutPanel37.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel37.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel37.Size = new System.Drawing.Size(404, 67);
            this.tableLayoutPanel37.TabIndex = 1;
            // 
            // lbl_INSP
            // 
            this.lbl_INSP.AutoSize = true;
            this.lbl_INSP.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_INSP.Location = new System.Drawing.Point(3, 39);
            this.lbl_INSP.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_INSP.Name = "lbl_INSP";
            this.lbl_INSP.Size = new System.Drawing.Size(50, 15);
            this.lbl_INSP.TabIndex = 3;
            this.lbl_INSP.Text = "INSP :  ";
            // 
            // txtMCR
            // 
            this.txtMCR.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMCR.Location = new System.Drawing.Point(205, 3);
            this.txtMCR.Name = "txtMCR";
            this.txtMCR.Size = new System.Drawing.Size(196, 22);
            this.txtMCR.TabIndex = 0;
            // 
            // lbl_MCR
            // 
            this.lbl_MCR.AutoSize = true;
            this.lbl_MCR.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_MCR.Location = new System.Drawing.Point(3, 6);
            this.lbl_MCR.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_MCR.Name = "lbl_MCR";
            this.lbl_MCR.Size = new System.Drawing.Size(46, 15);
            this.lbl_MCR.TabIndex = 2;
            this.lbl_MCR.Text = "MCR : ";
            // 
            // txtINSP
            // 
            this.txtINSP.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtINSP.Location = new System.Drawing.Point(205, 36);
            this.txtINSP.Name = "txtINSP";
            this.txtINSP.Size = new System.Drawing.Size(196, 22);
            this.txtINSP.TabIndex = 1;
            // 
            // gxtTimeout
            // 
            this.gxtTimeout.Controls.Add(this.tableLayoutPanel36);
            this.gxtTimeout.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtTimeout.ForeColor = System.Drawing.Color.White;
            this.gxtTimeout.Location = new System.Drawing.Point(3, 132);
            this.gxtTimeout.Name = "gxtTimeout";
            this.gxtTimeout.Size = new System.Drawing.Size(416, 263);
            this.gxtTimeout.TabIndex = 55;
            this.gxtTimeout.TabStop = false;
            this.gxtTimeout.Text = "     타임아웃    ";
            // 
            // tableLayoutPanel36
            // 
            this.tableLayoutPanel36.ColumnCount = 2;
            this.tableLayoutPanel36.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel36.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel36.Controls.Add(this.lbl_MutingTimeout, 0, 6);
            this.tableLayoutPanel36.Controls.Add(this.txtVacuumTimeout, 1, 0);
            this.tableLayoutPanel36.Controls.Add(this.lbl_VacuumTimeout, 0, 0);
            this.tableLayoutPanel36.Controls.Add(this.txtCylinderTimeout, 1, 1);
            this.tableLayoutPanel36.Controls.Add(this.txtPreAlignTimeout, 1, 2);
            this.tableLayoutPanel36.Controls.Add(this.txtBreakingAlignTimeout, 1, 3);
            this.tableLayoutPanel36.Controls.Add(this.txtInspectiongTimeout, 1, 4);
            this.tableLayoutPanel36.Controls.Add(this.txtSequenceMoveTimeout, 1, 5);
            this.tableLayoutPanel36.Controls.Add(this.txtMutingTimeout, 1, 6);
            this.tableLayoutPanel36.Controls.Add(this.lbl_CylinderTimeout, 0, 1);
            this.tableLayoutPanel36.Controls.Add(this.lbl_PreAlignTimeout, 0, 2);
            this.tableLayoutPanel36.Controls.Add(this.lbl_BreakingAlignTimeout, 0, 3);
            this.tableLayoutPanel36.Controls.Add(this.lbl_InspectionTimeout, 0, 4);
            this.tableLayoutPanel36.Controls.Add(this.lbl_SequenceMoveTimeout, 0, 5);
            this.tableLayoutPanel36.Location = new System.Drawing.Point(6, 22);
            this.tableLayoutPanel36.Name = "tableLayoutPanel36";
            this.tableLayoutPanel36.RowCount = 7;
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel36.Size = new System.Drawing.Size(404, 235);
            this.tableLayoutPanel36.TabIndex = 1;
            // 
            // lbl_MutingTimeout
            // 
            this.lbl_MutingTimeout.AutoSize = true;
            this.lbl_MutingTimeout.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_MutingTimeout.Location = new System.Drawing.Point(3, 204);
            this.lbl_MutingTimeout.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_MutingTimeout.Name = "lbl_MutingTimeout";
            this.lbl_MutingTimeout.Size = new System.Drawing.Size(125, 15);
            this.lbl_MutingTimeout.TabIndex = 22;
            this.lbl_MutingTimeout.Text = "Muting Timeout[s] : ";
            // 
            // txtVacuumTimeout
            // 
            this.txtVacuumTimeout.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtVacuumTimeout.Location = new System.Drawing.Point(205, 6);
            this.txtVacuumTimeout.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.txtVacuumTimeout.Name = "txtVacuumTimeout";
            this.txtVacuumTimeout.Size = new System.Drawing.Size(196, 22);
            this.txtVacuumTimeout.TabIndex = 0;
            // 
            // lbl_VacuumTimeout
            // 
            this.lbl_VacuumTimeout.AutoSize = true;
            this.lbl_VacuumTimeout.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_VacuumTimeout.Location = new System.Drawing.Point(3, 6);
            this.lbl_VacuumTimeout.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_VacuumTimeout.Name = "lbl_VacuumTimeout";
            this.lbl_VacuumTimeout.Size = new System.Drawing.Size(129, 15);
            this.lbl_VacuumTimeout.TabIndex = 2;
            this.lbl_VacuumTimeout.Text = "Vacuum Timeout[s] : ";
            // 
            // txtCylinderTimeout
            // 
            this.txtCylinderTimeout.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCylinderTimeout.Location = new System.Drawing.Point(205, 39);
            this.txtCylinderTimeout.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.txtCylinderTimeout.Name = "txtCylinderTimeout";
            this.txtCylinderTimeout.Size = new System.Drawing.Size(196, 22);
            this.txtCylinderTimeout.TabIndex = 1;
            // 
            // txtPreAlignTimeout
            // 
            this.txtPreAlignTimeout.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPreAlignTimeout.Location = new System.Drawing.Point(205, 72);
            this.txtPreAlignTimeout.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.txtPreAlignTimeout.Name = "txtPreAlignTimeout";
            this.txtPreAlignTimeout.Size = new System.Drawing.Size(196, 22);
            this.txtPreAlignTimeout.TabIndex = 2;
            // 
            // txtBreakingAlignTimeout
            // 
            this.txtBreakingAlignTimeout.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtBreakingAlignTimeout.Location = new System.Drawing.Point(205, 105);
            this.txtBreakingAlignTimeout.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.txtBreakingAlignTimeout.Name = "txtBreakingAlignTimeout";
            this.txtBreakingAlignTimeout.Size = new System.Drawing.Size(196, 22);
            this.txtBreakingAlignTimeout.TabIndex = 13;
            // 
            // txtInspectiongTimeout
            // 
            this.txtInspectiongTimeout.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtInspectiongTimeout.Location = new System.Drawing.Point(205, 138);
            this.txtInspectiongTimeout.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.txtInspectiongTimeout.Name = "txtInspectiongTimeout";
            this.txtInspectiongTimeout.Size = new System.Drawing.Size(196, 22);
            this.txtInspectiongTimeout.TabIndex = 14;
            // 
            // txtSequenceMoveTimeout
            // 
            this.txtSequenceMoveTimeout.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSequenceMoveTimeout.Location = new System.Drawing.Point(205, 171);
            this.txtSequenceMoveTimeout.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.txtSequenceMoveTimeout.Name = "txtSequenceMoveTimeout";
            this.txtSequenceMoveTimeout.Size = new System.Drawing.Size(196, 22);
            this.txtSequenceMoveTimeout.TabIndex = 15;
            // 
            // txtMutingTimeout
            // 
            this.txtMutingTimeout.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMutingTimeout.Location = new System.Drawing.Point(205, 204);
            this.txtMutingTimeout.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.txtMutingTimeout.Name = "txtMutingTimeout";
            this.txtMutingTimeout.Size = new System.Drawing.Size(196, 22);
            this.txtMutingTimeout.TabIndex = 16;
            // 
            // lbl_CylinderTimeout
            // 
            this.lbl_CylinderTimeout.AutoSize = true;
            this.lbl_CylinderTimeout.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_CylinderTimeout.Location = new System.Drawing.Point(3, 39);
            this.lbl_CylinderTimeout.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_CylinderTimeout.Name = "lbl_CylinderTimeout";
            this.lbl_CylinderTimeout.Size = new System.Drawing.Size(130, 15);
            this.lbl_CylinderTimeout.TabIndex = 17;
            this.lbl_CylinderTimeout.Text = "Cylinder Timeout[s] : ";
            // 
            // lbl_PreAlignTimeout
            // 
            this.lbl_PreAlignTimeout.AutoSize = true;
            this.lbl_PreAlignTimeout.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_PreAlignTimeout.Location = new System.Drawing.Point(3, 72);
            this.lbl_PreAlignTimeout.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_PreAlignTimeout.Name = "lbl_PreAlignTimeout";
            this.lbl_PreAlignTimeout.Size = new System.Drawing.Size(131, 15);
            this.lbl_PreAlignTimeout.TabIndex = 18;
            this.lbl_PreAlignTimeout.Text = "PreAlign Timeout[s] : ";
            // 
            // lbl_BreakingAlignTimeout
            // 
            this.lbl_BreakingAlignTimeout.AutoSize = true;
            this.lbl_BreakingAlignTimeout.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_BreakingAlignTimeout.Location = new System.Drawing.Point(3, 105);
            this.lbl_BreakingAlignTimeout.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_BreakingAlignTimeout.Name = "lbl_BreakingAlignTimeout";
            this.lbl_BreakingAlignTimeout.Size = new System.Drawing.Size(164, 15);
            this.lbl_BreakingAlignTimeout.TabIndex = 19;
            this.lbl_BreakingAlignTimeout.Text = "BreakingAilgn Timeout[s] : ";
            // 
            // lbl_InspectionTimeout
            // 
            this.lbl_InspectionTimeout.AutoSize = true;
            this.lbl_InspectionTimeout.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_InspectionTimeout.Location = new System.Drawing.Point(3, 138);
            this.lbl_InspectionTimeout.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_InspectionTimeout.Name = "lbl_InspectionTimeout";
            this.lbl_InspectionTimeout.Size = new System.Drawing.Size(143, 15);
            this.lbl_InspectionTimeout.TabIndex = 20;
            this.lbl_InspectionTimeout.Text = "Inspection Timeout[s] : ";
            // 
            // lbl_SequenceMoveTimeout
            // 
            this.lbl_SequenceMoveTimeout.AutoSize = true;
            this.lbl_SequenceMoveTimeout.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_SequenceMoveTimeout.Location = new System.Drawing.Point(3, 171);
            this.lbl_SequenceMoveTimeout.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_SequenceMoveTimeout.Name = "lbl_SequenceMoveTimeout";
            this.lbl_SequenceMoveTimeout.Size = new System.Drawing.Size(175, 15);
            this.lbl_SequenceMoveTimeout.TabIndex = 21;
            this.lbl_SequenceMoveTimeout.Text = "Sequence Move Timeout[s] : ";
            // 
            // gxtSetting_DistributionMode
            // 
            this.gxtSetting_DistributionMode.Controls.Add(this.tableLayoutPanel35);
            this.gxtSetting_DistributionMode.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtSetting_DistributionMode.ForeColor = System.Drawing.Color.White;
            this.gxtSetting_DistributionMode.Location = new System.Drawing.Point(3, 3);
            this.gxtSetting_DistributionMode.Name = "gxtSetting_DistributionMode";
            this.gxtSetting_DistributionMode.Size = new System.Drawing.Size(416, 123);
            this.gxtSetting_DistributionMode.TabIndex = 54;
            this.gxtSetting_DistributionMode.TabStop = false;
            this.gxtSetting_DistributionMode.Text = "     물류 모드    ";
            // 
            // tableLayoutPanel35
            // 
            this.tableLayoutPanel35.ColumnCount = 2;
            this.tableLayoutPanel35.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel35.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel35.Controls.Add(this.lbl_BreakingAlignSpeed, 0, 2);
            this.tableLayoutPanel35.Controls.Add(this.lbl_FineAlignSpeed, 0, 1);
            this.tableLayoutPanel35.Controls.Add(this.txtPreAlignSpeed, 1, 0);
            this.tableLayoutPanel35.Controls.Add(this.lbl_PreAlignSpeed, 0, 0);
            this.tableLayoutPanel35.Controls.Add(this.txtFineAlignSpeed, 1, 1);
            this.tableLayoutPanel35.Controls.Add(this.txtBreakingAlignSpeed, 1, 2);
            this.tableLayoutPanel35.Location = new System.Drawing.Point(6, 22);
            this.tableLayoutPanel35.Name = "tableLayoutPanel35";
            this.tableLayoutPanel35.RowCount = 3;
            this.tableLayoutPanel35.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel35.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel35.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel35.Size = new System.Drawing.Size(404, 95);
            this.tableLayoutPanel35.TabIndex = 1;
            // 
            // lbl_BreakingAlignSpeed
            // 
            this.lbl_BreakingAlignSpeed.AutoSize = true;
            this.lbl_BreakingAlignSpeed.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_BreakingAlignSpeed.Location = new System.Drawing.Point(3, 68);
            this.lbl_BreakingAlignSpeed.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_BreakingAlignSpeed.Name = "lbl_BreakingAlignSpeed";
            this.lbl_BreakingAlignSpeed.Size = new System.Drawing.Size(194, 15);
            this.lbl_BreakingAlignSpeed.TabIndex = 4;
            this.lbl_BreakingAlignSpeed.Text = "BreakingAlign Speed[mm/sec] : ";
            // 
            // lbl_FineAlignSpeed
            // 
            this.lbl_FineAlignSpeed.AutoSize = true;
            this.lbl_FineAlignSpeed.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_FineAlignSpeed.Location = new System.Drawing.Point(3, 37);
            this.lbl_FineAlignSpeed.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_FineAlignSpeed.Name = "lbl_FineAlignSpeed";
            this.lbl_FineAlignSpeed.Size = new System.Drawing.Size(165, 15);
            this.lbl_FineAlignSpeed.TabIndex = 3;
            this.lbl_FineAlignSpeed.Text = "FineAlign Speed[mm/sec] : ";
            // 
            // txtPreAlignSpeed
            // 
            this.txtPreAlignSpeed.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPreAlignSpeed.Location = new System.Drawing.Point(205, 3);
            this.txtPreAlignSpeed.Name = "txtPreAlignSpeed";
            this.txtPreAlignSpeed.Size = new System.Drawing.Size(196, 22);
            this.txtPreAlignSpeed.TabIndex = 0;
            // 
            // lbl_PreAlignSpeed
            // 
            this.lbl_PreAlignSpeed.AutoSize = true;
            this.lbl_PreAlignSpeed.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_PreAlignSpeed.Location = new System.Drawing.Point(3, 6);
            this.lbl_PreAlignSpeed.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_PreAlignSpeed.Name = "lbl_PreAlignSpeed";
            this.lbl_PreAlignSpeed.Size = new System.Drawing.Size(161, 15);
            this.lbl_PreAlignSpeed.TabIndex = 2;
            this.lbl_PreAlignSpeed.Text = "PreAlign Speed[mm/sec] : ";
            // 
            // txtFineAlignSpeed
            // 
            this.txtFineAlignSpeed.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtFineAlignSpeed.Location = new System.Drawing.Point(205, 34);
            this.txtFineAlignSpeed.Name = "txtFineAlignSpeed";
            this.txtFineAlignSpeed.Size = new System.Drawing.Size(196, 22);
            this.txtFineAlignSpeed.TabIndex = 1;
            // 
            // txtBreakingAlignSpeed
            // 
            this.txtBreakingAlignSpeed.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtBreakingAlignSpeed.Location = new System.Drawing.Point(205, 65);
            this.txtBreakingAlignSpeed.Name = "txtBreakingAlignSpeed";
            this.txtBreakingAlignSpeed.Size = new System.Drawing.Size(196, 22);
            this.txtBreakingAlignSpeed.TabIndex = 2;
            // 
            // tableLayoutPanel41
            // 
            this.tableLayoutPanel41.ColumnCount = 1;
            this.tableLayoutPanel41.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel41.Controls.Add(this.gxtLDS, 0, 6);
            this.tableLayoutPanel41.Controls.Add(this.gxtTemperatureAlarm, 0, 0);
            this.tableLayoutPanel41.Controls.Add(this.gxtMeasureCycle, 0, 1);
            this.tableLayoutPanel41.Controls.Add(this.gxtBlowingCheck, 0, 2);
            this.tableLayoutPanel41.Controls.Add(this.gxtRetryCount, 0, 3);
            this.tableLayoutPanel41.Controls.Add(this.gxtLaserPD7Error, 0, 4);
            this.tableLayoutPanel41.Controls.Add(this.gxtLaserPowerMeasure, 0, 5);
            this.tableLayoutPanel41.Location = new System.Drawing.Point(431, 3);
            this.tableLayoutPanel41.Name = "tableLayoutPanel41";
            this.tableLayoutPanel41.RowCount = 7;
            this.tableLayoutPanel41.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel41.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel41.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel41.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 61F));
            this.tableLayoutPanel41.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 98F));
            this.tableLayoutPanel41.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 267F));
            this.tableLayoutPanel41.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 106F));
            this.tableLayoutPanel41.Size = new System.Drawing.Size(422, 756);
            this.tableLayoutPanel41.TabIndex = 4;
            // 
            // gxtLDS
            // 
            this.gxtLDS.Controls.Add(this.tableLayoutPanel48);
            this.gxtLDS.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtLDS.ForeColor = System.Drawing.Color.White;
            this.gxtLDS.Location = new System.Drawing.Point(3, 613);
            this.gxtLDS.Name = "gxtLDS";
            this.gxtLDS.Size = new System.Drawing.Size(416, 140);
            this.gxtLDS.TabIndex = 64;
            this.gxtLDS.TabStop = false;
            this.gxtLDS.Text = "     LDS    ";
            // 
            // tableLayoutPanel48
            // 
            this.tableLayoutPanel48.ColumnCount = 2;
            this.tableLayoutPanel48.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel48.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel48.Controls.Add(this.lbl_ErrorRange, 0, 3);
            this.tableLayoutPanel48.Controls.Add(this.lbl_WaitingTime, 0, 1);
            this.tableLayoutPanel48.Controls.Add(this.txtLDS_MeasureCycle, 1, 0);
            this.tableLayoutPanel48.Controls.Add(this.lbl_LDS_MeasureCycle, 0, 0);
            this.tableLayoutPanel48.Controls.Add(this.txtWaitingTime, 1, 1);
            this.tableLayoutPanel48.Controls.Add(this.lbl_LDS_MeasureTime, 0, 2);
            this.tableLayoutPanel48.Controls.Add(this.txtLDS_MeasureTime, 1, 2);
            this.tableLayoutPanel48.Controls.Add(this.txtErrorRange, 1, 3);
            this.tableLayoutPanel48.Location = new System.Drawing.Point(6, 22);
            this.tableLayoutPanel48.Name = "tableLayoutPanel48";
            this.tableLayoutPanel48.RowCount = 4;
            this.tableLayoutPanel48.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel48.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel48.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel48.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel48.Size = new System.Drawing.Size(404, 112);
            this.tableLayoutPanel48.TabIndex = 1;
            // 
            // lbl_ErrorRange
            // 
            this.lbl_ErrorRange.AutoSize = true;
            this.lbl_ErrorRange.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_ErrorRange.Location = new System.Drawing.Point(3, 90);
            this.lbl_ErrorRange.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_ErrorRange.Name = "lbl_ErrorRange";
            this.lbl_ErrorRange.Size = new System.Drawing.Size(100, 15);
            this.lbl_ErrorRange.TabIndex = 5;
            this.lbl_ErrorRange.Text = "오차 범위[mm] : ";
            // 
            // lbl_WaitingTime
            // 
            this.lbl_WaitingTime.AutoSize = true;
            this.lbl_WaitingTime.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_WaitingTime.Location = new System.Drawing.Point(3, 34);
            this.lbl_WaitingTime.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_WaitingTime.Name = "lbl_WaitingTime";
            this.lbl_WaitingTime.Size = new System.Drawing.Size(113, 15);
            this.lbl_WaitingTime.TabIndex = 3;
            this.lbl_WaitingTime.Text = "Waiting Time[s] :  ";
            // 
            // txtLDS_MeasureCycle
            // 
            this.txtLDS_MeasureCycle.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLDS_MeasureCycle.Location = new System.Drawing.Point(205, 3);
            this.txtLDS_MeasureCycle.Name = "txtLDS_MeasureCycle";
            this.txtLDS_MeasureCycle.Size = new System.Drawing.Size(196, 22);
            this.txtLDS_MeasureCycle.TabIndex = 0;
            // 
            // lbl_LDS_MeasureCycle
            // 
            this.lbl_LDS_MeasureCycle.AutoSize = true;
            this.lbl_LDS_MeasureCycle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_LDS_MeasureCycle.Location = new System.Drawing.Point(3, 6);
            this.lbl_LDS_MeasureCycle.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_LDS_MeasureCycle.Name = "lbl_LDS_MeasureCycle";
            this.lbl_LDS_MeasureCycle.Size = new System.Drawing.Size(85, 15);
            this.lbl_LDS_MeasureCycle.TabIndex = 2;
            this.lbl_LDS_MeasureCycle.Text = "측정 주기[h] : ";
            // 
            // txtWaitingTime
            // 
            this.txtWaitingTime.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtWaitingTime.Location = new System.Drawing.Point(205, 31);
            this.txtWaitingTime.Name = "txtWaitingTime";
            this.txtWaitingTime.Size = new System.Drawing.Size(196, 22);
            this.txtWaitingTime.TabIndex = 1;
            // 
            // lbl_LDS_MeasureTime
            // 
            this.lbl_LDS_MeasureTime.AutoSize = true;
            this.lbl_LDS_MeasureTime.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_LDS_MeasureTime.Location = new System.Drawing.Point(3, 62);
            this.lbl_LDS_MeasureTime.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_LDS_MeasureTime.Name = "lbl_LDS_MeasureTime";
            this.lbl_LDS_MeasureTime.Size = new System.Drawing.Size(88, 15);
            this.lbl_LDS_MeasureTime.TabIndex = 4;
            this.lbl_LDS_MeasureTime.Text = "측정 시간[s] :  ";
            // 
            // txtLDS_MeasureTime
            // 
            this.txtLDS_MeasureTime.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLDS_MeasureTime.Location = new System.Drawing.Point(205, 59);
            this.txtLDS_MeasureTime.Name = "txtLDS_MeasureTime";
            this.txtLDS_MeasureTime.Size = new System.Drawing.Size(196, 22);
            this.txtLDS_MeasureTime.TabIndex = 6;
            // 
            // txtErrorRange
            // 
            this.txtErrorRange.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtErrorRange.Location = new System.Drawing.Point(205, 87);
            this.txtErrorRange.Name = "txtErrorRange";
            this.txtErrorRange.Size = new System.Drawing.Size(196, 22);
            this.txtErrorRange.TabIndex = 7;
            // 
            // gxtTemperatureAlarm
            // 
            this.gxtTemperatureAlarm.Controls.Add(this.tableLayoutPanel42);
            this.gxtTemperatureAlarm.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtTemperatureAlarm.ForeColor = System.Drawing.Color.White;
            this.gxtTemperatureAlarm.Location = new System.Drawing.Point(3, 3);
            this.gxtTemperatureAlarm.Name = "gxtTemperatureAlarm";
            this.gxtTemperatureAlarm.Size = new System.Drawing.Size(416, 54);
            this.gxtTemperatureAlarm.TabIndex = 58;
            this.gxtTemperatureAlarm.TabStop = false;
            this.gxtTemperatureAlarm.Text = "     온도 알림    ";
            // 
            // tableLayoutPanel42
            // 
            this.tableLayoutPanel42.ColumnCount = 3;
            this.tableLayoutPanel42.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel42.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel42.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel42.Controls.Add(this.txtTemperatureLightAlarm, 0, 0);
            this.tableLayoutPanel42.Controls.Add(this.txtTemperatureCriticalAlarm, 1, 0);
            this.tableLayoutPanel42.Controls.Add(this.lbl_TemperatureAlarm, 0, 0);
            this.tableLayoutPanel42.Location = new System.Drawing.Point(6, 22);
            this.tableLayoutPanel42.Name = "tableLayoutPanel42";
            this.tableLayoutPanel42.RowCount = 1;
            this.tableLayoutPanel42.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel42.Size = new System.Drawing.Size(404, 30);
            this.tableLayoutPanel42.TabIndex = 1;
            // 
            // txtTemperatureLightAlarm
            // 
            this.txtTemperatureLightAlarm.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTemperatureLightAlarm.Location = new System.Drawing.Point(205, 3);
            this.txtTemperatureLightAlarm.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.txtTemperatureLightAlarm.Name = "txtTemperatureLightAlarm";
            this.txtTemperatureLightAlarm.Size = new System.Drawing.Size(95, 22);
            this.txtTemperatureLightAlarm.TabIndex = 3;
            // 
            // txtTemperatureCriticalAlarm
            // 
            this.txtTemperatureCriticalAlarm.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTemperatureCriticalAlarm.Location = new System.Drawing.Point(309, 3);
            this.txtTemperatureCriticalAlarm.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
            this.txtTemperatureCriticalAlarm.Name = "txtTemperatureCriticalAlarm";
            this.txtTemperatureCriticalAlarm.Size = new System.Drawing.Size(92, 22);
            this.txtTemperatureCriticalAlarm.TabIndex = 0;
            // 
            // lbl_TemperatureAlarm
            // 
            this.lbl_TemperatureAlarm.AutoSize = true;
            this.lbl_TemperatureAlarm.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_TemperatureAlarm.Location = new System.Drawing.Point(3, 6);
            this.lbl_TemperatureAlarm.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_TemperatureAlarm.Name = "lbl_TemperatureAlarm";
            this.lbl_TemperatureAlarm.Size = new System.Drawing.Size(114, 15);
            this.lbl_TemperatureAlarm.TabIndex = 2;
            this.lbl_TemperatureAlarm.Text = "경알람 / 중알람[ºC]";
            // 
            // gxtMeasureCycle
            // 
            this.gxtMeasureCycle.Controls.Add(this.tableLayoutPanel43);
            this.gxtMeasureCycle.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtMeasureCycle.ForeColor = System.Drawing.Color.White;
            this.gxtMeasureCycle.Location = new System.Drawing.Point(3, 65);
            this.gxtMeasureCycle.Name = "gxtMeasureCycle";
            this.gxtMeasureCycle.Size = new System.Drawing.Size(416, 56);
            this.gxtMeasureCycle.TabIndex = 59;
            this.gxtMeasureCycle.TabStop = false;
            this.gxtMeasureCycle.Text = "     측정주기    ";
            // 
            // tableLayoutPanel43
            // 
            this.tableLayoutPanel43.ColumnCount = 2;
            this.tableLayoutPanel43.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel43.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel43.Controls.Add(this.txtCutLineMeasure, 1, 0);
            this.tableLayoutPanel43.Controls.Add(this.lbl_CutLineMeasure, 0, 0);
            this.tableLayoutPanel43.Location = new System.Drawing.Point(6, 22);
            this.tableLayoutPanel43.Name = "tableLayoutPanel43";
            this.tableLayoutPanel43.RowCount = 1;
            this.tableLayoutPanel43.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel43.Size = new System.Drawing.Size(404, 30);
            this.tableLayoutPanel43.TabIndex = 1;
            // 
            // txtCutLineMeasure
            // 
            this.txtCutLineMeasure.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCutLineMeasure.Location = new System.Drawing.Point(205, 3);
            this.txtCutLineMeasure.Name = "txtCutLineMeasure";
            this.txtCutLineMeasure.Size = new System.Drawing.Size(196, 22);
            this.txtCutLineMeasure.TabIndex = 0;
            // 
            // lbl_CutLineMeasure
            // 
            this.lbl_CutLineMeasure.AutoSize = true;
            this.lbl_CutLineMeasure.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_CutLineMeasure.Location = new System.Drawing.Point(3, 6);
            this.lbl_CutLineMeasure.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_CutLineMeasure.Name = "lbl_CutLineMeasure";
            this.lbl_CutLineMeasure.Size = new System.Drawing.Size(75, 15);
            this.lbl_CutLineMeasure.TabIndex = 2;
            this.lbl_CutLineMeasure.Text = "컷 라인 주기";
            // 
            // gxtBlowingCheck
            // 
            this.gxtBlowingCheck.Controls.Add(this.tableLayoutPanel44);
            this.gxtBlowingCheck.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtBlowingCheck.ForeColor = System.Drawing.Color.White;
            this.gxtBlowingCheck.Location = new System.Drawing.Point(3, 127);
            this.gxtBlowingCheck.Name = "gxtBlowingCheck";
            this.gxtBlowingCheck.Size = new System.Drawing.Size(416, 54);
            this.gxtBlowingCheck.TabIndex = 60;
            this.gxtBlowingCheck.TabStop = false;
            this.gxtBlowingCheck.Text = "     Blowing 체크    ";
            // 
            // tableLayoutPanel44
            // 
            this.tableLayoutPanel44.ColumnCount = 2;
            this.tableLayoutPanel44.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel44.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel44.Controls.Add(this.txtBlowingCheck, 1, 0);
            this.tableLayoutPanel44.Controls.Add(this.lbl_BlowingCheck, 0, 0);
            this.tableLayoutPanel44.Location = new System.Drawing.Point(6, 22);
            this.tableLayoutPanel44.Name = "tableLayoutPanel44";
            this.tableLayoutPanel44.RowCount = 1;
            this.tableLayoutPanel44.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel44.Size = new System.Drawing.Size(404, 30);
            this.tableLayoutPanel44.TabIndex = 1;
            // 
            // txtBlowingCheck
            // 
            this.txtBlowingCheck.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtBlowingCheck.Location = new System.Drawing.Point(205, 3);
            this.txtBlowingCheck.Name = "txtBlowingCheck";
            this.txtBlowingCheck.Size = new System.Drawing.Size(196, 22);
            this.txtBlowingCheck.TabIndex = 0;
            // 
            // lbl_BlowingCheck
            // 
            this.lbl_BlowingCheck.AutoSize = true;
            this.lbl_BlowingCheck.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_BlowingCheck.Location = new System.Drawing.Point(3, 6);
            this.lbl_BlowingCheck.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_BlowingCheck.Name = "lbl_BlowingCheck";
            this.lbl_BlowingCheck.Size = new System.Drawing.Size(92, 15);
            this.lbl_BlowingCheck.TabIndex = 2;
            this.lbl_BlowingCheck.Text = "Blowing 체크 : ";
            // 
            // gxtRetryCount
            // 
            this.gxtRetryCount.Controls.Add(this.tableLayoutPanel45);
            this.gxtRetryCount.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtRetryCount.ForeColor = System.Drawing.Color.White;
            this.gxtRetryCount.Location = new System.Drawing.Point(3, 187);
            this.gxtRetryCount.Name = "gxtRetryCount";
            this.gxtRetryCount.Size = new System.Drawing.Size(416, 55);
            this.gxtRetryCount.TabIndex = 61;
            this.gxtRetryCount.TabStop = false;
            this.gxtRetryCount.Text = "     Retry Count    ";
            // 
            // tableLayoutPanel45
            // 
            this.tableLayoutPanel45.ColumnCount = 2;
            this.tableLayoutPanel45.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel45.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel45.Controls.Add(this.txtRetryCount, 1, 0);
            this.tableLayoutPanel45.Controls.Add(this.lbl_RetryCount, 0, 0);
            this.tableLayoutPanel45.Location = new System.Drawing.Point(6, 22);
            this.tableLayoutPanel45.Name = "tableLayoutPanel45";
            this.tableLayoutPanel45.RowCount = 1;
            this.tableLayoutPanel45.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel45.Size = new System.Drawing.Size(404, 30);
            this.tableLayoutPanel45.TabIndex = 1;
            // 
            // txtRetryCount
            // 
            this.txtRetryCount.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtRetryCount.Location = new System.Drawing.Point(205, 3);
            this.txtRetryCount.Name = "txtRetryCount";
            this.txtRetryCount.Size = new System.Drawing.Size(196, 22);
            this.txtRetryCount.TabIndex = 0;
            // 
            // lbl_RetryCount
            // 
            this.lbl_RetryCount.AutoSize = true;
            this.lbl_RetryCount.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_RetryCount.Location = new System.Drawing.Point(3, 6);
            this.lbl_RetryCount.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_RetryCount.Name = "lbl_RetryCount";
            this.lbl_RetryCount.Size = new System.Drawing.Size(87, 15);
            this.lbl_RetryCount.TabIndex = 2;
            this.lbl_RetryCount.Text = "Retry Count : ";
            // 
            // gxtLaserPD7Error
            // 
            this.gxtLaserPD7Error.Controls.Add(this.tableLayoutPanel46);
            this.gxtLaserPD7Error.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtLaserPD7Error.ForeColor = System.Drawing.Color.White;
            this.gxtLaserPD7Error.Location = new System.Drawing.Point(3, 248);
            this.gxtLaserPD7Error.Name = "gxtLaserPD7Error";
            this.gxtLaserPD7Error.Size = new System.Drawing.Size(416, 92);
            this.gxtLaserPD7Error.TabIndex = 62;
            this.gxtLaserPD7Error.TabStop = false;
            this.gxtLaserPD7Error.Text = "     레이져 PD7 에러    ";
            // 
            // tableLayoutPanel46
            // 
            this.tableLayoutPanel46.ColumnCount = 2;
            this.tableLayoutPanel46.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel46.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel46.Controls.Add(this.lbl_LaserPD7ErrorRange, 0, 1);
            this.tableLayoutPanel46.Controls.Add(this.txtLaserPD7Power, 1, 0);
            this.tableLayoutPanel46.Controls.Add(this.lbl_LaserPD7Power, 0, 0);
            this.tableLayoutPanel46.Controls.Add(this.txtLaserPD7ErrorRange, 1, 1);
            this.tableLayoutPanel46.Location = new System.Drawing.Point(6, 22);
            this.tableLayoutPanel46.Name = "tableLayoutPanel46";
            this.tableLayoutPanel46.RowCount = 2;
            this.tableLayoutPanel46.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel46.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel46.Size = new System.Drawing.Size(404, 67);
            this.tableLayoutPanel46.TabIndex = 1;
            // 
            // lbl_LaserPD7ErrorRange
            // 
            this.lbl_LaserPD7ErrorRange.AutoSize = true;
            this.lbl_LaserPD7ErrorRange.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_LaserPD7ErrorRange.Location = new System.Drawing.Point(3, 39);
            this.lbl_LaserPD7ErrorRange.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_LaserPD7ErrorRange.Name = "lbl_LaserPD7ErrorRange";
            this.lbl_LaserPD7ErrorRange.Size = new System.Drawing.Size(160, 15);
            this.lbl_LaserPD7ErrorRange.TabIndex = 3;
            this.lbl_LaserPD7ErrorRange.Text = "레이져 PD7 오차 범위[%] :  ";
            // 
            // txtLaserPD7Power
            // 
            this.txtLaserPD7Power.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLaserPD7Power.Location = new System.Drawing.Point(205, 3);
            this.txtLaserPD7Power.Name = "txtLaserPD7Power";
            this.txtLaserPD7Power.Size = new System.Drawing.Size(196, 22);
            this.txtLaserPD7Power.TabIndex = 0;
            // 
            // lbl_LaserPD7Power
            // 
            this.lbl_LaserPD7Power.AutoSize = true;
            this.lbl_LaserPD7Power.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_LaserPD7Power.Location = new System.Drawing.Point(3, 6);
            this.lbl_LaserPD7Power.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_LaserPD7Power.Name = "lbl_LaserPD7Power";
            this.lbl_LaserPD7Power.Size = new System.Drawing.Size(127, 15);
            this.lbl_LaserPD7Power.TabIndex = 2;
            this.lbl_LaserPD7Power.Text = "레이져 PD7 파워[w] : ";
            // 
            // txtLaserPD7ErrorRange
            // 
            this.txtLaserPD7ErrorRange.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLaserPD7ErrorRange.Location = new System.Drawing.Point(205, 36);
            this.txtLaserPD7ErrorRange.Name = "txtLaserPD7ErrorRange";
            this.txtLaserPD7ErrorRange.Size = new System.Drawing.Size(196, 22);
            this.txtLaserPD7ErrorRange.TabIndex = 1;
            // 
            // gxtLaserPowerMeasure
            // 
            this.gxtLaserPowerMeasure.Controls.Add(this.tableLayoutPanel47);
            this.gxtLaserPowerMeasure.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtLaserPowerMeasure.ForeColor = System.Drawing.Color.White;
            this.gxtLaserPowerMeasure.Location = new System.Drawing.Point(3, 346);
            this.gxtLaserPowerMeasure.Name = "gxtLaserPowerMeasure";
            this.gxtLaserPowerMeasure.Size = new System.Drawing.Size(416, 261);
            this.gxtLaserPowerMeasure.TabIndex = 63;
            this.gxtLaserPowerMeasure.TabStop = false;
            this.gxtLaserPowerMeasure.Text = "     레이져 파워 측정    ";
            // 
            // tableLayoutPanel47
            // 
            this.tableLayoutPanel47.ColumnCount = 2;
            this.tableLayoutPanel47.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel47.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel47.Controls.Add(this.lbl_TargetErrorRange, 0, 6);
            this.tableLayoutPanel47.Controls.Add(this.txtMeasureCycle, 1, 0);
            this.tableLayoutPanel47.Controls.Add(this.lbl_LaserPowerMeasure_MeasureCycle, 0, 0);
            this.tableLayoutPanel47.Controls.Add(this.txtTargetPEC, 1, 1);
            this.tableLayoutPanel47.Controls.Add(this.txtTargetPower, 1, 2);
            this.tableLayoutPanel47.Controls.Add(this.txtWarmUpTime, 1, 3);
            this.tableLayoutPanel47.Controls.Add(this.txtMeasureTime, 1, 4);
            this.tableLayoutPanel47.Controls.Add(this.txtCorrectionErrorRange, 1, 5);
            this.tableLayoutPanel47.Controls.Add(this.txtTargetErrorRange, 1, 6);
            this.tableLayoutPanel47.Controls.Add(this.lbl_TargetPEC, 0, 1);
            this.tableLayoutPanel47.Controls.Add(this.lbl_TargetPower, 0, 2);
            this.tableLayoutPanel47.Controls.Add(this.lbl_WarmUpTime, 0, 3);
            this.tableLayoutPanel47.Controls.Add(this.lbl_MeasureTime, 0, 4);
            this.tableLayoutPanel47.Controls.Add(this.lbl_CorrectionErrorRange, 0, 5);
            this.tableLayoutPanel47.Location = new System.Drawing.Point(6, 22);
            this.tableLayoutPanel47.Name = "tableLayoutPanel47";
            this.tableLayoutPanel47.RowCount = 7;
            this.tableLayoutPanel47.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel47.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel47.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel47.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel47.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel47.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel47.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel47.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel47.Size = new System.Drawing.Size(404, 235);
            this.tableLayoutPanel47.TabIndex = 1;
            // 
            // lbl_TargetErrorRange
            // 
            this.lbl_TargetErrorRange.AutoSize = true;
            this.lbl_TargetErrorRange.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_TargetErrorRange.Location = new System.Drawing.Point(3, 204);
            this.lbl_TargetErrorRange.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_TargetErrorRange.Name = "lbl_TargetErrorRange";
            this.lbl_TargetErrorRange.Size = new System.Drawing.Size(131, 15);
            this.lbl_TargetErrorRange.TabIndex = 22;
            this.lbl_TargetErrorRange.Text = "Target 오차 범위[w] : ";
            // 
            // txtMeasureCycle
            // 
            this.txtMeasureCycle.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMeasureCycle.Location = new System.Drawing.Point(205, 6);
            this.txtMeasureCycle.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.txtMeasureCycle.Name = "txtMeasureCycle";
            this.txtMeasureCycle.Size = new System.Drawing.Size(196, 22);
            this.txtMeasureCycle.TabIndex = 0;
            // 
            // lbl_LaserPowerMeasure_MeasureCycle
            // 
            this.lbl_LaserPowerMeasure_MeasureCycle.AutoSize = true;
            this.lbl_LaserPowerMeasure_MeasureCycle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_LaserPowerMeasure_MeasureCycle.Location = new System.Drawing.Point(3, 6);
            this.lbl_LaserPowerMeasure_MeasureCycle.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_LaserPowerMeasure_MeasureCycle.Name = "lbl_LaserPowerMeasure_MeasureCycle";
            this.lbl_LaserPowerMeasure_MeasureCycle.Size = new System.Drawing.Size(85, 15);
            this.lbl_LaserPowerMeasure_MeasureCycle.TabIndex = 2;
            this.lbl_LaserPowerMeasure_MeasureCycle.Text = "측정 주기[h] : ";
            // 
            // txtTargetPEC
            // 
            this.txtTargetPEC.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTargetPEC.Location = new System.Drawing.Point(205, 39);
            this.txtTargetPEC.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.txtTargetPEC.Name = "txtTargetPEC";
            this.txtTargetPEC.Size = new System.Drawing.Size(196, 22);
            this.txtTargetPEC.TabIndex = 1;
            // 
            // txtTargetPower
            // 
            this.txtTargetPower.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTargetPower.Location = new System.Drawing.Point(205, 72);
            this.txtTargetPower.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.txtTargetPower.Name = "txtTargetPower";
            this.txtTargetPower.Size = new System.Drawing.Size(196, 22);
            this.txtTargetPower.TabIndex = 2;
            // 
            // txtWarmUpTime
            // 
            this.txtWarmUpTime.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtWarmUpTime.Location = new System.Drawing.Point(205, 105);
            this.txtWarmUpTime.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.txtWarmUpTime.Name = "txtWarmUpTime";
            this.txtWarmUpTime.Size = new System.Drawing.Size(196, 22);
            this.txtWarmUpTime.TabIndex = 13;
            // 
            // txtMeasureTime
            // 
            this.txtMeasureTime.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMeasureTime.Location = new System.Drawing.Point(205, 138);
            this.txtMeasureTime.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.txtMeasureTime.Name = "txtMeasureTime";
            this.txtMeasureTime.Size = new System.Drawing.Size(196, 22);
            this.txtMeasureTime.TabIndex = 14;
            // 
            // txtCorrectionErrorRange
            // 
            this.txtCorrectionErrorRange.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCorrectionErrorRange.Location = new System.Drawing.Point(205, 171);
            this.txtCorrectionErrorRange.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.txtCorrectionErrorRange.Name = "txtCorrectionErrorRange";
            this.txtCorrectionErrorRange.Size = new System.Drawing.Size(196, 22);
            this.txtCorrectionErrorRange.TabIndex = 15;
            // 
            // txtTargetErrorRange
            // 
            this.txtTargetErrorRange.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTargetErrorRange.Location = new System.Drawing.Point(205, 204);
            this.txtTargetErrorRange.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.txtTargetErrorRange.Name = "txtTargetErrorRange";
            this.txtTargetErrorRange.Size = new System.Drawing.Size(196, 22);
            this.txtTargetErrorRange.TabIndex = 16;
            // 
            // lbl_TargetPEC
            // 
            this.lbl_TargetPEC.AutoSize = true;
            this.lbl_TargetPEC.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_TargetPEC.Location = new System.Drawing.Point(3, 39);
            this.lbl_TargetPEC.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_TargetPEC.Name = "lbl_TargetPEC";
            this.lbl_TargetPEC.Size = new System.Drawing.Size(101, 15);
            this.lbl_TargetPEC.TabIndex = 17;
            this.lbl_TargetPEC.Text = "Target PEC[%] : ";
            // 
            // lbl_TargetPower
            // 
            this.lbl_TargetPower.AutoSize = true;
            this.lbl_TargetPower.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_TargetPower.Location = new System.Drawing.Point(3, 72);
            this.lbl_TargetPower.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_TargetPower.Name = "lbl_TargetPower";
            this.lbl_TargetPower.Size = new System.Drawing.Size(103, 15);
            this.lbl_TargetPower.TabIndex = 18;
            this.lbl_TargetPower.Text = "Target 파워[w] : ";
            // 
            // lbl_WarmUpTime
            // 
            this.lbl_WarmUpTime.AutoSize = true;
            this.lbl_WarmUpTime.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_WarmUpTime.Location = new System.Drawing.Point(3, 105);
            this.lbl_WarmUpTime.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_WarmUpTime.Name = "lbl_WarmUpTime";
            this.lbl_WarmUpTime.Size = new System.Drawing.Size(116, 15);
            this.lbl_WarmUpTime.TabIndex = 19;
            this.lbl_WarmUpTime.Text = "WarmUp Time[s] : ";
            // 
            // lbl_MeasureTime
            // 
            this.lbl_MeasureTime.AutoSize = true;
            this.lbl_MeasureTime.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_MeasureTime.Location = new System.Drawing.Point(3, 138);
            this.lbl_MeasureTime.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_MeasureTime.Name = "lbl_MeasureTime";
            this.lbl_MeasureTime.Size = new System.Drawing.Size(84, 15);
            this.lbl_MeasureTime.TabIndex = 20;
            this.lbl_MeasureTime.Text = "측정 시간[s] : ";
            // 
            // lbl_CorrectionErrorRange
            // 
            this.lbl_CorrectionErrorRange.AutoSize = true;
            this.lbl_CorrectionErrorRange.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_CorrectionErrorRange.Location = new System.Drawing.Point(3, 171);
            this.lbl_CorrectionErrorRange.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_CorrectionErrorRange.Name = "lbl_CorrectionErrorRange";
            this.lbl_CorrectionErrorRange.Size = new System.Drawing.Size(116, 15);
            this.lbl_CorrectionErrorRange.TabIndex = 21;
            this.lbl_CorrectionErrorRange.Text = "보정 오차 범위[w] : ";
            // 
            // btn_Save
            // 
            this.btn_Save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Save.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btn_Save.ForeColor = System.Drawing.Color.White;
            this.btn_Save.Location = new System.Drawing.Point(1575, 827);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(159, 45);
            this.btn_Save.TabIndex = 48;
            this.btn_Save.Text = "저장";
            this.btn_Save.UseVisualStyleBackColor = false;
            // 
            // btn_AlarmClear
            // 
            this.btn_AlarmClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_AlarmClear.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btn_AlarmClear.ForeColor = System.Drawing.Color.White;
            this.btn_AlarmClear.Location = new System.Drawing.Point(1410, 827);
            this.btn_AlarmClear.Name = "btn_AlarmClear";
            this.btn_AlarmClear.Size = new System.Drawing.Size(159, 45);
            this.btn_AlarmClear.TabIndex = 49;
            this.btn_AlarmClear.Text = "알람 클리어";
            this.btn_AlarmClear.UseVisualStyleBackColor = false;
            // 
            // Parameter_System
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Controls.Add(this.btn_AlarmClear);
            this.Controls.Add(this.btn_Save);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Parameter_System";
            this.Size = new System.Drawing.Size(1740, 875);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.gxtMode_DistributionMode.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.gxtSimulationMode.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.gxtSkipMode.ResumeLayout(false);
            this.gxtDoorSkip.ResumeLayout(false);
            this.tableLayoutPanel31.ResumeLayout(false);
            this.tableLayoutPanel31.PerformLayout();
            this.gxtGrabSwitchSkip.ResumeLayout(false);
            this.tableLayoutPanel30.ResumeLayout(false);
            this.tableLayoutPanel30.PerformLayout();
            this.gxtMcDownSkip.ResumeLayout(false);
            this.tableLayoutPanel29.ResumeLayout(false);
            this.tableLayoutPanel29.PerformLayout();
            this.gxtTeachSkip.ResumeLayout(false);
            this.tableLayoutPanel28.ResumeLayout(false);
            this.tableLayoutPanel28.PerformLayout();
            this.gxtMutingSkip.ResumeLayout(false);
            this.tableLayoutPanel27.ResumeLayout(false);
            this.tableLayoutPanel27.PerformLayout();
            this.gxtLaserMeasureSkip.ResumeLayout(false);
            this.tableLayoutPanel26.ResumeLayout(false);
            this.tableLayoutPanel26.PerformLayout();
            this.gxtUnloaderInputSkip.ResumeLayout(false);
            this.tableLayoutPanel21.ResumeLayout(false);
            this.tableLayoutPanel24.ResumeLayout(false);
            this.tableLayoutPanel24.PerformLayout();
            this.tableLayoutPanel25.ResumeLayout(false);
            this.tableLayoutPanel25.PerformLayout();
            this.gxtLoaderInputSkip.ResumeLayout(false);
            this.tableLayoutPanel20.ResumeLayout(false);
            this.tableLayoutPanel23.ResumeLayout(false);
            this.tableLayoutPanel23.PerformLayout();
            this.tableLayoutPanel22.ResumeLayout(false);
            this.tableLayoutPanel22.PerformLayout();
            this.gxtBufferPickerSkip.ResumeLayout(false);
            this.tableLayoutPanel19.ResumeLayout(false);
            this.tableLayoutPanel19.PerformLayout();
            this.gxtTiltMeasureSkip.ResumeLayout(false);
            this.tableLayoutPanel18.ResumeLayout(false);
            this.tableLayoutPanel18.PerformLayout();
            this.gxtBreakingAlignSkip.ResumeLayout(false);
            this.tableLayoutPanel17.ResumeLayout(false);
            this.tableLayoutPanel17.PerformLayout();
            this.gxtBcrSkip.ResumeLayout(false);
            this.tableLayoutPanel16.ResumeLayout(false);
            this.tableLayoutPanel16.PerformLayout();
            this.gxtMcrSkip.ResumeLayout(false);
            this.tableLayoutPanel15.ResumeLayout(false);
            this.tableLayoutPanel15.PerformLayout();
            this.gxtInspectionSkip.ResumeLayout(false);
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel14.PerformLayout();
            this.gxtBreakingMotionSkip.ResumeLayout(false);
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            this.gxtLaserSkip.ResumeLayout(false);
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel12.PerformLayout();
            this.gxtBreakingBSkip.ResumeLayout(false);
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            this.gxtBreakingASkip.ResumeLayout(false);
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.gxtProcessBSkip.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.gxtProcessASkip.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel32.ResumeLayout(false);
            this.tableLayoutPanel32.PerformLayout();
            this.tableLayoutPanel33.ResumeLayout(false);
            this.tableLayoutPanel34.ResumeLayout(false);
            this.gxtAutoDeleteCycle.ResumeLayout(false);
            this.tableLayoutPanel40.ResumeLayout(false);
            this.tableLayoutPanel40.PerformLayout();
            this.gxtBreakingCount.ResumeLayout(false);
            this.tableLayoutPanel39.ResumeLayout(false);
            this.tableLayoutPanel39.PerformLayout();
            this.gxtDoorOpenSpeed.ResumeLayout(false);
            this.tableLayoutPanel38.ResumeLayout(false);
            this.tableLayoutPanel38.PerformLayout();
            this.gxtCountError.ResumeLayout(false);
            this.tableLayoutPanel37.ResumeLayout(false);
            this.tableLayoutPanel37.PerformLayout();
            this.gxtTimeout.ResumeLayout(false);
            this.tableLayoutPanel36.ResumeLayout(false);
            this.tableLayoutPanel36.PerformLayout();
            this.gxtSetting_DistributionMode.ResumeLayout(false);
            this.tableLayoutPanel35.ResumeLayout(false);
            this.tableLayoutPanel35.PerformLayout();
            this.tableLayoutPanel41.ResumeLayout(false);
            this.gxtLDS.ResumeLayout(false);
            this.tableLayoutPanel48.ResumeLayout(false);
            this.tableLayoutPanel48.PerformLayout();
            this.gxtTemperatureAlarm.ResumeLayout(false);
            this.tableLayoutPanel42.ResumeLayout(false);
            this.tableLayoutPanel42.PerformLayout();
            this.gxtMeasureCycle.ResumeLayout(false);
            this.tableLayoutPanel43.ResumeLayout(false);
            this.tableLayoutPanel43.PerformLayout();
            this.gxtBlowingCheck.ResumeLayout(false);
            this.tableLayoutPanel44.ResumeLayout(false);
            this.tableLayoutPanel44.PerformLayout();
            this.gxtRetryCount.ResumeLayout(false);
            this.tableLayoutPanel45.ResumeLayout(false);
            this.tableLayoutPanel45.PerformLayout();
            this.gxtLaserPD7Error.ResumeLayout(false);
            this.tableLayoutPanel46.ResumeLayout(false);
            this.tableLayoutPanel46.PerformLayout();
            this.gxtLaserPowerMeasure.ResumeLayout(false);
            this.tableLayoutPanel47.ResumeLayout(false);
            this.tableLayoutPanel47.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label lbl_Mode;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.GroupBox gxtSimulationMode;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TextBox txtSimulationMode;
        private System.Windows.Forms.GroupBox gxtSkipMode;
        private System.Windows.Forms.GroupBox gxtLoaderInputSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel20;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel22;
        private System.Windows.Forms.Button btn_LoaderInputSkipA_DontUse;
        private System.Windows.Forms.TextBox txtLoaderInputSkipA;
        private System.Windows.Forms.Button btn_LoaderInputSkipA_Use;
        private System.Windows.Forms.GroupBox gxtBufferPickerSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel19;
        private System.Windows.Forms.Button btnBufferPickerSkipDontUse;
        private System.Windows.Forms.TextBox txtBufferPickerSkip;
        private System.Windows.Forms.Button btnBufferPickerSkipUse;
        private System.Windows.Forms.GroupBox gxtTiltMeasureSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel18;
        private System.Windows.Forms.Button btnTiltMeasureSkipDontUse;
        private System.Windows.Forms.TextBox txtTiltMeasureSkip;
        private System.Windows.Forms.Button btnTiltMeasureSkipUse;
        private System.Windows.Forms.GroupBox gxtBreakingAlignSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel17;
        private System.Windows.Forms.Button btnBreakingAlignSkipDontUse;
        private System.Windows.Forms.TextBox txtBreakingAlignSkip;
        private System.Windows.Forms.Button btnBreakingAlignSkipUse;
        private System.Windows.Forms.GroupBox gxtBcrSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        private System.Windows.Forms.Button btnBcrSkipDontUse;
        private System.Windows.Forms.TextBox txtBcrSkip;
        private System.Windows.Forms.Button btnBcrSkipUse;
        private System.Windows.Forms.GroupBox gxtMcrSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private System.Windows.Forms.Button btnMcrSkipDontUse;
        private System.Windows.Forms.TextBox txtMcrSkip;
        private System.Windows.Forms.Button btnMcrSkipUse;
        private System.Windows.Forms.GroupBox gxtInspectionSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.Button btnInspectionSkipDontUse;
        private System.Windows.Forms.TextBox txtInspectionSkip;
        private System.Windows.Forms.Button btninspectionSkipUse;
        private System.Windows.Forms.GroupBox gxtBreakingMotionSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.Button btnBreakingMotionSkipDontUse;
        private System.Windows.Forms.TextBox txtBreakingMotionSkip;
        private System.Windows.Forms.Button btnBreakingMotionSkipUse;
        private System.Windows.Forms.GroupBox gxtLaserSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.Button btnLaserSkipDontUse;
        private System.Windows.Forms.TextBox txtLaserSkip;
        private System.Windows.Forms.Button btnLaserSkipUse;
        private System.Windows.Forms.GroupBox gxtBreakingBSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.Button btnBreakingBSkipDontUse;
        private System.Windows.Forms.TextBox txtBreakingBSkip;
        private System.Windows.Forms.Button btnBreakingBSkipUse;
        private System.Windows.Forms.GroupBox gxtBreakingASkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.Button btnBreakingASkipDontUse;
        private System.Windows.Forms.TextBox txtBreakingASkip;
        private System.Windows.Forms.Button btnBreakingASkipUse;
        private System.Windows.Forms.GroupBox gxtProcessBSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Button btnProcessBSkipDontUse;
        private System.Windows.Forms.TextBox txtProcessBSkip;
        private System.Windows.Forms.Button btnProcessBSkipUse;
        private System.Windows.Forms.GroupBox gxtProcessASkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Button btnProcessASkipDontUse;
        private System.Windows.Forms.TextBox txtProcessASkip;
        private System.Windows.Forms.Button btnProcessASkipUse;
        private System.Windows.Forms.GroupBox gxtMode_DistributionMode;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Button btnModeDistributionModeDontUse;
        private System.Windows.Forms.TextBox txtMode_DistributionMode;
        private System.Windows.Forms.Button btnModeDistributionModeUse;
        private System.Windows.Forms.Button btnSimulationModeDontUse;
        private System.Windows.Forms.Button btnSimulationModeUse;
        private System.Windows.Forms.GroupBox gxtDoorSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel31;
        private System.Windows.Forms.Button btn_DoorSkip_DontUSe;
        private System.Windows.Forms.TextBox txtDoorSkip;
        private System.Windows.Forms.Button btn_DoorSkip_Use;
        private System.Windows.Forms.GroupBox gxtGrabSwitchSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel30;
        private System.Windows.Forms.Button btn_GrabSwitchSkip_DontUse;
        private System.Windows.Forms.TextBox txtGrabSwitchSkip;
        private System.Windows.Forms.Button btn_GrabSwitchSkip_Use;
        private System.Windows.Forms.GroupBox gxtMcDownSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel29;
        private System.Windows.Forms.Button btn_McDownSkip_DontUse;
        private System.Windows.Forms.TextBox txtMcDownSkip;
        private System.Windows.Forms.Button btn_McDownSkip_Use;
        private System.Windows.Forms.GroupBox gxtTeachSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel28;
        private System.Windows.Forms.Button btn_TeachSkip_DontUse;
        private System.Windows.Forms.TextBox txtTeachSkip;
        private System.Windows.Forms.Button btn_TeachSkip_Use;
        private System.Windows.Forms.GroupBox gxtMutingSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel27;
        private System.Windows.Forms.Button btn_MutingSkip_DontUse;
        private System.Windows.Forms.TextBox txtMutingSkip;
        private System.Windows.Forms.Button btn_MutingSkip_Use;
        private System.Windows.Forms.GroupBox gxtLaserMeasureSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel26;
        private System.Windows.Forms.Button btn_LaserMeasureSkip_DontUse;
        private System.Windows.Forms.TextBox txtLaserMeasureSkip;
        private System.Windows.Forms.Button btn_LaserMeasureSkip_Use;
        private System.Windows.Forms.GroupBox gxtUnloaderInputSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel21;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel24;
        private System.Windows.Forms.Button btn_UnloaderInputSkipB_DontUse;
        private System.Windows.Forms.TextBox txtUnloaderInputSkipB;
        private System.Windows.Forms.Button btn_UnloaderInputSkipB_Use;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel25;
        private System.Windows.Forms.Button btn_UnloaderInputSkipA_DontUse;
        private System.Windows.Forms.TextBox txtUnloaderInputSkipA;
        private System.Windows.Forms.Button btn_UnloaderInputSkipA_Use;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel23;
        private System.Windows.Forms.Button btn_LoaderInputSkipB_DontUse;
        private System.Windows.Forms.TextBox txtLoaderInputSkipB;
        private System.Windows.Forms.Button btn_LoaderInputSkipB_Use;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel32;
        private System.Windows.Forms.Label lbl_Setting;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel33;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel34;
        private System.Windows.Forms.GroupBox gxtSetting_DistributionMode;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel35;
        private System.Windows.Forms.Label lbl_BreakingAlignSpeed;
        private System.Windows.Forms.Label lbl_FineAlignSpeed;
        private System.Windows.Forms.Label lbl_PreAlignSpeed;
        private System.Windows.Forms.GroupBox gxtAutoDeleteCycle;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel40;
        private System.Windows.Forms.TextBox txtAutoDeleteCycle;
        private System.Windows.Forms.Label lbl_AutoDeleteCycle;
        private System.Windows.Forms.GroupBox gxtBreakingCount;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel39;
        private System.Windows.Forms.Label lbl_CriticalAlarm;
        private System.Windows.Forms.TextBox txtLightAlarm;
        private System.Windows.Forms.Label lbl_LightAlarm;
        private System.Windows.Forms.TextBox txtCriticalAlarm;
        private System.Windows.Forms.GroupBox gxtDoorOpenSpeed;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel38;
        private System.Windows.Forms.TextBox txtDoorOpenSpeed;
        private System.Windows.Forms.Label lbl_DoorOpenSpped;
        private System.Windows.Forms.GroupBox gxtCountError;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel37;
        private System.Windows.Forms.Label lbl_INSP;
        private System.Windows.Forms.TextBox txtMCR;
        private System.Windows.Forms.Label lbl_MCR;
        private System.Windows.Forms.TextBox txtINSP;
        private System.Windows.Forms.GroupBox gxtTimeout;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel36;
        private System.Windows.Forms.Label lbl_MutingTimeout;
        private System.Windows.Forms.TextBox txtVacuumTimeout;
        private System.Windows.Forms.Label lbl_VacuumTimeout;
        private System.Windows.Forms.TextBox txtCylinderTimeout;
        private System.Windows.Forms.TextBox txtPreAlignTimeout;
        private System.Windows.Forms.TextBox txtBreakingAlignTimeout;
        private System.Windows.Forms.TextBox txtInspectiongTimeout;
        private System.Windows.Forms.TextBox txtSequenceMoveTimeout;
        private System.Windows.Forms.TextBox txtMutingTimeout;
        private System.Windows.Forms.Label lbl_CylinderTimeout;
        private System.Windows.Forms.Label lbl_PreAlignTimeout;
        private System.Windows.Forms.Label lbl_BreakingAlignTimeout;
        private System.Windows.Forms.Label lbl_InspectionTimeout;
        private System.Windows.Forms.Label lbl_SequenceMoveTimeout;
        private System.Windows.Forms.TextBox txtPreAlignSpeed;
        private System.Windows.Forms.TextBox txtFineAlignSpeed;
        private System.Windows.Forms.TextBox txtBreakingAlignSpeed;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel41;
        private System.Windows.Forms.GroupBox gxtLDS;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel48;
        private System.Windows.Forms.Label lbl_ErrorRange;
        private System.Windows.Forms.Label lbl_WaitingTime;
        private System.Windows.Forms.TextBox txtLDS_MeasureCycle;
        private System.Windows.Forms.Label lbl_LDS_MeasureCycle;
        private System.Windows.Forms.TextBox txtWaitingTime;
        private System.Windows.Forms.Label lbl_LDS_MeasureTime;
        private System.Windows.Forms.TextBox txtLDS_MeasureTime;
        private System.Windows.Forms.TextBox txtErrorRange;
        private System.Windows.Forms.GroupBox gxtTemperatureAlarm;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel42;
        private System.Windows.Forms.TextBox txtTemperatureLightAlarm;
        private System.Windows.Forms.TextBox txtTemperatureCriticalAlarm;
        private System.Windows.Forms.Label lbl_TemperatureAlarm;
        private System.Windows.Forms.GroupBox gxtMeasureCycle;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel43;
        private System.Windows.Forms.TextBox txtCutLineMeasure;
        private System.Windows.Forms.Label lbl_CutLineMeasure;
        private System.Windows.Forms.GroupBox gxtBlowingCheck;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel44;
        private System.Windows.Forms.TextBox txtBlowingCheck;
        private System.Windows.Forms.Label lbl_BlowingCheck;
        private System.Windows.Forms.GroupBox gxtRetryCount;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel45;
        private System.Windows.Forms.TextBox txtRetryCount;
        private System.Windows.Forms.Label lbl_RetryCount;
        private System.Windows.Forms.GroupBox gxtLaserPD7Error;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel46;
        private System.Windows.Forms.Label lbl_LaserPD7ErrorRange;
        private System.Windows.Forms.TextBox txtLaserPD7Power;
        private System.Windows.Forms.Label lbl_LaserPD7Power;
        private System.Windows.Forms.TextBox txtLaserPD7ErrorRange;
        private System.Windows.Forms.GroupBox gxtLaserPowerMeasure;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel47;
        private System.Windows.Forms.Label lbl_TargetErrorRange;
        private System.Windows.Forms.TextBox txtMeasureCycle;
        private System.Windows.Forms.Label lbl_LaserPowerMeasure_MeasureCycle;
        private System.Windows.Forms.TextBox txtTargetPEC;
        private System.Windows.Forms.TextBox txtTargetPower;
        private System.Windows.Forms.TextBox txtWarmUpTime;
        private System.Windows.Forms.TextBox txtMeasureTime;
        private System.Windows.Forms.TextBox txtCorrectionErrorRange;
        private System.Windows.Forms.TextBox txtTargetErrorRange;
        private System.Windows.Forms.Label lbl_TargetPEC;
        private System.Windows.Forms.Label lbl_TargetPower;
        private System.Windows.Forms.Label lbl_WarmUpTime;
        private System.Windows.Forms.Label lbl_MeasureTime;
        private System.Windows.Forms.Label lbl_CorrectionErrorRange;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.Button btn_AlarmClear;
    }
}
