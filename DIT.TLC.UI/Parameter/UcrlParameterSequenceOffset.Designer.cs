﻿namespace DIT.TLC.UI
{
    partial class Parameter_SequenceOffset
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.gxtLoad = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.gxtLoad_B = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.txtloadBT2 = new System.Windows.Forms.TextBox();
            this.lbl_Load_B_T2 = new System.Windows.Forms.Label();
            this.lbl_angle4 = new System.Windows.Forms.Label();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.txtloadBX2 = new System.Windows.Forms.TextBox();
            this.lbl_Load_B_X2 = new System.Windows.Forms.Label();
            this.lbl_mm4 = new System.Windows.Forms.Label();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.txtloadBT1 = new System.Windows.Forms.TextBox();
            this.lbl_Load_B_T1 = new System.Windows.Forms.Label();
            this.lbl_angle3 = new System.Windows.Forms.Label();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.txtloadBX1 = new System.Windows.Forms.TextBox();
            this.lbl_Load_B_X1 = new System.Windows.Forms.Label();
            this.lbl_mm3 = new System.Windows.Forms.Label();
            this.gxtLoad_A = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.txtloadAT2 = new System.Windows.Forms.TextBox();
            this.lbl_Load_A_T2 = new System.Windows.Forms.Label();
            this.lbl_angle2 = new System.Windows.Forms.Label();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.txtloadAX2 = new System.Windows.Forms.TextBox();
            this.lbl_Load_A_X2 = new System.Windows.Forms.Label();
            this.lbl_mm2 = new System.Windows.Forms.Label();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.txtloadAT1 = new System.Windows.Forms.TextBox();
            this.lbl_Load_A_T1 = new System.Windows.Forms.Label();
            this.lbl_angle1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.txtloadAX1 = new System.Windows.Forms.TextBox();
            this.lbl_Load_A_X1 = new System.Windows.Forms.Label();
            this.lbl_mm1 = new System.Windows.Forms.Label();
            this.gxtUnload = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.gxtUnload_B = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel22 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_angle8 = new System.Windows.Forms.Label();
            this.txtUnloadBT2 = new System.Windows.Forms.TextBox();
            this.lbl_angle7 = new System.Windows.Forms.Label();
            this.lbl_Unload_B_T2 = new System.Windows.Forms.Label();
            this.txtUnloadBT1 = new System.Windows.Forms.TextBox();
            this.lbl_Unload_B_T1 = new System.Windows.Forms.Label();
            this.gxtUnload_A = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel21 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_angle6 = new System.Windows.Forms.Label();
            this.txtUnloadAT2 = new System.Windows.Forms.TextBox();
            this.lbl_angle5 = new System.Windows.Forms.Label();
            this.lbl_Unload_A_T2 = new System.Windows.Forms.Label();
            this.txtUnloadAT1 = new System.Windows.Forms.TextBox();
            this.lbl_Unload_A_T1 = new System.Windows.Forms.Label();
            this.gxtAfterIR = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.gxtAfterIR_B = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel24 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_mm8 = new System.Windows.Forms.Label();
            this.txtAfterIRBX2 = new System.Windows.Forms.TextBox();
            this.lbl_mm7 = new System.Windows.Forms.Label();
            this.lbl_AfterIR_B_X2 = new System.Windows.Forms.Label();
            this.txtAfterIRBX1 = new System.Windows.Forms.TextBox();
            this.lbl_AfterIR_B_X1 = new System.Windows.Forms.Label();
            this.gxtAfterIR_A = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel23 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_mm6 = new System.Windows.Forms.Label();
            this.txtAfterIRAX2 = new System.Windows.Forms.TextBox();
            this.lbl_mm5 = new System.Windows.Forms.Label();
            this.lbl_AfterIR_A_X2 = new System.Windows.Forms.Label();
            this.txtAfterIRAX1 = new System.Windows.Forms.TextBox();
            this.lbl_AfterIR_A_X1 = new System.Windows.Forms.Label();
            this.gxtUldRotate180 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.gxtUldRotate180_B = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel26 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_mm12 = new System.Windows.Forms.Label();
            this.txtUldRotate180BX2 = new System.Windows.Forms.TextBox();
            this.lbl_mm11 = new System.Windows.Forms.Label();
            this.lbl_UldRotate180_B_X2 = new System.Windows.Forms.Label();
            this.txtUldRotate180BX1 = new System.Windows.Forms.TextBox();
            this.lbl_UldRotate180_B_X1 = new System.Windows.Forms.Label();
            this.gxtUldRotate180_A = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel25 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_mm10 = new System.Windows.Forms.Label();
            this.txtULDrotate180AX2 = new System.Windows.Forms.TextBox();
            this.lbl_mm9 = new System.Windows.Forms.Label();
            this.lbl_UldRotate180_A_X2 = new System.Windows.Forms.Label();
            this.txtUldRotate180AX1 = new System.Windows.Forms.TextBox();
            this.lbl_UldRotate180_A_X1 = new System.Windows.Forms.Label();
            this.gxtInspectionOffset = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.gxtInspection_4 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel30 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_mm20 = new System.Windows.Forms.Label();
            this.txtInspection4Y = new System.Windows.Forms.TextBox();
            this.lbl_mm19 = new System.Windows.Forms.Label();
            this.lbl_Inspection_4_Y = new System.Windows.Forms.Label();
            this.txtInspection4X = new System.Windows.Forms.TextBox();
            this.lbl_Inspection_4_X = new System.Windows.Forms.Label();
            this.gxtInspection_3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel29 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_mm18 = new System.Windows.Forms.Label();
            this.txtInspection3Y = new System.Windows.Forms.TextBox();
            this.lbl_mm17 = new System.Windows.Forms.Label();
            this.lbl_Inspection_3_Y = new System.Windows.Forms.Label();
            this.txtInspection3X = new System.Windows.Forms.TextBox();
            this.lbl_Inspection_3_X = new System.Windows.Forms.Label();
            this.gxtInspection_1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel27 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_mm14 = new System.Windows.Forms.Label();
            this.txtInspection1Y = new System.Windows.Forms.TextBox();
            this.lbl_mm13 = new System.Windows.Forms.Label();
            this.lbl_Inspection_1_Y = new System.Windows.Forms.Label();
            this.txtInspection1X = new System.Windows.Forms.TextBox();
            this.lbl_Inspection_1_X = new System.Windows.Forms.Label();
            this.gxtInspection_2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel28 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_mm16 = new System.Windows.Forms.Label();
            this.txtInspection2Y = new System.Windows.Forms.TextBox();
            this.lbl_mm15 = new System.Windows.Forms.Label();
            this.lbl_Inspection_2_Y = new System.Windows.Forms.Label();
            this.txtInspection2X = new System.Windows.Forms.TextBox();
            this.lbl_Inspection_2_X = new System.Windows.Forms.Label();
            this.gxtLoad.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.gxtLoad_B.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.gxtLoad_A.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.gxtUnload.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.gxtUnload_B.SuspendLayout();
            this.tableLayoutPanel22.SuspendLayout();
            this.gxtUnload_A.SuspendLayout();
            this.tableLayoutPanel21.SuspendLayout();
            this.gxtAfterIR.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.gxtAfterIR_B.SuspendLayout();
            this.tableLayoutPanel24.SuspendLayout();
            this.gxtAfterIR_A.SuspendLayout();
            this.tableLayoutPanel23.SuspendLayout();
            this.gxtUldRotate180.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.gxtUldRotate180_B.SuspendLayout();
            this.tableLayoutPanel26.SuspendLayout();
            this.gxtUldRotate180_A.SuspendLayout();
            this.tableLayoutPanel25.SuspendLayout();
            this.gxtInspectionOffset.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.gxtInspection_4.SuspendLayout();
            this.tableLayoutPanel30.SuspendLayout();
            this.gxtInspection_3.SuspendLayout();
            this.tableLayoutPanel29.SuspendLayout();
            this.gxtInspection_1.SuspendLayout();
            this.tableLayoutPanel27.SuspendLayout();
            this.gxtInspection_2.SuspendLayout();
            this.tableLayoutPanel28.SuspendLayout();
            this.SuspendLayout();
            // 
            // gxtLoad
            // 
            this.gxtLoad.Controls.Add(this.tableLayoutPanel1);
            this.gxtLoad.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtLoad.ForeColor = System.Drawing.Color.White;
            this.gxtLoad.Location = new System.Drawing.Point(48, 29);
            this.gxtLoad.Name = "gxtLoad";
            this.gxtLoad.Size = new System.Drawing.Size(825, 155);
            this.gxtLoad.TabIndex = 53;
            this.gxtLoad.TabStop = false;
            this.gxtLoad.Text = "     로드 이재기     ";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.gxtLoad_B, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.gxtLoad_A, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(813, 121);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // gxtLoad_B
            // 
            this.gxtLoad_B.Controls.Add(this.tableLayoutPanel11);
            this.gxtLoad_B.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtLoad_B.ForeColor = System.Drawing.Color.White;
            this.gxtLoad_B.Location = new System.Drawing.Point(409, 3);
            this.gxtLoad_B.Name = "gxtLoad_B";
            this.gxtLoad_B.Size = new System.Drawing.Size(401, 115);
            this.gxtLoad_B.TabIndex = 57;
            this.gxtLoad_B.TabStop = false;
            this.gxtLoad_B.Text = "     B     ";
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tableLayoutPanel11.ColumnCount = 2;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.Controls.Add(this.tableLayoutPanel12, 1, 1);
            this.tableLayoutPanel11.Controls.Add(this.tableLayoutPanel13, 0, 1);
            this.tableLayoutPanel11.Controls.Add(this.tableLayoutPanel14, 1, 0);
            this.tableLayoutPanel11.Controls.Add(this.tableLayoutPanel15, 0, 0);
            this.tableLayoutPanel11.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 2;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(382, 81);
            this.tableLayoutPanel11.TabIndex = 58;
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 3;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel12.Controls.Add(this.txtloadBT2, 1, 0);
            this.tableLayoutPanel12.Controls.Add(this.lbl_Load_B_T2, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.lbl_angle4, 2, 0);
            this.tableLayoutPanel12.Location = new System.Drawing.Point(194, 43);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 1;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(185, 34);
            this.tableLayoutPanel12.TabIndex = 3;
            // 
            // txtloadBT2
            // 
            this.txtloadBT2.Font = new System.Drawing.Font("굴림", 13F);
            this.txtloadBT2.Location = new System.Drawing.Point(58, 3);
            this.txtloadBT2.Name = "txtloadBT2";
            this.txtloadBT2.Size = new System.Drawing.Size(68, 27);
            this.txtloadBT2.TabIndex = 60;
            // 
            // lbl_Load_B_T2
            // 
            this.lbl_Load_B_T2.AutoSize = true;
            this.lbl_Load_B_T2.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Load_B_T2.ForeColor = System.Drawing.Color.White;
            this.lbl_Load_B_T2.Location = new System.Drawing.Point(3, 0);
            this.lbl_Load_B_T2.Name = "lbl_Load_B_T2";
            this.lbl_Load_B_T2.Size = new System.Drawing.Size(44, 21);
            this.lbl_Load_B_T2.TabIndex = 58;
            this.lbl_Load_B_T2.Text = "T2 : ";
            // 
            // lbl_angle4
            // 
            this.lbl_angle4.AutoSize = true;
            this.lbl_angle4.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.lbl_angle4.ForeColor = System.Drawing.Color.White;
            this.lbl_angle4.Location = new System.Drawing.Point(132, 0);
            this.lbl_angle4.Name = "lbl_angle4";
            this.lbl_angle4.Size = new System.Drawing.Size(16, 20);
            this.lbl_angle4.TabIndex = 59;
            this.lbl_angle4.Text = "º";
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 3;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel13.Controls.Add(this.txtloadBX2, 1, 0);
            this.tableLayoutPanel13.Controls.Add(this.lbl_Load_B_X2, 0, 0);
            this.tableLayoutPanel13.Controls.Add(this.lbl_mm4, 2, 0);
            this.tableLayoutPanel13.Location = new System.Drawing.Point(3, 43);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 1;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(185, 34);
            this.tableLayoutPanel13.TabIndex = 2;
            // 
            // txtloadBX2
            // 
            this.txtloadBX2.Font = new System.Drawing.Font("굴림", 13F);
            this.txtloadBX2.Location = new System.Drawing.Point(58, 3);
            this.txtloadBX2.Name = "txtloadBX2";
            this.txtloadBX2.Size = new System.Drawing.Size(68, 27);
            this.txtloadBX2.TabIndex = 60;
            // 
            // lbl_Load_B_X2
            // 
            this.lbl_Load_B_X2.AutoSize = true;
            this.lbl_Load_B_X2.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Load_B_X2.ForeColor = System.Drawing.Color.White;
            this.lbl_Load_B_X2.Location = new System.Drawing.Point(3, 0);
            this.lbl_Load_B_X2.Name = "lbl_Load_B_X2";
            this.lbl_Load_B_X2.Size = new System.Drawing.Size(45, 21);
            this.lbl_Load_B_X2.TabIndex = 58;
            this.lbl_Load_B_X2.Text = "X2 : ";
            // 
            // lbl_mm4
            // 
            this.lbl_mm4.AutoSize = true;
            this.lbl_mm4.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.lbl_mm4.ForeColor = System.Drawing.Color.White;
            this.lbl_mm4.Location = new System.Drawing.Point(132, 0);
            this.lbl_mm4.Name = "lbl_mm4";
            this.lbl_mm4.Size = new System.Drawing.Size(47, 20);
            this.lbl_mm4.TabIndex = 59;
            this.lbl_mm4.Text = "(mm)";
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 3;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel14.Controls.Add(this.txtloadBT1, 1, 0);
            this.tableLayoutPanel14.Controls.Add(this.lbl_Load_B_T1, 0, 0);
            this.tableLayoutPanel14.Controls.Add(this.lbl_angle3, 2, 0);
            this.tableLayoutPanel14.Location = new System.Drawing.Point(194, 3);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 1;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(185, 34);
            this.tableLayoutPanel14.TabIndex = 1;
            // 
            // txtloadBT1
            // 
            this.txtloadBT1.Font = new System.Drawing.Font("굴림", 13F);
            this.txtloadBT1.Location = new System.Drawing.Point(58, 3);
            this.txtloadBT1.Name = "txtloadBT1";
            this.txtloadBT1.Size = new System.Drawing.Size(68, 27);
            this.txtloadBT1.TabIndex = 60;
            // 
            // lbl_Load_B_T1
            // 
            this.lbl_Load_B_T1.AutoSize = true;
            this.lbl_Load_B_T1.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Load_B_T1.ForeColor = System.Drawing.Color.White;
            this.lbl_Load_B_T1.Location = new System.Drawing.Point(3, 0);
            this.lbl_Load_B_T1.Name = "lbl_Load_B_T1";
            this.lbl_Load_B_T1.Size = new System.Drawing.Size(44, 21);
            this.lbl_Load_B_T1.TabIndex = 58;
            this.lbl_Load_B_T1.Text = "T1 : ";
            // 
            // lbl_angle3
            // 
            this.lbl_angle3.AutoSize = true;
            this.lbl_angle3.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.lbl_angle3.ForeColor = System.Drawing.Color.White;
            this.lbl_angle3.Location = new System.Drawing.Point(132, 0);
            this.lbl_angle3.Name = "lbl_angle3";
            this.lbl_angle3.Size = new System.Drawing.Size(16, 20);
            this.lbl_angle3.TabIndex = 59;
            this.lbl_angle3.Text = "º";
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 3;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel15.Controls.Add(this.txtloadBX1, 1, 0);
            this.tableLayoutPanel15.Controls.Add(this.lbl_Load_B_X1, 0, 0);
            this.tableLayoutPanel15.Controls.Add(this.lbl_mm3, 2, 0);
            this.tableLayoutPanel15.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 1;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(185, 34);
            this.tableLayoutPanel15.TabIndex = 0;
            // 
            // txtloadBX1
            // 
            this.txtloadBX1.Font = new System.Drawing.Font("굴림", 13F);
            this.txtloadBX1.Location = new System.Drawing.Point(58, 3);
            this.txtloadBX1.Name = "txtloadBX1";
            this.txtloadBX1.Size = new System.Drawing.Size(68, 27);
            this.txtloadBX1.TabIndex = 60;
            // 
            // lbl_Load_B_X1
            // 
            this.lbl_Load_B_X1.AutoSize = true;
            this.lbl_Load_B_X1.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Load_B_X1.ForeColor = System.Drawing.Color.White;
            this.lbl_Load_B_X1.Location = new System.Drawing.Point(3, 0);
            this.lbl_Load_B_X1.Name = "lbl_Load_B_X1";
            this.lbl_Load_B_X1.Size = new System.Drawing.Size(45, 21);
            this.lbl_Load_B_X1.TabIndex = 58;
            this.lbl_Load_B_X1.Text = "X1 : ";
            // 
            // lbl_mm3
            // 
            this.lbl_mm3.AutoSize = true;
            this.lbl_mm3.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.lbl_mm3.ForeColor = System.Drawing.Color.White;
            this.lbl_mm3.Location = new System.Drawing.Point(132, 0);
            this.lbl_mm3.Name = "lbl_mm3";
            this.lbl_mm3.Size = new System.Drawing.Size(47, 20);
            this.lbl_mm3.TabIndex = 59;
            this.lbl_mm3.Text = "(mm)";
            // 
            // gxtLoad_A
            // 
            this.gxtLoad_A.Controls.Add(this.tableLayoutPanel6);
            this.gxtLoad_A.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtLoad_A.ForeColor = System.Drawing.Color.White;
            this.gxtLoad_A.Location = new System.Drawing.Point(3, 3);
            this.gxtLoad_A.Name = "gxtLoad_A";
            this.gxtLoad_A.Size = new System.Drawing.Size(400, 115);
            this.gxtLoad_A.TabIndex = 56;
            this.gxtLoad_A.TabStop = false;
            this.gxtLoad_A.Text = "     A     ";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel10, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel9, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel8, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel7, 0, 0);
            this.tableLayoutPanel6.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 2;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(382, 81);
            this.tableLayoutPanel6.TabIndex = 57;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 3;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel10.Controls.Add(this.txtloadAT2, 1, 0);
            this.tableLayoutPanel10.Controls.Add(this.lbl_Load_A_T2, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.lbl_angle2, 2, 0);
            this.tableLayoutPanel10.Location = new System.Drawing.Point(194, 43);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 1;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(185, 34);
            this.tableLayoutPanel10.TabIndex = 3;
            // 
            // txtloadAT2
            // 
            this.txtloadAT2.Font = new System.Drawing.Font("굴림", 13F);
            this.txtloadAT2.Location = new System.Drawing.Point(58, 3);
            this.txtloadAT2.Name = "txtloadAT2";
            this.txtloadAT2.Size = new System.Drawing.Size(68, 27);
            this.txtloadAT2.TabIndex = 60;
            // 
            // lbl_Load_A_T2
            // 
            this.lbl_Load_A_T2.AutoSize = true;
            this.lbl_Load_A_T2.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Load_A_T2.ForeColor = System.Drawing.Color.White;
            this.lbl_Load_A_T2.Location = new System.Drawing.Point(3, 0);
            this.lbl_Load_A_T2.Name = "lbl_Load_A_T2";
            this.lbl_Load_A_T2.Size = new System.Drawing.Size(44, 21);
            this.lbl_Load_A_T2.TabIndex = 58;
            this.lbl_Load_A_T2.Text = "T2 : ";
            // 
            // lbl_angle2
            // 
            this.lbl_angle2.AutoSize = true;
            this.lbl_angle2.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.lbl_angle2.ForeColor = System.Drawing.Color.White;
            this.lbl_angle2.Location = new System.Drawing.Point(132, 0);
            this.lbl_angle2.Name = "lbl_angle2";
            this.lbl_angle2.Size = new System.Drawing.Size(16, 20);
            this.lbl_angle2.TabIndex = 59;
            this.lbl_angle2.Text = "º";
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 3;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel9.Controls.Add(this.txtloadAX2, 1, 0);
            this.tableLayoutPanel9.Controls.Add(this.lbl_Load_A_X2, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.lbl_mm2, 2, 0);
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 43);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(185, 34);
            this.tableLayoutPanel9.TabIndex = 2;
            // 
            // txtloadAX2
            // 
            this.txtloadAX2.Font = new System.Drawing.Font("굴림", 13F);
            this.txtloadAX2.Location = new System.Drawing.Point(58, 3);
            this.txtloadAX2.Name = "txtloadAX2";
            this.txtloadAX2.Size = new System.Drawing.Size(68, 27);
            this.txtloadAX2.TabIndex = 60;
            // 
            // lbl_Load_A_X2
            // 
            this.lbl_Load_A_X2.AutoSize = true;
            this.lbl_Load_A_X2.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Load_A_X2.ForeColor = System.Drawing.Color.White;
            this.lbl_Load_A_X2.Location = new System.Drawing.Point(3, 0);
            this.lbl_Load_A_X2.Name = "lbl_Load_A_X2";
            this.lbl_Load_A_X2.Size = new System.Drawing.Size(45, 21);
            this.lbl_Load_A_X2.TabIndex = 58;
            this.lbl_Load_A_X2.Text = "X2 : ";
            // 
            // lbl_mm2
            // 
            this.lbl_mm2.AutoSize = true;
            this.lbl_mm2.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.lbl_mm2.ForeColor = System.Drawing.Color.White;
            this.lbl_mm2.Location = new System.Drawing.Point(132, 0);
            this.lbl_mm2.Name = "lbl_mm2";
            this.lbl_mm2.Size = new System.Drawing.Size(47, 20);
            this.lbl_mm2.TabIndex = 59;
            this.lbl_mm2.Text = "(mm)";
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 3;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel8.Controls.Add(this.txtloadAT1, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.lbl_Load_A_T1, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.lbl_angle1, 2, 0);
            this.tableLayoutPanel8.Location = new System.Drawing.Point(194, 3);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(185, 34);
            this.tableLayoutPanel8.TabIndex = 1;
            // 
            // txtloadAT1
            // 
            this.txtloadAT1.Font = new System.Drawing.Font("굴림", 13F);
            this.txtloadAT1.Location = new System.Drawing.Point(58, 3);
            this.txtloadAT1.Name = "txtloadAT1";
            this.txtloadAT1.Size = new System.Drawing.Size(68, 27);
            this.txtloadAT1.TabIndex = 60;
            // 
            // lbl_Load_A_T1
            // 
            this.lbl_Load_A_T1.AutoSize = true;
            this.lbl_Load_A_T1.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Load_A_T1.ForeColor = System.Drawing.Color.White;
            this.lbl_Load_A_T1.Location = new System.Drawing.Point(3, 0);
            this.lbl_Load_A_T1.Name = "lbl_Load_A_T1";
            this.lbl_Load_A_T1.Size = new System.Drawing.Size(44, 21);
            this.lbl_Load_A_T1.TabIndex = 58;
            this.lbl_Load_A_T1.Text = "T1 : ";
            // 
            // lbl_angle1
            // 
            this.lbl_angle1.AutoSize = true;
            this.lbl_angle1.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.lbl_angle1.ForeColor = System.Drawing.Color.White;
            this.lbl_angle1.Location = new System.Drawing.Point(132, 0);
            this.lbl_angle1.Name = "lbl_angle1";
            this.lbl_angle1.Size = new System.Drawing.Size(16, 20);
            this.lbl_angle1.TabIndex = 59;
            this.lbl_angle1.Text = "º";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 3;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel7.Controls.Add(this.txtloadAX1, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.lbl_Load_A_X1, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.lbl_mm1, 2, 0);
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(185, 34);
            this.tableLayoutPanel7.TabIndex = 0;
            // 
            // txtloadAX1
            // 
            this.txtloadAX1.Font = new System.Drawing.Font("굴림", 13F);
            this.txtloadAX1.Location = new System.Drawing.Point(58, 3);
            this.txtloadAX1.Name = "txtloadAX1";
            this.txtloadAX1.Size = new System.Drawing.Size(68, 27);
            this.txtloadAX1.TabIndex = 60;
            // 
            // lbl_Load_A_X1
            // 
            this.lbl_Load_A_X1.AutoSize = true;
            this.lbl_Load_A_X1.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Load_A_X1.ForeColor = System.Drawing.Color.White;
            this.lbl_Load_A_X1.Location = new System.Drawing.Point(3, 0);
            this.lbl_Load_A_X1.Name = "lbl_Load_A_X1";
            this.lbl_Load_A_X1.Size = new System.Drawing.Size(45, 21);
            this.lbl_Load_A_X1.TabIndex = 58;
            this.lbl_Load_A_X1.Text = "X1 : ";
            // 
            // lbl_mm1
            // 
            this.lbl_mm1.AutoSize = true;
            this.lbl_mm1.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.lbl_mm1.ForeColor = System.Drawing.Color.White;
            this.lbl_mm1.Location = new System.Drawing.Point(132, 0);
            this.lbl_mm1.Name = "lbl_mm1";
            this.lbl_mm1.Size = new System.Drawing.Size(47, 20);
            this.lbl_mm1.TabIndex = 59;
            this.lbl_mm1.Text = "(mm)";
            // 
            // gxtUnload
            // 
            this.gxtUnload.Controls.Add(this.tableLayoutPanel4);
            this.gxtUnload.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtUnload.ForeColor = System.Drawing.Color.White;
            this.gxtUnload.Location = new System.Drawing.Point(48, 190);
            this.gxtUnload.Name = "gxtUnload";
            this.gxtUnload.Size = new System.Drawing.Size(406, 155);
            this.gxtUnload.TabIndex = 54;
            this.gxtUnload.TabStop = false;
            this.gxtUnload.Text = "     언로드 이재기     ";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.gxtUnload_B, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.gxtUnload_A, 0, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(394, 121);
            this.tableLayoutPanel4.TabIndex = 2;
            // 
            // gxtUnload_B
            // 
            this.gxtUnload_B.Controls.Add(this.tableLayoutPanel22);
            this.gxtUnload_B.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtUnload_B.ForeColor = System.Drawing.Color.White;
            this.gxtUnload_B.Location = new System.Drawing.Point(200, 3);
            this.gxtUnload_B.Name = "gxtUnload_B";
            this.gxtUnload_B.Size = new System.Drawing.Size(191, 115);
            this.gxtUnload_B.TabIndex = 57;
            this.gxtUnload_B.TabStop = false;
            this.gxtUnload_B.Text = "     B     ";
            // 
            // tableLayoutPanel22
            // 
            this.tableLayoutPanel22.ColumnCount = 3;
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel22.Controls.Add(this.lbl_angle8, 2, 1);
            this.tableLayoutPanel22.Controls.Add(this.txtUnloadBT2, 1, 1);
            this.tableLayoutPanel22.Controls.Add(this.lbl_angle7, 2, 0);
            this.tableLayoutPanel22.Controls.Add(this.lbl_Unload_B_T2, 0, 1);
            this.tableLayoutPanel22.Controls.Add(this.txtUnloadBT1, 1, 0);
            this.tableLayoutPanel22.Controls.Add(this.lbl_Unload_B_T1, 0, 0);
            this.tableLayoutPanel22.Location = new System.Drawing.Point(3, 25);
            this.tableLayoutPanel22.Name = "tableLayoutPanel22";
            this.tableLayoutPanel22.RowCount = 2;
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel22.Size = new System.Drawing.Size(182, 84);
            this.tableLayoutPanel22.TabIndex = 1;
            // 
            // lbl_angle8
            // 
            this.lbl_angle8.AutoSize = true;
            this.lbl_angle8.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.lbl_angle8.ForeColor = System.Drawing.Color.White;
            this.lbl_angle8.Location = new System.Drawing.Point(129, 42);
            this.lbl_angle8.Name = "lbl_angle8";
            this.lbl_angle8.Size = new System.Drawing.Size(16, 20);
            this.lbl_angle8.TabIndex = 59;
            this.lbl_angle8.Text = "º";
            // 
            // txtUnloadBT2
            // 
            this.txtUnloadBT2.Font = new System.Drawing.Font("굴림", 13F);
            this.txtUnloadBT2.Location = new System.Drawing.Point(57, 45);
            this.txtUnloadBT2.Name = "txtUnloadBT2";
            this.txtUnloadBT2.Size = new System.Drawing.Size(66, 27);
            this.txtUnloadBT2.TabIndex = 60;
            // 
            // lbl_angle7
            // 
            this.lbl_angle7.AutoSize = true;
            this.lbl_angle7.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.lbl_angle7.ForeColor = System.Drawing.Color.White;
            this.lbl_angle7.Location = new System.Drawing.Point(129, 0);
            this.lbl_angle7.Name = "lbl_angle7";
            this.lbl_angle7.Size = new System.Drawing.Size(16, 20);
            this.lbl_angle7.TabIndex = 59;
            this.lbl_angle7.Text = "º";
            // 
            // lbl_Unload_B_T2
            // 
            this.lbl_Unload_B_T2.AutoSize = true;
            this.lbl_Unload_B_T2.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Unload_B_T2.ForeColor = System.Drawing.Color.White;
            this.lbl_Unload_B_T2.Location = new System.Drawing.Point(3, 42);
            this.lbl_Unload_B_T2.Name = "lbl_Unload_B_T2";
            this.lbl_Unload_B_T2.Size = new System.Drawing.Size(44, 21);
            this.lbl_Unload_B_T2.TabIndex = 58;
            this.lbl_Unload_B_T2.Text = "T2 : ";
            // 
            // txtUnloadBT1
            // 
            this.txtUnloadBT1.Font = new System.Drawing.Font("굴림", 13F);
            this.txtUnloadBT1.Location = new System.Drawing.Point(57, 3);
            this.txtUnloadBT1.Name = "txtUnloadBT1";
            this.txtUnloadBT1.Size = new System.Drawing.Size(66, 27);
            this.txtUnloadBT1.TabIndex = 60;
            // 
            // lbl_Unload_B_T1
            // 
            this.lbl_Unload_B_T1.AutoSize = true;
            this.lbl_Unload_B_T1.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Unload_B_T1.ForeColor = System.Drawing.Color.White;
            this.lbl_Unload_B_T1.Location = new System.Drawing.Point(3, 0);
            this.lbl_Unload_B_T1.Name = "lbl_Unload_B_T1";
            this.lbl_Unload_B_T1.Size = new System.Drawing.Size(44, 21);
            this.lbl_Unload_B_T1.TabIndex = 58;
            this.lbl_Unload_B_T1.Text = "T1 : ";
            // 
            // gxtUnload_A
            // 
            this.gxtUnload_A.Controls.Add(this.tableLayoutPanel21);
            this.gxtUnload_A.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtUnload_A.ForeColor = System.Drawing.Color.White;
            this.gxtUnload_A.Location = new System.Drawing.Point(3, 3);
            this.gxtUnload_A.Name = "gxtUnload_A";
            this.gxtUnload_A.Size = new System.Drawing.Size(191, 115);
            this.gxtUnload_A.TabIndex = 56;
            this.gxtUnload_A.TabStop = false;
            this.gxtUnload_A.Text = "     A     ";
            // 
            // tableLayoutPanel21
            // 
            this.tableLayoutPanel21.ColumnCount = 3;
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel21.Controls.Add(this.lbl_angle6, 2, 1);
            this.tableLayoutPanel21.Controls.Add(this.txtUnloadAT2, 1, 1);
            this.tableLayoutPanel21.Controls.Add(this.lbl_angle5, 2, 0);
            this.tableLayoutPanel21.Controls.Add(this.lbl_Unload_A_T2, 0, 1);
            this.tableLayoutPanel21.Controls.Add(this.txtUnloadAT1, 1, 0);
            this.tableLayoutPanel21.Controls.Add(this.lbl_Unload_A_T1, 0, 0);
            this.tableLayoutPanel21.Location = new System.Drawing.Point(3, 25);
            this.tableLayoutPanel21.Name = "tableLayoutPanel21";
            this.tableLayoutPanel21.RowCount = 2;
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel21.Size = new System.Drawing.Size(182, 84);
            this.tableLayoutPanel21.TabIndex = 0;
            // 
            // lbl_angle6
            // 
            this.lbl_angle6.AutoSize = true;
            this.lbl_angle6.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.lbl_angle6.ForeColor = System.Drawing.Color.White;
            this.lbl_angle6.Location = new System.Drawing.Point(129, 42);
            this.lbl_angle6.Name = "lbl_angle6";
            this.lbl_angle6.Size = new System.Drawing.Size(16, 20);
            this.lbl_angle6.TabIndex = 59;
            this.lbl_angle6.Text = "º";
            // 
            // txtUnloadAT2
            // 
            this.txtUnloadAT2.Font = new System.Drawing.Font("굴림", 13F);
            this.txtUnloadAT2.Location = new System.Drawing.Point(57, 45);
            this.txtUnloadAT2.Name = "txtUnloadAT2";
            this.txtUnloadAT2.Size = new System.Drawing.Size(66, 27);
            this.txtUnloadAT2.TabIndex = 60;
            // 
            // lbl_angle5
            // 
            this.lbl_angle5.AutoSize = true;
            this.lbl_angle5.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.lbl_angle5.ForeColor = System.Drawing.Color.White;
            this.lbl_angle5.Location = new System.Drawing.Point(129, 0);
            this.lbl_angle5.Name = "lbl_angle5";
            this.lbl_angle5.Size = new System.Drawing.Size(16, 20);
            this.lbl_angle5.TabIndex = 59;
            this.lbl_angle5.Text = "º";
            // 
            // lbl_Unload_A_T2
            // 
            this.lbl_Unload_A_T2.AutoSize = true;
            this.lbl_Unload_A_T2.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Unload_A_T2.ForeColor = System.Drawing.Color.White;
            this.lbl_Unload_A_T2.Location = new System.Drawing.Point(3, 42);
            this.lbl_Unload_A_T2.Name = "lbl_Unload_A_T2";
            this.lbl_Unload_A_T2.Size = new System.Drawing.Size(44, 21);
            this.lbl_Unload_A_T2.TabIndex = 58;
            this.lbl_Unload_A_T2.Text = "T2 : ";
            // 
            // txtUnloadAT1
            // 
            this.txtUnloadAT1.Font = new System.Drawing.Font("굴림", 13F);
            this.txtUnloadAT1.Location = new System.Drawing.Point(57, 3);
            this.txtUnloadAT1.Name = "txtUnloadAT1";
            this.txtUnloadAT1.Size = new System.Drawing.Size(66, 27);
            this.txtUnloadAT1.TabIndex = 60;
            // 
            // lbl_Unload_A_T1
            // 
            this.lbl_Unload_A_T1.AutoSize = true;
            this.lbl_Unload_A_T1.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Unload_A_T1.ForeColor = System.Drawing.Color.White;
            this.lbl_Unload_A_T1.Location = new System.Drawing.Point(3, 0);
            this.lbl_Unload_A_T1.Name = "lbl_Unload_A_T1";
            this.lbl_Unload_A_T1.Size = new System.Drawing.Size(44, 21);
            this.lbl_Unload_A_T1.TabIndex = 58;
            this.lbl_Unload_A_T1.Text = "T1 : ";
            // 
            // gxtAfterIR
            // 
            this.gxtAfterIR.Controls.Add(this.tableLayoutPanel2);
            this.gxtAfterIR.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtAfterIR.ForeColor = System.Drawing.Color.White;
            this.gxtAfterIR.Location = new System.Drawing.Point(879, 29);
            this.gxtAfterIR.Name = "gxtAfterIR";
            this.gxtAfterIR.Size = new System.Drawing.Size(406, 155);
            this.gxtAfterIR.TabIndex = 55;
            this.gxtAfterIR.TabStop = false;
            this.gxtAfterIR.Text = "     After IR 이재기     ";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.gxtAfterIR_B, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.gxtAfterIR_A, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(394, 121);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // gxtAfterIR_B
            // 
            this.gxtAfterIR_B.Controls.Add(this.tableLayoutPanel24);
            this.gxtAfterIR_B.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtAfterIR_B.ForeColor = System.Drawing.Color.White;
            this.gxtAfterIR_B.Location = new System.Drawing.Point(200, 3);
            this.gxtAfterIR_B.Name = "gxtAfterIR_B";
            this.gxtAfterIR_B.Size = new System.Drawing.Size(191, 115);
            this.gxtAfterIR_B.TabIndex = 57;
            this.gxtAfterIR_B.TabStop = false;
            this.gxtAfterIR_B.Text = "     B     ";
            // 
            // tableLayoutPanel24
            // 
            this.tableLayoutPanel24.ColumnCount = 3;
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel24.Controls.Add(this.lbl_mm8, 2, 1);
            this.tableLayoutPanel24.Controls.Add(this.txtAfterIRBX2, 1, 1);
            this.tableLayoutPanel24.Controls.Add(this.lbl_mm7, 2, 0);
            this.tableLayoutPanel24.Controls.Add(this.lbl_AfterIR_B_X2, 0, 1);
            this.tableLayoutPanel24.Controls.Add(this.txtAfterIRBX1, 1, 0);
            this.tableLayoutPanel24.Controls.Add(this.lbl_AfterIR_B_X1, 0, 0);
            this.tableLayoutPanel24.Location = new System.Drawing.Point(3, 28);
            this.tableLayoutPanel24.Name = "tableLayoutPanel24";
            this.tableLayoutPanel24.RowCount = 2;
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel24.Size = new System.Drawing.Size(182, 84);
            this.tableLayoutPanel24.TabIndex = 2;
            // 
            // lbl_mm8
            // 
            this.lbl_mm8.AutoSize = true;
            this.lbl_mm8.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.lbl_mm8.ForeColor = System.Drawing.Color.White;
            this.lbl_mm8.Location = new System.Drawing.Point(129, 42);
            this.lbl_mm8.Name = "lbl_mm8";
            this.lbl_mm8.Size = new System.Drawing.Size(47, 20);
            this.lbl_mm8.TabIndex = 59;
            this.lbl_mm8.Text = "(mm)";
            // 
            // txtAfterIRBX2
            // 
            this.txtAfterIRBX2.Font = new System.Drawing.Font("굴림", 13F);
            this.txtAfterIRBX2.Location = new System.Drawing.Point(57, 45);
            this.txtAfterIRBX2.Name = "txtAfterIRBX2";
            this.txtAfterIRBX2.Size = new System.Drawing.Size(66, 27);
            this.txtAfterIRBX2.TabIndex = 60;
            // 
            // lbl_mm7
            // 
            this.lbl_mm7.AutoSize = true;
            this.lbl_mm7.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.lbl_mm7.ForeColor = System.Drawing.Color.White;
            this.lbl_mm7.Location = new System.Drawing.Point(129, 0);
            this.lbl_mm7.Name = "lbl_mm7";
            this.lbl_mm7.Size = new System.Drawing.Size(47, 20);
            this.lbl_mm7.TabIndex = 59;
            this.lbl_mm7.Text = "(mm)";
            // 
            // lbl_AfterIR_B_X2
            // 
            this.lbl_AfterIR_B_X2.AutoSize = true;
            this.lbl_AfterIR_B_X2.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_AfterIR_B_X2.ForeColor = System.Drawing.Color.White;
            this.lbl_AfterIR_B_X2.Location = new System.Drawing.Point(3, 42);
            this.lbl_AfterIR_B_X2.Name = "lbl_AfterIR_B_X2";
            this.lbl_AfterIR_B_X2.Size = new System.Drawing.Size(45, 21);
            this.lbl_AfterIR_B_X2.TabIndex = 58;
            this.lbl_AfterIR_B_X2.Text = "X2 : ";
            // 
            // txtAfterIRBX1
            // 
            this.txtAfterIRBX1.Font = new System.Drawing.Font("굴림", 13F);
            this.txtAfterIRBX1.Location = new System.Drawing.Point(57, 3);
            this.txtAfterIRBX1.Name = "txtAfterIRBX1";
            this.txtAfterIRBX1.Size = new System.Drawing.Size(66, 27);
            this.txtAfterIRBX1.TabIndex = 60;
            // 
            // lbl_AfterIR_B_X1
            // 
            this.lbl_AfterIR_B_X1.AutoSize = true;
            this.lbl_AfterIR_B_X1.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_AfterIR_B_X1.ForeColor = System.Drawing.Color.White;
            this.lbl_AfterIR_B_X1.Location = new System.Drawing.Point(3, 0);
            this.lbl_AfterIR_B_X1.Name = "lbl_AfterIR_B_X1";
            this.lbl_AfterIR_B_X1.Size = new System.Drawing.Size(45, 21);
            this.lbl_AfterIR_B_X1.TabIndex = 58;
            this.lbl_AfterIR_B_X1.Text = "X1 : ";
            // 
            // gxtAfterIR_A
            // 
            this.gxtAfterIR_A.Controls.Add(this.tableLayoutPanel23);
            this.gxtAfterIR_A.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtAfterIR_A.ForeColor = System.Drawing.Color.White;
            this.gxtAfterIR_A.Location = new System.Drawing.Point(3, 3);
            this.gxtAfterIR_A.Name = "gxtAfterIR_A";
            this.gxtAfterIR_A.Size = new System.Drawing.Size(191, 115);
            this.gxtAfterIR_A.TabIndex = 56;
            this.gxtAfterIR_A.TabStop = false;
            this.gxtAfterIR_A.Text = "     A     ";
            // 
            // tableLayoutPanel23
            // 
            this.tableLayoutPanel23.ColumnCount = 3;
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel23.Controls.Add(this.lbl_mm6, 2, 1);
            this.tableLayoutPanel23.Controls.Add(this.txtAfterIRAX2, 1, 1);
            this.tableLayoutPanel23.Controls.Add(this.lbl_mm5, 2, 0);
            this.tableLayoutPanel23.Controls.Add(this.lbl_AfterIR_A_X2, 0, 1);
            this.tableLayoutPanel23.Controls.Add(this.txtAfterIRAX1, 1, 0);
            this.tableLayoutPanel23.Controls.Add(this.lbl_AfterIR_A_X1, 0, 0);
            this.tableLayoutPanel23.Location = new System.Drawing.Point(3, 28);
            this.tableLayoutPanel23.Name = "tableLayoutPanel23";
            this.tableLayoutPanel23.RowCount = 2;
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel23.Size = new System.Drawing.Size(182, 84);
            this.tableLayoutPanel23.TabIndex = 2;
            // 
            // lbl_mm6
            // 
            this.lbl_mm6.AutoSize = true;
            this.lbl_mm6.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.lbl_mm6.ForeColor = System.Drawing.Color.White;
            this.lbl_mm6.Location = new System.Drawing.Point(129, 42);
            this.lbl_mm6.Name = "lbl_mm6";
            this.lbl_mm6.Size = new System.Drawing.Size(47, 20);
            this.lbl_mm6.TabIndex = 59;
            this.lbl_mm6.Text = "(mm)";
            // 
            // txtAfterIRAX2
            // 
            this.txtAfterIRAX2.Font = new System.Drawing.Font("굴림", 13F);
            this.txtAfterIRAX2.Location = new System.Drawing.Point(57, 45);
            this.txtAfterIRAX2.Name = "txtAfterIRAX2";
            this.txtAfterIRAX2.Size = new System.Drawing.Size(66, 27);
            this.txtAfterIRAX2.TabIndex = 60;
            // 
            // lbl_mm5
            // 
            this.lbl_mm5.AutoSize = true;
            this.lbl_mm5.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.lbl_mm5.ForeColor = System.Drawing.Color.White;
            this.lbl_mm5.Location = new System.Drawing.Point(129, 0);
            this.lbl_mm5.Name = "lbl_mm5";
            this.lbl_mm5.Size = new System.Drawing.Size(47, 20);
            this.lbl_mm5.TabIndex = 59;
            this.lbl_mm5.Text = "(mm)";
            // 
            // lbl_AfterIR_A_X2
            // 
            this.lbl_AfterIR_A_X2.AutoSize = true;
            this.lbl_AfterIR_A_X2.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_AfterIR_A_X2.ForeColor = System.Drawing.Color.White;
            this.lbl_AfterIR_A_X2.Location = new System.Drawing.Point(3, 42);
            this.lbl_AfterIR_A_X2.Name = "lbl_AfterIR_A_X2";
            this.lbl_AfterIR_A_X2.Size = new System.Drawing.Size(45, 21);
            this.lbl_AfterIR_A_X2.TabIndex = 58;
            this.lbl_AfterIR_A_X2.Text = "X2 : ";
            // 
            // txtAfterIRAX1
            // 
            this.txtAfterIRAX1.Font = new System.Drawing.Font("굴림", 13F);
            this.txtAfterIRAX1.Location = new System.Drawing.Point(57, 3);
            this.txtAfterIRAX1.Name = "txtAfterIRAX1";
            this.txtAfterIRAX1.Size = new System.Drawing.Size(66, 27);
            this.txtAfterIRAX1.TabIndex = 60;
            // 
            // lbl_AfterIR_A_X1
            // 
            this.lbl_AfterIR_A_X1.AutoSize = true;
            this.lbl_AfterIR_A_X1.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_AfterIR_A_X1.ForeColor = System.Drawing.Color.White;
            this.lbl_AfterIR_A_X1.Location = new System.Drawing.Point(3, 0);
            this.lbl_AfterIR_A_X1.Name = "lbl_AfterIR_A_X1";
            this.lbl_AfterIR_A_X1.Size = new System.Drawing.Size(45, 21);
            this.lbl_AfterIR_A_X1.TabIndex = 58;
            this.lbl_AfterIR_A_X1.Text = "X1 : ";
            // 
            // gxtUldRotate180
            // 
            this.gxtUldRotate180.Controls.Add(this.tableLayoutPanel3);
            this.gxtUldRotate180.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtUldRotate180.ForeColor = System.Drawing.Color.White;
            this.gxtUldRotate180.Location = new System.Drawing.Point(1291, 29);
            this.gxtUldRotate180.Name = "gxtUldRotate180";
            this.gxtUldRotate180.Size = new System.Drawing.Size(406, 155);
            this.gxtUldRotate180.TabIndex = 56;
            this.gxtUldRotate180.TabStop = false;
            this.gxtUldRotate180.Text = "     ULD Rotate 180     ";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.gxtUldRotate180_B, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.gxtUldRotate180_A, 0, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(394, 121);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // gxtUldRotate180_B
            // 
            this.gxtUldRotate180_B.Controls.Add(this.tableLayoutPanel26);
            this.gxtUldRotate180_B.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtUldRotate180_B.ForeColor = System.Drawing.Color.White;
            this.gxtUldRotate180_B.Location = new System.Drawing.Point(200, 3);
            this.gxtUldRotate180_B.Name = "gxtUldRotate180_B";
            this.gxtUldRotate180_B.Size = new System.Drawing.Size(191, 115);
            this.gxtUldRotate180_B.TabIndex = 57;
            this.gxtUldRotate180_B.TabStop = false;
            this.gxtUldRotate180_B.Text = "     B     ";
            // 
            // tableLayoutPanel26
            // 
            this.tableLayoutPanel26.ColumnCount = 3;
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel26.Controls.Add(this.lbl_mm12, 2, 1);
            this.tableLayoutPanel26.Controls.Add(this.txtUldRotate180BX2, 1, 1);
            this.tableLayoutPanel26.Controls.Add(this.lbl_mm11, 2, 0);
            this.tableLayoutPanel26.Controls.Add(this.lbl_UldRotate180_B_X2, 0, 1);
            this.tableLayoutPanel26.Controls.Add(this.txtUldRotate180BX1, 1, 0);
            this.tableLayoutPanel26.Controls.Add(this.lbl_UldRotate180_B_X1, 0, 0);
            this.tableLayoutPanel26.Location = new System.Drawing.Point(3, 28);
            this.tableLayoutPanel26.Name = "tableLayoutPanel26";
            this.tableLayoutPanel26.RowCount = 2;
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel26.Size = new System.Drawing.Size(182, 84);
            this.tableLayoutPanel26.TabIndex = 2;
            // 
            // lbl_mm12
            // 
            this.lbl_mm12.AutoSize = true;
            this.lbl_mm12.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.lbl_mm12.ForeColor = System.Drawing.Color.White;
            this.lbl_mm12.Location = new System.Drawing.Point(129, 42);
            this.lbl_mm12.Name = "lbl_mm12";
            this.lbl_mm12.Size = new System.Drawing.Size(47, 20);
            this.lbl_mm12.TabIndex = 59;
            this.lbl_mm12.Text = "(mm)";
            // 
            // txtUldRotate180BX2
            // 
            this.txtUldRotate180BX2.Font = new System.Drawing.Font("굴림", 13F);
            this.txtUldRotate180BX2.Location = new System.Drawing.Point(57, 45);
            this.txtUldRotate180BX2.Name = "txtUldRotate180BX2";
            this.txtUldRotate180BX2.Size = new System.Drawing.Size(66, 27);
            this.txtUldRotate180BX2.TabIndex = 60;
            // 
            // lbl_mm11
            // 
            this.lbl_mm11.AutoSize = true;
            this.lbl_mm11.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.lbl_mm11.ForeColor = System.Drawing.Color.White;
            this.lbl_mm11.Location = new System.Drawing.Point(129, 0);
            this.lbl_mm11.Name = "lbl_mm11";
            this.lbl_mm11.Size = new System.Drawing.Size(47, 20);
            this.lbl_mm11.TabIndex = 59;
            this.lbl_mm11.Text = "(mm)";
            // 
            // lbl_UldRotate180_B_X2
            // 
            this.lbl_UldRotate180_B_X2.AutoSize = true;
            this.lbl_UldRotate180_B_X2.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_UldRotate180_B_X2.ForeColor = System.Drawing.Color.White;
            this.lbl_UldRotate180_B_X2.Location = new System.Drawing.Point(3, 42);
            this.lbl_UldRotate180_B_X2.Name = "lbl_UldRotate180_B_X2";
            this.lbl_UldRotate180_B_X2.Size = new System.Drawing.Size(45, 21);
            this.lbl_UldRotate180_B_X2.TabIndex = 58;
            this.lbl_UldRotate180_B_X2.Text = "X2 : ";
            // 
            // txtUldRotate180BX1
            // 
            this.txtUldRotate180BX1.Font = new System.Drawing.Font("굴림", 13F);
            this.txtUldRotate180BX1.Location = new System.Drawing.Point(57, 3);
            this.txtUldRotate180BX1.Name = "txtUldRotate180BX1";
            this.txtUldRotate180BX1.Size = new System.Drawing.Size(66, 27);
            this.txtUldRotate180BX1.TabIndex = 60;
            // 
            // lbl_UldRotate180_B_X1
            // 
            this.lbl_UldRotate180_B_X1.AutoSize = true;
            this.lbl_UldRotate180_B_X1.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_UldRotate180_B_X1.ForeColor = System.Drawing.Color.White;
            this.lbl_UldRotate180_B_X1.Location = new System.Drawing.Point(3, 0);
            this.lbl_UldRotate180_B_X1.Name = "lbl_UldRotate180_B_X1";
            this.lbl_UldRotate180_B_X1.Size = new System.Drawing.Size(45, 21);
            this.lbl_UldRotate180_B_X1.TabIndex = 58;
            this.lbl_UldRotate180_B_X1.Text = "X1 : ";
            // 
            // gxtUldRotate180_A
            // 
            this.gxtUldRotate180_A.Controls.Add(this.tableLayoutPanel25);
            this.gxtUldRotate180_A.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtUldRotate180_A.ForeColor = System.Drawing.Color.White;
            this.gxtUldRotate180_A.Location = new System.Drawing.Point(3, 3);
            this.gxtUldRotate180_A.Name = "gxtUldRotate180_A";
            this.gxtUldRotate180_A.Size = new System.Drawing.Size(191, 115);
            this.gxtUldRotate180_A.TabIndex = 56;
            this.gxtUldRotate180_A.TabStop = false;
            this.gxtUldRotate180_A.Text = "     A     ";
            // 
            // tableLayoutPanel25
            // 
            this.tableLayoutPanel25.ColumnCount = 3;
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel25.Controls.Add(this.lbl_mm10, 2, 1);
            this.tableLayoutPanel25.Controls.Add(this.txtULDrotate180AX2, 1, 1);
            this.tableLayoutPanel25.Controls.Add(this.lbl_mm9, 2, 0);
            this.tableLayoutPanel25.Controls.Add(this.lbl_UldRotate180_A_X2, 0, 1);
            this.tableLayoutPanel25.Controls.Add(this.txtUldRotate180AX1, 1, 0);
            this.tableLayoutPanel25.Controls.Add(this.lbl_UldRotate180_A_X1, 0, 0);
            this.tableLayoutPanel25.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel25.Name = "tableLayoutPanel25";
            this.tableLayoutPanel25.RowCount = 2;
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel25.Size = new System.Drawing.Size(182, 84);
            this.tableLayoutPanel25.TabIndex = 2;
            // 
            // lbl_mm10
            // 
            this.lbl_mm10.AutoSize = true;
            this.lbl_mm10.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.lbl_mm10.ForeColor = System.Drawing.Color.White;
            this.lbl_mm10.Location = new System.Drawing.Point(129, 42);
            this.lbl_mm10.Name = "lbl_mm10";
            this.lbl_mm10.Size = new System.Drawing.Size(47, 20);
            this.lbl_mm10.TabIndex = 59;
            this.lbl_mm10.Text = "(mm)";
            // 
            // txtULDrotate180AX2
            // 
            this.txtULDrotate180AX2.Font = new System.Drawing.Font("굴림", 13F);
            this.txtULDrotate180AX2.Location = new System.Drawing.Point(57, 45);
            this.txtULDrotate180AX2.Name = "txtULDrotate180AX2";
            this.txtULDrotate180AX2.Size = new System.Drawing.Size(66, 27);
            this.txtULDrotate180AX2.TabIndex = 60;
            // 
            // lbl_mm9
            // 
            this.lbl_mm9.AutoSize = true;
            this.lbl_mm9.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.lbl_mm9.ForeColor = System.Drawing.Color.White;
            this.lbl_mm9.Location = new System.Drawing.Point(129, 0);
            this.lbl_mm9.Name = "lbl_mm9";
            this.lbl_mm9.Size = new System.Drawing.Size(47, 20);
            this.lbl_mm9.TabIndex = 59;
            this.lbl_mm9.Text = "(mm)";
            // 
            // lbl_UldRotate180_A_X2
            // 
            this.lbl_UldRotate180_A_X2.AutoSize = true;
            this.lbl_UldRotate180_A_X2.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_UldRotate180_A_X2.ForeColor = System.Drawing.Color.White;
            this.lbl_UldRotate180_A_X2.Location = new System.Drawing.Point(3, 42);
            this.lbl_UldRotate180_A_X2.Name = "lbl_UldRotate180_A_X2";
            this.lbl_UldRotate180_A_X2.Size = new System.Drawing.Size(45, 21);
            this.lbl_UldRotate180_A_X2.TabIndex = 58;
            this.lbl_UldRotate180_A_X2.Text = "X2 : ";
            // 
            // txtUldRotate180AX1
            // 
            this.txtUldRotate180AX1.Font = new System.Drawing.Font("굴림", 13F);
            this.txtUldRotate180AX1.Location = new System.Drawing.Point(57, 3);
            this.txtUldRotate180AX1.Name = "txtUldRotate180AX1";
            this.txtUldRotate180AX1.Size = new System.Drawing.Size(66, 27);
            this.txtUldRotate180AX1.TabIndex = 60;
            // 
            // lbl_UldRotate180_A_X1
            // 
            this.lbl_UldRotate180_A_X1.AutoSize = true;
            this.lbl_UldRotate180_A_X1.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_UldRotate180_A_X1.ForeColor = System.Drawing.Color.White;
            this.lbl_UldRotate180_A_X1.Location = new System.Drawing.Point(3, 0);
            this.lbl_UldRotate180_A_X1.Name = "lbl_UldRotate180_A_X1";
            this.lbl_UldRotate180_A_X1.Size = new System.Drawing.Size(45, 21);
            this.lbl_UldRotate180_A_X1.TabIndex = 58;
            this.lbl_UldRotate180_A_X1.Text = "X1 : ";
            // 
            // gxtInspectionOffset
            // 
            this.gxtInspectionOffset.Controls.Add(this.tableLayoutPanel5);
            this.gxtInspectionOffset.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtInspectionOffset.ForeColor = System.Drawing.Color.White;
            this.gxtInspectionOffset.Location = new System.Drawing.Point(460, 190);
            this.gxtInspectionOffset.Name = "gxtInspectionOffset";
            this.gxtInspectionOffset.Size = new System.Drawing.Size(825, 155);
            this.gxtInspectionOffset.TabIndex = 54;
            this.gxtInspectionOffset.TabStop = false;
            this.gxtInspectionOffset.Text = "     인스팩션 Offset     ";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 4;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.Controls.Add(this.gxtInspection_4, 3, 0);
            this.tableLayoutPanel5.Controls.Add(this.gxtInspection_3, 2, 0);
            this.tableLayoutPanel5.Controls.Add(this.gxtInspection_1, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.gxtInspection_2, 0, 0);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(813, 121);
            this.tableLayoutPanel5.TabIndex = 1;
            // 
            // gxtInspection_4
            // 
            this.gxtInspection_4.Controls.Add(this.tableLayoutPanel30);
            this.gxtInspection_4.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtInspection_4.ForeColor = System.Drawing.Color.White;
            this.gxtInspection_4.Location = new System.Drawing.Point(612, 3);
            this.gxtInspection_4.Name = "gxtInspection_4";
            this.gxtInspection_4.Size = new System.Drawing.Size(197, 115);
            this.gxtInspection_4.TabIndex = 59;
            this.gxtInspection_4.TabStop = false;
            this.gxtInspection_4.Text = "     4     ";
            // 
            // tableLayoutPanel30
            // 
            this.tableLayoutPanel30.ColumnCount = 3;
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel30.Controls.Add(this.lbl_mm20, 2, 1);
            this.tableLayoutPanel30.Controls.Add(this.txtInspection4Y, 1, 1);
            this.tableLayoutPanel30.Controls.Add(this.lbl_mm19, 2, 0);
            this.tableLayoutPanel30.Controls.Add(this.lbl_Inspection_4_Y, 0, 1);
            this.tableLayoutPanel30.Controls.Add(this.txtInspection4X, 1, 0);
            this.tableLayoutPanel30.Controls.Add(this.lbl_Inspection_4_X, 0, 0);
            this.tableLayoutPanel30.Location = new System.Drawing.Point(7, 25);
            this.tableLayoutPanel30.Name = "tableLayoutPanel30";
            this.tableLayoutPanel30.RowCount = 2;
            this.tableLayoutPanel30.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel30.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel30.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel30.Size = new System.Drawing.Size(182, 84);
            this.tableLayoutPanel30.TabIndex = 3;
            // 
            // lbl_mm20
            // 
            this.lbl_mm20.AutoSize = true;
            this.lbl_mm20.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.lbl_mm20.ForeColor = System.Drawing.Color.White;
            this.lbl_mm20.Location = new System.Drawing.Point(129, 42);
            this.lbl_mm20.Name = "lbl_mm20";
            this.lbl_mm20.Size = new System.Drawing.Size(47, 20);
            this.lbl_mm20.TabIndex = 59;
            this.lbl_mm20.Text = "(mm)";
            // 
            // txtInspection4Y
            // 
            this.txtInspection4Y.Font = new System.Drawing.Font("굴림", 13F);
            this.txtInspection4Y.Location = new System.Drawing.Point(57, 45);
            this.txtInspection4Y.Name = "txtInspection4Y";
            this.txtInspection4Y.Size = new System.Drawing.Size(66, 27);
            this.txtInspection4Y.TabIndex = 60;
            // 
            // lbl_mm19
            // 
            this.lbl_mm19.AutoSize = true;
            this.lbl_mm19.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.lbl_mm19.ForeColor = System.Drawing.Color.White;
            this.lbl_mm19.Location = new System.Drawing.Point(129, 0);
            this.lbl_mm19.Name = "lbl_mm19";
            this.lbl_mm19.Size = new System.Drawing.Size(47, 20);
            this.lbl_mm19.TabIndex = 59;
            this.lbl_mm19.Text = "(mm)";
            // 
            // lbl_Inspection_4_Y
            // 
            this.lbl_Inspection_4_Y.AutoSize = true;
            this.lbl_Inspection_4_Y.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Inspection_4_Y.ForeColor = System.Drawing.Color.White;
            this.lbl_Inspection_4_Y.Location = new System.Drawing.Point(3, 42);
            this.lbl_Inspection_4_Y.Name = "lbl_Inspection_4_Y";
            this.lbl_Inspection_4_Y.Size = new System.Drawing.Size(20, 21);
            this.lbl_Inspection_4_Y.TabIndex = 58;
            this.lbl_Inspection_4_Y.Text = "Y";
            // 
            // txtInspection4X
            // 
            this.txtInspection4X.Font = new System.Drawing.Font("굴림", 13F);
            this.txtInspection4X.Location = new System.Drawing.Point(57, 3);
            this.txtInspection4X.Name = "txtInspection4X";
            this.txtInspection4X.Size = new System.Drawing.Size(66, 27);
            this.txtInspection4X.TabIndex = 60;
            // 
            // lbl_Inspection_4_X
            // 
            this.lbl_Inspection_4_X.AutoSize = true;
            this.lbl_Inspection_4_X.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Inspection_4_X.ForeColor = System.Drawing.Color.White;
            this.lbl_Inspection_4_X.Location = new System.Drawing.Point(3, 0);
            this.lbl_Inspection_4_X.Name = "lbl_Inspection_4_X";
            this.lbl_Inspection_4_X.Size = new System.Drawing.Size(20, 21);
            this.lbl_Inspection_4_X.TabIndex = 58;
            this.lbl_Inspection_4_X.Text = "X";
            // 
            // gxtInspection_3
            // 
            this.gxtInspection_3.Controls.Add(this.tableLayoutPanel29);
            this.gxtInspection_3.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtInspection_3.ForeColor = System.Drawing.Color.White;
            this.gxtInspection_3.Location = new System.Drawing.Point(409, 3);
            this.gxtInspection_3.Name = "gxtInspection_3";
            this.gxtInspection_3.Size = new System.Drawing.Size(197, 115);
            this.gxtInspection_3.TabIndex = 58;
            this.gxtInspection_3.TabStop = false;
            this.gxtInspection_3.Text = "     3     ";
            // 
            // tableLayoutPanel29
            // 
            this.tableLayoutPanel29.ColumnCount = 3;
            this.tableLayoutPanel29.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel29.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel29.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel29.Controls.Add(this.lbl_mm18, 2, 1);
            this.tableLayoutPanel29.Controls.Add(this.txtInspection3Y, 1, 1);
            this.tableLayoutPanel29.Controls.Add(this.lbl_mm17, 2, 0);
            this.tableLayoutPanel29.Controls.Add(this.lbl_Inspection_3_Y, 0, 1);
            this.tableLayoutPanel29.Controls.Add(this.txtInspection3X, 1, 0);
            this.tableLayoutPanel29.Controls.Add(this.lbl_Inspection_3_X, 0, 0);
            this.tableLayoutPanel29.Location = new System.Drawing.Point(6, 25);
            this.tableLayoutPanel29.Name = "tableLayoutPanel29";
            this.tableLayoutPanel29.RowCount = 2;
            this.tableLayoutPanel29.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel29.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel29.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel29.Size = new System.Drawing.Size(182, 84);
            this.tableLayoutPanel29.TabIndex = 3;
            // 
            // lbl_mm18
            // 
            this.lbl_mm18.AutoSize = true;
            this.lbl_mm18.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.lbl_mm18.ForeColor = System.Drawing.Color.White;
            this.lbl_mm18.Location = new System.Drawing.Point(129, 42);
            this.lbl_mm18.Name = "lbl_mm18";
            this.lbl_mm18.Size = new System.Drawing.Size(47, 20);
            this.lbl_mm18.TabIndex = 59;
            this.lbl_mm18.Text = "(mm)";
            // 
            // txtInspection3Y
            // 
            this.txtInspection3Y.Font = new System.Drawing.Font("굴림", 13F);
            this.txtInspection3Y.Location = new System.Drawing.Point(57, 45);
            this.txtInspection3Y.Name = "txtInspection3Y";
            this.txtInspection3Y.Size = new System.Drawing.Size(66, 27);
            this.txtInspection3Y.TabIndex = 60;
            // 
            // lbl_mm17
            // 
            this.lbl_mm17.AutoSize = true;
            this.lbl_mm17.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.lbl_mm17.ForeColor = System.Drawing.Color.White;
            this.lbl_mm17.Location = new System.Drawing.Point(129, 0);
            this.lbl_mm17.Name = "lbl_mm17";
            this.lbl_mm17.Size = new System.Drawing.Size(47, 20);
            this.lbl_mm17.TabIndex = 59;
            this.lbl_mm17.Text = "(mm)";
            // 
            // lbl_Inspection_3_Y
            // 
            this.lbl_Inspection_3_Y.AutoSize = true;
            this.lbl_Inspection_3_Y.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Inspection_3_Y.ForeColor = System.Drawing.Color.White;
            this.lbl_Inspection_3_Y.Location = new System.Drawing.Point(3, 42);
            this.lbl_Inspection_3_Y.Name = "lbl_Inspection_3_Y";
            this.lbl_Inspection_3_Y.Size = new System.Drawing.Size(20, 21);
            this.lbl_Inspection_3_Y.TabIndex = 58;
            this.lbl_Inspection_3_Y.Text = "Y";
            // 
            // txtInspection3X
            // 
            this.txtInspection3X.Font = new System.Drawing.Font("굴림", 13F);
            this.txtInspection3X.Location = new System.Drawing.Point(57, 3);
            this.txtInspection3X.Name = "txtInspection3X";
            this.txtInspection3X.Size = new System.Drawing.Size(66, 27);
            this.txtInspection3X.TabIndex = 60;
            // 
            // lbl_Inspection_3_X
            // 
            this.lbl_Inspection_3_X.AutoSize = true;
            this.lbl_Inspection_3_X.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Inspection_3_X.ForeColor = System.Drawing.Color.White;
            this.lbl_Inspection_3_X.Location = new System.Drawing.Point(3, 0);
            this.lbl_Inspection_3_X.Name = "lbl_Inspection_3_X";
            this.lbl_Inspection_3_X.Size = new System.Drawing.Size(20, 21);
            this.lbl_Inspection_3_X.TabIndex = 58;
            this.lbl_Inspection_3_X.Text = "X";
            // 
            // gxtInspection_1
            // 
            this.gxtInspection_1.Controls.Add(this.tableLayoutPanel27);
            this.gxtInspection_1.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtInspection_1.ForeColor = System.Drawing.Color.White;
            this.gxtInspection_1.Location = new System.Drawing.Point(3, 3);
            this.gxtInspection_1.Name = "gxtInspection_1";
            this.gxtInspection_1.Size = new System.Drawing.Size(197, 115);
            this.gxtInspection_1.TabIndex = 57;
            this.gxtInspection_1.TabStop = false;
            this.gxtInspection_1.Text = "     1     ";
            // 
            // tableLayoutPanel27
            // 
            this.tableLayoutPanel27.ColumnCount = 3;
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel27.Controls.Add(this.lbl_mm14, 2, 1);
            this.tableLayoutPanel27.Controls.Add(this.txtInspection1Y, 1, 1);
            this.tableLayoutPanel27.Controls.Add(this.lbl_mm13, 2, 0);
            this.tableLayoutPanel27.Controls.Add(this.lbl_Inspection_1_Y, 0, 1);
            this.tableLayoutPanel27.Controls.Add(this.txtInspection1X, 1, 0);
            this.tableLayoutPanel27.Controls.Add(this.lbl_Inspection_1_X, 0, 0);
            this.tableLayoutPanel27.Location = new System.Drawing.Point(6, 25);
            this.tableLayoutPanel27.Name = "tableLayoutPanel27";
            this.tableLayoutPanel27.RowCount = 2;
            this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel27.Size = new System.Drawing.Size(182, 84);
            this.tableLayoutPanel27.TabIndex = 3;
            // 
            // lbl_mm14
            // 
            this.lbl_mm14.AutoSize = true;
            this.lbl_mm14.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.lbl_mm14.ForeColor = System.Drawing.Color.White;
            this.lbl_mm14.Location = new System.Drawing.Point(129, 42);
            this.lbl_mm14.Name = "lbl_mm14";
            this.lbl_mm14.Size = new System.Drawing.Size(47, 20);
            this.lbl_mm14.TabIndex = 59;
            this.lbl_mm14.Text = "(mm)";
            // 
            // txtInspection1Y
            // 
            this.txtInspection1Y.Font = new System.Drawing.Font("굴림", 13F);
            this.txtInspection1Y.Location = new System.Drawing.Point(57, 45);
            this.txtInspection1Y.Name = "txtInspection1Y";
            this.txtInspection1Y.Size = new System.Drawing.Size(66, 27);
            this.txtInspection1Y.TabIndex = 60;
            // 
            // lbl_mm13
            // 
            this.lbl_mm13.AutoSize = true;
            this.lbl_mm13.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.lbl_mm13.ForeColor = System.Drawing.Color.White;
            this.lbl_mm13.Location = new System.Drawing.Point(129, 0);
            this.lbl_mm13.Name = "lbl_mm13";
            this.lbl_mm13.Size = new System.Drawing.Size(47, 20);
            this.lbl_mm13.TabIndex = 59;
            this.lbl_mm13.Text = "(mm)";
            // 
            // lbl_Inspection_1_Y
            // 
            this.lbl_Inspection_1_Y.AutoSize = true;
            this.lbl_Inspection_1_Y.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Inspection_1_Y.ForeColor = System.Drawing.Color.White;
            this.lbl_Inspection_1_Y.Location = new System.Drawing.Point(3, 42);
            this.lbl_Inspection_1_Y.Name = "lbl_Inspection_1_Y";
            this.lbl_Inspection_1_Y.Size = new System.Drawing.Size(20, 21);
            this.lbl_Inspection_1_Y.TabIndex = 58;
            this.lbl_Inspection_1_Y.Text = "Y";
            // 
            // txtInspection1X
            // 
            this.txtInspection1X.Font = new System.Drawing.Font("굴림", 13F);
            this.txtInspection1X.Location = new System.Drawing.Point(57, 3);
            this.txtInspection1X.Name = "txtInspection1X";
            this.txtInspection1X.Size = new System.Drawing.Size(66, 27);
            this.txtInspection1X.TabIndex = 60;
            // 
            // lbl_Inspection_1_X
            // 
            this.lbl_Inspection_1_X.AutoSize = true;
            this.lbl_Inspection_1_X.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Inspection_1_X.ForeColor = System.Drawing.Color.White;
            this.lbl_Inspection_1_X.Location = new System.Drawing.Point(3, 0);
            this.lbl_Inspection_1_X.Name = "lbl_Inspection_1_X";
            this.lbl_Inspection_1_X.Size = new System.Drawing.Size(20, 21);
            this.lbl_Inspection_1_X.TabIndex = 58;
            this.lbl_Inspection_1_X.Text = "X";
            // 
            // gxtInspection_2
            // 
            this.gxtInspection_2.Controls.Add(this.tableLayoutPanel28);
            this.gxtInspection_2.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtInspection_2.ForeColor = System.Drawing.Color.White;
            this.gxtInspection_2.Location = new System.Drawing.Point(206, 3);
            this.gxtInspection_2.Name = "gxtInspection_2";
            this.gxtInspection_2.Size = new System.Drawing.Size(197, 115);
            this.gxtInspection_2.TabIndex = 56;
            this.gxtInspection_2.TabStop = false;
            this.gxtInspection_2.Text = "     2     ";
            // 
            // tableLayoutPanel28
            // 
            this.tableLayoutPanel28.ColumnCount = 3;
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel28.Controls.Add(this.lbl_mm16, 2, 1);
            this.tableLayoutPanel28.Controls.Add(this.txtInspection2Y, 1, 1);
            this.tableLayoutPanel28.Controls.Add(this.lbl_mm15, 2, 0);
            this.tableLayoutPanel28.Controls.Add(this.lbl_Inspection_2_Y, 0, 1);
            this.tableLayoutPanel28.Controls.Add(this.txtInspection2X, 1, 0);
            this.tableLayoutPanel28.Controls.Add(this.lbl_Inspection_2_X, 0, 0);
            this.tableLayoutPanel28.Location = new System.Drawing.Point(6, 25);
            this.tableLayoutPanel28.Name = "tableLayoutPanel28";
            this.tableLayoutPanel28.RowCount = 2;
            this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel28.Size = new System.Drawing.Size(182, 84);
            this.tableLayoutPanel28.TabIndex = 3;
            // 
            // lbl_mm16
            // 
            this.lbl_mm16.AutoSize = true;
            this.lbl_mm16.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.lbl_mm16.ForeColor = System.Drawing.Color.White;
            this.lbl_mm16.Location = new System.Drawing.Point(129, 42);
            this.lbl_mm16.Name = "lbl_mm16";
            this.lbl_mm16.Size = new System.Drawing.Size(47, 20);
            this.lbl_mm16.TabIndex = 59;
            this.lbl_mm16.Text = "(mm)";
            // 
            // txtInspection2Y
            // 
            this.txtInspection2Y.Font = new System.Drawing.Font("굴림", 13F);
            this.txtInspection2Y.Location = new System.Drawing.Point(57, 45);
            this.txtInspection2Y.Name = "txtInspection2Y";
            this.txtInspection2Y.Size = new System.Drawing.Size(66, 27);
            this.txtInspection2Y.TabIndex = 60;
            // 
            // lbl_mm15
            // 
            this.lbl_mm15.AutoSize = true;
            this.lbl_mm15.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.lbl_mm15.ForeColor = System.Drawing.Color.White;
            this.lbl_mm15.Location = new System.Drawing.Point(129, 0);
            this.lbl_mm15.Name = "lbl_mm15";
            this.lbl_mm15.Size = new System.Drawing.Size(47, 20);
            this.lbl_mm15.TabIndex = 59;
            this.lbl_mm15.Text = "(mm)";
            // 
            // lbl_Inspection_2_Y
            // 
            this.lbl_Inspection_2_Y.AutoSize = true;
            this.lbl_Inspection_2_Y.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Inspection_2_Y.ForeColor = System.Drawing.Color.White;
            this.lbl_Inspection_2_Y.Location = new System.Drawing.Point(3, 42);
            this.lbl_Inspection_2_Y.Name = "lbl_Inspection_2_Y";
            this.lbl_Inspection_2_Y.Size = new System.Drawing.Size(20, 21);
            this.lbl_Inspection_2_Y.TabIndex = 58;
            this.lbl_Inspection_2_Y.Text = "Y";
            // 
            // txtInspection2X
            // 
            this.txtInspection2X.Font = new System.Drawing.Font("굴림", 13F);
            this.txtInspection2X.Location = new System.Drawing.Point(57, 3);
            this.txtInspection2X.Name = "txtInspection2X";
            this.txtInspection2X.Size = new System.Drawing.Size(66, 27);
            this.txtInspection2X.TabIndex = 60;
            // 
            // lbl_Inspection_2_X
            // 
            this.lbl_Inspection_2_X.AutoSize = true;
            this.lbl_Inspection_2_X.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Inspection_2_X.ForeColor = System.Drawing.Color.White;
            this.lbl_Inspection_2_X.Location = new System.Drawing.Point(3, 0);
            this.lbl_Inspection_2_X.Name = "lbl_Inspection_2_X";
            this.lbl_Inspection_2_X.Size = new System.Drawing.Size(20, 21);
            this.lbl_Inspection_2_X.TabIndex = 58;
            this.lbl_Inspection_2_X.Text = "X";
            // 
            // Parameter_SequenceOffset
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Controls.Add(this.gxtInspectionOffset);
            this.Controls.Add(this.gxtUldRotate180);
            this.Controls.Add(this.gxtAfterIR);
            this.Controls.Add(this.gxtUnload);
            this.Controls.Add(this.gxtLoad);
            this.Name = "Parameter_SequenceOffset";
            this.Size = new System.Drawing.Size(1740, 875);
            this.gxtLoad.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.gxtLoad_B.ResumeLayout(false);
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel12.PerformLayout();
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel14.PerformLayout();
            this.tableLayoutPanel15.ResumeLayout(false);
            this.tableLayoutPanel15.PerformLayout();
            this.gxtLoad_A.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.gxtUnload.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.gxtUnload_B.ResumeLayout(false);
            this.tableLayoutPanel22.ResumeLayout(false);
            this.tableLayoutPanel22.PerformLayout();
            this.gxtUnload_A.ResumeLayout(false);
            this.tableLayoutPanel21.ResumeLayout(false);
            this.tableLayoutPanel21.PerformLayout();
            this.gxtAfterIR.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.gxtAfterIR_B.ResumeLayout(false);
            this.tableLayoutPanel24.ResumeLayout(false);
            this.tableLayoutPanel24.PerformLayout();
            this.gxtAfterIR_A.ResumeLayout(false);
            this.tableLayoutPanel23.ResumeLayout(false);
            this.tableLayoutPanel23.PerformLayout();
            this.gxtUldRotate180.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.gxtUldRotate180_B.ResumeLayout(false);
            this.tableLayoutPanel26.ResumeLayout(false);
            this.tableLayoutPanel26.PerformLayout();
            this.gxtUldRotate180_A.ResumeLayout(false);
            this.tableLayoutPanel25.ResumeLayout(false);
            this.tableLayoutPanel25.PerformLayout();
            this.gxtInspectionOffset.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.gxtInspection_4.ResumeLayout(false);
            this.tableLayoutPanel30.ResumeLayout(false);
            this.tableLayoutPanel30.PerformLayout();
            this.gxtInspection_3.ResumeLayout(false);
            this.tableLayoutPanel29.ResumeLayout(false);
            this.tableLayoutPanel29.PerformLayout();
            this.gxtInspection_1.ResumeLayout(false);
            this.tableLayoutPanel27.ResumeLayout(false);
            this.tableLayoutPanel27.PerformLayout();
            this.gxtInspection_2.ResumeLayout(false);
            this.tableLayoutPanel28.ResumeLayout(false);
            this.tableLayoutPanel28.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gxtLoad;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox gxtLoad_B;
        private System.Windows.Forms.GroupBox gxtLoad_A;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Label lbl_Load_A_X1;
        private System.Windows.Forms.Label lbl_mm1;
        private System.Windows.Forms.GroupBox gxtUnload;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.GroupBox gxtUnload_B;
        private System.Windows.Forms.GroupBox gxtUnload_A;
        private System.Windows.Forms.GroupBox gxtAfterIR;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.GroupBox gxtAfterIR_B;
        private System.Windows.Forms.GroupBox gxtAfterIR_A;
        private System.Windows.Forms.GroupBox gxtUldRotate180;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.GroupBox gxtUldRotate180_B;
        private System.Windows.Forms.GroupBox gxtUldRotate180_A;
        private System.Windows.Forms.GroupBox gxtInspectionOffset;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.GroupBox gxtInspection_4;
        private System.Windows.Forms.GroupBox gxtInspection_3;
        private System.Windows.Forms.GroupBox gxtInspection_1;
        private System.Windows.Forms.GroupBox gxtInspection_2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.TextBox txtloadBT2;
        private System.Windows.Forms.Label lbl_Load_B_T2;
        private System.Windows.Forms.Label lbl_angle4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.TextBox txtloadBX2;
        private System.Windows.Forms.Label lbl_Load_B_X2;
        private System.Windows.Forms.Label lbl_mm4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.TextBox txtloadBT1;
        private System.Windows.Forms.Label lbl_Load_B_T1;
        private System.Windows.Forms.Label lbl_angle3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private System.Windows.Forms.TextBox txtloadBX1;
        private System.Windows.Forms.Label lbl_Load_B_X1;
        private System.Windows.Forms.Label lbl_mm3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.TextBox txtloadAT2;
        private System.Windows.Forms.Label lbl_Load_A_T2;
        private System.Windows.Forms.Label lbl_angle2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.TextBox txtloadAX2;
        private System.Windows.Forms.Label lbl_Load_A_X2;
        private System.Windows.Forms.Label lbl_mm2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.TextBox txtloadAT1;
        private System.Windows.Forms.Label lbl_Load_A_T1;
        private System.Windows.Forms.Label lbl_angle1;
        private System.Windows.Forms.TextBox txtloadAX1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel22;
        private System.Windows.Forms.Label lbl_angle8;
        private System.Windows.Forms.TextBox txtUnloadBT2;
        private System.Windows.Forms.Label lbl_angle7;
        private System.Windows.Forms.Label lbl_Unload_B_T2;
        private System.Windows.Forms.TextBox txtUnloadBT1;
        private System.Windows.Forms.Label lbl_Unload_B_T1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel21;
        private System.Windows.Forms.Label lbl_angle6;
        private System.Windows.Forms.TextBox txtUnloadAT2;
        private System.Windows.Forms.Label lbl_angle5;
        private System.Windows.Forms.Label lbl_Unload_A_T2;
        private System.Windows.Forms.TextBox txtUnloadAT1;
        private System.Windows.Forms.Label lbl_Unload_A_T1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel24;
        private System.Windows.Forms.Label lbl_mm8;
        private System.Windows.Forms.TextBox txtAfterIRBX2;
        private System.Windows.Forms.Label lbl_mm7;
        private System.Windows.Forms.Label lbl_AfterIR_B_X2;
        private System.Windows.Forms.TextBox txtAfterIRBX1;
        private System.Windows.Forms.Label lbl_AfterIR_B_X1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel23;
        private System.Windows.Forms.Label lbl_mm6;
        private System.Windows.Forms.TextBox txtAfterIRAX2;
        private System.Windows.Forms.Label lbl_mm5;
        private System.Windows.Forms.Label lbl_AfterIR_A_X2;
        private System.Windows.Forms.TextBox txtAfterIRAX1;
        private System.Windows.Forms.Label lbl_AfterIR_A_X1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel26;
        private System.Windows.Forms.Label lbl_mm12;
        private System.Windows.Forms.TextBox txtUldRotate180BX2;
        private System.Windows.Forms.Label lbl_mm11;
        private System.Windows.Forms.Label lbl_UldRotate180_B_X2;
        private System.Windows.Forms.TextBox txtUldRotate180BX1;
        private System.Windows.Forms.Label lbl_UldRotate180_B_X1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel25;
        private System.Windows.Forms.Label lbl_mm10;
        private System.Windows.Forms.TextBox txtULDrotate180AX2;
        private System.Windows.Forms.Label lbl_mm9;
        private System.Windows.Forms.Label lbl_UldRotate180_A_X2;
        private System.Windows.Forms.TextBox txtUldRotate180AX1;
        private System.Windows.Forms.Label lbl_UldRotate180_A_X1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel30;
        private System.Windows.Forms.Label lbl_mm20;
        private System.Windows.Forms.TextBox txtInspection4Y;
        private System.Windows.Forms.Label lbl_mm19;
        private System.Windows.Forms.Label lbl_Inspection_4_Y;
        private System.Windows.Forms.TextBox txtInspection4X;
        private System.Windows.Forms.Label lbl_Inspection_4_X;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel29;
        private System.Windows.Forms.Label lbl_mm18;
        private System.Windows.Forms.TextBox txtInspection3Y;
        private System.Windows.Forms.Label lbl_mm17;
        private System.Windows.Forms.Label lbl_Inspection_3_Y;
        private System.Windows.Forms.TextBox txtInspection3X;
        private System.Windows.Forms.Label lbl_Inspection_3_X;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel27;
        private System.Windows.Forms.Label lbl_mm14;
        private System.Windows.Forms.TextBox txtInspection1Y;
        private System.Windows.Forms.Label lbl_mm13;
        private System.Windows.Forms.Label lbl_Inspection_1_Y;
        private System.Windows.Forms.TextBox txtInspection1X;
        private System.Windows.Forms.Label lbl_Inspection_1_X;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel28;
        private System.Windows.Forms.Label lbl_mm16;
        private System.Windows.Forms.TextBox txtInspection2Y;
        private System.Windows.Forms.Label lbl_mm15;
        private System.Windows.Forms.Label lbl_Inspection_2_Y;
        private System.Windows.Forms.TextBox txtInspection2X;
        private System.Windows.Forms.Label lbl_Inspection_2_X;
    }
}
