﻿namespace DIT.TLC.UI
{
    partial class ParameterAxis
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tc_iostatus_info = new System.Windows.Forms.TabControl();
            this.tp_iostatus_ld = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbl_Loader_AxisParameter = new System.Windows.Forms.Label();
            this.lbl_Loader_ServoOnLevel = new System.Windows.Forms.Label();
            this.txtLoaderPulse = new System.Windows.Forms.TextBox();
            this.lbl_Loader_Home_Signal = new System.Windows.Forms.Label();
            this.txtLoaderUnit = new System.Windows.Forms.TextBox();
            this.lbl_Loader_Home_Vel1 = new System.Windows.Forms.Label();
            this.txtLoaderMinVelocitu = new System.Windows.Forms.TextBox();
            this.lbl_Loader_AxisNo = new System.Windows.Forms.Label();
            this.txtLoaderMaxVelocitu = new System.Windows.Forms.TextBox();
            this.lbl_Loader_PlusEndLimit = new System.Windows.Forms.Label();
            this.cbxtLoaderEncoderType = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_Home_Vel2 = new System.Windows.Forms.Label();
            this.cbxtLoaderAlarmResetLevel = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_VelProfileMode = new System.Windows.Forms.Label();
            this.cbxtLoaderStopLevel = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_Home_VelLast = new System.Windows.Forms.Label();
            this.cbxtLoaderStopMode = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_EncInput = new System.Windows.Forms.Label();
            this.cbxtLoaderZPhase = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_PulseOutput = new System.Windows.Forms.Label();
            this.txtLoaderHomeOffset = new System.Windows.Forms.TextBox();
            this.lbl_Loader_Home = new System.Windows.Forms.Label();
            this.txtLoaderHomeClearTime = new System.Windows.Forms.TextBox();
            this.lbl_Loader_MotionSignalSetting = new System.Windows.Forms.Label();
            this.txtLoaderHomeAccelation2 = new System.Windows.Forms.TextBox();
            this.lbl_Loader_AbsRelMode = new System.Windows.Forms.Label();
            this.txtLoaderHomeAccelation1 = new System.Windows.Forms.TextBox();
            this.lbl_Loader_InPosition = new System.Windows.Forms.Label();
            this.cbxtLoaderHomeZPhase = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_Alarm = new System.Windows.Forms.Label();
            this.cbxtLoaderHomeDirection = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_Home_Level = new System.Windows.Forms.Label();
            this.cbxtLoaderSWLimitEnable = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_SW_PlusLimit = new System.Windows.Forms.Label();
            this.cbxtLoaderSWLimitStopMode = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_Home_Vel3 = new System.Windows.Forms.Label();
            this.txtLoaderInitInposition = new System.Windows.Forms.TextBox();
            this.lbl_Loader_SW = new System.Windows.Forms.Label();
            this.txtLoaderInitDecelation = new System.Windows.Forms.TextBox();
            this.lbl_Loader_SW_MinusLimit = new System.Windows.Forms.Label();
            this.txtLoaderInitAccelation = new System.Windows.Forms.TextBox();
            this.lbl_Loader_Init = new System.Windows.Forms.Label();
            this.txtLoaderInitAccelationTime = new System.Windows.Forms.TextBox();
            this.lbl_Loader_SW_LimitMode = new System.Windows.Forms.Label();
            this.txtLoaderInitPtpSpeed = new System.Windows.Forms.TextBox();
            this.lbl_Loader_MinusEndLimit = new System.Windows.Forms.Label();
            this.txtLoaderInitVelocity = new System.Windows.Forms.TextBox();
            this.lbl_Loader_Init_Position = new System.Windows.Forms.Label();
            this.txtLoaderInitPosition = new System.Windows.Forms.TextBox();
            this.lbl_Loader_Init_Velocity = new System.Windows.Forms.Label();
            this.cbxtLoaderSWLimitMode = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_Init_PtpSpeed = new System.Windows.Forms.Label();
            this.txtLoaderSWPlusLimit = new System.Windows.Forms.TextBox();
            this.lbl_Loader_Init_AccelationTime = new System.Windows.Forms.Label();
            this.txtLoaderSWMinusLimit = new System.Windows.Forms.TextBox();
            this.lbl_Loader_Pulse = new System.Windows.Forms.Label();
            this.txtLoaderHomeVelLast = new System.Windows.Forms.TextBox();
            this.lbl_Loader_MinVelocitu = new System.Windows.Forms.Label();
            this.txtLoaderHomeVel3 = new System.Windows.Forms.TextBox();
            this.lbl_Loader_MaxVelocitu = new System.Windows.Forms.Label();
            this.txtLoaderHomeVel2 = new System.Windows.Forms.TextBox();
            this.lbl_Loader_Unit = new System.Windows.Forms.Label();
            this.txtLoaderHomeVel1 = new System.Windows.Forms.TextBox();
            this.lbl_Loader_EncoderType = new System.Windows.Forms.Label();
            this.cbxtLoaderHomeLevel = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_AlarmResetLevel = new System.Windows.Forms.Label();
            this.cbxtLoaderHomeSignal = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_ZPhase = new System.Windows.Forms.Label();
            this.cbxtLoaderServoOnLevel = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_StopMode = new System.Windows.Forms.Label();
            this.cbxtLoaderPlusEndLimit = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_StopLevel = new System.Windows.Forms.Label();
            this.cbxtLoaderMinusEndLimit = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_Home_Direction = new System.Windows.Forms.Label();
            this.cbxtLoaderAlarm = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_Home_Accelation1 = new System.Windows.Forms.Label();
            this.cbxtLoaderInposition = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_Home_Accelation2 = new System.Windows.Forms.Label();
            this.cbxtLoaderAbsRelMode = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_Home_Offset = new System.Windows.Forms.Label();
            this.cbxtLoaderEncInput = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_Home_ZPhase = new System.Windows.Forms.Label();
            this.cbxtLoaderPulseOutput = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_Home_ClearTime = new System.Windows.Forms.Label();
            this.cbxtLoaderVelProfileMode = new System.Windows.Forms.ComboBox();
            this.lbl_Loader_SW_LimitEnable = new System.Windows.Forms.Label();
            this.txtLoaderAxisNo = new System.Windows.Forms.TextBox();
            this.lbl_Loader_SW_LimitStopMode = new System.Windows.Forms.Label();
            this.lbl_Loader_Init_InPosition = new System.Windows.Forms.Label();
            this.lbl_Loader_Init_Accelation = new System.Windows.Forms.Label();
            this.lbl_Loader_Init_Decelation = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pnLoader15 = new System.Windows.Forms.Panel();
            this.lblLoader15 = new System.Windows.Forms.Label();
            this.pnLoader14 = new System.Windows.Forms.Panel();
            this.lblLoader14 = new System.Windows.Forms.Label();
            this.pnLoader13 = new System.Windows.Forms.Panel();
            this.lblLoader13 = new System.Windows.Forms.Label();
            this.pnLoader12 = new System.Windows.Forms.Panel();
            this.lblLoader12 = new System.Windows.Forms.Label();
            this.pnLoader11 = new System.Windows.Forms.Panel();
            this.lblLoader11 = new System.Windows.Forms.Label();
            this.pnLoader10 = new System.Windows.Forms.Panel();
            this.lblLoader10 = new System.Windows.Forms.Label();
            this.pnLoader09 = new System.Windows.Forms.Panel();
            this.lblLoader09 = new System.Windows.Forms.Label();
            this.pnLoader08 = new System.Windows.Forms.Panel();
            this.lblLoader08 = new System.Windows.Forms.Label();
            this.pnLoader07 = new System.Windows.Forms.Panel();
            this.lblLoader07 = new System.Windows.Forms.Label();
            this.pnLoader06 = new System.Windows.Forms.Panel();
            this.lblLoader06 = new System.Windows.Forms.Label();
            this.pnLoader05 = new System.Windows.Forms.Panel();
            this.lblLoader05 = new System.Windows.Forms.Label();
            this.pnLoader04 = new System.Windows.Forms.Panel();
            this.lblLoader04 = new System.Windows.Forms.Label();
            this.pnLoader36 = new System.Windows.Forms.Panel();
            this.lblLoader36 = new System.Windows.Forms.Label();
            this.pnLoader35 = new System.Windows.Forms.Panel();
            this.lblLoader35 = new System.Windows.Forms.Label();
            this.pnLoader33 = new System.Windows.Forms.Panel();
            this.lblLoader33 = new System.Windows.Forms.Label();
            this.pnLoader32 = new System.Windows.Forms.Panel();
            this.lblLoader32 = new System.Windows.Forms.Label();
            this.pnLoader31 = new System.Windows.Forms.Panel();
            this.lblLoader31 = new System.Windows.Forms.Label();
            this.tp_iostatus_process = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.pnProcess21 = new System.Windows.Forms.Panel();
            this.lblProcess21 = new System.Windows.Forms.Label();
            this.pnProcess19 = new System.Windows.Forms.Panel();
            this.lblProcess19 = new System.Windows.Forms.Label();
            this.pnProcess18 = new System.Windows.Forms.Panel();
            this.lblProcess18 = new System.Windows.Forms.Label();
            this.pnProcess14 = new System.Windows.Forms.Panel();
            this.lblProcess14 = new System.Windows.Forms.Label();
            this.pnProcess13 = new System.Windows.Forms.Panel();
            this.lblProcess13 = new System.Windows.Forms.Label();
            this.pnProcess12 = new System.Windows.Forms.Panel();
            this.lblProcess12 = new System.Windows.Forms.Label();
            this.pnProcess11 = new System.Windows.Forms.Panel();
            this.lblProcess11 = new System.Windows.Forms.Label();
            this.pnProcess10 = new System.Windows.Forms.Panel();
            this.lblProcess10 = new System.Windows.Forms.Label();
            this.pnProcess09 = new System.Windows.Forms.Panel();
            this.lblProcess09 = new System.Windows.Forms.Label();
            this.pnProcess22 = new System.Windows.Forms.Panel();
            this.lblProcess22 = new System.Windows.Forms.Label();
            this.pnProcess07 = new System.Windows.Forms.Panel();
            this.lblProcess07 = new System.Windows.Forms.Label();
            this.pnProcess06 = new System.Windows.Forms.Panel();
            this.lblProcess06 = new System.Windows.Forms.Label();
            this.pnProcess05 = new System.Windows.Forms.Panel();
            this.lblProcess05 = new System.Windows.Forms.Label();
            this.pnProcess04 = new System.Windows.Forms.Panel();
            this.lblProcess04 = new System.Windows.Forms.Label();
            this.pnProcess17 = new System.Windows.Forms.Panel();
            this.lblProcess17 = new System.Windows.Forms.Label();
            this.pnProcess03 = new System.Windows.Forms.Panel();
            this.lblProcess03 = new System.Windows.Forms.Label();
            this.pnProcess02 = new System.Windows.Forms.Panel();
            this.lblProcess02 = new System.Windows.Forms.Label();
            this.pnProcess01 = new System.Windows.Forms.Panel();
            this.lblProcess01 = new System.Windows.Forms.Label();
            this.pnProcess20 = new System.Windows.Forms.Panel();
            this.lblProcess20 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbl_Process_AxisParameter = new System.Windows.Forms.Label();
            this.txtProcessAccelationTime = new System.Windows.Forms.TextBox();
            this.lbl_Process_JogSpeed = new System.Windows.Forms.Label();
            this.txtProcessSWLimitHigh = new System.Windows.Forms.TextBox();
            this.lbl_Process_PtpSpeed = new System.Windows.Forms.Label();
            this.txtProcessSWLimitLow = new System.Windows.Forms.TextBox();
            this.lbl_Process_SWLimitHigh = new System.Windows.Forms.Label();
            this.txtProcessPtpSpeed = new System.Windows.Forms.TextBox();
            this.lbl_Process_AxisNo = new System.Windows.Forms.Label();
            this.txtProcessStepSpeed = new System.Windows.Forms.TextBox();
            this.lbl_Process_HomeAcceleration = new System.Windows.Forms.Label();
            this.txtProcessJogSpeed = new System.Windows.Forms.TextBox();
            this.lbl_Process_AccelationTime = new System.Windows.Forms.Label();
            this.txtProcessHomeAcceleration = new System.Windows.Forms.TextBox();
            this.lbl_Process_InPosition = new System.Windows.Forms.Label();
            this.txtProcessHome2Velocity = new System.Windows.Forms.TextBox();
            this.lbl_Process_SpeedRate = new System.Windows.Forms.Label();
            this.txtProcessHome1Velocity = new System.Windows.Forms.TextBox();
            this.lbl_Process_PositionRate = new System.Windows.Forms.Label();
            this.txtProcessDefaultAcceleration = new System.Windows.Forms.TextBox();
            this.lbl_Process_StepSpeed = new System.Windows.Forms.Label();
            this.txtProcessDefauleVelocity = new System.Windows.Forms.TextBox();
            this.lbl_Process_DefauleVelocity = new System.Windows.Forms.Label();
            this.txtProcessInPosition = new System.Windows.Forms.TextBox();
            this.lbl_Process_MoveTimeOut = new System.Windows.Forms.Label();
            this.txtProcessMoveTimeOut = new System.Windows.Forms.TextBox();
            this.lbl_Process_DefaultAcceleration = new System.Windows.Forms.Label();
            this.txtProcessSpeedRate = new System.Windows.Forms.TextBox();
            this.lbl_Process_Home1Velocity = new System.Windows.Forms.Label();
            this.txtProcessPositionRate = new System.Windows.Forms.TextBox();
            this.lbl_Process_SWLimitLow = new System.Windows.Forms.Label();
            this.txtProcessAxisNo = new System.Windows.Forms.TextBox();
            this.lbl_Process_Home2Velocity = new System.Windows.Forms.Label();
            this.tp_iostatus_uld = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.pnUnloader3 = new System.Windows.Forms.Panel();
            this.lblUnloader3 = new System.Windows.Forms.Label();
            this.pnUnloader0 = new System.Windows.Forms.Panel();
            this.lblUnloader0 = new System.Windows.Forms.Label();
            this.pnUnloader30 = new System.Windows.Forms.Panel();
            this.lblUnloader30 = new System.Windows.Forms.Label();
            this.pnUnloader29 = new System.Windows.Forms.Panel();
            this.lblUnloader29 = new System.Windows.Forms.Label();
            this.pnUnloader28 = new System.Windows.Forms.Panel();
            this.lblUnloader28 = new System.Windows.Forms.Label();
            this.pnUnloader27 = new System.Windows.Forms.Panel();
            this.lblUnloader27 = new System.Windows.Forms.Label();
            this.pnUnloader26 = new System.Windows.Forms.Panel();
            this.lblUnloader26 = new System.Windows.Forms.Label();
            this.pnUnloader24 = new System.Windows.Forms.Panel();
            this.lblUnloader24 = new System.Windows.Forms.Label();
            this.pnUnloader23 = new System.Windows.Forms.Panel();
            this.lblUnloader23 = new System.Windows.Forms.Label();
            this.pnUnloader21 = new System.Windows.Forms.Panel();
            this.lblUnloader21 = new System.Windows.Forms.Label();
            this.pnUnloader20 = new System.Windows.Forms.Panel();
            this.lblUnloader20 = new System.Windows.Forms.Label();
            this.pnUnloader19 = new System.Windows.Forms.Panel();
            this.lblUnloader19 = new System.Windows.Forms.Label();
            this.pnUnloader18 = new System.Windows.Forms.Panel();
            this.lblUnloader18 = new System.Windows.Forms.Label();
            this.pnUnloader17 = new System.Windows.Forms.Panel();
            this.lblUnloader17 = new System.Windows.Forms.Label();
            this.pnUnloader16 = new System.Windows.Forms.Panel();
            this.lblUnloader16 = new System.Windows.Forms.Label();
            this.pnUnloader41 = new System.Windows.Forms.Panel();
            this.lblUnloader41 = new System.Windows.Forms.Label();
            this.pnUnloader40 = new System.Windows.Forms.Panel();
            this.lblUnloader40 = new System.Windows.Forms.Label();
            this.pnUnloader39 = new System.Windows.Forms.Panel();
            this.lblUnloader39 = new System.Windows.Forms.Label();
            this.pnUnloader38 = new System.Windows.Forms.Panel();
            this.lblUnloader38 = new System.Windows.Forms.Label();
            this.pnUnloader1 = new System.Windows.Forms.Panel();
            this.lblUnloader1 = new System.Windows.Forms.Label();
            this.pnUnloader37 = new System.Windows.Forms.Panel();
            this.lblUnloader37 = new System.Windows.Forms.Label();
            this.pnUnloader2 = new System.Windows.Forms.Panel();
            this.lblUnloader2 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lbl_Unloader_AxisParameter = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtUnloaderPulse = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtUnloaderUnit = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtUnloaderMinVelocitu = new System.Windows.Forms.TextBox();
            this.lbl_Unloader_AxisNo = new System.Windows.Forms.Label();
            this.txtUnloaderMaxVelocitu = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbxtUnloaderEncoderType = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbxtUnloaderAlarmResetLevel = new System.Windows.Forms.ComboBox();
            this.lbl_Unloader_VelProfileMode = new System.Windows.Forms.Label();
            this.cbxtUnloaderStopLevel = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cbxtUnloaderStopMode = new System.Windows.Forms.ComboBox();
            this.lbl_Unloader_EncInput = new System.Windows.Forms.Label();
            this.cbxtUnloaderZPhase = new System.Windows.Forms.ComboBox();
            this.lbl_Unloader_PulseOutput = new System.Windows.Forms.Label();
            this.txtUnloaderHomeOffset = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtUnloaderHomeClearTime = new System.Windows.Forms.TextBox();
            this.lbl_Unloader_MotionSignalSetting = new System.Windows.Forms.Label();
            this.txtUnloaderHomeAccelation2 = new System.Windows.Forms.TextBox();
            this.lbl_Unloader_AbsRelMode = new System.Windows.Forms.Label();
            this.txtUnloaderHomeAccelation1 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cbxtUnloaderHomeZPhase = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cbxtUnloaderHomeDirection = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.cbxtUnloaderSWLimitEnable = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.cbxtUnloaderSWLimitStopMode = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtUnloaderInitInPosition = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtUnloaderInitDecelation = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtUnloaderInitAccelation = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtUnloaderInitAccelationTime = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtUnloaderInitPtpSpeed = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtUnloaderInitVelocity = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtUnloaderInitPosition = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.cbxtUnloaderSWLimitMode = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtUnloaderSWPlusLimit = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtUnloaderSWMinusLimit = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtUnloaderHomeVelLast = new System.Windows.Forms.TextBox();
            this.lbl_Unloader_MinVelocitu = new System.Windows.Forms.Label();
            this.txtUnloaderHomeVel3 = new System.Windows.Forms.TextBox();
            this.lbl_Unloader_MaxVelocitu = new System.Windows.Forms.Label();
            this.txtUnloaderHomeVel2 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.txtUnloaderHomeVel1 = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.cbxtUnloaderHomeLevel = new System.Windows.Forms.ComboBox();
            this.label34 = new System.Windows.Forms.Label();
            this.cbxtUnloaderHomeSignal = new System.Windows.Forms.ComboBox();
            this.label35 = new System.Windows.Forms.Label();
            this.cbxtUnloaderServoOnLevel = new System.Windows.Forms.ComboBox();
            this.label36 = new System.Windows.Forms.Label();
            this.cbxtUnloaderPlusEndLimit = new System.Windows.Forms.ComboBox();
            this.label37 = new System.Windows.Forms.Label();
            this.cbxtUnloaderMinusEndLismit = new System.Windows.Forms.ComboBox();
            this.label38 = new System.Windows.Forms.Label();
            this.cbxtUnloaderAlarm = new System.Windows.Forms.ComboBox();
            this.label39 = new System.Windows.Forms.Label();
            this.cbxtUnloaderInposition = new System.Windows.Forms.ComboBox();
            this.label40 = new System.Windows.Forms.Label();
            this.cbxtUnloaderAbsRelMode = new System.Windows.Forms.ComboBox();
            this.label41 = new System.Windows.Forms.Label();
            this.cbxtUnloaderEncInput = new System.Windows.Forms.ComboBox();
            this.label42 = new System.Windows.Forms.Label();
            this.cbxtUnloaderPulseOutput = new System.Windows.Forms.ComboBox();
            this.label43 = new System.Windows.Forms.Label();
            this.cbxtUnloaderVelProfileMode = new System.Windows.Forms.ComboBox();
            this.label44 = new System.Windows.Forms.Label();
            this.txtUnloaderAxisNo = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.tc_iostatus_info.SuspendLayout();
            this.tp_iostatus_ld.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.pnLoader15.SuspendLayout();
            this.pnLoader14.SuspendLayout();
            this.pnLoader13.SuspendLayout();
            this.pnLoader12.SuspendLayout();
            this.pnLoader11.SuspendLayout();
            this.pnLoader10.SuspendLayout();
            this.pnLoader09.SuspendLayout();
            this.pnLoader08.SuspendLayout();
            this.pnLoader07.SuspendLayout();
            this.pnLoader06.SuspendLayout();
            this.pnLoader05.SuspendLayout();
            this.pnLoader04.SuspendLayout();
            this.pnLoader36.SuspendLayout();
            this.pnLoader35.SuspendLayout();
            this.pnLoader33.SuspendLayout();
            this.pnLoader32.SuspendLayout();
            this.pnLoader31.SuspendLayout();
            this.tp_iostatus_process.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.pnProcess21.SuspendLayout();
            this.pnProcess19.SuspendLayout();
            this.pnProcess18.SuspendLayout();
            this.pnProcess14.SuspendLayout();
            this.pnProcess13.SuspendLayout();
            this.pnProcess12.SuspendLayout();
            this.pnProcess11.SuspendLayout();
            this.pnProcess10.SuspendLayout();
            this.pnProcess09.SuspendLayout();
            this.pnProcess22.SuspendLayout();
            this.pnProcess07.SuspendLayout();
            this.pnProcess06.SuspendLayout();
            this.pnProcess05.SuspendLayout();
            this.pnProcess04.SuspendLayout();
            this.pnProcess17.SuspendLayout();
            this.pnProcess03.SuspendLayout();
            this.pnProcess02.SuspendLayout();
            this.pnProcess01.SuspendLayout();
            this.pnProcess20.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tp_iostatus_uld.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.pnUnloader3.SuspendLayout();
            this.pnUnloader0.SuspendLayout();
            this.pnUnloader30.SuspendLayout();
            this.pnUnloader29.SuspendLayout();
            this.pnUnloader28.SuspendLayout();
            this.pnUnloader27.SuspendLayout();
            this.pnUnloader26.SuspendLayout();
            this.pnUnloader24.SuspendLayout();
            this.pnUnloader23.SuspendLayout();
            this.pnUnloader21.SuspendLayout();
            this.pnUnloader20.SuspendLayout();
            this.pnUnloader19.SuspendLayout();
            this.pnUnloader18.SuspendLayout();
            this.pnUnloader17.SuspendLayout();
            this.pnUnloader16.SuspendLayout();
            this.pnUnloader41.SuspendLayout();
            this.pnUnloader40.SuspendLayout();
            this.pnUnloader39.SuspendLayout();
            this.pnUnloader38.SuspendLayout();
            this.pnUnloader1.SuspendLayout();
            this.pnUnloader37.SuspendLayout();
            this.pnUnloader2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tc_iostatus_info
            // 
            this.tc_iostatus_info.Controls.Add(this.tp_iostatus_ld);
            this.tc_iostatus_info.Controls.Add(this.tp_iostatus_process);
            this.tc_iostatus_info.Controls.Add(this.tp_iostatus_uld);
            this.tc_iostatus_info.ItemSize = new System.Drawing.Size(577, 30);
            this.tc_iostatus_info.Location = new System.Drawing.Point(3, 10);
            this.tc_iostatus_info.Name = "tc_iostatus_info";
            this.tc_iostatus_info.SelectedIndex = 0;
            this.tc_iostatus_info.Size = new System.Drawing.Size(1734, 805);
            this.tc_iostatus_info.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tc_iostatus_info.TabIndex = 1;
            // 
            // tp_iostatus_ld
            // 
            this.tp_iostatus_ld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tp_iostatus_ld.Controls.Add(this.panel1);
            this.tp_iostatus_ld.Controls.Add(this.tableLayoutPanel1);
            this.tp_iostatus_ld.Font = new System.Drawing.Font("굴림", 20F);
            this.tp_iostatus_ld.Location = new System.Drawing.Point(4, 34);
            this.tp_iostatus_ld.Name = "tp_iostatus_ld";
            this.tp_iostatus_ld.Padding = new System.Windows.Forms.Padding(3);
            this.tp_iostatus_ld.Size = new System.Drawing.Size(1726, 767);
            this.tp_iostatus_ld.TabIndex = 0;
            this.tp_iostatus_ld.Text = "로더";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DimGray;
            this.panel1.Controls.Add(this.lbl_Loader_AxisParameter);
            this.panel1.Controls.Add(this.lbl_Loader_ServoOnLevel);
            this.panel1.Controls.Add(this.txtLoaderPulse);
            this.panel1.Controls.Add(this.lbl_Loader_Home_Signal);
            this.panel1.Controls.Add(this.txtLoaderUnit);
            this.panel1.Controls.Add(this.lbl_Loader_Home_Vel1);
            this.panel1.Controls.Add(this.txtLoaderMinVelocitu);
            this.panel1.Controls.Add(this.lbl_Loader_AxisNo);
            this.panel1.Controls.Add(this.txtLoaderMaxVelocitu);
            this.panel1.Controls.Add(this.lbl_Loader_PlusEndLimit);
            this.panel1.Controls.Add(this.cbxtLoaderEncoderType);
            this.panel1.Controls.Add(this.lbl_Loader_Home_Vel2);
            this.panel1.Controls.Add(this.cbxtLoaderAlarmResetLevel);
            this.panel1.Controls.Add(this.lbl_Loader_VelProfileMode);
            this.panel1.Controls.Add(this.cbxtLoaderStopLevel);
            this.panel1.Controls.Add(this.lbl_Loader_Home_VelLast);
            this.panel1.Controls.Add(this.cbxtLoaderStopMode);
            this.panel1.Controls.Add(this.lbl_Loader_EncInput);
            this.panel1.Controls.Add(this.cbxtLoaderZPhase);
            this.panel1.Controls.Add(this.lbl_Loader_PulseOutput);
            this.panel1.Controls.Add(this.txtLoaderHomeOffset);
            this.panel1.Controls.Add(this.lbl_Loader_Home);
            this.panel1.Controls.Add(this.txtLoaderHomeClearTime);
            this.panel1.Controls.Add(this.lbl_Loader_MotionSignalSetting);
            this.panel1.Controls.Add(this.txtLoaderHomeAccelation2);
            this.panel1.Controls.Add(this.lbl_Loader_AbsRelMode);
            this.panel1.Controls.Add(this.txtLoaderHomeAccelation1);
            this.panel1.Controls.Add(this.lbl_Loader_InPosition);
            this.panel1.Controls.Add(this.cbxtLoaderHomeZPhase);
            this.panel1.Controls.Add(this.lbl_Loader_Alarm);
            this.panel1.Controls.Add(this.cbxtLoaderHomeDirection);
            this.panel1.Controls.Add(this.lbl_Loader_Home_Level);
            this.panel1.Controls.Add(this.cbxtLoaderSWLimitEnable);
            this.panel1.Controls.Add(this.lbl_Loader_SW_PlusLimit);
            this.panel1.Controls.Add(this.cbxtLoaderSWLimitStopMode);
            this.panel1.Controls.Add(this.lbl_Loader_Home_Vel3);
            this.panel1.Controls.Add(this.txtLoaderInitInposition);
            this.panel1.Controls.Add(this.lbl_Loader_SW);
            this.panel1.Controls.Add(this.txtLoaderInitDecelation);
            this.panel1.Controls.Add(this.lbl_Loader_SW_MinusLimit);
            this.panel1.Controls.Add(this.txtLoaderInitAccelation);
            this.panel1.Controls.Add(this.lbl_Loader_Init);
            this.panel1.Controls.Add(this.txtLoaderInitAccelationTime);
            this.panel1.Controls.Add(this.lbl_Loader_SW_LimitMode);
            this.panel1.Controls.Add(this.txtLoaderInitPtpSpeed);
            this.panel1.Controls.Add(this.lbl_Loader_MinusEndLimit);
            this.panel1.Controls.Add(this.txtLoaderInitVelocity);
            this.panel1.Controls.Add(this.lbl_Loader_Init_Position);
            this.panel1.Controls.Add(this.txtLoaderInitPosition);
            this.panel1.Controls.Add(this.lbl_Loader_Init_Velocity);
            this.panel1.Controls.Add(this.cbxtLoaderSWLimitMode);
            this.panel1.Controls.Add(this.lbl_Loader_Init_PtpSpeed);
            this.panel1.Controls.Add(this.txtLoaderSWPlusLimit);
            this.panel1.Controls.Add(this.lbl_Loader_Init_AccelationTime);
            this.panel1.Controls.Add(this.txtLoaderSWMinusLimit);
            this.panel1.Controls.Add(this.lbl_Loader_Pulse);
            this.panel1.Controls.Add(this.txtLoaderHomeVelLast);
            this.panel1.Controls.Add(this.lbl_Loader_MinVelocitu);
            this.panel1.Controls.Add(this.txtLoaderHomeVel3);
            this.panel1.Controls.Add(this.lbl_Loader_MaxVelocitu);
            this.panel1.Controls.Add(this.txtLoaderHomeVel2);
            this.panel1.Controls.Add(this.lbl_Loader_Unit);
            this.panel1.Controls.Add(this.txtLoaderHomeVel1);
            this.panel1.Controls.Add(this.lbl_Loader_EncoderType);
            this.panel1.Controls.Add(this.cbxtLoaderHomeLevel);
            this.panel1.Controls.Add(this.lbl_Loader_AlarmResetLevel);
            this.panel1.Controls.Add(this.cbxtLoaderHomeSignal);
            this.panel1.Controls.Add(this.lbl_Loader_ZPhase);
            this.panel1.Controls.Add(this.cbxtLoaderServoOnLevel);
            this.panel1.Controls.Add(this.lbl_Loader_StopMode);
            this.panel1.Controls.Add(this.cbxtLoaderPlusEndLimit);
            this.panel1.Controls.Add(this.lbl_Loader_StopLevel);
            this.panel1.Controls.Add(this.cbxtLoaderMinusEndLimit);
            this.panel1.Controls.Add(this.lbl_Loader_Home_Direction);
            this.panel1.Controls.Add(this.cbxtLoaderAlarm);
            this.panel1.Controls.Add(this.lbl_Loader_Home_Accelation1);
            this.panel1.Controls.Add(this.cbxtLoaderInposition);
            this.panel1.Controls.Add(this.lbl_Loader_Home_Accelation2);
            this.panel1.Controls.Add(this.cbxtLoaderAbsRelMode);
            this.panel1.Controls.Add(this.lbl_Loader_Home_Offset);
            this.panel1.Controls.Add(this.cbxtLoaderEncInput);
            this.panel1.Controls.Add(this.lbl_Loader_Home_ZPhase);
            this.panel1.Controls.Add(this.cbxtLoaderPulseOutput);
            this.panel1.Controls.Add(this.lbl_Loader_Home_ClearTime);
            this.panel1.Controls.Add(this.cbxtLoaderVelProfileMode);
            this.panel1.Controls.Add(this.lbl_Loader_SW_LimitEnable);
            this.panel1.Controls.Add(this.txtLoaderAxisNo);
            this.panel1.Controls.Add(this.lbl_Loader_SW_LimitStopMode);
            this.panel1.Controls.Add(this.lbl_Loader_Init_InPosition);
            this.panel1.Controls.Add(this.lbl_Loader_Init_Accelation);
            this.panel1.Controls.Add(this.lbl_Loader_Init_Decelation);
            this.panel1.Location = new System.Drawing.Point(881, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(834, 745);
            this.panel1.TabIndex = 0;
            // 
            // lbl_Loader_AxisParameter
            // 
            this.lbl_Loader_AxisParameter.AutoSize = true;
            this.lbl_Loader_AxisParameter.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_AxisParameter.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_AxisParameter.Location = new System.Drawing.Point(31, 10);
            this.lbl_Loader_AxisParameter.Name = "lbl_Loader_AxisParameter";
            this.lbl_Loader_AxisParameter.Size = new System.Drawing.Size(125, 23);
            this.lbl_Loader_AxisParameter.TabIndex = 0;
            this.lbl_Loader_AxisParameter.Text = "Axis Parameter";
            // 
            // lbl_Loader_ServoOnLevel
            // 
            this.lbl_Loader_ServoOnLevel.AutoSize = true;
            this.lbl_Loader_ServoOnLevel.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_ServoOnLevel.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_ServoOnLevel.Location = new System.Drawing.Point(31, 274);
            this.lbl_Loader_ServoOnLevel.Name = "lbl_Loader_ServoOnLevel";
            this.lbl_Loader_ServoOnLevel.Size = new System.Drawing.Size(135, 23);
            this.lbl_Loader_ServoOnLevel.TabIndex = 1;
            this.lbl_Loader_ServoOnLevel.Text = "Servo On Level :";
            // 
            // txtLoaderPulse
            // 
            this.txtLoaderPulse.Font = new System.Drawing.Font("굴림", 10F);
            this.txtLoaderPulse.Location = new System.Drawing.Point(672, 135);
            this.txtLoaderPulse.Name = "txtLoaderPulse";
            this.txtLoaderPulse.Size = new System.Drawing.Size(121, 23);
            this.txtLoaderPulse.TabIndex = 90;
            // 
            // lbl_Loader_Home_Signal
            // 
            this.lbl_Loader_Home_Signal.AutoSize = true;
            this.lbl_Loader_Home_Signal.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Home_Signal.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Home_Signal.Location = new System.Drawing.Point(31, 322);
            this.lbl_Loader_Home_Signal.Name = "lbl_Loader_Home_Signal";
            this.lbl_Loader_Home_Signal.Size = new System.Drawing.Size(118, 23);
            this.lbl_Loader_Home_Signal.TabIndex = 2;
            this.lbl_Loader_Home_Signal.Text = "Home Signal :";
            // 
            // txtLoaderUnit
            // 
            this.txtLoaderUnit.Font = new System.Drawing.Font("굴림", 10F);
            this.txtLoaderUnit.Location = new System.Drawing.Point(672, 110);
            this.txtLoaderUnit.Name = "txtLoaderUnit";
            this.txtLoaderUnit.Size = new System.Drawing.Size(121, 23);
            this.txtLoaderUnit.TabIndex = 89;
            // 
            // lbl_Loader_Home_Vel1
            // 
            this.lbl_Loader_Home_Vel1.AutoSize = true;
            this.lbl_Loader_Home_Vel1.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Home_Vel1.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Home_Vel1.Location = new System.Drawing.Point(31, 370);
            this.lbl_Loader_Home_Vel1.Name = "lbl_Loader_Home_Vel1";
            this.lbl_Loader_Home_Vel1.Size = new System.Drawing.Size(124, 23);
            this.lbl_Loader_Home_Vel1.TabIndex = 3;
            this.lbl_Loader_Home_Vel1.Text = "Home Vel 1st :";
            // 
            // txtLoaderMinVelocitu
            // 
            this.txtLoaderMinVelocitu.Font = new System.Drawing.Font("굴림", 10F);
            this.txtLoaderMinVelocitu.Location = new System.Drawing.Point(672, 85);
            this.txtLoaderMinVelocitu.Name = "txtLoaderMinVelocitu";
            this.txtLoaderMinVelocitu.Size = new System.Drawing.Size(121, 23);
            this.txtLoaderMinVelocitu.TabIndex = 88;
            // 
            // lbl_Loader_AxisNo
            // 
            this.lbl_Loader_AxisNo.AutoSize = true;
            this.lbl_Loader_AxisNo.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_AxisNo.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_AxisNo.Location = new System.Drawing.Point(31, 34);
            this.lbl_Loader_AxisNo.Name = "lbl_Loader_AxisNo";
            this.lbl_Loader_AxisNo.Size = new System.Drawing.Size(67, 23);
            this.lbl_Loader_AxisNo.TabIndex = 4;
            this.lbl_Loader_AxisNo.Text = "AxisNo.";
            // 
            // txtLoaderMaxVelocitu
            // 
            this.txtLoaderMaxVelocitu.Font = new System.Drawing.Font("굴림", 10F);
            this.txtLoaderMaxVelocitu.Location = new System.Drawing.Point(672, 60);
            this.txtLoaderMaxVelocitu.Name = "txtLoaderMaxVelocitu";
            this.txtLoaderMaxVelocitu.Size = new System.Drawing.Size(121, 23);
            this.txtLoaderMaxVelocitu.TabIndex = 87;
            // 
            // lbl_Loader_PlusEndLimit
            // 
            this.lbl_Loader_PlusEndLimit.AutoSize = true;
            this.lbl_Loader_PlusEndLimit.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_PlusEndLimit.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_PlusEndLimit.Location = new System.Drawing.Point(31, 250);
            this.lbl_Loader_PlusEndLimit.Name = "lbl_Loader_PlusEndLimit";
            this.lbl_Loader_PlusEndLimit.Size = new System.Drawing.Size(110, 23);
            this.lbl_Loader_PlusEndLimit.TabIndex = 5;
            this.lbl_Loader_PlusEndLimit.Text = "+ End Limit :";
            // 
            // cbxtLoaderEncoderType
            // 
            this.cbxtLoaderEncoderType.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtLoaderEncoderType.FormattingEnabled = true;
            this.cbxtLoaderEncoderType.Location = new System.Drawing.Point(672, 279);
            this.cbxtLoaderEncoderType.Name = "cbxtLoaderEncoderType";
            this.cbxtLoaderEncoderType.Size = new System.Drawing.Size(121, 21);
            this.cbxtLoaderEncoderType.TabIndex = 86;
            // 
            // lbl_Loader_Home_Vel2
            // 
            this.lbl_Loader_Home_Vel2.AutoSize = true;
            this.lbl_Loader_Home_Vel2.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Home_Vel2.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Home_Vel2.Location = new System.Drawing.Point(31, 394);
            this.lbl_Loader_Home_Vel2.Name = "lbl_Loader_Home_Vel2";
            this.lbl_Loader_Home_Vel2.Size = new System.Drawing.Size(131, 23);
            this.lbl_Loader_Home_Vel2.TabIndex = 6;
            this.lbl_Loader_Home_Vel2.Text = "Home Vel 2nd :";
            // 
            // cbxtLoaderAlarmResetLevel
            // 
            this.cbxtLoaderAlarmResetLevel.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtLoaderAlarmResetLevel.FormattingEnabled = true;
            this.cbxtLoaderAlarmResetLevel.Location = new System.Drawing.Point(672, 255);
            this.cbxtLoaderAlarmResetLevel.Name = "cbxtLoaderAlarmResetLevel";
            this.cbxtLoaderAlarmResetLevel.Size = new System.Drawing.Size(121, 21);
            this.cbxtLoaderAlarmResetLevel.TabIndex = 85;
            // 
            // lbl_Loader_VelProfileMode
            // 
            this.lbl_Loader_VelProfileMode.AutoSize = true;
            this.lbl_Loader_VelProfileMode.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_VelProfileMode.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_VelProfileMode.Location = new System.Drawing.Point(31, 130);
            this.lbl_Loader_VelProfileMode.Name = "lbl_Loader_VelProfileMode";
            this.lbl_Loader_VelProfileMode.Size = new System.Drawing.Size(149, 23);
            this.lbl_Loader_VelProfileMode.TabIndex = 7;
            this.lbl_Loader_VelProfileMode.Text = "Vel Profile Mode :";
            // 
            // cbxtLoaderStopLevel
            // 
            this.cbxtLoaderStopLevel.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtLoaderStopLevel.FormattingEnabled = true;
            this.cbxtLoaderStopLevel.Location = new System.Drawing.Point(672, 231);
            this.cbxtLoaderStopLevel.Name = "cbxtLoaderStopLevel";
            this.cbxtLoaderStopLevel.Size = new System.Drawing.Size(121, 21);
            this.cbxtLoaderStopLevel.TabIndex = 84;
            // 
            // lbl_Loader_Home_VelLast
            // 
            this.lbl_Loader_Home_VelLast.AutoSize = true;
            this.lbl_Loader_Home_VelLast.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Home_VelLast.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Home_VelLast.Location = new System.Drawing.Point(31, 442);
            this.lbl_Loader_Home_VelLast.Name = "lbl_Loader_Home_VelLast";
            this.lbl_Loader_Home_VelLast.Size = new System.Drawing.Size(132, 23);
            this.lbl_Loader_Home_VelLast.TabIndex = 8;
            this.lbl_Loader_Home_VelLast.Text = "Home Vel Last :";
            // 
            // cbxtLoaderStopMode
            // 
            this.cbxtLoaderStopMode.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtLoaderStopMode.FormattingEnabled = true;
            this.cbxtLoaderStopMode.Location = new System.Drawing.Point(672, 207);
            this.cbxtLoaderStopMode.Name = "cbxtLoaderStopMode";
            this.cbxtLoaderStopMode.Size = new System.Drawing.Size(121, 21);
            this.cbxtLoaderStopMode.TabIndex = 83;
            // 
            // lbl_Loader_EncInput
            // 
            this.lbl_Loader_EncInput.AutoSize = true;
            this.lbl_Loader_EncInput.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_EncInput.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_EncInput.Location = new System.Drawing.Point(31, 82);
            this.lbl_Loader_EncInput.Name = "lbl_Loader_EncInput";
            this.lbl_Loader_EncInput.Size = new System.Drawing.Size(98, 23);
            this.lbl_Loader_EncInput.TabIndex = 9;
            this.lbl_Loader_EncInput.Text = "Enc. Input :";
            // 
            // cbxtLoaderZPhase
            // 
            this.cbxtLoaderZPhase.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtLoaderZPhase.FormattingEnabled = true;
            this.cbxtLoaderZPhase.Location = new System.Drawing.Point(672, 183);
            this.cbxtLoaderZPhase.Name = "cbxtLoaderZPhase";
            this.cbxtLoaderZPhase.Size = new System.Drawing.Size(121, 21);
            this.cbxtLoaderZPhase.TabIndex = 82;
            // 
            // lbl_Loader_PulseOutput
            // 
            this.lbl_Loader_PulseOutput.AutoSize = true;
            this.lbl_Loader_PulseOutput.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_PulseOutput.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_PulseOutput.Location = new System.Drawing.Point(31, 58);
            this.lbl_Loader_PulseOutput.Name = "lbl_Loader_PulseOutput";
            this.lbl_Loader_PulseOutput.Size = new System.Drawing.Size(121, 23);
            this.lbl_Loader_PulseOutput.TabIndex = 10;
            this.lbl_Loader_PulseOutput.Text = "Pulse Output :";
            // 
            // txtLoaderHomeOffset
            // 
            this.txtLoaderHomeOffset.Font = new System.Drawing.Font("굴림", 10F);
            this.txtLoaderHomeOffset.Location = new System.Drawing.Point(672, 448);
            this.txtLoaderHomeOffset.Name = "txtLoaderHomeOffset";
            this.txtLoaderHomeOffset.Size = new System.Drawing.Size(121, 23);
            this.txtLoaderHomeOffset.TabIndex = 81;
            // 
            // lbl_Loader_Home
            // 
            this.lbl_Loader_Home.AutoSize = true;
            this.lbl_Loader_Home.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Home.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Home.Location = new System.Drawing.Point(31, 298);
            this.lbl_Loader_Home.Name = "lbl_Loader_Home";
            this.lbl_Loader_Home.Size = new System.Drawing.Size(179, 23);
            this.lbl_Loader_Home.TabIndex = 11;
            this.lbl_Loader_Home.Text = "Home  Search Setting";
            // 
            // txtLoaderHomeClearTime
            // 
            this.txtLoaderHomeClearTime.Font = new System.Drawing.Font("굴림", 10F);
            this.txtLoaderHomeClearTime.Location = new System.Drawing.Point(672, 423);
            this.txtLoaderHomeClearTime.Name = "txtLoaderHomeClearTime";
            this.txtLoaderHomeClearTime.Size = new System.Drawing.Size(121, 23);
            this.txtLoaderHomeClearTime.TabIndex = 80;
            // 
            // lbl_Loader_MotionSignalSetting
            // 
            this.lbl_Loader_MotionSignalSetting.AutoSize = true;
            this.lbl_Loader_MotionSignalSetting.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_MotionSignalSetting.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_MotionSignalSetting.Location = new System.Drawing.Point(31, 154);
            this.lbl_Loader_MotionSignalSetting.Name = "lbl_Loader_MotionSignalSetting";
            this.lbl_Loader_MotionSignalSetting.Size = new System.Drawing.Size(178, 23);
            this.lbl_Loader_MotionSignalSetting.TabIndex = 12;
            this.lbl_Loader_MotionSignalSetting.Text = "Motion Signal Setting";
            // 
            // txtLoaderHomeAccelation2
            // 
            this.txtLoaderHomeAccelation2.Font = new System.Drawing.Font("굴림", 10F);
            this.txtLoaderHomeAccelation2.Location = new System.Drawing.Point(672, 398);
            this.txtLoaderHomeAccelation2.Name = "txtLoaderHomeAccelation2";
            this.txtLoaderHomeAccelation2.Size = new System.Drawing.Size(121, 23);
            this.txtLoaderHomeAccelation2.TabIndex = 79;
            // 
            // lbl_Loader_AbsRelMode
            // 
            this.lbl_Loader_AbsRelMode.AutoSize = true;
            this.lbl_Loader_AbsRelMode.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_AbsRelMode.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_AbsRelMode.Location = new System.Drawing.Point(31, 106);
            this.lbl_Loader_AbsRelMode.Name = "lbl_Loader_AbsRelMode";
            this.lbl_Loader_AbsRelMode.Size = new System.Drawing.Size(126, 23);
            this.lbl_Loader_AbsRelMode.TabIndex = 13;
            this.lbl_Loader_AbsRelMode.Text = "Abs.Rel Mode :";
            // 
            // txtLoaderHomeAccelation1
            // 
            this.txtLoaderHomeAccelation1.Font = new System.Drawing.Font("굴림", 10F);
            this.txtLoaderHomeAccelation1.Location = new System.Drawing.Point(672, 373);
            this.txtLoaderHomeAccelation1.Name = "txtLoaderHomeAccelation1";
            this.txtLoaderHomeAccelation1.Size = new System.Drawing.Size(121, 23);
            this.txtLoaderHomeAccelation1.TabIndex = 78;
            // 
            // lbl_Loader_InPosition
            // 
            this.lbl_Loader_InPosition.AutoSize = true;
            this.lbl_Loader_InPosition.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_InPosition.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_InPosition.Location = new System.Drawing.Point(31, 178);
            this.lbl_Loader_InPosition.Name = "lbl_Loader_InPosition";
            this.lbl_Loader_InPosition.Size = new System.Drawing.Size(102, 23);
            this.lbl_Loader_InPosition.TabIndex = 14;
            this.lbl_Loader_InPosition.Text = "In Position :";
            // 
            // cbxtLoaderHomeZPhase
            // 
            this.cbxtLoaderHomeZPhase.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtLoaderHomeZPhase.FormattingEnabled = true;
            this.cbxtLoaderHomeZPhase.Location = new System.Drawing.Point(672, 350);
            this.cbxtLoaderHomeZPhase.Name = "cbxtLoaderHomeZPhase";
            this.cbxtLoaderHomeZPhase.Size = new System.Drawing.Size(121, 21);
            this.cbxtLoaderHomeZPhase.TabIndex = 77;
            // 
            // lbl_Loader_Alarm
            // 
            this.lbl_Loader_Alarm.AutoSize = true;
            this.lbl_Loader_Alarm.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Alarm.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Alarm.Location = new System.Drawing.Point(31, 202);
            this.lbl_Loader_Alarm.Name = "lbl_Loader_Alarm";
            this.lbl_Loader_Alarm.Size = new System.Drawing.Size(65, 23);
            this.lbl_Loader_Alarm.TabIndex = 15;
            this.lbl_Loader_Alarm.Text = "Alarm :";
            // 
            // cbxtLoaderHomeDirection
            // 
            this.cbxtLoaderHomeDirection.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtLoaderHomeDirection.FormattingEnabled = true;
            this.cbxtLoaderHomeDirection.Location = new System.Drawing.Point(672, 327);
            this.cbxtLoaderHomeDirection.Name = "cbxtLoaderHomeDirection";
            this.cbxtLoaderHomeDirection.Size = new System.Drawing.Size(121, 21);
            this.cbxtLoaderHomeDirection.TabIndex = 76;
            // 
            // lbl_Loader_Home_Level
            // 
            this.lbl_Loader_Home_Level.AutoSize = true;
            this.lbl_Loader_Home_Level.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Home_Level.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Home_Level.Location = new System.Drawing.Point(31, 346);
            this.lbl_Loader_Home_Level.Name = "lbl_Loader_Home_Level";
            this.lbl_Loader_Home_Level.Size = new System.Drawing.Size(110, 23);
            this.lbl_Loader_Home_Level.TabIndex = 16;
            this.lbl_Loader_Home_Level.Text = "Home Level :";
            // 
            // cbxtLoaderSWLimitEnable
            // 
            this.cbxtLoaderSWLimitEnable.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtLoaderSWLimitEnable.FormattingEnabled = true;
            this.cbxtLoaderSWLimitEnable.Location = new System.Drawing.Point(672, 518);
            this.cbxtLoaderSWLimitEnable.Name = "cbxtLoaderSWLimitEnable";
            this.cbxtLoaderSWLimitEnable.Size = new System.Drawing.Size(121, 21);
            this.cbxtLoaderSWLimitEnable.TabIndex = 75;
            // 
            // lbl_Loader_SW_PlusLimit
            // 
            this.lbl_Loader_SW_PlusLimit.AutoSize = true;
            this.lbl_Loader_SW_PlusLimit.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_SW_PlusLimit.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_SW_PlusLimit.Location = new System.Drawing.Point(31, 514);
            this.lbl_Loader_SW_PlusLimit.Name = "lbl_Loader_SW_PlusLimit";
            this.lbl_Loader_SW_PlusLimit.Size = new System.Drawing.Size(153, 23);
            this.lbl_Loader_SW_PlusLimit.TabIndex = 17;
            this.lbl_Loader_SW_PlusLimit.Text = "S/W +Limit [mm] :";
            // 
            // cbxtLoaderSWLimitStopMode
            // 
            this.cbxtLoaderSWLimitStopMode.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtLoaderSWLimitStopMode.FormattingEnabled = true;
            this.cbxtLoaderSWLimitStopMode.Location = new System.Drawing.Point(672, 495);
            this.cbxtLoaderSWLimitStopMode.Name = "cbxtLoaderSWLimitStopMode";
            this.cbxtLoaderSWLimitStopMode.Size = new System.Drawing.Size(121, 21);
            this.cbxtLoaderSWLimitStopMode.TabIndex = 74;
            // 
            // lbl_Loader_Home_Vel3
            // 
            this.lbl_Loader_Home_Vel3.AutoSize = true;
            this.lbl_Loader_Home_Vel3.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Home_Vel3.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Home_Vel3.Location = new System.Drawing.Point(31, 418);
            this.lbl_Loader_Home_Vel3.Name = "lbl_Loader_Home_Vel3";
            this.lbl_Loader_Home_Vel3.Size = new System.Drawing.Size(127, 23);
            this.lbl_Loader_Home_Vel3.TabIndex = 18;
            this.lbl_Loader_Home_Vel3.Text = "Home Vel 3rd :";
            // 
            // txtLoaderInitInposition
            // 
            this.txtLoaderInitInposition.Font = new System.Drawing.Font("굴림", 10F);
            this.txtLoaderInitInposition.Location = new System.Drawing.Point(672, 636);
            this.txtLoaderInitInposition.Name = "txtLoaderInitInposition";
            this.txtLoaderInitInposition.Size = new System.Drawing.Size(121, 23);
            this.txtLoaderInitInposition.TabIndex = 73;
            // 
            // lbl_Loader_SW
            // 
            this.lbl_Loader_SW.AutoSize = true;
            this.lbl_Loader_SW.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_SW.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_SW.Location = new System.Drawing.Point(31, 466);
            this.lbl_Loader_SW.Name = "lbl_Loader_SW";
            this.lbl_Loader_SW.Size = new System.Drawing.Size(145, 23);
            this.lbl_Loader_SW.TabIndex = 19;
            this.lbl_Loader_SW.Text = "S/W Limit Setting";
            // 
            // txtLoaderInitDecelation
            // 
            this.txtLoaderInitDecelation.Font = new System.Drawing.Font("굴림", 10F);
            this.txtLoaderInitDecelation.Location = new System.Drawing.Point(672, 611);
            this.txtLoaderInitDecelation.Name = "txtLoaderInitDecelation";
            this.txtLoaderInitDecelation.Size = new System.Drawing.Size(121, 23);
            this.txtLoaderInitDecelation.TabIndex = 72;
            // 
            // lbl_Loader_SW_MinusLimit
            // 
            this.lbl_Loader_SW_MinusLimit.AutoSize = true;
            this.lbl_Loader_SW_MinusLimit.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_SW_MinusLimit.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_SW_MinusLimit.Location = new System.Drawing.Point(31, 490);
            this.lbl_Loader_SW_MinusLimit.Name = "lbl_Loader_SW_MinusLimit";
            this.lbl_Loader_SW_MinusLimit.Size = new System.Drawing.Size(148, 23);
            this.lbl_Loader_SW_MinusLimit.TabIndex = 20;
            this.lbl_Loader_SW_MinusLimit.Text = "S/W -Limit [mm] :";
            // 
            // txtLoaderInitAccelation
            // 
            this.txtLoaderInitAccelation.Font = new System.Drawing.Font("굴림", 10F);
            this.txtLoaderInitAccelation.Location = new System.Drawing.Point(672, 586);
            this.txtLoaderInitAccelation.Name = "txtLoaderInitAccelation";
            this.txtLoaderInitAccelation.Size = new System.Drawing.Size(121, 23);
            this.txtLoaderInitAccelation.TabIndex = 71;
            // 
            // lbl_Loader_Init
            // 
            this.lbl_Loader_Init.AutoSize = true;
            this.lbl_Loader_Init.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Init.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Init.Location = new System.Drawing.Point(31, 562);
            this.lbl_Loader_Init.Name = "lbl_Loader_Init";
            this.lbl_Loader_Init.Size = new System.Drawing.Size(95, 23);
            this.lbl_Loader_Init.TabIndex = 21;
            this.lbl_Loader_Init.Text = "Init Setting";
            // 
            // txtLoaderInitAccelationTime
            // 
            this.txtLoaderInitAccelationTime.Font = new System.Drawing.Font("굴림", 10F);
            this.txtLoaderInitAccelationTime.Location = new System.Drawing.Point(245, 661);
            this.txtLoaderInitAccelationTime.Name = "txtLoaderInitAccelationTime";
            this.txtLoaderInitAccelationTime.Size = new System.Drawing.Size(121, 23);
            this.txtLoaderInitAccelationTime.TabIndex = 70;
            // 
            // lbl_Loader_SW_LimitMode
            // 
            this.lbl_Loader_SW_LimitMode.AutoSize = true;
            this.lbl_Loader_SW_LimitMode.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_SW_LimitMode.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_SW_LimitMode.Location = new System.Drawing.Point(31, 538);
            this.lbl_Loader_SW_LimitMode.Name = "lbl_Loader_SW_LimitMode";
            this.lbl_Loader_SW_LimitMode.Size = new System.Drawing.Size(146, 23);
            this.lbl_Loader_SW_LimitMode.TabIndex = 22;
            this.lbl_Loader_SW_LimitMode.Text = "S/W Limit Mode :";
            // 
            // txtLoaderInitPtpSpeed
            // 
            this.txtLoaderInitPtpSpeed.Font = new System.Drawing.Font("굴림", 10F);
            this.txtLoaderInitPtpSpeed.Location = new System.Drawing.Point(245, 636);
            this.txtLoaderInitPtpSpeed.Name = "txtLoaderInitPtpSpeed";
            this.txtLoaderInitPtpSpeed.Size = new System.Drawing.Size(121, 23);
            this.txtLoaderInitPtpSpeed.TabIndex = 69;
            // 
            // lbl_Loader_MinusEndLimit
            // 
            this.lbl_Loader_MinusEndLimit.AutoSize = true;
            this.lbl_Loader_MinusEndLimit.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_MinusEndLimit.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_MinusEndLimit.Location = new System.Drawing.Point(31, 226);
            this.lbl_Loader_MinusEndLimit.Name = "lbl_Loader_MinusEndLimit";
            this.lbl_Loader_MinusEndLimit.Size = new System.Drawing.Size(105, 23);
            this.lbl_Loader_MinusEndLimit.TabIndex = 23;
            this.lbl_Loader_MinusEndLimit.Text = "- End Limit :";
            // 
            // txtLoaderInitVelocity
            // 
            this.txtLoaderInitVelocity.Font = new System.Drawing.Font("굴림", 10F);
            this.txtLoaderInitVelocity.Location = new System.Drawing.Point(245, 611);
            this.txtLoaderInitVelocity.Name = "txtLoaderInitVelocity";
            this.txtLoaderInitVelocity.Size = new System.Drawing.Size(121, 23);
            this.txtLoaderInitVelocity.TabIndex = 68;
            // 
            // lbl_Loader_Init_Position
            // 
            this.lbl_Loader_Init_Position.AutoSize = true;
            this.lbl_Loader_Init_Position.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Init_Position.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Init_Position.Location = new System.Drawing.Point(31, 586);
            this.lbl_Loader_Init_Position.Name = "lbl_Loader_Init_Position";
            this.lbl_Loader_Init_Position.Size = new System.Drawing.Size(158, 23);
            this.lbl_Loader_Init_Position.TabIndex = 24;
            this.lbl_Loader_Init_Position.Text = "Init Position [mm] :";
            // 
            // txtLoaderInitPosition
            // 
            this.txtLoaderInitPosition.Font = new System.Drawing.Font("굴림", 10F);
            this.txtLoaderInitPosition.Location = new System.Drawing.Point(245, 586);
            this.txtLoaderInitPosition.Name = "txtLoaderInitPosition";
            this.txtLoaderInitPosition.Size = new System.Drawing.Size(121, 23);
            this.txtLoaderInitPosition.TabIndex = 67;
            // 
            // lbl_Loader_Init_Velocity
            // 
            this.lbl_Loader_Init_Velocity.AutoSize = true;
            this.lbl_Loader_Init_Velocity.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Init_Velocity.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Init_Velocity.Location = new System.Drawing.Point(31, 610);
            this.lbl_Loader_Init_Velocity.Name = "lbl_Loader_Init_Velocity";
            this.lbl_Loader_Init_Velocity.Size = new System.Drawing.Size(188, 23);
            this.lbl_Loader_Init_Velocity.TabIndex = 25;
            this.lbl_Loader_Init_Velocity.Text = "Init Velocity [mm/sec] :";
            // 
            // cbxtLoaderSWLimitMode
            // 
            this.cbxtLoaderSWLimitMode.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtLoaderSWLimitMode.FormattingEnabled = true;
            this.cbxtLoaderSWLimitMode.Location = new System.Drawing.Point(245, 541);
            this.cbxtLoaderSWLimitMode.Name = "cbxtLoaderSWLimitMode";
            this.cbxtLoaderSWLimitMode.Size = new System.Drawing.Size(121, 21);
            this.cbxtLoaderSWLimitMode.TabIndex = 66;
            // 
            // lbl_Loader_Init_PtpSpeed
            // 
            this.lbl_Loader_Init_PtpSpeed.AutoSize = true;
            this.lbl_Loader_Init_PtpSpeed.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Init_PtpSpeed.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Init_PtpSpeed.Location = new System.Drawing.Point(31, 634);
            this.lbl_Loader_Init_PtpSpeed.Name = "lbl_Loader_Init_PtpSpeed";
            this.lbl_Loader_Init_PtpSpeed.Size = new System.Drawing.Size(99, 23);
            this.lbl_Loader_Init_PtpSpeed.TabIndex = 26;
            this.lbl_Loader_Init_PtpSpeed.Text = "Ptp Speed :";
            // 
            // txtLoaderSWPlusLimit
            // 
            this.txtLoaderSWPlusLimit.Font = new System.Drawing.Font("굴림", 10F);
            this.txtLoaderSWPlusLimit.Location = new System.Drawing.Point(245, 515);
            this.txtLoaderSWPlusLimit.Name = "txtLoaderSWPlusLimit";
            this.txtLoaderSWPlusLimit.Size = new System.Drawing.Size(121, 23);
            this.txtLoaderSWPlusLimit.TabIndex = 65;
            // 
            // lbl_Loader_Init_AccelationTime
            // 
            this.lbl_Loader_Init_AccelationTime.AutoSize = true;
            this.lbl_Loader_Init_AccelationTime.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Init_AccelationTime.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Init_AccelationTime.Location = new System.Drawing.Point(31, 658);
            this.lbl_Loader_Init_AccelationTime.Name = "lbl_Loader_Init_AccelationTime";
            this.lbl_Loader_Init_AccelationTime.Size = new System.Drawing.Size(165, 23);
            this.lbl_Loader_Init_AccelationTime.TabIndex = 27;
            this.lbl_Loader_Init_AccelationTime.Text = "Accelation Time [s] :";
            // 
            // txtLoaderSWMinusLimit
            // 
            this.txtLoaderSWMinusLimit.Font = new System.Drawing.Font("굴림", 10F);
            this.txtLoaderSWMinusLimit.Location = new System.Drawing.Point(245, 490);
            this.txtLoaderSWMinusLimit.Name = "txtLoaderSWMinusLimit";
            this.txtLoaderSWMinusLimit.Size = new System.Drawing.Size(121, 23);
            this.txtLoaderSWMinusLimit.TabIndex = 64;
            // 
            // lbl_Loader_Pulse
            // 
            this.lbl_Loader_Pulse.AutoSize = true;
            this.lbl_Loader_Pulse.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Pulse.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Pulse.Location = new System.Drawing.Point(469, 130);
            this.lbl_Loader_Pulse.Name = "lbl_Loader_Pulse";
            this.lbl_Loader_Pulse.Size = new System.Drawing.Size(60, 23);
            this.lbl_Loader_Pulse.TabIndex = 28;
            this.lbl_Loader_Pulse.Text = "Pulse :";
            // 
            // txtLoaderHomeVelLast
            // 
            this.txtLoaderHomeVelLast.Font = new System.Drawing.Font("굴림", 10F);
            this.txtLoaderHomeVelLast.Location = new System.Drawing.Point(245, 443);
            this.txtLoaderHomeVelLast.Name = "txtLoaderHomeVelLast";
            this.txtLoaderHomeVelLast.Size = new System.Drawing.Size(121, 23);
            this.txtLoaderHomeVelLast.TabIndex = 63;
            // 
            // lbl_Loader_MinVelocitu
            // 
            this.lbl_Loader_MinVelocitu.AutoSize = true;
            this.lbl_Loader_MinVelocitu.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_MinVelocitu.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_MinVelocitu.Location = new System.Drawing.Point(469, 82);
            this.lbl_Loader_MinVelocitu.Name = "lbl_Loader_MinVelocitu";
            this.lbl_Loader_MinVelocitu.Size = new System.Drawing.Size(195, 23);
            this.lbl_Loader_MinVelocitu.TabIndex = 29;
            this.lbl_Loader_MinVelocitu.Text = "Min Velocitu [mm/sec] :";
            // 
            // txtLoaderHomeVel3
            // 
            this.txtLoaderHomeVel3.Font = new System.Drawing.Font("굴림", 10F);
            this.txtLoaderHomeVel3.Location = new System.Drawing.Point(245, 418);
            this.txtLoaderHomeVel3.Name = "txtLoaderHomeVel3";
            this.txtLoaderHomeVel3.Size = new System.Drawing.Size(121, 23);
            this.txtLoaderHomeVel3.TabIndex = 62;
            // 
            // lbl_Loader_MaxVelocitu
            // 
            this.lbl_Loader_MaxVelocitu.AutoSize = true;
            this.lbl_Loader_MaxVelocitu.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_MaxVelocitu.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_MaxVelocitu.Location = new System.Drawing.Point(469, 58);
            this.lbl_Loader_MaxVelocitu.Name = "lbl_Loader_MaxVelocitu";
            this.lbl_Loader_MaxVelocitu.Size = new System.Drawing.Size(198, 23);
            this.lbl_Loader_MaxVelocitu.TabIndex = 30;
            this.lbl_Loader_MaxVelocitu.Text = "Max Velocitu [mm/sec] :";
            // 
            // txtLoaderHomeVel2
            // 
            this.txtLoaderHomeVel2.Font = new System.Drawing.Font("굴림", 10F);
            this.txtLoaderHomeVel2.Location = new System.Drawing.Point(245, 393);
            this.txtLoaderHomeVel2.Name = "txtLoaderHomeVel2";
            this.txtLoaderHomeVel2.Size = new System.Drawing.Size(121, 23);
            this.txtLoaderHomeVel2.TabIndex = 61;
            // 
            // lbl_Loader_Unit
            // 
            this.lbl_Loader_Unit.AutoSize = true;
            this.lbl_Loader_Unit.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Unit.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Unit.Location = new System.Drawing.Point(469, 106);
            this.lbl_Loader_Unit.Name = "lbl_Loader_Unit";
            this.lbl_Loader_Unit.Size = new System.Drawing.Size(52, 23);
            this.lbl_Loader_Unit.TabIndex = 31;
            this.lbl_Loader_Unit.Text = "Unit :";
            // 
            // txtLoaderHomeVel1
            // 
            this.txtLoaderHomeVel1.Font = new System.Drawing.Font("굴림", 10F);
            this.txtLoaderHomeVel1.Location = new System.Drawing.Point(245, 368);
            this.txtLoaderHomeVel1.Name = "txtLoaderHomeVel1";
            this.txtLoaderHomeVel1.Size = new System.Drawing.Size(121, 23);
            this.txtLoaderHomeVel1.TabIndex = 60;
            // 
            // lbl_Loader_EncoderType
            // 
            this.lbl_Loader_EncoderType.AutoSize = true;
            this.lbl_Loader_EncoderType.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_EncoderType.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_EncoderType.Location = new System.Drawing.Point(469, 274);
            this.lbl_Loader_EncoderType.Name = "lbl_Loader_EncoderType";
            this.lbl_Loader_EncoderType.Size = new System.Drawing.Size(124, 23);
            this.lbl_Loader_EncoderType.TabIndex = 32;
            this.lbl_Loader_EncoderType.Text = "Encoder Type :";
            // 
            // cbxtLoaderHomeLevel
            // 
            this.cbxtLoaderHomeLevel.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtLoaderHomeLevel.FormattingEnabled = true;
            this.cbxtLoaderHomeLevel.Location = new System.Drawing.Point(245, 345);
            this.cbxtLoaderHomeLevel.Name = "cbxtLoaderHomeLevel";
            this.cbxtLoaderHomeLevel.Size = new System.Drawing.Size(121, 21);
            this.cbxtLoaderHomeLevel.TabIndex = 59;
            // 
            // lbl_Loader_AlarmResetLevel
            // 
            this.lbl_Loader_AlarmResetLevel.AutoSize = true;
            this.lbl_Loader_AlarmResetLevel.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_AlarmResetLevel.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_AlarmResetLevel.Location = new System.Drawing.Point(469, 250);
            this.lbl_Loader_AlarmResetLevel.Name = "lbl_Loader_AlarmResetLevel";
            this.lbl_Loader_AlarmResetLevel.Size = new System.Drawing.Size(156, 23);
            this.lbl_Loader_AlarmResetLevel.TabIndex = 33;
            this.lbl_Loader_AlarmResetLevel.Text = "Alarm Reset Level :";
            // 
            // cbxtLoaderHomeSignal
            // 
            this.cbxtLoaderHomeSignal.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtLoaderHomeSignal.FormattingEnabled = true;
            this.cbxtLoaderHomeSignal.Location = new System.Drawing.Point(245, 322);
            this.cbxtLoaderHomeSignal.Name = "cbxtLoaderHomeSignal";
            this.cbxtLoaderHomeSignal.Size = new System.Drawing.Size(121, 21);
            this.cbxtLoaderHomeSignal.TabIndex = 58;
            // 
            // lbl_Loader_ZPhase
            // 
            this.lbl_Loader_ZPhase.AutoSize = true;
            this.lbl_Loader_ZPhase.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_ZPhase.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_ZPhase.Location = new System.Drawing.Point(469, 178);
            this.lbl_Loader_ZPhase.Name = "lbl_Loader_ZPhase";
            this.lbl_Loader_ZPhase.Size = new System.Drawing.Size(82, 23);
            this.lbl_Loader_ZPhase.TabIndex = 34;
            this.lbl_Loader_ZPhase.Text = "Z-Phase :";
            // 
            // cbxtLoaderServoOnLevel
            // 
            this.cbxtLoaderServoOnLevel.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtLoaderServoOnLevel.FormattingEnabled = true;
            this.cbxtLoaderServoOnLevel.Location = new System.Drawing.Point(245, 276);
            this.cbxtLoaderServoOnLevel.Name = "cbxtLoaderServoOnLevel";
            this.cbxtLoaderServoOnLevel.Size = new System.Drawing.Size(121, 21);
            this.cbxtLoaderServoOnLevel.TabIndex = 57;
            // 
            // lbl_Loader_StopMode
            // 
            this.lbl_Loader_StopMode.AutoSize = true;
            this.lbl_Loader_StopMode.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_StopMode.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_StopMode.Location = new System.Drawing.Point(469, 202);
            this.lbl_Loader_StopMode.Name = "lbl_Loader_StopMode";
            this.lbl_Loader_StopMode.Size = new System.Drawing.Size(106, 23);
            this.lbl_Loader_StopMode.TabIndex = 35;
            this.lbl_Loader_StopMode.Text = "Stop Mode :";
            // 
            // cbxtLoaderPlusEndLimit
            // 
            this.cbxtLoaderPlusEndLimit.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtLoaderPlusEndLimit.FormattingEnabled = true;
            this.cbxtLoaderPlusEndLimit.Location = new System.Drawing.Point(245, 252);
            this.cbxtLoaderPlusEndLimit.Name = "cbxtLoaderPlusEndLimit";
            this.cbxtLoaderPlusEndLimit.Size = new System.Drawing.Size(121, 21);
            this.cbxtLoaderPlusEndLimit.TabIndex = 56;
            // 
            // lbl_Loader_StopLevel
            // 
            this.lbl_Loader_StopLevel.AutoSize = true;
            this.lbl_Loader_StopLevel.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_StopLevel.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_StopLevel.Location = new System.Drawing.Point(469, 226);
            this.lbl_Loader_StopLevel.Name = "lbl_Loader_StopLevel";
            this.lbl_Loader_StopLevel.Size = new System.Drawing.Size(99, 23);
            this.lbl_Loader_StopLevel.TabIndex = 36;
            this.lbl_Loader_StopLevel.Text = "Stop Level :";
            // 
            // cbxtLoaderMinusEndLimit
            // 
            this.cbxtLoaderMinusEndLimit.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtLoaderMinusEndLimit.FormattingEnabled = true;
            this.cbxtLoaderMinusEndLimit.Location = new System.Drawing.Point(245, 228);
            this.cbxtLoaderMinusEndLimit.Name = "cbxtLoaderMinusEndLimit";
            this.cbxtLoaderMinusEndLimit.Size = new System.Drawing.Size(121, 21);
            this.cbxtLoaderMinusEndLimit.TabIndex = 55;
            // 
            // lbl_Loader_Home_Direction
            // 
            this.lbl_Loader_Home_Direction.AutoSize = true;
            this.lbl_Loader_Home_Direction.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Home_Direction.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Home_Direction.Location = new System.Drawing.Point(469, 322);
            this.lbl_Loader_Home_Direction.Name = "lbl_Loader_Home_Direction";
            this.lbl_Loader_Home_Direction.Size = new System.Drawing.Size(141, 23);
            this.lbl_Loader_Home_Direction.TabIndex = 37;
            this.lbl_Loader_Home_Direction.Text = "Home Direction :";
            // 
            // cbxtLoaderAlarm
            // 
            this.cbxtLoaderAlarm.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtLoaderAlarm.FormattingEnabled = true;
            this.cbxtLoaderAlarm.Location = new System.Drawing.Point(245, 204);
            this.cbxtLoaderAlarm.Name = "cbxtLoaderAlarm";
            this.cbxtLoaderAlarm.Size = new System.Drawing.Size(121, 21);
            this.cbxtLoaderAlarm.TabIndex = 54;
            // 
            // lbl_Loader_Home_Accelation1
            // 
            this.lbl_Loader_Home_Accelation1.AutoSize = true;
            this.lbl_Loader_Home_Accelation1.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Home_Accelation1.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Home_Accelation1.Location = new System.Drawing.Point(469, 370);
            this.lbl_Loader_Home_Accelation1.Name = "lbl_Loader_Home_Accelation1";
            this.lbl_Loader_Home_Accelation1.Size = new System.Drawing.Size(179, 23);
            this.lbl_Loader_Home_Accelation1.TabIndex = 38;
            this.lbl_Loader_Home_Accelation1.Text = "Home Accelation 1st :";
            // 
            // cbxtLoaderInposition
            // 
            this.cbxtLoaderInposition.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtLoaderInposition.FormattingEnabled = true;
            this.cbxtLoaderInposition.Location = new System.Drawing.Point(245, 180);
            this.cbxtLoaderInposition.Name = "cbxtLoaderInposition";
            this.cbxtLoaderInposition.Size = new System.Drawing.Size(121, 21);
            this.cbxtLoaderInposition.TabIndex = 53;
            // 
            // lbl_Loader_Home_Accelation2
            // 
            this.lbl_Loader_Home_Accelation2.AutoSize = true;
            this.lbl_Loader_Home_Accelation2.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Home_Accelation2.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Home_Accelation2.Location = new System.Drawing.Point(469, 394);
            this.lbl_Loader_Home_Accelation2.Name = "lbl_Loader_Home_Accelation2";
            this.lbl_Loader_Home_Accelation2.Size = new System.Drawing.Size(186, 23);
            this.lbl_Loader_Home_Accelation2.TabIndex = 39;
            this.lbl_Loader_Home_Accelation2.Text = "Home Accelation 2nd :";
            // 
            // cbxtLoaderAbsRelMode
            // 
            this.cbxtLoaderAbsRelMode.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtLoaderAbsRelMode.FormattingEnabled = true;
            this.cbxtLoaderAbsRelMode.Location = new System.Drawing.Point(245, 110);
            this.cbxtLoaderAbsRelMode.Name = "cbxtLoaderAbsRelMode";
            this.cbxtLoaderAbsRelMode.Size = new System.Drawing.Size(121, 21);
            this.cbxtLoaderAbsRelMode.TabIndex = 52;
            // 
            // lbl_Loader_Home_Offset
            // 
            this.lbl_Loader_Home_Offset.AutoSize = true;
            this.lbl_Loader_Home_Offset.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Home_Offset.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Home_Offset.Location = new System.Drawing.Point(469, 442);
            this.lbl_Loader_Home_Offset.Name = "lbl_Loader_Home_Offset";
            this.lbl_Loader_Home_Offset.Size = new System.Drawing.Size(117, 23);
            this.lbl_Loader_Home_Offset.TabIndex = 40;
            this.lbl_Loader_Home_Offset.Text = "Home Offset :";
            // 
            // cbxtLoaderEncInput
            // 
            this.cbxtLoaderEncInput.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtLoaderEncInput.FormattingEnabled = true;
            this.cbxtLoaderEncInput.Location = new System.Drawing.Point(245, 86);
            this.cbxtLoaderEncInput.Name = "cbxtLoaderEncInput";
            this.cbxtLoaderEncInput.Size = new System.Drawing.Size(121, 21);
            this.cbxtLoaderEncInput.TabIndex = 51;
            // 
            // lbl_Loader_Home_ZPhase
            // 
            this.lbl_Loader_Home_ZPhase.AutoSize = true;
            this.lbl_Loader_Home_ZPhase.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Home_ZPhase.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Home_ZPhase.Location = new System.Drawing.Point(469, 346);
            this.lbl_Loader_Home_ZPhase.Name = "lbl_Loader_Home_ZPhase";
            this.lbl_Loader_Home_ZPhase.Size = new System.Drawing.Size(134, 23);
            this.lbl_Loader_Home_ZPhase.TabIndex = 41;
            this.lbl_Loader_Home_ZPhase.Text = "Home Z_Phase :";
            // 
            // cbxtLoaderPulseOutput
            // 
            this.cbxtLoaderPulseOutput.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtLoaderPulseOutput.FormattingEnabled = true;
            this.cbxtLoaderPulseOutput.Location = new System.Drawing.Point(245, 63);
            this.cbxtLoaderPulseOutput.Name = "cbxtLoaderPulseOutput";
            this.cbxtLoaderPulseOutput.Size = new System.Drawing.Size(121, 21);
            this.cbxtLoaderPulseOutput.TabIndex = 50;
            // 
            // lbl_Loader_Home_ClearTime
            // 
            this.lbl_Loader_Home_ClearTime.AutoSize = true;
            this.lbl_Loader_Home_ClearTime.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Home_ClearTime.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Home_ClearTime.Location = new System.Drawing.Point(469, 418);
            this.lbl_Loader_Home_ClearTime.Name = "lbl_Loader_Home_ClearTime";
            this.lbl_Loader_Home_ClearTime.Size = new System.Drawing.Size(154, 23);
            this.lbl_Loader_Home_ClearTime.TabIndex = 42;
            this.lbl_Loader_Home_ClearTime.Text = "Home Clear Time :";
            // 
            // cbxtLoaderVelProfileMode
            // 
            this.cbxtLoaderVelProfileMode.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtLoaderVelProfileMode.FormattingEnabled = true;
            this.cbxtLoaderVelProfileMode.Location = new System.Drawing.Point(245, 135);
            this.cbxtLoaderVelProfileMode.Name = "cbxtLoaderVelProfileMode";
            this.cbxtLoaderVelProfileMode.Size = new System.Drawing.Size(121, 21);
            this.cbxtLoaderVelProfileMode.TabIndex = 49;
            // 
            // lbl_Loader_SW_LimitEnable
            // 
            this.lbl_Loader_SW_LimitEnable.AutoSize = true;
            this.lbl_Loader_SW_LimitEnable.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_SW_LimitEnable.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_SW_LimitEnable.Location = new System.Drawing.Point(469, 514);
            this.lbl_Loader_SW_LimitEnable.Name = "lbl_Loader_SW_LimitEnable";
            this.lbl_Loader_SW_LimitEnable.Size = new System.Drawing.Size(152, 23);
            this.lbl_Loader_SW_LimitEnable.TabIndex = 43;
            this.lbl_Loader_SW_LimitEnable.Text = "S/W Limit Enable :";
            // 
            // txtLoaderAxisNo
            // 
            this.txtLoaderAxisNo.Font = new System.Drawing.Font("굴림", 10F);
            this.txtLoaderAxisNo.Location = new System.Drawing.Point(245, 38);
            this.txtLoaderAxisNo.Name = "txtLoaderAxisNo";
            this.txtLoaderAxisNo.Size = new System.Drawing.Size(121, 23);
            this.txtLoaderAxisNo.TabIndex = 48;
            // 
            // lbl_Loader_SW_LimitStopMode
            // 
            this.lbl_Loader_SW_LimitStopMode.AutoSize = true;
            this.lbl_Loader_SW_LimitStopMode.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_SW_LimitStopMode.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_SW_LimitStopMode.Location = new System.Drawing.Point(469, 490);
            this.lbl_Loader_SW_LimitStopMode.Name = "lbl_Loader_SW_LimitStopMode";
            this.lbl_Loader_SW_LimitStopMode.Size = new System.Drawing.Size(187, 23);
            this.lbl_Loader_SW_LimitStopMode.TabIndex = 44;
            this.lbl_Loader_SW_LimitStopMode.Text = "S/W Limit Stop Mode :";
            // 
            // lbl_Loader_Init_InPosition
            // 
            this.lbl_Loader_Init_InPosition.AutoSize = true;
            this.lbl_Loader_Init_InPosition.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Init_InPosition.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Init_InPosition.Location = new System.Drawing.Point(469, 634);
            this.lbl_Loader_Init_InPosition.Name = "lbl_Loader_Init_InPosition";
            this.lbl_Loader_Init_InPosition.Size = new System.Drawing.Size(138, 23);
            this.lbl_Loader_Init_InPosition.TabIndex = 47;
            this.lbl_Loader_Init_InPosition.Text = "In Position [mm]";
            // 
            // lbl_Loader_Init_Accelation
            // 
            this.lbl_Loader_Init_Accelation.AutoSize = true;
            this.lbl_Loader_Init_Accelation.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Init_Accelation.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Init_Accelation.Location = new System.Drawing.Point(469, 586);
            this.lbl_Loader_Init_Accelation.Name = "lbl_Loader_Init_Accelation";
            this.lbl_Loader_Init_Accelation.Size = new System.Drawing.Size(130, 23);
            this.lbl_Loader_Init_Accelation.TabIndex = 45;
            this.lbl_Loader_Init_Accelation.Text = "Init Accelation :";
            // 
            // lbl_Loader_Init_Decelation
            // 
            this.lbl_Loader_Init_Decelation.AutoSize = true;
            this.lbl_Loader_Init_Decelation.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Loader_Init_Decelation.ForeColor = System.Drawing.Color.White;
            this.lbl_Loader_Init_Decelation.Location = new System.Drawing.Point(469, 610);
            this.lbl_Loader_Init_Decelation.Name = "lbl_Loader_Init_Decelation";
            this.lbl_Loader_Init_Decelation.Size = new System.Drawing.Size(132, 23);
            this.lbl_Loader_Init_Decelation.TabIndex = 46;
            this.lbl_Loader_Init_Decelation.Text = "Init Decelation :";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.pnLoader15, 0, 16);
            this.tableLayoutPanel1.Controls.Add(this.pnLoader14, 0, 15);
            this.tableLayoutPanel1.Controls.Add(this.pnLoader13, 0, 14);
            this.tableLayoutPanel1.Controls.Add(this.pnLoader12, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.pnLoader11, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.pnLoader10, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.pnLoader09, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.pnLoader08, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.pnLoader07, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.pnLoader06, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.pnLoader05, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.pnLoader04, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.pnLoader36, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.pnLoader35, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.pnLoader33, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.pnLoader32, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.pnLoader31, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 26);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 17;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.882354F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.882354F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.882354F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.882354F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.882354F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.882354F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.882354F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.882354F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.882354F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.882354F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.882354F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.882354F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.882354F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.882354F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.882354F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.882354F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.882354F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(520, 529);
            this.tableLayoutPanel1.TabIndex = 32;
            // 
            // pnLoader15
            // 
            this.pnLoader15.Controls.Add(this.lblLoader15);
            this.pnLoader15.Location = new System.Drawing.Point(3, 499);
            this.pnLoader15.Name = "pnLoader15";
            this.pnLoader15.Size = new System.Drawing.Size(514, 25);
            this.pnLoader15.TabIndex = 49;
            this.pnLoader15.Click += new System.EventHandler(this.pn_Loader_15_Click);
            // 
            // lblLoader15
            // 
            this.lblLoader15.AutoSize = true;
            this.lblLoader15.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblLoader15.ForeColor = System.Drawing.Color.White;
            this.lblLoader15.Location = new System.Drawing.Point(3, 5);
            this.lblLoader15.Name = "lblLoader15";
            this.lblLoader15.Size = new System.Drawing.Size(169, 19);
            this.lblLoader15.TabIndex = 0;
            this.lblLoader15.Text = "15. CELL_TRANSFER_T_2";
            this.lblLoader15.Click += new System.EventHandler(this.pn_Loader_15_Click);
            // 
            // pnLoader14
            // 
            this.pnLoader14.Controls.Add(this.lblLoader14);
            this.pnLoader14.Location = new System.Drawing.Point(3, 468);
            this.pnLoader14.Name = "pnLoader14";
            this.pnLoader14.Size = new System.Drawing.Size(514, 25);
            this.pnLoader14.TabIndex = 48;
            this.pnLoader14.Click += new System.EventHandler(this.pn_Loader_14_Click);
            // 
            // lblLoader14
            // 
            this.lblLoader14.AutoSize = true;
            this.lblLoader14.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblLoader14.ForeColor = System.Drawing.Color.White;
            this.lblLoader14.Location = new System.Drawing.Point(3, 5);
            this.lblLoader14.Name = "lblLoader14";
            this.lblLoader14.Size = new System.Drawing.Size(169, 19);
            this.lblLoader14.TabIndex = 0;
            this.lblLoader14.Text = "14. CELL_TRANSFER_Y_4";
            this.lblLoader14.Click += new System.EventHandler(this.pn_Loader_14_Click);
            // 
            // pnLoader13
            // 
            this.pnLoader13.Controls.Add(this.lblLoader13);
            this.pnLoader13.Location = new System.Drawing.Point(3, 437);
            this.pnLoader13.Name = "pnLoader13";
            this.pnLoader13.Size = new System.Drawing.Size(514, 25);
            this.pnLoader13.TabIndex = 47;
            this.pnLoader13.Click += new System.EventHandler(this.pn_Loader_13_Click);
            // 
            // lblLoader13
            // 
            this.lblLoader13.AutoSize = true;
            this.lblLoader13.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblLoader13.ForeColor = System.Drawing.Color.White;
            this.lblLoader13.Location = new System.Drawing.Point(3, 5);
            this.lblLoader13.Name = "lblLoader13";
            this.lblLoader13.Size = new System.Drawing.Size(93, 19);
            this.lblLoader13.TabIndex = 0;
            this.lblLoader13.Text = "13. CELL_Y_2";
            this.lblLoader13.Click += new System.EventHandler(this.pn_Loader_13_Click);
            // 
            // pnLoader12
            // 
            this.pnLoader12.Controls.Add(this.lblLoader12);
            this.pnLoader12.Location = new System.Drawing.Point(3, 406);
            this.pnLoader12.Name = "pnLoader12";
            this.pnLoader12.Size = new System.Drawing.Size(514, 25);
            this.pnLoader12.TabIndex = 46;
            this.pnLoader12.Click += new System.EventHandler(this.pn_Loader_12_Click);
            // 
            // lblLoader12
            // 
            this.lblLoader12.AutoSize = true;
            this.lblLoader12.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblLoader12.ForeColor = System.Drawing.Color.White;
            this.lblLoader12.Location = new System.Drawing.Point(3, 5);
            this.lblLoader12.Name = "lblLoader12";
            this.lblLoader12.Size = new System.Drawing.Size(169, 19);
            this.lblLoader12.TabIndex = 0;
            this.lblLoader12.Text = "12. CELL_TRANSFER_T_1";
            this.lblLoader12.Click += new System.EventHandler(this.pn_Loader_12_Click);
            // 
            // pnLoader11
            // 
            this.pnLoader11.Controls.Add(this.lblLoader11);
            this.pnLoader11.Location = new System.Drawing.Point(3, 375);
            this.pnLoader11.Name = "pnLoader11";
            this.pnLoader11.Size = new System.Drawing.Size(514, 25);
            this.pnLoader11.TabIndex = 45;
            this.pnLoader11.Click += new System.EventHandler(this.pn_Loader_11_Click);
            // 
            // lblLoader11
            // 
            this.lblLoader11.AutoSize = true;
            this.lblLoader11.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblLoader11.ForeColor = System.Drawing.Color.White;
            this.lblLoader11.Location = new System.Drawing.Point(3, 5);
            this.lblLoader11.Name = "lblLoader11";
            this.lblLoader11.Size = new System.Drawing.Size(169, 19);
            this.lblLoader11.TabIndex = 0;
            this.lblLoader11.Text = "11. CELL_TRANSFER_Y_3";
            this.lblLoader11.Click += new System.EventHandler(this.pn_Loader_11_Click);
            // 
            // pnLoader10
            // 
            this.pnLoader10.Controls.Add(this.lblLoader10);
            this.pnLoader10.Location = new System.Drawing.Point(3, 344);
            this.pnLoader10.Name = "pnLoader10";
            this.pnLoader10.Size = new System.Drawing.Size(514, 25);
            this.pnLoader10.TabIndex = 44;
            this.pnLoader10.Click += new System.EventHandler(this.pn_Loader_10_Click);
            // 
            // lblLoader10
            // 
            this.lblLoader10.AutoSize = true;
            this.lblLoader10.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblLoader10.ForeColor = System.Drawing.Color.White;
            this.lblLoader10.Location = new System.Drawing.Point(3, 5);
            this.lblLoader10.Name = "lblLoader10";
            this.lblLoader10.Size = new System.Drawing.Size(93, 19);
            this.lblLoader10.TabIndex = 0;
            this.lblLoader10.Text = "10. CELL_Y_1";
            this.lblLoader10.Click += new System.EventHandler(this.pn_Loader_10_Click);
            // 
            // pnLoader09
            // 
            this.pnLoader09.Controls.Add(this.lblLoader09);
            this.pnLoader09.Location = new System.Drawing.Point(3, 313);
            this.pnLoader09.Name = "pnLoader09";
            this.pnLoader09.Size = new System.Drawing.Size(514, 25);
            this.pnLoader09.TabIndex = 43;
            this.pnLoader09.Click += new System.EventHandler(this.pn_Loader_09_Click);
            // 
            // lblLoader09
            // 
            this.lblLoader09.AutoSize = true;
            this.lblLoader09.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblLoader09.ForeColor = System.Drawing.Color.White;
            this.lblLoader09.Location = new System.Drawing.Point(3, 5);
            this.lblLoader09.Name = "lblLoader09";
            this.lblLoader09.Size = new System.Drawing.Size(248, 19);
            this.lblLoader09.TabIndex = 0;
            this.lblLoader09.Text = "09. CASSETTE_ELECTRICTY_Z_RIGHT";
            this.lblLoader09.Click += new System.EventHandler(this.pn_Loader_09_Click);
            // 
            // pnLoader08
            // 
            this.pnLoader08.Controls.Add(this.lblLoader08);
            this.pnLoader08.Location = new System.Drawing.Point(3, 282);
            this.pnLoader08.Name = "pnLoader08";
            this.pnLoader08.Size = new System.Drawing.Size(514, 25);
            this.pnLoader08.TabIndex = 42;
            this.pnLoader08.Click += new System.EventHandler(this.pn_Loader_08_Click);
            // 
            // lblLoader08
            // 
            this.lblLoader08.AutoSize = true;
            this.lblLoader08.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblLoader08.ForeColor = System.Drawing.Color.White;
            this.lblLoader08.Location = new System.Drawing.Point(3, 5);
            this.lblLoader08.Name = "lblLoader08";
            this.lblLoader08.Size = new System.Drawing.Size(236, 19);
            this.lblLoader08.TabIndex = 0;
            this.lblLoader08.Text = "08. CASSETTE_ELECTRICTY_Z_LEFT";
            this.lblLoader08.Click += new System.EventHandler(this.pn_Loader_08_Click);
            // 
            // pnLoader07
            // 
            this.pnLoader07.Controls.Add(this.lblLoader07);
            this.pnLoader07.Location = new System.Drawing.Point(3, 251);
            this.pnLoader07.Name = "pnLoader07";
            this.pnLoader07.Size = new System.Drawing.Size(514, 25);
            this.pnLoader07.TabIndex = 41;
            this.pnLoader07.Click += new System.EventHandler(this.pn_Loader_07_Click);
            // 
            // lblLoader07
            // 
            this.lblLoader07.AutoSize = true;
            this.lblLoader07.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblLoader07.ForeColor = System.Drawing.Color.White;
            this.lblLoader07.Location = new System.Drawing.Point(3, 5);
            this.lblLoader07.Name = "lblLoader07";
            this.lblLoader07.Size = new System.Drawing.Size(279, 19);
            this.lblLoader07.TabIndex = 0;
            this.lblLoader07.Text = "07. CASSETTE_ROTATION_DOWN_RIGHT";
            this.lblLoader07.Click += new System.EventHandler(this.pn_Loader_07_Click);
            // 
            // pnLoader06
            // 
            this.pnLoader06.Controls.Add(this.lblLoader06);
            this.pnLoader06.Location = new System.Drawing.Point(3, 220);
            this.pnLoader06.Name = "pnLoader06";
            this.pnLoader06.Size = new System.Drawing.Size(514, 25);
            this.pnLoader06.TabIndex = 40;
            this.pnLoader06.Click += new System.EventHandler(this.pn_Loader_06_Click);
            // 
            // lblLoader06
            // 
            this.lblLoader06.AutoSize = true;
            this.lblLoader06.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblLoader06.ForeColor = System.Drawing.Color.White;
            this.lblLoader06.Location = new System.Drawing.Point(3, 5);
            this.lblLoader06.Name = "lblLoader06";
            this.lblLoader06.Size = new System.Drawing.Size(252, 19);
            this.lblLoader06.TabIndex = 0;
            this.lblLoader06.Text = "06. CASSETTE_ROTATION_UP_RIGHT";
            this.lblLoader06.Click += new System.EventHandler(this.pn_Loader_06_Click);
            // 
            // pnLoader05
            // 
            this.pnLoader05.Controls.Add(this.lblLoader05);
            this.pnLoader05.Location = new System.Drawing.Point(3, 189);
            this.pnLoader05.Name = "pnLoader05";
            this.pnLoader05.Size = new System.Drawing.Size(514, 25);
            this.pnLoader05.TabIndex = 39;
            this.pnLoader05.Click += new System.EventHandler(this.pn_Loader_05_Click);
            // 
            // lblLoader05
            // 
            this.lblLoader05.AutoSize = true;
            this.lblLoader05.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblLoader05.ForeColor = System.Drawing.Color.White;
            this.lblLoader05.Location = new System.Drawing.Point(3, 5);
            this.lblLoader05.Name = "lblLoader05";
            this.lblLoader05.Size = new System.Drawing.Size(267, 19);
            this.lblLoader05.TabIndex = 0;
            this.lblLoader05.Text = "05. CASSETTE_ROTATION_DOWN_LEFT";
            this.lblLoader05.Click += new System.EventHandler(this.pn_Loader_05_Click);
            // 
            // pnLoader04
            // 
            this.pnLoader04.Controls.Add(this.lblLoader04);
            this.pnLoader04.Location = new System.Drawing.Point(3, 158);
            this.pnLoader04.Name = "pnLoader04";
            this.pnLoader04.Size = new System.Drawing.Size(514, 25);
            this.pnLoader04.TabIndex = 38;
            this.pnLoader04.Click += new System.EventHandler(this.pn_Loader_04_Click);
            // 
            // lblLoader04
            // 
            this.lblLoader04.AutoSize = true;
            this.lblLoader04.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblLoader04.ForeColor = System.Drawing.Color.White;
            this.lblLoader04.Location = new System.Drawing.Point(3, 5);
            this.lblLoader04.Name = "lblLoader04";
            this.lblLoader04.Size = new System.Drawing.Size(240, 19);
            this.lblLoader04.TabIndex = 0;
            this.lblLoader04.Text = "04. CASSETTE_ROTATION_UP_LEFT";
            this.lblLoader04.Click += new System.EventHandler(this.pn_Loader_04_Click);
            // 
            // pnLoader36
            // 
            this.pnLoader36.Controls.Add(this.lblLoader36);
            this.pnLoader36.Location = new System.Drawing.Point(3, 127);
            this.pnLoader36.Name = "pnLoader36";
            this.pnLoader36.Size = new System.Drawing.Size(514, 25);
            this.pnLoader36.TabIndex = 37;
            this.pnLoader36.Click += new System.EventHandler(this.pn_Loader_36_Click);
            // 
            // lblLoader36
            // 
            this.lblLoader36.AutoSize = true;
            this.lblLoader36.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblLoader36.ForeColor = System.Drawing.Color.White;
            this.lblLoader36.Location = new System.Drawing.Point(3, 5);
            this.lblLoader36.Name = "lblLoader36";
            this.lblLoader36.Size = new System.Drawing.Size(170, 19);
            this.lblLoader36.TabIndex = 0;
            this.lblLoader36.Text = "36. CELL_TRANSFER_X_2";
            this.lblLoader36.Click += new System.EventHandler(this.pn_Loader_36_Click);
            // 
            // pnLoader35
            // 
            this.pnLoader35.Controls.Add(this.lblLoader35);
            this.pnLoader35.Location = new System.Drawing.Point(3, 96);
            this.pnLoader35.Name = "pnLoader35";
            this.pnLoader35.Size = new System.Drawing.Size(514, 25);
            this.pnLoader35.TabIndex = 36;
            this.pnLoader35.Click += new System.EventHandler(this.pn_Loader_35_Click);
            // 
            // lblLoader35
            // 
            this.lblLoader35.AutoSize = true;
            this.lblLoader35.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblLoader35.ForeColor = System.Drawing.Color.White;
            this.lblLoader35.Location = new System.Drawing.Point(3, 5);
            this.lblLoader35.Name = "lblLoader35";
            this.lblLoader35.Size = new System.Drawing.Size(170, 19);
            this.lblLoader35.TabIndex = 0;
            this.lblLoader35.Text = "35. CELL_TRANSFER_X_1";
            this.lblLoader35.Click += new System.EventHandler(this.pn_Loader_35_Click);
            // 
            // pnLoader33
            // 
            this.pnLoader33.Controls.Add(this.lblLoader33);
            this.pnLoader33.Location = new System.Drawing.Point(3, 65);
            this.pnLoader33.Name = "pnLoader33";
            this.pnLoader33.Size = new System.Drawing.Size(514, 25);
            this.pnLoader33.TabIndex = 35;
            this.pnLoader33.Click += new System.EventHandler(this.pn_Loader_33_Click);
            // 
            // lblLoader33
            // 
            this.lblLoader33.AutoSize = true;
            this.lblLoader33.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblLoader33.ForeColor = System.Drawing.Color.White;
            this.lblLoader33.Location = new System.Drawing.Point(3, 5);
            this.lblLoader33.Name = "lblLoader33";
            this.lblLoader33.Size = new System.Drawing.Size(169, 19);
            this.lblLoader33.TabIndex = 0;
            this.lblLoader33.Text = "33. CELL_TRANSFER_Y_1";
            this.lblLoader33.Click += new System.EventHandler(this.pn_Loader_33_Click);
            // 
            // pnLoader32
            // 
            this.pnLoader32.Controls.Add(this.lblLoader32);
            this.pnLoader32.Location = new System.Drawing.Point(3, 34);
            this.pnLoader32.Name = "pnLoader32";
            this.pnLoader32.Size = new System.Drawing.Size(514, 25);
            this.pnLoader32.TabIndex = 34;
            this.pnLoader32.Click += new System.EventHandler(this.pn_Loader_32_Click);
            // 
            // lblLoader32
            // 
            this.lblLoader32.AutoSize = true;
            this.lblLoader32.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblLoader32.ForeColor = System.Drawing.Color.White;
            this.lblLoader32.Location = new System.Drawing.Point(3, 5);
            this.lblLoader32.Name = "lblLoader32";
            this.lblLoader32.Size = new System.Drawing.Size(94, 19);
            this.lblLoader32.TabIndex = 0;
            this.lblLoader32.Text = "32. CELL_X_2";
            this.lblLoader32.Click += new System.EventHandler(this.pn_Loader_32_Click);
            // 
            // pnLoader31
            // 
            this.pnLoader31.Controls.Add(this.lblLoader31);
            this.pnLoader31.Location = new System.Drawing.Point(3, 3);
            this.pnLoader31.Name = "pnLoader31";
            this.pnLoader31.Size = new System.Drawing.Size(514, 25);
            this.pnLoader31.TabIndex = 33;
            this.pnLoader31.Click += new System.EventHandler(this.pn_Loader_31_Click);
            // 
            // lblLoader31
            // 
            this.lblLoader31.AutoSize = true;
            this.lblLoader31.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblLoader31.ForeColor = System.Drawing.Color.White;
            this.lblLoader31.Location = new System.Drawing.Point(3, 5);
            this.lblLoader31.Name = "lblLoader31";
            this.lblLoader31.Size = new System.Drawing.Size(94, 19);
            this.lblLoader31.TabIndex = 0;
            this.lblLoader31.Text = "31. CELL_X_1";
            this.lblLoader31.Click += new System.EventHandler(this.pn_Loader_31_Click);
            // 
            // tp_iostatus_process
            // 
            this.tp_iostatus_process.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tp_iostatus_process.Controls.Add(this.tableLayoutPanel2);
            this.tp_iostatus_process.Controls.Add(this.panel2);
            this.tp_iostatus_process.Font = new System.Drawing.Font("굴림", 20F);
            this.tp_iostatus_process.Location = new System.Drawing.Point(4, 34);
            this.tp_iostatus_process.Name = "tp_iostatus_process";
            this.tp_iostatus_process.Padding = new System.Windows.Forms.Padding(3);
            this.tp_iostatus_process.Size = new System.Drawing.Size(1726, 767);
            this.tp_iostatus_process.TabIndex = 1;
            this.tp_iostatus_process.Text = "프로세스";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.pnProcess21, 0, 18);
            this.tableLayoutPanel2.Controls.Add(this.pnProcess19, 0, 16);
            this.tableLayoutPanel2.Controls.Add(this.pnProcess18, 0, 15);
            this.tableLayoutPanel2.Controls.Add(this.pnProcess14, 0, 14);
            this.tableLayoutPanel2.Controls.Add(this.pnProcess13, 0, 13);
            this.tableLayoutPanel2.Controls.Add(this.pnProcess12, 0, 12);
            this.tableLayoutPanel2.Controls.Add(this.pnProcess11, 0, 11);
            this.tableLayoutPanel2.Controls.Add(this.pnProcess10, 0, 10);
            this.tableLayoutPanel2.Controls.Add(this.pnProcess09, 0, 9);
            this.tableLayoutPanel2.Controls.Add(this.pnProcess22, 0, 8);
            this.tableLayoutPanel2.Controls.Add(this.pnProcess07, 0, 7);
            this.tableLayoutPanel2.Controls.Add(this.pnProcess06, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.pnProcess05, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.pnProcess04, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.pnProcess17, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.pnProcess03, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.pnProcess02, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.pnProcess01, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.pnProcess20, 0, 17);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(6, 26);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 19;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263157F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(520, 593);
            this.tableLayoutPanel2.TabIndex = 81;
            // 
            // pnProcess21
            // 
            this.pnProcess21.Controls.Add(this.lblProcess21);
            this.pnProcess21.Location = new System.Drawing.Point(3, 561);
            this.pnProcess21.Name = "pnProcess21";
            this.pnProcess21.Size = new System.Drawing.Size(514, 29);
            this.pnProcess21.TabIndex = 51;
            this.pnProcess21.Click += new System.EventHandler(this.pn_Process_21_Click);
            // 
            // lblProcess21
            // 
            this.lblProcess21.AutoSize = true;
            this.lblProcess21.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblProcess21.ForeColor = System.Drawing.Color.White;
            this.lblProcess21.Location = new System.Drawing.Point(3, 5);
            this.lblProcess21.Name = "lblProcess21";
            this.lblProcess21.Size = new System.Drawing.Size(108, 19);
            this.lblProcess21.TabIndex = 0;
            this.lblProcess21.Text = "21. BREAK_Z_4";
            this.lblProcess21.Click += new System.EventHandler(this.pn_Process_21_Click);
            // 
            // pnProcess19
            // 
            this.pnProcess19.Controls.Add(this.lblProcess19);
            this.pnProcess19.Location = new System.Drawing.Point(3, 499);
            this.pnProcess19.Name = "pnProcess19";
            this.pnProcess19.Size = new System.Drawing.Size(514, 25);
            this.pnProcess19.TabIndex = 49;
            this.pnProcess19.Click += new System.EventHandler(this.pn_Process_19_Click);
            // 
            // lblProcess19
            // 
            this.lblProcess19.AutoSize = true;
            this.lblProcess19.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblProcess19.ForeColor = System.Drawing.Color.White;
            this.lblProcess19.Location = new System.Drawing.Point(3, 5);
            this.lblProcess19.Name = "lblProcess19";
            this.lblProcess19.Size = new System.Drawing.Size(108, 19);
            this.lblProcess19.TabIndex = 0;
            this.lblProcess19.Text = "19. BREAK_Z_2";
            this.lblProcess19.Click += new System.EventHandler(this.pn_Process_19_Click);
            // 
            // pnProcess18
            // 
            this.pnProcess18.Controls.Add(this.lblProcess18);
            this.pnProcess18.Location = new System.Drawing.Point(3, 468);
            this.pnProcess18.Name = "pnProcess18";
            this.pnProcess18.Size = new System.Drawing.Size(514, 25);
            this.pnProcess18.TabIndex = 48;
            this.pnProcess18.Click += new System.EventHandler(this.pn_Process_18_Click);
            // 
            // lblProcess18
            // 
            this.lblProcess18.AutoSize = true;
            this.lblProcess18.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblProcess18.ForeColor = System.Drawing.Color.White;
            this.lblProcess18.Location = new System.Drawing.Point(3, 5);
            this.lblProcess18.Name = "lblProcess18";
            this.lblProcess18.Size = new System.Drawing.Size(108, 19);
            this.lblProcess18.TabIndex = 0;
            this.lblProcess18.Text = "18. BREAK_Z_1";
            this.lblProcess18.Click += new System.EventHandler(this.pn_Process_18_Click);
            // 
            // pnProcess14
            // 
            this.pnProcess14.Controls.Add(this.lblProcess14);
            this.pnProcess14.Location = new System.Drawing.Point(3, 437);
            this.pnProcess14.Name = "pnProcess14";
            this.pnProcess14.Size = new System.Drawing.Size(514, 25);
            this.pnProcess14.TabIndex = 47;
            this.pnProcess14.Click += new System.EventHandler(this.pn_Process_14_Click);
            // 
            // lblProcess14
            // 
            this.lblProcess14.AutoSize = true;
            this.lblProcess14.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblProcess14.ForeColor = System.Drawing.Color.White;
            this.lblProcess14.Location = new System.Drawing.Point(3, 5);
            this.lblProcess14.Name = "lblProcess14";
            this.lblProcess14.Size = new System.Drawing.Size(108, 19);
            this.lblProcess14.TabIndex = 0;
            this.lblProcess14.Text = "14. BREAK_X_4";
            this.lblProcess14.Click += new System.EventHandler(this.pn_Process_14_Click);
            // 
            // pnProcess13
            // 
            this.pnProcess13.Controls.Add(this.lblProcess13);
            this.pnProcess13.Location = new System.Drawing.Point(3, 406);
            this.pnProcess13.Name = "pnProcess13";
            this.pnProcess13.Size = new System.Drawing.Size(514, 25);
            this.pnProcess13.TabIndex = 46;
            this.pnProcess13.Click += new System.EventHandler(this.pn_Process_13_Click);
            // 
            // lblProcess13
            // 
            this.lblProcess13.AutoSize = true;
            this.lblProcess13.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblProcess13.ForeColor = System.Drawing.Color.White;
            this.lblProcess13.Location = new System.Drawing.Point(3, 5);
            this.lblProcess13.Name = "lblProcess13";
            this.lblProcess13.Size = new System.Drawing.Size(108, 19);
            this.lblProcess13.TabIndex = 0;
            this.lblProcess13.Text = "13. BREAK_X_3";
            this.lblProcess13.Click += new System.EventHandler(this.pn_Process_13_Click);
            // 
            // pnProcess12
            // 
            this.pnProcess12.Controls.Add(this.lblProcess12);
            this.pnProcess12.Location = new System.Drawing.Point(3, 375);
            this.pnProcess12.Name = "pnProcess12";
            this.pnProcess12.Size = new System.Drawing.Size(514, 25);
            this.pnProcess12.TabIndex = 45;
            this.pnProcess12.Click += new System.EventHandler(this.pn_Process_12_Click);
            // 
            // lblProcess12
            // 
            this.lblProcess12.AutoSize = true;
            this.lblProcess12.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblProcess12.ForeColor = System.Drawing.Color.White;
            this.lblProcess12.Location = new System.Drawing.Point(3, 5);
            this.lblProcess12.Name = "lblProcess12";
            this.lblProcess12.Size = new System.Drawing.Size(108, 19);
            this.lblProcess12.TabIndex = 0;
            this.lblProcess12.Text = "12. BREAK_X_2";
            this.lblProcess12.Click += new System.EventHandler(this.pn_Process_12_Click);
            // 
            // pnProcess11
            // 
            this.pnProcess11.Controls.Add(this.lblProcess11);
            this.pnProcess11.Location = new System.Drawing.Point(3, 344);
            this.pnProcess11.Name = "pnProcess11";
            this.pnProcess11.Size = new System.Drawing.Size(514, 25);
            this.pnProcess11.TabIndex = 44;
            this.pnProcess11.Click += new System.EventHandler(this.pn_Process_11_Click);
            // 
            // lblProcess11
            // 
            this.lblProcess11.AutoSize = true;
            this.lblProcess11.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblProcess11.ForeColor = System.Drawing.Color.White;
            this.lblProcess11.Location = new System.Drawing.Point(3, 5);
            this.lblProcess11.Name = "lblProcess11";
            this.lblProcess11.Size = new System.Drawing.Size(108, 19);
            this.lblProcess11.TabIndex = 0;
            this.lblProcess11.Text = "11. BREAK_X_1";
            this.lblProcess11.Click += new System.EventHandler(this.pn_Process_11_Click);
            // 
            // pnProcess10
            // 
            this.pnProcess10.Controls.Add(this.lblProcess10);
            this.pnProcess10.Location = new System.Drawing.Point(3, 313);
            this.pnProcess10.Name = "pnProcess10";
            this.pnProcess10.Size = new System.Drawing.Size(514, 25);
            this.pnProcess10.TabIndex = 43;
            this.pnProcess10.Click += new System.EventHandler(this.pn_Process_10_Click);
            // 
            // lblProcess10
            // 
            this.lblProcess10.AutoSize = true;
            this.lblProcess10.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblProcess10.ForeColor = System.Drawing.Color.White;
            this.lblProcess10.Location = new System.Drawing.Point(3, 5);
            this.lblProcess10.Name = "lblProcess10";
            this.lblProcess10.Size = new System.Drawing.Size(155, 19);
            this.lblProcess10.TabIndex = 0;
            this.lblProcess10.Text = "10. BREAK_TABLE_Y_2";
            this.lblProcess10.Click += new System.EventHandler(this.pn_Process_10_Click);
            // 
            // pnProcess09
            // 
            this.pnProcess09.Controls.Add(this.lblProcess09);
            this.pnProcess09.Location = new System.Drawing.Point(3, 282);
            this.pnProcess09.Name = "pnProcess09";
            this.pnProcess09.Size = new System.Drawing.Size(514, 25);
            this.pnProcess09.TabIndex = 42;
            this.pnProcess09.Click += new System.EventHandler(this.pn_Process_09_Click);
            // 
            // lblProcess09
            // 
            this.lblProcess09.AutoSize = true;
            this.lblProcess09.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblProcess09.ForeColor = System.Drawing.Color.White;
            this.lblProcess09.Location = new System.Drawing.Point(3, 5);
            this.lblProcess09.Name = "lblProcess09";
            this.lblProcess09.Size = new System.Drawing.Size(155, 19);
            this.lblProcess09.TabIndex = 0;
            this.lblProcess09.Text = "09. BREAK_TABLE_Y_1";
            this.lblProcess09.Click += new System.EventHandler(this.pn_Process_09_Click);
            // 
            // pnProcess22
            // 
            this.pnProcess22.Controls.Add(this.lblProcess22);
            this.pnProcess22.Location = new System.Drawing.Point(3, 251);
            this.pnProcess22.Name = "pnProcess22";
            this.pnProcess22.Size = new System.Drawing.Size(514, 25);
            this.pnProcess22.TabIndex = 41;
            this.pnProcess22.Click += new System.EventHandler(this.pn_Process_22_Click);
            // 
            // lblProcess22
            // 
            this.lblProcess22.AutoSize = true;
            this.lblProcess22.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblProcess22.ForeColor = System.Drawing.Color.White;
            this.lblProcess22.Location = new System.Drawing.Point(3, 5);
            this.lblProcess22.Name = "lblProcess22";
            this.lblProcess22.Size = new System.Drawing.Size(122, 19);
            this.lblProcess22.TabIndex = 0;
            this.lblProcess22.Text = "22. MASURE_X_1";
            this.lblProcess22.Click += new System.EventHandler(this.pn_Process_22_Click);
            // 
            // pnProcess07
            // 
            this.pnProcess07.Controls.Add(this.lblProcess07);
            this.pnProcess07.Location = new System.Drawing.Point(3, 220);
            this.pnProcess07.Name = "pnProcess07";
            this.pnProcess07.Size = new System.Drawing.Size(514, 25);
            this.pnProcess07.TabIndex = 40;
            this.pnProcess07.Click += new System.EventHandler(this.pn_Process_07_Click);
            // 
            // lblProcess07
            // 
            this.lblProcess07.AutoSize = true;
            this.lblProcess07.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblProcess07.ForeColor = System.Drawing.Color.White;
            this.lblProcess07.Location = new System.Drawing.Point(3, 5);
            this.lblProcess07.Name = "lblProcess07";
            this.lblProcess07.Size = new System.Drawing.Size(169, 19);
            this.lblProcess07.TabIndex = 0;
            this.lblProcess07.Text = "07. CELL_TRANSFER_Y_1";
            this.lblProcess07.Click += new System.EventHandler(this.pn_Process_07_Click);
            // 
            // pnProcess06
            // 
            this.pnProcess06.Controls.Add(this.lblProcess06);
            this.pnProcess06.Location = new System.Drawing.Point(3, 189);
            this.pnProcess06.Name = "pnProcess06";
            this.pnProcess06.Size = new System.Drawing.Size(514, 25);
            this.pnProcess06.TabIndex = 39;
            this.pnProcess06.Click += new System.EventHandler(this.pn_Process_06_Click);
            // 
            // lblProcess06
            // 
            this.lblProcess06.AutoSize = true;
            this.lblProcess06.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblProcess06.ForeColor = System.Drawing.Color.White;
            this.lblProcess06.Location = new System.Drawing.Point(3, 5);
            this.lblProcess06.Name = "lblProcess06";
            this.lblProcess06.Size = new System.Drawing.Size(170, 19);
            this.lblProcess06.TabIndex = 0;
            this.lblProcess06.Text = "06. CELL_TRANSFER_X_2";
            this.lblProcess06.Click += new System.EventHandler(this.pn_Process_06_Click);
            // 
            // pnProcess05
            // 
            this.pnProcess05.Controls.Add(this.lblProcess05);
            this.pnProcess05.Location = new System.Drawing.Point(3, 158);
            this.pnProcess05.Name = "pnProcess05";
            this.pnProcess05.Size = new System.Drawing.Size(514, 25);
            this.pnProcess05.TabIndex = 38;
            this.pnProcess05.Click += new System.EventHandler(this.pn_Process_05_Click);
            // 
            // lblProcess05
            // 
            this.lblProcess05.AutoSize = true;
            this.lblProcess05.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblProcess05.ForeColor = System.Drawing.Color.White;
            this.lblProcess05.Location = new System.Drawing.Point(3, 5);
            this.lblProcess05.Name = "lblProcess05";
            this.lblProcess05.Size = new System.Drawing.Size(170, 19);
            this.lblProcess05.TabIndex = 0;
            this.lblProcess05.Text = "05. CELL_TRANSFER_X_1";
            this.lblProcess05.Click += new System.EventHandler(this.pn_Process_05_Click);
            // 
            // pnProcess04
            // 
            this.pnProcess04.Controls.Add(this.lblProcess04);
            this.pnProcess04.Location = new System.Drawing.Point(3, 127);
            this.pnProcess04.Name = "pnProcess04";
            this.pnProcess04.Size = new System.Drawing.Size(514, 25);
            this.pnProcess04.TabIndex = 37;
            this.pnProcess04.Click += new System.EventHandler(this.pn_Process_04_Click);
            // 
            // lblProcess04
            // 
            this.lblProcess04.AutoSize = true;
            this.lblProcess04.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblProcess04.ForeColor = System.Drawing.Color.White;
            this.lblProcess04.Location = new System.Drawing.Point(3, 5);
            this.lblProcess04.Name = "lblProcess04";
            this.lblProcess04.Size = new System.Drawing.Size(105, 19);
            this.lblProcess04.TabIndex = 0;
            this.lblProcess04.Text = "04. ALIGN_X_1";
            this.lblProcess04.Click += new System.EventHandler(this.pn_Process_04_Click);
            // 
            // pnProcess17
            // 
            this.pnProcess17.Controls.Add(this.lblProcess17);
            this.pnProcess17.Location = new System.Drawing.Point(3, 96);
            this.pnProcess17.Name = "pnProcess17";
            this.pnProcess17.Size = new System.Drawing.Size(514, 25);
            this.pnProcess17.TabIndex = 36;
            this.pnProcess17.Click += new System.EventHandler(this.pn_Process_17_Click);
            // 
            // lblProcess17
            // 
            this.lblProcess17.AutoSize = true;
            this.lblProcess17.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblProcess17.ForeColor = System.Drawing.Color.White;
            this.lblProcess17.Location = new System.Drawing.Point(3, 5);
            this.lblProcess17.Name = "lblProcess17";
            this.lblProcess17.Size = new System.Drawing.Size(102, 19);
            this.lblProcess17.TabIndex = 0;
            this.lblProcess17.Text = "17. HEAD_Z_1";
            this.lblProcess17.Click += new System.EventHandler(this.pn_Process_17_Click);
            // 
            // pnProcess03
            // 
            this.pnProcess03.Controls.Add(this.lblProcess03);
            this.pnProcess03.Location = new System.Drawing.Point(3, 65);
            this.pnProcess03.Name = "pnProcess03";
            this.pnProcess03.Size = new System.Drawing.Size(514, 25);
            this.pnProcess03.TabIndex = 35;
            this.pnProcess03.Click += new System.EventHandler(this.pn_Process_03_Click);
            // 
            // lblProcess03
            // 
            this.lblProcess03.AutoSize = true;
            this.lblProcess03.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblProcess03.ForeColor = System.Drawing.Color.White;
            this.lblProcess03.Location = new System.Drawing.Point(3, 5);
            this.lblProcess03.Name = "lblProcess03";
            this.lblProcess03.Size = new System.Drawing.Size(102, 19);
            this.lblProcess03.TabIndex = 0;
            this.lblProcess03.Text = "03. HEAD_X_1";
            this.lblProcess03.Click += new System.EventHandler(this.pn_Process_03_Click);
            // 
            // pnProcess02
            // 
            this.pnProcess02.Controls.Add(this.lblProcess02);
            this.pnProcess02.Location = new System.Drawing.Point(3, 34);
            this.pnProcess02.Name = "pnProcess02";
            this.pnProcess02.Size = new System.Drawing.Size(514, 25);
            this.pnProcess02.TabIndex = 34;
            this.pnProcess02.Click += new System.EventHandler(this.pn_Process_02_Click);
            // 
            // lblProcess02
            // 
            this.lblProcess02.AutoSize = true;
            this.lblProcess02.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblProcess02.ForeColor = System.Drawing.Color.White;
            this.lblProcess02.Location = new System.Drawing.Point(3, 5);
            this.lblProcess02.Name = "lblProcess02";
            this.lblProcess02.Size = new System.Drawing.Size(104, 19);
            this.lblProcess02.TabIndex = 0;
            this.lblProcess02.Text = "02. TABLE_Y_2";
            this.lblProcess02.Click += new System.EventHandler(this.pn_Process_02_Click);
            // 
            // pnProcess01
            // 
            this.pnProcess01.Controls.Add(this.lblProcess01);
            this.pnProcess01.Location = new System.Drawing.Point(3, 3);
            this.pnProcess01.Name = "pnProcess01";
            this.pnProcess01.Size = new System.Drawing.Size(514, 25);
            this.pnProcess01.TabIndex = 33;
            this.pnProcess01.Click += new System.EventHandler(this.pn_Process_01_Click);
            // 
            // lblProcess01
            // 
            this.lblProcess01.AutoSize = true;
            this.lblProcess01.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblProcess01.ForeColor = System.Drawing.Color.White;
            this.lblProcess01.Location = new System.Drawing.Point(3, 5);
            this.lblProcess01.Name = "lblProcess01";
            this.lblProcess01.Size = new System.Drawing.Size(104, 19);
            this.lblProcess01.TabIndex = 0;
            this.lblProcess01.Text = "01. TABLE_Y_1";
            this.lblProcess01.Click += new System.EventHandler(this.pn_Process_01_Click);
            // 
            // pnProcess20
            // 
            this.pnProcess20.Controls.Add(this.lblProcess20);
            this.pnProcess20.Location = new System.Drawing.Point(3, 530);
            this.pnProcess20.Name = "pnProcess20";
            this.pnProcess20.Size = new System.Drawing.Size(514, 25);
            this.pnProcess20.TabIndex = 50;
            this.pnProcess20.Click += new System.EventHandler(this.pn_Process_20_Click);
            // 
            // lblProcess20
            // 
            this.lblProcess20.AutoSize = true;
            this.lblProcess20.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblProcess20.ForeColor = System.Drawing.Color.White;
            this.lblProcess20.Location = new System.Drawing.Point(3, 5);
            this.lblProcess20.Name = "lblProcess20";
            this.lblProcess20.Size = new System.Drawing.Size(108, 19);
            this.lblProcess20.TabIndex = 0;
            this.lblProcess20.Text = "20. BREAK_Z_3";
            this.lblProcess20.Click += new System.EventHandler(this.pn_Process_20_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DimGray;
            this.panel2.Controls.Add(this.lbl_Process_AxisParameter);
            this.panel2.Controls.Add(this.txtProcessAccelationTime);
            this.panel2.Controls.Add(this.lbl_Process_JogSpeed);
            this.panel2.Controls.Add(this.txtProcessSWLimitHigh);
            this.panel2.Controls.Add(this.lbl_Process_PtpSpeed);
            this.panel2.Controls.Add(this.txtProcessSWLimitLow);
            this.panel2.Controls.Add(this.lbl_Process_SWLimitHigh);
            this.panel2.Controls.Add(this.txtProcessPtpSpeed);
            this.panel2.Controls.Add(this.lbl_Process_AxisNo);
            this.panel2.Controls.Add(this.txtProcessStepSpeed);
            this.panel2.Controls.Add(this.lbl_Process_HomeAcceleration);
            this.panel2.Controls.Add(this.txtProcessJogSpeed);
            this.panel2.Controls.Add(this.lbl_Process_AccelationTime);
            this.panel2.Controls.Add(this.txtProcessHomeAcceleration);
            this.panel2.Controls.Add(this.lbl_Process_InPosition);
            this.panel2.Controls.Add(this.txtProcessHome2Velocity);
            this.panel2.Controls.Add(this.lbl_Process_SpeedRate);
            this.panel2.Controls.Add(this.txtProcessHome1Velocity);
            this.panel2.Controls.Add(this.lbl_Process_PositionRate);
            this.panel2.Controls.Add(this.txtProcessDefaultAcceleration);
            this.panel2.Controls.Add(this.lbl_Process_StepSpeed);
            this.panel2.Controls.Add(this.txtProcessDefauleVelocity);
            this.panel2.Controls.Add(this.lbl_Process_DefauleVelocity);
            this.panel2.Controls.Add(this.txtProcessInPosition);
            this.panel2.Controls.Add(this.lbl_Process_MoveTimeOut);
            this.panel2.Controls.Add(this.txtProcessMoveTimeOut);
            this.panel2.Controls.Add(this.lbl_Process_DefaultAcceleration);
            this.panel2.Controls.Add(this.txtProcessSpeedRate);
            this.panel2.Controls.Add(this.lbl_Process_Home1Velocity);
            this.panel2.Controls.Add(this.txtProcessPositionRate);
            this.panel2.Controls.Add(this.lbl_Process_SWLimitLow);
            this.panel2.Controls.Add(this.txtProcessAxisNo);
            this.panel2.Controls.Add(this.lbl_Process_Home2Velocity);
            this.panel2.Location = new System.Drawing.Point(881, 16);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(834, 745);
            this.panel2.TabIndex = 80;
            // 
            // lbl_Process_AxisParameter
            // 
            this.lbl_Process_AxisParameter.AutoSize = true;
            this.lbl_Process_AxisParameter.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Process_AxisParameter.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_AxisParameter.Location = new System.Drawing.Point(28, 24);
            this.lbl_Process_AxisParameter.Name = "lbl_Process_AxisParameter";
            this.lbl_Process_AxisParameter.Size = new System.Drawing.Size(125, 23);
            this.lbl_Process_AxisParameter.TabIndex = 24;
            this.lbl_Process_AxisParameter.Text = "Axis Parameter";
            // 
            // txtProcessAccelationTime
            // 
            this.txtProcessAccelationTime.Font = new System.Drawing.Font("굴림", 10F);
            this.txtProcessAccelationTime.Location = new System.Drawing.Point(334, 496);
            this.txtProcessAccelationTime.Name = "txtProcessAccelationTime";
            this.txtProcessAccelationTime.Size = new System.Drawing.Size(333, 23);
            this.txtProcessAccelationTime.TabIndex = 79;
            // 
            // lbl_Process_JogSpeed
            // 
            this.lbl_Process_JogSpeed.AutoSize = true;
            this.lbl_Process_JogSpeed.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Process_JogSpeed.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_JogSpeed.Location = new System.Drawing.Point(28, 346);
            this.lbl_Process_JogSpeed.Name = "lbl_Process_JogSpeed";
            this.lbl_Process_JogSpeed.Size = new System.Drawing.Size(99, 23);
            this.lbl_Process_JogSpeed.TabIndex = 25;
            this.lbl_Process_JogSpeed.Text = "Jog Speed :";
            // 
            // txtProcessSWLimitHigh
            // 
            this.txtProcessSWLimitHigh.Font = new System.Drawing.Font("굴림", 10F);
            this.txtProcessSWLimitHigh.Location = new System.Drawing.Point(334, 467);
            this.txtProcessSWLimitHigh.Name = "txtProcessSWLimitHigh";
            this.txtProcessSWLimitHigh.Size = new System.Drawing.Size(333, 23);
            this.txtProcessSWLimitHigh.TabIndex = 78;
            // 
            // lbl_Process_PtpSpeed
            // 
            this.lbl_Process_PtpSpeed.AutoSize = true;
            this.lbl_Process_PtpSpeed.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Process_PtpSpeed.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_PtpSpeed.Location = new System.Drawing.Point(28, 404);
            this.lbl_Process_PtpSpeed.Name = "lbl_Process_PtpSpeed";
            this.lbl_Process_PtpSpeed.Size = new System.Drawing.Size(99, 23);
            this.lbl_Process_PtpSpeed.TabIndex = 26;
            this.lbl_Process_PtpSpeed.Text = "Ptp Speed :";
            // 
            // txtProcessSWLimitLow
            // 
            this.txtProcessSWLimitLow.Font = new System.Drawing.Font("굴림", 10F);
            this.txtProcessSWLimitLow.Location = new System.Drawing.Point(334, 438);
            this.txtProcessSWLimitLow.Name = "txtProcessSWLimitLow";
            this.txtProcessSWLimitLow.Size = new System.Drawing.Size(333, 23);
            this.txtProcessSWLimitLow.TabIndex = 77;
            // 
            // lbl_Process_SWLimitHigh
            // 
            this.lbl_Process_SWLimitHigh.AutoSize = true;
            this.lbl_Process_SWLimitHigh.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Process_SWLimitHigh.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_SWLimitHigh.Location = new System.Drawing.Point(28, 467);
            this.lbl_Process_SWLimitHigh.Name = "lbl_Process_SWLimitHigh";
            this.lbl_Process_SWLimitHigh.Size = new System.Drawing.Size(172, 23);
            this.lbl_Process_SWLimitHigh.TabIndex = 27;
            this.lbl_Process_SWLimitHigh.Text = "Software Limit High :";
            // 
            // txtProcessPtpSpeed
            // 
            this.txtProcessPtpSpeed.Font = new System.Drawing.Font("굴림", 10F);
            this.txtProcessPtpSpeed.Location = new System.Drawing.Point(334, 409);
            this.txtProcessPtpSpeed.Name = "txtProcessPtpSpeed";
            this.txtProcessPtpSpeed.Size = new System.Drawing.Size(333, 23);
            this.txtProcessPtpSpeed.TabIndex = 76;
            // 
            // lbl_Process_AxisNo
            // 
            this.lbl_Process_AxisNo.AutoSize = true;
            this.lbl_Process_AxisNo.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Process_AxisNo.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_AxisNo.Location = new System.Drawing.Point(28, 61);
            this.lbl_Process_AxisNo.Name = "lbl_Process_AxisNo";
            this.lbl_Process_AxisNo.Size = new System.Drawing.Size(77, 23);
            this.lbl_Process_AxisNo.TabIndex = 28;
            this.lbl_Process_AxisNo.Text = "AxisNo. :";
            // 
            // txtProcessStepSpeed
            // 
            this.txtProcessStepSpeed.Font = new System.Drawing.Font("굴림", 10F);
            this.txtProcessStepSpeed.Location = new System.Drawing.Point(334, 380);
            this.txtProcessStepSpeed.Name = "txtProcessStepSpeed";
            this.txtProcessStepSpeed.Size = new System.Drawing.Size(333, 23);
            this.txtProcessStepSpeed.TabIndex = 75;
            // 
            // lbl_Process_HomeAcceleration
            // 
            this.lbl_Process_HomeAcceleration.AutoSize = true;
            this.lbl_Process_HomeAcceleration.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Process_HomeAcceleration.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_HomeAcceleration.Location = new System.Drawing.Point(28, 322);
            this.lbl_Process_HomeAcceleration.Name = "lbl_Process_HomeAcceleration";
            this.lbl_Process_HomeAcceleration.Size = new System.Drawing.Size(166, 23);
            this.lbl_Process_HomeAcceleration.TabIndex = 29;
            this.lbl_Process_HomeAcceleration.Text = "Home Acceleration :";
            // 
            // txtProcessJogSpeed
            // 
            this.txtProcessJogSpeed.Font = new System.Drawing.Font("굴림", 10F);
            this.txtProcessJogSpeed.Location = new System.Drawing.Point(334, 351);
            this.txtProcessJogSpeed.Name = "txtProcessJogSpeed";
            this.txtProcessJogSpeed.Size = new System.Drawing.Size(333, 23);
            this.txtProcessJogSpeed.TabIndex = 74;
            // 
            // lbl_Process_AccelationTime
            // 
            this.lbl_Process_AccelationTime.AutoSize = true;
            this.lbl_Process_AccelationTime.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Process_AccelationTime.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_AccelationTime.Location = new System.Drawing.Point(28, 496);
            this.lbl_Process_AccelationTime.Name = "lbl_Process_AccelationTime";
            this.lbl_Process_AccelationTime.Size = new System.Drawing.Size(142, 23);
            this.lbl_Process_AccelationTime.TabIndex = 30;
            this.lbl_Process_AccelationTime.Text = "Accelation Time :";
            // 
            // txtProcessHomeAcceleration
            // 
            this.txtProcessHomeAcceleration.Font = new System.Drawing.Font("굴림", 10F);
            this.txtProcessHomeAcceleration.Location = new System.Drawing.Point(334, 322);
            this.txtProcessHomeAcceleration.Name = "txtProcessHomeAcceleration";
            this.txtProcessHomeAcceleration.Size = new System.Drawing.Size(333, 23);
            this.txtProcessHomeAcceleration.TabIndex = 73;
            // 
            // lbl_Process_InPosition
            // 
            this.lbl_Process_InPosition.AutoSize = true;
            this.lbl_Process_InPosition.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Process_InPosition.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_InPosition.Location = new System.Drawing.Point(28, 177);
            this.lbl_Process_InPosition.Name = "lbl_Process_InPosition";
            this.lbl_Process_InPosition.Size = new System.Drawing.Size(102, 23);
            this.lbl_Process_InPosition.TabIndex = 31;
            this.lbl_Process_InPosition.Text = "In Position :";
            // 
            // txtProcessHome2Velocity
            // 
            this.txtProcessHome2Velocity.Font = new System.Drawing.Font("굴림", 10F);
            this.txtProcessHome2Velocity.Location = new System.Drawing.Point(334, 293);
            this.txtProcessHome2Velocity.Name = "txtProcessHome2Velocity";
            this.txtProcessHome2Velocity.Size = new System.Drawing.Size(333, 23);
            this.txtProcessHome2Velocity.TabIndex = 72;
            // 
            // lbl_Process_SpeedRate
            // 
            this.lbl_Process_SpeedRate.AutoSize = true;
            this.lbl_Process_SpeedRate.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Process_SpeedRate.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_SpeedRate.Location = new System.Drawing.Point(28, 119);
            this.lbl_Process_SpeedRate.Name = "lbl_Process_SpeedRate";
            this.lbl_Process_SpeedRate.Size = new System.Drawing.Size(107, 23);
            this.lbl_Process_SpeedRate.TabIndex = 32;
            this.lbl_Process_SpeedRate.Text = "Speed Rate :";
            // 
            // txtProcessHome1Velocity
            // 
            this.txtProcessHome1Velocity.Font = new System.Drawing.Font("굴림", 10F);
            this.txtProcessHome1Velocity.Location = new System.Drawing.Point(334, 264);
            this.txtProcessHome1Velocity.Name = "txtProcessHome1Velocity";
            this.txtProcessHome1Velocity.Size = new System.Drawing.Size(333, 23);
            this.txtProcessHome1Velocity.TabIndex = 71;
            // 
            // lbl_Process_PositionRate
            // 
            this.lbl_Process_PositionRate.AutoSize = true;
            this.lbl_Process_PositionRate.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Process_PositionRate.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_PositionRate.Location = new System.Drawing.Point(28, 90);
            this.lbl_Process_PositionRate.Name = "lbl_Process_PositionRate";
            this.lbl_Process_PositionRate.Size = new System.Drawing.Size(121, 23);
            this.lbl_Process_PositionRate.TabIndex = 33;
            this.lbl_Process_PositionRate.Text = "Position Rate :";
            // 
            // txtProcessDefaultAcceleration
            // 
            this.txtProcessDefaultAcceleration.Font = new System.Drawing.Font("굴림", 10F);
            this.txtProcessDefaultAcceleration.Location = new System.Drawing.Point(334, 235);
            this.txtProcessDefaultAcceleration.Name = "txtProcessDefaultAcceleration";
            this.txtProcessDefaultAcceleration.Size = new System.Drawing.Size(333, 23);
            this.txtProcessDefaultAcceleration.TabIndex = 70;
            // 
            // lbl_Process_StepSpeed
            // 
            this.lbl_Process_StepSpeed.AutoSize = true;
            this.lbl_Process_StepSpeed.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Process_StepSpeed.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_StepSpeed.Location = new System.Drawing.Point(28, 375);
            this.lbl_Process_StepSpeed.Name = "lbl_Process_StepSpeed";
            this.lbl_Process_StepSpeed.Size = new System.Drawing.Size(107, 23);
            this.lbl_Process_StepSpeed.TabIndex = 34;
            this.lbl_Process_StepSpeed.Text = "Step Speed :";
            // 
            // txtProcessDefauleVelocity
            // 
            this.txtProcessDefauleVelocity.Font = new System.Drawing.Font("굴림", 10F);
            this.txtProcessDefauleVelocity.Location = new System.Drawing.Point(334, 206);
            this.txtProcessDefauleVelocity.Name = "txtProcessDefauleVelocity";
            this.txtProcessDefauleVelocity.Size = new System.Drawing.Size(333, 23);
            this.txtProcessDefauleVelocity.TabIndex = 69;
            // 
            // lbl_Process_DefauleVelocity
            // 
            this.lbl_Process_DefauleVelocity.AutoSize = true;
            this.lbl_Process_DefauleVelocity.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Process_DefauleVelocity.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_DefauleVelocity.Location = new System.Drawing.Point(28, 206);
            this.lbl_Process_DefauleVelocity.Name = "lbl_Process_DefauleVelocity";
            this.lbl_Process_DefauleVelocity.Size = new System.Drawing.Size(144, 23);
            this.lbl_Process_DefauleVelocity.TabIndex = 35;
            this.lbl_Process_DefauleVelocity.Text = "Defaule Velocity :";
            // 
            // txtProcessInPosition
            // 
            this.txtProcessInPosition.Font = new System.Drawing.Font("굴림", 10F);
            this.txtProcessInPosition.Location = new System.Drawing.Point(334, 177);
            this.txtProcessInPosition.Name = "txtProcessInPosition";
            this.txtProcessInPosition.Size = new System.Drawing.Size(333, 23);
            this.txtProcessInPosition.TabIndex = 68;
            // 
            // lbl_Process_MoveTimeOut
            // 
            this.lbl_Process_MoveTimeOut.AutoSize = true;
            this.lbl_Process_MoveTimeOut.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Process_MoveTimeOut.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_MoveTimeOut.Location = new System.Drawing.Point(28, 148);
            this.lbl_Process_MoveTimeOut.Name = "lbl_Process_MoveTimeOut";
            this.lbl_Process_MoveTimeOut.Size = new System.Drawing.Size(141, 23);
            this.lbl_Process_MoveTimeOut.TabIndex = 36;
            this.lbl_Process_MoveTimeOut.Text = "Move Time Out :";
            // 
            // txtProcessMoveTimeOut
            // 
            this.txtProcessMoveTimeOut.Font = new System.Drawing.Font("굴림", 10F);
            this.txtProcessMoveTimeOut.Location = new System.Drawing.Point(334, 148);
            this.txtProcessMoveTimeOut.Name = "txtProcessMoveTimeOut";
            this.txtProcessMoveTimeOut.Size = new System.Drawing.Size(333, 23);
            this.txtProcessMoveTimeOut.TabIndex = 67;
            // 
            // lbl_Process_DefaultAcceleration
            // 
            this.lbl_Process_DefaultAcceleration.AutoSize = true;
            this.lbl_Process_DefaultAcceleration.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Process_DefaultAcceleration.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_DefaultAcceleration.Location = new System.Drawing.Point(28, 235);
            this.lbl_Process_DefaultAcceleration.Name = "lbl_Process_DefaultAcceleration";
            this.lbl_Process_DefaultAcceleration.Size = new System.Drawing.Size(175, 23);
            this.lbl_Process_DefaultAcceleration.TabIndex = 37;
            this.lbl_Process_DefaultAcceleration.Text = "Default Acceleration :";
            // 
            // txtProcessSpeedRate
            // 
            this.txtProcessSpeedRate.Font = new System.Drawing.Font("굴림", 10F);
            this.txtProcessSpeedRate.Location = new System.Drawing.Point(334, 119);
            this.txtProcessSpeedRate.Name = "txtProcessSpeedRate";
            this.txtProcessSpeedRate.Size = new System.Drawing.Size(333, 23);
            this.txtProcessSpeedRate.TabIndex = 66;
            // 
            // lbl_Process_Home1Velocity
            // 
            this.lbl_Process_Home1Velocity.AutoSize = true;
            this.lbl_Process_Home1Velocity.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Process_Home1Velocity.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_Home1Velocity.Location = new System.Drawing.Point(28, 264);
            this.lbl_Process_Home1Velocity.Name = "lbl_Process_Home1Velocity";
            this.lbl_Process_Home1Velocity.Size = new System.Drawing.Size(169, 23);
            this.lbl_Process_Home1Velocity.TabIndex = 38;
            this.lbl_Process_Home1Velocity.Text = "Home First Velocity :";
            // 
            // txtProcessPositionRate
            // 
            this.txtProcessPositionRate.Font = new System.Drawing.Font("굴림", 10F);
            this.txtProcessPositionRate.Location = new System.Drawing.Point(334, 90);
            this.txtProcessPositionRate.Name = "txtProcessPositionRate";
            this.txtProcessPositionRate.Size = new System.Drawing.Size(333, 23);
            this.txtProcessPositionRate.TabIndex = 65;
            // 
            // lbl_Process_SWLimitLow
            // 
            this.lbl_Process_SWLimitLow.AutoSize = true;
            this.lbl_Process_SWLimitLow.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Process_SWLimitLow.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_SWLimitLow.Location = new System.Drawing.Point(28, 438);
            this.lbl_Process_SWLimitLow.Name = "lbl_Process_SWLimitLow";
            this.lbl_Process_SWLimitLow.Size = new System.Drawing.Size(167, 23);
            this.lbl_Process_SWLimitLow.TabIndex = 39;
            this.lbl_Process_SWLimitLow.Text = "Software Limit Low :";
            // 
            // txtProcessAxisNo
            // 
            this.txtProcessAxisNo.Font = new System.Drawing.Font("굴림", 10F);
            this.txtProcessAxisNo.Location = new System.Drawing.Point(334, 61);
            this.txtProcessAxisNo.Name = "txtProcessAxisNo";
            this.txtProcessAxisNo.Size = new System.Drawing.Size(333, 23);
            this.txtProcessAxisNo.TabIndex = 64;
            // 
            // lbl_Process_Home2Velocity
            // 
            this.lbl_Process_Home2Velocity.AutoSize = true;
            this.lbl_Process_Home2Velocity.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Process_Home2Velocity.ForeColor = System.Drawing.Color.White;
            this.lbl_Process_Home2Velocity.Location = new System.Drawing.Point(28, 293);
            this.lbl_Process_Home2Velocity.Name = "lbl_Process_Home2Velocity";
            this.lbl_Process_Home2Velocity.Size = new System.Drawing.Size(194, 23);
            this.lbl_Process_Home2Velocity.TabIndex = 40;
            this.lbl_Process_Home2Velocity.Text = "Home Second Velocity :";
            // 
            // tp_iostatus_uld
            // 
            this.tp_iostatus_uld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tp_iostatus_uld.Controls.Add(this.tableLayoutPanel3);
            this.tp_iostatus_uld.Controls.Add(this.panel3);
            this.tp_iostatus_uld.Font = new System.Drawing.Font("굴림", 20F);
            this.tp_iostatus_uld.Location = new System.Drawing.Point(4, 34);
            this.tp_iostatus_uld.Name = "tp_iostatus_uld";
            this.tp_iostatus_uld.Padding = new System.Windows.Forms.Padding(3);
            this.tp_iostatus_uld.Size = new System.Drawing.Size(1726, 767);
            this.tp_iostatus_uld.TabIndex = 2;
            this.tp_iostatus_uld.Text = "언로더";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.pnUnloader3, 0, 21);
            this.tableLayoutPanel3.Controls.Add(this.pnUnloader0, 0, 18);
            this.tableLayoutPanel3.Controls.Add(this.pnUnloader30, 0, 17);
            this.tableLayoutPanel3.Controls.Add(this.pnUnloader29, 0, 16);
            this.tableLayoutPanel3.Controls.Add(this.pnUnloader28, 0, 15);
            this.tableLayoutPanel3.Controls.Add(this.pnUnloader27, 0, 14);
            this.tableLayoutPanel3.Controls.Add(this.pnUnloader26, 0, 13);
            this.tableLayoutPanel3.Controls.Add(this.pnUnloader24, 0, 12);
            this.tableLayoutPanel3.Controls.Add(this.pnUnloader23, 0, 11);
            this.tableLayoutPanel3.Controls.Add(this.pnUnloader21, 0, 10);
            this.tableLayoutPanel3.Controls.Add(this.pnUnloader20, 0, 9);
            this.tableLayoutPanel3.Controls.Add(this.pnUnloader19, 0, 8);
            this.tableLayoutPanel3.Controls.Add(this.pnUnloader18, 0, 7);
            this.tableLayoutPanel3.Controls.Add(this.pnUnloader17, 0, 6);
            this.tableLayoutPanel3.Controls.Add(this.pnUnloader16, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.pnUnloader41, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.pnUnloader40, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.pnUnloader39, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.pnUnloader38, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.pnUnloader1, 0, 19);
            this.tableLayoutPanel3.Controls.Add(this.pnUnloader37, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.pnUnloader2, 0, 20);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(6, 26);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 22;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545454F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(520, 706);
            this.tableLayoutPanel3.TabIndex = 82;
            // 
            // pnUnloader3
            // 
            this.pnUnloader3.Controls.Add(this.lblUnloader3);
            this.pnUnloader3.Location = new System.Drawing.Point(3, 675);
            this.pnUnloader3.Name = "pnUnloader3";
            this.pnUnloader3.Size = new System.Drawing.Size(514, 26);
            this.pnUnloader3.TabIndex = 53;
            this.pnUnloader3.Click += new System.EventHandler(this.pn_Unloader_3_Click);
            // 
            // lblUnloader3
            // 
            this.lblUnloader3.AutoSize = true;
            this.lblUnloader3.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblUnloader3.ForeColor = System.Drawing.Color.White;
            this.lblUnloader3.Location = new System.Drawing.Point(3, 5);
            this.lblUnloader3.Name = "lblUnloader3";
            this.lblUnloader3.Size = new System.Drawing.Size(147, 19);
            this.lblUnloader3.TabIndex = 0;
            this.lblUnloader3.Text = "3. BREAK_TABLE_T_4";
            this.lblUnloader3.Click += new System.EventHandler(this.pn_Unloader_3_Click);
            // 
            // pnUnloader0
            // 
            this.pnUnloader0.Controls.Add(this.lblUnloader0);
            this.pnUnloader0.Location = new System.Drawing.Point(3, 579);
            this.pnUnloader0.Name = "pnUnloader0";
            this.pnUnloader0.Size = new System.Drawing.Size(514, 26);
            this.pnUnloader0.TabIndex = 52;
            this.pnUnloader0.Click += new System.EventHandler(this.pn_Unloader_0_Click);
            // 
            // lblUnloader0
            // 
            this.lblUnloader0.AutoSize = true;
            this.lblUnloader0.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblUnloader0.ForeColor = System.Drawing.Color.White;
            this.lblUnloader0.Location = new System.Drawing.Point(3, 5);
            this.lblUnloader0.Name = "lblUnloader0";
            this.lblUnloader0.Size = new System.Drawing.Size(147, 19);
            this.lblUnloader0.TabIndex = 0;
            this.lblUnloader0.Text = "0. BREAK_TABLE_T_1";
            this.lblUnloader0.Click += new System.EventHandler(this.pn_Unloader_0_Click);
            // 
            // pnUnloader30
            // 
            this.pnUnloader30.Controls.Add(this.lblUnloader30);
            this.pnUnloader30.Location = new System.Drawing.Point(3, 547);
            this.pnUnloader30.Name = "pnUnloader30";
            this.pnUnloader30.Size = new System.Drawing.Size(514, 26);
            this.pnUnloader30.TabIndex = 51;
            this.pnUnloader30.Click += new System.EventHandler(this.pn_Unloader_30_Click);
            // 
            // lblUnloader30
            // 
            this.lblUnloader30.AutoSize = true;
            this.lblUnloader30.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblUnloader30.ForeColor = System.Drawing.Color.White;
            this.lblUnloader30.Location = new System.Drawing.Point(3, 5);
            this.lblUnloader30.Name = "lblUnloader30";
            this.lblUnloader30.Size = new System.Drawing.Size(279, 19);
            this.lblUnloader30.TabIndex = 0;
            this.lblUnloader30.Text = "30. CASSETTE_ROTATION_DOWN_RIGHT";
            this.lblUnloader30.Click += new System.EventHandler(this.pn_Unloader_30_Click);
            // 
            // pnUnloader29
            // 
            this.pnUnloader29.Controls.Add(this.lblUnloader29);
            this.pnUnloader29.Location = new System.Drawing.Point(3, 515);
            this.pnUnloader29.Name = "pnUnloader29";
            this.pnUnloader29.Size = new System.Drawing.Size(514, 26);
            this.pnUnloader29.TabIndex = 50;
            this.pnUnloader29.Click += new System.EventHandler(this.pn_Unloader_29_Click);
            // 
            // lblUnloader29
            // 
            this.lblUnloader29.AutoSize = true;
            this.lblUnloader29.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblUnloader29.ForeColor = System.Drawing.Color.White;
            this.lblUnloader29.Location = new System.Drawing.Point(3, 5);
            this.lblUnloader29.Name = "lblUnloader29";
            this.lblUnloader29.Size = new System.Drawing.Size(252, 19);
            this.lblUnloader29.TabIndex = 0;
            this.lblUnloader29.Text = "29. CASSETTE_ROTATION_UP_RIGHT";
            this.lblUnloader29.Click += new System.EventHandler(this.pn_Unloader_29_Click);
            // 
            // pnUnloader28
            // 
            this.pnUnloader28.Controls.Add(this.lblUnloader28);
            this.pnUnloader28.Location = new System.Drawing.Point(3, 483);
            this.pnUnloader28.Name = "pnUnloader28";
            this.pnUnloader28.Size = new System.Drawing.Size(514, 26);
            this.pnUnloader28.TabIndex = 49;
            this.pnUnloader28.Click += new System.EventHandler(this.pn_Unloader_28_Click);
            // 
            // lblUnloader28
            // 
            this.lblUnloader28.AutoSize = true;
            this.lblUnloader28.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblUnloader28.ForeColor = System.Drawing.Color.White;
            this.lblUnloader28.Location = new System.Drawing.Point(3, 5);
            this.lblUnloader28.Name = "lblUnloader28";
            this.lblUnloader28.Size = new System.Drawing.Size(93, 19);
            this.lblUnloader28.TabIndex = 0;
            this.lblUnloader28.Text = "28. CELL_Y_2";
            this.lblUnloader28.Click += new System.EventHandler(this.pn_Unloader_28_Click);
            // 
            // pnUnloader27
            // 
            this.pnUnloader27.Controls.Add(this.lblUnloader27);
            this.pnUnloader27.Location = new System.Drawing.Point(3, 451);
            this.pnUnloader27.Name = "pnUnloader27";
            this.pnUnloader27.Size = new System.Drawing.Size(514, 26);
            this.pnUnloader27.TabIndex = 48;
            this.pnUnloader27.Click += new System.EventHandler(this.pn_Unloader_27_Click);
            // 
            // lblUnloader27
            // 
            this.lblUnloader27.AutoSize = true;
            this.lblUnloader27.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblUnloader27.ForeColor = System.Drawing.Color.White;
            this.lblUnloader27.Location = new System.Drawing.Point(3, 5);
            this.lblUnloader27.Name = "lblUnloader27";
            this.lblUnloader27.Size = new System.Drawing.Size(93, 19);
            this.lblUnloader27.TabIndex = 0;
            this.lblUnloader27.Text = "27. CELL_Y_1";
            this.lblUnloader27.Click += new System.EventHandler(this.pn_Unloader_27_Click);
            // 
            // pnUnloader26
            // 
            this.pnUnloader26.Controls.Add(this.lblUnloader26);
            this.pnUnloader26.Location = new System.Drawing.Point(3, 419);
            this.pnUnloader26.Name = "pnUnloader26";
            this.pnUnloader26.Size = new System.Drawing.Size(514, 26);
            this.pnUnloader26.TabIndex = 47;
            this.pnUnloader26.Click += new System.EventHandler(this.pn_Unloader_26_Click);
            // 
            // lblUnloader26
            // 
            this.lblUnloader26.AutoSize = true;
            this.lblUnloader26.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblUnloader26.ForeColor = System.Drawing.Color.White;
            this.lblUnloader26.Location = new System.Drawing.Point(3, 5);
            this.lblUnloader26.Name = "lblUnloader26";
            this.lblUnloader26.Size = new System.Drawing.Size(95, 19);
            this.lblUnloader26.TabIndex = 0;
            this.lblUnloader26.Text = "26. INSP_Z_1";
            this.lblUnloader26.Click += new System.EventHandler(this.pn_Unloader_26_Click);
            // 
            // pnUnloader24
            // 
            this.pnUnloader24.Controls.Add(this.lblUnloader24);
            this.pnUnloader24.Location = new System.Drawing.Point(3, 387);
            this.pnUnloader24.Name = "pnUnloader24";
            this.pnUnloader24.Size = new System.Drawing.Size(514, 26);
            this.pnUnloader24.TabIndex = 46;
            this.pnUnloader24.Click += new System.EventHandler(this.pn_Unloader_24_Click);
            // 
            // lblUnloader24
            // 
            this.lblUnloader24.AutoSize = true;
            this.lblUnloader24.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblUnloader24.ForeColor = System.Drawing.Color.White;
            this.lblUnloader24.Location = new System.Drawing.Point(3, 5);
            this.lblUnloader24.Name = "lblUnloader24";
            this.lblUnloader24.Size = new System.Drawing.Size(169, 19);
            this.lblUnloader24.TabIndex = 0;
            this.lblUnloader24.Text = "24. CELL_TRANSFER_T_4";
            this.lblUnloader24.Click += new System.EventHandler(this.pn_Unloader_24_Click);
            // 
            // pnUnloader23
            // 
            this.pnUnloader23.Controls.Add(this.lblUnloader23);
            this.pnUnloader23.Location = new System.Drawing.Point(3, 355);
            this.pnUnloader23.Name = "pnUnloader23";
            this.pnUnloader23.Size = new System.Drawing.Size(514, 26);
            this.pnUnloader23.TabIndex = 45;
            this.pnUnloader23.Click += new System.EventHandler(this.pn_Unloader_23_Click);
            // 
            // lblUnloader23
            // 
            this.lblUnloader23.AutoSize = true;
            this.lblUnloader23.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblUnloader23.ForeColor = System.Drawing.Color.White;
            this.lblUnloader23.Location = new System.Drawing.Point(3, 5);
            this.lblUnloader23.Name = "lblUnloader23";
            this.lblUnloader23.Size = new System.Drawing.Size(169, 19);
            this.lblUnloader23.TabIndex = 0;
            this.lblUnloader23.Text = "23. CELL_TRANSFER_T_3";
            this.lblUnloader23.Click += new System.EventHandler(this.pn_Unloader_23_Click);
            // 
            // pnUnloader21
            // 
            this.pnUnloader21.Controls.Add(this.lblUnloader21);
            this.pnUnloader21.Location = new System.Drawing.Point(3, 323);
            this.pnUnloader21.Name = "pnUnloader21";
            this.pnUnloader21.Size = new System.Drawing.Size(514, 26);
            this.pnUnloader21.TabIndex = 44;
            this.pnUnloader21.Click += new System.EventHandler(this.pn_Unloader_21_Click);
            // 
            // lblUnloader21
            // 
            this.lblUnloader21.AutoSize = true;
            this.lblUnloader21.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblUnloader21.ForeColor = System.Drawing.Color.White;
            this.lblUnloader21.Location = new System.Drawing.Point(3, 5);
            this.lblUnloader21.Name = "lblUnloader21";
            this.lblUnloader21.Size = new System.Drawing.Size(169, 19);
            this.lblUnloader21.TabIndex = 0;
            this.lblUnloader21.Text = "21. CELL_TRANSFER_T_2";
            this.lblUnloader21.Click += new System.EventHandler(this.pn_Unloader_21_Click);
            // 
            // pnUnloader20
            // 
            this.pnUnloader20.Controls.Add(this.lblUnloader20);
            this.pnUnloader20.Location = new System.Drawing.Point(3, 291);
            this.pnUnloader20.Name = "pnUnloader20";
            this.pnUnloader20.Size = new System.Drawing.Size(514, 26);
            this.pnUnloader20.TabIndex = 43;
            this.pnUnloader20.Click += new System.EventHandler(this.pn_Unloader_20_Click);
            // 
            // lblUnloader20
            // 
            this.lblUnloader20.AutoSize = true;
            this.lblUnloader20.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblUnloader20.ForeColor = System.Drawing.Color.White;
            this.lblUnloader20.Location = new System.Drawing.Point(3, 5);
            this.lblUnloader20.Name = "lblUnloader20";
            this.lblUnloader20.Size = new System.Drawing.Size(169, 19);
            this.lblUnloader20.TabIndex = 0;
            this.lblUnloader20.Text = "20. CELL_TRANSFER_T_1";
            this.lblUnloader20.Click += new System.EventHandler(this.pn_Unloader_20_Click);
            // 
            // pnUnloader19
            // 
            this.pnUnloader19.Controls.Add(this.lblUnloader19);
            this.pnUnloader19.Location = new System.Drawing.Point(3, 259);
            this.pnUnloader19.Name = "pnUnloader19";
            this.pnUnloader19.Size = new System.Drawing.Size(514, 26);
            this.pnUnloader19.TabIndex = 42;
            this.pnUnloader19.Click += new System.EventHandler(this.pn_Unloader_19_Click);
            // 
            // lblUnloader19
            // 
            this.lblUnloader19.AutoSize = true;
            this.lblUnloader19.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblUnloader19.ForeColor = System.Drawing.Color.White;
            this.lblUnloader19.Location = new System.Drawing.Point(3, 5);
            this.lblUnloader19.Name = "lblUnloader19";
            this.lblUnloader19.Size = new System.Drawing.Size(248, 19);
            this.lblUnloader19.TabIndex = 0;
            this.lblUnloader19.Text = "19. CASSETTE_ELECTRICTY_Z_RIGHT";
            this.lblUnloader19.Click += new System.EventHandler(this.pn_Unloader_19_Click);
            // 
            // pnUnloader18
            // 
            this.pnUnloader18.Controls.Add(this.lblUnloader18);
            this.pnUnloader18.Location = new System.Drawing.Point(3, 227);
            this.pnUnloader18.Name = "pnUnloader18";
            this.pnUnloader18.Size = new System.Drawing.Size(514, 26);
            this.pnUnloader18.TabIndex = 41;
            this.pnUnloader18.Click += new System.EventHandler(this.pn_Unloader_18_Click);
            // 
            // lblUnloader18
            // 
            this.lblUnloader18.AutoSize = true;
            this.lblUnloader18.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblUnloader18.ForeColor = System.Drawing.Color.White;
            this.lblUnloader18.Location = new System.Drawing.Point(3, 5);
            this.lblUnloader18.Name = "lblUnloader18";
            this.lblUnloader18.Size = new System.Drawing.Size(236, 19);
            this.lblUnloader18.TabIndex = 0;
            this.lblUnloader18.Text = "18. CASSETTE_ELECTRICTY_Z_LEFT";
            this.lblUnloader18.Click += new System.EventHandler(this.pn_Unloader_18_Click);
            // 
            // pnUnloader17
            // 
            this.pnUnloader17.Controls.Add(this.lblUnloader17);
            this.pnUnloader17.Location = new System.Drawing.Point(3, 195);
            this.pnUnloader17.Name = "pnUnloader17";
            this.pnUnloader17.Size = new System.Drawing.Size(514, 26);
            this.pnUnloader17.TabIndex = 40;
            this.pnUnloader17.Click += new System.EventHandler(this.pn_Unloader_17_Click);
            // 
            // lblUnloader17
            // 
            this.lblUnloader17.AutoSize = true;
            this.lblUnloader17.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblUnloader17.ForeColor = System.Drawing.Color.White;
            this.lblUnloader17.Location = new System.Drawing.Point(3, 5);
            this.lblUnloader17.Name = "lblUnloader17";
            this.lblUnloader17.Size = new System.Drawing.Size(267, 19);
            this.lblUnloader17.TabIndex = 0;
            this.lblUnloader17.Text = "17. CASSETTE_ROTATION_DOWN_LEFT";
            this.lblUnloader17.Click += new System.EventHandler(this.pn_Unloader_17_Click);
            // 
            // pnUnloader16
            // 
            this.pnUnloader16.Controls.Add(this.lblUnloader16);
            this.pnUnloader16.Location = new System.Drawing.Point(3, 163);
            this.pnUnloader16.Name = "pnUnloader16";
            this.pnUnloader16.Size = new System.Drawing.Size(514, 26);
            this.pnUnloader16.TabIndex = 39;
            this.pnUnloader16.Click += new System.EventHandler(this.pn_Unloader_16_Click);
            // 
            // lblUnloader16
            // 
            this.lblUnloader16.AutoSize = true;
            this.lblUnloader16.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblUnloader16.ForeColor = System.Drawing.Color.White;
            this.lblUnloader16.Location = new System.Drawing.Point(3, 5);
            this.lblUnloader16.Name = "lblUnloader16";
            this.lblUnloader16.Size = new System.Drawing.Size(240, 19);
            this.lblUnloader16.TabIndex = 0;
            this.lblUnloader16.Text = "16. CASSETTE_ROTATION_UP_LEFT";
            this.lblUnloader16.Click += new System.EventHandler(this.pn_Unloader_16_Click);
            // 
            // pnUnloader41
            // 
            this.pnUnloader41.Controls.Add(this.lblUnloader41);
            this.pnUnloader41.Location = new System.Drawing.Point(3, 131);
            this.pnUnloader41.Name = "pnUnloader41";
            this.pnUnloader41.Size = new System.Drawing.Size(514, 26);
            this.pnUnloader41.TabIndex = 38;
            this.pnUnloader41.Click += new System.EventHandler(this.pn_Unloader_41_Click);
            // 
            // lblUnloader41
            // 
            this.lblUnloader41.AutoSize = true;
            this.lblUnloader41.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblUnloader41.ForeColor = System.Drawing.Color.White;
            this.lblUnloader41.Location = new System.Drawing.Point(3, 5);
            this.lblUnloader41.Name = "lblUnloader41";
            this.lblUnloader41.Size = new System.Drawing.Size(94, 19);
            this.lblUnloader41.TabIndex = 0;
            this.lblUnloader41.Text = "41. CELL_X_2";
            this.lblUnloader41.Click += new System.EventHandler(this.pn_Unloader_41_Click);
            // 
            // pnUnloader40
            // 
            this.pnUnloader40.Controls.Add(this.lblUnloader40);
            this.pnUnloader40.Location = new System.Drawing.Point(3, 99);
            this.pnUnloader40.Name = "pnUnloader40";
            this.pnUnloader40.Size = new System.Drawing.Size(514, 26);
            this.pnUnloader40.TabIndex = 37;
            this.pnUnloader40.Click += new System.EventHandler(this.pn_Unloader_40_Click);
            // 
            // lblUnloader40
            // 
            this.lblUnloader40.AutoSize = true;
            this.lblUnloader40.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblUnloader40.ForeColor = System.Drawing.Color.White;
            this.lblUnloader40.Location = new System.Drawing.Point(3, 5);
            this.lblUnloader40.Name = "lblUnloader40";
            this.lblUnloader40.Size = new System.Drawing.Size(94, 19);
            this.lblUnloader40.TabIndex = 0;
            this.lblUnloader40.Text = "40. CELL_X_1";
            this.lblUnloader40.Click += new System.EventHandler(this.pn_Unloader_40_Click);
            // 
            // pnUnloader39
            // 
            this.pnUnloader39.Controls.Add(this.lblUnloader39);
            this.pnUnloader39.Location = new System.Drawing.Point(3, 67);
            this.pnUnloader39.Name = "pnUnloader39";
            this.pnUnloader39.Size = new System.Drawing.Size(514, 26);
            this.pnUnloader39.TabIndex = 36;
            this.pnUnloader39.Click += new System.EventHandler(this.pn_Unloader_39_Click);
            // 
            // lblUnloader39
            // 
            this.lblUnloader39.AutoSize = true;
            this.lblUnloader39.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblUnloader39.ForeColor = System.Drawing.Color.White;
            this.lblUnloader39.Location = new System.Drawing.Point(3, 5);
            this.lblUnloader39.Name = "lblUnloader39";
            this.lblUnloader39.Size = new System.Drawing.Size(95, 19);
            this.lblUnloader39.TabIndex = 0;
            this.lblUnloader39.Text = "39. INSP_X_1";
            this.lblUnloader39.Click += new System.EventHandler(this.pn_Unloader_39_Click);
            // 
            // pnUnloader38
            // 
            this.pnUnloader38.Controls.Add(this.lblUnloader38);
            this.pnUnloader38.Location = new System.Drawing.Point(3, 35);
            this.pnUnloader38.Name = "pnUnloader38";
            this.pnUnloader38.Size = new System.Drawing.Size(514, 26);
            this.pnUnloader38.TabIndex = 35;
            this.pnUnloader38.Click += new System.EventHandler(this.pn_Unloader_38_Click);
            // 
            // lblUnloader38
            // 
            this.lblUnloader38.AutoSize = true;
            this.lblUnloader38.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblUnloader38.ForeColor = System.Drawing.Color.White;
            this.lblUnloader38.Location = new System.Drawing.Point(3, 5);
            this.lblUnloader38.Name = "lblUnloader38";
            this.lblUnloader38.Size = new System.Drawing.Size(169, 19);
            this.lblUnloader38.TabIndex = 0;
            this.lblUnloader38.Text = "38. CELL_TRANSFER_Y_2";
            this.lblUnloader38.Click += new System.EventHandler(this.pn_Unloader_38_Click);
            // 
            // pnUnloader1
            // 
            this.pnUnloader1.Controls.Add(this.lblUnloader1);
            this.pnUnloader1.Location = new System.Drawing.Point(3, 611);
            this.pnUnloader1.Name = "pnUnloader1";
            this.pnUnloader1.Size = new System.Drawing.Size(514, 26);
            this.pnUnloader1.TabIndex = 34;
            this.pnUnloader1.Click += new System.EventHandler(this.pn_Unloader_1_Click);
            // 
            // lblUnloader1
            // 
            this.lblUnloader1.AutoSize = true;
            this.lblUnloader1.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblUnloader1.ForeColor = System.Drawing.Color.White;
            this.lblUnloader1.Location = new System.Drawing.Point(3, 5);
            this.lblUnloader1.Name = "lblUnloader1";
            this.lblUnloader1.Size = new System.Drawing.Size(147, 19);
            this.lblUnloader1.TabIndex = 0;
            this.lblUnloader1.Text = "1. BREAK_TABLE_T_2";
            this.lblUnloader1.Click += new System.EventHandler(this.pn_Unloader_1_Click);
            // 
            // pnUnloader37
            // 
            this.pnUnloader37.Controls.Add(this.lblUnloader37);
            this.pnUnloader37.Location = new System.Drawing.Point(3, 3);
            this.pnUnloader37.Name = "pnUnloader37";
            this.pnUnloader37.Size = new System.Drawing.Size(514, 26);
            this.pnUnloader37.TabIndex = 33;
            this.pnUnloader37.Click += new System.EventHandler(this.pn_Unloader_37_Click);
            // 
            // lblUnloader37
            // 
            this.lblUnloader37.AutoSize = true;
            this.lblUnloader37.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblUnloader37.ForeColor = System.Drawing.Color.White;
            this.lblUnloader37.Location = new System.Drawing.Point(3, 5);
            this.lblUnloader37.Name = "lblUnloader37";
            this.lblUnloader37.Size = new System.Drawing.Size(169, 19);
            this.lblUnloader37.TabIndex = 0;
            this.lblUnloader37.Text = "37. CELL_TRANSFER_Y_1";
            this.lblUnloader37.Click += new System.EventHandler(this.pn_Unloader_37_Click);
            // 
            // pnUnloader2
            // 
            this.pnUnloader2.Controls.Add(this.lblUnloader2);
            this.pnUnloader2.Location = new System.Drawing.Point(3, 643);
            this.pnUnloader2.Name = "pnUnloader2";
            this.pnUnloader2.Size = new System.Drawing.Size(514, 26);
            this.pnUnloader2.TabIndex = 34;
            this.pnUnloader2.Click += new System.EventHandler(this.pn_Unloader_2_Click);
            // 
            // lblUnloader2
            // 
            this.lblUnloader2.AutoSize = true;
            this.lblUnloader2.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.lblUnloader2.ForeColor = System.Drawing.Color.White;
            this.lblUnloader2.Location = new System.Drawing.Point(3, 5);
            this.lblUnloader2.Name = "lblUnloader2";
            this.lblUnloader2.Size = new System.Drawing.Size(147, 19);
            this.lblUnloader2.TabIndex = 0;
            this.lblUnloader2.Text = "2. BREAK_TABLE_T_3";
            this.lblUnloader2.Click += new System.EventHandler(this.pn_Unloader_2_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.DimGray;
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Location = new System.Drawing.Point(881, 16);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(834, 745);
            this.panel3.TabIndex = 81;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.DimGray;
            this.panel4.Controls.Add(this.lbl_Unloader_AxisParameter);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.txtUnloaderPulse);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.txtUnloaderUnit);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.txtUnloaderMinVelocitu);
            this.panel4.Controls.Add(this.lbl_Unloader_AxisNo);
            this.panel4.Controls.Add(this.txtUnloaderMaxVelocitu);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.cbxtUnloaderEncoderType);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.cbxtUnloaderAlarmResetLevel);
            this.panel4.Controls.Add(this.lbl_Unloader_VelProfileMode);
            this.panel4.Controls.Add(this.cbxtUnloaderStopLevel);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.cbxtUnloaderStopMode);
            this.panel4.Controls.Add(this.lbl_Unloader_EncInput);
            this.panel4.Controls.Add(this.cbxtUnloaderZPhase);
            this.panel4.Controls.Add(this.lbl_Unloader_PulseOutput);
            this.panel4.Controls.Add(this.txtUnloaderHomeOffset);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.txtUnloaderHomeClearTime);
            this.panel4.Controls.Add(this.lbl_Unloader_MotionSignalSetting);
            this.panel4.Controls.Add(this.txtUnloaderHomeAccelation2);
            this.panel4.Controls.Add(this.lbl_Unloader_AbsRelMode);
            this.panel4.Controls.Add(this.txtUnloaderHomeAccelation1);
            this.panel4.Controls.Add(this.label15);
            this.panel4.Controls.Add(this.cbxtUnloaderHomeZPhase);
            this.panel4.Controls.Add(this.label16);
            this.panel4.Controls.Add(this.cbxtUnloaderHomeDirection);
            this.panel4.Controls.Add(this.label17);
            this.panel4.Controls.Add(this.cbxtUnloaderSWLimitEnable);
            this.panel4.Controls.Add(this.label18);
            this.panel4.Controls.Add(this.cbxtUnloaderSWLimitStopMode);
            this.panel4.Controls.Add(this.label19);
            this.panel4.Controls.Add(this.txtUnloaderInitInPosition);
            this.panel4.Controls.Add(this.label20);
            this.panel4.Controls.Add(this.txtUnloaderInitDecelation);
            this.panel4.Controls.Add(this.label21);
            this.panel4.Controls.Add(this.txtUnloaderInitAccelation);
            this.panel4.Controls.Add(this.label22);
            this.panel4.Controls.Add(this.txtUnloaderInitAccelationTime);
            this.panel4.Controls.Add(this.label23);
            this.panel4.Controls.Add(this.txtUnloaderInitPtpSpeed);
            this.panel4.Controls.Add(this.label24);
            this.panel4.Controls.Add(this.txtUnloaderInitVelocity);
            this.panel4.Controls.Add(this.label25);
            this.panel4.Controls.Add(this.txtUnloaderInitPosition);
            this.panel4.Controls.Add(this.label26);
            this.panel4.Controls.Add(this.cbxtUnloaderSWLimitMode);
            this.panel4.Controls.Add(this.label27);
            this.panel4.Controls.Add(this.txtUnloaderSWPlusLimit);
            this.panel4.Controls.Add(this.label28);
            this.panel4.Controls.Add(this.txtUnloaderSWMinusLimit);
            this.panel4.Controls.Add(this.label29);
            this.panel4.Controls.Add(this.txtUnloaderHomeVelLast);
            this.panel4.Controls.Add(this.lbl_Unloader_MinVelocitu);
            this.panel4.Controls.Add(this.txtUnloaderHomeVel3);
            this.panel4.Controls.Add(this.lbl_Unloader_MaxVelocitu);
            this.panel4.Controls.Add(this.txtUnloaderHomeVel2);
            this.panel4.Controls.Add(this.label32);
            this.panel4.Controls.Add(this.txtUnloaderHomeVel1);
            this.panel4.Controls.Add(this.label33);
            this.panel4.Controls.Add(this.cbxtUnloaderHomeLevel);
            this.panel4.Controls.Add(this.label34);
            this.panel4.Controls.Add(this.cbxtUnloaderHomeSignal);
            this.panel4.Controls.Add(this.label35);
            this.panel4.Controls.Add(this.cbxtUnloaderServoOnLevel);
            this.panel4.Controls.Add(this.label36);
            this.panel4.Controls.Add(this.cbxtUnloaderPlusEndLimit);
            this.panel4.Controls.Add(this.label37);
            this.panel4.Controls.Add(this.cbxtUnloaderMinusEndLismit);
            this.panel4.Controls.Add(this.label38);
            this.panel4.Controls.Add(this.cbxtUnloaderAlarm);
            this.panel4.Controls.Add(this.label39);
            this.panel4.Controls.Add(this.cbxtUnloaderInposition);
            this.panel4.Controls.Add(this.label40);
            this.panel4.Controls.Add(this.cbxtUnloaderAbsRelMode);
            this.panel4.Controls.Add(this.label41);
            this.panel4.Controls.Add(this.cbxtUnloaderEncInput);
            this.panel4.Controls.Add(this.label42);
            this.panel4.Controls.Add(this.cbxtUnloaderPulseOutput);
            this.panel4.Controls.Add(this.label43);
            this.panel4.Controls.Add(this.cbxtUnloaderVelProfileMode);
            this.panel4.Controls.Add(this.label44);
            this.panel4.Controls.Add(this.txtUnloaderAxisNo);
            this.panel4.Controls.Add(this.label45);
            this.panel4.Controls.Add(this.label46);
            this.panel4.Controls.Add(this.label47);
            this.panel4.Controls.Add(this.label48);
            this.panel4.Location = new System.Drawing.Point(0, -1);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(834, 755);
            this.panel4.TabIndex = 1;
            // 
            // lbl_Unloader_AxisParameter
            // 
            this.lbl_Unloader_AxisParameter.AutoSize = true;
            this.lbl_Unloader_AxisParameter.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Unloader_AxisParameter.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_AxisParameter.Location = new System.Drawing.Point(31, 15);
            this.lbl_Unloader_AxisParameter.Name = "lbl_Unloader_AxisParameter";
            this.lbl_Unloader_AxisParameter.Size = new System.Drawing.Size(125, 23);
            this.lbl_Unloader_AxisParameter.TabIndex = 0;
            this.lbl_Unloader_AxisParameter.Text = "Axis Parameter";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(31, 279);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(135, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "Servo On Level :";
            // 
            // txtUnloaderPulse
            // 
            this.txtUnloaderPulse.Font = new System.Drawing.Font("굴림", 10F);
            this.txtUnloaderPulse.Location = new System.Drawing.Point(672, 140);
            this.txtUnloaderPulse.Name = "txtUnloaderPulse";
            this.txtUnloaderPulse.Size = new System.Drawing.Size(121, 23);
            this.txtUnloaderPulse.TabIndex = 90;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(31, 327);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(118, 23);
            this.label3.TabIndex = 2;
            this.label3.Text = "Home Signal :";
            // 
            // txtUnloaderUnit
            // 
            this.txtUnloaderUnit.Font = new System.Drawing.Font("굴림", 10F);
            this.txtUnloaderUnit.Location = new System.Drawing.Point(672, 115);
            this.txtUnloaderUnit.Name = "txtUnloaderUnit";
            this.txtUnloaderUnit.Size = new System.Drawing.Size(121, 23);
            this.txtUnloaderUnit.TabIndex = 89;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(31, 375);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(124, 23);
            this.label4.TabIndex = 3;
            this.label4.Text = "Home Vel 1st :";
            // 
            // txtUnloaderMinVelocitu
            // 
            this.txtUnloaderMinVelocitu.Font = new System.Drawing.Font("굴림", 10F);
            this.txtUnloaderMinVelocitu.Location = new System.Drawing.Point(672, 90);
            this.txtUnloaderMinVelocitu.Name = "txtUnloaderMinVelocitu";
            this.txtUnloaderMinVelocitu.Size = new System.Drawing.Size(121, 23);
            this.txtUnloaderMinVelocitu.TabIndex = 88;
            // 
            // lbl_Unloader_AxisNo
            // 
            this.lbl_Unloader_AxisNo.AutoSize = true;
            this.lbl_Unloader_AxisNo.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Unloader_AxisNo.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_AxisNo.Location = new System.Drawing.Point(31, 39);
            this.lbl_Unloader_AxisNo.Name = "lbl_Unloader_AxisNo";
            this.lbl_Unloader_AxisNo.Size = new System.Drawing.Size(67, 23);
            this.lbl_Unloader_AxisNo.TabIndex = 4;
            this.lbl_Unloader_AxisNo.Text = "AxisNo.";
            // 
            // txtUnloaderMaxVelocitu
            // 
            this.txtUnloaderMaxVelocitu.Font = new System.Drawing.Font("굴림", 10F);
            this.txtUnloaderMaxVelocitu.Location = new System.Drawing.Point(672, 65);
            this.txtUnloaderMaxVelocitu.Name = "txtUnloaderMaxVelocitu";
            this.txtUnloaderMaxVelocitu.Size = new System.Drawing.Size(121, 23);
            this.txtUnloaderMaxVelocitu.TabIndex = 87;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(31, 255);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 23);
            this.label6.TabIndex = 5;
            this.label6.Text = "+ End Limit :";
            // 
            // cbxtUnloaderEncoderType
            // 
            this.cbxtUnloaderEncoderType.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtUnloaderEncoderType.FormattingEnabled = true;
            this.cbxtUnloaderEncoderType.Location = new System.Drawing.Point(672, 284);
            this.cbxtUnloaderEncoderType.Name = "cbxtUnloaderEncoderType";
            this.cbxtUnloaderEncoderType.Size = new System.Drawing.Size(121, 21);
            this.cbxtUnloaderEncoderType.TabIndex = 86;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(31, 399);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(131, 23);
            this.label7.TabIndex = 6;
            this.label7.Text = "Home Vel 2nd :";
            // 
            // cbxtUnloaderAlarmResetLevel
            // 
            this.cbxtUnloaderAlarmResetLevel.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtUnloaderAlarmResetLevel.FormattingEnabled = true;
            this.cbxtUnloaderAlarmResetLevel.Location = new System.Drawing.Point(672, 260);
            this.cbxtUnloaderAlarmResetLevel.Name = "cbxtUnloaderAlarmResetLevel";
            this.cbxtUnloaderAlarmResetLevel.Size = new System.Drawing.Size(121, 21);
            this.cbxtUnloaderAlarmResetLevel.TabIndex = 85;
            // 
            // lbl_Unloader_VelProfileMode
            // 
            this.lbl_Unloader_VelProfileMode.AutoSize = true;
            this.lbl_Unloader_VelProfileMode.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Unloader_VelProfileMode.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_VelProfileMode.Location = new System.Drawing.Point(31, 135);
            this.lbl_Unloader_VelProfileMode.Name = "lbl_Unloader_VelProfileMode";
            this.lbl_Unloader_VelProfileMode.Size = new System.Drawing.Size(149, 23);
            this.lbl_Unloader_VelProfileMode.TabIndex = 7;
            this.lbl_Unloader_VelProfileMode.Text = "Vel Profile Mode :";
            // 
            // cbxtUnloaderStopLevel
            // 
            this.cbxtUnloaderStopLevel.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtUnloaderStopLevel.FormattingEnabled = true;
            this.cbxtUnloaderStopLevel.Location = new System.Drawing.Point(672, 236);
            this.cbxtUnloaderStopLevel.Name = "cbxtUnloaderStopLevel";
            this.cbxtUnloaderStopLevel.Size = new System.Drawing.Size(121, 21);
            this.cbxtUnloaderStopLevel.TabIndex = 84;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(31, 447);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(132, 23);
            this.label9.TabIndex = 8;
            this.label9.Text = "Home Vel Last :";
            // 
            // cbxtUnloaderStopMode
            // 
            this.cbxtUnloaderStopMode.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtUnloaderStopMode.FormattingEnabled = true;
            this.cbxtUnloaderStopMode.Location = new System.Drawing.Point(672, 212);
            this.cbxtUnloaderStopMode.Name = "cbxtUnloaderStopMode";
            this.cbxtUnloaderStopMode.Size = new System.Drawing.Size(121, 21);
            this.cbxtUnloaderStopMode.TabIndex = 83;
            // 
            // lbl_Unloader_EncInput
            // 
            this.lbl_Unloader_EncInput.AutoSize = true;
            this.lbl_Unloader_EncInput.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Unloader_EncInput.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_EncInput.Location = new System.Drawing.Point(31, 87);
            this.lbl_Unloader_EncInput.Name = "lbl_Unloader_EncInput";
            this.lbl_Unloader_EncInput.Size = new System.Drawing.Size(98, 23);
            this.lbl_Unloader_EncInput.TabIndex = 9;
            this.lbl_Unloader_EncInput.Text = "Enc. Input :";
            // 
            // cbxtUnloaderZPhase
            // 
            this.cbxtUnloaderZPhase.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtUnloaderZPhase.FormattingEnabled = true;
            this.cbxtUnloaderZPhase.Location = new System.Drawing.Point(672, 188);
            this.cbxtUnloaderZPhase.Name = "cbxtUnloaderZPhase";
            this.cbxtUnloaderZPhase.Size = new System.Drawing.Size(121, 21);
            this.cbxtUnloaderZPhase.TabIndex = 82;
            // 
            // lbl_Unloader_PulseOutput
            // 
            this.lbl_Unloader_PulseOutput.AutoSize = true;
            this.lbl_Unloader_PulseOutput.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Unloader_PulseOutput.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_PulseOutput.Location = new System.Drawing.Point(31, 63);
            this.lbl_Unloader_PulseOutput.Name = "lbl_Unloader_PulseOutput";
            this.lbl_Unloader_PulseOutput.Size = new System.Drawing.Size(121, 23);
            this.lbl_Unloader_PulseOutput.TabIndex = 10;
            this.lbl_Unloader_PulseOutput.Text = "Pulse Output :";
            // 
            // txtUnloaderHomeOffset
            // 
            this.txtUnloaderHomeOffset.Font = new System.Drawing.Font("굴림", 10F);
            this.txtUnloaderHomeOffset.Location = new System.Drawing.Point(672, 453);
            this.txtUnloaderHomeOffset.Name = "txtUnloaderHomeOffset";
            this.txtUnloaderHomeOffset.Size = new System.Drawing.Size(121, 23);
            this.txtUnloaderHomeOffset.TabIndex = 81;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(31, 303);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(179, 23);
            this.label12.TabIndex = 11;
            this.label12.Text = "Home  Search Setting";
            // 
            // txtUnloaderHomeClearTime
            // 
            this.txtUnloaderHomeClearTime.Font = new System.Drawing.Font("굴림", 10F);
            this.txtUnloaderHomeClearTime.Location = new System.Drawing.Point(672, 428);
            this.txtUnloaderHomeClearTime.Name = "txtUnloaderHomeClearTime";
            this.txtUnloaderHomeClearTime.Size = new System.Drawing.Size(121, 23);
            this.txtUnloaderHomeClearTime.TabIndex = 80;
            // 
            // lbl_Unloader_MotionSignalSetting
            // 
            this.lbl_Unloader_MotionSignalSetting.AutoSize = true;
            this.lbl_Unloader_MotionSignalSetting.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Unloader_MotionSignalSetting.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_MotionSignalSetting.Location = new System.Drawing.Point(31, 159);
            this.lbl_Unloader_MotionSignalSetting.Name = "lbl_Unloader_MotionSignalSetting";
            this.lbl_Unloader_MotionSignalSetting.Size = new System.Drawing.Size(178, 23);
            this.lbl_Unloader_MotionSignalSetting.TabIndex = 12;
            this.lbl_Unloader_MotionSignalSetting.Text = "Motion Signal Setting";
            // 
            // txtUnloaderHomeAccelation2
            // 
            this.txtUnloaderHomeAccelation2.Font = new System.Drawing.Font("굴림", 10F);
            this.txtUnloaderHomeAccelation2.Location = new System.Drawing.Point(672, 403);
            this.txtUnloaderHomeAccelation2.Name = "txtUnloaderHomeAccelation2";
            this.txtUnloaderHomeAccelation2.Size = new System.Drawing.Size(121, 23);
            this.txtUnloaderHomeAccelation2.TabIndex = 79;
            // 
            // lbl_Unloader_AbsRelMode
            // 
            this.lbl_Unloader_AbsRelMode.AutoSize = true;
            this.lbl_Unloader_AbsRelMode.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Unloader_AbsRelMode.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_AbsRelMode.Location = new System.Drawing.Point(31, 111);
            this.lbl_Unloader_AbsRelMode.Name = "lbl_Unloader_AbsRelMode";
            this.lbl_Unloader_AbsRelMode.Size = new System.Drawing.Size(126, 23);
            this.lbl_Unloader_AbsRelMode.TabIndex = 13;
            this.lbl_Unloader_AbsRelMode.Text = "Abs.Rel Mode :";
            // 
            // txtUnloaderHomeAccelation1
            // 
            this.txtUnloaderHomeAccelation1.Font = new System.Drawing.Font("굴림", 10F);
            this.txtUnloaderHomeAccelation1.Location = new System.Drawing.Point(672, 378);
            this.txtUnloaderHomeAccelation1.Name = "txtUnloaderHomeAccelation1";
            this.txtUnloaderHomeAccelation1.Size = new System.Drawing.Size(121, 23);
            this.txtUnloaderHomeAccelation1.TabIndex = 78;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(31, 183);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(102, 23);
            this.label15.TabIndex = 14;
            this.label15.Text = "In Position :";
            // 
            // cbxtUnloaderHomeZPhase
            // 
            this.cbxtUnloaderHomeZPhase.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtUnloaderHomeZPhase.FormattingEnabled = true;
            this.cbxtUnloaderHomeZPhase.Location = new System.Drawing.Point(672, 355);
            this.cbxtUnloaderHomeZPhase.Name = "cbxtUnloaderHomeZPhase";
            this.cbxtUnloaderHomeZPhase.Size = new System.Drawing.Size(121, 21);
            this.cbxtUnloaderHomeZPhase.TabIndex = 77;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(31, 207);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(65, 23);
            this.label16.TabIndex = 15;
            this.label16.Text = "Alarm :";
            // 
            // cbxtUnloaderHomeDirection
            // 
            this.cbxtUnloaderHomeDirection.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtUnloaderHomeDirection.FormattingEnabled = true;
            this.cbxtUnloaderHomeDirection.Location = new System.Drawing.Point(672, 332);
            this.cbxtUnloaderHomeDirection.Name = "cbxtUnloaderHomeDirection";
            this.cbxtUnloaderHomeDirection.Size = new System.Drawing.Size(121, 21);
            this.cbxtUnloaderHomeDirection.TabIndex = 76;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(31, 351);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(110, 23);
            this.label17.TabIndex = 16;
            this.label17.Text = "Home Level :";
            // 
            // cbxtUnloaderSWLimitEnable
            // 
            this.cbxtUnloaderSWLimitEnable.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtUnloaderSWLimitEnable.FormattingEnabled = true;
            this.cbxtUnloaderSWLimitEnable.Location = new System.Drawing.Point(672, 523);
            this.cbxtUnloaderSWLimitEnable.Name = "cbxtUnloaderSWLimitEnable";
            this.cbxtUnloaderSWLimitEnable.Size = new System.Drawing.Size(121, 21);
            this.cbxtUnloaderSWLimitEnable.TabIndex = 75;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(31, 519);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(153, 23);
            this.label18.TabIndex = 17;
            this.label18.Text = "S/W +Limit [mm] :";
            // 
            // cbxtUnloaderSWLimitStopMode
            // 
            this.cbxtUnloaderSWLimitStopMode.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtUnloaderSWLimitStopMode.FormattingEnabled = true;
            this.cbxtUnloaderSWLimitStopMode.Location = new System.Drawing.Point(672, 500);
            this.cbxtUnloaderSWLimitStopMode.Name = "cbxtUnloaderSWLimitStopMode";
            this.cbxtUnloaderSWLimitStopMode.Size = new System.Drawing.Size(121, 21);
            this.cbxtUnloaderSWLimitStopMode.TabIndex = 74;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(31, 423);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(127, 23);
            this.label19.TabIndex = 18;
            this.label19.Text = "Home Vel 3rd :";
            // 
            // txtUnloaderInitInPosition
            // 
            this.txtUnloaderInitInPosition.Font = new System.Drawing.Font("굴림", 10F);
            this.txtUnloaderInitInPosition.Location = new System.Drawing.Point(672, 641);
            this.txtUnloaderInitInPosition.Name = "txtUnloaderInitInPosition";
            this.txtUnloaderInitInPosition.Size = new System.Drawing.Size(121, 23);
            this.txtUnloaderInitInPosition.TabIndex = 73;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(31, 471);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(145, 23);
            this.label20.TabIndex = 19;
            this.label20.Text = "S/W Limit Setting";
            // 
            // txtUnloaderInitDecelation
            // 
            this.txtUnloaderInitDecelation.Font = new System.Drawing.Font("굴림", 10F);
            this.txtUnloaderInitDecelation.Location = new System.Drawing.Point(672, 616);
            this.txtUnloaderInitDecelation.Name = "txtUnloaderInitDecelation";
            this.txtUnloaderInitDecelation.Size = new System.Drawing.Size(121, 23);
            this.txtUnloaderInitDecelation.TabIndex = 72;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(31, 495);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(148, 23);
            this.label21.TabIndex = 20;
            this.label21.Text = "S/W -Limit [mm] :";
            // 
            // txtUnloaderInitAccelation
            // 
            this.txtUnloaderInitAccelation.Font = new System.Drawing.Font("굴림", 10F);
            this.txtUnloaderInitAccelation.Location = new System.Drawing.Point(672, 591);
            this.txtUnloaderInitAccelation.Name = "txtUnloaderInitAccelation";
            this.txtUnloaderInitAccelation.Size = new System.Drawing.Size(121, 23);
            this.txtUnloaderInitAccelation.TabIndex = 71;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label22.ForeColor = System.Drawing.Color.White;
            this.label22.Location = new System.Drawing.Point(31, 567);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(95, 23);
            this.label22.TabIndex = 21;
            this.label22.Text = "Init Setting";
            // 
            // txtUnloaderInitAccelationTime
            // 
            this.txtUnloaderInitAccelationTime.Font = new System.Drawing.Font("굴림", 10F);
            this.txtUnloaderInitAccelationTime.Location = new System.Drawing.Point(245, 666);
            this.txtUnloaderInitAccelationTime.Name = "txtUnloaderInitAccelationTime";
            this.txtUnloaderInitAccelationTime.Size = new System.Drawing.Size(121, 23);
            this.txtUnloaderInitAccelationTime.TabIndex = 70;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label23.ForeColor = System.Drawing.Color.White;
            this.label23.Location = new System.Drawing.Point(31, 543);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(146, 23);
            this.label23.TabIndex = 22;
            this.label23.Text = "S/W Limit Mode :";
            // 
            // txtUnloaderInitPtpSpeed
            // 
            this.txtUnloaderInitPtpSpeed.Font = new System.Drawing.Font("굴림", 10F);
            this.txtUnloaderInitPtpSpeed.Location = new System.Drawing.Point(245, 641);
            this.txtUnloaderInitPtpSpeed.Name = "txtUnloaderInitPtpSpeed";
            this.txtUnloaderInitPtpSpeed.Size = new System.Drawing.Size(121, 23);
            this.txtUnloaderInitPtpSpeed.TabIndex = 69;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label24.ForeColor = System.Drawing.Color.White;
            this.label24.Location = new System.Drawing.Point(31, 231);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(105, 23);
            this.label24.TabIndex = 23;
            this.label24.Text = "- End Limit :";
            // 
            // txtUnloaderInitVelocity
            // 
            this.txtUnloaderInitVelocity.Font = new System.Drawing.Font("굴림", 10F);
            this.txtUnloaderInitVelocity.Location = new System.Drawing.Point(245, 616);
            this.txtUnloaderInitVelocity.Name = "txtUnloaderInitVelocity";
            this.txtUnloaderInitVelocity.Size = new System.Drawing.Size(121, 23);
            this.txtUnloaderInitVelocity.TabIndex = 68;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label25.ForeColor = System.Drawing.Color.White;
            this.label25.Location = new System.Drawing.Point(31, 591);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(158, 23);
            this.label25.TabIndex = 24;
            this.label25.Text = "Init Position [mm] :";
            // 
            // txtUnloaderInitPosition
            // 
            this.txtUnloaderInitPosition.Font = new System.Drawing.Font("굴림", 10F);
            this.txtUnloaderInitPosition.Location = new System.Drawing.Point(245, 591);
            this.txtUnloaderInitPosition.Name = "txtUnloaderInitPosition";
            this.txtUnloaderInitPosition.Size = new System.Drawing.Size(121, 23);
            this.txtUnloaderInitPosition.TabIndex = 67;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label26.ForeColor = System.Drawing.Color.White;
            this.label26.Location = new System.Drawing.Point(31, 615);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(188, 23);
            this.label26.TabIndex = 25;
            this.label26.Text = "Init Velocity [mm/sec] :";
            // 
            // cbxtUnloaderSWLimitMode
            // 
            this.cbxtUnloaderSWLimitMode.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtUnloaderSWLimitMode.FormattingEnabled = true;
            this.cbxtUnloaderSWLimitMode.Location = new System.Drawing.Point(245, 546);
            this.cbxtUnloaderSWLimitMode.Name = "cbxtUnloaderSWLimitMode";
            this.cbxtUnloaderSWLimitMode.Size = new System.Drawing.Size(121, 21);
            this.cbxtUnloaderSWLimitMode.TabIndex = 66;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label27.ForeColor = System.Drawing.Color.White;
            this.label27.Location = new System.Drawing.Point(31, 639);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(99, 23);
            this.label27.TabIndex = 26;
            this.label27.Text = "Ptp Speed :";
            // 
            // txtUnloaderSWPlusLimit
            // 
            this.txtUnloaderSWPlusLimit.Font = new System.Drawing.Font("굴림", 10F);
            this.txtUnloaderSWPlusLimit.Location = new System.Drawing.Point(245, 520);
            this.txtUnloaderSWPlusLimit.Name = "txtUnloaderSWPlusLimit";
            this.txtUnloaderSWPlusLimit.Size = new System.Drawing.Size(121, 23);
            this.txtUnloaderSWPlusLimit.TabIndex = 65;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label28.ForeColor = System.Drawing.Color.White;
            this.label28.Location = new System.Drawing.Point(31, 663);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(165, 23);
            this.label28.TabIndex = 27;
            this.label28.Text = "Accelation Time [s] :";
            // 
            // txtUnloaderSWMinusLimit
            // 
            this.txtUnloaderSWMinusLimit.Font = new System.Drawing.Font("굴림", 10F);
            this.txtUnloaderSWMinusLimit.Location = new System.Drawing.Point(245, 495);
            this.txtUnloaderSWMinusLimit.Name = "txtUnloaderSWMinusLimit";
            this.txtUnloaderSWMinusLimit.Size = new System.Drawing.Size(121, 23);
            this.txtUnloaderSWMinusLimit.TabIndex = 64;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label29.ForeColor = System.Drawing.Color.White;
            this.label29.Location = new System.Drawing.Point(469, 135);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(60, 23);
            this.label29.TabIndex = 28;
            this.label29.Text = "Pulse :";
            // 
            // txtUnloaderHomeVelLast
            // 
            this.txtUnloaderHomeVelLast.Font = new System.Drawing.Font("굴림", 10F);
            this.txtUnloaderHomeVelLast.Location = new System.Drawing.Point(245, 448);
            this.txtUnloaderHomeVelLast.Name = "txtUnloaderHomeVelLast";
            this.txtUnloaderHomeVelLast.Size = new System.Drawing.Size(121, 23);
            this.txtUnloaderHomeVelLast.TabIndex = 63;
            // 
            // lbl_Unloader_MinVelocitu
            // 
            this.lbl_Unloader_MinVelocitu.AutoSize = true;
            this.lbl_Unloader_MinVelocitu.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Unloader_MinVelocitu.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_MinVelocitu.Location = new System.Drawing.Point(469, 87);
            this.lbl_Unloader_MinVelocitu.Name = "lbl_Unloader_MinVelocitu";
            this.lbl_Unloader_MinVelocitu.Size = new System.Drawing.Size(195, 23);
            this.lbl_Unloader_MinVelocitu.TabIndex = 29;
            this.lbl_Unloader_MinVelocitu.Text = "Min Velocitu [mm/sec] :";
            // 
            // txtUnloaderHomeVel3
            // 
            this.txtUnloaderHomeVel3.Font = new System.Drawing.Font("굴림", 10F);
            this.txtUnloaderHomeVel3.Location = new System.Drawing.Point(245, 423);
            this.txtUnloaderHomeVel3.Name = "txtUnloaderHomeVel3";
            this.txtUnloaderHomeVel3.Size = new System.Drawing.Size(121, 23);
            this.txtUnloaderHomeVel3.TabIndex = 62;
            // 
            // lbl_Unloader_MaxVelocitu
            // 
            this.lbl_Unloader_MaxVelocitu.AutoSize = true;
            this.lbl_Unloader_MaxVelocitu.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Unloader_MaxVelocitu.ForeColor = System.Drawing.Color.White;
            this.lbl_Unloader_MaxVelocitu.Location = new System.Drawing.Point(469, 63);
            this.lbl_Unloader_MaxVelocitu.Name = "lbl_Unloader_MaxVelocitu";
            this.lbl_Unloader_MaxVelocitu.Size = new System.Drawing.Size(198, 23);
            this.lbl_Unloader_MaxVelocitu.TabIndex = 30;
            this.lbl_Unloader_MaxVelocitu.Text = "Max Velocitu [mm/sec] :";
            // 
            // txtUnloaderHomeVel2
            // 
            this.txtUnloaderHomeVel2.Font = new System.Drawing.Font("굴림", 10F);
            this.txtUnloaderHomeVel2.Location = new System.Drawing.Point(245, 398);
            this.txtUnloaderHomeVel2.Name = "txtUnloaderHomeVel2";
            this.txtUnloaderHomeVel2.Size = new System.Drawing.Size(121, 23);
            this.txtUnloaderHomeVel2.TabIndex = 61;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label32.ForeColor = System.Drawing.Color.White;
            this.label32.Location = new System.Drawing.Point(469, 111);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(52, 23);
            this.label32.TabIndex = 31;
            this.label32.Text = "Unit :";
            // 
            // txtUnloaderHomeVel1
            // 
            this.txtUnloaderHomeVel1.Font = new System.Drawing.Font("굴림", 10F);
            this.txtUnloaderHomeVel1.Location = new System.Drawing.Point(245, 373);
            this.txtUnloaderHomeVel1.Name = "txtUnloaderHomeVel1";
            this.txtUnloaderHomeVel1.Size = new System.Drawing.Size(121, 23);
            this.txtUnloaderHomeVel1.TabIndex = 60;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label33.ForeColor = System.Drawing.Color.White;
            this.label33.Location = new System.Drawing.Point(469, 279);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(124, 23);
            this.label33.TabIndex = 32;
            this.label33.Text = "Encoder Type :";
            // 
            // cbxtUnloaderHomeLevel
            // 
            this.cbxtUnloaderHomeLevel.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtUnloaderHomeLevel.FormattingEnabled = true;
            this.cbxtUnloaderHomeLevel.Location = new System.Drawing.Point(245, 350);
            this.cbxtUnloaderHomeLevel.Name = "cbxtUnloaderHomeLevel";
            this.cbxtUnloaderHomeLevel.Size = new System.Drawing.Size(121, 21);
            this.cbxtUnloaderHomeLevel.TabIndex = 59;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label34.ForeColor = System.Drawing.Color.White;
            this.label34.Location = new System.Drawing.Point(469, 255);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(156, 23);
            this.label34.TabIndex = 33;
            this.label34.Text = "Alarm Reset Level :";
            // 
            // cbxtUnloaderHomeSignal
            // 
            this.cbxtUnloaderHomeSignal.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtUnloaderHomeSignal.FormattingEnabled = true;
            this.cbxtUnloaderHomeSignal.Location = new System.Drawing.Point(245, 327);
            this.cbxtUnloaderHomeSignal.Name = "cbxtUnloaderHomeSignal";
            this.cbxtUnloaderHomeSignal.Size = new System.Drawing.Size(121, 21);
            this.cbxtUnloaderHomeSignal.TabIndex = 58;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label35.ForeColor = System.Drawing.Color.White;
            this.label35.Location = new System.Drawing.Point(469, 183);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(82, 23);
            this.label35.TabIndex = 34;
            this.label35.Text = "Z-Phase :";
            // 
            // cbxtUnloaderServoOnLevel
            // 
            this.cbxtUnloaderServoOnLevel.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtUnloaderServoOnLevel.FormattingEnabled = true;
            this.cbxtUnloaderServoOnLevel.Location = new System.Drawing.Point(245, 279);
            this.cbxtUnloaderServoOnLevel.Name = "cbxtUnloaderServoOnLevel";
            this.cbxtUnloaderServoOnLevel.Size = new System.Drawing.Size(121, 21);
            this.cbxtUnloaderServoOnLevel.TabIndex = 57;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label36.ForeColor = System.Drawing.Color.White;
            this.label36.Location = new System.Drawing.Point(469, 207);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(106, 23);
            this.label36.TabIndex = 35;
            this.label36.Text = "Stop Mode :";
            // 
            // cbxtUnloaderPlusEndLimit
            // 
            this.cbxtUnloaderPlusEndLimit.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtUnloaderPlusEndLimit.FormattingEnabled = true;
            this.cbxtUnloaderPlusEndLimit.Location = new System.Drawing.Point(245, 255);
            this.cbxtUnloaderPlusEndLimit.Name = "cbxtUnloaderPlusEndLimit";
            this.cbxtUnloaderPlusEndLimit.Size = new System.Drawing.Size(121, 21);
            this.cbxtUnloaderPlusEndLimit.TabIndex = 56;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label37.ForeColor = System.Drawing.Color.White;
            this.label37.Location = new System.Drawing.Point(469, 231);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(99, 23);
            this.label37.TabIndex = 36;
            this.label37.Text = "Stop Level :";
            // 
            // cbxtUnloaderMinusEndLismit
            // 
            this.cbxtUnloaderMinusEndLismit.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtUnloaderMinusEndLismit.FormattingEnabled = true;
            this.cbxtUnloaderMinusEndLismit.Location = new System.Drawing.Point(245, 231);
            this.cbxtUnloaderMinusEndLismit.Name = "cbxtUnloaderMinusEndLismit";
            this.cbxtUnloaderMinusEndLismit.Size = new System.Drawing.Size(121, 21);
            this.cbxtUnloaderMinusEndLismit.TabIndex = 55;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label38.ForeColor = System.Drawing.Color.White;
            this.label38.Location = new System.Drawing.Point(469, 327);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(141, 23);
            this.label38.TabIndex = 37;
            this.label38.Text = "Home Direction :";
            // 
            // cbxtUnloaderAlarm
            // 
            this.cbxtUnloaderAlarm.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtUnloaderAlarm.FormattingEnabled = true;
            this.cbxtUnloaderAlarm.Location = new System.Drawing.Point(245, 207);
            this.cbxtUnloaderAlarm.Name = "cbxtUnloaderAlarm";
            this.cbxtUnloaderAlarm.Size = new System.Drawing.Size(121, 21);
            this.cbxtUnloaderAlarm.TabIndex = 54;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label39.ForeColor = System.Drawing.Color.White;
            this.label39.Location = new System.Drawing.Point(469, 375);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(179, 23);
            this.label39.TabIndex = 38;
            this.label39.Text = "Home Accelation 1st :";
            // 
            // cbxtUnloaderInposition
            // 
            this.cbxtUnloaderInposition.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtUnloaderInposition.FormattingEnabled = true;
            this.cbxtUnloaderInposition.Location = new System.Drawing.Point(245, 183);
            this.cbxtUnloaderInposition.Name = "cbxtUnloaderInposition";
            this.cbxtUnloaderInposition.Size = new System.Drawing.Size(121, 21);
            this.cbxtUnloaderInposition.TabIndex = 53;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label40.ForeColor = System.Drawing.Color.White;
            this.label40.Location = new System.Drawing.Point(469, 399);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(186, 23);
            this.label40.TabIndex = 39;
            this.label40.Text = "Home Accelation 2nd :";
            // 
            // cbxtUnloaderAbsRelMode
            // 
            this.cbxtUnloaderAbsRelMode.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtUnloaderAbsRelMode.FormattingEnabled = true;
            this.cbxtUnloaderAbsRelMode.Location = new System.Drawing.Point(245, 115);
            this.cbxtUnloaderAbsRelMode.Name = "cbxtUnloaderAbsRelMode";
            this.cbxtUnloaderAbsRelMode.Size = new System.Drawing.Size(121, 21);
            this.cbxtUnloaderAbsRelMode.TabIndex = 52;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label41.ForeColor = System.Drawing.Color.White;
            this.label41.Location = new System.Drawing.Point(469, 447);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(117, 23);
            this.label41.TabIndex = 40;
            this.label41.Text = "Home Offset :";
            // 
            // cbxtUnloaderEncInput
            // 
            this.cbxtUnloaderEncInput.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtUnloaderEncInput.FormattingEnabled = true;
            this.cbxtUnloaderEncInput.Location = new System.Drawing.Point(245, 91);
            this.cbxtUnloaderEncInput.Name = "cbxtUnloaderEncInput";
            this.cbxtUnloaderEncInput.Size = new System.Drawing.Size(121, 21);
            this.cbxtUnloaderEncInput.TabIndex = 51;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label42.ForeColor = System.Drawing.Color.White;
            this.label42.Location = new System.Drawing.Point(469, 351);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(134, 23);
            this.label42.TabIndex = 41;
            this.label42.Text = "Home Z_Phase :";
            // 
            // cbxtUnloaderPulseOutput
            // 
            this.cbxtUnloaderPulseOutput.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtUnloaderPulseOutput.FormattingEnabled = true;
            this.cbxtUnloaderPulseOutput.Location = new System.Drawing.Point(245, 68);
            this.cbxtUnloaderPulseOutput.Name = "cbxtUnloaderPulseOutput";
            this.cbxtUnloaderPulseOutput.Size = new System.Drawing.Size(121, 21);
            this.cbxtUnloaderPulseOutput.TabIndex = 50;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label43.ForeColor = System.Drawing.Color.White;
            this.label43.Location = new System.Drawing.Point(469, 423);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(154, 23);
            this.label43.TabIndex = 42;
            this.label43.Text = "Home Clear Time :";
            // 
            // cbxtUnloaderVelProfileMode
            // 
            this.cbxtUnloaderVelProfileMode.Font = new System.Drawing.Font("굴림", 10F);
            this.cbxtUnloaderVelProfileMode.FormattingEnabled = true;
            this.cbxtUnloaderVelProfileMode.Location = new System.Drawing.Point(245, 140);
            this.cbxtUnloaderVelProfileMode.Name = "cbxtUnloaderVelProfileMode";
            this.cbxtUnloaderVelProfileMode.Size = new System.Drawing.Size(121, 21);
            this.cbxtUnloaderVelProfileMode.TabIndex = 49;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label44.ForeColor = System.Drawing.Color.White;
            this.label44.Location = new System.Drawing.Point(469, 519);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(152, 23);
            this.label44.TabIndex = 43;
            this.label44.Text = "S/W Limit Enable :";
            // 
            // txtUnloaderAxisNo
            // 
            this.txtUnloaderAxisNo.Font = new System.Drawing.Font("굴림", 10F);
            this.txtUnloaderAxisNo.Location = new System.Drawing.Point(245, 43);
            this.txtUnloaderAxisNo.Name = "txtUnloaderAxisNo";
            this.txtUnloaderAxisNo.Size = new System.Drawing.Size(121, 23);
            this.txtUnloaderAxisNo.TabIndex = 48;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label45.ForeColor = System.Drawing.Color.White;
            this.label45.Location = new System.Drawing.Point(469, 495);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(187, 23);
            this.label45.TabIndex = 44;
            this.label45.Text = "S/W Limit Stop Mode :";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label46.ForeColor = System.Drawing.Color.White;
            this.label46.Location = new System.Drawing.Point(469, 639);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(138, 23);
            this.label46.TabIndex = 47;
            this.label46.Text = "In Position [mm]";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label47.ForeColor = System.Drawing.Color.White;
            this.label47.Location = new System.Drawing.Point(469, 591);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(130, 23);
            this.label47.TabIndex = 45;
            this.label47.Text = "Init Accelation :";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label48.ForeColor = System.Drawing.Color.White;
            this.label48.Location = new System.Drawing.Point(469, 615);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(132, 23);
            this.label48.TabIndex = 46;
            this.label48.Text = "Init Decelation :";
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.DimGray;
            this.btnSave.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(1642, 834);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(80, 30);
            this.btnSave.TabIndex = 31;
            this.btnSave.Text = "저장";
            this.btnSave.UseVisualStyleBackColor = false;
            // 
            // ParameterAxis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.tc_iostatus_info);
            this.Name = "ParameterAxis";
            this.Size = new System.Drawing.Size(1740, 875);
            this.tc_iostatus_info.ResumeLayout(false);
            this.tp_iostatus_ld.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.pnLoader15.ResumeLayout(false);
            this.pnLoader15.PerformLayout();
            this.pnLoader14.ResumeLayout(false);
            this.pnLoader14.PerformLayout();
            this.pnLoader13.ResumeLayout(false);
            this.pnLoader13.PerformLayout();
            this.pnLoader12.ResumeLayout(false);
            this.pnLoader12.PerformLayout();
            this.pnLoader11.ResumeLayout(false);
            this.pnLoader11.PerformLayout();
            this.pnLoader10.ResumeLayout(false);
            this.pnLoader10.PerformLayout();
            this.pnLoader09.ResumeLayout(false);
            this.pnLoader09.PerformLayout();
            this.pnLoader08.ResumeLayout(false);
            this.pnLoader08.PerformLayout();
            this.pnLoader07.ResumeLayout(false);
            this.pnLoader07.PerformLayout();
            this.pnLoader06.ResumeLayout(false);
            this.pnLoader06.PerformLayout();
            this.pnLoader05.ResumeLayout(false);
            this.pnLoader05.PerformLayout();
            this.pnLoader04.ResumeLayout(false);
            this.pnLoader04.PerformLayout();
            this.pnLoader36.ResumeLayout(false);
            this.pnLoader36.PerformLayout();
            this.pnLoader35.ResumeLayout(false);
            this.pnLoader35.PerformLayout();
            this.pnLoader33.ResumeLayout(false);
            this.pnLoader33.PerformLayout();
            this.pnLoader32.ResumeLayout(false);
            this.pnLoader32.PerformLayout();
            this.pnLoader31.ResumeLayout(false);
            this.pnLoader31.PerformLayout();
            this.tp_iostatus_process.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.pnProcess21.ResumeLayout(false);
            this.pnProcess21.PerformLayout();
            this.pnProcess19.ResumeLayout(false);
            this.pnProcess19.PerformLayout();
            this.pnProcess18.ResumeLayout(false);
            this.pnProcess18.PerformLayout();
            this.pnProcess14.ResumeLayout(false);
            this.pnProcess14.PerformLayout();
            this.pnProcess13.ResumeLayout(false);
            this.pnProcess13.PerformLayout();
            this.pnProcess12.ResumeLayout(false);
            this.pnProcess12.PerformLayout();
            this.pnProcess11.ResumeLayout(false);
            this.pnProcess11.PerformLayout();
            this.pnProcess10.ResumeLayout(false);
            this.pnProcess10.PerformLayout();
            this.pnProcess09.ResumeLayout(false);
            this.pnProcess09.PerformLayout();
            this.pnProcess22.ResumeLayout(false);
            this.pnProcess22.PerformLayout();
            this.pnProcess07.ResumeLayout(false);
            this.pnProcess07.PerformLayout();
            this.pnProcess06.ResumeLayout(false);
            this.pnProcess06.PerformLayout();
            this.pnProcess05.ResumeLayout(false);
            this.pnProcess05.PerformLayout();
            this.pnProcess04.ResumeLayout(false);
            this.pnProcess04.PerformLayout();
            this.pnProcess17.ResumeLayout(false);
            this.pnProcess17.PerformLayout();
            this.pnProcess03.ResumeLayout(false);
            this.pnProcess03.PerformLayout();
            this.pnProcess02.ResumeLayout(false);
            this.pnProcess02.PerformLayout();
            this.pnProcess01.ResumeLayout(false);
            this.pnProcess01.PerformLayout();
            this.pnProcess20.ResumeLayout(false);
            this.pnProcess20.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tp_iostatus_uld.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.pnUnloader3.ResumeLayout(false);
            this.pnUnloader3.PerformLayout();
            this.pnUnloader0.ResumeLayout(false);
            this.pnUnloader0.PerformLayout();
            this.pnUnloader30.ResumeLayout(false);
            this.pnUnloader30.PerformLayout();
            this.pnUnloader29.ResumeLayout(false);
            this.pnUnloader29.PerformLayout();
            this.pnUnloader28.ResumeLayout(false);
            this.pnUnloader28.PerformLayout();
            this.pnUnloader27.ResumeLayout(false);
            this.pnUnloader27.PerformLayout();
            this.pnUnloader26.ResumeLayout(false);
            this.pnUnloader26.PerformLayout();
            this.pnUnloader24.ResumeLayout(false);
            this.pnUnloader24.PerformLayout();
            this.pnUnloader23.ResumeLayout(false);
            this.pnUnloader23.PerformLayout();
            this.pnUnloader21.ResumeLayout(false);
            this.pnUnloader21.PerformLayout();
            this.pnUnloader20.ResumeLayout(false);
            this.pnUnloader20.PerformLayout();
            this.pnUnloader19.ResumeLayout(false);
            this.pnUnloader19.PerformLayout();
            this.pnUnloader18.ResumeLayout(false);
            this.pnUnloader18.PerformLayout();
            this.pnUnloader17.ResumeLayout(false);
            this.pnUnloader17.PerformLayout();
            this.pnUnloader16.ResumeLayout(false);
            this.pnUnloader16.PerformLayout();
            this.pnUnloader41.ResumeLayout(false);
            this.pnUnloader41.PerformLayout();
            this.pnUnloader40.ResumeLayout(false);
            this.pnUnloader40.PerformLayout();
            this.pnUnloader39.ResumeLayout(false);
            this.pnUnloader39.PerformLayout();
            this.pnUnloader38.ResumeLayout(false);
            this.pnUnloader38.PerformLayout();
            this.pnUnloader1.ResumeLayout(false);
            this.pnUnloader1.PerformLayout();
            this.pnUnloader37.ResumeLayout(false);
            this.pnUnloader37.PerformLayout();
            this.pnUnloader2.ResumeLayout(false);
            this.pnUnloader2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tc_iostatus_info;
        private System.Windows.Forms.TabPage tp_iostatus_ld;
        private System.Windows.Forms.TabPage tp_iostatus_process;
        private System.Windows.Forms.TabPage tp_iostatus_uld;
        private System.Windows.Forms.TextBox txtLoaderPulse;
        private System.Windows.Forms.TextBox txtLoaderUnit;
        private System.Windows.Forms.TextBox txtLoaderMinVelocitu;
        private System.Windows.Forms.TextBox txtLoaderMaxVelocitu;
        private System.Windows.Forms.ComboBox cbxtLoaderEncoderType;
        private System.Windows.Forms.ComboBox cbxtLoaderAlarmResetLevel;
        private System.Windows.Forms.ComboBox cbxtLoaderStopLevel;
        private System.Windows.Forms.ComboBox cbxtLoaderStopMode;
        private System.Windows.Forms.ComboBox cbxtLoaderZPhase;
        private System.Windows.Forms.TextBox txtLoaderHomeOffset;
        private System.Windows.Forms.TextBox txtLoaderHomeClearTime;
        private System.Windows.Forms.TextBox txtLoaderHomeAccelation2;
        private System.Windows.Forms.TextBox txtLoaderHomeAccelation1;
        private System.Windows.Forms.ComboBox cbxtLoaderHomeZPhase;
        private System.Windows.Forms.ComboBox cbxtLoaderHomeDirection;
        private System.Windows.Forms.ComboBox cbxtLoaderSWLimitEnable;
        private System.Windows.Forms.ComboBox cbxtLoaderSWLimitStopMode;
        private System.Windows.Forms.TextBox txtLoaderInitInposition;
        private System.Windows.Forms.TextBox txtLoaderInitDecelation;
        private System.Windows.Forms.TextBox txtLoaderInitAccelation;
        private System.Windows.Forms.TextBox txtLoaderInitAccelationTime;
        private System.Windows.Forms.TextBox txtLoaderInitPtpSpeed;
        private System.Windows.Forms.TextBox txtLoaderInitVelocity;
        private System.Windows.Forms.TextBox txtLoaderInitPosition;
        private System.Windows.Forms.ComboBox cbxtLoaderSWLimitMode;
        private System.Windows.Forms.TextBox txtLoaderSWPlusLimit;
        private System.Windows.Forms.TextBox txtLoaderSWMinusLimit;
        private System.Windows.Forms.TextBox txtLoaderHomeVelLast;
        private System.Windows.Forms.TextBox txtLoaderHomeVel3;
        private System.Windows.Forms.TextBox txtLoaderHomeVel2;
        private System.Windows.Forms.TextBox txtLoaderHomeVel1;
        private System.Windows.Forms.ComboBox cbxtLoaderHomeLevel;
        private System.Windows.Forms.ComboBox cbxtLoaderHomeSignal;
        private System.Windows.Forms.ComboBox cbxtLoaderServoOnLevel;
        private System.Windows.Forms.ComboBox cbxtLoaderPlusEndLimit;
        private System.Windows.Forms.ComboBox cbxtLoaderMinusEndLimit;
        private System.Windows.Forms.ComboBox cbxtLoaderAlarm;
        private System.Windows.Forms.ComboBox cbxtLoaderInposition;
        private System.Windows.Forms.ComboBox cbxtLoaderAbsRelMode;
        private System.Windows.Forms.ComboBox cbxtLoaderEncInput;
        private System.Windows.Forms.ComboBox cbxtLoaderPulseOutput;
        private System.Windows.Forms.ComboBox cbxtLoaderVelProfileMode;
        private System.Windows.Forms.TextBox txtLoaderAxisNo;
        private System.Windows.Forms.Label lbl_Loader_Init_InPosition;
        private System.Windows.Forms.Label lbl_Loader_Init_Decelation;
        private System.Windows.Forms.Label lbl_Loader_Init_Accelation;
        private System.Windows.Forms.Label lbl_Loader_SW_LimitStopMode;
        private System.Windows.Forms.Label lbl_Loader_SW_LimitEnable;
        private System.Windows.Forms.Label lbl_Loader_Home_ClearTime;
        private System.Windows.Forms.Label lbl_Loader_Home_ZPhase;
        private System.Windows.Forms.Label lbl_Loader_Home_Offset;
        private System.Windows.Forms.Label lbl_Loader_Home_Accelation2;
        private System.Windows.Forms.Label lbl_Loader_Home_Accelation1;
        private System.Windows.Forms.Label lbl_Loader_Home_Direction;
        private System.Windows.Forms.Label lbl_Loader_StopLevel;
        private System.Windows.Forms.Label lbl_Loader_StopMode;
        private System.Windows.Forms.Label lbl_Loader_ZPhase;
        private System.Windows.Forms.Label lbl_Loader_AlarmResetLevel;
        private System.Windows.Forms.Label lbl_Loader_EncoderType;
        private System.Windows.Forms.Label lbl_Loader_Unit;
        private System.Windows.Forms.Label lbl_Loader_MaxVelocitu;
        private System.Windows.Forms.Label lbl_Loader_MinVelocitu;
        private System.Windows.Forms.Label lbl_Loader_Pulse;
        private System.Windows.Forms.Label lbl_Loader_Init_AccelationTime;
        private System.Windows.Forms.Label lbl_Loader_Init_PtpSpeed;
        private System.Windows.Forms.Label lbl_Loader_Init_Velocity;
        private System.Windows.Forms.Label lbl_Loader_Init_Position;
        private System.Windows.Forms.Label lbl_Loader_MinusEndLimit;
        private System.Windows.Forms.Label lbl_Loader_SW_LimitMode;
        private System.Windows.Forms.Label lbl_Loader_Init;
        private System.Windows.Forms.Label lbl_Loader_SW_MinusLimit;
        private System.Windows.Forms.Label lbl_Loader_SW;
        private System.Windows.Forms.Label lbl_Loader_Home_Vel3;
        private System.Windows.Forms.Label lbl_Loader_SW_PlusLimit;
        private System.Windows.Forms.Label lbl_Loader_Home_Level;
        private System.Windows.Forms.Label lbl_Loader_Alarm;
        private System.Windows.Forms.Label lbl_Loader_InPosition;
        private System.Windows.Forms.Label lbl_Loader_AbsRelMode;
        private System.Windows.Forms.Label lbl_Loader_MotionSignalSetting;
        private System.Windows.Forms.Label lbl_Loader_Home;
        private System.Windows.Forms.Label lbl_Loader_PulseOutput;
        private System.Windows.Forms.Label lbl_Loader_EncInput;
        private System.Windows.Forms.Label lbl_Loader_Home_VelLast;
        private System.Windows.Forms.Label lbl_Loader_VelProfileMode;
        private System.Windows.Forms.Label lbl_Loader_Home_Vel2;
        private System.Windows.Forms.Label lbl_Loader_PlusEndLimit;
        private System.Windows.Forms.Label lbl_Loader_AxisNo;
        private System.Windows.Forms.Label lbl_Loader_Home_Vel1;
        private System.Windows.Forms.Label lbl_Loader_Home_Signal;
        private System.Windows.Forms.Label lbl_Loader_ServoOnLevel;
        private System.Windows.Forms.Label lbl_Loader_AxisParameter;
        private System.Windows.Forms.TextBox txtProcessSWLimitHigh;
        private System.Windows.Forms.TextBox txtProcessSWLimitLow;
        private System.Windows.Forms.TextBox txtProcessPtpSpeed;
        private System.Windows.Forms.TextBox txtProcessStepSpeed;
        private System.Windows.Forms.TextBox txtProcessJogSpeed;
        private System.Windows.Forms.TextBox txtProcessHomeAcceleration;
        private System.Windows.Forms.TextBox txtProcessHome2Velocity;
        private System.Windows.Forms.TextBox txtProcessHome1Velocity;
        private System.Windows.Forms.TextBox txtProcessDefaultAcceleration;
        private System.Windows.Forms.TextBox txtProcessDefauleVelocity;
        private System.Windows.Forms.TextBox txtProcessInPosition;
        private System.Windows.Forms.TextBox txtProcessMoveTimeOut;
        private System.Windows.Forms.TextBox txtProcessSpeedRate;
        private System.Windows.Forms.TextBox txtProcessPositionRate;
        private System.Windows.Forms.TextBox txtProcessAxisNo;
        private System.Windows.Forms.Label lbl_Process_Home2Velocity;
        private System.Windows.Forms.Label lbl_Process_SWLimitLow;
        private System.Windows.Forms.Label lbl_Process_Home1Velocity;
        private System.Windows.Forms.Label lbl_Process_DefaultAcceleration;
        private System.Windows.Forms.Label lbl_Process_MoveTimeOut;
        private System.Windows.Forms.Label lbl_Process_DefauleVelocity;
        private System.Windows.Forms.Label lbl_Process_StepSpeed;
        private System.Windows.Forms.Label lbl_Process_PositionRate;
        private System.Windows.Forms.Label lbl_Process_SpeedRate;
        private System.Windows.Forms.Label lbl_Process_InPosition;
        private System.Windows.Forms.Label lbl_Process_AccelationTime;
        private System.Windows.Forms.Label lbl_Process_HomeAcceleration;
        private System.Windows.Forms.Label lbl_Process_AxisNo;
        private System.Windows.Forms.Label lbl_Process_SWLimitHigh;
        private System.Windows.Forms.Label lbl_Process_PtpSpeed;
        private System.Windows.Forms.Label lbl_Process_JogSpeed;
        private System.Windows.Forms.Label lbl_Process_AxisParameter;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtProcessAccelationTime;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnLoader31;
        private System.Windows.Forms.Label lblLoader31;
        private System.Windows.Forms.Panel pnLoader15;
        private System.Windows.Forms.Label lblLoader15;
        private System.Windows.Forms.Panel pnLoader14;
        private System.Windows.Forms.Label lblLoader14;
        private System.Windows.Forms.Panel pnLoader13;
        private System.Windows.Forms.Label lblLoader13;
        private System.Windows.Forms.Panel pnLoader12;
        private System.Windows.Forms.Label lblLoader12;
        private System.Windows.Forms.Panel pnLoader11;
        private System.Windows.Forms.Label lblLoader11;
        private System.Windows.Forms.Panel pnLoader10;
        private System.Windows.Forms.Label lblLoader10;
        private System.Windows.Forms.Panel pnLoader09;
        private System.Windows.Forms.Label lblLoader09;
        private System.Windows.Forms.Panel pnLoader08;
        private System.Windows.Forms.Label lblLoader08;
        private System.Windows.Forms.Panel pnLoader07;
        private System.Windows.Forms.Label lblLoader07;
        private System.Windows.Forms.Panel pnLoader06;
        private System.Windows.Forms.Label lblLoader06;
        private System.Windows.Forms.Panel pnLoader05;
        private System.Windows.Forms.Label lblLoader05;
        private System.Windows.Forms.Panel pnLoader04;
        private System.Windows.Forms.Label lblLoader04;
        private System.Windows.Forms.Panel pnLoader36;
        private System.Windows.Forms.Label lblLoader36;
        private System.Windows.Forms.Panel pnLoader35;
        private System.Windows.Forms.Label lblLoader35;
        private System.Windows.Forms.Panel pnLoader33;
        private System.Windows.Forms.Label lblLoader33;
        private System.Windows.Forms.Panel pnLoader32;
        private System.Windows.Forms.Label lblLoader32;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel pnProcess21;
        private System.Windows.Forms.Label lblProcess21;
        private System.Windows.Forms.Panel pnProcess19;
        private System.Windows.Forms.Label lblProcess19;
        private System.Windows.Forms.Panel pnProcess18;
        private System.Windows.Forms.Label lblProcess18;
        private System.Windows.Forms.Panel pnProcess14;
        private System.Windows.Forms.Label lblProcess14;
        private System.Windows.Forms.Panel pnProcess13;
        private System.Windows.Forms.Label lblProcess13;
        private System.Windows.Forms.Panel pnProcess12;
        private System.Windows.Forms.Label lblProcess12;
        private System.Windows.Forms.Panel pnProcess11;
        private System.Windows.Forms.Label lblProcess11;
        private System.Windows.Forms.Panel pnProcess10;
        private System.Windows.Forms.Label lblProcess10;
        private System.Windows.Forms.Panel pnProcess09;
        private System.Windows.Forms.Label lblProcess09;
        private System.Windows.Forms.Panel pnProcess22;
        private System.Windows.Forms.Label lblProcess22;
        private System.Windows.Forms.Panel pnProcess07;
        private System.Windows.Forms.Label lblProcess07;
        private System.Windows.Forms.Panel pnProcess06;
        private System.Windows.Forms.Label lblProcess06;
        private System.Windows.Forms.Panel pnProcess05;
        private System.Windows.Forms.Label lblProcess05;
        private System.Windows.Forms.Panel pnProcess04;
        private System.Windows.Forms.Label lblProcess04;
        private System.Windows.Forms.Panel pnProcess17;
        private System.Windows.Forms.Label lblProcess17;
        private System.Windows.Forms.Panel pnProcess03;
        private System.Windows.Forms.Label lblProcess03;
        private System.Windows.Forms.Panel pnProcess02;
        private System.Windows.Forms.Label lblProcess02;
        private System.Windows.Forms.Panel pnProcess01;
        private System.Windows.Forms.Label lblProcess01;
        private System.Windows.Forms.Panel pnProcess20;
        private System.Windows.Forms.Label lblProcess20;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Panel pnUnloader3;
        private System.Windows.Forms.Label lblUnloader3;
        private System.Windows.Forms.Panel pnUnloader0;
        private System.Windows.Forms.Label lblUnloader0;
        private System.Windows.Forms.Panel pnUnloader30;
        private System.Windows.Forms.Label lblUnloader30;
        private System.Windows.Forms.Panel pnUnloader29;
        private System.Windows.Forms.Label lblUnloader29;
        private System.Windows.Forms.Panel pnUnloader28;
        private System.Windows.Forms.Label lblUnloader28;
        private System.Windows.Forms.Panel pnUnloader27;
        private System.Windows.Forms.Label lblUnloader27;
        private System.Windows.Forms.Panel pnUnloader26;
        private System.Windows.Forms.Label lblUnloader26;
        private System.Windows.Forms.Panel pnUnloader24;
        private System.Windows.Forms.Label lblUnloader24;
        private System.Windows.Forms.Panel pnUnloader23;
        private System.Windows.Forms.Label lblUnloader23;
        private System.Windows.Forms.Panel pnUnloader21;
        private System.Windows.Forms.Label lblUnloader21;
        private System.Windows.Forms.Panel pnUnloader20;
        private System.Windows.Forms.Label lblUnloader20;
        private System.Windows.Forms.Panel pnUnloader19;
        private System.Windows.Forms.Label lblUnloader19;
        private System.Windows.Forms.Panel pnUnloader18;
        private System.Windows.Forms.Label lblUnloader18;
        private System.Windows.Forms.Panel pnUnloader17;
        private System.Windows.Forms.Label lblUnloader17;
        private System.Windows.Forms.Panel pnUnloader16;
        private System.Windows.Forms.Label lblUnloader16;
        private System.Windows.Forms.Panel pnUnloader41;
        private System.Windows.Forms.Label lblUnloader41;
        private System.Windows.Forms.Panel pnUnloader40;
        private System.Windows.Forms.Label lblUnloader40;
        private System.Windows.Forms.Panel pnUnloader39;
        private System.Windows.Forms.Label lblUnloader39;
        private System.Windows.Forms.Panel pnUnloader38;
        private System.Windows.Forms.Label lblUnloader38;
        private System.Windows.Forms.Panel pnUnloader1;
        private System.Windows.Forms.Label lblUnloader1;
        private System.Windows.Forms.Panel pnUnloader37;
        private System.Windows.Forms.Label lblUnloader37;
        private System.Windows.Forms.Panel pnUnloader2;
        private System.Windows.Forms.Label lblUnloader2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lbl_Unloader_AxisParameter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtUnloaderPulse;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtUnloaderUnit;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtUnloaderMinVelocitu;
        private System.Windows.Forms.Label lbl_Unloader_AxisNo;
        private System.Windows.Forms.TextBox txtUnloaderMaxVelocitu;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbxtUnloaderEncoderType;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbxtUnloaderAlarmResetLevel;
        private System.Windows.Forms.Label lbl_Unloader_VelProfileMode;
        private System.Windows.Forms.ComboBox cbxtUnloaderStopLevel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbxtUnloaderStopMode;
        private System.Windows.Forms.Label lbl_Unloader_EncInput;
        private System.Windows.Forms.ComboBox cbxtUnloaderZPhase;
        private System.Windows.Forms.Label lbl_Unloader_PulseOutput;
        private System.Windows.Forms.TextBox txtUnloaderHomeOffset;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtUnloaderHomeClearTime;
        private System.Windows.Forms.Label lbl_Unloader_MotionSignalSetting;
        private System.Windows.Forms.TextBox txtUnloaderHomeAccelation2;
        private System.Windows.Forms.Label lbl_Unloader_AbsRelMode;
        private System.Windows.Forms.TextBox txtUnloaderHomeAccelation1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cbxtUnloaderHomeZPhase;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cbxtUnloaderHomeDirection;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox cbxtUnloaderSWLimitEnable;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox cbxtUnloaderSWLimitStopMode;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtUnloaderInitInPosition;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtUnloaderInitDecelation;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtUnloaderInitAccelation;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtUnloaderInitAccelationTime;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtUnloaderInitPtpSpeed;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtUnloaderInitVelocity;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtUnloaderInitPosition;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox cbxtUnloaderSWLimitMode;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtUnloaderSWPlusLimit;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtUnloaderSWMinusLimit;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtUnloaderHomeVelLast;
        private System.Windows.Forms.Label lbl_Unloader_MinVelocitu;
        private System.Windows.Forms.TextBox txtUnloaderHomeVel3;
        private System.Windows.Forms.Label lbl_Unloader_MaxVelocitu;
        private System.Windows.Forms.TextBox txtUnloaderHomeVel2;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtUnloaderHomeVel1;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.ComboBox cbxtUnloaderHomeLevel;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.ComboBox cbxtUnloaderHomeSignal;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.ComboBox cbxtUnloaderServoOnLevel;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.ComboBox cbxtUnloaderPlusEndLimit;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.ComboBox cbxtUnloaderMinusEndLismit;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.ComboBox cbxtUnloaderAlarm;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ComboBox cbxtUnloaderInposition;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.ComboBox cbxtUnloaderAbsRelMode;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.ComboBox cbxtUnloaderEncInput;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.ComboBox cbxtUnloaderPulseOutput;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.ComboBox cbxtUnloaderVelProfileMode;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox txtUnloaderAxisNo;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
    }
}
