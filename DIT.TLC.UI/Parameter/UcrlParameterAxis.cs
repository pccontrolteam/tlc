﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DIT.TLC.UI
{
    public partial class ParameterAxis : UserControl
    {
        public ParameterAxis()
        {
            InitializeComponent();
        }

        // 클릭시 색 변경 함수
        Panel SelectedMenu = null;
        private void MenuColorChange(object sender, EventArgs e)
        {
            if (SelectedMenu != null)
                SelectedMenu.BackColor = Color.FromArgb(64, 64, 64);
            
            SelectedMenu = sender as Panel;
            SelectedMenu.BackColor = Color.Blue;
        }

        #region 로더 페이지

        private void pn_Loader_31_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnLoader31, e);
        }

        private void pn_Loader_32_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnLoader32, e);
        }

        private void pn_Loader_33_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnLoader33, e);
        }

        private void pn_Loader_35_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnLoader35, e);
        }

        private void pn_Loader_36_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnLoader36, e);
        }

        private void pn_Loader_04_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnLoader04, e);
        }

        private void pn_Loader_05_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnLoader05, e);
        }

        private void pn_Loader_06_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnLoader06, e);
        }

        private void pn_Loader_07_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnLoader07, e);
        }

        private void pn_Loader_08_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnLoader08, e);
        }

        private void pn_Loader_09_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnLoader09, e);
        }

        private void pn_Loader_10_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnLoader10, e);
        }

        private void pn_Loader_11_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnLoader11, e);
        }

        private void pn_Loader_12_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnLoader12, e);
        }

        private void pn_Loader_13_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnLoader13, e);
        }

        private void pn_Loader_14_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnLoader14, e);
        }

        private void pn_Loader_15_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnLoader15, e);
        }



        #endregion

        #region 프로세스 페이지

        private void pn_Process_01_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnProcess01, e);
        }

        private void pn_Process_02_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnProcess02, e);
        }

        private void pn_Process_03_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnProcess03, e);
        }

        private void pn_Process_17_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnProcess17, e);
        }

        private void pn_Process_04_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnProcess04, e);
        }

        private void pn_Process_05_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnProcess05, e);
        }

        private void pn_Process_06_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnProcess06, e);
        }

        private void pn_Process_07_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnProcess07, e);
        }

        private void pn_Process_22_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnProcess22, e);
        }

        private void pn_Process_09_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnProcess09, e);
        }

        private void pn_Process_10_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnProcess10, e);
        }

        private void pn_Process_11_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnProcess11, e);
        }

        private void pn_Process_12_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnProcess12, e);
        }

        private void pn_Process_13_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnProcess13, e);
        }

        private void pn_Process_14_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnProcess14, e);
        }

        private void pn_Process_18_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnProcess18, e);
        }

        private void pn_Process_19_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnProcess19, e);
        }

        private void pn_Process_20_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnProcess20, e);
        }

        private void pn_Process_21_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnProcess21, e);
        }


        #endregion

        #region 언로더 페이지

        private void pn_Unloader_37_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnUnloader37, e);
        }

        private void pn_Unloader_38_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnUnloader38, e);
        }

        private void pn_Unloader_39_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnUnloader39, e);
        }

        private void pn_Unloader_40_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnUnloader40, e);
        }

        private void pn_Unloader_41_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnUnloader41, e);
        }

        private void pn_Unloader_16_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnUnloader16, e);
        }

        private void pn_Unloader_17_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnUnloader17, e);
        }

        private void pn_Unloader_18_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnUnloader18, e);
        }

        private void pn_Unloader_19_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnUnloader19, e);
        }

        private void pn_Unloader_20_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnUnloader20, e);
        }

        private void pn_Unloader_21_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnUnloader21, e);
        }

        private void pn_Unloader_23_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnUnloader23, e);
        }

        private void pn_Unloader_24_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnUnloader24, e);
        }

        private void pn_Unloader_26_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnUnloader26, e);
        }

        private void pn_Unloader_27_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnUnloader27, e);
        }

        private void pn_Unloader_28_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnUnloader28, e);
        }

        private void pn_Unloader_29_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnUnloader29, e);
        }

        private void pn_Unloader_30_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnUnloader30, e);
        }

        private void pn_Unloader_0_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnUnloader0, e);
        }

        private void pn_Unloader_1_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnUnloader1, e);
        }

        private void pn_Unloader_2_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnUnloader2, e);
        }

        private void pn_Unloader_3_Click(object sender, EventArgs e)
        {
            MenuColorChange(pnUnloader3, e);
        }

        #endregion
    }
}
