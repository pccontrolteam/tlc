﻿namespace DIT.TLC.UI
{
    partial class Parameter_location
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.tc_iostatus_info = new System.Windows.Forms.TabControl();
            this.tp_CasseteLoad = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCasseteLoadABCSD = new System.Windows.Forms.Button();
            this.btnCasseteLoadABCSU = new System.Windows.Forms.Button();
            this.btnCasseteLoadABCUG = new System.Windows.Forms.Button();
            this.btnCasseteLoadABCG = new System.Windows.Forms.Button();
            this.btnCasseteLoadALTSD = new System.Windows.Forms.Button();
            this.btnCasseteLoadALTSU = new System.Windows.Forms.Button();
            this.btnCasseteLoadALUG = new System.Windows.Forms.Button();
            this.btnCasseteLoadALG = new System.Windows.Forms.Button();
            this.btnCasseteLoadATCSD = new System.Windows.Forms.Button();
            this.btnCasseteLoadATCSU = new System.Windows.Forms.Button();
            this.btnCasseteLoadATCUG = new System.Windows.Forms.Button();
            this.btnCasseteLoadATCG = new System.Windows.Forms.Button();
            this.gxtCasseteLoad_Move = new System.Windows.Forms.GroupBox();
            this.gxtCasseteLoad_MoveB = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCasseteLoadMoveBZ2CellOut = new System.Windows.Forms.Button();
            this.btnCasseteLoadMoveBZ2Tilt = new System.Windows.Forms.Button();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCasseteLoadMoveBZ2Out = new System.Windows.Forms.Button();
            this.btnCasseteLoadMoveBZ2InWait = new System.Windows.Forms.Button();
            this.btnCasseteLoadMoveBZ2In = new System.Windows.Forms.Button();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCasseteLoadMoveBT4Out = new System.Windows.Forms.Button();
            this.btnCasseteLoadMoveBT3Move = new System.Windows.Forms.Button();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCasseteLoadMoveBT4Move = new System.Windows.Forms.Button();
            this.btnCasseteLoadMoveBT3In = new System.Windows.Forms.Button();
            this.gxtCasseteLoad_MoveA = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCasseteLoadMoveAZ1CellOut = new System.Windows.Forms.Button();
            this.btnCasseteLoadMoveAZ1Tilt = new System.Windows.Forms.Button();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCasseteLoadMoveAZ1Out = new System.Windows.Forms.Button();
            this.btnCasseteLoadMoveAZ1InWait = new System.Windows.Forms.Button();
            this.btnCasseteLoadMoveAZ1In = new System.Windows.Forms.Button();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCasseteLoadMoveAT2Out = new System.Windows.Forms.Button();
            this.btnCasseteLoadMoveAT1Move = new System.Windows.Forms.Button();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCasseteLoadMoveAT2Move = new System.Windows.Forms.Button();
            this.btnCasseteLoadMoveAT1In = new System.Windows.Forms.Button();
            this.gxtCasseteLoad_B = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCasseteLoadBBCSD = new System.Windows.Forms.Button();
            this.btnCasseteLoadBBCSU = new System.Windows.Forms.Button();
            this.btnCasseteLoadBBCUG = new System.Windows.Forms.Button();
            this.btnCasseteLoadBBCG = new System.Windows.Forms.Button();
            this.btnCasseteLoadBLTSD = new System.Windows.Forms.Button();
            this.btnCasseteLoadBLTSU = new System.Windows.Forms.Button();
            this.btnCasseteLoadBLUG = new System.Windows.Forms.Button();
            this.btnCasseteLoadBLG = new System.Windows.Forms.Button();
            this.btnCasseteLoadBTCSD = new System.Windows.Forms.Button();
            this.btnCasseteLoadBTCSU = new System.Windows.Forms.Button();
            this.btnCasseteLoadBTCUG = new System.Windows.Forms.Button();
            this.btnCasseteLoadBTCG = new System.Windows.Forms.Button();
            this.gxtCasseteLoad_A = new System.Windows.Forms.GroupBox();
            this.btnCasseteLoadSave = new System.Windows.Forms.Button();
            this.gxtCasseteLoad_Muting = new System.Windows.Forms.GroupBox();
            this.btnCasseteLoadMuting4 = new System.Windows.Forms.Button();
            this.btnCasseteLoadMuting2 = new System.Windows.Forms.Button();
            this.btnCasseteLoadMuting3 = new System.Windows.Forms.Button();
            this.btnCasseteLoadMuting1 = new System.Windows.Forms.Button();
            this.btnCasseteLoadMutingOff = new System.Windows.Forms.Button();
            this.btnCasseteLoadMutingIn = new System.Windows.Forms.Button();
            this.btnCasseteLoadSpeedSetting = new System.Windows.Forms.Button();
            this.btnCasseteLoadAllSetting = new System.Windows.Forms.Button();
            this.btnCasseteLoadLocationSetting = new System.Windows.Forms.Button();
            this.btnCasseteLoadMoveLocation = new System.Windows.Forms.Button();
            this.txtCasseteLoadLocation = new System.Windows.Forms.TextBox();
            this.txtCasseteLoadSpeed = new System.Windows.Forms.TextBox();
            this.txtCasseteLoadCurrentLocation = new System.Windows.Forms.TextBox();
            this.txtCasseteLoadSelectedShaft = new System.Windows.Forms.TextBox();
            this.lbl_CasseteLoad_Location = new System.Windows.Forms.Label();
            this.lbl_CasseteLoad_Speed = new System.Windows.Forms.Label();
            this.lbl_CasseteLoad_SelectedShaft = new System.Windows.Forms.Label();
            this.txtCasseteLoad_LocationSetting = new System.Windows.Forms.TextBox();
            this.lbl_CasseteLoad_CurrentLocation = new System.Windows.Forms.Label();
            this.btnCasseteLoadGrabSwitch3 = new System.Windows.Forms.Button();
            this.btnCasseteLoadGrabSwitch2 = new System.Windows.Forms.Button();
            this.btnCasseteLoadGrabSwitch1 = new System.Windows.Forms.Button();
            this.lvCasseteLoad = new System.Windows.Forms.ListView();
            this.col_CasseteLoad_NamebyLocation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_CasseteLoad_Shaft = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_CasseteLoad_Location = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_CasseteLoad_Speed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ajin_Setting_CasseteLoad = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.tp_Cellpurge = new System.Windows.Forms.TabPage();
            this.gxtCellpurge_Move = new System.Windows.Forms.GroupBox();
            this.gxtCellpurge_MoveCenter = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel21 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCellpurgeMoveCenterY2C = new System.Windows.Forms.Button();
            this.btnCellpurgeMoveCenterBBuffer = new System.Windows.Forms.Button();
            this.btnCellpurgeMoveCenterY2Uld = new System.Windows.Forms.Button();
            this.btnCellpurgeMoveCenterY1C = new System.Windows.Forms.Button();
            this.btnCellpurgeMoveCenterABuffer = new System.Windows.Forms.Button();
            this.btnCellpurgeMoveCenterY1Uld = new System.Windows.Forms.Button();
            this.gxtCellpurge_MoveB = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel20 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCellpurgeMoveBX2BC = new System.Windows.Forms.Button();
            this.btnCellpurgeMoveBX2BW = new System.Windows.Forms.Button();
            this.btnCellpurgeMoveBX1BW = new System.Windows.Forms.Button();
            this.btnCellpurgeMoveBX1BC = new System.Windows.Forms.Button();
            this.gxtCellpurge_MoveA = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel19 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCellpurgeMoveAX2AC = new System.Windows.Forms.Button();
            this.btnCellpurgeMoveAX2AW = new System.Windows.Forms.Button();
            this.btnCellpurgeMoveAX1AW = new System.Windows.Forms.Button();
            this.btnCellpurgeMoveAX1AC = new System.Windows.Forms.Button();
            this.gxtCellpurge_B = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel18 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCellpurgeBDestructionOn = new System.Windows.Forms.Button();
            this.btnCellpurgeBDestructionOff = new System.Windows.Forms.Button();
            this.btnCellpurgeBPneumaticOff = new System.Windows.Forms.Button();
            this.btnCellpurgeBPneumaticOn = new System.Windows.Forms.Button();
            this.gxtCellpurge_A = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel17 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCellpurgeADestructionOn = new System.Windows.Forms.Button();
            this.btnCellpurgeADestructionOff = new System.Windows.Forms.Button();
            this.btnCellpurgeAPneumaticOff = new System.Windows.Forms.Button();
            this.btnCellpurgeAPneumaticOn = new System.Windows.Forms.Button();
            this.btnCellpurgeSave = new System.Windows.Forms.Button();
            this.gxtCellpurge_Ionizer = new System.Windows.Forms.GroupBox();
            this.btnCellpurgeDestructionOff = new System.Windows.Forms.Button();
            this.btnCellpurgeIonizerOff = new System.Windows.Forms.Button();
            this.btnCellpurgeDestructionOn = new System.Windows.Forms.Button();
            this.btnCellpurgeIonizerOn = new System.Windows.Forms.Button();
            this.btnCellpurgeSpeedSetting = new System.Windows.Forms.Button();
            this.btnCellpurgeAllSetting = new System.Windows.Forms.Button();
            this.btnCellpurgeLocationSetting = new System.Windows.Forms.Button();
            this.btnCellpurgeMoveLocation = new System.Windows.Forms.Button();
            this.txtCellpurgeLocation = new System.Windows.Forms.TextBox();
            this.txtCellpurgeSpeed = new System.Windows.Forms.TextBox();
            this.txtCellpurgeCurrentLocation = new System.Windows.Forms.TextBox();
            this.txtCellpurgeSelectedShaft = new System.Windows.Forms.TextBox();
            this.lbl_Cellpurge_Location = new System.Windows.Forms.Label();
            this.lbl_Cellpurge_Speed = new System.Windows.Forms.Label();
            this.lbl_Cellpurge_SelectedShaft = new System.Windows.Forms.Label();
            this.txtCellpurge_LocationSetting = new System.Windows.Forms.TextBox();
            this.lbl_Cellpurge_CurrentLocation = new System.Windows.Forms.Label();
            this.btnCellpurgeGrabSwitch3 = new System.Windows.Forms.Button();
            this.btnCellpurgeGrabSwitch2 = new System.Windows.Forms.Button();
            this.btnCellpurgeGrabSwitch1 = new System.Windows.Forms.Button();
            this.lvCellpurge = new System.Windows.Forms.ListView();
            this.col_Cellpurge_NamebyLocation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_Cellpurge_Shaft = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_Cellpurge_Location = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_Cellpurge_Speed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ajin_Setting_Cellpurge = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.tp_CellLoad = new System.Windows.Forms.TabPage();
            this.gxtCellLoad_Move = new System.Windows.Forms.GroupBox();
            this.gxtCellLoad_MoveLorUn = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel28 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCellLoadMoveLorUnPreAlignMark1 = new System.Windows.Forms.Button();
            this.btnCellLoadMoveLorUnPreAlignMark2 = new System.Windows.Forms.Button();
            this.btnCellLoadMoveLorUnY1UnL = new System.Windows.Forms.Button();
            this.btnCellLoadMoveLorUnY1L = new System.Windows.Forms.Button();
            this.btnCellLoadMoveLorUnX2L = new System.Windows.Forms.Button();
            this.btnCellLoadMoveLorUnX1L = new System.Windows.Forms.Button();
            this.gxtCellLoad_MoveB = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel31 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel32 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCellLoadMoveBBPickerM90Angle = new System.Windows.Forms.Button();
            this.btnCellLoadMoveBBPickerP90Angle = new System.Windows.Forms.Button();
            this.btnCellLoadMoveBBPicker0Angle = new System.Windows.Forms.Button();
            this.tableLayoutPanel33 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCellLoadMoveBBPickerUnL = new System.Windows.Forms.Button();
            this.btnCellLoadMoveBBPickerL = new System.Windows.Forms.Button();
            this.tableLayoutPanel34 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCellLoadMoveBX2BStageUnL = new System.Windows.Forms.Button();
            this.btnCellLoadMoveBX1BStageUnL = new System.Windows.Forms.Button();
            this.gxtCellLoad_MoveA = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel26 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel30 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCellLoadMoveAAPickerM90Angle = new System.Windows.Forms.Button();
            this.btnCellLoadMoveAAPickerP90Angle = new System.Windows.Forms.Button();
            this.btnCellLoadMoveAAPicker0Angle = new System.Windows.Forms.Button();
            this.tableLayoutPanel29 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCellLoadMoveAAPickerUnL = new System.Windows.Forms.Button();
            this.btnCellLoadMoveAAPickerL = new System.Windows.Forms.Button();
            this.tableLayoutPanel27 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCellLoadMoveAX2AStageUnL = new System.Windows.Forms.Button();
            this.btnCellLoadMoveAX1AStageUnL = new System.Windows.Forms.Button();
            this.gxtCellLoad_B = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel25 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCellLoadBPickerDestructionOn = new System.Windows.Forms.Button();
            this.btnCellLoadBPickerDestructionOff = new System.Windows.Forms.Button();
            this.btnCellLoadBPickerUp = new System.Windows.Forms.Button();
            this.btnCellLoadBPickerDown = new System.Windows.Forms.Button();
            this.btnCellLoadBPickerPneumaticOn = new System.Windows.Forms.Button();
            this.btnCellLoadBPickerPneumaticOff = new System.Windows.Forms.Button();
            this.gxtCellLoad_A = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel24 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCellLoadAPickerDestructionOn = new System.Windows.Forms.Button();
            this.btnCellLoadAPickerDestructionOff = new System.Windows.Forms.Button();
            this.btnCellLoadAPickerUp = new System.Windows.Forms.Button();
            this.btnCellLoadAPickerDown = new System.Windows.Forms.Button();
            this.btnCellLoadAPickerPneumaticOn = new System.Windows.Forms.Button();
            this.btnCellLoadAPickerPneumaticOff = new System.Windows.Forms.Button();
            this.btnCellLoadSave = new System.Windows.Forms.Button();
            this.btnCellLoadSpeedSetting = new System.Windows.Forms.Button();
            this.btnCellLoadAllSetting = new System.Windows.Forms.Button();
            this.btnCellLoadLocationSetting = new System.Windows.Forms.Button();
            this.btnCellLoadMoveLocation = new System.Windows.Forms.Button();
            this.txtCellLoadLocation = new System.Windows.Forms.TextBox();
            this.txtCellLoadSpeed = new System.Windows.Forms.TextBox();
            this.txtCellLoadCurrentLocation = new System.Windows.Forms.TextBox();
            this.txtCellLoadSelectedShaft = new System.Windows.Forms.TextBox();
            this.lbl_CellLoad_Location = new System.Windows.Forms.Label();
            this.lbl_CellLoad_Speed = new System.Windows.Forms.Label();
            this.lbl_CellLoad_SelectedShaft = new System.Windows.Forms.Label();
            this.txtCellLoad_LocationSetting = new System.Windows.Forms.TextBox();
            this.lbl_CellLoad_CurrentLocation = new System.Windows.Forms.Label();
            this.btnCellLoadGrabSwitch3 = new System.Windows.Forms.Button();
            this.btnCellLoadGrabSwitch2 = new System.Windows.Forms.Button();
            this.btnCellLoadGrabSwitch1 = new System.Windows.Forms.Button();
            this.lvCellLoad = new System.Windows.Forms.ListView();
            this.col_CellLoad_NamebyLocation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_CellLoad_Shaft = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_CellLoad_Location = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_CellLoad_Speed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ajin_Setting_CellLoad = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.tp_IRCutProcess = new System.Windows.Forms.TabPage();
            this.gxtIRCutProcess_Ionizer = new System.Windows.Forms.GroupBox();
            this.btnIRCutProcessDestruction = new System.Windows.Forms.Button();
            this.btnIRCutProcessIonizerOff = new System.Windows.Forms.Button();
            this.btnIRCutProcessDestructionOn = new System.Windows.Forms.Button();
            this.btnIRCutProcessIonizerOn = new System.Windows.Forms.Button();
            this.gxtIRCutProcess_Move = new System.Windows.Forms.GroupBox();
            this.gxtIRCutProcess_RightOffset = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel40 = new System.Windows.Forms.TableLayoutPanel();
            this.btnIRCutProcessRightOffsetRightVision2Focus = new System.Windows.Forms.Button();
            this.btnIRCutProcessRightOffsetRightFocus2Vision = new System.Windows.Forms.Button();
            this.btnIRCutProcessRightOffsetLeftVision2Focus = new System.Windows.Forms.Button();
            this.btnIRCutProcessRightOffsetLeftFocus2Vision = new System.Windows.Forms.Button();
            this.gxtIRCutProcess_LeftOffset = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel39 = new System.Windows.Forms.TableLayoutPanel();
            this.btnIRCutProcessLeftOffsetRightVisoin2Focus = new System.Windows.Forms.Button();
            this.btnIRCutProcessLeftOffsetRightFocus2Visoin = new System.Windows.Forms.Button();
            this.btnIRCutProcessLeftOffsetLeftVision2Focus = new System.Windows.Forms.Button();
            this.btnIRCutProcessLeftOffsetLeftFocus2Vision = new System.Windows.Forms.Button();
            this.gxtIRCutProcess_MoveB = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel38 = new System.Windows.Forms.TableLayoutPanel();
            this.btnIRCutProcessMoveBR1Camera = new System.Windows.Forms.Button();
            this.btnIRCutProcessMoveBR2Camera = new System.Windows.Forms.Button();
            this.btnIRCutProcessMoveBRightCellLoad = new System.Windows.Forms.Button();
            this.btnIRCutProcessMoveBRightCellUnload = new System.Windows.Forms.Button();
            this.btnIRCutProcessMoveBR1Laser = new System.Windows.Forms.Button();
            this.btnIRCutProcessMoveBR2Laser = new System.Windows.Forms.Button();
            this.gxtIRCutProcess_MoveA = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel37 = new System.Windows.Forms.TableLayoutPanel();
            this.btnIRCutProcessMoveAL1Camera = new System.Windows.Forms.Button();
            this.btnIRCutProcessMoveAL2Camera = new System.Windows.Forms.Button();
            this.btnIRCutProcessMoveALeftCellLoad = new System.Windows.Forms.Button();
            this.btnIRCutProcessMoveALeftCellUnload = new System.Windows.Forms.Button();
            this.btnIRCutProcessMoveAL1Laser = new System.Windows.Forms.Button();
            this.btnIRCutProcessMoveAL2Laser = new System.Windows.Forms.Button();
            this.gxtIRCutProcess_B = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel36 = new System.Windows.Forms.TableLayoutPanel();
            this.btnIRCutProcessBR1PneumaticCh1Off = new System.Windows.Forms.Button();
            this.btnIRCutProcessBR2PneumaticCh1On = new System.Windows.Forms.Button();
            this.btnIRCutProcessBR2PneumaticCh1Off = new System.Windows.Forms.Button();
            this.btnIRCutProcessBR1PneumaticCh2Off = new System.Windows.Forms.Button();
            this.btnIRCutProcessBR2PneumaticCh2On = new System.Windows.Forms.Button();
            this.btnIRCutProcessBR2PneumaticCh2Off = new System.Windows.Forms.Button();
            this.btnIRCutProcessBR1PneumaticCh2On = new System.Windows.Forms.Button();
            this.btnIRCutProcessBR1PneumaticCh1On = new System.Windows.Forms.Button();
            this.btnIRCutProcessAR1DestructionCh1On = new System.Windows.Forms.Button();
            this.btnIRCutProcessAR1DestructionCh1Off = new System.Windows.Forms.Button();
            this.btnIRCutProcessAR2DestructionCh1On = new System.Windows.Forms.Button();
            this.btnIRCutProcessAR2DestructionCh1Off = new System.Windows.Forms.Button();
            this.btnIRCutProcessAR1DestructionCh2On = new System.Windows.Forms.Button();
            this.btnIRCutProcessAR1DestructionCh2Off = new System.Windows.Forms.Button();
            this.btnIRCutProcessAR2DestructionCh2Off = new System.Windows.Forms.Button();
            this.btnIRCutProcessAR2DestructionCh2On = new System.Windows.Forms.Button();
            this.gxtIRCutProcess_A = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel35 = new System.Windows.Forms.TableLayoutPanel();
            this.btnIRCutProcessAL1DestructionCh2Off = new System.Windows.Forms.Button();
            this.btnIRCutProcessAL1DestructionCh2On = new System.Windows.Forms.Button();
            this.btnIRCutProcessAL2DestructionCh2Off = new System.Windows.Forms.Button();
            this.btnIRCutProcessAL2DestructionCh2On = new System.Windows.Forms.Button();
            this.btnIRCutProcessAL1PneumaticCh1Off = new System.Windows.Forms.Button();
            this.btnIRCutProcessAL2PneumaticCh1On = new System.Windows.Forms.Button();
            this.btnIRCutProcessAL2PneumaticCh1Off = new System.Windows.Forms.Button();
            this.btnIRCutProcessAL1PneumaticCh2Off = new System.Windows.Forms.Button();
            this.btnIRCutProcessAL2PneumaticCh2On = new System.Windows.Forms.Button();
            this.btnIRCutProcessAL2PneumaticCh2Off = new System.Windows.Forms.Button();
            this.btnIRCutProcessAL1PneumaticCh2On = new System.Windows.Forms.Button();
            this.btnIRCutProcessAL1PneumaticCh1On = new System.Windows.Forms.Button();
            this.btnIRCutProcessAL1DestructionCh1On = new System.Windows.Forms.Button();
            this.btnIRCutProcessAL1DestructionCh1Off = new System.Windows.Forms.Button();
            this.btnIRCutProcessAL2DestructionCh1On = new System.Windows.Forms.Button();
            this.btnIRCutProcessAL2DestructionCh1Off = new System.Windows.Forms.Button();
            this.btnIRCutProcessSave = new System.Windows.Forms.Button();
            this.btnIRCutProcessSpeedSetting = new System.Windows.Forms.Button();
            this.btnIRCutProcessAllSetting = new System.Windows.Forms.Button();
            this.btnIRCutProcessLocationSetting = new System.Windows.Forms.Button();
            this.btnIRCutProcessMoveLocation = new System.Windows.Forms.Button();
            this.txtIRCutProcessLocation = new System.Windows.Forms.TextBox();
            this.txtIRCutProcessSpeed = new System.Windows.Forms.TextBox();
            this.txtIRCutProcessCurrentLocation = new System.Windows.Forms.TextBox();
            this.txtIRCutProcessSelectedShaft = new System.Windows.Forms.TextBox();
            this.lbl_IRCutProcess_Location = new System.Windows.Forms.Label();
            this.lbl_IRCutProcess_Speed = new System.Windows.Forms.Label();
            this.lbl_IRCutProcess_SelectedShaft = new System.Windows.Forms.Label();
            this.txtIRCutProcess_LocationSetting = new System.Windows.Forms.TextBox();
            this.lbl_IRCutProcess_CurrentLocation = new System.Windows.Forms.Label();
            this.btnIRCutProcessGrabSwitch3 = new System.Windows.Forms.Button();
            this.btnIRCutProcessGrabSwitch2 = new System.Windows.Forms.Button();
            this.btnIRCutProcessGrabSwitch1 = new System.Windows.Forms.Button();
            this.lvIRCutProcess = new System.Windows.Forms.ListView();
            this.col_IRCutProcess_NamebyLocation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_IRCutProcess_Shaft = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_IRCutProcess_Location = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_IRCutProcess_Speed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ajin_Setting_IRCutProcess = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.tp_BreakTransfer = new System.Windows.Forms.TabPage();
            this.gxtBreakTransfer_Move = new System.Windows.Forms.GroupBox();
            this.gxtBreakTransfer_MoveB = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel44 = new System.Windows.Forms.TableLayoutPanel();
            this.btnBreakTransferMoveBUnload = new System.Windows.Forms.Button();
            this.btnBreakTransferMoveBLoad = new System.Windows.Forms.Button();
            this.gxtBreakTransfer_MoveA = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel43 = new System.Windows.Forms.TableLayoutPanel();
            this.btnBreakTransferMoveAUnload = new System.Windows.Forms.Button();
            this.btnBreakTransferMoveALoad = new System.Windows.Forms.Button();
            this.gxtBreakTransfer_B = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel42 = new System.Windows.Forms.TableLayoutPanel();
            this.btnBreakTransferBPickerDestructionOn = new System.Windows.Forms.Button();
            this.btnBreakTransferBPickerDestructionOff = new System.Windows.Forms.Button();
            this.btnBreakTransferBPickerUp = new System.Windows.Forms.Button();
            this.btnBreakTransferBPickerDown = new System.Windows.Forms.Button();
            this.btnBreakTransferBPickerPneumaticOn = new System.Windows.Forms.Button();
            this.btnBreakTransferBPneumaticOff = new System.Windows.Forms.Button();
            this.gxtBreakTransfer_A = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel41 = new System.Windows.Forms.TableLayoutPanel();
            this.btnBreakTransferAPickerDestructionOn = new System.Windows.Forms.Button();
            this.btnBreakTransferAPickerDestructionOff = new System.Windows.Forms.Button();
            this.btnBreakTransferAPickerUp = new System.Windows.Forms.Button();
            this.btnBreakTransferAPickerDown = new System.Windows.Forms.Button();
            this.btnBreakTransferAPickerPneumaticOn = new System.Windows.Forms.Button();
            this.btnBreakTransferAPickerPneumaticOff = new System.Windows.Forms.Button();
            this.btnBreakTransferSave = new System.Windows.Forms.Button();
            this.btnBreakTransferSpeedSetting = new System.Windows.Forms.Button();
            this.btnBreakTransferAllSetting = new System.Windows.Forms.Button();
            this.btnBreakTransferLocationSetting = new System.Windows.Forms.Button();
            this.btnBreakTransferMoveLocation = new System.Windows.Forms.Button();
            this.txtBreakTransferLocation = new System.Windows.Forms.TextBox();
            this.txtBreakTransferSpeed = new System.Windows.Forms.TextBox();
            this.txtBreakTransferCurrentLocation = new System.Windows.Forms.TextBox();
            this.txtBreakTransferSelectedShaft = new System.Windows.Forms.TextBox();
            this.lbl_BreakTransfer_Location = new System.Windows.Forms.Label();
            this.lbl_BreakTransfer_Speed = new System.Windows.Forms.Label();
            this.lbl_BreakTransfer_SelectedShaft = new System.Windows.Forms.Label();
            this.txtBreakTransfer_LocationSetting = new System.Windows.Forms.TextBox();
            this.lbl_BreakTransfer_CurrentLocation = new System.Windows.Forms.Label();
            this.btnBreakTransferGrabSwitch3 = new System.Windows.Forms.Button();
            this.btnBreakTransferGrabSwitch2 = new System.Windows.Forms.Button();
            this.btnBreakTransferGrabSwitch1 = new System.Windows.Forms.Button();
            this.lvBreakTransfer = new System.Windows.Forms.ListView();
            this.col_BreakTransfer_NamebyLocation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_BreakTransfer_Shaft = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_BreakTransfer_Location = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_BreakTransfer_Speed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ajin_Setting_BreakTransfer = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.tp_BreakUnitXZ = new System.Windows.Forms.TabPage();
            this.gxtBreakUnitXZ_Move = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel45 = new System.Windows.Forms.TableLayoutPanel();
            this.gxtBreakUnitXZ_MoveZ4 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel49 = new System.Windows.Forms.TableLayoutPanel();
            this.gxtBreakUnitXZMoveZ4SlowDescent = new System.Windows.Forms.Button();
            this.gxtBreakUnitXZMoveZ4VisionCheck = new System.Windows.Forms.Button();
            this.gxtBreakUnitXZMoveZ4FastDescent = new System.Windows.Forms.Button();
            this.gxtBreakUnitXZ_MoveZ3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel48 = new System.Windows.Forms.TableLayoutPanel();
            this.gxtBreakUnitXZMoveZ3SlowDescent = new System.Windows.Forms.Button();
            this.gxtBreakUnitXZMoveZ3VisionCheck = new System.Windows.Forms.Button();
            this.gxtBreakUnitXZMoveZ3FastDescent = new System.Windows.Forms.Button();
            this.gxtBreakUnitXZ_MoveZ2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel47 = new System.Windows.Forms.TableLayoutPanel();
            this.gxtBreakUnitXZMoveZ2SlowDescent = new System.Windows.Forms.Button();
            this.gxtBreakUnitXZMoveZ2VisionCheck = new System.Windows.Forms.Button();
            this.gxtBreakUnitXZMoveZ2FastDescent = new System.Windows.Forms.Button();
            this.gxtBreakUnitXZ_MoveZ1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel46 = new System.Windows.Forms.TableLayoutPanel();
            this.gxtBreakUnitXZMoveZ1SlowDescent = new System.Windows.Forms.Button();
            this.gxtBreakUnitXZMoveZ1VisionCheck = new System.Windows.Forms.Button();
            this.gxtBreakUnitXZMoveZ1FastDescent = new System.Windows.Forms.Button();
            this.gxtBreakUnitXZSave = new System.Windows.Forms.Button();
            this.btnBreakUnitXZSpeedSetting = new System.Windows.Forms.Button();
            this.btnBreakUnitXZAllSetting = new System.Windows.Forms.Button();
            this.btnBreakUnitXZLocationSetting = new System.Windows.Forms.Button();
            this.btnBreakUnitXZMoveLocation = new System.Windows.Forms.Button();
            this.txtBreakUnitXZLocation = new System.Windows.Forms.TextBox();
            this.txtBreakUnitXZSpeed = new System.Windows.Forms.TextBox();
            this.txtBreakUnitXZCurrentLocation = new System.Windows.Forms.TextBox();
            this.txtBreakUnitXZSelectedShaft = new System.Windows.Forms.TextBox();
            this.lbl_BreakUnitXZ_Location = new System.Windows.Forms.Label();
            this.lbl_BreakUnitXZ_Speed = new System.Windows.Forms.Label();
            this.lbl_BreakUnitXZ_SelectedShaft = new System.Windows.Forms.Label();
            this.txtBreakUnitXZ_LocationSetting = new System.Windows.Forms.TextBox();
            this.lbl_BreakUnitXZ_CurrentLocation = new System.Windows.Forms.Label();
            this.btnBreakUnitXZGrabSwitch3 = new System.Windows.Forms.Button();
            this.btnBreakUnitXZGrabSwitch2 = new System.Windows.Forms.Button();
            this.btnBreakUnitXZGrabSwitch1 = new System.Windows.Forms.Button();
            this.lvBreakUnitXZ = new System.Windows.Forms.ListView();
            this.col_BreakUnitXZ_NamebyLocation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_BreakUnitXZ_Shaft = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_BreakUnitXZ_Location = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_BreakUnitXZ_Speed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ajin_Setting_BreakUnitXZ = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.tp_BreakUnitTY = new System.Windows.Forms.TabPage();
            this.gxtBreakUnitTY_Shutter = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel53 = new System.Windows.Forms.TableLayoutPanel();
            this.btnBreakUnitTYShutterClose = new System.Windows.Forms.Button();
            this.btnBreakUnitTYShutterOpen = new System.Windows.Forms.Button();
            this.gxtBreakUnitTY_DummyBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel52 = new System.Windows.Forms.TableLayoutPanel();
            this.btnBreakUnitTYDummyxtOutput = new System.Windows.Forms.Button();
            this.btnBreakUnitTYDummyxtInput = new System.Windows.Forms.Button();
            this.gxtBreakUnitTY_Ionizer = new System.Windows.Forms.GroupBox();
            this.btnBreakUnitTYDestructionOff = new System.Windows.Forms.Button();
            this.btnBreakUnitTYIonizerOff = new System.Windows.Forms.Button();
            this.btnBreakUnitTYDestructionOn = new System.Windows.Forms.Button();
            this.btnBreakUnitTYIonizerOn = new System.Windows.Forms.Button();
            this.gxtBreakUnitTY_Move = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel54 = new System.Windows.Forms.TableLayoutPanel();
            this.gxtBreakUnitTY_Theta = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel57 = new System.Windows.Forms.TableLayoutPanel();
            this.btnBreakUnitTYThetaT2Wait = new System.Windows.Forms.Button();
            this.btnBreakUnitTYThetaT4Wait = new System.Windows.Forms.Button();
            this.btnBreakUnitTYThetaT1Wait = new System.Windows.Forms.Button();
            this.btnBreakUnitTYThetaT3Wait = new System.Windows.Forms.Button();
            this.gxtBreakUnitTY_MoveB = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel56 = new System.Windows.Forms.TableLayoutPanel();
            this.btnBreakUnitTYMoveBUnload = new System.Windows.Forms.Button();
            this.btnBreakUnitTYMoveBLoad = new System.Windows.Forms.Button();
            this.gxtBreakUnitTY_MoveA = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel55 = new System.Windows.Forms.TableLayoutPanel();
            this.btnBreakUnitTYMoveAUnload = new System.Windows.Forms.Button();
            this.btnBreakUnitTYMoveALoad = new System.Windows.Forms.Button();
            this.gxtBreakUnitTY_B = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel50 = new System.Windows.Forms.TableLayoutPanel();
            this.btnBreakUnitTYB1DestructionOn = new System.Windows.Forms.Button();
            this.btnBreakUnitTYB1DestructionOff = new System.Windows.Forms.Button();
            this.btnBreakUnitTYB2DestructionOn = new System.Windows.Forms.Button();
            this.btnBreakUnitTYB2DestructionOff = new System.Windows.Forms.Button();
            this.btnBreakUnitTYB1PneumaticOff = new System.Windows.Forms.Button();
            this.btnBreakUnitTYB1PneumaticOn = new System.Windows.Forms.Button();
            this.btnBreakUnitTYb2PneumaticOn = new System.Windows.Forms.Button();
            this.btnBreakUnitTYB2PneumaticOff = new System.Windows.Forms.Button();
            this.gxtBreakUnitTY_A = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel51 = new System.Windows.Forms.TableLayoutPanel();
            this.btnBreakUnitTYA1DestructionOn = new System.Windows.Forms.Button();
            this.btnBreakUnitTYA1DestructionOff = new System.Windows.Forms.Button();
            this.btnBreakUnitTYA2DestructionOn = new System.Windows.Forms.Button();
            this.btnBreakUnitTYA2DestructionOff = new System.Windows.Forms.Button();
            this.btnBreakUnitTYA1PneumaticOff = new System.Windows.Forms.Button();
            this.btnBreakUnitTYA1PneumaticOn = new System.Windows.Forms.Button();
            this.btnBreakUnitTYA2PneumaticOn = new System.Windows.Forms.Button();
            this.btnBreakUnitTYA2PneumaticOff = new System.Windows.Forms.Button();
            this.btnBreakUnitTYSave = new System.Windows.Forms.Button();
            this.btnBreakUnitTYSpeedSetting = new System.Windows.Forms.Button();
            this.btnBreakUnitTYAllSetting = new System.Windows.Forms.Button();
            this.btnBreakUnitTYLocationSetting = new System.Windows.Forms.Button();
            this.btnBreakUnitTYMoveLocation = new System.Windows.Forms.Button();
            this.txtBreakUnitTYLocation = new System.Windows.Forms.TextBox();
            this.txtBreakUnitTYSpeed = new System.Windows.Forms.TextBox();
            this.txtBreakUnitTYCurrentLocation = new System.Windows.Forms.TextBox();
            this.txtBreakUnitTYSelectedShaft = new System.Windows.Forms.TextBox();
            this.lbl_BreakUnitTY_Location = new System.Windows.Forms.Label();
            this.lbl_BreakUnitTY_Speed = new System.Windows.Forms.Label();
            this.lbl_BreakUnitTY_SelectedShaft = new System.Windows.Forms.Label();
            this.txtBreakUnitTY_LocationSetting = new System.Windows.Forms.TextBox();
            this.lbl_BreakUnitTY_CurrentLocation = new System.Windows.Forms.Label();
            this.btnBreakUnitTYGrabSwitch3 = new System.Windows.Forms.Button();
            this.btnBreakUnitTYGrabSwitch2 = new System.Windows.Forms.Button();
            this.btnBreakUnitTYGrabSwitch1 = new System.Windows.Forms.Button();
            this.lvBreakUnitTY = new System.Windows.Forms.ListView();
            this.col_BreakUnitTY_NamebyLocation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_BreakUnitTY_Shaft = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_BreakUnitTY_Location = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_BreakUnitTY_Speed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ajin_Setting_BreakUnitTY = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.tp_UnloaderTransfer = new System.Windows.Forms.TabPage();
            this.gxtUnloaderTransfer_Move = new System.Windows.Forms.GroupBox();
            this.gxtUnloaderTransfer_MoveB = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel61 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel62 = new System.Windows.Forms.TableLayoutPanel();
            this.btnUnloaderTransferMoveBT40Angle = new System.Windows.Forms.Button();
            this.btnUnloaderTransferMoveBT4P90Angle = new System.Windows.Forms.Button();
            this.btnUnloaderTransferMoveBT4M90Angle = new System.Windows.Forms.Button();
            this.btnUnloaderTransferMoveBT30Angle = new System.Windows.Forms.Button();
            this.btnUnloaderTransferMoveBT3P90Angle = new System.Windows.Forms.Button();
            this.btnUnloaderTransferMoveBT3M90Angle = new System.Windows.Forms.Button();
            this.tableLayoutPanel63 = new System.Windows.Forms.TableLayoutPanel();
            this.btnUnloaderTransferMoveBBStage = new System.Windows.Forms.Button();
            this.btnUnloaderTransferMoveBBUnloading = new System.Windows.Forms.Button();
            this.gxtUnloaderTransfer_MoveA = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel58 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel60 = new System.Windows.Forms.TableLayoutPanel();
            this.btnUnloaderTransferMoveAT20Angle = new System.Windows.Forms.Button();
            this.btnUnloaderTransferMoveAT2P90Angle = new System.Windows.Forms.Button();
            this.btnUnloaderTransferMoveAT2M90Angle = new System.Windows.Forms.Button();
            this.btnUnloaderTransferMoveAT10Angle = new System.Windows.Forms.Button();
            this.btnUnloaderTransferMoveAT1P90Angle = new System.Windows.Forms.Button();
            this.btnUnloaderTransferMoveAT1M90Angle = new System.Windows.Forms.Button();
            this.tableLayoutPanel59 = new System.Windows.Forms.TableLayoutPanel();
            this.btnUnloaderTransferMoveAAStage = new System.Windows.Forms.Button();
            this.btnUnloaderTransferMoveAAUnloading = new System.Windows.Forms.Button();
            this.gxtUnloaderTransfer_B = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.btnUnloaderTransferBPickerDestruction2 = new System.Windows.Forms.Button();
            this.btnUnloaderTransferBPickerDestructionOn2 = new System.Windows.Forms.Button();
            this.btnUnloaderTransferBPickerDestructionOff1 = new System.Windows.Forms.Button();
            this.btnUnloaderTransferBPickerDestructionOn1 = new System.Windows.Forms.Button();
            this.btnUnloaderTransferBPickerPneumaticOff2 = new System.Windows.Forms.Button();
            this.btnUnloaderTransferBPickerPneumaticOn2 = new System.Windows.Forms.Button();
            this.btnUnloaderTransferBPickerPneumaticOff1 = new System.Windows.Forms.Button();
            this.btnUnloaderTransferBPickerPneumaticOn1 = new System.Windows.Forms.Button();
            this.btnUnloaderTransferBPickerDown2 = new System.Windows.Forms.Button();
            this.btnUnloaderTransferBPickerUp2 = new System.Windows.Forms.Button();
            this.btnUnloaderTransferBPickerDown1 = new System.Windows.Forms.Button();
            this.btnUnloaderTransferBPickerUp1 = new System.Windows.Forms.Button();
            this.gxtUnloaderTransfer_A = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btnUnloaderTransferAPickerDestructionOff2 = new System.Windows.Forms.Button();
            this.btnUnloaderTransferAPickerDestructionOn2 = new System.Windows.Forms.Button();
            this.btnUnloaderTransferAPickerDestructionOff1 = new System.Windows.Forms.Button();
            this.btnUnloaderTransferAPickerDestructionOn1 = new System.Windows.Forms.Button();
            this.btnUnloaderTransferAPickerPneumaticOff2 = new System.Windows.Forms.Button();
            this.btnUnloaderTransferAPickerPneumaticOn2 = new System.Windows.Forms.Button();
            this.btnUnloaderTransferAPickerPneumaticOff1 = new System.Windows.Forms.Button();
            this.btnUnloaderTransferAPickerPneumaticOn1 = new System.Windows.Forms.Button();
            this.btnUnloaderTransferAPickerDown2 = new System.Windows.Forms.Button();
            this.btnUnloaderTransferAPickerUp2 = new System.Windows.Forms.Button();
            this.btnUnloaderTransferAPickerDown1 = new System.Windows.Forms.Button();
            this.btnUnloaderTransferAPickerUp1 = new System.Windows.Forms.Button();
            this.btnUnloaderTransferSave = new System.Windows.Forms.Button();
            this.btnUnloaderTransferSpeedSetting = new System.Windows.Forms.Button();
            this.btnUnloaderTransferAllSetting = new System.Windows.Forms.Button();
            this.btnUnloaderTransferLocationSetting = new System.Windows.Forms.Button();
            this.btnUnloaderTransferMoveLocation = new System.Windows.Forms.Button();
            this.txtUnloaderTransferLocation = new System.Windows.Forms.TextBox();
            this.txtUnloaderTransferSpeed = new System.Windows.Forms.TextBox();
            this.txtUnloaderTransferCurrentLocation = new System.Windows.Forms.TextBox();
            this.txtUnloaderTransferSelectedShaft = new System.Windows.Forms.TextBox();
            this.lbl_UnloaderTransfer_Location = new System.Windows.Forms.Label();
            this.lbl_UnloaderTransfer_Speed = new System.Windows.Forms.Label();
            this.lbl_UnloaderTransfer_SeletedShaft = new System.Windows.Forms.Label();
            this.txtUnloaderTransfer_LocationSetting = new System.Windows.Forms.TextBox();
            this.lbl_UnloaderTransfer_CurrentLocation = new System.Windows.Forms.Label();
            this.btnUnloaderTransferGrabSwitch3 = new System.Windows.Forms.Button();
            this.btnUnloaderTransferGrabSwitch2 = new System.Windows.Forms.Button();
            this.btnUnloaderTransferGrabSwitch1 = new System.Windows.Forms.Button();
            this.lvUnloaderTransfer = new System.Windows.Forms.ListView();
            this.col_UnloaderTransfer_NamebyLocation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_UnloaderTransfer_Shaft = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_UnloaderTransfer_Location = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_UnloaderTransfer_Speed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ajin_Setting_UnloaderTransfer = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.tp_CameraUnit = new System.Windows.Forms.TabPage();
            this.gxtCameraUnit_Move = new System.Windows.Forms.GroupBox();
            this.gxtCameraUnit_MoveB = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel65 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCameraUnitMoveBInspectionMove4 = new System.Windows.Forms.Button();
            this.btnCameraUnitMoveBInspectionMove3 = new System.Windows.Forms.Button();
            this.btnCameraUnitMoveBMCRMove = new System.Windows.Forms.Button();
            this.gxtCameraUnit_MoveA = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel64 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCameraUnitMoveAInspectionMove2 = new System.Windows.Forms.Button();
            this.btnCameraUnitMoveAInspectionMove1 = new System.Windows.Forms.Button();
            this.btnCameraUnitMoveAMCRMove = new System.Windows.Forms.Button();
            this.btnCameraUnitSave = new System.Windows.Forms.Button();
            this.btnCameraUnitSpeedSetting = new System.Windows.Forms.Button();
            this.btnCameraUnitAllSetting = new System.Windows.Forms.Button();
            this.btnCameraUnitLocationSetting = new System.Windows.Forms.Button();
            this.btnCameraUnitMoveLocation = new System.Windows.Forms.Button();
            this.txtCameraUnitLocation = new System.Windows.Forms.TextBox();
            this.txtCameraUnitSpeed = new System.Windows.Forms.TextBox();
            this.txtCameraUnitCurrentLocation = new System.Windows.Forms.TextBox();
            this.txtCameraUnitSelectedShaft = new System.Windows.Forms.TextBox();
            this.lbl_CameraUnit_Location = new System.Windows.Forms.Label();
            this.lbl_CameraUnit_Speed = new System.Windows.Forms.Label();
            this.lbl_CameraUnit_SelectedShaft = new System.Windows.Forms.Label();
            this.txtCameraUnit_LocationSetting = new System.Windows.Forms.TextBox();
            this.lbl_CameraUnit_CurrentLocation = new System.Windows.Forms.Label();
            this.btnCameraUnitGrabSwitch3 = new System.Windows.Forms.Button();
            this.btnCameraUnitGrabSwitch2 = new System.Windows.Forms.Button();
            this.btnCameraUnitGrabSwitch1 = new System.Windows.Forms.Button();
            this.lvCameraUnit = new System.Windows.Forms.ListView();
            this.col_CameraUnit_NamebyLocation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_CameraUnit_Shaft = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_CameraUnit_Location = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_CameraUnit_Speed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ajin_Setting_CameraUnit = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.tp_CellInput = new System.Windows.Forms.TabPage();
            this.gxtCellInput_Buffer = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel66 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCellInputBufferDestructionOn = new System.Windows.Forms.Button();
            this.btnCellInputBufferDestructionOff = new System.Windows.Forms.Button();
            this.btnCellInputBufferPickerUp = new System.Windows.Forms.Button();
            this.btnCellInputBufferPickerDown = new System.Windows.Forms.Button();
            this.btnCellInputBufferPneumaticOn = new System.Windows.Forms.Button();
            this.btnCellInputBufferPneumaticOff = new System.Windows.Forms.Button();
            this.gxtCellInput_Ionizer = new System.Windows.Forms.GroupBox();
            this.btnCellInputDestructionOff = new System.Windows.Forms.Button();
            this.btnCellInputIonizerOff = new System.Windows.Forms.Button();
            this.btnCellInputDestructionOn = new System.Windows.Forms.Button();
            this.btnCellInputIonizerOn = new System.Windows.Forms.Button();
            this.gxtCellInput_Move = new System.Windows.Forms.GroupBox();
            this.gxtCellInput_MoveCenter = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel69 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCellInputMoveCenterABuffer = new System.Windows.Forms.Button();
            this.btnCellInputMoveCenterBBuffer = new System.Windows.Forms.Button();
            this.btnCellInputMoveCenterY1CstMid = new System.Windows.Forms.Button();
            this.btnCellInputMoveCenterY2CstMid = new System.Windows.Forms.Button();
            this.gxtCellInput_MoveB = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel68 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCellInputMoveBX2BUld = new System.Windows.Forms.Button();
            this.btnCellInputMoveBY2BUld = new System.Windows.Forms.Button();
            this.btnCellInputMoveBX2BCstMid = new System.Windows.Forms.Button();
            this.btnCellInputMoveBX2BCstWait = new System.Windows.Forms.Button();
            this.btnCellInputMoveBX1AUld = new System.Windows.Forms.Button();
            this.btnCellInputMoveBY1BUld = new System.Windows.Forms.Button();
            this.btnCellInputMoveBX1BCstMId = new System.Windows.Forms.Button();
            this.btnCellInputMoveBX1BCstWait = new System.Windows.Forms.Button();
            this.gxtCellInput_MoveA = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel67 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCellInputMoveAX2AUld = new System.Windows.Forms.Button();
            this.btnCellInputMoveAY2AUld = new System.Windows.Forms.Button();
            this.btnCellInputMoveAX2ACstMid = new System.Windows.Forms.Button();
            this.btnCellInputMoveAX2ACstWait = new System.Windows.Forms.Button();
            this.btnCellInputMoveAX1AUld = new System.Windows.Forms.Button();
            this.btnCellInputMoveAY1AUld = new System.Windows.Forms.Button();
            this.btnCellInputMoveAX1ACstMid = new System.Windows.Forms.Button();
            this.btnCellInputMoveAX1ACstWait = new System.Windows.Forms.Button();
            this.gxtCellInput_B = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel23 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCellInputBDestructionOn = new System.Windows.Forms.Button();
            this.btnCellInputBDestructionOff = new System.Windows.Forms.Button();
            this.btnCellInputBPneumaticOff = new System.Windows.Forms.Button();
            this.btnCellInputBPneumaticOn = new System.Windows.Forms.Button();
            this.gxtCellInput_A = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel22 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCellInputADestructionOn = new System.Windows.Forms.Button();
            this.btnCellInputADestructionOff = new System.Windows.Forms.Button();
            this.btnCellInputAPneumaticOff = new System.Windows.Forms.Button();
            this.btnCellInputAPneumaticOn = new System.Windows.Forms.Button();
            this.btnCellInputSave = new System.Windows.Forms.Button();
            this.btnCellInputSpeedSetting = new System.Windows.Forms.Button();
            this.btnCellInputAllSetting = new System.Windows.Forms.Button();
            this.btnCellInputLocationSetting = new System.Windows.Forms.Button();
            this.btnCellInputMoveLocation = new System.Windows.Forms.Button();
            this.txtCellInputLocation = new System.Windows.Forms.TextBox();
            this.txtCellInputSpeed = new System.Windows.Forms.TextBox();
            this.txtCellInputCurrentLocation = new System.Windows.Forms.TextBox();
            this.txtCellInputSelectedShaft = new System.Windows.Forms.TextBox();
            this.lbl_CellInput_Location = new System.Windows.Forms.Label();
            this.lbl_CellInput_Speed = new System.Windows.Forms.Label();
            this.lbl_CellInput_SelectedShaft = new System.Windows.Forms.Label();
            this.txtCellInput_LocationSetting = new System.Windows.Forms.TextBox();
            this.lbl_CellInput_CurrentLocation = new System.Windows.Forms.Label();
            this.btnCellInputGrabSwitch3 = new System.Windows.Forms.Button();
            this.btnCellInputGrabSwitch2 = new System.Windows.Forms.Button();
            this.btnCellInputGrabSwitch1 = new System.Windows.Forms.Button();
            this.lvCellInput = new System.Windows.Forms.ListView();
            this.col_CellInput_NamebyLocation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_CellInput_Shaft = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_CellInput_Location = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_CellInput_Speed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ajin_Setting_CellInput = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.tp_CasseteUnload = new System.Windows.Forms.TabPage();
            this.gxtCasseteUnload_Move = new System.Windows.Forms.GroupBox();
            this.gxtCasseteUnload_MoveB = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel75 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel76 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCasseteUnloadMoveBLiftZ2CellOut = new System.Windows.Forms.Button();
            this.btnCasseteUnloadMoveBLiftZ2CstTilt = new System.Windows.Forms.Button();
            this.tableLayoutPanel77 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCasseteUnloadMoveBLiftZ2CstOut = new System.Windows.Forms.Button();
            this.btnCasseteUnloadMoveBLiftZ2CstWait = new System.Windows.Forms.Button();
            this.btnCasseteUnloadMoveBLiftZ2CstIn = new System.Windows.Forms.Button();
            this.tableLayoutPanel78 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCasseteUnloadMoveBBottomT4LiftOut = new System.Windows.Forms.Button();
            this.btnCasseteUnloadMoveBTopT3CstMove = new System.Windows.Forms.Button();
            this.tableLayoutPanel79 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCasseteUnloadMoveBBottomT4CstIn = new System.Windows.Forms.Button();
            this.btnCasseteUnloadMoveBTopT3Out = new System.Windows.Forms.Button();
            this.gxtCasseteUnload_MoveA = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel70 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel71 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCasseteUnloadMoveALiftZ1CellOut = new System.Windows.Forms.Button();
            this.btnCasseteUnloadMoveALiftZ1CstTilt = new System.Windows.Forms.Button();
            this.tableLayoutPanel72 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCasseteUnloadMoveALiftZ1CstOut = new System.Windows.Forms.Button();
            this.btnCasseteUnloadMoveALiftZ1CstWait = new System.Windows.Forms.Button();
            this.btnCasseteUnloadMoveALiftZ1CstIn = new System.Windows.Forms.Button();
            this.tableLayoutPanel73 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCasseteUnloadMoveABottomT2Move = new System.Windows.Forms.Button();
            this.btnCasseteUnloadMoveATopT1Move = new System.Windows.Forms.Button();
            this.tableLayoutPanel74 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCasseteUnloadMoveABottomT2In = new System.Windows.Forms.Button();
            this.btnCasseteUnloadMoveATopT1Out = new System.Windows.Forms.Button();
            this.gxtCasseteUnload_B = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCasseteUnloadBBottomCstStopperDown = new System.Windows.Forms.Button();
            this.btnCasseteUnloadBBottomCstStopperUp = new System.Windows.Forms.Button();
            this.btnCasseteUnloadBBottomCstUngrib = new System.Windows.Forms.Button();
            this.btnCasseteUnloadBBottomCstGrib = new System.Windows.Forms.Button();
            this.btnCasseteUnloadBLiftTiltSlinderDown = new System.Windows.Forms.Button();
            this.btnCasseteUnloadBLiftTiltSlinderUp = new System.Windows.Forms.Button();
            this.btnCasseteUnloadBLiftUngrib = new System.Windows.Forms.Button();
            this.btnCasseteUnloadBLiftGrib = new System.Windows.Forms.Button();
            this.btnCasseteUnloadBTopCstStopperDown = new System.Windows.Forms.Button();
            this.btnCasseteUnloadBTopCstStopperUp = new System.Windows.Forms.Button();
            this.btnCasseteUnloadBTopCstUngrib = new System.Windows.Forms.Button();
            this.btnCasseteUnloadBTopCstGrib = new System.Windows.Forms.Button();
            this.gxtCasseteUnload_A = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCasseteUnloadABottomCstStopperDown = new System.Windows.Forms.Button();
            this.btnCasseteUnloadABottomCstStopperUp = new System.Windows.Forms.Button();
            this.btnCasseteUnloadABottomCstUngrib = new System.Windows.Forms.Button();
            this.btnCasseteUnloadABottomCstGrib = new System.Windows.Forms.Button();
            this.btnCasseteUnloadALiftTiltSlinderDown = new System.Windows.Forms.Button();
            this.btnCasseteUnloadALiftTiltSlinderUp = new System.Windows.Forms.Button();
            this.btnCasseteUnloadALiftUngrib = new System.Windows.Forms.Button();
            this.btnCasseteUnloadALiftGrib = new System.Windows.Forms.Button();
            this.btnCasseteUnloadATopCstStopperDown = new System.Windows.Forms.Button();
            this.btnCasseteUnloadATopCstStopperUp = new System.Windows.Forms.Button();
            this.btnCasseteUnloadATopCstUngrib = new System.Windows.Forms.Button();
            this.btnCasseteUnloadATopCstGrib = new System.Windows.Forms.Button();
            this.btnCasseteUnloadSave = new System.Windows.Forms.Button();
            this.gxtCasseteUnload = new System.Windows.Forms.GroupBox();
            this.btnCasseteUnloadMuting4 = new System.Windows.Forms.Button();
            this.btnCasseteUnloadMuting2 = new System.Windows.Forms.Button();
            this.btnCasseteUnloadMuting3 = new System.Windows.Forms.Button();
            this.btnCasseteUnloadMuting1 = new System.Windows.Forms.Button();
            this.btnCasseteUnloadMutingOut = new System.Windows.Forms.Button();
            this.btncasseteUnloadMutingIn = new System.Windows.Forms.Button();
            this.btnCasseteUnloadSpeedSetting = new System.Windows.Forms.Button();
            this.btnCasseteUnloadAllSetting = new System.Windows.Forms.Button();
            this.btnCasseteUnloadLocationSetting = new System.Windows.Forms.Button();
            this.btnCasseteUnloadMoveLocation = new System.Windows.Forms.Button();
            this.txtCasseteUnloadLocation = new System.Windows.Forms.TextBox();
            this.txtCasseteUnloadSpeed = new System.Windows.Forms.TextBox();
            this.txtCasseteUnloadCurrentLocation = new System.Windows.Forms.TextBox();
            this.txtCasseteUnload_SelectedShaft = new System.Windows.Forms.TextBox();
            this.lbl_CasseteUnload_Location = new System.Windows.Forms.Label();
            this.lbl_CasseteUnload_Speed = new System.Windows.Forms.Label();
            this.lbl_CasseteUnload_SelectedShaft = new System.Windows.Forms.Label();
            this.txtCasseteUnload_LocationSetting = new System.Windows.Forms.TextBox();
            this.lbl_CasseteUnload_CurrentLocation = new System.Windows.Forms.Label();
            this.btncasseteUnloadGrabSwitch3 = new System.Windows.Forms.Button();
            this.btnCasseteUnloadGrabSwitch2 = new System.Windows.Forms.Button();
            this.btnCasseteUnloadGrabSwitch1 = new System.Windows.Forms.Button();
            this.lvCasseteUnload = new System.Windows.Forms.ListView();
            this.col_CasseteUnload_NamebyLocation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_CasseteUnload_Shaft = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_CasseteUnload_Location = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_CasseteUnload_Speed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ajin_Setting_CasseteUnload = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.tc_iostatus_info.SuspendLayout();
            this.tp_CasseteLoad.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.gxtCasseteLoad_Move.SuspendLayout();
            this.gxtCasseteLoad_MoveB.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            this.gxtCasseteLoad_MoveA.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.gxtCasseteLoad_B.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.gxtCasseteLoad_Muting.SuspendLayout();
            this.tp_Cellpurge.SuspendLayout();
            this.gxtCellpurge_Move.SuspendLayout();
            this.gxtCellpurge_MoveCenter.SuspendLayout();
            this.tableLayoutPanel21.SuspendLayout();
            this.gxtCellpurge_MoveB.SuspendLayout();
            this.tableLayoutPanel20.SuspendLayout();
            this.gxtCellpurge_MoveA.SuspendLayout();
            this.tableLayoutPanel19.SuspendLayout();
            this.gxtCellpurge_B.SuspendLayout();
            this.tableLayoutPanel18.SuspendLayout();
            this.gxtCellpurge_A.SuspendLayout();
            this.tableLayoutPanel17.SuspendLayout();
            this.gxtCellpurge_Ionizer.SuspendLayout();
            this.tp_CellLoad.SuspendLayout();
            this.gxtCellLoad_Move.SuspendLayout();
            this.gxtCellLoad_MoveLorUn.SuspendLayout();
            this.tableLayoutPanel28.SuspendLayout();
            this.gxtCellLoad_MoveB.SuspendLayout();
            this.tableLayoutPanel31.SuspendLayout();
            this.tableLayoutPanel32.SuspendLayout();
            this.tableLayoutPanel33.SuspendLayout();
            this.tableLayoutPanel34.SuspendLayout();
            this.gxtCellLoad_MoveA.SuspendLayout();
            this.tableLayoutPanel26.SuspendLayout();
            this.tableLayoutPanel30.SuspendLayout();
            this.tableLayoutPanel29.SuspendLayout();
            this.tableLayoutPanel27.SuspendLayout();
            this.gxtCellLoad_B.SuspendLayout();
            this.tableLayoutPanel25.SuspendLayout();
            this.gxtCellLoad_A.SuspendLayout();
            this.tableLayoutPanel24.SuspendLayout();
            this.tp_IRCutProcess.SuspendLayout();
            this.gxtIRCutProcess_Ionizer.SuspendLayout();
            this.gxtIRCutProcess_Move.SuspendLayout();
            this.gxtIRCutProcess_RightOffset.SuspendLayout();
            this.tableLayoutPanel40.SuspendLayout();
            this.gxtIRCutProcess_LeftOffset.SuspendLayout();
            this.tableLayoutPanel39.SuspendLayout();
            this.gxtIRCutProcess_MoveB.SuspendLayout();
            this.tableLayoutPanel38.SuspendLayout();
            this.gxtIRCutProcess_MoveA.SuspendLayout();
            this.tableLayoutPanel37.SuspendLayout();
            this.gxtIRCutProcess_B.SuspendLayout();
            this.tableLayoutPanel36.SuspendLayout();
            this.gxtIRCutProcess_A.SuspendLayout();
            this.tableLayoutPanel35.SuspendLayout();
            this.tp_BreakTransfer.SuspendLayout();
            this.gxtBreakTransfer_Move.SuspendLayout();
            this.gxtBreakTransfer_MoveB.SuspendLayout();
            this.tableLayoutPanel44.SuspendLayout();
            this.gxtBreakTransfer_MoveA.SuspendLayout();
            this.tableLayoutPanel43.SuspendLayout();
            this.gxtBreakTransfer_B.SuspendLayout();
            this.tableLayoutPanel42.SuspendLayout();
            this.gxtBreakTransfer_A.SuspendLayout();
            this.tableLayoutPanel41.SuspendLayout();
            this.tp_BreakUnitXZ.SuspendLayout();
            this.gxtBreakUnitXZ_Move.SuspendLayout();
            this.tableLayoutPanel45.SuspendLayout();
            this.gxtBreakUnitXZ_MoveZ4.SuspendLayout();
            this.tableLayoutPanel49.SuspendLayout();
            this.gxtBreakUnitXZ_MoveZ3.SuspendLayout();
            this.tableLayoutPanel48.SuspendLayout();
            this.gxtBreakUnitXZ_MoveZ2.SuspendLayout();
            this.tableLayoutPanel47.SuspendLayout();
            this.gxtBreakUnitXZ_MoveZ1.SuspendLayout();
            this.tableLayoutPanel46.SuspendLayout();
            this.tp_BreakUnitTY.SuspendLayout();
            this.gxtBreakUnitTY_Shutter.SuspendLayout();
            this.tableLayoutPanel53.SuspendLayout();
            this.gxtBreakUnitTY_DummyBox.SuspendLayout();
            this.tableLayoutPanel52.SuspendLayout();
            this.gxtBreakUnitTY_Ionizer.SuspendLayout();
            this.gxtBreakUnitTY_Move.SuspendLayout();
            this.tableLayoutPanel54.SuspendLayout();
            this.gxtBreakUnitTY_Theta.SuspendLayout();
            this.tableLayoutPanel57.SuspendLayout();
            this.gxtBreakUnitTY_MoveB.SuspendLayout();
            this.tableLayoutPanel56.SuspendLayout();
            this.gxtBreakUnitTY_MoveA.SuspendLayout();
            this.tableLayoutPanel55.SuspendLayout();
            this.gxtBreakUnitTY_B.SuspendLayout();
            this.tableLayoutPanel50.SuspendLayout();
            this.gxtBreakUnitTY_A.SuspendLayout();
            this.tableLayoutPanel51.SuspendLayout();
            this.tp_UnloaderTransfer.SuspendLayout();
            this.gxtUnloaderTransfer_Move.SuspendLayout();
            this.gxtUnloaderTransfer_MoveB.SuspendLayout();
            this.tableLayoutPanel61.SuspendLayout();
            this.tableLayoutPanel62.SuspendLayout();
            this.tableLayoutPanel63.SuspendLayout();
            this.gxtUnloaderTransfer_MoveA.SuspendLayout();
            this.tableLayoutPanel58.SuspendLayout();
            this.tableLayoutPanel60.SuspendLayout();
            this.tableLayoutPanel59.SuspendLayout();
            this.gxtUnloaderTransfer_B.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.gxtUnloaderTransfer_A.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tp_CameraUnit.SuspendLayout();
            this.gxtCameraUnit_Move.SuspendLayout();
            this.gxtCameraUnit_MoveB.SuspendLayout();
            this.tableLayoutPanel65.SuspendLayout();
            this.gxtCameraUnit_MoveA.SuspendLayout();
            this.tableLayoutPanel64.SuspendLayout();
            this.tp_CellInput.SuspendLayout();
            this.gxtCellInput_Buffer.SuspendLayout();
            this.tableLayoutPanel66.SuspendLayout();
            this.gxtCellInput_Ionizer.SuspendLayout();
            this.gxtCellInput_Move.SuspendLayout();
            this.gxtCellInput_MoveCenter.SuspendLayout();
            this.tableLayoutPanel69.SuspendLayout();
            this.gxtCellInput_MoveB.SuspendLayout();
            this.tableLayoutPanel68.SuspendLayout();
            this.gxtCellInput_MoveA.SuspendLayout();
            this.tableLayoutPanel67.SuspendLayout();
            this.gxtCellInput_B.SuspendLayout();
            this.tableLayoutPanel23.SuspendLayout();
            this.gxtCellInput_A.SuspendLayout();
            this.tableLayoutPanel22.SuspendLayout();
            this.tp_CasseteUnload.SuspendLayout();
            this.gxtCasseteUnload_Move.SuspendLayout();
            this.gxtCasseteUnload_MoveB.SuspendLayout();
            this.tableLayoutPanel75.SuspendLayout();
            this.tableLayoutPanel76.SuspendLayout();
            this.tableLayoutPanel77.SuspendLayout();
            this.tableLayoutPanel78.SuspendLayout();
            this.tableLayoutPanel79.SuspendLayout();
            this.gxtCasseteUnload_MoveA.SuspendLayout();
            this.tableLayoutPanel70.SuspendLayout();
            this.tableLayoutPanel71.SuspendLayout();
            this.tableLayoutPanel72.SuspendLayout();
            this.tableLayoutPanel73.SuspendLayout();
            this.tableLayoutPanel74.SuspendLayout();
            this.gxtCasseteUnload_B.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.gxtCasseteUnload_A.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.gxtCasseteUnload.SuspendLayout();
            this.SuspendLayout();
            // 
            // tc_iostatus_info
            // 
            this.tc_iostatus_info.Controls.Add(this.tp_CasseteLoad);
            this.tc_iostatus_info.Controls.Add(this.tp_Cellpurge);
            this.tc_iostatus_info.Controls.Add(this.tp_CellLoad);
            this.tc_iostatus_info.Controls.Add(this.tp_IRCutProcess);
            this.tc_iostatus_info.Controls.Add(this.tp_BreakTransfer);
            this.tc_iostatus_info.Controls.Add(this.tp_BreakUnitXZ);
            this.tc_iostatus_info.Controls.Add(this.tp_BreakUnitTY);
            this.tc_iostatus_info.Controls.Add(this.tp_UnloaderTransfer);
            this.tc_iostatus_info.Controls.Add(this.tp_CameraUnit);
            this.tc_iostatus_info.Controls.Add(this.tp_CellInput);
            this.tc_iostatus_info.Controls.Add(this.tp_CasseteUnload);
            this.tc_iostatus_info.ItemSize = new System.Drawing.Size(156, 30);
            this.tc_iostatus_info.Location = new System.Drawing.Point(3, 10);
            this.tc_iostatus_info.Name = "tc_iostatus_info";
            this.tc_iostatus_info.SelectedIndex = 0;
            this.tc_iostatus_info.Size = new System.Drawing.Size(1734, 854);
            this.tc_iostatus_info.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tc_iostatus_info.TabIndex = 0;
            // 
            // tp_CasseteLoad
            // 
            this.tp_CasseteLoad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tp_CasseteLoad.Controls.Add(this.tableLayoutPanel1);
            this.tp_CasseteLoad.Controls.Add(this.gxtCasseteLoad_Move);
            this.tp_CasseteLoad.Controls.Add(this.gxtCasseteLoad_B);
            this.tp_CasseteLoad.Controls.Add(this.gxtCasseteLoad_A);
            this.tp_CasseteLoad.Controls.Add(this.btnCasseteLoadSave);
            this.tp_CasseteLoad.Controls.Add(this.gxtCasseteLoad_Muting);
            this.tp_CasseteLoad.Controls.Add(this.btnCasseteLoadSpeedSetting);
            this.tp_CasseteLoad.Controls.Add(this.btnCasseteLoadAllSetting);
            this.tp_CasseteLoad.Controls.Add(this.btnCasseteLoadLocationSetting);
            this.tp_CasseteLoad.Controls.Add(this.btnCasseteLoadMoveLocation);
            this.tp_CasseteLoad.Controls.Add(this.txtCasseteLoadLocation);
            this.tp_CasseteLoad.Controls.Add(this.txtCasseteLoadSpeed);
            this.tp_CasseteLoad.Controls.Add(this.txtCasseteLoadCurrentLocation);
            this.tp_CasseteLoad.Controls.Add(this.txtCasseteLoadSelectedShaft);
            this.tp_CasseteLoad.Controls.Add(this.lbl_CasseteLoad_Location);
            this.tp_CasseteLoad.Controls.Add(this.lbl_CasseteLoad_Speed);
            this.tp_CasseteLoad.Controls.Add(this.lbl_CasseteLoad_SelectedShaft);
            this.tp_CasseteLoad.Controls.Add(this.txtCasseteLoad_LocationSetting);
            this.tp_CasseteLoad.Controls.Add(this.lbl_CasseteLoad_CurrentLocation);
            this.tp_CasseteLoad.Controls.Add(this.btnCasseteLoadGrabSwitch3);
            this.tp_CasseteLoad.Controls.Add(this.btnCasseteLoadGrabSwitch2);
            this.tp_CasseteLoad.Controls.Add(this.btnCasseteLoadGrabSwitch1);
            this.tp_CasseteLoad.Controls.Add(this.lvCasseteLoad);
            this.tp_CasseteLoad.Controls.Add(this.ajin_Setting_CasseteLoad);
            this.tp_CasseteLoad.Location = new System.Drawing.Point(4, 34);
            this.tp_CasseteLoad.Name = "tp_CasseteLoad";
            this.tp_CasseteLoad.Size = new System.Drawing.Size(1726, 816);
            this.tp_CasseteLoad.TabIndex = 0;
            this.tp_CasseteLoad.Text = "카세트 로드";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Controls.Add(this.btnCasseteLoadABCSD, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.btnCasseteLoadABCSU, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.btnCasseteLoadABCUG, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.btnCasseteLoadABCG, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.btnCasseteLoadALTSD, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnCasseteLoadALTSU, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnCasseteLoadALUG, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnCasseteLoadALG, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnCasseteLoadATCSD, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnCasseteLoadATCSU, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnCasseteLoadATCUG, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnCasseteLoadATCG, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(798, 438);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(437, 145);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // btnCasseteLoadABCSD
            // 
            this.btnCasseteLoadABCSD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadABCSD.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadABCSD.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadABCSD.Location = new System.Drawing.Point(330, 99);
            this.btnCasseteLoadABCSD.Name = "btnCasseteLoadABCSD";
            this.btnCasseteLoadABCSD.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteLoadABCSD.TabIndex = 31;
            this.btnCasseteLoadABCSD.Text = "하단 카세트\r\n스토퍼 다운";
            this.btnCasseteLoadABCSD.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadABCSU
            // 
            this.btnCasseteLoadABCSU.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadABCSU.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadABCSU.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadABCSU.Location = new System.Drawing.Point(221, 99);
            this.btnCasseteLoadABCSU.Name = "btnCasseteLoadABCSU";
            this.btnCasseteLoadABCSU.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteLoadABCSU.TabIndex = 30;
            this.btnCasseteLoadABCSU.Text = "하단 카세트\r\n스토퍼 업";
            this.btnCasseteLoadABCSU.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadABCUG
            // 
            this.btnCasseteLoadABCUG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadABCUG.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadABCUG.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadABCUG.Location = new System.Drawing.Point(112, 99);
            this.btnCasseteLoadABCUG.Name = "btnCasseteLoadABCUG";
            this.btnCasseteLoadABCUG.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteLoadABCUG.TabIndex = 29;
            this.btnCasseteLoadABCUG.Text = "하단 카세트\r\n언그립";
            this.btnCasseteLoadABCUG.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadABCG
            // 
            this.btnCasseteLoadABCG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadABCG.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadABCG.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadABCG.Location = new System.Drawing.Point(3, 99);
            this.btnCasseteLoadABCG.Name = "btnCasseteLoadABCG";
            this.btnCasseteLoadABCG.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteLoadABCG.TabIndex = 28;
            this.btnCasseteLoadABCG.Text = "하단 카세트\r\n그립";
            this.btnCasseteLoadABCG.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadALTSD
            // 
            this.btnCasseteLoadALTSD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadALTSD.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadALTSD.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadALTSD.Location = new System.Drawing.Point(330, 51);
            this.btnCasseteLoadALTSD.Name = "btnCasseteLoadALTSD";
            this.btnCasseteLoadALTSD.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteLoadALTSD.TabIndex = 27;
            this.btnCasseteLoadALTSD.Text = "리프트 틸트\r\n실린더 다운";
            this.btnCasseteLoadALTSD.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadALTSU
            // 
            this.btnCasseteLoadALTSU.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadALTSU.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadALTSU.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadALTSU.Location = new System.Drawing.Point(221, 51);
            this.btnCasseteLoadALTSU.Name = "btnCasseteLoadALTSU";
            this.btnCasseteLoadALTSU.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteLoadALTSU.TabIndex = 26;
            this.btnCasseteLoadALTSU.Text = "리프트 틸트\r\n실린더 업";
            this.btnCasseteLoadALTSU.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadALUG
            // 
            this.btnCasseteLoadALUG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadALUG.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadALUG.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadALUG.Location = new System.Drawing.Point(112, 51);
            this.btnCasseteLoadALUG.Name = "btnCasseteLoadALUG";
            this.btnCasseteLoadALUG.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteLoadALUG.TabIndex = 25;
            this.btnCasseteLoadALUG.Text = "리프트\r\n언그립";
            this.btnCasseteLoadALUG.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadALG
            // 
            this.btnCasseteLoadALG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadALG.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadALG.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadALG.Location = new System.Drawing.Point(3, 51);
            this.btnCasseteLoadALG.Name = "btnCasseteLoadALG";
            this.btnCasseteLoadALG.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteLoadALG.TabIndex = 24;
            this.btnCasseteLoadALG.Text = "리프트\r\n그립";
            this.btnCasseteLoadALG.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadATCSD
            // 
            this.btnCasseteLoadATCSD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadATCSD.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadATCSD.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadATCSD.Location = new System.Drawing.Point(330, 3);
            this.btnCasseteLoadATCSD.Name = "btnCasseteLoadATCSD";
            this.btnCasseteLoadATCSD.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteLoadATCSD.TabIndex = 23;
            this.btnCasseteLoadATCSD.Text = "상단 카세트\r\n스토퍼 다운";
            this.btnCasseteLoadATCSD.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadATCSU
            // 
            this.btnCasseteLoadATCSU.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadATCSU.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadATCSU.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadATCSU.Location = new System.Drawing.Point(221, 3);
            this.btnCasseteLoadATCSU.Name = "btnCasseteLoadATCSU";
            this.btnCasseteLoadATCSU.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteLoadATCSU.TabIndex = 22;
            this.btnCasseteLoadATCSU.Text = "상단 카세트\r\n스토퍼 업";
            this.btnCasseteLoadATCSU.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadATCUG
            // 
            this.btnCasseteLoadATCUG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadATCUG.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadATCUG.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadATCUG.Location = new System.Drawing.Point(112, 3);
            this.btnCasseteLoadATCUG.Name = "btnCasseteLoadATCUG";
            this.btnCasseteLoadATCUG.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteLoadATCUG.TabIndex = 21;
            this.btnCasseteLoadATCUG.Text = "상단 카세트\r\n언그립";
            this.btnCasseteLoadATCUG.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadATCG
            // 
            this.btnCasseteLoadATCG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadATCG.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadATCG.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadATCG.Location = new System.Drawing.Point(3, 3);
            this.btnCasseteLoadATCG.Name = "btnCasseteLoadATCG";
            this.btnCasseteLoadATCG.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteLoadATCG.TabIndex = 20;
            this.btnCasseteLoadATCG.Text = "상단 카세트\r\n그립";
            this.btnCasseteLoadATCG.UseVisualStyleBackColor = false;
            // 
            // gxtCasseteLoad_Move
            // 
            this.gxtCasseteLoad_Move.Controls.Add(this.gxtCasseteLoad_MoveB);
            this.gxtCasseteLoad_Move.Controls.Add(this.gxtCasseteLoad_MoveA);
            this.gxtCasseteLoad_Move.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCasseteLoad_Move.ForeColor = System.Drawing.Color.White;
            this.gxtCasseteLoad_Move.Location = new System.Drawing.Point(792, 595);
            this.gxtCasseteLoad_Move.Name = "gxtCasseteLoad_Move";
            this.gxtCasseteLoad_Move.Size = new System.Drawing.Size(904, 179);
            this.gxtCasseteLoad_Move.TabIndex = 44;
            this.gxtCasseteLoad_Move.TabStop = false;
            this.gxtCasseteLoad_Move.Text = "     이동     ";
            // 
            // gxtCasseteLoad_MoveB
            // 
            this.gxtCasseteLoad_MoveB.Controls.Add(this.tableLayoutPanel12);
            this.gxtCasseteLoad_MoveB.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCasseteLoad_MoveB.ForeColor = System.Drawing.Color.White;
            this.gxtCasseteLoad_MoveB.Location = new System.Drawing.Point(455, 21);
            this.gxtCasseteLoad_MoveB.Name = "gxtCasseteLoad_MoveB";
            this.gxtCasseteLoad_MoveB.Size = new System.Drawing.Size(443, 152);
            this.gxtCasseteLoad_MoveB.TabIndex = 53;
            this.gxtCasseteLoad_MoveB.TabStop = false;
            this.gxtCasseteLoad_MoveB.Text = "     B    ";
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 4;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel12.Controls.Add(this.tableLayoutPanel13, 3, 0);
            this.tableLayoutPanel12.Controls.Add(this.tableLayoutPanel14, 2, 0);
            this.tableLayoutPanel12.Controls.Add(this.tableLayoutPanel15, 1, 0);
            this.tableLayoutPanel12.Controls.Add(this.tableLayoutPanel16, 0, 0);
            this.tableLayoutPanel12.Location = new System.Drawing.Point(6, 21);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 1;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(436, 124);
            this.tableLayoutPanel12.TabIndex = 1;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 1;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.Controls.Add(this.btnCasseteLoadMoveBZ2CellOut, 0, 1);
            this.tableLayoutPanel13.Controls.Add(this.btnCasseteLoadMoveBZ2Tilt, 0, 0);
            this.tableLayoutPanel13.Location = new System.Drawing.Point(330, 3);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 2;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel13.TabIndex = 3;
            // 
            // btnCasseteLoadMoveBZ2CellOut
            // 
            this.btnCasseteLoadMoveBZ2CellOut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadMoveBZ2CellOut.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadMoveBZ2CellOut.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadMoveBZ2CellOut.Location = new System.Drawing.Point(3, 62);
            this.btnCasseteLoadMoveBZ2CellOut.Name = "btnCasseteLoadMoveBZ2CellOut";
            this.btnCasseteLoadMoveBZ2CellOut.Size = new System.Drawing.Size(97, 53);
            this.btnCasseteLoadMoveBZ2CellOut.TabIndex = 62;
            this.btnCasseteLoadMoveBZ2CellOut.Text = "리프트 Z2\r\n셀 배출 시작";
            this.btnCasseteLoadMoveBZ2CellOut.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadMoveBZ2Tilt
            // 
            this.btnCasseteLoadMoveBZ2Tilt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadMoveBZ2Tilt.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadMoveBZ2Tilt.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadMoveBZ2Tilt.Location = new System.Drawing.Point(3, 3);
            this.btnCasseteLoadMoveBZ2Tilt.Name = "btnCasseteLoadMoveBZ2Tilt";
            this.btnCasseteLoadMoveBZ2Tilt.Size = new System.Drawing.Size(97, 53);
            this.btnCasseteLoadMoveBZ2Tilt.TabIndex = 57;
            this.btnCasseteLoadMoveBZ2Tilt.Text = "리프트 Z2\r\n틸트 측정";
            this.btnCasseteLoadMoveBZ2Tilt.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 1;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel14.Controls.Add(this.btnCasseteLoadMoveBZ2Out, 0, 2);
            this.tableLayoutPanel14.Controls.Add(this.btnCasseteLoadMoveBZ2InWait, 0, 0);
            this.tableLayoutPanel14.Controls.Add(this.btnCasseteLoadMoveBZ2In, 0, 1);
            this.tableLayoutPanel14.Location = new System.Drawing.Point(221, 3);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 3;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel14.TabIndex = 2;
            // 
            // btnCasseteLoadMoveBZ2Out
            // 
            this.btnCasseteLoadMoveBZ2Out.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadMoveBZ2Out.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadMoveBZ2Out.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadMoveBZ2Out.Location = new System.Drawing.Point(3, 81);
            this.btnCasseteLoadMoveBZ2Out.Name = "btnCasseteLoadMoveBZ2Out";
            this.btnCasseteLoadMoveBZ2Out.Size = new System.Drawing.Size(97, 34);
            this.btnCasseteLoadMoveBZ2Out.TabIndex = 61;
            this.btnCasseteLoadMoveBZ2Out.Text = "리프트 Z2\r\n카세트 배출";
            this.btnCasseteLoadMoveBZ2Out.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadMoveBZ2InWait
            // 
            this.btnCasseteLoadMoveBZ2InWait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadMoveBZ2InWait.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadMoveBZ2InWait.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadMoveBZ2InWait.Location = new System.Drawing.Point(3, 3);
            this.btnCasseteLoadMoveBZ2InWait.Name = "btnCasseteLoadMoveBZ2InWait";
            this.btnCasseteLoadMoveBZ2InWait.Size = new System.Drawing.Size(97, 33);
            this.btnCasseteLoadMoveBZ2InWait.TabIndex = 56;
            this.btnCasseteLoadMoveBZ2InWait.Text = "카세트 Z2\r\n투입 대기";
            this.btnCasseteLoadMoveBZ2InWait.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadMoveBZ2In
            // 
            this.btnCasseteLoadMoveBZ2In.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadMoveBZ2In.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadMoveBZ2In.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadMoveBZ2In.Location = new System.Drawing.Point(3, 42);
            this.btnCasseteLoadMoveBZ2In.Name = "btnCasseteLoadMoveBZ2In";
            this.btnCasseteLoadMoveBZ2In.Size = new System.Drawing.Size(97, 33);
            this.btnCasseteLoadMoveBZ2In.TabIndex = 60;
            this.btnCasseteLoadMoveBZ2In.Text = "카세트 Z2\r\n카세트 투입";
            this.btnCasseteLoadMoveBZ2In.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 1;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.Controls.Add(this.btnCasseteLoadMoveBT4Out, 0, 1);
            this.tableLayoutPanel15.Controls.Add(this.btnCasseteLoadMoveBT3Move, 0, 0);
            this.tableLayoutPanel15.Location = new System.Drawing.Point(112, 3);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 2;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel15.TabIndex = 1;
            // 
            // btnCasseteLoadMoveBT4Out
            // 
            this.btnCasseteLoadMoveBT4Out.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadMoveBT4Out.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadMoveBT4Out.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadMoveBT4Out.Location = new System.Drawing.Point(3, 62);
            this.btnCasseteLoadMoveBT4Out.Name = "btnCasseteLoadMoveBT4Out";
            this.btnCasseteLoadMoveBT4Out.Size = new System.Drawing.Size(97, 53);
            this.btnCasseteLoadMoveBT4Out.TabIndex = 59;
            this.btnCasseteLoadMoveBT4Out.Text = "하부 카세트 T4\r\n카세트 배출";
            this.btnCasseteLoadMoveBT4Out.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadMoveBT3Move
            // 
            this.btnCasseteLoadMoveBT3Move.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadMoveBT3Move.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadMoveBT3Move.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadMoveBT3Move.Location = new System.Drawing.Point(3, 3);
            this.btnCasseteLoadMoveBT3Move.Name = "btnCasseteLoadMoveBT3Move";
            this.btnCasseteLoadMoveBT3Move.Size = new System.Drawing.Size(97, 53);
            this.btnCasseteLoadMoveBT3Move.TabIndex = 55;
            this.btnCasseteLoadMoveBT3Move.Text = "상부 카세트 T3\r\n리프트 이동";
            this.btnCasseteLoadMoveBT3Move.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.ColumnCount = 1;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.Controls.Add(this.btnCasseteLoadMoveBT4Move, 0, 1);
            this.tableLayoutPanel16.Controls.Add(this.btnCasseteLoadMoveBT3In, 0, 0);
            this.tableLayoutPanel16.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 2;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel16.TabIndex = 0;
            // 
            // btnCasseteLoadMoveBT4Move
            // 
            this.btnCasseteLoadMoveBT4Move.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadMoveBT4Move.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadMoveBT4Move.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadMoveBT4Move.Location = new System.Drawing.Point(3, 62);
            this.btnCasseteLoadMoveBT4Move.Name = "btnCasseteLoadMoveBT4Move";
            this.btnCasseteLoadMoveBT4Move.Size = new System.Drawing.Size(97, 53);
            this.btnCasseteLoadMoveBT4Move.TabIndex = 58;
            this.btnCasseteLoadMoveBT4Move.Text = "하부 카세트 T4\r\n카세트 이동";
            this.btnCasseteLoadMoveBT4Move.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadMoveBT3In
            // 
            this.btnCasseteLoadMoveBT3In.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadMoveBT3In.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadMoveBT3In.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadMoveBT3In.Location = new System.Drawing.Point(3, 3);
            this.btnCasseteLoadMoveBT3In.Name = "btnCasseteLoadMoveBT3In";
            this.btnCasseteLoadMoveBT3In.Size = new System.Drawing.Size(97, 53);
            this.btnCasseteLoadMoveBT3In.TabIndex = 54;
            this.btnCasseteLoadMoveBT3In.Text = "상부 카세트 T3\r\n카세트 투입";
            this.btnCasseteLoadMoveBT3In.UseVisualStyleBackColor = false;
            // 
            // gxtCasseteLoad_MoveA
            // 
            this.gxtCasseteLoad_MoveA.Controls.Add(this.tableLayoutPanel7);
            this.gxtCasseteLoad_MoveA.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCasseteLoad_MoveA.ForeColor = System.Drawing.Color.White;
            this.gxtCasseteLoad_MoveA.Location = new System.Drawing.Point(6, 21);
            this.gxtCasseteLoad_MoveA.Name = "gxtCasseteLoad_MoveA";
            this.gxtCasseteLoad_MoveA.Size = new System.Drawing.Size(443, 152);
            this.gxtCasseteLoad_MoveA.TabIndex = 44;
            this.gxtCasseteLoad_MoveA.TabStop = false;
            this.gxtCasseteLoad_MoveA.Text = "     A    ";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 4;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel11, 3, 0);
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel10, 2, 0);
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel9, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel8, 0, 0);
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 21);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(437, 124);
            this.tableLayoutPanel7.TabIndex = 0;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 1;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.Controls.Add(this.btnCasseteLoadMoveAZ1CellOut, 0, 1);
            this.tableLayoutPanel11.Controls.Add(this.btnCasseteLoadMoveAZ1Tilt, 0, 0);
            this.tableLayoutPanel11.Location = new System.Drawing.Point(330, 3);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 2;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel11.TabIndex = 3;
            // 
            // btnCasseteLoadMoveAZ1CellOut
            // 
            this.btnCasseteLoadMoveAZ1CellOut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadMoveAZ1CellOut.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadMoveAZ1CellOut.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadMoveAZ1CellOut.Location = new System.Drawing.Point(3, 62);
            this.btnCasseteLoadMoveAZ1CellOut.Name = "btnCasseteLoadMoveAZ1CellOut";
            this.btnCasseteLoadMoveAZ1CellOut.Size = new System.Drawing.Size(97, 53);
            this.btnCasseteLoadMoveAZ1CellOut.TabIndex = 52;
            this.btnCasseteLoadMoveAZ1CellOut.Text = "리프트 Z1\r\n셀 배출 시작";
            this.btnCasseteLoadMoveAZ1CellOut.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadMoveAZ1Tilt
            // 
            this.btnCasseteLoadMoveAZ1Tilt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadMoveAZ1Tilt.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadMoveAZ1Tilt.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadMoveAZ1Tilt.Location = new System.Drawing.Point(3, 3);
            this.btnCasseteLoadMoveAZ1Tilt.Name = "btnCasseteLoadMoveAZ1Tilt";
            this.btnCasseteLoadMoveAZ1Tilt.Size = new System.Drawing.Size(97, 53);
            this.btnCasseteLoadMoveAZ1Tilt.TabIndex = 47;
            this.btnCasseteLoadMoveAZ1Tilt.Text = "리프트 Z1\r\n틸트 측정";
            this.btnCasseteLoadMoveAZ1Tilt.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 1;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Controls.Add(this.btnCasseteLoadMoveAZ1Out, 0, 2);
            this.tableLayoutPanel10.Controls.Add(this.btnCasseteLoadMoveAZ1InWait, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.btnCasseteLoadMoveAZ1In, 0, 1);
            this.tableLayoutPanel10.Location = new System.Drawing.Point(221, 3);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 3;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel10.TabIndex = 2;
            // 
            // btnCasseteLoadMoveAZ1Out
            // 
            this.btnCasseteLoadMoveAZ1Out.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadMoveAZ1Out.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadMoveAZ1Out.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadMoveAZ1Out.Location = new System.Drawing.Point(3, 81);
            this.btnCasseteLoadMoveAZ1Out.Name = "btnCasseteLoadMoveAZ1Out";
            this.btnCasseteLoadMoveAZ1Out.Size = new System.Drawing.Size(97, 34);
            this.btnCasseteLoadMoveAZ1Out.TabIndex = 51;
            this.btnCasseteLoadMoveAZ1Out.Text = "리프트 Z1\r\n카세트 배출";
            this.btnCasseteLoadMoveAZ1Out.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadMoveAZ1InWait
            // 
            this.btnCasseteLoadMoveAZ1InWait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadMoveAZ1InWait.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadMoveAZ1InWait.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadMoveAZ1InWait.Location = new System.Drawing.Point(3, 3);
            this.btnCasseteLoadMoveAZ1InWait.Name = "btnCasseteLoadMoveAZ1InWait";
            this.btnCasseteLoadMoveAZ1InWait.Size = new System.Drawing.Size(97, 33);
            this.btnCasseteLoadMoveAZ1InWait.TabIndex = 46;
            this.btnCasseteLoadMoveAZ1InWait.Text = "카세트 Z1\r\n투입 대기";
            this.btnCasseteLoadMoveAZ1InWait.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadMoveAZ1In
            // 
            this.btnCasseteLoadMoveAZ1In.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadMoveAZ1In.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadMoveAZ1In.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadMoveAZ1In.Location = new System.Drawing.Point(3, 42);
            this.btnCasseteLoadMoveAZ1In.Name = "btnCasseteLoadMoveAZ1In";
            this.btnCasseteLoadMoveAZ1In.Size = new System.Drawing.Size(97, 33);
            this.btnCasseteLoadMoveAZ1In.TabIndex = 50;
            this.btnCasseteLoadMoveAZ1In.Text = "카세트 Z1\r\n카세트 투입";
            this.btnCasseteLoadMoveAZ1In.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 1;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Controls.Add(this.btnCasseteLoadMoveAT2Out, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.btnCasseteLoadMoveAT1Move, 0, 0);
            this.tableLayoutPanel9.Location = new System.Drawing.Point(112, 3);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 2;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel9.TabIndex = 1;
            // 
            // btnCasseteLoadMoveAT2Out
            // 
            this.btnCasseteLoadMoveAT2Out.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadMoveAT2Out.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadMoveAT2Out.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadMoveAT2Out.Location = new System.Drawing.Point(3, 62);
            this.btnCasseteLoadMoveAT2Out.Name = "btnCasseteLoadMoveAT2Out";
            this.btnCasseteLoadMoveAT2Out.Size = new System.Drawing.Size(97, 53);
            this.btnCasseteLoadMoveAT2Out.TabIndex = 49;
            this.btnCasseteLoadMoveAT2Out.Text = "하부 카세트 T2\r\n카세트 배출";
            this.btnCasseteLoadMoveAT2Out.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadMoveAT1Move
            // 
            this.btnCasseteLoadMoveAT1Move.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadMoveAT1Move.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadMoveAT1Move.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadMoveAT1Move.Location = new System.Drawing.Point(3, 3);
            this.btnCasseteLoadMoveAT1Move.Name = "btnCasseteLoadMoveAT1Move";
            this.btnCasseteLoadMoveAT1Move.Size = new System.Drawing.Size(97, 53);
            this.btnCasseteLoadMoveAT1Move.TabIndex = 45;
            this.btnCasseteLoadMoveAT1Move.Text = "상부 카세트 T1\r\n리프트 이동";
            this.btnCasseteLoadMoveAT1Move.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 1;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Controls.Add(this.btnCasseteLoadMoveAT2Move, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.btnCasseteLoadMoveAT1In, 0, 0);
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 2;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel8.TabIndex = 0;
            // 
            // btnCasseteLoadMoveAT2Move
            // 
            this.btnCasseteLoadMoveAT2Move.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadMoveAT2Move.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadMoveAT2Move.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadMoveAT2Move.Location = new System.Drawing.Point(3, 62);
            this.btnCasseteLoadMoveAT2Move.Name = "btnCasseteLoadMoveAT2Move";
            this.btnCasseteLoadMoveAT2Move.Size = new System.Drawing.Size(97, 53);
            this.btnCasseteLoadMoveAT2Move.TabIndex = 48;
            this.btnCasseteLoadMoveAT2Move.Text = "하부 카세트 T2\r\n카세트 이동";
            this.btnCasseteLoadMoveAT2Move.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadMoveAT1In
            // 
            this.btnCasseteLoadMoveAT1In.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadMoveAT1In.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadMoveAT1In.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadMoveAT1In.Location = new System.Drawing.Point(3, 3);
            this.btnCasseteLoadMoveAT1In.Name = "btnCasseteLoadMoveAT1In";
            this.btnCasseteLoadMoveAT1In.Size = new System.Drawing.Size(97, 53);
            this.btnCasseteLoadMoveAT1In.TabIndex = 44;
            this.btnCasseteLoadMoveAT1In.Text = "상부 카세트 T1\r\n카세트 투입";
            this.btnCasseteLoadMoveAT1In.UseVisualStyleBackColor = false;
            // 
            // gxtCasseteLoad_B
            // 
            this.gxtCasseteLoad_B.Controls.Add(this.tableLayoutPanel2);
            this.gxtCasseteLoad_B.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCasseteLoad_B.ForeColor = System.Drawing.Color.White;
            this.gxtCasseteLoad_B.Location = new System.Drawing.Point(1247, 410);
            this.gxtCasseteLoad_B.Name = "gxtCasseteLoad_B";
            this.gxtCasseteLoad_B.Size = new System.Drawing.Size(449, 179);
            this.gxtCasseteLoad_B.TabIndex = 32;
            this.gxtCasseteLoad_B.TabStop = false;
            this.gxtCasseteLoad_B.Text = "     B     ";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Controls.Add(this.btnCasseteLoadBBCSD, 3, 2);
            this.tableLayoutPanel2.Controls.Add(this.btnCasseteLoadBBCSU, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.btnCasseteLoadBBCUG, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.btnCasseteLoadBBCG, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.btnCasseteLoadBLTSD, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.btnCasseteLoadBLTSU, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.btnCasseteLoadBLUG, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.btnCasseteLoadBLG, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.btnCasseteLoadBTCSD, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnCasseteLoadBTCSU, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnCasseteLoadBTCUG, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnCasseteLoadBTCG, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(437, 145);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // btnCasseteLoadBBCSD
            // 
            this.btnCasseteLoadBBCSD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadBBCSD.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadBBCSD.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadBBCSD.Location = new System.Drawing.Point(330, 99);
            this.btnCasseteLoadBBCSD.Name = "btnCasseteLoadBBCSD";
            this.btnCasseteLoadBBCSD.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteLoadBBCSD.TabIndex = 43;
            this.btnCasseteLoadBBCSD.Text = "하단 카세트\r\n스토퍼 다운";
            this.btnCasseteLoadBBCSD.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadBBCSU
            // 
            this.btnCasseteLoadBBCSU.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadBBCSU.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadBBCSU.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadBBCSU.Location = new System.Drawing.Point(221, 99);
            this.btnCasseteLoadBBCSU.Name = "btnCasseteLoadBBCSU";
            this.btnCasseteLoadBBCSU.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteLoadBBCSU.TabIndex = 42;
            this.btnCasseteLoadBBCSU.Text = "하단 카세트\r\n스토퍼 업";
            this.btnCasseteLoadBBCSU.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadBBCUG
            // 
            this.btnCasseteLoadBBCUG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadBBCUG.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadBBCUG.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadBBCUG.Location = new System.Drawing.Point(112, 99);
            this.btnCasseteLoadBBCUG.Name = "btnCasseteLoadBBCUG";
            this.btnCasseteLoadBBCUG.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteLoadBBCUG.TabIndex = 41;
            this.btnCasseteLoadBBCUG.Text = "하단 카세트\r\n언그립";
            this.btnCasseteLoadBBCUG.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadBBCG
            // 
            this.btnCasseteLoadBBCG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadBBCG.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadBBCG.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadBBCG.Location = new System.Drawing.Point(3, 99);
            this.btnCasseteLoadBBCG.Name = "btnCasseteLoadBBCG";
            this.btnCasseteLoadBBCG.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteLoadBBCG.TabIndex = 40;
            this.btnCasseteLoadBBCG.Text = "하단 카세트\r\n그립";
            this.btnCasseteLoadBBCG.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadBLTSD
            // 
            this.btnCasseteLoadBLTSD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadBLTSD.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadBLTSD.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadBLTSD.Location = new System.Drawing.Point(330, 51);
            this.btnCasseteLoadBLTSD.Name = "btnCasseteLoadBLTSD";
            this.btnCasseteLoadBLTSD.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteLoadBLTSD.TabIndex = 39;
            this.btnCasseteLoadBLTSD.Text = "리프트 틸트\r\n실린더 다운";
            this.btnCasseteLoadBLTSD.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadBLTSU
            // 
            this.btnCasseteLoadBLTSU.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadBLTSU.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadBLTSU.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadBLTSU.Location = new System.Drawing.Point(221, 51);
            this.btnCasseteLoadBLTSU.Name = "btnCasseteLoadBLTSU";
            this.btnCasseteLoadBLTSU.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteLoadBLTSU.TabIndex = 38;
            this.btnCasseteLoadBLTSU.Text = "리프트 틸트\r\n실린더 업";
            this.btnCasseteLoadBLTSU.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadBLUG
            // 
            this.btnCasseteLoadBLUG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadBLUG.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadBLUG.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadBLUG.Location = new System.Drawing.Point(112, 51);
            this.btnCasseteLoadBLUG.Name = "btnCasseteLoadBLUG";
            this.btnCasseteLoadBLUG.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteLoadBLUG.TabIndex = 37;
            this.btnCasseteLoadBLUG.Text = "리프트\r\n언그립";
            this.btnCasseteLoadBLUG.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadBLG
            // 
            this.btnCasseteLoadBLG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadBLG.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadBLG.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadBLG.Location = new System.Drawing.Point(3, 51);
            this.btnCasseteLoadBLG.Name = "btnCasseteLoadBLG";
            this.btnCasseteLoadBLG.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteLoadBLG.TabIndex = 36;
            this.btnCasseteLoadBLG.Text = "리프트\r\n그립";
            this.btnCasseteLoadBLG.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadBTCSD
            // 
            this.btnCasseteLoadBTCSD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadBTCSD.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadBTCSD.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadBTCSD.Location = new System.Drawing.Point(330, 3);
            this.btnCasseteLoadBTCSD.Name = "btnCasseteLoadBTCSD";
            this.btnCasseteLoadBTCSD.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteLoadBTCSD.TabIndex = 35;
            this.btnCasseteLoadBTCSD.Text = "상단 카세트\r\n스토퍼 다운";
            this.btnCasseteLoadBTCSD.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadBTCSU
            // 
            this.btnCasseteLoadBTCSU.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadBTCSU.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadBTCSU.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadBTCSU.Location = new System.Drawing.Point(221, 3);
            this.btnCasseteLoadBTCSU.Name = "btnCasseteLoadBTCSU";
            this.btnCasseteLoadBTCSU.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteLoadBTCSU.TabIndex = 34;
            this.btnCasseteLoadBTCSU.Text = "상단 카세트\r\n스토퍼 업";
            this.btnCasseteLoadBTCSU.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadBTCUG
            // 
            this.btnCasseteLoadBTCUG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadBTCUG.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadBTCUG.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadBTCUG.Location = new System.Drawing.Point(112, 3);
            this.btnCasseteLoadBTCUG.Name = "btnCasseteLoadBTCUG";
            this.btnCasseteLoadBTCUG.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteLoadBTCUG.TabIndex = 33;
            this.btnCasseteLoadBTCUG.Text = "상단 카세트\r\n언그립";
            this.btnCasseteLoadBTCUG.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadBTCG
            // 
            this.btnCasseteLoadBTCG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadBTCG.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadBTCG.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadBTCG.Location = new System.Drawing.Point(3, 3);
            this.btnCasseteLoadBTCG.Name = "btnCasseteLoadBTCG";
            this.btnCasseteLoadBTCG.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteLoadBTCG.TabIndex = 32;
            this.btnCasseteLoadBTCG.Text = "상단 카세트\r\n그립";
            this.btnCasseteLoadBTCG.UseVisualStyleBackColor = false;
            // 
            // gxtCasseteLoad_A
            // 
            this.gxtCasseteLoad_A.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCasseteLoad_A.ForeColor = System.Drawing.Color.White;
            this.gxtCasseteLoad_A.Location = new System.Drawing.Point(792, 410);
            this.gxtCasseteLoad_A.Name = "gxtCasseteLoad_A";
            this.gxtCasseteLoad_A.Size = new System.Drawing.Size(449, 179);
            this.gxtCasseteLoad_A.TabIndex = 20;
            this.gxtCasseteLoad_A.TabStop = false;
            this.gxtCasseteLoad_A.Text = "     A    ";
            // 
            // btnCasseteLoadSave
            // 
            this.btnCasseteLoadSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadSave.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadSave.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadSave.Location = new System.Drawing.Point(1482, 783);
            this.btnCasseteLoadSave.Name = "btnCasseteLoadSave";
            this.btnCasseteLoadSave.Size = new System.Drawing.Size(228, 28);
            this.btnCasseteLoadSave.TabIndex = 64;
            this.btnCasseteLoadSave.Text = "저장";
            this.btnCasseteLoadSave.UseVisualStyleBackColor = false;
            // 
            // gxtCasseteLoad_Muting
            // 
            this.gxtCasseteLoad_Muting.Controls.Add(this.btnCasseteLoadMuting4);
            this.gxtCasseteLoad_Muting.Controls.Add(this.btnCasseteLoadMuting2);
            this.gxtCasseteLoad_Muting.Controls.Add(this.btnCasseteLoadMuting3);
            this.gxtCasseteLoad_Muting.Controls.Add(this.btnCasseteLoadMuting1);
            this.gxtCasseteLoad_Muting.Controls.Add(this.btnCasseteLoadMutingOff);
            this.gxtCasseteLoad_Muting.Controls.Add(this.btnCasseteLoadMutingIn);
            this.gxtCasseteLoad_Muting.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCasseteLoad_Muting.ForeColor = System.Drawing.Color.White;
            this.gxtCasseteLoad_Muting.Location = new System.Drawing.Point(1483, 223);
            this.gxtCasseteLoad_Muting.Name = "gxtCasseteLoad_Muting";
            this.gxtCasseteLoad_Muting.Size = new System.Drawing.Size(212, 181);
            this.gxtCasseteLoad_Muting.TabIndex = 13;
            this.gxtCasseteLoad_Muting.TabStop = false;
            this.gxtCasseteLoad_Muting.Text = "     뮤팅     ";
            // 
            // btnCasseteLoadMuting4
            // 
            this.btnCasseteLoadMuting4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadMuting4.Location = new System.Drawing.Point(107, 125);
            this.btnCasseteLoadMuting4.Name = "btnCasseteLoadMuting4";
            this.btnCasseteLoadMuting4.Size = new System.Drawing.Size(99, 44);
            this.btnCasseteLoadMuting4.TabIndex = 19;
            this.btnCasseteLoadMuting4.Text = "뮤팅 4";
            this.btnCasseteLoadMuting4.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadMuting2
            // 
            this.btnCasseteLoadMuting2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadMuting2.Location = new System.Drawing.Point(6, 125);
            this.btnCasseteLoadMuting2.Name = "btnCasseteLoadMuting2";
            this.btnCasseteLoadMuting2.Size = new System.Drawing.Size(99, 44);
            this.btnCasseteLoadMuting2.TabIndex = 18;
            this.btnCasseteLoadMuting2.Text = "뮤팅 2";
            this.btnCasseteLoadMuting2.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadMuting3
            // 
            this.btnCasseteLoadMuting3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadMuting3.Location = new System.Drawing.Point(107, 75);
            this.btnCasseteLoadMuting3.Name = "btnCasseteLoadMuting3";
            this.btnCasseteLoadMuting3.Size = new System.Drawing.Size(99, 44);
            this.btnCasseteLoadMuting3.TabIndex = 17;
            this.btnCasseteLoadMuting3.Text = "뮤팅 3";
            this.btnCasseteLoadMuting3.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadMuting1
            // 
            this.btnCasseteLoadMuting1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadMuting1.Location = new System.Drawing.Point(6, 75);
            this.btnCasseteLoadMuting1.Name = "btnCasseteLoadMuting1";
            this.btnCasseteLoadMuting1.Size = new System.Drawing.Size(99, 44);
            this.btnCasseteLoadMuting1.TabIndex = 16;
            this.btnCasseteLoadMuting1.Text = "뮤팅 1";
            this.btnCasseteLoadMuting1.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadMutingOff
            // 
            this.btnCasseteLoadMutingOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadMutingOff.Location = new System.Drawing.Point(107, 29);
            this.btnCasseteLoadMutingOff.Name = "btnCasseteLoadMutingOff";
            this.btnCasseteLoadMutingOff.Size = new System.Drawing.Size(99, 44);
            this.btnCasseteLoadMutingOff.TabIndex = 15;
            this.btnCasseteLoadMutingOff.Text = "뮤팅 아웃";
            this.btnCasseteLoadMutingOff.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadMutingIn
            // 
            this.btnCasseteLoadMutingIn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadMutingIn.Location = new System.Drawing.Point(6, 28);
            this.btnCasseteLoadMutingIn.Name = "btnCasseteLoadMutingIn";
            this.btnCasseteLoadMutingIn.Size = new System.Drawing.Size(99, 44);
            this.btnCasseteLoadMutingIn.TabIndex = 14;
            this.btnCasseteLoadMutingIn.Text = "뮤팅 인";
            this.btnCasseteLoadMutingIn.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadSpeedSetting
            // 
            this.btnCasseteLoadSpeedSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadSpeedSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCasseteLoadSpeedSetting.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadSpeedSetting.Location = new System.Drawing.Point(1291, 315);
            this.btnCasseteLoadSpeedSetting.Name = "btnCasseteLoadSpeedSetting";
            this.btnCasseteLoadSpeedSetting.Size = new System.Drawing.Size(90, 27);
            this.btnCasseteLoadSpeedSetting.TabIndex = 12;
            this.btnCasseteLoadSpeedSetting.Text = "속도 설정";
            this.btnCasseteLoadSpeedSetting.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadAllSetting
            // 
            this.btnCasseteLoadAllSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadAllSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCasseteLoadAllSetting.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadAllSetting.Location = new System.Drawing.Point(1387, 273);
            this.btnCasseteLoadAllSetting.Name = "btnCasseteLoadAllSetting";
            this.btnCasseteLoadAllSetting.Size = new System.Drawing.Size(90, 27);
            this.btnCasseteLoadAllSetting.TabIndex = 8;
            this.btnCasseteLoadAllSetting.Text = "전체 설정";
            this.btnCasseteLoadAllSetting.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadLocationSetting
            // 
            this.btnCasseteLoadLocationSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadLocationSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCasseteLoadLocationSetting.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadLocationSetting.Location = new System.Drawing.Point(1071, 315);
            this.btnCasseteLoadLocationSetting.Name = "btnCasseteLoadLocationSetting";
            this.btnCasseteLoadLocationSetting.Size = new System.Drawing.Size(90, 27);
            this.btnCasseteLoadLocationSetting.TabIndex = 11;
            this.btnCasseteLoadLocationSetting.Text = "위치 설정";
            this.btnCasseteLoadLocationSetting.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadMoveLocation
            // 
            this.btnCasseteLoadMoveLocation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadMoveLocation.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCasseteLoadMoveLocation.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadMoveLocation.Location = new System.Drawing.Point(975, 315);
            this.btnCasseteLoadMoveLocation.Name = "btnCasseteLoadMoveLocation";
            this.btnCasseteLoadMoveLocation.Size = new System.Drawing.Size(90, 27);
            this.btnCasseteLoadMoveLocation.TabIndex = 10;
            this.btnCasseteLoadMoveLocation.Text = "위치 이동";
            this.btnCasseteLoadMoveLocation.UseVisualStyleBackColor = false;
            // 
            // txtCasseteLoadLocation
            // 
            this.txtCasseteLoadLocation.Font = new System.Drawing.Font("굴림", 13F);
            this.txtCasseteLoadLocation.Location = new System.Drawing.Point(879, 316);
            this.txtCasseteLoadLocation.Name = "txtCasseteLoadLocation";
            this.txtCasseteLoadLocation.Size = new System.Drawing.Size(90, 27);
            this.txtCasseteLoadLocation.TabIndex = 9;
            // 
            // txtCasseteLoadSpeed
            // 
            this.txtCasseteLoadSpeed.Font = new System.Drawing.Font("굴림", 13F);
            this.txtCasseteLoadSpeed.Location = new System.Drawing.Point(1291, 273);
            this.txtCasseteLoadSpeed.Name = "txtCasseteLoadSpeed";
            this.txtCasseteLoadSpeed.Size = new System.Drawing.Size(90, 27);
            this.txtCasseteLoadSpeed.TabIndex = 7;
            // 
            // txtCasseteLoadCurrentLocation
            // 
            this.txtCasseteLoadCurrentLocation.Font = new System.Drawing.Font("굴림", 13F);
            this.txtCasseteLoadCurrentLocation.Location = new System.Drawing.Point(1084, 274);
            this.txtCasseteLoadCurrentLocation.Name = "txtCasseteLoadCurrentLocation";
            this.txtCasseteLoadCurrentLocation.Size = new System.Drawing.Size(90, 27);
            this.txtCasseteLoadCurrentLocation.TabIndex = 6;
            // 
            // txtCasseteLoadSelectedShaft
            // 
            this.txtCasseteLoadSelectedShaft.Font = new System.Drawing.Font("굴림", 13F);
            this.txtCasseteLoadSelectedShaft.Location = new System.Drawing.Point(879, 273);
            this.txtCasseteLoadSelectedShaft.Name = "txtCasseteLoadSelectedShaft";
            this.txtCasseteLoadSelectedShaft.Size = new System.Drawing.Size(90, 27);
            this.txtCasseteLoadSelectedShaft.TabIndex = 5;
            // 
            // lbl_CasseteLoad_Location
            // 
            this.lbl_CasseteLoad_Location.AutoSize = true;
            this.lbl_CasseteLoad_Location.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CasseteLoad_Location.ForeColor = System.Drawing.Color.White;
            this.lbl_CasseteLoad_Location.Location = new System.Drawing.Point(801, 316);
            this.lbl_CasseteLoad_Location.Name = "lbl_CasseteLoad_Location";
            this.lbl_CasseteLoad_Location.Size = new System.Drawing.Size(84, 21);
            this.lbl_CasseteLoad_Location.TabIndex = 0;
            this.lbl_CasseteLoad_Location.Text = "위치[mm]";
            // 
            // lbl_CasseteLoad_Speed
            // 
            this.lbl_CasseteLoad_Speed.AutoSize = true;
            this.lbl_CasseteLoad_Speed.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CasseteLoad_Speed.ForeColor = System.Drawing.Color.White;
            this.lbl_CasseteLoad_Speed.Location = new System.Drawing.Point(1179, 275);
            this.lbl_CasseteLoad_Speed.Name = "lbl_CasseteLoad_Speed";
            this.lbl_CasseteLoad_Speed.Size = new System.Drawing.Size(115, 21);
            this.lbl_CasseteLoad_Speed.TabIndex = 0;
            this.lbl_CasseteLoad_Speed.Text = "속도[mm/sec]";
            // 
            // lbl_CasseteLoad_SelectedShaft
            // 
            this.lbl_CasseteLoad_SelectedShaft.AutoSize = true;
            this.lbl_CasseteLoad_SelectedShaft.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CasseteLoad_SelectedShaft.ForeColor = System.Drawing.Color.White;
            this.lbl_CasseteLoad_SelectedShaft.Location = new System.Drawing.Point(805, 274);
            this.lbl_CasseteLoad_SelectedShaft.Name = "lbl_CasseteLoad_SelectedShaft";
            this.lbl_CasseteLoad_SelectedShaft.Size = new System.Drawing.Size(80, 21);
            this.lbl_CasseteLoad_SelectedShaft.TabIndex = 0;
            this.lbl_CasseteLoad_SelectedShaft.Text = "선택된 축";
            // 
            // txtCasseteLoad_LocationSetting
            // 
            this.txtCasseteLoad_LocationSetting.BackColor = System.Drawing.SystemColors.Highlight;
            this.txtCasseteLoad_LocationSetting.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtCasseteLoad_LocationSetting.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.txtCasseteLoad_LocationSetting.ForeColor = System.Drawing.Color.White;
            this.txtCasseteLoad_LocationSetting.Location = new System.Drawing.Point(792, 223);
            this.txtCasseteLoad_LocationSetting.Name = "txtCasseteLoad_LocationSetting";
            this.txtCasseteLoad_LocationSetting.ReadOnly = true;
            this.txtCasseteLoad_LocationSetting.Size = new System.Drawing.Size(89, 27);
            this.txtCasseteLoad_LocationSetting.TabIndex = 37;
            this.txtCasseteLoad_LocationSetting.TabStop = false;
            this.txtCasseteLoad_LocationSetting.Text = "위치값 설정";
            this.txtCasseteLoad_LocationSetting.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbl_CasseteLoad_CurrentLocation
            // 
            this.lbl_CasseteLoad_CurrentLocation.AutoSize = true;
            this.lbl_CasseteLoad_CurrentLocation.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CasseteLoad_CurrentLocation.ForeColor = System.Drawing.Color.White;
            this.lbl_CasseteLoad_CurrentLocation.Location = new System.Drawing.Point(975, 274);
            this.lbl_CasseteLoad_CurrentLocation.Name = "lbl_CasseteLoad_CurrentLocation";
            this.lbl_CasseteLoad_CurrentLocation.Size = new System.Drawing.Size(116, 21);
            this.lbl_CasseteLoad_CurrentLocation.TabIndex = 0;
            this.lbl_CasseteLoad_CurrentLocation.Text = "현재위치[mm]";
            // 
            // btnCasseteLoadGrabSwitch3
            // 
            this.btnCasseteLoadGrabSwitch3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadGrabSwitch3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadGrabSwitch3.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadGrabSwitch3.Location = new System.Drawing.Point(1514, 6);
            this.btnCasseteLoadGrabSwitch3.Name = "btnCasseteLoadGrabSwitch3";
            this.btnCasseteLoadGrabSwitch3.Size = new System.Drawing.Size(172, 23);
            this.btnCasseteLoadGrabSwitch3.TabIndex = 3;
            this.btnCasseteLoadGrabSwitch3.Text = "Grab Switch 3";
            this.btnCasseteLoadGrabSwitch3.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadGrabSwitch2
            // 
            this.btnCasseteLoadGrabSwitch2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadGrabSwitch2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadGrabSwitch2.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadGrabSwitch2.Location = new System.Drawing.Point(1336, 6);
            this.btnCasseteLoadGrabSwitch2.Name = "btnCasseteLoadGrabSwitch2";
            this.btnCasseteLoadGrabSwitch2.Size = new System.Drawing.Size(172, 23);
            this.btnCasseteLoadGrabSwitch2.TabIndex = 2;
            this.btnCasseteLoadGrabSwitch2.Text = "Grab Switch 2";
            this.btnCasseteLoadGrabSwitch2.UseVisualStyleBackColor = false;
            // 
            // btnCasseteLoadGrabSwitch1
            // 
            this.btnCasseteLoadGrabSwitch1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteLoadGrabSwitch1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteLoadGrabSwitch1.ForeColor = System.Drawing.Color.White;
            this.btnCasseteLoadGrabSwitch1.Location = new System.Drawing.Point(1158, 6);
            this.btnCasseteLoadGrabSwitch1.Name = "btnCasseteLoadGrabSwitch1";
            this.btnCasseteLoadGrabSwitch1.Size = new System.Drawing.Size(172, 23);
            this.btnCasseteLoadGrabSwitch1.TabIndex = 1;
            this.btnCasseteLoadGrabSwitch1.Text = "Grab Switch 1";
            this.btnCasseteLoadGrabSwitch1.UseVisualStyleBackColor = false;
            // 
            // lvCasseteLoad
            // 
            this.lvCasseteLoad.BackColor = System.Drawing.SystemColors.Window;
            this.lvCasseteLoad.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.col_CasseteLoad_NamebyLocation,
            this.col_CasseteLoad_Shaft,
            this.col_CasseteLoad_Location,
            this.col_CasseteLoad_Speed});
            this.lvCasseteLoad.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lvCasseteLoad.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lvCasseteLoad.GridLines = true;
            this.lvCasseteLoad.Location = new System.Drawing.Point(17, 16);
            this.lvCasseteLoad.Name = "lvCasseteLoad";
            this.lvCasseteLoad.Size = new System.Drawing.Size(758, 734);
            this.lvCasseteLoad.TabIndex = 0;
            this.lvCasseteLoad.TabStop = false;
            this.lvCasseteLoad.UseCompatibleStateImageBehavior = false;
            this.lvCasseteLoad.View = System.Windows.Forms.View.Details;
            // 
            // col_CasseteLoad_NamebyLocation
            // 
            this.col_CasseteLoad_NamebyLocation.Text = "위치별 명칭";
            this.col_CasseteLoad_NamebyLocation.Width = 430;
            // 
            // col_CasseteLoad_Shaft
            // 
            this.col_CasseteLoad_Shaft.Text = "축";
            this.col_CasseteLoad_Shaft.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // col_CasseteLoad_Location
            // 
            this.col_CasseteLoad_Location.Text = "위치[mm]";
            this.col_CasseteLoad_Location.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_CasseteLoad_Location.Width = 125;
            // 
            // col_CasseteLoad_Speed
            // 
            this.col_CasseteLoad_Speed.Text = "속도[mm/s]";
            this.col_CasseteLoad_Speed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_CasseteLoad_Speed.Width = 125;
            // 
            // ajin_Setting_CasseteLoad
            // 
            this.ajin_Setting_CasseteLoad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CasseteLoad.Location = new System.Drawing.Point(792, 34);
            this.ajin_Setting_CasseteLoad.Name = "ajin_Setting_CasseteLoad";
            this.ajin_Setting_CasseteLoad.Servo = null;
            this.ajin_Setting_CasseteLoad.Size = new System.Drawing.Size(903, 183);
            this.ajin_Setting_CasseteLoad.TabIndex = 4;
            // 
            // tp_Cellpurge
            // 
            this.tp_Cellpurge.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tp_Cellpurge.Controls.Add(this.gxtCellpurge_Move);
            this.tp_Cellpurge.Controls.Add(this.gxtCellpurge_B);
            this.tp_Cellpurge.Controls.Add(this.gxtCellpurge_A);
            this.tp_Cellpurge.Controls.Add(this.btnCellpurgeSave);
            this.tp_Cellpurge.Controls.Add(this.gxtCellpurge_Ionizer);
            this.tp_Cellpurge.Controls.Add(this.btnCellpurgeSpeedSetting);
            this.tp_Cellpurge.Controls.Add(this.btnCellpurgeAllSetting);
            this.tp_Cellpurge.Controls.Add(this.btnCellpurgeLocationSetting);
            this.tp_Cellpurge.Controls.Add(this.btnCellpurgeMoveLocation);
            this.tp_Cellpurge.Controls.Add(this.txtCellpurgeLocation);
            this.tp_Cellpurge.Controls.Add(this.txtCellpurgeSpeed);
            this.tp_Cellpurge.Controls.Add(this.txtCellpurgeCurrentLocation);
            this.tp_Cellpurge.Controls.Add(this.txtCellpurgeSelectedShaft);
            this.tp_Cellpurge.Controls.Add(this.lbl_Cellpurge_Location);
            this.tp_Cellpurge.Controls.Add(this.lbl_Cellpurge_Speed);
            this.tp_Cellpurge.Controls.Add(this.lbl_Cellpurge_SelectedShaft);
            this.tp_Cellpurge.Controls.Add(this.txtCellpurge_LocationSetting);
            this.tp_Cellpurge.Controls.Add(this.lbl_Cellpurge_CurrentLocation);
            this.tp_Cellpurge.Controls.Add(this.btnCellpurgeGrabSwitch3);
            this.tp_Cellpurge.Controls.Add(this.btnCellpurgeGrabSwitch2);
            this.tp_Cellpurge.Controls.Add(this.btnCellpurgeGrabSwitch1);
            this.tp_Cellpurge.Controls.Add(this.lvCellpurge);
            this.tp_Cellpurge.Controls.Add(this.ajin_Setting_Cellpurge);
            this.tp_Cellpurge.Location = new System.Drawing.Point(4, 34);
            this.tp_Cellpurge.Name = "tp_Cellpurge";
            this.tp_Cellpurge.Size = new System.Drawing.Size(1726, 816);
            this.tp_Cellpurge.TabIndex = 1;
            this.tp_Cellpurge.Text = "셀 취출 이재기";
            // 
            // gxtCellpurge_Move
            // 
            this.gxtCellpurge_Move.Controls.Add(this.gxtCellpurge_MoveCenter);
            this.gxtCellpurge_Move.Controls.Add(this.gxtCellpurge_MoveB);
            this.gxtCellpurge_Move.Controls.Add(this.gxtCellpurge_MoveA);
            this.gxtCellpurge_Move.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCellpurge_Move.ForeColor = System.Drawing.Color.White;
            this.gxtCellpurge_Move.Location = new System.Drawing.Point(792, 595);
            this.gxtCellpurge_Move.Name = "gxtCellpurge_Move";
            this.gxtCellpurge_Move.Size = new System.Drawing.Size(904, 179);
            this.gxtCellpurge_Move.TabIndex = 25;
            this.gxtCellpurge_Move.TabStop = false;
            this.gxtCellpurge_Move.Text = "     이동     ";
            // 
            // gxtCellpurge_MoveCenter
            // 
            this.gxtCellpurge_MoveCenter.Controls.Add(this.tableLayoutPanel21);
            this.gxtCellpurge_MoveCenter.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCellpurge_MoveCenter.ForeColor = System.Drawing.Color.White;
            this.gxtCellpurge_MoveCenter.Location = new System.Drawing.Point(604, 28);
            this.gxtCellpurge_MoveCenter.Name = "gxtCellpurge_MoveCenter";
            this.gxtCellpurge_MoveCenter.Size = new System.Drawing.Size(293, 145);
            this.gxtCellpurge_MoveCenter.TabIndex = 33;
            this.gxtCellpurge_MoveCenter.TabStop = false;
            this.gxtCellpurge_MoveCenter.Text = "     중간, 언로딩    ";
            // 
            // tableLayoutPanel21
            // 
            this.tableLayoutPanel21.ColumnCount = 3;
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel21.Controls.Add(this.btnCellpurgeMoveCenterY2C, 0, 1);
            this.tableLayoutPanel21.Controls.Add(this.btnCellpurgeMoveCenterBBuffer, 0, 1);
            this.tableLayoutPanel21.Controls.Add(this.btnCellpurgeMoveCenterY2Uld, 0, 1);
            this.tableLayoutPanel21.Controls.Add(this.btnCellpurgeMoveCenterY1C, 0, 0);
            this.tableLayoutPanel21.Controls.Add(this.btnCellpurgeMoveCenterABuffer, 1, 0);
            this.tableLayoutPanel21.Controls.Add(this.btnCellpurgeMoveCenterY1Uld, 2, 0);
            this.tableLayoutPanel21.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel21.Name = "tableLayoutPanel21";
            this.tableLayoutPanel21.RowCount = 2;
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel21.Size = new System.Drawing.Size(281, 111);
            this.tableLayoutPanel21.TabIndex = 32;
            // 
            // btnCellpurgeMoveCenterY2C
            // 
            this.btnCellpurgeMoveCenterY2C.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellpurgeMoveCenterY2C.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellpurgeMoveCenterY2C.ForeColor = System.Drawing.Color.White;
            this.btnCellpurgeMoveCenterY2C.Location = new System.Drawing.Point(3, 58);
            this.btnCellpurgeMoveCenterY2C.Name = "btnCellpurgeMoveCenterY2C";
            this.btnCellpurgeMoveCenterY2C.Size = new System.Drawing.Size(87, 50);
            this.btnCellpurgeMoveCenterY2C.TabIndex = 36;
            this.btnCellpurgeMoveCenterY2C.Text = "Y2\r\n카세트\r\n중간";
            this.btnCellpurgeMoveCenterY2C.UseVisualStyleBackColor = false;
            // 
            // btnCellpurgeMoveCenterBBuffer
            // 
            this.btnCellpurgeMoveCenterBBuffer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellpurgeMoveCenterBBuffer.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnCellpurgeMoveCenterBBuffer.ForeColor = System.Drawing.Color.White;
            this.btnCellpurgeMoveCenterBBuffer.Location = new System.Drawing.Point(96, 58);
            this.btnCellpurgeMoveCenterBBuffer.Name = "btnCellpurgeMoveCenterBBuffer";
            this.btnCellpurgeMoveCenterBBuffer.Size = new System.Drawing.Size(87, 50);
            this.btnCellpurgeMoveCenterBBuffer.TabIndex = 37;
            this.btnCellpurgeMoveCenterBBuffer.Text = "B\r\nBUFFER";
            this.btnCellpurgeMoveCenterBBuffer.UseVisualStyleBackColor = false;
            // 
            // btnCellpurgeMoveCenterY2Uld
            // 
            this.btnCellpurgeMoveCenterY2Uld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellpurgeMoveCenterY2Uld.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellpurgeMoveCenterY2Uld.ForeColor = System.Drawing.Color.White;
            this.btnCellpurgeMoveCenterY2Uld.Location = new System.Drawing.Point(189, 58);
            this.btnCellpurgeMoveCenterY2Uld.Name = "btnCellpurgeMoveCenterY2Uld";
            this.btnCellpurgeMoveCenterY2Uld.Size = new System.Drawing.Size(87, 50);
            this.btnCellpurgeMoveCenterY2Uld.TabIndex = 38;
            this.btnCellpurgeMoveCenterY2Uld.Text = "Y2\r\n언로딩 이재기";
            this.btnCellpurgeMoveCenterY2Uld.UseVisualStyleBackColor = false;
            // 
            // btnCellpurgeMoveCenterY1C
            // 
            this.btnCellpurgeMoveCenterY1C.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellpurgeMoveCenterY1C.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellpurgeMoveCenterY1C.ForeColor = System.Drawing.Color.White;
            this.btnCellpurgeMoveCenterY1C.Location = new System.Drawing.Point(3, 3);
            this.btnCellpurgeMoveCenterY1C.Name = "btnCellpurgeMoveCenterY1C";
            this.btnCellpurgeMoveCenterY1C.Size = new System.Drawing.Size(87, 49);
            this.btnCellpurgeMoveCenterY1C.TabIndex = 33;
            this.btnCellpurgeMoveCenterY1C.Text = "Y1\r\n카세트\r\n중간";
            this.btnCellpurgeMoveCenterY1C.UseVisualStyleBackColor = false;
            // 
            // btnCellpurgeMoveCenterABuffer
            // 
            this.btnCellpurgeMoveCenterABuffer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellpurgeMoveCenterABuffer.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnCellpurgeMoveCenterABuffer.ForeColor = System.Drawing.Color.White;
            this.btnCellpurgeMoveCenterABuffer.Location = new System.Drawing.Point(96, 3);
            this.btnCellpurgeMoveCenterABuffer.Name = "btnCellpurgeMoveCenterABuffer";
            this.btnCellpurgeMoveCenterABuffer.Size = new System.Drawing.Size(87, 49);
            this.btnCellpurgeMoveCenterABuffer.TabIndex = 34;
            this.btnCellpurgeMoveCenterABuffer.Text = "A\r\nBUFFER";
            this.btnCellpurgeMoveCenterABuffer.UseVisualStyleBackColor = false;
            // 
            // btnCellpurgeMoveCenterY1Uld
            // 
            this.btnCellpurgeMoveCenterY1Uld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellpurgeMoveCenterY1Uld.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellpurgeMoveCenterY1Uld.ForeColor = System.Drawing.Color.White;
            this.btnCellpurgeMoveCenterY1Uld.Location = new System.Drawing.Point(189, 3);
            this.btnCellpurgeMoveCenterY1Uld.Name = "btnCellpurgeMoveCenterY1Uld";
            this.btnCellpurgeMoveCenterY1Uld.Size = new System.Drawing.Size(89, 49);
            this.btnCellpurgeMoveCenterY1Uld.TabIndex = 35;
            this.btnCellpurgeMoveCenterY1Uld.Text = "Y1\r\n언로딩 이재기";
            this.btnCellpurgeMoveCenterY1Uld.UseVisualStyleBackColor = false;
            // 
            // gxtCellpurge_MoveB
            // 
            this.gxtCellpurge_MoveB.Controls.Add(this.tableLayoutPanel20);
            this.gxtCellpurge_MoveB.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCellpurge_MoveB.ForeColor = System.Drawing.Color.White;
            this.gxtCellpurge_MoveB.Location = new System.Drawing.Point(305, 28);
            this.gxtCellpurge_MoveB.Name = "gxtCellpurge_MoveB";
            this.gxtCellpurge_MoveB.Size = new System.Drawing.Size(293, 145);
            this.gxtCellpurge_MoveB.TabIndex = 29;
            this.gxtCellpurge_MoveB.TabStop = false;
            this.gxtCellpurge_MoveB.Text = "     B    ";
            // 
            // tableLayoutPanel20
            // 
            this.tableLayoutPanel20.ColumnCount = 2;
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel20.Controls.Add(this.btnCellpurgeMoveBX2BC, 0, 1);
            this.tableLayoutPanel20.Controls.Add(this.btnCellpurgeMoveBX2BW, 0, 1);
            this.tableLayoutPanel20.Controls.Add(this.btnCellpurgeMoveBX1BW, 1, 0);
            this.tableLayoutPanel20.Controls.Add(this.btnCellpurgeMoveBX1BC, 0, 0);
            this.tableLayoutPanel20.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel20.Name = "tableLayoutPanel20";
            this.tableLayoutPanel20.RowCount = 2;
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel20.Size = new System.Drawing.Size(281, 111);
            this.tableLayoutPanel20.TabIndex = 56;
            // 
            // btnCellpurgeMoveBX2BC
            // 
            this.btnCellpurgeMoveBX2BC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellpurgeMoveBX2BC.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellpurgeMoveBX2BC.ForeColor = System.Drawing.Color.White;
            this.btnCellpurgeMoveBX2BC.Location = new System.Drawing.Point(3, 58);
            this.btnCellpurgeMoveBX2BC.Name = "btnCellpurgeMoveBX2BC";
            this.btnCellpurgeMoveBX2BC.Size = new System.Drawing.Size(134, 50);
            this.btnCellpurgeMoveBX2BC.TabIndex = 31;
            this.btnCellpurgeMoveBX2BC.Text = "X2\r\nB 카세트\r\n중간";
            this.btnCellpurgeMoveBX2BC.UseVisualStyleBackColor = false;
            // 
            // btnCellpurgeMoveBX2BW
            // 
            this.btnCellpurgeMoveBX2BW.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellpurgeMoveBX2BW.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellpurgeMoveBX2BW.ForeColor = System.Drawing.Color.White;
            this.btnCellpurgeMoveBX2BW.Location = new System.Drawing.Point(143, 58);
            this.btnCellpurgeMoveBX2BW.Name = "btnCellpurgeMoveBX2BW";
            this.btnCellpurgeMoveBX2BW.Size = new System.Drawing.Size(135, 50);
            this.btnCellpurgeMoveBX2BW.TabIndex = 32;
            this.btnCellpurgeMoveBX2BW.Text = "X2\r\nB 카세트\r\n대기";
            this.btnCellpurgeMoveBX2BW.UseVisualStyleBackColor = false;
            // 
            // btnCellpurgeMoveBX1BW
            // 
            this.btnCellpurgeMoveBX1BW.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellpurgeMoveBX1BW.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellpurgeMoveBX1BW.ForeColor = System.Drawing.Color.White;
            this.btnCellpurgeMoveBX1BW.Location = new System.Drawing.Point(143, 3);
            this.btnCellpurgeMoveBX1BW.Name = "btnCellpurgeMoveBX1BW";
            this.btnCellpurgeMoveBX1BW.Size = new System.Drawing.Size(135, 49);
            this.btnCellpurgeMoveBX1BW.TabIndex = 30;
            this.btnCellpurgeMoveBX1BW.Text = "X1\r\nB 카세트\r\n대기";
            this.btnCellpurgeMoveBX1BW.UseVisualStyleBackColor = false;
            // 
            // btnCellpurgeMoveBX1BC
            // 
            this.btnCellpurgeMoveBX1BC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellpurgeMoveBX1BC.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellpurgeMoveBX1BC.ForeColor = System.Drawing.Color.White;
            this.btnCellpurgeMoveBX1BC.Location = new System.Drawing.Point(3, 3);
            this.btnCellpurgeMoveBX1BC.Name = "btnCellpurgeMoveBX1BC";
            this.btnCellpurgeMoveBX1BC.Size = new System.Drawing.Size(134, 49);
            this.btnCellpurgeMoveBX1BC.TabIndex = 29;
            this.btnCellpurgeMoveBX1BC.Text = "X1\r\nB 카세트\r\n중간";
            this.btnCellpurgeMoveBX1BC.UseVisualStyleBackColor = false;
            // 
            // gxtCellpurge_MoveA
            // 
            this.gxtCellpurge_MoveA.Controls.Add(this.tableLayoutPanel19);
            this.gxtCellpurge_MoveA.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCellpurge_MoveA.ForeColor = System.Drawing.Color.White;
            this.gxtCellpurge_MoveA.Location = new System.Drawing.Point(6, 28);
            this.gxtCellpurge_MoveA.Name = "gxtCellpurge_MoveA";
            this.gxtCellpurge_MoveA.Size = new System.Drawing.Size(293, 145);
            this.gxtCellpurge_MoveA.TabIndex = 25;
            this.gxtCellpurge_MoveA.TabStop = false;
            this.gxtCellpurge_MoveA.Text = "     A    ";
            // 
            // tableLayoutPanel19
            // 
            this.tableLayoutPanel19.ColumnCount = 2;
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.Controls.Add(this.btnCellpurgeMoveAX2AC, 0, 1);
            this.tableLayoutPanel19.Controls.Add(this.btnCellpurgeMoveAX2AW, 0, 1);
            this.tableLayoutPanel19.Controls.Add(this.btnCellpurgeMoveAX1AW, 1, 0);
            this.tableLayoutPanel19.Controls.Add(this.btnCellpurgeMoveAX1AC, 0, 0);
            this.tableLayoutPanel19.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel19.Name = "tableLayoutPanel19";
            this.tableLayoutPanel19.RowCount = 2;
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel19.Size = new System.Drawing.Size(281, 111);
            this.tableLayoutPanel19.TabIndex = 55;
            // 
            // btnCellpurgeMoveAX2AC
            // 
            this.btnCellpurgeMoveAX2AC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellpurgeMoveAX2AC.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellpurgeMoveAX2AC.ForeColor = System.Drawing.Color.White;
            this.btnCellpurgeMoveAX2AC.Location = new System.Drawing.Point(3, 58);
            this.btnCellpurgeMoveAX2AC.Name = "btnCellpurgeMoveAX2AC";
            this.btnCellpurgeMoveAX2AC.Size = new System.Drawing.Size(134, 50);
            this.btnCellpurgeMoveAX2AC.TabIndex = 27;
            this.btnCellpurgeMoveAX2AC.Text = "X2\r\nA 카세트\r\n중간";
            this.btnCellpurgeMoveAX2AC.UseVisualStyleBackColor = false;
            // 
            // btnCellpurgeMoveAX2AW
            // 
            this.btnCellpurgeMoveAX2AW.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellpurgeMoveAX2AW.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellpurgeMoveAX2AW.ForeColor = System.Drawing.Color.White;
            this.btnCellpurgeMoveAX2AW.Location = new System.Drawing.Point(143, 58);
            this.btnCellpurgeMoveAX2AW.Name = "btnCellpurgeMoveAX2AW";
            this.btnCellpurgeMoveAX2AW.Size = new System.Drawing.Size(135, 50);
            this.btnCellpurgeMoveAX2AW.TabIndex = 28;
            this.btnCellpurgeMoveAX2AW.Text = "X2\r\nA 카세트\r\n대기";
            this.btnCellpurgeMoveAX2AW.UseVisualStyleBackColor = false;
            // 
            // btnCellpurgeMoveAX1AW
            // 
            this.btnCellpurgeMoveAX1AW.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellpurgeMoveAX1AW.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellpurgeMoveAX1AW.ForeColor = System.Drawing.Color.White;
            this.btnCellpurgeMoveAX1AW.Location = new System.Drawing.Point(143, 3);
            this.btnCellpurgeMoveAX1AW.Name = "btnCellpurgeMoveAX1AW";
            this.btnCellpurgeMoveAX1AW.Size = new System.Drawing.Size(135, 49);
            this.btnCellpurgeMoveAX1AW.TabIndex = 26;
            this.btnCellpurgeMoveAX1AW.Text = "X1\r\nA 카세트\r\n대기";
            this.btnCellpurgeMoveAX1AW.UseVisualStyleBackColor = false;
            // 
            // btnCellpurgeMoveAX1AC
            // 
            this.btnCellpurgeMoveAX1AC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellpurgeMoveAX1AC.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellpurgeMoveAX1AC.ForeColor = System.Drawing.Color.White;
            this.btnCellpurgeMoveAX1AC.Location = new System.Drawing.Point(3, 3);
            this.btnCellpurgeMoveAX1AC.Name = "btnCellpurgeMoveAX1AC";
            this.btnCellpurgeMoveAX1AC.Size = new System.Drawing.Size(134, 49);
            this.btnCellpurgeMoveAX1AC.TabIndex = 25;
            this.btnCellpurgeMoveAX1AC.Text = "X1\r\nA 카세트\r\n중간";
            this.btnCellpurgeMoveAX1AC.UseVisualStyleBackColor = false;
            // 
            // gxtCellpurge_B
            // 
            this.gxtCellpurge_B.Controls.Add(this.tableLayoutPanel18);
            this.gxtCellpurge_B.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCellpurge_B.ForeColor = System.Drawing.Color.White;
            this.gxtCellpurge_B.Location = new System.Drawing.Point(1247, 410);
            this.gxtCellpurge_B.Name = "gxtCellpurge_B";
            this.gxtCellpurge_B.Size = new System.Drawing.Size(449, 179);
            this.gxtCellpurge_B.TabIndex = 21;
            this.gxtCellpurge_B.TabStop = false;
            this.gxtCellpurge_B.Text = "     B     ";
            // 
            // tableLayoutPanel18
            // 
            this.tableLayoutPanel18.ColumnCount = 2;
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.Controls.Add(this.btnCellpurgeBDestructionOn, 0, 1);
            this.tableLayoutPanel18.Controls.Add(this.btnCellpurgeBDestructionOff, 0, 1);
            this.tableLayoutPanel18.Controls.Add(this.btnCellpurgeBPneumaticOff, 1, 0);
            this.tableLayoutPanel18.Controls.Add(this.btnCellpurgeBPneumaticOn, 0, 0);
            this.tableLayoutPanel18.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel18.Name = "tableLayoutPanel18";
            this.tableLayoutPanel18.RowCount = 2;
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel18.Size = new System.Drawing.Size(437, 145);
            this.tableLayoutPanel18.TabIndex = 55;
            // 
            // btnCellpurgeBDestructionOn
            // 
            this.btnCellpurgeBDestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellpurgeBDestructionOn.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellpurgeBDestructionOn.ForeColor = System.Drawing.Color.White;
            this.btnCellpurgeBDestructionOn.Location = new System.Drawing.Point(3, 75);
            this.btnCellpurgeBDestructionOn.Name = "btnCellpurgeBDestructionOn";
            this.btnCellpurgeBDestructionOn.Size = new System.Drawing.Size(212, 66);
            this.btnCellpurgeBDestructionOn.TabIndex = 23;
            this.btnCellpurgeBDestructionOn.Text = "파기 온";
            this.btnCellpurgeBDestructionOn.UseVisualStyleBackColor = false;
            // 
            // btnCellpurgeBDestructionOff
            // 
            this.btnCellpurgeBDestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellpurgeBDestructionOff.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellpurgeBDestructionOff.ForeColor = System.Drawing.Color.White;
            this.btnCellpurgeBDestructionOff.Location = new System.Drawing.Point(221, 75);
            this.btnCellpurgeBDestructionOff.Name = "btnCellpurgeBDestructionOff";
            this.btnCellpurgeBDestructionOff.Size = new System.Drawing.Size(212, 66);
            this.btnCellpurgeBDestructionOff.TabIndex = 24;
            this.btnCellpurgeBDestructionOff.Text = "파기 오프";
            this.btnCellpurgeBDestructionOff.UseVisualStyleBackColor = false;
            // 
            // btnCellpurgeBPneumaticOff
            // 
            this.btnCellpurgeBPneumaticOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellpurgeBPneumaticOff.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellpurgeBPneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btnCellpurgeBPneumaticOff.Location = new System.Drawing.Point(221, 3);
            this.btnCellpurgeBPneumaticOff.Name = "btnCellpurgeBPneumaticOff";
            this.btnCellpurgeBPneumaticOff.Size = new System.Drawing.Size(212, 66);
            this.btnCellpurgeBPneumaticOff.TabIndex = 22;
            this.btnCellpurgeBPneumaticOff.Text = "공압 오프";
            this.btnCellpurgeBPneumaticOff.UseVisualStyleBackColor = false;
            // 
            // btnCellpurgeBPneumaticOn
            // 
            this.btnCellpurgeBPneumaticOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellpurgeBPneumaticOn.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellpurgeBPneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btnCellpurgeBPneumaticOn.Location = new System.Drawing.Point(3, 3);
            this.btnCellpurgeBPneumaticOn.Name = "btnCellpurgeBPneumaticOn";
            this.btnCellpurgeBPneumaticOn.Size = new System.Drawing.Size(212, 66);
            this.btnCellpurgeBPneumaticOn.TabIndex = 21;
            this.btnCellpurgeBPneumaticOn.Text = "공압 온";
            this.btnCellpurgeBPneumaticOn.UseVisualStyleBackColor = false;
            // 
            // gxtCellpurge_A
            // 
            this.gxtCellpurge_A.Controls.Add(this.tableLayoutPanel17);
            this.gxtCellpurge_A.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCellpurge_A.ForeColor = System.Drawing.Color.White;
            this.gxtCellpurge_A.Location = new System.Drawing.Point(792, 410);
            this.gxtCellpurge_A.Name = "gxtCellpurge_A";
            this.gxtCellpurge_A.Size = new System.Drawing.Size(449, 179);
            this.gxtCellpurge_A.TabIndex = 17;
            this.gxtCellpurge_A.TabStop = false;
            this.gxtCellpurge_A.Text = "     A    ";
            // 
            // tableLayoutPanel17
            // 
            this.tableLayoutPanel17.ColumnCount = 2;
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel17.Controls.Add(this.btnCellpurgeADestructionOn, 0, 1);
            this.tableLayoutPanel17.Controls.Add(this.btnCellpurgeADestructionOff, 0, 1);
            this.tableLayoutPanel17.Controls.Add(this.btnCellpurgeAPneumaticOff, 1, 0);
            this.tableLayoutPanel17.Controls.Add(this.btnCellpurgeAPneumaticOn, 0, 0);
            this.tableLayoutPanel17.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel17.Name = "tableLayoutPanel17";
            this.tableLayoutPanel17.RowCount = 2;
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel17.Size = new System.Drawing.Size(437, 145);
            this.tableLayoutPanel17.TabIndex = 54;
            // 
            // btnCellpurgeADestructionOn
            // 
            this.btnCellpurgeADestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellpurgeADestructionOn.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellpurgeADestructionOn.ForeColor = System.Drawing.Color.White;
            this.btnCellpurgeADestructionOn.Location = new System.Drawing.Point(3, 75);
            this.btnCellpurgeADestructionOn.Name = "btnCellpurgeADestructionOn";
            this.btnCellpurgeADestructionOn.Size = new System.Drawing.Size(212, 66);
            this.btnCellpurgeADestructionOn.TabIndex = 19;
            this.btnCellpurgeADestructionOn.Text = "파기 온";
            this.btnCellpurgeADestructionOn.UseVisualStyleBackColor = false;
            // 
            // btnCellpurgeADestructionOff
            // 
            this.btnCellpurgeADestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellpurgeADestructionOff.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellpurgeADestructionOff.ForeColor = System.Drawing.Color.White;
            this.btnCellpurgeADestructionOff.Location = new System.Drawing.Point(221, 75);
            this.btnCellpurgeADestructionOff.Name = "btnCellpurgeADestructionOff";
            this.btnCellpurgeADestructionOff.Size = new System.Drawing.Size(212, 66);
            this.btnCellpurgeADestructionOff.TabIndex = 20;
            this.btnCellpurgeADestructionOff.Text = "파기 오프";
            this.btnCellpurgeADestructionOff.UseVisualStyleBackColor = false;
            // 
            // btnCellpurgeAPneumaticOff
            // 
            this.btnCellpurgeAPneumaticOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellpurgeAPneumaticOff.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellpurgeAPneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btnCellpurgeAPneumaticOff.Location = new System.Drawing.Point(221, 3);
            this.btnCellpurgeAPneumaticOff.Name = "btnCellpurgeAPneumaticOff";
            this.btnCellpurgeAPneumaticOff.Size = new System.Drawing.Size(212, 66);
            this.btnCellpurgeAPneumaticOff.TabIndex = 18;
            this.btnCellpurgeAPneumaticOff.Text = "공압 오프";
            this.btnCellpurgeAPneumaticOff.UseVisualStyleBackColor = false;
            // 
            // btnCellpurgeAPneumaticOn
            // 
            this.btnCellpurgeAPneumaticOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellpurgeAPneumaticOn.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellpurgeAPneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btnCellpurgeAPneumaticOn.Location = new System.Drawing.Point(3, 3);
            this.btnCellpurgeAPneumaticOn.Name = "btnCellpurgeAPneumaticOn";
            this.btnCellpurgeAPneumaticOn.Size = new System.Drawing.Size(212, 66);
            this.btnCellpurgeAPneumaticOn.TabIndex = 17;
            this.btnCellpurgeAPneumaticOn.Text = "공압 온";
            this.btnCellpurgeAPneumaticOn.UseVisualStyleBackColor = false;
            // 
            // btnCellpurgeSave
            // 
            this.btnCellpurgeSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellpurgeSave.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellpurgeSave.ForeColor = System.Drawing.Color.White;
            this.btnCellpurgeSave.Location = new System.Drawing.Point(1482, 783);
            this.btnCellpurgeSave.Name = "btnCellpurgeSave";
            this.btnCellpurgeSave.Size = new System.Drawing.Size(228, 28);
            this.btnCellpurgeSave.TabIndex = 50;
            this.btnCellpurgeSave.Text = "저장";
            this.btnCellpurgeSave.UseVisualStyleBackColor = false;
            // 
            // gxtCellpurge_Ionizer
            // 
            this.gxtCellpurge_Ionizer.Controls.Add(this.btnCellpurgeDestructionOff);
            this.gxtCellpurge_Ionizer.Controls.Add(this.btnCellpurgeIonizerOff);
            this.gxtCellpurge_Ionizer.Controls.Add(this.btnCellpurgeDestructionOn);
            this.gxtCellpurge_Ionizer.Controls.Add(this.btnCellpurgeIonizerOn);
            this.gxtCellpurge_Ionizer.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCellpurge_Ionizer.ForeColor = System.Drawing.Color.White;
            this.gxtCellpurge_Ionizer.Location = new System.Drawing.Point(1483, 223);
            this.gxtCellpurge_Ionizer.Name = "gxtCellpurge_Ionizer";
            this.gxtCellpurge_Ionizer.Size = new System.Drawing.Size(212, 181);
            this.gxtCellpurge_Ionizer.TabIndex = 13;
            this.gxtCellpurge_Ionizer.TabStop = false;
            this.gxtCellpurge_Ionizer.Text = "     이오나이저     ";
            // 
            // btnCellpurgeDestructionOff
            // 
            this.btnCellpurgeDestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellpurgeDestructionOff.Location = new System.Drawing.Point(107, 103);
            this.btnCellpurgeDestructionOff.Name = "btnCellpurgeDestructionOff";
            this.btnCellpurgeDestructionOff.Size = new System.Drawing.Size(99, 72);
            this.btnCellpurgeDestructionOff.TabIndex = 16;
            this.btnCellpurgeDestructionOff.Text = "파기 오프";
            this.btnCellpurgeDestructionOff.UseVisualStyleBackColor = false;
            // 
            // btnCellpurgeIonizerOff
            // 
            this.btnCellpurgeIonizerOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellpurgeIonizerOff.Location = new System.Drawing.Point(107, 28);
            this.btnCellpurgeIonizerOff.Name = "btnCellpurgeIonizerOff";
            this.btnCellpurgeIonizerOff.Size = new System.Drawing.Size(99, 72);
            this.btnCellpurgeIonizerOff.TabIndex = 14;
            this.btnCellpurgeIonizerOff.Text = "이오나이저\r\n오프";
            this.btnCellpurgeIonizerOff.UseVisualStyleBackColor = false;
            // 
            // btnCellpurgeDestructionOn
            // 
            this.btnCellpurgeDestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellpurgeDestructionOn.Location = new System.Drawing.Point(6, 103);
            this.btnCellpurgeDestructionOn.Name = "btnCellpurgeDestructionOn";
            this.btnCellpurgeDestructionOn.Size = new System.Drawing.Size(99, 72);
            this.btnCellpurgeDestructionOn.TabIndex = 15;
            this.btnCellpurgeDestructionOn.Text = "파기 온";
            this.btnCellpurgeDestructionOn.UseVisualStyleBackColor = false;
            // 
            // btnCellpurgeIonizerOn
            // 
            this.btnCellpurgeIonizerOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellpurgeIonizerOn.Location = new System.Drawing.Point(6, 28);
            this.btnCellpurgeIonizerOn.Name = "btnCellpurgeIonizerOn";
            this.btnCellpurgeIonizerOn.Size = new System.Drawing.Size(99, 72);
            this.btnCellpurgeIonizerOn.TabIndex = 13;
            this.btnCellpurgeIonizerOn.Text = "이오나이저\r\n온";
            this.btnCellpurgeIonizerOn.UseVisualStyleBackColor = false;
            // 
            // btnCellpurgeSpeedSetting
            // 
            this.btnCellpurgeSpeedSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellpurgeSpeedSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCellpurgeSpeedSetting.ForeColor = System.Drawing.Color.White;
            this.btnCellpurgeSpeedSetting.Location = new System.Drawing.Point(1291, 315);
            this.btnCellpurgeSpeedSetting.Name = "btnCellpurgeSpeedSetting";
            this.btnCellpurgeSpeedSetting.Size = new System.Drawing.Size(90, 27);
            this.btnCellpurgeSpeedSetting.TabIndex = 12;
            this.btnCellpurgeSpeedSetting.Text = "속도 설정";
            this.btnCellpurgeSpeedSetting.UseVisualStyleBackColor = false;
            // 
            // btnCellpurgeAllSetting
            // 
            this.btnCellpurgeAllSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellpurgeAllSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCellpurgeAllSetting.ForeColor = System.Drawing.Color.White;
            this.btnCellpurgeAllSetting.Location = new System.Drawing.Point(1387, 273);
            this.btnCellpurgeAllSetting.Name = "btnCellpurgeAllSetting";
            this.btnCellpurgeAllSetting.Size = new System.Drawing.Size(90, 27);
            this.btnCellpurgeAllSetting.TabIndex = 8;
            this.btnCellpurgeAllSetting.Text = "전체 설정";
            this.btnCellpurgeAllSetting.UseVisualStyleBackColor = false;
            // 
            // btnCellpurgeLocationSetting
            // 
            this.btnCellpurgeLocationSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellpurgeLocationSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCellpurgeLocationSetting.ForeColor = System.Drawing.Color.White;
            this.btnCellpurgeLocationSetting.Location = new System.Drawing.Point(1071, 315);
            this.btnCellpurgeLocationSetting.Name = "btnCellpurgeLocationSetting";
            this.btnCellpurgeLocationSetting.Size = new System.Drawing.Size(90, 27);
            this.btnCellpurgeLocationSetting.TabIndex = 11;
            this.btnCellpurgeLocationSetting.Text = "위치 설정";
            this.btnCellpurgeLocationSetting.UseVisualStyleBackColor = false;
            // 
            // btnCellpurgeMoveLocation
            // 
            this.btnCellpurgeMoveLocation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellpurgeMoveLocation.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCellpurgeMoveLocation.ForeColor = System.Drawing.Color.White;
            this.btnCellpurgeMoveLocation.Location = new System.Drawing.Point(975, 315);
            this.btnCellpurgeMoveLocation.Name = "btnCellpurgeMoveLocation";
            this.btnCellpurgeMoveLocation.Size = new System.Drawing.Size(90, 27);
            this.btnCellpurgeMoveLocation.TabIndex = 10;
            this.btnCellpurgeMoveLocation.Text = "위치 이동";
            this.btnCellpurgeMoveLocation.UseVisualStyleBackColor = false;
            // 
            // txtCellpurgeLocation
            // 
            this.txtCellpurgeLocation.Font = new System.Drawing.Font("굴림", 13F);
            this.txtCellpurgeLocation.Location = new System.Drawing.Point(879, 316);
            this.txtCellpurgeLocation.Name = "txtCellpurgeLocation";
            this.txtCellpurgeLocation.Size = new System.Drawing.Size(90, 27);
            this.txtCellpurgeLocation.TabIndex = 9;
            // 
            // txtCellpurgeSpeed
            // 
            this.txtCellpurgeSpeed.Font = new System.Drawing.Font("굴림", 13F);
            this.txtCellpurgeSpeed.Location = new System.Drawing.Point(1291, 273);
            this.txtCellpurgeSpeed.Name = "txtCellpurgeSpeed";
            this.txtCellpurgeSpeed.Size = new System.Drawing.Size(90, 27);
            this.txtCellpurgeSpeed.TabIndex = 7;
            // 
            // txtCellpurgeCurrentLocation
            // 
            this.txtCellpurgeCurrentLocation.Font = new System.Drawing.Font("굴림", 13F);
            this.txtCellpurgeCurrentLocation.Location = new System.Drawing.Point(1084, 274);
            this.txtCellpurgeCurrentLocation.Name = "txtCellpurgeCurrentLocation";
            this.txtCellpurgeCurrentLocation.Size = new System.Drawing.Size(90, 27);
            this.txtCellpurgeCurrentLocation.TabIndex = 6;
            // 
            // txtCellpurgeSelectedShaft
            // 
            this.txtCellpurgeSelectedShaft.Font = new System.Drawing.Font("굴림", 13F);
            this.txtCellpurgeSelectedShaft.Location = new System.Drawing.Point(879, 273);
            this.txtCellpurgeSelectedShaft.Name = "txtCellpurgeSelectedShaft";
            this.txtCellpurgeSelectedShaft.Size = new System.Drawing.Size(90, 27);
            this.txtCellpurgeSelectedShaft.TabIndex = 5;
            // 
            // lbl_Cellpurge_Location
            // 
            this.lbl_Cellpurge_Location.AutoSize = true;
            this.lbl_Cellpurge_Location.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Cellpurge_Location.ForeColor = System.Drawing.Color.White;
            this.lbl_Cellpurge_Location.Location = new System.Drawing.Point(801, 316);
            this.lbl_Cellpurge_Location.Name = "lbl_Cellpurge_Location";
            this.lbl_Cellpurge_Location.Size = new System.Drawing.Size(84, 21);
            this.lbl_Cellpurge_Location.TabIndex = 0;
            this.lbl_Cellpurge_Location.Text = "위치[mm]";
            // 
            // lbl_Cellpurge_Speed
            // 
            this.lbl_Cellpurge_Speed.AutoSize = true;
            this.lbl_Cellpurge_Speed.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Cellpurge_Speed.ForeColor = System.Drawing.Color.White;
            this.lbl_Cellpurge_Speed.Location = new System.Drawing.Point(1179, 275);
            this.lbl_Cellpurge_Speed.Name = "lbl_Cellpurge_Speed";
            this.lbl_Cellpurge_Speed.Size = new System.Drawing.Size(115, 21);
            this.lbl_Cellpurge_Speed.TabIndex = 0;
            this.lbl_Cellpurge_Speed.Text = "속도[mm/sec]";
            // 
            // lbl_Cellpurge_SelectedShaft
            // 
            this.lbl_Cellpurge_SelectedShaft.AutoSize = true;
            this.lbl_Cellpurge_SelectedShaft.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Cellpurge_SelectedShaft.ForeColor = System.Drawing.Color.White;
            this.lbl_Cellpurge_SelectedShaft.Location = new System.Drawing.Point(805, 274);
            this.lbl_Cellpurge_SelectedShaft.Name = "lbl_Cellpurge_SelectedShaft";
            this.lbl_Cellpurge_SelectedShaft.Size = new System.Drawing.Size(80, 21);
            this.lbl_Cellpurge_SelectedShaft.TabIndex = 0;
            this.lbl_Cellpurge_SelectedShaft.Text = "선택된 축";
            // 
            // txtCellpurge_LocationSetting
            // 
            this.txtCellpurge_LocationSetting.BackColor = System.Drawing.SystemColors.Highlight;
            this.txtCellpurge_LocationSetting.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.txtCellpurge_LocationSetting.ForeColor = System.Drawing.Color.White;
            this.txtCellpurge_LocationSetting.Location = new System.Drawing.Point(792, 223);
            this.txtCellpurge_LocationSetting.Name = "txtCellpurge_LocationSetting";
            this.txtCellpurge_LocationSetting.ReadOnly = true;
            this.txtCellpurge_LocationSetting.Size = new System.Drawing.Size(89, 27);
            this.txtCellpurge_LocationSetting.TabIndex = 0;
            this.txtCellpurge_LocationSetting.TabStop = false;
            this.txtCellpurge_LocationSetting.Text = "위치값 설정";
            this.txtCellpurge_LocationSetting.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbl_Cellpurge_CurrentLocation
            // 
            this.lbl_Cellpurge_CurrentLocation.AutoSize = true;
            this.lbl_Cellpurge_CurrentLocation.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Cellpurge_CurrentLocation.ForeColor = System.Drawing.Color.White;
            this.lbl_Cellpurge_CurrentLocation.Location = new System.Drawing.Point(975, 274);
            this.lbl_Cellpurge_CurrentLocation.Name = "lbl_Cellpurge_CurrentLocation";
            this.lbl_Cellpurge_CurrentLocation.Size = new System.Drawing.Size(116, 21);
            this.lbl_Cellpurge_CurrentLocation.TabIndex = 0;
            this.lbl_Cellpurge_CurrentLocation.Text = "현재위치[mm]";
            // 
            // btnCellpurgeGrabSwitch3
            // 
            this.btnCellpurgeGrabSwitch3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellpurgeGrabSwitch3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellpurgeGrabSwitch3.ForeColor = System.Drawing.Color.White;
            this.btnCellpurgeGrabSwitch3.Location = new System.Drawing.Point(1514, 6);
            this.btnCellpurgeGrabSwitch3.Name = "btnCellpurgeGrabSwitch3";
            this.btnCellpurgeGrabSwitch3.Size = new System.Drawing.Size(172, 23);
            this.btnCellpurgeGrabSwitch3.TabIndex = 3;
            this.btnCellpurgeGrabSwitch3.Text = "Grab Switch 3";
            this.btnCellpurgeGrabSwitch3.UseVisualStyleBackColor = false;
            // 
            // btnCellpurgeGrabSwitch2
            // 
            this.btnCellpurgeGrabSwitch2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellpurgeGrabSwitch2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellpurgeGrabSwitch2.ForeColor = System.Drawing.Color.White;
            this.btnCellpurgeGrabSwitch2.Location = new System.Drawing.Point(1336, 6);
            this.btnCellpurgeGrabSwitch2.Name = "btnCellpurgeGrabSwitch2";
            this.btnCellpurgeGrabSwitch2.Size = new System.Drawing.Size(172, 23);
            this.btnCellpurgeGrabSwitch2.TabIndex = 2;
            this.btnCellpurgeGrabSwitch2.Text = "Grab Switch 2";
            this.btnCellpurgeGrabSwitch2.UseVisualStyleBackColor = false;
            // 
            // btnCellpurgeGrabSwitch1
            // 
            this.btnCellpurgeGrabSwitch1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellpurgeGrabSwitch1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellpurgeGrabSwitch1.ForeColor = System.Drawing.Color.White;
            this.btnCellpurgeGrabSwitch1.Location = new System.Drawing.Point(1158, 6);
            this.btnCellpurgeGrabSwitch1.Name = "btnCellpurgeGrabSwitch1";
            this.btnCellpurgeGrabSwitch1.Size = new System.Drawing.Size(172, 23);
            this.btnCellpurgeGrabSwitch1.TabIndex = 1;
            this.btnCellpurgeGrabSwitch1.Text = "Grab Switch 1";
            this.btnCellpurgeGrabSwitch1.UseVisualStyleBackColor = false;
            // 
            // lvCellpurge
            // 
            this.lvCellpurge.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.col_Cellpurge_NamebyLocation,
            this.col_Cellpurge_Shaft,
            this.col_Cellpurge_Location,
            this.col_Cellpurge_Speed});
            this.lvCellpurge.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lvCellpurge.GridLines = true;
            this.lvCellpurge.Location = new System.Drawing.Point(17, 16);
            this.lvCellpurge.Name = "lvCellpurge";
            this.lvCellpurge.Size = new System.Drawing.Size(758, 734);
            this.lvCellpurge.TabIndex = 0;
            this.lvCellpurge.TabStop = false;
            this.lvCellpurge.UseCompatibleStateImageBehavior = false;
            this.lvCellpurge.View = System.Windows.Forms.View.Details;
            // 
            // col_Cellpurge_NamebyLocation
            // 
            this.col_Cellpurge_NamebyLocation.Text = "위치별 명칭";
            this.col_Cellpurge_NamebyLocation.Width = 430;
            // 
            // col_Cellpurge_Shaft
            // 
            this.col_Cellpurge_Shaft.Text = "축";
            this.col_Cellpurge_Shaft.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // col_Cellpurge_Location
            // 
            this.col_Cellpurge_Location.Text = "위치[mm]";
            this.col_Cellpurge_Location.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_Cellpurge_Location.Width = 125;
            // 
            // col_Cellpurge_Speed
            // 
            this.col_Cellpurge_Speed.Text = "속도[mm/s]";
            this.col_Cellpurge_Speed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_Cellpurge_Speed.Width = 125;
            // 
            // ajin_Setting_Cellpurge
            // 
            this.ajin_Setting_Cellpurge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_Cellpurge.Location = new System.Drawing.Point(792, 34);
            this.ajin_Setting_Cellpurge.Name = "ajin_Setting_Cellpurge";
            this.ajin_Setting_Cellpurge.Servo = null;
            this.ajin_Setting_Cellpurge.Size = new System.Drawing.Size(903, 183);
            this.ajin_Setting_Cellpurge.TabIndex = 4;
            // 
            // tp_CellLoad
            // 
            this.tp_CellLoad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tp_CellLoad.Controls.Add(this.gxtCellLoad_Move);
            this.tp_CellLoad.Controls.Add(this.gxtCellLoad_B);
            this.tp_CellLoad.Controls.Add(this.gxtCellLoad_A);
            this.tp_CellLoad.Controls.Add(this.btnCellLoadSave);
            this.tp_CellLoad.Controls.Add(this.btnCellLoadSpeedSetting);
            this.tp_CellLoad.Controls.Add(this.btnCellLoadAllSetting);
            this.tp_CellLoad.Controls.Add(this.btnCellLoadLocationSetting);
            this.tp_CellLoad.Controls.Add(this.btnCellLoadMoveLocation);
            this.tp_CellLoad.Controls.Add(this.txtCellLoadLocation);
            this.tp_CellLoad.Controls.Add(this.txtCellLoadSpeed);
            this.tp_CellLoad.Controls.Add(this.txtCellLoadCurrentLocation);
            this.tp_CellLoad.Controls.Add(this.txtCellLoadSelectedShaft);
            this.tp_CellLoad.Controls.Add(this.lbl_CellLoad_Location);
            this.tp_CellLoad.Controls.Add(this.lbl_CellLoad_Speed);
            this.tp_CellLoad.Controls.Add(this.lbl_CellLoad_SelectedShaft);
            this.tp_CellLoad.Controls.Add(this.txtCellLoad_LocationSetting);
            this.tp_CellLoad.Controls.Add(this.lbl_CellLoad_CurrentLocation);
            this.tp_CellLoad.Controls.Add(this.btnCellLoadGrabSwitch3);
            this.tp_CellLoad.Controls.Add(this.btnCellLoadGrabSwitch2);
            this.tp_CellLoad.Controls.Add(this.btnCellLoadGrabSwitch1);
            this.tp_CellLoad.Controls.Add(this.lvCellLoad);
            this.tp_CellLoad.Controls.Add(this.ajin_Setting_CellLoad);
            this.tp_CellLoad.Location = new System.Drawing.Point(4, 34);
            this.tp_CellLoad.Name = "tp_CellLoad";
            this.tp_CellLoad.Size = new System.Drawing.Size(1726, 816);
            this.tp_CellLoad.TabIndex = 2;
            this.tp_CellLoad.Text = "셀 로드 이재기";
            // 
            // gxtCellLoad_Move
            // 
            this.gxtCellLoad_Move.Controls.Add(this.gxtCellLoad_MoveLorUn);
            this.gxtCellLoad_Move.Controls.Add(this.gxtCellLoad_MoveB);
            this.gxtCellLoad_Move.Controls.Add(this.gxtCellLoad_MoveA);
            this.gxtCellLoad_Move.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCellLoad_Move.ForeColor = System.Drawing.Color.White;
            this.gxtCellLoad_Move.Location = new System.Drawing.Point(792, 595);
            this.gxtCellLoad_Move.Name = "gxtCellLoad_Move";
            this.gxtCellLoad_Move.Size = new System.Drawing.Size(904, 179);
            this.gxtCellLoad_Move.TabIndex = 53;
            this.gxtCellLoad_Move.TabStop = false;
            this.gxtCellLoad_Move.Text = "     이동     ";
            // 
            // gxtCellLoad_MoveLorUn
            // 
            this.gxtCellLoad_MoveLorUn.Controls.Add(this.tableLayoutPanel28);
            this.gxtCellLoad_MoveLorUn.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCellLoad_MoveLorUn.ForeColor = System.Drawing.Color.White;
            this.gxtCellLoad_MoveLorUn.Location = new System.Drawing.Point(7, 21);
            this.gxtCellLoad_MoveLorUn.Name = "gxtCellLoad_MoveLorUn";
            this.gxtCellLoad_MoveLorUn.Size = new System.Drawing.Size(293, 152);
            this.gxtCellLoad_MoveLorUn.TabIndex = 57;
            this.gxtCellLoad_MoveLorUn.TabStop = false;
            this.gxtCellLoad_MoveLorUn.Text = "     로딩/언로딩    ";
            // 
            // tableLayoutPanel28
            // 
            this.tableLayoutPanel28.ColumnCount = 2;
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel28.Controls.Add(this.btnCellLoadMoveLorUnPreAlignMark1, 0, 2);
            this.tableLayoutPanel28.Controls.Add(this.btnCellLoadMoveLorUnPreAlignMark2, 0, 2);
            this.tableLayoutPanel28.Controls.Add(this.btnCellLoadMoveLorUnY1UnL, 0, 1);
            this.tableLayoutPanel28.Controls.Add(this.btnCellLoadMoveLorUnY1L, 0, 1);
            this.tableLayoutPanel28.Controls.Add(this.btnCellLoadMoveLorUnX2L, 1, 0);
            this.tableLayoutPanel28.Controls.Add(this.btnCellLoadMoveLorUnX1L, 0, 0);
            this.tableLayoutPanel28.Location = new System.Drawing.Point(6, 21);
            this.tableLayoutPanel28.Name = "tableLayoutPanel28";
            this.tableLayoutPanel28.RowCount = 3;
            this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel28.Size = new System.Drawing.Size(281, 125);
            this.tableLayoutPanel28.TabIndex = 55;
            // 
            // btnCellLoadMoveLorUnPreAlignMark1
            // 
            this.btnCellLoadMoveLorUnPreAlignMark1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadMoveLorUnPreAlignMark1.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellLoadMoveLorUnPreAlignMark1.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadMoveLorUnPreAlignMark1.Location = new System.Drawing.Point(3, 85);
            this.btnCellLoadMoveLorUnPreAlignMark1.Name = "btnCellLoadMoveLorUnPreAlignMark1";
            this.btnCellLoadMoveLorUnPreAlignMark1.Size = new System.Drawing.Size(134, 37);
            this.btnCellLoadMoveLorUnPreAlignMark1.TabIndex = 60;
            this.btnCellLoadMoveLorUnPreAlignMark1.Text = "PreAlign\r\nMark1";
            this.btnCellLoadMoveLorUnPreAlignMark1.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadMoveLorUnPreAlignMark2
            // 
            this.btnCellLoadMoveLorUnPreAlignMark2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadMoveLorUnPreAlignMark2.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellLoadMoveLorUnPreAlignMark2.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadMoveLorUnPreAlignMark2.Location = new System.Drawing.Point(143, 85);
            this.btnCellLoadMoveLorUnPreAlignMark2.Name = "btnCellLoadMoveLorUnPreAlignMark2";
            this.btnCellLoadMoveLorUnPreAlignMark2.Size = new System.Drawing.Size(134, 37);
            this.btnCellLoadMoveLorUnPreAlignMark2.TabIndex = 59;
            this.btnCellLoadMoveLorUnPreAlignMark2.Text = "PreAlign\r\nMark2";
            this.btnCellLoadMoveLorUnPreAlignMark2.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadMoveLorUnY1UnL
            // 
            this.btnCellLoadMoveLorUnY1UnL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadMoveLorUnY1UnL.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellLoadMoveLorUnY1UnL.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadMoveLorUnY1UnL.Location = new System.Drawing.Point(143, 44);
            this.btnCellLoadMoveLorUnY1UnL.Name = "btnCellLoadMoveLorUnY1UnL";
            this.btnCellLoadMoveLorUnY1UnL.Size = new System.Drawing.Size(134, 35);
            this.btnCellLoadMoveLorUnY1UnL.TabIndex = 58;
            this.btnCellLoadMoveLorUnY1UnL.Text = "Y1\r\n언로딩";
            this.btnCellLoadMoveLorUnY1UnL.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadMoveLorUnY1L
            // 
            this.btnCellLoadMoveLorUnY1L.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadMoveLorUnY1L.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellLoadMoveLorUnY1L.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadMoveLorUnY1L.Location = new System.Drawing.Point(3, 44);
            this.btnCellLoadMoveLorUnY1L.Name = "btnCellLoadMoveLorUnY1L";
            this.btnCellLoadMoveLorUnY1L.Size = new System.Drawing.Size(134, 35);
            this.btnCellLoadMoveLorUnY1L.TabIndex = 56;
            this.btnCellLoadMoveLorUnY1L.Text = "Y1\r\n로딩";
            this.btnCellLoadMoveLorUnY1L.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadMoveLorUnX2L
            // 
            this.btnCellLoadMoveLorUnX2L.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadMoveLorUnX2L.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellLoadMoveLorUnX2L.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadMoveLorUnX2L.Location = new System.Drawing.Point(143, 3);
            this.btnCellLoadMoveLorUnX2L.Name = "btnCellLoadMoveLorUnX2L";
            this.btnCellLoadMoveLorUnX2L.Size = new System.Drawing.Size(135, 35);
            this.btnCellLoadMoveLorUnX2L.TabIndex = 55;
            this.btnCellLoadMoveLorUnX2L.Text = "X2\r\n로딩";
            this.btnCellLoadMoveLorUnX2L.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadMoveLorUnX1L
            // 
            this.btnCellLoadMoveLorUnX1L.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadMoveLorUnX1L.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellLoadMoveLorUnX1L.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadMoveLorUnX1L.Location = new System.Drawing.Point(3, 3);
            this.btnCellLoadMoveLorUnX1L.Name = "btnCellLoadMoveLorUnX1L";
            this.btnCellLoadMoveLorUnX1L.Size = new System.Drawing.Size(134, 35);
            this.btnCellLoadMoveLorUnX1L.TabIndex = 57;
            this.btnCellLoadMoveLorUnX1L.Text = "X1\r\n로딩";
            this.btnCellLoadMoveLorUnX1L.UseVisualStyleBackColor = false;
            // 
            // gxtCellLoad_MoveB
            // 
            this.gxtCellLoad_MoveB.Controls.Add(this.tableLayoutPanel31);
            this.gxtCellLoad_MoveB.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCellLoad_MoveB.ForeColor = System.Drawing.Color.White;
            this.gxtCellLoad_MoveB.Location = new System.Drawing.Point(605, 21);
            this.gxtCellLoad_MoveB.Name = "gxtCellLoad_MoveB";
            this.gxtCellLoad_MoveB.Size = new System.Drawing.Size(293, 152);
            this.gxtCellLoad_MoveB.TabIndex = 56;
            this.gxtCellLoad_MoveB.TabStop = false;
            this.gxtCellLoad_MoveB.Text = "     B    ";
            // 
            // tableLayoutPanel31
            // 
            this.tableLayoutPanel31.ColumnCount = 1;
            this.tableLayoutPanel31.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel31.Controls.Add(this.tableLayoutPanel32, 0, 2);
            this.tableLayoutPanel31.Controls.Add(this.tableLayoutPanel33, 0, 1);
            this.tableLayoutPanel31.Controls.Add(this.tableLayoutPanel34, 0, 0);
            this.tableLayoutPanel31.Location = new System.Drawing.Point(6, 21);
            this.tableLayoutPanel31.Name = "tableLayoutPanel31";
            this.tableLayoutPanel31.RowCount = 3;
            this.tableLayoutPanel31.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel31.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel31.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel31.Size = new System.Drawing.Size(281, 125);
            this.tableLayoutPanel31.TabIndex = 1;
            // 
            // tableLayoutPanel32
            // 
            this.tableLayoutPanel32.ColumnCount = 3;
            this.tableLayoutPanel32.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel32.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel32.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel32.Controls.Add(this.btnCellLoadMoveBBPickerM90Angle, 0, 0);
            this.tableLayoutPanel32.Controls.Add(this.btnCellLoadMoveBBPickerP90Angle, 0, 0);
            this.tableLayoutPanel32.Controls.Add(this.btnCellLoadMoveBBPicker0Angle, 0, 0);
            this.tableLayoutPanel32.Location = new System.Drawing.Point(3, 85);
            this.tableLayoutPanel32.Name = "tableLayoutPanel32";
            this.tableLayoutPanel32.RowCount = 1;
            this.tableLayoutPanel32.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel32.Size = new System.Drawing.Size(275, 35);
            this.tableLayoutPanel32.TabIndex = 2;
            // 
            // btnCellLoadMoveBBPickerM90Angle
            // 
            this.btnCellLoadMoveBBPickerM90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadMoveBBPickerM90Angle.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnCellLoadMoveBBPickerM90Angle.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadMoveBBPickerM90Angle.Location = new System.Drawing.Point(185, 3);
            this.btnCellLoadMoveBBPickerM90Angle.Name = "btnCellLoadMoveBBPickerM90Angle";
            this.btnCellLoadMoveBBPickerM90Angle.Size = new System.Drawing.Size(87, 29);
            this.btnCellLoadMoveBBPickerM90Angle.TabIndex = 60;
            this.btnCellLoadMoveBBPickerM90Angle.Text = "B 피커 -90도";
            this.btnCellLoadMoveBBPickerM90Angle.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadMoveBBPickerP90Angle
            // 
            this.btnCellLoadMoveBBPickerP90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadMoveBBPickerP90Angle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnCellLoadMoveBBPickerP90Angle.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadMoveBBPickerP90Angle.Location = new System.Drawing.Point(94, 3);
            this.btnCellLoadMoveBBPickerP90Angle.Name = "btnCellLoadMoveBBPickerP90Angle";
            this.btnCellLoadMoveBBPickerP90Angle.Size = new System.Drawing.Size(85, 29);
            this.btnCellLoadMoveBBPickerP90Angle.TabIndex = 59;
            this.btnCellLoadMoveBBPickerP90Angle.Text = "B 피커 +90도";
            this.btnCellLoadMoveBBPickerP90Angle.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadMoveBBPicker0Angle
            // 
            this.btnCellLoadMoveBBPicker0Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadMoveBBPicker0Angle.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnCellLoadMoveBBPicker0Angle.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadMoveBBPicker0Angle.Location = new System.Drawing.Point(3, 3);
            this.btnCellLoadMoveBBPicker0Angle.Name = "btnCellLoadMoveBBPicker0Angle";
            this.btnCellLoadMoveBBPicker0Angle.Size = new System.Drawing.Size(85, 29);
            this.btnCellLoadMoveBBPicker0Angle.TabIndex = 58;
            this.btnCellLoadMoveBBPicker0Angle.Text = "B 피커 0도";
            this.btnCellLoadMoveBBPicker0Angle.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel33
            // 
            this.tableLayoutPanel33.ColumnCount = 2;
            this.tableLayoutPanel33.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel33.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel33.Controls.Add(this.btnCellLoadMoveBBPickerUnL, 0, 0);
            this.tableLayoutPanel33.Controls.Add(this.btnCellLoadMoveBBPickerL, 0, 0);
            this.tableLayoutPanel33.Location = new System.Drawing.Point(3, 44);
            this.tableLayoutPanel33.Name = "tableLayoutPanel33";
            this.tableLayoutPanel33.RowCount = 1;
            this.tableLayoutPanel33.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel33.Size = new System.Drawing.Size(275, 35);
            this.tableLayoutPanel33.TabIndex = 1;
            // 
            // btnCellLoadMoveBBPickerUnL
            // 
            this.btnCellLoadMoveBBPickerUnL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadMoveBBPickerUnL.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnCellLoadMoveBBPickerUnL.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadMoveBBPickerUnL.Location = new System.Drawing.Point(140, 3);
            this.btnCellLoadMoveBBPickerUnL.Name = "btnCellLoadMoveBBPickerUnL";
            this.btnCellLoadMoveBBPickerUnL.Size = new System.Drawing.Size(132, 29);
            this.btnCellLoadMoveBBPickerUnL.TabIndex = 59;
            this.btnCellLoadMoveBBPickerUnL.Text = "B 피커 언로딩";
            this.btnCellLoadMoveBBPickerUnL.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadMoveBBPickerL
            // 
            this.btnCellLoadMoveBBPickerL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadMoveBBPickerL.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnCellLoadMoveBBPickerL.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadMoveBBPickerL.Location = new System.Drawing.Point(3, 3);
            this.btnCellLoadMoveBBPickerL.Name = "btnCellLoadMoveBBPickerL";
            this.btnCellLoadMoveBBPickerL.Size = new System.Drawing.Size(131, 29);
            this.btnCellLoadMoveBBPickerL.TabIndex = 58;
            this.btnCellLoadMoveBBPickerL.Text = "B 피커 로딩";
            this.btnCellLoadMoveBBPickerL.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel34
            // 
            this.tableLayoutPanel34.ColumnCount = 2;
            this.tableLayoutPanel34.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel34.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel34.Controls.Add(this.btnCellLoadMoveBX2BStageUnL, 0, 0);
            this.tableLayoutPanel34.Controls.Add(this.btnCellLoadMoveBX1BStageUnL, 0, 0);
            this.tableLayoutPanel34.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel34.Name = "tableLayoutPanel34";
            this.tableLayoutPanel34.RowCount = 1;
            this.tableLayoutPanel34.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel34.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel34.Size = new System.Drawing.Size(275, 35);
            this.tableLayoutPanel34.TabIndex = 0;
            // 
            // btnCellLoadMoveBX2BStageUnL
            // 
            this.btnCellLoadMoveBX2BStageUnL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadMoveBX2BStageUnL.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellLoadMoveBX2BStageUnL.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadMoveBX2BStageUnL.Location = new System.Drawing.Point(140, 3);
            this.btnCellLoadMoveBX2BStageUnL.Name = "btnCellLoadMoveBX2BStageUnL";
            this.btnCellLoadMoveBX2BStageUnL.Size = new System.Drawing.Size(132, 29);
            this.btnCellLoadMoveBX2BStageUnL.TabIndex = 59;
            this.btnCellLoadMoveBX2BStageUnL.Text = "X2 B 스테이지 언로딩";
            this.btnCellLoadMoveBX2BStageUnL.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadMoveBX1BStageUnL
            // 
            this.btnCellLoadMoveBX1BStageUnL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadMoveBX1BStageUnL.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellLoadMoveBX1BStageUnL.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadMoveBX1BStageUnL.Location = new System.Drawing.Point(3, 3);
            this.btnCellLoadMoveBX1BStageUnL.Name = "btnCellLoadMoveBX1BStageUnL";
            this.btnCellLoadMoveBX1BStageUnL.Size = new System.Drawing.Size(131, 29);
            this.btnCellLoadMoveBX1BStageUnL.TabIndex = 58;
            this.btnCellLoadMoveBX1BStageUnL.Text = "X1 B 스테이지 언로딩";
            this.btnCellLoadMoveBX1BStageUnL.UseVisualStyleBackColor = false;
            // 
            // gxtCellLoad_MoveA
            // 
            this.gxtCellLoad_MoveA.Controls.Add(this.tableLayoutPanel26);
            this.gxtCellLoad_MoveA.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCellLoad_MoveA.ForeColor = System.Drawing.Color.White;
            this.gxtCellLoad_MoveA.Location = new System.Drawing.Point(306, 21);
            this.gxtCellLoad_MoveA.Name = "gxtCellLoad_MoveA";
            this.gxtCellLoad_MoveA.Size = new System.Drawing.Size(293, 152);
            this.gxtCellLoad_MoveA.TabIndex = 31;
            this.gxtCellLoad_MoveA.TabStop = false;
            this.gxtCellLoad_MoveA.Text = "     A    ";
            // 
            // tableLayoutPanel26
            // 
            this.tableLayoutPanel26.ColumnCount = 1;
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel26.Controls.Add(this.tableLayoutPanel30, 0, 2);
            this.tableLayoutPanel26.Controls.Add(this.tableLayoutPanel29, 0, 1);
            this.tableLayoutPanel26.Controls.Add(this.tableLayoutPanel27, 0, 0);
            this.tableLayoutPanel26.Location = new System.Drawing.Point(6, 21);
            this.tableLayoutPanel26.Name = "tableLayoutPanel26";
            this.tableLayoutPanel26.RowCount = 3;
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel26.Size = new System.Drawing.Size(281, 125);
            this.tableLayoutPanel26.TabIndex = 0;
            // 
            // tableLayoutPanel30
            // 
            this.tableLayoutPanel30.ColumnCount = 3;
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel30.Controls.Add(this.btnCellLoadMoveAAPickerM90Angle, 0, 0);
            this.tableLayoutPanel30.Controls.Add(this.btnCellLoadMoveAAPickerP90Angle, 0, 0);
            this.tableLayoutPanel30.Controls.Add(this.btnCellLoadMoveAAPicker0Angle, 0, 0);
            this.tableLayoutPanel30.Location = new System.Drawing.Point(3, 85);
            this.tableLayoutPanel30.Name = "tableLayoutPanel30";
            this.tableLayoutPanel30.RowCount = 1;
            this.tableLayoutPanel30.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel30.Size = new System.Drawing.Size(275, 35);
            this.tableLayoutPanel30.TabIndex = 2;
            // 
            // btnCellLoadMoveAAPickerM90Angle
            // 
            this.btnCellLoadMoveAAPickerM90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadMoveAAPickerM90Angle.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnCellLoadMoveAAPickerM90Angle.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadMoveAAPickerM90Angle.Location = new System.Drawing.Point(185, 3);
            this.btnCellLoadMoveAAPickerM90Angle.Name = "btnCellLoadMoveAAPickerM90Angle";
            this.btnCellLoadMoveAAPickerM90Angle.Size = new System.Drawing.Size(87, 29);
            this.btnCellLoadMoveAAPickerM90Angle.TabIndex = 60;
            this.btnCellLoadMoveAAPickerM90Angle.Text = "A 피커 -90도";
            this.btnCellLoadMoveAAPickerM90Angle.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadMoveAAPickerP90Angle
            // 
            this.btnCellLoadMoveAAPickerP90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadMoveAAPickerP90Angle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnCellLoadMoveAAPickerP90Angle.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadMoveAAPickerP90Angle.Location = new System.Drawing.Point(94, 3);
            this.btnCellLoadMoveAAPickerP90Angle.Name = "btnCellLoadMoveAAPickerP90Angle";
            this.btnCellLoadMoveAAPickerP90Angle.Size = new System.Drawing.Size(85, 29);
            this.btnCellLoadMoveAAPickerP90Angle.TabIndex = 59;
            this.btnCellLoadMoveAAPickerP90Angle.Text = "A 피커 +90도";
            this.btnCellLoadMoveAAPickerP90Angle.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadMoveAAPicker0Angle
            // 
            this.btnCellLoadMoveAAPicker0Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadMoveAAPicker0Angle.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnCellLoadMoveAAPicker0Angle.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadMoveAAPicker0Angle.Location = new System.Drawing.Point(3, 3);
            this.btnCellLoadMoveAAPicker0Angle.Name = "btnCellLoadMoveAAPicker0Angle";
            this.btnCellLoadMoveAAPicker0Angle.Size = new System.Drawing.Size(85, 29);
            this.btnCellLoadMoveAAPicker0Angle.TabIndex = 58;
            this.btnCellLoadMoveAAPicker0Angle.Text = "A 피커 0도";
            this.btnCellLoadMoveAAPicker0Angle.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel29
            // 
            this.tableLayoutPanel29.ColumnCount = 2;
            this.tableLayoutPanel29.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel29.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel29.Controls.Add(this.btnCellLoadMoveAAPickerUnL, 0, 0);
            this.tableLayoutPanel29.Controls.Add(this.btnCellLoadMoveAAPickerL, 0, 0);
            this.tableLayoutPanel29.Location = new System.Drawing.Point(3, 44);
            this.tableLayoutPanel29.Name = "tableLayoutPanel29";
            this.tableLayoutPanel29.RowCount = 1;
            this.tableLayoutPanel29.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel29.Size = new System.Drawing.Size(275, 35);
            this.tableLayoutPanel29.TabIndex = 1;
            // 
            // btnCellLoadMoveAAPickerUnL
            // 
            this.btnCellLoadMoveAAPickerUnL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadMoveAAPickerUnL.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnCellLoadMoveAAPickerUnL.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadMoveAAPickerUnL.Location = new System.Drawing.Point(140, 3);
            this.btnCellLoadMoveAAPickerUnL.Name = "btnCellLoadMoveAAPickerUnL";
            this.btnCellLoadMoveAAPickerUnL.Size = new System.Drawing.Size(132, 29);
            this.btnCellLoadMoveAAPickerUnL.TabIndex = 59;
            this.btnCellLoadMoveAAPickerUnL.Text = "A 피커 언로딩";
            this.btnCellLoadMoveAAPickerUnL.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadMoveAAPickerL
            // 
            this.btnCellLoadMoveAAPickerL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadMoveAAPickerL.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnCellLoadMoveAAPickerL.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadMoveAAPickerL.Location = new System.Drawing.Point(3, 3);
            this.btnCellLoadMoveAAPickerL.Name = "btnCellLoadMoveAAPickerL";
            this.btnCellLoadMoveAAPickerL.Size = new System.Drawing.Size(131, 29);
            this.btnCellLoadMoveAAPickerL.TabIndex = 58;
            this.btnCellLoadMoveAAPickerL.Text = "A 피커 로딩";
            this.btnCellLoadMoveAAPickerL.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel27
            // 
            this.tableLayoutPanel27.ColumnCount = 2;
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel27.Controls.Add(this.btnCellLoadMoveAX2AStageUnL, 0, 0);
            this.tableLayoutPanel27.Controls.Add(this.btnCellLoadMoveAX1AStageUnL, 0, 0);
            this.tableLayoutPanel27.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel27.Name = "tableLayoutPanel27";
            this.tableLayoutPanel27.RowCount = 1;
            this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel27.Size = new System.Drawing.Size(275, 35);
            this.tableLayoutPanel27.TabIndex = 0;
            // 
            // btnCellLoadMoveAX2AStageUnL
            // 
            this.btnCellLoadMoveAX2AStageUnL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadMoveAX2AStageUnL.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellLoadMoveAX2AStageUnL.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadMoveAX2AStageUnL.Location = new System.Drawing.Point(140, 3);
            this.btnCellLoadMoveAX2AStageUnL.Name = "btnCellLoadMoveAX2AStageUnL";
            this.btnCellLoadMoveAX2AStageUnL.Size = new System.Drawing.Size(132, 29);
            this.btnCellLoadMoveAX2AStageUnL.TabIndex = 59;
            this.btnCellLoadMoveAX2AStageUnL.Text = "X2 A 스테이지 언로딩";
            this.btnCellLoadMoveAX2AStageUnL.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadMoveAX1AStageUnL
            // 
            this.btnCellLoadMoveAX1AStageUnL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadMoveAX1AStageUnL.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellLoadMoveAX1AStageUnL.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadMoveAX1AStageUnL.Location = new System.Drawing.Point(3, 3);
            this.btnCellLoadMoveAX1AStageUnL.Name = "btnCellLoadMoveAX1AStageUnL";
            this.btnCellLoadMoveAX1AStageUnL.Size = new System.Drawing.Size(131, 29);
            this.btnCellLoadMoveAX1AStageUnL.TabIndex = 58;
            this.btnCellLoadMoveAX1AStageUnL.Text = "X1 A 스테이지 언로딩";
            this.btnCellLoadMoveAX1AStageUnL.UseVisualStyleBackColor = false;
            // 
            // gxtCellLoad_B
            // 
            this.gxtCellLoad_B.Controls.Add(this.tableLayoutPanel25);
            this.gxtCellLoad_B.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCellLoad_B.ForeColor = System.Drawing.Color.White;
            this.gxtCellLoad_B.Location = new System.Drawing.Point(1247, 410);
            this.gxtCellLoad_B.Name = "gxtCellLoad_B";
            this.gxtCellLoad_B.Size = new System.Drawing.Size(449, 179);
            this.gxtCellLoad_B.TabIndex = 52;
            this.gxtCellLoad_B.TabStop = false;
            this.gxtCellLoad_B.Text = "     B     ";
            // 
            // tableLayoutPanel25
            // 
            this.tableLayoutPanel25.ColumnCount = 2;
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel25.Controls.Add(this.btnCellLoadBPickerDestructionOn, 0, 2);
            this.tableLayoutPanel25.Controls.Add(this.btnCellLoadBPickerDestructionOff, 0, 2);
            this.tableLayoutPanel25.Controls.Add(this.btnCellLoadBPickerUp, 0, 0);
            this.tableLayoutPanel25.Controls.Add(this.btnCellLoadBPickerDown, 1, 0);
            this.tableLayoutPanel25.Controls.Add(this.btnCellLoadBPickerPneumaticOn, 0, 1);
            this.tableLayoutPanel25.Controls.Add(this.btnCellLoadBPickerPneumaticOff, 1, 1);
            this.tableLayoutPanel25.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel25.Name = "tableLayoutPanel25";
            this.tableLayoutPanel25.RowCount = 3;
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel25.Size = new System.Drawing.Size(437, 145);
            this.tableLayoutPanel25.TabIndex = 55;
            // 
            // btnCellLoadBPickerDestructionOn
            // 
            this.btnCellLoadBPickerDestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadBPickerDestructionOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnCellLoadBPickerDestructionOn.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadBPickerDestructionOn.Location = new System.Drawing.Point(3, 99);
            this.btnCellLoadBPickerDestructionOn.Name = "btnCellLoadBPickerDestructionOn";
            this.btnCellLoadBPickerDestructionOn.Size = new System.Drawing.Size(212, 43);
            this.btnCellLoadBPickerDestructionOn.TabIndex = 63;
            this.btnCellLoadBPickerDestructionOn.Text = "피커\r\n파기 온";
            this.btnCellLoadBPickerDestructionOn.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadBPickerDestructionOff
            // 
            this.btnCellLoadBPickerDestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadBPickerDestructionOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnCellLoadBPickerDestructionOff.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadBPickerDestructionOff.Location = new System.Drawing.Point(221, 99);
            this.btnCellLoadBPickerDestructionOff.Name = "btnCellLoadBPickerDestructionOff";
            this.btnCellLoadBPickerDestructionOff.Size = new System.Drawing.Size(212, 43);
            this.btnCellLoadBPickerDestructionOff.TabIndex = 62;
            this.btnCellLoadBPickerDestructionOff.Text = "피커\r\n파기 오프";
            this.btnCellLoadBPickerDestructionOff.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadBPickerUp
            // 
            this.btnCellLoadBPickerUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadBPickerUp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnCellLoadBPickerUp.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadBPickerUp.Location = new System.Drawing.Point(3, 3);
            this.btnCellLoadBPickerUp.Name = "btnCellLoadBPickerUp";
            this.btnCellLoadBPickerUp.Size = new System.Drawing.Size(212, 42);
            this.btnCellLoadBPickerUp.TabIndex = 58;
            this.btnCellLoadBPickerUp.Text = "피커\r\n업";
            this.btnCellLoadBPickerUp.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadBPickerDown
            // 
            this.btnCellLoadBPickerDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadBPickerDown.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnCellLoadBPickerDown.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadBPickerDown.Location = new System.Drawing.Point(221, 3);
            this.btnCellLoadBPickerDown.Name = "btnCellLoadBPickerDown";
            this.btnCellLoadBPickerDown.Size = new System.Drawing.Size(212, 42);
            this.btnCellLoadBPickerDown.TabIndex = 59;
            this.btnCellLoadBPickerDown.Text = "피커\r\n다운";
            this.btnCellLoadBPickerDown.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadBPickerPneumaticOn
            // 
            this.btnCellLoadBPickerPneumaticOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadBPickerPneumaticOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnCellLoadBPickerPneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadBPickerPneumaticOn.Location = new System.Drawing.Point(3, 51);
            this.btnCellLoadBPickerPneumaticOn.Name = "btnCellLoadBPickerPneumaticOn";
            this.btnCellLoadBPickerPneumaticOn.Size = new System.Drawing.Size(212, 42);
            this.btnCellLoadBPickerPneumaticOn.TabIndex = 60;
            this.btnCellLoadBPickerPneumaticOn.Text = "피커\r\n공압 온";
            this.btnCellLoadBPickerPneumaticOn.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadBPickerPneumaticOff
            // 
            this.btnCellLoadBPickerPneumaticOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadBPickerPneumaticOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnCellLoadBPickerPneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadBPickerPneumaticOff.Location = new System.Drawing.Point(221, 51);
            this.btnCellLoadBPickerPneumaticOff.Name = "btnCellLoadBPickerPneumaticOff";
            this.btnCellLoadBPickerPneumaticOff.Size = new System.Drawing.Size(212, 42);
            this.btnCellLoadBPickerPneumaticOff.TabIndex = 61;
            this.btnCellLoadBPickerPneumaticOff.Text = "피커\r\n공압 오프";
            this.btnCellLoadBPickerPneumaticOff.UseVisualStyleBackColor = false;
            // 
            // gxtCellLoad_A
            // 
            this.gxtCellLoad_A.Controls.Add(this.tableLayoutPanel24);
            this.gxtCellLoad_A.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCellLoad_A.ForeColor = System.Drawing.Color.White;
            this.gxtCellLoad_A.Location = new System.Drawing.Point(792, 410);
            this.gxtCellLoad_A.Name = "gxtCellLoad_A";
            this.gxtCellLoad_A.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gxtCellLoad_A.Size = new System.Drawing.Size(449, 179);
            this.gxtCellLoad_A.TabIndex = 51;
            this.gxtCellLoad_A.TabStop = false;
            this.gxtCellLoad_A.Text = "     A    ";
            // 
            // tableLayoutPanel24
            // 
            this.tableLayoutPanel24.ColumnCount = 2;
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel24.Controls.Add(this.btnCellLoadAPickerDestructionOn, 0, 2);
            this.tableLayoutPanel24.Controls.Add(this.btnCellLoadAPickerDestructionOff, 0, 2);
            this.tableLayoutPanel24.Controls.Add(this.btnCellLoadAPickerUp, 0, 0);
            this.tableLayoutPanel24.Controls.Add(this.btnCellLoadAPickerDown, 1, 0);
            this.tableLayoutPanel24.Controls.Add(this.btnCellLoadAPickerPneumaticOn, 0, 1);
            this.tableLayoutPanel24.Controls.Add(this.btnCellLoadAPickerPneumaticOff, 1, 1);
            this.tableLayoutPanel24.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel24.Name = "tableLayoutPanel24";
            this.tableLayoutPanel24.RowCount = 3;
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel24.Size = new System.Drawing.Size(437, 145);
            this.tableLayoutPanel24.TabIndex = 54;
            // 
            // btnCellLoadAPickerDestructionOn
            // 
            this.btnCellLoadAPickerDestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadAPickerDestructionOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnCellLoadAPickerDestructionOn.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadAPickerDestructionOn.Location = new System.Drawing.Point(3, 99);
            this.btnCellLoadAPickerDestructionOn.Name = "btnCellLoadAPickerDestructionOn";
            this.btnCellLoadAPickerDestructionOn.Size = new System.Drawing.Size(212, 43);
            this.btnCellLoadAPickerDestructionOn.TabIndex = 63;
            this.btnCellLoadAPickerDestructionOn.Text = "피커\r\n파기 온";
            this.btnCellLoadAPickerDestructionOn.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadAPickerDestructionOff
            // 
            this.btnCellLoadAPickerDestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadAPickerDestructionOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnCellLoadAPickerDestructionOff.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadAPickerDestructionOff.Location = new System.Drawing.Point(221, 99);
            this.btnCellLoadAPickerDestructionOff.Name = "btnCellLoadAPickerDestructionOff";
            this.btnCellLoadAPickerDestructionOff.Size = new System.Drawing.Size(212, 43);
            this.btnCellLoadAPickerDestructionOff.TabIndex = 62;
            this.btnCellLoadAPickerDestructionOff.Text = "피커\r\n파기 오프";
            this.btnCellLoadAPickerDestructionOff.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadAPickerUp
            // 
            this.btnCellLoadAPickerUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadAPickerUp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnCellLoadAPickerUp.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadAPickerUp.Location = new System.Drawing.Point(3, 3);
            this.btnCellLoadAPickerUp.Name = "btnCellLoadAPickerUp";
            this.btnCellLoadAPickerUp.Size = new System.Drawing.Size(212, 42);
            this.btnCellLoadAPickerUp.TabIndex = 58;
            this.btnCellLoadAPickerUp.Text = "피커\r\n업";
            this.btnCellLoadAPickerUp.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadAPickerDown
            // 
            this.btnCellLoadAPickerDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadAPickerDown.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnCellLoadAPickerDown.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadAPickerDown.Location = new System.Drawing.Point(221, 3);
            this.btnCellLoadAPickerDown.Name = "btnCellLoadAPickerDown";
            this.btnCellLoadAPickerDown.Size = new System.Drawing.Size(212, 42);
            this.btnCellLoadAPickerDown.TabIndex = 59;
            this.btnCellLoadAPickerDown.Text = "피커\r\n다운";
            this.btnCellLoadAPickerDown.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadAPickerPneumaticOn
            // 
            this.btnCellLoadAPickerPneumaticOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadAPickerPneumaticOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnCellLoadAPickerPneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadAPickerPneumaticOn.Location = new System.Drawing.Point(3, 51);
            this.btnCellLoadAPickerPneumaticOn.Name = "btnCellLoadAPickerPneumaticOn";
            this.btnCellLoadAPickerPneumaticOn.Size = new System.Drawing.Size(212, 42);
            this.btnCellLoadAPickerPneumaticOn.TabIndex = 60;
            this.btnCellLoadAPickerPneumaticOn.Text = "피커\r\n공압 온";
            this.btnCellLoadAPickerPneumaticOn.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadAPickerPneumaticOff
            // 
            this.btnCellLoadAPickerPneumaticOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadAPickerPneumaticOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnCellLoadAPickerPneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadAPickerPneumaticOff.Location = new System.Drawing.Point(221, 51);
            this.btnCellLoadAPickerPneumaticOff.Name = "btnCellLoadAPickerPneumaticOff";
            this.btnCellLoadAPickerPneumaticOff.Size = new System.Drawing.Size(212, 42);
            this.btnCellLoadAPickerPneumaticOff.TabIndex = 61;
            this.btnCellLoadAPickerPneumaticOff.Text = "피커\r\n공압 오프";
            this.btnCellLoadAPickerPneumaticOff.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadSave
            // 
            this.btnCellLoadSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadSave.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellLoadSave.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadSave.Location = new System.Drawing.Point(1482, 783);
            this.btnCellLoadSave.Name = "btnCellLoadSave";
            this.btnCellLoadSave.Size = new System.Drawing.Size(228, 28);
            this.btnCellLoadSave.TabIndex = 50;
            this.btnCellLoadSave.Text = "저장";
            this.btnCellLoadSave.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadSpeedSetting
            // 
            this.btnCellLoadSpeedSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadSpeedSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCellLoadSpeedSetting.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadSpeedSetting.Location = new System.Drawing.Point(1291, 315);
            this.btnCellLoadSpeedSetting.Name = "btnCellLoadSpeedSetting";
            this.btnCellLoadSpeedSetting.Size = new System.Drawing.Size(90, 27);
            this.btnCellLoadSpeedSetting.TabIndex = 48;
            this.btnCellLoadSpeedSetting.Text = "속도 설정";
            this.btnCellLoadSpeedSetting.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadAllSetting
            // 
            this.btnCellLoadAllSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadAllSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCellLoadAllSetting.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadAllSetting.Location = new System.Drawing.Point(1387, 273);
            this.btnCellLoadAllSetting.Name = "btnCellLoadAllSetting";
            this.btnCellLoadAllSetting.Size = new System.Drawing.Size(90, 27);
            this.btnCellLoadAllSetting.TabIndex = 8;
            this.btnCellLoadAllSetting.Text = "전체 설정";
            this.btnCellLoadAllSetting.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadLocationSetting
            // 
            this.btnCellLoadLocationSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadLocationSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCellLoadLocationSetting.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadLocationSetting.Location = new System.Drawing.Point(1071, 315);
            this.btnCellLoadLocationSetting.Name = "btnCellLoadLocationSetting";
            this.btnCellLoadLocationSetting.Size = new System.Drawing.Size(90, 27);
            this.btnCellLoadLocationSetting.TabIndex = 46;
            this.btnCellLoadLocationSetting.Text = "위치 설정";
            this.btnCellLoadLocationSetting.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadMoveLocation
            // 
            this.btnCellLoadMoveLocation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadMoveLocation.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCellLoadMoveLocation.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadMoveLocation.Location = new System.Drawing.Point(975, 315);
            this.btnCellLoadMoveLocation.Name = "btnCellLoadMoveLocation";
            this.btnCellLoadMoveLocation.Size = new System.Drawing.Size(90, 27);
            this.btnCellLoadMoveLocation.TabIndex = 45;
            this.btnCellLoadMoveLocation.Text = "위치 이동";
            this.btnCellLoadMoveLocation.UseVisualStyleBackColor = false;
            // 
            // txtCellLoadLocation
            // 
            this.txtCellLoadLocation.Font = new System.Drawing.Font("굴림", 13F);
            this.txtCellLoadLocation.Location = new System.Drawing.Point(879, 316);
            this.txtCellLoadLocation.Name = "txtCellLoadLocation";
            this.txtCellLoadLocation.Size = new System.Drawing.Size(90, 27);
            this.txtCellLoadLocation.TabIndex = 44;
            // 
            // txtCellLoadSpeed
            // 
            this.txtCellLoadSpeed.Font = new System.Drawing.Font("굴림", 13F);
            this.txtCellLoadSpeed.Location = new System.Drawing.Point(1291, 273);
            this.txtCellLoadSpeed.Name = "txtCellLoadSpeed";
            this.txtCellLoadSpeed.Size = new System.Drawing.Size(90, 27);
            this.txtCellLoadSpeed.TabIndex = 7;
            // 
            // txtCellLoadCurrentLocation
            // 
            this.txtCellLoadCurrentLocation.Font = new System.Drawing.Font("굴림", 13F);
            this.txtCellLoadCurrentLocation.Location = new System.Drawing.Point(1084, 274);
            this.txtCellLoadCurrentLocation.Name = "txtCellLoadCurrentLocation";
            this.txtCellLoadCurrentLocation.Size = new System.Drawing.Size(90, 27);
            this.txtCellLoadCurrentLocation.TabIndex = 6;
            // 
            // txtCellLoadSelectedShaft
            // 
            this.txtCellLoadSelectedShaft.Font = new System.Drawing.Font("굴림", 13F);
            this.txtCellLoadSelectedShaft.Location = new System.Drawing.Point(879, 273);
            this.txtCellLoadSelectedShaft.Name = "txtCellLoadSelectedShaft";
            this.txtCellLoadSelectedShaft.Size = new System.Drawing.Size(90, 27);
            this.txtCellLoadSelectedShaft.TabIndex = 5;
            // 
            // lbl_CellLoad_Location
            // 
            this.lbl_CellLoad_Location.AutoSize = true;
            this.lbl_CellLoad_Location.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CellLoad_Location.ForeColor = System.Drawing.Color.White;
            this.lbl_CellLoad_Location.Location = new System.Drawing.Point(801, 316);
            this.lbl_CellLoad_Location.Name = "lbl_CellLoad_Location";
            this.lbl_CellLoad_Location.Size = new System.Drawing.Size(84, 21);
            this.lbl_CellLoad_Location.TabIndex = 40;
            this.lbl_CellLoad_Location.Text = "위치[mm]";
            // 
            // lbl_CellLoad_Speed
            // 
            this.lbl_CellLoad_Speed.AutoSize = true;
            this.lbl_CellLoad_Speed.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CellLoad_Speed.ForeColor = System.Drawing.Color.White;
            this.lbl_CellLoad_Speed.Location = new System.Drawing.Point(1179, 275);
            this.lbl_CellLoad_Speed.Name = "lbl_CellLoad_Speed";
            this.lbl_CellLoad_Speed.Size = new System.Drawing.Size(115, 21);
            this.lbl_CellLoad_Speed.TabIndex = 7;
            this.lbl_CellLoad_Speed.Text = "속도[mm/sec]";
            // 
            // lbl_CellLoad_SelectedShaft
            // 
            this.lbl_CellLoad_SelectedShaft.AutoSize = true;
            this.lbl_CellLoad_SelectedShaft.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CellLoad_SelectedShaft.ForeColor = System.Drawing.Color.White;
            this.lbl_CellLoad_SelectedShaft.Location = new System.Drawing.Point(805, 274);
            this.lbl_CellLoad_SelectedShaft.Name = "lbl_CellLoad_SelectedShaft";
            this.lbl_CellLoad_SelectedShaft.Size = new System.Drawing.Size(80, 21);
            this.lbl_CellLoad_SelectedShaft.TabIndex = 5;
            this.lbl_CellLoad_SelectedShaft.Text = "선택된 축";
            // 
            // txtCellLoad_LocationSetting
            // 
            this.txtCellLoad_LocationSetting.BackColor = System.Drawing.SystemColors.Highlight;
            this.txtCellLoad_LocationSetting.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.txtCellLoad_LocationSetting.ForeColor = System.Drawing.Color.White;
            this.txtCellLoad_LocationSetting.Location = new System.Drawing.Point(792, 223);
            this.txtCellLoad_LocationSetting.Name = "txtCellLoad_LocationSetting";
            this.txtCellLoad_LocationSetting.ReadOnly = true;
            this.txtCellLoad_LocationSetting.Size = new System.Drawing.Size(89, 27);
            this.txtCellLoad_LocationSetting.TabIndex = 5;
            this.txtCellLoad_LocationSetting.TabStop = false;
            this.txtCellLoad_LocationSetting.Text = "위치값 설정";
            this.txtCellLoad_LocationSetting.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbl_CellLoad_CurrentLocation
            // 
            this.lbl_CellLoad_CurrentLocation.AutoSize = true;
            this.lbl_CellLoad_CurrentLocation.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CellLoad_CurrentLocation.ForeColor = System.Drawing.Color.White;
            this.lbl_CellLoad_CurrentLocation.Location = new System.Drawing.Point(975, 274);
            this.lbl_CellLoad_CurrentLocation.Name = "lbl_CellLoad_CurrentLocation";
            this.lbl_CellLoad_CurrentLocation.Size = new System.Drawing.Size(116, 21);
            this.lbl_CellLoad_CurrentLocation.TabIndex = 6;
            this.lbl_CellLoad_CurrentLocation.Text = "현재위치[mm]";
            // 
            // btnCellLoadGrabSwitch3
            // 
            this.btnCellLoadGrabSwitch3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadGrabSwitch3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellLoadGrabSwitch3.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadGrabSwitch3.Location = new System.Drawing.Point(1514, 6);
            this.btnCellLoadGrabSwitch3.Name = "btnCellLoadGrabSwitch3";
            this.btnCellLoadGrabSwitch3.Size = new System.Drawing.Size(172, 23);
            this.btnCellLoadGrabSwitch3.TabIndex = 3;
            this.btnCellLoadGrabSwitch3.Text = "Grab Switch 3";
            this.btnCellLoadGrabSwitch3.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadGrabSwitch2
            // 
            this.btnCellLoadGrabSwitch2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadGrabSwitch2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellLoadGrabSwitch2.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadGrabSwitch2.Location = new System.Drawing.Point(1336, 6);
            this.btnCellLoadGrabSwitch2.Name = "btnCellLoadGrabSwitch2";
            this.btnCellLoadGrabSwitch2.Size = new System.Drawing.Size(172, 23);
            this.btnCellLoadGrabSwitch2.TabIndex = 2;
            this.btnCellLoadGrabSwitch2.Text = "Grab Switch 2";
            this.btnCellLoadGrabSwitch2.UseVisualStyleBackColor = false;
            // 
            // btnCellLoadGrabSwitch1
            // 
            this.btnCellLoadGrabSwitch1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellLoadGrabSwitch1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellLoadGrabSwitch1.ForeColor = System.Drawing.Color.White;
            this.btnCellLoadGrabSwitch1.Location = new System.Drawing.Point(1158, 6);
            this.btnCellLoadGrabSwitch1.Name = "btnCellLoadGrabSwitch1";
            this.btnCellLoadGrabSwitch1.Size = new System.Drawing.Size(172, 23);
            this.btnCellLoadGrabSwitch1.TabIndex = 1;
            this.btnCellLoadGrabSwitch1.Text = "Grab Switch 1";
            this.btnCellLoadGrabSwitch1.UseVisualStyleBackColor = false;
            // 
            // lvCellLoad
            // 
            this.lvCellLoad.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.col_CellLoad_NamebyLocation,
            this.col_CellLoad_Shaft,
            this.col_CellLoad_Location,
            this.col_CellLoad_Speed});
            this.lvCellLoad.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lvCellLoad.GridLines = true;
            this.lvCellLoad.Location = new System.Drawing.Point(17, 16);
            this.lvCellLoad.Name = "lvCellLoad";
            this.lvCellLoad.Size = new System.Drawing.Size(758, 734);
            this.lvCellLoad.TabIndex = 0;
            this.lvCellLoad.TabStop = false;
            this.lvCellLoad.UseCompatibleStateImageBehavior = false;
            this.lvCellLoad.View = System.Windows.Forms.View.Details;
            // 
            // col_CellLoad_NamebyLocation
            // 
            this.col_CellLoad_NamebyLocation.Text = "위치별 명칭";
            this.col_CellLoad_NamebyLocation.Width = 430;
            // 
            // col_CellLoad_Shaft
            // 
            this.col_CellLoad_Shaft.Text = "축";
            this.col_CellLoad_Shaft.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // col_CellLoad_Location
            // 
            this.col_CellLoad_Location.Text = "위치[mm]";
            this.col_CellLoad_Location.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_CellLoad_Location.Width = 125;
            // 
            // col_CellLoad_Speed
            // 
            this.col_CellLoad_Speed.Text = "속도[mm/s]";
            this.col_CellLoad_Speed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_CellLoad_Speed.Width = 125;
            // 
            // ajin_Setting_CellLoad
            // 
            this.ajin_Setting_CellLoad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CellLoad.Location = new System.Drawing.Point(792, 34);
            this.ajin_Setting_CellLoad.Name = "ajin_Setting_CellLoad";
            this.ajin_Setting_CellLoad.Servo = null;
            this.ajin_Setting_CellLoad.Size = new System.Drawing.Size(903, 183);
            this.ajin_Setting_CellLoad.TabIndex = 4;
            // 
            // tp_IRCutProcess
            // 
            this.tp_IRCutProcess.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tp_IRCutProcess.Controls.Add(this.gxtIRCutProcess_Ionizer);
            this.tp_IRCutProcess.Controls.Add(this.gxtIRCutProcess_Move);
            this.tp_IRCutProcess.Controls.Add(this.gxtIRCutProcess_B);
            this.tp_IRCutProcess.Controls.Add(this.gxtIRCutProcess_A);
            this.tp_IRCutProcess.Controls.Add(this.btnIRCutProcessSave);
            this.tp_IRCutProcess.Controls.Add(this.btnIRCutProcessSpeedSetting);
            this.tp_IRCutProcess.Controls.Add(this.btnIRCutProcessAllSetting);
            this.tp_IRCutProcess.Controls.Add(this.btnIRCutProcessLocationSetting);
            this.tp_IRCutProcess.Controls.Add(this.btnIRCutProcessMoveLocation);
            this.tp_IRCutProcess.Controls.Add(this.txtIRCutProcessLocation);
            this.tp_IRCutProcess.Controls.Add(this.txtIRCutProcessSpeed);
            this.tp_IRCutProcess.Controls.Add(this.txtIRCutProcessCurrentLocation);
            this.tp_IRCutProcess.Controls.Add(this.txtIRCutProcessSelectedShaft);
            this.tp_IRCutProcess.Controls.Add(this.lbl_IRCutProcess_Location);
            this.tp_IRCutProcess.Controls.Add(this.lbl_IRCutProcess_Speed);
            this.tp_IRCutProcess.Controls.Add(this.lbl_IRCutProcess_SelectedShaft);
            this.tp_IRCutProcess.Controls.Add(this.txtIRCutProcess_LocationSetting);
            this.tp_IRCutProcess.Controls.Add(this.lbl_IRCutProcess_CurrentLocation);
            this.tp_IRCutProcess.Controls.Add(this.btnIRCutProcessGrabSwitch3);
            this.tp_IRCutProcess.Controls.Add(this.btnIRCutProcessGrabSwitch2);
            this.tp_IRCutProcess.Controls.Add(this.btnIRCutProcessGrabSwitch1);
            this.tp_IRCutProcess.Controls.Add(this.lvIRCutProcess);
            this.tp_IRCutProcess.Controls.Add(this.ajin_Setting_IRCutProcess);
            this.tp_IRCutProcess.Location = new System.Drawing.Point(4, 34);
            this.tp_IRCutProcess.Name = "tp_IRCutProcess";
            this.tp_IRCutProcess.Size = new System.Drawing.Size(1726, 816);
            this.tp_IRCutProcess.TabIndex = 3;
            this.tp_IRCutProcess.Text = "IR Cut 프로세스";
            // 
            // gxtIRCutProcess_Ionizer
            // 
            this.gxtIRCutProcess_Ionizer.Controls.Add(this.btnIRCutProcessDestruction);
            this.gxtIRCutProcess_Ionizer.Controls.Add(this.btnIRCutProcessIonizerOff);
            this.gxtIRCutProcess_Ionizer.Controls.Add(this.btnIRCutProcessDestructionOn);
            this.gxtIRCutProcess_Ionizer.Controls.Add(this.btnIRCutProcessIonizerOn);
            this.gxtIRCutProcess_Ionizer.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtIRCutProcess_Ionizer.ForeColor = System.Drawing.Color.White;
            this.gxtIRCutProcess_Ionizer.Location = new System.Drawing.Point(1484, 223);
            this.gxtIRCutProcess_Ionizer.Name = "gxtIRCutProcess_Ionizer";
            this.gxtIRCutProcess_Ionizer.Size = new System.Drawing.Size(212, 181);
            this.gxtIRCutProcess_Ionizer.TabIndex = 50;
            this.gxtIRCutProcess_Ionizer.TabStop = false;
            this.gxtIRCutProcess_Ionizer.Text = "     이오나이저     ";
            // 
            // btnIRCutProcessDestruction
            // 
            this.btnIRCutProcessDestruction.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessDestruction.Location = new System.Drawing.Point(107, 103);
            this.btnIRCutProcessDestruction.Name = "btnIRCutProcessDestruction";
            this.btnIRCutProcessDestruction.Size = new System.Drawing.Size(99, 72);
            this.btnIRCutProcessDestruction.TabIndex = 6;
            this.btnIRCutProcessDestruction.Text = "파기 오프";
            this.btnIRCutProcessDestruction.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessIonizerOff
            // 
            this.btnIRCutProcessIonizerOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessIonizerOff.Location = new System.Drawing.Point(107, 28);
            this.btnIRCutProcessIonizerOff.Name = "btnIRCutProcessIonizerOff";
            this.btnIRCutProcessIonizerOff.Size = new System.Drawing.Size(99, 72);
            this.btnIRCutProcessIonizerOff.TabIndex = 5;
            this.btnIRCutProcessIonizerOff.Text = "이오나이저\r\n오프";
            this.btnIRCutProcessIonizerOff.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessDestructionOn
            // 
            this.btnIRCutProcessDestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessDestructionOn.Location = new System.Drawing.Point(6, 103);
            this.btnIRCutProcessDestructionOn.Name = "btnIRCutProcessDestructionOn";
            this.btnIRCutProcessDestructionOn.Size = new System.Drawing.Size(99, 72);
            this.btnIRCutProcessDestructionOn.TabIndex = 4;
            this.btnIRCutProcessDestructionOn.Text = "파기 온";
            this.btnIRCutProcessDestructionOn.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessIonizerOn
            // 
            this.btnIRCutProcessIonizerOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessIonizerOn.Location = new System.Drawing.Point(6, 28);
            this.btnIRCutProcessIonizerOn.Name = "btnIRCutProcessIonizerOn";
            this.btnIRCutProcessIonizerOn.Size = new System.Drawing.Size(99, 72);
            this.btnIRCutProcessIonizerOn.TabIndex = 3;
            this.btnIRCutProcessIonizerOn.Text = "이오나이저\r\n온";
            this.btnIRCutProcessIonizerOn.UseVisualStyleBackColor = false;
            // 
            // gxtIRCutProcess_Move
            // 
            this.gxtIRCutProcess_Move.Controls.Add(this.gxtIRCutProcess_RightOffset);
            this.gxtIRCutProcess_Move.Controls.Add(this.gxtIRCutProcess_LeftOffset);
            this.gxtIRCutProcess_Move.Controls.Add(this.gxtIRCutProcess_MoveB);
            this.gxtIRCutProcess_Move.Controls.Add(this.gxtIRCutProcess_MoveA);
            this.gxtIRCutProcess_Move.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtIRCutProcess_Move.ForeColor = System.Drawing.Color.White;
            this.gxtIRCutProcess_Move.Location = new System.Drawing.Point(792, 595);
            this.gxtIRCutProcess_Move.Name = "gxtIRCutProcess_Move";
            this.gxtIRCutProcess_Move.Size = new System.Drawing.Size(904, 179);
            this.gxtIRCutProcess_Move.TabIndex = 53;
            this.gxtIRCutProcess_Move.TabStop = false;
            this.gxtIRCutProcess_Move.Text = "     이동     ";
            // 
            // gxtIRCutProcess_RightOffset
            // 
            this.gxtIRCutProcess_RightOffset.Controls.Add(this.tableLayoutPanel40);
            this.gxtIRCutProcess_RightOffset.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtIRCutProcess_RightOffset.ForeColor = System.Drawing.Color.White;
            this.gxtIRCutProcess_RightOffset.Location = new System.Drawing.Point(678, 19);
            this.gxtIRCutProcess_RightOffset.Name = "gxtIRCutProcess_RightOffset";
            this.gxtIRCutProcess_RightOffset.Size = new System.Drawing.Size(212, 154);
            this.gxtIRCutProcess_RightOffset.TabIndex = 32;
            this.gxtIRCutProcess_RightOffset.TabStop = false;
            this.gxtIRCutProcess_RightOffset.Text = "     우측 오프셋    ";
            // 
            // tableLayoutPanel40
            // 
            this.tableLayoutPanel40.ColumnCount = 2;
            this.tableLayoutPanel40.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel40.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel40.Controls.Add(this.btnIRCutProcessRightOffsetRightVision2Focus, 0, 1);
            this.tableLayoutPanel40.Controls.Add(this.btnIRCutProcessRightOffsetRightFocus2Vision, 0, 1);
            this.tableLayoutPanel40.Controls.Add(this.btnIRCutProcessRightOffsetLeftVision2Focus, 0, 0);
            this.tableLayoutPanel40.Controls.Add(this.btnIRCutProcessRightOffsetLeftFocus2Vision, 1, 0);
            this.tableLayoutPanel40.Location = new System.Drawing.Point(6, 25);
            this.tableLayoutPanel40.Name = "tableLayoutPanel40";
            this.tableLayoutPanel40.RowCount = 2;
            this.tableLayoutPanel40.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel40.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel40.Size = new System.Drawing.Size(202, 118);
            this.tableLayoutPanel40.TabIndex = 34;
            // 
            // btnIRCutProcessRightOffsetRightVision2Focus
            // 
            this.btnIRCutProcessRightOffsetRightVision2Focus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessRightOffsetRightVision2Focus.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessRightOffsetRightVision2Focus.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessRightOffsetRightVision2Focus.Location = new System.Drawing.Point(3, 62);
            this.btnIRCutProcessRightOffsetRightVision2Focus.Name = "btnIRCutProcessRightOffsetRightVision2Focus";
            this.btnIRCutProcessRightOffsetRightVision2Focus.Size = new System.Drawing.Size(95, 53);
            this.btnIRCutProcessRightOffsetRightVision2Focus.TabIndex = 66;
            this.btnIRCutProcessRightOffsetRightVision2Focus.Text = "우측\r\n비젼 ▶ 포커스";
            this.btnIRCutProcessRightOffsetRightVision2Focus.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessRightOffsetRightFocus2Vision
            // 
            this.btnIRCutProcessRightOffsetRightFocus2Vision.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessRightOffsetRightFocus2Vision.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessRightOffsetRightFocus2Vision.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessRightOffsetRightFocus2Vision.Location = new System.Drawing.Point(104, 62);
            this.btnIRCutProcessRightOffsetRightFocus2Vision.Name = "btnIRCutProcessRightOffsetRightFocus2Vision";
            this.btnIRCutProcessRightOffsetRightFocus2Vision.Size = new System.Drawing.Size(95, 53);
            this.btnIRCutProcessRightOffsetRightFocus2Vision.TabIndex = 65;
            this.btnIRCutProcessRightOffsetRightFocus2Vision.Text = "우측\r\n포커스 ▶ 비젼";
            this.btnIRCutProcessRightOffsetRightFocus2Vision.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessRightOffsetLeftVision2Focus
            // 
            this.btnIRCutProcessRightOffsetLeftVision2Focus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessRightOffsetLeftVision2Focus.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessRightOffsetLeftVision2Focus.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessRightOffsetLeftVision2Focus.Location = new System.Drawing.Point(3, 3);
            this.btnIRCutProcessRightOffsetLeftVision2Focus.Name = "btnIRCutProcessRightOffsetLeftVision2Focus";
            this.btnIRCutProcessRightOffsetLeftVision2Focus.Size = new System.Drawing.Size(95, 53);
            this.btnIRCutProcessRightOffsetLeftVision2Focus.TabIndex = 63;
            this.btnIRCutProcessRightOffsetLeftVision2Focus.Text = "좌측\r\n비젼 ▶ 포커스";
            this.btnIRCutProcessRightOffsetLeftVision2Focus.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessRightOffsetLeftFocus2Vision
            // 
            this.btnIRCutProcessRightOffsetLeftFocus2Vision.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessRightOffsetLeftFocus2Vision.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessRightOffsetLeftFocus2Vision.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessRightOffsetLeftFocus2Vision.Location = new System.Drawing.Point(104, 3);
            this.btnIRCutProcessRightOffsetLeftFocus2Vision.Name = "btnIRCutProcessRightOffsetLeftFocus2Vision";
            this.btnIRCutProcessRightOffsetLeftFocus2Vision.Size = new System.Drawing.Size(95, 53);
            this.btnIRCutProcessRightOffsetLeftFocus2Vision.TabIndex = 64;
            this.btnIRCutProcessRightOffsetLeftFocus2Vision.Text = "좌측\r\n포커스 ▶ 비젼";
            this.btnIRCutProcessRightOffsetLeftFocus2Vision.UseVisualStyleBackColor = false;
            // 
            // gxtIRCutProcess_LeftOffset
            // 
            this.gxtIRCutProcess_LeftOffset.Controls.Add(this.tableLayoutPanel39);
            this.gxtIRCutProcess_LeftOffset.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtIRCutProcess_LeftOffset.ForeColor = System.Drawing.Color.White;
            this.gxtIRCutProcess_LeftOffset.Location = new System.Drawing.Point(457, 19);
            this.gxtIRCutProcess_LeftOffset.Name = "gxtIRCutProcess_LeftOffset";
            this.gxtIRCutProcess_LeftOffset.Size = new System.Drawing.Size(212, 154);
            this.gxtIRCutProcess_LeftOffset.TabIndex = 32;
            this.gxtIRCutProcess_LeftOffset.TabStop = false;
            this.gxtIRCutProcess_LeftOffset.Text = "     좌측 오프셋    ";
            // 
            // tableLayoutPanel39
            // 
            this.tableLayoutPanel39.ColumnCount = 2;
            this.tableLayoutPanel39.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel39.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel39.Controls.Add(this.btnIRCutProcessLeftOffsetRightVisoin2Focus, 0, 1);
            this.tableLayoutPanel39.Controls.Add(this.btnIRCutProcessLeftOffsetRightFocus2Visoin, 0, 1);
            this.tableLayoutPanel39.Controls.Add(this.btnIRCutProcessLeftOffsetLeftVision2Focus, 0, 0);
            this.tableLayoutPanel39.Controls.Add(this.btnIRCutProcessLeftOffsetLeftFocus2Vision, 1, 0);
            this.tableLayoutPanel39.Location = new System.Drawing.Point(4, 25);
            this.tableLayoutPanel39.Name = "tableLayoutPanel39";
            this.tableLayoutPanel39.RowCount = 2;
            this.tableLayoutPanel39.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel39.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel39.Size = new System.Drawing.Size(202, 118);
            this.tableLayoutPanel39.TabIndex = 33;
            // 
            // btnIRCutProcessLeftOffsetRightVisoin2Focus
            // 
            this.btnIRCutProcessLeftOffsetRightVisoin2Focus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessLeftOffsetRightVisoin2Focus.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessLeftOffsetRightVisoin2Focus.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessLeftOffsetRightVisoin2Focus.Location = new System.Drawing.Point(3, 62);
            this.btnIRCutProcessLeftOffsetRightVisoin2Focus.Name = "btnIRCutProcessLeftOffsetRightVisoin2Focus";
            this.btnIRCutProcessLeftOffsetRightVisoin2Focus.Size = new System.Drawing.Size(95, 53);
            this.btnIRCutProcessLeftOffsetRightVisoin2Focus.TabIndex = 66;
            this.btnIRCutProcessLeftOffsetRightVisoin2Focus.Text = "우측\r\n비젼 ▶ 포커스";
            this.btnIRCutProcessLeftOffsetRightVisoin2Focus.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessLeftOffsetRightFocus2Visoin
            // 
            this.btnIRCutProcessLeftOffsetRightFocus2Visoin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessLeftOffsetRightFocus2Visoin.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessLeftOffsetRightFocus2Visoin.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessLeftOffsetRightFocus2Visoin.Location = new System.Drawing.Point(104, 62);
            this.btnIRCutProcessLeftOffsetRightFocus2Visoin.Name = "btnIRCutProcessLeftOffsetRightFocus2Visoin";
            this.btnIRCutProcessLeftOffsetRightFocus2Visoin.Size = new System.Drawing.Size(95, 53);
            this.btnIRCutProcessLeftOffsetRightFocus2Visoin.TabIndex = 65;
            this.btnIRCutProcessLeftOffsetRightFocus2Visoin.Text = "우측\r\n포커스 ▶ 비젼";
            this.btnIRCutProcessLeftOffsetRightFocus2Visoin.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessLeftOffsetLeftVision2Focus
            // 
            this.btnIRCutProcessLeftOffsetLeftVision2Focus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessLeftOffsetLeftVision2Focus.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessLeftOffsetLeftVision2Focus.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessLeftOffsetLeftVision2Focus.Location = new System.Drawing.Point(3, 3);
            this.btnIRCutProcessLeftOffsetLeftVision2Focus.Name = "btnIRCutProcessLeftOffsetLeftVision2Focus";
            this.btnIRCutProcessLeftOffsetLeftVision2Focus.Size = new System.Drawing.Size(95, 53);
            this.btnIRCutProcessLeftOffsetLeftVision2Focus.TabIndex = 63;
            this.btnIRCutProcessLeftOffsetLeftVision2Focus.Text = "좌측\r\n비젼 ▶ 포커스";
            this.btnIRCutProcessLeftOffsetLeftVision2Focus.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessLeftOffsetLeftFocus2Vision
            // 
            this.btnIRCutProcessLeftOffsetLeftFocus2Vision.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessLeftOffsetLeftFocus2Vision.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessLeftOffsetLeftFocus2Vision.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessLeftOffsetLeftFocus2Vision.Location = new System.Drawing.Point(104, 3);
            this.btnIRCutProcessLeftOffsetLeftFocus2Vision.Name = "btnIRCutProcessLeftOffsetLeftFocus2Vision";
            this.btnIRCutProcessLeftOffsetLeftFocus2Vision.Size = new System.Drawing.Size(95, 53);
            this.btnIRCutProcessLeftOffsetLeftFocus2Vision.TabIndex = 64;
            this.btnIRCutProcessLeftOffsetLeftFocus2Vision.Text = "좌측\r\n포커스 ▶ 비젼";
            this.btnIRCutProcessLeftOffsetLeftFocus2Vision.UseVisualStyleBackColor = false;
            // 
            // gxtIRCutProcess_MoveB
            // 
            this.gxtIRCutProcess_MoveB.Controls.Add(this.tableLayoutPanel38);
            this.gxtIRCutProcess_MoveB.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtIRCutProcess_MoveB.ForeColor = System.Drawing.Color.White;
            this.gxtIRCutProcess_MoveB.Location = new System.Drawing.Point(237, 19);
            this.gxtIRCutProcess_MoveB.Name = "gxtIRCutProcess_MoveB";
            this.gxtIRCutProcess_MoveB.Size = new System.Drawing.Size(212, 154);
            this.gxtIRCutProcess_MoveB.TabIndex = 31;
            this.gxtIRCutProcess_MoveB.TabStop = false;
            this.gxtIRCutProcess_MoveB.Text = "     B    ";
            // 
            // tableLayoutPanel38
            // 
            this.tableLayoutPanel38.ColumnCount = 2;
            this.tableLayoutPanel38.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel38.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel38.Controls.Add(this.btnIRCutProcessMoveBR1Camera, 0, 2);
            this.tableLayoutPanel38.Controls.Add(this.btnIRCutProcessMoveBR2Camera, 0, 2);
            this.tableLayoutPanel38.Controls.Add(this.btnIRCutProcessMoveBRightCellLoad, 0, 0);
            this.tableLayoutPanel38.Controls.Add(this.btnIRCutProcessMoveBRightCellUnload, 1, 0);
            this.tableLayoutPanel38.Controls.Add(this.btnIRCutProcessMoveBR1Laser, 0, 1);
            this.tableLayoutPanel38.Controls.Add(this.btnIRCutProcessMoveBR2Laser, 1, 1);
            this.tableLayoutPanel38.Location = new System.Drawing.Point(4, 25);
            this.tableLayoutPanel38.Name = "tableLayoutPanel38";
            this.tableLayoutPanel38.RowCount = 3;
            this.tableLayoutPanel38.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel38.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel38.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel38.Size = new System.Drawing.Size(206, 123);
            this.tableLayoutPanel38.TabIndex = 55;
            // 
            // btnIRCutProcessMoveBR1Camera
            // 
            this.btnIRCutProcessMoveBR1Camera.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessMoveBR1Camera.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessMoveBR1Camera.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessMoveBR1Camera.Location = new System.Drawing.Point(3, 85);
            this.btnIRCutProcessMoveBR1Camera.Name = "btnIRCutProcessMoveBR1Camera";
            this.btnIRCutProcessMoveBR1Camera.Size = new System.Drawing.Size(97, 35);
            this.btnIRCutProcessMoveBR1Camera.TabIndex = 65;
            this.btnIRCutProcessMoveBR1Camera.Text = "우측1\r\n카메라 확인";
            this.btnIRCutProcessMoveBR1Camera.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessMoveBR2Camera
            // 
            this.btnIRCutProcessMoveBR2Camera.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessMoveBR2Camera.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessMoveBR2Camera.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessMoveBR2Camera.Location = new System.Drawing.Point(106, 85);
            this.btnIRCutProcessMoveBR2Camera.Name = "btnIRCutProcessMoveBR2Camera";
            this.btnIRCutProcessMoveBR2Camera.Size = new System.Drawing.Size(97, 35);
            this.btnIRCutProcessMoveBR2Camera.TabIndex = 64;
            this.btnIRCutProcessMoveBR2Camera.Text = "우측2\r\n카메라 확인";
            this.btnIRCutProcessMoveBR2Camera.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessMoveBRightCellLoad
            // 
            this.btnIRCutProcessMoveBRightCellLoad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessMoveBRightCellLoad.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessMoveBRightCellLoad.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessMoveBRightCellLoad.Location = new System.Drawing.Point(3, 3);
            this.btnIRCutProcessMoveBRightCellLoad.Name = "btnIRCutProcessMoveBRightCellLoad";
            this.btnIRCutProcessMoveBRightCellLoad.Size = new System.Drawing.Size(97, 34);
            this.btnIRCutProcessMoveBRightCellLoad.TabIndex = 60;
            this.btnIRCutProcessMoveBRightCellLoad.Text = "우측 셀 로드";
            this.btnIRCutProcessMoveBRightCellLoad.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessMoveBRightCellUnload
            // 
            this.btnIRCutProcessMoveBRightCellUnload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessMoveBRightCellUnload.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessMoveBRightCellUnload.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessMoveBRightCellUnload.Location = new System.Drawing.Point(106, 3);
            this.btnIRCutProcessMoveBRightCellUnload.Name = "btnIRCutProcessMoveBRightCellUnload";
            this.btnIRCutProcessMoveBRightCellUnload.Size = new System.Drawing.Size(97, 34);
            this.btnIRCutProcessMoveBRightCellUnload.TabIndex = 62;
            this.btnIRCutProcessMoveBRightCellUnload.Text = "우측 셀 언로드";
            this.btnIRCutProcessMoveBRightCellUnload.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessMoveBR1Laser
            // 
            this.btnIRCutProcessMoveBR1Laser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessMoveBR1Laser.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessMoveBR1Laser.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessMoveBR1Laser.Location = new System.Drawing.Point(3, 44);
            this.btnIRCutProcessMoveBR1Laser.Name = "btnIRCutProcessMoveBR1Laser";
            this.btnIRCutProcessMoveBR1Laser.Size = new System.Drawing.Size(97, 34);
            this.btnIRCutProcessMoveBR1Laser.TabIndex = 61;
            this.btnIRCutProcessMoveBR1Laser.Text = "우측1 레이저샷";
            this.btnIRCutProcessMoveBR1Laser.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessMoveBR2Laser
            // 
            this.btnIRCutProcessMoveBR2Laser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessMoveBR2Laser.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessMoveBR2Laser.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessMoveBR2Laser.Location = new System.Drawing.Point(106, 44);
            this.btnIRCutProcessMoveBR2Laser.Name = "btnIRCutProcessMoveBR2Laser";
            this.btnIRCutProcessMoveBR2Laser.Size = new System.Drawing.Size(97, 34);
            this.btnIRCutProcessMoveBR2Laser.TabIndex = 63;
            this.btnIRCutProcessMoveBR2Laser.Text = "우측2 레이저샷";
            this.btnIRCutProcessMoveBR2Laser.UseVisualStyleBackColor = false;
            // 
            // gxtIRCutProcess_MoveA
            // 
            this.gxtIRCutProcess_MoveA.Controls.Add(this.tableLayoutPanel37);
            this.gxtIRCutProcess_MoveA.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtIRCutProcess_MoveA.ForeColor = System.Drawing.Color.White;
            this.gxtIRCutProcess_MoveA.Location = new System.Drawing.Point(14, 19);
            this.gxtIRCutProcess_MoveA.Name = "gxtIRCutProcess_MoveA";
            this.gxtIRCutProcess_MoveA.Size = new System.Drawing.Size(215, 154);
            this.gxtIRCutProcess_MoveA.TabIndex = 30;
            this.gxtIRCutProcess_MoveA.TabStop = false;
            this.gxtIRCutProcess_MoveA.Text = "     A    ";
            // 
            // tableLayoutPanel37
            // 
            this.tableLayoutPanel37.ColumnCount = 2;
            this.tableLayoutPanel37.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel37.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel37.Controls.Add(this.btnIRCutProcessMoveAL1Camera, 0, 2);
            this.tableLayoutPanel37.Controls.Add(this.btnIRCutProcessMoveAL2Camera, 0, 2);
            this.tableLayoutPanel37.Controls.Add(this.btnIRCutProcessMoveALeftCellLoad, 0, 0);
            this.tableLayoutPanel37.Controls.Add(this.btnIRCutProcessMoveALeftCellUnload, 1, 0);
            this.tableLayoutPanel37.Controls.Add(this.btnIRCutProcessMoveAL1Laser, 0, 1);
            this.tableLayoutPanel37.Controls.Add(this.btnIRCutProcessMoveAL2Laser, 1, 1);
            this.tableLayoutPanel37.Location = new System.Drawing.Point(3, 25);
            this.tableLayoutPanel37.Name = "tableLayoutPanel37";
            this.tableLayoutPanel37.RowCount = 3;
            this.tableLayoutPanel37.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel37.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel37.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel37.Size = new System.Drawing.Size(206, 123);
            this.tableLayoutPanel37.TabIndex = 54;
            // 
            // btnIRCutProcessMoveAL1Camera
            // 
            this.btnIRCutProcessMoveAL1Camera.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessMoveAL1Camera.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessMoveAL1Camera.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessMoveAL1Camera.Location = new System.Drawing.Point(3, 85);
            this.btnIRCutProcessMoveAL1Camera.Name = "btnIRCutProcessMoveAL1Camera";
            this.btnIRCutProcessMoveAL1Camera.Size = new System.Drawing.Size(97, 35);
            this.btnIRCutProcessMoveAL1Camera.TabIndex = 65;
            this.btnIRCutProcessMoveAL1Camera.Text = "좌측1\r\n카메라 확인";
            this.btnIRCutProcessMoveAL1Camera.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessMoveAL2Camera
            // 
            this.btnIRCutProcessMoveAL2Camera.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessMoveAL2Camera.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessMoveAL2Camera.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessMoveAL2Camera.Location = new System.Drawing.Point(106, 85);
            this.btnIRCutProcessMoveAL2Camera.Name = "btnIRCutProcessMoveAL2Camera";
            this.btnIRCutProcessMoveAL2Camera.Size = new System.Drawing.Size(97, 35);
            this.btnIRCutProcessMoveAL2Camera.TabIndex = 64;
            this.btnIRCutProcessMoveAL2Camera.Text = "좌측2\r\n카메라 확인";
            this.btnIRCutProcessMoveAL2Camera.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessMoveALeftCellLoad
            // 
            this.btnIRCutProcessMoveALeftCellLoad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessMoveALeftCellLoad.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessMoveALeftCellLoad.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessMoveALeftCellLoad.Location = new System.Drawing.Point(3, 3);
            this.btnIRCutProcessMoveALeftCellLoad.Name = "btnIRCutProcessMoveALeftCellLoad";
            this.btnIRCutProcessMoveALeftCellLoad.Size = new System.Drawing.Size(97, 34);
            this.btnIRCutProcessMoveALeftCellLoad.TabIndex = 60;
            this.btnIRCutProcessMoveALeftCellLoad.Text = "좌측 셀 로드";
            this.btnIRCutProcessMoveALeftCellLoad.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessMoveALeftCellUnload
            // 
            this.btnIRCutProcessMoveALeftCellUnload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessMoveALeftCellUnload.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessMoveALeftCellUnload.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessMoveALeftCellUnload.Location = new System.Drawing.Point(106, 3);
            this.btnIRCutProcessMoveALeftCellUnload.Name = "btnIRCutProcessMoveALeftCellUnload";
            this.btnIRCutProcessMoveALeftCellUnload.Size = new System.Drawing.Size(97, 34);
            this.btnIRCutProcessMoveALeftCellUnload.TabIndex = 62;
            this.btnIRCutProcessMoveALeftCellUnload.Text = "좌측 셀 언로드";
            this.btnIRCutProcessMoveALeftCellUnload.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessMoveAL1Laser
            // 
            this.btnIRCutProcessMoveAL1Laser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessMoveAL1Laser.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessMoveAL1Laser.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessMoveAL1Laser.Location = new System.Drawing.Point(3, 44);
            this.btnIRCutProcessMoveAL1Laser.Name = "btnIRCutProcessMoveAL1Laser";
            this.btnIRCutProcessMoveAL1Laser.Size = new System.Drawing.Size(97, 34);
            this.btnIRCutProcessMoveAL1Laser.TabIndex = 61;
            this.btnIRCutProcessMoveAL1Laser.Text = "좌측1 레이저샷";
            this.btnIRCutProcessMoveAL1Laser.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessMoveAL2Laser
            // 
            this.btnIRCutProcessMoveAL2Laser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessMoveAL2Laser.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessMoveAL2Laser.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessMoveAL2Laser.Location = new System.Drawing.Point(106, 44);
            this.btnIRCutProcessMoveAL2Laser.Name = "btnIRCutProcessMoveAL2Laser";
            this.btnIRCutProcessMoveAL2Laser.Size = new System.Drawing.Size(97, 34);
            this.btnIRCutProcessMoveAL2Laser.TabIndex = 63;
            this.btnIRCutProcessMoveAL2Laser.Text = "좌측2 레이저샷";
            this.btnIRCutProcessMoveAL2Laser.UseVisualStyleBackColor = false;
            // 
            // gxtIRCutProcess_B
            // 
            this.gxtIRCutProcess_B.Controls.Add(this.tableLayoutPanel36);
            this.gxtIRCutProcess_B.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtIRCutProcess_B.ForeColor = System.Drawing.Color.White;
            this.gxtIRCutProcess_B.Location = new System.Drawing.Point(1247, 397);
            this.gxtIRCutProcess_B.Name = "gxtIRCutProcess_B";
            this.gxtIRCutProcess_B.Size = new System.Drawing.Size(449, 192);
            this.gxtIRCutProcess_B.TabIndex = 52;
            this.gxtIRCutProcess_B.TabStop = false;
            this.gxtIRCutProcess_B.Text = "     B     ";
            // 
            // tableLayoutPanel36
            // 
            this.tableLayoutPanel36.ColumnCount = 4;
            this.tableLayoutPanel36.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel36.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel36.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel36.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel36.Controls.Add(this.btnIRCutProcessBR1PneumaticCh1Off, 1, 0);
            this.tableLayoutPanel36.Controls.Add(this.btnIRCutProcessBR2PneumaticCh1On, 2, 0);
            this.tableLayoutPanel36.Controls.Add(this.btnIRCutProcessBR2PneumaticCh1Off, 3, 0);
            this.tableLayoutPanel36.Controls.Add(this.btnIRCutProcessBR1PneumaticCh2Off, 1, 1);
            this.tableLayoutPanel36.Controls.Add(this.btnIRCutProcessBR2PneumaticCh2On, 2, 1);
            this.tableLayoutPanel36.Controls.Add(this.btnIRCutProcessBR2PneumaticCh2Off, 3, 1);
            this.tableLayoutPanel36.Controls.Add(this.btnIRCutProcessBR1PneumaticCh2On, 0, 1);
            this.tableLayoutPanel36.Controls.Add(this.btnIRCutProcessBR1PneumaticCh1On, 0, 0);
            this.tableLayoutPanel36.Controls.Add(this.btnIRCutProcessAR1DestructionCh1On, 0, 2);
            this.tableLayoutPanel36.Controls.Add(this.btnIRCutProcessAR1DestructionCh1Off, 1, 2);
            this.tableLayoutPanel36.Controls.Add(this.btnIRCutProcessAR2DestructionCh1On, 2, 2);
            this.tableLayoutPanel36.Controls.Add(this.btnIRCutProcessAR2DestructionCh1Off, 3, 2);
            this.tableLayoutPanel36.Controls.Add(this.btnIRCutProcessAR1DestructionCh2On, 0, 3);
            this.tableLayoutPanel36.Controls.Add(this.btnIRCutProcessAR1DestructionCh2Off, 1, 3);
            this.tableLayoutPanel36.Controls.Add(this.btnIRCutProcessAR2DestructionCh2Off, 3, 3);
            this.tableLayoutPanel36.Controls.Add(this.btnIRCutProcessAR2DestructionCh2On, 2, 3);
            this.tableLayoutPanel36.Location = new System.Drawing.Point(6, 21);
            this.tableLayoutPanel36.Name = "tableLayoutPanel36";
            this.tableLayoutPanel36.RowCount = 4;
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel36.Size = new System.Drawing.Size(437, 165);
            this.tableLayoutPanel36.TabIndex = 55;
            // 
            // btnIRCutProcessBR1PneumaticCh1Off
            // 
            this.btnIRCutProcessBR1PneumaticCh1Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessBR1PneumaticCh1Off.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessBR1PneumaticCh1Off.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessBR1PneumaticCh1Off.Location = new System.Drawing.Point(112, 3);
            this.btnIRCutProcessBR1PneumaticCh1Off.Name = "btnIRCutProcessBR1PneumaticCh1Off";
            this.btnIRCutProcessBR1PneumaticCh1Off.Size = new System.Drawing.Size(103, 35);
            this.btnIRCutProcessBR1PneumaticCh1Off.TabIndex = 60;
            this.btnIRCutProcessBR1PneumaticCh1Off.Text = "우측1 공압\r\n채널1 오프";
            this.btnIRCutProcessBR1PneumaticCh1Off.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessBR2PneumaticCh1On
            // 
            this.btnIRCutProcessBR2PneumaticCh1On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessBR2PneumaticCh1On.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessBR2PneumaticCh1On.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessBR2PneumaticCh1On.Location = new System.Drawing.Point(221, 3);
            this.btnIRCutProcessBR2PneumaticCh1On.Name = "btnIRCutProcessBR2PneumaticCh1On";
            this.btnIRCutProcessBR2PneumaticCh1On.Size = new System.Drawing.Size(103, 35);
            this.btnIRCutProcessBR2PneumaticCh1On.TabIndex = 63;
            this.btnIRCutProcessBR2PneumaticCh1On.Text = "우측2 공압\r\n채널1 온";
            this.btnIRCutProcessBR2PneumaticCh1On.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessBR2PneumaticCh1Off
            // 
            this.btnIRCutProcessBR2PneumaticCh1Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessBR2PneumaticCh1Off.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessBR2PneumaticCh1Off.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessBR2PneumaticCh1Off.Location = new System.Drawing.Point(330, 3);
            this.btnIRCutProcessBR2PneumaticCh1Off.Name = "btnIRCutProcessBR2PneumaticCh1Off";
            this.btnIRCutProcessBR2PneumaticCh1Off.Size = new System.Drawing.Size(103, 35);
            this.btnIRCutProcessBR2PneumaticCh1Off.TabIndex = 62;
            this.btnIRCutProcessBR2PneumaticCh1Off.Text = "우측2 공압\r\n채널1 오프";
            this.btnIRCutProcessBR2PneumaticCh1Off.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessBR1PneumaticCh2Off
            // 
            this.btnIRCutProcessBR1PneumaticCh2Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessBR1PneumaticCh2Off.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessBR1PneumaticCh2Off.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessBR1PneumaticCh2Off.Location = new System.Drawing.Point(112, 44);
            this.btnIRCutProcessBR1PneumaticCh2Off.Name = "btnIRCutProcessBR1PneumaticCh2Off";
            this.btnIRCutProcessBR1PneumaticCh2Off.Size = new System.Drawing.Size(103, 35);
            this.btnIRCutProcessBR1PneumaticCh2Off.TabIndex = 65;
            this.btnIRCutProcessBR1PneumaticCh2Off.Text = "우측1 공압\r\n채널2 오프";
            this.btnIRCutProcessBR1PneumaticCh2Off.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessBR2PneumaticCh2On
            // 
            this.btnIRCutProcessBR2PneumaticCh2On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessBR2PneumaticCh2On.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessBR2PneumaticCh2On.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessBR2PneumaticCh2On.Location = new System.Drawing.Point(221, 44);
            this.btnIRCutProcessBR2PneumaticCh2On.Name = "btnIRCutProcessBR2PneumaticCh2On";
            this.btnIRCutProcessBR2PneumaticCh2On.Size = new System.Drawing.Size(103, 35);
            this.btnIRCutProcessBR2PneumaticCh2On.TabIndex = 64;
            this.btnIRCutProcessBR2PneumaticCh2On.Text = "우측2 공압\r\n채널2 온";
            this.btnIRCutProcessBR2PneumaticCh2On.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessBR2PneumaticCh2Off
            // 
            this.btnIRCutProcessBR2PneumaticCh2Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessBR2PneumaticCh2Off.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessBR2PneumaticCh2Off.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessBR2PneumaticCh2Off.Location = new System.Drawing.Point(330, 44);
            this.btnIRCutProcessBR2PneumaticCh2Off.Name = "btnIRCutProcessBR2PneumaticCh2Off";
            this.btnIRCutProcessBR2PneumaticCh2Off.Size = new System.Drawing.Size(104, 35);
            this.btnIRCutProcessBR2PneumaticCh2Off.TabIndex = 68;
            this.btnIRCutProcessBR2PneumaticCh2Off.Text = "우측2 공압\r\n채널2 오프";
            this.btnIRCutProcessBR2PneumaticCh2Off.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessBR1PneumaticCh2On
            // 
            this.btnIRCutProcessBR1PneumaticCh2On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessBR1PneumaticCh2On.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessBR1PneumaticCh2On.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessBR1PneumaticCh2On.Location = new System.Drawing.Point(3, 44);
            this.btnIRCutProcessBR1PneumaticCh2On.Name = "btnIRCutProcessBR1PneumaticCh2On";
            this.btnIRCutProcessBR1PneumaticCh2On.Size = new System.Drawing.Size(103, 35);
            this.btnIRCutProcessBR1PneumaticCh2On.TabIndex = 61;
            this.btnIRCutProcessBR1PneumaticCh2On.Text = "우측1 공압\r\n채널2 온";
            this.btnIRCutProcessBR1PneumaticCh2On.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessBR1PneumaticCh1On
            // 
            this.btnIRCutProcessBR1PneumaticCh1On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessBR1PneumaticCh1On.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessBR1PneumaticCh1On.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessBR1PneumaticCh1On.Location = new System.Drawing.Point(3, 3);
            this.btnIRCutProcessBR1PneumaticCh1On.Name = "btnIRCutProcessBR1PneumaticCh1On";
            this.btnIRCutProcessBR1PneumaticCh1On.Size = new System.Drawing.Size(103, 35);
            this.btnIRCutProcessBR1PneumaticCh1On.TabIndex = 59;
            this.btnIRCutProcessBR1PneumaticCh1On.Text = "우측1 공압\r\n채널1 온";
            this.btnIRCutProcessBR1PneumaticCh1On.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessAR1DestructionCh1On
            // 
            this.btnIRCutProcessAR1DestructionCh1On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessAR1DestructionCh1On.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessAR1DestructionCh1On.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessAR1DestructionCh1On.Location = new System.Drawing.Point(3, 85);
            this.btnIRCutProcessAR1DestructionCh1On.Name = "btnIRCutProcessAR1DestructionCh1On";
            this.btnIRCutProcessAR1DestructionCh1On.Size = new System.Drawing.Size(103, 35);
            this.btnIRCutProcessAR1DestructionCh1On.TabIndex = 69;
            this.btnIRCutProcessAR1DestructionCh1On.Text = "우측1 파기\r\n채널1 온";
            this.btnIRCutProcessAR1DestructionCh1On.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessAR1DestructionCh1Off
            // 
            this.btnIRCutProcessAR1DestructionCh1Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessAR1DestructionCh1Off.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessAR1DestructionCh1Off.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessAR1DestructionCh1Off.Location = new System.Drawing.Point(112, 85);
            this.btnIRCutProcessAR1DestructionCh1Off.Name = "btnIRCutProcessAR1DestructionCh1Off";
            this.btnIRCutProcessAR1DestructionCh1Off.Size = new System.Drawing.Size(103, 35);
            this.btnIRCutProcessAR1DestructionCh1Off.TabIndex = 70;
            this.btnIRCutProcessAR1DestructionCh1Off.Text = "우측1 파기\r\n채널1 오프";
            this.btnIRCutProcessAR1DestructionCh1Off.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessAR2DestructionCh1On
            // 
            this.btnIRCutProcessAR2DestructionCh1On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessAR2DestructionCh1On.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessAR2DestructionCh1On.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessAR2DestructionCh1On.Location = new System.Drawing.Point(221, 85);
            this.btnIRCutProcessAR2DestructionCh1On.Name = "btnIRCutProcessAR2DestructionCh1On";
            this.btnIRCutProcessAR2DestructionCh1On.Size = new System.Drawing.Size(103, 35);
            this.btnIRCutProcessAR2DestructionCh1On.TabIndex = 71;
            this.btnIRCutProcessAR2DestructionCh1On.Text = "우측2 파기\r\n채널1 온";
            this.btnIRCutProcessAR2DestructionCh1On.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessAR2DestructionCh1Off
            // 
            this.btnIRCutProcessAR2DestructionCh1Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessAR2DestructionCh1Off.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessAR2DestructionCh1Off.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessAR2DestructionCh1Off.Location = new System.Drawing.Point(330, 85);
            this.btnIRCutProcessAR2DestructionCh1Off.Name = "btnIRCutProcessAR2DestructionCh1Off";
            this.btnIRCutProcessAR2DestructionCh1Off.Size = new System.Drawing.Size(103, 35);
            this.btnIRCutProcessAR2DestructionCh1Off.TabIndex = 72;
            this.btnIRCutProcessAR2DestructionCh1Off.Text = "우측2 파기\r\n채널1 오프";
            this.btnIRCutProcessAR2DestructionCh1Off.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessAR1DestructionCh2On
            // 
            this.btnIRCutProcessAR1DestructionCh2On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessAR1DestructionCh2On.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessAR1DestructionCh2On.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessAR1DestructionCh2On.Location = new System.Drawing.Point(3, 126);
            this.btnIRCutProcessAR1DestructionCh2On.Name = "btnIRCutProcessAR1DestructionCh2On";
            this.btnIRCutProcessAR1DestructionCh2On.Size = new System.Drawing.Size(103, 35);
            this.btnIRCutProcessAR1DestructionCh2On.TabIndex = 73;
            this.btnIRCutProcessAR1DestructionCh2On.Text = "우측1 파기\r\n채널2 온";
            this.btnIRCutProcessAR1DestructionCh2On.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessAR1DestructionCh2Off
            // 
            this.btnIRCutProcessAR1DestructionCh2Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessAR1DestructionCh2Off.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessAR1DestructionCh2Off.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessAR1DestructionCh2Off.Location = new System.Drawing.Point(112, 126);
            this.btnIRCutProcessAR1DestructionCh2Off.Name = "btnIRCutProcessAR1DestructionCh2Off";
            this.btnIRCutProcessAR1DestructionCh2Off.Size = new System.Drawing.Size(103, 35);
            this.btnIRCutProcessAR1DestructionCh2Off.TabIndex = 74;
            this.btnIRCutProcessAR1DestructionCh2Off.Text = "우측1 파기\r\n채널2 오프";
            this.btnIRCutProcessAR1DestructionCh2Off.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessAR2DestructionCh2Off
            // 
            this.btnIRCutProcessAR2DestructionCh2Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessAR2DestructionCh2Off.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessAR2DestructionCh2Off.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessAR2DestructionCh2Off.Location = new System.Drawing.Point(330, 126);
            this.btnIRCutProcessAR2DestructionCh2Off.Name = "btnIRCutProcessAR2DestructionCh2Off";
            this.btnIRCutProcessAR2DestructionCh2Off.Size = new System.Drawing.Size(103, 35);
            this.btnIRCutProcessAR2DestructionCh2Off.TabIndex = 76;
            this.btnIRCutProcessAR2DestructionCh2Off.Text = "우측2 파기\r\n채널2 오프";
            this.btnIRCutProcessAR2DestructionCh2Off.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessAR2DestructionCh2On
            // 
            this.btnIRCutProcessAR2DestructionCh2On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessAR2DestructionCh2On.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessAR2DestructionCh2On.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessAR2DestructionCh2On.Location = new System.Drawing.Point(221, 126);
            this.btnIRCutProcessAR2DestructionCh2On.Name = "btnIRCutProcessAR2DestructionCh2On";
            this.btnIRCutProcessAR2DestructionCh2On.Size = new System.Drawing.Size(103, 35);
            this.btnIRCutProcessAR2DestructionCh2On.TabIndex = 75;
            this.btnIRCutProcessAR2DestructionCh2On.Text = "우측2 파기\r\n채널2 온";
            this.btnIRCutProcessAR2DestructionCh2On.UseVisualStyleBackColor = false;
            // 
            // gxtIRCutProcess_A
            // 
            this.gxtIRCutProcess_A.Controls.Add(this.tableLayoutPanel35);
            this.gxtIRCutProcess_A.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtIRCutProcess_A.ForeColor = System.Drawing.Color.White;
            this.gxtIRCutProcess_A.Location = new System.Drawing.Point(792, 397);
            this.gxtIRCutProcess_A.Name = "gxtIRCutProcess_A";
            this.gxtIRCutProcess_A.Size = new System.Drawing.Size(449, 192);
            this.gxtIRCutProcess_A.TabIndex = 51;
            this.gxtIRCutProcess_A.TabStop = false;
            this.gxtIRCutProcess_A.Text = "     A    ";
            // 
            // tableLayoutPanel35
            // 
            this.tableLayoutPanel35.ColumnCount = 4;
            this.tableLayoutPanel35.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel35.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel35.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel35.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel35.Controls.Add(this.btnIRCutProcessAL1DestructionCh2Off, 0, 3);
            this.tableLayoutPanel35.Controls.Add(this.btnIRCutProcessAL1DestructionCh2On, 0, 3);
            this.tableLayoutPanel35.Controls.Add(this.btnIRCutProcessAL2DestructionCh2Off, 0, 3);
            this.tableLayoutPanel35.Controls.Add(this.btnIRCutProcessAL2DestructionCh2On, 0, 3);
            this.tableLayoutPanel35.Controls.Add(this.btnIRCutProcessAL1PneumaticCh1Off, 1, 0);
            this.tableLayoutPanel35.Controls.Add(this.btnIRCutProcessAL2PneumaticCh1On, 2, 0);
            this.tableLayoutPanel35.Controls.Add(this.btnIRCutProcessAL2PneumaticCh1Off, 3, 0);
            this.tableLayoutPanel35.Controls.Add(this.btnIRCutProcessAL1PneumaticCh2Off, 1, 1);
            this.tableLayoutPanel35.Controls.Add(this.btnIRCutProcessAL2PneumaticCh2On, 2, 1);
            this.tableLayoutPanel35.Controls.Add(this.btnIRCutProcessAL2PneumaticCh2Off, 3, 1);
            this.tableLayoutPanel35.Controls.Add(this.btnIRCutProcessAL1PneumaticCh2On, 0, 1);
            this.tableLayoutPanel35.Controls.Add(this.btnIRCutProcessAL1PneumaticCh1On, 0, 0);
            this.tableLayoutPanel35.Controls.Add(this.btnIRCutProcessAL1DestructionCh1On, 0, 2);
            this.tableLayoutPanel35.Controls.Add(this.btnIRCutProcessAL1DestructionCh1Off, 1, 2);
            this.tableLayoutPanel35.Controls.Add(this.btnIRCutProcessAL2DestructionCh1On, 2, 2);
            this.tableLayoutPanel35.Controls.Add(this.btnIRCutProcessAL2DestructionCh1Off, 3, 2);
            this.tableLayoutPanel35.Location = new System.Drawing.Point(6, 21);
            this.tableLayoutPanel35.Name = "tableLayoutPanel35";
            this.tableLayoutPanel35.RowCount = 4;
            this.tableLayoutPanel35.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel35.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel35.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel35.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel35.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel35.Size = new System.Drawing.Size(437, 165);
            this.tableLayoutPanel35.TabIndex = 54;
            // 
            // btnIRCutProcessAL1DestructionCh2Off
            // 
            this.btnIRCutProcessAL1DestructionCh2Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessAL1DestructionCh2Off.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessAL1DestructionCh2Off.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessAL1DestructionCh2Off.Location = new System.Drawing.Point(112, 126);
            this.btnIRCutProcessAL1DestructionCh2Off.Name = "btnIRCutProcessAL1DestructionCh2Off";
            this.btnIRCutProcessAL1DestructionCh2Off.Size = new System.Drawing.Size(103, 35);
            this.btnIRCutProcessAL1DestructionCh2Off.TabIndex = 76;
            this.btnIRCutProcessAL1DestructionCh2Off.Text = "좌측1 파기\r\n채널2 오프";
            this.btnIRCutProcessAL1DestructionCh2Off.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessAL1DestructionCh2On
            // 
            this.btnIRCutProcessAL1DestructionCh2On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessAL1DestructionCh2On.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessAL1DestructionCh2On.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessAL1DestructionCh2On.Location = new System.Drawing.Point(3, 126);
            this.btnIRCutProcessAL1DestructionCh2On.Name = "btnIRCutProcessAL1DestructionCh2On";
            this.btnIRCutProcessAL1DestructionCh2On.Size = new System.Drawing.Size(103, 35);
            this.btnIRCutProcessAL1DestructionCh2On.TabIndex = 75;
            this.btnIRCutProcessAL1DestructionCh2On.Text = "좌측1 파기\r\n채널2 온";
            this.btnIRCutProcessAL1DestructionCh2On.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessAL2DestructionCh2Off
            // 
            this.btnIRCutProcessAL2DestructionCh2Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessAL2DestructionCh2Off.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessAL2DestructionCh2Off.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessAL2DestructionCh2Off.Location = new System.Drawing.Point(330, 126);
            this.btnIRCutProcessAL2DestructionCh2Off.Name = "btnIRCutProcessAL2DestructionCh2Off";
            this.btnIRCutProcessAL2DestructionCh2Off.Size = new System.Drawing.Size(103, 35);
            this.btnIRCutProcessAL2DestructionCh2Off.TabIndex = 74;
            this.btnIRCutProcessAL2DestructionCh2Off.Text = "좌측2 파기\r\n채널2 오프";
            this.btnIRCutProcessAL2DestructionCh2Off.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessAL2DestructionCh2On
            // 
            this.btnIRCutProcessAL2DestructionCh2On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessAL2DestructionCh2On.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessAL2DestructionCh2On.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessAL2DestructionCh2On.Location = new System.Drawing.Point(221, 126);
            this.btnIRCutProcessAL2DestructionCh2On.Name = "btnIRCutProcessAL2DestructionCh2On";
            this.btnIRCutProcessAL2DestructionCh2On.Size = new System.Drawing.Size(103, 35);
            this.btnIRCutProcessAL2DestructionCh2On.TabIndex = 73;
            this.btnIRCutProcessAL2DestructionCh2On.Text = "좌측2 파기\r\n채널2 온";
            this.btnIRCutProcessAL2DestructionCh2On.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessAL1PneumaticCh1Off
            // 
            this.btnIRCutProcessAL1PneumaticCh1Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessAL1PneumaticCh1Off.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessAL1PneumaticCh1Off.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessAL1PneumaticCh1Off.Location = new System.Drawing.Point(112, 3);
            this.btnIRCutProcessAL1PneumaticCh1Off.Name = "btnIRCutProcessAL1PneumaticCh1Off";
            this.btnIRCutProcessAL1PneumaticCh1Off.Size = new System.Drawing.Size(103, 35);
            this.btnIRCutProcessAL1PneumaticCh1Off.TabIndex = 60;
            this.btnIRCutProcessAL1PneumaticCh1Off.Text = "좌측1 공압\r\n채널1 오프";
            this.btnIRCutProcessAL1PneumaticCh1Off.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessAL2PneumaticCh1On
            // 
            this.btnIRCutProcessAL2PneumaticCh1On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessAL2PneumaticCh1On.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessAL2PneumaticCh1On.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessAL2PneumaticCh1On.Location = new System.Drawing.Point(221, 3);
            this.btnIRCutProcessAL2PneumaticCh1On.Name = "btnIRCutProcessAL2PneumaticCh1On";
            this.btnIRCutProcessAL2PneumaticCh1On.Size = new System.Drawing.Size(103, 35);
            this.btnIRCutProcessAL2PneumaticCh1On.TabIndex = 63;
            this.btnIRCutProcessAL2PneumaticCh1On.Text = "좌측2 공압\r\n채널1 온";
            this.btnIRCutProcessAL2PneumaticCh1On.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessAL2PneumaticCh1Off
            // 
            this.btnIRCutProcessAL2PneumaticCh1Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessAL2PneumaticCh1Off.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessAL2PneumaticCh1Off.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessAL2PneumaticCh1Off.Location = new System.Drawing.Point(330, 3);
            this.btnIRCutProcessAL2PneumaticCh1Off.Name = "btnIRCutProcessAL2PneumaticCh1Off";
            this.btnIRCutProcessAL2PneumaticCh1Off.Size = new System.Drawing.Size(103, 35);
            this.btnIRCutProcessAL2PneumaticCh1Off.TabIndex = 62;
            this.btnIRCutProcessAL2PneumaticCh1Off.Text = "좌측2 공압\r\n채널1 오프";
            this.btnIRCutProcessAL2PneumaticCh1Off.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessAL1PneumaticCh2Off
            // 
            this.btnIRCutProcessAL1PneumaticCh2Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessAL1PneumaticCh2Off.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessAL1PneumaticCh2Off.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessAL1PneumaticCh2Off.Location = new System.Drawing.Point(112, 44);
            this.btnIRCutProcessAL1PneumaticCh2Off.Name = "btnIRCutProcessAL1PneumaticCh2Off";
            this.btnIRCutProcessAL1PneumaticCh2Off.Size = new System.Drawing.Size(103, 35);
            this.btnIRCutProcessAL1PneumaticCh2Off.TabIndex = 65;
            this.btnIRCutProcessAL1PneumaticCh2Off.Text = "좌측1 공압\r\n채널2 오프";
            this.btnIRCutProcessAL1PneumaticCh2Off.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessAL2PneumaticCh2On
            // 
            this.btnIRCutProcessAL2PneumaticCh2On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessAL2PneumaticCh2On.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessAL2PneumaticCh2On.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessAL2PneumaticCh2On.Location = new System.Drawing.Point(221, 44);
            this.btnIRCutProcessAL2PneumaticCh2On.Name = "btnIRCutProcessAL2PneumaticCh2On";
            this.btnIRCutProcessAL2PneumaticCh2On.Size = new System.Drawing.Size(103, 35);
            this.btnIRCutProcessAL2PneumaticCh2On.TabIndex = 64;
            this.btnIRCutProcessAL2PneumaticCh2On.Text = "좌측2 공압\r\n채널2 온";
            this.btnIRCutProcessAL2PneumaticCh2On.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessAL2PneumaticCh2Off
            // 
            this.btnIRCutProcessAL2PneumaticCh2Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessAL2PneumaticCh2Off.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessAL2PneumaticCh2Off.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessAL2PneumaticCh2Off.Location = new System.Drawing.Point(330, 44);
            this.btnIRCutProcessAL2PneumaticCh2Off.Name = "btnIRCutProcessAL2PneumaticCh2Off";
            this.btnIRCutProcessAL2PneumaticCh2Off.Size = new System.Drawing.Size(104, 35);
            this.btnIRCutProcessAL2PneumaticCh2Off.TabIndex = 68;
            this.btnIRCutProcessAL2PneumaticCh2Off.Text = "좌측2 공압\r\n채널2 오프";
            this.btnIRCutProcessAL2PneumaticCh2Off.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessAL1PneumaticCh2On
            // 
            this.btnIRCutProcessAL1PneumaticCh2On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessAL1PneumaticCh2On.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessAL1PneumaticCh2On.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessAL1PneumaticCh2On.Location = new System.Drawing.Point(3, 44);
            this.btnIRCutProcessAL1PneumaticCh2On.Name = "btnIRCutProcessAL1PneumaticCh2On";
            this.btnIRCutProcessAL1PneumaticCh2On.Size = new System.Drawing.Size(103, 35);
            this.btnIRCutProcessAL1PneumaticCh2On.TabIndex = 61;
            this.btnIRCutProcessAL1PneumaticCh2On.Text = "좌측1 공압\r\n채널2 온";
            this.btnIRCutProcessAL1PneumaticCh2On.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessAL1PneumaticCh1On
            // 
            this.btnIRCutProcessAL1PneumaticCh1On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessAL1PneumaticCh1On.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessAL1PneumaticCh1On.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessAL1PneumaticCh1On.Location = new System.Drawing.Point(3, 3);
            this.btnIRCutProcessAL1PneumaticCh1On.Name = "btnIRCutProcessAL1PneumaticCh1On";
            this.btnIRCutProcessAL1PneumaticCh1On.Size = new System.Drawing.Size(103, 35);
            this.btnIRCutProcessAL1PneumaticCh1On.TabIndex = 59;
            this.btnIRCutProcessAL1PneumaticCh1On.Text = "좌측1 공압\r\n채널1 온";
            this.btnIRCutProcessAL1PneumaticCh1On.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessAL1DestructionCh1On
            // 
            this.btnIRCutProcessAL1DestructionCh1On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessAL1DestructionCh1On.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessAL1DestructionCh1On.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessAL1DestructionCh1On.Location = new System.Drawing.Point(3, 85);
            this.btnIRCutProcessAL1DestructionCh1On.Name = "btnIRCutProcessAL1DestructionCh1On";
            this.btnIRCutProcessAL1DestructionCh1On.Size = new System.Drawing.Size(103, 35);
            this.btnIRCutProcessAL1DestructionCh1On.TabIndex = 69;
            this.btnIRCutProcessAL1DestructionCh1On.Text = "좌측1 파기\r\n채널1 온";
            this.btnIRCutProcessAL1DestructionCh1On.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessAL1DestructionCh1Off
            // 
            this.btnIRCutProcessAL1DestructionCh1Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessAL1DestructionCh1Off.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessAL1DestructionCh1Off.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessAL1DestructionCh1Off.Location = new System.Drawing.Point(112, 85);
            this.btnIRCutProcessAL1DestructionCh1Off.Name = "btnIRCutProcessAL1DestructionCh1Off";
            this.btnIRCutProcessAL1DestructionCh1Off.Size = new System.Drawing.Size(103, 35);
            this.btnIRCutProcessAL1DestructionCh1Off.TabIndex = 70;
            this.btnIRCutProcessAL1DestructionCh1Off.Text = "좌측1 파기\r\n채널1 오프";
            this.btnIRCutProcessAL1DestructionCh1Off.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessAL2DestructionCh1On
            // 
            this.btnIRCutProcessAL2DestructionCh1On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessAL2DestructionCh1On.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessAL2DestructionCh1On.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessAL2DestructionCh1On.Location = new System.Drawing.Point(221, 85);
            this.btnIRCutProcessAL2DestructionCh1On.Name = "btnIRCutProcessAL2DestructionCh1On";
            this.btnIRCutProcessAL2DestructionCh1On.Size = new System.Drawing.Size(103, 35);
            this.btnIRCutProcessAL2DestructionCh1On.TabIndex = 71;
            this.btnIRCutProcessAL2DestructionCh1On.Text = "좌측2 파기\r\n채널1 온";
            this.btnIRCutProcessAL2DestructionCh1On.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessAL2DestructionCh1Off
            // 
            this.btnIRCutProcessAL2DestructionCh1Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessAL2DestructionCh1Off.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnIRCutProcessAL2DestructionCh1Off.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessAL2DestructionCh1Off.Location = new System.Drawing.Point(330, 85);
            this.btnIRCutProcessAL2DestructionCh1Off.Name = "btnIRCutProcessAL2DestructionCh1Off";
            this.btnIRCutProcessAL2DestructionCh1Off.Size = new System.Drawing.Size(103, 35);
            this.btnIRCutProcessAL2DestructionCh1Off.TabIndex = 72;
            this.btnIRCutProcessAL2DestructionCh1Off.Text = "좌측2 파기\r\n채널1 오프";
            this.btnIRCutProcessAL2DestructionCh1Off.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessSave
            // 
            this.btnIRCutProcessSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessSave.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnIRCutProcessSave.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessSave.Location = new System.Drawing.Point(1482, 783);
            this.btnIRCutProcessSave.Name = "btnIRCutProcessSave";
            this.btnIRCutProcessSave.Size = new System.Drawing.Size(228, 28);
            this.btnIRCutProcessSave.TabIndex = 50;
            this.btnIRCutProcessSave.Text = "저장";
            this.btnIRCutProcessSave.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessSpeedSetting
            // 
            this.btnIRCutProcessSpeedSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessSpeedSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnIRCutProcessSpeedSetting.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessSpeedSetting.Location = new System.Drawing.Point(1291, 315);
            this.btnIRCutProcessSpeedSetting.Name = "btnIRCutProcessSpeedSetting";
            this.btnIRCutProcessSpeedSetting.Size = new System.Drawing.Size(90, 27);
            this.btnIRCutProcessSpeedSetting.TabIndex = 48;
            this.btnIRCutProcessSpeedSetting.Text = "속도 설정";
            this.btnIRCutProcessSpeedSetting.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessAllSetting
            // 
            this.btnIRCutProcessAllSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessAllSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnIRCutProcessAllSetting.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessAllSetting.Location = new System.Drawing.Point(1387, 273);
            this.btnIRCutProcessAllSetting.Name = "btnIRCutProcessAllSetting";
            this.btnIRCutProcessAllSetting.Size = new System.Drawing.Size(90, 27);
            this.btnIRCutProcessAllSetting.TabIndex = 47;
            this.btnIRCutProcessAllSetting.Text = "전체 설정";
            this.btnIRCutProcessAllSetting.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessLocationSetting
            // 
            this.btnIRCutProcessLocationSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessLocationSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnIRCutProcessLocationSetting.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessLocationSetting.Location = new System.Drawing.Point(1071, 315);
            this.btnIRCutProcessLocationSetting.Name = "btnIRCutProcessLocationSetting";
            this.btnIRCutProcessLocationSetting.Size = new System.Drawing.Size(90, 27);
            this.btnIRCutProcessLocationSetting.TabIndex = 46;
            this.btnIRCutProcessLocationSetting.Text = "위치 설정";
            this.btnIRCutProcessLocationSetting.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessMoveLocation
            // 
            this.btnIRCutProcessMoveLocation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessMoveLocation.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnIRCutProcessMoveLocation.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessMoveLocation.Location = new System.Drawing.Point(975, 315);
            this.btnIRCutProcessMoveLocation.Name = "btnIRCutProcessMoveLocation";
            this.btnIRCutProcessMoveLocation.Size = new System.Drawing.Size(90, 27);
            this.btnIRCutProcessMoveLocation.TabIndex = 45;
            this.btnIRCutProcessMoveLocation.Text = "위치 이동";
            this.btnIRCutProcessMoveLocation.UseVisualStyleBackColor = false;
            // 
            // txtIRCutProcessLocation
            // 
            this.txtIRCutProcessLocation.Font = new System.Drawing.Font("굴림", 13F);
            this.txtIRCutProcessLocation.Location = new System.Drawing.Point(879, 316);
            this.txtIRCutProcessLocation.Name = "txtIRCutProcessLocation";
            this.txtIRCutProcessLocation.Size = new System.Drawing.Size(90, 27);
            this.txtIRCutProcessLocation.TabIndex = 44;
            // 
            // txtIRCutProcessSpeed
            // 
            this.txtIRCutProcessSpeed.Font = new System.Drawing.Font("굴림", 13F);
            this.txtIRCutProcessSpeed.Location = new System.Drawing.Point(1291, 273);
            this.txtIRCutProcessSpeed.Name = "txtIRCutProcessSpeed";
            this.txtIRCutProcessSpeed.Size = new System.Drawing.Size(90, 27);
            this.txtIRCutProcessSpeed.TabIndex = 43;
            // 
            // txtIRCutProcessCurrentLocation
            // 
            this.txtIRCutProcessCurrentLocation.Font = new System.Drawing.Font("굴림", 13F);
            this.txtIRCutProcessCurrentLocation.Location = new System.Drawing.Point(1084, 274);
            this.txtIRCutProcessCurrentLocation.Name = "txtIRCutProcessCurrentLocation";
            this.txtIRCutProcessCurrentLocation.Size = new System.Drawing.Size(90, 27);
            this.txtIRCutProcessCurrentLocation.TabIndex = 42;
            // 
            // txtIRCutProcessSelectedShaft
            // 
            this.txtIRCutProcessSelectedShaft.Font = new System.Drawing.Font("굴림", 13F);
            this.txtIRCutProcessSelectedShaft.Location = new System.Drawing.Point(879, 273);
            this.txtIRCutProcessSelectedShaft.Name = "txtIRCutProcessSelectedShaft";
            this.txtIRCutProcessSelectedShaft.Size = new System.Drawing.Size(90, 27);
            this.txtIRCutProcessSelectedShaft.TabIndex = 41;
            // 
            // lbl_IRCutProcess_Location
            // 
            this.lbl_IRCutProcess_Location.AutoSize = true;
            this.lbl_IRCutProcess_Location.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_IRCutProcess_Location.ForeColor = System.Drawing.Color.White;
            this.lbl_IRCutProcess_Location.Location = new System.Drawing.Point(801, 316);
            this.lbl_IRCutProcess_Location.Name = "lbl_IRCutProcess_Location";
            this.lbl_IRCutProcess_Location.Size = new System.Drawing.Size(84, 21);
            this.lbl_IRCutProcess_Location.TabIndex = 40;
            this.lbl_IRCutProcess_Location.Text = "위치[mm]";
            // 
            // lbl_IRCutProcess_Speed
            // 
            this.lbl_IRCutProcess_Speed.AutoSize = true;
            this.lbl_IRCutProcess_Speed.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_IRCutProcess_Speed.ForeColor = System.Drawing.Color.White;
            this.lbl_IRCutProcess_Speed.Location = new System.Drawing.Point(1179, 275);
            this.lbl_IRCutProcess_Speed.Name = "lbl_IRCutProcess_Speed";
            this.lbl_IRCutProcess_Speed.Size = new System.Drawing.Size(115, 21);
            this.lbl_IRCutProcess_Speed.TabIndex = 39;
            this.lbl_IRCutProcess_Speed.Text = "속도[mm/sec]";
            // 
            // lbl_IRCutProcess_SelectedShaft
            // 
            this.lbl_IRCutProcess_SelectedShaft.AutoSize = true;
            this.lbl_IRCutProcess_SelectedShaft.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_IRCutProcess_SelectedShaft.ForeColor = System.Drawing.Color.White;
            this.lbl_IRCutProcess_SelectedShaft.Location = new System.Drawing.Point(805, 274);
            this.lbl_IRCutProcess_SelectedShaft.Name = "lbl_IRCutProcess_SelectedShaft";
            this.lbl_IRCutProcess_SelectedShaft.Size = new System.Drawing.Size(80, 21);
            this.lbl_IRCutProcess_SelectedShaft.TabIndex = 38;
            this.lbl_IRCutProcess_SelectedShaft.Text = "선택된 축";
            // 
            // txtIRCutProcess_LocationSetting
            // 
            this.txtIRCutProcess_LocationSetting.BackColor = System.Drawing.SystemColors.Highlight;
            this.txtIRCutProcess_LocationSetting.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.txtIRCutProcess_LocationSetting.ForeColor = System.Drawing.Color.White;
            this.txtIRCutProcess_LocationSetting.Location = new System.Drawing.Point(792, 223);
            this.txtIRCutProcess_LocationSetting.Name = "txtIRCutProcess_LocationSetting";
            this.txtIRCutProcess_LocationSetting.ReadOnly = true;
            this.txtIRCutProcess_LocationSetting.Size = new System.Drawing.Size(89, 27);
            this.txtIRCutProcess_LocationSetting.TabIndex = 37;
            this.txtIRCutProcess_LocationSetting.Text = "위치값 설정";
            this.txtIRCutProcess_LocationSetting.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbl_IRCutProcess_CurrentLocation
            // 
            this.lbl_IRCutProcess_CurrentLocation.AutoSize = true;
            this.lbl_IRCutProcess_CurrentLocation.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_IRCutProcess_CurrentLocation.ForeColor = System.Drawing.Color.White;
            this.lbl_IRCutProcess_CurrentLocation.Location = new System.Drawing.Point(975, 274);
            this.lbl_IRCutProcess_CurrentLocation.Name = "lbl_IRCutProcess_CurrentLocation";
            this.lbl_IRCutProcess_CurrentLocation.Size = new System.Drawing.Size(116, 21);
            this.lbl_IRCutProcess_CurrentLocation.TabIndex = 36;
            this.lbl_IRCutProcess_CurrentLocation.Text = "현재위치[mm]";
            // 
            // btnIRCutProcessGrabSwitch3
            // 
            this.btnIRCutProcessGrabSwitch3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessGrabSwitch3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnIRCutProcessGrabSwitch3.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessGrabSwitch3.Location = new System.Drawing.Point(1514, 6);
            this.btnIRCutProcessGrabSwitch3.Name = "btnIRCutProcessGrabSwitch3";
            this.btnIRCutProcessGrabSwitch3.Size = new System.Drawing.Size(172, 23);
            this.btnIRCutProcessGrabSwitch3.TabIndex = 35;
            this.btnIRCutProcessGrabSwitch3.Text = "Grab Switch 3";
            this.btnIRCutProcessGrabSwitch3.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessGrabSwitch2
            // 
            this.btnIRCutProcessGrabSwitch2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessGrabSwitch2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnIRCutProcessGrabSwitch2.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessGrabSwitch2.Location = new System.Drawing.Point(1336, 6);
            this.btnIRCutProcessGrabSwitch2.Name = "btnIRCutProcessGrabSwitch2";
            this.btnIRCutProcessGrabSwitch2.Size = new System.Drawing.Size(172, 23);
            this.btnIRCutProcessGrabSwitch2.TabIndex = 34;
            this.btnIRCutProcessGrabSwitch2.Text = "Grab Switch 2";
            this.btnIRCutProcessGrabSwitch2.UseVisualStyleBackColor = false;
            // 
            // btnIRCutProcessGrabSwitch1
            // 
            this.btnIRCutProcessGrabSwitch1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIRCutProcessGrabSwitch1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnIRCutProcessGrabSwitch1.ForeColor = System.Drawing.Color.White;
            this.btnIRCutProcessGrabSwitch1.Location = new System.Drawing.Point(1158, 6);
            this.btnIRCutProcessGrabSwitch1.Name = "btnIRCutProcessGrabSwitch1";
            this.btnIRCutProcessGrabSwitch1.Size = new System.Drawing.Size(172, 23);
            this.btnIRCutProcessGrabSwitch1.TabIndex = 33;
            this.btnIRCutProcessGrabSwitch1.Text = "Grab Switch 1";
            this.btnIRCutProcessGrabSwitch1.UseVisualStyleBackColor = false;
            // 
            // lvIRCutProcess
            // 
            this.lvIRCutProcess.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.col_IRCutProcess_NamebyLocation,
            this.col_IRCutProcess_Shaft,
            this.col_IRCutProcess_Location,
            this.col_IRCutProcess_Speed});
            this.lvIRCutProcess.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lvIRCutProcess.GridLines = true;
            this.lvIRCutProcess.Location = new System.Drawing.Point(17, 16);
            this.lvIRCutProcess.Name = "lvIRCutProcess";
            this.lvIRCutProcess.Size = new System.Drawing.Size(758, 734);
            this.lvIRCutProcess.TabIndex = 32;
            this.lvIRCutProcess.UseCompatibleStateImageBehavior = false;
            this.lvIRCutProcess.View = System.Windows.Forms.View.Details;
            // 
            // col_IRCutProcess_NamebyLocation
            // 
            this.col_IRCutProcess_NamebyLocation.Text = "위치별 명칭";
            this.col_IRCutProcess_NamebyLocation.Width = 430;
            // 
            // col_IRCutProcess_Shaft
            // 
            this.col_IRCutProcess_Shaft.Text = "축";
            this.col_IRCutProcess_Shaft.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // col_IRCutProcess_Location
            // 
            this.col_IRCutProcess_Location.Text = "위치[mm]";
            this.col_IRCutProcess_Location.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_IRCutProcess_Location.Width = 125;
            // 
            // col_IRCutProcess_Speed
            // 
            this.col_IRCutProcess_Speed.Text = "속도[mm/s]";
            this.col_IRCutProcess_Speed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_IRCutProcess_Speed.Width = 125;
            // 
            // ajin_Setting_IRCutProcess
            // 
            this.ajin_Setting_IRCutProcess.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_IRCutProcess.Location = new System.Drawing.Point(792, 34);
            this.ajin_Setting_IRCutProcess.Name = "ajin_Setting_IRCutProcess";
            this.ajin_Setting_IRCutProcess.Servo = null;
            this.ajin_Setting_IRCutProcess.Size = new System.Drawing.Size(903, 183);
            this.ajin_Setting_IRCutProcess.TabIndex = 4;
            // 
            // tp_BreakTransfer
            // 
            this.tp_BreakTransfer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tp_BreakTransfer.Controls.Add(this.gxtBreakTransfer_Move);
            this.tp_BreakTransfer.Controls.Add(this.gxtBreakTransfer_B);
            this.tp_BreakTransfer.Controls.Add(this.gxtBreakTransfer_A);
            this.tp_BreakTransfer.Controls.Add(this.btnBreakTransferSave);
            this.tp_BreakTransfer.Controls.Add(this.btnBreakTransferSpeedSetting);
            this.tp_BreakTransfer.Controls.Add(this.btnBreakTransferAllSetting);
            this.tp_BreakTransfer.Controls.Add(this.btnBreakTransferLocationSetting);
            this.tp_BreakTransfer.Controls.Add(this.btnBreakTransferMoveLocation);
            this.tp_BreakTransfer.Controls.Add(this.txtBreakTransferLocation);
            this.tp_BreakTransfer.Controls.Add(this.txtBreakTransferSpeed);
            this.tp_BreakTransfer.Controls.Add(this.txtBreakTransferCurrentLocation);
            this.tp_BreakTransfer.Controls.Add(this.txtBreakTransferSelectedShaft);
            this.tp_BreakTransfer.Controls.Add(this.lbl_BreakTransfer_Location);
            this.tp_BreakTransfer.Controls.Add(this.lbl_BreakTransfer_Speed);
            this.tp_BreakTransfer.Controls.Add(this.lbl_BreakTransfer_SelectedShaft);
            this.tp_BreakTransfer.Controls.Add(this.txtBreakTransfer_LocationSetting);
            this.tp_BreakTransfer.Controls.Add(this.lbl_BreakTransfer_CurrentLocation);
            this.tp_BreakTransfer.Controls.Add(this.btnBreakTransferGrabSwitch3);
            this.tp_BreakTransfer.Controls.Add(this.btnBreakTransferGrabSwitch2);
            this.tp_BreakTransfer.Controls.Add(this.btnBreakTransferGrabSwitch1);
            this.tp_BreakTransfer.Controls.Add(this.lvBreakTransfer);
            this.tp_BreakTransfer.Controls.Add(this.ajin_Setting_BreakTransfer);
            this.tp_BreakTransfer.Location = new System.Drawing.Point(4, 34);
            this.tp_BreakTransfer.Name = "tp_BreakTransfer";
            this.tp_BreakTransfer.Size = new System.Drawing.Size(1726, 816);
            this.tp_BreakTransfer.TabIndex = 4;
            this.tp_BreakTransfer.Text = "Break 트랜스퍼";
            // 
            // gxtBreakTransfer_Move
            // 
            this.gxtBreakTransfer_Move.Controls.Add(this.gxtBreakTransfer_MoveB);
            this.gxtBreakTransfer_Move.Controls.Add(this.gxtBreakTransfer_MoveA);
            this.gxtBreakTransfer_Move.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtBreakTransfer_Move.ForeColor = System.Drawing.Color.White;
            this.gxtBreakTransfer_Move.Location = new System.Drawing.Point(792, 595);
            this.gxtBreakTransfer_Move.Name = "gxtBreakTransfer_Move";
            this.gxtBreakTransfer_Move.Size = new System.Drawing.Size(904, 179);
            this.gxtBreakTransfer_Move.TabIndex = 53;
            this.gxtBreakTransfer_Move.TabStop = false;
            this.gxtBreakTransfer_Move.Text = "     이동     ";
            // 
            // gxtBreakTransfer_MoveB
            // 
            this.gxtBreakTransfer_MoveB.Controls.Add(this.tableLayoutPanel44);
            this.gxtBreakTransfer_MoveB.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtBreakTransfer_MoveB.ForeColor = System.Drawing.Color.White;
            this.gxtBreakTransfer_MoveB.Location = new System.Drawing.Point(455, 28);
            this.gxtBreakTransfer_MoveB.Name = "gxtBreakTransfer_MoveB";
            this.gxtBreakTransfer_MoveB.Size = new System.Drawing.Size(443, 145);
            this.gxtBreakTransfer_MoveB.TabIndex = 31;
            this.gxtBreakTransfer_MoveB.TabStop = false;
            this.gxtBreakTransfer_MoveB.Text = "     B    ";
            // 
            // tableLayoutPanel44
            // 
            this.tableLayoutPanel44.ColumnCount = 2;
            this.tableLayoutPanel44.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel44.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel44.Controls.Add(this.btnBreakTransferMoveBUnload, 0, 0);
            this.tableLayoutPanel44.Controls.Add(this.btnBreakTransferMoveBLoad, 0, 0);
            this.tableLayoutPanel44.Location = new System.Drawing.Point(6, 27);
            this.tableLayoutPanel44.Name = "tableLayoutPanel44";
            this.tableLayoutPanel44.RowCount = 1;
            this.tableLayoutPanel44.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel44.Size = new System.Drawing.Size(430, 112);
            this.tableLayoutPanel44.TabIndex = 55;
            // 
            // btnBreakTransferMoveBUnload
            // 
            this.btnBreakTransferMoveBUnload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakTransferMoveBUnload.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btnBreakTransferMoveBUnload.ForeColor = System.Drawing.Color.White;
            this.btnBreakTransferMoveBUnload.Location = new System.Drawing.Point(218, 3);
            this.btnBreakTransferMoveBUnload.Name = "btnBreakTransferMoveBUnload";
            this.btnBreakTransferMoveBUnload.Size = new System.Drawing.Size(209, 106);
            this.btnBreakTransferMoveBUnload.TabIndex = 67;
            this.btnBreakTransferMoveBUnload.Text = "언로드";
            this.btnBreakTransferMoveBUnload.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferMoveBLoad
            // 
            this.btnBreakTransferMoveBLoad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakTransferMoveBLoad.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btnBreakTransferMoveBLoad.ForeColor = System.Drawing.Color.White;
            this.btnBreakTransferMoveBLoad.Location = new System.Drawing.Point(3, 3);
            this.btnBreakTransferMoveBLoad.Name = "btnBreakTransferMoveBLoad";
            this.btnBreakTransferMoveBLoad.Size = new System.Drawing.Size(209, 106);
            this.btnBreakTransferMoveBLoad.TabIndex = 66;
            this.btnBreakTransferMoveBLoad.Text = "로드";
            this.btnBreakTransferMoveBLoad.UseVisualStyleBackColor = false;
            // 
            // gxtBreakTransfer_MoveA
            // 
            this.gxtBreakTransfer_MoveA.Controls.Add(this.tableLayoutPanel43);
            this.gxtBreakTransfer_MoveA.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtBreakTransfer_MoveA.ForeColor = System.Drawing.Color.White;
            this.gxtBreakTransfer_MoveA.Location = new System.Drawing.Point(6, 28);
            this.gxtBreakTransfer_MoveA.Name = "gxtBreakTransfer_MoveA";
            this.gxtBreakTransfer_MoveA.Size = new System.Drawing.Size(443, 145);
            this.gxtBreakTransfer_MoveA.TabIndex = 30;
            this.gxtBreakTransfer_MoveA.TabStop = false;
            this.gxtBreakTransfer_MoveA.Text = "     A    ";
            // 
            // tableLayoutPanel43
            // 
            this.tableLayoutPanel43.ColumnCount = 2;
            this.tableLayoutPanel43.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel43.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel43.Controls.Add(this.btnBreakTransferMoveAUnload, 0, 0);
            this.tableLayoutPanel43.Controls.Add(this.btnBreakTransferMoveALoad, 0, 0);
            this.tableLayoutPanel43.Location = new System.Drawing.Point(7, 27);
            this.tableLayoutPanel43.Name = "tableLayoutPanel43";
            this.tableLayoutPanel43.RowCount = 1;
            this.tableLayoutPanel43.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel43.Size = new System.Drawing.Size(430, 112);
            this.tableLayoutPanel43.TabIndex = 54;
            // 
            // btnBreakTransferMoveAUnload
            // 
            this.btnBreakTransferMoveAUnload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakTransferMoveAUnload.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btnBreakTransferMoveAUnload.ForeColor = System.Drawing.Color.White;
            this.btnBreakTransferMoveAUnload.Location = new System.Drawing.Point(218, 3);
            this.btnBreakTransferMoveAUnload.Name = "btnBreakTransferMoveAUnload";
            this.btnBreakTransferMoveAUnload.Size = new System.Drawing.Size(209, 106);
            this.btnBreakTransferMoveAUnload.TabIndex = 67;
            this.btnBreakTransferMoveAUnload.Text = "언로드";
            this.btnBreakTransferMoveAUnload.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferMoveALoad
            // 
            this.btnBreakTransferMoveALoad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakTransferMoveALoad.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btnBreakTransferMoveALoad.ForeColor = System.Drawing.Color.White;
            this.btnBreakTransferMoveALoad.Location = new System.Drawing.Point(3, 3);
            this.btnBreakTransferMoveALoad.Name = "btnBreakTransferMoveALoad";
            this.btnBreakTransferMoveALoad.Size = new System.Drawing.Size(209, 106);
            this.btnBreakTransferMoveALoad.TabIndex = 66;
            this.btnBreakTransferMoveALoad.Text = "로드";
            this.btnBreakTransferMoveALoad.UseVisualStyleBackColor = false;
            // 
            // gxtBreakTransfer_B
            // 
            this.gxtBreakTransfer_B.Controls.Add(this.tableLayoutPanel42);
            this.gxtBreakTransfer_B.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtBreakTransfer_B.ForeColor = System.Drawing.Color.White;
            this.gxtBreakTransfer_B.Location = new System.Drawing.Point(1247, 410);
            this.gxtBreakTransfer_B.Name = "gxtBreakTransfer_B";
            this.gxtBreakTransfer_B.Size = new System.Drawing.Size(449, 179);
            this.gxtBreakTransfer_B.TabIndex = 52;
            this.gxtBreakTransfer_B.TabStop = false;
            this.gxtBreakTransfer_B.Text = "     B     ";
            // 
            // tableLayoutPanel42
            // 
            this.tableLayoutPanel42.ColumnCount = 2;
            this.tableLayoutPanel42.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel42.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel42.Controls.Add(this.btnBreakTransferBPickerDestructionOn, 0, 2);
            this.tableLayoutPanel42.Controls.Add(this.btnBreakTransferBPickerDestructionOff, 0, 2);
            this.tableLayoutPanel42.Controls.Add(this.btnBreakTransferBPickerUp, 0, 0);
            this.tableLayoutPanel42.Controls.Add(this.btnBreakTransferBPickerDown, 1, 0);
            this.tableLayoutPanel42.Controls.Add(this.btnBreakTransferBPickerPneumaticOn, 0, 1);
            this.tableLayoutPanel42.Controls.Add(this.btnBreakTransferBPneumaticOff, 1, 1);
            this.tableLayoutPanel42.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel42.Name = "tableLayoutPanel42";
            this.tableLayoutPanel42.RowCount = 3;
            this.tableLayoutPanel42.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel42.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel42.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel42.Size = new System.Drawing.Size(437, 145);
            this.tableLayoutPanel42.TabIndex = 55;
            // 
            // btnBreakTransferBPickerDestructionOn
            // 
            this.btnBreakTransferBPickerDestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakTransferBPickerDestructionOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakTransferBPickerDestructionOn.ForeColor = System.Drawing.Color.White;
            this.btnBreakTransferBPickerDestructionOn.Location = new System.Drawing.Point(3, 99);
            this.btnBreakTransferBPickerDestructionOn.Name = "btnBreakTransferBPickerDestructionOn";
            this.btnBreakTransferBPickerDestructionOn.Size = new System.Drawing.Size(212, 42);
            this.btnBreakTransferBPickerDestructionOn.TabIndex = 65;
            this.btnBreakTransferBPickerDestructionOn.Text = "피커\r\n파기 온";
            this.btnBreakTransferBPickerDestructionOn.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferBPickerDestructionOff
            // 
            this.btnBreakTransferBPickerDestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakTransferBPickerDestructionOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakTransferBPickerDestructionOff.ForeColor = System.Drawing.Color.White;
            this.btnBreakTransferBPickerDestructionOff.Location = new System.Drawing.Point(221, 99);
            this.btnBreakTransferBPickerDestructionOff.Name = "btnBreakTransferBPickerDestructionOff";
            this.btnBreakTransferBPickerDestructionOff.Size = new System.Drawing.Size(212, 42);
            this.btnBreakTransferBPickerDestructionOff.TabIndex = 64;
            this.btnBreakTransferBPickerDestructionOff.Text = "피커\r\n파기 오프";
            this.btnBreakTransferBPickerDestructionOff.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferBPickerUp
            // 
            this.btnBreakTransferBPickerUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakTransferBPickerUp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakTransferBPickerUp.ForeColor = System.Drawing.Color.White;
            this.btnBreakTransferBPickerUp.Location = new System.Drawing.Point(3, 3);
            this.btnBreakTransferBPickerUp.Name = "btnBreakTransferBPickerUp";
            this.btnBreakTransferBPickerUp.Size = new System.Drawing.Size(212, 42);
            this.btnBreakTransferBPickerUp.TabIndex = 60;
            this.btnBreakTransferBPickerUp.Text = "피커\r\n업";
            this.btnBreakTransferBPickerUp.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferBPickerDown
            // 
            this.btnBreakTransferBPickerDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakTransferBPickerDown.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakTransferBPickerDown.ForeColor = System.Drawing.Color.White;
            this.btnBreakTransferBPickerDown.Location = new System.Drawing.Point(221, 3);
            this.btnBreakTransferBPickerDown.Name = "btnBreakTransferBPickerDown";
            this.btnBreakTransferBPickerDown.Size = new System.Drawing.Size(212, 42);
            this.btnBreakTransferBPickerDown.TabIndex = 61;
            this.btnBreakTransferBPickerDown.Text = "피커\r\n다운";
            this.btnBreakTransferBPickerDown.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferBPickerPneumaticOn
            // 
            this.btnBreakTransferBPickerPneumaticOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakTransferBPickerPneumaticOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakTransferBPickerPneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btnBreakTransferBPickerPneumaticOn.Location = new System.Drawing.Point(3, 51);
            this.btnBreakTransferBPickerPneumaticOn.Name = "btnBreakTransferBPickerPneumaticOn";
            this.btnBreakTransferBPickerPneumaticOn.Size = new System.Drawing.Size(212, 42);
            this.btnBreakTransferBPickerPneumaticOn.TabIndex = 62;
            this.btnBreakTransferBPickerPneumaticOn.Text = "피커\r\n공압 온";
            this.btnBreakTransferBPickerPneumaticOn.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferBPneumaticOff
            // 
            this.btnBreakTransferBPneumaticOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakTransferBPneumaticOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakTransferBPneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btnBreakTransferBPneumaticOff.Location = new System.Drawing.Point(221, 51);
            this.btnBreakTransferBPneumaticOff.Name = "btnBreakTransferBPneumaticOff";
            this.btnBreakTransferBPneumaticOff.Size = new System.Drawing.Size(212, 42);
            this.btnBreakTransferBPneumaticOff.TabIndex = 63;
            this.btnBreakTransferBPneumaticOff.Text = "피커\r\n공압 오프";
            this.btnBreakTransferBPneumaticOff.UseVisualStyleBackColor = false;
            // 
            // gxtBreakTransfer_A
            // 
            this.gxtBreakTransfer_A.Controls.Add(this.tableLayoutPanel41);
            this.gxtBreakTransfer_A.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtBreakTransfer_A.ForeColor = System.Drawing.Color.White;
            this.gxtBreakTransfer_A.Location = new System.Drawing.Point(792, 410);
            this.gxtBreakTransfer_A.Name = "gxtBreakTransfer_A";
            this.gxtBreakTransfer_A.Size = new System.Drawing.Size(449, 179);
            this.gxtBreakTransfer_A.TabIndex = 51;
            this.gxtBreakTransfer_A.TabStop = false;
            this.gxtBreakTransfer_A.Text = "     A    ";
            // 
            // tableLayoutPanel41
            // 
            this.tableLayoutPanel41.ColumnCount = 2;
            this.tableLayoutPanel41.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel41.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel41.Controls.Add(this.btnBreakTransferAPickerDestructionOn, 0, 2);
            this.tableLayoutPanel41.Controls.Add(this.btnBreakTransferAPickerDestructionOff, 0, 2);
            this.tableLayoutPanel41.Controls.Add(this.btnBreakTransferAPickerUp, 0, 0);
            this.tableLayoutPanel41.Controls.Add(this.btnBreakTransferAPickerDown, 1, 0);
            this.tableLayoutPanel41.Controls.Add(this.btnBreakTransferAPickerPneumaticOn, 0, 1);
            this.tableLayoutPanel41.Controls.Add(this.btnBreakTransferAPickerPneumaticOff, 1, 1);
            this.tableLayoutPanel41.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel41.Name = "tableLayoutPanel41";
            this.tableLayoutPanel41.RowCount = 3;
            this.tableLayoutPanel41.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel41.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel41.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel41.Size = new System.Drawing.Size(437, 145);
            this.tableLayoutPanel41.TabIndex = 54;
            // 
            // btnBreakTransferAPickerDestructionOn
            // 
            this.btnBreakTransferAPickerDestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakTransferAPickerDestructionOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakTransferAPickerDestructionOn.ForeColor = System.Drawing.Color.White;
            this.btnBreakTransferAPickerDestructionOn.Location = new System.Drawing.Point(3, 99);
            this.btnBreakTransferAPickerDestructionOn.Name = "btnBreakTransferAPickerDestructionOn";
            this.btnBreakTransferAPickerDestructionOn.Size = new System.Drawing.Size(212, 42);
            this.btnBreakTransferAPickerDestructionOn.TabIndex = 65;
            this.btnBreakTransferAPickerDestructionOn.Text = "피커\r\n파기 온";
            this.btnBreakTransferAPickerDestructionOn.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferAPickerDestructionOff
            // 
            this.btnBreakTransferAPickerDestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakTransferAPickerDestructionOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakTransferAPickerDestructionOff.ForeColor = System.Drawing.Color.White;
            this.btnBreakTransferAPickerDestructionOff.Location = new System.Drawing.Point(221, 99);
            this.btnBreakTransferAPickerDestructionOff.Name = "btnBreakTransferAPickerDestructionOff";
            this.btnBreakTransferAPickerDestructionOff.Size = new System.Drawing.Size(212, 42);
            this.btnBreakTransferAPickerDestructionOff.TabIndex = 64;
            this.btnBreakTransferAPickerDestructionOff.Text = "피커\r\n파기 오프";
            this.btnBreakTransferAPickerDestructionOff.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferAPickerUp
            // 
            this.btnBreakTransferAPickerUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakTransferAPickerUp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakTransferAPickerUp.ForeColor = System.Drawing.Color.White;
            this.btnBreakTransferAPickerUp.Location = new System.Drawing.Point(3, 3);
            this.btnBreakTransferAPickerUp.Name = "btnBreakTransferAPickerUp";
            this.btnBreakTransferAPickerUp.Size = new System.Drawing.Size(212, 42);
            this.btnBreakTransferAPickerUp.TabIndex = 60;
            this.btnBreakTransferAPickerUp.Text = "피커\r\n업";
            this.btnBreakTransferAPickerUp.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferAPickerDown
            // 
            this.btnBreakTransferAPickerDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakTransferAPickerDown.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakTransferAPickerDown.ForeColor = System.Drawing.Color.White;
            this.btnBreakTransferAPickerDown.Location = new System.Drawing.Point(221, 3);
            this.btnBreakTransferAPickerDown.Name = "btnBreakTransferAPickerDown";
            this.btnBreakTransferAPickerDown.Size = new System.Drawing.Size(212, 42);
            this.btnBreakTransferAPickerDown.TabIndex = 61;
            this.btnBreakTransferAPickerDown.Text = "피커\r\n다운";
            this.btnBreakTransferAPickerDown.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferAPickerPneumaticOn
            // 
            this.btnBreakTransferAPickerPneumaticOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakTransferAPickerPneumaticOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakTransferAPickerPneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btnBreakTransferAPickerPneumaticOn.Location = new System.Drawing.Point(3, 51);
            this.btnBreakTransferAPickerPneumaticOn.Name = "btnBreakTransferAPickerPneumaticOn";
            this.btnBreakTransferAPickerPneumaticOn.Size = new System.Drawing.Size(212, 42);
            this.btnBreakTransferAPickerPneumaticOn.TabIndex = 62;
            this.btnBreakTransferAPickerPneumaticOn.Text = "피커\r\n공압 온";
            this.btnBreakTransferAPickerPneumaticOn.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferAPickerPneumaticOff
            // 
            this.btnBreakTransferAPickerPneumaticOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakTransferAPickerPneumaticOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakTransferAPickerPneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btnBreakTransferAPickerPneumaticOff.Location = new System.Drawing.Point(221, 51);
            this.btnBreakTransferAPickerPneumaticOff.Name = "btnBreakTransferAPickerPneumaticOff";
            this.btnBreakTransferAPickerPneumaticOff.Size = new System.Drawing.Size(212, 42);
            this.btnBreakTransferAPickerPneumaticOff.TabIndex = 63;
            this.btnBreakTransferAPickerPneumaticOff.Text = "피커\r\n공압 오프";
            this.btnBreakTransferAPickerPneumaticOff.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferSave
            // 
            this.btnBreakTransferSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakTransferSave.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakTransferSave.ForeColor = System.Drawing.Color.White;
            this.btnBreakTransferSave.Location = new System.Drawing.Point(1482, 783);
            this.btnBreakTransferSave.Name = "btnBreakTransferSave";
            this.btnBreakTransferSave.Size = new System.Drawing.Size(228, 28);
            this.btnBreakTransferSave.TabIndex = 50;
            this.btnBreakTransferSave.Text = "저장";
            this.btnBreakTransferSave.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferSpeedSetting
            // 
            this.btnBreakTransferSpeedSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakTransferSpeedSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnBreakTransferSpeedSetting.ForeColor = System.Drawing.Color.White;
            this.btnBreakTransferSpeedSetting.Location = new System.Drawing.Point(1291, 315);
            this.btnBreakTransferSpeedSetting.Name = "btnBreakTransferSpeedSetting";
            this.btnBreakTransferSpeedSetting.Size = new System.Drawing.Size(90, 27);
            this.btnBreakTransferSpeedSetting.TabIndex = 48;
            this.btnBreakTransferSpeedSetting.Text = "속도 설정";
            this.btnBreakTransferSpeedSetting.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferAllSetting
            // 
            this.btnBreakTransferAllSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakTransferAllSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnBreakTransferAllSetting.ForeColor = System.Drawing.Color.White;
            this.btnBreakTransferAllSetting.Location = new System.Drawing.Point(1387, 273);
            this.btnBreakTransferAllSetting.Name = "btnBreakTransferAllSetting";
            this.btnBreakTransferAllSetting.Size = new System.Drawing.Size(90, 27);
            this.btnBreakTransferAllSetting.TabIndex = 47;
            this.btnBreakTransferAllSetting.Text = "전체 설정";
            this.btnBreakTransferAllSetting.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferLocationSetting
            // 
            this.btnBreakTransferLocationSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakTransferLocationSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnBreakTransferLocationSetting.ForeColor = System.Drawing.Color.White;
            this.btnBreakTransferLocationSetting.Location = new System.Drawing.Point(1071, 315);
            this.btnBreakTransferLocationSetting.Name = "btnBreakTransferLocationSetting";
            this.btnBreakTransferLocationSetting.Size = new System.Drawing.Size(90, 27);
            this.btnBreakTransferLocationSetting.TabIndex = 46;
            this.btnBreakTransferLocationSetting.Text = "위치 설정";
            this.btnBreakTransferLocationSetting.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferMoveLocation
            // 
            this.btnBreakTransferMoveLocation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakTransferMoveLocation.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnBreakTransferMoveLocation.ForeColor = System.Drawing.Color.White;
            this.btnBreakTransferMoveLocation.Location = new System.Drawing.Point(975, 315);
            this.btnBreakTransferMoveLocation.Name = "btnBreakTransferMoveLocation";
            this.btnBreakTransferMoveLocation.Size = new System.Drawing.Size(90, 27);
            this.btnBreakTransferMoveLocation.TabIndex = 45;
            this.btnBreakTransferMoveLocation.Text = "위치 이동";
            this.btnBreakTransferMoveLocation.UseVisualStyleBackColor = false;
            // 
            // txtBreakTransferLocation
            // 
            this.txtBreakTransferLocation.Font = new System.Drawing.Font("굴림", 13F);
            this.txtBreakTransferLocation.Location = new System.Drawing.Point(879, 316);
            this.txtBreakTransferLocation.Name = "txtBreakTransferLocation";
            this.txtBreakTransferLocation.Size = new System.Drawing.Size(90, 27);
            this.txtBreakTransferLocation.TabIndex = 44;
            // 
            // txtBreakTransferSpeed
            // 
            this.txtBreakTransferSpeed.Font = new System.Drawing.Font("굴림", 13F);
            this.txtBreakTransferSpeed.Location = new System.Drawing.Point(1291, 273);
            this.txtBreakTransferSpeed.Name = "txtBreakTransferSpeed";
            this.txtBreakTransferSpeed.Size = new System.Drawing.Size(90, 27);
            this.txtBreakTransferSpeed.TabIndex = 43;
            // 
            // txtBreakTransferCurrentLocation
            // 
            this.txtBreakTransferCurrentLocation.Font = new System.Drawing.Font("굴림", 13F);
            this.txtBreakTransferCurrentLocation.Location = new System.Drawing.Point(1084, 274);
            this.txtBreakTransferCurrentLocation.Name = "txtBreakTransferCurrentLocation";
            this.txtBreakTransferCurrentLocation.Size = new System.Drawing.Size(90, 27);
            this.txtBreakTransferCurrentLocation.TabIndex = 42;
            // 
            // txtBreakTransferSelectedShaft
            // 
            this.txtBreakTransferSelectedShaft.Font = new System.Drawing.Font("굴림", 13F);
            this.txtBreakTransferSelectedShaft.Location = new System.Drawing.Point(879, 273);
            this.txtBreakTransferSelectedShaft.Name = "txtBreakTransferSelectedShaft";
            this.txtBreakTransferSelectedShaft.Size = new System.Drawing.Size(90, 27);
            this.txtBreakTransferSelectedShaft.TabIndex = 41;
            // 
            // lbl_BreakTransfer_Location
            // 
            this.lbl_BreakTransfer_Location.AutoSize = true;
            this.lbl_BreakTransfer_Location.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_BreakTransfer_Location.ForeColor = System.Drawing.Color.White;
            this.lbl_BreakTransfer_Location.Location = new System.Drawing.Point(801, 316);
            this.lbl_BreakTransfer_Location.Name = "lbl_BreakTransfer_Location";
            this.lbl_BreakTransfer_Location.Size = new System.Drawing.Size(84, 21);
            this.lbl_BreakTransfer_Location.TabIndex = 40;
            this.lbl_BreakTransfer_Location.Text = "위치[mm]";
            // 
            // lbl_BreakTransfer_Speed
            // 
            this.lbl_BreakTransfer_Speed.AutoSize = true;
            this.lbl_BreakTransfer_Speed.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_BreakTransfer_Speed.ForeColor = System.Drawing.Color.White;
            this.lbl_BreakTransfer_Speed.Location = new System.Drawing.Point(1179, 275);
            this.lbl_BreakTransfer_Speed.Name = "lbl_BreakTransfer_Speed";
            this.lbl_BreakTransfer_Speed.Size = new System.Drawing.Size(115, 21);
            this.lbl_BreakTransfer_Speed.TabIndex = 39;
            this.lbl_BreakTransfer_Speed.Text = "속도[mm/sec]";
            // 
            // lbl_BreakTransfer_SelectedShaft
            // 
            this.lbl_BreakTransfer_SelectedShaft.AutoSize = true;
            this.lbl_BreakTransfer_SelectedShaft.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_BreakTransfer_SelectedShaft.ForeColor = System.Drawing.Color.White;
            this.lbl_BreakTransfer_SelectedShaft.Location = new System.Drawing.Point(805, 274);
            this.lbl_BreakTransfer_SelectedShaft.Name = "lbl_BreakTransfer_SelectedShaft";
            this.lbl_BreakTransfer_SelectedShaft.Size = new System.Drawing.Size(80, 21);
            this.lbl_BreakTransfer_SelectedShaft.TabIndex = 38;
            this.lbl_BreakTransfer_SelectedShaft.Text = "선택된 축";
            // 
            // txtBreakTransfer_LocationSetting
            // 
            this.txtBreakTransfer_LocationSetting.BackColor = System.Drawing.SystemColors.Highlight;
            this.txtBreakTransfer_LocationSetting.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.txtBreakTransfer_LocationSetting.ForeColor = System.Drawing.Color.White;
            this.txtBreakTransfer_LocationSetting.Location = new System.Drawing.Point(792, 223);
            this.txtBreakTransfer_LocationSetting.Name = "txtBreakTransfer_LocationSetting";
            this.txtBreakTransfer_LocationSetting.ReadOnly = true;
            this.txtBreakTransfer_LocationSetting.Size = new System.Drawing.Size(89, 27);
            this.txtBreakTransfer_LocationSetting.TabIndex = 37;
            this.txtBreakTransfer_LocationSetting.Text = "위치값 설정";
            this.txtBreakTransfer_LocationSetting.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbl_BreakTransfer_CurrentLocation
            // 
            this.lbl_BreakTransfer_CurrentLocation.AutoSize = true;
            this.lbl_BreakTransfer_CurrentLocation.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_BreakTransfer_CurrentLocation.ForeColor = System.Drawing.Color.White;
            this.lbl_BreakTransfer_CurrentLocation.Location = new System.Drawing.Point(975, 274);
            this.lbl_BreakTransfer_CurrentLocation.Name = "lbl_BreakTransfer_CurrentLocation";
            this.lbl_BreakTransfer_CurrentLocation.Size = new System.Drawing.Size(116, 21);
            this.lbl_BreakTransfer_CurrentLocation.TabIndex = 36;
            this.lbl_BreakTransfer_CurrentLocation.Text = "현재위치[mm]";
            // 
            // btnBreakTransferGrabSwitch3
            // 
            this.btnBreakTransferGrabSwitch3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakTransferGrabSwitch3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakTransferGrabSwitch3.ForeColor = System.Drawing.Color.White;
            this.btnBreakTransferGrabSwitch3.Location = new System.Drawing.Point(1514, 6);
            this.btnBreakTransferGrabSwitch3.Name = "btnBreakTransferGrabSwitch3";
            this.btnBreakTransferGrabSwitch3.Size = new System.Drawing.Size(172, 23);
            this.btnBreakTransferGrabSwitch3.TabIndex = 35;
            this.btnBreakTransferGrabSwitch3.Text = "Grab Switch 3";
            this.btnBreakTransferGrabSwitch3.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferGrabSwitch2
            // 
            this.btnBreakTransferGrabSwitch2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakTransferGrabSwitch2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakTransferGrabSwitch2.ForeColor = System.Drawing.Color.White;
            this.btnBreakTransferGrabSwitch2.Location = new System.Drawing.Point(1336, 6);
            this.btnBreakTransferGrabSwitch2.Name = "btnBreakTransferGrabSwitch2";
            this.btnBreakTransferGrabSwitch2.Size = new System.Drawing.Size(172, 23);
            this.btnBreakTransferGrabSwitch2.TabIndex = 34;
            this.btnBreakTransferGrabSwitch2.Text = "Grab Switch 2";
            this.btnBreakTransferGrabSwitch2.UseVisualStyleBackColor = false;
            // 
            // btnBreakTransferGrabSwitch1
            // 
            this.btnBreakTransferGrabSwitch1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakTransferGrabSwitch1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakTransferGrabSwitch1.ForeColor = System.Drawing.Color.White;
            this.btnBreakTransferGrabSwitch1.Location = new System.Drawing.Point(1158, 6);
            this.btnBreakTransferGrabSwitch1.Name = "btnBreakTransferGrabSwitch1";
            this.btnBreakTransferGrabSwitch1.Size = new System.Drawing.Size(172, 23);
            this.btnBreakTransferGrabSwitch1.TabIndex = 33;
            this.btnBreakTransferGrabSwitch1.Text = "Grab Switch 1";
            this.btnBreakTransferGrabSwitch1.UseVisualStyleBackColor = false;
            // 
            // lvBreakTransfer
            // 
            this.lvBreakTransfer.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.col_BreakTransfer_NamebyLocation,
            this.col_BreakTransfer_Shaft,
            this.col_BreakTransfer_Location,
            this.col_BreakTransfer_Speed});
            this.lvBreakTransfer.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lvBreakTransfer.GridLines = true;
            this.lvBreakTransfer.Location = new System.Drawing.Point(17, 16);
            this.lvBreakTransfer.Name = "lvBreakTransfer";
            this.lvBreakTransfer.Size = new System.Drawing.Size(758, 734);
            this.lvBreakTransfer.TabIndex = 32;
            this.lvBreakTransfer.UseCompatibleStateImageBehavior = false;
            this.lvBreakTransfer.View = System.Windows.Forms.View.Details;
            // 
            // col_BreakTransfer_NamebyLocation
            // 
            this.col_BreakTransfer_NamebyLocation.Text = "위치별 명칭";
            this.col_BreakTransfer_NamebyLocation.Width = 430;
            // 
            // col_BreakTransfer_Shaft
            // 
            this.col_BreakTransfer_Shaft.Text = "축";
            this.col_BreakTransfer_Shaft.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // col_BreakTransfer_Location
            // 
            this.col_BreakTransfer_Location.Text = "위치[mm]";
            this.col_BreakTransfer_Location.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_BreakTransfer_Location.Width = 125;
            // 
            // col_BreakTransfer_Speed
            // 
            this.col_BreakTransfer_Speed.Text = "속도[mm/s]";
            this.col_BreakTransfer_Speed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_BreakTransfer_Speed.Width = 125;
            // 
            // ajin_Setting_BreakTransfer
            // 
            this.ajin_Setting_BreakTransfer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_BreakTransfer.Location = new System.Drawing.Point(792, 34);
            this.ajin_Setting_BreakTransfer.Name = "ajin_Setting_BreakTransfer";
            this.ajin_Setting_BreakTransfer.Servo = null;
            this.ajin_Setting_BreakTransfer.Size = new System.Drawing.Size(903, 183);
            this.ajin_Setting_BreakTransfer.TabIndex = 31;
            // 
            // tp_BreakUnitXZ
            // 
            this.tp_BreakUnitXZ.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tp_BreakUnitXZ.Controls.Add(this.gxtBreakUnitXZ_Move);
            this.tp_BreakUnitXZ.Controls.Add(this.gxtBreakUnitXZSave);
            this.tp_BreakUnitXZ.Controls.Add(this.btnBreakUnitXZSpeedSetting);
            this.tp_BreakUnitXZ.Controls.Add(this.btnBreakUnitXZAllSetting);
            this.tp_BreakUnitXZ.Controls.Add(this.btnBreakUnitXZLocationSetting);
            this.tp_BreakUnitXZ.Controls.Add(this.btnBreakUnitXZMoveLocation);
            this.tp_BreakUnitXZ.Controls.Add(this.txtBreakUnitXZLocation);
            this.tp_BreakUnitXZ.Controls.Add(this.txtBreakUnitXZSpeed);
            this.tp_BreakUnitXZ.Controls.Add(this.txtBreakUnitXZCurrentLocation);
            this.tp_BreakUnitXZ.Controls.Add(this.txtBreakUnitXZSelectedShaft);
            this.tp_BreakUnitXZ.Controls.Add(this.lbl_BreakUnitXZ_Location);
            this.tp_BreakUnitXZ.Controls.Add(this.lbl_BreakUnitXZ_Speed);
            this.tp_BreakUnitXZ.Controls.Add(this.lbl_BreakUnitXZ_SelectedShaft);
            this.tp_BreakUnitXZ.Controls.Add(this.txtBreakUnitXZ_LocationSetting);
            this.tp_BreakUnitXZ.Controls.Add(this.lbl_BreakUnitXZ_CurrentLocation);
            this.tp_BreakUnitXZ.Controls.Add(this.btnBreakUnitXZGrabSwitch3);
            this.tp_BreakUnitXZ.Controls.Add(this.btnBreakUnitXZGrabSwitch2);
            this.tp_BreakUnitXZ.Controls.Add(this.btnBreakUnitXZGrabSwitch1);
            this.tp_BreakUnitXZ.Controls.Add(this.lvBreakUnitXZ);
            this.tp_BreakUnitXZ.Controls.Add(this.ajin_Setting_BreakUnitXZ);
            this.tp_BreakUnitXZ.Location = new System.Drawing.Point(4, 34);
            this.tp_BreakUnitXZ.Name = "tp_BreakUnitXZ";
            this.tp_BreakUnitXZ.Size = new System.Drawing.Size(1726, 816);
            this.tp_BreakUnitXZ.TabIndex = 5;
            this.tp_BreakUnitXZ.Text = "Break 유닛(X축 Z축)";
            // 
            // gxtBreakUnitXZ_Move
            // 
            this.gxtBreakUnitXZ_Move.Controls.Add(this.tableLayoutPanel45);
            this.gxtBreakUnitXZ_Move.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtBreakUnitXZ_Move.ForeColor = System.Drawing.Color.White;
            this.gxtBreakUnitXZ_Move.Location = new System.Drawing.Point(792, 595);
            this.gxtBreakUnitXZ_Move.Name = "gxtBreakUnitXZ_Move";
            this.gxtBreakUnitXZ_Move.Size = new System.Drawing.Size(904, 179);
            this.gxtBreakUnitXZ_Move.TabIndex = 53;
            this.gxtBreakUnitXZ_Move.TabStop = false;
            this.gxtBreakUnitXZ_Move.Text = "     이동     ";
            // 
            // tableLayoutPanel45
            // 
            this.tableLayoutPanel45.ColumnCount = 4;
            this.tableLayoutPanel45.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel45.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel45.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel45.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel45.Controls.Add(this.gxtBreakUnitXZ_MoveZ4, 3, 0);
            this.tableLayoutPanel45.Controls.Add(this.gxtBreakUnitXZ_MoveZ3, 2, 0);
            this.tableLayoutPanel45.Controls.Add(this.gxtBreakUnitXZ_MoveZ2, 1, 0);
            this.tableLayoutPanel45.Controls.Add(this.gxtBreakUnitXZ_MoveZ1, 0, 0);
            this.tableLayoutPanel45.Location = new System.Drawing.Point(6, 19);
            this.tableLayoutPanel45.Name = "tableLayoutPanel45";
            this.tableLayoutPanel45.RowCount = 1;
            this.tableLayoutPanel45.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel45.Size = new System.Drawing.Size(661, 154);
            this.tableLayoutPanel45.TabIndex = 54;
            // 
            // gxtBreakUnitXZ_MoveZ4
            // 
            this.gxtBreakUnitXZ_MoveZ4.Controls.Add(this.tableLayoutPanel49);
            this.gxtBreakUnitXZ_MoveZ4.ForeColor = System.Drawing.Color.White;
            this.gxtBreakUnitXZ_MoveZ4.Location = new System.Drawing.Point(498, 3);
            this.gxtBreakUnitXZ_MoveZ4.Name = "gxtBreakUnitXZ_MoveZ4";
            this.gxtBreakUnitXZ_MoveZ4.Size = new System.Drawing.Size(159, 148);
            this.gxtBreakUnitXZ_MoveZ4.TabIndex = 57;
            this.gxtBreakUnitXZ_MoveZ4.TabStop = false;
            this.gxtBreakUnitXZ_MoveZ4.Text = "     Z4     ";
            // 
            // tableLayoutPanel49
            // 
            this.tableLayoutPanel49.ColumnCount = 1;
            this.tableLayoutPanel49.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel49.Controls.Add(this.gxtBreakUnitXZMoveZ4SlowDescent, 0, 2);
            this.tableLayoutPanel49.Controls.Add(this.gxtBreakUnitXZMoveZ4VisionCheck, 0, 0);
            this.tableLayoutPanel49.Controls.Add(this.gxtBreakUnitXZMoveZ4FastDescent, 0, 1);
            this.tableLayoutPanel49.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel49.Name = "tableLayoutPanel49";
            this.tableLayoutPanel49.RowCount = 3;
            this.tableLayoutPanel49.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel49.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel49.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel49.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel49.Size = new System.Drawing.Size(149, 114);
            this.tableLayoutPanel49.TabIndex = 55;
            // 
            // gxtBreakUnitXZMoveZ4SlowDescent
            // 
            this.gxtBreakUnitXZMoveZ4SlowDescent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gxtBreakUnitXZMoveZ4SlowDescent.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.gxtBreakUnitXZMoveZ4SlowDescent.ForeColor = System.Drawing.Color.White;
            this.gxtBreakUnitXZMoveZ4SlowDescent.Location = new System.Drawing.Point(3, 79);
            this.gxtBreakUnitXZMoveZ4SlowDescent.Name = "gxtBreakUnitXZMoveZ4SlowDescent";
            this.gxtBreakUnitXZMoveZ4SlowDescent.Size = new System.Drawing.Size(143, 32);
            this.gxtBreakUnitXZMoveZ4SlowDescent.TabIndex = 63;
            this.gxtBreakUnitXZMoveZ4SlowDescent.Text = "저속 하강";
            this.gxtBreakUnitXZMoveZ4SlowDescent.UseVisualStyleBackColor = false;
            // 
            // gxtBreakUnitXZMoveZ4VisionCheck
            // 
            this.gxtBreakUnitXZMoveZ4VisionCheck.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gxtBreakUnitXZMoveZ4VisionCheck.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.gxtBreakUnitXZMoveZ4VisionCheck.ForeColor = System.Drawing.Color.White;
            this.gxtBreakUnitXZMoveZ4VisionCheck.Location = new System.Drawing.Point(3, 3);
            this.gxtBreakUnitXZMoveZ4VisionCheck.Name = "gxtBreakUnitXZMoveZ4VisionCheck";
            this.gxtBreakUnitXZMoveZ4VisionCheck.Size = new System.Drawing.Size(143, 31);
            this.gxtBreakUnitXZMoveZ4VisionCheck.TabIndex = 61;
            this.gxtBreakUnitXZMoveZ4VisionCheck.Text = "비젼 확인";
            this.gxtBreakUnitXZMoveZ4VisionCheck.UseVisualStyleBackColor = false;
            // 
            // gxtBreakUnitXZMoveZ4FastDescent
            // 
            this.gxtBreakUnitXZMoveZ4FastDescent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gxtBreakUnitXZMoveZ4FastDescent.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.gxtBreakUnitXZMoveZ4FastDescent.ForeColor = System.Drawing.Color.White;
            this.gxtBreakUnitXZMoveZ4FastDescent.Location = new System.Drawing.Point(3, 41);
            this.gxtBreakUnitXZMoveZ4FastDescent.Name = "gxtBreakUnitXZMoveZ4FastDescent";
            this.gxtBreakUnitXZMoveZ4FastDescent.Size = new System.Drawing.Size(143, 31);
            this.gxtBreakUnitXZMoveZ4FastDescent.TabIndex = 62;
            this.gxtBreakUnitXZMoveZ4FastDescent.Text = "고속 하강";
            this.gxtBreakUnitXZMoveZ4FastDescent.UseVisualStyleBackColor = false;
            // 
            // gxtBreakUnitXZ_MoveZ3
            // 
            this.gxtBreakUnitXZ_MoveZ3.Controls.Add(this.tableLayoutPanel48);
            this.gxtBreakUnitXZ_MoveZ3.ForeColor = System.Drawing.Color.White;
            this.gxtBreakUnitXZ_MoveZ3.Location = new System.Drawing.Point(333, 3);
            this.gxtBreakUnitXZ_MoveZ3.Name = "gxtBreakUnitXZ_MoveZ3";
            this.gxtBreakUnitXZ_MoveZ3.Size = new System.Drawing.Size(159, 148);
            this.gxtBreakUnitXZ_MoveZ3.TabIndex = 56;
            this.gxtBreakUnitXZ_MoveZ3.TabStop = false;
            this.gxtBreakUnitXZ_MoveZ3.Text = "     Z3     ";
            // 
            // tableLayoutPanel48
            // 
            this.tableLayoutPanel48.ColumnCount = 1;
            this.tableLayoutPanel48.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel48.Controls.Add(this.gxtBreakUnitXZMoveZ3SlowDescent, 0, 2);
            this.tableLayoutPanel48.Controls.Add(this.gxtBreakUnitXZMoveZ3VisionCheck, 0, 0);
            this.tableLayoutPanel48.Controls.Add(this.gxtBreakUnitXZMoveZ3FastDescent, 0, 1);
            this.tableLayoutPanel48.Location = new System.Drawing.Point(4, 28);
            this.tableLayoutPanel48.Name = "tableLayoutPanel48";
            this.tableLayoutPanel48.RowCount = 3;
            this.tableLayoutPanel48.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel48.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel48.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel48.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel48.Size = new System.Drawing.Size(149, 114);
            this.tableLayoutPanel48.TabIndex = 55;
            // 
            // gxtBreakUnitXZMoveZ3SlowDescent
            // 
            this.gxtBreakUnitXZMoveZ3SlowDescent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gxtBreakUnitXZMoveZ3SlowDescent.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.gxtBreakUnitXZMoveZ3SlowDescent.ForeColor = System.Drawing.Color.White;
            this.gxtBreakUnitXZMoveZ3SlowDescent.Location = new System.Drawing.Point(3, 79);
            this.gxtBreakUnitXZMoveZ3SlowDescent.Name = "gxtBreakUnitXZMoveZ3SlowDescent";
            this.gxtBreakUnitXZMoveZ3SlowDescent.Size = new System.Drawing.Size(143, 32);
            this.gxtBreakUnitXZMoveZ3SlowDescent.TabIndex = 63;
            this.gxtBreakUnitXZMoveZ3SlowDescent.Text = "저속 하강";
            this.gxtBreakUnitXZMoveZ3SlowDescent.UseVisualStyleBackColor = false;
            // 
            // gxtBreakUnitXZMoveZ3VisionCheck
            // 
            this.gxtBreakUnitXZMoveZ3VisionCheck.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gxtBreakUnitXZMoveZ3VisionCheck.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.gxtBreakUnitXZMoveZ3VisionCheck.ForeColor = System.Drawing.Color.White;
            this.gxtBreakUnitXZMoveZ3VisionCheck.Location = new System.Drawing.Point(3, 3);
            this.gxtBreakUnitXZMoveZ3VisionCheck.Name = "gxtBreakUnitXZMoveZ3VisionCheck";
            this.gxtBreakUnitXZMoveZ3VisionCheck.Size = new System.Drawing.Size(143, 31);
            this.gxtBreakUnitXZMoveZ3VisionCheck.TabIndex = 61;
            this.gxtBreakUnitXZMoveZ3VisionCheck.Text = "비젼 확인";
            this.gxtBreakUnitXZMoveZ3VisionCheck.UseVisualStyleBackColor = false;
            // 
            // gxtBreakUnitXZMoveZ3FastDescent
            // 
            this.gxtBreakUnitXZMoveZ3FastDescent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gxtBreakUnitXZMoveZ3FastDescent.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.gxtBreakUnitXZMoveZ3FastDescent.ForeColor = System.Drawing.Color.White;
            this.gxtBreakUnitXZMoveZ3FastDescent.Location = new System.Drawing.Point(3, 41);
            this.gxtBreakUnitXZMoveZ3FastDescent.Name = "gxtBreakUnitXZMoveZ3FastDescent";
            this.gxtBreakUnitXZMoveZ3FastDescent.Size = new System.Drawing.Size(143, 31);
            this.gxtBreakUnitXZMoveZ3FastDescent.TabIndex = 62;
            this.gxtBreakUnitXZMoveZ3FastDescent.Text = "고속 하강";
            this.gxtBreakUnitXZMoveZ3FastDescent.UseVisualStyleBackColor = false;
            // 
            // gxtBreakUnitXZ_MoveZ2
            // 
            this.gxtBreakUnitXZ_MoveZ2.Controls.Add(this.tableLayoutPanel47);
            this.gxtBreakUnitXZ_MoveZ2.ForeColor = System.Drawing.Color.White;
            this.gxtBreakUnitXZ_MoveZ2.Location = new System.Drawing.Point(168, 3);
            this.gxtBreakUnitXZ_MoveZ2.Name = "gxtBreakUnitXZ_MoveZ2";
            this.gxtBreakUnitXZ_MoveZ2.Size = new System.Drawing.Size(159, 148);
            this.gxtBreakUnitXZ_MoveZ2.TabIndex = 55;
            this.gxtBreakUnitXZ_MoveZ2.TabStop = false;
            this.gxtBreakUnitXZ_MoveZ2.Text = "     Z2     ";
            // 
            // tableLayoutPanel47
            // 
            this.tableLayoutPanel47.ColumnCount = 1;
            this.tableLayoutPanel47.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel47.Controls.Add(this.gxtBreakUnitXZMoveZ2SlowDescent, 0, 2);
            this.tableLayoutPanel47.Controls.Add(this.gxtBreakUnitXZMoveZ2VisionCheck, 0, 0);
            this.tableLayoutPanel47.Controls.Add(this.gxtBreakUnitXZMoveZ2FastDescent, 0, 1);
            this.tableLayoutPanel47.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel47.Name = "tableLayoutPanel47";
            this.tableLayoutPanel47.RowCount = 3;
            this.tableLayoutPanel47.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel47.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel47.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel47.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel47.Size = new System.Drawing.Size(149, 114);
            this.tableLayoutPanel47.TabIndex = 55;
            // 
            // gxtBreakUnitXZMoveZ2SlowDescent
            // 
            this.gxtBreakUnitXZMoveZ2SlowDescent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gxtBreakUnitXZMoveZ2SlowDescent.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.gxtBreakUnitXZMoveZ2SlowDescent.ForeColor = System.Drawing.Color.White;
            this.gxtBreakUnitXZMoveZ2SlowDescent.Location = new System.Drawing.Point(3, 79);
            this.gxtBreakUnitXZMoveZ2SlowDescent.Name = "gxtBreakUnitXZMoveZ2SlowDescent";
            this.gxtBreakUnitXZMoveZ2SlowDescent.Size = new System.Drawing.Size(143, 32);
            this.gxtBreakUnitXZMoveZ2SlowDescent.TabIndex = 63;
            this.gxtBreakUnitXZMoveZ2SlowDescent.Text = "저속 하강";
            this.gxtBreakUnitXZMoveZ2SlowDescent.UseVisualStyleBackColor = false;
            // 
            // gxtBreakUnitXZMoveZ2VisionCheck
            // 
            this.gxtBreakUnitXZMoveZ2VisionCheck.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gxtBreakUnitXZMoveZ2VisionCheck.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.gxtBreakUnitXZMoveZ2VisionCheck.ForeColor = System.Drawing.Color.White;
            this.gxtBreakUnitXZMoveZ2VisionCheck.Location = new System.Drawing.Point(3, 3);
            this.gxtBreakUnitXZMoveZ2VisionCheck.Name = "gxtBreakUnitXZMoveZ2VisionCheck";
            this.gxtBreakUnitXZMoveZ2VisionCheck.Size = new System.Drawing.Size(143, 31);
            this.gxtBreakUnitXZMoveZ2VisionCheck.TabIndex = 61;
            this.gxtBreakUnitXZMoveZ2VisionCheck.Text = "비젼 확인";
            this.gxtBreakUnitXZMoveZ2VisionCheck.UseVisualStyleBackColor = false;
            // 
            // gxtBreakUnitXZMoveZ2FastDescent
            // 
            this.gxtBreakUnitXZMoveZ2FastDescent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gxtBreakUnitXZMoveZ2FastDescent.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.gxtBreakUnitXZMoveZ2FastDescent.ForeColor = System.Drawing.Color.White;
            this.gxtBreakUnitXZMoveZ2FastDescent.Location = new System.Drawing.Point(3, 41);
            this.gxtBreakUnitXZMoveZ2FastDescent.Name = "gxtBreakUnitXZMoveZ2FastDescent";
            this.gxtBreakUnitXZMoveZ2FastDescent.Size = new System.Drawing.Size(143, 31);
            this.gxtBreakUnitXZMoveZ2FastDescent.TabIndex = 62;
            this.gxtBreakUnitXZMoveZ2FastDescent.Text = "고속 하강";
            this.gxtBreakUnitXZMoveZ2FastDescent.UseVisualStyleBackColor = false;
            // 
            // gxtBreakUnitXZ_MoveZ1
            // 
            this.gxtBreakUnitXZ_MoveZ1.Controls.Add(this.tableLayoutPanel46);
            this.gxtBreakUnitXZ_MoveZ1.ForeColor = System.Drawing.Color.White;
            this.gxtBreakUnitXZ_MoveZ1.Location = new System.Drawing.Point(3, 3);
            this.gxtBreakUnitXZ_MoveZ1.Name = "gxtBreakUnitXZ_MoveZ1";
            this.gxtBreakUnitXZ_MoveZ1.Size = new System.Drawing.Size(159, 148);
            this.gxtBreakUnitXZ_MoveZ1.TabIndex = 54;
            this.gxtBreakUnitXZ_MoveZ1.TabStop = false;
            this.gxtBreakUnitXZ_MoveZ1.Text = "     Z1     ";
            // 
            // tableLayoutPanel46
            // 
            this.tableLayoutPanel46.ColumnCount = 1;
            this.tableLayoutPanel46.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel46.Controls.Add(this.gxtBreakUnitXZMoveZ1SlowDescent, 0, 2);
            this.tableLayoutPanel46.Controls.Add(this.gxtBreakUnitXZMoveZ1VisionCheck, 0, 0);
            this.tableLayoutPanel46.Controls.Add(this.gxtBreakUnitXZMoveZ1FastDescent, 0, 1);
            this.tableLayoutPanel46.Location = new System.Drawing.Point(4, 28);
            this.tableLayoutPanel46.Name = "tableLayoutPanel46";
            this.tableLayoutPanel46.RowCount = 3;
            this.tableLayoutPanel46.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel46.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel46.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel46.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel46.Size = new System.Drawing.Size(149, 114);
            this.tableLayoutPanel46.TabIndex = 54;
            // 
            // gxtBreakUnitXZMoveZ1SlowDescent
            // 
            this.gxtBreakUnitXZMoveZ1SlowDescent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gxtBreakUnitXZMoveZ1SlowDescent.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.gxtBreakUnitXZMoveZ1SlowDescent.ForeColor = System.Drawing.Color.White;
            this.gxtBreakUnitXZMoveZ1SlowDescent.Location = new System.Drawing.Point(3, 79);
            this.gxtBreakUnitXZMoveZ1SlowDescent.Name = "gxtBreakUnitXZMoveZ1SlowDescent";
            this.gxtBreakUnitXZMoveZ1SlowDescent.Size = new System.Drawing.Size(143, 32);
            this.gxtBreakUnitXZMoveZ1SlowDescent.TabIndex = 63;
            this.gxtBreakUnitXZMoveZ1SlowDescent.Text = "저속 하강";
            this.gxtBreakUnitXZMoveZ1SlowDescent.UseVisualStyleBackColor = false;
            // 
            // gxtBreakUnitXZMoveZ1VisionCheck
            // 
            this.gxtBreakUnitXZMoveZ1VisionCheck.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gxtBreakUnitXZMoveZ1VisionCheck.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.gxtBreakUnitXZMoveZ1VisionCheck.ForeColor = System.Drawing.Color.White;
            this.gxtBreakUnitXZMoveZ1VisionCheck.Location = new System.Drawing.Point(3, 3);
            this.gxtBreakUnitXZMoveZ1VisionCheck.Name = "gxtBreakUnitXZMoveZ1VisionCheck";
            this.gxtBreakUnitXZMoveZ1VisionCheck.Size = new System.Drawing.Size(143, 31);
            this.gxtBreakUnitXZMoveZ1VisionCheck.TabIndex = 61;
            this.gxtBreakUnitXZMoveZ1VisionCheck.Text = "비젼 확인";
            this.gxtBreakUnitXZMoveZ1VisionCheck.UseVisualStyleBackColor = false;
            // 
            // gxtBreakUnitXZMoveZ1FastDescent
            // 
            this.gxtBreakUnitXZMoveZ1FastDescent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gxtBreakUnitXZMoveZ1FastDescent.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.gxtBreakUnitXZMoveZ1FastDescent.ForeColor = System.Drawing.Color.White;
            this.gxtBreakUnitXZMoveZ1FastDescent.Location = new System.Drawing.Point(3, 41);
            this.gxtBreakUnitXZMoveZ1FastDescent.Name = "gxtBreakUnitXZMoveZ1FastDescent";
            this.gxtBreakUnitXZMoveZ1FastDescent.Size = new System.Drawing.Size(143, 31);
            this.gxtBreakUnitXZMoveZ1FastDescent.TabIndex = 62;
            this.gxtBreakUnitXZMoveZ1FastDescent.Text = "고속 하강";
            this.gxtBreakUnitXZMoveZ1FastDescent.UseVisualStyleBackColor = false;
            // 
            // gxtBreakUnitXZSave
            // 
            this.gxtBreakUnitXZSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gxtBreakUnitXZSave.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtBreakUnitXZSave.ForeColor = System.Drawing.Color.White;
            this.gxtBreakUnitXZSave.Location = new System.Drawing.Point(1482, 783);
            this.gxtBreakUnitXZSave.Name = "gxtBreakUnitXZSave";
            this.gxtBreakUnitXZSave.Size = new System.Drawing.Size(228, 28);
            this.gxtBreakUnitXZSave.TabIndex = 50;
            this.gxtBreakUnitXZSave.Text = "저장";
            this.gxtBreakUnitXZSave.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitXZSpeedSetting
            // 
            this.btnBreakUnitXZSpeedSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitXZSpeedSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnBreakUnitXZSpeedSetting.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitXZSpeedSetting.Location = new System.Drawing.Point(1291, 315);
            this.btnBreakUnitXZSpeedSetting.Name = "btnBreakUnitXZSpeedSetting";
            this.btnBreakUnitXZSpeedSetting.Size = new System.Drawing.Size(90, 27);
            this.btnBreakUnitXZSpeedSetting.TabIndex = 48;
            this.btnBreakUnitXZSpeedSetting.Text = "속도 설정";
            this.btnBreakUnitXZSpeedSetting.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitXZAllSetting
            // 
            this.btnBreakUnitXZAllSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitXZAllSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnBreakUnitXZAllSetting.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitXZAllSetting.Location = new System.Drawing.Point(1387, 273);
            this.btnBreakUnitXZAllSetting.Name = "btnBreakUnitXZAllSetting";
            this.btnBreakUnitXZAllSetting.Size = new System.Drawing.Size(90, 27);
            this.btnBreakUnitXZAllSetting.TabIndex = 47;
            this.btnBreakUnitXZAllSetting.Text = "전체 설정";
            this.btnBreakUnitXZAllSetting.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitXZLocationSetting
            // 
            this.btnBreakUnitXZLocationSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitXZLocationSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnBreakUnitXZLocationSetting.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitXZLocationSetting.Location = new System.Drawing.Point(1071, 315);
            this.btnBreakUnitXZLocationSetting.Name = "btnBreakUnitXZLocationSetting";
            this.btnBreakUnitXZLocationSetting.Size = new System.Drawing.Size(90, 27);
            this.btnBreakUnitXZLocationSetting.TabIndex = 46;
            this.btnBreakUnitXZLocationSetting.Text = "위치 설정";
            this.btnBreakUnitXZLocationSetting.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitXZMoveLocation
            // 
            this.btnBreakUnitXZMoveLocation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitXZMoveLocation.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnBreakUnitXZMoveLocation.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitXZMoveLocation.Location = new System.Drawing.Point(975, 315);
            this.btnBreakUnitXZMoveLocation.Name = "btnBreakUnitXZMoveLocation";
            this.btnBreakUnitXZMoveLocation.Size = new System.Drawing.Size(90, 27);
            this.btnBreakUnitXZMoveLocation.TabIndex = 45;
            this.btnBreakUnitXZMoveLocation.Text = "위치 이동";
            this.btnBreakUnitXZMoveLocation.UseVisualStyleBackColor = false;
            // 
            // txtBreakUnitXZLocation
            // 
            this.txtBreakUnitXZLocation.Font = new System.Drawing.Font("굴림", 13F);
            this.txtBreakUnitXZLocation.Location = new System.Drawing.Point(879, 316);
            this.txtBreakUnitXZLocation.Name = "txtBreakUnitXZLocation";
            this.txtBreakUnitXZLocation.Size = new System.Drawing.Size(90, 27);
            this.txtBreakUnitXZLocation.TabIndex = 44;
            // 
            // txtBreakUnitXZSpeed
            // 
            this.txtBreakUnitXZSpeed.Font = new System.Drawing.Font("굴림", 13F);
            this.txtBreakUnitXZSpeed.Location = new System.Drawing.Point(1291, 273);
            this.txtBreakUnitXZSpeed.Name = "txtBreakUnitXZSpeed";
            this.txtBreakUnitXZSpeed.Size = new System.Drawing.Size(90, 27);
            this.txtBreakUnitXZSpeed.TabIndex = 43;
            // 
            // txtBreakUnitXZCurrentLocation
            // 
            this.txtBreakUnitXZCurrentLocation.Font = new System.Drawing.Font("굴림", 13F);
            this.txtBreakUnitXZCurrentLocation.Location = new System.Drawing.Point(1084, 274);
            this.txtBreakUnitXZCurrentLocation.Name = "txtBreakUnitXZCurrentLocation";
            this.txtBreakUnitXZCurrentLocation.Size = new System.Drawing.Size(90, 27);
            this.txtBreakUnitXZCurrentLocation.TabIndex = 42;
            // 
            // txtBreakUnitXZSelectedShaft
            // 
            this.txtBreakUnitXZSelectedShaft.Font = new System.Drawing.Font("굴림", 13F);
            this.txtBreakUnitXZSelectedShaft.Location = new System.Drawing.Point(879, 273);
            this.txtBreakUnitXZSelectedShaft.Name = "txtBreakUnitXZSelectedShaft";
            this.txtBreakUnitXZSelectedShaft.Size = new System.Drawing.Size(90, 27);
            this.txtBreakUnitXZSelectedShaft.TabIndex = 41;
            // 
            // lbl_BreakUnitXZ_Location
            // 
            this.lbl_BreakUnitXZ_Location.AutoSize = true;
            this.lbl_BreakUnitXZ_Location.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_BreakUnitXZ_Location.ForeColor = System.Drawing.Color.White;
            this.lbl_BreakUnitXZ_Location.Location = new System.Drawing.Point(801, 316);
            this.lbl_BreakUnitXZ_Location.Name = "lbl_BreakUnitXZ_Location";
            this.lbl_BreakUnitXZ_Location.Size = new System.Drawing.Size(84, 21);
            this.lbl_BreakUnitXZ_Location.TabIndex = 40;
            this.lbl_BreakUnitXZ_Location.Text = "위치[mm]";
            // 
            // lbl_BreakUnitXZ_Speed
            // 
            this.lbl_BreakUnitXZ_Speed.AutoSize = true;
            this.lbl_BreakUnitXZ_Speed.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_BreakUnitXZ_Speed.ForeColor = System.Drawing.Color.White;
            this.lbl_BreakUnitXZ_Speed.Location = new System.Drawing.Point(1179, 275);
            this.lbl_BreakUnitXZ_Speed.Name = "lbl_BreakUnitXZ_Speed";
            this.lbl_BreakUnitXZ_Speed.Size = new System.Drawing.Size(115, 21);
            this.lbl_BreakUnitXZ_Speed.TabIndex = 39;
            this.lbl_BreakUnitXZ_Speed.Text = "속도[mm/sec]";
            // 
            // lbl_BreakUnitXZ_SelectedShaft
            // 
            this.lbl_BreakUnitXZ_SelectedShaft.AutoSize = true;
            this.lbl_BreakUnitXZ_SelectedShaft.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_BreakUnitXZ_SelectedShaft.ForeColor = System.Drawing.Color.White;
            this.lbl_BreakUnitXZ_SelectedShaft.Location = new System.Drawing.Point(805, 274);
            this.lbl_BreakUnitXZ_SelectedShaft.Name = "lbl_BreakUnitXZ_SelectedShaft";
            this.lbl_BreakUnitXZ_SelectedShaft.Size = new System.Drawing.Size(80, 21);
            this.lbl_BreakUnitXZ_SelectedShaft.TabIndex = 38;
            this.lbl_BreakUnitXZ_SelectedShaft.Text = "선택된 축";
            // 
            // txtBreakUnitXZ_LocationSetting
            // 
            this.txtBreakUnitXZ_LocationSetting.BackColor = System.Drawing.SystemColors.Highlight;
            this.txtBreakUnitXZ_LocationSetting.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.txtBreakUnitXZ_LocationSetting.ForeColor = System.Drawing.Color.White;
            this.txtBreakUnitXZ_LocationSetting.Location = new System.Drawing.Point(792, 223);
            this.txtBreakUnitXZ_LocationSetting.Name = "txtBreakUnitXZ_LocationSetting";
            this.txtBreakUnitXZ_LocationSetting.ReadOnly = true;
            this.txtBreakUnitXZ_LocationSetting.Size = new System.Drawing.Size(89, 27);
            this.txtBreakUnitXZ_LocationSetting.TabIndex = 37;
            this.txtBreakUnitXZ_LocationSetting.Text = "위치값 설정";
            this.txtBreakUnitXZ_LocationSetting.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbl_BreakUnitXZ_CurrentLocation
            // 
            this.lbl_BreakUnitXZ_CurrentLocation.AutoSize = true;
            this.lbl_BreakUnitXZ_CurrentLocation.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_BreakUnitXZ_CurrentLocation.ForeColor = System.Drawing.Color.White;
            this.lbl_BreakUnitXZ_CurrentLocation.Location = new System.Drawing.Point(975, 274);
            this.lbl_BreakUnitXZ_CurrentLocation.Name = "lbl_BreakUnitXZ_CurrentLocation";
            this.lbl_BreakUnitXZ_CurrentLocation.Size = new System.Drawing.Size(116, 21);
            this.lbl_BreakUnitXZ_CurrentLocation.TabIndex = 36;
            this.lbl_BreakUnitXZ_CurrentLocation.Text = "현재위치[mm]";
            // 
            // btnBreakUnitXZGrabSwitch3
            // 
            this.btnBreakUnitXZGrabSwitch3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitXZGrabSwitch3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitXZGrabSwitch3.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitXZGrabSwitch3.Location = new System.Drawing.Point(1514, 6);
            this.btnBreakUnitXZGrabSwitch3.Name = "btnBreakUnitXZGrabSwitch3";
            this.btnBreakUnitXZGrabSwitch3.Size = new System.Drawing.Size(172, 23);
            this.btnBreakUnitXZGrabSwitch3.TabIndex = 35;
            this.btnBreakUnitXZGrabSwitch3.Text = "Grab Switch 3";
            this.btnBreakUnitXZGrabSwitch3.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitXZGrabSwitch2
            // 
            this.btnBreakUnitXZGrabSwitch2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitXZGrabSwitch2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitXZGrabSwitch2.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitXZGrabSwitch2.Location = new System.Drawing.Point(1336, 6);
            this.btnBreakUnitXZGrabSwitch2.Name = "btnBreakUnitXZGrabSwitch2";
            this.btnBreakUnitXZGrabSwitch2.Size = new System.Drawing.Size(172, 23);
            this.btnBreakUnitXZGrabSwitch2.TabIndex = 34;
            this.btnBreakUnitXZGrabSwitch2.Text = "Grab Switch 2";
            this.btnBreakUnitXZGrabSwitch2.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitXZGrabSwitch1
            // 
            this.btnBreakUnitXZGrabSwitch1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitXZGrabSwitch1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitXZGrabSwitch1.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitXZGrabSwitch1.Location = new System.Drawing.Point(1158, 6);
            this.btnBreakUnitXZGrabSwitch1.Name = "btnBreakUnitXZGrabSwitch1";
            this.btnBreakUnitXZGrabSwitch1.Size = new System.Drawing.Size(172, 23);
            this.btnBreakUnitXZGrabSwitch1.TabIndex = 33;
            this.btnBreakUnitXZGrabSwitch1.Text = "Grab Switch 1";
            this.btnBreakUnitXZGrabSwitch1.UseVisualStyleBackColor = false;
            // 
            // lvBreakUnitXZ
            // 
            this.lvBreakUnitXZ.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.col_BreakUnitXZ_NamebyLocation,
            this.col_BreakUnitXZ_Shaft,
            this.col_BreakUnitXZ_Location,
            this.col_BreakUnitXZ_Speed});
            this.lvBreakUnitXZ.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lvBreakUnitXZ.GridLines = true;
            this.lvBreakUnitXZ.Location = new System.Drawing.Point(17, 16);
            this.lvBreakUnitXZ.Name = "lvBreakUnitXZ";
            this.lvBreakUnitXZ.Size = new System.Drawing.Size(758, 734);
            this.lvBreakUnitXZ.TabIndex = 32;
            this.lvBreakUnitXZ.UseCompatibleStateImageBehavior = false;
            this.lvBreakUnitXZ.View = System.Windows.Forms.View.Details;
            // 
            // col_BreakUnitXZ_NamebyLocation
            // 
            this.col_BreakUnitXZ_NamebyLocation.Text = "위치별 명칭";
            this.col_BreakUnitXZ_NamebyLocation.Width = 430;
            // 
            // col_BreakUnitXZ_Shaft
            // 
            this.col_BreakUnitXZ_Shaft.Text = "축";
            this.col_BreakUnitXZ_Shaft.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // col_BreakUnitXZ_Location
            // 
            this.col_BreakUnitXZ_Location.Text = "위치[mm]";
            this.col_BreakUnitXZ_Location.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_BreakUnitXZ_Location.Width = 125;
            // 
            // col_BreakUnitXZ_Speed
            // 
            this.col_BreakUnitXZ_Speed.Text = "속도[mm/s]";
            this.col_BreakUnitXZ_Speed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_BreakUnitXZ_Speed.Width = 125;
            // 
            // ajin_Setting_BreakUnitXZ
            // 
            this.ajin_Setting_BreakUnitXZ.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_BreakUnitXZ.Location = new System.Drawing.Point(792, 34);
            this.ajin_Setting_BreakUnitXZ.Name = "ajin_Setting_BreakUnitXZ";
            this.ajin_Setting_BreakUnitXZ.Servo = null;
            this.ajin_Setting_BreakUnitXZ.Size = new System.Drawing.Size(903, 183);
            this.ajin_Setting_BreakUnitXZ.TabIndex = 31;
            // 
            // tp_BreakUnitTY
            // 
            this.tp_BreakUnitTY.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tp_BreakUnitTY.Controls.Add(this.gxtBreakUnitTY_Shutter);
            this.tp_BreakUnitTY.Controls.Add(this.gxtBreakUnitTY_DummyBox);
            this.tp_BreakUnitTY.Controls.Add(this.gxtBreakUnitTY_Ionizer);
            this.tp_BreakUnitTY.Controls.Add(this.gxtBreakUnitTY_Move);
            this.tp_BreakUnitTY.Controls.Add(this.gxtBreakUnitTY_B);
            this.tp_BreakUnitTY.Controls.Add(this.gxtBreakUnitTY_A);
            this.tp_BreakUnitTY.Controls.Add(this.btnBreakUnitTYSave);
            this.tp_BreakUnitTY.Controls.Add(this.btnBreakUnitTYSpeedSetting);
            this.tp_BreakUnitTY.Controls.Add(this.btnBreakUnitTYAllSetting);
            this.tp_BreakUnitTY.Controls.Add(this.btnBreakUnitTYLocationSetting);
            this.tp_BreakUnitTY.Controls.Add(this.btnBreakUnitTYMoveLocation);
            this.tp_BreakUnitTY.Controls.Add(this.txtBreakUnitTYLocation);
            this.tp_BreakUnitTY.Controls.Add(this.txtBreakUnitTYSpeed);
            this.tp_BreakUnitTY.Controls.Add(this.txtBreakUnitTYCurrentLocation);
            this.tp_BreakUnitTY.Controls.Add(this.txtBreakUnitTYSelectedShaft);
            this.tp_BreakUnitTY.Controls.Add(this.lbl_BreakUnitTY_Location);
            this.tp_BreakUnitTY.Controls.Add(this.lbl_BreakUnitTY_Speed);
            this.tp_BreakUnitTY.Controls.Add(this.lbl_BreakUnitTY_SelectedShaft);
            this.tp_BreakUnitTY.Controls.Add(this.txtBreakUnitTY_LocationSetting);
            this.tp_BreakUnitTY.Controls.Add(this.lbl_BreakUnitTY_CurrentLocation);
            this.tp_BreakUnitTY.Controls.Add(this.btnBreakUnitTYGrabSwitch3);
            this.tp_BreakUnitTY.Controls.Add(this.btnBreakUnitTYGrabSwitch2);
            this.tp_BreakUnitTY.Controls.Add(this.btnBreakUnitTYGrabSwitch1);
            this.tp_BreakUnitTY.Controls.Add(this.lvBreakUnitTY);
            this.tp_BreakUnitTY.Controls.Add(this.ajin_Setting_BreakUnitTY);
            this.tp_BreakUnitTY.Location = new System.Drawing.Point(4, 34);
            this.tp_BreakUnitTY.Name = "tp_BreakUnitTY";
            this.tp_BreakUnitTY.Size = new System.Drawing.Size(1726, 816);
            this.tp_BreakUnitTY.TabIndex = 6;
            this.tp_BreakUnitTY.Text = "Break 유닛(T축 Y축)";
            // 
            // gxtBreakUnitTY_Shutter
            // 
            this.gxtBreakUnitTY_Shutter.Controls.Add(this.tableLayoutPanel53);
            this.gxtBreakUnitTY_Shutter.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtBreakUnitTY_Shutter.ForeColor = System.Drawing.Color.White;
            this.gxtBreakUnitTY_Shutter.Location = new System.Drawing.Point(1484, 480);
            this.gxtBreakUnitTY_Shutter.Name = "gxtBreakUnitTY_Shutter";
            this.gxtBreakUnitTY_Shutter.Size = new System.Drawing.Size(212, 109);
            this.gxtBreakUnitTY_Shutter.TabIndex = 55;
            this.gxtBreakUnitTY_Shutter.TabStop = false;
            this.gxtBreakUnitTY_Shutter.Text = "     셔터    ";
            // 
            // tableLayoutPanel53
            // 
            this.tableLayoutPanel53.ColumnCount = 2;
            this.tableLayoutPanel53.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel53.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel53.Controls.Add(this.btnBreakUnitTYShutterClose, 0, 0);
            this.tableLayoutPanel53.Controls.Add(this.btnBreakUnitTYShutterOpen, 0, 0);
            this.tableLayoutPanel53.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel53.Name = "tableLayoutPanel53";
            this.tableLayoutPanel53.RowCount = 1;
            this.tableLayoutPanel53.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel53.Size = new System.Drawing.Size(200, 75);
            this.tableLayoutPanel53.TabIndex = 57;
            // 
            // btnBreakUnitTYShutterClose
            // 
            this.btnBreakUnitTYShutterClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYShutterClose.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYShutterClose.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYShutterClose.Location = new System.Drawing.Point(103, 3);
            this.btnBreakUnitTYShutterClose.Name = "btnBreakUnitTYShutterClose";
            this.btnBreakUnitTYShutterClose.Size = new System.Drawing.Size(94, 69);
            this.btnBreakUnitTYShutterClose.TabIndex = 58;
            this.btnBreakUnitTYShutterClose.Text = "더미 셔터\r\nClose";
            this.btnBreakUnitTYShutterClose.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYShutterOpen
            // 
            this.btnBreakUnitTYShutterOpen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYShutterOpen.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYShutterOpen.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYShutterOpen.Location = new System.Drawing.Point(3, 3);
            this.btnBreakUnitTYShutterOpen.Name = "btnBreakUnitTYShutterOpen";
            this.btnBreakUnitTYShutterOpen.Size = new System.Drawing.Size(94, 69);
            this.btnBreakUnitTYShutterOpen.TabIndex = 57;
            this.btnBreakUnitTYShutterOpen.Text = "더미 셔터\r\nOpen";
            this.btnBreakUnitTYShutterOpen.UseVisualStyleBackColor = false;
            // 
            // gxtBreakUnitTY_DummyBox
            // 
            this.gxtBreakUnitTY_DummyBox.Controls.Add(this.tableLayoutPanel52);
            this.gxtBreakUnitTY_DummyBox.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtBreakUnitTY_DummyBox.ForeColor = System.Drawing.Color.White;
            this.gxtBreakUnitTY_DummyBox.Location = new System.Drawing.Point(1484, 410);
            this.gxtBreakUnitTY_DummyBox.Name = "gxtBreakUnitTY_DummyBox";
            this.gxtBreakUnitTY_DummyBox.Size = new System.Drawing.Size(212, 64);
            this.gxtBreakUnitTY_DummyBox.TabIndex = 31;
            this.gxtBreakUnitTY_DummyBox.TabStop = false;
            this.gxtBreakUnitTY_DummyBox.Text = "     더미 박스    ";
            // 
            // tableLayoutPanel52
            // 
            this.tableLayoutPanel52.ColumnCount = 2;
            this.tableLayoutPanel52.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel52.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel52.Controls.Add(this.btnBreakUnitTYDummyxtOutput, 0, 0);
            this.tableLayoutPanel52.Controls.Add(this.btnBreakUnitTYDummyxtInput, 0, 0);
            this.tableLayoutPanel52.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel52.Name = "tableLayoutPanel52";
            this.tableLayoutPanel52.RowCount = 1;
            this.tableLayoutPanel52.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel52.Size = new System.Drawing.Size(200, 30);
            this.tableLayoutPanel52.TabIndex = 56;
            // 
            // btnBreakUnitTYDummyxtOutput
            // 
            this.btnBreakUnitTYDummyxtOutput.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYDummyxtOutput.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakUnitTYDummyxtOutput.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYDummyxtOutput.Location = new System.Drawing.Point(103, 3);
            this.btnBreakUnitTYDummyxtOutput.Name = "btnBreakUnitTYDummyxtOutput";
            this.btnBreakUnitTYDummyxtOutput.Size = new System.Drawing.Size(94, 24);
            this.btnBreakUnitTYDummyxtOutput.TabIndex = 58;
            this.btnBreakUnitTYDummyxtOutput.Text = "더미 배출";
            this.btnBreakUnitTYDummyxtOutput.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYDummyxtInput
            // 
            this.btnBreakUnitTYDummyxtInput.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYDummyxtInput.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakUnitTYDummyxtInput.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYDummyxtInput.Location = new System.Drawing.Point(3, 3);
            this.btnBreakUnitTYDummyxtInput.Name = "btnBreakUnitTYDummyxtInput";
            this.btnBreakUnitTYDummyxtInput.Size = new System.Drawing.Size(94, 24);
            this.btnBreakUnitTYDummyxtInput.TabIndex = 57;
            this.btnBreakUnitTYDummyxtInput.Text = "더미 투입";
            this.btnBreakUnitTYDummyxtInput.UseVisualStyleBackColor = false;
            // 
            // gxtBreakUnitTY_Ionizer
            // 
            this.gxtBreakUnitTY_Ionizer.Controls.Add(this.btnBreakUnitTYDestructionOff);
            this.gxtBreakUnitTY_Ionizer.Controls.Add(this.btnBreakUnitTYIonizerOff);
            this.gxtBreakUnitTY_Ionizer.Controls.Add(this.btnBreakUnitTYDestructionOn);
            this.gxtBreakUnitTY_Ionizer.Controls.Add(this.btnBreakUnitTYIonizerOn);
            this.gxtBreakUnitTY_Ionizer.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtBreakUnitTY_Ionizer.ForeColor = System.Drawing.Color.White;
            this.gxtBreakUnitTY_Ionizer.Location = new System.Drawing.Point(1484, 223);
            this.gxtBreakUnitTY_Ionizer.Name = "gxtBreakUnitTY_Ionizer";
            this.gxtBreakUnitTY_Ionizer.Size = new System.Drawing.Size(212, 181);
            this.gxtBreakUnitTY_Ionizer.TabIndex = 54;
            this.gxtBreakUnitTY_Ionizer.TabStop = false;
            this.gxtBreakUnitTY_Ionizer.Text = "     이오나이저     ";
            // 
            // btnBreakUnitTYDestructionOff
            // 
            this.btnBreakUnitTYDestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYDestructionOff.Location = new System.Drawing.Point(107, 103);
            this.btnBreakUnitTYDestructionOff.Name = "btnBreakUnitTYDestructionOff";
            this.btnBreakUnitTYDestructionOff.Size = new System.Drawing.Size(99, 72);
            this.btnBreakUnitTYDestructionOff.TabIndex = 6;
            this.btnBreakUnitTYDestructionOff.Text = "파기 오프";
            this.btnBreakUnitTYDestructionOff.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYIonizerOff
            // 
            this.btnBreakUnitTYIonizerOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYIonizerOff.Location = new System.Drawing.Point(107, 28);
            this.btnBreakUnitTYIonizerOff.Name = "btnBreakUnitTYIonizerOff";
            this.btnBreakUnitTYIonizerOff.Size = new System.Drawing.Size(99, 72);
            this.btnBreakUnitTYIonizerOff.TabIndex = 5;
            this.btnBreakUnitTYIonizerOff.Text = "이오나이저\r\n오프";
            this.btnBreakUnitTYIonizerOff.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYDestructionOn
            // 
            this.btnBreakUnitTYDestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYDestructionOn.Location = new System.Drawing.Point(6, 103);
            this.btnBreakUnitTYDestructionOn.Name = "btnBreakUnitTYDestructionOn";
            this.btnBreakUnitTYDestructionOn.Size = new System.Drawing.Size(99, 72);
            this.btnBreakUnitTYDestructionOn.TabIndex = 4;
            this.btnBreakUnitTYDestructionOn.Text = "파기 온";
            this.btnBreakUnitTYDestructionOn.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYIonizerOn
            // 
            this.btnBreakUnitTYIonizerOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYIonizerOn.Location = new System.Drawing.Point(6, 28);
            this.btnBreakUnitTYIonizerOn.Name = "btnBreakUnitTYIonizerOn";
            this.btnBreakUnitTYIonizerOn.Size = new System.Drawing.Size(99, 72);
            this.btnBreakUnitTYIonizerOn.TabIndex = 3;
            this.btnBreakUnitTYIonizerOn.Text = "이오나이저\r\n온";
            this.btnBreakUnitTYIonizerOn.UseVisualStyleBackColor = false;
            // 
            // gxtBreakUnitTY_Move
            // 
            this.gxtBreakUnitTY_Move.Controls.Add(this.tableLayoutPanel54);
            this.gxtBreakUnitTY_Move.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtBreakUnitTY_Move.ForeColor = System.Drawing.Color.White;
            this.gxtBreakUnitTY_Move.Location = new System.Drawing.Point(792, 595);
            this.gxtBreakUnitTY_Move.Name = "gxtBreakUnitTY_Move";
            this.gxtBreakUnitTY_Move.Size = new System.Drawing.Size(904, 179);
            this.gxtBreakUnitTY_Move.TabIndex = 53;
            this.gxtBreakUnitTY_Move.TabStop = false;
            this.gxtBreakUnitTY_Move.Text = "     이동     ";
            // 
            // tableLayoutPanel54
            // 
            this.tableLayoutPanel54.ColumnCount = 3;
            this.tableLayoutPanel54.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel54.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel54.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel54.Controls.Add(this.gxtBreakUnitTY_Theta, 0, 0);
            this.tableLayoutPanel54.Controls.Add(this.gxtBreakUnitTY_MoveB, 0, 0);
            this.tableLayoutPanel54.Controls.Add(this.gxtBreakUnitTY_MoveA, 0, 0);
            this.tableLayoutPanel54.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel54.Name = "tableLayoutPanel54";
            this.tableLayoutPanel54.RowCount = 1;
            this.tableLayoutPanel54.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel54.Size = new System.Drawing.Size(892, 145);
            this.tableLayoutPanel54.TabIndex = 0;
            // 
            // gxtBreakUnitTY_Theta
            // 
            this.gxtBreakUnitTY_Theta.Controls.Add(this.tableLayoutPanel57);
            this.gxtBreakUnitTY_Theta.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtBreakUnitTY_Theta.ForeColor = System.Drawing.Color.White;
            this.gxtBreakUnitTY_Theta.Location = new System.Drawing.Point(597, 3);
            this.gxtBreakUnitTY_Theta.Name = "gxtBreakUnitTY_Theta";
            this.gxtBreakUnitTY_Theta.Size = new System.Drawing.Size(290, 139);
            this.gxtBreakUnitTY_Theta.TabIndex = 54;
            this.gxtBreakUnitTY_Theta.TabStop = false;
            this.gxtBreakUnitTY_Theta.Text = "     세타    ";
            // 
            // tableLayoutPanel57
            // 
            this.tableLayoutPanel57.ColumnCount = 2;
            this.tableLayoutPanel57.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel57.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel57.Controls.Add(this.btnBreakUnitTYThetaT2Wait, 0, 1);
            this.tableLayoutPanel57.Controls.Add(this.btnBreakUnitTYThetaT4Wait, 0, 1);
            this.tableLayoutPanel57.Controls.Add(this.btnBreakUnitTYThetaT1Wait, 0, 0);
            this.tableLayoutPanel57.Controls.Add(this.btnBreakUnitTYThetaT3Wait, 1, 0);
            this.tableLayoutPanel57.Location = new System.Drawing.Point(3, 25);
            this.tableLayoutPanel57.Name = "tableLayoutPanel57";
            this.tableLayoutPanel57.RowCount = 2;
            this.tableLayoutPanel57.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel57.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel57.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel57.Size = new System.Drawing.Size(281, 108);
            this.tableLayoutPanel57.TabIndex = 0;
            // 
            // btnBreakUnitTYThetaT2Wait
            // 
            this.btnBreakUnitTYThetaT2Wait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYThetaT2Wait.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYThetaT2Wait.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYThetaT2Wait.Location = new System.Drawing.Point(3, 57);
            this.btnBreakUnitTYThetaT2Wait.Name = "btnBreakUnitTYThetaT2Wait";
            this.btnBreakUnitTYThetaT2Wait.Size = new System.Drawing.Size(134, 48);
            this.btnBreakUnitTYThetaT2Wait.TabIndex = 65;
            this.btnBreakUnitTYThetaT2Wait.Text = "T2 대기";
            this.btnBreakUnitTYThetaT2Wait.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYThetaT4Wait
            // 
            this.btnBreakUnitTYThetaT4Wait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYThetaT4Wait.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYThetaT4Wait.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYThetaT4Wait.Location = new System.Drawing.Point(143, 57);
            this.btnBreakUnitTYThetaT4Wait.Name = "btnBreakUnitTYThetaT4Wait";
            this.btnBreakUnitTYThetaT4Wait.Size = new System.Drawing.Size(134, 48);
            this.btnBreakUnitTYThetaT4Wait.TabIndex = 64;
            this.btnBreakUnitTYThetaT4Wait.Text = "T4 대기";
            this.btnBreakUnitTYThetaT4Wait.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYThetaT1Wait
            // 
            this.btnBreakUnitTYThetaT1Wait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYThetaT1Wait.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYThetaT1Wait.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYThetaT1Wait.Location = new System.Drawing.Point(3, 3);
            this.btnBreakUnitTYThetaT1Wait.Name = "btnBreakUnitTYThetaT1Wait";
            this.btnBreakUnitTYThetaT1Wait.Size = new System.Drawing.Size(134, 48);
            this.btnBreakUnitTYThetaT1Wait.TabIndex = 62;
            this.btnBreakUnitTYThetaT1Wait.Text = "T1 대기";
            this.btnBreakUnitTYThetaT1Wait.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYThetaT3Wait
            // 
            this.btnBreakUnitTYThetaT3Wait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYThetaT3Wait.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYThetaT3Wait.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYThetaT3Wait.Location = new System.Drawing.Point(143, 3);
            this.btnBreakUnitTYThetaT3Wait.Name = "btnBreakUnitTYThetaT3Wait";
            this.btnBreakUnitTYThetaT3Wait.Size = new System.Drawing.Size(134, 48);
            this.btnBreakUnitTYThetaT3Wait.TabIndex = 63;
            this.btnBreakUnitTYThetaT3Wait.Text = "T3 대기";
            this.btnBreakUnitTYThetaT3Wait.UseVisualStyleBackColor = false;
            // 
            // gxtBreakUnitTY_MoveB
            // 
            this.gxtBreakUnitTY_MoveB.Controls.Add(this.tableLayoutPanel56);
            this.gxtBreakUnitTY_MoveB.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtBreakUnitTY_MoveB.ForeColor = System.Drawing.Color.White;
            this.gxtBreakUnitTY_MoveB.Location = new System.Drawing.Point(300, 3);
            this.gxtBreakUnitTY_MoveB.Name = "gxtBreakUnitTY_MoveB";
            this.gxtBreakUnitTY_MoveB.Size = new System.Drawing.Size(290, 139);
            this.gxtBreakUnitTY_MoveB.TabIndex = 53;
            this.gxtBreakUnitTY_MoveB.TabStop = false;
            this.gxtBreakUnitTY_MoveB.Text = "     B    ";
            // 
            // tableLayoutPanel56
            // 
            this.tableLayoutPanel56.ColumnCount = 2;
            this.tableLayoutPanel56.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel56.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel56.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel56.Controls.Add(this.btnBreakUnitTYMoveBUnload, 0, 0);
            this.tableLayoutPanel56.Controls.Add(this.btnBreakUnitTYMoveBLoad, 0, 0);
            this.tableLayoutPanel56.Location = new System.Drawing.Point(3, 25);
            this.tableLayoutPanel56.Name = "tableLayoutPanel56";
            this.tableLayoutPanel56.RowCount = 1;
            this.tableLayoutPanel56.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel56.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 108F));
            this.tableLayoutPanel56.Size = new System.Drawing.Size(281, 108);
            this.tableLayoutPanel56.TabIndex = 0;
            // 
            // btnBreakUnitTYMoveBUnload
            // 
            this.btnBreakUnitTYMoveBUnload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYMoveBUnload.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYMoveBUnload.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYMoveBUnload.Location = new System.Drawing.Point(143, 3);
            this.btnBreakUnitTYMoveBUnload.Name = "btnBreakUnitTYMoveBUnload";
            this.btnBreakUnitTYMoveBUnload.Size = new System.Drawing.Size(134, 102);
            this.btnBreakUnitTYMoveBUnload.TabIndex = 63;
            this.btnBreakUnitTYMoveBUnload.Text = "언로드";
            this.btnBreakUnitTYMoveBUnload.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYMoveBLoad
            // 
            this.btnBreakUnitTYMoveBLoad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYMoveBLoad.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYMoveBLoad.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYMoveBLoad.Location = new System.Drawing.Point(3, 3);
            this.btnBreakUnitTYMoveBLoad.Name = "btnBreakUnitTYMoveBLoad";
            this.btnBreakUnitTYMoveBLoad.Size = new System.Drawing.Size(134, 102);
            this.btnBreakUnitTYMoveBLoad.TabIndex = 62;
            this.btnBreakUnitTYMoveBLoad.Text = "로드";
            this.btnBreakUnitTYMoveBLoad.UseVisualStyleBackColor = false;
            // 
            // gxtBreakUnitTY_MoveA
            // 
            this.gxtBreakUnitTY_MoveA.Controls.Add(this.tableLayoutPanel55);
            this.gxtBreakUnitTY_MoveA.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtBreakUnitTY_MoveA.ForeColor = System.Drawing.Color.White;
            this.gxtBreakUnitTY_MoveA.Location = new System.Drawing.Point(3, 3);
            this.gxtBreakUnitTY_MoveA.Name = "gxtBreakUnitTY_MoveA";
            this.gxtBreakUnitTY_MoveA.Size = new System.Drawing.Size(290, 139);
            this.gxtBreakUnitTY_MoveA.TabIndex = 52;
            this.gxtBreakUnitTY_MoveA.TabStop = false;
            this.gxtBreakUnitTY_MoveA.Text = "     A    ";
            // 
            // tableLayoutPanel55
            // 
            this.tableLayoutPanel55.ColumnCount = 2;
            this.tableLayoutPanel55.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel55.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel55.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel55.Controls.Add(this.btnBreakUnitTYMoveAUnload, 0, 0);
            this.tableLayoutPanel55.Controls.Add(this.btnBreakUnitTYMoveALoad, 0, 0);
            this.tableLayoutPanel55.Location = new System.Drawing.Point(3, 25);
            this.tableLayoutPanel55.Name = "tableLayoutPanel55";
            this.tableLayoutPanel55.RowCount = 1;
            this.tableLayoutPanel55.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel55.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 108F));
            this.tableLayoutPanel55.Size = new System.Drawing.Size(281, 108);
            this.tableLayoutPanel55.TabIndex = 0;
            // 
            // btnBreakUnitTYMoveAUnload
            // 
            this.btnBreakUnitTYMoveAUnload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYMoveAUnload.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYMoveAUnload.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYMoveAUnload.Location = new System.Drawing.Point(143, 3);
            this.btnBreakUnitTYMoveAUnload.Name = "btnBreakUnitTYMoveAUnload";
            this.btnBreakUnitTYMoveAUnload.Size = new System.Drawing.Size(134, 102);
            this.btnBreakUnitTYMoveAUnload.TabIndex = 63;
            this.btnBreakUnitTYMoveAUnload.Text = "언로드";
            this.btnBreakUnitTYMoveAUnload.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYMoveALoad
            // 
            this.btnBreakUnitTYMoveALoad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYMoveALoad.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYMoveALoad.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYMoveALoad.Location = new System.Drawing.Point(3, 3);
            this.btnBreakUnitTYMoveALoad.Name = "btnBreakUnitTYMoveALoad";
            this.btnBreakUnitTYMoveALoad.Size = new System.Drawing.Size(134, 102);
            this.btnBreakUnitTYMoveALoad.TabIndex = 62;
            this.btnBreakUnitTYMoveALoad.Text = "로드";
            this.btnBreakUnitTYMoveALoad.UseVisualStyleBackColor = false;
            // 
            // gxtBreakUnitTY_B
            // 
            this.gxtBreakUnitTY_B.Controls.Add(this.tableLayoutPanel50);
            this.gxtBreakUnitTY_B.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtBreakUnitTY_B.ForeColor = System.Drawing.Color.White;
            this.gxtBreakUnitTY_B.Location = new System.Drawing.Point(1138, 410);
            this.gxtBreakUnitTY_B.Name = "gxtBreakUnitTY_B";
            this.gxtBreakUnitTY_B.Size = new System.Drawing.Size(340, 179);
            this.gxtBreakUnitTY_B.TabIndex = 52;
            this.gxtBreakUnitTY_B.TabStop = false;
            this.gxtBreakUnitTY_B.Text = "     B     ";
            // 
            // tableLayoutPanel50
            // 
            this.tableLayoutPanel50.ColumnCount = 4;
            this.tableLayoutPanel50.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel50.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel50.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel50.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel50.Controls.Add(this.btnBreakUnitTYB1DestructionOn, 0, 1);
            this.tableLayoutPanel50.Controls.Add(this.btnBreakUnitTYB1DestructionOff, 0, 1);
            this.tableLayoutPanel50.Controls.Add(this.btnBreakUnitTYB2DestructionOn, 0, 1);
            this.tableLayoutPanel50.Controls.Add(this.btnBreakUnitTYB2DestructionOff, 0, 1);
            this.tableLayoutPanel50.Controls.Add(this.btnBreakUnitTYB1PneumaticOff, 1, 0);
            this.tableLayoutPanel50.Controls.Add(this.btnBreakUnitTYB1PneumaticOn, 0, 0);
            this.tableLayoutPanel50.Controls.Add(this.btnBreakUnitTYb2PneumaticOn, 2, 0);
            this.tableLayoutPanel50.Controls.Add(this.btnBreakUnitTYB2PneumaticOff, 3, 0);
            this.tableLayoutPanel50.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel50.Name = "tableLayoutPanel50";
            this.tableLayoutPanel50.RowCount = 2;
            this.tableLayoutPanel50.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel50.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel50.Size = new System.Drawing.Size(328, 145);
            this.tableLayoutPanel50.TabIndex = 56;
            // 
            // btnBreakUnitTYB1DestructionOn
            // 
            this.btnBreakUnitTYB1DestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYB1DestructionOn.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYB1DestructionOn.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYB1DestructionOn.Location = new System.Drawing.Point(3, 75);
            this.btnBreakUnitTYB1DestructionOn.Name = "btnBreakUnitTYB1DestructionOn";
            this.btnBreakUnitTYB1DestructionOn.Size = new System.Drawing.Size(76, 66);
            this.btnBreakUnitTYB1DestructionOn.TabIndex = 62;
            this.btnBreakUnitTYB1DestructionOn.Text = "1 파기\r\n온";
            this.btnBreakUnitTYB1DestructionOn.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYB1DestructionOff
            // 
            this.btnBreakUnitTYB1DestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYB1DestructionOff.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYB1DestructionOff.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYB1DestructionOff.Location = new System.Drawing.Point(85, 75);
            this.btnBreakUnitTYB1DestructionOff.Name = "btnBreakUnitTYB1DestructionOff";
            this.btnBreakUnitTYB1DestructionOff.Size = new System.Drawing.Size(76, 66);
            this.btnBreakUnitTYB1DestructionOff.TabIndex = 61;
            this.btnBreakUnitTYB1DestructionOff.Text = "1 파기\r\n오프";
            this.btnBreakUnitTYB1DestructionOff.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYB2DestructionOn
            // 
            this.btnBreakUnitTYB2DestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYB2DestructionOn.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYB2DestructionOn.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYB2DestructionOn.Location = new System.Drawing.Point(167, 75);
            this.btnBreakUnitTYB2DestructionOn.Name = "btnBreakUnitTYB2DestructionOn";
            this.btnBreakUnitTYB2DestructionOn.Size = new System.Drawing.Size(76, 66);
            this.btnBreakUnitTYB2DestructionOn.TabIndex = 60;
            this.btnBreakUnitTYB2DestructionOn.Text = "2 파기\r\n온";
            this.btnBreakUnitTYB2DestructionOn.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYB2DestructionOff
            // 
            this.btnBreakUnitTYB2DestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYB2DestructionOff.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYB2DestructionOff.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYB2DestructionOff.Location = new System.Drawing.Point(249, 75);
            this.btnBreakUnitTYB2DestructionOff.Name = "btnBreakUnitTYB2DestructionOff";
            this.btnBreakUnitTYB2DestructionOff.Size = new System.Drawing.Size(76, 66);
            this.btnBreakUnitTYB2DestructionOff.TabIndex = 58;
            this.btnBreakUnitTYB2DestructionOff.Text = "2 파기\r\n오프";
            this.btnBreakUnitTYB2DestructionOff.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYB1PneumaticOff
            // 
            this.btnBreakUnitTYB1PneumaticOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYB1PneumaticOff.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYB1PneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYB1PneumaticOff.Location = new System.Drawing.Point(85, 3);
            this.btnBreakUnitTYB1PneumaticOff.Name = "btnBreakUnitTYB1PneumaticOff";
            this.btnBreakUnitTYB1PneumaticOff.Size = new System.Drawing.Size(76, 66);
            this.btnBreakUnitTYB1PneumaticOff.TabIndex = 55;
            this.btnBreakUnitTYB1PneumaticOff.Text = "1 공압\r\n오프";
            this.btnBreakUnitTYB1PneumaticOff.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYB1PneumaticOn
            // 
            this.btnBreakUnitTYB1PneumaticOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYB1PneumaticOn.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYB1PneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYB1PneumaticOn.Location = new System.Drawing.Point(3, 3);
            this.btnBreakUnitTYB1PneumaticOn.Name = "btnBreakUnitTYB1PneumaticOn";
            this.btnBreakUnitTYB1PneumaticOn.Size = new System.Drawing.Size(76, 66);
            this.btnBreakUnitTYB1PneumaticOn.TabIndex = 57;
            this.btnBreakUnitTYB1PneumaticOn.Text = "1 공압\r\n온";
            this.btnBreakUnitTYB1PneumaticOn.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYb2PneumaticOn
            // 
            this.btnBreakUnitTYb2PneumaticOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYb2PneumaticOn.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYb2PneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYb2PneumaticOn.Location = new System.Drawing.Point(167, 3);
            this.btnBreakUnitTYb2PneumaticOn.Name = "btnBreakUnitTYb2PneumaticOn";
            this.btnBreakUnitTYb2PneumaticOn.Size = new System.Drawing.Size(76, 66);
            this.btnBreakUnitTYb2PneumaticOn.TabIndex = 59;
            this.btnBreakUnitTYb2PneumaticOn.Text = "2 공압\r\n온";
            this.btnBreakUnitTYb2PneumaticOn.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYB2PneumaticOff
            // 
            this.btnBreakUnitTYB2PneumaticOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYB2PneumaticOff.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYB2PneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYB2PneumaticOff.Location = new System.Drawing.Point(249, 3);
            this.btnBreakUnitTYB2PneumaticOff.Name = "btnBreakUnitTYB2PneumaticOff";
            this.btnBreakUnitTYB2PneumaticOff.Size = new System.Drawing.Size(76, 66);
            this.btnBreakUnitTYB2PneumaticOff.TabIndex = 56;
            this.btnBreakUnitTYB2PneumaticOff.Text = "2 공압\r\n오프";
            this.btnBreakUnitTYB2PneumaticOff.UseVisualStyleBackColor = false;
            // 
            // gxtBreakUnitTY_A
            // 
            this.gxtBreakUnitTY_A.Controls.Add(this.tableLayoutPanel51);
            this.gxtBreakUnitTY_A.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtBreakUnitTY_A.ForeColor = System.Drawing.Color.White;
            this.gxtBreakUnitTY_A.Location = new System.Drawing.Point(792, 410);
            this.gxtBreakUnitTY_A.Name = "gxtBreakUnitTY_A";
            this.gxtBreakUnitTY_A.Size = new System.Drawing.Size(340, 179);
            this.gxtBreakUnitTY_A.TabIndex = 51;
            this.gxtBreakUnitTY_A.TabStop = false;
            this.gxtBreakUnitTY_A.Text = "     A    ";
            // 
            // tableLayoutPanel51
            // 
            this.tableLayoutPanel51.ColumnCount = 4;
            this.tableLayoutPanel51.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel51.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel51.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel51.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel51.Controls.Add(this.btnBreakUnitTYA1DestructionOn, 0, 1);
            this.tableLayoutPanel51.Controls.Add(this.btnBreakUnitTYA1DestructionOff, 0, 1);
            this.tableLayoutPanel51.Controls.Add(this.btnBreakUnitTYA2DestructionOn, 0, 1);
            this.tableLayoutPanel51.Controls.Add(this.btnBreakUnitTYA2DestructionOff, 0, 1);
            this.tableLayoutPanel51.Controls.Add(this.btnBreakUnitTYA1PneumaticOff, 1, 0);
            this.tableLayoutPanel51.Controls.Add(this.btnBreakUnitTYA1PneumaticOn, 0, 0);
            this.tableLayoutPanel51.Controls.Add(this.btnBreakUnitTYA2PneumaticOn, 2, 0);
            this.tableLayoutPanel51.Controls.Add(this.btnBreakUnitTYA2PneumaticOff, 3, 0);
            this.tableLayoutPanel51.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel51.Name = "tableLayoutPanel51";
            this.tableLayoutPanel51.RowCount = 2;
            this.tableLayoutPanel51.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel51.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel51.Size = new System.Drawing.Size(328, 145);
            this.tableLayoutPanel51.TabIndex = 55;
            // 
            // btnBreakUnitTYA1DestructionOn
            // 
            this.btnBreakUnitTYA1DestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYA1DestructionOn.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYA1DestructionOn.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYA1DestructionOn.Location = new System.Drawing.Point(3, 75);
            this.btnBreakUnitTYA1DestructionOn.Name = "btnBreakUnitTYA1DestructionOn";
            this.btnBreakUnitTYA1DestructionOn.Size = new System.Drawing.Size(76, 66);
            this.btnBreakUnitTYA1DestructionOn.TabIndex = 62;
            this.btnBreakUnitTYA1DestructionOn.Text = "1 파기\r\n온";
            this.btnBreakUnitTYA1DestructionOn.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYA1DestructionOff
            // 
            this.btnBreakUnitTYA1DestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYA1DestructionOff.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYA1DestructionOff.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYA1DestructionOff.Location = new System.Drawing.Point(85, 75);
            this.btnBreakUnitTYA1DestructionOff.Name = "btnBreakUnitTYA1DestructionOff";
            this.btnBreakUnitTYA1DestructionOff.Size = new System.Drawing.Size(76, 66);
            this.btnBreakUnitTYA1DestructionOff.TabIndex = 61;
            this.btnBreakUnitTYA1DestructionOff.Text = "1 파기\r\n오프";
            this.btnBreakUnitTYA1DestructionOff.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYA2DestructionOn
            // 
            this.btnBreakUnitTYA2DestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYA2DestructionOn.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYA2DestructionOn.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYA2DestructionOn.Location = new System.Drawing.Point(167, 75);
            this.btnBreakUnitTYA2DestructionOn.Name = "btnBreakUnitTYA2DestructionOn";
            this.btnBreakUnitTYA2DestructionOn.Size = new System.Drawing.Size(76, 66);
            this.btnBreakUnitTYA2DestructionOn.TabIndex = 60;
            this.btnBreakUnitTYA2DestructionOn.Text = "2 파기\r\n온";
            this.btnBreakUnitTYA2DestructionOn.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYA2DestructionOff
            // 
            this.btnBreakUnitTYA2DestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYA2DestructionOff.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYA2DestructionOff.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYA2DestructionOff.Location = new System.Drawing.Point(249, 75);
            this.btnBreakUnitTYA2DestructionOff.Name = "btnBreakUnitTYA2DestructionOff";
            this.btnBreakUnitTYA2DestructionOff.Size = new System.Drawing.Size(76, 66);
            this.btnBreakUnitTYA2DestructionOff.TabIndex = 58;
            this.btnBreakUnitTYA2DestructionOff.Text = "2 파기\r\n오프";
            this.btnBreakUnitTYA2DestructionOff.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYA1PneumaticOff
            // 
            this.btnBreakUnitTYA1PneumaticOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYA1PneumaticOff.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYA1PneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYA1PneumaticOff.Location = new System.Drawing.Point(85, 3);
            this.btnBreakUnitTYA1PneumaticOff.Name = "btnBreakUnitTYA1PneumaticOff";
            this.btnBreakUnitTYA1PneumaticOff.Size = new System.Drawing.Size(76, 66);
            this.btnBreakUnitTYA1PneumaticOff.TabIndex = 55;
            this.btnBreakUnitTYA1PneumaticOff.Text = "1 공압\r\n오프";
            this.btnBreakUnitTYA1PneumaticOff.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYA1PneumaticOn
            // 
            this.btnBreakUnitTYA1PneumaticOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYA1PneumaticOn.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYA1PneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYA1PneumaticOn.Location = new System.Drawing.Point(3, 3);
            this.btnBreakUnitTYA1PneumaticOn.Name = "btnBreakUnitTYA1PneumaticOn";
            this.btnBreakUnitTYA1PneumaticOn.Size = new System.Drawing.Size(76, 66);
            this.btnBreakUnitTYA1PneumaticOn.TabIndex = 57;
            this.btnBreakUnitTYA1PneumaticOn.Text = "1 공압\r\n온";
            this.btnBreakUnitTYA1PneumaticOn.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYA2PneumaticOn
            // 
            this.btnBreakUnitTYA2PneumaticOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYA2PneumaticOn.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYA2PneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYA2PneumaticOn.Location = new System.Drawing.Point(167, 3);
            this.btnBreakUnitTYA2PneumaticOn.Name = "btnBreakUnitTYA2PneumaticOn";
            this.btnBreakUnitTYA2PneumaticOn.Size = new System.Drawing.Size(76, 66);
            this.btnBreakUnitTYA2PneumaticOn.TabIndex = 59;
            this.btnBreakUnitTYA2PneumaticOn.Text = "2 공압\r\n온";
            this.btnBreakUnitTYA2PneumaticOn.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYA2PneumaticOff
            // 
            this.btnBreakUnitTYA2PneumaticOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYA2PneumaticOff.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYA2PneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYA2PneumaticOff.Location = new System.Drawing.Point(249, 3);
            this.btnBreakUnitTYA2PneumaticOff.Name = "btnBreakUnitTYA2PneumaticOff";
            this.btnBreakUnitTYA2PneumaticOff.Size = new System.Drawing.Size(76, 66);
            this.btnBreakUnitTYA2PneumaticOff.TabIndex = 56;
            this.btnBreakUnitTYA2PneumaticOff.Text = "2 공압\r\n오프";
            this.btnBreakUnitTYA2PneumaticOff.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYSave
            // 
            this.btnBreakUnitTYSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYSave.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYSave.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYSave.Location = new System.Drawing.Point(1482, 783);
            this.btnBreakUnitTYSave.Name = "btnBreakUnitTYSave";
            this.btnBreakUnitTYSave.Size = new System.Drawing.Size(228, 28);
            this.btnBreakUnitTYSave.TabIndex = 50;
            this.btnBreakUnitTYSave.Text = "저장";
            this.btnBreakUnitTYSave.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYSpeedSetting
            // 
            this.btnBreakUnitTYSpeedSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYSpeedSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnBreakUnitTYSpeedSetting.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYSpeedSetting.Location = new System.Drawing.Point(1291, 315);
            this.btnBreakUnitTYSpeedSetting.Name = "btnBreakUnitTYSpeedSetting";
            this.btnBreakUnitTYSpeedSetting.Size = new System.Drawing.Size(90, 27);
            this.btnBreakUnitTYSpeedSetting.TabIndex = 48;
            this.btnBreakUnitTYSpeedSetting.Text = "속도 설정";
            this.btnBreakUnitTYSpeedSetting.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYAllSetting
            // 
            this.btnBreakUnitTYAllSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYAllSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnBreakUnitTYAllSetting.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYAllSetting.Location = new System.Drawing.Point(1387, 273);
            this.btnBreakUnitTYAllSetting.Name = "btnBreakUnitTYAllSetting";
            this.btnBreakUnitTYAllSetting.Size = new System.Drawing.Size(90, 27);
            this.btnBreakUnitTYAllSetting.TabIndex = 47;
            this.btnBreakUnitTYAllSetting.Text = "전체 설정";
            this.btnBreakUnitTYAllSetting.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYLocationSetting
            // 
            this.btnBreakUnitTYLocationSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYLocationSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnBreakUnitTYLocationSetting.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYLocationSetting.Location = new System.Drawing.Point(1071, 315);
            this.btnBreakUnitTYLocationSetting.Name = "btnBreakUnitTYLocationSetting";
            this.btnBreakUnitTYLocationSetting.Size = new System.Drawing.Size(90, 27);
            this.btnBreakUnitTYLocationSetting.TabIndex = 46;
            this.btnBreakUnitTYLocationSetting.Text = "위치 설정";
            this.btnBreakUnitTYLocationSetting.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYMoveLocation
            // 
            this.btnBreakUnitTYMoveLocation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYMoveLocation.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnBreakUnitTYMoveLocation.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYMoveLocation.Location = new System.Drawing.Point(975, 315);
            this.btnBreakUnitTYMoveLocation.Name = "btnBreakUnitTYMoveLocation";
            this.btnBreakUnitTYMoveLocation.Size = new System.Drawing.Size(90, 27);
            this.btnBreakUnitTYMoveLocation.TabIndex = 45;
            this.btnBreakUnitTYMoveLocation.Text = "위치 이동";
            this.btnBreakUnitTYMoveLocation.UseVisualStyleBackColor = false;
            // 
            // txtBreakUnitTYLocation
            // 
            this.txtBreakUnitTYLocation.Font = new System.Drawing.Font("굴림", 13F);
            this.txtBreakUnitTYLocation.Location = new System.Drawing.Point(879, 316);
            this.txtBreakUnitTYLocation.Name = "txtBreakUnitTYLocation";
            this.txtBreakUnitTYLocation.Size = new System.Drawing.Size(90, 27);
            this.txtBreakUnitTYLocation.TabIndex = 44;
            // 
            // txtBreakUnitTYSpeed
            // 
            this.txtBreakUnitTYSpeed.Font = new System.Drawing.Font("굴림", 13F);
            this.txtBreakUnitTYSpeed.Location = new System.Drawing.Point(1291, 273);
            this.txtBreakUnitTYSpeed.Name = "txtBreakUnitTYSpeed";
            this.txtBreakUnitTYSpeed.Size = new System.Drawing.Size(90, 27);
            this.txtBreakUnitTYSpeed.TabIndex = 43;
            // 
            // txtBreakUnitTYCurrentLocation
            // 
            this.txtBreakUnitTYCurrentLocation.Font = new System.Drawing.Font("굴림", 13F);
            this.txtBreakUnitTYCurrentLocation.Location = new System.Drawing.Point(1084, 274);
            this.txtBreakUnitTYCurrentLocation.Name = "txtBreakUnitTYCurrentLocation";
            this.txtBreakUnitTYCurrentLocation.Size = new System.Drawing.Size(90, 27);
            this.txtBreakUnitTYCurrentLocation.TabIndex = 42;
            // 
            // txtBreakUnitTYSelectedShaft
            // 
            this.txtBreakUnitTYSelectedShaft.Font = new System.Drawing.Font("굴림", 13F);
            this.txtBreakUnitTYSelectedShaft.Location = new System.Drawing.Point(879, 273);
            this.txtBreakUnitTYSelectedShaft.Name = "txtBreakUnitTYSelectedShaft";
            this.txtBreakUnitTYSelectedShaft.Size = new System.Drawing.Size(90, 27);
            this.txtBreakUnitTYSelectedShaft.TabIndex = 41;
            // 
            // lbl_BreakUnitTY_Location
            // 
            this.lbl_BreakUnitTY_Location.AutoSize = true;
            this.lbl_BreakUnitTY_Location.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_BreakUnitTY_Location.ForeColor = System.Drawing.Color.White;
            this.lbl_BreakUnitTY_Location.Location = new System.Drawing.Point(801, 316);
            this.lbl_BreakUnitTY_Location.Name = "lbl_BreakUnitTY_Location";
            this.lbl_BreakUnitTY_Location.Size = new System.Drawing.Size(84, 21);
            this.lbl_BreakUnitTY_Location.TabIndex = 40;
            this.lbl_BreakUnitTY_Location.Text = "위치[mm]";
            // 
            // lbl_BreakUnitTY_Speed
            // 
            this.lbl_BreakUnitTY_Speed.AutoSize = true;
            this.lbl_BreakUnitTY_Speed.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_BreakUnitTY_Speed.ForeColor = System.Drawing.Color.White;
            this.lbl_BreakUnitTY_Speed.Location = new System.Drawing.Point(1179, 275);
            this.lbl_BreakUnitTY_Speed.Name = "lbl_BreakUnitTY_Speed";
            this.lbl_BreakUnitTY_Speed.Size = new System.Drawing.Size(115, 21);
            this.lbl_BreakUnitTY_Speed.TabIndex = 39;
            this.lbl_BreakUnitTY_Speed.Text = "속도[mm/sec]";
            // 
            // lbl_BreakUnitTY_SelectedShaft
            // 
            this.lbl_BreakUnitTY_SelectedShaft.AutoSize = true;
            this.lbl_BreakUnitTY_SelectedShaft.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_BreakUnitTY_SelectedShaft.ForeColor = System.Drawing.Color.White;
            this.lbl_BreakUnitTY_SelectedShaft.Location = new System.Drawing.Point(805, 274);
            this.lbl_BreakUnitTY_SelectedShaft.Name = "lbl_BreakUnitTY_SelectedShaft";
            this.lbl_BreakUnitTY_SelectedShaft.Size = new System.Drawing.Size(80, 21);
            this.lbl_BreakUnitTY_SelectedShaft.TabIndex = 38;
            this.lbl_BreakUnitTY_SelectedShaft.Text = "선택된 축";
            // 
            // txtBreakUnitTY_LocationSetting
            // 
            this.txtBreakUnitTY_LocationSetting.BackColor = System.Drawing.SystemColors.Highlight;
            this.txtBreakUnitTY_LocationSetting.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.txtBreakUnitTY_LocationSetting.ForeColor = System.Drawing.Color.White;
            this.txtBreakUnitTY_LocationSetting.Location = new System.Drawing.Point(792, 223);
            this.txtBreakUnitTY_LocationSetting.Name = "txtBreakUnitTY_LocationSetting";
            this.txtBreakUnitTY_LocationSetting.ReadOnly = true;
            this.txtBreakUnitTY_LocationSetting.Size = new System.Drawing.Size(89, 27);
            this.txtBreakUnitTY_LocationSetting.TabIndex = 37;
            this.txtBreakUnitTY_LocationSetting.Text = "위치값 설정";
            this.txtBreakUnitTY_LocationSetting.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbl_BreakUnitTY_CurrentLocation
            // 
            this.lbl_BreakUnitTY_CurrentLocation.AutoSize = true;
            this.lbl_BreakUnitTY_CurrentLocation.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_BreakUnitTY_CurrentLocation.ForeColor = System.Drawing.Color.White;
            this.lbl_BreakUnitTY_CurrentLocation.Location = new System.Drawing.Point(975, 274);
            this.lbl_BreakUnitTY_CurrentLocation.Name = "lbl_BreakUnitTY_CurrentLocation";
            this.lbl_BreakUnitTY_CurrentLocation.Size = new System.Drawing.Size(116, 21);
            this.lbl_BreakUnitTY_CurrentLocation.TabIndex = 36;
            this.lbl_BreakUnitTY_CurrentLocation.Text = "현재위치[mm]";
            // 
            // btnBreakUnitTYGrabSwitch3
            // 
            this.btnBreakUnitTYGrabSwitch3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYGrabSwitch3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYGrabSwitch3.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYGrabSwitch3.Location = new System.Drawing.Point(1514, 6);
            this.btnBreakUnitTYGrabSwitch3.Name = "btnBreakUnitTYGrabSwitch3";
            this.btnBreakUnitTYGrabSwitch3.Size = new System.Drawing.Size(172, 23);
            this.btnBreakUnitTYGrabSwitch3.TabIndex = 35;
            this.btnBreakUnitTYGrabSwitch3.Text = "Grab Switch 3";
            this.btnBreakUnitTYGrabSwitch3.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYGrabSwitch2
            // 
            this.btnBreakUnitTYGrabSwitch2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYGrabSwitch2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYGrabSwitch2.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYGrabSwitch2.Location = new System.Drawing.Point(1336, 6);
            this.btnBreakUnitTYGrabSwitch2.Name = "btnBreakUnitTYGrabSwitch2";
            this.btnBreakUnitTYGrabSwitch2.Size = new System.Drawing.Size(172, 23);
            this.btnBreakUnitTYGrabSwitch2.TabIndex = 34;
            this.btnBreakUnitTYGrabSwitch2.Text = "Grab Switch 2";
            this.btnBreakUnitTYGrabSwitch2.UseVisualStyleBackColor = false;
            // 
            // btnBreakUnitTYGrabSwitch1
            // 
            this.btnBreakUnitTYGrabSwitch1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBreakUnitTYGrabSwitch1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakUnitTYGrabSwitch1.ForeColor = System.Drawing.Color.White;
            this.btnBreakUnitTYGrabSwitch1.Location = new System.Drawing.Point(1158, 6);
            this.btnBreakUnitTYGrabSwitch1.Name = "btnBreakUnitTYGrabSwitch1";
            this.btnBreakUnitTYGrabSwitch1.Size = new System.Drawing.Size(172, 23);
            this.btnBreakUnitTYGrabSwitch1.TabIndex = 33;
            this.btnBreakUnitTYGrabSwitch1.Text = "Grab Switch 1";
            this.btnBreakUnitTYGrabSwitch1.UseVisualStyleBackColor = false;
            // 
            // lvBreakUnitTY
            // 
            this.lvBreakUnitTY.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.col_BreakUnitTY_NamebyLocation,
            this.col_BreakUnitTY_Shaft,
            this.col_BreakUnitTY_Location,
            this.col_BreakUnitTY_Speed});
            this.lvBreakUnitTY.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lvBreakUnitTY.GridLines = true;
            this.lvBreakUnitTY.Location = new System.Drawing.Point(17, 16);
            this.lvBreakUnitTY.Name = "lvBreakUnitTY";
            this.lvBreakUnitTY.Size = new System.Drawing.Size(758, 734);
            this.lvBreakUnitTY.TabIndex = 32;
            this.lvBreakUnitTY.UseCompatibleStateImageBehavior = false;
            this.lvBreakUnitTY.View = System.Windows.Forms.View.Details;
            // 
            // col_BreakUnitTY_NamebyLocation
            // 
            this.col_BreakUnitTY_NamebyLocation.Text = "위치별 명칭";
            this.col_BreakUnitTY_NamebyLocation.Width = 430;
            // 
            // col_BreakUnitTY_Shaft
            // 
            this.col_BreakUnitTY_Shaft.Text = "축";
            this.col_BreakUnitTY_Shaft.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // col_BreakUnitTY_Location
            // 
            this.col_BreakUnitTY_Location.Text = "위치[mm]";
            this.col_BreakUnitTY_Location.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_BreakUnitTY_Location.Width = 125;
            // 
            // col_BreakUnitTY_Speed
            // 
            this.col_BreakUnitTY_Speed.Text = "속도[mm/s]";
            this.col_BreakUnitTY_Speed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_BreakUnitTY_Speed.Width = 125;
            // 
            // ajin_Setting_BreakUnitTY
            // 
            this.ajin_Setting_BreakUnitTY.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_BreakUnitTY.Location = new System.Drawing.Point(792, 34);
            this.ajin_Setting_BreakUnitTY.Name = "ajin_Setting_BreakUnitTY";
            this.ajin_Setting_BreakUnitTY.Servo = null;
            this.ajin_Setting_BreakUnitTY.Size = new System.Drawing.Size(903, 183);
            this.ajin_Setting_BreakUnitTY.TabIndex = 31;
            // 
            // tp_UnloaderTransfer
            // 
            this.tp_UnloaderTransfer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tp_UnloaderTransfer.Controls.Add(this.gxtUnloaderTransfer_Move);
            this.tp_UnloaderTransfer.Controls.Add(this.gxtUnloaderTransfer_B);
            this.tp_UnloaderTransfer.Controls.Add(this.gxtUnloaderTransfer_A);
            this.tp_UnloaderTransfer.Controls.Add(this.btnUnloaderTransferSave);
            this.tp_UnloaderTransfer.Controls.Add(this.btnUnloaderTransferSpeedSetting);
            this.tp_UnloaderTransfer.Controls.Add(this.btnUnloaderTransferAllSetting);
            this.tp_UnloaderTransfer.Controls.Add(this.btnUnloaderTransferLocationSetting);
            this.tp_UnloaderTransfer.Controls.Add(this.btnUnloaderTransferMoveLocation);
            this.tp_UnloaderTransfer.Controls.Add(this.txtUnloaderTransferLocation);
            this.tp_UnloaderTransfer.Controls.Add(this.txtUnloaderTransferSpeed);
            this.tp_UnloaderTransfer.Controls.Add(this.txtUnloaderTransferCurrentLocation);
            this.tp_UnloaderTransfer.Controls.Add(this.txtUnloaderTransferSelectedShaft);
            this.tp_UnloaderTransfer.Controls.Add(this.lbl_UnloaderTransfer_Location);
            this.tp_UnloaderTransfer.Controls.Add(this.lbl_UnloaderTransfer_Speed);
            this.tp_UnloaderTransfer.Controls.Add(this.lbl_UnloaderTransfer_SeletedShaft);
            this.tp_UnloaderTransfer.Controls.Add(this.txtUnloaderTransfer_LocationSetting);
            this.tp_UnloaderTransfer.Controls.Add(this.lbl_UnloaderTransfer_CurrentLocation);
            this.tp_UnloaderTransfer.Controls.Add(this.btnUnloaderTransferGrabSwitch3);
            this.tp_UnloaderTransfer.Controls.Add(this.btnUnloaderTransferGrabSwitch2);
            this.tp_UnloaderTransfer.Controls.Add(this.btnUnloaderTransferGrabSwitch1);
            this.tp_UnloaderTransfer.Controls.Add(this.lvUnloaderTransfer);
            this.tp_UnloaderTransfer.Controls.Add(this.ajin_Setting_UnloaderTransfer);
            this.tp_UnloaderTransfer.Location = new System.Drawing.Point(4, 34);
            this.tp_UnloaderTransfer.Name = "tp_UnloaderTransfer";
            this.tp_UnloaderTransfer.Size = new System.Drawing.Size(1726, 816);
            this.tp_UnloaderTransfer.TabIndex = 7;
            this.tp_UnloaderTransfer.Text = "언로더 트랜스퍼";
            // 
            // gxtUnloaderTransfer_Move
            // 
            this.gxtUnloaderTransfer_Move.Controls.Add(this.gxtUnloaderTransfer_MoveB);
            this.gxtUnloaderTransfer_Move.Controls.Add(this.gxtUnloaderTransfer_MoveA);
            this.gxtUnloaderTransfer_Move.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtUnloaderTransfer_Move.ForeColor = System.Drawing.Color.White;
            this.gxtUnloaderTransfer_Move.Location = new System.Drawing.Point(792, 595);
            this.gxtUnloaderTransfer_Move.Name = "gxtUnloaderTransfer_Move";
            this.gxtUnloaderTransfer_Move.Size = new System.Drawing.Size(904, 179);
            this.gxtUnloaderTransfer_Move.TabIndex = 53;
            this.gxtUnloaderTransfer_Move.TabStop = false;
            this.gxtUnloaderTransfer_Move.Text = "     이동     ";
            // 
            // gxtUnloaderTransfer_MoveB
            // 
            this.gxtUnloaderTransfer_MoveB.Controls.Add(this.tableLayoutPanel61);
            this.gxtUnloaderTransfer_MoveB.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtUnloaderTransfer_MoveB.ForeColor = System.Drawing.Color.White;
            this.gxtUnloaderTransfer_MoveB.Location = new System.Drawing.Point(455, 28);
            this.gxtUnloaderTransfer_MoveB.Name = "gxtUnloaderTransfer_MoveB";
            this.gxtUnloaderTransfer_MoveB.Size = new System.Drawing.Size(443, 145);
            this.gxtUnloaderTransfer_MoveB.TabIndex = 31;
            this.gxtUnloaderTransfer_MoveB.TabStop = false;
            this.gxtUnloaderTransfer_MoveB.Text = "     B    ";
            // 
            // tableLayoutPanel61
            // 
            this.tableLayoutPanel61.ColumnCount = 1;
            this.tableLayoutPanel61.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel61.Controls.Add(this.tableLayoutPanel62, 0, 1);
            this.tableLayoutPanel61.Controls.Add(this.tableLayoutPanel63, 0, 0);
            this.tableLayoutPanel61.Location = new System.Drawing.Point(3, 27);
            this.tableLayoutPanel61.Name = "tableLayoutPanel61";
            this.tableLayoutPanel61.RowCount = 2;
            this.tableLayoutPanel61.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel61.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel61.Size = new System.Drawing.Size(434, 112);
            this.tableLayoutPanel61.TabIndex = 55;
            // 
            // tableLayoutPanel62
            // 
            this.tableLayoutPanel62.ColumnCount = 6;
            this.tableLayoutPanel62.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel62.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel62.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel62.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel62.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel62.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel62.Controls.Add(this.btnUnloaderTransferMoveBT40Angle, 0, 0);
            this.tableLayoutPanel62.Controls.Add(this.btnUnloaderTransferMoveBT4P90Angle, 0, 0);
            this.tableLayoutPanel62.Controls.Add(this.btnUnloaderTransferMoveBT4M90Angle, 0, 0);
            this.tableLayoutPanel62.Controls.Add(this.btnUnloaderTransferMoveBT30Angle, 0, 0);
            this.tableLayoutPanel62.Controls.Add(this.btnUnloaderTransferMoveBT3P90Angle, 0, 0);
            this.tableLayoutPanel62.Controls.Add(this.btnUnloaderTransferMoveBT3M90Angle, 0, 0);
            this.tableLayoutPanel62.Location = new System.Drawing.Point(3, 59);
            this.tableLayoutPanel62.Name = "tableLayoutPanel62";
            this.tableLayoutPanel62.RowCount = 1;
            this.tableLayoutPanel62.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel62.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel62.Size = new System.Drawing.Size(428, 50);
            this.tableLayoutPanel62.TabIndex = 56;
            // 
            // btnUnloaderTransferMoveBT40Angle
            // 
            this.btnUnloaderTransferMoveBT40Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferMoveBT40Angle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferMoveBT40Angle.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferMoveBT40Angle.Location = new System.Drawing.Point(216, 3);
            this.btnUnloaderTransferMoveBT40Angle.Name = "btnUnloaderTransferMoveBT40Angle";
            this.btnUnloaderTransferMoveBT40Angle.Size = new System.Drawing.Size(65, 44);
            this.btnUnloaderTransferMoveBT40Angle.TabIndex = 61;
            this.btnUnloaderTransferMoveBT40Angle.Text = "T4\n0도";
            this.btnUnloaderTransferMoveBT40Angle.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferMoveBT4P90Angle
            // 
            this.btnUnloaderTransferMoveBT4P90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferMoveBT4P90Angle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferMoveBT4P90Angle.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferMoveBT4P90Angle.Location = new System.Drawing.Point(287, 3);
            this.btnUnloaderTransferMoveBT4P90Angle.Name = "btnUnloaderTransferMoveBT4P90Angle";
            this.btnUnloaderTransferMoveBT4P90Angle.Size = new System.Drawing.Size(65, 44);
            this.btnUnloaderTransferMoveBT4P90Angle.TabIndex = 60;
            this.btnUnloaderTransferMoveBT4P90Angle.Text = "T4\r\n+90도";
            this.btnUnloaderTransferMoveBT4P90Angle.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferMoveBT4M90Angle
            // 
            this.btnUnloaderTransferMoveBT4M90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferMoveBT4M90Angle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferMoveBT4M90Angle.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferMoveBT4M90Angle.Location = new System.Drawing.Point(358, 3);
            this.btnUnloaderTransferMoveBT4M90Angle.Name = "btnUnloaderTransferMoveBT4M90Angle";
            this.btnUnloaderTransferMoveBT4M90Angle.Size = new System.Drawing.Size(67, 44);
            this.btnUnloaderTransferMoveBT4M90Angle.TabIndex = 59;
            this.btnUnloaderTransferMoveBT4M90Angle.Text = "T4\r\n-90도";
            this.btnUnloaderTransferMoveBT4M90Angle.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferMoveBT30Angle
            // 
            this.btnUnloaderTransferMoveBT30Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferMoveBT30Angle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferMoveBT30Angle.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferMoveBT30Angle.Location = new System.Drawing.Point(3, 3);
            this.btnUnloaderTransferMoveBT30Angle.Name = "btnUnloaderTransferMoveBT30Angle";
            this.btnUnloaderTransferMoveBT30Angle.Size = new System.Drawing.Size(65, 44);
            this.btnUnloaderTransferMoveBT30Angle.TabIndex = 58;
            this.btnUnloaderTransferMoveBT30Angle.Text = "T3\r\n0도";
            this.btnUnloaderTransferMoveBT30Angle.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferMoveBT3P90Angle
            // 
            this.btnUnloaderTransferMoveBT3P90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferMoveBT3P90Angle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferMoveBT3P90Angle.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferMoveBT3P90Angle.Location = new System.Drawing.Point(74, 3);
            this.btnUnloaderTransferMoveBT3P90Angle.Name = "btnUnloaderTransferMoveBT3P90Angle";
            this.btnUnloaderTransferMoveBT3P90Angle.Size = new System.Drawing.Size(65, 44);
            this.btnUnloaderTransferMoveBT3P90Angle.TabIndex = 57;
            this.btnUnloaderTransferMoveBT3P90Angle.Text = "T3\r\n+90도";
            this.btnUnloaderTransferMoveBT3P90Angle.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferMoveBT3M90Angle
            // 
            this.btnUnloaderTransferMoveBT3M90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferMoveBT3M90Angle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferMoveBT3M90Angle.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferMoveBT3M90Angle.Location = new System.Drawing.Point(145, 3);
            this.btnUnloaderTransferMoveBT3M90Angle.Name = "btnUnloaderTransferMoveBT3M90Angle";
            this.btnUnloaderTransferMoveBT3M90Angle.Size = new System.Drawing.Size(65, 44);
            this.btnUnloaderTransferMoveBT3M90Angle.TabIndex = 56;
            this.btnUnloaderTransferMoveBT3M90Angle.Text = "T3\r\n-90도";
            this.btnUnloaderTransferMoveBT3M90Angle.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel63
            // 
            this.tableLayoutPanel63.ColumnCount = 5;
            this.tableLayoutPanel63.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel63.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel63.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel63.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel63.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel63.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel63.Controls.Add(this.btnUnloaderTransferMoveBBStage, 0, 0);
            this.tableLayoutPanel63.Controls.Add(this.btnUnloaderTransferMoveBBUnloading, 4, 0);
            this.tableLayoutPanel63.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel63.Name = "tableLayoutPanel63";
            this.tableLayoutPanel63.RowCount = 1;
            this.tableLayoutPanel63.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel63.Size = new System.Drawing.Size(428, 50);
            this.tableLayoutPanel63.TabIndex = 55;
            // 
            // btnUnloaderTransferMoveBBStage
            // 
            this.btnUnloaderTransferMoveBBStage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferMoveBBStage.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferMoveBBStage.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferMoveBBStage.Location = new System.Drawing.Point(3, 3);
            this.btnUnloaderTransferMoveBBStage.Name = "btnUnloaderTransferMoveBBStage";
            this.btnUnloaderTransferMoveBBStage.Size = new System.Drawing.Size(79, 44);
            this.btnUnloaderTransferMoveBBStage.TabIndex = 55;
            this.btnUnloaderTransferMoveBBStage.Text = "B 스테이지";
            this.btnUnloaderTransferMoveBBStage.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferMoveBBUnloading
            // 
            this.btnUnloaderTransferMoveBBUnloading.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferMoveBBUnloading.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferMoveBBUnloading.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferMoveBBUnloading.Location = new System.Drawing.Point(343, 3);
            this.btnUnloaderTransferMoveBBUnloading.Name = "btnUnloaderTransferMoveBBUnloading";
            this.btnUnloaderTransferMoveBBUnloading.Size = new System.Drawing.Size(82, 44);
            this.btnUnloaderTransferMoveBBUnloading.TabIndex = 56;
            this.btnUnloaderTransferMoveBBUnloading.Text = "B 언로딩";
            this.btnUnloaderTransferMoveBBUnloading.UseVisualStyleBackColor = false;
            // 
            // gxtUnloaderTransfer_MoveA
            // 
            this.gxtUnloaderTransfer_MoveA.Controls.Add(this.tableLayoutPanel58);
            this.gxtUnloaderTransfer_MoveA.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtUnloaderTransfer_MoveA.ForeColor = System.Drawing.Color.White;
            this.gxtUnloaderTransfer_MoveA.Location = new System.Drawing.Point(6, 28);
            this.gxtUnloaderTransfer_MoveA.Name = "gxtUnloaderTransfer_MoveA";
            this.gxtUnloaderTransfer_MoveA.Size = new System.Drawing.Size(443, 145);
            this.gxtUnloaderTransfer_MoveA.TabIndex = 30;
            this.gxtUnloaderTransfer_MoveA.TabStop = false;
            this.gxtUnloaderTransfer_MoveA.Text = "     A    ";
            // 
            // tableLayoutPanel58
            // 
            this.tableLayoutPanel58.ColumnCount = 1;
            this.tableLayoutPanel58.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel58.Controls.Add(this.tableLayoutPanel60, 0, 1);
            this.tableLayoutPanel58.Controls.Add(this.tableLayoutPanel59, 0, 0);
            this.tableLayoutPanel58.Location = new System.Drawing.Point(3, 27);
            this.tableLayoutPanel58.Name = "tableLayoutPanel58";
            this.tableLayoutPanel58.RowCount = 2;
            this.tableLayoutPanel58.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel58.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel58.Size = new System.Drawing.Size(434, 112);
            this.tableLayoutPanel58.TabIndex = 54;
            // 
            // tableLayoutPanel60
            // 
            this.tableLayoutPanel60.ColumnCount = 6;
            this.tableLayoutPanel60.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel60.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel60.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel60.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel60.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel60.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel60.Controls.Add(this.btnUnloaderTransferMoveAT20Angle, 0, 0);
            this.tableLayoutPanel60.Controls.Add(this.btnUnloaderTransferMoveAT2P90Angle, 0, 0);
            this.tableLayoutPanel60.Controls.Add(this.btnUnloaderTransferMoveAT2M90Angle, 0, 0);
            this.tableLayoutPanel60.Controls.Add(this.btnUnloaderTransferMoveAT10Angle, 0, 0);
            this.tableLayoutPanel60.Controls.Add(this.btnUnloaderTransferMoveAT1P90Angle, 0, 0);
            this.tableLayoutPanel60.Controls.Add(this.btnUnloaderTransferMoveAT1M90Angle, 0, 0);
            this.tableLayoutPanel60.Location = new System.Drawing.Point(3, 59);
            this.tableLayoutPanel60.Name = "tableLayoutPanel60";
            this.tableLayoutPanel60.RowCount = 1;
            this.tableLayoutPanel60.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel60.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel60.Size = new System.Drawing.Size(428, 50);
            this.tableLayoutPanel60.TabIndex = 56;
            // 
            // btnUnloaderTransferMoveAT20Angle
            // 
            this.btnUnloaderTransferMoveAT20Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferMoveAT20Angle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferMoveAT20Angle.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferMoveAT20Angle.Location = new System.Drawing.Point(216, 3);
            this.btnUnloaderTransferMoveAT20Angle.Name = "btnUnloaderTransferMoveAT20Angle";
            this.btnUnloaderTransferMoveAT20Angle.Size = new System.Drawing.Size(65, 44);
            this.btnUnloaderTransferMoveAT20Angle.TabIndex = 61;
            this.btnUnloaderTransferMoveAT20Angle.Text = "T2\r\n0도";
            this.btnUnloaderTransferMoveAT20Angle.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferMoveAT2P90Angle
            // 
            this.btnUnloaderTransferMoveAT2P90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferMoveAT2P90Angle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferMoveAT2P90Angle.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferMoveAT2P90Angle.Location = new System.Drawing.Point(287, 3);
            this.btnUnloaderTransferMoveAT2P90Angle.Name = "btnUnloaderTransferMoveAT2P90Angle";
            this.btnUnloaderTransferMoveAT2P90Angle.Size = new System.Drawing.Size(65, 44);
            this.btnUnloaderTransferMoveAT2P90Angle.TabIndex = 60;
            this.btnUnloaderTransferMoveAT2P90Angle.Text = "T2\r\n+90도";
            this.btnUnloaderTransferMoveAT2P90Angle.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferMoveAT2M90Angle
            // 
            this.btnUnloaderTransferMoveAT2M90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferMoveAT2M90Angle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferMoveAT2M90Angle.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferMoveAT2M90Angle.Location = new System.Drawing.Point(358, 3);
            this.btnUnloaderTransferMoveAT2M90Angle.Name = "btnUnloaderTransferMoveAT2M90Angle";
            this.btnUnloaderTransferMoveAT2M90Angle.Size = new System.Drawing.Size(67, 44);
            this.btnUnloaderTransferMoveAT2M90Angle.TabIndex = 59;
            this.btnUnloaderTransferMoveAT2M90Angle.Text = "T2\r\n-90도";
            this.btnUnloaderTransferMoveAT2M90Angle.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferMoveAT10Angle
            // 
            this.btnUnloaderTransferMoveAT10Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferMoveAT10Angle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferMoveAT10Angle.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferMoveAT10Angle.Location = new System.Drawing.Point(3, 3);
            this.btnUnloaderTransferMoveAT10Angle.Name = "btnUnloaderTransferMoveAT10Angle";
            this.btnUnloaderTransferMoveAT10Angle.Size = new System.Drawing.Size(65, 44);
            this.btnUnloaderTransferMoveAT10Angle.TabIndex = 58;
            this.btnUnloaderTransferMoveAT10Angle.Text = "T1\r\n0도";
            this.btnUnloaderTransferMoveAT10Angle.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferMoveAT1P90Angle
            // 
            this.btnUnloaderTransferMoveAT1P90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferMoveAT1P90Angle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferMoveAT1P90Angle.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferMoveAT1P90Angle.Location = new System.Drawing.Point(74, 3);
            this.btnUnloaderTransferMoveAT1P90Angle.Name = "btnUnloaderTransferMoveAT1P90Angle";
            this.btnUnloaderTransferMoveAT1P90Angle.Size = new System.Drawing.Size(65, 44);
            this.btnUnloaderTransferMoveAT1P90Angle.TabIndex = 57;
            this.btnUnloaderTransferMoveAT1P90Angle.Text = "T1\r\n+90도";
            this.btnUnloaderTransferMoveAT1P90Angle.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferMoveAT1M90Angle
            // 
            this.btnUnloaderTransferMoveAT1M90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferMoveAT1M90Angle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferMoveAT1M90Angle.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferMoveAT1M90Angle.Location = new System.Drawing.Point(145, 3);
            this.btnUnloaderTransferMoveAT1M90Angle.Name = "btnUnloaderTransferMoveAT1M90Angle";
            this.btnUnloaderTransferMoveAT1M90Angle.Size = new System.Drawing.Size(65, 44);
            this.btnUnloaderTransferMoveAT1M90Angle.TabIndex = 56;
            this.btnUnloaderTransferMoveAT1M90Angle.Text = "T1\r\n-90도";
            this.btnUnloaderTransferMoveAT1M90Angle.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel59
            // 
            this.tableLayoutPanel59.ColumnCount = 5;
            this.tableLayoutPanel59.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel59.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel59.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel59.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel59.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel59.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel59.Controls.Add(this.btnUnloaderTransferMoveAAStage, 0, 0);
            this.tableLayoutPanel59.Controls.Add(this.btnUnloaderTransferMoveAAUnloading, 4, 0);
            this.tableLayoutPanel59.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel59.Name = "tableLayoutPanel59";
            this.tableLayoutPanel59.RowCount = 1;
            this.tableLayoutPanel59.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel59.Size = new System.Drawing.Size(428, 50);
            this.tableLayoutPanel59.TabIndex = 55;
            // 
            // btnUnloaderTransferMoveAAStage
            // 
            this.btnUnloaderTransferMoveAAStage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferMoveAAStage.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferMoveAAStage.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferMoveAAStage.Location = new System.Drawing.Point(3, 3);
            this.btnUnloaderTransferMoveAAStage.Name = "btnUnloaderTransferMoveAAStage";
            this.btnUnloaderTransferMoveAAStage.Size = new System.Drawing.Size(79, 44);
            this.btnUnloaderTransferMoveAAStage.TabIndex = 55;
            this.btnUnloaderTransferMoveAAStage.Text = "A 스테이지";
            this.btnUnloaderTransferMoveAAStage.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferMoveAAUnloading
            // 
            this.btnUnloaderTransferMoveAAUnloading.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferMoveAAUnloading.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferMoveAAUnloading.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferMoveAAUnloading.Location = new System.Drawing.Point(343, 3);
            this.btnUnloaderTransferMoveAAUnloading.Name = "btnUnloaderTransferMoveAAUnloading";
            this.btnUnloaderTransferMoveAAUnloading.Size = new System.Drawing.Size(82, 44);
            this.btnUnloaderTransferMoveAAUnloading.TabIndex = 56;
            this.btnUnloaderTransferMoveAAUnloading.Text = "A 언로딩";
            this.btnUnloaderTransferMoveAAUnloading.UseVisualStyleBackColor = false;
            // 
            // gxtUnloaderTransfer_B
            // 
            this.gxtUnloaderTransfer_B.Controls.Add(this.tableLayoutPanel4);
            this.gxtUnloaderTransfer_B.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtUnloaderTransfer_B.ForeColor = System.Drawing.Color.White;
            this.gxtUnloaderTransfer_B.Location = new System.Drawing.Point(1247, 410);
            this.gxtUnloaderTransfer_B.Name = "gxtUnloaderTransfer_B";
            this.gxtUnloaderTransfer_B.Size = new System.Drawing.Size(449, 179);
            this.gxtUnloaderTransfer_B.TabIndex = 52;
            this.gxtUnloaderTransfer_B.TabStop = false;
            this.gxtUnloaderTransfer_B.Text = "     B     ";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 4;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.Controls.Add(this.btnUnloaderTransferBPickerDestruction2, 3, 2);
            this.tableLayoutPanel4.Controls.Add(this.btnUnloaderTransferBPickerDestructionOn2, 2, 2);
            this.tableLayoutPanel4.Controls.Add(this.btnUnloaderTransferBPickerDestructionOff1, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.btnUnloaderTransferBPickerDestructionOn1, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.btnUnloaderTransferBPickerPneumaticOff2, 3, 1);
            this.tableLayoutPanel4.Controls.Add(this.btnUnloaderTransferBPickerPneumaticOn2, 2, 1);
            this.tableLayoutPanel4.Controls.Add(this.btnUnloaderTransferBPickerPneumaticOff1, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.btnUnloaderTransferBPickerPneumaticOn1, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.btnUnloaderTransferBPickerDown2, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnUnloaderTransferBPickerUp2, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnUnloaderTransferBPickerDown1, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnUnloaderTransferBPickerUp1, 0, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 3;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(437, 145);
            this.tableLayoutPanel4.TabIndex = 2;
            // 
            // btnUnloaderTransferBPickerDestruction2
            // 
            this.btnUnloaderTransferBPickerDestruction2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferBPickerDestruction2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferBPickerDestruction2.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferBPickerDestruction2.Location = new System.Drawing.Point(330, 99);
            this.btnUnloaderTransferBPickerDestruction2.Name = "btnUnloaderTransferBPickerDestruction2";
            this.btnUnloaderTransferBPickerDestruction2.Size = new System.Drawing.Size(103, 42);
            this.btnUnloaderTransferBPickerDestruction2.TabIndex = 65;
            this.btnUnloaderTransferBPickerDestruction2.Text = "피커\r\n파기 오프";
            this.btnUnloaderTransferBPickerDestruction2.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferBPickerDestructionOn2
            // 
            this.btnUnloaderTransferBPickerDestructionOn2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferBPickerDestructionOn2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferBPickerDestructionOn2.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferBPickerDestructionOn2.Location = new System.Drawing.Point(221, 99);
            this.btnUnloaderTransferBPickerDestructionOn2.Name = "btnUnloaderTransferBPickerDestructionOn2";
            this.btnUnloaderTransferBPickerDestructionOn2.Size = new System.Drawing.Size(103, 42);
            this.btnUnloaderTransferBPickerDestructionOn2.TabIndex = 64;
            this.btnUnloaderTransferBPickerDestructionOn2.Text = "피커\r\n파기 온";
            this.btnUnloaderTransferBPickerDestructionOn2.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferBPickerDestructionOff1
            // 
            this.btnUnloaderTransferBPickerDestructionOff1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferBPickerDestructionOff1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferBPickerDestructionOff1.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferBPickerDestructionOff1.Location = new System.Drawing.Point(112, 99);
            this.btnUnloaderTransferBPickerDestructionOff1.Name = "btnUnloaderTransferBPickerDestructionOff1";
            this.btnUnloaderTransferBPickerDestructionOff1.Size = new System.Drawing.Size(103, 42);
            this.btnUnloaderTransferBPickerDestructionOff1.TabIndex = 63;
            this.btnUnloaderTransferBPickerDestructionOff1.Text = "피커\r\n파기 오프";
            this.btnUnloaderTransferBPickerDestructionOff1.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferBPickerDestructionOn1
            // 
            this.btnUnloaderTransferBPickerDestructionOn1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferBPickerDestructionOn1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferBPickerDestructionOn1.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferBPickerDestructionOn1.Location = new System.Drawing.Point(3, 99);
            this.btnUnloaderTransferBPickerDestructionOn1.Name = "btnUnloaderTransferBPickerDestructionOn1";
            this.btnUnloaderTransferBPickerDestructionOn1.Size = new System.Drawing.Size(103, 42);
            this.btnUnloaderTransferBPickerDestructionOn1.TabIndex = 62;
            this.btnUnloaderTransferBPickerDestructionOn1.Text = "피커\r\n파기 온";
            this.btnUnloaderTransferBPickerDestructionOn1.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferBPickerPneumaticOff2
            // 
            this.btnUnloaderTransferBPickerPneumaticOff2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferBPickerPneumaticOff2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferBPickerPneumaticOff2.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferBPickerPneumaticOff2.Location = new System.Drawing.Point(330, 51);
            this.btnUnloaderTransferBPickerPneumaticOff2.Name = "btnUnloaderTransferBPickerPneumaticOff2";
            this.btnUnloaderTransferBPickerPneumaticOff2.Size = new System.Drawing.Size(103, 42);
            this.btnUnloaderTransferBPickerPneumaticOff2.TabIndex = 61;
            this.btnUnloaderTransferBPickerPneumaticOff2.Text = "피커\r\n공압 오프";
            this.btnUnloaderTransferBPickerPneumaticOff2.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferBPickerPneumaticOn2
            // 
            this.btnUnloaderTransferBPickerPneumaticOn2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferBPickerPneumaticOn2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferBPickerPneumaticOn2.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferBPickerPneumaticOn2.Location = new System.Drawing.Point(221, 51);
            this.btnUnloaderTransferBPickerPneumaticOn2.Name = "btnUnloaderTransferBPickerPneumaticOn2";
            this.btnUnloaderTransferBPickerPneumaticOn2.Size = new System.Drawing.Size(103, 42);
            this.btnUnloaderTransferBPickerPneumaticOn2.TabIndex = 60;
            this.btnUnloaderTransferBPickerPneumaticOn2.Text = "피커\r\n공압 온";
            this.btnUnloaderTransferBPickerPneumaticOn2.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferBPickerPneumaticOff1
            // 
            this.btnUnloaderTransferBPickerPneumaticOff1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferBPickerPneumaticOff1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferBPickerPneumaticOff1.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferBPickerPneumaticOff1.Location = new System.Drawing.Point(112, 51);
            this.btnUnloaderTransferBPickerPneumaticOff1.Name = "btnUnloaderTransferBPickerPneumaticOff1";
            this.btnUnloaderTransferBPickerPneumaticOff1.Size = new System.Drawing.Size(103, 42);
            this.btnUnloaderTransferBPickerPneumaticOff1.TabIndex = 59;
            this.btnUnloaderTransferBPickerPneumaticOff1.Text = "피커\r\n공압 오프";
            this.btnUnloaderTransferBPickerPneumaticOff1.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferBPickerPneumaticOn1
            // 
            this.btnUnloaderTransferBPickerPneumaticOn1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferBPickerPneumaticOn1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferBPickerPneumaticOn1.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferBPickerPneumaticOn1.Location = new System.Drawing.Point(3, 51);
            this.btnUnloaderTransferBPickerPneumaticOn1.Name = "btnUnloaderTransferBPickerPneumaticOn1";
            this.btnUnloaderTransferBPickerPneumaticOn1.Size = new System.Drawing.Size(103, 42);
            this.btnUnloaderTransferBPickerPneumaticOn1.TabIndex = 58;
            this.btnUnloaderTransferBPickerPneumaticOn1.Text = "피커\r\n공압 온";
            this.btnUnloaderTransferBPickerPneumaticOn1.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferBPickerDown2
            // 
            this.btnUnloaderTransferBPickerDown2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferBPickerDown2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferBPickerDown2.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferBPickerDown2.Location = new System.Drawing.Point(330, 3);
            this.btnUnloaderTransferBPickerDown2.Name = "btnUnloaderTransferBPickerDown2";
            this.btnUnloaderTransferBPickerDown2.Size = new System.Drawing.Size(103, 42);
            this.btnUnloaderTransferBPickerDown2.TabIndex = 57;
            this.btnUnloaderTransferBPickerDown2.Text = "피커\r\n다운";
            this.btnUnloaderTransferBPickerDown2.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferBPickerUp2
            // 
            this.btnUnloaderTransferBPickerUp2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferBPickerUp2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferBPickerUp2.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferBPickerUp2.Location = new System.Drawing.Point(221, 3);
            this.btnUnloaderTransferBPickerUp2.Name = "btnUnloaderTransferBPickerUp2";
            this.btnUnloaderTransferBPickerUp2.Size = new System.Drawing.Size(103, 42);
            this.btnUnloaderTransferBPickerUp2.TabIndex = 56;
            this.btnUnloaderTransferBPickerUp2.Text = "피커\r\n업";
            this.btnUnloaderTransferBPickerUp2.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferBPickerDown1
            // 
            this.btnUnloaderTransferBPickerDown1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferBPickerDown1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferBPickerDown1.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferBPickerDown1.Location = new System.Drawing.Point(112, 3);
            this.btnUnloaderTransferBPickerDown1.Name = "btnUnloaderTransferBPickerDown1";
            this.btnUnloaderTransferBPickerDown1.Size = new System.Drawing.Size(103, 42);
            this.btnUnloaderTransferBPickerDown1.TabIndex = 55;
            this.btnUnloaderTransferBPickerDown1.Text = "피커\r\n다운";
            this.btnUnloaderTransferBPickerDown1.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferBPickerUp1
            // 
            this.btnUnloaderTransferBPickerUp1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferBPickerUp1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferBPickerUp1.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferBPickerUp1.Location = new System.Drawing.Point(3, 3);
            this.btnUnloaderTransferBPickerUp1.Name = "btnUnloaderTransferBPickerUp1";
            this.btnUnloaderTransferBPickerUp1.Size = new System.Drawing.Size(103, 42);
            this.btnUnloaderTransferBPickerUp1.TabIndex = 54;
            this.btnUnloaderTransferBPickerUp1.Text = "피커\r\n업";
            this.btnUnloaderTransferBPickerUp1.UseVisualStyleBackColor = false;
            // 
            // gxtUnloaderTransfer_A
            // 
            this.gxtUnloaderTransfer_A.Controls.Add(this.tableLayoutPanel3);
            this.gxtUnloaderTransfer_A.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtUnloaderTransfer_A.ForeColor = System.Drawing.Color.White;
            this.gxtUnloaderTransfer_A.Location = new System.Drawing.Point(792, 410);
            this.gxtUnloaderTransfer_A.Name = "gxtUnloaderTransfer_A";
            this.gxtUnloaderTransfer_A.Size = new System.Drawing.Size(449, 179);
            this.gxtUnloaderTransfer_A.TabIndex = 51;
            this.gxtUnloaderTransfer_A.TabStop = false;
            this.gxtUnloaderTransfer_A.Text = "     A    ";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.Controls.Add(this.btnUnloaderTransferAPickerDestructionOff2, 3, 2);
            this.tableLayoutPanel3.Controls.Add(this.btnUnloaderTransferAPickerDestructionOn2, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.btnUnloaderTransferAPickerDestructionOff1, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.btnUnloaderTransferAPickerDestructionOn1, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.btnUnloaderTransferAPickerPneumaticOff2, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.btnUnloaderTransferAPickerPneumaticOn2, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.btnUnloaderTransferAPickerPneumaticOff1, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.btnUnloaderTransferAPickerPneumaticOn1, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.btnUnloaderTransferAPickerDown2, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnUnloaderTransferAPickerUp2, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnUnloaderTransferAPickerDown1, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnUnloaderTransferAPickerUp1, 0, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(437, 145);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // btnUnloaderTransferAPickerDestructionOff2
            // 
            this.btnUnloaderTransferAPickerDestructionOff2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferAPickerDestructionOff2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferAPickerDestructionOff2.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferAPickerDestructionOff2.Location = new System.Drawing.Point(330, 99);
            this.btnUnloaderTransferAPickerDestructionOff2.Name = "btnUnloaderTransferAPickerDestructionOff2";
            this.btnUnloaderTransferAPickerDestructionOff2.Size = new System.Drawing.Size(103, 42);
            this.btnUnloaderTransferAPickerDestructionOff2.TabIndex = 65;
            this.btnUnloaderTransferAPickerDestructionOff2.Text = "피커\r\n파기 오프";
            this.btnUnloaderTransferAPickerDestructionOff2.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferAPickerDestructionOn2
            // 
            this.btnUnloaderTransferAPickerDestructionOn2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferAPickerDestructionOn2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferAPickerDestructionOn2.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferAPickerDestructionOn2.Location = new System.Drawing.Point(221, 99);
            this.btnUnloaderTransferAPickerDestructionOn2.Name = "btnUnloaderTransferAPickerDestructionOn2";
            this.btnUnloaderTransferAPickerDestructionOn2.Size = new System.Drawing.Size(103, 42);
            this.btnUnloaderTransferAPickerDestructionOn2.TabIndex = 64;
            this.btnUnloaderTransferAPickerDestructionOn2.Text = "피커\r\n파기 온";
            this.btnUnloaderTransferAPickerDestructionOn2.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferAPickerDestructionOff1
            // 
            this.btnUnloaderTransferAPickerDestructionOff1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferAPickerDestructionOff1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferAPickerDestructionOff1.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferAPickerDestructionOff1.Location = new System.Drawing.Point(112, 99);
            this.btnUnloaderTransferAPickerDestructionOff1.Name = "btnUnloaderTransferAPickerDestructionOff1";
            this.btnUnloaderTransferAPickerDestructionOff1.Size = new System.Drawing.Size(103, 42);
            this.btnUnloaderTransferAPickerDestructionOff1.TabIndex = 63;
            this.btnUnloaderTransferAPickerDestructionOff1.Text = "피커\r\n파기 오프";
            this.btnUnloaderTransferAPickerDestructionOff1.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferAPickerDestructionOn1
            // 
            this.btnUnloaderTransferAPickerDestructionOn1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferAPickerDestructionOn1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferAPickerDestructionOn1.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferAPickerDestructionOn1.Location = new System.Drawing.Point(3, 99);
            this.btnUnloaderTransferAPickerDestructionOn1.Name = "btnUnloaderTransferAPickerDestructionOn1";
            this.btnUnloaderTransferAPickerDestructionOn1.Size = new System.Drawing.Size(103, 42);
            this.btnUnloaderTransferAPickerDestructionOn1.TabIndex = 62;
            this.btnUnloaderTransferAPickerDestructionOn1.Text = "피커\r\n파기 온";
            this.btnUnloaderTransferAPickerDestructionOn1.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferAPickerPneumaticOff2
            // 
            this.btnUnloaderTransferAPickerPneumaticOff2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferAPickerPneumaticOff2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferAPickerPneumaticOff2.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferAPickerPneumaticOff2.Location = new System.Drawing.Point(330, 51);
            this.btnUnloaderTransferAPickerPneumaticOff2.Name = "btnUnloaderTransferAPickerPneumaticOff2";
            this.btnUnloaderTransferAPickerPneumaticOff2.Size = new System.Drawing.Size(103, 42);
            this.btnUnloaderTransferAPickerPneumaticOff2.TabIndex = 61;
            this.btnUnloaderTransferAPickerPneumaticOff2.Text = "피커\r\n공압 오프";
            this.btnUnloaderTransferAPickerPneumaticOff2.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferAPickerPneumaticOn2
            // 
            this.btnUnloaderTransferAPickerPneumaticOn2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferAPickerPneumaticOn2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferAPickerPneumaticOn2.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferAPickerPneumaticOn2.Location = new System.Drawing.Point(221, 51);
            this.btnUnloaderTransferAPickerPneumaticOn2.Name = "btnUnloaderTransferAPickerPneumaticOn2";
            this.btnUnloaderTransferAPickerPneumaticOn2.Size = new System.Drawing.Size(103, 42);
            this.btnUnloaderTransferAPickerPneumaticOn2.TabIndex = 60;
            this.btnUnloaderTransferAPickerPneumaticOn2.Text = "피커\r\n공압 온";
            this.btnUnloaderTransferAPickerPneumaticOn2.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferAPickerPneumaticOff1
            // 
            this.btnUnloaderTransferAPickerPneumaticOff1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferAPickerPneumaticOff1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferAPickerPneumaticOff1.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferAPickerPneumaticOff1.Location = new System.Drawing.Point(112, 51);
            this.btnUnloaderTransferAPickerPneumaticOff1.Name = "btnUnloaderTransferAPickerPneumaticOff1";
            this.btnUnloaderTransferAPickerPneumaticOff1.Size = new System.Drawing.Size(103, 42);
            this.btnUnloaderTransferAPickerPneumaticOff1.TabIndex = 59;
            this.btnUnloaderTransferAPickerPneumaticOff1.Text = "피커\r\n공압 오프";
            this.btnUnloaderTransferAPickerPneumaticOff1.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferAPickerPneumaticOn1
            // 
            this.btnUnloaderTransferAPickerPneumaticOn1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferAPickerPneumaticOn1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferAPickerPneumaticOn1.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferAPickerPneumaticOn1.Location = new System.Drawing.Point(3, 51);
            this.btnUnloaderTransferAPickerPneumaticOn1.Name = "btnUnloaderTransferAPickerPneumaticOn1";
            this.btnUnloaderTransferAPickerPneumaticOn1.Size = new System.Drawing.Size(103, 42);
            this.btnUnloaderTransferAPickerPneumaticOn1.TabIndex = 58;
            this.btnUnloaderTransferAPickerPneumaticOn1.Text = "피커\r\n공압 온";
            this.btnUnloaderTransferAPickerPneumaticOn1.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferAPickerDown2
            // 
            this.btnUnloaderTransferAPickerDown2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferAPickerDown2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferAPickerDown2.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferAPickerDown2.Location = new System.Drawing.Point(330, 3);
            this.btnUnloaderTransferAPickerDown2.Name = "btnUnloaderTransferAPickerDown2";
            this.btnUnloaderTransferAPickerDown2.Size = new System.Drawing.Size(103, 42);
            this.btnUnloaderTransferAPickerDown2.TabIndex = 57;
            this.btnUnloaderTransferAPickerDown2.Text = "피커\r\n다운";
            this.btnUnloaderTransferAPickerDown2.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferAPickerUp2
            // 
            this.btnUnloaderTransferAPickerUp2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferAPickerUp2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferAPickerUp2.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferAPickerUp2.Location = new System.Drawing.Point(221, 3);
            this.btnUnloaderTransferAPickerUp2.Name = "btnUnloaderTransferAPickerUp2";
            this.btnUnloaderTransferAPickerUp2.Size = new System.Drawing.Size(103, 42);
            this.btnUnloaderTransferAPickerUp2.TabIndex = 56;
            this.btnUnloaderTransferAPickerUp2.Text = "피커\r\n업";
            this.btnUnloaderTransferAPickerUp2.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferAPickerDown1
            // 
            this.btnUnloaderTransferAPickerDown1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferAPickerDown1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferAPickerDown1.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferAPickerDown1.Location = new System.Drawing.Point(112, 3);
            this.btnUnloaderTransferAPickerDown1.Name = "btnUnloaderTransferAPickerDown1";
            this.btnUnloaderTransferAPickerDown1.Size = new System.Drawing.Size(103, 42);
            this.btnUnloaderTransferAPickerDown1.TabIndex = 55;
            this.btnUnloaderTransferAPickerDown1.Text = "피커\r\n다운";
            this.btnUnloaderTransferAPickerDown1.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferAPickerUp1
            // 
            this.btnUnloaderTransferAPickerUp1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferAPickerUp1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferAPickerUp1.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferAPickerUp1.Location = new System.Drawing.Point(3, 3);
            this.btnUnloaderTransferAPickerUp1.Name = "btnUnloaderTransferAPickerUp1";
            this.btnUnloaderTransferAPickerUp1.Size = new System.Drawing.Size(103, 42);
            this.btnUnloaderTransferAPickerUp1.TabIndex = 54;
            this.btnUnloaderTransferAPickerUp1.Text = "피커\r\n업";
            this.btnUnloaderTransferAPickerUp1.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferSave
            // 
            this.btnUnloaderTransferSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferSave.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferSave.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferSave.Location = new System.Drawing.Point(1482, 783);
            this.btnUnloaderTransferSave.Name = "btnUnloaderTransferSave";
            this.btnUnloaderTransferSave.Size = new System.Drawing.Size(228, 28);
            this.btnUnloaderTransferSave.TabIndex = 50;
            this.btnUnloaderTransferSave.Text = "저장";
            this.btnUnloaderTransferSave.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferSpeedSetting
            // 
            this.btnUnloaderTransferSpeedSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferSpeedSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnUnloaderTransferSpeedSetting.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferSpeedSetting.Location = new System.Drawing.Point(1291, 315);
            this.btnUnloaderTransferSpeedSetting.Name = "btnUnloaderTransferSpeedSetting";
            this.btnUnloaderTransferSpeedSetting.Size = new System.Drawing.Size(90, 27);
            this.btnUnloaderTransferSpeedSetting.TabIndex = 48;
            this.btnUnloaderTransferSpeedSetting.Text = "속도 설정";
            this.btnUnloaderTransferSpeedSetting.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferAllSetting
            // 
            this.btnUnloaderTransferAllSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferAllSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnUnloaderTransferAllSetting.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferAllSetting.Location = new System.Drawing.Point(1387, 273);
            this.btnUnloaderTransferAllSetting.Name = "btnUnloaderTransferAllSetting";
            this.btnUnloaderTransferAllSetting.Size = new System.Drawing.Size(90, 27);
            this.btnUnloaderTransferAllSetting.TabIndex = 47;
            this.btnUnloaderTransferAllSetting.Text = "전체 설정";
            this.btnUnloaderTransferAllSetting.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferLocationSetting
            // 
            this.btnUnloaderTransferLocationSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferLocationSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnUnloaderTransferLocationSetting.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferLocationSetting.Location = new System.Drawing.Point(1071, 315);
            this.btnUnloaderTransferLocationSetting.Name = "btnUnloaderTransferLocationSetting";
            this.btnUnloaderTransferLocationSetting.Size = new System.Drawing.Size(90, 27);
            this.btnUnloaderTransferLocationSetting.TabIndex = 46;
            this.btnUnloaderTransferLocationSetting.Text = "위치 설정";
            this.btnUnloaderTransferLocationSetting.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferMoveLocation
            // 
            this.btnUnloaderTransferMoveLocation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferMoveLocation.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnUnloaderTransferMoveLocation.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferMoveLocation.Location = new System.Drawing.Point(975, 315);
            this.btnUnloaderTransferMoveLocation.Name = "btnUnloaderTransferMoveLocation";
            this.btnUnloaderTransferMoveLocation.Size = new System.Drawing.Size(90, 27);
            this.btnUnloaderTransferMoveLocation.TabIndex = 45;
            this.btnUnloaderTransferMoveLocation.Text = "위치 이동";
            this.btnUnloaderTransferMoveLocation.UseVisualStyleBackColor = false;
            // 
            // txtUnloaderTransferLocation
            // 
            this.txtUnloaderTransferLocation.Font = new System.Drawing.Font("굴림", 13F);
            this.txtUnloaderTransferLocation.Location = new System.Drawing.Point(879, 316);
            this.txtUnloaderTransferLocation.Name = "txtUnloaderTransferLocation";
            this.txtUnloaderTransferLocation.Size = new System.Drawing.Size(90, 27);
            this.txtUnloaderTransferLocation.TabIndex = 44;
            // 
            // txtUnloaderTransferSpeed
            // 
            this.txtUnloaderTransferSpeed.Font = new System.Drawing.Font("굴림", 13F);
            this.txtUnloaderTransferSpeed.Location = new System.Drawing.Point(1291, 273);
            this.txtUnloaderTransferSpeed.Name = "txtUnloaderTransferSpeed";
            this.txtUnloaderTransferSpeed.Size = new System.Drawing.Size(90, 27);
            this.txtUnloaderTransferSpeed.TabIndex = 43;
            // 
            // txtUnloaderTransferCurrentLocation
            // 
            this.txtUnloaderTransferCurrentLocation.Font = new System.Drawing.Font("굴림", 13F);
            this.txtUnloaderTransferCurrentLocation.Location = new System.Drawing.Point(1084, 274);
            this.txtUnloaderTransferCurrentLocation.Name = "txtUnloaderTransferCurrentLocation";
            this.txtUnloaderTransferCurrentLocation.Size = new System.Drawing.Size(90, 27);
            this.txtUnloaderTransferCurrentLocation.TabIndex = 42;
            // 
            // txtUnloaderTransferSelectedShaft
            // 
            this.txtUnloaderTransferSelectedShaft.Font = new System.Drawing.Font("굴림", 13F);
            this.txtUnloaderTransferSelectedShaft.Location = new System.Drawing.Point(879, 273);
            this.txtUnloaderTransferSelectedShaft.Name = "txtUnloaderTransferSelectedShaft";
            this.txtUnloaderTransferSelectedShaft.Size = new System.Drawing.Size(90, 27);
            this.txtUnloaderTransferSelectedShaft.TabIndex = 41;
            // 
            // lbl_UnloaderTransfer_Location
            // 
            this.lbl_UnloaderTransfer_Location.AutoSize = true;
            this.lbl_UnloaderTransfer_Location.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_UnloaderTransfer_Location.ForeColor = System.Drawing.Color.White;
            this.lbl_UnloaderTransfer_Location.Location = new System.Drawing.Point(801, 316);
            this.lbl_UnloaderTransfer_Location.Name = "lbl_UnloaderTransfer_Location";
            this.lbl_UnloaderTransfer_Location.Size = new System.Drawing.Size(84, 21);
            this.lbl_UnloaderTransfer_Location.TabIndex = 40;
            this.lbl_UnloaderTransfer_Location.Text = "위치[mm]";
            // 
            // lbl_UnloaderTransfer_Speed
            // 
            this.lbl_UnloaderTransfer_Speed.AutoSize = true;
            this.lbl_UnloaderTransfer_Speed.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_UnloaderTransfer_Speed.ForeColor = System.Drawing.Color.White;
            this.lbl_UnloaderTransfer_Speed.Location = new System.Drawing.Point(1179, 275);
            this.lbl_UnloaderTransfer_Speed.Name = "lbl_UnloaderTransfer_Speed";
            this.lbl_UnloaderTransfer_Speed.Size = new System.Drawing.Size(115, 21);
            this.lbl_UnloaderTransfer_Speed.TabIndex = 39;
            this.lbl_UnloaderTransfer_Speed.Text = "속도[mm/sec]";
            // 
            // lbl_UnloaderTransfer_SeletedShaft
            // 
            this.lbl_UnloaderTransfer_SeletedShaft.AutoSize = true;
            this.lbl_UnloaderTransfer_SeletedShaft.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_UnloaderTransfer_SeletedShaft.ForeColor = System.Drawing.Color.White;
            this.lbl_UnloaderTransfer_SeletedShaft.Location = new System.Drawing.Point(805, 274);
            this.lbl_UnloaderTransfer_SeletedShaft.Name = "lbl_UnloaderTransfer_SeletedShaft";
            this.lbl_UnloaderTransfer_SeletedShaft.Size = new System.Drawing.Size(80, 21);
            this.lbl_UnloaderTransfer_SeletedShaft.TabIndex = 38;
            this.lbl_UnloaderTransfer_SeletedShaft.Text = "선택된 축";
            // 
            // txtUnloaderTransfer_LocationSetting
            // 
            this.txtUnloaderTransfer_LocationSetting.BackColor = System.Drawing.SystemColors.Highlight;
            this.txtUnloaderTransfer_LocationSetting.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.txtUnloaderTransfer_LocationSetting.ForeColor = System.Drawing.Color.White;
            this.txtUnloaderTransfer_LocationSetting.Location = new System.Drawing.Point(792, 223);
            this.txtUnloaderTransfer_LocationSetting.Name = "txtUnloaderTransfer_LocationSetting";
            this.txtUnloaderTransfer_LocationSetting.ReadOnly = true;
            this.txtUnloaderTransfer_LocationSetting.Size = new System.Drawing.Size(89, 27);
            this.txtUnloaderTransfer_LocationSetting.TabIndex = 37;
            this.txtUnloaderTransfer_LocationSetting.Text = "위치값 설정";
            this.txtUnloaderTransfer_LocationSetting.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbl_UnloaderTransfer_CurrentLocation
            // 
            this.lbl_UnloaderTransfer_CurrentLocation.AutoSize = true;
            this.lbl_UnloaderTransfer_CurrentLocation.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_UnloaderTransfer_CurrentLocation.ForeColor = System.Drawing.Color.White;
            this.lbl_UnloaderTransfer_CurrentLocation.Location = new System.Drawing.Point(975, 274);
            this.lbl_UnloaderTransfer_CurrentLocation.Name = "lbl_UnloaderTransfer_CurrentLocation";
            this.lbl_UnloaderTransfer_CurrentLocation.Size = new System.Drawing.Size(116, 21);
            this.lbl_UnloaderTransfer_CurrentLocation.TabIndex = 36;
            this.lbl_UnloaderTransfer_CurrentLocation.Text = "현재위치[mm]";
            // 
            // btnUnloaderTransferGrabSwitch3
            // 
            this.btnUnloaderTransferGrabSwitch3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferGrabSwitch3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferGrabSwitch3.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferGrabSwitch3.Location = new System.Drawing.Point(1514, 6);
            this.btnUnloaderTransferGrabSwitch3.Name = "btnUnloaderTransferGrabSwitch3";
            this.btnUnloaderTransferGrabSwitch3.Size = new System.Drawing.Size(172, 23);
            this.btnUnloaderTransferGrabSwitch3.TabIndex = 35;
            this.btnUnloaderTransferGrabSwitch3.Text = "Grab Switch 3";
            this.btnUnloaderTransferGrabSwitch3.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferGrabSwitch2
            // 
            this.btnUnloaderTransferGrabSwitch2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferGrabSwitch2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferGrabSwitch2.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferGrabSwitch2.Location = new System.Drawing.Point(1336, 6);
            this.btnUnloaderTransferGrabSwitch2.Name = "btnUnloaderTransferGrabSwitch2";
            this.btnUnloaderTransferGrabSwitch2.Size = new System.Drawing.Size(172, 23);
            this.btnUnloaderTransferGrabSwitch2.TabIndex = 34;
            this.btnUnloaderTransferGrabSwitch2.Text = "Grab Switch 2";
            this.btnUnloaderTransferGrabSwitch2.UseVisualStyleBackColor = false;
            // 
            // btnUnloaderTransferGrabSwitch1
            // 
            this.btnUnloaderTransferGrabSwitch1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUnloaderTransferGrabSwitch1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnUnloaderTransferGrabSwitch1.ForeColor = System.Drawing.Color.White;
            this.btnUnloaderTransferGrabSwitch1.Location = new System.Drawing.Point(1158, 6);
            this.btnUnloaderTransferGrabSwitch1.Name = "btnUnloaderTransferGrabSwitch1";
            this.btnUnloaderTransferGrabSwitch1.Size = new System.Drawing.Size(172, 23);
            this.btnUnloaderTransferGrabSwitch1.TabIndex = 33;
            this.btnUnloaderTransferGrabSwitch1.Text = "Grab Switch 1";
            this.btnUnloaderTransferGrabSwitch1.UseVisualStyleBackColor = false;
            // 
            // lvUnloaderTransfer
            // 
            this.lvUnloaderTransfer.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.col_UnloaderTransfer_NamebyLocation,
            this.col_UnloaderTransfer_Shaft,
            this.col_UnloaderTransfer_Location,
            this.col_UnloaderTransfer_Speed});
            this.lvUnloaderTransfer.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lvUnloaderTransfer.GridLines = true;
            this.lvUnloaderTransfer.Location = new System.Drawing.Point(17, 16);
            this.lvUnloaderTransfer.Name = "lvUnloaderTransfer";
            this.lvUnloaderTransfer.Size = new System.Drawing.Size(758, 734);
            this.lvUnloaderTransfer.TabIndex = 32;
            this.lvUnloaderTransfer.UseCompatibleStateImageBehavior = false;
            this.lvUnloaderTransfer.View = System.Windows.Forms.View.Details;
            // 
            // col_UnloaderTransfer_NamebyLocation
            // 
            this.col_UnloaderTransfer_NamebyLocation.Text = "위치별 명칭";
            this.col_UnloaderTransfer_NamebyLocation.Width = 430;
            // 
            // col_UnloaderTransfer_Shaft
            // 
            this.col_UnloaderTransfer_Shaft.Text = "축";
            this.col_UnloaderTransfer_Shaft.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // col_UnloaderTransfer_Location
            // 
            this.col_UnloaderTransfer_Location.Text = "위치[mm]";
            this.col_UnloaderTransfer_Location.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_UnloaderTransfer_Location.Width = 125;
            // 
            // col_UnloaderTransfer_Speed
            // 
            this.col_UnloaderTransfer_Speed.Text = "속도[mm/s]";
            this.col_UnloaderTransfer_Speed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_UnloaderTransfer_Speed.Width = 125;
            // 
            // ajin_Setting_UnloaderTransfer
            // 
            this.ajin_Setting_UnloaderTransfer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_UnloaderTransfer.Location = new System.Drawing.Point(792, 34);
            this.ajin_Setting_UnloaderTransfer.Name = "ajin_Setting_UnloaderTransfer";
            this.ajin_Setting_UnloaderTransfer.Servo = null;
            this.ajin_Setting_UnloaderTransfer.Size = new System.Drawing.Size(903, 183);
            this.ajin_Setting_UnloaderTransfer.TabIndex = 31;
            // 
            // tp_CameraUnit
            // 
            this.tp_CameraUnit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tp_CameraUnit.Controls.Add(this.gxtCameraUnit_Move);
            this.tp_CameraUnit.Controls.Add(this.btnCameraUnitSave);
            this.tp_CameraUnit.Controls.Add(this.btnCameraUnitSpeedSetting);
            this.tp_CameraUnit.Controls.Add(this.btnCameraUnitAllSetting);
            this.tp_CameraUnit.Controls.Add(this.btnCameraUnitLocationSetting);
            this.tp_CameraUnit.Controls.Add(this.btnCameraUnitMoveLocation);
            this.tp_CameraUnit.Controls.Add(this.txtCameraUnitLocation);
            this.tp_CameraUnit.Controls.Add(this.txtCameraUnitSpeed);
            this.tp_CameraUnit.Controls.Add(this.txtCameraUnitCurrentLocation);
            this.tp_CameraUnit.Controls.Add(this.txtCameraUnitSelectedShaft);
            this.tp_CameraUnit.Controls.Add(this.lbl_CameraUnit_Location);
            this.tp_CameraUnit.Controls.Add(this.lbl_CameraUnit_Speed);
            this.tp_CameraUnit.Controls.Add(this.lbl_CameraUnit_SelectedShaft);
            this.tp_CameraUnit.Controls.Add(this.txtCameraUnit_LocationSetting);
            this.tp_CameraUnit.Controls.Add(this.lbl_CameraUnit_CurrentLocation);
            this.tp_CameraUnit.Controls.Add(this.btnCameraUnitGrabSwitch3);
            this.tp_CameraUnit.Controls.Add(this.btnCameraUnitGrabSwitch2);
            this.tp_CameraUnit.Controls.Add(this.btnCameraUnitGrabSwitch1);
            this.tp_CameraUnit.Controls.Add(this.lvCameraUnit);
            this.tp_CameraUnit.Controls.Add(this.ajin_Setting_CameraUnit);
            this.tp_CameraUnit.Location = new System.Drawing.Point(4, 34);
            this.tp_CameraUnit.Name = "tp_CameraUnit";
            this.tp_CameraUnit.Size = new System.Drawing.Size(1726, 816);
            this.tp_CameraUnit.TabIndex = 8;
            this.tp_CameraUnit.Text = "카메라 유닛";
            // 
            // gxtCameraUnit_Move
            // 
            this.gxtCameraUnit_Move.Controls.Add(this.gxtCameraUnit_MoveB);
            this.gxtCameraUnit_Move.Controls.Add(this.gxtCameraUnit_MoveA);
            this.gxtCameraUnit_Move.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCameraUnit_Move.ForeColor = System.Drawing.Color.White;
            this.gxtCameraUnit_Move.Location = new System.Drawing.Point(792, 595);
            this.gxtCameraUnit_Move.Name = "gxtCameraUnit_Move";
            this.gxtCameraUnit_Move.Size = new System.Drawing.Size(904, 179);
            this.gxtCameraUnit_Move.TabIndex = 53;
            this.gxtCameraUnit_Move.TabStop = false;
            this.gxtCameraUnit_Move.Text = "     이동     ";
            // 
            // gxtCameraUnit_MoveB
            // 
            this.gxtCameraUnit_MoveB.Controls.Add(this.tableLayoutPanel65);
            this.gxtCameraUnit_MoveB.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCameraUnit_MoveB.ForeColor = System.Drawing.Color.White;
            this.gxtCameraUnit_MoveB.Location = new System.Drawing.Point(455, 28);
            this.gxtCameraUnit_MoveB.Name = "gxtCameraUnit_MoveB";
            this.gxtCameraUnit_MoveB.Size = new System.Drawing.Size(443, 145);
            this.gxtCameraUnit_MoveB.TabIndex = 31;
            this.gxtCameraUnit_MoveB.TabStop = false;
            this.gxtCameraUnit_MoveB.Text = "     B    ";
            // 
            // tableLayoutPanel65
            // 
            this.tableLayoutPanel65.ColumnCount = 3;
            this.tableLayoutPanel65.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel65.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel65.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel65.Controls.Add(this.btnCameraUnitMoveBInspectionMove4, 0, 0);
            this.tableLayoutPanel65.Controls.Add(this.btnCameraUnitMoveBInspectionMove3, 0, 0);
            this.tableLayoutPanel65.Controls.Add(this.btnCameraUnitMoveBMCRMove, 0, 0);
            this.tableLayoutPanel65.Location = new System.Drawing.Point(6, 27);
            this.tableLayoutPanel65.Name = "tableLayoutPanel65";
            this.tableLayoutPanel65.RowCount = 1;
            this.tableLayoutPanel65.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel65.Size = new System.Drawing.Size(431, 112);
            this.tableLayoutPanel65.TabIndex = 55;
            // 
            // btnCameraUnitMoveBInspectionMove4
            // 
            this.btnCameraUnitMoveBInspectionMove4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCameraUnitMoveBInspectionMove4.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btnCameraUnitMoveBInspectionMove4.ForeColor = System.Drawing.Color.White;
            this.btnCameraUnitMoveBInspectionMove4.Location = new System.Drawing.Point(289, 3);
            this.btnCameraUnitMoveBInspectionMove4.Name = "btnCameraUnitMoveBInspectionMove4";
            this.btnCameraUnitMoveBInspectionMove4.Size = new System.Drawing.Size(137, 106);
            this.btnCameraUnitMoveBInspectionMove4.TabIndex = 65;
            this.btnCameraUnitMoveBInspectionMove4.Text = "Inspection 위치\r\n이동 4";
            this.btnCameraUnitMoveBInspectionMove4.UseVisualStyleBackColor = false;
            // 
            // btnCameraUnitMoveBInspectionMove3
            // 
            this.btnCameraUnitMoveBInspectionMove3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCameraUnitMoveBInspectionMove3.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btnCameraUnitMoveBInspectionMove3.ForeColor = System.Drawing.Color.White;
            this.btnCameraUnitMoveBInspectionMove3.Location = new System.Drawing.Point(146, 3);
            this.btnCameraUnitMoveBInspectionMove3.Name = "btnCameraUnitMoveBInspectionMove3";
            this.btnCameraUnitMoveBInspectionMove3.Size = new System.Drawing.Size(137, 106);
            this.btnCameraUnitMoveBInspectionMove3.TabIndex = 64;
            this.btnCameraUnitMoveBInspectionMove3.Text = "Inspection 위치\r\n이동 3";
            this.btnCameraUnitMoveBInspectionMove3.UseVisualStyleBackColor = false;
            // 
            // btnCameraUnitMoveBMCRMove
            // 
            this.btnCameraUnitMoveBMCRMove.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCameraUnitMoveBMCRMove.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btnCameraUnitMoveBMCRMove.ForeColor = System.Drawing.Color.White;
            this.btnCameraUnitMoveBMCRMove.Location = new System.Drawing.Point(3, 3);
            this.btnCameraUnitMoveBMCRMove.Name = "btnCameraUnitMoveBMCRMove";
            this.btnCameraUnitMoveBMCRMove.Size = new System.Drawing.Size(137, 106);
            this.btnCameraUnitMoveBMCRMove.TabIndex = 63;
            this.btnCameraUnitMoveBMCRMove.Text = "MCR 위치\r\n이동";
            this.btnCameraUnitMoveBMCRMove.UseVisualStyleBackColor = false;
            // 
            // gxtCameraUnit_MoveA
            // 
            this.gxtCameraUnit_MoveA.Controls.Add(this.tableLayoutPanel64);
            this.gxtCameraUnit_MoveA.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCameraUnit_MoveA.ForeColor = System.Drawing.Color.White;
            this.gxtCameraUnit_MoveA.Location = new System.Drawing.Point(6, 28);
            this.gxtCameraUnit_MoveA.Name = "gxtCameraUnit_MoveA";
            this.gxtCameraUnit_MoveA.Size = new System.Drawing.Size(443, 145);
            this.gxtCameraUnit_MoveA.TabIndex = 30;
            this.gxtCameraUnit_MoveA.TabStop = false;
            this.gxtCameraUnit_MoveA.Text = "     A    ";
            // 
            // tableLayoutPanel64
            // 
            this.tableLayoutPanel64.ColumnCount = 3;
            this.tableLayoutPanel64.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel64.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel64.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel64.Controls.Add(this.btnCameraUnitMoveAInspectionMove2, 0, 0);
            this.tableLayoutPanel64.Controls.Add(this.btnCameraUnitMoveAInspectionMove1, 0, 0);
            this.tableLayoutPanel64.Controls.Add(this.btnCameraUnitMoveAMCRMove, 0, 0);
            this.tableLayoutPanel64.Location = new System.Drawing.Point(6, 27);
            this.tableLayoutPanel64.Name = "tableLayoutPanel64";
            this.tableLayoutPanel64.RowCount = 1;
            this.tableLayoutPanel64.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel64.Size = new System.Drawing.Size(431, 112);
            this.tableLayoutPanel64.TabIndex = 54;
            // 
            // btnCameraUnitMoveAInspectionMove2
            // 
            this.btnCameraUnitMoveAInspectionMove2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCameraUnitMoveAInspectionMove2.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btnCameraUnitMoveAInspectionMove2.ForeColor = System.Drawing.Color.White;
            this.btnCameraUnitMoveAInspectionMove2.Location = new System.Drawing.Point(289, 3);
            this.btnCameraUnitMoveAInspectionMove2.Name = "btnCameraUnitMoveAInspectionMove2";
            this.btnCameraUnitMoveAInspectionMove2.Size = new System.Drawing.Size(137, 106);
            this.btnCameraUnitMoveAInspectionMove2.TabIndex = 65;
            this.btnCameraUnitMoveAInspectionMove2.Text = "Inspection 위치\r\n이동 2";
            this.btnCameraUnitMoveAInspectionMove2.UseVisualStyleBackColor = false;
            // 
            // btnCameraUnitMoveAInspectionMove1
            // 
            this.btnCameraUnitMoveAInspectionMove1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCameraUnitMoveAInspectionMove1.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btnCameraUnitMoveAInspectionMove1.ForeColor = System.Drawing.Color.White;
            this.btnCameraUnitMoveAInspectionMove1.Location = new System.Drawing.Point(146, 3);
            this.btnCameraUnitMoveAInspectionMove1.Name = "btnCameraUnitMoveAInspectionMove1";
            this.btnCameraUnitMoveAInspectionMove1.Size = new System.Drawing.Size(137, 106);
            this.btnCameraUnitMoveAInspectionMove1.TabIndex = 64;
            this.btnCameraUnitMoveAInspectionMove1.Text = "Inspection 위치\r\n이동 1";
            this.btnCameraUnitMoveAInspectionMove1.UseVisualStyleBackColor = false;
            // 
            // btnCameraUnitMoveAMCRMove
            // 
            this.btnCameraUnitMoveAMCRMove.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCameraUnitMoveAMCRMove.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btnCameraUnitMoveAMCRMove.ForeColor = System.Drawing.Color.White;
            this.btnCameraUnitMoveAMCRMove.Location = new System.Drawing.Point(3, 3);
            this.btnCameraUnitMoveAMCRMove.Name = "btnCameraUnitMoveAMCRMove";
            this.btnCameraUnitMoveAMCRMove.Size = new System.Drawing.Size(137, 106);
            this.btnCameraUnitMoveAMCRMove.TabIndex = 63;
            this.btnCameraUnitMoveAMCRMove.Text = "MCR 위치\r\n이동";
            this.btnCameraUnitMoveAMCRMove.UseVisualStyleBackColor = false;
            // 
            // btnCameraUnitSave
            // 
            this.btnCameraUnitSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCameraUnitSave.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCameraUnitSave.ForeColor = System.Drawing.Color.White;
            this.btnCameraUnitSave.Location = new System.Drawing.Point(1482, 783);
            this.btnCameraUnitSave.Name = "btnCameraUnitSave";
            this.btnCameraUnitSave.Size = new System.Drawing.Size(228, 28);
            this.btnCameraUnitSave.TabIndex = 50;
            this.btnCameraUnitSave.Text = "저장";
            this.btnCameraUnitSave.UseVisualStyleBackColor = false;
            // 
            // btnCameraUnitSpeedSetting
            // 
            this.btnCameraUnitSpeedSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCameraUnitSpeedSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCameraUnitSpeedSetting.ForeColor = System.Drawing.Color.White;
            this.btnCameraUnitSpeedSetting.Location = new System.Drawing.Point(1291, 315);
            this.btnCameraUnitSpeedSetting.Name = "btnCameraUnitSpeedSetting";
            this.btnCameraUnitSpeedSetting.Size = new System.Drawing.Size(90, 27);
            this.btnCameraUnitSpeedSetting.TabIndex = 48;
            this.btnCameraUnitSpeedSetting.Text = "속도 설정";
            this.btnCameraUnitSpeedSetting.UseVisualStyleBackColor = false;
            // 
            // btnCameraUnitAllSetting
            // 
            this.btnCameraUnitAllSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCameraUnitAllSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCameraUnitAllSetting.ForeColor = System.Drawing.Color.White;
            this.btnCameraUnitAllSetting.Location = new System.Drawing.Point(1387, 273);
            this.btnCameraUnitAllSetting.Name = "btnCameraUnitAllSetting";
            this.btnCameraUnitAllSetting.Size = new System.Drawing.Size(90, 27);
            this.btnCameraUnitAllSetting.TabIndex = 47;
            this.btnCameraUnitAllSetting.Text = "전체 설정";
            this.btnCameraUnitAllSetting.UseVisualStyleBackColor = false;
            // 
            // btnCameraUnitLocationSetting
            // 
            this.btnCameraUnitLocationSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCameraUnitLocationSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCameraUnitLocationSetting.ForeColor = System.Drawing.Color.White;
            this.btnCameraUnitLocationSetting.Location = new System.Drawing.Point(1071, 315);
            this.btnCameraUnitLocationSetting.Name = "btnCameraUnitLocationSetting";
            this.btnCameraUnitLocationSetting.Size = new System.Drawing.Size(90, 27);
            this.btnCameraUnitLocationSetting.TabIndex = 46;
            this.btnCameraUnitLocationSetting.Text = "위치 설정";
            this.btnCameraUnitLocationSetting.UseVisualStyleBackColor = false;
            // 
            // btnCameraUnitMoveLocation
            // 
            this.btnCameraUnitMoveLocation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCameraUnitMoveLocation.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCameraUnitMoveLocation.ForeColor = System.Drawing.Color.White;
            this.btnCameraUnitMoveLocation.Location = new System.Drawing.Point(975, 315);
            this.btnCameraUnitMoveLocation.Name = "btnCameraUnitMoveLocation";
            this.btnCameraUnitMoveLocation.Size = new System.Drawing.Size(90, 27);
            this.btnCameraUnitMoveLocation.TabIndex = 45;
            this.btnCameraUnitMoveLocation.Text = "위치 이동";
            this.btnCameraUnitMoveLocation.UseVisualStyleBackColor = false;
            // 
            // txtCameraUnitLocation
            // 
            this.txtCameraUnitLocation.Font = new System.Drawing.Font("굴림", 13F);
            this.txtCameraUnitLocation.Location = new System.Drawing.Point(879, 316);
            this.txtCameraUnitLocation.Name = "txtCameraUnitLocation";
            this.txtCameraUnitLocation.Size = new System.Drawing.Size(90, 27);
            this.txtCameraUnitLocation.TabIndex = 44;
            // 
            // txtCameraUnitSpeed
            // 
            this.txtCameraUnitSpeed.Font = new System.Drawing.Font("굴림", 13F);
            this.txtCameraUnitSpeed.Location = new System.Drawing.Point(1291, 273);
            this.txtCameraUnitSpeed.Name = "txtCameraUnitSpeed";
            this.txtCameraUnitSpeed.Size = new System.Drawing.Size(90, 27);
            this.txtCameraUnitSpeed.TabIndex = 43;
            // 
            // txtCameraUnitCurrentLocation
            // 
            this.txtCameraUnitCurrentLocation.Font = new System.Drawing.Font("굴림", 13F);
            this.txtCameraUnitCurrentLocation.Location = new System.Drawing.Point(1084, 274);
            this.txtCameraUnitCurrentLocation.Name = "txtCameraUnitCurrentLocation";
            this.txtCameraUnitCurrentLocation.Size = new System.Drawing.Size(90, 27);
            this.txtCameraUnitCurrentLocation.TabIndex = 42;
            // 
            // txtCameraUnitSelectedShaft
            // 
            this.txtCameraUnitSelectedShaft.Font = new System.Drawing.Font("굴림", 13F);
            this.txtCameraUnitSelectedShaft.Location = new System.Drawing.Point(879, 273);
            this.txtCameraUnitSelectedShaft.Name = "txtCameraUnitSelectedShaft";
            this.txtCameraUnitSelectedShaft.Size = new System.Drawing.Size(90, 27);
            this.txtCameraUnitSelectedShaft.TabIndex = 41;
            // 
            // lbl_CameraUnit_Location
            // 
            this.lbl_CameraUnit_Location.AutoSize = true;
            this.lbl_CameraUnit_Location.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CameraUnit_Location.ForeColor = System.Drawing.Color.White;
            this.lbl_CameraUnit_Location.Location = new System.Drawing.Point(801, 316);
            this.lbl_CameraUnit_Location.Name = "lbl_CameraUnit_Location";
            this.lbl_CameraUnit_Location.Size = new System.Drawing.Size(84, 21);
            this.lbl_CameraUnit_Location.TabIndex = 40;
            this.lbl_CameraUnit_Location.Text = "위치[mm]";
            // 
            // lbl_CameraUnit_Speed
            // 
            this.lbl_CameraUnit_Speed.AutoSize = true;
            this.lbl_CameraUnit_Speed.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CameraUnit_Speed.ForeColor = System.Drawing.Color.White;
            this.lbl_CameraUnit_Speed.Location = new System.Drawing.Point(1179, 275);
            this.lbl_CameraUnit_Speed.Name = "lbl_CameraUnit_Speed";
            this.lbl_CameraUnit_Speed.Size = new System.Drawing.Size(115, 21);
            this.lbl_CameraUnit_Speed.TabIndex = 39;
            this.lbl_CameraUnit_Speed.Text = "속도[mm/sec]";
            // 
            // lbl_CameraUnit_SelectedShaft
            // 
            this.lbl_CameraUnit_SelectedShaft.AutoSize = true;
            this.lbl_CameraUnit_SelectedShaft.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CameraUnit_SelectedShaft.ForeColor = System.Drawing.Color.White;
            this.lbl_CameraUnit_SelectedShaft.Location = new System.Drawing.Point(805, 274);
            this.lbl_CameraUnit_SelectedShaft.Name = "lbl_CameraUnit_SelectedShaft";
            this.lbl_CameraUnit_SelectedShaft.Size = new System.Drawing.Size(80, 21);
            this.lbl_CameraUnit_SelectedShaft.TabIndex = 38;
            this.lbl_CameraUnit_SelectedShaft.Text = "선택된 축";
            // 
            // txtCameraUnit_LocationSetting
            // 
            this.txtCameraUnit_LocationSetting.BackColor = System.Drawing.SystemColors.Highlight;
            this.txtCameraUnit_LocationSetting.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.txtCameraUnit_LocationSetting.ForeColor = System.Drawing.Color.White;
            this.txtCameraUnit_LocationSetting.Location = new System.Drawing.Point(792, 223);
            this.txtCameraUnit_LocationSetting.Name = "txtCameraUnit_LocationSetting";
            this.txtCameraUnit_LocationSetting.ReadOnly = true;
            this.txtCameraUnit_LocationSetting.Size = new System.Drawing.Size(89, 27);
            this.txtCameraUnit_LocationSetting.TabIndex = 37;
            this.txtCameraUnit_LocationSetting.Text = "위치값 설정";
            this.txtCameraUnit_LocationSetting.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbl_CameraUnit_CurrentLocation
            // 
            this.lbl_CameraUnit_CurrentLocation.AutoSize = true;
            this.lbl_CameraUnit_CurrentLocation.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CameraUnit_CurrentLocation.ForeColor = System.Drawing.Color.White;
            this.lbl_CameraUnit_CurrentLocation.Location = new System.Drawing.Point(975, 274);
            this.lbl_CameraUnit_CurrentLocation.Name = "lbl_CameraUnit_CurrentLocation";
            this.lbl_CameraUnit_CurrentLocation.Size = new System.Drawing.Size(116, 21);
            this.lbl_CameraUnit_CurrentLocation.TabIndex = 36;
            this.lbl_CameraUnit_CurrentLocation.Text = "현재위치[mm]";
            // 
            // btnCameraUnitGrabSwitch3
            // 
            this.btnCameraUnitGrabSwitch3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCameraUnitGrabSwitch3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCameraUnitGrabSwitch3.ForeColor = System.Drawing.Color.White;
            this.btnCameraUnitGrabSwitch3.Location = new System.Drawing.Point(1514, 6);
            this.btnCameraUnitGrabSwitch3.Name = "btnCameraUnitGrabSwitch3";
            this.btnCameraUnitGrabSwitch3.Size = new System.Drawing.Size(172, 23);
            this.btnCameraUnitGrabSwitch3.TabIndex = 35;
            this.btnCameraUnitGrabSwitch3.Text = "Grab Switch 3";
            this.btnCameraUnitGrabSwitch3.UseVisualStyleBackColor = false;
            // 
            // btnCameraUnitGrabSwitch2
            // 
            this.btnCameraUnitGrabSwitch2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCameraUnitGrabSwitch2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCameraUnitGrabSwitch2.ForeColor = System.Drawing.Color.White;
            this.btnCameraUnitGrabSwitch2.Location = new System.Drawing.Point(1336, 6);
            this.btnCameraUnitGrabSwitch2.Name = "btnCameraUnitGrabSwitch2";
            this.btnCameraUnitGrabSwitch2.Size = new System.Drawing.Size(172, 23);
            this.btnCameraUnitGrabSwitch2.TabIndex = 34;
            this.btnCameraUnitGrabSwitch2.Text = "Grab Switch 2";
            this.btnCameraUnitGrabSwitch2.UseVisualStyleBackColor = false;
            // 
            // btnCameraUnitGrabSwitch1
            // 
            this.btnCameraUnitGrabSwitch1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCameraUnitGrabSwitch1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCameraUnitGrabSwitch1.ForeColor = System.Drawing.Color.White;
            this.btnCameraUnitGrabSwitch1.Location = new System.Drawing.Point(1158, 6);
            this.btnCameraUnitGrabSwitch1.Name = "btnCameraUnitGrabSwitch1";
            this.btnCameraUnitGrabSwitch1.Size = new System.Drawing.Size(172, 23);
            this.btnCameraUnitGrabSwitch1.TabIndex = 33;
            this.btnCameraUnitGrabSwitch1.Text = "Grab Switch 1";
            this.btnCameraUnitGrabSwitch1.UseVisualStyleBackColor = false;
            // 
            // lvCameraUnit
            // 
            this.lvCameraUnit.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.col_CameraUnit_NamebyLocation,
            this.col_CameraUnit_Shaft,
            this.col_CameraUnit_Location,
            this.col_CameraUnit_Speed});
            this.lvCameraUnit.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lvCameraUnit.GridLines = true;
            this.lvCameraUnit.Location = new System.Drawing.Point(17, 16);
            this.lvCameraUnit.Name = "lvCameraUnit";
            this.lvCameraUnit.Size = new System.Drawing.Size(758, 734);
            this.lvCameraUnit.TabIndex = 32;
            this.lvCameraUnit.UseCompatibleStateImageBehavior = false;
            this.lvCameraUnit.View = System.Windows.Forms.View.Details;
            // 
            // col_CameraUnit_NamebyLocation
            // 
            this.col_CameraUnit_NamebyLocation.Text = "위치별 명칭";
            this.col_CameraUnit_NamebyLocation.Width = 430;
            // 
            // col_CameraUnit_Shaft
            // 
            this.col_CameraUnit_Shaft.Text = "축";
            this.col_CameraUnit_Shaft.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // col_CameraUnit_Location
            // 
            this.col_CameraUnit_Location.Text = "위치[mm]";
            this.col_CameraUnit_Location.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_CameraUnit_Location.Width = 125;
            // 
            // col_CameraUnit_Speed
            // 
            this.col_CameraUnit_Speed.Text = "속도[mm/s]";
            this.col_CameraUnit_Speed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_CameraUnit_Speed.Width = 125;
            // 
            // ajin_Setting_CameraUnit
            // 
            this.ajin_Setting_CameraUnit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CameraUnit.Location = new System.Drawing.Point(792, 34);
            this.ajin_Setting_CameraUnit.Name = "ajin_Setting_CameraUnit";
            this.ajin_Setting_CameraUnit.Servo = null;
            this.ajin_Setting_CameraUnit.Size = new System.Drawing.Size(903, 183);
            this.ajin_Setting_CameraUnit.TabIndex = 31;
            // 
            // tp_CellInput
            // 
            this.tp_CellInput.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tp_CellInput.Controls.Add(this.gxtCellInput_Buffer);
            this.tp_CellInput.Controls.Add(this.gxtCellInput_Ionizer);
            this.tp_CellInput.Controls.Add(this.gxtCellInput_Move);
            this.tp_CellInput.Controls.Add(this.gxtCellInput_B);
            this.tp_CellInput.Controls.Add(this.gxtCellInput_A);
            this.tp_CellInput.Controls.Add(this.btnCellInputSave);
            this.tp_CellInput.Controls.Add(this.btnCellInputSpeedSetting);
            this.tp_CellInput.Controls.Add(this.btnCellInputAllSetting);
            this.tp_CellInput.Controls.Add(this.btnCellInputLocationSetting);
            this.tp_CellInput.Controls.Add(this.btnCellInputMoveLocation);
            this.tp_CellInput.Controls.Add(this.txtCellInputLocation);
            this.tp_CellInput.Controls.Add(this.txtCellInputSpeed);
            this.tp_CellInput.Controls.Add(this.txtCellInputCurrentLocation);
            this.tp_CellInput.Controls.Add(this.txtCellInputSelectedShaft);
            this.tp_CellInput.Controls.Add(this.lbl_CellInput_Location);
            this.tp_CellInput.Controls.Add(this.lbl_CellInput_Speed);
            this.tp_CellInput.Controls.Add(this.lbl_CellInput_SelectedShaft);
            this.tp_CellInput.Controls.Add(this.txtCellInput_LocationSetting);
            this.tp_CellInput.Controls.Add(this.lbl_CellInput_CurrentLocation);
            this.tp_CellInput.Controls.Add(this.btnCellInputGrabSwitch3);
            this.tp_CellInput.Controls.Add(this.btnCellInputGrabSwitch2);
            this.tp_CellInput.Controls.Add(this.btnCellInputGrabSwitch1);
            this.tp_CellInput.Controls.Add(this.lvCellInput);
            this.tp_CellInput.Controls.Add(this.ajin_Setting_CellInput);
            this.tp_CellInput.Location = new System.Drawing.Point(4, 34);
            this.tp_CellInput.Name = "tp_CellInput";
            this.tp_CellInput.Size = new System.Drawing.Size(1726, 816);
            this.tp_CellInput.TabIndex = 9;
            this.tp_CellInput.Text = "셀 투입 이재기";
            // 
            // gxtCellInput_Buffer
            // 
            this.gxtCellInput_Buffer.Controls.Add(this.tableLayoutPanel66);
            this.gxtCellInput_Buffer.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCellInput_Buffer.ForeColor = System.Drawing.Color.White;
            this.gxtCellInput_Buffer.Location = new System.Drawing.Point(1484, 410);
            this.gxtCellInput_Buffer.Name = "gxtCellInput_Buffer";
            this.gxtCellInput_Buffer.Size = new System.Drawing.Size(212, 179);
            this.gxtCellInput_Buffer.TabIndex = 54;
            this.gxtCellInput_Buffer.TabStop = false;
            this.gxtCellInput_Buffer.Text = "     BUFFER    ";
            // 
            // tableLayoutPanel66
            // 
            this.tableLayoutPanel66.ColumnCount = 2;
            this.tableLayoutPanel66.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel66.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel66.Controls.Add(this.btnCellInputBufferDestructionOn, 0, 2);
            this.tableLayoutPanel66.Controls.Add(this.btnCellInputBufferDestructionOff, 0, 2);
            this.tableLayoutPanel66.Controls.Add(this.btnCellInputBufferPickerUp, 0, 0);
            this.tableLayoutPanel66.Controls.Add(this.btnCellInputBufferPickerDown, 1, 0);
            this.tableLayoutPanel66.Controls.Add(this.btnCellInputBufferPneumaticOn, 0, 1);
            this.tableLayoutPanel66.Controls.Add(this.btnCellInputBufferPneumaticOff, 1, 1);
            this.tableLayoutPanel66.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel66.Name = "tableLayoutPanel66";
            this.tableLayoutPanel66.RowCount = 3;
            this.tableLayoutPanel66.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel66.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel66.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel66.Size = new System.Drawing.Size(200, 141);
            this.tableLayoutPanel66.TabIndex = 55;
            // 
            // btnCellInputBufferDestructionOn
            // 
            this.btnCellInputBufferDestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputBufferDestructionOn.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellInputBufferDestructionOn.ForeColor = System.Drawing.Color.White;
            this.btnCellInputBufferDestructionOn.Location = new System.Drawing.Point(3, 97);
            this.btnCellInputBufferDestructionOn.Name = "btnCellInputBufferDestructionOn";
            this.btnCellInputBufferDestructionOn.Size = new System.Drawing.Size(94, 41);
            this.btnCellInputBufferDestructionOn.TabIndex = 63;
            this.btnCellInputBufferDestructionOn.Text = "파기 온";
            this.btnCellInputBufferDestructionOn.UseVisualStyleBackColor = false;
            // 
            // btnCellInputBufferDestructionOff
            // 
            this.btnCellInputBufferDestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputBufferDestructionOff.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellInputBufferDestructionOff.ForeColor = System.Drawing.Color.White;
            this.btnCellInputBufferDestructionOff.Location = new System.Drawing.Point(103, 97);
            this.btnCellInputBufferDestructionOff.Name = "btnCellInputBufferDestructionOff";
            this.btnCellInputBufferDestructionOff.Size = new System.Drawing.Size(94, 41);
            this.btnCellInputBufferDestructionOff.TabIndex = 62;
            this.btnCellInputBufferDestructionOff.Text = "파기 오프";
            this.btnCellInputBufferDestructionOff.UseVisualStyleBackColor = false;
            // 
            // btnCellInputBufferPickerUp
            // 
            this.btnCellInputBufferPickerUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputBufferPickerUp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnCellInputBufferPickerUp.ForeColor = System.Drawing.Color.White;
            this.btnCellInputBufferPickerUp.Location = new System.Drawing.Point(3, 3);
            this.btnCellInputBufferPickerUp.Name = "btnCellInputBufferPickerUp";
            this.btnCellInputBufferPickerUp.Size = new System.Drawing.Size(94, 40);
            this.btnCellInputBufferPickerUp.TabIndex = 58;
            this.btnCellInputBufferPickerUp.Text = "버퍼 피커\r\n업";
            this.btnCellInputBufferPickerUp.UseVisualStyleBackColor = false;
            // 
            // btnCellInputBufferPickerDown
            // 
            this.btnCellInputBufferPickerDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputBufferPickerDown.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnCellInputBufferPickerDown.ForeColor = System.Drawing.Color.White;
            this.btnCellInputBufferPickerDown.Location = new System.Drawing.Point(103, 3);
            this.btnCellInputBufferPickerDown.Name = "btnCellInputBufferPickerDown";
            this.btnCellInputBufferPickerDown.Size = new System.Drawing.Size(94, 40);
            this.btnCellInputBufferPickerDown.TabIndex = 60;
            this.btnCellInputBufferPickerDown.Text = "버퍼 피커\r\n다운";
            this.btnCellInputBufferPickerDown.UseVisualStyleBackColor = false;
            // 
            // btnCellInputBufferPneumaticOn
            // 
            this.btnCellInputBufferPneumaticOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputBufferPneumaticOn.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellInputBufferPneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btnCellInputBufferPneumaticOn.Location = new System.Drawing.Point(3, 50);
            this.btnCellInputBufferPneumaticOn.Name = "btnCellInputBufferPneumaticOn";
            this.btnCellInputBufferPneumaticOn.Size = new System.Drawing.Size(94, 40);
            this.btnCellInputBufferPneumaticOn.TabIndex = 59;
            this.btnCellInputBufferPneumaticOn.Text = "공압 온";
            this.btnCellInputBufferPneumaticOn.UseVisualStyleBackColor = false;
            // 
            // btnCellInputBufferPneumaticOff
            // 
            this.btnCellInputBufferPneumaticOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputBufferPneumaticOff.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellInputBufferPneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btnCellInputBufferPneumaticOff.Location = new System.Drawing.Point(103, 50);
            this.btnCellInputBufferPneumaticOff.Name = "btnCellInputBufferPneumaticOff";
            this.btnCellInputBufferPneumaticOff.Size = new System.Drawing.Size(94, 40);
            this.btnCellInputBufferPneumaticOff.TabIndex = 61;
            this.btnCellInputBufferPneumaticOff.Text = "공압 오프";
            this.btnCellInputBufferPneumaticOff.UseVisualStyleBackColor = false;
            // 
            // gxtCellInput_Ionizer
            // 
            this.gxtCellInput_Ionizer.Controls.Add(this.btnCellInputDestructionOff);
            this.gxtCellInput_Ionizer.Controls.Add(this.btnCellInputIonizerOff);
            this.gxtCellInput_Ionizer.Controls.Add(this.btnCellInputDestructionOn);
            this.gxtCellInput_Ionizer.Controls.Add(this.btnCellInputIonizerOn);
            this.gxtCellInput_Ionizer.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCellInput_Ionizer.ForeColor = System.Drawing.Color.White;
            this.gxtCellInput_Ionizer.Location = new System.Drawing.Point(1484, 223);
            this.gxtCellInput_Ionizer.Name = "gxtCellInput_Ionizer";
            this.gxtCellInput_Ionizer.Size = new System.Drawing.Size(212, 181);
            this.gxtCellInput_Ionizer.TabIndex = 50;
            this.gxtCellInput_Ionizer.TabStop = false;
            this.gxtCellInput_Ionizer.Text = "     이오나이저     ";
            // 
            // btnCellInputDestructionOff
            // 
            this.btnCellInputDestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputDestructionOff.Location = new System.Drawing.Point(107, 103);
            this.btnCellInputDestructionOff.Name = "btnCellInputDestructionOff";
            this.btnCellInputDestructionOff.Size = new System.Drawing.Size(99, 72);
            this.btnCellInputDestructionOff.TabIndex = 6;
            this.btnCellInputDestructionOff.Text = "파기 오프";
            this.btnCellInputDestructionOff.UseVisualStyleBackColor = false;
            // 
            // btnCellInputIonizerOff
            // 
            this.btnCellInputIonizerOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputIonizerOff.Location = new System.Drawing.Point(107, 28);
            this.btnCellInputIonizerOff.Name = "btnCellInputIonizerOff";
            this.btnCellInputIonizerOff.Size = new System.Drawing.Size(99, 72);
            this.btnCellInputIonizerOff.TabIndex = 5;
            this.btnCellInputIonizerOff.Text = "이오나이저\r\n오프";
            this.btnCellInputIonizerOff.UseVisualStyleBackColor = false;
            // 
            // btnCellInputDestructionOn
            // 
            this.btnCellInputDestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputDestructionOn.Location = new System.Drawing.Point(6, 103);
            this.btnCellInputDestructionOn.Name = "btnCellInputDestructionOn";
            this.btnCellInputDestructionOn.Size = new System.Drawing.Size(99, 72);
            this.btnCellInputDestructionOn.TabIndex = 4;
            this.btnCellInputDestructionOn.Text = "파기 온";
            this.btnCellInputDestructionOn.UseVisualStyleBackColor = false;
            // 
            // btnCellInputIonizerOn
            // 
            this.btnCellInputIonizerOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputIonizerOn.Location = new System.Drawing.Point(6, 28);
            this.btnCellInputIonizerOn.Name = "btnCellInputIonizerOn";
            this.btnCellInputIonizerOn.Size = new System.Drawing.Size(99, 72);
            this.btnCellInputIonizerOn.TabIndex = 3;
            this.btnCellInputIonizerOn.Text = "이오나이저\r\n온";
            this.btnCellInputIonizerOn.UseVisualStyleBackColor = false;
            // 
            // gxtCellInput_Move
            // 
            this.gxtCellInput_Move.Controls.Add(this.gxtCellInput_MoveCenter);
            this.gxtCellInput_Move.Controls.Add(this.gxtCellInput_MoveB);
            this.gxtCellInput_Move.Controls.Add(this.gxtCellInput_MoveA);
            this.gxtCellInput_Move.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCellInput_Move.ForeColor = System.Drawing.Color.White;
            this.gxtCellInput_Move.Location = new System.Drawing.Point(792, 595);
            this.gxtCellInput_Move.Name = "gxtCellInput_Move";
            this.gxtCellInput_Move.Size = new System.Drawing.Size(904, 179);
            this.gxtCellInput_Move.TabIndex = 53;
            this.gxtCellInput_Move.TabStop = false;
            this.gxtCellInput_Move.Text = "     이동     ";
            // 
            // gxtCellInput_MoveCenter
            // 
            this.gxtCellInput_MoveCenter.Controls.Add(this.tableLayoutPanel69);
            this.gxtCellInput_MoveCenter.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCellInput_MoveCenter.ForeColor = System.Drawing.Color.White;
            this.gxtCellInput_MoveCenter.Location = new System.Drawing.Point(692, 21);
            this.gxtCellInput_MoveCenter.Name = "gxtCellInput_MoveCenter";
            this.gxtCellInput_MoveCenter.Size = new System.Drawing.Size(206, 152);
            this.gxtCellInput_MoveCenter.TabIndex = 32;
            this.gxtCellInput_MoveCenter.TabStop = false;
            this.gxtCellInput_MoveCenter.Text = "     중간    ";
            // 
            // tableLayoutPanel69
            // 
            this.tableLayoutPanel69.ColumnCount = 2;
            this.tableLayoutPanel69.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel69.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel69.Controls.Add(this.btnCellInputMoveCenterABuffer, 0, 1);
            this.tableLayoutPanel69.Controls.Add(this.btnCellInputMoveCenterBBuffer, 0, 1);
            this.tableLayoutPanel69.Controls.Add(this.btnCellInputMoveCenterY1CstMid, 0, 0);
            this.tableLayoutPanel69.Controls.Add(this.btnCellInputMoveCenterY2CstMid, 1, 0);
            this.tableLayoutPanel69.Location = new System.Drawing.Point(3, 28);
            this.tableLayoutPanel69.Name = "tableLayoutPanel69";
            this.tableLayoutPanel69.RowCount = 2;
            this.tableLayoutPanel69.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel69.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel69.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel69.Size = new System.Drawing.Size(200, 115);
            this.tableLayoutPanel69.TabIndex = 0;
            // 
            // btnCellInputMoveCenterABuffer
            // 
            this.btnCellInputMoveCenterABuffer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputMoveCenterABuffer.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnCellInputMoveCenterABuffer.ForeColor = System.Drawing.Color.White;
            this.btnCellInputMoveCenterABuffer.Location = new System.Drawing.Point(3, 60);
            this.btnCellInputMoveCenterABuffer.Name = "btnCellInputMoveCenterABuffer";
            this.btnCellInputMoveCenterABuffer.Size = new System.Drawing.Size(94, 52);
            this.btnCellInputMoveCenterABuffer.TabIndex = 60;
            this.btnCellInputMoveCenterABuffer.Text = "A\r\nBUFFER";
            this.btnCellInputMoveCenterABuffer.UseVisualStyleBackColor = false;
            // 
            // btnCellInputMoveCenterBBuffer
            // 
            this.btnCellInputMoveCenterBBuffer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputMoveCenterBBuffer.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnCellInputMoveCenterBBuffer.ForeColor = System.Drawing.Color.White;
            this.btnCellInputMoveCenterBBuffer.Location = new System.Drawing.Point(103, 60);
            this.btnCellInputMoveCenterBBuffer.Name = "btnCellInputMoveCenterBBuffer";
            this.btnCellInputMoveCenterBBuffer.Size = new System.Drawing.Size(94, 52);
            this.btnCellInputMoveCenterBBuffer.TabIndex = 59;
            this.btnCellInputMoveCenterBBuffer.Text = "B\r\nBUFFER";
            this.btnCellInputMoveCenterBBuffer.UseVisualStyleBackColor = false;
            // 
            // btnCellInputMoveCenterY1CstMid
            // 
            this.btnCellInputMoveCenterY1CstMid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputMoveCenterY1CstMid.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellInputMoveCenterY1CstMid.ForeColor = System.Drawing.Color.White;
            this.btnCellInputMoveCenterY1CstMid.Location = new System.Drawing.Point(3, 3);
            this.btnCellInputMoveCenterY1CstMid.Name = "btnCellInputMoveCenterY1CstMid";
            this.btnCellInputMoveCenterY1CstMid.Size = new System.Drawing.Size(94, 51);
            this.btnCellInputMoveCenterY1CstMid.TabIndex = 57;
            this.btnCellInputMoveCenterY1CstMid.Text = "Y1\r\n카세트\r\n중간";
            this.btnCellInputMoveCenterY1CstMid.UseVisualStyleBackColor = false;
            // 
            // btnCellInputMoveCenterY2CstMid
            // 
            this.btnCellInputMoveCenterY2CstMid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputMoveCenterY2CstMid.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellInputMoveCenterY2CstMid.ForeColor = System.Drawing.Color.White;
            this.btnCellInputMoveCenterY2CstMid.Location = new System.Drawing.Point(103, 3);
            this.btnCellInputMoveCenterY2CstMid.Name = "btnCellInputMoveCenterY2CstMid";
            this.btnCellInputMoveCenterY2CstMid.Size = new System.Drawing.Size(94, 51);
            this.btnCellInputMoveCenterY2CstMid.TabIndex = 58;
            this.btnCellInputMoveCenterY2CstMid.Text = "Y2\r\n카세트\r\n중간";
            this.btnCellInputMoveCenterY2CstMid.UseVisualStyleBackColor = false;
            // 
            // gxtCellInput_MoveB
            // 
            this.gxtCellInput_MoveB.Controls.Add(this.tableLayoutPanel68);
            this.gxtCellInput_MoveB.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCellInput_MoveB.ForeColor = System.Drawing.Color.White;
            this.gxtCellInput_MoveB.Location = new System.Drawing.Point(346, 21);
            this.gxtCellInput_MoveB.Name = "gxtCellInput_MoveB";
            this.gxtCellInput_MoveB.Size = new System.Drawing.Size(340, 152);
            this.gxtCellInput_MoveB.TabIndex = 31;
            this.gxtCellInput_MoveB.TabStop = false;
            this.gxtCellInput_MoveB.Text = "     B    ";
            // 
            // tableLayoutPanel68
            // 
            this.tableLayoutPanel68.ColumnCount = 4;
            this.tableLayoutPanel68.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel68.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel68.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel68.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel68.Controls.Add(this.btnCellInputMoveBX2BUld, 0, 1);
            this.tableLayoutPanel68.Controls.Add(this.btnCellInputMoveBY2BUld, 0, 1);
            this.tableLayoutPanel68.Controls.Add(this.btnCellInputMoveBX2BCstMid, 0, 1);
            this.tableLayoutPanel68.Controls.Add(this.btnCellInputMoveBX2BCstWait, 0, 1);
            this.tableLayoutPanel68.Controls.Add(this.btnCellInputMoveBX1AUld, 0, 0);
            this.tableLayoutPanel68.Controls.Add(this.btnCellInputMoveBY1BUld, 1, 0);
            this.tableLayoutPanel68.Controls.Add(this.btnCellInputMoveBX1BCstMId, 2, 0);
            this.tableLayoutPanel68.Controls.Add(this.btnCellInputMoveBX1BCstWait, 3, 0);
            this.tableLayoutPanel68.Location = new System.Drawing.Point(9, 28);
            this.tableLayoutPanel68.Name = "tableLayoutPanel68";
            this.tableLayoutPanel68.RowCount = 2;
            this.tableLayoutPanel68.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel68.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel68.Size = new System.Drawing.Size(318, 118);
            this.tableLayoutPanel68.TabIndex = 56;
            // 
            // btnCellInputMoveBX2BUld
            // 
            this.btnCellInputMoveBX2BUld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputMoveBX2BUld.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellInputMoveBX2BUld.ForeColor = System.Drawing.Color.White;
            this.btnCellInputMoveBX2BUld.Location = new System.Drawing.Point(3, 62);
            this.btnCellInputMoveBX2BUld.Name = "btnCellInputMoveBX2BUld";
            this.btnCellInputMoveBX2BUld.Size = new System.Drawing.Size(73, 53);
            this.btnCellInputMoveBX2BUld.TabIndex = 65;
            this.btnCellInputMoveBX2BUld.Text = "X2\r\nB 언로딩\r\n이재기";
            this.btnCellInputMoveBX2BUld.UseVisualStyleBackColor = false;
            // 
            // btnCellInputMoveBY2BUld
            // 
            this.btnCellInputMoveBY2BUld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputMoveBY2BUld.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellInputMoveBY2BUld.ForeColor = System.Drawing.Color.White;
            this.btnCellInputMoveBY2BUld.Location = new System.Drawing.Point(82, 62);
            this.btnCellInputMoveBY2BUld.Name = "btnCellInputMoveBY2BUld";
            this.btnCellInputMoveBY2BUld.Size = new System.Drawing.Size(73, 53);
            this.btnCellInputMoveBY2BUld.TabIndex = 64;
            this.btnCellInputMoveBY2BUld.Text = "Y2\r\nB 언로딩\r\n이재기";
            this.btnCellInputMoveBY2BUld.UseVisualStyleBackColor = false;
            // 
            // btnCellInputMoveBX2BCstMid
            // 
            this.btnCellInputMoveBX2BCstMid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputMoveBX2BCstMid.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellInputMoveBX2BCstMid.ForeColor = System.Drawing.Color.White;
            this.btnCellInputMoveBX2BCstMid.Location = new System.Drawing.Point(161, 62);
            this.btnCellInputMoveBX2BCstMid.Name = "btnCellInputMoveBX2BCstMid";
            this.btnCellInputMoveBX2BCstMid.Size = new System.Drawing.Size(73, 53);
            this.btnCellInputMoveBX2BCstMid.TabIndex = 63;
            this.btnCellInputMoveBX2BCstMid.Text = "X2\r\nB 카세트\r\n중간";
            this.btnCellInputMoveBX2BCstMid.UseVisualStyleBackColor = false;
            // 
            // btnCellInputMoveBX2BCstWait
            // 
            this.btnCellInputMoveBX2BCstWait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputMoveBX2BCstWait.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellInputMoveBX2BCstWait.ForeColor = System.Drawing.Color.White;
            this.btnCellInputMoveBX2BCstWait.Location = new System.Drawing.Point(240, 62);
            this.btnCellInputMoveBX2BCstWait.Name = "btnCellInputMoveBX2BCstWait";
            this.btnCellInputMoveBX2BCstWait.Size = new System.Drawing.Size(73, 53);
            this.btnCellInputMoveBX2BCstWait.TabIndex = 62;
            this.btnCellInputMoveBX2BCstWait.Text = "X2\r\nB 카세트\r\n대기";
            this.btnCellInputMoveBX2BCstWait.UseVisualStyleBackColor = false;
            // 
            // btnCellInputMoveBX1AUld
            // 
            this.btnCellInputMoveBX1AUld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputMoveBX1AUld.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellInputMoveBX1AUld.ForeColor = System.Drawing.Color.White;
            this.btnCellInputMoveBX1AUld.Location = new System.Drawing.Point(3, 3);
            this.btnCellInputMoveBX1AUld.Name = "btnCellInputMoveBX1AUld";
            this.btnCellInputMoveBX1AUld.Size = new System.Drawing.Size(73, 53);
            this.btnCellInputMoveBX1AUld.TabIndex = 58;
            this.btnCellInputMoveBX1AUld.Text = "X1\r\nB 언로딩\r\n이재기";
            this.btnCellInputMoveBX1AUld.UseVisualStyleBackColor = false;
            // 
            // btnCellInputMoveBY1BUld
            // 
            this.btnCellInputMoveBY1BUld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputMoveBY1BUld.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellInputMoveBY1BUld.ForeColor = System.Drawing.Color.White;
            this.btnCellInputMoveBY1BUld.Location = new System.Drawing.Point(82, 3);
            this.btnCellInputMoveBY1BUld.Name = "btnCellInputMoveBY1BUld";
            this.btnCellInputMoveBY1BUld.Size = new System.Drawing.Size(73, 53);
            this.btnCellInputMoveBY1BUld.TabIndex = 59;
            this.btnCellInputMoveBY1BUld.Text = "Y1\r\nB 언로딩\r\n이재기";
            this.btnCellInputMoveBY1BUld.UseVisualStyleBackColor = false;
            // 
            // btnCellInputMoveBX1BCstMId
            // 
            this.btnCellInputMoveBX1BCstMId.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputMoveBX1BCstMId.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellInputMoveBX1BCstMId.ForeColor = System.Drawing.Color.White;
            this.btnCellInputMoveBX1BCstMId.Location = new System.Drawing.Point(161, 3);
            this.btnCellInputMoveBX1BCstMId.Name = "btnCellInputMoveBX1BCstMId";
            this.btnCellInputMoveBX1BCstMId.Size = new System.Drawing.Size(73, 53);
            this.btnCellInputMoveBX1BCstMId.TabIndex = 60;
            this.btnCellInputMoveBX1BCstMId.Text = "X1\r\nB 카세트\r\n중간";
            this.btnCellInputMoveBX1BCstMId.UseVisualStyleBackColor = false;
            // 
            // btnCellInputMoveBX1BCstWait
            // 
            this.btnCellInputMoveBX1BCstWait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputMoveBX1BCstWait.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellInputMoveBX1BCstWait.ForeColor = System.Drawing.Color.White;
            this.btnCellInputMoveBX1BCstWait.Location = new System.Drawing.Point(240, 3);
            this.btnCellInputMoveBX1BCstWait.Name = "btnCellInputMoveBX1BCstWait";
            this.btnCellInputMoveBX1BCstWait.Size = new System.Drawing.Size(73, 53);
            this.btnCellInputMoveBX1BCstWait.TabIndex = 61;
            this.btnCellInputMoveBX1BCstWait.Text = "X1\r\nB 카세트\r\n대기";
            this.btnCellInputMoveBX1BCstWait.UseVisualStyleBackColor = false;
            // 
            // gxtCellInput_MoveA
            // 
            this.gxtCellInput_MoveA.Controls.Add(this.tableLayoutPanel67);
            this.gxtCellInput_MoveA.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCellInput_MoveA.ForeColor = System.Drawing.Color.White;
            this.gxtCellInput_MoveA.Location = new System.Drawing.Point(6, 21);
            this.gxtCellInput_MoveA.Name = "gxtCellInput_MoveA";
            this.gxtCellInput_MoveA.Size = new System.Drawing.Size(334, 152);
            this.gxtCellInput_MoveA.TabIndex = 30;
            this.gxtCellInput_MoveA.TabStop = false;
            this.gxtCellInput_MoveA.Text = "     A    ";
            // 
            // tableLayoutPanel67
            // 
            this.tableLayoutPanel67.ColumnCount = 4;
            this.tableLayoutPanel67.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel67.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel67.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel67.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel67.Controls.Add(this.btnCellInputMoveAX2AUld, 0, 1);
            this.tableLayoutPanel67.Controls.Add(this.btnCellInputMoveAY2AUld, 0, 1);
            this.tableLayoutPanel67.Controls.Add(this.btnCellInputMoveAX2ACstMid, 0, 1);
            this.tableLayoutPanel67.Controls.Add(this.btnCellInputMoveAX2ACstWait, 0, 1);
            this.tableLayoutPanel67.Controls.Add(this.btnCellInputMoveAX1AUld, 0, 0);
            this.tableLayoutPanel67.Controls.Add(this.btnCellInputMoveAY1AUld, 1, 0);
            this.tableLayoutPanel67.Controls.Add(this.btnCellInputMoveAX1ACstMid, 2, 0);
            this.tableLayoutPanel67.Controls.Add(this.btnCellInputMoveAX1ACstWait, 3, 0);
            this.tableLayoutPanel67.Location = new System.Drawing.Point(7, 28);
            this.tableLayoutPanel67.Name = "tableLayoutPanel67";
            this.tableLayoutPanel67.RowCount = 2;
            this.tableLayoutPanel67.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel67.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel67.Size = new System.Drawing.Size(318, 118);
            this.tableLayoutPanel67.TabIndex = 55;
            // 
            // btnCellInputMoveAX2AUld
            // 
            this.btnCellInputMoveAX2AUld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputMoveAX2AUld.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellInputMoveAX2AUld.ForeColor = System.Drawing.Color.White;
            this.btnCellInputMoveAX2AUld.Location = new System.Drawing.Point(3, 62);
            this.btnCellInputMoveAX2AUld.Name = "btnCellInputMoveAX2AUld";
            this.btnCellInputMoveAX2AUld.Size = new System.Drawing.Size(73, 53);
            this.btnCellInputMoveAX2AUld.TabIndex = 65;
            this.btnCellInputMoveAX2AUld.Text = "X2\r\nA 언로딩\r\n이재기";
            this.btnCellInputMoveAX2AUld.UseVisualStyleBackColor = false;
            // 
            // btnCellInputMoveAY2AUld
            // 
            this.btnCellInputMoveAY2AUld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputMoveAY2AUld.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellInputMoveAY2AUld.ForeColor = System.Drawing.Color.White;
            this.btnCellInputMoveAY2AUld.Location = new System.Drawing.Point(82, 62);
            this.btnCellInputMoveAY2AUld.Name = "btnCellInputMoveAY2AUld";
            this.btnCellInputMoveAY2AUld.Size = new System.Drawing.Size(73, 53);
            this.btnCellInputMoveAY2AUld.TabIndex = 64;
            this.btnCellInputMoveAY2AUld.Text = "Y2\r\nA 언로딩\r\n이재기";
            this.btnCellInputMoveAY2AUld.UseVisualStyleBackColor = false;
            // 
            // btnCellInputMoveAX2ACstMid
            // 
            this.btnCellInputMoveAX2ACstMid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputMoveAX2ACstMid.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellInputMoveAX2ACstMid.ForeColor = System.Drawing.Color.White;
            this.btnCellInputMoveAX2ACstMid.Location = new System.Drawing.Point(161, 62);
            this.btnCellInputMoveAX2ACstMid.Name = "btnCellInputMoveAX2ACstMid";
            this.btnCellInputMoveAX2ACstMid.Size = new System.Drawing.Size(73, 53);
            this.btnCellInputMoveAX2ACstMid.TabIndex = 63;
            this.btnCellInputMoveAX2ACstMid.Text = "X2\r\nA 카세트\r\n중간";
            this.btnCellInputMoveAX2ACstMid.UseVisualStyleBackColor = false;
            // 
            // btnCellInputMoveAX2ACstWait
            // 
            this.btnCellInputMoveAX2ACstWait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputMoveAX2ACstWait.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellInputMoveAX2ACstWait.ForeColor = System.Drawing.Color.White;
            this.btnCellInputMoveAX2ACstWait.Location = new System.Drawing.Point(240, 62);
            this.btnCellInputMoveAX2ACstWait.Name = "btnCellInputMoveAX2ACstWait";
            this.btnCellInputMoveAX2ACstWait.Size = new System.Drawing.Size(73, 53);
            this.btnCellInputMoveAX2ACstWait.TabIndex = 62;
            this.btnCellInputMoveAX2ACstWait.Text = "X2\r\nA 카세트\r\n대기";
            this.btnCellInputMoveAX2ACstWait.UseVisualStyleBackColor = false;
            // 
            // btnCellInputMoveAX1AUld
            // 
            this.btnCellInputMoveAX1AUld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputMoveAX1AUld.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellInputMoveAX1AUld.ForeColor = System.Drawing.Color.White;
            this.btnCellInputMoveAX1AUld.Location = new System.Drawing.Point(3, 3);
            this.btnCellInputMoveAX1AUld.Name = "btnCellInputMoveAX1AUld";
            this.btnCellInputMoveAX1AUld.Size = new System.Drawing.Size(73, 53);
            this.btnCellInputMoveAX1AUld.TabIndex = 58;
            this.btnCellInputMoveAX1AUld.Text = "X1\r\nA 언로딩\r\n이재기";
            this.btnCellInputMoveAX1AUld.UseVisualStyleBackColor = false;
            // 
            // btnCellInputMoveAY1AUld
            // 
            this.btnCellInputMoveAY1AUld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputMoveAY1AUld.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellInputMoveAY1AUld.ForeColor = System.Drawing.Color.White;
            this.btnCellInputMoveAY1AUld.Location = new System.Drawing.Point(82, 3);
            this.btnCellInputMoveAY1AUld.Name = "btnCellInputMoveAY1AUld";
            this.btnCellInputMoveAY1AUld.Size = new System.Drawing.Size(73, 53);
            this.btnCellInputMoveAY1AUld.TabIndex = 59;
            this.btnCellInputMoveAY1AUld.Text = "Y1\r\nA 언로딩\r\n이재기";
            this.btnCellInputMoveAY1AUld.UseVisualStyleBackColor = false;
            // 
            // btnCellInputMoveAX1ACstMid
            // 
            this.btnCellInputMoveAX1ACstMid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputMoveAX1ACstMid.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellInputMoveAX1ACstMid.ForeColor = System.Drawing.Color.White;
            this.btnCellInputMoveAX1ACstMid.Location = new System.Drawing.Point(161, 3);
            this.btnCellInputMoveAX1ACstMid.Name = "btnCellInputMoveAX1ACstMid";
            this.btnCellInputMoveAX1ACstMid.Size = new System.Drawing.Size(73, 53);
            this.btnCellInputMoveAX1ACstMid.TabIndex = 60;
            this.btnCellInputMoveAX1ACstMid.Text = "X1\r\nA 카세트\r\n중간";
            this.btnCellInputMoveAX1ACstMid.UseVisualStyleBackColor = false;
            // 
            // btnCellInputMoveAX1ACstWait
            // 
            this.btnCellInputMoveAX1ACstWait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputMoveAX1ACstWait.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btnCellInputMoveAX1ACstWait.ForeColor = System.Drawing.Color.White;
            this.btnCellInputMoveAX1ACstWait.Location = new System.Drawing.Point(240, 3);
            this.btnCellInputMoveAX1ACstWait.Name = "btnCellInputMoveAX1ACstWait";
            this.btnCellInputMoveAX1ACstWait.Size = new System.Drawing.Size(73, 53);
            this.btnCellInputMoveAX1ACstWait.TabIndex = 61;
            this.btnCellInputMoveAX1ACstWait.Text = "X1\r\nA 카세트\r\n대기";
            this.btnCellInputMoveAX1ACstWait.UseVisualStyleBackColor = false;
            // 
            // gxtCellInput_B
            // 
            this.gxtCellInput_B.Controls.Add(this.tableLayoutPanel23);
            this.gxtCellInput_B.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCellInput_B.ForeColor = System.Drawing.Color.White;
            this.gxtCellInput_B.Location = new System.Drawing.Point(1138, 410);
            this.gxtCellInput_B.Name = "gxtCellInput_B";
            this.gxtCellInput_B.Size = new System.Drawing.Size(340, 179);
            this.gxtCellInput_B.TabIndex = 52;
            this.gxtCellInput_B.TabStop = false;
            this.gxtCellInput_B.Text = "     B     ";
            // 
            // tableLayoutPanel23
            // 
            this.tableLayoutPanel23.ColumnCount = 2;
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel23.Controls.Add(this.btnCellInputBDestructionOn, 0, 1);
            this.tableLayoutPanel23.Controls.Add(this.btnCellInputBDestructionOff, 0, 1);
            this.tableLayoutPanel23.Controls.Add(this.btnCellInputBPneumaticOff, 1, 0);
            this.tableLayoutPanel23.Controls.Add(this.btnCellInputBPneumaticOn, 0, 0);
            this.tableLayoutPanel23.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel23.Name = "tableLayoutPanel23";
            this.tableLayoutPanel23.RowCount = 2;
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel23.Size = new System.Drawing.Size(328, 145);
            this.tableLayoutPanel23.TabIndex = 55;
            // 
            // btnCellInputBDestructionOn
            // 
            this.btnCellInputBDestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputBDestructionOn.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellInputBDestructionOn.ForeColor = System.Drawing.Color.White;
            this.btnCellInputBDestructionOn.Location = new System.Drawing.Point(3, 75);
            this.btnCellInputBDestructionOn.Name = "btnCellInputBDestructionOn";
            this.btnCellInputBDestructionOn.Size = new System.Drawing.Size(158, 66);
            this.btnCellInputBDestructionOn.TabIndex = 58;
            this.btnCellInputBDestructionOn.Text = "파기 온";
            this.btnCellInputBDestructionOn.UseVisualStyleBackColor = false;
            // 
            // btnCellInputBDestructionOff
            // 
            this.btnCellInputBDestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputBDestructionOff.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellInputBDestructionOff.ForeColor = System.Drawing.Color.White;
            this.btnCellInputBDestructionOff.Location = new System.Drawing.Point(167, 75);
            this.btnCellInputBDestructionOff.Name = "btnCellInputBDestructionOff";
            this.btnCellInputBDestructionOff.Size = new System.Drawing.Size(158, 66);
            this.btnCellInputBDestructionOff.TabIndex = 56;
            this.btnCellInputBDestructionOff.Text = "파기 오프";
            this.btnCellInputBDestructionOff.UseVisualStyleBackColor = false;
            // 
            // btnCellInputBPneumaticOff
            // 
            this.btnCellInputBPneumaticOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputBPneumaticOff.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellInputBPneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btnCellInputBPneumaticOff.Location = new System.Drawing.Point(167, 3);
            this.btnCellInputBPneumaticOff.Name = "btnCellInputBPneumaticOff";
            this.btnCellInputBPneumaticOff.Size = new System.Drawing.Size(158, 66);
            this.btnCellInputBPneumaticOff.TabIndex = 55;
            this.btnCellInputBPneumaticOff.Text = "공압 오프";
            this.btnCellInputBPneumaticOff.UseVisualStyleBackColor = false;
            // 
            // btnCellInputBPneumaticOn
            // 
            this.btnCellInputBPneumaticOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputBPneumaticOn.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellInputBPneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btnCellInputBPneumaticOn.Location = new System.Drawing.Point(3, 3);
            this.btnCellInputBPneumaticOn.Name = "btnCellInputBPneumaticOn";
            this.btnCellInputBPneumaticOn.Size = new System.Drawing.Size(158, 66);
            this.btnCellInputBPneumaticOn.TabIndex = 57;
            this.btnCellInputBPneumaticOn.Text = "공압 온";
            this.btnCellInputBPneumaticOn.UseVisualStyleBackColor = false;
            // 
            // gxtCellInput_A
            // 
            this.gxtCellInput_A.Controls.Add(this.tableLayoutPanel22);
            this.gxtCellInput_A.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCellInput_A.ForeColor = System.Drawing.Color.White;
            this.gxtCellInput_A.Location = new System.Drawing.Point(792, 410);
            this.gxtCellInput_A.Name = "gxtCellInput_A";
            this.gxtCellInput_A.Size = new System.Drawing.Size(340, 179);
            this.gxtCellInput_A.TabIndex = 51;
            this.gxtCellInput_A.TabStop = false;
            this.gxtCellInput_A.Text = "     A    ";
            // 
            // tableLayoutPanel22
            // 
            this.tableLayoutPanel22.ColumnCount = 2;
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel22.Controls.Add(this.btnCellInputADestructionOn, 0, 1);
            this.tableLayoutPanel22.Controls.Add(this.btnCellInputADestructionOff, 0, 1);
            this.tableLayoutPanel22.Controls.Add(this.btnCellInputAPneumaticOff, 1, 0);
            this.tableLayoutPanel22.Controls.Add(this.btnCellInputAPneumaticOn, 0, 0);
            this.tableLayoutPanel22.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel22.Name = "tableLayoutPanel22";
            this.tableLayoutPanel22.RowCount = 2;
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel22.Size = new System.Drawing.Size(328, 145);
            this.tableLayoutPanel22.TabIndex = 55;
            // 
            // btnCellInputADestructionOn
            // 
            this.btnCellInputADestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputADestructionOn.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellInputADestructionOn.ForeColor = System.Drawing.Color.White;
            this.btnCellInputADestructionOn.Location = new System.Drawing.Point(3, 75);
            this.btnCellInputADestructionOn.Name = "btnCellInputADestructionOn";
            this.btnCellInputADestructionOn.Size = new System.Drawing.Size(158, 66);
            this.btnCellInputADestructionOn.TabIndex = 58;
            this.btnCellInputADestructionOn.Text = "파기 온";
            this.btnCellInputADestructionOn.UseVisualStyleBackColor = false;
            // 
            // btnCellInputADestructionOff
            // 
            this.btnCellInputADestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputADestructionOff.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellInputADestructionOff.ForeColor = System.Drawing.Color.White;
            this.btnCellInputADestructionOff.Location = new System.Drawing.Point(167, 75);
            this.btnCellInputADestructionOff.Name = "btnCellInputADestructionOff";
            this.btnCellInputADestructionOff.Size = new System.Drawing.Size(158, 66);
            this.btnCellInputADestructionOff.TabIndex = 56;
            this.btnCellInputADestructionOff.Text = "파기 오프";
            this.btnCellInputADestructionOff.UseVisualStyleBackColor = false;
            // 
            // btnCellInputAPneumaticOff
            // 
            this.btnCellInputAPneumaticOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputAPneumaticOff.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellInputAPneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btnCellInputAPneumaticOff.Location = new System.Drawing.Point(167, 3);
            this.btnCellInputAPneumaticOff.Name = "btnCellInputAPneumaticOff";
            this.btnCellInputAPneumaticOff.Size = new System.Drawing.Size(158, 66);
            this.btnCellInputAPneumaticOff.TabIndex = 55;
            this.btnCellInputAPneumaticOff.Text = "공압 오프";
            this.btnCellInputAPneumaticOff.UseVisualStyleBackColor = false;
            // 
            // btnCellInputAPneumaticOn
            // 
            this.btnCellInputAPneumaticOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputAPneumaticOn.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellInputAPneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btnCellInputAPneumaticOn.Location = new System.Drawing.Point(3, 3);
            this.btnCellInputAPneumaticOn.Name = "btnCellInputAPneumaticOn";
            this.btnCellInputAPneumaticOn.Size = new System.Drawing.Size(158, 66);
            this.btnCellInputAPneumaticOn.TabIndex = 57;
            this.btnCellInputAPneumaticOn.Text = "공압 온";
            this.btnCellInputAPneumaticOn.UseVisualStyleBackColor = false;
            // 
            // btnCellInputSave
            // 
            this.btnCellInputSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputSave.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellInputSave.ForeColor = System.Drawing.Color.White;
            this.btnCellInputSave.Location = new System.Drawing.Point(1482, 783);
            this.btnCellInputSave.Name = "btnCellInputSave";
            this.btnCellInputSave.Size = new System.Drawing.Size(228, 28);
            this.btnCellInputSave.TabIndex = 50;
            this.btnCellInputSave.Text = "저장";
            this.btnCellInputSave.UseVisualStyleBackColor = false;
            // 
            // btnCellInputSpeedSetting
            // 
            this.btnCellInputSpeedSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputSpeedSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCellInputSpeedSetting.ForeColor = System.Drawing.Color.White;
            this.btnCellInputSpeedSetting.Location = new System.Drawing.Point(1291, 315);
            this.btnCellInputSpeedSetting.Name = "btnCellInputSpeedSetting";
            this.btnCellInputSpeedSetting.Size = new System.Drawing.Size(90, 27);
            this.btnCellInputSpeedSetting.TabIndex = 48;
            this.btnCellInputSpeedSetting.Text = "속도 설정";
            this.btnCellInputSpeedSetting.UseVisualStyleBackColor = false;
            // 
            // btnCellInputAllSetting
            // 
            this.btnCellInputAllSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputAllSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCellInputAllSetting.ForeColor = System.Drawing.Color.White;
            this.btnCellInputAllSetting.Location = new System.Drawing.Point(1387, 273);
            this.btnCellInputAllSetting.Name = "btnCellInputAllSetting";
            this.btnCellInputAllSetting.Size = new System.Drawing.Size(90, 27);
            this.btnCellInputAllSetting.TabIndex = 47;
            this.btnCellInputAllSetting.Text = "전체 설정";
            this.btnCellInputAllSetting.UseVisualStyleBackColor = false;
            // 
            // btnCellInputLocationSetting
            // 
            this.btnCellInputLocationSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputLocationSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCellInputLocationSetting.ForeColor = System.Drawing.Color.White;
            this.btnCellInputLocationSetting.Location = new System.Drawing.Point(1071, 315);
            this.btnCellInputLocationSetting.Name = "btnCellInputLocationSetting";
            this.btnCellInputLocationSetting.Size = new System.Drawing.Size(90, 27);
            this.btnCellInputLocationSetting.TabIndex = 46;
            this.btnCellInputLocationSetting.Text = "위치 설정";
            this.btnCellInputLocationSetting.UseVisualStyleBackColor = false;
            // 
            // btnCellInputMoveLocation
            // 
            this.btnCellInputMoveLocation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputMoveLocation.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCellInputMoveLocation.ForeColor = System.Drawing.Color.White;
            this.btnCellInputMoveLocation.Location = new System.Drawing.Point(975, 315);
            this.btnCellInputMoveLocation.Name = "btnCellInputMoveLocation";
            this.btnCellInputMoveLocation.Size = new System.Drawing.Size(90, 27);
            this.btnCellInputMoveLocation.TabIndex = 45;
            this.btnCellInputMoveLocation.Text = "위치 이동";
            this.btnCellInputMoveLocation.UseVisualStyleBackColor = false;
            // 
            // txtCellInputLocation
            // 
            this.txtCellInputLocation.Font = new System.Drawing.Font("굴림", 13F);
            this.txtCellInputLocation.Location = new System.Drawing.Point(879, 316);
            this.txtCellInputLocation.Name = "txtCellInputLocation";
            this.txtCellInputLocation.Size = new System.Drawing.Size(90, 27);
            this.txtCellInputLocation.TabIndex = 44;
            // 
            // txtCellInputSpeed
            // 
            this.txtCellInputSpeed.Font = new System.Drawing.Font("굴림", 13F);
            this.txtCellInputSpeed.Location = new System.Drawing.Point(1291, 273);
            this.txtCellInputSpeed.Name = "txtCellInputSpeed";
            this.txtCellInputSpeed.Size = new System.Drawing.Size(90, 27);
            this.txtCellInputSpeed.TabIndex = 43;
            // 
            // txtCellInputCurrentLocation
            // 
            this.txtCellInputCurrentLocation.Font = new System.Drawing.Font("굴림", 13F);
            this.txtCellInputCurrentLocation.Location = new System.Drawing.Point(1084, 274);
            this.txtCellInputCurrentLocation.Name = "txtCellInputCurrentLocation";
            this.txtCellInputCurrentLocation.Size = new System.Drawing.Size(90, 27);
            this.txtCellInputCurrentLocation.TabIndex = 42;
            // 
            // txtCellInputSelectedShaft
            // 
            this.txtCellInputSelectedShaft.Font = new System.Drawing.Font("굴림", 13F);
            this.txtCellInputSelectedShaft.Location = new System.Drawing.Point(879, 273);
            this.txtCellInputSelectedShaft.Name = "txtCellInputSelectedShaft";
            this.txtCellInputSelectedShaft.Size = new System.Drawing.Size(90, 27);
            this.txtCellInputSelectedShaft.TabIndex = 41;
            // 
            // lbl_CellInput_Location
            // 
            this.lbl_CellInput_Location.AutoSize = true;
            this.lbl_CellInput_Location.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CellInput_Location.ForeColor = System.Drawing.Color.White;
            this.lbl_CellInput_Location.Location = new System.Drawing.Point(801, 316);
            this.lbl_CellInput_Location.Name = "lbl_CellInput_Location";
            this.lbl_CellInput_Location.Size = new System.Drawing.Size(84, 21);
            this.lbl_CellInput_Location.TabIndex = 40;
            this.lbl_CellInput_Location.Text = "위치[mm]";
            // 
            // lbl_CellInput_Speed
            // 
            this.lbl_CellInput_Speed.AutoSize = true;
            this.lbl_CellInput_Speed.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CellInput_Speed.ForeColor = System.Drawing.Color.White;
            this.lbl_CellInput_Speed.Location = new System.Drawing.Point(1179, 275);
            this.lbl_CellInput_Speed.Name = "lbl_CellInput_Speed";
            this.lbl_CellInput_Speed.Size = new System.Drawing.Size(115, 21);
            this.lbl_CellInput_Speed.TabIndex = 39;
            this.lbl_CellInput_Speed.Text = "속도[mm/sec]";
            // 
            // lbl_CellInput_SelectedShaft
            // 
            this.lbl_CellInput_SelectedShaft.AutoSize = true;
            this.lbl_CellInput_SelectedShaft.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CellInput_SelectedShaft.ForeColor = System.Drawing.Color.White;
            this.lbl_CellInput_SelectedShaft.Location = new System.Drawing.Point(805, 274);
            this.lbl_CellInput_SelectedShaft.Name = "lbl_CellInput_SelectedShaft";
            this.lbl_CellInput_SelectedShaft.Size = new System.Drawing.Size(80, 21);
            this.lbl_CellInput_SelectedShaft.TabIndex = 38;
            this.lbl_CellInput_SelectedShaft.Text = "선택된 축";
            // 
            // txtCellInput_LocationSetting
            // 
            this.txtCellInput_LocationSetting.BackColor = System.Drawing.SystemColors.Highlight;
            this.txtCellInput_LocationSetting.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.txtCellInput_LocationSetting.ForeColor = System.Drawing.Color.White;
            this.txtCellInput_LocationSetting.Location = new System.Drawing.Point(792, 223);
            this.txtCellInput_LocationSetting.Name = "txtCellInput_LocationSetting";
            this.txtCellInput_LocationSetting.ReadOnly = true;
            this.txtCellInput_LocationSetting.Size = new System.Drawing.Size(89, 27);
            this.txtCellInput_LocationSetting.TabIndex = 37;
            this.txtCellInput_LocationSetting.Text = "위치값 설정";
            this.txtCellInput_LocationSetting.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbl_CellInput_CurrentLocation
            // 
            this.lbl_CellInput_CurrentLocation.AutoSize = true;
            this.lbl_CellInput_CurrentLocation.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CellInput_CurrentLocation.ForeColor = System.Drawing.Color.White;
            this.lbl_CellInput_CurrentLocation.Location = new System.Drawing.Point(975, 274);
            this.lbl_CellInput_CurrentLocation.Name = "lbl_CellInput_CurrentLocation";
            this.lbl_CellInput_CurrentLocation.Size = new System.Drawing.Size(116, 21);
            this.lbl_CellInput_CurrentLocation.TabIndex = 36;
            this.lbl_CellInput_CurrentLocation.Text = "현재위치[mm]";
            // 
            // btnCellInputGrabSwitch3
            // 
            this.btnCellInputGrabSwitch3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputGrabSwitch3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellInputGrabSwitch3.ForeColor = System.Drawing.Color.White;
            this.btnCellInputGrabSwitch3.Location = new System.Drawing.Point(1514, 6);
            this.btnCellInputGrabSwitch3.Name = "btnCellInputGrabSwitch3";
            this.btnCellInputGrabSwitch3.Size = new System.Drawing.Size(172, 23);
            this.btnCellInputGrabSwitch3.TabIndex = 35;
            this.btnCellInputGrabSwitch3.Text = "Grab Switch 3";
            this.btnCellInputGrabSwitch3.UseVisualStyleBackColor = false;
            // 
            // btnCellInputGrabSwitch2
            // 
            this.btnCellInputGrabSwitch2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputGrabSwitch2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellInputGrabSwitch2.ForeColor = System.Drawing.Color.White;
            this.btnCellInputGrabSwitch2.Location = new System.Drawing.Point(1336, 6);
            this.btnCellInputGrabSwitch2.Name = "btnCellInputGrabSwitch2";
            this.btnCellInputGrabSwitch2.Size = new System.Drawing.Size(172, 23);
            this.btnCellInputGrabSwitch2.TabIndex = 34;
            this.btnCellInputGrabSwitch2.Text = "Grab Switch 2";
            this.btnCellInputGrabSwitch2.UseVisualStyleBackColor = false;
            // 
            // btnCellInputGrabSwitch1
            // 
            this.btnCellInputGrabSwitch1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCellInputGrabSwitch1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCellInputGrabSwitch1.ForeColor = System.Drawing.Color.White;
            this.btnCellInputGrabSwitch1.Location = new System.Drawing.Point(1158, 6);
            this.btnCellInputGrabSwitch1.Name = "btnCellInputGrabSwitch1";
            this.btnCellInputGrabSwitch1.Size = new System.Drawing.Size(172, 23);
            this.btnCellInputGrabSwitch1.TabIndex = 33;
            this.btnCellInputGrabSwitch1.Text = "Grab Switch 1";
            this.btnCellInputGrabSwitch1.UseVisualStyleBackColor = false;
            // 
            // lvCellInput
            // 
            this.lvCellInput.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.col_CellInput_NamebyLocation,
            this.col_CellInput_Shaft,
            this.col_CellInput_Location,
            this.col_CellInput_Speed});
            this.lvCellInput.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lvCellInput.GridLines = true;
            this.lvCellInput.Location = new System.Drawing.Point(17, 16);
            this.lvCellInput.Name = "lvCellInput";
            this.lvCellInput.Size = new System.Drawing.Size(758, 734);
            this.lvCellInput.TabIndex = 32;
            this.lvCellInput.UseCompatibleStateImageBehavior = false;
            this.lvCellInput.View = System.Windows.Forms.View.Details;
            // 
            // col_CellInput_NamebyLocation
            // 
            this.col_CellInput_NamebyLocation.Text = "위치별 명칭";
            this.col_CellInput_NamebyLocation.Width = 430;
            // 
            // col_CellInput_Shaft
            // 
            this.col_CellInput_Shaft.Text = "축";
            this.col_CellInput_Shaft.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // col_CellInput_Location
            // 
            this.col_CellInput_Location.Text = "위치[mm]";
            this.col_CellInput_Location.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_CellInput_Location.Width = 125;
            // 
            // col_CellInput_Speed
            // 
            this.col_CellInput_Speed.Text = "속도[mm/s]";
            this.col_CellInput_Speed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_CellInput_Speed.Width = 125;
            // 
            // ajin_Setting_CellInput
            // 
            this.ajin_Setting_CellInput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CellInput.Location = new System.Drawing.Point(792, 34);
            this.ajin_Setting_CellInput.Name = "ajin_Setting_CellInput";
            this.ajin_Setting_CellInput.Servo = null;
            this.ajin_Setting_CellInput.Size = new System.Drawing.Size(903, 183);
            this.ajin_Setting_CellInput.TabIndex = 31;
            // 
            // tp_CasseteUnload
            // 
            this.tp_CasseteUnload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tp_CasseteUnload.Controls.Add(this.gxtCasseteUnload_Move);
            this.tp_CasseteUnload.Controls.Add(this.gxtCasseteUnload_B);
            this.tp_CasseteUnload.Controls.Add(this.gxtCasseteUnload_A);
            this.tp_CasseteUnload.Controls.Add(this.btnCasseteUnloadSave);
            this.tp_CasseteUnload.Controls.Add(this.gxtCasseteUnload);
            this.tp_CasseteUnload.Controls.Add(this.btnCasseteUnloadSpeedSetting);
            this.tp_CasseteUnload.Controls.Add(this.btnCasseteUnloadAllSetting);
            this.tp_CasseteUnload.Controls.Add(this.btnCasseteUnloadLocationSetting);
            this.tp_CasseteUnload.Controls.Add(this.btnCasseteUnloadMoveLocation);
            this.tp_CasseteUnload.Controls.Add(this.txtCasseteUnloadLocation);
            this.tp_CasseteUnload.Controls.Add(this.txtCasseteUnloadSpeed);
            this.tp_CasseteUnload.Controls.Add(this.txtCasseteUnloadCurrentLocation);
            this.tp_CasseteUnload.Controls.Add(this.txtCasseteUnload_SelectedShaft);
            this.tp_CasseteUnload.Controls.Add(this.lbl_CasseteUnload_Location);
            this.tp_CasseteUnload.Controls.Add(this.lbl_CasseteUnload_Speed);
            this.tp_CasseteUnload.Controls.Add(this.lbl_CasseteUnload_SelectedShaft);
            this.tp_CasseteUnload.Controls.Add(this.txtCasseteUnload_LocationSetting);
            this.tp_CasseteUnload.Controls.Add(this.lbl_CasseteUnload_CurrentLocation);
            this.tp_CasseteUnload.Controls.Add(this.btncasseteUnloadGrabSwitch3);
            this.tp_CasseteUnload.Controls.Add(this.btnCasseteUnloadGrabSwitch2);
            this.tp_CasseteUnload.Controls.Add(this.btnCasseteUnloadGrabSwitch1);
            this.tp_CasseteUnload.Controls.Add(this.lvCasseteUnload);
            this.tp_CasseteUnload.Controls.Add(this.ajin_Setting_CasseteUnload);
            this.tp_CasseteUnload.Location = new System.Drawing.Point(4, 34);
            this.tp_CasseteUnload.Name = "tp_CasseteUnload";
            this.tp_CasseteUnload.Size = new System.Drawing.Size(1726, 816);
            this.tp_CasseteUnload.TabIndex = 10;
            this.tp_CasseteUnload.Text = "카세트 언로드";
            // 
            // gxtCasseteUnload_Move
            // 
            this.gxtCasseteUnload_Move.Controls.Add(this.gxtCasseteUnload_MoveB);
            this.gxtCasseteUnload_Move.Controls.Add(this.gxtCasseteUnload_MoveA);
            this.gxtCasseteUnload_Move.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCasseteUnload_Move.ForeColor = System.Drawing.Color.White;
            this.gxtCasseteUnload_Move.Location = new System.Drawing.Point(792, 595);
            this.gxtCasseteUnload_Move.Name = "gxtCasseteUnload_Move";
            this.gxtCasseteUnload_Move.Size = new System.Drawing.Size(904, 179);
            this.gxtCasseteUnload_Move.TabIndex = 53;
            this.gxtCasseteUnload_Move.TabStop = false;
            this.gxtCasseteUnload_Move.Text = "     이동     ";
            // 
            // gxtCasseteUnload_MoveB
            // 
            this.gxtCasseteUnload_MoveB.Controls.Add(this.tableLayoutPanel75);
            this.gxtCasseteUnload_MoveB.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCasseteUnload_MoveB.ForeColor = System.Drawing.Color.White;
            this.gxtCasseteUnload_MoveB.Location = new System.Drawing.Point(455, 28);
            this.gxtCasseteUnload_MoveB.Name = "gxtCasseteUnload_MoveB";
            this.gxtCasseteUnload_MoveB.Size = new System.Drawing.Size(443, 145);
            this.gxtCasseteUnload_MoveB.TabIndex = 31;
            this.gxtCasseteUnload_MoveB.TabStop = false;
            this.gxtCasseteUnload_MoveB.Text = "     B    ";
            // 
            // tableLayoutPanel75
            // 
            this.tableLayoutPanel75.ColumnCount = 4;
            this.tableLayoutPanel75.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel75.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel75.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel75.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel75.Controls.Add(this.tableLayoutPanel76, 3, 0);
            this.tableLayoutPanel75.Controls.Add(this.tableLayoutPanel77, 2, 0);
            this.tableLayoutPanel75.Controls.Add(this.tableLayoutPanel78, 1, 0);
            this.tableLayoutPanel75.Controls.Add(this.tableLayoutPanel79, 0, 0);
            this.tableLayoutPanel75.Location = new System.Drawing.Point(5, 17);
            this.tableLayoutPanel75.Name = "tableLayoutPanel75";
            this.tableLayoutPanel75.RowCount = 1;
            this.tableLayoutPanel75.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel75.Size = new System.Drawing.Size(437, 124);
            this.tableLayoutPanel75.TabIndex = 2;
            // 
            // tableLayoutPanel76
            // 
            this.tableLayoutPanel76.ColumnCount = 1;
            this.tableLayoutPanel76.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel76.Controls.Add(this.btnCasseteUnloadMoveBLiftZ2CellOut, 0, 1);
            this.tableLayoutPanel76.Controls.Add(this.btnCasseteUnloadMoveBLiftZ2CstTilt, 0, 0);
            this.tableLayoutPanel76.Location = new System.Drawing.Point(330, 3);
            this.tableLayoutPanel76.Name = "tableLayoutPanel76";
            this.tableLayoutPanel76.RowCount = 2;
            this.tableLayoutPanel76.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel76.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel76.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel76.TabIndex = 3;
            // 
            // btnCasseteUnloadMoveBLiftZ2CellOut
            // 
            this.btnCasseteUnloadMoveBLiftZ2CellOut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadMoveBLiftZ2CellOut.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteUnloadMoveBLiftZ2CellOut.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadMoveBLiftZ2CellOut.Location = new System.Drawing.Point(3, 62);
            this.btnCasseteUnloadMoveBLiftZ2CellOut.Name = "btnCasseteUnloadMoveBLiftZ2CellOut";
            this.btnCasseteUnloadMoveBLiftZ2CellOut.Size = new System.Drawing.Size(97, 53);
            this.btnCasseteUnloadMoveBLiftZ2CellOut.TabIndex = 56;
            this.btnCasseteUnloadMoveBLiftZ2CellOut.Text = "리프트 Z2\r\n셀 배출\r\n시작";
            this.btnCasseteUnloadMoveBLiftZ2CellOut.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadMoveBLiftZ2CstTilt
            // 
            this.btnCasseteUnloadMoveBLiftZ2CstTilt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadMoveBLiftZ2CstTilt.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteUnloadMoveBLiftZ2CstTilt.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadMoveBLiftZ2CstTilt.Location = new System.Drawing.Point(3, 3);
            this.btnCasseteUnloadMoveBLiftZ2CstTilt.Name = "btnCasseteUnloadMoveBLiftZ2CstTilt";
            this.btnCasseteUnloadMoveBLiftZ2CstTilt.Size = new System.Drawing.Size(97, 53);
            this.btnCasseteUnloadMoveBLiftZ2CstTilt.TabIndex = 55;
            this.btnCasseteUnloadMoveBLiftZ2CstTilt.Text = "리프트 Z2\r\n카세트 틸트\r\n측정";
            this.btnCasseteUnloadMoveBLiftZ2CstTilt.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel77
            // 
            this.tableLayoutPanel77.ColumnCount = 1;
            this.tableLayoutPanel77.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel77.Controls.Add(this.btnCasseteUnloadMoveBLiftZ2CstOut, 0, 2);
            this.tableLayoutPanel77.Controls.Add(this.btnCasseteUnloadMoveBLiftZ2CstWait, 0, 0);
            this.tableLayoutPanel77.Controls.Add(this.btnCasseteUnloadMoveBLiftZ2CstIn, 0, 1);
            this.tableLayoutPanel77.Location = new System.Drawing.Point(221, 3);
            this.tableLayoutPanel77.Name = "tableLayoutPanel77";
            this.tableLayoutPanel77.RowCount = 3;
            this.tableLayoutPanel77.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel77.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel77.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel77.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel77.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel77.TabIndex = 2;
            // 
            // btnCasseteUnloadMoveBLiftZ2CstOut
            // 
            this.btnCasseteUnloadMoveBLiftZ2CstOut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadMoveBLiftZ2CstOut.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteUnloadMoveBLiftZ2CstOut.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadMoveBLiftZ2CstOut.Location = new System.Drawing.Point(3, 81);
            this.btnCasseteUnloadMoveBLiftZ2CstOut.Name = "btnCasseteUnloadMoveBLiftZ2CstOut";
            this.btnCasseteUnloadMoveBLiftZ2CstOut.Size = new System.Drawing.Size(97, 34);
            this.btnCasseteUnloadMoveBLiftZ2CstOut.TabIndex = 57;
            this.btnCasseteUnloadMoveBLiftZ2CstOut.Text = "리프트 Z2\r\n카세트 배출";
            this.btnCasseteUnloadMoveBLiftZ2CstOut.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadMoveBLiftZ2CstWait
            // 
            this.btnCasseteUnloadMoveBLiftZ2CstWait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadMoveBLiftZ2CstWait.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteUnloadMoveBLiftZ2CstWait.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadMoveBLiftZ2CstWait.Location = new System.Drawing.Point(3, 3);
            this.btnCasseteUnloadMoveBLiftZ2CstWait.Name = "btnCasseteUnloadMoveBLiftZ2CstWait";
            this.btnCasseteUnloadMoveBLiftZ2CstWait.Size = new System.Drawing.Size(97, 33);
            this.btnCasseteUnloadMoveBLiftZ2CstWait.TabIndex = 55;
            this.btnCasseteUnloadMoveBLiftZ2CstWait.Text = "리프트 Z2\r\n카세트 투입 대기";
            this.btnCasseteUnloadMoveBLiftZ2CstWait.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadMoveBLiftZ2CstIn
            // 
            this.btnCasseteUnloadMoveBLiftZ2CstIn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadMoveBLiftZ2CstIn.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteUnloadMoveBLiftZ2CstIn.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadMoveBLiftZ2CstIn.Location = new System.Drawing.Point(3, 42);
            this.btnCasseteUnloadMoveBLiftZ2CstIn.Name = "btnCasseteUnloadMoveBLiftZ2CstIn";
            this.btnCasseteUnloadMoveBLiftZ2CstIn.Size = new System.Drawing.Size(97, 33);
            this.btnCasseteUnloadMoveBLiftZ2CstIn.TabIndex = 56;
            this.btnCasseteUnloadMoveBLiftZ2CstIn.Text = "리프트 Z2\r\n카세트 투입";
            this.btnCasseteUnloadMoveBLiftZ2CstIn.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel78
            // 
            this.tableLayoutPanel78.ColumnCount = 1;
            this.tableLayoutPanel78.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel78.Controls.Add(this.btnCasseteUnloadMoveBBottomT4LiftOut, 0, 1);
            this.tableLayoutPanel78.Controls.Add(this.btnCasseteUnloadMoveBTopT3CstMove, 0, 0);
            this.tableLayoutPanel78.Location = new System.Drawing.Point(112, 3);
            this.tableLayoutPanel78.Name = "tableLayoutPanel78";
            this.tableLayoutPanel78.RowCount = 2;
            this.tableLayoutPanel78.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel78.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel78.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel78.TabIndex = 1;
            // 
            // btnCasseteUnloadMoveBBottomT4LiftOut
            // 
            this.btnCasseteUnloadMoveBBottomT4LiftOut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadMoveBBottomT4LiftOut.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteUnloadMoveBBottomT4LiftOut.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadMoveBBottomT4LiftOut.Location = new System.Drawing.Point(3, 62);
            this.btnCasseteUnloadMoveBBottomT4LiftOut.Name = "btnCasseteUnloadMoveBBottomT4LiftOut";
            this.btnCasseteUnloadMoveBBottomT4LiftOut.Size = new System.Drawing.Size(97, 53);
            this.btnCasseteUnloadMoveBBottomT4LiftOut.TabIndex = 56;
            this.btnCasseteUnloadMoveBBottomT4LiftOut.Text = "하부 카세트 T4\r\n리프트 배출";
            this.btnCasseteUnloadMoveBBottomT4LiftOut.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadMoveBTopT3CstMove
            // 
            this.btnCasseteUnloadMoveBTopT3CstMove.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadMoveBTopT3CstMove.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteUnloadMoveBTopT3CstMove.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadMoveBTopT3CstMove.Location = new System.Drawing.Point(3, 3);
            this.btnCasseteUnloadMoveBTopT3CstMove.Name = "btnCasseteUnloadMoveBTopT3CstMove";
            this.btnCasseteUnloadMoveBTopT3CstMove.Size = new System.Drawing.Size(97, 53);
            this.btnCasseteUnloadMoveBTopT3CstMove.TabIndex = 55;
            this.btnCasseteUnloadMoveBTopT3CstMove.Text = "상부 카세트 T3\r\n카세트 이동";
            this.btnCasseteUnloadMoveBTopT3CstMove.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel79
            // 
            this.tableLayoutPanel79.ColumnCount = 1;
            this.tableLayoutPanel79.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel79.Controls.Add(this.btnCasseteUnloadMoveBBottomT4CstIn, 0, 1);
            this.tableLayoutPanel79.Controls.Add(this.btnCasseteUnloadMoveBTopT3Out, 0, 0);
            this.tableLayoutPanel79.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel79.Name = "tableLayoutPanel79";
            this.tableLayoutPanel79.RowCount = 2;
            this.tableLayoutPanel79.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel79.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel79.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel79.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel79.TabIndex = 0;
            // 
            // btnCasseteUnloadMoveBBottomT4CstIn
            // 
            this.btnCasseteUnloadMoveBBottomT4CstIn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadMoveBBottomT4CstIn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteUnloadMoveBBottomT4CstIn.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadMoveBBottomT4CstIn.Location = new System.Drawing.Point(3, 62);
            this.btnCasseteUnloadMoveBBottomT4CstIn.Name = "btnCasseteUnloadMoveBBottomT4CstIn";
            this.btnCasseteUnloadMoveBBottomT4CstIn.Size = new System.Drawing.Size(97, 53);
            this.btnCasseteUnloadMoveBBottomT4CstIn.TabIndex = 56;
            this.btnCasseteUnloadMoveBBottomT4CstIn.Text = "하부 카세트 T4\r\n카세트 투입";
            this.btnCasseteUnloadMoveBBottomT4CstIn.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadMoveBTopT3Out
            // 
            this.btnCasseteUnloadMoveBTopT3Out.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadMoveBTopT3Out.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteUnloadMoveBTopT3Out.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadMoveBTopT3Out.Location = new System.Drawing.Point(3, 3);
            this.btnCasseteUnloadMoveBTopT3Out.Name = "btnCasseteUnloadMoveBTopT3Out";
            this.btnCasseteUnloadMoveBTopT3Out.Size = new System.Drawing.Size(97, 53);
            this.btnCasseteUnloadMoveBTopT3Out.TabIndex = 55;
            this.btnCasseteUnloadMoveBTopT3Out.Text = "상부 카세트 T3\r\n카세트 배출";
            this.btnCasseteUnloadMoveBTopT3Out.UseVisualStyleBackColor = false;
            // 
            // gxtCasseteUnload_MoveA
            // 
            this.gxtCasseteUnload_MoveA.Controls.Add(this.tableLayoutPanel70);
            this.gxtCasseteUnload_MoveA.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCasseteUnload_MoveA.ForeColor = System.Drawing.Color.White;
            this.gxtCasseteUnload_MoveA.Location = new System.Drawing.Point(6, 28);
            this.gxtCasseteUnload_MoveA.Name = "gxtCasseteUnload_MoveA";
            this.gxtCasseteUnload_MoveA.Size = new System.Drawing.Size(443, 145);
            this.gxtCasseteUnload_MoveA.TabIndex = 30;
            this.gxtCasseteUnload_MoveA.TabStop = false;
            this.gxtCasseteUnload_MoveA.Text = "     A    ";
            // 
            // tableLayoutPanel70
            // 
            this.tableLayoutPanel70.ColumnCount = 4;
            this.tableLayoutPanel70.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel70.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel70.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel70.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel70.Controls.Add(this.tableLayoutPanel71, 3, 0);
            this.tableLayoutPanel70.Controls.Add(this.tableLayoutPanel72, 2, 0);
            this.tableLayoutPanel70.Controls.Add(this.tableLayoutPanel73, 1, 0);
            this.tableLayoutPanel70.Controls.Add(this.tableLayoutPanel74, 0, 0);
            this.tableLayoutPanel70.Location = new System.Drawing.Point(3, 17);
            this.tableLayoutPanel70.Name = "tableLayoutPanel70";
            this.tableLayoutPanel70.RowCount = 1;
            this.tableLayoutPanel70.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel70.Size = new System.Drawing.Size(437, 124);
            this.tableLayoutPanel70.TabIndex = 1;
            // 
            // tableLayoutPanel71
            // 
            this.tableLayoutPanel71.ColumnCount = 1;
            this.tableLayoutPanel71.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel71.Controls.Add(this.btnCasseteUnloadMoveALiftZ1CellOut, 0, 1);
            this.tableLayoutPanel71.Controls.Add(this.btnCasseteUnloadMoveALiftZ1CstTilt, 0, 0);
            this.tableLayoutPanel71.Location = new System.Drawing.Point(330, 3);
            this.tableLayoutPanel71.Name = "tableLayoutPanel71";
            this.tableLayoutPanel71.RowCount = 2;
            this.tableLayoutPanel71.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel71.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel71.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel71.TabIndex = 3;
            // 
            // btnCasseteUnloadMoveALiftZ1CellOut
            // 
            this.btnCasseteUnloadMoveALiftZ1CellOut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadMoveALiftZ1CellOut.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteUnloadMoveALiftZ1CellOut.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadMoveALiftZ1CellOut.Location = new System.Drawing.Point(3, 62);
            this.btnCasseteUnloadMoveALiftZ1CellOut.Name = "btnCasseteUnloadMoveALiftZ1CellOut";
            this.btnCasseteUnloadMoveALiftZ1CellOut.Size = new System.Drawing.Size(97, 53);
            this.btnCasseteUnloadMoveALiftZ1CellOut.TabIndex = 56;
            this.btnCasseteUnloadMoveALiftZ1CellOut.Text = "리프트 Z1\r\n셀 배출 \r\n시작";
            this.btnCasseteUnloadMoveALiftZ1CellOut.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadMoveALiftZ1CstTilt
            // 
            this.btnCasseteUnloadMoveALiftZ1CstTilt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadMoveALiftZ1CstTilt.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteUnloadMoveALiftZ1CstTilt.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadMoveALiftZ1CstTilt.Location = new System.Drawing.Point(3, 3);
            this.btnCasseteUnloadMoveALiftZ1CstTilt.Name = "btnCasseteUnloadMoveALiftZ1CstTilt";
            this.btnCasseteUnloadMoveALiftZ1CstTilt.Size = new System.Drawing.Size(97, 53);
            this.btnCasseteUnloadMoveALiftZ1CstTilt.TabIndex = 55;
            this.btnCasseteUnloadMoveALiftZ1CstTilt.Text = "리프트 Z1\r\n카세트 틸트\r\n측정";
            this.btnCasseteUnloadMoveALiftZ1CstTilt.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel72
            // 
            this.tableLayoutPanel72.ColumnCount = 1;
            this.tableLayoutPanel72.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel72.Controls.Add(this.btnCasseteUnloadMoveALiftZ1CstOut, 0, 2);
            this.tableLayoutPanel72.Controls.Add(this.btnCasseteUnloadMoveALiftZ1CstWait, 0, 0);
            this.tableLayoutPanel72.Controls.Add(this.btnCasseteUnloadMoveALiftZ1CstIn, 0, 1);
            this.tableLayoutPanel72.Location = new System.Drawing.Point(221, 3);
            this.tableLayoutPanel72.Name = "tableLayoutPanel72";
            this.tableLayoutPanel72.RowCount = 3;
            this.tableLayoutPanel72.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel72.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel72.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel72.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel72.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel72.TabIndex = 2;
            // 
            // btnCasseteUnloadMoveALiftZ1CstOut
            // 
            this.btnCasseteUnloadMoveALiftZ1CstOut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadMoveALiftZ1CstOut.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteUnloadMoveALiftZ1CstOut.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadMoveALiftZ1CstOut.Location = new System.Drawing.Point(3, 81);
            this.btnCasseteUnloadMoveALiftZ1CstOut.Name = "btnCasseteUnloadMoveALiftZ1CstOut";
            this.btnCasseteUnloadMoveALiftZ1CstOut.Size = new System.Drawing.Size(97, 34);
            this.btnCasseteUnloadMoveALiftZ1CstOut.TabIndex = 57;
            this.btnCasseteUnloadMoveALiftZ1CstOut.Text = "리프트 Z1\r\n카세트 배출";
            this.btnCasseteUnloadMoveALiftZ1CstOut.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadMoveALiftZ1CstWait
            // 
            this.btnCasseteUnloadMoveALiftZ1CstWait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadMoveALiftZ1CstWait.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteUnloadMoveALiftZ1CstWait.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadMoveALiftZ1CstWait.Location = new System.Drawing.Point(3, 3);
            this.btnCasseteUnloadMoveALiftZ1CstWait.Name = "btnCasseteUnloadMoveALiftZ1CstWait";
            this.btnCasseteUnloadMoveALiftZ1CstWait.Size = new System.Drawing.Size(97, 33);
            this.btnCasseteUnloadMoveALiftZ1CstWait.TabIndex = 55;
            this.btnCasseteUnloadMoveALiftZ1CstWait.Text = "리프트 Z1\r\n카세트 투입 대기";
            this.btnCasseteUnloadMoveALiftZ1CstWait.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadMoveALiftZ1CstIn
            // 
            this.btnCasseteUnloadMoveALiftZ1CstIn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadMoveALiftZ1CstIn.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteUnloadMoveALiftZ1CstIn.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadMoveALiftZ1CstIn.Location = new System.Drawing.Point(3, 42);
            this.btnCasseteUnloadMoveALiftZ1CstIn.Name = "btnCasseteUnloadMoveALiftZ1CstIn";
            this.btnCasseteUnloadMoveALiftZ1CstIn.Size = new System.Drawing.Size(97, 33);
            this.btnCasseteUnloadMoveALiftZ1CstIn.TabIndex = 56;
            this.btnCasseteUnloadMoveALiftZ1CstIn.Text = "리프트 Z1\r\n카세트 투입";
            this.btnCasseteUnloadMoveALiftZ1CstIn.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel73
            // 
            this.tableLayoutPanel73.ColumnCount = 1;
            this.tableLayoutPanel73.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel73.Controls.Add(this.btnCasseteUnloadMoveABottomT2Move, 0, 1);
            this.tableLayoutPanel73.Controls.Add(this.btnCasseteUnloadMoveATopT1Move, 0, 0);
            this.tableLayoutPanel73.Location = new System.Drawing.Point(112, 3);
            this.tableLayoutPanel73.Name = "tableLayoutPanel73";
            this.tableLayoutPanel73.RowCount = 2;
            this.tableLayoutPanel73.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel73.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel73.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel73.TabIndex = 1;
            // 
            // btnCasseteUnloadMoveABottomT2Move
            // 
            this.btnCasseteUnloadMoveABottomT2Move.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadMoveABottomT2Move.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteUnloadMoveABottomT2Move.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadMoveABottomT2Move.Location = new System.Drawing.Point(3, 62);
            this.btnCasseteUnloadMoveABottomT2Move.Name = "btnCasseteUnloadMoveABottomT2Move";
            this.btnCasseteUnloadMoveABottomT2Move.Size = new System.Drawing.Size(97, 53);
            this.btnCasseteUnloadMoveABottomT2Move.TabIndex = 56;
            this.btnCasseteUnloadMoveABottomT2Move.Text = "하부 카세트 T2\r\n리프트 이동";
            this.btnCasseteUnloadMoveABottomT2Move.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadMoveATopT1Move
            // 
            this.btnCasseteUnloadMoveATopT1Move.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadMoveATopT1Move.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteUnloadMoveATopT1Move.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadMoveATopT1Move.Location = new System.Drawing.Point(3, 3);
            this.btnCasseteUnloadMoveATopT1Move.Name = "btnCasseteUnloadMoveATopT1Move";
            this.btnCasseteUnloadMoveATopT1Move.Size = new System.Drawing.Size(97, 53);
            this.btnCasseteUnloadMoveATopT1Move.TabIndex = 55;
            this.btnCasseteUnloadMoveATopT1Move.Text = "상부 카세트 T1\r\n카세트 이동";
            this.btnCasseteUnloadMoveATopT1Move.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel74
            // 
            this.tableLayoutPanel74.ColumnCount = 1;
            this.tableLayoutPanel74.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel74.Controls.Add(this.btnCasseteUnloadMoveABottomT2In, 0, 1);
            this.tableLayoutPanel74.Controls.Add(this.btnCasseteUnloadMoveATopT1Out, 0, 0);
            this.tableLayoutPanel74.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel74.Name = "tableLayoutPanel74";
            this.tableLayoutPanel74.RowCount = 2;
            this.tableLayoutPanel74.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel74.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel74.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel74.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel74.TabIndex = 0;
            // 
            // btnCasseteUnloadMoveABottomT2In
            // 
            this.btnCasseteUnloadMoveABottomT2In.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadMoveABottomT2In.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteUnloadMoveABottomT2In.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadMoveABottomT2In.Location = new System.Drawing.Point(3, 62);
            this.btnCasseteUnloadMoveABottomT2In.Name = "btnCasseteUnloadMoveABottomT2In";
            this.btnCasseteUnloadMoveABottomT2In.Size = new System.Drawing.Size(97, 53);
            this.btnCasseteUnloadMoveABottomT2In.TabIndex = 56;
            this.btnCasseteUnloadMoveABottomT2In.Text = "하부 카세트 T2\r\n카세트 투입";
            this.btnCasseteUnloadMoveABottomT2In.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadMoveATopT1Out
            // 
            this.btnCasseteUnloadMoveATopT1Out.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadMoveATopT1Out.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteUnloadMoveATopT1Out.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadMoveATopT1Out.Location = new System.Drawing.Point(3, 3);
            this.btnCasseteUnloadMoveATopT1Out.Name = "btnCasseteUnloadMoveATopT1Out";
            this.btnCasseteUnloadMoveATopT1Out.Size = new System.Drawing.Size(97, 53);
            this.btnCasseteUnloadMoveATopT1Out.TabIndex = 55;
            this.btnCasseteUnloadMoveATopT1Out.Text = "상부 카세트 T1\r\n카세트 배출";
            this.btnCasseteUnloadMoveATopT1Out.UseVisualStyleBackColor = false;
            // 
            // gxtCasseteUnload_B
            // 
            this.gxtCasseteUnload_B.Controls.Add(this.tableLayoutPanel6);
            this.gxtCasseteUnload_B.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCasseteUnload_B.ForeColor = System.Drawing.Color.White;
            this.gxtCasseteUnload_B.Location = new System.Drawing.Point(1247, 410);
            this.gxtCasseteUnload_B.Name = "gxtCasseteUnload_B";
            this.gxtCasseteUnload_B.Size = new System.Drawing.Size(449, 179);
            this.gxtCasseteUnload_B.TabIndex = 52;
            this.gxtCasseteUnload_B.TabStop = false;
            this.gxtCasseteUnload_B.Text = "     B     ";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 4;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.Controls.Add(this.btnCasseteUnloadBBottomCstStopperDown, 3, 2);
            this.tableLayoutPanel6.Controls.Add(this.btnCasseteUnloadBBottomCstStopperUp, 2, 2);
            this.tableLayoutPanel6.Controls.Add(this.btnCasseteUnloadBBottomCstUngrib, 1, 2);
            this.tableLayoutPanel6.Controls.Add(this.btnCasseteUnloadBBottomCstGrib, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.btnCasseteUnloadBLiftTiltSlinderDown, 3, 1);
            this.tableLayoutPanel6.Controls.Add(this.btnCasseteUnloadBLiftTiltSlinderUp, 2, 1);
            this.tableLayoutPanel6.Controls.Add(this.btnCasseteUnloadBLiftUngrib, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.btnCasseteUnloadBLiftGrib, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.btnCasseteUnloadBTopCstStopperDown, 3, 0);
            this.tableLayoutPanel6.Controls.Add(this.btnCasseteUnloadBTopCstStopperUp, 2, 0);
            this.tableLayoutPanel6.Controls.Add(this.btnCasseteUnloadBTopCstUngrib, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.btnCasseteUnloadBTopCstGrib, 0, 0);
            this.tableLayoutPanel6.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 3;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(437, 145);
            this.tableLayoutPanel6.TabIndex = 1;
            // 
            // btnCasseteUnloadBBottomCstStopperDown
            // 
            this.btnCasseteUnloadBBottomCstStopperDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadBBottomCstStopperDown.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCasseteUnloadBBottomCstStopperDown.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadBBottomCstStopperDown.Location = new System.Drawing.Point(330, 99);
            this.btnCasseteUnloadBBottomCstStopperDown.Name = "btnCasseteUnloadBBottomCstStopperDown";
            this.btnCasseteUnloadBBottomCstStopperDown.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteUnloadBBottomCstStopperDown.TabIndex = 65;
            this.btnCasseteUnloadBBottomCstStopperDown.Text = "하단 카세트\r\n스토퍼 다운";
            this.btnCasseteUnloadBBottomCstStopperDown.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadBBottomCstStopperUp
            // 
            this.btnCasseteUnloadBBottomCstStopperUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadBBottomCstStopperUp.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCasseteUnloadBBottomCstStopperUp.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadBBottomCstStopperUp.Location = new System.Drawing.Point(221, 99);
            this.btnCasseteUnloadBBottomCstStopperUp.Name = "btnCasseteUnloadBBottomCstStopperUp";
            this.btnCasseteUnloadBBottomCstStopperUp.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteUnloadBBottomCstStopperUp.TabIndex = 64;
            this.btnCasseteUnloadBBottomCstStopperUp.Text = "하단 카세트\r\n스토퍼 업";
            this.btnCasseteUnloadBBottomCstStopperUp.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadBBottomCstUngrib
            // 
            this.btnCasseteUnloadBBottomCstUngrib.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadBBottomCstUngrib.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCasseteUnloadBBottomCstUngrib.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadBBottomCstUngrib.Location = new System.Drawing.Point(112, 99);
            this.btnCasseteUnloadBBottomCstUngrib.Name = "btnCasseteUnloadBBottomCstUngrib";
            this.btnCasseteUnloadBBottomCstUngrib.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteUnloadBBottomCstUngrib.TabIndex = 63;
            this.btnCasseteUnloadBBottomCstUngrib.Text = "하단 카세트\r\n언그립";
            this.btnCasseteUnloadBBottomCstUngrib.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadBBottomCstGrib
            // 
            this.btnCasseteUnloadBBottomCstGrib.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadBBottomCstGrib.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCasseteUnloadBBottomCstGrib.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadBBottomCstGrib.Location = new System.Drawing.Point(3, 99);
            this.btnCasseteUnloadBBottomCstGrib.Name = "btnCasseteUnloadBBottomCstGrib";
            this.btnCasseteUnloadBBottomCstGrib.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteUnloadBBottomCstGrib.TabIndex = 62;
            this.btnCasseteUnloadBBottomCstGrib.Text = "하단 카세트\r\n그립";
            this.btnCasseteUnloadBBottomCstGrib.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadBLiftTiltSlinderDown
            // 
            this.btnCasseteUnloadBLiftTiltSlinderDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadBLiftTiltSlinderDown.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCasseteUnloadBLiftTiltSlinderDown.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadBLiftTiltSlinderDown.Location = new System.Drawing.Point(330, 51);
            this.btnCasseteUnloadBLiftTiltSlinderDown.Name = "btnCasseteUnloadBLiftTiltSlinderDown";
            this.btnCasseteUnloadBLiftTiltSlinderDown.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteUnloadBLiftTiltSlinderDown.TabIndex = 61;
            this.btnCasseteUnloadBLiftTiltSlinderDown.Text = "리프트 틸트\r\n실린더 다운";
            this.btnCasseteUnloadBLiftTiltSlinderDown.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadBLiftTiltSlinderUp
            // 
            this.btnCasseteUnloadBLiftTiltSlinderUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadBLiftTiltSlinderUp.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCasseteUnloadBLiftTiltSlinderUp.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadBLiftTiltSlinderUp.Location = new System.Drawing.Point(221, 51);
            this.btnCasseteUnloadBLiftTiltSlinderUp.Name = "btnCasseteUnloadBLiftTiltSlinderUp";
            this.btnCasseteUnloadBLiftTiltSlinderUp.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteUnloadBLiftTiltSlinderUp.TabIndex = 60;
            this.btnCasseteUnloadBLiftTiltSlinderUp.Text = "리프트 틸트\r\n실린더 업";
            this.btnCasseteUnloadBLiftTiltSlinderUp.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadBLiftUngrib
            // 
            this.btnCasseteUnloadBLiftUngrib.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadBLiftUngrib.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCasseteUnloadBLiftUngrib.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadBLiftUngrib.Location = new System.Drawing.Point(112, 51);
            this.btnCasseteUnloadBLiftUngrib.Name = "btnCasseteUnloadBLiftUngrib";
            this.btnCasseteUnloadBLiftUngrib.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteUnloadBLiftUngrib.TabIndex = 59;
            this.btnCasseteUnloadBLiftUngrib.Text = "리프트\r\n언그립";
            this.btnCasseteUnloadBLiftUngrib.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadBLiftGrib
            // 
            this.btnCasseteUnloadBLiftGrib.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadBLiftGrib.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCasseteUnloadBLiftGrib.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadBLiftGrib.Location = new System.Drawing.Point(3, 51);
            this.btnCasseteUnloadBLiftGrib.Name = "btnCasseteUnloadBLiftGrib";
            this.btnCasseteUnloadBLiftGrib.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteUnloadBLiftGrib.TabIndex = 58;
            this.btnCasseteUnloadBLiftGrib.Text = "리프트\r\n그립";
            this.btnCasseteUnloadBLiftGrib.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadBTopCstStopperDown
            // 
            this.btnCasseteUnloadBTopCstStopperDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadBTopCstStopperDown.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCasseteUnloadBTopCstStopperDown.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadBTopCstStopperDown.Location = new System.Drawing.Point(330, 3);
            this.btnCasseteUnloadBTopCstStopperDown.Name = "btnCasseteUnloadBTopCstStopperDown";
            this.btnCasseteUnloadBTopCstStopperDown.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteUnloadBTopCstStopperDown.TabIndex = 57;
            this.btnCasseteUnloadBTopCstStopperDown.Text = "상단 카세트\r\n스토퍼 다운";
            this.btnCasseteUnloadBTopCstStopperDown.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadBTopCstStopperUp
            // 
            this.btnCasseteUnloadBTopCstStopperUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadBTopCstStopperUp.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCasseteUnloadBTopCstStopperUp.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadBTopCstStopperUp.Location = new System.Drawing.Point(221, 3);
            this.btnCasseteUnloadBTopCstStopperUp.Name = "btnCasseteUnloadBTopCstStopperUp";
            this.btnCasseteUnloadBTopCstStopperUp.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteUnloadBTopCstStopperUp.TabIndex = 56;
            this.btnCasseteUnloadBTopCstStopperUp.Text = "상단 카세트\r\n스토퍼 업";
            this.btnCasseteUnloadBTopCstStopperUp.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadBTopCstUngrib
            // 
            this.btnCasseteUnloadBTopCstUngrib.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadBTopCstUngrib.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCasseteUnloadBTopCstUngrib.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadBTopCstUngrib.Location = new System.Drawing.Point(112, 3);
            this.btnCasseteUnloadBTopCstUngrib.Name = "btnCasseteUnloadBTopCstUngrib";
            this.btnCasseteUnloadBTopCstUngrib.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteUnloadBTopCstUngrib.TabIndex = 55;
            this.btnCasseteUnloadBTopCstUngrib.Text = "상단 카세트\r\n언그립";
            this.btnCasseteUnloadBTopCstUngrib.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadBTopCstGrib
            // 
            this.btnCasseteUnloadBTopCstGrib.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadBTopCstGrib.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCasseteUnloadBTopCstGrib.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadBTopCstGrib.Location = new System.Drawing.Point(3, 3);
            this.btnCasseteUnloadBTopCstGrib.Name = "btnCasseteUnloadBTopCstGrib";
            this.btnCasseteUnloadBTopCstGrib.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteUnloadBTopCstGrib.TabIndex = 54;
            this.btnCasseteUnloadBTopCstGrib.Text = "상단 카세트\r\n그립";
            this.btnCasseteUnloadBTopCstGrib.UseVisualStyleBackColor = false;
            // 
            // gxtCasseteUnload_A
            // 
            this.gxtCasseteUnload_A.Controls.Add(this.tableLayoutPanel5);
            this.gxtCasseteUnload_A.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCasseteUnload_A.ForeColor = System.Drawing.Color.White;
            this.gxtCasseteUnload_A.Location = new System.Drawing.Point(792, 410);
            this.gxtCasseteUnload_A.Name = "gxtCasseteUnload_A";
            this.gxtCasseteUnload_A.Size = new System.Drawing.Size(449, 179);
            this.gxtCasseteUnload_A.TabIndex = 51;
            this.gxtCasseteUnload_A.TabStop = false;
            this.gxtCasseteUnload_A.Text = "     A    ";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 4;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.Controls.Add(this.btnCasseteUnloadABottomCstStopperDown, 3, 2);
            this.tableLayoutPanel5.Controls.Add(this.btnCasseteUnloadABottomCstStopperUp, 2, 2);
            this.tableLayoutPanel5.Controls.Add(this.btnCasseteUnloadABottomCstUngrib, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.btnCasseteUnloadABottomCstGrib, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.btnCasseteUnloadALiftTiltSlinderDown, 3, 1);
            this.tableLayoutPanel5.Controls.Add(this.btnCasseteUnloadALiftTiltSlinderUp, 2, 1);
            this.tableLayoutPanel5.Controls.Add(this.btnCasseteUnloadALiftUngrib, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.btnCasseteUnloadALiftGrib, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.btnCasseteUnloadATopCstStopperDown, 3, 0);
            this.tableLayoutPanel5.Controls.Add(this.btnCasseteUnloadATopCstStopperUp, 2, 0);
            this.tableLayoutPanel5.Controls.Add(this.btnCasseteUnloadATopCstUngrib, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.btnCasseteUnloadATopCstGrib, 0, 0);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 3;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(437, 145);
            this.tableLayoutPanel5.TabIndex = 1;
            // 
            // btnCasseteUnloadABottomCstStopperDown
            // 
            this.btnCasseteUnloadABottomCstStopperDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadABottomCstStopperDown.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCasseteUnloadABottomCstStopperDown.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadABottomCstStopperDown.Location = new System.Drawing.Point(330, 99);
            this.btnCasseteUnloadABottomCstStopperDown.Name = "btnCasseteUnloadABottomCstStopperDown";
            this.btnCasseteUnloadABottomCstStopperDown.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteUnloadABottomCstStopperDown.TabIndex = 65;
            this.btnCasseteUnloadABottomCstStopperDown.Text = "하단 카세트\r\n스토퍼 다운";
            this.btnCasseteUnloadABottomCstStopperDown.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadABottomCstStopperUp
            // 
            this.btnCasseteUnloadABottomCstStopperUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadABottomCstStopperUp.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCasseteUnloadABottomCstStopperUp.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadABottomCstStopperUp.Location = new System.Drawing.Point(221, 99);
            this.btnCasseteUnloadABottomCstStopperUp.Name = "btnCasseteUnloadABottomCstStopperUp";
            this.btnCasseteUnloadABottomCstStopperUp.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteUnloadABottomCstStopperUp.TabIndex = 64;
            this.btnCasseteUnloadABottomCstStopperUp.Text = "하단 카세트\r\n스토퍼 업";
            this.btnCasseteUnloadABottomCstStopperUp.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadABottomCstUngrib
            // 
            this.btnCasseteUnloadABottomCstUngrib.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadABottomCstUngrib.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCasseteUnloadABottomCstUngrib.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadABottomCstUngrib.Location = new System.Drawing.Point(112, 99);
            this.btnCasseteUnloadABottomCstUngrib.Name = "btnCasseteUnloadABottomCstUngrib";
            this.btnCasseteUnloadABottomCstUngrib.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteUnloadABottomCstUngrib.TabIndex = 63;
            this.btnCasseteUnloadABottomCstUngrib.Text = "하단 카세트\r\n언그립";
            this.btnCasseteUnloadABottomCstUngrib.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadABottomCstGrib
            // 
            this.btnCasseteUnloadABottomCstGrib.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadABottomCstGrib.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCasseteUnloadABottomCstGrib.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadABottomCstGrib.Location = new System.Drawing.Point(3, 99);
            this.btnCasseteUnloadABottomCstGrib.Name = "btnCasseteUnloadABottomCstGrib";
            this.btnCasseteUnloadABottomCstGrib.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteUnloadABottomCstGrib.TabIndex = 62;
            this.btnCasseteUnloadABottomCstGrib.Text = "하단 카세트\r\n그립";
            this.btnCasseteUnloadABottomCstGrib.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadALiftTiltSlinderDown
            // 
            this.btnCasseteUnloadALiftTiltSlinderDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadALiftTiltSlinderDown.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCasseteUnloadALiftTiltSlinderDown.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadALiftTiltSlinderDown.Location = new System.Drawing.Point(330, 51);
            this.btnCasseteUnloadALiftTiltSlinderDown.Name = "btnCasseteUnloadALiftTiltSlinderDown";
            this.btnCasseteUnloadALiftTiltSlinderDown.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteUnloadALiftTiltSlinderDown.TabIndex = 61;
            this.btnCasseteUnloadALiftTiltSlinderDown.Text = "리프트 틸트\r\n실린더 다운";
            this.btnCasseteUnloadALiftTiltSlinderDown.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadALiftTiltSlinderUp
            // 
            this.btnCasseteUnloadALiftTiltSlinderUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadALiftTiltSlinderUp.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCasseteUnloadALiftTiltSlinderUp.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadALiftTiltSlinderUp.Location = new System.Drawing.Point(221, 51);
            this.btnCasseteUnloadALiftTiltSlinderUp.Name = "btnCasseteUnloadALiftTiltSlinderUp";
            this.btnCasseteUnloadALiftTiltSlinderUp.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteUnloadALiftTiltSlinderUp.TabIndex = 60;
            this.btnCasseteUnloadALiftTiltSlinderUp.Text = "리프트 틸트\r\n실린더 업";
            this.btnCasseteUnloadALiftTiltSlinderUp.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadALiftUngrib
            // 
            this.btnCasseteUnloadALiftUngrib.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadALiftUngrib.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCasseteUnloadALiftUngrib.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadALiftUngrib.Location = new System.Drawing.Point(112, 51);
            this.btnCasseteUnloadALiftUngrib.Name = "btnCasseteUnloadALiftUngrib";
            this.btnCasseteUnloadALiftUngrib.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteUnloadALiftUngrib.TabIndex = 59;
            this.btnCasseteUnloadALiftUngrib.Text = "리프트\r\n언그립";
            this.btnCasseteUnloadALiftUngrib.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadALiftGrib
            // 
            this.btnCasseteUnloadALiftGrib.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadALiftGrib.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCasseteUnloadALiftGrib.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadALiftGrib.Location = new System.Drawing.Point(3, 51);
            this.btnCasseteUnloadALiftGrib.Name = "btnCasseteUnloadALiftGrib";
            this.btnCasseteUnloadALiftGrib.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteUnloadALiftGrib.TabIndex = 58;
            this.btnCasseteUnloadALiftGrib.Text = "리프트\r\n그립";
            this.btnCasseteUnloadALiftGrib.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadATopCstStopperDown
            // 
            this.btnCasseteUnloadATopCstStopperDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadATopCstStopperDown.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCasseteUnloadATopCstStopperDown.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadATopCstStopperDown.Location = new System.Drawing.Point(330, 3);
            this.btnCasseteUnloadATopCstStopperDown.Name = "btnCasseteUnloadATopCstStopperDown";
            this.btnCasseteUnloadATopCstStopperDown.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteUnloadATopCstStopperDown.TabIndex = 57;
            this.btnCasseteUnloadATopCstStopperDown.Text = "상단 카세트\r\n스토퍼 다운";
            this.btnCasseteUnloadATopCstStopperDown.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadATopCstStopperUp
            // 
            this.btnCasseteUnloadATopCstStopperUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadATopCstStopperUp.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCasseteUnloadATopCstStopperUp.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadATopCstStopperUp.Location = new System.Drawing.Point(221, 3);
            this.btnCasseteUnloadATopCstStopperUp.Name = "btnCasseteUnloadATopCstStopperUp";
            this.btnCasseteUnloadATopCstStopperUp.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteUnloadATopCstStopperUp.TabIndex = 56;
            this.btnCasseteUnloadATopCstStopperUp.Text = "상단 카세트\r\n스토퍼 업";
            this.btnCasseteUnloadATopCstStopperUp.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadATopCstUngrib
            // 
            this.btnCasseteUnloadATopCstUngrib.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadATopCstUngrib.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCasseteUnloadATopCstUngrib.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadATopCstUngrib.Location = new System.Drawing.Point(112, 3);
            this.btnCasseteUnloadATopCstUngrib.Name = "btnCasseteUnloadATopCstUngrib";
            this.btnCasseteUnloadATopCstUngrib.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteUnloadATopCstUngrib.TabIndex = 55;
            this.btnCasseteUnloadATopCstUngrib.Text = "상단 카세트\r\n언그립";
            this.btnCasseteUnloadATopCstUngrib.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadATopCstGrib
            // 
            this.btnCasseteUnloadATopCstGrib.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadATopCstGrib.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCasseteUnloadATopCstGrib.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadATopCstGrib.Location = new System.Drawing.Point(3, 3);
            this.btnCasseteUnloadATopCstGrib.Name = "btnCasseteUnloadATopCstGrib";
            this.btnCasseteUnloadATopCstGrib.Size = new System.Drawing.Size(103, 42);
            this.btnCasseteUnloadATopCstGrib.TabIndex = 54;
            this.btnCasseteUnloadATopCstGrib.Text = "상단 카세트\r\n그립";
            this.btnCasseteUnloadATopCstGrib.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadSave
            // 
            this.btnCasseteUnloadSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadSave.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteUnloadSave.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadSave.Location = new System.Drawing.Point(1482, 783);
            this.btnCasseteUnloadSave.Name = "btnCasseteUnloadSave";
            this.btnCasseteUnloadSave.Size = new System.Drawing.Size(228, 28);
            this.btnCasseteUnloadSave.TabIndex = 50;
            this.btnCasseteUnloadSave.Text = "저장";
            this.btnCasseteUnloadSave.UseVisualStyleBackColor = false;
            // 
            // gxtCasseteUnload
            // 
            this.gxtCasseteUnload.Controls.Add(this.btnCasseteUnloadMuting4);
            this.gxtCasseteUnload.Controls.Add(this.btnCasseteUnloadMuting2);
            this.gxtCasseteUnload.Controls.Add(this.btnCasseteUnloadMuting3);
            this.gxtCasseteUnload.Controls.Add(this.btnCasseteUnloadMuting1);
            this.gxtCasseteUnload.Controls.Add(this.btnCasseteUnloadMutingOut);
            this.gxtCasseteUnload.Controls.Add(this.btncasseteUnloadMutingIn);
            this.gxtCasseteUnload.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtCasseteUnload.ForeColor = System.Drawing.Color.White;
            this.gxtCasseteUnload.Location = new System.Drawing.Point(1483, 223);
            this.gxtCasseteUnload.Name = "gxtCasseteUnload";
            this.gxtCasseteUnload.Size = new System.Drawing.Size(212, 181);
            this.gxtCasseteUnload.TabIndex = 49;
            this.gxtCasseteUnload.TabStop = false;
            this.gxtCasseteUnload.Text = "     뮤팅     ";
            // 
            // btnCasseteUnloadMuting4
            // 
            this.btnCasseteUnloadMuting4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadMuting4.Location = new System.Drawing.Point(107, 125);
            this.btnCasseteUnloadMuting4.Name = "btnCasseteUnloadMuting4";
            this.btnCasseteUnloadMuting4.Size = new System.Drawing.Size(99, 44);
            this.btnCasseteUnloadMuting4.TabIndex = 5;
            this.btnCasseteUnloadMuting4.Text = "뮤팅 4";
            this.btnCasseteUnloadMuting4.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadMuting2
            // 
            this.btnCasseteUnloadMuting2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadMuting2.Location = new System.Drawing.Point(6, 125);
            this.btnCasseteUnloadMuting2.Name = "btnCasseteUnloadMuting2";
            this.btnCasseteUnloadMuting2.Size = new System.Drawing.Size(99, 44);
            this.btnCasseteUnloadMuting2.TabIndex = 4;
            this.btnCasseteUnloadMuting2.Text = "뮤팅 2";
            this.btnCasseteUnloadMuting2.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadMuting3
            // 
            this.btnCasseteUnloadMuting3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadMuting3.Location = new System.Drawing.Point(107, 75);
            this.btnCasseteUnloadMuting3.Name = "btnCasseteUnloadMuting3";
            this.btnCasseteUnloadMuting3.Size = new System.Drawing.Size(99, 44);
            this.btnCasseteUnloadMuting3.TabIndex = 3;
            this.btnCasseteUnloadMuting3.Text = "뮤팅 3";
            this.btnCasseteUnloadMuting3.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadMuting1
            // 
            this.btnCasseteUnloadMuting1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadMuting1.Location = new System.Drawing.Point(6, 75);
            this.btnCasseteUnloadMuting1.Name = "btnCasseteUnloadMuting1";
            this.btnCasseteUnloadMuting1.Size = new System.Drawing.Size(99, 44);
            this.btnCasseteUnloadMuting1.TabIndex = 2;
            this.btnCasseteUnloadMuting1.Text = "뮤팅 1";
            this.btnCasseteUnloadMuting1.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadMutingOut
            // 
            this.btnCasseteUnloadMutingOut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadMutingOut.Location = new System.Drawing.Point(107, 29);
            this.btnCasseteUnloadMutingOut.Name = "btnCasseteUnloadMutingOut";
            this.btnCasseteUnloadMutingOut.Size = new System.Drawing.Size(99, 44);
            this.btnCasseteUnloadMutingOut.TabIndex = 1;
            this.btnCasseteUnloadMutingOut.Text = "뮤팅 아웃";
            this.btnCasseteUnloadMutingOut.UseVisualStyleBackColor = false;
            // 
            // btncasseteUnloadMutingIn
            // 
            this.btncasseteUnloadMutingIn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btncasseteUnloadMutingIn.Location = new System.Drawing.Point(6, 28);
            this.btncasseteUnloadMutingIn.Name = "btncasseteUnloadMutingIn";
            this.btncasseteUnloadMutingIn.Size = new System.Drawing.Size(99, 44);
            this.btncasseteUnloadMutingIn.TabIndex = 0;
            this.btncasseteUnloadMutingIn.Text = "뮤팅 인";
            this.btncasseteUnloadMutingIn.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadSpeedSetting
            // 
            this.btnCasseteUnloadSpeedSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadSpeedSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCasseteUnloadSpeedSetting.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadSpeedSetting.Location = new System.Drawing.Point(1291, 315);
            this.btnCasseteUnloadSpeedSetting.Name = "btnCasseteUnloadSpeedSetting";
            this.btnCasseteUnloadSpeedSetting.Size = new System.Drawing.Size(90, 27);
            this.btnCasseteUnloadSpeedSetting.TabIndex = 48;
            this.btnCasseteUnloadSpeedSetting.Text = "속도 설정";
            this.btnCasseteUnloadSpeedSetting.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadAllSetting
            // 
            this.btnCasseteUnloadAllSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadAllSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCasseteUnloadAllSetting.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadAllSetting.Location = new System.Drawing.Point(1387, 273);
            this.btnCasseteUnloadAllSetting.Name = "btnCasseteUnloadAllSetting";
            this.btnCasseteUnloadAllSetting.Size = new System.Drawing.Size(90, 27);
            this.btnCasseteUnloadAllSetting.TabIndex = 47;
            this.btnCasseteUnloadAllSetting.Text = "전체 설정";
            this.btnCasseteUnloadAllSetting.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadLocationSetting
            // 
            this.btnCasseteUnloadLocationSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadLocationSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCasseteUnloadLocationSetting.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadLocationSetting.Location = new System.Drawing.Point(1071, 315);
            this.btnCasseteUnloadLocationSetting.Name = "btnCasseteUnloadLocationSetting";
            this.btnCasseteUnloadLocationSetting.Size = new System.Drawing.Size(90, 27);
            this.btnCasseteUnloadLocationSetting.TabIndex = 46;
            this.btnCasseteUnloadLocationSetting.Text = "위치 설정";
            this.btnCasseteUnloadLocationSetting.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadMoveLocation
            // 
            this.btnCasseteUnloadMoveLocation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadMoveLocation.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btnCasseteUnloadMoveLocation.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadMoveLocation.Location = new System.Drawing.Point(975, 315);
            this.btnCasseteUnloadMoveLocation.Name = "btnCasseteUnloadMoveLocation";
            this.btnCasseteUnloadMoveLocation.Size = new System.Drawing.Size(90, 27);
            this.btnCasseteUnloadMoveLocation.TabIndex = 45;
            this.btnCasseteUnloadMoveLocation.Text = "위치 이동";
            this.btnCasseteUnloadMoveLocation.UseVisualStyleBackColor = false;
            // 
            // txtCasseteUnloadLocation
            // 
            this.txtCasseteUnloadLocation.Font = new System.Drawing.Font("굴림", 13F);
            this.txtCasseteUnloadLocation.Location = new System.Drawing.Point(879, 316);
            this.txtCasseteUnloadLocation.Name = "txtCasseteUnloadLocation";
            this.txtCasseteUnloadLocation.Size = new System.Drawing.Size(90, 27);
            this.txtCasseteUnloadLocation.TabIndex = 44;
            // 
            // txtCasseteUnloadSpeed
            // 
            this.txtCasseteUnloadSpeed.Font = new System.Drawing.Font("굴림", 13F);
            this.txtCasseteUnloadSpeed.Location = new System.Drawing.Point(1291, 273);
            this.txtCasseteUnloadSpeed.Name = "txtCasseteUnloadSpeed";
            this.txtCasseteUnloadSpeed.Size = new System.Drawing.Size(90, 27);
            this.txtCasseteUnloadSpeed.TabIndex = 43;
            // 
            // txtCasseteUnloadCurrentLocation
            // 
            this.txtCasseteUnloadCurrentLocation.Font = new System.Drawing.Font("굴림", 13F);
            this.txtCasseteUnloadCurrentLocation.Location = new System.Drawing.Point(1084, 274);
            this.txtCasseteUnloadCurrentLocation.Name = "txtCasseteUnloadCurrentLocation";
            this.txtCasseteUnloadCurrentLocation.Size = new System.Drawing.Size(90, 27);
            this.txtCasseteUnloadCurrentLocation.TabIndex = 42;
            // 
            // txtCasseteUnload_SelectedShaft
            // 
            this.txtCasseteUnload_SelectedShaft.Font = new System.Drawing.Font("굴림", 13F);
            this.txtCasseteUnload_SelectedShaft.Location = new System.Drawing.Point(879, 273);
            this.txtCasseteUnload_SelectedShaft.Name = "txtCasseteUnload_SelectedShaft";
            this.txtCasseteUnload_SelectedShaft.Size = new System.Drawing.Size(90, 27);
            this.txtCasseteUnload_SelectedShaft.TabIndex = 41;
            // 
            // lbl_CasseteUnload_Location
            // 
            this.lbl_CasseteUnload_Location.AutoSize = true;
            this.lbl_CasseteUnload_Location.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CasseteUnload_Location.ForeColor = System.Drawing.Color.White;
            this.lbl_CasseteUnload_Location.Location = new System.Drawing.Point(801, 316);
            this.lbl_CasseteUnload_Location.Name = "lbl_CasseteUnload_Location";
            this.lbl_CasseteUnload_Location.Size = new System.Drawing.Size(84, 21);
            this.lbl_CasseteUnload_Location.TabIndex = 40;
            this.lbl_CasseteUnload_Location.Text = "위치[mm]";
            // 
            // lbl_CasseteUnload_Speed
            // 
            this.lbl_CasseteUnload_Speed.AutoSize = true;
            this.lbl_CasseteUnload_Speed.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CasseteUnload_Speed.ForeColor = System.Drawing.Color.White;
            this.lbl_CasseteUnload_Speed.Location = new System.Drawing.Point(1179, 275);
            this.lbl_CasseteUnload_Speed.Name = "lbl_CasseteUnload_Speed";
            this.lbl_CasseteUnload_Speed.Size = new System.Drawing.Size(115, 21);
            this.lbl_CasseteUnload_Speed.TabIndex = 39;
            this.lbl_CasseteUnload_Speed.Text = "속도[mm/sec]";
            // 
            // lbl_CasseteUnload_SelectedShaft
            // 
            this.lbl_CasseteUnload_SelectedShaft.AutoSize = true;
            this.lbl_CasseteUnload_SelectedShaft.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CasseteUnload_SelectedShaft.ForeColor = System.Drawing.Color.White;
            this.lbl_CasseteUnload_SelectedShaft.Location = new System.Drawing.Point(805, 274);
            this.lbl_CasseteUnload_SelectedShaft.Name = "lbl_CasseteUnload_SelectedShaft";
            this.lbl_CasseteUnload_SelectedShaft.Size = new System.Drawing.Size(80, 21);
            this.lbl_CasseteUnload_SelectedShaft.TabIndex = 38;
            this.lbl_CasseteUnload_SelectedShaft.Text = "선택된 축";
            // 
            // txtCasseteUnload_LocationSetting
            // 
            this.txtCasseteUnload_LocationSetting.BackColor = System.Drawing.SystemColors.Highlight;
            this.txtCasseteUnload_LocationSetting.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.txtCasseteUnload_LocationSetting.ForeColor = System.Drawing.Color.White;
            this.txtCasseteUnload_LocationSetting.Location = new System.Drawing.Point(792, 223);
            this.txtCasseteUnload_LocationSetting.Name = "txtCasseteUnload_LocationSetting";
            this.txtCasseteUnload_LocationSetting.ReadOnly = true;
            this.txtCasseteUnload_LocationSetting.Size = new System.Drawing.Size(89, 27);
            this.txtCasseteUnload_LocationSetting.TabIndex = 37;
            this.txtCasseteUnload_LocationSetting.Text = "위치값 설정";
            this.txtCasseteUnload_LocationSetting.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbl_CasseteUnload_CurrentLocation
            // 
            this.lbl_CasseteUnload_CurrentLocation.AutoSize = true;
            this.lbl_CasseteUnload_CurrentLocation.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CasseteUnload_CurrentLocation.ForeColor = System.Drawing.Color.White;
            this.lbl_CasseteUnload_CurrentLocation.Location = new System.Drawing.Point(975, 274);
            this.lbl_CasseteUnload_CurrentLocation.Name = "lbl_CasseteUnload_CurrentLocation";
            this.lbl_CasseteUnload_CurrentLocation.Size = new System.Drawing.Size(116, 21);
            this.lbl_CasseteUnload_CurrentLocation.TabIndex = 36;
            this.lbl_CasseteUnload_CurrentLocation.Text = "현재위치[mm]";
            // 
            // btncasseteUnloadGrabSwitch3
            // 
            this.btncasseteUnloadGrabSwitch3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btncasseteUnloadGrabSwitch3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btncasseteUnloadGrabSwitch3.ForeColor = System.Drawing.Color.White;
            this.btncasseteUnloadGrabSwitch3.Location = new System.Drawing.Point(1514, 6);
            this.btncasseteUnloadGrabSwitch3.Name = "btncasseteUnloadGrabSwitch3";
            this.btncasseteUnloadGrabSwitch3.Size = new System.Drawing.Size(172, 23);
            this.btncasseteUnloadGrabSwitch3.TabIndex = 35;
            this.btncasseteUnloadGrabSwitch3.Text = "Grab Switch 3";
            this.btncasseteUnloadGrabSwitch3.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadGrabSwitch2
            // 
            this.btnCasseteUnloadGrabSwitch2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadGrabSwitch2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteUnloadGrabSwitch2.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadGrabSwitch2.Location = new System.Drawing.Point(1336, 6);
            this.btnCasseteUnloadGrabSwitch2.Name = "btnCasseteUnloadGrabSwitch2";
            this.btnCasseteUnloadGrabSwitch2.Size = new System.Drawing.Size(172, 23);
            this.btnCasseteUnloadGrabSwitch2.TabIndex = 34;
            this.btnCasseteUnloadGrabSwitch2.Text = "Grab Switch 2";
            this.btnCasseteUnloadGrabSwitch2.UseVisualStyleBackColor = false;
            // 
            // btnCasseteUnloadGrabSwitch1
            // 
            this.btnCasseteUnloadGrabSwitch1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCasseteUnloadGrabSwitch1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCasseteUnloadGrabSwitch1.ForeColor = System.Drawing.Color.White;
            this.btnCasseteUnloadGrabSwitch1.Location = new System.Drawing.Point(1158, 6);
            this.btnCasseteUnloadGrabSwitch1.Name = "btnCasseteUnloadGrabSwitch1";
            this.btnCasseteUnloadGrabSwitch1.Size = new System.Drawing.Size(172, 23);
            this.btnCasseteUnloadGrabSwitch1.TabIndex = 33;
            this.btnCasseteUnloadGrabSwitch1.Text = "Grab Switch 1";
            this.btnCasseteUnloadGrabSwitch1.UseVisualStyleBackColor = false;
            // 
            // lvCasseteUnload
            // 
            this.lvCasseteUnload.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.col_CasseteUnload_NamebyLocation,
            this.col_CasseteUnload_Shaft,
            this.col_CasseteUnload_Location,
            this.col_CasseteUnload_Speed});
            this.lvCasseteUnload.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lvCasseteUnload.GridLines = true;
            this.lvCasseteUnload.Location = new System.Drawing.Point(17, 16);
            this.lvCasseteUnload.Name = "lvCasseteUnload";
            this.lvCasseteUnload.Size = new System.Drawing.Size(758, 734);
            this.lvCasseteUnload.TabIndex = 32;
            this.lvCasseteUnload.UseCompatibleStateImageBehavior = false;
            this.lvCasseteUnload.View = System.Windows.Forms.View.Details;
            // 
            // col_CasseteUnload_NamebyLocation
            // 
            this.col_CasseteUnload_NamebyLocation.Text = "위치별 명칭";
            this.col_CasseteUnload_NamebyLocation.Width = 430;
            // 
            // col_CasseteUnload_Shaft
            // 
            this.col_CasseteUnload_Shaft.Text = "축";
            this.col_CasseteUnload_Shaft.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // col_CasseteUnload_Location
            // 
            this.col_CasseteUnload_Location.Text = "위치[mm]";
            this.col_CasseteUnload_Location.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_CasseteUnload_Location.Width = 125;
            // 
            // col_CasseteUnload_Speed
            // 
            this.col_CasseteUnload_Speed.Text = "속도[mm/s]";
            this.col_CasseteUnload_Speed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_CasseteUnload_Speed.Width = 125;
            // 
            // ajin_Setting_CasseteUnload
            // 
            this.ajin_Setting_CasseteUnload.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CasseteUnload.Location = new System.Drawing.Point(792, 34);
            this.ajin_Setting_CasseteUnload.Name = "ajin_Setting_CasseteUnload";
            this.ajin_Setting_CasseteUnload.Servo = null;
            this.ajin_Setting_CasseteUnload.Size = new System.Drawing.Size(903, 183);
            this.ajin_Setting_CasseteUnload.TabIndex = 31;
            // 
            // Parameter_location
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Controls.Add(this.tc_iostatus_info);
            this.Name = "Parameter_location";
            this.Size = new System.Drawing.Size(1740, 875);
            this.tc_iostatus_info.ResumeLayout(false);
            this.tp_CasseteLoad.ResumeLayout(false);
            this.tp_CasseteLoad.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.gxtCasseteLoad_Move.ResumeLayout(false);
            this.gxtCasseteLoad_MoveB.ResumeLayout(false);
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel15.ResumeLayout(false);
            this.tableLayoutPanel16.ResumeLayout(false);
            this.gxtCasseteLoad_MoveA.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.gxtCasseteLoad_B.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.gxtCasseteLoad_Muting.ResumeLayout(false);
            this.tp_Cellpurge.ResumeLayout(false);
            this.tp_Cellpurge.PerformLayout();
            this.gxtCellpurge_Move.ResumeLayout(false);
            this.gxtCellpurge_MoveCenter.ResumeLayout(false);
            this.tableLayoutPanel21.ResumeLayout(false);
            this.gxtCellpurge_MoveB.ResumeLayout(false);
            this.tableLayoutPanel20.ResumeLayout(false);
            this.gxtCellpurge_MoveA.ResumeLayout(false);
            this.tableLayoutPanel19.ResumeLayout(false);
            this.gxtCellpurge_B.ResumeLayout(false);
            this.tableLayoutPanel18.ResumeLayout(false);
            this.gxtCellpurge_A.ResumeLayout(false);
            this.tableLayoutPanel17.ResumeLayout(false);
            this.gxtCellpurge_Ionizer.ResumeLayout(false);
            this.tp_CellLoad.ResumeLayout(false);
            this.tp_CellLoad.PerformLayout();
            this.gxtCellLoad_Move.ResumeLayout(false);
            this.gxtCellLoad_MoveLorUn.ResumeLayout(false);
            this.tableLayoutPanel28.ResumeLayout(false);
            this.gxtCellLoad_MoveB.ResumeLayout(false);
            this.tableLayoutPanel31.ResumeLayout(false);
            this.tableLayoutPanel32.ResumeLayout(false);
            this.tableLayoutPanel33.ResumeLayout(false);
            this.tableLayoutPanel34.ResumeLayout(false);
            this.gxtCellLoad_MoveA.ResumeLayout(false);
            this.tableLayoutPanel26.ResumeLayout(false);
            this.tableLayoutPanel30.ResumeLayout(false);
            this.tableLayoutPanel29.ResumeLayout(false);
            this.tableLayoutPanel27.ResumeLayout(false);
            this.gxtCellLoad_B.ResumeLayout(false);
            this.tableLayoutPanel25.ResumeLayout(false);
            this.gxtCellLoad_A.ResumeLayout(false);
            this.tableLayoutPanel24.ResumeLayout(false);
            this.tp_IRCutProcess.ResumeLayout(false);
            this.tp_IRCutProcess.PerformLayout();
            this.gxtIRCutProcess_Ionizer.ResumeLayout(false);
            this.gxtIRCutProcess_Move.ResumeLayout(false);
            this.gxtIRCutProcess_RightOffset.ResumeLayout(false);
            this.tableLayoutPanel40.ResumeLayout(false);
            this.gxtIRCutProcess_LeftOffset.ResumeLayout(false);
            this.tableLayoutPanel39.ResumeLayout(false);
            this.gxtIRCutProcess_MoveB.ResumeLayout(false);
            this.tableLayoutPanel38.ResumeLayout(false);
            this.gxtIRCutProcess_MoveA.ResumeLayout(false);
            this.tableLayoutPanel37.ResumeLayout(false);
            this.gxtIRCutProcess_B.ResumeLayout(false);
            this.tableLayoutPanel36.ResumeLayout(false);
            this.gxtIRCutProcess_A.ResumeLayout(false);
            this.tableLayoutPanel35.ResumeLayout(false);
            this.tp_BreakTransfer.ResumeLayout(false);
            this.tp_BreakTransfer.PerformLayout();
            this.gxtBreakTransfer_Move.ResumeLayout(false);
            this.gxtBreakTransfer_MoveB.ResumeLayout(false);
            this.tableLayoutPanel44.ResumeLayout(false);
            this.gxtBreakTransfer_MoveA.ResumeLayout(false);
            this.tableLayoutPanel43.ResumeLayout(false);
            this.gxtBreakTransfer_B.ResumeLayout(false);
            this.tableLayoutPanel42.ResumeLayout(false);
            this.gxtBreakTransfer_A.ResumeLayout(false);
            this.tableLayoutPanel41.ResumeLayout(false);
            this.tp_BreakUnitXZ.ResumeLayout(false);
            this.tp_BreakUnitXZ.PerformLayout();
            this.gxtBreakUnitXZ_Move.ResumeLayout(false);
            this.tableLayoutPanel45.ResumeLayout(false);
            this.gxtBreakUnitXZ_MoveZ4.ResumeLayout(false);
            this.tableLayoutPanel49.ResumeLayout(false);
            this.gxtBreakUnitXZ_MoveZ3.ResumeLayout(false);
            this.tableLayoutPanel48.ResumeLayout(false);
            this.gxtBreakUnitXZ_MoveZ2.ResumeLayout(false);
            this.tableLayoutPanel47.ResumeLayout(false);
            this.gxtBreakUnitXZ_MoveZ1.ResumeLayout(false);
            this.tableLayoutPanel46.ResumeLayout(false);
            this.tp_BreakUnitTY.ResumeLayout(false);
            this.tp_BreakUnitTY.PerformLayout();
            this.gxtBreakUnitTY_Shutter.ResumeLayout(false);
            this.tableLayoutPanel53.ResumeLayout(false);
            this.gxtBreakUnitTY_DummyBox.ResumeLayout(false);
            this.tableLayoutPanel52.ResumeLayout(false);
            this.gxtBreakUnitTY_Ionizer.ResumeLayout(false);
            this.gxtBreakUnitTY_Move.ResumeLayout(false);
            this.tableLayoutPanel54.ResumeLayout(false);
            this.gxtBreakUnitTY_Theta.ResumeLayout(false);
            this.tableLayoutPanel57.ResumeLayout(false);
            this.gxtBreakUnitTY_MoveB.ResumeLayout(false);
            this.tableLayoutPanel56.ResumeLayout(false);
            this.gxtBreakUnitTY_MoveA.ResumeLayout(false);
            this.tableLayoutPanel55.ResumeLayout(false);
            this.gxtBreakUnitTY_B.ResumeLayout(false);
            this.tableLayoutPanel50.ResumeLayout(false);
            this.gxtBreakUnitTY_A.ResumeLayout(false);
            this.tableLayoutPanel51.ResumeLayout(false);
            this.tp_UnloaderTransfer.ResumeLayout(false);
            this.tp_UnloaderTransfer.PerformLayout();
            this.gxtUnloaderTransfer_Move.ResumeLayout(false);
            this.gxtUnloaderTransfer_MoveB.ResumeLayout(false);
            this.tableLayoutPanel61.ResumeLayout(false);
            this.tableLayoutPanel62.ResumeLayout(false);
            this.tableLayoutPanel63.ResumeLayout(false);
            this.gxtUnloaderTransfer_MoveA.ResumeLayout(false);
            this.tableLayoutPanel58.ResumeLayout(false);
            this.tableLayoutPanel60.ResumeLayout(false);
            this.tableLayoutPanel59.ResumeLayout(false);
            this.gxtUnloaderTransfer_B.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.gxtUnloaderTransfer_A.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tp_CameraUnit.ResumeLayout(false);
            this.tp_CameraUnit.PerformLayout();
            this.gxtCameraUnit_Move.ResumeLayout(false);
            this.gxtCameraUnit_MoveB.ResumeLayout(false);
            this.tableLayoutPanel65.ResumeLayout(false);
            this.gxtCameraUnit_MoveA.ResumeLayout(false);
            this.tableLayoutPanel64.ResumeLayout(false);
            this.tp_CellInput.ResumeLayout(false);
            this.tp_CellInput.PerformLayout();
            this.gxtCellInput_Buffer.ResumeLayout(false);
            this.tableLayoutPanel66.ResumeLayout(false);
            this.gxtCellInput_Ionizer.ResumeLayout(false);
            this.gxtCellInput_Move.ResumeLayout(false);
            this.gxtCellInput_MoveCenter.ResumeLayout(false);
            this.tableLayoutPanel69.ResumeLayout(false);
            this.gxtCellInput_MoveB.ResumeLayout(false);
            this.tableLayoutPanel68.ResumeLayout(false);
            this.gxtCellInput_MoveA.ResumeLayout(false);
            this.tableLayoutPanel67.ResumeLayout(false);
            this.gxtCellInput_B.ResumeLayout(false);
            this.tableLayoutPanel23.ResumeLayout(false);
            this.gxtCellInput_A.ResumeLayout(false);
            this.tableLayoutPanel22.ResumeLayout(false);
            this.tp_CasseteUnload.ResumeLayout(false);
            this.tp_CasseteUnload.PerformLayout();
            this.gxtCasseteUnload_Move.ResumeLayout(false);
            this.gxtCasseteUnload_MoveB.ResumeLayout(false);
            this.tableLayoutPanel75.ResumeLayout(false);
            this.tableLayoutPanel76.ResumeLayout(false);
            this.tableLayoutPanel77.ResumeLayout(false);
            this.tableLayoutPanel78.ResumeLayout(false);
            this.tableLayoutPanel79.ResumeLayout(false);
            this.gxtCasseteUnload_MoveA.ResumeLayout(false);
            this.tableLayoutPanel70.ResumeLayout(false);
            this.tableLayoutPanel71.ResumeLayout(false);
            this.tableLayoutPanel72.ResumeLayout(false);
            this.tableLayoutPanel73.ResumeLayout(false);
            this.tableLayoutPanel74.ResumeLayout(false);
            this.gxtCasseteUnload_B.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.gxtCasseteUnload_A.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.gxtCasseteUnload.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tc_iostatus_info;
        private System.Windows.Forms.TabPage tp_CasseteLoad;
        private System.Windows.Forms.TabPage tp_Cellpurge;
        private System.Windows.Forms.TabPage tp_CellLoad;
        private System.Windows.Forms.TabPage tp_IRCutProcess;
        private System.Windows.Forms.TabPage tp_BreakTransfer;
        private System.Windows.Forms.TabPage tp_BreakUnitXZ;
        private System.Windows.Forms.TabPage tp_BreakUnitTY;
        private System.Windows.Forms.TabPage tp_UnloaderTransfer;
        private System.Windows.Forms.TabPage tp_CameraUnit;
        private System.Windows.Forms.TabPage tp_CellInput;
        private System.Windows.Forms.TabPage tp_CasseteUnload;
        private System.Windows.Forms.GroupBox gxtCasseteLoad_Move;
        private System.Windows.Forms.GroupBox gxtCasseteLoad_MoveB;
        private System.Windows.Forms.GroupBox gxtCasseteLoad_MoveA;
        private System.Windows.Forms.GroupBox gxtCasseteLoad_B;
        private System.Windows.Forms.GroupBox gxtCasseteLoad_A;
        private System.Windows.Forms.Button btnCasseteLoadSave;
        private System.Windows.Forms.GroupBox gxtCasseteLoad_Muting;
        private System.Windows.Forms.Button btnCasseteLoadMuting4;
        private System.Windows.Forms.Button btnCasseteLoadMuting2;
        private System.Windows.Forms.Button btnCasseteLoadMuting3;
        private System.Windows.Forms.Button btnCasseteLoadMuting1;
        private System.Windows.Forms.Button btnCasseteLoadMutingOff;
        private System.Windows.Forms.Button btnCasseteLoadMutingIn;
        private System.Windows.Forms.Button btnCasseteLoadSpeedSetting;
        private System.Windows.Forms.Button btnCasseteLoadAllSetting;
        private System.Windows.Forms.Button btnCasseteLoadLocationSetting;
        private System.Windows.Forms.Button btnCasseteLoadMoveLocation;
        private System.Windows.Forms.TextBox txtCasseteLoadLocation;
        private System.Windows.Forms.TextBox txtCasseteLoadSpeed;
        private System.Windows.Forms.TextBox txtCasseteLoadCurrentLocation;
        private System.Windows.Forms.TextBox txtCasseteLoadSelectedShaft;
        private System.Windows.Forms.Label lbl_CasseteLoad_Location;
        private System.Windows.Forms.Label lbl_CasseteLoad_Speed;
        private System.Windows.Forms.Label lbl_CasseteLoad_SelectedShaft;
        private System.Windows.Forms.TextBox txtCasseteLoad_LocationSetting;
        private System.Windows.Forms.Label lbl_CasseteLoad_CurrentLocation;
        private System.Windows.Forms.Button btnCasseteLoadGrabSwitch3;
        private System.Windows.Forms.Button btnCasseteLoadGrabSwitch2;
        private System.Windows.Forms.Button btnCasseteLoadGrabSwitch1;
        private System.Windows.Forms.ListView lvCasseteLoad;
        private System.Windows.Forms.ColumnHeader col_CasseteLoad_NamebyLocation;
        private System.Windows.Forms.ColumnHeader col_CasseteLoad_Shaft;
        private System.Windows.Forms.ColumnHeader col_CasseteLoad_Location;
        private System.Windows.Forms.ColumnHeader col_CasseteLoad_Speed;
        private UcrlAjinServoCtrl ajin_Setting_CasseteLoad;
        private System.Windows.Forms.GroupBox gxtCellpurge_Move;
        private System.Windows.Forms.GroupBox gxtCellpurge_MoveA;
        private System.Windows.Forms.GroupBox gxtCellpurge_B;
        private System.Windows.Forms.GroupBox gxtCellpurge_A;
        private System.Windows.Forms.Button btnCellpurgeSave;
        private System.Windows.Forms.GroupBox gxtCellpurge_Ionizer;
        private System.Windows.Forms.Button btnCellpurgeDestructionOff;
        private System.Windows.Forms.Button btnCellpurgeIonizerOff;
        private System.Windows.Forms.Button btnCellpurgeDestructionOn;
        private System.Windows.Forms.Button btnCellpurgeIonizerOn;
        private System.Windows.Forms.Button btnCellpurgeSpeedSetting;
        private System.Windows.Forms.Button btnCellpurgeAllSetting;
        private System.Windows.Forms.Button btnCellpurgeLocationSetting;
        private System.Windows.Forms.Button btnCellpurgeMoveLocation;
        private System.Windows.Forms.TextBox txtCellpurgeLocation;
        private System.Windows.Forms.TextBox txtCellpurgeSpeed;
        private System.Windows.Forms.TextBox txtCellpurgeCurrentLocation;
        private System.Windows.Forms.TextBox txtCellpurgeSelectedShaft;
        private System.Windows.Forms.Label lbl_Cellpurge_Location;
        private System.Windows.Forms.Label lbl_Cellpurge_Speed;
        private System.Windows.Forms.Label lbl_Cellpurge_SelectedShaft;
        private System.Windows.Forms.TextBox txtCellpurge_LocationSetting;
        private System.Windows.Forms.Label lbl_Cellpurge_CurrentLocation;
        private System.Windows.Forms.Button btnCellpurgeGrabSwitch3;
        private System.Windows.Forms.Button btnCellpurgeGrabSwitch2;
        private System.Windows.Forms.Button btnCellpurgeGrabSwitch1;
        private System.Windows.Forms.ListView lvCellpurge;
        private System.Windows.Forms.ColumnHeader col_Cellpurge_NamebyLocation;
        private System.Windows.Forms.ColumnHeader col_Cellpurge_Shaft;
        private System.Windows.Forms.ColumnHeader col_Cellpurge_Location;
        private System.Windows.Forms.ColumnHeader col_Cellpurge_Speed;
        private UcrlAjinServoCtrl ajin_Setting_Cellpurge;
        private System.Windows.Forms.GroupBox gxtCellLoad_Move;
        private System.Windows.Forms.GroupBox gxtCellLoad_B;
        private System.Windows.Forms.GroupBox gxtCellLoad_A;
        private System.Windows.Forms.Button btnCellLoadSave;
        private System.Windows.Forms.Button btnCellLoadSpeedSetting;
        private System.Windows.Forms.Button btnCellLoadAllSetting;
        private System.Windows.Forms.Button btnCellLoadLocationSetting;
        private System.Windows.Forms.Button btnCellLoadMoveLocation;
        private System.Windows.Forms.TextBox txtCellLoadLocation;
        private System.Windows.Forms.TextBox txtCellLoadSpeed;
        private System.Windows.Forms.TextBox txtCellLoadCurrentLocation;
        private System.Windows.Forms.TextBox txtCellLoadSelectedShaft;
        private System.Windows.Forms.Label lbl_CellLoad_Location;
        private System.Windows.Forms.Label lbl_CellLoad_Speed;
        private System.Windows.Forms.Label lbl_CellLoad_SelectedShaft;
        private System.Windows.Forms.TextBox txtCellLoad_LocationSetting;
        private System.Windows.Forms.Label lbl_CellLoad_CurrentLocation;
        private System.Windows.Forms.Button btnCellLoadGrabSwitch3;
        private System.Windows.Forms.Button btnCellLoadGrabSwitch2;
        private System.Windows.Forms.Button btnCellLoadGrabSwitch1;
        private System.Windows.Forms.ListView lvCellLoad;
        private System.Windows.Forms.ColumnHeader col_CellLoad_NamebyLocation;
        private System.Windows.Forms.ColumnHeader col_CellLoad_Shaft;
        private System.Windows.Forms.ColumnHeader col_CellLoad_Location;
        private System.Windows.Forms.ColumnHeader col_CellLoad_Speed;
        private UcrlAjinServoCtrl ajin_Setting_CellLoad;
        private System.Windows.Forms.GroupBox gxtIRCutProcess_Ionizer;
        private System.Windows.Forms.Button btnIRCutProcessDestruction;
        private System.Windows.Forms.Button btnIRCutProcessIonizerOff;
        private System.Windows.Forms.Button btnIRCutProcessDestructionOn;
        private System.Windows.Forms.Button btnIRCutProcessIonizerOn;
        private System.Windows.Forms.GroupBox gxtIRCutProcess_Move;
        private System.Windows.Forms.GroupBox gxtIRCutProcess_MoveB;
        private System.Windows.Forms.GroupBox gxtIRCutProcess_MoveA;
        private System.Windows.Forms.GroupBox gxtIRCutProcess_B;
        private System.Windows.Forms.GroupBox gxtIRCutProcess_A;
        private System.Windows.Forms.Button btnIRCutProcessSave;
        private System.Windows.Forms.Button btnIRCutProcessSpeedSetting;
        private System.Windows.Forms.Button btnIRCutProcessAllSetting;
        private System.Windows.Forms.Button btnIRCutProcessLocationSetting;
        private System.Windows.Forms.Button btnIRCutProcessMoveLocation;
        private System.Windows.Forms.TextBox txtIRCutProcessLocation;
        private System.Windows.Forms.TextBox txtIRCutProcessSpeed;
        private System.Windows.Forms.TextBox txtIRCutProcessCurrentLocation;
        private System.Windows.Forms.TextBox txtIRCutProcessSelectedShaft;
        private System.Windows.Forms.Label lbl_IRCutProcess_Location;
        private System.Windows.Forms.Label lbl_IRCutProcess_Speed;
        private System.Windows.Forms.Label lbl_IRCutProcess_SelectedShaft;
        private System.Windows.Forms.TextBox txtIRCutProcess_LocationSetting;
        private System.Windows.Forms.Label lbl_IRCutProcess_CurrentLocation;
        private System.Windows.Forms.Button btnIRCutProcessGrabSwitch3;
        private System.Windows.Forms.Button btnIRCutProcessGrabSwitch2;
        private System.Windows.Forms.Button btnIRCutProcessGrabSwitch1;
        private System.Windows.Forms.ListView lvIRCutProcess;
        private System.Windows.Forms.ColumnHeader col_IRCutProcess_NamebyLocation;
        private System.Windows.Forms.ColumnHeader col_IRCutProcess_Shaft;
        private System.Windows.Forms.ColumnHeader col_IRCutProcess_Location;
        private System.Windows.Forms.ColumnHeader col_IRCutProcess_Speed;
        private UcrlAjinServoCtrl ajin_Setting_IRCutProcess;
        private System.Windows.Forms.GroupBox gxtBreakTransfer_Move;
        private System.Windows.Forms.GroupBox gxtBreakTransfer_MoveB;
        private System.Windows.Forms.GroupBox gxtBreakTransfer_MoveA;
        private System.Windows.Forms.GroupBox gxtBreakTransfer_B;
        private System.Windows.Forms.GroupBox gxtBreakTransfer_A;
        private System.Windows.Forms.Button btnBreakTransferSave;
        private System.Windows.Forms.Button btnBreakTransferSpeedSetting;
        private System.Windows.Forms.Button btnBreakTransferAllSetting;
        private System.Windows.Forms.Button btnBreakTransferLocationSetting;
        private System.Windows.Forms.Button btnBreakTransferMoveLocation;
        private System.Windows.Forms.TextBox txtBreakTransferLocation;
        private System.Windows.Forms.TextBox txtBreakTransferSpeed;
        private System.Windows.Forms.TextBox txtBreakTransferCurrentLocation;
        private System.Windows.Forms.TextBox txtBreakTransferSelectedShaft;
        private System.Windows.Forms.Label lbl_BreakTransfer_Location;
        private System.Windows.Forms.Label lbl_BreakTransfer_Speed;
        private System.Windows.Forms.Label lbl_BreakTransfer_SelectedShaft;
        private System.Windows.Forms.TextBox txtBreakTransfer_LocationSetting;
        private System.Windows.Forms.Label lbl_BreakTransfer_CurrentLocation;
        private System.Windows.Forms.Button btnBreakTransferGrabSwitch3;
        private System.Windows.Forms.Button btnBreakTransferGrabSwitch2;
        private System.Windows.Forms.Button btnBreakTransferGrabSwitch1;
        private System.Windows.Forms.ListView lvBreakTransfer;
        private System.Windows.Forms.ColumnHeader col_BreakTransfer_NamebyLocation;
        private System.Windows.Forms.ColumnHeader col_BreakTransfer_Shaft;
        private System.Windows.Forms.ColumnHeader col_BreakTransfer_Location;
        private System.Windows.Forms.ColumnHeader col_BreakTransfer_Speed;
        private UcrlAjinServoCtrl ajin_Setting_BreakTransfer;
        private System.Windows.Forms.GroupBox gxtBreakUnitXZ_Move;
        private System.Windows.Forms.Button gxtBreakUnitXZSave;
        private System.Windows.Forms.Button btnBreakUnitXZSpeedSetting;
        private System.Windows.Forms.Button btnBreakUnitXZAllSetting;
        private System.Windows.Forms.Button btnBreakUnitXZLocationSetting;
        private System.Windows.Forms.Button btnBreakUnitXZMoveLocation;
        private System.Windows.Forms.TextBox txtBreakUnitXZLocation;
        private System.Windows.Forms.TextBox txtBreakUnitXZSpeed;
        private System.Windows.Forms.TextBox txtBreakUnitXZCurrentLocation;
        private System.Windows.Forms.TextBox txtBreakUnitXZSelectedShaft;
        private System.Windows.Forms.Label lbl_BreakUnitXZ_Location;
        private System.Windows.Forms.Label lbl_BreakUnitXZ_Speed;
        private System.Windows.Forms.Label lbl_BreakUnitXZ_SelectedShaft;
        private System.Windows.Forms.TextBox txtBreakUnitXZ_LocationSetting;
        private System.Windows.Forms.Label lbl_BreakUnitXZ_CurrentLocation;
        private System.Windows.Forms.Button btnBreakUnitXZGrabSwitch3;
        private System.Windows.Forms.Button btnBreakUnitXZGrabSwitch2;
        private System.Windows.Forms.Button btnBreakUnitXZGrabSwitch1;
        private System.Windows.Forms.ListView lvBreakUnitXZ;
        private System.Windows.Forms.ColumnHeader col_BreakUnitXZ_NamebyLocation;
        private System.Windows.Forms.ColumnHeader col_BreakUnitXZ_Shaft;
        private System.Windows.Forms.ColumnHeader col_BreakUnitXZ_Location;
        private System.Windows.Forms.ColumnHeader col_BreakUnitXZ_Speed;
        private UcrlAjinServoCtrl ajin_Setting_BreakUnitXZ;
        private System.Windows.Forms.GroupBox gxtBreakUnitTY_Ionizer;
        private System.Windows.Forms.Button btnBreakUnitTYDestructionOff;
        private System.Windows.Forms.Button btnBreakUnitTYIonizerOff;
        private System.Windows.Forms.Button btnBreakUnitTYDestructionOn;
        private System.Windows.Forms.Button btnBreakUnitTYIonizerOn;
        private System.Windows.Forms.GroupBox gxtBreakUnitTY_Move;
        private System.Windows.Forms.Button btnBreakUnitTYSave;
        private System.Windows.Forms.Button btnBreakUnitTYSpeedSetting;
        private System.Windows.Forms.Button btnBreakUnitTYAllSetting;
        private System.Windows.Forms.Button btnBreakUnitTYLocationSetting;
        private System.Windows.Forms.Button btnBreakUnitTYMoveLocation;
        private System.Windows.Forms.TextBox txtBreakUnitTYLocation;
        private System.Windows.Forms.TextBox txtBreakUnitTYSpeed;
        private System.Windows.Forms.TextBox txtBreakUnitTYCurrentLocation;
        private System.Windows.Forms.TextBox txtBreakUnitTYSelectedShaft;
        private System.Windows.Forms.Label lbl_BreakUnitTY_Location;
        private System.Windows.Forms.Label lbl_BreakUnitTY_Speed;
        private System.Windows.Forms.Label lbl_BreakUnitTY_SelectedShaft;
        private System.Windows.Forms.TextBox txtBreakUnitTY_LocationSetting;
        private System.Windows.Forms.Label lbl_BreakUnitTY_CurrentLocation;
        private System.Windows.Forms.Button btnBreakUnitTYGrabSwitch3;
        private System.Windows.Forms.Button btnBreakUnitTYGrabSwitch2;
        private System.Windows.Forms.Button btnBreakUnitTYGrabSwitch1;
        private System.Windows.Forms.ListView lvBreakUnitTY;
        private System.Windows.Forms.ColumnHeader col_BreakUnitTY_NamebyLocation;
        private System.Windows.Forms.ColumnHeader col_BreakUnitTY_Shaft;
        private System.Windows.Forms.ColumnHeader col_BreakUnitTY_Location;
        private System.Windows.Forms.ColumnHeader col_BreakUnitTY_Speed;
        private UcrlAjinServoCtrl ajin_Setting_BreakUnitTY;
        private System.Windows.Forms.GroupBox gxtUnloaderTransfer_Move;
        private System.Windows.Forms.GroupBox gxtUnloaderTransfer_MoveB;
        private System.Windows.Forms.GroupBox gxtUnloaderTransfer_MoveA;
        private System.Windows.Forms.GroupBox gxtUnloaderTransfer_B;
        private System.Windows.Forms.GroupBox gxtUnloaderTransfer_A;
        private System.Windows.Forms.Button btnUnloaderTransferSave;
        private System.Windows.Forms.Button btnUnloaderTransferSpeedSetting;
        private System.Windows.Forms.Button btnUnloaderTransferAllSetting;
        private System.Windows.Forms.Button btnUnloaderTransferLocationSetting;
        private System.Windows.Forms.Button btnUnloaderTransferMoveLocation;
        private System.Windows.Forms.TextBox txtUnloaderTransferLocation;
        private System.Windows.Forms.TextBox txtUnloaderTransferSpeed;
        private System.Windows.Forms.TextBox txtUnloaderTransferCurrentLocation;
        private System.Windows.Forms.TextBox txtUnloaderTransferSelectedShaft;
        private System.Windows.Forms.Label lbl_UnloaderTransfer_Location;
        private System.Windows.Forms.Label lbl_UnloaderTransfer_Speed;
        private System.Windows.Forms.Label lbl_UnloaderTransfer_SeletedShaft;
        private System.Windows.Forms.TextBox txtUnloaderTransfer_LocationSetting;
        private System.Windows.Forms.Label lbl_UnloaderTransfer_CurrentLocation;
        private System.Windows.Forms.Button btnUnloaderTransferGrabSwitch3;
        private System.Windows.Forms.Button btnUnloaderTransferGrabSwitch2;
        private System.Windows.Forms.Button btnUnloaderTransferGrabSwitch1;
        private System.Windows.Forms.ListView lvUnloaderTransfer;
        private System.Windows.Forms.ColumnHeader col_UnloaderTransfer_NamebyLocation;
        private System.Windows.Forms.ColumnHeader col_UnloaderTransfer_Shaft;
        private System.Windows.Forms.ColumnHeader col_UnloaderTransfer_Location;
        private System.Windows.Forms.ColumnHeader col_UnloaderTransfer_Speed;
        private UcrlAjinServoCtrl ajin_Setting_UnloaderTransfer;
        private System.Windows.Forms.GroupBox gxtCameraUnit_Move;
        private System.Windows.Forms.GroupBox gxtCameraUnit_MoveB;
        private System.Windows.Forms.GroupBox gxtCameraUnit_MoveA;
        private System.Windows.Forms.Button btnCameraUnitSave;
        private System.Windows.Forms.Button btnCameraUnitSpeedSetting;
        private System.Windows.Forms.Button btnCameraUnitAllSetting;
        private System.Windows.Forms.Button btnCameraUnitLocationSetting;
        private System.Windows.Forms.Button btnCameraUnitMoveLocation;
        private System.Windows.Forms.TextBox txtCameraUnitLocation;
        private System.Windows.Forms.TextBox txtCameraUnitSpeed;
        private System.Windows.Forms.TextBox txtCameraUnitCurrentLocation;
        private System.Windows.Forms.TextBox txtCameraUnitSelectedShaft;
        private System.Windows.Forms.Label lbl_CameraUnit_Location;
        private System.Windows.Forms.Label lbl_CameraUnit_Speed;
        private System.Windows.Forms.Label lbl_CameraUnit_SelectedShaft;
        private System.Windows.Forms.TextBox txtCameraUnit_LocationSetting;
        private System.Windows.Forms.Label lbl_CameraUnit_CurrentLocation;
        private System.Windows.Forms.Button btnCameraUnitGrabSwitch3;
        private System.Windows.Forms.Button btnCameraUnitGrabSwitch2;
        private System.Windows.Forms.Button btnCameraUnitGrabSwitch1;
        private System.Windows.Forms.ListView lvCameraUnit;
        private System.Windows.Forms.ColumnHeader col_CameraUnit_NamebyLocation;
        private System.Windows.Forms.ColumnHeader col_CameraUnit_Shaft;
        private System.Windows.Forms.ColumnHeader col_CameraUnit_Location;
        private System.Windows.Forms.ColumnHeader col_CameraUnit_Speed;
        private UcrlAjinServoCtrl ajin_Setting_CameraUnit;
        private System.Windows.Forms.GroupBox gxtCellInput_Ionizer;
        private System.Windows.Forms.Button btnCellInputDestructionOff;
        private System.Windows.Forms.Button btnCellInputIonizerOff;
        private System.Windows.Forms.Button btnCellInputDestructionOn;
        private System.Windows.Forms.Button btnCellInputIonizerOn;
        private System.Windows.Forms.GroupBox gxtCellInput_Move;
        private System.Windows.Forms.GroupBox gxtCellInput_MoveB;
        private System.Windows.Forms.GroupBox gxtCellInput_MoveA;
        private System.Windows.Forms.Button btnCellInputSave;
        private System.Windows.Forms.Button btnCellInputSpeedSetting;
        private System.Windows.Forms.Button btnCellInputAllSetting;
        private System.Windows.Forms.Button btnCellInputLocationSetting;
        private System.Windows.Forms.Button btnCellInputMoveLocation;
        private System.Windows.Forms.TextBox txtCellInputLocation;
        private System.Windows.Forms.TextBox txtCellInputSpeed;
        private System.Windows.Forms.TextBox txtCellInputCurrentLocation;
        private System.Windows.Forms.TextBox txtCellInputSelectedShaft;
        private System.Windows.Forms.Label lbl_CellInput_Location;
        private System.Windows.Forms.Label lbl_CellInput_Speed;
        private System.Windows.Forms.Label lbl_CellInput_SelectedShaft;
        private System.Windows.Forms.TextBox txtCellInput_LocationSetting;
        private System.Windows.Forms.Label lbl_CellInput_CurrentLocation;
        private System.Windows.Forms.Button btnCellInputGrabSwitch3;
        private System.Windows.Forms.Button btnCellInputGrabSwitch2;
        private System.Windows.Forms.Button btnCellInputGrabSwitch1;
        private System.Windows.Forms.ListView lvCellInput;
        private System.Windows.Forms.ColumnHeader col_CellInput_NamebyLocation;
        private System.Windows.Forms.ColumnHeader col_CellInput_Shaft;
        private System.Windows.Forms.ColumnHeader col_CellInput_Location;
        private System.Windows.Forms.ColumnHeader col_CellInput_Speed;
        private UcrlAjinServoCtrl ajin_Setting_CellInput;
        private System.Windows.Forms.GroupBox gxtCasseteUnload_Move;
        private System.Windows.Forms.GroupBox gxtCasseteUnload_MoveB;
        private System.Windows.Forms.GroupBox gxtCasseteUnload_MoveA;
        private System.Windows.Forms.GroupBox gxtCasseteUnload_B;
        private System.Windows.Forms.GroupBox gxtCasseteUnload_A;
        private System.Windows.Forms.Button btnCasseteUnloadSave;
        private System.Windows.Forms.GroupBox gxtCasseteUnload;
        private System.Windows.Forms.Button btnCasseteUnloadMuting4;
        private System.Windows.Forms.Button btnCasseteUnloadMuting2;
        private System.Windows.Forms.Button btnCasseteUnloadMuting3;
        private System.Windows.Forms.Button btnCasseteUnloadMuting1;
        private System.Windows.Forms.Button btnCasseteUnloadMutingOut;
        private System.Windows.Forms.Button btncasseteUnloadMutingIn;
        private System.Windows.Forms.Button btnCasseteUnloadSpeedSetting;
        private System.Windows.Forms.Button btnCasseteUnloadAllSetting;
        private System.Windows.Forms.Button btnCasseteUnloadLocationSetting;
        private System.Windows.Forms.Button btnCasseteUnloadMoveLocation;
        private System.Windows.Forms.TextBox txtCasseteUnloadLocation;
        private System.Windows.Forms.TextBox txtCasseteUnloadSpeed;
        private System.Windows.Forms.TextBox txtCasseteUnloadCurrentLocation;
        private System.Windows.Forms.TextBox txtCasseteUnload_SelectedShaft;
        private System.Windows.Forms.Label lbl_CasseteUnload_Location;
        private System.Windows.Forms.Label lbl_CasseteUnload_Speed;
        private System.Windows.Forms.Label lbl_CasseteUnload_SelectedShaft;
        private System.Windows.Forms.TextBox txtCasseteUnload_LocationSetting;
        private System.Windows.Forms.Label lbl_CasseteUnload_CurrentLocation;
        private System.Windows.Forms.Button btncasseteUnloadGrabSwitch3;
        private System.Windows.Forms.Button btnCasseteUnloadGrabSwitch2;
        private System.Windows.Forms.Button btnCasseteUnloadGrabSwitch1;
        private System.Windows.Forms.ListView lvCasseteUnload;
        private System.Windows.Forms.ColumnHeader col_CasseteUnload_NamebyLocation;
        private System.Windows.Forms.ColumnHeader col_CasseteUnload_Shaft;
        private System.Windows.Forms.ColumnHeader col_CasseteUnload_Location;
        private System.Windows.Forms.ColumnHeader col_CasseteUnload_Speed;
        private UcrlAjinServoCtrl ajin_Setting_CasseteUnload;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btnCasseteLoadABCSD;
        private System.Windows.Forms.Button btnCasseteLoadABCSU;
        private System.Windows.Forms.Button btnCasseteLoadABCUG;
        private System.Windows.Forms.Button btnCasseteLoadABCG;
        private System.Windows.Forms.Button btnCasseteLoadALTSD;
        private System.Windows.Forms.Button btnCasseteLoadALTSU;
        private System.Windows.Forms.Button btnCasseteLoadALUG;
        private System.Windows.Forms.Button btnCasseteLoadALG;
        private System.Windows.Forms.Button btnCasseteLoadATCSD;
        private System.Windows.Forms.Button btnCasseteLoadATCSU;
        private System.Windows.Forms.Button btnCasseteLoadATCUG;
        private System.Windows.Forms.Button btnCasseteLoadATCG;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button btnCasseteLoadBBCSD;
        private System.Windows.Forms.Button btnCasseteLoadBBCSU;
        private System.Windows.Forms.Button btnCasseteLoadBBCUG;
        private System.Windows.Forms.Button btnCasseteLoadBBCG;
        private System.Windows.Forms.Button btnCasseteLoadBLTSD;
        private System.Windows.Forms.Button btnCasseteLoadBLTSU;
        private System.Windows.Forms.Button btnCasseteLoadBLUG;
        private System.Windows.Forms.Button btnCasseteLoadBLG;
        private System.Windows.Forms.Button btnCasseteLoadBTCSD;
        private System.Windows.Forms.Button btnCasseteLoadBTCSU;
        private System.Windows.Forms.Button btnCasseteLoadBTCUG;
        private System.Windows.Forms.Button btnCasseteLoadBTCG;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Button btnUnloaderTransferBPickerDestruction2;
        private System.Windows.Forms.Button btnUnloaderTransferBPickerDestructionOn2;
        private System.Windows.Forms.Button btnUnloaderTransferBPickerDestructionOff1;
        private System.Windows.Forms.Button btnUnloaderTransferBPickerDestructionOn1;
        private System.Windows.Forms.Button btnUnloaderTransferBPickerPneumaticOff2;
        private System.Windows.Forms.Button btnUnloaderTransferBPickerPneumaticOn2;
        private System.Windows.Forms.Button btnUnloaderTransferBPickerPneumaticOff1;
        private System.Windows.Forms.Button btnUnloaderTransferBPickerPneumaticOn1;
        private System.Windows.Forms.Button btnUnloaderTransferBPickerDown2;
        private System.Windows.Forms.Button btnUnloaderTransferBPickerUp2;
        private System.Windows.Forms.Button btnUnloaderTransferBPickerDown1;
        private System.Windows.Forms.Button btnUnloaderTransferBPickerUp1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button btnUnloaderTransferAPickerDestructionOff2;
        private System.Windows.Forms.Button btnUnloaderTransferAPickerDestructionOn2;
        private System.Windows.Forms.Button btnUnloaderTransferAPickerDestructionOff1;
        private System.Windows.Forms.Button btnUnloaderTransferAPickerDestructionOn1;
        private System.Windows.Forms.Button btnUnloaderTransferAPickerPneumaticOff2;
        private System.Windows.Forms.Button btnUnloaderTransferAPickerPneumaticOn2;
        private System.Windows.Forms.Button btnUnloaderTransferAPickerPneumaticOff1;
        private System.Windows.Forms.Button btnUnloaderTransferAPickerPneumaticOn1;
        private System.Windows.Forms.Button btnUnloaderTransferAPickerDown2;
        private System.Windows.Forms.Button btnUnloaderTransferAPickerUp2;
        private System.Windows.Forms.Button btnUnloaderTransferAPickerDown1;
        private System.Windows.Forms.Button btnUnloaderTransferAPickerUp1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Button btnCasseteUnloadBBottomCstStopperDown;
        private System.Windows.Forms.Button btnCasseteUnloadBBottomCstStopperUp;
        private System.Windows.Forms.Button btnCasseteUnloadBBottomCstUngrib;
        private System.Windows.Forms.Button btnCasseteUnloadBBottomCstGrib;
        private System.Windows.Forms.Button btnCasseteUnloadBLiftTiltSlinderDown;
        private System.Windows.Forms.Button btnCasseteUnloadBLiftTiltSlinderUp;
        private System.Windows.Forms.Button btnCasseteUnloadBLiftUngrib;
        private System.Windows.Forms.Button btnCasseteUnloadBLiftGrib;
        private System.Windows.Forms.Button btnCasseteUnloadBTopCstStopperDown;
        private System.Windows.Forms.Button btnCasseteUnloadBTopCstStopperUp;
        private System.Windows.Forms.Button btnCasseteUnloadBTopCstUngrib;
        private System.Windows.Forms.Button btnCasseteUnloadBTopCstGrib;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Button btnCasseteUnloadABottomCstStopperDown;
        private System.Windows.Forms.Button btnCasseteUnloadABottomCstStopperUp;
        private System.Windows.Forms.Button btnCasseteUnloadABottomCstUngrib;
        private System.Windows.Forms.Button btnCasseteUnloadABottomCstGrib;
        private System.Windows.Forms.Button btnCasseteUnloadALiftTiltSlinderDown;
        private System.Windows.Forms.Button btnCasseteUnloadALiftTiltSlinderUp;
        private System.Windows.Forms.Button btnCasseteUnloadALiftUngrib;
        private System.Windows.Forms.Button btnCasseteUnloadALiftGrib;
        private System.Windows.Forms.Button btnCasseteUnloadATopCstStopperDown;
        private System.Windows.Forms.Button btnCasseteUnloadATopCstStopperUp;
        private System.Windows.Forms.Button btnCasseteUnloadATopCstUngrib;
        private System.Windows.Forms.Button btnCasseteUnloadATopCstGrib;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.Button btnCasseteLoadMoveBZ2CellOut;
        private System.Windows.Forms.Button btnCasseteLoadMoveBZ2Tilt;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.Button btnCasseteLoadMoveBZ2Out;
        private System.Windows.Forms.Button btnCasseteLoadMoveBZ2InWait;
        private System.Windows.Forms.Button btnCasseteLoadMoveBZ2In;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private System.Windows.Forms.Button btnCasseteLoadMoveBT4Out;
        private System.Windows.Forms.Button btnCasseteLoadMoveBT3Move;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        private System.Windows.Forms.Button btnCasseteLoadMoveBT4Move;
        private System.Windows.Forms.Button btnCasseteLoadMoveBT3In;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.Button btnCasseteLoadMoveAZ1CellOut;
        private System.Windows.Forms.Button btnCasseteLoadMoveAZ1Tilt;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.Button btnCasseteLoadMoveAZ1Out;
        private System.Windows.Forms.Button btnCasseteLoadMoveAZ1InWait;
        private System.Windows.Forms.Button btnCasseteLoadMoveAZ1In;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Button btnCasseteLoadMoveAT2Out;
        private System.Windows.Forms.Button btnCasseteLoadMoveAT1Move;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Button btnCasseteLoadMoveAT2Move;
        private System.Windows.Forms.Button btnCasseteLoadMoveAT1In;
        private System.Windows.Forms.GroupBox gxtCellpurge_MoveCenter;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel21;
        private System.Windows.Forms.Button btnCellpurgeMoveCenterY2C;
        private System.Windows.Forms.Button btnCellpurgeMoveCenterBBuffer;
        private System.Windows.Forms.Button btnCellpurgeMoveCenterY2Uld;
        private System.Windows.Forms.Button btnCellpurgeMoveCenterY1C;
        private System.Windows.Forms.Button btnCellpurgeMoveCenterABuffer;
        private System.Windows.Forms.Button btnCellpurgeMoveCenterY1Uld;
        private System.Windows.Forms.GroupBox gxtCellpurge_MoveB;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel20;
        private System.Windows.Forms.Button btnCellpurgeMoveBX2BC;
        private System.Windows.Forms.Button btnCellpurgeMoveBX2BW;
        private System.Windows.Forms.Button btnCellpurgeMoveBX1BW;
        private System.Windows.Forms.Button btnCellpurgeMoveBX1BC;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel19;
        private System.Windows.Forms.Button btnCellpurgeMoveAX2AC;
        private System.Windows.Forms.Button btnCellpurgeMoveAX2AW;
        private System.Windows.Forms.Button btnCellpurgeMoveAX1AW;
        private System.Windows.Forms.Button btnCellpurgeMoveAX1AC;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel18;
        private System.Windows.Forms.Button btnCellpurgeBDestructionOn;
        private System.Windows.Forms.Button btnCellpurgeBDestructionOff;
        private System.Windows.Forms.Button btnCellpurgeBPneumaticOff;
        private System.Windows.Forms.Button btnCellpurgeBPneumaticOn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel17;
        private System.Windows.Forms.Button btnCellpurgeADestructionOn;
        private System.Windows.Forms.Button btnCellpurgeADestructionOff;
        private System.Windows.Forms.Button btnCellpurgeAPneumaticOff;
        private System.Windows.Forms.Button btnCellpurgeAPneumaticOn;
        private System.Windows.Forms.GroupBox gxtCellLoad_MoveLorUn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel28;
        private System.Windows.Forms.Button btnCellLoadMoveLorUnPreAlignMark1;
        private System.Windows.Forms.Button btnCellLoadMoveLorUnPreAlignMark2;
        private System.Windows.Forms.Button btnCellLoadMoveLorUnY1UnL;
        private System.Windows.Forms.Button btnCellLoadMoveLorUnY1L;
        private System.Windows.Forms.Button btnCellLoadMoveLorUnX2L;
        private System.Windows.Forms.Button btnCellLoadMoveLorUnX1L;
        private System.Windows.Forms.GroupBox gxtCellLoad_MoveB;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel31;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel32;
        private System.Windows.Forms.Button btnCellLoadMoveBBPickerM90Angle;
        private System.Windows.Forms.Button btnCellLoadMoveBBPickerP90Angle;
        private System.Windows.Forms.Button btnCellLoadMoveBBPicker0Angle;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel33;
        private System.Windows.Forms.Button btnCellLoadMoveBBPickerUnL;
        private System.Windows.Forms.Button btnCellLoadMoveBBPickerL;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel34;
        private System.Windows.Forms.Button btnCellLoadMoveBX2BStageUnL;
        private System.Windows.Forms.Button btnCellLoadMoveBX1BStageUnL;
        private System.Windows.Forms.GroupBox gxtCellLoad_MoveA;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel26;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel30;
        private System.Windows.Forms.Button btnCellLoadMoveAAPickerM90Angle;
        private System.Windows.Forms.Button btnCellLoadMoveAAPickerP90Angle;
        private System.Windows.Forms.Button btnCellLoadMoveAAPicker0Angle;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel29;
        private System.Windows.Forms.Button btnCellLoadMoveAAPickerUnL;
        private System.Windows.Forms.Button btnCellLoadMoveAAPickerL;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel27;
        private System.Windows.Forms.Button btnCellLoadMoveAX2AStageUnL;
        private System.Windows.Forms.Button btnCellLoadMoveAX1AStageUnL;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel25;
        private System.Windows.Forms.Button btnCellLoadBPickerDestructionOn;
        private System.Windows.Forms.Button btnCellLoadBPickerDestructionOff;
        private System.Windows.Forms.Button btnCellLoadBPickerUp;
        private System.Windows.Forms.Button btnCellLoadBPickerDown;
        private System.Windows.Forms.Button btnCellLoadBPickerPneumaticOn;
        private System.Windows.Forms.Button btnCellLoadBPickerPneumaticOff;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel24;
        private System.Windows.Forms.Button btnCellLoadAPickerDestructionOn;
        private System.Windows.Forms.Button btnCellLoadAPickerDestructionOff;
        private System.Windows.Forms.Button btnCellLoadAPickerUp;
        private System.Windows.Forms.Button btnCellLoadAPickerDown;
        private System.Windows.Forms.Button btnCellLoadAPickerPneumaticOn;
        private System.Windows.Forms.Button btnCellLoadAPickerPneumaticOff;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel35;
        private System.Windows.Forms.Button btnIRCutProcessAL1PneumaticCh1On;
        private System.Windows.Forms.Button btnIRCutProcessAL1PneumaticCh1Off;
        private System.Windows.Forms.Button btnIRCutProcessAL2PneumaticCh1On;
        private System.Windows.Forms.Button btnIRCutProcessAL2PneumaticCh1Off;
        private System.Windows.Forms.Button btnIRCutProcessAL1PneumaticCh2On;
        private System.Windows.Forms.Button btnIRCutProcessAL1PneumaticCh2Off;
        private System.Windows.Forms.Button btnIRCutProcessAL2PneumaticCh2On;
        private System.Windows.Forms.Button btnIRCutProcessAL2PneumaticCh2Off;
        private System.Windows.Forms.GroupBox gxtCellInput_B;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel23;
        private System.Windows.Forms.Button btnCellInputBDestructionOn;
        private System.Windows.Forms.Button btnCellInputBDestructionOff;
        private System.Windows.Forms.Button btnCellInputBPneumaticOff;
        private System.Windows.Forms.Button btnCellInputBPneumaticOn;
        private System.Windows.Forms.GroupBox gxtCellInput_A;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel22;
        private System.Windows.Forms.Button btnCellInputADestructionOn;
        private System.Windows.Forms.Button btnCellInputADestructionOff;
        private System.Windows.Forms.Button btnCellInputAPneumaticOff;
        private System.Windows.Forms.Button btnCellInputAPneumaticOn;
        private System.Windows.Forms.GroupBox gxtIRCutProcess_RightOffset;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel40;
        private System.Windows.Forms.Button btnIRCutProcessRightOffsetRightVision2Focus;
        private System.Windows.Forms.Button btnIRCutProcessRightOffsetRightFocus2Vision;
        private System.Windows.Forms.Button btnIRCutProcessRightOffsetLeftVision2Focus;
        private System.Windows.Forms.Button btnIRCutProcessRightOffsetLeftFocus2Vision;
        private System.Windows.Forms.GroupBox gxtIRCutProcess_LeftOffset;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel39;
        private System.Windows.Forms.Button btnIRCutProcessLeftOffsetRightVisoin2Focus;
        private System.Windows.Forms.Button btnIRCutProcessLeftOffsetRightFocus2Visoin;
        private System.Windows.Forms.Button btnIRCutProcessLeftOffsetLeftVision2Focus;
        private System.Windows.Forms.Button btnIRCutProcessLeftOffsetLeftFocus2Vision;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel38;
        private System.Windows.Forms.Button btnIRCutProcessMoveBR1Camera;
        private System.Windows.Forms.Button btnIRCutProcessMoveBR2Camera;
        private System.Windows.Forms.Button btnIRCutProcessMoveBRightCellLoad;
        private System.Windows.Forms.Button btnIRCutProcessMoveBRightCellUnload;
        private System.Windows.Forms.Button btnIRCutProcessMoveBR1Laser;
        private System.Windows.Forms.Button btnIRCutProcessMoveBR2Laser;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel37;
        private System.Windows.Forms.Button btnIRCutProcessMoveAL1Camera;
        private System.Windows.Forms.Button btnIRCutProcessMoveAL2Camera;
        private System.Windows.Forms.Button btnIRCutProcessMoveALeftCellLoad;
        private System.Windows.Forms.Button btnIRCutProcessMoveALeftCellUnload;
        private System.Windows.Forms.Button btnIRCutProcessMoveAL1Laser;
        private System.Windows.Forms.Button btnIRCutProcessMoveAL2Laser;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel36;
        private System.Windows.Forms.Button btnIRCutProcessAR2DestructionCh2Off;
        private System.Windows.Forms.Button btnIRCutProcessAR2DestructionCh2On;
        private System.Windows.Forms.Button btnIRCutProcessAR1DestructionCh2Off;
        private System.Windows.Forms.Button btnIRCutProcessAR1DestructionCh2On;
        private System.Windows.Forms.Button btnIRCutProcessBR1PneumaticCh1Off;
        private System.Windows.Forms.Button btnIRCutProcessBR2PneumaticCh1On;
        private System.Windows.Forms.Button btnIRCutProcessBR2PneumaticCh1Off;
        private System.Windows.Forms.Button btnIRCutProcessBR1PneumaticCh2Off;
        private System.Windows.Forms.Button btnIRCutProcessBR2PneumaticCh2On;
        private System.Windows.Forms.Button btnIRCutProcessBR2PneumaticCh2Off;
        private System.Windows.Forms.Button btnIRCutProcessBR1PneumaticCh2On;
        private System.Windows.Forms.Button btnIRCutProcessBR1PneumaticCh1On;
        private System.Windows.Forms.Button btnIRCutProcessAR1DestructionCh1On;
        private System.Windows.Forms.Button btnIRCutProcessAR1DestructionCh1Off;
        private System.Windows.Forms.Button btnIRCutProcessAR2DestructionCh1On;
        private System.Windows.Forms.Button btnIRCutProcessAR2DestructionCh1Off;
        private System.Windows.Forms.Button btnIRCutProcessAL1DestructionCh2Off;
        private System.Windows.Forms.Button btnIRCutProcessAL1DestructionCh2On;
        private System.Windows.Forms.Button btnIRCutProcessAL2DestructionCh2Off;
        private System.Windows.Forms.Button btnIRCutProcessAL2DestructionCh2On;
        private System.Windows.Forms.Button btnIRCutProcessAL1DestructionCh1On;
        private System.Windows.Forms.Button btnIRCutProcessAL1DestructionCh1Off;
        private System.Windows.Forms.Button btnIRCutProcessAL2DestructionCh1On;
        private System.Windows.Forms.Button btnIRCutProcessAL2DestructionCh1Off;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel43;
        private System.Windows.Forms.Button btnBreakTransferMoveAUnload;
        private System.Windows.Forms.Button btnBreakTransferMoveALoad;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel42;
        private System.Windows.Forms.Button btnBreakTransferBPickerDestructionOn;
        private System.Windows.Forms.Button btnBreakTransferBPickerDestructionOff;
        private System.Windows.Forms.Button btnBreakTransferBPickerUp;
        private System.Windows.Forms.Button btnBreakTransferBPickerDown;
        private System.Windows.Forms.Button btnBreakTransferBPickerPneumaticOn;
        private System.Windows.Forms.Button btnBreakTransferBPneumaticOff;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel41;
        private System.Windows.Forms.Button btnBreakTransferAPickerDestructionOn;
        private System.Windows.Forms.Button btnBreakTransferAPickerDestructionOff;
        private System.Windows.Forms.Button btnBreakTransferAPickerUp;
        private System.Windows.Forms.Button btnBreakTransferAPickerDown;
        private System.Windows.Forms.Button btnBreakTransferAPickerPneumaticOn;
        private System.Windows.Forms.Button btnBreakTransferAPickerPneumaticOff;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel44;
        private System.Windows.Forms.Button btnBreakTransferMoveBUnload;
        private System.Windows.Forms.Button btnBreakTransferMoveBLoad;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel45;
        private System.Windows.Forms.GroupBox gxtBreakUnitXZ_MoveZ4;
        private System.Windows.Forms.GroupBox gxtBreakUnitXZ_MoveZ3;
        private System.Windows.Forms.GroupBox gxtBreakUnitXZ_MoveZ2;
        private System.Windows.Forms.GroupBox gxtBreakUnitXZ_MoveZ1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel46;
        private System.Windows.Forms.Button gxtBreakUnitXZMoveZ1SlowDescent;
        private System.Windows.Forms.Button gxtBreakUnitXZMoveZ1VisionCheck;
        private System.Windows.Forms.Button gxtBreakUnitXZMoveZ1FastDescent;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel49;
        private System.Windows.Forms.Button gxtBreakUnitXZMoveZ4SlowDescent;
        private System.Windows.Forms.Button gxtBreakUnitXZMoveZ4VisionCheck;
        private System.Windows.Forms.Button gxtBreakUnitXZMoveZ4FastDescent;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel48;
        private System.Windows.Forms.Button gxtBreakUnitXZMoveZ3SlowDescent;
        private System.Windows.Forms.Button gxtBreakUnitXZMoveZ3VisionCheck;
        private System.Windows.Forms.Button gxtBreakUnitXZMoveZ3FastDescent;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel47;
        private System.Windows.Forms.Button gxtBreakUnitXZMoveZ2SlowDescent;
        private System.Windows.Forms.Button gxtBreakUnitXZMoveZ2VisionCheck;
        private System.Windows.Forms.Button gxtBreakUnitXZMoveZ2FastDescent;
        private System.Windows.Forms.GroupBox gxtBreakUnitTY_Shutter;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel53;
        private System.Windows.Forms.Button btnBreakUnitTYShutterClose;
        private System.Windows.Forms.Button btnBreakUnitTYShutterOpen;
        private System.Windows.Forms.GroupBox gxtBreakUnitTY_DummyBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel52;
        private System.Windows.Forms.Button btnBreakUnitTYDummyxtOutput;
        private System.Windows.Forms.Button btnBreakUnitTYDummyxtInput;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel54;
        private System.Windows.Forms.GroupBox gxtBreakUnitTY_Theta;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel57;
        private System.Windows.Forms.Button btnBreakUnitTYThetaT2Wait;
        private System.Windows.Forms.Button btnBreakUnitTYThetaT4Wait;
        private System.Windows.Forms.Button btnBreakUnitTYThetaT1Wait;
        private System.Windows.Forms.Button btnBreakUnitTYThetaT3Wait;
        private System.Windows.Forms.GroupBox gxtBreakUnitTY_MoveB;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel56;
        private System.Windows.Forms.Button btnBreakUnitTYMoveBUnload;
        private System.Windows.Forms.Button btnBreakUnitTYMoveBLoad;
        private System.Windows.Forms.GroupBox gxtBreakUnitTY_MoveA;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel55;
        private System.Windows.Forms.Button btnBreakUnitTYMoveAUnload;
        private System.Windows.Forms.Button btnBreakUnitTYMoveALoad;
        private System.Windows.Forms.GroupBox gxtBreakUnitTY_B;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel50;
        private System.Windows.Forms.Button btnBreakUnitTYB1DestructionOn;
        private System.Windows.Forms.Button btnBreakUnitTYB1DestructionOff;
        private System.Windows.Forms.Button btnBreakUnitTYB2DestructionOn;
        private System.Windows.Forms.Button btnBreakUnitTYB2DestructionOff;
        private System.Windows.Forms.Button btnBreakUnitTYB1PneumaticOff;
        private System.Windows.Forms.Button btnBreakUnitTYB1PneumaticOn;
        private System.Windows.Forms.Button btnBreakUnitTYb2PneumaticOn;
        private System.Windows.Forms.Button btnBreakUnitTYB2PneumaticOff;
        private System.Windows.Forms.GroupBox gxtBreakUnitTY_A;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel51;
        private System.Windows.Forms.Button btnBreakUnitTYA1DestructionOn;
        private System.Windows.Forms.Button btnBreakUnitTYA1DestructionOff;
        private System.Windows.Forms.Button btnBreakUnitTYA2DestructionOn;
        private System.Windows.Forms.Button btnBreakUnitTYA2DestructionOff;
        private System.Windows.Forms.Button btnBreakUnitTYA1PneumaticOff;
        private System.Windows.Forms.Button btnBreakUnitTYA1PneumaticOn;
        private System.Windows.Forms.Button btnBreakUnitTYA2PneumaticOn;
        private System.Windows.Forms.Button btnBreakUnitTYA2PneumaticOff;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel61;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel62;
        private System.Windows.Forms.Button btnUnloaderTransferMoveBT40Angle;
        private System.Windows.Forms.Button btnUnloaderTransferMoveBT4P90Angle;
        private System.Windows.Forms.Button btnUnloaderTransferMoveBT4M90Angle;
        private System.Windows.Forms.Button btnUnloaderTransferMoveBT30Angle;
        private System.Windows.Forms.Button btnUnloaderTransferMoveBT3P90Angle;
        private System.Windows.Forms.Button btnUnloaderTransferMoveBT3M90Angle;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel63;
        private System.Windows.Forms.Button btnUnloaderTransferMoveBBStage;
        private System.Windows.Forms.Button btnUnloaderTransferMoveBBUnloading;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel58;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel60;
        private System.Windows.Forms.Button btnUnloaderTransferMoveAT20Angle;
        private System.Windows.Forms.Button btnUnloaderTransferMoveAT2P90Angle;
        private System.Windows.Forms.Button btnUnloaderTransferMoveAT2M90Angle;
        private System.Windows.Forms.Button btnUnloaderTransferMoveAT10Angle;
        private System.Windows.Forms.Button btnUnloaderTransferMoveAT1P90Angle;
        private System.Windows.Forms.Button btnUnloaderTransferMoveAT1M90Angle;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel59;
        private System.Windows.Forms.Button btnUnloaderTransferMoveAAStage;
        private System.Windows.Forms.Button btnUnloaderTransferMoveAAUnloading;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel65;
        private System.Windows.Forms.Button btnCameraUnitMoveBInspectionMove4;
        private System.Windows.Forms.Button btnCameraUnitMoveBInspectionMove3;
        private System.Windows.Forms.Button btnCameraUnitMoveBMCRMove;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel64;
        private System.Windows.Forms.Button btnCameraUnitMoveAInspectionMove2;
        private System.Windows.Forms.Button btnCameraUnitMoveAInspectionMove1;
        private System.Windows.Forms.Button btnCameraUnitMoveAMCRMove;
        private System.Windows.Forms.GroupBox gxtCellInput_Buffer;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel66;
        private System.Windows.Forms.Button btnCellInputBufferDestructionOn;
        private System.Windows.Forms.Button btnCellInputBufferDestructionOff;
        private System.Windows.Forms.Button btnCellInputBufferPickerUp;
        private System.Windows.Forms.Button btnCellInputBufferPickerDown;
        private System.Windows.Forms.Button btnCellInputBufferPneumaticOn;
        private System.Windows.Forms.Button btnCellInputBufferPneumaticOff;
        private System.Windows.Forms.GroupBox gxtCellInput_MoveCenter;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel69;
        private System.Windows.Forms.Button btnCellInputMoveCenterABuffer;
        private System.Windows.Forms.Button btnCellInputMoveCenterBBuffer;
        private System.Windows.Forms.Button btnCellInputMoveCenterY1CstMid;
        private System.Windows.Forms.Button btnCellInputMoveCenterY2CstMid;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel68;
        private System.Windows.Forms.Button btnCellInputMoveBX2BUld;
        private System.Windows.Forms.Button btnCellInputMoveBY2BUld;
        private System.Windows.Forms.Button btnCellInputMoveBX2BCstMid;
        private System.Windows.Forms.Button btnCellInputMoveBX2BCstWait;
        private System.Windows.Forms.Button btnCellInputMoveBX1AUld;
        private System.Windows.Forms.Button btnCellInputMoveBY1BUld;
        private System.Windows.Forms.Button btnCellInputMoveBX1BCstMId;
        private System.Windows.Forms.Button btnCellInputMoveBX1BCstWait;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel67;
        private System.Windows.Forms.Button btnCellInputMoveAX2AUld;
        private System.Windows.Forms.Button btnCellInputMoveAY2AUld;
        private System.Windows.Forms.Button btnCellInputMoveAX2ACstMid;
        private System.Windows.Forms.Button btnCellInputMoveAX2ACstWait;
        private System.Windows.Forms.Button btnCellInputMoveAX1AUld;
        private System.Windows.Forms.Button btnCellInputMoveAY1AUld;
        private System.Windows.Forms.Button btnCellInputMoveAX1ACstMid;
        private System.Windows.Forms.Button btnCellInputMoveAX1ACstWait;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel75;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel76;
        private System.Windows.Forms.Button btnCasseteUnloadMoveBLiftZ2CellOut;
        private System.Windows.Forms.Button btnCasseteUnloadMoveBLiftZ2CstTilt;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel77;
        private System.Windows.Forms.Button btnCasseteUnloadMoveBLiftZ2CstOut;
        private System.Windows.Forms.Button btnCasseteUnloadMoveBLiftZ2CstWait;
        private System.Windows.Forms.Button btnCasseteUnloadMoveBLiftZ2CstIn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel78;
        private System.Windows.Forms.Button btnCasseteUnloadMoveBBottomT4LiftOut;
        private System.Windows.Forms.Button btnCasseteUnloadMoveBTopT3CstMove;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel79;
        private System.Windows.Forms.Button btnCasseteUnloadMoveBBottomT4CstIn;
        private System.Windows.Forms.Button btnCasseteUnloadMoveBTopT3Out;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel70;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel71;
        private System.Windows.Forms.Button btnCasseteUnloadMoveALiftZ1CellOut;
        private System.Windows.Forms.Button btnCasseteUnloadMoveALiftZ1CstTilt;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel72;
        private System.Windows.Forms.Button btnCasseteUnloadMoveALiftZ1CstOut;
        private System.Windows.Forms.Button btnCasseteUnloadMoveALiftZ1CstWait;
        private System.Windows.Forms.Button btnCasseteUnloadMoveALiftZ1CstIn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel73;
        private System.Windows.Forms.Button btnCasseteUnloadMoveABottomT2Move;
        private System.Windows.Forms.Button btnCasseteUnloadMoveATopT1Move;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel74;
        private System.Windows.Forms.Button btnCasseteUnloadMoveABottomT2In;
        private System.Windows.Forms.Button btnCasseteUnloadMoveATopT1Out;
    }
}
