﻿namespace DIT.TLC.UI
{
    partial class FrmManager_Process
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.panel23 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.btnProcessVision = new System.Windows.Forms.Button();
            this.btnProcessCurrent = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnProcessBR2Blow = new System.Windows.Forms.Button();
            this.btnProcessBR2Vac = new System.Windows.Forms.Button();
            this.btnProcessBR1Blow = new System.Windows.Forms.Button();
            this.btnProcessBR1Vac = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.btnProcessAAlignL3 = new System.Windows.Forms.Button();
            this.btnProcessAAlignL2 = new System.Windows.Forms.Button();
            this.btnProces_AAlignL1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.btnProcessBAlignR3 = new System.Windows.Forms.Button();
            this.btnProcessBAlignR1 = new System.Windows.Forms.Button();
            this.btnProcessBAlignR2 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnProcessAL2Blow = new System.Windows.Forms.Button();
            this.btnProcessAL2Vac = new System.Windows.Forms.Button();
            this.btnProcessAL1Blow = new System.Windows.Forms.Button();
            this.btnProcessAL1Vac = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.btnProcessAProcessL3 = new System.Windows.Forms.Button();
            this.btnProcessAProcessL2 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.btnProcessAProcessL1 = new System.Windows.Forms.Button();
            this.panel12 = new System.Windows.Forms.Panel();
            this.btnProcessBProcessR3 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.btnProcessBProcessR1 = new System.Windows.Forms.Button();
            this.btnProcessBProcessR2 = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnBreakAL2Blow = new System.Windows.Forms.Button();
            this.btnBreakAL2Vac = new System.Windows.Forms.Button();
            this.btnBreakAL1Blow = new System.Windows.Forms.Button();
            this.btnBreakAL1Vac = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnBreakBR2Blow = new System.Windows.Forms.Button();
            this.btnBreakBR2Vac = new System.Windows.Forms.Button();
            this.btnBreakBR1Blow = new System.Windows.Forms.Button();
            this.btnBreakBR1Vac = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.btnBreakBAlignR3 = new System.Windows.Forms.Button();
            this.btnBreakBAlignR1 = new System.Windows.Forms.Button();
            this.btnBreakBAlignR2 = new System.Windows.Forms.Button();
            this.panel15 = new System.Windows.Forms.Panel();
            this.btnBreakAAlignL3 = new System.Windows.Forms.Button();
            this.btnBreakAAlignL2 = new System.Windows.Forms.Button();
            this.btnBreakAAlignL1 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.btnBreakBProcessR3 = new System.Windows.Forms.Button();
            this.btnBreakBProcessR1 = new System.Windows.Forms.Button();
            this.btnBreakBProcessR2 = new System.Windows.Forms.Button();
            this.panel17 = new System.Windows.Forms.Panel();
            this.btnBreakAProcessL3 = new System.Windows.Forms.Button();
            this.btnBreakAProcessL2 = new System.Windows.Forms.Button();
            this.btnBreakAProcessL1 = new System.Windows.Forms.Button();
            this.panel19 = new System.Windows.Forms.Panel();
            this.panel25 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.btnMoveBreakTableRightUnload = new System.Windows.Forms.Button();
            this.btnMoveBreakTableLeftUnload = new System.Windows.Forms.Button();
            this.btnMoveBreakTableRightLoad = new System.Windows.Forms.Button();
            this.btnMoveBreakTableLeftLoad = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btnMoveProcessRightUnload = new System.Windows.Forms.Button();
            this.btnMoveProcessLeftUnload = new System.Windows.Forms.Button();
            this.btnMoveProcessRightLoad = new System.Windows.Forms.Button();
            this.btnMoveProcessLeftLoad = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btnMoveBreakTransferRightUnload = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.btnMoveBreakTransferLeftUnload = new System.Windows.Forms.Button();
            this.btnMoveBreakTransferRightLoad = new System.Windows.Forms.Button();
            this.btnMoveBreakTransferLeftLoad = new System.Windows.Forms.Button();
            this.panel21 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel27 = new System.Windows.Forms.Panel();
            this.label23 = new System.Windows.Forms.Label();
            this.panel22 = new System.Windows.Forms.Panel();
            this.btnAlignIR2 = new System.Windows.Forms.Button();
            this.btnAlignIR1 = new System.Windows.Forms.Button();
            this.btnAlignIL2 = new System.Windows.Forms.Button();
            this.btnAlignIL1 = new System.Windows.Forms.Button();
            this.btnAlignIdle = new System.Windows.Forms.Button();
            this.btnAlignTestInsp = new System.Windows.Forms.Button();
            this.btnAlignPreAlign = new System.Windows.Forms.Button();
            this.txtAlignTotalInspCnt = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtAlignInspCount = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.panel20 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel26 = new System.Windows.Forms.Panel();
            this.label22 = new System.Windows.Forms.Label();
            this.txtEtcTimeShot = new System.Windows.Forms.TextBox();
            this.btnEtcTimeShot = new System.Windows.Forms.Button();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.btnEtcShutterClose = new System.Windows.Forms.Button();
            this.btnEtcShutterOpen = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.btnEtcOneProcess = new System.Windows.Forms.Button();
            this.btnEtcRepeatMove = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.panel24 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel25.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel21.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel20.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel26.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.panel24.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox5);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(398, 684);
            this.panel1.TabIndex = 3;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.panel23);
            this.groupBox5.Controls.Add(this.panel18);
            this.groupBox5.Controls.Add(this.panel2);
            this.groupBox5.Controls.Add(this.panel11);
            this.groupBox5.Controls.Add(this.panel10);
            this.groupBox5.Controls.Add(this.panel3);
            this.groupBox5.Controls.Add(this.panel13);
            this.groupBox5.Controls.Add(this.panel12);
            this.groupBox5.Location = new System.Drawing.Point(7, -2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(390, 686);
            this.groupBox5.TabIndex = 73;
            this.groupBox5.TabStop = false;
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel23.Controls.Add(this.label19);
            this.panel23.ForeColor = System.Drawing.Color.White;
            this.panel23.Location = new System.Drawing.Point(2, 8);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(387, 38);
            this.panel23.TabIndex = 53;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold);
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(143, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(101, 32);
            this.label19.TabIndex = 53;
            this.label19.Text = "Process";
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.CornflowerBlue;
            this.panel18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel18.Controls.Add(this.btnProcessVision);
            this.panel18.Controls.Add(this.btnProcessCurrent);
            this.panel18.Location = new System.Drawing.Point(1, 459);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(386, 50);
            this.panel18.TabIndex = 51;
            // 
            // btnProcessVision
            // 
            this.btnProcessVision.BackColor = System.Drawing.Color.DimGray;
            this.btnProcessVision.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnProcessVision.ForeColor = System.Drawing.Color.White;
            this.btnProcessVision.Location = new System.Drawing.Point(203, 7);
            this.btnProcessVision.Name = "btnProcessVision";
            this.btnProcessVision.Size = new System.Drawing.Size(174, 33);
            this.btnProcessVision.TabIndex = 47;
            this.btnProcessVision.Text = "Vision";
            this.btnProcessVision.UseVisualStyleBackColor = false;
            // 
            // btnProcessCurrent
            // 
            this.btnProcessCurrent.BackColor = System.Drawing.Color.DimGray;
            this.btnProcessCurrent.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnProcessCurrent.ForeColor = System.Drawing.Color.White;
            this.btnProcessCurrent.Location = new System.Drawing.Point(8, 7);
            this.btnProcessCurrent.Name = "btnProcessCurrent";
            this.btnProcessCurrent.Size = new System.Drawing.Size(174, 33);
            this.btnProcessCurrent.TabIndex = 46;
            this.btnProcessCurrent.Text = "Current";
            this.btnProcessCurrent.UseVisualStyleBackColor = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Purple;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btnProcessBR2Blow);
            this.panel2.Controls.Add(this.btnProcessBR2Vac);
            this.panel2.Controls.Add(this.btnProcessBR1Blow);
            this.panel2.Controls.Add(this.btnProcessBR1Vac);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(197, 56);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(190, 225);
            this.panel2.TabIndex = 10;
            // 
            // btnProcessBR2Blow
            // 
            this.btnProcessBR2Blow.BackColor = System.Drawing.Color.DimGray;
            this.btnProcessBR2Blow.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnProcessBR2Blow.ForeColor = System.Drawing.Color.White;
            this.btnProcessBR2Blow.Location = new System.Drawing.Point(95, 140);
            this.btnProcessBR2Blow.Name = "btnProcessBR2Blow";
            this.btnProcessBR2Blow.Size = new System.Drawing.Size(85, 67);
            this.btnProcessBR2Blow.TabIndex = 42;
            this.btnProcessBR2Blow.Text = "R2 Blow";
            this.btnProcessBR2Blow.UseVisualStyleBackColor = false;
            // 
            // btnProcessBR2Vac
            // 
            this.btnProcessBR2Vac.BackColor = System.Drawing.Color.DimGray;
            this.btnProcessBR2Vac.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnProcessBR2Vac.ForeColor = System.Drawing.Color.White;
            this.btnProcessBR2Vac.Location = new System.Drawing.Point(6, 140);
            this.btnProcessBR2Vac.Name = "btnProcessBR2Vac";
            this.btnProcessBR2Vac.Size = new System.Drawing.Size(85, 67);
            this.btnProcessBR2Vac.TabIndex = 41;
            this.btnProcessBR2Vac.Text = "R2 Vac";
            this.btnProcessBR2Vac.UseVisualStyleBackColor = false;
            // 
            // btnProcessBR1Blow
            // 
            this.btnProcessBR1Blow.BackColor = System.Drawing.Color.DimGray;
            this.btnProcessBR1Blow.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnProcessBR1Blow.ForeColor = System.Drawing.Color.White;
            this.btnProcessBR1Blow.Location = new System.Drawing.Point(96, 40);
            this.btnProcessBR1Blow.Name = "btnProcessBR1Blow";
            this.btnProcessBR1Blow.Size = new System.Drawing.Size(85, 67);
            this.btnProcessBR1Blow.TabIndex = 40;
            this.btnProcessBR1Blow.Text = "R1 Blow";
            this.btnProcessBR1Blow.UseVisualStyleBackColor = false;
            // 
            // btnProcessBR1Vac
            // 
            this.btnProcessBR1Vac.BackColor = System.Drawing.Color.DimGray;
            this.btnProcessBR1Vac.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnProcessBR1Vac.ForeColor = System.Drawing.Color.White;
            this.btnProcessBR1Vac.Location = new System.Drawing.Point(7, 40);
            this.btnProcessBR1Vac.Name = "btnProcessBR1Vac";
            this.btnProcessBR1Vac.Size = new System.Drawing.Size(85, 67);
            this.btnProcessBR1Vac.TabIndex = 39;
            this.btnProcessBR1Vac.Text = "R1 Vac";
            this.btnProcessBR1Vac.UseVisualStyleBackColor = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(80, -9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 30);
            this.label2.TabIndex = 52;
            this.label2.Text = "B";
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.SlateGray;
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.btnProcessAAlignL3);
            this.panel11.Controls.Add(this.btnProcessAAlignL2);
            this.panel11.Controls.Add(this.btnProces_AAlignL1);
            this.panel11.Controls.Add(this.label4);
            this.panel11.Location = new System.Drawing.Point(1, 287);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(190, 165);
            this.panel11.TabIndex = 47;
            // 
            // btnProcessAAlignL3
            // 
            this.btnProcessAAlignL3.BackColor = System.Drawing.Color.DimGray;
            this.btnProcessAAlignL3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnProcessAAlignL3.ForeColor = System.Drawing.Color.White;
            this.btnProcessAAlignL3.Location = new System.Drawing.Point(7, 115);
            this.btnProcessAAlignL3.Name = "btnProcessAAlignL3";
            this.btnProcessAAlignL3.Size = new System.Drawing.Size(174, 42);
            this.btnProcessAAlignL3.TabIndex = 45;
            this.btnProcessAAlignL3.Text = "Align L3";
            this.btnProcessAAlignL3.UseVisualStyleBackColor = false;
            // 
            // btnProcessAAlignL2
            // 
            this.btnProcessAAlignL2.BackColor = System.Drawing.Color.DimGray;
            this.btnProcessAAlignL2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnProcessAAlignL2.ForeColor = System.Drawing.Color.White;
            this.btnProcessAAlignL2.Location = new System.Drawing.Point(7, 69);
            this.btnProcessAAlignL2.Name = "btnProcessAAlignL2";
            this.btnProcessAAlignL2.Size = new System.Drawing.Size(174, 42);
            this.btnProcessAAlignL2.TabIndex = 44;
            this.btnProcessAAlignL2.Text = "Align L2";
            this.btnProcessAAlignL2.UseVisualStyleBackColor = false;
            // 
            // btnProces_AAlignL1
            // 
            this.btnProces_AAlignL1.BackColor = System.Drawing.Color.DimGray;
            this.btnProces_AAlignL1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnProces_AAlignL1.ForeColor = System.Drawing.Color.White;
            this.btnProces_AAlignL1.Location = new System.Drawing.Point(7, 23);
            this.btnProces_AAlignL1.Name = "btnProces_AAlignL1";
            this.btnProces_AAlignL1.Size = new System.Drawing.Size(174, 42);
            this.btnProces_AAlignL1.TabIndex = 43;
            this.btnProces_AAlignL1.Text = "Align L1";
            this.btnProces_AAlignL1.UseVisualStyleBackColor = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(80, -8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 30);
            this.label4.TabIndex = 53;
            this.label4.Text = "A";
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.Purple;
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Controls.Add(this.btnProcessBAlignR3);
            this.panel10.Controls.Add(this.btnProcessBAlignR1);
            this.panel10.Controls.Add(this.btnProcessBAlignR2);
            this.panel10.Controls.Add(this.label3);
            this.panel10.Location = new System.Drawing.Point(197, 287);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(190, 165);
            this.panel10.TabIndex = 48;
            // 
            // btnProcessBAlignR3
            // 
            this.btnProcessBAlignR3.BackColor = System.Drawing.Color.DimGray;
            this.btnProcessBAlignR3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnProcessBAlignR3.ForeColor = System.Drawing.Color.White;
            this.btnProcessBAlignR3.Location = new System.Drawing.Point(7, 115);
            this.btnProcessBAlignR3.Name = "btnProcessBAlignR3";
            this.btnProcessBAlignR3.Size = new System.Drawing.Size(174, 42);
            this.btnProcessBAlignR3.TabIndex = 48;
            this.btnProcessBAlignR3.Text = "Align R3";
            this.btnProcessBAlignR3.UseVisualStyleBackColor = false;
            // 
            // btnProcessBAlignR1
            // 
            this.btnProcessBAlignR1.BackColor = System.Drawing.Color.DimGray;
            this.btnProcessBAlignR1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnProcessBAlignR1.ForeColor = System.Drawing.Color.White;
            this.btnProcessBAlignR1.Location = new System.Drawing.Point(7, 23);
            this.btnProcessBAlignR1.Name = "btnProcessBAlignR1";
            this.btnProcessBAlignR1.Size = new System.Drawing.Size(174, 42);
            this.btnProcessBAlignR1.TabIndex = 46;
            this.btnProcessBAlignR1.Text = "Align R1";
            this.btnProcessBAlignR1.UseVisualStyleBackColor = false;
            // 
            // btnProcessBAlignR2
            // 
            this.btnProcessBAlignR2.BackColor = System.Drawing.Color.DimGray;
            this.btnProcessBAlignR2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnProcessBAlignR2.ForeColor = System.Drawing.Color.White;
            this.btnProcessBAlignR2.Location = new System.Drawing.Point(7, 69);
            this.btnProcessBAlignR2.Name = "btnProcessBAlignR2";
            this.btnProcessBAlignR2.Size = new System.Drawing.Size(174, 42);
            this.btnProcessBAlignR2.TabIndex = 47;
            this.btnProcessBAlignR2.Text = "Align R2";
            this.btnProcessBAlignR2.UseVisualStyleBackColor = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(80, -8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 30);
            this.label3.TabIndex = 54;
            this.label3.Text = "B";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.SlateGray;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.btnProcessAL2Blow);
            this.panel3.Controls.Add(this.btnProcessAL2Vac);
            this.panel3.Controls.Add(this.btnProcessAL1Blow);
            this.panel3.Controls.Add(this.btnProcessAL1Vac);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Location = new System.Drawing.Point(1, 56);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(190, 225);
            this.panel3.TabIndex = 1;
            // 
            // btnProcessAL2Blow
            // 
            this.btnProcessAL2Blow.BackColor = System.Drawing.Color.DimGray;
            this.btnProcessAL2Blow.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnProcessAL2Blow.ForeColor = System.Drawing.Color.White;
            this.btnProcessAL2Blow.Location = new System.Drawing.Point(95, 140);
            this.btnProcessAL2Blow.Name = "btnProcessAL2Blow";
            this.btnProcessAL2Blow.Size = new System.Drawing.Size(85, 67);
            this.btnProcessAL2Blow.TabIndex = 46;
            this.btnProcessAL2Blow.Text = "L2 Blow";
            this.btnProcessAL2Blow.UseVisualStyleBackColor = false;
            // 
            // btnProcessAL2Vac
            // 
            this.btnProcessAL2Vac.BackColor = System.Drawing.Color.DimGray;
            this.btnProcessAL2Vac.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnProcessAL2Vac.ForeColor = System.Drawing.Color.White;
            this.btnProcessAL2Vac.Location = new System.Drawing.Point(6, 140);
            this.btnProcessAL2Vac.Name = "btnProcessAL2Vac";
            this.btnProcessAL2Vac.Size = new System.Drawing.Size(85, 67);
            this.btnProcessAL2Vac.TabIndex = 45;
            this.btnProcessAL2Vac.Text = "L2 Vac";
            this.btnProcessAL2Vac.UseVisualStyleBackColor = false;
            // 
            // btnProcessAL1Blow
            // 
            this.btnProcessAL1Blow.BackColor = System.Drawing.Color.DimGray;
            this.btnProcessAL1Blow.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnProcessAL1Blow.ForeColor = System.Drawing.Color.White;
            this.btnProcessAL1Blow.Location = new System.Drawing.Point(96, 40);
            this.btnProcessAL1Blow.Name = "btnProcessAL1Blow";
            this.btnProcessAL1Blow.Size = new System.Drawing.Size(85, 67);
            this.btnProcessAL1Blow.TabIndex = 44;
            this.btnProcessAL1Blow.Text = "L1 Blow";
            this.btnProcessAL1Blow.UseVisualStyleBackColor = false;
            // 
            // btnProcessAL1Vac
            // 
            this.btnProcessAL1Vac.BackColor = System.Drawing.Color.DimGray;
            this.btnProcessAL1Vac.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnProcessAL1Vac.ForeColor = System.Drawing.Color.White;
            this.btnProcessAL1Vac.Location = new System.Drawing.Point(7, 40);
            this.btnProcessAL1Vac.Name = "btnProcessAL1Vac";
            this.btnProcessAL1Vac.Size = new System.Drawing.Size(85, 67);
            this.btnProcessAL1Vac.TabIndex = 43;
            this.btnProcessAL1Vac.Text = "L1 Vac";
            this.btnProcessAL1Vac.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(80, -9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 30);
            this.label1.TabIndex = 15;
            this.label1.Text = "A";
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.SlateGray;
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel13.Controls.Add(this.btnProcessAProcessL3);
            this.panel13.Controls.Add(this.btnProcessAProcessL2);
            this.panel13.Controls.Add(this.label6);
            this.panel13.Controls.Add(this.btnProcessAProcessL1);
            this.panel13.Location = new System.Drawing.Point(1, 515);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(190, 165);
            this.panel13.TabIndex = 49;
            // 
            // btnProcessAProcessL3
            // 
            this.btnProcessAProcessL3.BackColor = System.Drawing.Color.DimGray;
            this.btnProcessAProcessL3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnProcessAProcessL3.ForeColor = System.Drawing.Color.White;
            this.btnProcessAProcessL3.Location = new System.Drawing.Point(7, 113);
            this.btnProcessAProcessL3.Name = "btnProcessAProcessL3";
            this.btnProcessAProcessL3.Size = new System.Drawing.Size(174, 42);
            this.btnProcessAProcessL3.TabIndex = 48;
            this.btnProcessAProcessL3.Text = "Process L3";
            this.btnProcessAProcessL3.UseVisualStyleBackColor = false;
            // 
            // btnProcessAProcessL2
            // 
            this.btnProcessAProcessL2.BackColor = System.Drawing.Color.DimGray;
            this.btnProcessAProcessL2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnProcessAProcessL2.ForeColor = System.Drawing.Color.White;
            this.btnProcessAProcessL2.Location = new System.Drawing.Point(7, 67);
            this.btnProcessAProcessL2.Name = "btnProcessAProcessL2";
            this.btnProcessAProcessL2.Size = new System.Drawing.Size(174, 42);
            this.btnProcessAProcessL2.TabIndex = 47;
            this.btnProcessAProcessL2.Text = "Process L2";
            this.btnProcessAProcessL2.UseVisualStyleBackColor = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(80, -10);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(28, 30);
            this.label6.TabIndex = 55;
            this.label6.Text = "A";
            // 
            // btnProcessAProcessL1
            // 
            this.btnProcessAProcessL1.BackColor = System.Drawing.Color.DimGray;
            this.btnProcessAProcessL1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnProcessAProcessL1.ForeColor = System.Drawing.Color.White;
            this.btnProcessAProcessL1.Location = new System.Drawing.Point(7, 21);
            this.btnProcessAProcessL1.Name = "btnProcessAProcessL1";
            this.btnProcessAProcessL1.Size = new System.Drawing.Size(174, 42);
            this.btnProcessAProcessL1.TabIndex = 46;
            this.btnProcessAProcessL1.Text = "Process L1";
            this.btnProcessAProcessL1.UseVisualStyleBackColor = false;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.Purple;
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Controls.Add(this.btnProcessBProcessR3);
            this.panel12.Controls.Add(this.label5);
            this.panel12.Controls.Add(this.btnProcessBProcessR1);
            this.panel12.Controls.Add(this.btnProcessBProcessR2);
            this.panel12.Location = new System.Drawing.Point(197, 515);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(190, 165);
            this.panel12.TabIndex = 50;
            // 
            // btnProcessBProcessR3
            // 
            this.btnProcessBProcessR3.BackColor = System.Drawing.Color.DimGray;
            this.btnProcessBProcessR3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnProcessBProcessR3.ForeColor = System.Drawing.Color.White;
            this.btnProcessBProcessR3.Location = new System.Drawing.Point(7, 113);
            this.btnProcessBProcessR3.Name = "btnProcessBProcessR3";
            this.btnProcessBProcessR3.Size = new System.Drawing.Size(174, 42);
            this.btnProcessBProcessR3.TabIndex = 54;
            this.btnProcessBProcessR3.Text = "Process R3";
            this.btnProcessBProcessR3.UseVisualStyleBackColor = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(80, -10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(26, 30);
            this.label5.TabIndex = 56;
            this.label5.Text = "B";
            // 
            // btnProcessBProcessR1
            // 
            this.btnProcessBProcessR1.BackColor = System.Drawing.Color.DimGray;
            this.btnProcessBProcessR1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnProcessBProcessR1.ForeColor = System.Drawing.Color.White;
            this.btnProcessBProcessR1.Location = new System.Drawing.Point(7, 21);
            this.btnProcessBProcessR1.Name = "btnProcessBProcessR1";
            this.btnProcessBProcessR1.Size = new System.Drawing.Size(174, 42);
            this.btnProcessBProcessR1.TabIndex = 52;
            this.btnProcessBProcessR1.Text = "Process R1";
            this.btnProcessBProcessR1.UseVisualStyleBackColor = false;
            // 
            // btnProcessBProcessR2
            // 
            this.btnProcessBProcessR2.BackColor = System.Drawing.Color.DimGray;
            this.btnProcessBProcessR2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnProcessBProcessR2.ForeColor = System.Drawing.Color.White;
            this.btnProcessBProcessR2.Location = new System.Drawing.Point(7, 67);
            this.btnProcessBProcessR2.Name = "btnProcessBProcessR2";
            this.btnProcessBProcessR2.Size = new System.Drawing.Size(174, 42);
            this.btnProcessBProcessR2.TabIndex = 53;
            this.btnProcessBProcessR2.Text = "Process R2";
            this.btnProcessBProcessR2.UseVisualStyleBackColor = false;
            // 
            // panel4
            // 
            this.panel4.Location = new System.Drawing.Point(416, 12);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(398, 684);
            this.panel4.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(82, -7);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(28, 30);
            this.label8.TabIndex = 57;
            this.label8.Text = "A";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.SlateGray;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.btnBreakAL2Blow);
            this.panel6.Controls.Add(this.label8);
            this.panel6.Controls.Add(this.btnBreakAL2Vac);
            this.panel6.Controls.Add(this.btnBreakAL1Blow);
            this.panel6.Controls.Add(this.btnBreakAL1Vac);
            this.panel6.Location = new System.Drawing.Point(2, 54);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(190, 266);
            this.panel6.TabIndex = 1;
            // 
            // btnBreakAL2Blow
            // 
            this.btnBreakAL2Blow.BackColor = System.Drawing.Color.DimGray;
            this.btnBreakAL2Blow.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakAL2Blow.ForeColor = System.Drawing.Color.White;
            this.btnBreakAL2Blow.Location = new System.Drawing.Point(96, 149);
            this.btnBreakAL2Blow.Name = "btnBreakAL2Blow";
            this.btnBreakAL2Blow.Size = new System.Drawing.Size(85, 67);
            this.btnBreakAL2Blow.TabIndex = 50;
            this.btnBreakAL2Blow.Text = "L2 Blow";
            this.btnBreakAL2Blow.UseVisualStyleBackColor = false;
            // 
            // btnBreakAL2Vac
            // 
            this.btnBreakAL2Vac.BackColor = System.Drawing.Color.DimGray;
            this.btnBreakAL2Vac.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakAL2Vac.ForeColor = System.Drawing.Color.White;
            this.btnBreakAL2Vac.Location = new System.Drawing.Point(7, 149);
            this.btnBreakAL2Vac.Name = "btnBreakAL2Vac";
            this.btnBreakAL2Vac.Size = new System.Drawing.Size(85, 67);
            this.btnBreakAL2Vac.TabIndex = 49;
            this.btnBreakAL2Vac.Text = "L2 Vac";
            this.btnBreakAL2Vac.UseVisualStyleBackColor = false;
            // 
            // btnBreakAL1Blow
            // 
            this.btnBreakAL1Blow.BackColor = System.Drawing.Color.DimGray;
            this.btnBreakAL1Blow.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakAL1Blow.ForeColor = System.Drawing.Color.White;
            this.btnBreakAL1Blow.Location = new System.Drawing.Point(97, 49);
            this.btnBreakAL1Blow.Name = "btnBreakAL1Blow";
            this.btnBreakAL1Blow.Size = new System.Drawing.Size(85, 67);
            this.btnBreakAL1Blow.TabIndex = 48;
            this.btnBreakAL1Blow.Text = "L1 Blow";
            this.btnBreakAL1Blow.UseVisualStyleBackColor = false;
            // 
            // btnBreakAL1Vac
            // 
            this.btnBreakAL1Vac.BackColor = System.Drawing.Color.DimGray;
            this.btnBreakAL1Vac.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakAL1Vac.ForeColor = System.Drawing.Color.White;
            this.btnBreakAL1Vac.Location = new System.Drawing.Point(8, 49);
            this.btnBreakAL1Vac.Name = "btnBreakAL1Vac";
            this.btnBreakAL1Vac.Size = new System.Drawing.Size(85, 67);
            this.btnBreakAL1Vac.TabIndex = 47;
            this.btnBreakAL1Vac.Text = "L1 Vac";
            this.btnBreakAL1Vac.UseVisualStyleBackColor = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(82, -7);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(26, 30);
            this.label7.TabIndex = 58;
            this.label7.Text = "B";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Purple;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.btnBreakBR2Blow);
            this.panel5.Controls.Add(this.btnBreakBR2Vac);
            this.panel5.Controls.Add(this.label7);
            this.panel5.Controls.Add(this.btnBreakBR1Blow);
            this.panel5.Controls.Add(this.btnBreakBR1Vac);
            this.panel5.Location = new System.Drawing.Point(198, 54);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(190, 266);
            this.panel5.TabIndex = 10;
            // 
            // btnBreakBR2Blow
            // 
            this.btnBreakBR2Blow.BackColor = System.Drawing.Color.DimGray;
            this.btnBreakBR2Blow.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakBR2Blow.ForeColor = System.Drawing.Color.White;
            this.btnBreakBR2Blow.Location = new System.Drawing.Point(96, 149);
            this.btnBreakBR2Blow.Name = "btnBreakBR2Blow";
            this.btnBreakBR2Blow.Size = new System.Drawing.Size(85, 67);
            this.btnBreakBR2Blow.TabIndex = 46;
            this.btnBreakBR2Blow.Text = "R2 Blow";
            this.btnBreakBR2Blow.UseVisualStyleBackColor = false;
            // 
            // btnBreakBR2Vac
            // 
            this.btnBreakBR2Vac.BackColor = System.Drawing.Color.DimGray;
            this.btnBreakBR2Vac.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakBR2Vac.ForeColor = System.Drawing.Color.White;
            this.btnBreakBR2Vac.Location = new System.Drawing.Point(7, 149);
            this.btnBreakBR2Vac.Name = "btnBreakBR2Vac";
            this.btnBreakBR2Vac.Size = new System.Drawing.Size(85, 67);
            this.btnBreakBR2Vac.TabIndex = 45;
            this.btnBreakBR2Vac.Text = "R2 Vac";
            this.btnBreakBR2Vac.UseVisualStyleBackColor = false;
            // 
            // btnBreakBR1Blow
            // 
            this.btnBreakBR1Blow.BackColor = System.Drawing.Color.DimGray;
            this.btnBreakBR1Blow.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakBR1Blow.ForeColor = System.Drawing.Color.White;
            this.btnBreakBR1Blow.Location = new System.Drawing.Point(97, 49);
            this.btnBreakBR1Blow.Name = "btnBreakBR1Blow";
            this.btnBreakBR1Blow.Size = new System.Drawing.Size(85, 67);
            this.btnBreakBR1Blow.TabIndex = 44;
            this.btnBreakBR1Blow.Text = "R1 Blow";
            this.btnBreakBR1Blow.UseVisualStyleBackColor = false;
            // 
            // btnBreakBR1Vac
            // 
            this.btnBreakBR1Vac.BackColor = System.Drawing.Color.DimGray;
            this.btnBreakBR1Vac.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakBR1Vac.ForeColor = System.Drawing.Color.White;
            this.btnBreakBR1Vac.Location = new System.Drawing.Point(8, 49);
            this.btnBreakBR1Vac.Name = "btnBreakBR1Vac";
            this.btnBreakBR1Vac.Size = new System.Drawing.Size(85, 67);
            this.btnBreakBR1Vac.TabIndex = 43;
            this.btnBreakBR1Vac.Text = "R1 Vac";
            this.btnBreakBR1Vac.UseVisualStyleBackColor = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(82, -8);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(26, 30);
            this.label9.TabIndex = 60;
            this.label9.Text = "B";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(82, -9);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(28, 30);
            this.label10.TabIndex = 59;
            this.label10.Text = "A";
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.Purple;
            this.panel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel14.Controls.Add(this.btnBreakBAlignR3);
            this.panel14.Controls.Add(this.btnBreakBAlignR1);
            this.panel14.Controls.Add(this.btnBreakBAlignR2);
            this.panel14.Controls.Add(this.label9);
            this.panel14.Location = new System.Drawing.Point(198, 335);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(190, 165);
            this.panel14.TabIndex = 50;
            // 
            // btnBreakBAlignR3
            // 
            this.btnBreakBAlignR3.BackColor = System.Drawing.Color.DimGray;
            this.btnBreakBAlignR3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakBAlignR3.ForeColor = System.Drawing.Color.White;
            this.btnBreakBAlignR3.Location = new System.Drawing.Point(8, 111);
            this.btnBreakBAlignR3.Name = "btnBreakBAlignR3";
            this.btnBreakBAlignR3.Size = new System.Drawing.Size(174, 42);
            this.btnBreakBAlignR3.TabIndex = 51;
            this.btnBreakBAlignR3.Text = "Align R3";
            this.btnBreakBAlignR3.UseVisualStyleBackColor = false;
            // 
            // btnBreakBAlignR1
            // 
            this.btnBreakBAlignR1.BackColor = System.Drawing.Color.DimGray;
            this.btnBreakBAlignR1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakBAlignR1.ForeColor = System.Drawing.Color.White;
            this.btnBreakBAlignR1.Location = new System.Drawing.Point(8, 19);
            this.btnBreakBAlignR1.Name = "btnBreakBAlignR1";
            this.btnBreakBAlignR1.Size = new System.Drawing.Size(174, 42);
            this.btnBreakBAlignR1.TabIndex = 49;
            this.btnBreakBAlignR1.Text = "Align R1";
            this.btnBreakBAlignR1.UseVisualStyleBackColor = false;
            // 
            // btnBreakBAlignR2
            // 
            this.btnBreakBAlignR2.BackColor = System.Drawing.Color.DimGray;
            this.btnBreakBAlignR2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakBAlignR2.ForeColor = System.Drawing.Color.White;
            this.btnBreakBAlignR2.Location = new System.Drawing.Point(8, 65);
            this.btnBreakBAlignR2.Name = "btnBreakBAlignR2";
            this.btnBreakBAlignR2.Size = new System.Drawing.Size(174, 42);
            this.btnBreakBAlignR2.TabIndex = 50;
            this.btnBreakBAlignR2.Text = "Align R2";
            this.btnBreakBAlignR2.UseVisualStyleBackColor = false;
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.SlateGray;
            this.panel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel15.Controls.Add(this.btnBreakAAlignL3);
            this.panel15.Controls.Add(this.btnBreakAAlignL2);
            this.panel15.Controls.Add(this.btnBreakAAlignL1);
            this.panel15.Controls.Add(this.label10);
            this.panel15.Location = new System.Drawing.Point(2, 335);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(190, 165);
            this.panel15.TabIndex = 49;
            // 
            // btnBreakAAlignL3
            // 
            this.btnBreakAAlignL3.BackColor = System.Drawing.Color.DimGray;
            this.btnBreakAAlignL3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakAAlignL3.ForeColor = System.Drawing.Color.White;
            this.btnBreakAAlignL3.Location = new System.Drawing.Point(8, 111);
            this.btnBreakAAlignL3.Name = "btnBreakAAlignL3";
            this.btnBreakAAlignL3.Size = new System.Drawing.Size(174, 42);
            this.btnBreakAAlignL3.TabIndex = 48;
            this.btnBreakAAlignL3.Text = "Align L3";
            this.btnBreakAAlignL3.UseVisualStyleBackColor = false;
            // 
            // btnBreakAAlignL2
            // 
            this.btnBreakAAlignL2.BackColor = System.Drawing.Color.DimGray;
            this.btnBreakAAlignL2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakAAlignL2.ForeColor = System.Drawing.Color.White;
            this.btnBreakAAlignL2.Location = new System.Drawing.Point(8, 65);
            this.btnBreakAAlignL2.Name = "btnBreakAAlignL2";
            this.btnBreakAAlignL2.Size = new System.Drawing.Size(174, 42);
            this.btnBreakAAlignL2.TabIndex = 47;
            this.btnBreakAAlignL2.Text = "Align L2";
            this.btnBreakAAlignL2.UseVisualStyleBackColor = false;
            // 
            // btnBreakAAlignL1
            // 
            this.btnBreakAAlignL1.BackColor = System.Drawing.Color.DimGray;
            this.btnBreakAAlignL1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakAAlignL1.ForeColor = System.Drawing.Color.White;
            this.btnBreakAAlignL1.Location = new System.Drawing.Point(8, 19);
            this.btnBreakAAlignL1.Name = "btnBreakAAlignL1";
            this.btnBreakAAlignL1.Size = new System.Drawing.Size(174, 42);
            this.btnBreakAAlignL1.TabIndex = 46;
            this.btnBreakAAlignL1.Text = "Align L1";
            this.btnBreakAAlignL1.UseVisualStyleBackColor = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(82, -8);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(26, 30);
            this.label11.TabIndex = 62;
            this.label11.Text = "B";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(82, -9);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(28, 30);
            this.label12.TabIndex = 61;
            this.label12.Text = "A";
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.Purple;
            this.panel16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel16.Controls.Add(this.btnBreakBProcessR3);
            this.panel16.Controls.Add(this.btnBreakBProcessR1);
            this.panel16.Controls.Add(this.btnBreakBProcessR2);
            this.panel16.Controls.Add(this.label11);
            this.panel16.Location = new System.Drawing.Point(198, 516);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(190, 165);
            this.panel16.TabIndex = 52;
            // 
            // btnBreakBProcessR3
            // 
            this.btnBreakBProcessR3.BackColor = System.Drawing.Color.DimGray;
            this.btnBreakBProcessR3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakBProcessR3.ForeColor = System.Drawing.Color.White;
            this.btnBreakBProcessR3.Location = new System.Drawing.Point(8, 113);
            this.btnBreakBProcessR3.Name = "btnBreakBProcessR3";
            this.btnBreakBProcessR3.Size = new System.Drawing.Size(174, 42);
            this.btnBreakBProcessR3.TabIndex = 57;
            this.btnBreakBProcessR3.Text = "Process R3";
            this.btnBreakBProcessR3.UseVisualStyleBackColor = false;
            // 
            // btnBreakBProcessR1
            // 
            this.btnBreakBProcessR1.BackColor = System.Drawing.Color.DimGray;
            this.btnBreakBProcessR1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakBProcessR1.ForeColor = System.Drawing.Color.White;
            this.btnBreakBProcessR1.Location = new System.Drawing.Point(8, 21);
            this.btnBreakBProcessR1.Name = "btnBreakBProcessR1";
            this.btnBreakBProcessR1.Size = new System.Drawing.Size(174, 42);
            this.btnBreakBProcessR1.TabIndex = 55;
            this.btnBreakBProcessR1.Text = "Process R1";
            this.btnBreakBProcessR1.UseVisualStyleBackColor = false;
            // 
            // btnBreakBProcessR2
            // 
            this.btnBreakBProcessR2.BackColor = System.Drawing.Color.DimGray;
            this.btnBreakBProcessR2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakBProcessR2.ForeColor = System.Drawing.Color.White;
            this.btnBreakBProcessR2.Location = new System.Drawing.Point(8, 67);
            this.btnBreakBProcessR2.Name = "btnBreakBProcessR2";
            this.btnBreakBProcessR2.Size = new System.Drawing.Size(174, 42);
            this.btnBreakBProcessR2.TabIndex = 56;
            this.btnBreakBProcessR2.Text = "Process R2";
            this.btnBreakBProcessR2.UseVisualStyleBackColor = false;
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.SlateGray;
            this.panel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel17.Controls.Add(this.btnBreakAProcessL3);
            this.panel17.Controls.Add(this.btnBreakAProcessL2);
            this.panel17.Controls.Add(this.btnBreakAProcessL1);
            this.panel17.Controls.Add(this.label12);
            this.panel17.Location = new System.Drawing.Point(2, 516);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(190, 165);
            this.panel17.TabIndex = 51;
            // 
            // btnBreakAProcessL3
            // 
            this.btnBreakAProcessL3.BackColor = System.Drawing.Color.DimGray;
            this.btnBreakAProcessL3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakAProcessL3.ForeColor = System.Drawing.Color.White;
            this.btnBreakAProcessL3.Location = new System.Drawing.Point(8, 113);
            this.btnBreakAProcessL3.Name = "btnBreakAProcessL3";
            this.btnBreakAProcessL3.Size = new System.Drawing.Size(174, 42);
            this.btnBreakAProcessL3.TabIndex = 51;
            this.btnBreakAProcessL3.Text = "Process L3";
            this.btnBreakAProcessL3.UseVisualStyleBackColor = false;
            // 
            // btnBreakAProcessL2
            // 
            this.btnBreakAProcessL2.BackColor = System.Drawing.Color.DimGray;
            this.btnBreakAProcessL2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakAProcessL2.ForeColor = System.Drawing.Color.White;
            this.btnBreakAProcessL2.Location = new System.Drawing.Point(8, 67);
            this.btnBreakAProcessL2.Name = "btnBreakAProcessL2";
            this.btnBreakAProcessL2.Size = new System.Drawing.Size(174, 42);
            this.btnBreakAProcessL2.TabIndex = 50;
            this.btnBreakAProcessL2.Text = "Process L2";
            this.btnBreakAProcessL2.UseVisualStyleBackColor = false;
            // 
            // btnBreakAProcessL1
            // 
            this.btnBreakAProcessL1.BackColor = System.Drawing.Color.DimGray;
            this.btnBreakAProcessL1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakAProcessL1.ForeColor = System.Drawing.Color.White;
            this.btnBreakAProcessL1.Location = new System.Drawing.Point(8, 21);
            this.btnBreakAProcessL1.Name = "btnBreakAProcessL1";
            this.btnBreakAProcessL1.Size = new System.Drawing.Size(174, 42);
            this.btnBreakAProcessL1.TabIndex = 49;
            this.btnBreakAProcessL1.Text = "Process L1";
            this.btnBreakAProcessL1.UseVisualStyleBackColor = false;
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.panel25);
            this.panel19.Controls.Add(this.groupBox3);
            this.panel19.Location = new System.Drawing.Point(820, 12);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(520, 300);
            this.panel19.TabIndex = 15;
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel25.Controls.Add(this.label21);
            this.panel25.ForeColor = System.Drawing.Color.White;
            this.panel25.Location = new System.Drawing.Point(4, 8);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(514, 38);
            this.panel25.TabIndex = 59;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold);
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(214, 3);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(79, 32);
            this.label21.TabIndex = 53;
            this.label21.Text = "Move";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.panel9);
            this.groupBox3.Controls.Add(this.panel7);
            this.groupBox3.Controls.Add(this.panel8);
            this.groupBox3.Location = new System.Drawing.Point(3, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(517, 302);
            this.groupBox3.TabIndex = 71;
            this.groupBox3.TabStop = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.SlateGray;
            this.label13.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(46, 53);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(89, 30);
            this.label13.TabIndex = 58;
            this.label13.Text = "Process";
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.LightSteelBlue;
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.label15);
            this.panel9.Controls.Add(this.btnMoveBreakTableRightUnload);
            this.panel9.Controls.Add(this.btnMoveBreakTableLeftUnload);
            this.panel9.Controls.Add(this.btnMoveBreakTableRightLoad);
            this.panel9.Controls.Add(this.btnMoveBreakTableLeftLoad);
            this.panel9.Location = new System.Drawing.Point(347, 52);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(165, 238);
            this.panel9.TabIndex = 17;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.LightSteelBlue;
            this.label15.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(50, -8);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(69, 60);
            this.label15.TabIndex = 60;
            this.label15.Text = "Break\r\ntable";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMoveBreakTableRightUnload
            // 
            this.btnMoveBreakTableRightUnload.BackColor = System.Drawing.Color.DimGray;
            this.btnMoveBreakTableRightUnload.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnMoveBreakTableRightUnload.ForeColor = System.Drawing.Color.White;
            this.btnMoveBreakTableRightUnload.Location = new System.Drawing.Point(86, 141);
            this.btnMoveBreakTableRightUnload.Name = "btnMoveBreakTableRightUnload";
            this.btnMoveBreakTableRightUnload.Size = new System.Drawing.Size(71, 50);
            this.btnMoveBreakTableRightUnload.TabIndex = 58;
            this.btnMoveBreakTableRightUnload.Text = "Right Unload";
            this.btnMoveBreakTableRightUnload.UseVisualStyleBackColor = false;
            // 
            // btnMoveBreakTableLeftUnload
            // 
            this.btnMoveBreakTableLeftUnload.BackColor = System.Drawing.Color.DimGray;
            this.btnMoveBreakTableLeftUnload.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnMoveBreakTableLeftUnload.ForeColor = System.Drawing.Color.White;
            this.btnMoveBreakTableLeftUnload.Location = new System.Drawing.Point(9, 141);
            this.btnMoveBreakTableLeftUnload.Name = "btnMoveBreakTableLeftUnload";
            this.btnMoveBreakTableLeftUnload.Size = new System.Drawing.Size(71, 50);
            this.btnMoveBreakTableLeftUnload.TabIndex = 57;
            this.btnMoveBreakTableLeftUnload.Text = "Left Unload";
            this.btnMoveBreakTableLeftUnload.UseVisualStyleBackColor = false;
            // 
            // btnMoveBreakTableRightLoad
            // 
            this.btnMoveBreakTableRightLoad.BackColor = System.Drawing.Color.DimGray;
            this.btnMoveBreakTableRightLoad.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnMoveBreakTableRightLoad.ForeColor = System.Drawing.Color.White;
            this.btnMoveBreakTableRightLoad.Location = new System.Drawing.Point(86, 67);
            this.btnMoveBreakTableRightLoad.Name = "btnMoveBreakTableRightLoad";
            this.btnMoveBreakTableRightLoad.Size = new System.Drawing.Size(71, 50);
            this.btnMoveBreakTableRightLoad.TabIndex = 56;
            this.btnMoveBreakTableRightLoad.Text = "Right Load";
            this.btnMoveBreakTableRightLoad.UseVisualStyleBackColor = false;
            // 
            // btnMoveBreakTableLeftLoad
            // 
            this.btnMoveBreakTableLeftLoad.BackColor = System.Drawing.Color.DimGray;
            this.btnMoveBreakTableLeftLoad.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnMoveBreakTableLeftLoad.ForeColor = System.Drawing.Color.White;
            this.btnMoveBreakTableLeftLoad.Location = new System.Drawing.Point(9, 67);
            this.btnMoveBreakTableLeftLoad.Name = "btnMoveBreakTableLeftLoad";
            this.btnMoveBreakTableLeftLoad.Size = new System.Drawing.Size(71, 50);
            this.btnMoveBreakTableLeftLoad.TabIndex = 55;
            this.btnMoveBreakTableLeftLoad.Text = "  Left    Load";
            this.btnMoveBreakTableLeftLoad.UseVisualStyleBackColor = false;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.SlateGray;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.btnMoveProcessRightUnload);
            this.panel7.Controls.Add(this.btnMoveProcessLeftUnload);
            this.panel7.Controls.Add(this.btnMoveProcessRightLoad);
            this.panel7.Controls.Add(this.btnMoveProcessLeftLoad);
            this.panel7.Location = new System.Drawing.Point(6, 52);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(165, 238);
            this.panel7.TabIndex = 15;
            // 
            // btnMoveProcessRightUnload
            // 
            this.btnMoveProcessRightUnload.BackColor = System.Drawing.Color.DimGray;
            this.btnMoveProcessRightUnload.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnMoveProcessRightUnload.ForeColor = System.Drawing.Color.White;
            this.btnMoveProcessRightUnload.Location = new System.Drawing.Point(85, 141);
            this.btnMoveProcessRightUnload.Name = "btnMoveProcessRightUnload";
            this.btnMoveProcessRightUnload.Size = new System.Drawing.Size(71, 50);
            this.btnMoveProcessRightUnload.TabIndex = 54;
            this.btnMoveProcessRightUnload.Text = "Right Unload";
            this.btnMoveProcessRightUnload.UseVisualStyleBackColor = false;
            // 
            // btnMoveProcessLeftUnload
            // 
            this.btnMoveProcessLeftUnload.BackColor = System.Drawing.Color.DimGray;
            this.btnMoveProcessLeftUnload.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnMoveProcessLeftUnload.ForeColor = System.Drawing.Color.White;
            this.btnMoveProcessLeftUnload.Location = new System.Drawing.Point(8, 141);
            this.btnMoveProcessLeftUnload.Name = "btnMoveProcessLeftUnload";
            this.btnMoveProcessLeftUnload.Size = new System.Drawing.Size(71, 50);
            this.btnMoveProcessLeftUnload.TabIndex = 53;
            this.btnMoveProcessLeftUnload.Text = "Left Unload";
            this.btnMoveProcessLeftUnload.UseVisualStyleBackColor = false;
            // 
            // btnMoveProcessRightLoad
            // 
            this.btnMoveProcessRightLoad.BackColor = System.Drawing.Color.DimGray;
            this.btnMoveProcessRightLoad.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnMoveProcessRightLoad.ForeColor = System.Drawing.Color.White;
            this.btnMoveProcessRightLoad.Location = new System.Drawing.Point(85, 67);
            this.btnMoveProcessRightLoad.Name = "btnMoveProcessRightLoad";
            this.btnMoveProcessRightLoad.Size = new System.Drawing.Size(71, 50);
            this.btnMoveProcessRightLoad.TabIndex = 52;
            this.btnMoveProcessRightLoad.Text = "Right Load";
            this.btnMoveProcessRightLoad.UseVisualStyleBackColor = false;
            // 
            // btnMoveProcessLeftLoad
            // 
            this.btnMoveProcessLeftLoad.BackColor = System.Drawing.Color.DimGray;
            this.btnMoveProcessLeftLoad.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnMoveProcessLeftLoad.ForeColor = System.Drawing.Color.White;
            this.btnMoveProcessLeftLoad.Location = new System.Drawing.Point(8, 67);
            this.btnMoveProcessLeftLoad.Name = "btnMoveProcessLeftLoad";
            this.btnMoveProcessLeftLoad.Size = new System.Drawing.Size(71, 50);
            this.btnMoveProcessLeftLoad.TabIndex = 51;
            this.btnMoveProcessLeftLoad.Text = "  Left    Load";
            this.btnMoveProcessLeftLoad.UseVisualStyleBackColor = false;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Purple;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.btnMoveBreakTransferRightUnload);
            this.panel8.Controls.Add(this.label14);
            this.panel8.Controls.Add(this.btnMoveBreakTransferLeftUnload);
            this.panel8.Controls.Add(this.btnMoveBreakTransferRightLoad);
            this.panel8.Controls.Add(this.btnMoveBreakTransferLeftLoad);
            this.panel8.Location = new System.Drawing.Point(177, 52);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(165, 238);
            this.panel8.TabIndex = 16;
            // 
            // btnMoveBreakTransferRightUnload
            // 
            this.btnMoveBreakTransferRightUnload.BackColor = System.Drawing.Color.DimGray;
            this.btnMoveBreakTransferRightUnload.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnMoveBreakTransferRightUnload.ForeColor = System.Drawing.Color.White;
            this.btnMoveBreakTransferRightUnload.Location = new System.Drawing.Point(85, 141);
            this.btnMoveBreakTransferRightUnload.Name = "btnMoveBreakTransferRightUnload";
            this.btnMoveBreakTransferRightUnload.Size = new System.Drawing.Size(71, 50);
            this.btnMoveBreakTransferRightUnload.TabIndex = 58;
            this.btnMoveBreakTransferRightUnload.Text = "Right Unload";
            this.btnMoveBreakTransferRightUnload.UseVisualStyleBackColor = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Purple;
            this.label14.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(35, -6);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(91, 60);
            this.label14.TabIndex = 59;
            this.label14.Text = "Break\r\ntransfer";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMoveBreakTransferLeftUnload
            // 
            this.btnMoveBreakTransferLeftUnload.BackColor = System.Drawing.Color.DimGray;
            this.btnMoveBreakTransferLeftUnload.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnMoveBreakTransferLeftUnload.ForeColor = System.Drawing.Color.White;
            this.btnMoveBreakTransferLeftUnload.Location = new System.Drawing.Point(8, 141);
            this.btnMoveBreakTransferLeftUnload.Name = "btnMoveBreakTransferLeftUnload";
            this.btnMoveBreakTransferLeftUnload.Size = new System.Drawing.Size(71, 50);
            this.btnMoveBreakTransferLeftUnload.TabIndex = 57;
            this.btnMoveBreakTransferLeftUnload.Text = "Left Unload";
            this.btnMoveBreakTransferLeftUnload.UseVisualStyleBackColor = false;
            // 
            // btnMoveBreakTransferRightLoad
            // 
            this.btnMoveBreakTransferRightLoad.BackColor = System.Drawing.Color.DimGray;
            this.btnMoveBreakTransferRightLoad.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnMoveBreakTransferRightLoad.ForeColor = System.Drawing.Color.White;
            this.btnMoveBreakTransferRightLoad.Location = new System.Drawing.Point(85, 67);
            this.btnMoveBreakTransferRightLoad.Name = "btnMoveBreakTransferRightLoad";
            this.btnMoveBreakTransferRightLoad.Size = new System.Drawing.Size(71, 50);
            this.btnMoveBreakTransferRightLoad.TabIndex = 56;
            this.btnMoveBreakTransferRightLoad.Text = "Right Load";
            this.btnMoveBreakTransferRightLoad.UseVisualStyleBackColor = false;
            // 
            // btnMoveBreakTransferLeftLoad
            // 
            this.btnMoveBreakTransferLeftLoad.BackColor = System.Drawing.Color.DimGray;
            this.btnMoveBreakTransferLeftLoad.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnMoveBreakTransferLeftLoad.ForeColor = System.Drawing.Color.White;
            this.btnMoveBreakTransferLeftLoad.Location = new System.Drawing.Point(8, 67);
            this.btnMoveBreakTransferLeftLoad.Name = "btnMoveBreakTransferLeftLoad";
            this.btnMoveBreakTransferLeftLoad.Size = new System.Drawing.Size(71, 50);
            this.btnMoveBreakTransferLeftLoad.TabIndex = 55;
            this.btnMoveBreakTransferLeftLoad.Text = "  Left    Load";
            this.btnMoveBreakTransferLeftLoad.UseVisualStyleBackColor = false;
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.groupBox1);
            this.panel21.Location = new System.Drawing.Point(820, 511);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(520, 169);
            this.panel21.TabIndex = 62;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel27);
            this.groupBox1.Controls.Add(this.panel22);
            this.groupBox1.Controls.Add(this.btnAlignIdle);
            this.groupBox1.Controls.Add(this.btnAlignTestInsp);
            this.groupBox1.Controls.Add(this.btnAlignPreAlign);
            this.groupBox1.Controls.Add(this.txtAlignTotalInspCnt);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.txtAlignInspCount);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Location = new System.Drawing.Point(3, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(517, 166);
            this.groupBox1.TabIndex = 63;
            this.groupBox1.TabStop = false;
            // 
            // panel27
            // 
            this.panel27.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel27.Controls.Add(this.label23);
            this.panel27.ForeColor = System.Drawing.Color.White;
            this.panel27.Location = new System.Drawing.Point(1, 8);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(514, 38);
            this.panel27.TabIndex = 59;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold);
            this.label23.ForeColor = System.Drawing.Color.White;
            this.label23.Location = new System.Drawing.Point(217, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(76, 32);
            this.label23.TabIndex = 53;
            this.label23.Text = "Align";
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.btnAlignIR2);
            this.panel22.Controls.Add(this.btnAlignIR1);
            this.panel22.Controls.Add(this.btnAlignIL2);
            this.panel22.Controls.Add(this.btnAlignIL1);
            this.panel22.Location = new System.Drawing.Point(158, 96);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(200, 65);
            this.panel22.TabIndex = 78;
            // 
            // btnAlignIR2
            // 
            this.btnAlignIR2.BackColor = System.Drawing.Color.DimGray;
            this.btnAlignIR2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnAlignIR2.ForeColor = System.Drawing.Color.White;
            this.btnAlignIR2.Location = new System.Drawing.Point(156, 15);
            this.btnAlignIR2.Name = "btnAlignIR2";
            this.btnAlignIR2.Size = new System.Drawing.Size(38, 41);
            this.btnAlignIR2.TabIndex = 71;
            this.btnAlignIR2.Text = "I   R2";
            this.btnAlignIR2.UseVisualStyleBackColor = false;
            // 
            // btnAlignIR1
            // 
            this.btnAlignIR1.BackColor = System.Drawing.Color.DimGray;
            this.btnAlignIR1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnAlignIR1.ForeColor = System.Drawing.Color.White;
            this.btnAlignIR1.Location = new System.Drawing.Point(106, 15);
            this.btnAlignIR1.Name = "btnAlignIR1";
            this.btnAlignIR1.Size = new System.Drawing.Size(38, 41);
            this.btnAlignIR1.TabIndex = 70;
            this.btnAlignIR1.Text = "I   R1";
            this.btnAlignIR1.UseVisualStyleBackColor = false;
            // 
            // btnAlignIL2
            // 
            this.btnAlignIL2.BackColor = System.Drawing.Color.DimGray;
            this.btnAlignIL2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnAlignIL2.ForeColor = System.Drawing.Color.White;
            this.btnAlignIL2.Location = new System.Drawing.Point(56, 15);
            this.btnAlignIL2.Name = "btnAlignIL2";
            this.btnAlignIL2.Size = new System.Drawing.Size(38, 41);
            this.btnAlignIL2.TabIndex = 69;
            this.btnAlignIL2.Text = "I   L2";
            this.btnAlignIL2.UseVisualStyleBackColor = false;
            // 
            // btnAlignIL1
            // 
            this.btnAlignIL1.BackColor = System.Drawing.Color.DimGray;
            this.btnAlignIL1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnAlignIL1.ForeColor = System.Drawing.Color.White;
            this.btnAlignIL1.Location = new System.Drawing.Point(6, 15);
            this.btnAlignIL1.Name = "btnAlignIL1";
            this.btnAlignIL1.Size = new System.Drawing.Size(38, 41);
            this.btnAlignIL1.TabIndex = 68;
            this.btnAlignIL1.Text = "I   L1";
            this.btnAlignIL1.UseVisualStyleBackColor = false;
            // 
            // btnAlignIdle
            // 
            this.btnAlignIdle.BackColor = System.Drawing.Color.DimGray;
            this.btnAlignIdle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnAlignIdle.ForeColor = System.Drawing.Color.White;
            this.btnAlignIdle.Location = new System.Drawing.Point(11, 55);
            this.btnAlignIdle.Name = "btnAlignIdle";
            this.btnAlignIdle.Size = new System.Drawing.Size(128, 41);
            this.btnAlignIdle.TabIndex = 77;
            this.btnAlignIdle.Text = "Idle";
            this.btnAlignIdle.UseVisualStyleBackColor = false;
            // 
            // btnAlignTestInsp
            // 
            this.btnAlignTestInsp.BackColor = System.Drawing.Color.DimGray;
            this.btnAlignTestInsp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnAlignTestInsp.ForeColor = System.Drawing.Color.White;
            this.btnAlignTestInsp.Location = new System.Drawing.Point(367, 111);
            this.btnAlignTestInsp.Name = "btnAlignTestInsp";
            this.btnAlignTestInsp.Size = new System.Drawing.Size(128, 41);
            this.btnAlignTestInsp.TabIndex = 76;
            this.btnAlignTestInsp.Text = "TestInsp";
            this.btnAlignTestInsp.UseVisualStyleBackColor = false;
            // 
            // btnAlignPreAlign
            // 
            this.btnAlignPreAlign.BackColor = System.Drawing.Color.DimGray;
            this.btnAlignPreAlign.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnAlignPreAlign.ForeColor = System.Drawing.Color.White;
            this.btnAlignPreAlign.Location = new System.Drawing.Point(11, 111);
            this.btnAlignPreAlign.Name = "btnAlignPreAlign";
            this.btnAlignPreAlign.Size = new System.Drawing.Size(128, 41);
            this.btnAlignPreAlign.TabIndex = 75;
            this.btnAlignPreAlign.Text = "PreAlign";
            this.btnAlignPreAlign.UseVisualStyleBackColor = false;
            // 
            // txtAlignTotalInspCnt
            // 
            this.txtAlignTotalInspCnt.Font = new System.Drawing.Font("맑은 고딕", 15F, System.Drawing.FontStyle.Bold);
            this.txtAlignTotalInspCnt.Location = new System.Drawing.Point(450, 52);
            this.txtAlignTotalInspCnt.Name = "txtAlignTotalInspCnt";
            this.txtAlignTotalInspCnt.Size = new System.Drawing.Size(46, 34);
            this.txtAlignTotalInspCnt.TabIndex = 74;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(344, 59);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(106, 21);
            this.label18.TabIndex = 73;
            this.label18.Text = "TotalInspCnt";
            // 
            // txtAlignInspCount
            // 
            this.txtAlignInspCount.Font = new System.Drawing.Font("맑은 고딕", 15F, System.Drawing.FontStyle.Bold);
            this.txtAlignInspCount.Location = new System.Drawing.Point(254, 52);
            this.txtAlignInspCount.Name = "txtAlignInspCount";
            this.txtAlignInspCount.Size = new System.Drawing.Size(76, 34);
            this.txtAlignInspCount.TabIndex = 72;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(160, 59);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(88, 21);
            this.label16.TabIndex = 71;
            this.label16.Text = "InspCount";
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.groupBox2);
            this.panel20.Location = new System.Drawing.Point(820, 320);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(520, 185);
            this.panel20.TabIndex = 63;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.panel26);
            this.groupBox2.Controls.Add(this.txtEtcTimeShot);
            this.groupBox2.Controls.Add(this.btnEtcTimeShot);
            this.groupBox2.Controls.Add(this.groupBox9);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.btnEtcOneProcess);
            this.groupBox2.Controls.Add(this.btnEtcRepeatMove);
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(517, 179);
            this.groupBox2.TabIndex = 71;
            this.groupBox2.TabStop = false;
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel26.Controls.Add(this.label22);
            this.panel26.ForeColor = System.Drawing.Color.White;
            this.panel26.Location = new System.Drawing.Point(2, 8);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(514, 38);
            this.panel26.TabIndex = 59;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold);
            this.label22.ForeColor = System.Drawing.Color.White;
            this.label22.Location = new System.Drawing.Point(227, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(49, 32);
            this.label22.TabIndex = 53;
            this.label22.Text = "Etc";
            // 
            // txtEtcTimeShot
            // 
            this.txtEtcTimeShot.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.txtEtcTimeShot.Location = new System.Drawing.Point(149, 73);
            this.txtEtcTimeShot.Name = "txtEtcTimeShot";
            this.txtEtcTimeShot.Size = new System.Drawing.Size(76, 25);
            this.txtEtcTimeShot.TabIndex = 63;
            // 
            // btnEtcTimeShot
            // 
            this.btnEtcTimeShot.BackColor = System.Drawing.Color.DimGray;
            this.btnEtcTimeShot.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btnEtcTimeShot.ForeColor = System.Drawing.Color.White;
            this.btnEtcTimeShot.Location = new System.Drawing.Point(230, 66);
            this.btnEtcTimeShot.Name = "btnEtcTimeShot";
            this.btnEtcTimeShot.Size = new System.Drawing.Size(64, 36);
            this.btnEtcTimeShot.TabIndex = 62;
            this.btnEtcTimeShot.Text = "Shot";
            this.btnEtcTimeShot.UseVisualStyleBackColor = false;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.btnEtcShutterClose);
            this.groupBox9.Controls.Add(this.btnEtcShutterOpen);
            this.groupBox9.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox9.ForeColor = System.Drawing.Color.White;
            this.groupBox9.Location = new System.Drawing.Point(305, 47);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(206, 125);
            this.groupBox9.TabIndex = 61;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Shutter";
            // 
            // btnEtcShutterClose
            // 
            this.btnEtcShutterClose.BackColor = System.Drawing.Color.DimGray;
            this.btnEtcShutterClose.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnEtcShutterClose.ForeColor = System.Drawing.Color.White;
            this.btnEtcShutterClose.Location = new System.Drawing.Point(111, 37);
            this.btnEtcShutterClose.Name = "btnEtcShutterClose";
            this.btnEtcShutterClose.Size = new System.Drawing.Size(80, 66);
            this.btnEtcShutterClose.TabIndex = 60;
            this.btnEtcShutterClose.Text = "Shutter Close";
            this.btnEtcShutterClose.UseVisualStyleBackColor = false;
            // 
            // btnEtcShutterOpen
            // 
            this.btnEtcShutterOpen.BackColor = System.Drawing.Color.DimGray;
            this.btnEtcShutterOpen.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnEtcShutterOpen.ForeColor = System.Drawing.Color.White;
            this.btnEtcShutterOpen.Location = new System.Drawing.Point(18, 37);
            this.btnEtcShutterOpen.Name = "btnEtcShutterOpen";
            this.btnEtcShutterOpen.Size = new System.Drawing.Size(80, 66);
            this.btnEtcShutterOpen.TabIndex = 59;
            this.btnEtcShutterOpen.Text = "Shutter Open";
            this.btnEtcShutterOpen.UseVisualStyleBackColor = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(15, 72);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(107, 21);
            this.label17.TabIndex = 58;
            this.label17.Text = "Time Shot[s]";
            // 
            // btnEtcOneProcess
            // 
            this.btnEtcOneProcess.BackColor = System.Drawing.Color.DimGray;
            this.btnEtcOneProcess.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnEtcOneProcess.ForeColor = System.Drawing.Color.White;
            this.btnEtcOneProcess.Location = new System.Drawing.Point(161, 120);
            this.btnEtcOneProcess.Name = "btnEtcOneProcess";
            this.btnEtcOneProcess.Size = new System.Drawing.Size(128, 41);
            this.btnEtcOneProcess.TabIndex = 60;
            this.btnEtcOneProcess.Text = "One Process";
            this.btnEtcOneProcess.UseVisualStyleBackColor = false;
            // 
            // btnEtcRepeatMove
            // 
            this.btnEtcRepeatMove.BackColor = System.Drawing.Color.DimGray;
            this.btnEtcRepeatMove.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnEtcRepeatMove.ForeColor = System.Drawing.Color.White;
            this.btnEtcRepeatMove.Location = new System.Drawing.Point(15, 121);
            this.btnEtcRepeatMove.Name = "btnEtcRepeatMove";
            this.btnEtcRepeatMove.Size = new System.Drawing.Size(128, 41);
            this.btnEtcRepeatMove.TabIndex = 59;
            this.btnEtcRepeatMove.Text = "Repeat Move";
            this.btnEtcRepeatMove.UseVisualStyleBackColor = false;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.DimGray;
            this.btnClose.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(1207, 689);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(128, 31);
            this.btnClose.TabIndex = 70;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.panel24);
            this.groupBox4.Controls.Add(this.panel6);
            this.groupBox4.Controls.Add(this.panel17);
            this.groupBox4.Controls.Add(this.panel5);
            this.groupBox4.Controls.Add(this.panel16);
            this.groupBox4.Controls.Add(this.panel14);
            this.groupBox4.Controls.Add(this.panel15);
            this.groupBox4.Location = new System.Drawing.Point(420, 10);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(390, 686);
            this.groupBox4.TabIndex = 72;
            this.groupBox4.TabStop = false;
            // 
            // panel24
            // 
            this.panel24.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel24.Controls.Add(this.label20);
            this.panel24.ForeColor = System.Drawing.Color.White;
            this.panel24.Location = new System.Drawing.Point(1, 8);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(387, 38);
            this.panel24.TabIndex = 58;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold);
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(153, 1);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(78, 32);
            this.label20.TabIndex = 53;
            this.label20.Text = "Break";
            // 
            // FrmManager_Process
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.ClientSize = new System.Drawing.Size(1344, 729);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.panel20);
            this.Controls.Add(this.panel21);
            this.Controls.Add(this.panel19);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel1);
            this.Name = "FrmManager_Process";
            this.Text = "FrmManager_Process";
            this.panel1.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.panel19.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            this.panel25.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel21.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            this.panel22.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel26.ResumeLayout(false);
            this.panel26.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Button btnProcessVision;
        private System.Windows.Forms.Button btnProcessCurrent;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Button btnProcessBProcessR3;
        private System.Windows.Forms.Button btnProcessBProcessR1;
        private System.Windows.Forms.Button btnProcessBProcessR2;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Button btnProcessAProcessL3;
        private System.Windows.Forms.Button btnProcessAProcessL2;
        private System.Windows.Forms.Button btnProcessAProcessL1;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Button btnProcessBAlignR3;
        private System.Windows.Forms.Button btnProcessBAlignR1;
        private System.Windows.Forms.Button btnProcessBAlignR2;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Button btnProcessAAlignL3;
        private System.Windows.Forms.Button btnProcessAAlignL2;
        private System.Windows.Forms.Button btnProces_AAlignL1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnProcessBR2Blow;
        private System.Windows.Forms.Button btnProcessBR2Vac;
        private System.Windows.Forms.Button btnProcessBR1Blow;
        private System.Windows.Forms.Button btnProcessBR1Vac;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnProcessAL2Blow;
        private System.Windows.Forms.Button btnProcessAL2Vac;
        private System.Windows.Forms.Button btnProcessAL1Blow;
        private System.Windows.Forms.Button btnProcessAL1Vac;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Button btnBreakBProcessR3;
        private System.Windows.Forms.Button btnBreakBProcessR1;
        private System.Windows.Forms.Button btnBreakBProcessR2;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Button btnBreakAProcessL3;
        private System.Windows.Forms.Button btnBreakAProcessL2;
        private System.Windows.Forms.Button btnBreakAProcessL1;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Button btnBreakBAlignR3;
        private System.Windows.Forms.Button btnBreakBAlignR1;
        private System.Windows.Forms.Button btnBreakBAlignR2;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Button btnBreakAAlignL3;
        private System.Windows.Forms.Button btnBreakAAlignL2;
        private System.Windows.Forms.Button btnBreakAAlignL1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnBreakBR2Blow;
        private System.Windows.Forms.Button btnBreakBR2Vac;
        private System.Windows.Forms.Button btnBreakBR1Blow;
        private System.Windows.Forms.Button btnBreakBR1Vac;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btnBreakAL2Blow;
        private System.Windows.Forms.Button btnBreakAL2Vac;
        private System.Windows.Forms.Button btnBreakAL1Blow;
        private System.Windows.Forms.Button btnBreakAL1Vac;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnMoveBreakTableRightUnload;
        private System.Windows.Forms.Button btnMoveBreakTableLeftUnload;
        private System.Windows.Forms.Button btnMoveBreakTableRightLoad;
        private System.Windows.Forms.Button btnMoveBreakTableLeftLoad;
        private System.Windows.Forms.Button btnMoveBreakTransferRightUnload;
        private System.Windows.Forms.Button btnMoveBreakTransferLeftUnload;
        private System.Windows.Forms.Button btnMoveBreakTransferRightLoad;
        private System.Windows.Forms.Button btnMoveBreakTransferLeftLoad;
        private System.Windows.Forms.Button btnMoveProcessRightUnload;
        private System.Windows.Forms.Button btnMoveProcessLeftUnload;
        private System.Windows.Forms.Button btnMoveProcessRightLoad;
        private System.Windows.Forms.Button btnMoveProcessLeftLoad;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Button btnEtcOneProcess;
        private System.Windows.Forms.Button btnEtcRepeatMove;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TextBox txtEtcTimeShot;
        private System.Windows.Forms.Button btnEtcTimeShot;
        private System.Windows.Forms.Button btnEtcShutterClose;
        private System.Windows.Forms.Button btnEtcShutterOpen;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Button btnAlignIR2;
        private System.Windows.Forms.Button btnAlignIR1;
        private System.Windows.Forms.Button btnAlignIL2;
        private System.Windows.Forms.Button btnAlignIL1;
        private System.Windows.Forms.Button btnAlignIdle;
        private System.Windows.Forms.Button btnAlignTestInsp;
        private System.Windows.Forms.Button btnAlignPreAlign;
        private System.Windows.Forms.TextBox txtAlignTotalInspCnt;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtAlignInspCount;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Label label20;
    }
}