﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DIT.TLC.CTRL;

namespace DIT.TLC.UI
{
    public partial class Recipe_ProCondition : UserControl
    {
        public Recipe_ProCondition()
        {
            InitializeComponent();
        }
        public void FillLstProcessRecipe()
        {
            lvProcess.Items.Clear();

            foreach (ProcessRecipe precp in GG.Equip.ProcessRecipeMgr.LstProcessRecipe)
                lvProcess.Items.Add(new ListViewItem(new string[] { precp.Name}));
        }

        string recipeName;
        private void btnProcessMake_Click(object sender, EventArgs e)
        {
            ProcessRecipe precp = new ProcessRecipe();

            //recipeName = txtrecipe.Text; //= lvRecipe.SelectedItems[0].SubItems[1].Text;
            //"이름 입력 요망"
            if (string.IsNullOrEmpty(recipeName))
            {
                MessageBox.Show("레시피 이름이 없습니다.!");
                return;
            }
            GG.Equip.ProcessRecipeMgr.LstProcessRecipe.Add(precp);
            FillLstProcessRecipe();
            lvProcess.Items[lvProcess.Items.Count - 1].Selected = true;
        }
        private void btnProcessCopy_Click(object sender, EventArgs e)
        {
            if (lvProcess.SelectedItems.Count <= 0) return;
            string recipeName = lvProcess.SelectedItems[0].SubItems[0].Text;

            ProcessRecipe precp = GG.Equip.ProcessRecipeMgr.LstProcessRecipe.FirstOrDefault(f => f.Name == recipeName);

            ProcessRecipe precpCopy = (ProcessRecipe)precp.Clone();

            //"이름 입력 요망"
            precpCopy.Name = recipeName;
            GG.Equip.ProcessRecipeMgr.LstProcessRecipe.Add(precpCopy);
            FillProcessData(precpCopy);
        }

        private void btnProcessDelete_Click(object sender, EventArgs e)
        {
            string recipeName = lvBreaking.SelectedItems[0].SubItems[0].Text;
            ProcessRecipe precpCopy = GG.Equip.ProcessRecipeMgr.LstProcessRecipe.FirstOrDefault(f => f.Name == recipeName);
            GG.Equip.ProcessRecipeMgr.LstProcessRecipe.Remove(precpCopy);
        }

        public void FillLstBreakRecipe()
        {
            lvBreaking.Items.Clear();

            foreach (BreakRecipe brecp in GG.Equip.BreakRecipeMgr.LstBreakRecipe)
                lvBreaking.Items.Add(new ListViewItem(new string[] { brecp.Name }));
        }
        private void btnBreakingMake_Click(object sender, EventArgs e)
        {
            BreakRecipe brecp = new BreakRecipe();

            //recipeName = txtrecipe.Text; //= lvRecipe.SelectedItems[0].SubItems[1].Text;
            //"이름 입력 요망"
            if (string.IsNullOrEmpty(recipeName))
            {
                MessageBox.Show("레시피 이름이 없습니다.!");
                return;
            }
            GG.Equip.BreakRecipeMgr.LstBreakRecipe.Add(brecp);
            FillLstBreakRecipe();
            lvBreaking.Items[lvBreaking.Items.Count - 1].Selected = true;
        }

        private void btnBreakingCopy_Click(object sender, EventArgs e)
        {
            if (lvBreaking.SelectedItems.Count <= 0) return;
            string recipeName = lvBreaking.SelectedItems[0].SubItems[0].Text;

            BreakRecipe brecp = GG.Equip.BreakRecipeMgr.LstBreakRecipe.FirstOrDefault(f => f.Name == recipeName);

            BreakRecipe brecpCopy = (BreakRecipe)brecp.Clone();

            //"이름 입력 요망"
            brecpCopy.Name = recipeName;
            GG.Equip.BreakRecipeMgr.LstBreakRecipe.Add(brecpCopy);
            FillBreakData(brecpCopy);
        }

        private void btnBreakingDelete_Click(object sender, EventArgs e)
        {
            string recipeName = lvBreaking.SelectedItems[0].SubItems[0].Text;
            BreakRecipe brecpCopy = GG.Equip.BreakRecipeMgr.LstBreakRecipe.FirstOrDefault(f => f.Name == recipeName);
            GG.Equip.BreakRecipeMgr.LstBreakRecipe.Remove(brecpCopy);
        }
        public void FillProcessData(ProcessRecipe precp)
        {
            txtprocessDivider.Text                      = precp.Divider.ToString();
            txtprocessPower.Text                        = precp.Power.ToString();
            txtprocessPowerTatio.Text                   = precp.PowerRatio.ToString();
            txtprocessBurst.Text                        = precp.Burst.ToString();
            txtprocessError.Text                        = precp.Error.ToString();
            txtprocessFrequence.Text                    = precp.Frequence.ToString(); 
            txtprocessSpeed.Text                        = precp.Speed.ToString();
            txtprocessZpos.Text                         = precp.ZPos.ToString();
            txtprocessOverlap.Text                      = precp.Overlap.ToString();
            txtprocessSegmentValue.Text                 = precp.SegmentValue.ToString();
            txtprocessG.Text                            = precp.G.ToString();
            txtprocessScan.Text                         = precp.Scan.ToString();
            txtprocessAccDec.Text                       = precp.AccDec.ToString();
        }

        public void UpdateProcessRecipe(ProcessRecipe precp)
        {
            precp.Divider                   = double.Parse(txtprocessDivider.Text);                     
            precp.Power                     = double.Parse(txtprocessPower.Text);
            precp.PowerRatio                = double.Parse(txtprocessPowerTatio.Text);
            precp.Burst                     = double.Parse(txtprocessBurst.Text);
            precp.Error                     = double.Parse(txtprocessError.Text);
            precp.Frequence                 = double.Parse(txtprocessFrequence.Text);
            precp.Speed                     = double.Parse(txtprocessSpeed.Text);
            precp.ZPos                      = double.Parse(txtprocessZpos.Text);
            precp.Overlap                   = double.Parse(txtprocessOverlap.Text);
            precp.SegmentValue              = double.Parse(txtprocessSegmentValue.Text);
            precp.G                         = double.Parse(txtprocessG.Text);
            precp.Scan                      = double.Parse(txtprocessScan.Text);
            precp.AccDec                    = double.Parse(txtprocessAccDec.Text);
        }

        public void FillBreakData(BreakRecipe brecp)
        {
            txtbreakingXyPinCenter1.Text                      = brecp.XYPinCenter.X.ToString();
            txtbreakingXyPinCenter2.Text                      = brecp.XYPinCenter.Y.ToString();
            txtbreakingXyAlignMark11.Text                     = brecp.XYAlignMak1.X.ToString();
            txtbreakingXyAlignMark12.Text                     = brecp.XYAlignMak1.Y.ToString();
            txtbreakingXyAlignMark21.Text                     = brecp.XYAlignMak2.X.ToString();
            txtbreakingXyAlignMark22.Text                     = brecp.XYAlignMak2.Y.ToString();
            txtbreakingXyMcr1.Text                            = brecp.XYMCR.X.ToString();
            txtbreakingXyMcr2.Text                            = brecp.XYMCR.Y.ToString();
            txtbreakingPinDistanceA.Text                      = brecp.PinDistanceA.ToString();
            txtbreakingPinDistanceB.Text                      = brecp.PinDistanceB.ToString();
            txtbreakingXytBreaking11.Text                     = brecp.XYTBreaking1.Left.ToString();
            txtbreakingXytBreaking12.Text                     = brecp.XYTBreaking1.Right.ToString();
            txtbreakingXytBreaking13.Text                     = brecp.XYTBreaking1.Half.ToString();
            txtbreakingXytBreaking21.Text                     = brecp.XYTBreaking2.Left.ToString();
            txtbreakingXytBreaking22.Text                     = brecp.XYTBreaking2.Right.ToString();
            txtbreakingXytBreaking23.Text                     = brecp.XYTBreaking2.Half.ToString();
            txtbreakingXytBreaking31.Text                     = brecp.XYTBreaking3.Left.ToString();
            txtbreakingXytBreaking32.Text                     = brecp.XYTBreaking3.Right.ToString();
            txtbreakingXytBreaking33.Text                     = brecp.XYTBreaking3.Half.ToString();
            txtbreakingXytBreaking41.Text                     = brecp.XYTBreaking4.Left.ToString();
            txtbreakingXytBreaking42.Text                     = brecp.XYTBreaking4.Right.ToString();
            txtbreakingXytBreaking43.Text                     = brecp.XYTBreaking4.Half.ToString();
            txtbreakingA1XyOffset1.Text                       = brecp.A1XYOffset.X.ToString();
            txtbreakingA1XyOffset2.Text                       = brecp.A1XYOffset.Y.ToString();
            txtbreakingA2XyOffset1.Text                       = brecp.A2XYOffset.X.ToString();
            txtbreakingA2XyOffset2.Text                       = brecp.A2XYOffset.Y.ToString();
            txtbreakingB1XyOffset1.Text                       = brecp.B1XYOffset.X.ToString();
            txtbreakingB1XyOffset2.Text                       = brecp.B1XYOffset.Y.ToString();
            txtbreakingB2XyOffset1.Text                       = brecp.B2XYOffset.X.ToString();
            txtbreakingB2XyOffset2.Text                       = brecp.B2XYOffset.Y.ToString();
            txtbreakingAMcrOffsetXy1.Text                     = brecp.AMCRXYOffset.X.ToString();
            txtbreakingAMcrOffsetXy2.Text                     = brecp.AMCRXYOffset.Y.ToString();
            txtbreakingBMcrOffsetXy1.Text                     = brecp.BMCRXYOffset.X.ToString();
            txtbreakingBMcrOffsetXy2.Text                     = brecp.BMCRXYOffset.Y.ToString();
            txtbreakingFastdownPitch.Text                     = brecp.ZAxisHighSpeedDownPitch.ToString();
            txtbreakingFastdownSpeed.Text                     = brecp.ZAxisHighSpeedDownSpeed.ToString();
            txtbreakingSlowdownPitch.Text                     = brecp.ZAxisLowSpeedDownPitch.ToString();
            txtbreakingSlowdownSpeed.Text                     = brecp.ZAxisLowSpeedDownSpeed.ToString();
            //로드이재기
            txtbreakingLoadTransSequenceOffsetAY1.Text        = brecp.ASequnceOffsetY1.ToString();
            txtbreakingLoadTransSequenceOffsetAY2.Text        = brecp.ASequnceOffsetY2.ToString();
            txtbreakingLoadTransSequenceOffsetBY1.Text        = brecp.BSequnceOffsetY1.ToString();
            txtbreakingLoadTransSequenceOffsetBY2.Text        = brecp.BSequnceOffsetY2.ToString();

        }

        public void UpdateBreakRecipe(ref BreakRecipe brecp)
        {
            brecp.XYPinCenter.X                     = double.Parse(txtbreakingXyPinCenter1.Text);
            brecp.XYPinCenter.Y                     = double.Parse(txtbreakingXyPinCenter2.Text);
            brecp.XYAlignMak1.X                     = double.Parse(txtbreakingXyAlignMark11.Text);
            brecp.XYAlignMak1.Y                     = double.Parse(txtbreakingXyAlignMark12.Text);
            brecp.XYAlignMak2.X                     = double.Parse(txtbreakingXyAlignMark21.Text);
            brecp.XYAlignMak2.Y                     = double.Parse(txtbreakingXyAlignMark22.Text);
            brecp.XYMCR.X                           = double.Parse(txtbreakingXyMcr1.Text);
            brecp.XYMCR.Y                           = double.Parse(txtbreakingXyMcr2.Text);
            brecp.PinDistanceA                      = double.Parse(txtbreakingPinDistanceA.Text);
            brecp.PinDistanceB                      = double.Parse(txtbreakingPinDistanceB.Text);
            brecp.XYTBreaking1.Left                 = double.Parse(txtbreakingXytBreaking11.Text);
            brecp.XYTBreaking1.Right                = double.Parse(txtbreakingXytBreaking12.Text);
            brecp.XYTBreaking1.Half                 = double.Parse(txtbreakingXytBreaking13.Text);
            brecp.XYTBreaking2.Left                 = double.Parse(txtbreakingXytBreaking21.Text);
            brecp.XYTBreaking2.Right                = double.Parse(txtbreakingXytBreaking22.Text);
            brecp.XYTBreaking2.Half                 = double.Parse(txtbreakingXytBreaking23.Text);
            brecp.XYTBreaking3.Left                 = double.Parse(txtbreakingXytBreaking31.Text);
            brecp.XYTBreaking3.Right                = double.Parse(txtbreakingXytBreaking32.Text);
            brecp.XYTBreaking3.Half                 = double.Parse(txtbreakingXytBreaking33.Text);
            brecp.XYTBreaking4.Left                 = double.Parse(txtbreakingXytBreaking41.Text);
            brecp.XYTBreaking4.Right                = double.Parse(txtbreakingXytBreaking42.Text);
            brecp.XYTBreaking4.Half                 = double.Parse(txtbreakingXytBreaking43.Text);
            brecp.A1XYOffset.X                      = double.Parse(txtbreakingA1XyOffset1.Text);
            brecp.A1XYOffset.Y                      = double.Parse(txtbreakingA1XyOffset2.Text);
            brecp.A2XYOffset.X                      = double.Parse(txtbreakingA2XyOffset1.Text);
            brecp.A2XYOffset.Y                      = double.Parse(txtbreakingA2XyOffset2.Text);
            brecp.B1XYOffset.X                      = double.Parse(txtbreakingB1XyOffset1.Text);
            brecp.B1XYOffset.Y                      = double.Parse(txtbreakingB1XyOffset2.Text);
            brecp.B2XYOffset.X                      = double.Parse(txtbreakingB2XyOffset1.Text);
            brecp.B2XYOffset.Y                      = double.Parse(txtbreakingB2XyOffset2.Text);
            brecp.AMCRXYOffset.X                    = double.Parse(txtbreakingAMcrOffsetXy1.Text);
            brecp.AMCRXYOffset.Y                    = double.Parse(txtbreakingAMcrOffsetXy2.Text);
            brecp.BMCRXYOffset.X                    = double.Parse(txtbreakingBMcrOffsetXy1.Text);
            brecp.BMCRXYOffset.Y                    = double.Parse(txtbreakingBMcrOffsetXy2.Text);
            brecp.ZAxisHighSpeedDownPitch           = double.Parse(txtbreakingFastdownPitch.Text);
            brecp.ZAxisHighSpeedDownSpeed           = double.Parse(txtbreakingFastdownSpeed.Text);
            brecp.ZAxisLowSpeedDownPitch            = double.Parse(txtbreakingSlowdownPitch.Text);
            brecp.ZAxisLowSpeedDownSpeed            = double.Parse(txtbreakingSlowdownSpeed.Text);
            //로드이재기                             
            brecp.ASequnceOffsetY1                  = double.Parse(txtbreakingLoadTransSequenceOffsetAY1.Text); 
            brecp.ASequnceOffsetY2                  = double.Parse(txtbreakingLoadTransSequenceOffsetAY2.Text); 
            brecp.BSequnceOffsetY1                  = double.Parse(txtbreakingLoadTransSequenceOffsetBY1.Text);
            brecp.BSequnceOffsetY2                  = double.Parse(txtbreakingLoadTransSequenceOffsetBY2.Text); 
        }

        public void FillCommonData(BreakZigRecipe bzrecp)
        {
            txtbreakingZigX.Text                             = bzrecp.BreakingZigXY.X.ToString();             
            txtbreakingZigY.Text                             = bzrecp.BreakingZigXY.Y.ToString();
            txtbreakingZigThickness.Text                     = bzrecp.BreakingZigThickness.ToString();
            txtbreakingZigCamToPin.Text                      = bzrecp.BreakingZigCamToPin.ToString();
                                               
            txtdefaultLaserOntime.Text                       = bzrecp.BreakingOnTime.ToString();
            txtdefaultLaserCstPitchNormal.Text               = bzrecp.BreakingCassettePitchNormal.ToString();
            txtdefaultLaserCstPitchReverse.Text              = bzrecp.BreakingCassettePitchReverse.ToString();
                                                
            txtdefaultProcessJumpSpeed.Text                  = bzrecp.BreakingJumpSpeed.ToString();
            txtdefaultProcessDefaultZ.Text                   = bzrecp.BreakingDefaultZ.ToString();
            txtdefaultProcessAlignMatch.Text                 = bzrecp.BreakinAlignMatch.ToString();
            txtdefaultProcessAlignAngle.Text                 = bzrecp.BreakinPreAlignErrorAng.ToString();
            txtdefaultProcessBreakErrorY.Text                = bzrecp.BreakinErrorY.ToString();
            txtdefaultProcessAlignDistance.Text              = bzrecp.BreakinAlignDistance.ToString();
            txtdefaultProcessPrealignErrorX.Text             = bzrecp.BreakinPreAlignErrorX.ToString();
            txtdefaultProcessPrealignErrorAngle.Text         = bzrecp.BreakinPreAlignAngle.ToString();
            txtdefaultProcessPrealignErrorY.Text             = bzrecp.BreakinPreAlignErrorY.ToString();
        }                                                     
                                            
        public void UpdateCommonData(ref BreakZigRecipe bzrecp)
        {
            bzrecp.BreakingZigXY.X = double.Parse(txtbreakingZigX.Text);
            bzrecp.BreakingZigXY.Y = double.Parse(txtbreakingZigY.Text);
            bzrecp.BreakingZigThickness = double.Parse(txtbreakingZigThickness.Text);
            bzrecp.BreakingZigCamToPin = double.Parse(txtbreakingZigCamToPin.Text);

            bzrecp.BreakingOnTime = double.Parse(txtdefaultLaserOntime.Text);
            bzrecp.BreakingCassettePitchNormal = double.Parse(txtdefaultLaserCstPitchNormal.Text);
            bzrecp.BreakingCassettePitchReverse = double.Parse(txtdefaultLaserCstPitchReverse.Text);

            bzrecp.BreakingJumpSpeed = double.Parse(txtdefaultProcessJumpSpeed.Text);
            bzrecp.BreakingDefaultZ = double.Parse(txtdefaultProcessDefaultZ.Text);
            bzrecp.BreakinAlignMatch = double.Parse(txtdefaultProcessAlignMatch.Text);
            bzrecp.BreakinPreAlignErrorAng = double.Parse(txtdefaultProcessAlignAngle.Text);
            bzrecp.BreakinErrorY = double.Parse(txtdefaultProcessBreakErrorY.Text);
            bzrecp.BreakinAlignDistance = double.Parse(txtdefaultProcessAlignDistance.Text);
            bzrecp.BreakinPreAlignErrorX = double.Parse(txtdefaultProcessPrealignErrorX.Text);
            bzrecp.BreakinPreAlignAngle = double.Parse(txtdefaultProcessPrealignErrorAngle.Text);
            bzrecp.BreakinPreAlignErrorY = double.Parse(txtdefaultProcessPrealignErrorY.Text);
        }


    }
}
