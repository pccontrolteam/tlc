﻿namespace DIT.TLC.UI
{
    partial class Recipe_Recipe
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.gxtdxfinfo = new System.Windows.Forms.GroupBox();
            this.gxtlayerinfo = new System.Windows.Forms.GroupBox();
            this.btnDxfPathLoad = new System.Windows.Forms.Button();
            this.lb_ta_thickness = new System.Windows.Forms.Label();
            this.txttThickness = new System.Windows.Forms.TextBox();
            this.comxtcellType = new System.Windows.Forms.ComboBox();
            this.lb_cell_type = new System.Windows.Forms.Label();
            this.comxtprocessParam = new System.Windows.Forms.ComboBox();
            this.lb_process_param = new System.Windows.Forms.Label();
            this.comxtguideLayer = new System.Windows.Forms.ComboBox();
            this.lb_guide_layer = new System.Windows.Forms.Label();
            this.comxtprocessLayer = new System.Windows.Forms.ComboBox();
            this.lb_process_layer = new System.Windows.Forms.Label();
            this.comxtalignmentLayer = new System.Windows.Forms.ComboBox();
            this.lb_alignment_layer = new System.Windows.Forms.Label();
            this.txtdxfpath = new System.Windows.Forms.TextBox();
            this.lb_dxfpath = new System.Windows.Forms.Label();
            this.gxtitem = new System.Windows.Forms.GroupBox();
            this.btnZMatrix = new System.Windows.Forms.Button();
            this.btnAlign = new System.Windows.Forms.Button();
            this.btnRecipeLoad = new System.Windows.Forms.Button();
            this.btnBreakRight2Move = new System.Windows.Forms.Button();
            this.btnBreakRight2Read = new System.Windows.Forms.Button();
            this.btnBreakRight2Calc = new System.Windows.Forms.Button();
            this.btnBreakLeft2Move = new System.Windows.Forms.Button();
            this.btnTable2AlignMark2Move = new System.Windows.Forms.Button();
            this.btnTable1AlignMark2Move = new System.Windows.Forms.Button();
            this.btnBreakLeft2Read = new System.Windows.Forms.Button();
            this.btnTable2AlignMark2Read = new System.Windows.Forms.Button();
            this.btnTable1AlignMark2Read = new System.Windows.Forms.Button();
            this.btnBreakLeft2Calc = new System.Windows.Forms.Button();
            this.btnTable2AlignMark2Calc = new System.Windows.Forms.Button();
            this.btnTable1AlignMark2Calc = new System.Windows.Forms.Button();
            this.btnBreakRightYMove = new System.Windows.Forms.Button();
            this.btnBreakRight1Move = new System.Windows.Forms.Button();
            this.btnBreakLeftYMove = new System.Windows.Forms.Button();
            this.btnBreakLeft1Move = new System.Windows.Forms.Button();
            this.btnTable2Alignmark1Move = new System.Windows.Forms.Button();
            this.btnTable1Alignmark1Move = new System.Windows.Forms.Button();
            this.btnBreakRightYRead = new System.Windows.Forms.Button();
            this.btnBreakRight1Read = new System.Windows.Forms.Button();
            this.btnBreakLeftYRead = new System.Windows.Forms.Button();
            this.btnBreakLeft1Read = new System.Windows.Forms.Button();
            this.btnTable2Alignmark1Read = new System.Windows.Forms.Button();
            this.btnTable1Alignmark1Read = new System.Windows.Forms.Button();
            this.btnBreakRightYCalc = new System.Windows.Forms.Button();
            this.btnBreakRight1Calc = new System.Windows.Forms.Button();
            this.btnBreakLeftYCalc = new System.Windows.Forms.Button();
            this.btnBreakLeft1Calc = new System.Windows.Forms.Button();
            this.btnTable2Alignmark1Calc = new System.Windows.Forms.Button();
            this.btnTable1Alignmark1Calc = new System.Windows.Forms.Button();
            this.txtcellSize2 = new System.Windows.Forms.TextBox();
            this.txtcellSize1 = new System.Windows.Forms.TextBox();
            this.lb_cell_size = new System.Windows.Forms.Label();
            this.lb_a_matrix = new System.Windows.Forms.Label();
            this.txtbreakRightY = new System.Windows.Forms.TextBox();
            this.txtbreakLeftY = new System.Windows.Forms.TextBox();
            this.lb_break_right_y = new System.Windows.Forms.Label();
            this.lb_break_left_y = new System.Windows.Forms.Label();
            this.txtbreakRight22 = new System.Windows.Forms.TextBox();
            this.txtbreakLeft22 = new System.Windows.Forms.TextBox();
            this.txtbreakRight21 = new System.Windows.Forms.TextBox();
            this.lb_break_right_2 = new System.Windows.Forms.Label();
            this.txtbreakLeft21 = new System.Windows.Forms.TextBox();
            this.txtbreakRight12 = new System.Windows.Forms.TextBox();
            this.txtbreakRight11 = new System.Windows.Forms.TextBox();
            this.lb_break_left_2 = new System.Windows.Forms.Label();
            this.lb_break_right_1 = new System.Windows.Forms.Label();
            this.txtbreakLeft12 = new System.Windows.Forms.TextBox();
            this.txtbreakLeft11 = new System.Windows.Forms.TextBox();
            this.lb_break_left_1 = new System.Windows.Forms.Label();
            this.txttable2Alignmark22 = new System.Windows.Forms.TextBox();
            this.txttable2Alignmark21 = new System.Windows.Forms.TextBox();
            this.lb_table2_alignmark2 = new System.Windows.Forms.Label();
            this.txttable2Alignmark12 = new System.Windows.Forms.TextBox();
            this.txttable2Alignmark11 = new System.Windows.Forms.TextBox();
            this.lb_table2_alignmark1 = new System.Windows.Forms.Label();
            this.txttable1Alignmark22 = new System.Windows.Forms.TextBox();
            this.txttable1Alignmark21 = new System.Windows.Forms.TextBox();
            this.txttable1Alignmark12 = new System.Windows.Forms.TextBox();
            this.lb_table1_alignmark2 = new System.Windows.Forms.Label();
            this.txttable1Alignmark11 = new System.Windows.Forms.TextBox();
            this.lb_table1_alignmark1 = new System.Windows.Forms.Label();
            this.txtrecipePath = new System.Windows.Forms.TextBox();
            this.lb_recipe = new System.Windows.Forms.Label();
            this.gxty_offset = new System.Windows.Forms.GroupBox();
            this.txtyOffsetBreak = new System.Windows.Forms.TextBox();
            this.lb_y_offset_break = new System.Windows.Forms.Label();
            this.txtyOffsetPre = new System.Windows.Forms.TextBox();
            this.lb_y_offset_pre = new System.Windows.Forms.Label();
            this.gxtlds_break = new System.Windows.Forms.GroupBox();
            this.btnLDSBreak4Move = new System.Windows.Forms.Button();
            this.txtldsBreak42 = new System.Windows.Forms.TextBox();
            this.txtldsBreak41 = new System.Windows.Forms.TextBox();
            this.btnLDSBreak1Move = new System.Windows.Forms.Button();
            this.btnLDSBreak3Move = new System.Windows.Forms.Button();
            this.txtldsBreak32 = new System.Windows.Forms.TextBox();
            this.txtldsBreak31 = new System.Windows.Forms.TextBox();
            this.btnLDSBreak1Read = new System.Windows.Forms.Button();
            this.txtldsBreak22 = new System.Windows.Forms.TextBox();
            this.txtldsBreak21 = new System.Windows.Forms.TextBox();
            this.btnLDSBreak2Move = new System.Windows.Forms.Button();
            this.txtldsBreak12 = new System.Windows.Forms.TextBox();
            this.lb_lds_break4 = new System.Windows.Forms.Label();
            this.btnLDSBreak2Read = new System.Windows.Forms.Button();
            this.lb_lds_break3 = new System.Windows.Forms.Label();
            this.lb_lds_break2 = new System.Windows.Forms.Label();
            this.btnLDSBreak3Read = new System.Windows.Forms.Button();
            this.txtldsBreak11 = new System.Windows.Forms.TextBox();
            this.lb_lds_break1 = new System.Windows.Forms.Label();
            this.btnLDSBreak4Read = new System.Windows.Forms.Button();
            this.panel19 = new System.Windows.Forms.Panel();
            this.btnRecipeCimDelete = new System.Windows.Forms.Button();
            this.btnRecipeCimApply = new System.Windows.Forms.Button();
            this.btnRecipeDelete = new System.Windows.Forms.Button();
            this.btnRecipeApply = new System.Windows.Forms.Button();
            this.btnRecipeSave = new System.Windows.Forms.Button();
            this.btnRecipeCopy = new System.Windows.Forms.Button();
            this.btnRecipeMake = new System.Windows.Forms.Button();
            this.gxtlds_process = new System.Windows.Forms.GroupBox();
            this.btnLDSProcess4Move = new System.Windows.Forms.Button();
            this.txtldsProcess42 = new System.Windows.Forms.TextBox();
            this.btnLDSProcess3Move = new System.Windows.Forms.Button();
            this.txtldsProcess41 = new System.Windows.Forms.TextBox();
            this.btnLDSProcess2Move = new System.Windows.Forms.Button();
            this.txtldsProcess32 = new System.Windows.Forms.TextBox();
            this.btnLDSProcess1Move = new System.Windows.Forms.Button();
            this.txtldsProcess31 = new System.Windows.Forms.TextBox();
            this.btnLDSProcess4Read = new System.Windows.Forms.Button();
            this.txtldsProcess22 = new System.Windows.Forms.TextBox();
            this.btnLDSProcess3Read = new System.Windows.Forms.Button();
            this.txtldsProcess21 = new System.Windows.Forms.TextBox();
            this.btnLDSProcess2Read = new System.Windows.Forms.Button();
            this.txtldsProcess12 = new System.Windows.Forms.TextBox();
            this.btnLDSProcess1Read = new System.Windows.Forms.Button();
            this.lb_lds_process4 = new System.Windows.Forms.Label();
            this.lb_lds_process3 = new System.Windows.Forms.Label();
            this.lb_lds_process2 = new System.Windows.Forms.Label();
            this.txtldsProcess11 = new System.Windows.Forms.TextBox();
            this.lb_lds_process1 = new System.Windows.Forms.Label();
            this.gxtpicker = new System.Windows.Forms.GroupBox();
            this.btnPickerCCW901 = new System.Windows.Forms.Button();
            this.btnPickerNormal1 = new System.Windows.Forms.Button();
            this.btnPickerCCW902 = new System.Windows.Forms.Button();
            this.btnPickerCWW902 = new System.Windows.Forms.Button();
            this.btnPickerCW1801 = new System.Windows.Forms.Button();
            this.btnPickerCW901 = new System.Windows.Forms.Button();
            this.btnPickerCw1802 = new System.Windows.Forms.Button();
            this.btnPickerNormal2 = new System.Windows.Forms.Button();
            this.gxtcassette = new System.Windows.Forms.GroupBox();
            this.btnCassette4 = new System.Windows.Forms.Button();
            this.btnCassette1 = new System.Windows.Forms.Button();
            this.btnCassette2 = new System.Windows.Forms.Button();
            this.btnCassetteReverse = new System.Windows.Forms.Button();
            this.btnCassetteNormal = new System.Windows.Forms.Button();
            this.txtcellCount = new System.Windows.Forms.TextBox();
            this.lb_cell_count = new System.Windows.Forms.Label();
            this.txtmaxHeightCount = new System.Windows.Forms.TextBox();
            this.lb_max_height_count = new System.Windows.Forms.Label();
            this.txtheight = new System.Windows.Forms.TextBox();
            this.lb_height = new System.Windows.Forms.Label();
            this.txtwidth = new System.Windows.Forms.TextBox();
            this.lb_width = new System.Windows.Forms.Label();
            this.listView2 = new System.Windows.Forms.ListView();
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvRecipe = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.txtrecipe = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.gxtlayerinfo.SuspendLayout();
            this.gxtitem.SuspendLayout();
            this.gxty_offset.SuspendLayout();
            this.gxtlds_break.SuspendLayout();
            this.panel19.SuspendLayout();
            this.gxtlds_process.SuspendLayout();
            this.gxtpicker.SuspendLayout();
            this.gxtcassette.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.Color.DimGray;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.splitContainer1.Panel1.Controls.Add(this.gxtdxfinfo);
            this.splitContainer1.Panel1.Controls.Add(this.gxtlayerinfo);
            this.splitContainer1.Panel1.Controls.Add(this.gxtitem);
            this.splitContainer1.Panel1.Controls.Add(this.gxty_offset);
            this.splitContainer1.Panel1.Controls.Add(this.gxtlds_break);
            this.splitContainer1.Panel1.Controls.Add(this.panel19);
            this.splitContainer1.Panel1.Controls.Add(this.gxtlds_process);
            this.splitContainer1.Panel1.Controls.Add(this.gxtpicker);
            this.splitContainer1.Panel1.Controls.Add(this.gxtcassette);
            this.splitContainer1.Panel1.Controls.Add(this.listView2);
            this.splitContainer1.Panel1.Controls.Add(this.lvRecipe);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.DarkSlateGray;
            this.splitContainer1.Panel2Collapsed = true;
            this.splitContainer1.Size = new System.Drawing.Size(1740, 860);
            this.splitContainer1.SplitterDistance = 835;
            this.splitContainer1.TabIndex = 0;
            // 
            // gxtdxfinfo
            // 
            this.gxtdxfinfo.BackColor = System.Drawing.Color.Transparent;
            this.gxtdxfinfo.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.gxtdxfinfo.ForeColor = System.Drawing.Color.White;
            this.gxtdxfinfo.Location = new System.Drawing.Point(284, 571);
            this.gxtdxfinfo.Name = "gxtdxfinfo";
            this.gxtdxfinfo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gxtdxfinfo.Size = new System.Drawing.Size(1104, 209);
            this.gxtdxfinfo.TabIndex = 69;
            this.gxtdxfinfo.TabStop = false;
            this.gxtdxfinfo.Text = "DxfInfo";
            // 
            // gxtlayerinfo
            // 
            this.gxtlayerinfo.BackColor = System.Drawing.Color.Transparent;
            this.gxtlayerinfo.Controls.Add(this.btnDxfPathLoad);
            this.gxtlayerinfo.Controls.Add(this.lb_ta_thickness);
            this.gxtlayerinfo.Controls.Add(this.txttThickness);
            this.gxtlayerinfo.Controls.Add(this.comxtcellType);
            this.gxtlayerinfo.Controls.Add(this.lb_cell_type);
            this.gxtlayerinfo.Controls.Add(this.comxtprocessParam);
            this.gxtlayerinfo.Controls.Add(this.lb_process_param);
            this.gxtlayerinfo.Controls.Add(this.comxtguideLayer);
            this.gxtlayerinfo.Controls.Add(this.lb_guide_layer);
            this.gxtlayerinfo.Controls.Add(this.comxtprocessLayer);
            this.gxtlayerinfo.Controls.Add(this.lb_process_layer);
            this.gxtlayerinfo.Controls.Add(this.comxtalignmentLayer);
            this.gxtlayerinfo.Controls.Add(this.lb_alignment_layer);
            this.gxtlayerinfo.Controls.Add(this.txtdxfpath);
            this.gxtlayerinfo.Controls.Add(this.lb_dxfpath);
            this.gxtlayerinfo.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.gxtlayerinfo.ForeColor = System.Drawing.Color.White;
            this.gxtlayerinfo.Location = new System.Drawing.Point(284, 358);
            this.gxtlayerinfo.Name = "gxtlayerinfo";
            this.gxtlayerinfo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gxtlayerinfo.Size = new System.Drawing.Size(1104, 170);
            this.gxtlayerinfo.TabIndex = 68;
            this.gxtlayerinfo.TabStop = false;
            this.gxtlayerinfo.Text = "LayerInfo";
            // 
            // btnDxfPathLoad
            // 
            this.btnDxfPathLoad.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnDxfPathLoad.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btnDxfPathLoad.ForeColor = System.Drawing.Color.White;
            this.btnDxfPathLoad.Location = new System.Drawing.Point(825, 25);
            this.btnDxfPathLoad.Name = "btnDxfPathLoad";
            this.btnDxfPathLoad.Size = new System.Drawing.Size(273, 26);
            this.btnDxfPathLoad.TabIndex = 168;
            this.btnDxfPathLoad.Text = "불러오기";
            this.btnDxfPathLoad.UseVisualStyleBackColor = false;
            // 
            // lb_ta_thickness
            // 
            this.lb_ta_thickness.AutoSize = true;
            this.lb_ta_thickness.Font = new System.Drawing.Font("Malgun Gothic", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_ta_thickness.Location = new System.Drawing.Point(6, 129);
            this.lb_ta_thickness.Name = "lb_ta_thickness";
            this.lb_ta_thickness.Size = new System.Drawing.Size(113, 15);
            this.lb_ta_thickness.TabIndex = 75;
            this.lb_ta_thickness.Text = "TA/Thickness[mm]";
            // 
            // txttThickness
            // 
            this.txttThickness.Location = new System.Drawing.Point(138, 123);
            this.txttThickness.Name = "txttThickness";
            this.txttThickness.Size = new System.Drawing.Size(121, 29);
            this.txttThickness.TabIndex = 65;
            // 
            // comxtcellType
            // 
            this.comxtcellType.FormattingEnabled = true;
            this.comxtcellType.Location = new System.Drawing.Point(377, 90);
            this.comxtcellType.Name = "comxtcellType";
            this.comxtcellType.Size = new System.Drawing.Size(121, 29);
            this.comxtcellType.TabIndex = 74;
            // 
            // lb_cell_type
            // 
            this.lb_cell_type.AutoSize = true;
            this.lb_cell_type.Font = new System.Drawing.Font("Malgun Gothic", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_cell_type.Location = new System.Drawing.Point(273, 97);
            this.lb_cell_type.Name = "lb_cell_type";
            this.lb_cell_type.Size = new System.Drawing.Size(60, 15);
            this.lb_cell_type.TabIndex = 73;
            this.lb_cell_type.Text = "Cell Type";
            // 
            // comxtprocessParam
            // 
            this.comxtprocessParam.FormattingEnabled = true;
            this.comxtprocessParam.Location = new System.Drawing.Point(138, 90);
            this.comxtprocessParam.Name = "comxtprocessParam";
            this.comxtprocessParam.Size = new System.Drawing.Size(121, 29);
            this.comxtprocessParam.TabIndex = 72;
            // 
            // lb_process_param
            // 
            this.lb_process_param.AutoSize = true;
            this.lb_process_param.Font = new System.Drawing.Font("Malgun Gothic", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_process_param.Location = new System.Drawing.Point(6, 97);
            this.lb_process_param.Name = "lb_process_param";
            this.lb_process_param.Size = new System.Drawing.Size(92, 15);
            this.lb_process_param.TabIndex = 71;
            this.lb_process_param.Text = "Process Param";
            // 
            // comxtguideLayer
            // 
            this.comxtguideLayer.FormattingEnabled = true;
            this.comxtguideLayer.Items.AddRange(new object[] {
            "guide"});
            this.comxtguideLayer.Location = new System.Drawing.Point(613, 57);
            this.comxtguideLayer.Name = "comxtguideLayer";
            this.comxtguideLayer.Size = new System.Drawing.Size(121, 29);
            this.comxtguideLayer.TabIndex = 70;
            // 
            // lb_guide_layer
            // 
            this.lb_guide_layer.AutoSize = true;
            this.lb_guide_layer.Font = new System.Drawing.Font("Malgun Gothic", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_guide_layer.Location = new System.Drawing.Point(523, 64);
            this.lb_guide_layer.Name = "lb_guide_layer";
            this.lb_guide_layer.Size = new System.Drawing.Size(76, 15);
            this.lb_guide_layer.TabIndex = 69;
            this.lb_guide_layer.Text = "Guide Layer";
            // 
            // comxtprocessLayer
            // 
            this.comxtprocessLayer.FormattingEnabled = true;
            this.comxtprocessLayer.Items.AddRange(new object[] {
            "CUT"});
            this.comxtprocessLayer.Location = new System.Drawing.Point(377, 57);
            this.comxtprocessLayer.Name = "comxtprocessLayer";
            this.comxtprocessLayer.Size = new System.Drawing.Size(121, 29);
            this.comxtprocessLayer.TabIndex = 68;
            // 
            // lb_process_layer
            // 
            this.lb_process_layer.AutoSize = true;
            this.lb_process_layer.Font = new System.Drawing.Font("Malgun Gothic", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_process_layer.Location = new System.Drawing.Point(273, 64);
            this.lb_process_layer.Name = "lb_process_layer";
            this.lb_process_layer.Size = new System.Drawing.Size(86, 15);
            this.lb_process_layer.TabIndex = 67;
            this.lb_process_layer.Text = "Process Layer";
            // 
            // comxtalignmentLayer
            // 
            this.comxtalignmentLayer.FormattingEnabled = true;
            this.comxtalignmentLayer.Items.AddRange(new object[] {
            "align"});
            this.comxtalignmentLayer.Location = new System.Drawing.Point(138, 57);
            this.comxtalignmentLayer.Name = "comxtalignmentLayer";
            this.comxtalignmentLayer.Size = new System.Drawing.Size(121, 29);
            this.comxtalignmentLayer.TabIndex = 66;
            // 
            // lb_alignment_layer
            // 
            this.lb_alignment_layer.AutoSize = true;
            this.lb_alignment_layer.Font = new System.Drawing.Font("Malgun Gothic", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_alignment_layer.Location = new System.Drawing.Point(6, 64);
            this.lb_alignment_layer.Name = "lb_alignment_layer";
            this.lb_alignment_layer.Size = new System.Drawing.Size(102, 15);
            this.lb_alignment_layer.TabIndex = 65;
            this.lb_alignment_layer.Text = "AlignMent Layer";
            // 
            // txtdxfpath
            // 
            this.txtdxfpath.Location = new System.Drawing.Point(138, 25);
            this.txtdxfpath.Name = "txtdxfpath";
            this.txtdxfpath.ReadOnly = true;
            this.txtdxfpath.Size = new System.Drawing.Size(681, 29);
            this.txtdxfpath.TabIndex = 5;
            // 
            // lb_dxfpath
            // 
            this.lb_dxfpath.AutoSize = true;
            this.lb_dxfpath.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_dxfpath.Location = new System.Drawing.Point(6, 29);
            this.lb_dxfpath.Name = "lb_dxfpath";
            this.lb_dxfpath.Size = new System.Drawing.Size(66, 20);
            this.lb_dxfpath.TabIndex = 4;
            this.lb_dxfpath.Text = "DxfPath";
            // 
            // gxtitem
            // 
            this.gxtitem.BackColor = System.Drawing.Color.Transparent;
            this.gxtitem.Controls.Add(this.txtrecipe);
            this.gxtitem.Controls.Add(this.btnZMatrix);
            this.gxtitem.Controls.Add(this.btnAlign);
            this.gxtitem.Controls.Add(this.btnRecipeLoad);
            this.gxtitem.Controls.Add(this.btnBreakRight2Move);
            this.gxtitem.Controls.Add(this.btnBreakRight2Read);
            this.gxtitem.Controls.Add(this.btnBreakRight2Calc);
            this.gxtitem.Controls.Add(this.btnBreakLeft2Move);
            this.gxtitem.Controls.Add(this.btnTable2AlignMark2Move);
            this.gxtitem.Controls.Add(this.btnTable1AlignMark2Move);
            this.gxtitem.Controls.Add(this.btnBreakLeft2Read);
            this.gxtitem.Controls.Add(this.btnTable2AlignMark2Read);
            this.gxtitem.Controls.Add(this.btnTable1AlignMark2Read);
            this.gxtitem.Controls.Add(this.btnBreakLeft2Calc);
            this.gxtitem.Controls.Add(this.btnTable2AlignMark2Calc);
            this.gxtitem.Controls.Add(this.btnTable1AlignMark2Calc);
            this.gxtitem.Controls.Add(this.btnBreakRightYMove);
            this.gxtitem.Controls.Add(this.btnBreakRight1Move);
            this.gxtitem.Controls.Add(this.btnBreakLeftYMove);
            this.gxtitem.Controls.Add(this.btnBreakLeft1Move);
            this.gxtitem.Controls.Add(this.btnTable2Alignmark1Move);
            this.gxtitem.Controls.Add(this.btnTable1Alignmark1Move);
            this.gxtitem.Controls.Add(this.btnBreakRightYRead);
            this.gxtitem.Controls.Add(this.btnBreakRight1Read);
            this.gxtitem.Controls.Add(this.btnBreakLeftYRead);
            this.gxtitem.Controls.Add(this.btnBreakLeft1Read);
            this.gxtitem.Controls.Add(this.btnTable2Alignmark1Read);
            this.gxtitem.Controls.Add(this.btnTable1Alignmark1Read);
            this.gxtitem.Controls.Add(this.btnBreakRightYCalc);
            this.gxtitem.Controls.Add(this.btnBreakRight1Calc);
            this.gxtitem.Controls.Add(this.btnBreakLeftYCalc);
            this.gxtitem.Controls.Add(this.btnBreakLeft1Calc);
            this.gxtitem.Controls.Add(this.btnTable2Alignmark1Calc);
            this.gxtitem.Controls.Add(this.btnTable1Alignmark1Calc);
            this.gxtitem.Controls.Add(this.txtcellSize2);
            this.gxtitem.Controls.Add(this.txtcellSize1);
            this.gxtitem.Controls.Add(this.lb_cell_size);
            this.gxtitem.Controls.Add(this.lb_a_matrix);
            this.gxtitem.Controls.Add(this.txtbreakRightY);
            this.gxtitem.Controls.Add(this.txtbreakLeftY);
            this.gxtitem.Controls.Add(this.lb_break_right_y);
            this.gxtitem.Controls.Add(this.lb_break_left_y);
            this.gxtitem.Controls.Add(this.txtbreakRight22);
            this.gxtitem.Controls.Add(this.txtbreakLeft22);
            this.gxtitem.Controls.Add(this.txtbreakRight21);
            this.gxtitem.Controls.Add(this.lb_break_right_2);
            this.gxtitem.Controls.Add(this.txtbreakLeft21);
            this.gxtitem.Controls.Add(this.txtbreakRight12);
            this.gxtitem.Controls.Add(this.txtbreakRight11);
            this.gxtitem.Controls.Add(this.lb_break_left_2);
            this.gxtitem.Controls.Add(this.lb_break_right_1);
            this.gxtitem.Controls.Add(this.txtbreakLeft12);
            this.gxtitem.Controls.Add(this.txtbreakLeft11);
            this.gxtitem.Controls.Add(this.lb_break_left_1);
            this.gxtitem.Controls.Add(this.txttable2Alignmark22);
            this.gxtitem.Controls.Add(this.txttable2Alignmark21);
            this.gxtitem.Controls.Add(this.lb_table2_alignmark2);
            this.gxtitem.Controls.Add(this.txttable2Alignmark12);
            this.gxtitem.Controls.Add(this.txttable2Alignmark11);
            this.gxtitem.Controls.Add(this.lb_table2_alignmark1);
            this.gxtitem.Controls.Add(this.txttable1Alignmark22);
            this.gxtitem.Controls.Add(this.txttable1Alignmark21);
            this.gxtitem.Controls.Add(this.txttable1Alignmark12);
            this.gxtitem.Controls.Add(this.lb_table1_alignmark2);
            this.gxtitem.Controls.Add(this.txttable1Alignmark11);
            this.gxtitem.Controls.Add(this.lb_table1_alignmark1);
            this.gxtitem.Controls.Add(this.txtrecipePath);
            this.gxtitem.Controls.Add(this.lb_recipe);
            this.gxtitem.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtitem.ForeColor = System.Drawing.Color.White;
            this.gxtitem.Location = new System.Drawing.Point(284, 3);
            this.gxtitem.Name = "gxtitem";
            this.gxtitem.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gxtitem.Size = new System.Drawing.Size(1104, 346);
            this.gxtitem.TabIndex = 67;
            this.gxtitem.TabStop = false;
            this.gxtitem.Text = "Item";
            // 
            // btnZMatrix
            // 
            this.btnZMatrix.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnZMatrix.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnZMatrix.ForeColor = System.Drawing.Color.White;
            this.btnZMatrix.Location = new System.Drawing.Point(155, 309);
            this.btnZMatrix.Name = "btnZMatrix";
            this.btnZMatrix.Size = new System.Drawing.Size(73, 26);
            this.btnZMatrix.TabIndex = 171;
            this.btnZMatrix.Text = "Z-Matrix";
            this.btnZMatrix.UseVisualStyleBackColor = false;
            // 
            // btnAlign
            // 
            this.btnAlign.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnAlign.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btnAlign.ForeColor = System.Drawing.Color.White;
            this.btnAlign.Location = new System.Drawing.Point(717, 56);
            this.btnAlign.Name = "btnAlign";
            this.btnAlign.Size = new System.Drawing.Size(102, 26);
            this.btnAlign.TabIndex = 170;
            this.btnAlign.Text = "Align";
            this.btnAlign.UseVisualStyleBackColor = false;
            // 
            // btnRecipeLoad
            // 
            this.btnRecipeLoad.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnRecipeLoad.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btnRecipeLoad.ForeColor = System.Drawing.Color.White;
            this.btnRecipeLoad.Location = new System.Drawing.Point(603, 56);
            this.btnRecipeLoad.Name = "btnRecipeLoad";
            this.btnRecipeLoad.Size = new System.Drawing.Size(102, 26);
            this.btnRecipeLoad.TabIndex = 169;
            this.btnRecipeLoad.Text = "불러오기";
            this.btnRecipeLoad.UseVisualStyleBackColor = false;
            this.btnRecipeLoad.Click += new System.EventHandler(this.btnRecipeLoad_Click);
            // 
            // btnBreakRight2Move
            // 
            this.btnBreakRight2Move.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnBreakRight2Move.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakRight2Move.ForeColor = System.Drawing.Color.White;
            this.btnBreakRight2Move.Location = new System.Drawing.Point(992, 230);
            this.btnBreakRight2Move.Name = "btnBreakRight2Move";
            this.btnBreakRight2Move.Size = new System.Drawing.Size(50, 26);
            this.btnBreakRight2Move.TabIndex = 168;
            this.btnBreakRight2Move.Text = "이동";
            this.btnBreakRight2Move.UseVisualStyleBackColor = false;
            // 
            // btnBreakRight2Read
            // 
            this.btnBreakRight2Read.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnBreakRight2Read.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakRight2Read.ForeColor = System.Drawing.Color.White;
            this.btnBreakRight2Read.Location = new System.Drawing.Point(936, 230);
            this.btnBreakRight2Read.Name = "btnBreakRight2Read";
            this.btnBreakRight2Read.Size = new System.Drawing.Size(50, 26);
            this.btnBreakRight2Read.TabIndex = 167;
            this.btnBreakRight2Read.Text = "읽기";
            this.btnBreakRight2Read.UseVisualStyleBackColor = false;
            // 
            // btnBreakRight2Calc
            // 
            this.btnBreakRight2Calc.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnBreakRight2Calc.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakRight2Calc.ForeColor = System.Drawing.Color.White;
            this.btnBreakRight2Calc.Location = new System.Drawing.Point(1049, 230);
            this.btnBreakRight2Calc.Name = "btnBreakRight2Calc";
            this.btnBreakRight2Calc.Size = new System.Drawing.Size(50, 26);
            this.btnBreakRight2Calc.TabIndex = 166;
            this.btnBreakRight2Calc.Text = "CALC";
            this.btnBreakRight2Calc.UseVisualStyleBackColor = false;
            // 
            // btnBreakLeft2Move
            // 
            this.btnBreakLeft2Move.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnBreakLeft2Move.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakLeft2Move.ForeColor = System.Drawing.Color.White;
            this.btnBreakLeft2Move.Location = new System.Drawing.Point(992, 165);
            this.btnBreakLeft2Move.Name = "btnBreakLeft2Move";
            this.btnBreakLeft2Move.Size = new System.Drawing.Size(50, 26);
            this.btnBreakLeft2Move.TabIndex = 165;
            this.btnBreakLeft2Move.Text = "이동";
            this.btnBreakLeft2Move.UseVisualStyleBackColor = false;
            // 
            // btnTable2AlignMark2Move
            // 
            this.btnTable2AlignMark2Move.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnTable2AlignMark2Move.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnTable2AlignMark2Move.ForeColor = System.Drawing.Color.White;
            this.btnTable2AlignMark2Move.Location = new System.Drawing.Point(993, 133);
            this.btnTable2AlignMark2Move.Name = "btnTable2AlignMark2Move";
            this.btnTable2AlignMark2Move.Size = new System.Drawing.Size(50, 26);
            this.btnTable2AlignMark2Move.TabIndex = 164;
            this.btnTable2AlignMark2Move.Text = "이동";
            this.btnTable2AlignMark2Move.UseVisualStyleBackColor = false;
            // 
            // btnTable1AlignMark2Move
            // 
            this.btnTable1AlignMark2Move.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnTable1AlignMark2Move.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnTable1AlignMark2Move.ForeColor = System.Drawing.Color.White;
            this.btnTable1AlignMark2Move.Location = new System.Drawing.Point(992, 102);
            this.btnTable1AlignMark2Move.Name = "btnTable1AlignMark2Move";
            this.btnTable1AlignMark2Move.Size = new System.Drawing.Size(50, 26);
            this.btnTable1AlignMark2Move.TabIndex = 163;
            this.btnTable1AlignMark2Move.Text = "이동";
            this.btnTable1AlignMark2Move.UseVisualStyleBackColor = false;
            // 
            // btnBreakLeft2Read
            // 
            this.btnBreakLeft2Read.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnBreakLeft2Read.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakLeft2Read.ForeColor = System.Drawing.Color.White;
            this.btnBreakLeft2Read.Location = new System.Drawing.Point(936, 165);
            this.btnBreakLeft2Read.Name = "btnBreakLeft2Read";
            this.btnBreakLeft2Read.Size = new System.Drawing.Size(50, 26);
            this.btnBreakLeft2Read.TabIndex = 162;
            this.btnBreakLeft2Read.Text = "읽기";
            this.btnBreakLeft2Read.UseVisualStyleBackColor = false;
            // 
            // btnTable2AlignMark2Read
            // 
            this.btnTable2AlignMark2Read.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnTable2AlignMark2Read.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnTable2AlignMark2Read.ForeColor = System.Drawing.Color.White;
            this.btnTable2AlignMark2Read.Location = new System.Drawing.Point(937, 133);
            this.btnTable2AlignMark2Read.Name = "btnTable2AlignMark2Read";
            this.btnTable2AlignMark2Read.Size = new System.Drawing.Size(50, 26);
            this.btnTable2AlignMark2Read.TabIndex = 161;
            this.btnTable2AlignMark2Read.Text = "읽기";
            this.btnTable2AlignMark2Read.UseVisualStyleBackColor = false;
            // 
            // btnTable1AlignMark2Read
            // 
            this.btnTable1AlignMark2Read.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnTable1AlignMark2Read.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnTable1AlignMark2Read.ForeColor = System.Drawing.Color.White;
            this.btnTable1AlignMark2Read.Location = new System.Drawing.Point(936, 102);
            this.btnTable1AlignMark2Read.Name = "btnTable1AlignMark2Read";
            this.btnTable1AlignMark2Read.Size = new System.Drawing.Size(50, 26);
            this.btnTable1AlignMark2Read.TabIndex = 160;
            this.btnTable1AlignMark2Read.Text = "읽기";
            this.btnTable1AlignMark2Read.UseVisualStyleBackColor = false;
            // 
            // btnBreakLeft2Calc
            // 
            this.btnBreakLeft2Calc.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnBreakLeft2Calc.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakLeft2Calc.ForeColor = System.Drawing.Color.White;
            this.btnBreakLeft2Calc.Location = new System.Drawing.Point(1049, 165);
            this.btnBreakLeft2Calc.Name = "btnBreakLeft2Calc";
            this.btnBreakLeft2Calc.Size = new System.Drawing.Size(50, 26);
            this.btnBreakLeft2Calc.TabIndex = 159;
            this.btnBreakLeft2Calc.Text = "CALC";
            this.btnBreakLeft2Calc.UseVisualStyleBackColor = false;
            // 
            // btnTable2AlignMark2Calc
            // 
            this.btnTable2AlignMark2Calc.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnTable2AlignMark2Calc.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnTable2AlignMark2Calc.ForeColor = System.Drawing.Color.White;
            this.btnTable2AlignMark2Calc.Location = new System.Drawing.Point(1049, 133);
            this.btnTable2AlignMark2Calc.Name = "btnTable2AlignMark2Calc";
            this.btnTable2AlignMark2Calc.Size = new System.Drawing.Size(50, 26);
            this.btnTable2AlignMark2Calc.TabIndex = 158;
            this.btnTable2AlignMark2Calc.Text = "CALC";
            this.btnTable2AlignMark2Calc.UseVisualStyleBackColor = false;
            // 
            // btnTable1AlignMark2Calc
            // 
            this.btnTable1AlignMark2Calc.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnTable1AlignMark2Calc.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnTable1AlignMark2Calc.ForeColor = System.Drawing.Color.White;
            this.btnTable1AlignMark2Calc.Location = new System.Drawing.Point(1049, 101);
            this.btnTable1AlignMark2Calc.Name = "btnTable1AlignMark2Calc";
            this.btnTable1AlignMark2Calc.Size = new System.Drawing.Size(50, 26);
            this.btnTable1AlignMark2Calc.TabIndex = 157;
            this.btnTable1AlignMark2Calc.Text = "CALC";
            this.btnTable1AlignMark2Calc.UseVisualStyleBackColor = false;
            // 
            // btnBreakRightYMove
            // 
            this.btnBreakRightYMove.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnBreakRightYMove.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakRightYMove.ForeColor = System.Drawing.Color.White;
            this.btnBreakRightYMove.Location = new System.Drawing.Point(441, 261);
            this.btnBreakRightYMove.Name = "btnBreakRightYMove";
            this.btnBreakRightYMove.Size = new System.Drawing.Size(50, 26);
            this.btnBreakRightYMove.TabIndex = 156;
            this.btnBreakRightYMove.Text = "이동";
            this.btnBreakRightYMove.UseVisualStyleBackColor = false;
            // 
            // btnBreakRight1Move
            // 
            this.btnBreakRight1Move.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnBreakRight1Move.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakRight1Move.ForeColor = System.Drawing.Color.White;
            this.btnBreakRight1Move.Location = new System.Drawing.Point(442, 229);
            this.btnBreakRight1Move.Name = "btnBreakRight1Move";
            this.btnBreakRight1Move.Size = new System.Drawing.Size(50, 26);
            this.btnBreakRight1Move.TabIndex = 155;
            this.btnBreakRight1Move.Text = "이동";
            this.btnBreakRight1Move.UseVisualStyleBackColor = false;
            // 
            // btnBreakLeftYMove
            // 
            this.btnBreakLeftYMove.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnBreakLeftYMove.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakLeftYMove.ForeColor = System.Drawing.Color.White;
            this.btnBreakLeftYMove.Location = new System.Drawing.Point(441, 198);
            this.btnBreakLeftYMove.Name = "btnBreakLeftYMove";
            this.btnBreakLeftYMove.Size = new System.Drawing.Size(50, 26);
            this.btnBreakLeftYMove.TabIndex = 154;
            this.btnBreakLeftYMove.Text = "이동";
            this.btnBreakLeftYMove.UseVisualStyleBackColor = false;
            // 
            // btnBreakLeft1Move
            // 
            this.btnBreakLeft1Move.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnBreakLeft1Move.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakLeft1Move.ForeColor = System.Drawing.Color.White;
            this.btnBreakLeft1Move.Location = new System.Drawing.Point(441, 166);
            this.btnBreakLeft1Move.Name = "btnBreakLeft1Move";
            this.btnBreakLeft1Move.Size = new System.Drawing.Size(50, 26);
            this.btnBreakLeft1Move.TabIndex = 153;
            this.btnBreakLeft1Move.Text = "이동";
            this.btnBreakLeft1Move.UseVisualStyleBackColor = false;
            // 
            // btnTable2Alignmark1Move
            // 
            this.btnTable2Alignmark1Move.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnTable2Alignmark1Move.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnTable2Alignmark1Move.ForeColor = System.Drawing.Color.White;
            this.btnTable2Alignmark1Move.Location = new System.Drawing.Point(442, 134);
            this.btnTable2Alignmark1Move.Name = "btnTable2Alignmark1Move";
            this.btnTable2Alignmark1Move.Size = new System.Drawing.Size(50, 26);
            this.btnTable2Alignmark1Move.TabIndex = 152;
            this.btnTable2Alignmark1Move.Text = "이동";
            this.btnTable2Alignmark1Move.UseVisualStyleBackColor = false;
            // 
            // btnTable1Alignmark1Move
            // 
            this.btnTable1Alignmark1Move.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnTable1Alignmark1Move.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnTable1Alignmark1Move.ForeColor = System.Drawing.Color.White;
            this.btnTable1Alignmark1Move.Location = new System.Drawing.Point(442, 102);
            this.btnTable1Alignmark1Move.Name = "btnTable1Alignmark1Move";
            this.btnTable1Alignmark1Move.Size = new System.Drawing.Size(50, 26);
            this.btnTable1Alignmark1Move.TabIndex = 151;
            this.btnTable1Alignmark1Move.Text = "이동";
            this.btnTable1Alignmark1Move.UseVisualStyleBackColor = false;
            // 
            // btnBreakRightYRead
            // 
            this.btnBreakRightYRead.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnBreakRightYRead.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakRightYRead.ForeColor = System.Drawing.Color.White;
            this.btnBreakRightYRead.Location = new System.Drawing.Point(385, 261);
            this.btnBreakRightYRead.Name = "btnBreakRightYRead";
            this.btnBreakRightYRead.Size = new System.Drawing.Size(50, 26);
            this.btnBreakRightYRead.TabIndex = 150;
            this.btnBreakRightYRead.Text = "읽기";
            this.btnBreakRightYRead.UseVisualStyleBackColor = false;
            // 
            // btnBreakRight1Read
            // 
            this.btnBreakRight1Read.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnBreakRight1Read.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakRight1Read.ForeColor = System.Drawing.Color.White;
            this.btnBreakRight1Read.Location = new System.Drawing.Point(386, 229);
            this.btnBreakRight1Read.Name = "btnBreakRight1Read";
            this.btnBreakRight1Read.Size = new System.Drawing.Size(50, 26);
            this.btnBreakRight1Read.TabIndex = 149;
            this.btnBreakRight1Read.Text = "읽기";
            this.btnBreakRight1Read.UseVisualStyleBackColor = false;
            // 
            // btnBreakLeftYRead
            // 
            this.btnBreakLeftYRead.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnBreakLeftYRead.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakLeftYRead.ForeColor = System.Drawing.Color.White;
            this.btnBreakLeftYRead.Location = new System.Drawing.Point(385, 198);
            this.btnBreakLeftYRead.Name = "btnBreakLeftYRead";
            this.btnBreakLeftYRead.Size = new System.Drawing.Size(50, 26);
            this.btnBreakLeftYRead.TabIndex = 148;
            this.btnBreakLeftYRead.Text = "읽기";
            this.btnBreakLeftYRead.UseVisualStyleBackColor = false;
            // 
            // btnBreakLeft1Read
            // 
            this.btnBreakLeft1Read.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnBreakLeft1Read.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakLeft1Read.ForeColor = System.Drawing.Color.White;
            this.btnBreakLeft1Read.Location = new System.Drawing.Point(385, 166);
            this.btnBreakLeft1Read.Name = "btnBreakLeft1Read";
            this.btnBreakLeft1Read.Size = new System.Drawing.Size(50, 26);
            this.btnBreakLeft1Read.TabIndex = 147;
            this.btnBreakLeft1Read.Text = "읽기";
            this.btnBreakLeft1Read.UseVisualStyleBackColor = false;
            // 
            // btnTable2Alignmark1Read
            // 
            this.btnTable2Alignmark1Read.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnTable2Alignmark1Read.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnTable2Alignmark1Read.ForeColor = System.Drawing.Color.White;
            this.btnTable2Alignmark1Read.Location = new System.Drawing.Point(386, 134);
            this.btnTable2Alignmark1Read.Name = "btnTable2Alignmark1Read";
            this.btnTable2Alignmark1Read.Size = new System.Drawing.Size(50, 26);
            this.btnTable2Alignmark1Read.TabIndex = 146;
            this.btnTable2Alignmark1Read.Text = "읽기";
            this.btnTable2Alignmark1Read.UseVisualStyleBackColor = false;
            // 
            // btnTable1Alignmark1Read
            // 
            this.btnTable1Alignmark1Read.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnTable1Alignmark1Read.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnTable1Alignmark1Read.ForeColor = System.Drawing.Color.White;
            this.btnTable1Alignmark1Read.Location = new System.Drawing.Point(386, 102);
            this.btnTable1Alignmark1Read.Name = "btnTable1Alignmark1Read";
            this.btnTable1Alignmark1Read.Size = new System.Drawing.Size(50, 26);
            this.btnTable1Alignmark1Read.TabIndex = 145;
            this.btnTable1Alignmark1Read.Text = "읽기";
            this.btnTable1Alignmark1Read.UseVisualStyleBackColor = false;
            // 
            // btnBreakRightYCalc
            // 
            this.btnBreakRightYCalc.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnBreakRightYCalc.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakRightYCalc.ForeColor = System.Drawing.Color.White;
            this.btnBreakRightYCalc.Location = new System.Drawing.Point(498, 261);
            this.btnBreakRightYCalc.Name = "btnBreakRightYCalc";
            this.btnBreakRightYCalc.Size = new System.Drawing.Size(50, 26);
            this.btnBreakRightYCalc.TabIndex = 144;
            this.btnBreakRightYCalc.Text = "CALC";
            this.btnBreakRightYCalc.UseVisualStyleBackColor = false;
            // 
            // btnBreakRight1Calc
            // 
            this.btnBreakRight1Calc.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnBreakRight1Calc.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakRight1Calc.ForeColor = System.Drawing.Color.White;
            this.btnBreakRight1Calc.Location = new System.Drawing.Point(498, 229);
            this.btnBreakRight1Calc.Name = "btnBreakRight1Calc";
            this.btnBreakRight1Calc.Size = new System.Drawing.Size(50, 26);
            this.btnBreakRight1Calc.TabIndex = 143;
            this.btnBreakRight1Calc.Text = "CALC";
            this.btnBreakRight1Calc.UseVisualStyleBackColor = false;
            // 
            // btnBreakLeftYCalc
            // 
            this.btnBreakLeftYCalc.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnBreakLeftYCalc.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakLeftYCalc.ForeColor = System.Drawing.Color.White;
            this.btnBreakLeftYCalc.Location = new System.Drawing.Point(498, 197);
            this.btnBreakLeftYCalc.Name = "btnBreakLeftYCalc";
            this.btnBreakLeftYCalc.Size = new System.Drawing.Size(50, 26);
            this.btnBreakLeftYCalc.TabIndex = 142;
            this.btnBreakLeftYCalc.Text = "CALC";
            this.btnBreakLeftYCalc.UseVisualStyleBackColor = false;
            // 
            // btnBreakLeft1Calc
            // 
            this.btnBreakLeft1Calc.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnBreakLeft1Calc.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakLeft1Calc.ForeColor = System.Drawing.Color.White;
            this.btnBreakLeft1Calc.Location = new System.Drawing.Point(498, 165);
            this.btnBreakLeft1Calc.Name = "btnBreakLeft1Calc";
            this.btnBreakLeft1Calc.Size = new System.Drawing.Size(50, 26);
            this.btnBreakLeft1Calc.TabIndex = 141;
            this.btnBreakLeft1Calc.Text = "CALC";
            this.btnBreakLeft1Calc.UseVisualStyleBackColor = false;
            // 
            // btnTable2Alignmark1Calc
            // 
            this.btnTable2Alignmark1Calc.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnTable2Alignmark1Calc.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnTable2Alignmark1Calc.ForeColor = System.Drawing.Color.White;
            this.btnTable2Alignmark1Calc.Location = new System.Drawing.Point(498, 133);
            this.btnTable2Alignmark1Calc.Name = "btnTable2Alignmark1Calc";
            this.btnTable2Alignmark1Calc.Size = new System.Drawing.Size(50, 26);
            this.btnTable2Alignmark1Calc.TabIndex = 140;
            this.btnTable2Alignmark1Calc.Text = "CALC";
            this.btnTable2Alignmark1Calc.UseVisualStyleBackColor = false;
            // 
            // btnTable1Alignmark1Calc
            // 
            this.btnTable1Alignmark1Calc.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnTable1Alignmark1Calc.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnTable1Alignmark1Calc.ForeColor = System.Drawing.Color.White;
            this.btnTable1Alignmark1Calc.Location = new System.Drawing.Point(498, 102);
            this.btnTable1Alignmark1Calc.Name = "btnTable1Alignmark1Calc";
            this.btnTable1Alignmark1Calc.Size = new System.Drawing.Size(50, 26);
            this.btnTable1Alignmark1Calc.TabIndex = 139;
            this.btnTable1Alignmark1Calc.Text = "CALC";
            this.btnTable1Alignmark1Calc.UseVisualStyleBackColor = false;
            // 
            // txtcellSize2
            // 
            this.txtcellSize2.Location = new System.Drawing.Point(385, 308);
            this.txtcellSize2.Name = "txtcellSize2";
            this.txtcellSize2.Size = new System.Drawing.Size(66, 26);
            this.txtcellSize2.TabIndex = 64;
            // 
            // txtcellSize1
            // 
            this.txtcellSize1.Location = new System.Drawing.Point(313, 308);
            this.txtcellSize1.Name = "txtcellSize1";
            this.txtcellSize1.Size = new System.Drawing.Size(66, 26);
            this.txtcellSize1.TabIndex = 63;
            // 
            // lb_cell_size
            // 
            this.lb_cell_size.AutoSize = true;
            this.lb_cell_size.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.999999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_cell_size.Location = new System.Drawing.Point(243, 316);
            this.lb_cell_size.Name = "lb_cell_size";
            this.lb_cell_size.Size = new System.Drawing.Size(67, 15);
            this.lb_cell_size.TabIndex = 62;
            this.lb_cell_size.Text = "Size[mm]";
            // 
            // lb_a_matrix
            // 
            this.lb_a_matrix.AutoSize = true;
            this.lb_a_matrix.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.999999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_a_matrix.Location = new System.Drawing.Point(6, 316);
            this.lb_a_matrix.Name = "lb_a_matrix";
            this.lb_a_matrix.Size = new System.Drawing.Size(60, 15);
            this.lb_a_matrix.TabIndex = 61;
            this.lb_a_matrix.Text = "A-Matrix";
            // 
            // txtbreakRightY
            // 
            this.txtbreakRightY.Location = new System.Drawing.Point(164, 262);
            this.txtbreakRightY.Name = "txtbreakRightY";
            this.txtbreakRightY.Size = new System.Drawing.Size(105, 26);
            this.txtbreakRightY.TabIndex = 53;
            // 
            // txtbreakLeftY
            // 
            this.txtbreakLeftY.Location = new System.Drawing.Point(164, 198);
            this.txtbreakLeftY.Name = "txtbreakLeftY";
            this.txtbreakLeftY.Size = new System.Drawing.Size(105, 26);
            this.txtbreakLeftY.TabIndex = 39;
            // 
            // lb_break_right_y
            // 
            this.lb_break_right_y.AutoSize = true;
            this.lb_break_right_y.Font = new System.Drawing.Font("Malgun Gothic", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_break_right_y.Location = new System.Drawing.Point(6, 269);
            this.lb_break_right_y.Name = "lb_break_right_y";
            this.lb_break_right_y.Size = new System.Drawing.Size(116, 15);
            this.lb_break_right_y.TabIndex = 51;
            this.lb_break_right_y.Text = "Break_right_Y[mm]";
            // 
            // lb_break_left_y
            // 
            this.lb_break_left_y.AutoSize = true;
            this.lb_break_left_y.Font = new System.Drawing.Font("Malgun Gothic", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_break_left_y.Location = new System.Drawing.Point(6, 205);
            this.lb_break_left_y.Name = "lb_break_left_y";
            this.lb_break_left_y.Size = new System.Drawing.Size(107, 15);
            this.lb_break_left_y.TabIndex = 38;
            this.lb_break_left_y.Text = "Break_left_Y[mm]";
            // 
            // txtbreakRight22
            // 
            this.txtbreakRight22.Location = new System.Drawing.Point(825, 230);
            this.txtbreakRight22.Name = "txtbreakRight22";
            this.txtbreakRight22.Size = new System.Drawing.Size(105, 26);
            this.txtbreakRight22.TabIndex = 54;
            // 
            // txtbreakLeft22
            // 
            this.txtbreakLeft22.Location = new System.Drawing.Point(825, 166);
            this.txtbreakLeft22.Name = "txtbreakLeft22";
            this.txtbreakLeft22.Size = new System.Drawing.Size(105, 26);
            this.txtbreakLeft22.TabIndex = 40;
            // 
            // txtbreakRight21
            // 
            this.txtbreakRight21.Location = new System.Drawing.Point(714, 230);
            this.txtbreakRight21.Name = "txtbreakRight21";
            this.txtbreakRight21.Size = new System.Drawing.Size(105, 26);
            this.txtbreakRight21.TabIndex = 52;
            // 
            // lb_break_right_2
            // 
            this.lb_break_right_2.AutoSize = true;
            this.lb_break_right_2.Font = new System.Drawing.Font("Malgun Gothic", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_break_right_2.Location = new System.Drawing.Point(556, 237);
            this.lb_break_right_2.Name = "lb_break_right_2";
            this.lb_break_right_2.Size = new System.Drawing.Size(116, 15);
            this.lb_break_right_2.TabIndex = 50;
            this.lb_break_right_2.Text = "Break_right_2[mm]";
            // 
            // txtbreakLeft21
            // 
            this.txtbreakLeft21.Location = new System.Drawing.Point(714, 166);
            this.txtbreakLeft21.Name = "txtbreakLeft21";
            this.txtbreakLeft21.Size = new System.Drawing.Size(105, 26);
            this.txtbreakLeft21.TabIndex = 39;
            // 
            // txtbreakRight12
            // 
            this.txtbreakRight12.Location = new System.Drawing.Point(275, 230);
            this.txtbreakRight12.Name = "txtbreakRight12";
            this.txtbreakRight12.Size = new System.Drawing.Size(105, 26);
            this.txtbreakRight12.TabIndex = 46;
            // 
            // txtbreakRight11
            // 
            this.txtbreakRight11.Location = new System.Drawing.Point(164, 230);
            this.txtbreakRight11.Name = "txtbreakRight11";
            this.txtbreakRight11.Size = new System.Drawing.Size(105, 26);
            this.txtbreakRight11.TabIndex = 45;
            // 
            // lb_break_left_2
            // 
            this.lb_break_left_2.AutoSize = true;
            this.lb_break_left_2.Font = new System.Drawing.Font("Malgun Gothic", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_break_left_2.Location = new System.Drawing.Point(556, 173);
            this.lb_break_left_2.Name = "lb_break_left_2";
            this.lb_break_left_2.Size = new System.Drawing.Size(107, 15);
            this.lb_break_left_2.TabIndex = 38;
            this.lb_break_left_2.Text = "Break_left_2[mm]";
            // 
            // lb_break_right_1
            // 
            this.lb_break_right_1.AutoSize = true;
            this.lb_break_right_1.Font = new System.Drawing.Font("Malgun Gothic", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_break_right_1.Location = new System.Drawing.Point(6, 237);
            this.lb_break_right_1.Name = "lb_break_right_1";
            this.lb_break_right_1.Size = new System.Drawing.Size(116, 15);
            this.lb_break_right_1.TabIndex = 44;
            this.lb_break_right_1.Text = "Break_right_1[mm]";
            // 
            // txtbreakLeft12
            // 
            this.txtbreakLeft12.Location = new System.Drawing.Point(275, 166);
            this.txtbreakLeft12.Name = "txtbreakLeft12";
            this.txtbreakLeft12.Size = new System.Drawing.Size(105, 26);
            this.txtbreakLeft12.TabIndex = 34;
            // 
            // txtbreakLeft11
            // 
            this.txtbreakLeft11.Location = new System.Drawing.Point(164, 166);
            this.txtbreakLeft11.Name = "txtbreakLeft11";
            this.txtbreakLeft11.Size = new System.Drawing.Size(105, 26);
            this.txtbreakLeft11.TabIndex = 33;
            // 
            // lb_break_left_1
            // 
            this.lb_break_left_1.AutoSize = true;
            this.lb_break_left_1.Font = new System.Drawing.Font("Malgun Gothic", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_break_left_1.Location = new System.Drawing.Point(6, 173);
            this.lb_break_left_1.Name = "lb_break_left_1";
            this.lb_break_left_1.Size = new System.Drawing.Size(107, 15);
            this.lb_break_left_1.TabIndex = 32;
            this.lb_break_left_1.Text = "Break_left_1[mm]";
            // 
            // txttable2Alignmark22
            // 
            this.txttable2Alignmark22.Location = new System.Drawing.Point(825, 134);
            this.txttable2Alignmark22.Name = "txttable2Alignmark22";
            this.txttable2Alignmark22.Size = new System.Drawing.Size(105, 26);
            this.txttable2Alignmark22.TabIndex = 34;
            // 
            // txttable2Alignmark21
            // 
            this.txttable2Alignmark21.Location = new System.Drawing.Point(714, 134);
            this.txttable2Alignmark21.Name = "txttable2Alignmark21";
            this.txttable2Alignmark21.Size = new System.Drawing.Size(105, 26);
            this.txttable2Alignmark21.TabIndex = 33;
            // 
            // lb_table2_alignmark2
            // 
            this.lb_table2_alignmark2.AutoSize = true;
            this.lb_table2_alignmark2.Font = new System.Drawing.Font("Malgun Gothic", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_table2_alignmark2.Location = new System.Drawing.Point(556, 141);
            this.lb_table2_alignmark2.Name = "lb_table2_alignmark2";
            this.lb_table2_alignmark2.Size = new System.Drawing.Size(147, 15);
            this.lb_table2_alignmark2.TabIndex = 32;
            this.lb_table2_alignmark2.Text = "Table2AlignMark 2[mm]";
            // 
            // txttable2Alignmark12
            // 
            this.txttable2Alignmark12.Location = new System.Drawing.Point(275, 134);
            this.txttable2Alignmark12.Name = "txttable2Alignmark12";
            this.txttable2Alignmark12.Size = new System.Drawing.Size(105, 26);
            this.txttable2Alignmark12.TabIndex = 28;
            // 
            // txttable2Alignmark11
            // 
            this.txttable2Alignmark11.Location = new System.Drawing.Point(164, 134);
            this.txttable2Alignmark11.Name = "txttable2Alignmark11";
            this.txttable2Alignmark11.Size = new System.Drawing.Size(105, 26);
            this.txttable2Alignmark11.TabIndex = 27;
            // 
            // lb_table2_alignmark1
            // 
            this.lb_table2_alignmark1.AutoSize = true;
            this.lb_table2_alignmark1.Font = new System.Drawing.Font("Malgun Gothic", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_table2_alignmark1.Location = new System.Drawing.Point(6, 141);
            this.lb_table2_alignmark1.Name = "lb_table2_alignmark1";
            this.lb_table2_alignmark1.Size = new System.Drawing.Size(147, 15);
            this.lb_table2_alignmark1.TabIndex = 26;
            this.lb_table2_alignmark1.Text = "Table2AlignMark 1[mm]";
            // 
            // txttable1Alignmark22
            // 
            this.txttable1Alignmark22.Location = new System.Drawing.Point(825, 102);
            this.txttable1Alignmark22.Name = "txttable1Alignmark22";
            this.txttable1Alignmark22.Size = new System.Drawing.Size(105, 26);
            this.txttable1Alignmark22.TabIndex = 28;
            // 
            // txttable1Alignmark21
            // 
            this.txttable1Alignmark21.Location = new System.Drawing.Point(714, 102);
            this.txttable1Alignmark21.Name = "txttable1Alignmark21";
            this.txttable1Alignmark21.Size = new System.Drawing.Size(105, 26);
            this.txttable1Alignmark21.TabIndex = 27;
            // 
            // txttable1Alignmark12
            // 
            this.txttable1Alignmark12.Location = new System.Drawing.Point(275, 102);
            this.txttable1Alignmark12.Name = "txttable1Alignmark12";
            this.txttable1Alignmark12.Size = new System.Drawing.Size(105, 26);
            this.txttable1Alignmark12.TabIndex = 22;
            // 
            // lb_table1_alignmark2
            // 
            this.lb_table1_alignmark2.AutoSize = true;
            this.lb_table1_alignmark2.Font = new System.Drawing.Font("Malgun Gothic", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_table1_alignmark2.Location = new System.Drawing.Point(556, 109);
            this.lb_table1_alignmark2.Name = "lb_table1_alignmark2";
            this.lb_table1_alignmark2.Size = new System.Drawing.Size(147, 15);
            this.lb_table1_alignmark2.TabIndex = 26;
            this.lb_table1_alignmark2.Text = "Table1AlignMark 2[mm]";
            // 
            // txttable1Alignmark11
            // 
            this.txttable1Alignmark11.Location = new System.Drawing.Point(164, 102);
            this.txttable1Alignmark11.Name = "txttable1Alignmark11";
            this.txttable1Alignmark11.Size = new System.Drawing.Size(105, 26);
            this.txttable1Alignmark11.TabIndex = 21;
            // 
            // lb_table1_alignmark1
            // 
            this.lb_table1_alignmark1.AutoSize = true;
            this.lb_table1_alignmark1.Font = new System.Drawing.Font("Malgun Gothic", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_table1_alignmark1.Location = new System.Drawing.Point(6, 109);
            this.lb_table1_alignmark1.Name = "lb_table1_alignmark1";
            this.lb_table1_alignmark1.Size = new System.Drawing.Size(147, 15);
            this.lb_table1_alignmark1.TabIndex = 20;
            this.lb_table1_alignmark1.Text = "Table1AlignMark 1[mm]";
            // 
            // txtrecipePath
            // 
            this.txtrecipePath.Location = new System.Drawing.Point(65, 56);
            this.txtrecipePath.Name = "txtrecipePath";
            this.txtrecipePath.ReadOnly = true;
            this.txtrecipePath.Size = new System.Drawing.Size(532, 26);
            this.txtrecipePath.TabIndex = 4;
            // 
            // lb_recipe
            // 
            this.lb_recipe.AutoSize = true;
            this.lb_recipe.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_recipe.Location = new System.Drawing.Point(6, 27);
            this.lb_recipe.Name = "lb_recipe";
            this.lb_recipe.Size = new System.Drawing.Size(55, 20);
            this.lb_recipe.TabIndex = 2;
            this.lb_recipe.Text = "Recipe";
            // 
            // gxty_offset
            // 
            this.gxty_offset.BackColor = System.Drawing.Color.Transparent;
            this.gxty_offset.Controls.Add(this.txtyOffsetBreak);
            this.gxty_offset.Controls.Add(this.lb_y_offset_break);
            this.gxty_offset.Controls.Add(this.txtyOffsetPre);
            this.gxty_offset.Controls.Add(this.lb_y_offset_pre);
            this.gxty_offset.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.gxty_offset.ForeColor = System.Drawing.Color.White;
            this.gxty_offset.Location = new System.Drawing.Point(1394, 671);
            this.gxty_offset.Name = "gxty_offset";
            this.gxty_offset.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gxty_offset.Size = new System.Drawing.Size(343, 109);
            this.gxty_offset.TabIndex = 66;
            this.gxty_offset.TabStop = false;
            this.gxty_offset.Text = "Y Offset";
            // 
            // txtyOffsetBreak
            // 
            this.txtyOffsetBreak.Location = new System.Drawing.Point(151, 64);
            this.txtyOffsetBreak.Name = "txtyOffsetBreak";
            this.txtyOffsetBreak.Size = new System.Drawing.Size(181, 29);
            this.txtyOffsetBreak.TabIndex = 6;
            // 
            // lb_y_offset_break
            // 
            this.lb_y_offset_break.AutoSize = true;
            this.lb_y_offset_break.Font = new System.Drawing.Font("Malgun Gothic", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_y_offset_break.Location = new System.Drawing.Point(6, 68);
            this.lb_y_offset_break.Name = "lb_y_offset_break";
            this.lb_y_offset_break.Size = new System.Drawing.Size(119, 17);
            this.lb_y_offset_break.TabIndex = 5;
            this.lb_y_offset_break.Text = "Offset Break[mm]";
            // 
            // txtyOffsetPre
            // 
            this.txtyOffsetPre.Location = new System.Drawing.Point(151, 32);
            this.txtyOffsetPre.Name = "txtyOffsetPre";
            this.txtyOffsetPre.Size = new System.Drawing.Size(181, 29);
            this.txtyOffsetPre.TabIndex = 4;
            // 
            // lb_y_offset_pre
            // 
            this.lb_y_offset_pre.AutoSize = true;
            this.lb_y_offset_pre.Font = new System.Drawing.Font("Malgun Gothic", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_y_offset_pre.Location = new System.Drawing.Point(6, 38);
            this.lb_y_offset_pre.Name = "lb_y_offset_pre";
            this.lb_y_offset_pre.Size = new System.Drawing.Size(105, 17);
            this.lb_y_offset_pre.TabIndex = 1;
            this.lb_y_offset_pre.Text = "Offset Pre[mm]";
            // 
            // gxtlds_break
            // 
            this.gxtlds_break.BackColor = System.Drawing.Color.Transparent;
            this.gxtlds_break.Controls.Add(this.btnLDSBreak4Move);
            this.gxtlds_break.Controls.Add(this.txtldsBreak42);
            this.gxtlds_break.Controls.Add(this.txtldsBreak41);
            this.gxtlds_break.Controls.Add(this.btnLDSBreak1Move);
            this.gxtlds_break.Controls.Add(this.btnLDSBreak3Move);
            this.gxtlds_break.Controls.Add(this.txtldsBreak32);
            this.gxtlds_break.Controls.Add(this.txtldsBreak31);
            this.gxtlds_break.Controls.Add(this.btnLDSBreak1Read);
            this.gxtlds_break.Controls.Add(this.txtldsBreak22);
            this.gxtlds_break.Controls.Add(this.txtldsBreak21);
            this.gxtlds_break.Controls.Add(this.btnLDSBreak2Move);
            this.gxtlds_break.Controls.Add(this.txtldsBreak12);
            this.gxtlds_break.Controls.Add(this.lb_lds_break4);
            this.gxtlds_break.Controls.Add(this.btnLDSBreak2Read);
            this.gxtlds_break.Controls.Add(this.lb_lds_break3);
            this.gxtlds_break.Controls.Add(this.lb_lds_break2);
            this.gxtlds_break.Controls.Add(this.btnLDSBreak3Read);
            this.gxtlds_break.Controls.Add(this.txtldsBreak11);
            this.gxtlds_break.Controls.Add(this.lb_lds_break1);
            this.gxtlds_break.Controls.Add(this.btnLDSBreak4Read);
            this.gxtlds_break.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.gxtlds_break.ForeColor = System.Drawing.Color.White;
            this.gxtlds_break.Location = new System.Drawing.Point(1394, 512);
            this.gxtlds_break.Name = "gxtlds_break";
            this.gxtlds_break.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gxtlds_break.Size = new System.Drawing.Size(343, 153);
            this.gxtlds_break.TabIndex = 65;
            this.gxtlds_break.TabStop = false;
            this.gxtlds_break.Text = "LDS Break";
            // 
            // btnLDSBreak4Move
            // 
            this.btnLDSBreak4Move.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnLDSBreak4Move.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnLDSBreak4Move.ForeColor = System.Drawing.Color.White;
            this.btnLDSBreak4Move.Location = new System.Drawing.Point(292, 119);
            this.btnLDSBreak4Move.Name = "btnLDSBreak4Move";
            this.btnLDSBreak4Move.Size = new System.Drawing.Size(49, 26);
            this.btnLDSBreak4Move.TabIndex = 186;
            this.btnLDSBreak4Move.Text = "이동";
            this.btnLDSBreak4Move.UseVisualStyleBackColor = false;
            // 
            // txtldsBreak42
            // 
            this.txtldsBreak42.Location = new System.Drawing.Point(172, 116);
            this.txtldsBreak42.Name = "txtldsBreak42";
            this.txtldsBreak42.Size = new System.Drawing.Size(69, 29);
            this.txtldsBreak42.TabIndex = 47;
            // 
            // txtldsBreak41
            // 
            this.txtldsBreak41.Location = new System.Drawing.Point(97, 116);
            this.txtldsBreak41.Name = "txtldsBreak41";
            this.txtldsBreak41.Size = new System.Drawing.Size(69, 29);
            this.txtldsBreak41.TabIndex = 46;
            // 
            // btnLDSBreak1Move
            // 
            this.btnLDSBreak1Move.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnLDSBreak1Move.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnLDSBreak1Move.ForeColor = System.Drawing.Color.White;
            this.btnLDSBreak1Move.Location = new System.Drawing.Point(293, 23);
            this.btnLDSBreak1Move.Name = "btnLDSBreak1Move";
            this.btnLDSBreak1Move.Size = new System.Drawing.Size(49, 26);
            this.btnLDSBreak1Move.TabIndex = 183;
            this.btnLDSBreak1Move.Text = "이동";
            this.btnLDSBreak1Move.UseVisualStyleBackColor = false;
            // 
            // btnLDSBreak3Move
            // 
            this.btnLDSBreak3Move.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnLDSBreak3Move.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnLDSBreak3Move.ForeColor = System.Drawing.Color.White;
            this.btnLDSBreak3Move.Location = new System.Drawing.Point(292, 87);
            this.btnLDSBreak3Move.Name = "btnLDSBreak3Move";
            this.btnLDSBreak3Move.Size = new System.Drawing.Size(49, 26);
            this.btnLDSBreak3Move.TabIndex = 185;
            this.btnLDSBreak3Move.Text = "이동";
            this.btnLDSBreak3Move.UseVisualStyleBackColor = false;
            // 
            // txtldsBreak32
            // 
            this.txtldsBreak32.Location = new System.Drawing.Point(172, 85);
            this.txtldsBreak32.Name = "txtldsBreak32";
            this.txtldsBreak32.Size = new System.Drawing.Size(69, 29);
            this.txtldsBreak32.TabIndex = 43;
            // 
            // txtldsBreak31
            // 
            this.txtldsBreak31.Location = new System.Drawing.Point(97, 85);
            this.txtldsBreak31.Name = "txtldsBreak31";
            this.txtldsBreak31.Size = new System.Drawing.Size(69, 29);
            this.txtldsBreak31.TabIndex = 42;
            // 
            // btnLDSBreak1Read
            // 
            this.btnLDSBreak1Read.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnLDSBreak1Read.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnLDSBreak1Read.ForeColor = System.Drawing.Color.White;
            this.btnLDSBreak1Read.Location = new System.Drawing.Point(243, 23);
            this.btnLDSBreak1Read.Name = "btnLDSBreak1Read";
            this.btnLDSBreak1Read.Size = new System.Drawing.Size(49, 26);
            this.btnLDSBreak1Read.TabIndex = 179;
            this.btnLDSBreak1Read.Text = "읽기";
            this.btnLDSBreak1Read.UseVisualStyleBackColor = false;
            // 
            // txtldsBreak22
            // 
            this.txtldsBreak22.Location = new System.Drawing.Point(172, 53);
            this.txtldsBreak22.Name = "txtldsBreak22";
            this.txtldsBreak22.Size = new System.Drawing.Size(69, 29);
            this.txtldsBreak22.TabIndex = 39;
            // 
            // txtldsBreak21
            // 
            this.txtldsBreak21.Location = new System.Drawing.Point(97, 53);
            this.txtldsBreak21.Name = "txtldsBreak21";
            this.txtldsBreak21.Size = new System.Drawing.Size(69, 29);
            this.txtldsBreak21.TabIndex = 38;
            // 
            // btnLDSBreak2Move
            // 
            this.btnLDSBreak2Move.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnLDSBreak2Move.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnLDSBreak2Move.ForeColor = System.Drawing.Color.White;
            this.btnLDSBreak2Move.Location = new System.Drawing.Point(293, 55);
            this.btnLDSBreak2Move.Name = "btnLDSBreak2Move";
            this.btnLDSBreak2Move.Size = new System.Drawing.Size(49, 26);
            this.btnLDSBreak2Move.TabIndex = 184;
            this.btnLDSBreak2Move.Text = "이동";
            this.btnLDSBreak2Move.UseVisualStyleBackColor = false;
            // 
            // txtldsBreak12
            // 
            this.txtldsBreak12.Location = new System.Drawing.Point(172, 21);
            this.txtldsBreak12.Name = "txtldsBreak12";
            this.txtldsBreak12.Size = new System.Drawing.Size(69, 29);
            this.txtldsBreak12.TabIndex = 35;
            // 
            // lb_lds_break4
            // 
            this.lb_lds_break4.AutoSize = true;
            this.lb_lds_break4.Font = new System.Drawing.Font("Malgun Gothic", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_lds_break4.Location = new System.Drawing.Point(6, 123);
            this.lb_lds_break4.Name = "lb_lds_break4";
            this.lb_lds_break4.Size = new System.Drawing.Size(78, 15);
            this.lb_lds_break4.TabIndex = 34;
            this.lb_lds_break4.Text = "Break4[mm]";
            // 
            // btnLDSBreak2Read
            // 
            this.btnLDSBreak2Read.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnLDSBreak2Read.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnLDSBreak2Read.ForeColor = System.Drawing.Color.White;
            this.btnLDSBreak2Read.Location = new System.Drawing.Point(243, 55);
            this.btnLDSBreak2Read.Name = "btnLDSBreak2Read";
            this.btnLDSBreak2Read.Size = new System.Drawing.Size(49, 26);
            this.btnLDSBreak2Read.TabIndex = 180;
            this.btnLDSBreak2Read.Text = "읽기";
            this.btnLDSBreak2Read.UseVisualStyleBackColor = false;
            // 
            // lb_lds_break3
            // 
            this.lb_lds_break3.AutoSize = true;
            this.lb_lds_break3.Font = new System.Drawing.Font("Malgun Gothic", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_lds_break3.Location = new System.Drawing.Point(6, 91);
            this.lb_lds_break3.Name = "lb_lds_break3";
            this.lb_lds_break3.Size = new System.Drawing.Size(78, 15);
            this.lb_lds_break3.TabIndex = 33;
            this.lb_lds_break3.Text = "Break3[mm]";
            // 
            // lb_lds_break2
            // 
            this.lb_lds_break2.AutoSize = true;
            this.lb_lds_break2.Font = new System.Drawing.Font("Malgun Gothic", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_lds_break2.Location = new System.Drawing.Point(6, 59);
            this.lb_lds_break2.Name = "lb_lds_break2";
            this.lb_lds_break2.Size = new System.Drawing.Size(78, 15);
            this.lb_lds_break2.TabIndex = 32;
            this.lb_lds_break2.Text = "Break2[mm]";
            // 
            // btnLDSBreak3Read
            // 
            this.btnLDSBreak3Read.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnLDSBreak3Read.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnLDSBreak3Read.ForeColor = System.Drawing.Color.White;
            this.btnLDSBreak3Read.Location = new System.Drawing.Point(242, 87);
            this.btnLDSBreak3Read.Name = "btnLDSBreak3Read";
            this.btnLDSBreak3Read.Size = new System.Drawing.Size(49, 26);
            this.btnLDSBreak3Read.TabIndex = 181;
            this.btnLDSBreak3Read.Text = "읽기";
            this.btnLDSBreak3Read.UseVisualStyleBackColor = false;
            // 
            // txtldsBreak11
            // 
            this.txtldsBreak11.Location = new System.Drawing.Point(97, 21);
            this.txtldsBreak11.Name = "txtldsBreak11";
            this.txtldsBreak11.Size = new System.Drawing.Size(69, 29);
            this.txtldsBreak11.TabIndex = 30;
            // 
            // lb_lds_break1
            // 
            this.lb_lds_break1.AutoSize = true;
            this.lb_lds_break1.Font = new System.Drawing.Font("Malgun Gothic", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_lds_break1.Location = new System.Drawing.Point(6, 27);
            this.lb_lds_break1.Name = "lb_lds_break1";
            this.lb_lds_break1.Size = new System.Drawing.Size(78, 15);
            this.lb_lds_break1.TabIndex = 28;
            this.lb_lds_break1.Text = "Break1[mm]";
            // 
            // btnLDSBreak4Read
            // 
            this.btnLDSBreak4Read.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnLDSBreak4Read.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnLDSBreak4Read.ForeColor = System.Drawing.Color.White;
            this.btnLDSBreak4Read.Location = new System.Drawing.Point(242, 119);
            this.btnLDSBreak4Read.Name = "btnLDSBreak4Read";
            this.btnLDSBreak4Read.Size = new System.Drawing.Size(49, 26);
            this.btnLDSBreak4Read.TabIndex = 182;
            this.btnLDSBreak4Read.Text = "읽기";
            this.btnLDSBreak4Read.UseVisualStyleBackColor = false;
            // 
            // panel19
            // 
            this.panel19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel19.Controls.Add(this.btnRecipeCimDelete);
            this.panel19.Controls.Add(this.btnRecipeCimApply);
            this.panel19.Controls.Add(this.btnRecipeDelete);
            this.panel19.Controls.Add(this.btnRecipeApply);
            this.panel19.Controls.Add(this.btnRecipeSave);
            this.panel19.Controls.Add(this.btnRecipeCopy);
            this.panel19.Controls.Add(this.btnRecipeMake);
            this.panel19.ForeColor = System.Drawing.Color.White;
            this.panel19.Location = new System.Drawing.Point(284, 800);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(1453, 57);
            this.panel19.TabIndex = 65;
            // 
            // btnRecipeCimDelete
            // 
            this.btnRecipeCimDelete.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnRecipeCimDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.btnRecipeCimDelete.ForeColor = System.Drawing.Color.White;
            this.btnRecipeCimDelete.Location = new System.Drawing.Point(1035, 3);
            this.btnRecipeCimDelete.Name = "btnRecipeCimDelete";
            this.btnRecipeCimDelete.Size = new System.Drawing.Size(132, 50);
            this.btnRecipeCimDelete.TabIndex = 43;
            this.btnRecipeCimDelete.Text = "CIM Delete";
            this.btnRecipeCimDelete.UseVisualStyleBackColor = false;
            this.btnRecipeCimDelete.Click += new System.EventHandler(this.btnRecipeCimDelete_Click);
            // 
            // btnRecipeCimApply
            // 
            this.btnRecipeCimApply.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnRecipeCimApply.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnRecipeCimApply.ForeColor = System.Drawing.Color.White;
            this.btnRecipeCimApply.Location = new System.Drawing.Point(897, 3);
            this.btnRecipeCimApply.Name = "btnRecipeCimApply";
            this.btnRecipeCimApply.Size = new System.Drawing.Size(132, 50);
            this.btnRecipeCimApply.TabIndex = 42;
            this.btnRecipeCimApply.Text = "CIM Apply";
            this.btnRecipeCimApply.UseVisualStyleBackColor = false;
            this.btnRecipeCimApply.Click += new System.EventHandler(this.btnRecipeCimApply_Click);
            // 
            // btnRecipeDelete
            // 
            this.btnRecipeDelete.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnRecipeDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnRecipeDelete.ForeColor = System.Drawing.Color.White;
            this.btnRecipeDelete.Location = new System.Drawing.Point(415, -1);
            this.btnRecipeDelete.Name = "btnRecipeDelete";
            this.btnRecipeDelete.Size = new System.Drawing.Size(132, 50);
            this.btnRecipeDelete.TabIndex = 41;
            this.btnRecipeDelete.Text = "Delete";
            this.btnRecipeDelete.UseVisualStyleBackColor = false;
            this.btnRecipeDelete.Click += new System.EventHandler(this.btnRecipeDelete_Click);
            // 
            // btnRecipeApply
            // 
            this.btnRecipeApply.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnRecipeApply.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnRecipeApply.ForeColor = System.Drawing.Color.White;
            this.btnRecipeApply.Location = new System.Drawing.Point(716, 3);
            this.btnRecipeApply.Name = "btnRecipeApply";
            this.btnRecipeApply.Size = new System.Drawing.Size(132, 50);
            this.btnRecipeApply.TabIndex = 40;
            this.btnRecipeApply.Text = "Apply";
            this.btnRecipeApply.UseVisualStyleBackColor = false;
            this.btnRecipeApply.Click += new System.EventHandler(this.btnRecipeApply_Click);
            // 
            // btnRecipeSave
            // 
            this.btnRecipeSave.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnRecipeSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnRecipeSave.ForeColor = System.Drawing.Color.White;
            this.btnRecipeSave.Location = new System.Drawing.Point(275, 3);
            this.btnRecipeSave.Name = "btnRecipeSave";
            this.btnRecipeSave.Size = new System.Drawing.Size(132, 50);
            this.btnRecipeSave.TabIndex = 39;
            this.btnRecipeSave.Text = "Save";
            this.btnRecipeSave.UseVisualStyleBackColor = false;
            this.btnRecipeSave.Click += new System.EventHandler(this.btnRecipeSave_Click);
            // 
            // btnRecipeCopy
            // 
            this.btnRecipeCopy.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnRecipeCopy.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnRecipeCopy.ForeColor = System.Drawing.Color.White;
            this.btnRecipeCopy.Location = new System.Drawing.Point(137, 3);
            this.btnRecipeCopy.Name = "btnRecipeCopy";
            this.btnRecipeCopy.Size = new System.Drawing.Size(132, 50);
            this.btnRecipeCopy.TabIndex = 38;
            this.btnRecipeCopy.Text = "Copy";
            this.btnRecipeCopy.UseVisualStyleBackColor = false;
            this.btnRecipeCopy.Click += new System.EventHandler(this.btnRecipeCopy_Click);
            // 
            // btnRecipeMake
            // 
            this.btnRecipeMake.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnRecipeMake.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnRecipeMake.ForeColor = System.Drawing.Color.White;
            this.btnRecipeMake.Location = new System.Drawing.Point(-1, 3);
            this.btnRecipeMake.Name = "btnRecipeMake";
            this.btnRecipeMake.Size = new System.Drawing.Size(132, 50);
            this.btnRecipeMake.TabIndex = 37;
            this.btnRecipeMake.Text = "Make";
            this.btnRecipeMake.UseVisualStyleBackColor = false;
            this.btnRecipeMake.Click += new System.EventHandler(this.btnRecipeMake_Click);
            // 
            // gxtlds_process
            // 
            this.gxtlds_process.BackColor = System.Drawing.Color.Transparent;
            this.gxtlds_process.Controls.Add(this.btnLDSProcess4Move);
            this.gxtlds_process.Controls.Add(this.txtldsProcess42);
            this.gxtlds_process.Controls.Add(this.btnLDSProcess3Move);
            this.gxtlds_process.Controls.Add(this.txtldsProcess41);
            this.gxtlds_process.Controls.Add(this.btnLDSProcess2Move);
            this.gxtlds_process.Controls.Add(this.txtldsProcess32);
            this.gxtlds_process.Controls.Add(this.btnLDSProcess1Move);
            this.gxtlds_process.Controls.Add(this.txtldsProcess31);
            this.gxtlds_process.Controls.Add(this.btnLDSProcess4Read);
            this.gxtlds_process.Controls.Add(this.txtldsProcess22);
            this.gxtlds_process.Controls.Add(this.btnLDSProcess3Read);
            this.gxtlds_process.Controls.Add(this.txtldsProcess21);
            this.gxtlds_process.Controls.Add(this.btnLDSProcess2Read);
            this.gxtlds_process.Controls.Add(this.txtldsProcess12);
            this.gxtlds_process.Controls.Add(this.btnLDSProcess1Read);
            this.gxtlds_process.Controls.Add(this.lb_lds_process4);
            this.gxtlds_process.Controls.Add(this.lb_lds_process3);
            this.gxtlds_process.Controls.Add(this.lb_lds_process2);
            this.gxtlds_process.Controls.Add(this.txtldsProcess11);
            this.gxtlds_process.Controls.Add(this.lb_lds_process1);
            this.gxtlds_process.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.gxtlds_process.ForeColor = System.Drawing.Color.White;
            this.gxtlds_process.Location = new System.Drawing.Point(1394, 358);
            this.gxtlds_process.Name = "gxtlds_process";
            this.gxtlds_process.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gxtlds_process.Size = new System.Drawing.Size(343, 153);
            this.gxtlds_process.TabIndex = 64;
            this.gxtlds_process.TabStop = false;
            this.gxtlds_process.Text = "LDS Process";
            // 
            // btnLDSProcess4Move
            // 
            this.btnLDSProcess4Move.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnLDSProcess4Move.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnLDSProcess4Move.ForeColor = System.Drawing.Color.White;
            this.btnLDSProcess4Move.Location = new System.Drawing.Point(291, 118);
            this.btnLDSProcess4Move.Name = "btnLDSProcess4Move";
            this.btnLDSProcess4Move.Size = new System.Drawing.Size(49, 26);
            this.btnLDSProcess4Move.TabIndex = 178;
            this.btnLDSProcess4Move.Text = "이동";
            this.btnLDSProcess4Move.UseVisualStyleBackColor = false;
            // 
            // txtldsProcess42
            // 
            this.txtldsProcess42.Location = new System.Drawing.Point(172, 117);
            this.txtldsProcess42.Name = "txtldsProcess42";
            this.txtldsProcess42.Size = new System.Drawing.Size(69, 29);
            this.txtldsProcess42.TabIndex = 27;
            // 
            // btnLDSProcess3Move
            // 
            this.btnLDSProcess3Move.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnLDSProcess3Move.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnLDSProcess3Move.ForeColor = System.Drawing.Color.White;
            this.btnLDSProcess3Move.Location = new System.Drawing.Point(291, 86);
            this.btnLDSProcess3Move.Name = "btnLDSProcess3Move";
            this.btnLDSProcess3Move.Size = new System.Drawing.Size(49, 26);
            this.btnLDSProcess3Move.TabIndex = 177;
            this.btnLDSProcess3Move.Text = "이동";
            this.btnLDSProcess3Move.UseVisualStyleBackColor = false;
            // 
            // txtldsProcess41
            // 
            this.txtldsProcess41.Location = new System.Drawing.Point(97, 117);
            this.txtldsProcess41.Name = "txtldsProcess41";
            this.txtldsProcess41.Size = new System.Drawing.Size(69, 29);
            this.txtldsProcess41.TabIndex = 26;
            // 
            // btnLDSProcess2Move
            // 
            this.btnLDSProcess2Move.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnLDSProcess2Move.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnLDSProcess2Move.ForeColor = System.Drawing.Color.White;
            this.btnLDSProcess2Move.Location = new System.Drawing.Point(292, 54);
            this.btnLDSProcess2Move.Name = "btnLDSProcess2Move";
            this.btnLDSProcess2Move.Size = new System.Drawing.Size(49, 26);
            this.btnLDSProcess2Move.TabIndex = 176;
            this.btnLDSProcess2Move.Text = "이동";
            this.btnLDSProcess2Move.UseVisualStyleBackColor = false;
            // 
            // txtldsProcess32
            // 
            this.txtldsProcess32.Location = new System.Drawing.Point(172, 86);
            this.txtldsProcess32.Name = "txtldsProcess32";
            this.txtldsProcess32.Size = new System.Drawing.Size(69, 29);
            this.txtldsProcess32.TabIndex = 23;
            // 
            // btnLDSProcess1Move
            // 
            this.btnLDSProcess1Move.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnLDSProcess1Move.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnLDSProcess1Move.ForeColor = System.Drawing.Color.White;
            this.btnLDSProcess1Move.Location = new System.Drawing.Point(292, 22);
            this.btnLDSProcess1Move.Name = "btnLDSProcess1Move";
            this.btnLDSProcess1Move.Size = new System.Drawing.Size(49, 26);
            this.btnLDSProcess1Move.TabIndex = 175;
            this.btnLDSProcess1Move.Text = "이동";
            this.btnLDSProcess1Move.UseVisualStyleBackColor = false;
            // 
            // txtldsProcess31
            // 
            this.txtldsProcess31.Location = new System.Drawing.Point(97, 86);
            this.txtldsProcess31.Name = "txtldsProcess31";
            this.txtldsProcess31.Size = new System.Drawing.Size(69, 29);
            this.txtldsProcess31.TabIndex = 22;
            // 
            // btnLDSProcess4Read
            // 
            this.btnLDSProcess4Read.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnLDSProcess4Read.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnLDSProcess4Read.ForeColor = System.Drawing.Color.White;
            this.btnLDSProcess4Read.Location = new System.Drawing.Point(241, 118);
            this.btnLDSProcess4Read.Name = "btnLDSProcess4Read";
            this.btnLDSProcess4Read.Size = new System.Drawing.Size(49, 26);
            this.btnLDSProcess4Read.TabIndex = 174;
            this.btnLDSProcess4Read.Text = "읽기";
            this.btnLDSProcess4Read.UseVisualStyleBackColor = false;
            // 
            // txtldsProcess22
            // 
            this.txtldsProcess22.Location = new System.Drawing.Point(172, 54);
            this.txtldsProcess22.Name = "txtldsProcess22";
            this.txtldsProcess22.Size = new System.Drawing.Size(69, 29);
            this.txtldsProcess22.TabIndex = 19;
            // 
            // btnLDSProcess3Read
            // 
            this.btnLDSProcess3Read.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnLDSProcess3Read.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnLDSProcess3Read.ForeColor = System.Drawing.Color.White;
            this.btnLDSProcess3Read.Location = new System.Drawing.Point(241, 86);
            this.btnLDSProcess3Read.Name = "btnLDSProcess3Read";
            this.btnLDSProcess3Read.Size = new System.Drawing.Size(49, 26);
            this.btnLDSProcess3Read.TabIndex = 173;
            this.btnLDSProcess3Read.Text = "읽기";
            this.btnLDSProcess3Read.UseVisualStyleBackColor = false;
            // 
            // txtldsProcess21
            // 
            this.txtldsProcess21.Location = new System.Drawing.Point(97, 54);
            this.txtldsProcess21.Name = "txtldsProcess21";
            this.txtldsProcess21.Size = new System.Drawing.Size(69, 29);
            this.txtldsProcess21.TabIndex = 18;
            // 
            // btnLDSProcess2Read
            // 
            this.btnLDSProcess2Read.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnLDSProcess2Read.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnLDSProcess2Read.ForeColor = System.Drawing.Color.White;
            this.btnLDSProcess2Read.Location = new System.Drawing.Point(242, 54);
            this.btnLDSProcess2Read.Name = "btnLDSProcess2Read";
            this.btnLDSProcess2Read.Size = new System.Drawing.Size(49, 26);
            this.btnLDSProcess2Read.TabIndex = 172;
            this.btnLDSProcess2Read.Text = "읽기";
            this.btnLDSProcess2Read.UseVisualStyleBackColor = false;
            // 
            // txtldsProcess12
            // 
            this.txtldsProcess12.Location = new System.Drawing.Point(172, 22);
            this.txtldsProcess12.Name = "txtldsProcess12";
            this.txtldsProcess12.Size = new System.Drawing.Size(69, 29);
            this.txtldsProcess12.TabIndex = 15;
            // 
            // btnLDSProcess1Read
            // 
            this.btnLDSProcess1Read.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnLDSProcess1Read.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnLDSProcess1Read.ForeColor = System.Drawing.Color.White;
            this.btnLDSProcess1Read.Location = new System.Drawing.Point(242, 22);
            this.btnLDSProcess1Read.Name = "btnLDSProcess1Read";
            this.btnLDSProcess1Read.Size = new System.Drawing.Size(49, 26);
            this.btnLDSProcess1Read.TabIndex = 171;
            this.btnLDSProcess1Read.Text = "읽기";
            this.btnLDSProcess1Read.UseVisualStyleBackColor = false;
            // 
            // lb_lds_process4
            // 
            this.lb_lds_process4.AutoSize = true;
            this.lb_lds_process4.Font = new System.Drawing.Font("Malgun Gothic", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_lds_process4.Location = new System.Drawing.Point(2, 124);
            this.lb_lds_process4.Name = "lb_lds_process4";
            this.lb_lds_process4.Size = new System.Drawing.Size(88, 15);
            this.lb_lds_process4.TabIndex = 14;
            this.lb_lds_process4.Text = "Process4[mm]";
            // 
            // lb_lds_process3
            // 
            this.lb_lds_process3.AutoSize = true;
            this.lb_lds_process3.Font = new System.Drawing.Font("Malgun Gothic", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_lds_process3.Location = new System.Drawing.Point(2, 92);
            this.lb_lds_process3.Name = "lb_lds_process3";
            this.lb_lds_process3.Size = new System.Drawing.Size(88, 15);
            this.lb_lds_process3.TabIndex = 13;
            this.lb_lds_process3.Text = "Process3[mm]";
            // 
            // lb_lds_process2
            // 
            this.lb_lds_process2.AutoSize = true;
            this.lb_lds_process2.Font = new System.Drawing.Font("Malgun Gothic", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_lds_process2.Location = new System.Drawing.Point(2, 60);
            this.lb_lds_process2.Name = "lb_lds_process2";
            this.lb_lds_process2.Size = new System.Drawing.Size(88, 15);
            this.lb_lds_process2.TabIndex = 12;
            this.lb_lds_process2.Text = "Process2[mm]";
            // 
            // txtldsProcess11
            // 
            this.txtldsProcess11.Location = new System.Drawing.Point(97, 22);
            this.txtldsProcess11.Name = "txtldsProcess11";
            this.txtldsProcess11.Size = new System.Drawing.Size(69, 29);
            this.txtldsProcess11.TabIndex = 4;
            // 
            // lb_lds_process1
            // 
            this.lb_lds_process1.AutoSize = true;
            this.lb_lds_process1.Font = new System.Drawing.Font("Malgun Gothic", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_lds_process1.Location = new System.Drawing.Point(2, 28);
            this.lb_lds_process1.Name = "lb_lds_process1";
            this.lb_lds_process1.Size = new System.Drawing.Size(88, 15);
            this.lb_lds_process1.TabIndex = 1;
            this.lb_lds_process1.Text = "Process1[mm]";
            // 
            // gxtpicker
            // 
            this.gxtpicker.BackColor = System.Drawing.Color.Transparent;
            this.gxtpicker.Controls.Add(this.btnPickerCCW901);
            this.gxtpicker.Controls.Add(this.btnPickerNormal1);
            this.gxtpicker.Controls.Add(this.btnPickerCCW902);
            this.gxtpicker.Controls.Add(this.btnPickerCWW902);
            this.gxtpicker.Controls.Add(this.btnPickerCW1801);
            this.gxtpicker.Controls.Add(this.btnPickerCW901);
            this.gxtpicker.Controls.Add(this.btnPickerCw1802);
            this.gxtpicker.Controls.Add(this.btnPickerNormal2);
            this.gxtpicker.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.gxtpicker.ForeColor = System.Drawing.Color.White;
            this.gxtpicker.Location = new System.Drawing.Point(1394, 186);
            this.gxtpicker.Name = "gxtpicker";
            this.gxtpicker.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gxtpicker.Size = new System.Drawing.Size(343, 171);
            this.gxtpicker.TabIndex = 64;
            this.gxtpicker.TabStop = false;
            this.gxtpicker.Text = "Picker";
            // 
            // btnPickerCCW901
            // 
            this.btnPickerCCW901.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnPickerCCW901.Font = new System.Drawing.Font("Malgun Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.btnPickerCCW901.ForeColor = System.Drawing.Color.White;
            this.btnPickerCCW901.Location = new System.Drawing.Point(9, 93);
            this.btnPickerCCW901.Name = "btnPickerCCW901";
            this.btnPickerCCW901.Size = new System.Drawing.Size(157, 30);
            this.btnPickerCCW901.TabIndex = 175;
            this.btnPickerCCW901.Text = "CCW_90";
            this.btnPickerCCW901.UseVisualStyleBackColor = false;
            // 
            // btnPickerNormal1
            // 
            this.btnPickerNormal1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnPickerNormal1.Font = new System.Drawing.Font("Malgun Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.btnPickerNormal1.ForeColor = System.Drawing.Color.White;
            this.btnPickerNormal1.Location = new System.Drawing.Point(9, 25);
            this.btnPickerNormal1.Name = "btnPickerNormal1";
            this.btnPickerNormal1.Size = new System.Drawing.Size(157, 30);
            this.btnPickerNormal1.TabIndex = 171;
            this.btnPickerNormal1.Text = "Normal";
            this.btnPickerNormal1.UseVisualStyleBackColor = false;
            // 
            // btnPickerCCW902
            // 
            this.btnPickerCCW902.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnPickerCCW902.Font = new System.Drawing.Font("Malgun Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.btnPickerCCW902.ForeColor = System.Drawing.Color.White;
            this.btnPickerCCW902.Location = new System.Drawing.Point(172, 93);
            this.btnPickerCCW902.Name = "btnPickerCCW902";
            this.btnPickerCCW902.Size = new System.Drawing.Size(157, 30);
            this.btnPickerCCW902.TabIndex = 176;
            this.btnPickerCCW902.Text = "CCW_90";
            this.btnPickerCCW902.UseVisualStyleBackColor = false;
            // 
            // btnPickerCWW902
            // 
            this.btnPickerCWW902.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnPickerCWW902.Font = new System.Drawing.Font("Malgun Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.btnPickerCWW902.ForeColor = System.Drawing.Color.White;
            this.btnPickerCWW902.Location = new System.Drawing.Point(172, 59);
            this.btnPickerCWW902.Name = "btnPickerCWW902";
            this.btnPickerCWW902.Size = new System.Drawing.Size(157, 30);
            this.btnPickerCWW902.TabIndex = 174;
            this.btnPickerCWW902.Text = "CW_90";
            this.btnPickerCWW902.UseVisualStyleBackColor = false;
            // 
            // btnPickerCW1801
            // 
            this.btnPickerCW1801.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnPickerCW1801.Font = new System.Drawing.Font("Malgun Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.btnPickerCW1801.ForeColor = System.Drawing.Color.White;
            this.btnPickerCW1801.Location = new System.Drawing.Point(9, 127);
            this.btnPickerCW1801.Name = "btnPickerCW1801";
            this.btnPickerCW1801.Size = new System.Drawing.Size(157, 30);
            this.btnPickerCW1801.TabIndex = 177;
            this.btnPickerCW1801.Text = "CW_180";
            this.btnPickerCW1801.UseVisualStyleBackColor = false;
            // 
            // btnPickerCW901
            // 
            this.btnPickerCW901.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnPickerCW901.Font = new System.Drawing.Font("Malgun Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.btnPickerCW901.ForeColor = System.Drawing.Color.White;
            this.btnPickerCW901.Location = new System.Drawing.Point(9, 59);
            this.btnPickerCW901.Name = "btnPickerCW901";
            this.btnPickerCW901.Size = new System.Drawing.Size(157, 30);
            this.btnPickerCW901.TabIndex = 173;
            this.btnPickerCW901.Text = "CW_90";
            this.btnPickerCW901.UseVisualStyleBackColor = false;
            // 
            // btnPickerCw1802
            // 
            this.btnPickerCw1802.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnPickerCw1802.Font = new System.Drawing.Font("Malgun Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.btnPickerCw1802.ForeColor = System.Drawing.Color.White;
            this.btnPickerCw1802.Location = new System.Drawing.Point(172, 127);
            this.btnPickerCw1802.Name = "btnPickerCw1802";
            this.btnPickerCw1802.Size = new System.Drawing.Size(157, 30);
            this.btnPickerCw1802.TabIndex = 178;
            this.btnPickerCw1802.Text = "CW_180";
            this.btnPickerCw1802.UseVisualStyleBackColor = false;
            // 
            // btnPickerNormal2
            // 
            this.btnPickerNormal2.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnPickerNormal2.Font = new System.Drawing.Font("Malgun Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.btnPickerNormal2.ForeColor = System.Drawing.Color.White;
            this.btnPickerNormal2.Location = new System.Drawing.Point(172, 25);
            this.btnPickerNormal2.Name = "btnPickerNormal2";
            this.btnPickerNormal2.Size = new System.Drawing.Size(157, 30);
            this.btnPickerNormal2.TabIndex = 172;
            this.btnPickerNormal2.Text = "Normal";
            this.btnPickerNormal2.UseVisualStyleBackColor = false;
            // 
            // gxtcassette
            // 
            this.gxtcassette.BackColor = System.Drawing.Color.Transparent;
            this.gxtcassette.Controls.Add(this.btnCassette4);
            this.gxtcassette.Controls.Add(this.btnCassette1);
            this.gxtcassette.Controls.Add(this.btnCassette2);
            this.gxtcassette.Controls.Add(this.btnCassetteReverse);
            this.gxtcassette.Controls.Add(this.btnCassetteNormal);
            this.gxtcassette.Controls.Add(this.txtcellCount);
            this.gxtcassette.Controls.Add(this.lb_cell_count);
            this.gxtcassette.Controls.Add(this.txtmaxHeightCount);
            this.gxtcassette.Controls.Add(this.lb_max_height_count);
            this.gxtcassette.Controls.Add(this.txtheight);
            this.gxtcassette.Controls.Add(this.lb_height);
            this.gxtcassette.Controls.Add(this.txtwidth);
            this.gxtcassette.Controls.Add(this.lb_width);
            this.gxtcassette.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.gxtcassette.ForeColor = System.Drawing.Color.White;
            this.gxtcassette.Location = new System.Drawing.Point(1394, 3);
            this.gxtcassette.Name = "gxtcassette";
            this.gxtcassette.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gxtcassette.Size = new System.Drawing.Size(343, 182);
            this.gxtcassette.TabIndex = 63;
            this.gxtcassette.TabStop = false;
            this.gxtcassette.Text = "Cassette";
            // 
            // btnCassette4
            // 
            this.btnCassette4.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnCassette4.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btnCassette4.ForeColor = System.Drawing.Color.White;
            this.btnCassette4.Location = new System.Drawing.Point(233, 150);
            this.btnCassette4.Name = "btnCassette4";
            this.btnCassette4.Size = new System.Drawing.Size(105, 27);
            this.btnCassette4.TabIndex = 173;
            this.btnCassette4.Text = "Cassette_4";
            this.btnCassette4.UseVisualStyleBackColor = false;
            // 
            // btnCassette1
            // 
            this.btnCassette1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnCassette1.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btnCassette1.ForeColor = System.Drawing.Color.White;
            this.btnCassette1.Location = new System.Drawing.Point(9, 150);
            this.btnCassette1.Name = "btnCassette1";
            this.btnCassette1.Size = new System.Drawing.Size(105, 27);
            this.btnCassette1.TabIndex = 171;
            this.btnCassette1.Text = "Cassette_1";
            this.btnCassette1.UseVisualStyleBackColor = false;
            // 
            // btnCassette2
            // 
            this.btnCassette2.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnCassette2.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.btnCassette2.ForeColor = System.Drawing.Color.White;
            this.btnCassette2.Location = new System.Drawing.Point(121, 150);
            this.btnCassette2.Name = "btnCassette2";
            this.btnCassette2.Size = new System.Drawing.Size(105, 27);
            this.btnCassette2.TabIndex = 172;
            this.btnCassette2.Text = "Cassette_2";
            this.btnCassette2.UseVisualStyleBackColor = false;
            // 
            // btnCassetteReverse
            // 
            this.btnCassetteReverse.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnCassetteReverse.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnCassetteReverse.ForeColor = System.Drawing.Color.White;
            this.btnCassetteReverse.Location = new System.Drawing.Point(247, 84);
            this.btnCassetteReverse.Name = "btnCassetteReverse";
            this.btnCassetteReverse.Size = new System.Drawing.Size(90, 58);
            this.btnCassetteReverse.TabIndex = 172;
            this.btnCassetteReverse.Text = "REVERSE(60)";
            this.btnCassetteReverse.UseVisualStyleBackColor = false;
            // 
            // btnCassetteNormal
            // 
            this.btnCassetteNormal.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnCassetteNormal.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.btnCassetteNormal.ForeColor = System.Drawing.Color.White;
            this.btnCassetteNormal.Location = new System.Drawing.Point(247, 20);
            this.btnCassetteNormal.Name = "btnCassetteNormal";
            this.btnCassetteNormal.Size = new System.Drawing.Size(90, 58);
            this.btnCassetteNormal.TabIndex = 171;
            this.btnCassetteNormal.Text = "NORMAL(40)";
            this.btnCassetteNormal.UseVisualStyleBackColor = false;
            // 
            // txtcellCount
            // 
            this.txtcellCount.Location = new System.Drawing.Point(141, 116);
            this.txtcellCount.Name = "txtcellCount";
            this.txtcellCount.Size = new System.Drawing.Size(100, 29);
            this.txtcellCount.TabIndex = 11;
            // 
            // lb_cell_count
            // 
            this.lb_cell_count.AutoSize = true;
            this.lb_cell_count.Font = new System.Drawing.Font("Malgun Gothic", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_cell_count.Location = new System.Drawing.Point(6, 123);
            this.lb_cell_count.Name = "lb_cell_count";
            this.lb_cell_count.Size = new System.Drawing.Size(66, 15);
            this.lb_cell_count.TabIndex = 10;
            this.lb_cell_count.Text = "Cell Count";
            // 
            // txtmaxHeightCount
            // 
            this.txtmaxHeightCount.Location = new System.Drawing.Point(141, 84);
            this.txtmaxHeightCount.Name = "txtmaxHeightCount";
            this.txtmaxHeightCount.Size = new System.Drawing.Size(100, 29);
            this.txtmaxHeightCount.TabIndex = 9;
            // 
            // lb_max_height_count
            // 
            this.lb_max_height_count.AutoSize = true;
            this.lb_max_height_count.Font = new System.Drawing.Font("Malgun Gothic", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_max_height_count.Location = new System.Drawing.Point(6, 91);
            this.lb_max_height_count.Name = "lb_max_height_count";
            this.lb_max_height_count.Size = new System.Drawing.Size(113, 15);
            this.lb_max_height_count.TabIndex = 7;
            this.lb_max_height_count.Text = "Max Height Count";
            // 
            // txtheight
            // 
            this.txtheight.Location = new System.Drawing.Point(141, 52);
            this.txtheight.Name = "txtheight";
            this.txtheight.Size = new System.Drawing.Size(100, 29);
            this.txtheight.TabIndex = 6;
            // 
            // lb_height
            // 
            this.lb_height.AutoSize = true;
            this.lb_height.Font = new System.Drawing.Font("Malgun Gothic", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_height.Location = new System.Drawing.Point(6, 58);
            this.lb_height.Name = "lb_height";
            this.lb_height.Size = new System.Drawing.Size(76, 15);
            this.lb_height.TabIndex = 5;
            this.lb_height.Text = "Height[mm]";
            // 
            // txtwidth
            // 
            this.txtwidth.Location = new System.Drawing.Point(141, 20);
            this.txtwidth.Name = "txtwidth";
            this.txtwidth.Size = new System.Drawing.Size(100, 29);
            this.txtwidth.TabIndex = 4;
            // 
            // lb_width
            // 
            this.lb_width.AutoSize = true;
            this.lb_width.Font = new System.Drawing.Font("Malgun Gothic", 8.999999F, System.Drawing.FontStyle.Bold);
            this.lb_width.Location = new System.Drawing.Point(6, 27);
            this.lb_width.Name = "lb_width";
            this.lb_width.Size = new System.Drawing.Size(72, 15);
            this.lb_width.TabIndex = 1;
            this.lb_width.Text = "Width[mm]";
            // 
            // listView2
            // 
            this.listView2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader5,
            this.columnHeader6});
            this.listView2.FullRowSelect = true;
            this.listView2.GridLines = true;
            this.listView2.Location = new System.Drawing.Point(3, 642);
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(275, 215);
            this.listView2.TabIndex = 1;
            this.listView2.UseCompatibleStateImageBehavior = false;
            this.listView2.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "No.";
            this.columnHeader5.Width = 45;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Name";
            this.columnHeader6.Width = 130;
            // 
            // lvRecipe
            // 
            this.lvRecipe.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.lvRecipe.FullRowSelect = true;
            this.lvRecipe.GridLines = true;
            this.lvRecipe.Location = new System.Drawing.Point(3, 3);
            this.lvRecipe.Name = "lvRecipe";
            this.lvRecipe.Size = new System.Drawing.Size(275, 624);
            this.lvRecipe.TabIndex = 0;
            this.lvRecipe.UseCompatibleStateImageBehavior = false;
            this.lvRecipe.View = System.Windows.Forms.View.Details;
            this.lvRecipe.SelectedIndexChanged += new System.EventHandler(this.lvRecipe_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "No.";
            this.columnHeader1.Width = 45;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Name";
            this.columnHeader2.Width = 130;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "CIM";
            this.columnHeader3.Width = 45;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Use";
            this.columnHeader4.Width = 45;
            // 
            // txtrecipe
            // 
            this.txtrecipe.Location = new System.Drawing.Point(65, 23);
            this.txtrecipe.Name = "txtrecipe";
            this.txtrecipe.Size = new System.Drawing.Size(754, 26);
            this.txtrecipe.TabIndex = 172;
            // 
            // Recipe_Recipe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "Recipe_Recipe";
            this.Size = new System.Drawing.Size(1740, 860);
            this.splitContainer1.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.gxtlayerinfo.ResumeLayout(false);
            this.gxtlayerinfo.PerformLayout();
            this.gxtitem.ResumeLayout(false);
            this.gxtitem.PerformLayout();
            this.gxty_offset.ResumeLayout(false);
            this.gxty_offset.PerformLayout();
            this.gxtlds_break.ResumeLayout(false);
            this.gxtlds_break.PerformLayout();
            this.panel19.ResumeLayout(false);
            this.gxtlds_process.ResumeLayout(false);
            this.gxtlds_process.PerformLayout();
            this.gxtpicker.ResumeLayout(false);
            this.gxtcassette.ResumeLayout(false);
            this.gxtcassette.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ListView lvRecipe;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.GroupBox gxtcassette;
        private System.Windows.Forms.Label lb_width;
        private System.Windows.Forms.TextBox txtcellCount;
        private System.Windows.Forms.Label lb_cell_count;
        private System.Windows.Forms.TextBox txtmaxHeightCount;
        private System.Windows.Forms.Label lb_max_height_count;
        private System.Windows.Forms.TextBox txtheight;
        private System.Windows.Forms.Label lb_height;
        private System.Windows.Forms.TextBox txtwidth;
        private System.Windows.Forms.GroupBox gxtpicker;
        private System.Windows.Forms.GroupBox gxtlds_process;
        private System.Windows.Forms.TextBox txtldsProcess11;
        private System.Windows.Forms.Label lb_lds_process1;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Button btnRecipeDelete;
        private System.Windows.Forms.Button btnRecipeApply;
        private System.Windows.Forms.Button btnRecipeSave;
        private System.Windows.Forms.Button btnRecipeCopy;
        private System.Windows.Forms.Button btnRecipeMake;
        private System.Windows.Forms.Button btnRecipeCimDelete;
        private System.Windows.Forms.Button btnRecipeCimApply;
        private System.Windows.Forms.GroupBox gxty_offset;
        private System.Windows.Forms.TextBox txtyOffsetBreak;
        private System.Windows.Forms.Label lb_y_offset_break;
        private System.Windows.Forms.TextBox txtyOffsetPre;
        private System.Windows.Forms.Label lb_y_offset_pre;
        private System.Windows.Forms.GroupBox gxtlds_break;
        private System.Windows.Forms.TextBox txtldsProcess42;
        private System.Windows.Forms.TextBox txtldsProcess41;
        private System.Windows.Forms.TextBox txtldsProcess32;
        private System.Windows.Forms.TextBox txtldsProcess31;
        private System.Windows.Forms.TextBox txtldsProcess22;
        private System.Windows.Forms.TextBox txtldsProcess21;
        private System.Windows.Forms.TextBox txtldsProcess12;
        private System.Windows.Forms.Label lb_lds_process4;
        private System.Windows.Forms.Label lb_lds_process3;
        private System.Windows.Forms.Label lb_lds_process2;
        private System.Windows.Forms.TextBox txtldsBreak42;
        private System.Windows.Forms.TextBox txtldsBreak41;
        private System.Windows.Forms.TextBox txtldsBreak32;
        private System.Windows.Forms.TextBox txtldsBreak31;
        private System.Windows.Forms.TextBox txtldsBreak22;
        private System.Windows.Forms.TextBox txtldsBreak21;
        private System.Windows.Forms.TextBox txtldsBreak12;
        private System.Windows.Forms.Label lb_lds_break4;
        private System.Windows.Forms.Label lb_lds_break3;
        private System.Windows.Forms.Label lb_lds_break2;
        private System.Windows.Forms.TextBox txtldsBreak11;
        private System.Windows.Forms.Label lb_lds_break1;
        private System.Windows.Forms.GroupBox gxtdxfinfo;
        private System.Windows.Forms.GroupBox gxtlayerinfo;
        private System.Windows.Forms.GroupBox gxtitem;
        private System.Windows.Forms.TextBox txtrecipePath;
        private System.Windows.Forms.Label lb_recipe;
        private System.Windows.Forms.TextBox txttable2Alignmark22;
        private System.Windows.Forms.TextBox txttable2Alignmark21;
        private System.Windows.Forms.Label lb_table2_alignmark2;
        private System.Windows.Forms.TextBox txttable2Alignmark12;
        private System.Windows.Forms.TextBox txttable2Alignmark11;
        private System.Windows.Forms.Label lb_table2_alignmark1;
        private System.Windows.Forms.TextBox txttable1Alignmark22;
        private System.Windows.Forms.TextBox txttable1Alignmark21;
        private System.Windows.Forms.TextBox txttable1Alignmark12;
        private System.Windows.Forms.Label lb_table1_alignmark2;
        private System.Windows.Forms.TextBox txttable1Alignmark11;
        private System.Windows.Forms.Label lb_table1_alignmark1;
        private System.Windows.Forms.TextBox txtcellSize2;
        private System.Windows.Forms.TextBox txtcellSize1;
        private System.Windows.Forms.Label lb_cell_size;
        private System.Windows.Forms.Label lb_a_matrix;
        private System.Windows.Forms.TextBox txtbreakRightY;
        private System.Windows.Forms.TextBox txtbreakLeftY;
        private System.Windows.Forms.Label lb_break_right_y;
        private System.Windows.Forms.Label lb_break_left_y;
        private System.Windows.Forms.TextBox txtbreakRight22;
        private System.Windows.Forms.TextBox txtbreakLeft22;
        private System.Windows.Forms.TextBox txtbreakRight21;
        private System.Windows.Forms.Label lb_break_right_2;
        private System.Windows.Forms.TextBox txtbreakLeft21;
        private System.Windows.Forms.TextBox txtbreakRight12;
        private System.Windows.Forms.TextBox txtbreakRight11;
        private System.Windows.Forms.Label lb_break_left_2;
        private System.Windows.Forms.Label lb_break_right_1;
        private System.Windows.Forms.TextBox txtbreakLeft12;
        private System.Windows.Forms.TextBox txtbreakLeft11;
        private System.Windows.Forms.Label lb_break_left_1;
        private System.Windows.Forms.TextBox txtdxfpath;
        private System.Windows.Forms.Label lb_dxfpath;
        private System.Windows.Forms.Label lb_ta_thickness;
        private System.Windows.Forms.TextBox txttThickness;
        private System.Windows.Forms.ComboBox comxtcellType;
        private System.Windows.Forms.Label lb_cell_type;
        private System.Windows.Forms.ComboBox comxtprocessParam;
        private System.Windows.Forms.Label lb_process_param;
        private System.Windows.Forms.ComboBox comxtguideLayer;
        private System.Windows.Forms.Label lb_guide_layer;
        private System.Windows.Forms.ComboBox comxtprocessLayer;
        private System.Windows.Forms.Label lb_process_layer;
        private System.Windows.Forms.ComboBox comxtalignmentLayer;
        private System.Windows.Forms.Label lb_alignment_layer;
        private System.Windows.Forms.Button btnCassette1;
        private System.Windows.Forms.Button btnAlign;
        private System.Windows.Forms.Button btnRecipeLoad;
        private System.Windows.Forms.Button btnBreakRight2Move;
        private System.Windows.Forms.Button btnBreakRight2Read;
        private System.Windows.Forms.Button btnBreakRight2Calc;
        private System.Windows.Forms.Button btnBreakLeft2Move;
        private System.Windows.Forms.Button btnTable2AlignMark2Move;
        private System.Windows.Forms.Button btnTable1AlignMark2Move;
        private System.Windows.Forms.Button btnBreakLeft2Read;
        private System.Windows.Forms.Button btnTable2AlignMark2Read;
        private System.Windows.Forms.Button btnTable1AlignMark2Read;
        private System.Windows.Forms.Button btnBreakLeft2Calc;
        private System.Windows.Forms.Button btnTable2AlignMark2Calc;
        private System.Windows.Forms.Button btnTable1AlignMark2Calc;
        private System.Windows.Forms.Button btnBreakRightYMove;
        private System.Windows.Forms.Button btnBreakRight1Move;
        private System.Windows.Forms.Button btnBreakLeftYMove;
        private System.Windows.Forms.Button btnBreakLeft1Move;
        private System.Windows.Forms.Button btnTable2Alignmark1Move;
        private System.Windows.Forms.Button btnTable1Alignmark1Move;
        private System.Windows.Forms.Button btnBreakRightYRead;
        private System.Windows.Forms.Button btnBreakRight1Read;
        private System.Windows.Forms.Button btnBreakLeftYRead;
        private System.Windows.Forms.Button btnBreakLeft1Read;
        private System.Windows.Forms.Button btnTable2Alignmark1Read;
        private System.Windows.Forms.Button btnTable1Alignmark1Read;
        private System.Windows.Forms.Button btnBreakLeft1Calc;
        private System.Windows.Forms.Button btnTable2Alignmark1Calc;
        private System.Windows.Forms.Button btnTable1Alignmark1Calc;
        private System.Windows.Forms.Button btnCassetteReverse;
        private System.Windows.Forms.Button btnCassetteNormal;
        private System.Windows.Forms.Button btnCassette4;
        private System.Windows.Forms.Button btnCassette2;
        private System.Windows.Forms.Button btnDxfPathLoad;
        private System.Windows.Forms.Button btnLDSBreak4Move;
        private System.Windows.Forms.Button btnLDSBreak1Move;
        private System.Windows.Forms.Button btnLDSBreak3Move;
        private System.Windows.Forms.Button btnLDSBreak1Read;
        private System.Windows.Forms.Button btnLDSBreak2Move;
        private System.Windows.Forms.Button btnLDSBreak2Read;
        private System.Windows.Forms.Button btnLDSBreak3Read;
        private System.Windows.Forms.Button btnLDSBreak4Read;
        private System.Windows.Forms.Button btnLDSProcess4Move;
        private System.Windows.Forms.Button btnLDSProcess3Move;
        private System.Windows.Forms.Button btnLDSProcess2Move;
        private System.Windows.Forms.Button btnLDSProcess1Move;
        private System.Windows.Forms.Button btnLDSProcess4Read;
        private System.Windows.Forms.Button btnLDSProcess3Read;
        private System.Windows.Forms.Button btnLDSProcess2Read;
        private System.Windows.Forms.Button btnLDSProcess1Read;
        private System.Windows.Forms.Button btnPickerCCW901;
        private System.Windows.Forms.Button btnPickerNormal1;
        private System.Windows.Forms.Button btnPickerCCW902;
        private System.Windows.Forms.Button btnPickerCWW902;
        private System.Windows.Forms.Button btnPickerCW1801;
        private System.Windows.Forms.Button btnPickerCW901;
        private System.Windows.Forms.Button btnPickerCw1802;
        private System.Windows.Forms.Button btnPickerNormal2;
        private System.Windows.Forms.Button btnZMatrix;
        private System.Windows.Forms.Button btnBreakRightYCalc;
        private System.Windows.Forms.Button btnBreakRight1Calc;
        private System.Windows.Forms.Button btnBreakLeftYCalc;
        private System.Windows.Forms.TextBox txtrecipe;
    }
}
