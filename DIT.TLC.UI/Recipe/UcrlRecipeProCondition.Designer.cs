﻿namespace DIT.TLC.UI
{
    partial class Recipe_ProCondition
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel_process = new System.Windows.Forms.Panel();
            this.btnProcessSave = new System.Windows.Forms.Button();
            this.txtprocessAccDec = new System.Windows.Forms.TextBox();
            this.lb_process_acc_dec = new System.Windows.Forms.Label();
            this.txtprocessScan = new System.Windows.Forms.TextBox();
            this.lb_process_scan = new System.Windows.Forms.Label();
            this.txtprocessG = new System.Windows.Forms.TextBox();
            this.lb_process_g = new System.Windows.Forms.Label();
            this.txtprocessSegmentValue = new System.Windows.Forms.TextBox();
            this.lb_process_segment_value = new System.Windows.Forms.Label();
            this.txtprocessOverlap = new System.Windows.Forms.TextBox();
            this.lb_process_overlap = new System.Windows.Forms.Label();
            this.txtprocessZpos = new System.Windows.Forms.TextBox();
            this.lb_process_zpos = new System.Windows.Forms.Label();
            this.txtprocessSpeed = new System.Windows.Forms.TextBox();
            this.lb_process_speed = new System.Windows.Forms.Label();
            this.txtprocessFrequence = new System.Windows.Forms.TextBox();
            this.lb_process_frequence = new System.Windows.Forms.Label();
            this.txtprocessBurst = new System.Windows.Forms.TextBox();
            this.lb_process_burst = new System.Windows.Forms.Label();
            this.txtprocessError = new System.Windows.Forms.TextBox();
            this.lb_process_error = new System.Windows.Forms.Label();
            this.txtprocessPowerTatio = new System.Windows.Forms.TextBox();
            this.lb_process_power_ratio = new System.Windows.Forms.Label();
            this.txtprocessPower = new System.Windows.Forms.TextBox();
            this.lb_process_power = new System.Windows.Forms.Label();
            this.txtprocessDivider = new System.Windows.Forms.TextBox();
            this.btnProcessDelete = new System.Windows.Forms.Button();
            this.lb_process_divider = new System.Windows.Forms.Label();
            this.btnProcessCopy = new System.Windows.Forms.Button();
            this.btnProcessMake = new System.Windows.Forms.Button();
            this.lvProcess = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel_breaking = new System.Windows.Forms.Panel();
            this.btnBreakingXytBreaking4Calc = new System.Windows.Forms.Button();
            this.btnBreakingXytBreaking3Calc = new System.Windows.Forms.Button();
            this.btnBreakingXytBreaking2Calc = new System.Windows.Forms.Button();
            this.btnBreakingXytBreaking1Calc = new System.Windows.Forms.Button();
            this.gxtbreaking_load_trans_sequence_offset = new System.Windows.Forms.GroupBox();
            this.gxtbreaking_load_trans_sequence_offset_b = new System.Windows.Forms.GroupBox();
            this.lb_breaking_load_trans_sequence_offset_b_y2_mm = new System.Windows.Forms.Label();
            this.txtbreakingLoadTransSequenceOffsetBY1 = new System.Windows.Forms.TextBox();
            this.lb_breaking_load_trans_sequence_offset_b_y1_mm = new System.Windows.Forms.Label();
            this.lb_breaking_load_trans_sequence_offset_b_y1 = new System.Windows.Forms.Label();
            this.txtbreakingLoadTransSequenceOffsetBY2 = new System.Windows.Forms.TextBox();
            this.lb_breaking_load_trans_sequence_offset_b_y2 = new System.Windows.Forms.Label();
            this.gxtbreaking_load_trans_sequence_offset_a = new System.Windows.Forms.GroupBox();
            this.lb_breaking_load_trans_sequence_offset_a_y2_mm = new System.Windows.Forms.Label();
            this.lb_breaking_load_trans_sequence_offset_a_y1_mm = new System.Windows.Forms.Label();
            this.txtbreakingLoadTransSequenceOffsetAY2 = new System.Windows.Forms.TextBox();
            this.txtbreakingLoadTransSequenceOffsetAY1 = new System.Windows.Forms.TextBox();
            this.lb_breaking_load_trans_sequence_offset_a_y2 = new System.Windows.Forms.Label();
            this.lb_breaking_load_trans_sequence_offset_a_y1 = new System.Windows.Forms.Label();
            this.txtbreakingSlowdownSpeed = new System.Windows.Forms.TextBox();
            this.lb_breaking_slowdown_speed = new System.Windows.Forms.Label();
            this.txtbreakingFastdownSpeed = new System.Windows.Forms.TextBox();
            this.lb_breaking_fastdown_speed = new System.Windows.Forms.Label();
            this.lb_breaking_slowdown_ptich_mm = new System.Windows.Forms.Label();
            this.lb_breaking_fastdown_ptich_mm = new System.Windows.Forms.Label();
            this.lb_breaking_slowdown_ptich = new System.Windows.Forms.Label();
            this.txtbreakingSlowdownPitch = new System.Windows.Forms.TextBox();
            this.txtbreakingFastdownPitch = new System.Windows.Forms.TextBox();
            this.lb_breaking_fastdown_ptich = new System.Windows.Forms.Label();
            this.panelBreaking2pass = new System.Windows.Forms.Panel();
            this.lb_breaking_2pass = new System.Windows.Forms.Label();
            this.txtbreakingBMcrOffsetXy2 = new System.Windows.Forms.TextBox();
            this.txtbreakingAMcrOffsetXy2 = new System.Windows.Forms.TextBox();
            this.txtbreakingBMcrOffsetXy1 = new System.Windows.Forms.TextBox();
            this.lb_breaking_b_mcr_offset_xy = new System.Windows.Forms.Label();
            this.txtbreakingAMcrOffsetXy1 = new System.Windows.Forms.TextBox();
            this.lb_breaking_a_mcr_offset_xy = new System.Windows.Forms.Label();
            this.txtbreakingB2XyOffset2 = new System.Windows.Forms.TextBox();
            this.txtbreakingB2XyOffset1 = new System.Windows.Forms.TextBox();
            this.lb_breaking_b2_xy_offset = new System.Windows.Forms.Label();
            this.txtbreakingB1XyOffset2 = new System.Windows.Forms.TextBox();
            this.txtbreakingB1XyOffset1 = new System.Windows.Forms.TextBox();
            this.lb_breaking_b1_xy_offset = new System.Windows.Forms.Label();
            this.txtbreakingA2XyOffset2 = new System.Windows.Forms.TextBox();
            this.txtbreakingA2XyOffset1 = new System.Windows.Forms.TextBox();
            this.lb_breaking_a2_xy_offset = new System.Windows.Forms.Label();
            this.txtbreakingA1XyOffset2 = new System.Windows.Forms.TextBox();
            this.txtbreakingA1XyOffset1 = new System.Windows.Forms.TextBox();
            this.lb_breaking_a1_xy_offset = new System.Windows.Forms.Label();
            this.txtbreakingXytBreaking43 = new System.Windows.Forms.TextBox();
            this.txtbreakingXytBreaking42 = new System.Windows.Forms.TextBox();
            this.txtbreakingXytBreaking41 = new System.Windows.Forms.TextBox();
            this.lb_breaking_xyt_breaking4 = new System.Windows.Forms.Label();
            this.txtbreakingXytBreaking33 = new System.Windows.Forms.TextBox();
            this.txtbreakingXytBreaking32 = new System.Windows.Forms.TextBox();
            this.txtbreakingXytBreaking31 = new System.Windows.Forms.TextBox();
            this.lb_breaking_xyt_breaking3 = new System.Windows.Forms.Label();
            this.txtbreakingXytBreaking23 = new System.Windows.Forms.TextBox();
            this.txtbreakingXytBreaking22 = new System.Windows.Forms.TextBox();
            this.panel_breaking_half = new System.Windows.Forms.Panel();
            this.lb_breaking_half = new System.Windows.Forms.Label();
            this.txtbreakingXytBreaking21 = new System.Windows.Forms.TextBox();
            this.panel_breaking_right = new System.Windows.Forms.Panel();
            this.lb_breaking_right = new System.Windows.Forms.Label();
            this.lb_breaking_xyt_breaking2 = new System.Windows.Forms.Label();
            this.txtbreakingXytBreaking13 = new System.Windows.Forms.TextBox();
            this.txtbreakingXytBreaking12 = new System.Windows.Forms.TextBox();
            this.txtbreakingXytBreaking11 = new System.Windows.Forms.TextBox();
            this.panel_breaking_left = new System.Windows.Forms.Panel();
            this.lb_breaking_left = new System.Windows.Forms.Label();
            this.txtbreakingXyMcr2 = new System.Windows.Forms.TextBox();
            this.txtbreakingXyAlignMark22 = new System.Windows.Forms.TextBox();
            this.txtbreakingXyAlignMark12 = new System.Windows.Forms.TextBox();
            this.txtbreakingXyPinCenter2 = new System.Windows.Forms.TextBox();
            this.lb_breaking_xyt_breaking1 = new System.Windows.Forms.Label();
            this.txtbreakingPinDistanceB = new System.Windows.Forms.TextBox();
            this.lb_breaking_pin_distance_b = new System.Windows.Forms.Label();
            this.txtbreakingPinDistanceA = new System.Windows.Forms.TextBox();
            this.lb_breaking_pin_distance_a = new System.Windows.Forms.Label();
            this.txtbreakingXyMcr1 = new System.Windows.Forms.TextBox();
            this.panel_breaking_xy_mcr = new System.Windows.Forms.Panel();
            this.lb_breaking_xy_mcr = new System.Windows.Forms.Label();
            this.txtbreakingXyAlignMark21 = new System.Windows.Forms.TextBox();
            this.panel_breaking_xy_align_mark2 = new System.Windows.Forms.Panel();
            this.lb_breaking_xy_align_mark2 = new System.Windows.Forms.Label();
            this.txtbreakingXyAlignMark11 = new System.Windows.Forms.TextBox();
            this.panel_breaking_xy_align_mark1 = new System.Windows.Forms.Panel();
            this.lb_breaking_xy_align_mark1 = new System.Windows.Forms.Label();
            this.txtbreakingXyPinCenter1 = new System.Windows.Forms.TextBox();
            this.panel_breaking_xy_pin_center = new System.Windows.Forms.Panel();
            this.lb_breaking_xy_pin_center = new System.Windows.Forms.Label();
            this.btnBreakingSave = new System.Windows.Forms.Button();
            this.btnBreakingDelete = new System.Windows.Forms.Button();
            this.btnBreakingCopy = new System.Windows.Forms.Button();
            this.btnBreakingMake = new System.Windows.Forms.Button();
            this.lvBreaking = new System.Windows.Forms.ListView();
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.textBox56 = new System.Windows.Forms.TextBox();
            this.textBox57 = new System.Windows.Forms.TextBox();
            this.textBox58 = new System.Windows.Forms.TextBox();
            this.textBox59 = new System.Windows.Forms.TextBox();
            this.button9 = new System.Windows.Forms.Button();
            this.gxtbreaking_zig = new System.Windows.Forms.GroupBox();
            this.btnBreakingZigSave = new System.Windows.Forms.Button();
            this.txtbreakingZigCamToPin = new System.Windows.Forms.TextBox();
            this.txtbreakingZigThickness = new System.Windows.Forms.TextBox();
            this.txtbreakingZigY = new System.Windows.Forms.TextBox();
            this.txtbreakingZigX = new System.Windows.Forms.TextBox();
            this.lb_breaking_zig_cam_to_pin = new System.Windows.Forms.Label();
            this.lb_breaking_zig_thickness = new System.Windows.Forms.Label();
            this.lb_breaking_zig_y = new System.Windows.Forms.Label();
            this.lb_breaking_zig_x = new System.Windows.Forms.Label();
            this.panel_default_process = new System.Windows.Forms.Panel();
            this.txtdefaultProcessPrealignErrorY = new System.Windows.Forms.TextBox();
            this.txtdefaultProcessAlignAngle = new System.Windows.Forms.TextBox();
            this.lb_default_process_prealign_error_y = new System.Windows.Forms.Label();
            this.lb_default_process_align_angle = new System.Windows.Forms.Label();
            this.txtdefaultProcessPrealignErrorX = new System.Windows.Forms.TextBox();
            this.txtdefaultProcessAlignDistance = new System.Windows.Forms.TextBox();
            this.lb_default_process_prealign_error_x = new System.Windows.Forms.Label();
            this.lb_default_process_align_distance = new System.Windows.Forms.Label();
            this.txtdefaultProcessBreakErrorY = new System.Windows.Forms.TextBox();
            this.lb_default_process_break_error_y = new System.Windows.Forms.Label();
            this.txtdefaultProcessPrealignErrorAngle = new System.Windows.Forms.TextBox();
            this.txtdefaultProcessAlignMatch = new System.Windows.Forms.TextBox();
            this.lb_default_process_prealign_error_angle = new System.Windows.Forms.Label();
            this.lb_default_process_align_match = new System.Windows.Forms.Label();
            this.btnDefaultProcessSave = new System.Windows.Forms.Button();
            this.txtdefaultProcessDefaultZ = new System.Windows.Forms.TextBox();
            this.txtdefaultProcessJumpSpeed = new System.Windows.Forms.TextBox();
            this.lb_default_process_default_z = new System.Windows.Forms.Label();
            this.lb_default_process_jump_speed = new System.Windows.Forms.Label();
            this.panel_default_laser = new System.Windows.Forms.Panel();
            this.btnDefaultLaserSave = new System.Windows.Forms.Button();
            this.txtdefaultLaserCstPitchReverse = new System.Windows.Forms.TextBox();
            this.txtdefaultLaserCstPitchNormal = new System.Windows.Forms.TextBox();
            this.txtdefaultLaserOntime = new System.Windows.Forms.TextBox();
            this.lb_default_laser_cst_pitch_reverse = new System.Windows.Forms.Label();
            this.lb_default_laser_cst_pitch_normal = new System.Windows.Forms.Label();
            this.lb_default_laser_ontime = new System.Windows.Forms.Label();
            this.panel_process.SuspendLayout();
            this.panel_breaking.SuspendLayout();
            this.gxtbreaking_load_trans_sequence_offset.SuspendLayout();
            this.gxtbreaking_load_trans_sequence_offset_b.SuspendLayout();
            this.gxtbreaking_load_trans_sequence_offset_a.SuspendLayout();
            this.panel_breaking_half.SuspendLayout();
            this.panel_breaking_right.SuspendLayout();
            this.panel_breaking_left.SuspendLayout();
            this.panel_breaking_xy_mcr.SuspendLayout();
            this.panel_breaking_xy_align_mark2.SuspendLayout();
            this.panel_breaking_xy_align_mark1.SuspendLayout();
            this.panel_breaking_xy_pin_center.SuspendLayout();
            this.gxtbreaking_zig.SuspendLayout();
            this.panel_default_process.SuspendLayout();
            this.panel_default_laser.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_process
            // 
            this.panel_process.BackColor = System.Drawing.Color.DimGray;
            this.panel_process.Controls.Add(this.btnProcessSave);
            this.panel_process.Controls.Add(this.txtprocessAccDec);
            this.panel_process.Controls.Add(this.lb_process_acc_dec);
            this.panel_process.Controls.Add(this.txtprocessScan);
            this.panel_process.Controls.Add(this.lb_process_scan);
            this.panel_process.Controls.Add(this.txtprocessG);
            this.panel_process.Controls.Add(this.lb_process_g);
            this.panel_process.Controls.Add(this.txtprocessSegmentValue);
            this.panel_process.Controls.Add(this.lb_process_segment_value);
            this.panel_process.Controls.Add(this.txtprocessOverlap);
            this.panel_process.Controls.Add(this.lb_process_overlap);
            this.panel_process.Controls.Add(this.txtprocessZpos);
            this.panel_process.Controls.Add(this.lb_process_zpos);
            this.panel_process.Controls.Add(this.txtprocessSpeed);
            this.panel_process.Controls.Add(this.lb_process_speed);
            this.panel_process.Controls.Add(this.txtprocessFrequence);
            this.panel_process.Controls.Add(this.lb_process_frequence);
            this.panel_process.Controls.Add(this.txtprocessBurst);
            this.panel_process.Controls.Add(this.lb_process_burst);
            this.panel_process.Controls.Add(this.txtprocessError);
            this.panel_process.Controls.Add(this.lb_process_error);
            this.panel_process.Controls.Add(this.txtprocessPowerTatio);
            this.panel_process.Controls.Add(this.lb_process_power_ratio);
            this.panel_process.Controls.Add(this.txtprocessPower);
            this.panel_process.Controls.Add(this.lb_process_power);
            this.panel_process.Controls.Add(this.txtprocessDivider);
            this.panel_process.Controls.Add(this.btnProcessDelete);
            this.panel_process.Controls.Add(this.lb_process_divider);
            this.panel_process.Controls.Add(this.btnProcessCopy);
            this.panel_process.Controls.Add(this.btnProcessMake);
            this.panel_process.Controls.Add(this.lvProcess);
            this.panel_process.Location = new System.Drawing.Point(3, 3);
            this.panel_process.Name = "panel_process";
            this.panel_process.Size = new System.Drawing.Size(1734, 173);
            this.panel_process.TabIndex = 0;
            // 
            // btnProcessSave
            // 
            this.btnProcessSave.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnProcessSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnProcessSave.ForeColor = System.Drawing.Color.White;
            this.btnProcessSave.Location = new System.Drawing.Point(1601, 139);
            this.btnProcessSave.Name = "btnProcessSave";
            this.btnProcessSave.Size = new System.Drawing.Size(130, 30);
            this.btnProcessSave.TabIndex = 69;
            this.btnProcessSave.Text = "Save";
            this.btnProcessSave.UseVisualStyleBackColor = false;
            // 
            // txtprocessAccDec
            // 
            this.txtprocessAccDec.Location = new System.Drawing.Point(1325, 106);
            this.txtprocessAccDec.Name = "txtprocessAccDec";
            this.txtprocessAccDec.Size = new System.Drawing.Size(130, 21);
            this.txtprocessAccDec.TabIndex = 68;
            // 
            // lb_process_acc_dec
            // 
            this.lb_process_acc_dec.AutoSize = true;
            this.lb_process_acc_dec.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_acc_dec.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_acc_dec.ForeColor = System.Drawing.Color.White;
            this.lb_process_acc_dec.Location = new System.Drawing.Point(1250, 108);
            this.lb_process_acc_dec.Name = "lb_process_acc_dec";
            this.lb_process_acc_dec.Size = new System.Drawing.Size(66, 13);
            this.lb_process_acc_dec.TabIndex = 67;
            this.lb_process_acc_dec.Text = "Acc dec";
            // 
            // txtprocessScan
            // 
            this.txtprocessScan.Location = new System.Drawing.Point(1325, 75);
            this.txtprocessScan.Name = "txtprocessScan";
            this.txtprocessScan.Size = new System.Drawing.Size(130, 21);
            this.txtprocessScan.TabIndex = 66;
            // 
            // lb_process_scan
            // 
            this.lb_process_scan.AutoSize = true;
            this.lb_process_scan.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_scan.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_scan.ForeColor = System.Drawing.Color.White;
            this.lb_process_scan.Location = new System.Drawing.Point(1272, 77);
            this.lb_process_scan.Name = "lb_process_scan";
            this.lb_process_scan.Size = new System.Drawing.Size(44, 13);
            this.lb_process_scan.TabIndex = 65;
            this.lb_process_scan.Text = "Scan";
            // 
            // txtprocessG
            // 
            this.txtprocessG.Location = new System.Drawing.Point(1325, 9);
            this.txtprocessG.Name = "txtprocessG";
            this.txtprocessG.Size = new System.Drawing.Size(130, 21);
            this.txtprocessG.TabIndex = 62;
            // 
            // lb_process_g
            // 
            this.lb_process_g.AutoSize = true;
            this.lb_process_g.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_g.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_g.ForeColor = System.Drawing.Color.White;
            this.lb_process_g.Location = new System.Drawing.Point(1298, 11);
            this.lb_process_g.Name = "lb_process_g";
            this.lb_process_g.Size = new System.Drawing.Size(18, 13);
            this.lb_process_g.TabIndex = 61;
            this.lb_process_g.Text = "G";
            // 
            // txtprocessSegmentValue
            // 
            this.txtprocessSegmentValue.Location = new System.Drawing.Point(1042, 106);
            this.txtprocessSegmentValue.Name = "txtprocessSegmentValue";
            this.txtprocessSegmentValue.Size = new System.Drawing.Size(130, 21);
            this.txtprocessSegmentValue.TabIndex = 60;
            // 
            // lb_process_segment_value
            // 
            this.lb_process_segment_value.AutoSize = true;
            this.lb_process_segment_value.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_segment_value.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_segment_value.ForeColor = System.Drawing.Color.White;
            this.lb_process_segment_value.Location = new System.Drawing.Point(922, 108);
            this.lb_process_segment_value.Name = "lb_process_segment_value";
            this.lb_process_segment_value.Size = new System.Drawing.Size(114, 13);
            this.lb_process_segment_value.TabIndex = 59;
            this.lb_process_segment_value.Text = "Segment Value";
            // 
            // txtprocessOverlap
            // 
            this.txtprocessOverlap.Location = new System.Drawing.Point(1042, 73);
            this.txtprocessOverlap.Name = "txtprocessOverlap";
            this.txtprocessOverlap.Size = new System.Drawing.Size(130, 21);
            this.txtprocessOverlap.TabIndex = 58;
            // 
            // lb_process_overlap
            // 
            this.lb_process_overlap.AutoSize = true;
            this.lb_process_overlap.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_overlap.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_overlap.ForeColor = System.Drawing.Color.White;
            this.lb_process_overlap.Location = new System.Drawing.Point(971, 75);
            this.lb_process_overlap.Name = "lb_process_overlap";
            this.lb_process_overlap.Size = new System.Drawing.Size(62, 13);
            this.lb_process_overlap.TabIndex = 57;
            this.lb_process_overlap.Text = "Overlap";
            // 
            // txtprocessZpos
            // 
            this.txtprocessZpos.Location = new System.Drawing.Point(1042, 42);
            this.txtprocessZpos.Name = "txtprocessZpos";
            this.txtprocessZpos.Size = new System.Drawing.Size(130, 21);
            this.txtprocessZpos.TabIndex = 56;
            // 
            // lb_process_zpos
            // 
            this.lb_process_zpos.AutoSize = true;
            this.lb_process_zpos.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_zpos.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_zpos.ForeColor = System.Drawing.Color.White;
            this.lb_process_zpos.Location = new System.Drawing.Point(957, 44);
            this.lb_process_zpos.Name = "lb_process_zpos";
            this.lb_process_zpos.Size = new System.Drawing.Size(85, 13);
            this.lb_process_zpos.TabIndex = 55;
            this.lb_process_zpos.Text = "ZPos[mm]";
            // 
            // txtprocessSpeed
            // 
            this.txtprocessSpeed.Location = new System.Drawing.Point(1042, 9);
            this.txtprocessSpeed.Name = "txtprocessSpeed";
            this.txtprocessSpeed.Size = new System.Drawing.Size(130, 21);
            this.txtprocessSpeed.TabIndex = 54;
            // 
            // lb_process_speed
            // 
            this.lb_process_speed.AutoSize = true;
            this.lb_process_speed.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_speed.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_speed.ForeColor = System.Drawing.Color.White;
            this.lb_process_speed.Location = new System.Drawing.Point(980, 11);
            this.lb_process_speed.Name = "lb_process_speed";
            this.lb_process_speed.Size = new System.Drawing.Size(53, 13);
            this.lb_process_speed.TabIndex = 53;
            this.lb_process_speed.Text = "Speed";
            // 
            // txtprocessFrequence
            // 
            this.txtprocessFrequence.Location = new System.Drawing.Point(771, 71);
            this.txtprocessFrequence.Name = "txtprocessFrequence";
            this.txtprocessFrequence.Size = new System.Drawing.Size(130, 21);
            this.txtprocessFrequence.TabIndex = 52;
            // 
            // lb_process_frequence
            // 
            this.lb_process_frequence.AutoSize = true;
            this.lb_process_frequence.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_frequence.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_frequence.ForeColor = System.Drawing.Color.White;
            this.lb_process_frequence.Location = new System.Drawing.Point(681, 77);
            this.lb_process_frequence.Name = "lb_process_frequence";
            this.lb_process_frequence.Size = new System.Drawing.Size(84, 13);
            this.lb_process_frequence.TabIndex = 51;
            this.lb_process_frequence.Text = "Frequence";
            // 
            // txtprocessBurst
            // 
            this.txtprocessBurst.Location = new System.Drawing.Point(512, 71);
            this.txtprocessBurst.Name = "txtprocessBurst";
            this.txtprocessBurst.Size = new System.Drawing.Size(130, 21);
            this.txtprocessBurst.TabIndex = 50;
            // 
            // lb_process_burst
            // 
            this.lb_process_burst.AutoSize = true;
            this.lb_process_burst.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_burst.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_burst.ForeColor = System.Drawing.Color.White;
            this.lb_process_burst.Location = new System.Drawing.Point(462, 75);
            this.lb_process_burst.Name = "lb_process_burst";
            this.lb_process_burst.Size = new System.Drawing.Size(45, 13);
            this.lb_process_burst.TabIndex = 49;
            this.lb_process_burst.Text = "Burst";
            // 
            // txtprocessError
            // 
            this.txtprocessError.Location = new System.Drawing.Point(512, 40);
            this.txtprocessError.Name = "txtprocessError";
            this.txtprocessError.Size = new System.Drawing.Size(130, 21);
            this.txtprocessError.TabIndex = 48;
            // 
            // lb_process_error
            // 
            this.lb_process_error.AutoSize = true;
            this.lb_process_error.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_error.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_error.ForeColor = System.Drawing.Color.White;
            this.lb_process_error.Location = new System.Drawing.Point(450, 42);
            this.lb_process_error.Name = "lb_process_error";
            this.lb_process_error.Size = new System.Drawing.Size(57, 13);
            this.lb_process_error.TabIndex = 47;
            this.lb_process_error.Text = "ERROR";
            // 
            // txtprocessPowerTatio
            // 
            this.txtprocessPowerTatio.Location = new System.Drawing.Point(275, 71);
            this.txtprocessPowerTatio.Name = "txtprocessPowerTatio";
            this.txtprocessPowerTatio.Size = new System.Drawing.Size(130, 21);
            this.txtprocessPowerTatio.TabIndex = 46;
            // 
            // lb_process_power_ratio
            // 
            this.lb_process_power_ratio.AutoSize = true;
            this.lb_process_power_ratio.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_power_ratio.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_power_ratio.ForeColor = System.Drawing.Color.White;
            this.lb_process_power_ratio.Location = new System.Drawing.Point(180, 73);
            this.lb_process_power_ratio.Name = "lb_process_power_ratio";
            this.lb_process_power_ratio.Size = new System.Drawing.Size(89, 13);
            this.lb_process_power_ratio.TabIndex = 45;
            this.lb_process_power_ratio.Text = "Power ratio";
            // 
            // txtprocessPower
            // 
            this.txtprocessPower.Location = new System.Drawing.Point(275, 40);
            this.txtprocessPower.Name = "txtprocessPower";
            this.txtprocessPower.Size = new System.Drawing.Size(130, 21);
            this.txtprocessPower.TabIndex = 44;
            // 
            // lb_process_power
            // 
            this.lb_process_power.AutoSize = true;
            this.lb_process_power.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_power.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_power.ForeColor = System.Drawing.Color.White;
            this.lb_process_power.Location = new System.Drawing.Point(190, 42);
            this.lb_process_power.Name = "lb_process_power";
            this.lb_process_power.Size = new System.Drawing.Size(79, 13);
            this.lb_process_power.TabIndex = 43;
            this.lb_process_power.Text = "Power[w]";
            // 
            // txtprocessDivider
            // 
            this.txtprocessDivider.Location = new System.Drawing.Point(275, 9);
            this.txtprocessDivider.Name = "txtprocessDivider";
            this.txtprocessDivider.Size = new System.Drawing.Size(130, 21);
            this.txtprocessDivider.TabIndex = 42;
            // 
            // btnProcessDelete
            // 
            this.btnProcessDelete.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnProcessDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnProcessDelete.ForeColor = System.Drawing.Color.White;
            this.btnProcessDelete.Location = new System.Drawing.Point(275, 140);
            this.btnProcessDelete.Name = "btnProcessDelete";
            this.btnProcessDelete.Size = new System.Drawing.Size(130, 30);
            this.btnProcessDelete.TabIndex = 41;
            this.btnProcessDelete.Text = "Delete";
            this.btnProcessDelete.UseVisualStyleBackColor = false;
            this.btnProcessDelete.Click += new System.EventHandler(this.btnProcessDelete_Click);
            // 
            // lb_process_divider
            // 
            this.lb_process_divider.AutoSize = true;
            this.lb_process_divider.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_divider.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_divider.ForeColor = System.Drawing.Color.White;
            this.lb_process_divider.Location = new System.Drawing.Point(213, 11);
            this.lb_process_divider.Name = "lb_process_divider";
            this.lb_process_divider.Size = new System.Drawing.Size(56, 13);
            this.lb_process_divider.TabIndex = 40;
            this.lb_process_divider.Text = "Divider";
            // 
            // btnProcessCopy
            // 
            this.btnProcessCopy.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnProcessCopy.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnProcessCopy.ForeColor = System.Drawing.Color.White;
            this.btnProcessCopy.Location = new System.Drawing.Point(139, 140);
            this.btnProcessCopy.Name = "btnProcessCopy";
            this.btnProcessCopy.Size = new System.Drawing.Size(130, 30);
            this.btnProcessCopy.TabIndex = 39;
            this.btnProcessCopy.Text = "Copy";
            this.btnProcessCopy.UseVisualStyleBackColor = false;
            this.btnProcessCopy.Click += new System.EventHandler(this.btnProcessCopy_Click);
            // 
            // btnProcessMake
            // 
            this.btnProcessMake.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnProcessMake.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnProcessMake.ForeColor = System.Drawing.Color.White;
            this.btnProcessMake.Location = new System.Drawing.Point(3, 139);
            this.btnProcessMake.Name = "btnProcessMake";
            this.btnProcessMake.Size = new System.Drawing.Size(130, 30);
            this.btnProcessMake.TabIndex = 38;
            this.btnProcessMake.Text = "Make";
            this.btnProcessMake.UseVisualStyleBackColor = false;
            this.btnProcessMake.Click += new System.EventHandler(this.btnProcessMake_Click);
            // 
            // lvProcess
            // 
            this.lvProcess.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.lvProcess.GridLines = true;
            this.lvProcess.Location = new System.Drawing.Point(3, 3);
            this.lvProcess.Name = "lvProcess";
            this.lvProcess.Size = new System.Drawing.Size(130, 130);
            this.lvProcess.TabIndex = 0;
            this.lvProcess.UseCompatibleStateImageBehavior = false;
            this.lvProcess.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Name";
            this.columnHeader1.Width = 120;
            // 
            // panel_breaking
            // 
            this.panel_breaking.BackColor = System.Drawing.Color.DimGray;
            this.panel_breaking.Controls.Add(this.btnBreakingXytBreaking4Calc);
            this.panel_breaking.Controls.Add(this.btnBreakingXytBreaking3Calc);
            this.panel_breaking.Controls.Add(this.btnBreakingXytBreaking2Calc);
            this.panel_breaking.Controls.Add(this.btnBreakingXytBreaking1Calc);
            this.panel_breaking.Controls.Add(this.gxtbreaking_load_trans_sequence_offset);
            this.panel_breaking.Controls.Add(this.txtbreakingSlowdownSpeed);
            this.panel_breaking.Controls.Add(this.lb_breaking_slowdown_speed);
            this.panel_breaking.Controls.Add(this.txtbreakingFastdownSpeed);
            this.panel_breaking.Controls.Add(this.lb_breaking_fastdown_speed);
            this.panel_breaking.Controls.Add(this.lb_breaking_slowdown_ptich_mm);
            this.panel_breaking.Controls.Add(this.lb_breaking_fastdown_ptich_mm);
            this.panel_breaking.Controls.Add(this.lb_breaking_slowdown_ptich);
            this.panel_breaking.Controls.Add(this.txtbreakingSlowdownPitch);
            this.panel_breaking.Controls.Add(this.txtbreakingFastdownPitch);
            this.panel_breaking.Controls.Add(this.lb_breaking_fastdown_ptich);
            this.panel_breaking.Controls.Add(this.panelBreaking2pass);
            this.panel_breaking.Controls.Add(this.lb_breaking_2pass);
            this.panel_breaking.Controls.Add(this.txtbreakingBMcrOffsetXy2);
            this.panel_breaking.Controls.Add(this.txtbreakingAMcrOffsetXy2);
            this.panel_breaking.Controls.Add(this.txtbreakingBMcrOffsetXy1);
            this.panel_breaking.Controls.Add(this.lb_breaking_b_mcr_offset_xy);
            this.panel_breaking.Controls.Add(this.txtbreakingAMcrOffsetXy1);
            this.panel_breaking.Controls.Add(this.lb_breaking_a_mcr_offset_xy);
            this.panel_breaking.Controls.Add(this.txtbreakingB2XyOffset2);
            this.panel_breaking.Controls.Add(this.txtbreakingB2XyOffset1);
            this.panel_breaking.Controls.Add(this.lb_breaking_b2_xy_offset);
            this.panel_breaking.Controls.Add(this.txtbreakingB1XyOffset2);
            this.panel_breaking.Controls.Add(this.txtbreakingB1XyOffset1);
            this.panel_breaking.Controls.Add(this.lb_breaking_b1_xy_offset);
            this.panel_breaking.Controls.Add(this.txtbreakingA2XyOffset2);
            this.panel_breaking.Controls.Add(this.txtbreakingA2XyOffset1);
            this.panel_breaking.Controls.Add(this.lb_breaking_a2_xy_offset);
            this.panel_breaking.Controls.Add(this.txtbreakingA1XyOffset2);
            this.panel_breaking.Controls.Add(this.txtbreakingA1XyOffset1);
            this.panel_breaking.Controls.Add(this.lb_breaking_a1_xy_offset);
            this.panel_breaking.Controls.Add(this.txtbreakingXytBreaking43);
            this.panel_breaking.Controls.Add(this.txtbreakingXytBreaking42);
            this.panel_breaking.Controls.Add(this.txtbreakingXytBreaking41);
            this.panel_breaking.Controls.Add(this.lb_breaking_xyt_breaking4);
            this.panel_breaking.Controls.Add(this.txtbreakingXytBreaking33);
            this.panel_breaking.Controls.Add(this.txtbreakingXytBreaking32);
            this.panel_breaking.Controls.Add(this.txtbreakingXytBreaking31);
            this.panel_breaking.Controls.Add(this.lb_breaking_xyt_breaking3);
            this.panel_breaking.Controls.Add(this.txtbreakingXytBreaking23);
            this.panel_breaking.Controls.Add(this.txtbreakingXytBreaking22);
            this.panel_breaking.Controls.Add(this.panel_breaking_half);
            this.panel_breaking.Controls.Add(this.txtbreakingXytBreaking21);
            this.panel_breaking.Controls.Add(this.panel_breaking_right);
            this.panel_breaking.Controls.Add(this.lb_breaking_xyt_breaking2);
            this.panel_breaking.Controls.Add(this.txtbreakingXytBreaking13);
            this.panel_breaking.Controls.Add(this.txtbreakingXytBreaking12);
            this.panel_breaking.Controls.Add(this.txtbreakingXytBreaking11);
            this.panel_breaking.Controls.Add(this.panel_breaking_left);
            this.panel_breaking.Controls.Add(this.txtbreakingXyMcr2);
            this.panel_breaking.Controls.Add(this.txtbreakingXyAlignMark22);
            this.panel_breaking.Controls.Add(this.txtbreakingXyAlignMark12);
            this.panel_breaking.Controls.Add(this.txtbreakingXyPinCenter2);
            this.panel_breaking.Controls.Add(this.lb_breaking_xyt_breaking1);
            this.panel_breaking.Controls.Add(this.txtbreakingPinDistanceB);
            this.panel_breaking.Controls.Add(this.lb_breaking_pin_distance_b);
            this.panel_breaking.Controls.Add(this.txtbreakingPinDistanceA);
            this.panel_breaking.Controls.Add(this.lb_breaking_pin_distance_a);
            this.panel_breaking.Controls.Add(this.txtbreakingXyMcr1);
            this.panel_breaking.Controls.Add(this.panel_breaking_xy_mcr);
            this.panel_breaking.Controls.Add(this.txtbreakingXyAlignMark21);
            this.panel_breaking.Controls.Add(this.panel_breaking_xy_align_mark2);
            this.panel_breaking.Controls.Add(this.txtbreakingXyAlignMark11);
            this.panel_breaking.Controls.Add(this.panel_breaking_xy_align_mark1);
            this.panel_breaking.Controls.Add(this.txtbreakingXyPinCenter1);
            this.panel_breaking.Controls.Add(this.panel_breaking_xy_pin_center);
            this.panel_breaking.Controls.Add(this.btnBreakingSave);
            this.panel_breaking.Controls.Add(this.btnBreakingDelete);
            this.panel_breaking.Controls.Add(this.btnBreakingCopy);
            this.panel_breaking.Controls.Add(this.btnBreakingMake);
            this.panel_breaking.Controls.Add(this.lvBreaking);
            this.panel_breaking.Location = new System.Drawing.Point(3, 178);
            this.panel_breaking.Name = "panel_breaking";
            this.panel_breaking.Size = new System.Drawing.Size(1734, 299);
            this.panel_breaking.TabIndex = 70;
            // 
            // btnBreakingXytBreaking4Calc
            // 
            this.btnBreakingXytBreaking4Calc.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnBreakingXytBreaking4Calc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakingXytBreaking4Calc.ForeColor = System.Drawing.Color.White;
            this.btnBreakingXytBreaking4Calc.Location = new System.Drawing.Point(1019, 107);
            this.btnBreakingXytBreaking4Calc.Name = "btnBreakingXytBreaking4Calc";
            this.btnBreakingXytBreaking4Calc.Size = new System.Drawing.Size(54, 28);
            this.btnBreakingXytBreaking4Calc.TabIndex = 141;
            this.btnBreakingXytBreaking4Calc.Text = "Calc";
            this.btnBreakingXytBreaking4Calc.UseVisualStyleBackColor = false;
            // 
            // btnBreakingXytBreaking3Calc
            // 
            this.btnBreakingXytBreaking3Calc.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnBreakingXytBreaking3Calc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakingXytBreaking3Calc.ForeColor = System.Drawing.Color.White;
            this.btnBreakingXytBreaking3Calc.Location = new System.Drawing.Point(1019, 80);
            this.btnBreakingXytBreaking3Calc.Name = "btnBreakingXytBreaking3Calc";
            this.btnBreakingXytBreaking3Calc.Size = new System.Drawing.Size(54, 28);
            this.btnBreakingXytBreaking3Calc.TabIndex = 140;
            this.btnBreakingXytBreaking3Calc.Text = "Calc";
            this.btnBreakingXytBreaking3Calc.UseVisualStyleBackColor = false;
            // 
            // btnBreakingXytBreaking2Calc
            // 
            this.btnBreakingXytBreaking2Calc.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnBreakingXytBreaking2Calc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakingXytBreaking2Calc.ForeColor = System.Drawing.Color.White;
            this.btnBreakingXytBreaking2Calc.Location = new System.Drawing.Point(1019, 53);
            this.btnBreakingXytBreaking2Calc.Name = "btnBreakingXytBreaking2Calc";
            this.btnBreakingXytBreaking2Calc.Size = new System.Drawing.Size(54, 28);
            this.btnBreakingXytBreaking2Calc.TabIndex = 139;
            this.btnBreakingXytBreaking2Calc.Text = "Calc";
            this.btnBreakingXytBreaking2Calc.UseVisualStyleBackColor = false;
            // 
            // btnBreakingXytBreaking1Calc
            // 
            this.btnBreakingXytBreaking1Calc.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnBreakingXytBreaking1Calc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.btnBreakingXytBreaking1Calc.ForeColor = System.Drawing.Color.White;
            this.btnBreakingXytBreaking1Calc.Location = new System.Drawing.Point(1019, 27);
            this.btnBreakingXytBreaking1Calc.Name = "btnBreakingXytBreaking1Calc";
            this.btnBreakingXytBreaking1Calc.Size = new System.Drawing.Size(54, 28);
            this.btnBreakingXytBreaking1Calc.TabIndex = 138;
            this.btnBreakingXytBreaking1Calc.Text = "Calc";
            this.btnBreakingXytBreaking1Calc.UseVisualStyleBackColor = false;
            // 
            // gxtbreaking_load_trans_sequence_offset
            // 
            this.gxtbreaking_load_trans_sequence_offset.BackColor = System.Drawing.Color.Transparent;
            this.gxtbreaking_load_trans_sequence_offset.Controls.Add(this.gxtbreaking_load_trans_sequence_offset_b);
            this.gxtbreaking_load_trans_sequence_offset.Controls.Add(this.gxtbreaking_load_trans_sequence_offset_a);
            this.gxtbreaking_load_trans_sequence_offset.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.gxtbreaking_load_trans_sequence_offset.ForeColor = System.Drawing.Color.White;
            this.gxtbreaking_load_trans_sequence_offset.Location = new System.Drawing.Point(1388, 129);
            this.gxtbreaking_load_trans_sequence_offset.Name = "gxtbreaking_load_trans_sequence_offset";
            this.gxtbreaking_load_trans_sequence_offset.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gxtbreaking_load_trans_sequence_offset.Size = new System.Drawing.Size(343, 131);
            this.gxtbreaking_load_trans_sequence_offset.TabIndex = 137;
            this.gxtbreaking_load_trans_sequence_offset.TabStop = false;
            this.gxtbreaking_load_trans_sequence_offset.Text = "로드 이재기 Sequence Offset";
            // 
            // gxtbreaking_load_trans_sequence_offset_b
            // 
            this.gxtbreaking_load_trans_sequence_offset_b.BackColor = System.Drawing.Color.Transparent;
            this.gxtbreaking_load_trans_sequence_offset_b.Controls.Add(this.lb_breaking_load_trans_sequence_offset_b_y2_mm);
            this.gxtbreaking_load_trans_sequence_offset_b.Controls.Add(this.txtbreakingLoadTransSequenceOffsetBY1);
            this.gxtbreaking_load_trans_sequence_offset_b.Controls.Add(this.lb_breaking_load_trans_sequence_offset_b_y1_mm);
            this.gxtbreaking_load_trans_sequence_offset_b.Controls.Add(this.lb_breaking_load_trans_sequence_offset_b_y1);
            this.gxtbreaking_load_trans_sequence_offset_b.Controls.Add(this.txtbreakingLoadTransSequenceOffsetBY2);
            this.gxtbreaking_load_trans_sequence_offset_b.Controls.Add(this.lb_breaking_load_trans_sequence_offset_b_y2);
            this.gxtbreaking_load_trans_sequence_offset_b.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtbreaking_load_trans_sequence_offset_b.ForeColor = System.Drawing.Color.White;
            this.gxtbreaking_load_trans_sequence_offset_b.Location = new System.Drawing.Point(176, 25);
            this.gxtbreaking_load_trans_sequence_offset_b.Name = "gxtbreaking_load_trans_sequence_offset_b";
            this.gxtbreaking_load_trans_sequence_offset_b.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gxtbreaking_load_trans_sequence_offset_b.Size = new System.Drawing.Size(160, 100);
            this.gxtbreaking_load_trans_sequence_offset_b.TabIndex = 139;
            this.gxtbreaking_load_trans_sequence_offset_b.TabStop = false;
            this.gxtbreaking_load_trans_sequence_offset_b.Text = "B";
            // 
            // lb_breaking_load_trans_sequence_offset_b_y2_mm
            // 
            this.lb_breaking_load_trans_sequence_offset_b_y2_mm.AutoSize = true;
            this.lb_breaking_load_trans_sequence_offset_b_y2_mm.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_load_trans_sequence_offset_b_y2_mm.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_load_trans_sequence_offset_b_y2_mm.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_load_trans_sequence_offset_b_y2_mm.Location = new System.Drawing.Point(108, 64);
            this.lb_breaking_load_trans_sequence_offset_b_y2_mm.Name = "lb_breaking_load_trans_sequence_offset_b_y2_mm";
            this.lb_breaking_load_trans_sequence_offset_b_y2_mm.Size = new System.Drawing.Size(43, 13);
            this.lb_breaking_load_trans_sequence_offset_b_y2_mm.TabIndex = 148;
            this.lb_breaking_load_trans_sequence_offset_b_y2_mm.Text = "(mm)";
            // 
            // txtbreakingLoadTransSequenceOffsetBY1
            // 
            this.txtbreakingLoadTransSequenceOffsetBY1.Location = new System.Drawing.Point(43, 29);
            this.txtbreakingLoadTransSequenceOffsetBY1.Name = "txtbreakingLoadTransSequenceOffsetBY1";
            this.txtbreakingLoadTransSequenceOffsetBY1.Size = new System.Drawing.Size(59, 22);
            this.txtbreakingLoadTransSequenceOffsetBY1.TabIndex = 143;
            // 
            // lb_breaking_load_trans_sequence_offset_b_y1_mm
            // 
            this.lb_breaking_load_trans_sequence_offset_b_y1_mm.AutoSize = true;
            this.lb_breaking_load_trans_sequence_offset_b_y1_mm.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_load_trans_sequence_offset_b_y1_mm.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_load_trans_sequence_offset_b_y1_mm.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_load_trans_sequence_offset_b_y1_mm.Location = new System.Drawing.Point(108, 33);
            this.lb_breaking_load_trans_sequence_offset_b_y1_mm.Name = "lb_breaking_load_trans_sequence_offset_b_y1_mm";
            this.lb_breaking_load_trans_sequence_offset_b_y1_mm.Size = new System.Drawing.Size(43, 13);
            this.lb_breaking_load_trans_sequence_offset_b_y1_mm.TabIndex = 147;
            this.lb_breaking_load_trans_sequence_offset_b_y1_mm.Text = "(mm)";
            // 
            // lb_breaking_load_trans_sequence_offset_b_y1
            // 
            this.lb_breaking_load_trans_sequence_offset_b_y1.AutoSize = true;
            this.lb_breaking_load_trans_sequence_offset_b_y1.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_load_trans_sequence_offset_b_y1.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_load_trans_sequence_offset_b_y1.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_load_trans_sequence_offset_b_y1.Location = new System.Drawing.Point(13, 31);
            this.lb_breaking_load_trans_sequence_offset_b_y1.Name = "lb_breaking_load_trans_sequence_offset_b_y1";
            this.lb_breaking_load_trans_sequence_offset_b_y1.Size = new System.Drawing.Size(24, 13);
            this.lb_breaking_load_trans_sequence_offset_b_y1.TabIndex = 144;
            this.lb_breaking_load_trans_sequence_offset_b_y1.Text = "Y1";
            // 
            // txtbreakingLoadTransSequenceOffsetBY2
            // 
            this.txtbreakingLoadTransSequenceOffsetBY2.Location = new System.Drawing.Point(43, 60);
            this.txtbreakingLoadTransSequenceOffsetBY2.Name = "txtbreakingLoadTransSequenceOffsetBY2";
            this.txtbreakingLoadTransSequenceOffsetBY2.Size = new System.Drawing.Size(59, 22);
            this.txtbreakingLoadTransSequenceOffsetBY2.TabIndex = 146;
            // 
            // lb_breaking_load_trans_sequence_offset_b_y2
            // 
            this.lb_breaking_load_trans_sequence_offset_b_y2.AutoSize = true;
            this.lb_breaking_load_trans_sequence_offset_b_y2.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_load_trans_sequence_offset_b_y2.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_load_trans_sequence_offset_b_y2.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_load_trans_sequence_offset_b_y2.Location = new System.Drawing.Point(13, 64);
            this.lb_breaking_load_trans_sequence_offset_b_y2.Name = "lb_breaking_load_trans_sequence_offset_b_y2";
            this.lb_breaking_load_trans_sequence_offset_b_y2.Size = new System.Drawing.Size(24, 13);
            this.lb_breaking_load_trans_sequence_offset_b_y2.TabIndex = 145;
            this.lb_breaking_load_trans_sequence_offset_b_y2.Text = "Y2";
            // 
            // gxtbreaking_load_trans_sequence_offset_a
            // 
            this.gxtbreaking_load_trans_sequence_offset_a.BackColor = System.Drawing.Color.Transparent;
            this.gxtbreaking_load_trans_sequence_offset_a.Controls.Add(this.lb_breaking_load_trans_sequence_offset_a_y2_mm);
            this.gxtbreaking_load_trans_sequence_offset_a.Controls.Add(this.lb_breaking_load_trans_sequence_offset_a_y1_mm);
            this.gxtbreaking_load_trans_sequence_offset_a.Controls.Add(this.txtbreakingLoadTransSequenceOffsetAY2);
            this.gxtbreaking_load_trans_sequence_offset_a.Controls.Add(this.txtbreakingLoadTransSequenceOffsetAY1);
            this.gxtbreaking_load_trans_sequence_offset_a.Controls.Add(this.lb_breaking_load_trans_sequence_offset_a_y2);
            this.gxtbreaking_load_trans_sequence_offset_a.Controls.Add(this.lb_breaking_load_trans_sequence_offset_a_y1);
            this.gxtbreaking_load_trans_sequence_offset_a.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtbreaking_load_trans_sequence_offset_a.ForeColor = System.Drawing.Color.White;
            this.gxtbreaking_load_trans_sequence_offset_a.Location = new System.Drawing.Point(10, 25);
            this.gxtbreaking_load_trans_sequence_offset_a.Name = "gxtbreaking_load_trans_sequence_offset_a";
            this.gxtbreaking_load_trans_sequence_offset_a.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gxtbreaking_load_trans_sequence_offset_a.Size = new System.Drawing.Size(160, 100);
            this.gxtbreaking_load_trans_sequence_offset_a.TabIndex = 138;
            this.gxtbreaking_load_trans_sequence_offset_a.TabStop = false;
            this.gxtbreaking_load_trans_sequence_offset_a.Text = "A";
            // 
            // lb_breaking_load_trans_sequence_offset_a_y2_mm
            // 
            this.lb_breaking_load_trans_sequence_offset_a_y2_mm.AutoSize = true;
            this.lb_breaking_load_trans_sequence_offset_a_y2_mm.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_load_trans_sequence_offset_a_y2_mm.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_load_trans_sequence_offset_a_y2_mm.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_load_trans_sequence_offset_a_y2_mm.Location = new System.Drawing.Point(106, 66);
            this.lb_breaking_load_trans_sequence_offset_a_y2_mm.Name = "lb_breaking_load_trans_sequence_offset_a_y2_mm";
            this.lb_breaking_load_trans_sequence_offset_a_y2_mm.Size = new System.Drawing.Size(43, 13);
            this.lb_breaking_load_trans_sequence_offset_a_y2_mm.TabIndex = 142;
            this.lb_breaking_load_trans_sequence_offset_a_y2_mm.Text = "(mm)";
            // 
            // lb_breaking_load_trans_sequence_offset_a_y1_mm
            // 
            this.lb_breaking_load_trans_sequence_offset_a_y1_mm.AutoSize = true;
            this.lb_breaking_load_trans_sequence_offset_a_y1_mm.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_load_trans_sequence_offset_a_y1_mm.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_load_trans_sequence_offset_a_y1_mm.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_load_trans_sequence_offset_a_y1_mm.Location = new System.Drawing.Point(106, 35);
            this.lb_breaking_load_trans_sequence_offset_a_y1_mm.Name = "lb_breaking_load_trans_sequence_offset_a_y1_mm";
            this.lb_breaking_load_trans_sequence_offset_a_y1_mm.Size = new System.Drawing.Size(43, 13);
            this.lb_breaking_load_trans_sequence_offset_a_y1_mm.TabIndex = 141;
            this.lb_breaking_load_trans_sequence_offset_a_y1_mm.Text = "(mm)";
            // 
            // txtbreakingLoadTransSequenceOffsetAY2
            // 
            this.txtbreakingLoadTransSequenceOffsetAY2.Location = new System.Drawing.Point(41, 62);
            this.txtbreakingLoadTransSequenceOffsetAY2.Name = "txtbreakingLoadTransSequenceOffsetAY2";
            this.txtbreakingLoadTransSequenceOffsetAY2.Size = new System.Drawing.Size(59, 22);
            this.txtbreakingLoadTransSequenceOffsetAY2.TabIndex = 140;
            // 
            // txtbreakingLoadTransSequenceOffsetAY1
            // 
            this.txtbreakingLoadTransSequenceOffsetAY1.Location = new System.Drawing.Point(41, 31);
            this.txtbreakingLoadTransSequenceOffsetAY1.Name = "txtbreakingLoadTransSequenceOffsetAY1";
            this.txtbreakingLoadTransSequenceOffsetAY1.Size = new System.Drawing.Size(59, 22);
            this.txtbreakingLoadTransSequenceOffsetAY1.TabIndex = 138;
            // 
            // lb_breaking_load_trans_sequence_offset_a_y2
            // 
            this.lb_breaking_load_trans_sequence_offset_a_y2.AutoSize = true;
            this.lb_breaking_load_trans_sequence_offset_a_y2.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_load_trans_sequence_offset_a_y2.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_load_trans_sequence_offset_a_y2.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_load_trans_sequence_offset_a_y2.Location = new System.Drawing.Point(11, 66);
            this.lb_breaking_load_trans_sequence_offset_a_y2.Name = "lb_breaking_load_trans_sequence_offset_a_y2";
            this.lb_breaking_load_trans_sequence_offset_a_y2.Size = new System.Drawing.Size(24, 13);
            this.lb_breaking_load_trans_sequence_offset_a_y2.TabIndex = 139;
            this.lb_breaking_load_trans_sequence_offset_a_y2.Text = "Y2";
            // 
            // lb_breaking_load_trans_sequence_offset_a_y1
            // 
            this.lb_breaking_load_trans_sequence_offset_a_y1.AutoSize = true;
            this.lb_breaking_load_trans_sequence_offset_a_y1.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_load_trans_sequence_offset_a_y1.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_load_trans_sequence_offset_a_y1.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_load_trans_sequence_offset_a_y1.Location = new System.Drawing.Point(11, 33);
            this.lb_breaking_load_trans_sequence_offset_a_y1.Name = "lb_breaking_load_trans_sequence_offset_a_y1";
            this.lb_breaking_load_trans_sequence_offset_a_y1.Size = new System.Drawing.Size(24, 13);
            this.lb_breaking_load_trans_sequence_offset_a_y1.TabIndex = 138;
            this.lb_breaking_load_trans_sequence_offset_a_y1.Text = "Y1";
            // 
            // txtbreakingSlowdownSpeed
            // 
            this.txtbreakingSlowdownSpeed.Location = new System.Drawing.Point(658, 199);
            this.txtbreakingSlowdownSpeed.Name = "txtbreakingSlowdownSpeed";
            this.txtbreakingSlowdownSpeed.Size = new System.Drawing.Size(137, 21);
            this.txtbreakingSlowdownSpeed.TabIndex = 136;
            // 
            // lb_breaking_slowdown_speed
            // 
            this.lb_breaking_slowdown_speed.AutoSize = true;
            this.lb_breaking_slowdown_speed.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_slowdown_speed.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_slowdown_speed.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_slowdown_speed.Location = new System.Drawing.Point(516, 201);
            this.lb_breaking_slowdown_speed.Name = "lb_breaking_slowdown_speed";
            this.lb_breaking_slowdown_speed.Size = new System.Drawing.Size(130, 13);
            this.lb_breaking_slowdown_speed.TabIndex = 135;
            this.lb_breaking_slowdown_speed.Text = "Z축 저속 하강 속도";
            // 
            // txtbreakingFastdownSpeed
            // 
            this.txtbreakingFastdownSpeed.Location = new System.Drawing.Point(658, 169);
            this.txtbreakingFastdownSpeed.Name = "txtbreakingFastdownSpeed";
            this.txtbreakingFastdownSpeed.Size = new System.Drawing.Size(137, 21);
            this.txtbreakingFastdownSpeed.TabIndex = 134;
            // 
            // lb_breaking_fastdown_speed
            // 
            this.lb_breaking_fastdown_speed.AutoSize = true;
            this.lb_breaking_fastdown_speed.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_fastdown_speed.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_fastdown_speed.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_fastdown_speed.Location = new System.Drawing.Point(516, 171);
            this.lb_breaking_fastdown_speed.Name = "lb_breaking_fastdown_speed";
            this.lb_breaking_fastdown_speed.Size = new System.Drawing.Size(130, 13);
            this.lb_breaking_fastdown_speed.TabIndex = 133;
            this.lb_breaking_fastdown_speed.Text = "Z축 고속 하강 속도";
            // 
            // lb_breaking_slowdown_ptich_mm
            // 
            this.lb_breaking_slowdown_ptich_mm.AutoSize = true;
            this.lb_breaking_slowdown_ptich_mm.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_slowdown_ptich_mm.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_slowdown_ptich_mm.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_slowdown_ptich_mm.Location = new System.Drawing.Point(441, 201);
            this.lb_breaking_slowdown_ptich_mm.Name = "lb_breaking_slowdown_ptich_mm";
            this.lb_breaking_slowdown_ptich_mm.Size = new System.Drawing.Size(43, 13);
            this.lb_breaking_slowdown_ptich_mm.TabIndex = 132;
            this.lb_breaking_slowdown_ptich_mm.Text = "(mm)";
            // 
            // lb_breaking_fastdown_ptich_mm
            // 
            this.lb_breaking_fastdown_ptich_mm.AutoSize = true;
            this.lb_breaking_fastdown_ptich_mm.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_fastdown_ptich_mm.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_fastdown_ptich_mm.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_fastdown_ptich_mm.Location = new System.Drawing.Point(441, 171);
            this.lb_breaking_fastdown_ptich_mm.Name = "lb_breaking_fastdown_ptich_mm";
            this.lb_breaking_fastdown_ptich_mm.Size = new System.Drawing.Size(43, 13);
            this.lb_breaking_fastdown_ptich_mm.TabIndex = 131;
            this.lb_breaking_fastdown_ptich_mm.Text = "(mm)";
            // 
            // lb_breaking_slowdown_ptich
            // 
            this.lb_breaking_slowdown_ptich.AutoSize = true;
            this.lb_breaking_slowdown_ptich.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_slowdown_ptich.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_slowdown_ptich.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_slowdown_ptich.Location = new System.Drawing.Point(143, 201);
            this.lb_breaking_slowdown_ptich.Name = "lb_breaking_slowdown_ptich";
            this.lb_breaking_slowdown_ptich.Size = new System.Drawing.Size(132, 13);
            this.lb_breaking_slowdown_ptich.TabIndex = 130;
            this.lb_breaking_slowdown_ptich.Text = "Z축 저속하강 pitch";
            // 
            // txtbreakingSlowdownPitch
            // 
            this.txtbreakingSlowdownPitch.Location = new System.Drawing.Point(301, 199);
            this.txtbreakingSlowdownPitch.Name = "txtbreakingSlowdownPitch";
            this.txtbreakingSlowdownPitch.Size = new System.Drawing.Size(137, 21);
            this.txtbreakingSlowdownPitch.TabIndex = 129;
            // 
            // txtbreakingFastdownPitch
            // 
            this.txtbreakingFastdownPitch.Location = new System.Drawing.Point(301, 169);
            this.txtbreakingFastdownPitch.Name = "txtbreakingFastdownPitch";
            this.txtbreakingFastdownPitch.Size = new System.Drawing.Size(137, 21);
            this.txtbreakingFastdownPitch.TabIndex = 127;
            // 
            // lb_breaking_fastdown_ptich
            // 
            this.lb_breaking_fastdown_ptich.AutoSize = true;
            this.lb_breaking_fastdown_ptich.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_fastdown_ptich.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_fastdown_ptich.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_fastdown_ptich.Location = new System.Drawing.Point(143, 171);
            this.lb_breaking_fastdown_ptich.Name = "lb_breaking_fastdown_ptich";
            this.lb_breaking_fastdown_ptich.Size = new System.Drawing.Size(132, 13);
            this.lb_breaking_fastdown_ptich.TabIndex = 126;
            this.lb_breaking_fastdown_ptich.Text = "Z축 고속하강 pitch";
            // 
            // panelBreaking2pass
            // 
            this.panelBreaking2pass.BackColor = System.Drawing.Color.Aquamarine;
            this.panelBreaking2pass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelBreaking2pass.Location = new System.Drawing.Point(1683, 99);
            this.panelBreaking2pass.Name = "panelBreaking2pass";
            this.panelBreaking2pass.Size = new System.Drawing.Size(30, 30);
            this.panelBreaking2pass.TabIndex = 74;
            // 
            // lb_breaking_2pass
            // 
            this.lb_breaking_2pass.AutoSize = true;
            this.lb_breaking_2pass.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_2pass.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_2pass.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_2pass.Location = new System.Drawing.Point(1595, 108);
            this.lb_breaking_2pass.Name = "lb_breaking_2pass";
            this.lb_breaking_2pass.Size = new System.Drawing.Size(82, 13);
            this.lb_breaking_2pass.TabIndex = 125;
            this.lb_breaking_2pass.Text = "상하부 가공";
            // 
            // txtbreakingBMcrOffsetXy2
            // 
            this.txtbreakingBMcrOffsetXy2.Location = new System.Drawing.Point(1623, 57);
            this.txtbreakingBMcrOffsetXy2.Name = "txtbreakingBMcrOffsetXy2";
            this.txtbreakingBMcrOffsetXy2.Size = new System.Drawing.Size(90, 21);
            this.txtbreakingBMcrOffsetXy2.TabIndex = 124;
            // 
            // txtbreakingAMcrOffsetXy2
            // 
            this.txtbreakingAMcrOffsetXy2.Location = new System.Drawing.Point(1623, 30);
            this.txtbreakingAMcrOffsetXy2.Name = "txtbreakingAMcrOffsetXy2";
            this.txtbreakingAMcrOffsetXy2.Size = new System.Drawing.Size(90, 21);
            this.txtbreakingAMcrOffsetXy2.TabIndex = 123;
            // 
            // txtbreakingBMcrOffsetXy1
            // 
            this.txtbreakingBMcrOffsetXy1.Location = new System.Drawing.Point(1527, 57);
            this.txtbreakingBMcrOffsetXy1.Name = "txtbreakingBMcrOffsetXy1";
            this.txtbreakingBMcrOffsetXy1.Size = new System.Drawing.Size(90, 21);
            this.txtbreakingBMcrOffsetXy1.TabIndex = 122;
            // 
            // lb_breaking_b_mcr_offset_xy
            // 
            this.lb_breaking_b_mcr_offset_xy.AutoSize = true;
            this.lb_breaking_b_mcr_offset_xy.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_b_mcr_offset_xy.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_b_mcr_offset_xy.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_b_mcr_offset_xy.Location = new System.Drawing.Point(1354, 61);
            this.lb_breaking_b_mcr_offset_xy.Name = "lb_breaking_b_mcr_offset_xy";
            this.lb_breaking_b_mcr_offset_xy.Size = new System.Drawing.Size(177, 13);
            this.lb_breaking_b_mcr_offset_xy.TabIndex = 121;
            this.lb_breaking_b_mcr_offset_xy.Text = "B열 MCR Offset XY[mm]";
            // 
            // txtbreakingAMcrOffsetXy1
            // 
            this.txtbreakingAMcrOffsetXy1.Location = new System.Drawing.Point(1527, 30);
            this.txtbreakingAMcrOffsetXy1.Name = "txtbreakingAMcrOffsetXy1";
            this.txtbreakingAMcrOffsetXy1.Size = new System.Drawing.Size(90, 21);
            this.txtbreakingAMcrOffsetXy1.TabIndex = 119;
            // 
            // lb_breaking_a_mcr_offset_xy
            // 
            this.lb_breaking_a_mcr_offset_xy.AutoSize = true;
            this.lb_breaking_a_mcr_offset_xy.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_a_mcr_offset_xy.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_a_mcr_offset_xy.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_a_mcr_offset_xy.Location = new System.Drawing.Point(1354, 34);
            this.lb_breaking_a_mcr_offset_xy.Name = "lb_breaking_a_mcr_offset_xy";
            this.lb_breaking_a_mcr_offset_xy.Size = new System.Drawing.Size(176, 13);
            this.lb_breaking_a_mcr_offset_xy.TabIndex = 118;
            this.lb_breaking_a_mcr_offset_xy.Text = "A열 MCR Offset XY[mm]";
            // 
            // txtbreakingB2XyOffset2
            // 
            this.txtbreakingB2XyOffset2.Location = new System.Drawing.Point(1289, 111);
            this.txtbreakingB2XyOffset2.Name = "txtbreakingB2XyOffset2";
            this.txtbreakingB2XyOffset2.Size = new System.Drawing.Size(59, 21);
            this.txtbreakingB2XyOffset2.TabIndex = 117;
            // 
            // txtbreakingB2XyOffset1
            // 
            this.txtbreakingB2XyOffset1.Location = new System.Drawing.Point(1224, 111);
            this.txtbreakingB2XyOffset1.Name = "txtbreakingB2XyOffset1";
            this.txtbreakingB2XyOffset1.Size = new System.Drawing.Size(59, 21);
            this.txtbreakingB2XyOffset1.TabIndex = 116;
            // 
            // lb_breaking_b2_xy_offset
            // 
            this.lb_breaking_b2_xy_offset.AutoSize = true;
            this.lb_breaking_b2_xy_offset.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_b2_xy_offset.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_b2_xy_offset.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_b2_xy_offset.Location = new System.Drawing.Point(1079, 115);
            this.lb_breaking_b2_xy_offset.Name = "lb_breaking_b2_xy_offset";
            this.lb_breaking_b2_xy_offset.Size = new System.Drawing.Size(146, 13);
            this.lb_breaking_b2_xy_offset.TabIndex = 115;
            this.lb_breaking_b2_xy_offset.Text = "B열2 XY offset[mm]";
            // 
            // txtbreakingB1XyOffset2
            // 
            this.txtbreakingB1XyOffset2.Location = new System.Drawing.Point(1289, 84);
            this.txtbreakingB1XyOffset2.Name = "txtbreakingB1XyOffset2";
            this.txtbreakingB1XyOffset2.Size = new System.Drawing.Size(59, 21);
            this.txtbreakingB1XyOffset2.TabIndex = 114;
            // 
            // txtbreakingB1XyOffset1
            // 
            this.txtbreakingB1XyOffset1.Location = new System.Drawing.Point(1224, 84);
            this.txtbreakingB1XyOffset1.Name = "txtbreakingB1XyOffset1";
            this.txtbreakingB1XyOffset1.Size = new System.Drawing.Size(59, 21);
            this.txtbreakingB1XyOffset1.TabIndex = 113;
            // 
            // lb_breaking_b1_xy_offset
            // 
            this.lb_breaking_b1_xy_offset.AutoSize = true;
            this.lb_breaking_b1_xy_offset.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_b1_xy_offset.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_b1_xy_offset.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_b1_xy_offset.Location = new System.Drawing.Point(1079, 88);
            this.lb_breaking_b1_xy_offset.Name = "lb_breaking_b1_xy_offset";
            this.lb_breaking_b1_xy_offset.Size = new System.Drawing.Size(146, 13);
            this.lb_breaking_b1_xy_offset.TabIndex = 112;
            this.lb_breaking_b1_xy_offset.Text = "B열1 XY offset[mm]";
            // 
            // txtbreakingA2XyOffset2
            // 
            this.txtbreakingA2XyOffset2.Location = new System.Drawing.Point(1289, 57);
            this.txtbreakingA2XyOffset2.Name = "txtbreakingA2XyOffset2";
            this.txtbreakingA2XyOffset2.Size = new System.Drawing.Size(59, 21);
            this.txtbreakingA2XyOffset2.TabIndex = 111;
            // 
            // txtbreakingA2XyOffset1
            // 
            this.txtbreakingA2XyOffset1.Location = new System.Drawing.Point(1224, 57);
            this.txtbreakingA2XyOffset1.Name = "txtbreakingA2XyOffset1";
            this.txtbreakingA2XyOffset1.Size = new System.Drawing.Size(59, 21);
            this.txtbreakingA2XyOffset1.TabIndex = 110;
            // 
            // lb_breaking_a2_xy_offset
            // 
            this.lb_breaking_a2_xy_offset.AutoSize = true;
            this.lb_breaking_a2_xy_offset.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_a2_xy_offset.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_a2_xy_offset.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_a2_xy_offset.Location = new System.Drawing.Point(1079, 61);
            this.lb_breaking_a2_xy_offset.Name = "lb_breaking_a2_xy_offset";
            this.lb_breaking_a2_xy_offset.Size = new System.Drawing.Size(145, 13);
            this.lb_breaking_a2_xy_offset.TabIndex = 109;
            this.lb_breaking_a2_xy_offset.Text = "A열2 XY offset[mm]";
            // 
            // txtbreakingA1XyOffset2
            // 
            this.txtbreakingA1XyOffset2.Location = new System.Drawing.Point(1289, 30);
            this.txtbreakingA1XyOffset2.Name = "txtbreakingA1XyOffset2";
            this.txtbreakingA1XyOffset2.Size = new System.Drawing.Size(59, 21);
            this.txtbreakingA1XyOffset2.TabIndex = 108;
            // 
            // txtbreakingA1XyOffset1
            // 
            this.txtbreakingA1XyOffset1.Location = new System.Drawing.Point(1224, 30);
            this.txtbreakingA1XyOffset1.Name = "txtbreakingA1XyOffset1";
            this.txtbreakingA1XyOffset1.Size = new System.Drawing.Size(59, 21);
            this.txtbreakingA1XyOffset1.TabIndex = 107;
            // 
            // lb_breaking_a1_xy_offset
            // 
            this.lb_breaking_a1_xy_offset.AutoSize = true;
            this.lb_breaking_a1_xy_offset.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_a1_xy_offset.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_a1_xy_offset.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_a1_xy_offset.Location = new System.Drawing.Point(1079, 34);
            this.lb_breaking_a1_xy_offset.Name = "lb_breaking_a1_xy_offset";
            this.lb_breaking_a1_xy_offset.Size = new System.Drawing.Size(145, 13);
            this.lb_breaking_a1_xy_offset.TabIndex = 106;
            this.lb_breaking_a1_xy_offset.Text = "A열1 XY offset[mm]";
            // 
            // txtbreakingXytBreaking43
            // 
            this.txtbreakingXytBreaking43.Location = new System.Drawing.Point(930, 111);
            this.txtbreakingXytBreaking43.Name = "txtbreakingXytBreaking43";
            this.txtbreakingXytBreaking43.Size = new System.Drawing.Size(83, 21);
            this.txtbreakingXytBreaking43.TabIndex = 104;
            // 
            // txtbreakingXytBreaking42
            // 
            this.txtbreakingXytBreaking42.Location = new System.Drawing.Point(841, 111);
            this.txtbreakingXytBreaking42.Name = "txtbreakingXytBreaking42";
            this.txtbreakingXytBreaking42.Size = new System.Drawing.Size(83, 21);
            this.txtbreakingXytBreaking42.TabIndex = 103;
            // 
            // txtbreakingXytBreaking41
            // 
            this.txtbreakingXytBreaking41.Location = new System.Drawing.Point(752, 111);
            this.txtbreakingXytBreaking41.Name = "txtbreakingXytBreaking41";
            this.txtbreakingXytBreaking41.Size = new System.Drawing.Size(83, 21);
            this.txtbreakingXytBreaking41.TabIndex = 102;
            // 
            // lb_breaking_xyt_breaking4
            // 
            this.lb_breaking_xyt_breaking4.AutoSize = true;
            this.lb_breaking_xyt_breaking4.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_xyt_breaking4.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_xyt_breaking4.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_xyt_breaking4.Location = new System.Drawing.Point(596, 116);
            this.lb_breaking_xyt_breaking4.Name = "lb_breaking_xyt_breaking4";
            this.lb_breaking_xyt_breaking4.Size = new System.Drawing.Size(150, 13);
            this.lb_breaking_xyt_breaking4.TabIndex = 101;
            this.lb_breaking_xyt_breaking4.Text = "XYT Breaking4[mm]";
            // 
            // txtbreakingXytBreaking33
            // 
            this.txtbreakingXytBreaking33.Location = new System.Drawing.Point(930, 84);
            this.txtbreakingXytBreaking33.Name = "txtbreakingXytBreaking33";
            this.txtbreakingXytBreaking33.Size = new System.Drawing.Size(83, 21);
            this.txtbreakingXytBreaking33.TabIndex = 99;
            // 
            // txtbreakingXytBreaking32
            // 
            this.txtbreakingXytBreaking32.Location = new System.Drawing.Point(841, 84);
            this.txtbreakingXytBreaking32.Name = "txtbreakingXytBreaking32";
            this.txtbreakingXytBreaking32.Size = new System.Drawing.Size(83, 21);
            this.txtbreakingXytBreaking32.TabIndex = 98;
            // 
            // txtbreakingXytBreaking31
            // 
            this.txtbreakingXytBreaking31.Location = new System.Drawing.Point(752, 84);
            this.txtbreakingXytBreaking31.Name = "txtbreakingXytBreaking31";
            this.txtbreakingXytBreaking31.Size = new System.Drawing.Size(83, 21);
            this.txtbreakingXytBreaking31.TabIndex = 97;
            // 
            // lb_breaking_xyt_breaking3
            // 
            this.lb_breaking_xyt_breaking3.AutoSize = true;
            this.lb_breaking_xyt_breaking3.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_xyt_breaking3.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_xyt_breaking3.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_xyt_breaking3.Location = new System.Drawing.Point(596, 89);
            this.lb_breaking_xyt_breaking3.Name = "lb_breaking_xyt_breaking3";
            this.lb_breaking_xyt_breaking3.Size = new System.Drawing.Size(150, 13);
            this.lb_breaking_xyt_breaking3.TabIndex = 96;
            this.lb_breaking_xyt_breaking3.Text = "XYT Breaking3[mm]";
            // 
            // txtbreakingXytBreaking23
            // 
            this.txtbreakingXytBreaking23.Location = new System.Drawing.Point(930, 57);
            this.txtbreakingXytBreaking23.Name = "txtbreakingXytBreaking23";
            this.txtbreakingXytBreaking23.Size = new System.Drawing.Size(83, 21);
            this.txtbreakingXytBreaking23.TabIndex = 94;
            // 
            // txtbreakingXytBreaking22
            // 
            this.txtbreakingXytBreaking22.Location = new System.Drawing.Point(841, 57);
            this.txtbreakingXytBreaking22.Name = "txtbreakingXytBreaking22";
            this.txtbreakingXytBreaking22.Size = new System.Drawing.Size(83, 21);
            this.txtbreakingXytBreaking22.TabIndex = 93;
            // 
            // panel_breaking_half
            // 
            this.panel_breaking_half.BackColor = System.Drawing.Color.Aquamarine;
            this.panel_breaking_half.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_breaking_half.Controls.Add(this.lb_breaking_half);
            this.panel_breaking_half.Location = new System.Drawing.Point(970, 5);
            this.panel_breaking_half.Name = "panel_breaking_half";
            this.panel_breaking_half.Size = new System.Drawing.Size(103, 21);
            this.panel_breaking_half.TabIndex = 73;
            // 
            // lb_breaking_half
            // 
            this.lb_breaking_half.AutoSize = true;
            this.lb_breaking_half.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_half.Location = new System.Drawing.Point(9, 0);
            this.lb_breaking_half.Name = "lb_breaking_half";
            this.lb_breaking_half.Size = new System.Drawing.Size(38, 18);
            this.lb_breaking_half.TabIndex = 0;
            this.lb_breaking_half.Text = "Half";
            // 
            // txtbreakingXytBreaking21
            // 
            this.txtbreakingXytBreaking21.Location = new System.Drawing.Point(752, 57);
            this.txtbreakingXytBreaking21.Name = "txtbreakingXytBreaking21";
            this.txtbreakingXytBreaking21.Size = new System.Drawing.Size(83, 21);
            this.txtbreakingXytBreaking21.TabIndex = 92;
            // 
            // panel_breaking_right
            // 
            this.panel_breaking_right.BackColor = System.Drawing.Color.Aquamarine;
            this.panel_breaking_right.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_breaking_right.Controls.Add(this.lb_breaking_right);
            this.panel_breaking_right.Location = new System.Drawing.Point(861, 5);
            this.panel_breaking_right.Name = "panel_breaking_right";
            this.panel_breaking_right.Size = new System.Drawing.Size(103, 21);
            this.panel_breaking_right.TabIndex = 72;
            // 
            // lb_breaking_right
            // 
            this.lb_breaking_right.AutoSize = true;
            this.lb_breaking_right.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_right.Location = new System.Drawing.Point(9, 0);
            this.lb_breaking_right.Name = "lb_breaking_right";
            this.lb_breaking_right.Size = new System.Drawing.Size(47, 18);
            this.lb_breaking_right.TabIndex = 0;
            this.lb_breaking_right.Text = "Right";
            // 
            // lb_breaking_xyt_breaking2
            // 
            this.lb_breaking_xyt_breaking2.AutoSize = true;
            this.lb_breaking_xyt_breaking2.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_xyt_breaking2.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_xyt_breaking2.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_xyt_breaking2.Location = new System.Drawing.Point(596, 62);
            this.lb_breaking_xyt_breaking2.Name = "lb_breaking_xyt_breaking2";
            this.lb_breaking_xyt_breaking2.Size = new System.Drawing.Size(150, 13);
            this.lb_breaking_xyt_breaking2.TabIndex = 91;
            this.lb_breaking_xyt_breaking2.Text = "XYT Breaking2[mm]";
            // 
            // txtbreakingXytBreaking13
            // 
            this.txtbreakingXytBreaking13.Location = new System.Drawing.Point(930, 30);
            this.txtbreakingXytBreaking13.Name = "txtbreakingXytBreaking13";
            this.txtbreakingXytBreaking13.Size = new System.Drawing.Size(83, 21);
            this.txtbreakingXytBreaking13.TabIndex = 89;
            // 
            // txtbreakingXytBreaking12
            // 
            this.txtbreakingXytBreaking12.Location = new System.Drawing.Point(841, 30);
            this.txtbreakingXytBreaking12.Name = "txtbreakingXytBreaking12";
            this.txtbreakingXytBreaking12.Size = new System.Drawing.Size(83, 21);
            this.txtbreakingXytBreaking12.TabIndex = 88;
            // 
            // txtbreakingXytBreaking11
            // 
            this.txtbreakingXytBreaking11.Location = new System.Drawing.Point(752, 30);
            this.txtbreakingXytBreaking11.Name = "txtbreakingXytBreaking11";
            this.txtbreakingXytBreaking11.Size = new System.Drawing.Size(83, 21);
            this.txtbreakingXytBreaking11.TabIndex = 87;
            // 
            // panel_breaking_left
            // 
            this.panel_breaking_left.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.panel_breaking_left.BackColor = System.Drawing.Color.Aquamarine;
            this.panel_breaking_left.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_breaking_left.Controls.Add(this.lb_breaking_left);
            this.panel_breaking_left.Location = new System.Drawing.Point(752, 5);
            this.panel_breaking_left.Name = "panel_breaking_left";
            this.panel_breaking_left.Size = new System.Drawing.Size(103, 21);
            this.panel_breaking_left.TabIndex = 71;
            // 
            // lb_breaking_left
            // 
            this.lb_breaking_left.AutoSize = true;
            this.lb_breaking_left.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_left.Location = new System.Drawing.Point(9, 0);
            this.lb_breaking_left.Name = "lb_breaking_left";
            this.lb_breaking_left.Size = new System.Drawing.Size(36, 18);
            this.lb_breaking_left.TabIndex = 0;
            this.lb_breaking_left.Text = "Left";
            // 
            // txtbreakingXyMcr2
            // 
            this.txtbreakingXyMcr2.Location = new System.Drawing.Point(444, 84);
            this.txtbreakingXyMcr2.Name = "txtbreakingXyMcr2";
            this.txtbreakingXyMcr2.Size = new System.Drawing.Size(137, 21);
            this.txtbreakingXyMcr2.TabIndex = 86;
            // 
            // txtbreakingXyAlignMark22
            // 
            this.txtbreakingXyAlignMark22.Location = new System.Drawing.Point(444, 57);
            this.txtbreakingXyAlignMark22.Name = "txtbreakingXyAlignMark22";
            this.txtbreakingXyAlignMark22.Size = new System.Drawing.Size(137, 21);
            this.txtbreakingXyAlignMark22.TabIndex = 85;
            // 
            // txtbreakingXyAlignMark12
            // 
            this.txtbreakingXyAlignMark12.Location = new System.Drawing.Point(444, 30);
            this.txtbreakingXyAlignMark12.Name = "txtbreakingXyAlignMark12";
            this.txtbreakingXyAlignMark12.Size = new System.Drawing.Size(137, 21);
            this.txtbreakingXyAlignMark12.TabIndex = 84;
            // 
            // txtbreakingXyPinCenter2
            // 
            this.txtbreakingXyPinCenter2.Location = new System.Drawing.Point(444, 4);
            this.txtbreakingXyPinCenter2.Name = "txtbreakingXyPinCenter2";
            this.txtbreakingXyPinCenter2.Size = new System.Drawing.Size(137, 21);
            this.txtbreakingXyPinCenter2.TabIndex = 83;
            // 
            // lb_breaking_xyt_breaking1
            // 
            this.lb_breaking_xyt_breaking1.AutoSize = true;
            this.lb_breaking_xyt_breaking1.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_xyt_breaking1.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_xyt_breaking1.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_xyt_breaking1.Location = new System.Drawing.Point(596, 35);
            this.lb_breaking_xyt_breaking1.Name = "lb_breaking_xyt_breaking1";
            this.lb_breaking_xyt_breaking1.Size = new System.Drawing.Size(150, 13);
            this.lb_breaking_xyt_breaking1.TabIndex = 70;
            this.lb_breaking_xyt_breaking1.Text = "XYT Breaking1[mm]";
            // 
            // txtbreakingPinDistanceB
            // 
            this.txtbreakingPinDistanceB.Location = new System.Drawing.Point(520, 111);
            this.txtbreakingPinDistanceB.Name = "txtbreakingPinDistanceB";
            this.txtbreakingPinDistanceB.Size = new System.Drawing.Size(61, 21);
            this.txtbreakingPinDistanceB.TabIndex = 82;
            // 
            // lb_breaking_pin_distance_b
            // 
            this.lb_breaking_pin_distance_b.AutoSize = true;
            this.lb_breaking_pin_distance_b.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_pin_distance_b.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_pin_distance_b.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_pin_distance_b.Location = new System.Drawing.Point(368, 115);
            this.lb_breaking_pin_distance_b.Name = "lb_breaking_pin_distance_b";
            this.lb_breaking_pin_distance_b.Size = new System.Drawing.Size(153, 13);
            this.lb_breaking_pin_distance_b.TabIndex = 81;
            this.lb_breaking_pin_distance_b.Text = "Pin Distance B[mm]";
            // 
            // txtbreakingPinDistanceA
            // 
            this.txtbreakingPinDistanceA.Location = new System.Drawing.Point(301, 111);
            this.txtbreakingPinDistanceA.Name = "txtbreakingPinDistanceA";
            this.txtbreakingPinDistanceA.Size = new System.Drawing.Size(61, 21);
            this.txtbreakingPinDistanceA.TabIndex = 71;
            // 
            // lb_breaking_pin_distance_a
            // 
            this.lb_breaking_pin_distance_a.AutoSize = true;
            this.lb_breaking_pin_distance_a.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_pin_distance_a.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_pin_distance_a.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_pin_distance_a.Location = new System.Drawing.Point(143, 115);
            this.lb_breaking_pin_distance_a.Name = "lb_breaking_pin_distance_a";
            this.lb_breaking_pin_distance_a.Size = new System.Drawing.Size(152, 13);
            this.lb_breaking_pin_distance_a.TabIndex = 70;
            this.lb_breaking_pin_distance_a.Text = "Pin Distance A[mm]";
            // 
            // txtbreakingXyMcr1
            // 
            this.txtbreakingXyMcr1.Location = new System.Drawing.Point(301, 84);
            this.txtbreakingXyMcr1.Name = "txtbreakingXyMcr1";
            this.txtbreakingXyMcr1.Size = new System.Drawing.Size(137, 21);
            this.txtbreakingXyMcr1.TabIndex = 78;
            // 
            // panel_breaking_xy_mcr
            // 
            this.panel_breaking_xy_mcr.BackColor = System.Drawing.Color.Red;
            this.panel_breaking_xy_mcr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_breaking_xy_mcr.Controls.Add(this.lb_breaking_xy_mcr);
            this.panel_breaking_xy_mcr.Location = new System.Drawing.Point(139, 84);
            this.panel_breaking_xy_mcr.Name = "panel_breaking_xy_mcr";
            this.panel_breaking_xy_mcr.Size = new System.Drawing.Size(156, 21);
            this.panel_breaking_xy_mcr.TabIndex = 79;
            // 
            // lb_breaking_xy_mcr
            // 
            this.lb_breaking_xy_mcr.AutoSize = true;
            this.lb_breaking_xy_mcr.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_xy_mcr.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_xy_mcr.Location = new System.Drawing.Point(25, 0);
            this.lb_breaking_xy_mcr.Name = "lb_breaking_xy_mcr";
            this.lb_breaking_xy_mcr.Size = new System.Drawing.Size(110, 18);
            this.lb_breaking_xy_mcr.TabIndex = 0;
            this.lb_breaking_xy_mcr.Text = "XY MCR[mm]";
            // 
            // txtbreakingXyAlignMark21
            // 
            this.txtbreakingXyAlignMark21.Location = new System.Drawing.Point(301, 57);
            this.txtbreakingXyAlignMark21.Name = "txtbreakingXyAlignMark21";
            this.txtbreakingXyAlignMark21.Size = new System.Drawing.Size(137, 21);
            this.txtbreakingXyAlignMark21.TabIndex = 75;
            // 
            // panel_breaking_xy_align_mark2
            // 
            this.panel_breaking_xy_align_mark2.BackColor = System.Drawing.Color.Blue;
            this.panel_breaking_xy_align_mark2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_breaking_xy_align_mark2.Controls.Add(this.lb_breaking_xy_align_mark2);
            this.panel_breaking_xy_align_mark2.Location = new System.Drawing.Point(139, 57);
            this.panel_breaking_xy_align_mark2.Name = "panel_breaking_xy_align_mark2";
            this.panel_breaking_xy_align_mark2.Size = new System.Drawing.Size(156, 21);
            this.panel_breaking_xy_align_mark2.TabIndex = 76;
            // 
            // lb_breaking_xy_align_mark2
            // 
            this.lb_breaking_xy_align_mark2.AutoSize = true;
            this.lb_breaking_xy_align_mark2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_xy_align_mark2.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_xy_align_mark2.Location = new System.Drawing.Point(-3, 0);
            this.lb_breaking_xy_align_mark2.Name = "lb_breaking_xy_align_mark2";
            this.lb_breaking_xy_align_mark2.Size = new System.Drawing.Size(160, 18);
            this.lb_breaking_xy_align_mark2.TabIndex = 0;
            this.lb_breaking_xy_align_mark2.Text = "XY Align Mark2[mm]";
            // 
            // txtbreakingXyAlignMark11
            // 
            this.txtbreakingXyAlignMark11.Location = new System.Drawing.Point(301, 30);
            this.txtbreakingXyAlignMark11.Name = "txtbreakingXyAlignMark11";
            this.txtbreakingXyAlignMark11.Size = new System.Drawing.Size(137, 21);
            this.txtbreakingXyAlignMark11.TabIndex = 72;
            // 
            // panel_breaking_xy_align_mark1
            // 
            this.panel_breaking_xy_align_mark1.BackColor = System.Drawing.Color.LimeGreen;
            this.panel_breaking_xy_align_mark1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_breaking_xy_align_mark1.Controls.Add(this.lb_breaking_xy_align_mark1);
            this.panel_breaking_xy_align_mark1.Location = new System.Drawing.Point(139, 30);
            this.panel_breaking_xy_align_mark1.Name = "panel_breaking_xy_align_mark1";
            this.panel_breaking_xy_align_mark1.Size = new System.Drawing.Size(156, 21);
            this.panel_breaking_xy_align_mark1.TabIndex = 73;
            // 
            // lb_breaking_xy_align_mark1
            // 
            this.lb_breaking_xy_align_mark1.AutoSize = true;
            this.lb_breaking_xy_align_mark1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_xy_align_mark1.Location = new System.Drawing.Point(-2, 0);
            this.lb_breaking_xy_align_mark1.Name = "lb_breaking_xy_align_mark1";
            this.lb_breaking_xy_align_mark1.Size = new System.Drawing.Size(160, 18);
            this.lb_breaking_xy_align_mark1.TabIndex = 0;
            this.lb_breaking_xy_align_mark1.Text = "XY Align Mark1[mm]";
            // 
            // txtbreakingXyPinCenter1
            // 
            this.txtbreakingXyPinCenter1.Location = new System.Drawing.Point(301, 3);
            this.txtbreakingXyPinCenter1.Name = "txtbreakingXyPinCenter1";
            this.txtbreakingXyPinCenter1.Size = new System.Drawing.Size(137, 21);
            this.txtbreakingXyPinCenter1.TabIndex = 70;
            // 
            // panel_breaking_xy_pin_center
            // 
            this.panel_breaking_xy_pin_center.BackColor = System.Drawing.Color.Yellow;
            this.panel_breaking_xy_pin_center.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_breaking_xy_pin_center.Controls.Add(this.lb_breaking_xy_pin_center);
            this.panel_breaking_xy_pin_center.Location = new System.Drawing.Point(139, 3);
            this.panel_breaking_xy_pin_center.Name = "panel_breaking_xy_pin_center";
            this.panel_breaking_xy_pin_center.Size = new System.Drawing.Size(156, 21);
            this.panel_breaking_xy_pin_center.TabIndex = 70;
            // 
            // lb_breaking_xy_pin_center
            // 
            this.lb_breaking_xy_pin_center.AutoSize = true;
            this.lb_breaking_xy_pin_center.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_xy_pin_center.Location = new System.Drawing.Point(4, 0);
            this.lb_breaking_xy_pin_center.Name = "lb_breaking_xy_pin_center";
            this.lb_breaking_xy_pin_center.Size = new System.Drawing.Size(151, 18);
            this.lb_breaking_xy_pin_center.TabIndex = 0;
            this.lb_breaking_xy_pin_center.Text = "XY Pin Center[mm]";
            // 
            // btnBreakingSave
            // 
            this.btnBreakingSave.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnBreakingSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakingSave.ForeColor = System.Drawing.Color.White;
            this.btnBreakingSave.Location = new System.Drawing.Point(1601, 266);
            this.btnBreakingSave.Name = "btnBreakingSave";
            this.btnBreakingSave.Size = new System.Drawing.Size(130, 30);
            this.btnBreakingSave.TabIndex = 69;
            this.btnBreakingSave.Text = "Save";
            this.btnBreakingSave.UseVisualStyleBackColor = false;
            // 
            // btnBreakingDelete
            // 
            this.btnBreakingDelete.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnBreakingDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakingDelete.ForeColor = System.Drawing.Color.White;
            this.btnBreakingDelete.Location = new System.Drawing.Point(275, 266);
            this.btnBreakingDelete.Name = "btnBreakingDelete";
            this.btnBreakingDelete.Size = new System.Drawing.Size(130, 30);
            this.btnBreakingDelete.TabIndex = 41;
            this.btnBreakingDelete.Text = "Delete";
            this.btnBreakingDelete.UseVisualStyleBackColor = false;
            this.btnBreakingDelete.Click += new System.EventHandler(this.btnBreakingDelete_Click);
            // 
            // btnBreakingCopy
            // 
            this.btnBreakingCopy.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnBreakingCopy.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakingCopy.ForeColor = System.Drawing.Color.White;
            this.btnBreakingCopy.Location = new System.Drawing.Point(139, 266);
            this.btnBreakingCopy.Name = "btnBreakingCopy";
            this.btnBreakingCopy.Size = new System.Drawing.Size(130, 30);
            this.btnBreakingCopy.TabIndex = 39;
            this.btnBreakingCopy.Text = "Copy";
            this.btnBreakingCopy.UseVisualStyleBackColor = false;
            this.btnBreakingCopy.Click += new System.EventHandler(this.btnBreakingCopy_Click);
            // 
            // btnBreakingMake
            // 
            this.btnBreakingMake.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnBreakingMake.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakingMake.ForeColor = System.Drawing.Color.White;
            this.btnBreakingMake.Location = new System.Drawing.Point(3, 266);
            this.btnBreakingMake.Name = "btnBreakingMake";
            this.btnBreakingMake.Size = new System.Drawing.Size(130, 30);
            this.btnBreakingMake.TabIndex = 38;
            this.btnBreakingMake.Text = "Make";
            this.btnBreakingMake.UseVisualStyleBackColor = false;
            this.btnBreakingMake.Click += new System.EventHandler(this.btnBreakingMake_Click);
            // 
            // lvBreaking
            // 
            this.lvBreaking.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader2});
            this.lvBreaking.GridLines = true;
            this.lvBreaking.Location = new System.Drawing.Point(3, 3);
            this.lvBreaking.Name = "lvBreaking";
            this.lvBreaking.Size = new System.Drawing.Size(130, 257);
            this.lvBreaking.TabIndex = 0;
            this.lvBreaking.UseCompatibleStateImageBehavior = false;
            this.lvBreaking.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Name";
            this.columnHeader2.Width = 120;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.BackColor = System.Drawing.Color.Transparent;
            this.label51.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label51.ForeColor = System.Drawing.Color.White;
            this.label51.Location = new System.Drawing.Point(6, 35);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(16, 13);
            this.label51.TabIndex = 139;
            this.label51.Text = "X";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.BackColor = System.Drawing.Color.Transparent;
            this.label52.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label52.ForeColor = System.Drawing.Color.White;
            this.label52.Location = new System.Drawing.Point(6, 67);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(16, 13);
            this.label52.TabIndex = 140;
            this.label52.Text = "Y";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.BackColor = System.Drawing.Color.Transparent;
            this.label53.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label53.ForeColor = System.Drawing.Color.White;
            this.label53.Location = new System.Drawing.Point(6, 99);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(81, 13);
            this.label53.TabIndex = 141;
            this.label53.Text = "Thickness";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.BackColor = System.Drawing.Color.Transparent;
            this.label54.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label54.ForeColor = System.Drawing.Color.White;
            this.label54.Location = new System.Drawing.Point(6, 131);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(89, 13);
            this.label54.TabIndex = 142;
            this.label54.Text = "Cam To Pin";
            // 
            // textBox56
            // 
            this.textBox56.Location = new System.Drawing.Point(105, 28);
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new System.Drawing.Size(77, 21);
            this.textBox56.TabIndex = 138;
            // 
            // textBox57
            // 
            this.textBox57.Location = new System.Drawing.Point(105, 60);
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new System.Drawing.Size(77, 21);
            this.textBox57.TabIndex = 143;
            // 
            // textBox58
            // 
            this.textBox58.Location = new System.Drawing.Point(105, 92);
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new System.Drawing.Size(77, 21);
            this.textBox58.TabIndex = 144;
            // 
            // textBox59
            // 
            this.textBox59.Location = new System.Drawing.Point(105, 124);
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new System.Drawing.Size(77, 21);
            this.textBox59.TabIndex = 145;
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button9.ForeColor = System.Drawing.Color.White;
            this.button9.Location = new System.Drawing.Point(374, 699);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(77, 30);
            this.button9.TabIndex = 146;
            this.button9.Text = "Save";
            this.button9.UseVisualStyleBackColor = false;
            // 
            // gxtbreaking_zig
            // 
            this.gxtbreaking_zig.BackColor = System.Drawing.Color.Transparent;
            this.gxtbreaking_zig.Controls.Add(this.btnBreakingZigSave);
            this.gxtbreaking_zig.Controls.Add(this.txtbreakingZigCamToPin);
            this.gxtbreaking_zig.Controls.Add(this.txtbreakingZigThickness);
            this.gxtbreaking_zig.Controls.Add(this.txtbreakingZigY);
            this.gxtbreaking_zig.Controls.Add(this.txtbreakingZigX);
            this.gxtbreaking_zig.Controls.Add(this.lb_breaking_zig_cam_to_pin);
            this.gxtbreaking_zig.Controls.Add(this.lb_breaking_zig_thickness);
            this.gxtbreaking_zig.Controls.Add(this.lb_breaking_zig_y);
            this.gxtbreaking_zig.Controls.Add(this.lb_breaking_zig_x);
            this.gxtbreaking_zig.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.gxtbreaking_zig.ForeColor = System.Drawing.Color.White;
            this.gxtbreaking_zig.Location = new System.Drawing.Point(278, 480);
            this.gxtbreaking_zig.Name = "gxtbreaking_zig";
            this.gxtbreaking_zig.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gxtbreaking_zig.Size = new System.Drawing.Size(188, 193);
            this.gxtbreaking_zig.TabIndex = 168;
            this.gxtbreaking_zig.TabStop = false;
            this.gxtbreaking_zig.Text = "브레이킹 Zig";
            // 
            // btnBreakingZigSave
            // 
            this.btnBreakingZigSave.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnBreakingZigSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBreakingZigSave.ForeColor = System.Drawing.Color.White;
            this.btnBreakingZigSave.Location = new System.Drawing.Point(105, 156);
            this.btnBreakingZigSave.Name = "btnBreakingZigSave";
            this.btnBreakingZigSave.Size = new System.Drawing.Size(77, 30);
            this.btnBreakingZigSave.TabIndex = 146;
            this.btnBreakingZigSave.Text = "Save";
            this.btnBreakingZigSave.UseVisualStyleBackColor = false;
            // 
            // txtbreakingZigCamToPin
            // 
            this.txtbreakingZigCamToPin.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtbreakingZigCamToPin.Location = new System.Drawing.Point(105, 124);
            this.txtbreakingZigCamToPin.Name = "txtbreakingZigCamToPin";
            this.txtbreakingZigCamToPin.Size = new System.Drawing.Size(77, 21);
            this.txtbreakingZigCamToPin.TabIndex = 145;
            // 
            // txtbreakingZigThickness
            // 
            this.txtbreakingZigThickness.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtbreakingZigThickness.Location = new System.Drawing.Point(105, 92);
            this.txtbreakingZigThickness.Name = "txtbreakingZigThickness";
            this.txtbreakingZigThickness.Size = new System.Drawing.Size(77, 21);
            this.txtbreakingZigThickness.TabIndex = 144;
            // 
            // txtbreakingZigY
            // 
            this.txtbreakingZigY.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtbreakingZigY.Location = new System.Drawing.Point(105, 60);
            this.txtbreakingZigY.Name = "txtbreakingZigY";
            this.txtbreakingZigY.Size = new System.Drawing.Size(77, 21);
            this.txtbreakingZigY.TabIndex = 143;
            // 
            // txtbreakingZigX
            // 
            this.txtbreakingZigX.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtbreakingZigX.Location = new System.Drawing.Point(105, 28);
            this.txtbreakingZigX.Name = "txtbreakingZigX";
            this.txtbreakingZigX.Size = new System.Drawing.Size(77, 21);
            this.txtbreakingZigX.TabIndex = 138;
            // 
            // lb_breaking_zig_cam_to_pin
            // 
            this.lb_breaking_zig_cam_to_pin.AutoSize = true;
            this.lb_breaking_zig_cam_to_pin.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_zig_cam_to_pin.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_zig_cam_to_pin.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_zig_cam_to_pin.Location = new System.Drawing.Point(6, 131);
            this.lb_breaking_zig_cam_to_pin.Name = "lb_breaking_zig_cam_to_pin";
            this.lb_breaking_zig_cam_to_pin.Size = new System.Drawing.Size(89, 13);
            this.lb_breaking_zig_cam_to_pin.TabIndex = 142;
            this.lb_breaking_zig_cam_to_pin.Text = "Cam To Pin";
            // 
            // lb_breaking_zig_thickness
            // 
            this.lb_breaking_zig_thickness.AutoSize = true;
            this.lb_breaking_zig_thickness.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_zig_thickness.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_zig_thickness.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_zig_thickness.Location = new System.Drawing.Point(6, 99);
            this.lb_breaking_zig_thickness.Name = "lb_breaking_zig_thickness";
            this.lb_breaking_zig_thickness.Size = new System.Drawing.Size(81, 13);
            this.lb_breaking_zig_thickness.TabIndex = 141;
            this.lb_breaking_zig_thickness.Text = "Thickness";
            // 
            // lb_breaking_zig_y
            // 
            this.lb_breaking_zig_y.AutoSize = true;
            this.lb_breaking_zig_y.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_zig_y.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_zig_y.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_zig_y.Location = new System.Drawing.Point(6, 67);
            this.lb_breaking_zig_y.Name = "lb_breaking_zig_y";
            this.lb_breaking_zig_y.Size = new System.Drawing.Size(16, 13);
            this.lb_breaking_zig_y.TabIndex = 140;
            this.lb_breaking_zig_y.Text = "Y";
            // 
            // lb_breaking_zig_x
            // 
            this.lb_breaking_zig_x.AutoSize = true;
            this.lb_breaking_zig_x.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_zig_x.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_zig_x.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_zig_x.Location = new System.Drawing.Point(6, 35);
            this.lb_breaking_zig_x.Name = "lb_breaking_zig_x";
            this.lb_breaking_zig_x.Size = new System.Drawing.Size(16, 13);
            this.lb_breaking_zig_x.TabIndex = 139;
            this.lb_breaking_zig_x.Text = "X";
            // 
            // panel_default_process
            // 
            this.panel_default_process.BackColor = System.Drawing.Color.DimGray;
            this.panel_default_process.Controls.Add(this.txtdefaultProcessPrealignErrorY);
            this.panel_default_process.Controls.Add(this.txtdefaultProcessAlignAngle);
            this.panel_default_process.Controls.Add(this.lb_default_process_prealign_error_y);
            this.panel_default_process.Controls.Add(this.lb_default_process_align_angle);
            this.panel_default_process.Controls.Add(this.txtdefaultProcessPrealignErrorX);
            this.panel_default_process.Controls.Add(this.txtdefaultProcessAlignDistance);
            this.panel_default_process.Controls.Add(this.lb_default_process_prealign_error_x);
            this.panel_default_process.Controls.Add(this.lb_default_process_align_distance);
            this.panel_default_process.Controls.Add(this.txtdefaultProcessBreakErrorY);
            this.panel_default_process.Controls.Add(this.lb_default_process_break_error_y);
            this.panel_default_process.Controls.Add(this.txtdefaultProcessPrealignErrorAngle);
            this.panel_default_process.Controls.Add(this.txtdefaultProcessAlignMatch);
            this.panel_default_process.Controls.Add(this.lb_default_process_prealign_error_angle);
            this.panel_default_process.Controls.Add(this.lb_default_process_align_match);
            this.panel_default_process.Controls.Add(this.btnDefaultProcessSave);
            this.panel_default_process.Controls.Add(this.txtdefaultProcessDefaultZ);
            this.panel_default_process.Controls.Add(this.txtdefaultProcessJumpSpeed);
            this.panel_default_process.Controls.Add(this.lb_default_process_default_z);
            this.panel_default_process.Controls.Add(this.lb_default_process_jump_speed);
            this.panel_default_process.Location = new System.Drawing.Point(506, 598);
            this.panel_default_process.Name = "panel_default_process";
            this.panel_default_process.Size = new System.Drawing.Size(916, 115);
            this.panel_default_process.TabIndex = 171;
            // 
            // txtdefaultProcessPrealignErrorY
            // 
            this.txtdefaultProcessPrealignErrorY.Location = new System.Drawing.Point(817, 46);
            this.txtdefaultProcessPrealignErrorY.Name = "txtdefaultProcessPrealignErrorY";
            this.txtdefaultProcessPrealignErrorY.Size = new System.Drawing.Size(65, 21);
            this.txtdefaultProcessPrealignErrorY.TabIndex = 162;
            // 
            // txtdefaultProcessAlignAngle
            // 
            this.txtdefaultProcessAlignAngle.Location = new System.Drawing.Point(817, 17);
            this.txtdefaultProcessAlignAngle.Name = "txtdefaultProcessAlignAngle";
            this.txtdefaultProcessAlignAngle.Size = new System.Drawing.Size(65, 21);
            this.txtdefaultProcessAlignAngle.TabIndex = 161;
            // 
            // lb_default_process_prealign_error_y
            // 
            this.lb_default_process_prealign_error_y.AutoSize = true;
            this.lb_default_process_prealign_error_y.BackColor = System.Drawing.Color.Transparent;
            this.lb_default_process_prealign_error_y.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_default_process_prealign_error_y.ForeColor = System.Drawing.Color.White;
            this.lb_default_process_prealign_error_y.Location = new System.Drawing.Point(662, 49);
            this.lb_default_process_prealign_error_y.Name = "lb_default_process_prealign_error_y";
            this.lb_default_process_prealign_error_y.Size = new System.Drawing.Size(122, 13);
            this.lb_default_process_prealign_error_y.TabIndex = 160;
            this.lb_default_process_prealign_error_y.Text = "PreAlign Error Y";
            // 
            // lb_default_process_align_angle
            // 
            this.lb_default_process_align_angle.AutoSize = true;
            this.lb_default_process_align_angle.BackColor = System.Drawing.Color.Transparent;
            this.lb_default_process_align_angle.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_default_process_align_angle.ForeColor = System.Drawing.Color.White;
            this.lb_default_process_align_angle.Location = new System.Drawing.Point(662, 20);
            this.lb_default_process_align_angle.Name = "lb_default_process_align_angle";
            this.lb_default_process_align_angle.Size = new System.Drawing.Size(87, 13);
            this.lb_default_process_align_angle.TabIndex = 159;
            this.lb_default_process_align_angle.Text = "Align angle";
            // 
            // txtdefaultProcessPrealignErrorX
            // 
            this.txtdefaultProcessPrealignErrorX.Location = new System.Drawing.Point(587, 46);
            this.txtdefaultProcessPrealignErrorX.Name = "txtdefaultProcessPrealignErrorX";
            this.txtdefaultProcessPrealignErrorX.Size = new System.Drawing.Size(65, 21);
            this.txtdefaultProcessPrealignErrorX.TabIndex = 158;
            // 
            // txtdefaultProcessAlignDistance
            // 
            this.txtdefaultProcessAlignDistance.Location = new System.Drawing.Point(587, 17);
            this.txtdefaultProcessAlignDistance.Name = "txtdefaultProcessAlignDistance";
            this.txtdefaultProcessAlignDistance.Size = new System.Drawing.Size(65, 21);
            this.txtdefaultProcessAlignDistance.TabIndex = 157;
            // 
            // lb_default_process_prealign_error_x
            // 
            this.lb_default_process_prealign_error_x.AutoSize = true;
            this.lb_default_process_prealign_error_x.BackColor = System.Drawing.Color.Transparent;
            this.lb_default_process_prealign_error_x.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_default_process_prealign_error_x.ForeColor = System.Drawing.Color.White;
            this.lb_default_process_prealign_error_x.Location = new System.Drawing.Point(434, 49);
            this.lb_default_process_prealign_error_x.Name = "lb_default_process_prealign_error_x";
            this.lb_default_process_prealign_error_x.Size = new System.Drawing.Size(122, 13);
            this.lb_default_process_prealign_error_x.TabIndex = 156;
            this.lb_default_process_prealign_error_x.Text = "PreAlign Error X";
            // 
            // lb_default_process_align_distance
            // 
            this.lb_default_process_align_distance.AutoSize = true;
            this.lb_default_process_align_distance.BackColor = System.Drawing.Color.Transparent;
            this.lb_default_process_align_distance.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_default_process_align_distance.ForeColor = System.Drawing.Color.White;
            this.lb_default_process_align_distance.Location = new System.Drawing.Point(434, 20);
            this.lb_default_process_align_distance.Name = "lb_default_process_align_distance";
            this.lb_default_process_align_distance.Size = new System.Drawing.Size(149, 13);
            this.lb_default_process_align_distance.TabIndex = 155;
            this.lb_default_process_align_distance.Text = "Align distance[mm]";
            // 
            // txtdefaultProcessBreakErrorY
            // 
            this.txtdefaultProcessBreakErrorY.Location = new System.Drawing.Point(360, 75);
            this.txtdefaultProcessBreakErrorY.Name = "txtdefaultProcessBreakErrorY";
            this.txtdefaultProcessBreakErrorY.Size = new System.Drawing.Size(65, 21);
            this.txtdefaultProcessBreakErrorY.TabIndex = 154;
            // 
            // lb_default_process_break_error_y
            // 
            this.lb_default_process_break_error_y.AutoSize = true;
            this.lb_default_process_break_error_y.BackColor = System.Drawing.Color.Transparent;
            this.lb_default_process_break_error_y.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_default_process_break_error_y.ForeColor = System.Drawing.Color.White;
            this.lb_default_process_break_error_y.Location = new System.Drawing.Point(203, 77);
            this.lb_default_process_break_error_y.Name = "lb_default_process_break_error_y";
            this.lb_default_process_break_error_y.Size = new System.Drawing.Size(103, 13);
            this.lb_default_process_break_error_y.TabIndex = 153;
            this.lb_default_process_break_error_y.Text = "Break Error Y";
            // 
            // txtdefaultProcessPrealignErrorAngle
            // 
            this.txtdefaultProcessPrealignErrorAngle.Location = new System.Drawing.Point(360, 46);
            this.txtdefaultProcessPrealignErrorAngle.Name = "txtdefaultProcessPrealignErrorAngle";
            this.txtdefaultProcessPrealignErrorAngle.Size = new System.Drawing.Size(65, 21);
            this.txtdefaultProcessPrealignErrorAngle.TabIndex = 152;
            // 
            // txtdefaultProcessAlignMatch
            // 
            this.txtdefaultProcessAlignMatch.Location = new System.Drawing.Point(360, 17);
            this.txtdefaultProcessAlignMatch.Name = "txtdefaultProcessAlignMatch";
            this.txtdefaultProcessAlignMatch.Size = new System.Drawing.Size(65, 21);
            this.txtdefaultProcessAlignMatch.TabIndex = 151;
            // 
            // lb_default_process_prealign_error_angle
            // 
            this.lb_default_process_prealign_error_angle.AutoSize = true;
            this.lb_default_process_prealign_error_angle.BackColor = System.Drawing.Color.Transparent;
            this.lb_default_process_prealign_error_angle.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_default_process_prealign_error_angle.ForeColor = System.Drawing.Color.White;
            this.lb_default_process_prealign_error_angle.Location = new System.Drawing.Point(203, 48);
            this.lb_default_process_prealign_error_angle.Name = "lb_default_process_prealign_error_angle";
            this.lb_default_process_prealign_error_angle.Size = new System.Drawing.Size(153, 13);
            this.lb_default_process_prealign_error_angle.TabIndex = 150;
            this.lb_default_process_prealign_error_angle.Text = "PreAlign Error Angle";
            // 
            // lb_default_process_align_match
            // 
            this.lb_default_process_align_match.AutoSize = true;
            this.lb_default_process_align_match.BackColor = System.Drawing.Color.Transparent;
            this.lb_default_process_align_match.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_default_process_align_match.ForeColor = System.Drawing.Color.White;
            this.lb_default_process_align_match.Location = new System.Drawing.Point(203, 20);
            this.lb_default_process_align_match.Name = "lb_default_process_align_match";
            this.lb_default_process_align_match.Size = new System.Drawing.Size(130, 13);
            this.lb_default_process_align_match.TabIndex = 149;
            this.lb_default_process_align_match.Text = "Align match[mm]";
            // 
            // btnDefaultProcessSave
            // 
            this.btnDefaultProcessSave.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnDefaultProcessSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnDefaultProcessSave.ForeColor = System.Drawing.Color.White;
            this.btnDefaultProcessSave.Location = new System.Drawing.Point(778, 79);
            this.btnDefaultProcessSave.Name = "btnDefaultProcessSave";
            this.btnDefaultProcessSave.Size = new System.Drawing.Size(130, 30);
            this.btnDefaultProcessSave.TabIndex = 138;
            this.btnDefaultProcessSave.Text = "Save";
            this.btnDefaultProcessSave.UseVisualStyleBackColor = false;
            // 
            // txtdefaultProcessDefaultZ
            // 
            this.txtdefaultProcessDefaultZ.Location = new System.Drawing.Point(128, 65);
            this.txtdefaultProcessDefaultZ.Name = "txtdefaultProcessDefaultZ";
            this.txtdefaultProcessDefaultZ.Size = new System.Drawing.Size(65, 21);
            this.txtdefaultProcessDefaultZ.TabIndex = 148;
            // 
            // txtdefaultProcessJumpSpeed
            // 
            this.txtdefaultProcessJumpSpeed.Location = new System.Drawing.Point(128, 36);
            this.txtdefaultProcessJumpSpeed.Name = "txtdefaultProcessJumpSpeed";
            this.txtdefaultProcessJumpSpeed.Size = new System.Drawing.Size(65, 21);
            this.txtdefaultProcessJumpSpeed.TabIndex = 147;
            // 
            // lb_default_process_default_z
            // 
            this.lb_default_process_default_z.AutoSize = true;
            this.lb_default_process_default_z.BackColor = System.Drawing.Color.Transparent;
            this.lb_default_process_default_z.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_default_process_default_z.ForeColor = System.Drawing.Color.White;
            this.lb_default_process_default_z.Location = new System.Drawing.Point(15, 67);
            this.lb_default_process_default_z.Name = "lb_default_process_default_z";
            this.lb_default_process_default_z.Size = new System.Drawing.Size(111, 13);
            this.lb_default_process_default_z.TabIndex = 128;
            this.lb_default_process_default_z.Text = "Default Z[mm]";
            // 
            // lb_default_process_jump_speed
            // 
            this.lb_default_process_jump_speed.AutoSize = true;
            this.lb_default_process_jump_speed.BackColor = System.Drawing.Color.Transparent;
            this.lb_default_process_jump_speed.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_default_process_jump_speed.ForeColor = System.Drawing.Color.White;
            this.lb_default_process_jump_speed.Location = new System.Drawing.Point(15, 39);
            this.lb_default_process_jump_speed.Name = "lb_default_process_jump_speed";
            this.lb_default_process_jump_speed.Size = new System.Drawing.Size(94, 13);
            this.lb_default_process_jump_speed.TabIndex = 127;
            this.lb_default_process_jump_speed.Text = "Jump speed";
            // 
            // panel_default_laser
            // 
            this.panel_default_laser.BackColor = System.Drawing.Color.DimGray;
            this.panel_default_laser.Controls.Add(this.btnDefaultLaserSave);
            this.panel_default_laser.Controls.Add(this.txtdefaultLaserCstPitchReverse);
            this.panel_default_laser.Controls.Add(this.txtdefaultLaserCstPitchNormal);
            this.panel_default_laser.Controls.Add(this.txtdefaultLaserOntime);
            this.panel_default_laser.Controls.Add(this.lb_default_laser_cst_pitch_reverse);
            this.panel_default_laser.Controls.Add(this.lb_default_laser_cst_pitch_normal);
            this.panel_default_laser.Controls.Add(this.lb_default_laser_ontime);
            this.panel_default_laser.Location = new System.Drawing.Point(506, 480);
            this.panel_default_laser.Name = "panel_default_laser";
            this.panel_default_laser.Size = new System.Drawing.Size(916, 115);
            this.panel_default_laser.TabIndex = 170;
            // 
            // btnDefaultLaserSave
            // 
            this.btnDefaultLaserSave.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnDefaultLaserSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnDefaultLaserSave.ForeColor = System.Drawing.Color.White;
            this.btnDefaultLaserSave.Location = new System.Drawing.Point(778, 80);
            this.btnDefaultLaserSave.Name = "btnDefaultLaserSave";
            this.btnDefaultLaserSave.Size = new System.Drawing.Size(130, 30);
            this.btnDefaultLaserSave.TabIndex = 138;
            this.btnDefaultLaserSave.Text = "Save";
            this.btnDefaultLaserSave.UseVisualStyleBackColor = false;
            // 
            // txtdefaultLaserCstPitchReverse
            // 
            this.txtdefaultLaserCstPitchReverse.Location = new System.Drawing.Point(560, 44);
            this.txtdefaultLaserCstPitchReverse.Name = "txtdefaultLaserCstPitchReverse";
            this.txtdefaultLaserCstPitchReverse.Size = new System.Drawing.Size(108, 21);
            this.txtdefaultLaserCstPitchReverse.TabIndex = 149;
            // 
            // txtdefaultLaserCstPitchNormal
            // 
            this.txtdefaultLaserCstPitchNormal.Location = new System.Drawing.Point(204, 44);
            this.txtdefaultLaserCstPitchNormal.Name = "txtdefaultLaserCstPitchNormal";
            this.txtdefaultLaserCstPitchNormal.Size = new System.Drawing.Size(108, 21);
            this.txtdefaultLaserCstPitchNormal.TabIndex = 148;
            // 
            // txtdefaultLaserOntime
            // 
            this.txtdefaultLaserOntime.Location = new System.Drawing.Point(204, 15);
            this.txtdefaultLaserOntime.Name = "txtdefaultLaserOntime";
            this.txtdefaultLaserOntime.Size = new System.Drawing.Size(109, 21);
            this.txtdefaultLaserOntime.TabIndex = 147;
            // 
            // lb_default_laser_cst_pitch_reverse
            // 
            this.lb_default_laser_cst_pitch_reverse.AutoSize = true;
            this.lb_default_laser_cst_pitch_reverse.BackColor = System.Drawing.Color.Transparent;
            this.lb_default_laser_cst_pitch_reverse.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_default_laser_cst_pitch_reverse.ForeColor = System.Drawing.Color.White;
            this.lb_default_laser_cst_pitch_reverse.Location = new System.Drawing.Point(367, 46);
            this.lb_default_laser_cst_pitch_reverse.Name = "lb_default_laser_cst_pitch_reverse";
            this.lb_default_laser_cst_pitch_reverse.Size = new System.Drawing.Size(181, 13);
            this.lb_default_laser_cst_pitch_reverse.TabIndex = 129;
            this.lb_default_laser_cst_pitch_reverse.Text = "Cassette pitch(Reverse)";
            // 
            // lb_default_laser_cst_pitch_normal
            // 
            this.lb_default_laser_cst_pitch_normal.AutoSize = true;
            this.lb_default_laser_cst_pitch_normal.BackColor = System.Drawing.Color.Transparent;
            this.lb_default_laser_cst_pitch_normal.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_default_laser_cst_pitch_normal.ForeColor = System.Drawing.Color.White;
            this.lb_default_laser_cst_pitch_normal.Location = new System.Drawing.Point(17, 46);
            this.lb_default_laser_cst_pitch_normal.Name = "lb_default_laser_cst_pitch_normal";
            this.lb_default_laser_cst_pitch_normal.Size = new System.Drawing.Size(172, 13);
            this.lb_default_laser_cst_pitch_normal.TabIndex = 128;
            this.lb_default_laser_cst_pitch_normal.Text = "Cassette pitch(Normal)";
            // 
            // lb_default_laser_ontime
            // 
            this.lb_default_laser_ontime.AutoSize = true;
            this.lb_default_laser_ontime.BackColor = System.Drawing.Color.Transparent;
            this.lb_default_laser_ontime.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_default_laser_ontime.ForeColor = System.Drawing.Color.White;
            this.lb_default_laser_ontime.Location = new System.Drawing.Point(17, 17);
            this.lb_default_laser_ontime.Name = "lb_default_laser_ontime";
            this.lb_default_laser_ontime.Size = new System.Drawing.Size(86, 13);
            this.lb_default_laser_ontime.TabIndex = 127;
            this.lb_default_laser_ontime.Text = "On time[s]";
            // 
            // Recipe_ProCondition
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.Controls.Add(this.panel_default_process);
            this.Controls.Add(this.panel_default_laser);
            this.Controls.Add(this.gxtbreaking_zig);
            this.Controls.Add(this.panel_breaking);
            this.Controls.Add(this.panel_process);
            this.Name = "Recipe_ProCondition";
            this.Size = new System.Drawing.Size(1740, 860);
            this.panel_process.ResumeLayout(false);
            this.panel_process.PerformLayout();
            this.panel_breaking.ResumeLayout(false);
            this.panel_breaking.PerformLayout();
            this.gxtbreaking_load_trans_sequence_offset.ResumeLayout(false);
            this.gxtbreaking_load_trans_sequence_offset_b.ResumeLayout(false);
            this.gxtbreaking_load_trans_sequence_offset_b.PerformLayout();
            this.gxtbreaking_load_trans_sequence_offset_a.ResumeLayout(false);
            this.gxtbreaking_load_trans_sequence_offset_a.PerformLayout();
            this.panel_breaking_half.ResumeLayout(false);
            this.panel_breaking_half.PerformLayout();
            this.panel_breaking_right.ResumeLayout(false);
            this.panel_breaking_right.PerformLayout();
            this.panel_breaking_left.ResumeLayout(false);
            this.panel_breaking_left.PerformLayout();
            this.panel_breaking_xy_mcr.ResumeLayout(false);
            this.panel_breaking_xy_mcr.PerformLayout();
            this.panel_breaking_xy_align_mark2.ResumeLayout(false);
            this.panel_breaking_xy_align_mark2.PerformLayout();
            this.panel_breaking_xy_align_mark1.ResumeLayout(false);
            this.panel_breaking_xy_align_mark1.PerformLayout();
            this.panel_breaking_xy_pin_center.ResumeLayout(false);
            this.panel_breaking_xy_pin_center.PerformLayout();
            this.gxtbreaking_zig.ResumeLayout(false);
            this.gxtbreaking_zig.PerformLayout();
            this.panel_default_process.ResumeLayout(false);
            this.panel_default_process.PerformLayout();
            this.panel_default_laser.ResumeLayout(false);
            this.panel_default_laser.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_process;
        private System.Windows.Forms.ListView lvProcess;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.Button btnProcessCopy;
        private System.Windows.Forms.Button btnProcessMake;
        private System.Windows.Forms.TextBox txtprocessPowerTatio;
        private System.Windows.Forms.Label lb_process_power_ratio;
        private System.Windows.Forms.TextBox txtprocessPower;
        private System.Windows.Forms.Label lb_process_power;
        private System.Windows.Forms.TextBox txtprocessDivider;
        private System.Windows.Forms.Button btnProcessDelete;
        private System.Windows.Forms.Label lb_process_divider;
        private System.Windows.Forms.Button btnProcessSave;
        private System.Windows.Forms.TextBox txtprocessAccDec;
        private System.Windows.Forms.Label lb_process_acc_dec;
        private System.Windows.Forms.TextBox txtprocessScan;
        private System.Windows.Forms.Label lb_process_scan;
        private System.Windows.Forms.TextBox txtprocessG;
        private System.Windows.Forms.Label lb_process_g;
        private System.Windows.Forms.TextBox txtprocessSegmentValue;
        private System.Windows.Forms.Label lb_process_segment_value;
        private System.Windows.Forms.TextBox txtprocessOverlap;
        private System.Windows.Forms.Label lb_process_overlap;
        private System.Windows.Forms.TextBox txtprocessZpos;
        private System.Windows.Forms.Label lb_process_zpos;
        private System.Windows.Forms.TextBox txtprocessSpeed;
        private System.Windows.Forms.Label lb_process_speed;
        private System.Windows.Forms.TextBox txtprocessFrequence;
        private System.Windows.Forms.Label lb_process_frequence;
        private System.Windows.Forms.TextBox txtprocessBurst;
        private System.Windows.Forms.Label lb_process_burst;
        private System.Windows.Forms.TextBox txtprocessError;
        private System.Windows.Forms.Label lb_process_error;
        private System.Windows.Forms.Panel panel_breaking;
        private System.Windows.Forms.Button btnBreakingSave;
        private System.Windows.Forms.Button btnBreakingDelete;
        private System.Windows.Forms.Button btnBreakingCopy;
        private System.Windows.Forms.Button btnBreakingMake;
        private System.Windows.Forms.ListView lvBreaking;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Panel panel_breaking_half;
        private System.Windows.Forms.Label lb_breaking_half;
        private System.Windows.Forms.Panel panel_breaking_right;
        private System.Windows.Forms.Label lb_breaking_right;
        private System.Windows.Forms.TextBox txtbreakingXytBreaking13;
        private System.Windows.Forms.TextBox txtbreakingXytBreaking12;
        private System.Windows.Forms.TextBox txtbreakingXytBreaking11;
        private System.Windows.Forms.Panel panel_breaking_left;
        private System.Windows.Forms.Label lb_breaking_left;
        private System.Windows.Forms.TextBox txtbreakingXyMcr2;
        private System.Windows.Forms.TextBox txtbreakingXyAlignMark22;
        private System.Windows.Forms.TextBox txtbreakingXyAlignMark12;
        private System.Windows.Forms.TextBox txtbreakingXyPinCenter2;
        private System.Windows.Forms.Label lb_breaking_xyt_breaking1;
        private System.Windows.Forms.TextBox txtbreakingPinDistanceB;
        private System.Windows.Forms.Label lb_breaking_pin_distance_b;
        private System.Windows.Forms.TextBox txtbreakingPinDistanceA;
        private System.Windows.Forms.Label lb_breaking_pin_distance_a;
        private System.Windows.Forms.TextBox txtbreakingXyMcr1;
        private System.Windows.Forms.Panel panel_breaking_xy_mcr;
        private System.Windows.Forms.Label lb_breaking_xy_mcr;
        private System.Windows.Forms.TextBox txtbreakingXyAlignMark21;
        private System.Windows.Forms.Panel panel_breaking_xy_align_mark2;
        private System.Windows.Forms.Label lb_breaking_xy_align_mark2;
        private System.Windows.Forms.TextBox txtbreakingXyAlignMark11;
        private System.Windows.Forms.Panel panel_breaking_xy_align_mark1;
        private System.Windows.Forms.Label lb_breaking_xy_align_mark1;
        private System.Windows.Forms.TextBox txtbreakingXyPinCenter1;
        private System.Windows.Forms.Panel panel_breaking_xy_pin_center;
        private System.Windows.Forms.Label lb_breaking_xy_pin_center;
        private System.Windows.Forms.TextBox txtbreakingBMcrOffsetXy2;
        private System.Windows.Forms.TextBox txtbreakingAMcrOffsetXy2;
        private System.Windows.Forms.TextBox txtbreakingBMcrOffsetXy1;
        private System.Windows.Forms.Label lb_breaking_b_mcr_offset_xy;
        private System.Windows.Forms.TextBox txtbreakingAMcrOffsetXy1;
        private System.Windows.Forms.Label lb_breaking_a_mcr_offset_xy;
        private System.Windows.Forms.TextBox txtbreakingB2XyOffset2;
        private System.Windows.Forms.TextBox txtbreakingB2XyOffset1;
        private System.Windows.Forms.Label lb_breaking_b2_xy_offset;
        private System.Windows.Forms.TextBox txtbreakingB1XyOffset2;
        private System.Windows.Forms.TextBox txtbreakingB1XyOffset1;
        private System.Windows.Forms.Label lb_breaking_b1_xy_offset;
        private System.Windows.Forms.TextBox txtbreakingA2XyOffset2;
        private System.Windows.Forms.TextBox txtbreakingA2XyOffset1;
        private System.Windows.Forms.Label lb_breaking_a2_xy_offset;
        private System.Windows.Forms.TextBox txtbreakingA1XyOffset2;
        private System.Windows.Forms.TextBox txtbreakingA1XyOffset1;
        private System.Windows.Forms.Label lb_breaking_a1_xy_offset;
        private System.Windows.Forms.TextBox txtbreakingXytBreaking43;
        private System.Windows.Forms.TextBox txtbreakingXytBreaking42;
        private System.Windows.Forms.TextBox txtbreakingXytBreaking41;
        private System.Windows.Forms.Label lb_breaking_xyt_breaking4;
        private System.Windows.Forms.TextBox txtbreakingXytBreaking33;
        private System.Windows.Forms.TextBox txtbreakingXytBreaking32;
        private System.Windows.Forms.TextBox txtbreakingXytBreaking31;
        private System.Windows.Forms.Label lb_breaking_xyt_breaking3;
        private System.Windows.Forms.TextBox txtbreakingXytBreaking23;
        private System.Windows.Forms.TextBox txtbreakingXytBreaking22;
        private System.Windows.Forms.TextBox txtbreakingXytBreaking21;
        private System.Windows.Forms.Label lb_breaking_xyt_breaking2;
        private System.Windows.Forms.Panel panelBreaking2pass;
        private System.Windows.Forms.Label lb_breaking_2pass;
        private System.Windows.Forms.TextBox txtbreakingSlowdownPitch;
        private System.Windows.Forms.TextBox txtbreakingFastdownPitch;
        private System.Windows.Forms.Label lb_breaking_fastdown_ptich;
        private System.Windows.Forms.TextBox txtbreakingSlowdownSpeed;
        private System.Windows.Forms.Label lb_breaking_slowdown_speed;
        private System.Windows.Forms.TextBox txtbreakingFastdownSpeed;
        private System.Windows.Forms.Label lb_breaking_fastdown_speed;
        private System.Windows.Forms.Label lb_breaking_slowdown_ptich_mm;
        private System.Windows.Forms.Label lb_breaking_fastdown_ptich_mm;
        private System.Windows.Forms.Label lb_breaking_slowdown_ptich;
        private System.Windows.Forms.GroupBox gxtbreaking_load_trans_sequence_offset;
        private System.Windows.Forms.GroupBox gxtbreaking_load_trans_sequence_offset_b;
        private System.Windows.Forms.Label lb_breaking_load_trans_sequence_offset_b_y2_mm;
        private System.Windows.Forms.TextBox txtbreakingLoadTransSequenceOffsetBY1;
        private System.Windows.Forms.Label lb_breaking_load_trans_sequence_offset_b_y1_mm;
        private System.Windows.Forms.Label lb_breaking_load_trans_sequence_offset_b_y1;
        private System.Windows.Forms.TextBox txtbreakingLoadTransSequenceOffsetBY2;
        private System.Windows.Forms.Label lb_breaking_load_trans_sequence_offset_b_y2;
        private System.Windows.Forms.GroupBox gxtbreaking_load_trans_sequence_offset_a;
        private System.Windows.Forms.Label lb_breaking_load_trans_sequence_offset_a_y2_mm;
        private System.Windows.Forms.Label lb_breaking_load_trans_sequence_offset_a_y1_mm;
        private System.Windows.Forms.TextBox txtbreakingLoadTransSequenceOffsetAY2;
        private System.Windows.Forms.TextBox txtbreakingLoadTransSequenceOffsetAY1;
        private System.Windows.Forms.Label lb_breaking_load_trans_sequence_offset_a_y2;
        private System.Windows.Forms.Label lb_breaking_load_trans_sequence_offset_a_y1;
        private System.Windows.Forms.TextBox textBox59;
        private System.Windows.Forms.TextBox textBox58;
        private System.Windows.Forms.TextBox textBox57;
        private System.Windows.Forms.TextBox textBox56;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.GroupBox gxtbreaking_zig;
        private System.Windows.Forms.Button btnBreakingZigSave;
        private System.Windows.Forms.TextBox txtbreakingZigCamToPin;
        private System.Windows.Forms.TextBox txtbreakingZigThickness;
        private System.Windows.Forms.TextBox txtbreakingZigY;
        private System.Windows.Forms.TextBox txtbreakingZigX;
        private System.Windows.Forms.Label lb_breaking_zig_cam_to_pin;
        private System.Windows.Forms.Label lb_breaking_zig_thickness;
        private System.Windows.Forms.Label lb_breaking_zig_y;
        private System.Windows.Forms.Label lb_breaking_zig_x;
        private System.Windows.Forms.Panel panel_default_process;
        private System.Windows.Forms.TextBox txtdefaultProcessPrealignErrorY;
        private System.Windows.Forms.TextBox txtdefaultProcessAlignAngle;
        private System.Windows.Forms.Label lb_default_process_prealign_error_y;
        private System.Windows.Forms.Label lb_default_process_align_angle;
        private System.Windows.Forms.TextBox txtdefaultProcessPrealignErrorX;
        private System.Windows.Forms.TextBox txtdefaultProcessAlignDistance;
        private System.Windows.Forms.Label lb_default_process_prealign_error_x;
        private System.Windows.Forms.Label lb_default_process_align_distance;
        private System.Windows.Forms.TextBox txtdefaultProcessBreakErrorY;
        private System.Windows.Forms.Label lb_default_process_break_error_y;
        private System.Windows.Forms.TextBox txtdefaultProcessPrealignErrorAngle;
        private System.Windows.Forms.TextBox txtdefaultProcessAlignMatch;
        private System.Windows.Forms.Label lb_default_process_prealign_error_angle;
        private System.Windows.Forms.Label lb_default_process_align_match;
        private System.Windows.Forms.Button btnDefaultProcessSave;
        private System.Windows.Forms.TextBox txtdefaultProcessDefaultZ;
        private System.Windows.Forms.TextBox txtdefaultProcessJumpSpeed;
        private System.Windows.Forms.Label lb_default_process_default_z;
        private System.Windows.Forms.Label lb_default_process_jump_speed;
        private System.Windows.Forms.Panel panel_default_laser;
        private System.Windows.Forms.Button btnDefaultLaserSave;
        private System.Windows.Forms.TextBox txtdefaultLaserCstPitchReverse;
        private System.Windows.Forms.TextBox txtdefaultLaserCstPitchNormal;
        private System.Windows.Forms.TextBox txtdefaultLaserOntime;
        private System.Windows.Forms.Label lb_default_laser_cst_pitch_reverse;
        private System.Windows.Forms.Label lb_default_laser_cst_pitch_normal;
        private System.Windows.Forms.Label lb_default_laser_ontime;
        private System.Windows.Forms.Button btnBreakingXytBreaking4Calc;
        private System.Windows.Forms.Button btnBreakingXytBreaking3Calc;
        private System.Windows.Forms.Button btnBreakingXytBreaking2Calc;
        private System.Windows.Forms.Button btnBreakingXytBreaking1Calc;
    }
}
