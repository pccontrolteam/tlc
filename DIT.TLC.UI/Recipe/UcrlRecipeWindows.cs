﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DIT.TLC.CTRL;

namespace DIT.TLC.UI
{
    public partial class Recipe_Recipe : UserControl
    {
        public Recipe_Recipe()
        {
            InitializeComponent();
        }
        public void FillLstRecipe()
        {
            int iPos = 0;
            lvRecipe.Items.Clear();

            foreach (EqpRecipe recp in GG.Equip.EqpRecipeMgr.LstEqpRecipe)
                lvRecipe.Items.Add(new ListViewItem(new string[] { iPos.ToString(), recp.Name, recp.Cim ? "O" : "X", recp.Used ? "O" : "X" }));
        }

        string recipeName;
        private void btnRecipeMake_Click(object sender, EventArgs e)
        {
            //EqpRecipe recp = new EqpRecipe() { Name = "TTTT" };
            EqpRecipe recp = new EqpRecipe();

            recipeName = txtrecipe.Text; //= lvRecipe.SelectedItems[0].SubItems[1].Text;
            //"이름 입력 요망"
            if (string.IsNullOrEmpty(recipeName))
            {
                MessageBox.Show("레시피 이름이 없습니다.!");
                return;
            }

            GG.Equip.EqpRecipeMgr.LstEqpRecipe.Add(recp);
            FillLstRecipe();
            lvRecipe.Items[lvRecipe.Items.Count - 1].Selected = true;
            
        }

        private void btnRecipeCopy_Click(object sender, EventArgs e)
        {
            if (lvRecipe.SelectedItems.Count <= 0) return;
            string recipeName = lvRecipe.SelectedItems[0].SubItems[1].Text;

            EqpRecipe recp  = GG.Equip.EqpRecipeMgr.LstEqpRecipe.FirstOrDefault( f => f.Name == recipeName);

            EqpRecipe recpCopy = (EqpRecipe)recp.Clone();            
            
            //"이름 입력 요망"
            recpCopy.Name = recipeName;            
            GG.Equip.EqpRecipeMgr.LstEqpRecipe.Add(recpCopy);
            FillData(recpCopy);
        }

        private void btnRecipeSave_Click(object sender, EventArgs e)
        {
            string recipeName = lvRecipe.SelectedItems[0].SubItems[1].Text;
            EqpRecipe recp = GG.Equip.EqpRecipeMgr.LstEqpRecipe.FirstOrDefault(f => f.Name == recipeName);
            UpdateRecipe(ref recp);
            GG.Equip.EqpRecipeMgr.Save();

        }

        private void btnRecipeApply_Click(object sender, EventArgs e)
        {
            //save apply 차이점?
            string recipeName = lvRecipe.SelectedItems[0].SubItems[1].Text;
            EqpRecipe recp = GG.Equip.EqpRecipeMgr.LstEqpRecipe.FirstOrDefault(f => f.Name == recipeName);
            UpdateRecipe(ref recp);
            GG.Equip.EqpRecipeMgr.Save();
        }

        private void btnRecipeDelete_Click(object sender, EventArgs e)
        {
            string recipeName = lvRecipe.SelectedItems[0].SubItems[1].Text;
            EqpRecipe recp = GG.Equip.EqpRecipeMgr.LstEqpRecipe.FirstOrDefault(f => f.Name == recipeName);
            GG.Equip.EqpRecipeMgr.LstEqpRecipe.Remove(recp);

        }

        private void btnRecipeCimApply_Click(object sender, EventArgs e)
        {

        }
        private void btnRecipeCimDelete_Click(object sender, EventArgs e)
        {

        }

        private void lvRecipe_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvRecipe.SelectedItems.Count <= 0) return;
            string recipeName = lvRecipe.SelectedItems[0].SubItems[1].Text;
            EqpRecipe rcep = GG.Equip.EqpRecipeMgr.LstEqpRecipe.FirstOrDefault(f => f.Name == recipeName);

        }

        public void FillData(EqpRecipe rcep)
        {
            #region FillData
            txtrecipe.Text                      = rcep.Name;
            txttable1Alignmark11.Text           = rcep.Table1AlignMark1.X.ToString();
            txttable1Alignmark12.Text           = rcep.Table1AlignMark1.Y.ToString();
            txttable1Alignmark21.Text           = rcep.Table1AlignMark2.X.ToString();
            txttable1Alignmark22.Text           = rcep.Table1AlignMark2.Y.ToString();
            txttable2Alignmark11.Text           = rcep.Table2AlignMark1.X.ToString();
            txttable2Alignmark12.Text           = rcep.Table2AlignMark1.Y.ToString();
            txttable2Alignmark21.Text           = rcep.Table2AlignMark2.X.ToString();
            txttable2Alignmark22.Text           = rcep.Table2AlignMark2.Y.ToString();
            txtbreakLeft11.Text                 = rcep.BreakLeft1.X.ToString();      
            txtbreakLeft12.Text                 = rcep.BreakLeft1.Y.ToString();      
            txtbreakLeft21.Text                 = rcep.BreakLeft2.X.ToString();      
            txtbreakLeft22.Text                 = rcep.BreakLeft2.Y.ToString();
            txtbreakLeftY.Text                  = rcep.BreakLeftY.ToString();
            txtbreakRight11.Text                = rcep.BreakRight1.X.ToString();     
            txtbreakRight12.Text                = rcep.BreakRight1.Y.ToString();     
            txtbreakRight21.Text                = rcep.BreakRight2.X.ToString();     
            txtbreakRight22.Text                = rcep.BreakRight2.Y.ToString();     
            txtbreakRightY.Text                 = rcep.BreakRightY.ToString();
            txtcellSize1.Text                   = rcep.AMatrixSize.X.ToString();
            txtcellSize2.Text                   = rcep.AMatrixSize.Y.ToString();
            comxtalignmentLayer.Text            = rcep.AlignMentLayer.ToString();
            comxtprocessLayer.Text              = rcep.ProcessLayer.ToString();
            comxtguideLayer.Text                = rcep.GuideLayer.ToString();
            comxtprocessParam.Text              = rcep.ProcessParam.ToString();
            comxtcellType.Text                  = rcep.CellType.ToString();
            txttThickness.Text                  = rcep.TaThickness.ToString();

            //Cassette
            txtwidth.Text                       = rcep.CassetteWidth.ToString();
            txtheight.Text                      = rcep.CassetteHeight.ToString();
            txtmaxHeightCount.Text              = rcep.CassetteMaxHeightCount.ToString();
            txtcellCount.Text                   = rcep.CellType.ToString();

            //Picker

            //LDS Process
            txtldsProcess11.Text                = rcep.LDSProcess01.X.ToString();
            txtldsProcess12.Text                = rcep.LDSProcess01.Y.ToString();
            txtldsProcess21.Text                = rcep.LDSProcess02.X.ToString();
            txtldsProcess22.Text                = rcep.LDSProcess02.Y.ToString();
            txtldsProcess31.Text                = rcep.LDSProcess03.X.ToString();
            txtldsProcess32.Text                = rcep.LDSProcess03.Y.ToString();
            txtldsProcess41.Text                = rcep.LDSProcess04.X.ToString();
            txtldsProcess42.Text                = rcep.LDSProcess04.Y.ToString();

            //LDS Break
            txtldsBreak11.Text                  = rcep.LDSBreak01.X.ToString();
            txtldsBreak12.Text                  = rcep.LDSBreak01.Y.ToString();
            txtldsBreak21.Text                  = rcep.LDSBreak02.X.ToString();
            txtldsBreak22.Text                  = rcep.LDSBreak02.Y.ToString();
            txtldsBreak31.Text                  = rcep.LDSBreak03.X.ToString();
            txtldsBreak32.Text                  = rcep.LDSBreak03.Y.ToString();
            txtldsBreak41.Text                  = rcep.LDSBreak04.X.ToString();
            txtldsBreak42.Text                  = rcep.LDSBreak04.Y.ToString();

            //Y-Offset
            txtyOffsetPre.Text                  = rcep.OffsetPre.ToString();
            txtyOffsetBreak.Text                = rcep.OffsetBreak.ToString();
            #endregion
        }
        public void UpdateRecipe(ref EqpRecipe rcep)
        {
            #region UpdateRecipe
            rcep.Name                       = txtrecipe.Text;
            rcep.Table1AlignMark1.X         = double.Parse(txttable1Alignmark11.Text);
            rcep.Table1AlignMark1.Y         = double.Parse(txttable1Alignmark12.Text);
            rcep.Table1AlignMark2.X         = double.Parse(txttable1Alignmark21.Text);
            rcep.Table1AlignMark2.Y         = double.Parse(txttable1Alignmark22.Text);
            rcep.Table2AlignMark1.X         = double.Parse(txttable2Alignmark11.Text);
            rcep.Table2AlignMark1.Y         = double.Parse(txttable2Alignmark12.Text);
            rcep.Table2AlignMark2.X         = double.Parse(txttable2Alignmark21.Text);
            rcep.Table2AlignMark2.Y         = double.Parse(txttable2Alignmark22.Text);
            rcep.BreakLeft1.X               = double.Parse(txtbreakLeft11.Text);
            rcep.BreakLeft1.Y               = double.Parse(txtbreakLeft12.Text);
            rcep.BreakLeft2.X               = double.Parse(txtbreakLeft21.Text);
            rcep.BreakLeft2.Y               = double.Parse(txtbreakLeft22.Text);
            rcep.BreakLeftY                 = double.Parse(txtbreakLeftY.Text);
            rcep.BreakRight1.X              = double.Parse(txtbreakRight11.Text);
            rcep.BreakRight1.Y              = double.Parse(txtbreakRight12.Text);
            rcep.BreakRight2.X              = double.Parse(txtbreakRight21.Text);
            rcep.BreakRight2.Y              = double.Parse(txtbreakRight22.Text);
            rcep.BreakRightY                = double.Parse(txtbreakRightY.Text);
            rcep.AMatrixSize.X              = double.Parse(txtcellSize1.Text);
            rcep.AMatrixSize.Y              = double.Parse(txtcellSize2.Text);
            rcep.AlignMentLayer             = comxtalignmentLayer.Text;
            rcep.ProcessLayer               = comxtprocessLayer.Text;
            rcep.GuideLayer                 = comxtguideLayer.Text;
            rcep.ProcessParam               = comxtprocessParam.Text;
            rcep.CellType                   = comxtcellType.Text;
            rcep.TaThickness                = txttThickness.Text;

            //Cassette
            rcep.CassetteWidth              = double.Parse(txtwidth.Text);
            rcep.CassetteHeight             = double.Parse(txtheight.Text);
            rcep.CassetteMaxHeightCount     = double.Parse(txtmaxHeightCount.Text);
            rcep.CellType                   = txtcellCount.Text;                 

            //Picker

            //LDS Process
            rcep.LDSProcess01.X             = double.Parse(txtldsProcess11.Text); 
            rcep.LDSProcess01.Y             = double.Parse(txtldsProcess12.Text); 
            rcep.LDSProcess02.X             = double.Parse(txtldsProcess21.Text); 
            rcep.LDSProcess02.Y             = double.Parse(txtldsProcess22.Text); 
            rcep.LDSProcess03.X             = double.Parse(txtldsProcess31.Text); 
            rcep.LDSProcess03.Y             = double.Parse(txtldsProcess32.Text); 
            rcep.LDSProcess04.X             = double.Parse(txtldsProcess41.Text);
            rcep.LDSProcess04.Y             = double.Parse(txtldsProcess42.Text);                  

            //LDS Break
            rcep.LDSBreak01.X               = double.Parse(txtldsBreak11.Text);
            rcep.LDSBreak01.Y               = double.Parse(txtldsBreak12.Text);
            rcep.LDSBreak02.X               = double.Parse(txtldsBreak21.Text);
            rcep.LDSBreak02.Y               = double.Parse(txtldsBreak22.Text);
            rcep.LDSBreak03.X               = double.Parse(txtldsBreak31.Text);
            rcep.LDSBreak03.Y               = double.Parse(txtldsBreak32.Text);
            rcep.LDSBreak04.X               = double.Parse(txtldsBreak41.Text);
            rcep.LDSBreak04.Y               = double.Parse(txtldsBreak42.Text);

            //Y-Offset
            rcep.OffsetPre                  = double.Parse(txtyOffsetPre.Text);
            rcep.OffsetBreak                = double.Parse(txtyOffsetBreak.Text);
            #endregion
        }

        private void btnRecipeLoad_Click(object sender, EventArgs e)
        {

        }
    }                                                
}
