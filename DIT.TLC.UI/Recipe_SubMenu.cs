﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DIT.TLC.UI
{
    public partial class Recipe_SubMenu : UserControl
    {
        Recipe_Recipe recipe_reform = new Recipe_Recipe();                  // 레시피-레시피 UI
        Recipe_ProCondition recipe_proconform = new Recipe_ProCondition();  // 레시피-가공조건 UI

        public Recipe_SubMenu()
        {
            InitializeComponent();

            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(recipe_reform);
            recipe_reform.Location = new Point(0, 0);
        }

        // 서브메뉴 버튼 클릭시 색 변경 함수
        Button SelectedButton = null;
        private void SubMenuButtonColorChange(object sender, EventArgs e)
        {
            if (SelectedButton != null)
                SelectedButton.BackColor = Color.DarkSlateGray;

            SelectedButton = sender as Button;
            SelectedButton.BackColor = Color.Blue;
        }

        // 레시피 버튼 클릭
        private void btn_sub_recipe_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(recipe_reform);
            recipe_reform.Location = new Point(0, 0);

            SubMenuButtonColorChange(sender, e);
        }

        // 가공조건 버튼 클릭
        private void btn_sub_procondition_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(recipe_proconform);
            recipe_proconform.Location = new Point(0, 0);

            SubMenuButtonColorChange(sender, e);
        }
    }
}
