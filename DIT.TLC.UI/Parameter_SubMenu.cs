﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DIT.TLC.UI
{
    public partial class Parameter_SubMenu : UserControl
    {
        Parameter_location parameter_location = new Parameter_location();                       // 위치 파라미터 UI
        Parameter_SequenceOffset Parameter_sequenceoffset = new Parameter_SequenceOffset();     // 시퀀스 offset UI
        ParameterAxis parameter_axis = new ParameterAxis();                                     // 축 파라미터 UI
        Parameter_System parameter_system = new Parameter_System();                             // 시스템 파라미터 UI
        Parameter_UserManager parameter_usermanager = new Parameter_UserManager();              // 사용자 매니저 UI
      

        public Parameter_SubMenu()
        {
            InitializeComponent();

            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(parameter_location);
            parameter_location.Location = new Point(0, 0);
        }

        // 서브메뉴 버튼 클릭시 색 변경 함수
        Button SelectedButton = null;
        private void SubMenuButtonColorChange(object sender, EventArgs e)
        {
            if (SelectedButton != null)
                SelectedButton.BackColor = Color.DarkSlateGray;

            SelectedButton = sender as Button;
            SelectedButton.BackColor = Color.Blue;
        }

        /// <summary>
        /// 위치 파라미터 버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Location_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(parameter_location);
            parameter_location.Location = new Point(0, 0);

            SubMenuButtonColorChange(sender, e);
        }

        /// <summary>
        /// 시퀀스 offset 버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_SequenceOffset_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(Parameter_sequenceoffset);
            Parameter_sequenceoffset.Location = new Point(0, 0);

            SubMenuButtonColorChange(sender, e);
        }

        /// <summary>
        /// 축 파라미터 버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Axis_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(parameter_axis);
            parameter_axis.Location = new Point(0, 0);

            SubMenuButtonColorChange(sender, e);
        }

        /// <summary>
        /// 시스템 파라미터 버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_System_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(parameter_system);
            parameter_system.Location = new Point(0, 0);

            SubMenuButtonColorChange(sender, e);
        }

        /// <summary>
        /// 사용자 매니저 버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_UserManager_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(parameter_usermanager);
            parameter_usermanager.Location = new Point(0, 0);

            SubMenuButtonColorChange(sender, e);
        }
    }
}
