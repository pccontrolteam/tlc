﻿namespace DIT.TLC.UI
{
    partial class MainWindow
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        } 

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.bt_start = new System.Windows.Forms.Button();
            this.btn_pause = new System.Windows.Forms.Button();
            this.btn_stop = new System.Windows.Forms.Button();
            this.btnKeyswitch = new System.Windows.Forms.Button();
            this.btnPm = new System.Windows.Forms.Button();
            this.btnDooropen = new System.Windows.Forms.Button();
            this.btnLight = new System.Windows.Forms.Button();
            this.lb_process_connect_info = new System.Windows.Forms.Label();
            this.panelCimConnectInfo = new System.Windows.Forms.Panel();
            this.panelAjinConnectInfo = new System.Windows.Forms.Panel();
            this.lb_cim_connect_info = new System.Windows.Forms.Label();
            this.lb_ajin_connect_info = new System.Windows.Forms.Label();
            this.panelUmacConnectInfo = new System.Windows.Forms.Panel();
            this.lb_umac_connect_info = new System.Windows.Forms.Label();
            this.panelLaserConnectInfo = new System.Windows.Forms.Panel();
            this.panelSequenceConnectInfo = new System.Windows.Forms.Panel();
            this.lb_laser_connect_info = new System.Windows.Forms.Label();
            this.panelInsp1ConnectInfo = new System.Windows.Forms.Panel();
            this.lb_sequence_connect_info = new System.Windows.Forms.Label();
            this.lb_insp1_connect_info = new System.Windows.Forms.Label();
            this.panelSerialConnectInfo = new System.Windows.Forms.Panel();
            this.panelInsp2ConnectInfo = new System.Windows.Forms.Panel();
            this.lb_serial_connect_info = new System.Windows.Forms.Label();
            this.lb_insp2_connect_info = new System.Windows.Forms.Label();
            this.panel_runinfo = new System.Windows.Forms.Panel();
            this.txttactTime = new System.Windows.Forms.TextBox();
            this.txtrunCount = new System.Windows.Forms.TextBox();
            this.txtrunTime = new System.Windows.Forms.TextBox();
            this.txtstartTime = new System.Windows.Forms.TextBox();
            this.txtcellSize = new System.Windows.Forms.TextBox();
            this.txtprocess = new System.Windows.Forms.TextBox();
            this.txtrunName = new System.Windows.Forms.TextBox();
            this.txtrecipeName = new System.Windows.Forms.TextBox();
            this.lb_tact_time = new System.Windows.Forms.Label();
            this.lb_run_count = new System.Windows.Forms.Label();
            this.lb_run_time = new System.Windows.Forms.Label();
            this.lb_start_time = new System.Windows.Forms.Label();
            this.lb_cell_size = new System.Windows.Forms.Label();
            this.lb_process = new System.Windows.Forms.Label();
            this.lb_recipe_name = new System.Windows.Forms.Label();
            this.lb_run_name = new System.Windows.Forms.Label();
            this.lb_run_info = new System.Windows.Forms.Label();
            this.gxtlaser_info = new System.Windows.Forms.GroupBox();
            this.lbShutter = new System.Windows.Forms.Label();
            this.lb_shutter_info = new System.Windows.Forms.Label();
            this.lbPower = new System.Windows.Forms.Label();
            this.lb_power_info = new System.Windows.Forms.Label();
            this.lbDivider = new System.Windows.Forms.Label();
            this.lb_divider_info = new System.Windows.Forms.Label();
            this.lbPd7 = new System.Windows.Forms.Label();
            this.lb_pd7power_info = new System.Windows.Forms.Label();
            this.lbOutamplifier = new System.Windows.Forms.Label();
            this.lbAmplifier = new System.Windows.Forms.Label();
            this.lbBurst = new System.Windows.Forms.Label();
            this.lbPulseMode = new System.Windows.Forms.Label();
            this.lb_outamplifier_info = new System.Windows.Forms.Label();
            this.lb_amplifier_info = new System.Windows.Forms.Label();
            this.lb_burst_info = new System.Windows.Forms.Label();
            this.lb_pulsemode_info = new System.Windows.Forms.Label();
            this.gxtmcr_count = new System.Windows.Forms.GroupBox();
            this.panel_cstinfo = new System.Windows.Forms.Panel();
            this.lbCstTotalCount = new System.Windows.Forms.Label();
            this.lb_cst_total_count_info = new System.Windows.Forms.Label();
            this.lbCstReadCount = new System.Windows.Forms.Label();
            this.lb_cst_read_count_info = new System.Windows.Forms.Label();
            this.lb_cst = new System.Windows.Forms.Label();
            this.panel_day_info = new System.Windows.Forms.Panel();
            this.lbDayTotalCount = new System.Windows.Forms.Label();
            this.lb_day_total_count_info = new System.Windows.Forms.Label();
            this.lbDayReadCount = new System.Windows.Forms.Label();
            this.lb_day_read_count_info = new System.Windows.Forms.Label();
            this.lb_day = new System.Windows.Forms.Label();
            this.gxtuld = new System.Windows.Forms.GroupBox();
            this.btnUldOutMuting = new System.Windows.Forms.Button();
            this.btnUldInMuting = new System.Windows.Forms.Button();
            this.gxtld = new System.Windows.Forms.GroupBox();
            this.btnLdOutMuting = new System.Windows.Forms.Button();
            this.btnLdInMuting = new System.Windows.Forms.Button();
            this.btn_safety_reset = new System.Windows.Forms.Button();
            this.panel_run = new System.Windows.Forms.Panel();
            this.btnACSTSkip = new System.Windows.Forms.Button();
            this.btnBCSTSkip = new System.Windows.Forms.Button();
            this.panel_lot_b_info = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.lb_lot_b_info = new System.Windows.Forms.Label();
            this.panel_lot_a_info = new System.Windows.Forms.Panel();
            this.lb_lot_a = new System.Windows.Forms.Label();
            this.lb_lot_a_info = new System.Windows.Forms.Label();
            this.panel_ld_muting_out = new System.Windows.Forms.Panel();
            this.lbLdMutingOut = new System.Windows.Forms.Label();
            this.panelUldMutingIn = new System.Windows.Forms.Panel();
            this.lbUldMutingIn = new System.Windows.Forms.Label();
            this.panel_ld_muting_in = new System.Windows.Forms.Panel();
            this.lbLdMutingIn = new System.Windows.Forms.Label();
            this.panelUldMutingOut = new System.Windows.Forms.Panel();
            this.lbUldMuting_out = new System.Windows.Forms.Label();
            this.panelDoor2 = new System.Windows.Forms.Panel();
            this.lbDoor2 = new System.Windows.Forms.Label();
            this.panel38 = new System.Windows.Forms.Panel();
            this.label51 = new System.Windows.Forms.Label();
            this.panelDoor3 = new System.Windows.Forms.Panel();
            this.lb_door3 = new System.Windows.Forms.Label();
            this.panelDoor5 = new System.Windows.Forms.Panel();
            this.lbDoor5 = new System.Windows.Forms.Label();
            this.panelDoor4 = new System.Windows.Forms.Panel();
            this.lb_door4 = new System.Windows.Forms.Label();
            this.panelDoor6 = new System.Windows.Forms.Panel();
            this.lb_door6 = new System.Windows.Forms.Label();
            this.panelDoor8 = new System.Windows.Forms.Panel();
            this.lb_door8 = new System.Windows.Forms.Label();
            this.panelDoor7 = new System.Windows.Forms.Panel();
            this.lb_door7 = new System.Windows.Forms.Label();
            this.panelDoor9 = new System.Windows.Forms.Panel();
            this.lb_door9 = new System.Windows.Forms.Label();
            this.panelLdBox = new System.Windows.Forms.Panel();
            this.lb_ld_box = new System.Windows.Forms.Label();
            this.panelEms3 = new System.Windows.Forms.Panel();
            this.lb_ems3 = new System.Windows.Forms.Label();
            this.panelProcessBox = new System.Windows.Forms.Panel();
            this.lb_process_box = new System.Windows.Forms.Label();
            this.panelEms5 = new System.Windows.Forms.Panel();
            this.lb_ems5 = new System.Windows.Forms.Label();
            this.panelUldBox = new System.Windows.Forms.Panel();
            this.lb_uld_box = new System.Windows.Forms.Label();
            this.panel_run2 = new System.Windows.Forms.Panel();
            this.btnUldLight = new System.Windows.Forms.Button();
            this.btnProcessLight = new System.Windows.Forms.Button();
            this.btnLdLight = new System.Windows.Forms.Button();
            this.panelEms1 = new System.Windows.Forms.Panel();
            this.lb_ems1 = new System.Windows.Forms.Label();
            this.panelTargetAvgpower = new System.Windows.Forms.Panel();
            this.lbAvgpower = new System.Windows.Forms.Label();
            this.lb_avgpower_info = new System.Windows.Forms.Label();
            this.lbTartget = new System.Windows.Forms.Label();
            this.lb_target_info = new System.Windows.Forms.Label();
            this.panelZposGap = new System.Windows.Forms.Panel();
            this.lbZposGap = new System.Windows.Forms.Label();
            this.lb_zpos_gap_info = new System.Windows.Forms.Label();
            this.panelEms4 = new System.Windows.Forms.Panel();
            this.lb_ems4 = new System.Windows.Forms.Label();
            this.panelLdOutB = new System.Windows.Forms.Panel();
            this.lb_ld_out_b = new System.Windows.Forms.Label();
            this.panelLdInB = new System.Windows.Forms.Panel();
            this.lb_ld_in_b = new System.Windows.Forms.Label();
            this.panelLdOutA = new System.Windows.Forms.Panel();
            this.lb_ld_out_a = new System.Windows.Forms.Label();
            this.panelLdInA = new System.Windows.Forms.Panel();
            this.lb_ld_in_a = new System.Windows.Forms.Label();
            this.panelUldInB = new System.Windows.Forms.Panel();
            this.lb_uld_in_b = new System.Windows.Forms.Label();
            this.panelUldOutB = new System.Windows.Forms.Panel();
            this.lb_uld_out_b = new System.Windows.Forms.Label();
            this.panelUldInA = new System.Windows.Forms.Panel();
            this.lb_uld_in_a = new System.Windows.Forms.Label();
            this.panelUldOutA = new System.Windows.Forms.Panel();
            this.lb_uld_out_a = new System.Windows.Forms.Label();
            this.panelUld1Out2 = new System.Windows.Forms.Panel();
            this.panelUld2Out2 = new System.Windows.Forms.Panel();
            this.panelUld1Table = new System.Windows.Forms.Panel();
            this.panelUld2Table = new System.Windows.Forms.Panel();
            this.panelUld1Out1 = new System.Windows.Forms.Panel();
            this.panelUld2Out1 = new System.Windows.Forms.Panel();
            this.panelUld1 = new System.Windows.Forms.Panel();
            this.lb_uld1 = new System.Windows.Forms.Label();
            this.panelUld2 = new System.Windows.Forms.Panel();
            this.lb_uld2 = new System.Windows.Forms.Label();
            this.panelUldTransA1 = new System.Windows.Forms.Panel();
            this.lb_uld_trans_a1 = new System.Windows.Forms.Label();
            this.panelUldTransB1 = new System.Windows.Forms.Panel();
            this.lb_uld_trans_b1 = new System.Windows.Forms.Label();
            this.panelUldTransA2 = new System.Windows.Forms.Panel();
            this.lb_uld_trans_a2 = new System.Windows.Forms.Label();
            this.panel_uld_trans_b2 = new System.Windows.Forms.Panel();
            this.lbUldTransB2 = new System.Windows.Forms.Label();
            this.panelBreakTableA1 = new System.Windows.Forms.Panel();
            this.lb_break_table_a1 = new System.Windows.Forms.Label();
            this.panelBreakTableB1 = new System.Windows.Forms.Panel();
            this.lb_break_table_b1 = new System.Windows.Forms.Label();
            this.panelBreakTrans1 = new System.Windows.Forms.Panel();
            this.lb_break_trans1 = new System.Windows.Forms.Label();
            this.panelBreakTrans2 = new System.Windows.Forms.Panel();
            this.lb_break_trans2 = new System.Windows.Forms.Label();
            this.panelBreakTableA2 = new System.Windows.Forms.Panel();
            this.lb_break_table_a2 = new System.Windows.Forms.Label();
            this.panel_break_table_b2 = new System.Windows.Forms.Panel();
            this.lbBreakTableB2 = new System.Windows.Forms.Label();
            this.panelProcessTableA1 = new System.Windows.Forms.Panel();
            this.lb_process_table_a1 = new System.Windows.Forms.Label();
            this.panelProcessTableB1 = new System.Windows.Forms.Panel();
            this.lb_process_table_b1 = new System.Windows.Forms.Label();
            this.panelProcessTableA2 = new System.Windows.Forms.Panel();
            this.lb_process_table_a2 = new System.Windows.Forms.Label();
            this.panelLd_Trans1 = new System.Windows.Forms.Panel();
            this.lb_ld_trans1 = new System.Windows.Forms.Label();
            this.panelProcessTableB2 = new System.Windows.Forms.Panel();
            this.lb_process_table_b2 = new System.Windows.Forms.Label();
            this.panelLd1 = new System.Windows.Forms.Panel();
            this.lb_ld1 = new System.Windows.Forms.Label();
            this.panelLdTrans2 = new System.Windows.Forms.Panel();
            this.lb_ld_trans2 = new System.Windows.Forms.Label();
            this.panelLd1Table = new System.Windows.Forms.Panel();
            this.panelLd2 = new System.Windows.Forms.Panel();
            this.lbLD2 = new System.Windows.Forms.Label();
            this.panelL1In2 = new System.Windows.Forms.Panel();
            this.panelLd2Table = new System.Windows.Forms.Panel();
            this.panelLd1In1 = new System.Windows.Forms.Panel();
            this.panelLd2In2 = new System.Windows.Forms.Panel();
            this.panelLd2In1 = new System.Windows.Forms.Panel();
            this.lbLd1OTime = new System.Windows.Forms.Label();
            this.lb_ld1_o = new System.Windows.Forms.Label();
            this.lbLd1ITime = new System.Windows.Forms.Label();
            this.lb_ld1_i = new System.Windows.Forms.Label();
            this.lbLd2OTime = new System.Windows.Forms.Label();
            this.lb_ld2_o = new System.Windows.Forms.Label();
            this.lbLd2ITime = new System.Windows.Forms.Label();
            this.lb_ld2_i = new System.Windows.Forms.Label();
            this.lbUld1ITime = new System.Windows.Forms.Label();
            this.lb_uld1_i = new System.Windows.Forms.Label();
            this.lbUld1OTime = new System.Windows.Forms.Label();
            this.lb_uld1_o = new System.Windows.Forms.Label();
            this.lbUld2ITime = new System.Windows.Forms.Label();
            this.lb_uld2_i = new System.Windows.Forms.Label();
            this.lbUld2OTime = new System.Windows.Forms.Label();
            this.lb_uld2_o = new System.Windows.Forms.Label();
            this.panelShutterOpen = new System.Windows.Forms.Panel();
            this.lb_shutter_open = new System.Windows.Forms.Label();
            this.panelLock = new System.Windows.Forms.Panel();
            this.lb_lock = new System.Windows.Forms.Label();
            this.panelLaserCover = new System.Windows.Forms.Panel();
            this.lb_laser_cover = new System.Windows.Forms.Label();
            this.panelEms2 = new System.Windows.Forms.Panel();
            this.lb_ems2 = new System.Windows.Forms.Label();
            this.panelEms6 = new System.Windows.Forms.Panel();
            this.lb_ems6 = new System.Windows.Forms.Label();
            this.panelGrabEms1 = new System.Windows.Forms.Panel();
            this.lb_grab_ems1 = new System.Windows.Forms.Label();
            this.panelGrabEms2 = new System.Windows.Forms.Panel();
            this.lb_grab_ems2 = new System.Windows.Forms.Label();
            this.panelGrabSwitch1 = new System.Windows.Forms.Panel();
            this.lb_grab_switch1 = new System.Windows.Forms.Label();
            this.panelGrabSwitch2 = new System.Windows.Forms.Panel();
            this.lb_grab_switch2 = new System.Windows.Forms.Label();
            this.panelGrabEms3 = new System.Windows.Forms.Panel();
            this.lb_grab_ems3 = new System.Windows.Forms.Label();
            this.panelGrabSwitch3 = new System.Windows.Forms.Panel();
            this.lb_grab_switch3 = new System.Windows.Forms.Label();
            this.panel_door10 = new System.Windows.Forms.Panel();
            this.lbDoor10 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label30 = new System.Windows.Forms.Label();
            this.panel_camera_l_info = new System.Windows.Forms.Panel();
            this.lb_camera_l = new System.Windows.Forms.Label();
            this.panel_camera_l = new System.Windows.Forms.Panel();
            this.panel_camera_r_info = new System.Windows.Forms.Panel();
            this.lb_camera_r = new System.Windows.Forms.Label();
            this.panel_camera_r = new System.Windows.Forms.Panel();
            this.panel_align = new System.Windows.Forms.Panel();
            this.txtaligBbreak4OffsetA = new System.Windows.Forms.TextBox();
            this.txtalignBreak3OffsetA = new System.Windows.Forms.TextBox();
            this.txtalignPre2OffsetA = new System.Windows.Forms.TextBox();
            this.lb_align_offset_a2 = new System.Windows.Forms.Label();
            this.txtalignBreak4OffsetY = new System.Windows.Forms.TextBox();
            this.txtalignBreak3OffsetY = new System.Windows.Forms.TextBox();
            this.txtalignPre2OffsetY = new System.Windows.Forms.TextBox();
            this.lb_align_offset_y2 = new System.Windows.Forms.Label();
            this.txtalignBreak4OffsetX = new System.Windows.Forms.TextBox();
            this.txtalignBreak3OffsetX = new System.Windows.Forms.TextBox();
            this.txtalignPre2OffsetX = new System.Windows.Forms.TextBox();
            this.lb_align_offset_x2 = new System.Windows.Forms.Label();
            this.txtalignBreak4Result = new System.Windows.Forms.TextBox();
            this.txtalignBreak3Result = new System.Windows.Forms.TextBox();
            this.txtalignPre2Result = new System.Windows.Forms.TextBox();
            this.lb_align_result2 = new System.Windows.Forms.Label();
            this.lb_align_break4 = new System.Windows.Forms.Label();
            this.lb_align_break3 = new System.Windows.Forms.Label();
            this.lb_align_pre2 = new System.Windows.Forms.Label();
            this.lb_align_name2 = new System.Windows.Forms.Label();
            this.txtalignBreak2OffsetA = new System.Windows.Forms.TextBox();
            this.txtalignBreak1Offset_A = new System.Windows.Forms.TextBox();
            this.txtalignPre1OffsetA = new System.Windows.Forms.TextBox();
            this.lb_align_offset_a1 = new System.Windows.Forms.Label();
            this.txtalignBreak2OffsetY = new System.Windows.Forms.TextBox();
            this.txtalignBreak1OffsetY = new System.Windows.Forms.TextBox();
            this.txtalignPre1OffsetY = new System.Windows.Forms.TextBox();
            this.lb_align_offset_y1 = new System.Windows.Forms.Label();
            this.txtalignBreak2OffsetX = new System.Windows.Forms.TextBox();
            this.txtalignBreak1OffsetX = new System.Windows.Forms.TextBox();
            this.txtalignPre1OffsetX = new System.Windows.Forms.TextBox();
            this.lb_align_offset_x1 = new System.Windows.Forms.Label();
            this.txtalignBreak2Result = new System.Windows.Forms.TextBox();
            this.txtalignBreak1Result = new System.Windows.Forms.TextBox();
            this.txtalignPre1Result = new System.Windows.Forms.TextBox();
            this.lb_align_result1 = new System.Windows.Forms.Label();
            this.lb_align_break2 = new System.Windows.Forms.Label();
            this.lb_align_break1 = new System.Windows.Forms.Label();
            this.lb_align_pre1 = new System.Windows.Forms.Label();
            this.lb_align_name1 = new System.Windows.Forms.Label();
            this.gxtbreaking_count = new System.Windows.Forms.GroupBox();
            this.btnBreakingCountClear = new System.Windows.Forms.Button();
            this.lb_breaking_count = new System.Windows.Forms.Label();
            this.lb_breaking_count_info = new System.Windows.Forms.Label();
            this.gxtdummy_tank = new System.Windows.Forms.GroupBox();
            this.btnDummyTankSet = new System.Windows.Forms.Button();
            this.btnDummyTankGet = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.detailTactView21 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView17 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView16 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView15 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView10 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView9 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView8 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView7 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView6 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView23 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView22 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView20 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView19 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView18 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView14 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView13 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView12 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView5 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView4 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView3 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView11 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView2 = new DIT.TLC.UI.Main.DetailTactView();
            this.detailTactView1 = new DIT.TLC.UI.Main.DetailTactView();
            this.cstCellCount3 = new DIT.TLC.UI.Main.CstCellCount();
            this.cstCellCount4 = new DIT.TLC.UI.Main.CstCellCount();
            this.cstCellCount2 = new DIT.TLC.UI.Main.CstCellCount();
            this.cstCellCount1 = new DIT.TLC.UI.Main.CstCellCount();
            this.panel_runinfo.SuspendLayout();
            this.gxtlaser_info.SuspendLayout();
            this.gxtmcr_count.SuspendLayout();
            this.panel_cstinfo.SuspendLayout();
            this.panel_day_info.SuspendLayout();
            this.gxtuld.SuspendLayout();
            this.gxtld.SuspendLayout();
            this.panel_run.SuspendLayout();
            this.panel_lot_b_info.SuspendLayout();
            this.panel_lot_a_info.SuspendLayout();
            this.panel_ld_muting_out.SuspendLayout();
            this.panelUldMutingIn.SuspendLayout();
            this.panel_ld_muting_in.SuspendLayout();
            this.panelUldMutingOut.SuspendLayout();
            this.panelDoor2.SuspendLayout();
            this.panel38.SuspendLayout();
            this.panelDoor3.SuspendLayout();
            this.panelDoor5.SuspendLayout();
            this.panelDoor4.SuspendLayout();
            this.panelDoor6.SuspendLayout();
            this.panelDoor8.SuspendLayout();
            this.panelDoor7.SuspendLayout();
            this.panelDoor9.SuspendLayout();
            this.panelLdBox.SuspendLayout();
            this.panelEms3.SuspendLayout();
            this.panelProcessBox.SuspendLayout();
            this.panelEms5.SuspendLayout();
            this.panelUldBox.SuspendLayout();
            this.panel_run2.SuspendLayout();
            this.panelEms1.SuspendLayout();
            this.panelTargetAvgpower.SuspendLayout();
            this.panelZposGap.SuspendLayout();
            this.panelEms4.SuspendLayout();
            this.panelLdOutB.SuspendLayout();
            this.panelLdInB.SuspendLayout();
            this.panelLdOutA.SuspendLayout();
            this.panelLdInA.SuspendLayout();
            this.panelUldInB.SuspendLayout();
            this.panelUldOutB.SuspendLayout();
            this.panelUldInA.SuspendLayout();
            this.panelUldOutA.SuspendLayout();
            this.panelUld1.SuspendLayout();
            this.panelUld2.SuspendLayout();
            this.panelUldTransA1.SuspendLayout();
            this.panelUldTransB1.SuspendLayout();
            this.panelUldTransA2.SuspendLayout();
            this.panel_uld_trans_b2.SuspendLayout();
            this.panelBreakTableA1.SuspendLayout();
            this.panelBreakTableB1.SuspendLayout();
            this.panelBreakTrans1.SuspendLayout();
            this.panelBreakTrans2.SuspendLayout();
            this.panelBreakTableA2.SuspendLayout();
            this.panel_break_table_b2.SuspendLayout();
            this.panelProcessTableA1.SuspendLayout();
            this.panelProcessTableB1.SuspendLayout();
            this.panelProcessTableA2.SuspendLayout();
            this.panelLd_Trans1.SuspendLayout();
            this.panelProcessTableB2.SuspendLayout();
            this.panelLd1.SuspendLayout();
            this.panelLdTrans2.SuspendLayout();
            this.panelLd2.SuspendLayout();
            this.panelShutterOpen.SuspendLayout();
            this.panelLock.SuspendLayout();
            this.panelLaserCover.SuspendLayout();
            this.panelEms2.SuspendLayout();
            this.panelEms6.SuspendLayout();
            this.panelGrabEms1.SuspendLayout();
            this.panelGrabEms2.SuspendLayout();
            this.panelGrabSwitch1.SuspendLayout();
            this.panelGrabSwitch2.SuspendLayout();
            this.panelGrabEms3.SuspendLayout();
            this.panelGrabSwitch3.SuspendLayout();
            this.panel_door10.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel_camera_l_info.SuspendLayout();
            this.panel_camera_r_info.SuspendLayout();
            this.panel_align.SuspendLayout();
            this.gxtbreaking_count.SuspendLayout();
            this.gxtdummy_tank.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bt_start
            // 
            this.bt_start.BackColor = System.Drawing.Color.DarkSlateGray;
            this.bt_start.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.bt_start.ForeColor = System.Drawing.Color.White;
            this.bt_start.Image = ((System.Drawing.Image)(resources.GetObject("bt_start.Image")));
            this.bt_start.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_start.Location = new System.Drawing.Point(3, 23);
            this.bt_start.Name = "bt_start";
            this.bt_start.Size = new System.Drawing.Size(132, 50);
            this.bt_start.TabIndex = 32;
            this.bt_start.Text = "시작";
            this.bt_start.UseVisualStyleBackColor = false;
            this.bt_start.Click += new System.EventHandler(this.btn_start_Click);
            // 
            // btn_pause
            // 
            this.btn_pause.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_pause.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_pause.ForeColor = System.Drawing.Color.White;
            this.btn_pause.Image = ((System.Drawing.Image)(resources.GetObject("btn_pause.Image")));
            this.btn_pause.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_pause.Location = new System.Drawing.Point(3, 79);
            this.btn_pause.Name = "btn_pause";
            this.btn_pause.Size = new System.Drawing.Size(132, 50);
            this.btn_pause.TabIndex = 34;
            this.btn_pause.Text = "일시정지";
            this.btn_pause.UseVisualStyleBackColor = false;
            this.btn_pause.Click += new System.EventHandler(this.btn_pause_Click);
            // 
            // btn_stop
            // 
            this.btn_stop.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_stop.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_stop.ForeColor = System.Drawing.Color.White;
            this.btn_stop.Image = ((System.Drawing.Image)(resources.GetObject("btn_stop.Image")));
            this.btn_stop.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_stop.Location = new System.Drawing.Point(3, 135);
            this.btn_stop.Name = "btn_stop";
            this.btn_stop.Size = new System.Drawing.Size(132, 50);
            this.btn_stop.TabIndex = 35;
            this.btn_stop.Text = "작업종료";
            this.btn_stop.UseVisualStyleBackColor = false;
            this.btn_stop.Click += new System.EventHandler(this.btn_stop_Click);
            // 
            // btnKeyswitch
            // 
            this.btnKeyswitch.BackColor = System.Drawing.Color.DimGray;
            this.btnKeyswitch.Font = new System.Drawing.Font("Malgun Gothic", 15.75F, System.Drawing.FontStyle.Bold);
            this.btnKeyswitch.ForeColor = System.Drawing.Color.White;
            this.btnKeyswitch.Location = new System.Drawing.Point(3, 230);
            this.btnKeyswitch.Name = "btnKeyswitch";
            this.btnKeyswitch.Size = new System.Drawing.Size(132, 50);
            this.btnKeyswitch.TabIndex = 36;
            this.btnKeyswitch.Text = "키 스위치";
            this.btnKeyswitch.UseVisualStyleBackColor = false;
            // 
            // btnPm
            // 
            this.btnPm.BackColor = System.Drawing.Color.DimGray;
            this.btnPm.Font = new System.Drawing.Font("Malgun Gothic", 15.75F, System.Drawing.FontStyle.Bold);
            this.btnPm.ForeColor = System.Drawing.Color.White;
            this.btnPm.Location = new System.Drawing.Point(2, 286);
            this.btnPm.Name = "btnPm";
            this.btnPm.Size = new System.Drawing.Size(132, 50);
            this.btnPm.TabIndex = 37;
            this.btnPm.Text = "PM";
            this.btnPm.UseVisualStyleBackColor = false;
            // 
            // btnDooropen
            // 
            this.btnDooropen.BackColor = System.Drawing.Color.DimGray;
            this.btnDooropen.Font = new System.Drawing.Font("Malgun Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.btnDooropen.ForeColor = System.Drawing.Color.White;
            this.btnDooropen.Location = new System.Drawing.Point(3, 342);
            this.btnDooropen.Name = "btnDooropen";
            this.btnDooropen.Size = new System.Drawing.Size(132, 50);
            this.btnDooropen.TabIndex = 38;
            this.btnDooropen.Text = "Door-Open";
            this.btnDooropen.UseVisualStyleBackColor = false;
            // 
            // btnLight
            // 
            this.btnLight.BackColor = System.Drawing.Color.DimGray;
            this.btnLight.Font = new System.Drawing.Font("Malgun Gothic", 15.75F, System.Drawing.FontStyle.Bold);
            this.btnLight.ForeColor = System.Drawing.Color.White;
            this.btnLight.Location = new System.Drawing.Point(3, 398);
            this.btnLight.Name = "btnLight";
            this.btnLight.Size = new System.Drawing.Size(132, 50);
            this.btnLight.TabIndex = 39;
            this.btnLight.Text = "설비조명";
            this.btnLight.UseVisualStyleBackColor = false;
            // 
            // lb_process_connect_info
            // 
            this.lb_process_connect_info.AutoSize = true;
            this.lb_process_connect_info.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.lb_process_connect_info.ForeColor = System.Drawing.Color.White;
            this.lb_process_connect_info.Location = new System.Drawing.Point(8, 560);
            this.lb_process_connect_info.Name = "lb_process_connect_info";
            this.lb_process_connect_info.Size = new System.Drawing.Size(126, 19);
            this.lb_process_connect_info.TabIndex = 29;
            this.lb_process_connect_info.Text = "프로세스 연결상태";
            // 
            // panelCimConnectInfo
            // 
            this.panelCimConnectInfo.BackColor = System.Drawing.Color.Red;
            this.panelCimConnectInfo.ForeColor = System.Drawing.Color.White;
            this.panelCimConnectInfo.Location = new System.Drawing.Point(11, 590);
            this.panelCimConnectInfo.Name = "panelCimConnectInfo";
            this.panelCimConnectInfo.Size = new System.Drawing.Size(25, 25);
            this.panelCimConnectInfo.TabIndex = 30;
            // 
            // panelAjinConnectInfo
            // 
            this.panelAjinConnectInfo.BackColor = System.Drawing.Color.Red;
            this.panelAjinConnectInfo.ForeColor = System.Drawing.Color.White;
            this.panelAjinConnectInfo.Location = new System.Drawing.Point(11, 623);
            this.panelAjinConnectInfo.Name = "panelAjinConnectInfo";
            this.panelAjinConnectInfo.Size = new System.Drawing.Size(25, 25);
            this.panelAjinConnectInfo.TabIndex = 31;
            // 
            // lb_cim_connect_info
            // 
            this.lb_cim_connect_info.AutoSize = true;
            this.lb_cim_connect_info.Font = new System.Drawing.Font("Malgun Gothic", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_cim_connect_info.ForeColor = System.Drawing.Color.White;
            this.lb_cim_connect_info.Location = new System.Drawing.Point(42, 594);
            this.lb_cim_connect_info.Name = "lb_cim_connect_info";
            this.lb_cim_connect_info.Size = new System.Drawing.Size(54, 17);
            this.lb_cim_connect_info.TabIndex = 33;
            this.lb_cim_connect_info.Text = "CIM-PC";
            // 
            // lb_ajin_connect_info
            // 
            this.lb_ajin_connect_info.AutoSize = true;
            this.lb_ajin_connect_info.Font = new System.Drawing.Font("Malgun Gothic", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_ajin_connect_info.ForeColor = System.Drawing.Color.White;
            this.lb_ajin_connect_info.Location = new System.Drawing.Point(42, 627);
            this.lb_ajin_connect_info.Name = "lb_ajin_connect_info";
            this.lb_ajin_connect_info.Size = new System.Drawing.Size(65, 17);
            this.lb_ajin_connect_info.TabIndex = 40;
            this.lb_ajin_connect_info.Text = "AJIN-PLC";
            // 
            // panelUmacConnectInfo
            // 
            this.panelUmacConnectInfo.BackColor = System.Drawing.Color.Red;
            this.panelUmacConnectInfo.ForeColor = System.Drawing.Color.White;
            this.panelUmacConnectInfo.Location = new System.Drawing.Point(11, 654);
            this.panelUmacConnectInfo.Name = "panelUmacConnectInfo";
            this.panelUmacConnectInfo.Size = new System.Drawing.Size(25, 25);
            this.panelUmacConnectInfo.TabIndex = 41;
            // 
            // lb_umac_connect_info
            // 
            this.lb_umac_connect_info.AutoSize = true;
            this.lb_umac_connect_info.Font = new System.Drawing.Font("Malgun Gothic", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_umac_connect_info.ForeColor = System.Drawing.Color.White;
            this.lb_umac_connect_info.Location = new System.Drawing.Point(42, 658);
            this.lb_umac_connect_info.Name = "lb_umac_connect_info";
            this.lb_umac_connect_info.Size = new System.Drawing.Size(76, 17);
            this.lb_umac_connect_info.TabIndex = 42;
            this.lb_umac_connect_info.Text = "UMAC-PLC";
            // 
            // panelLaserConnectInfo
            // 
            this.panelLaserConnectInfo.BackColor = System.Drawing.Color.Red;
            this.panelLaserConnectInfo.ForeColor = System.Drawing.Color.White;
            this.panelLaserConnectInfo.Location = new System.Drawing.Point(11, 686);
            this.panelLaserConnectInfo.Name = "panelLaserConnectInfo";
            this.panelLaserConnectInfo.Size = new System.Drawing.Size(25, 25);
            this.panelLaserConnectInfo.TabIndex = 43;
            // 
            // panelSequenceConnectInfo
            // 
            this.panelSequenceConnectInfo.BackColor = System.Drawing.Color.Red;
            this.panelSequenceConnectInfo.ForeColor = System.Drawing.Color.White;
            this.panelSequenceConnectInfo.Location = new System.Drawing.Point(11, 719);
            this.panelSequenceConnectInfo.Name = "panelSequenceConnectInfo";
            this.panelSequenceConnectInfo.Size = new System.Drawing.Size(25, 25);
            this.panelSequenceConnectInfo.TabIndex = 44;
            // 
            // lb_laser_connect_info
            // 
            this.lb_laser_connect_info.AutoSize = true;
            this.lb_laser_connect_info.Font = new System.Drawing.Font("Malgun Gothic", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_laser_connect_info.ForeColor = System.Drawing.Color.White;
            this.lb_laser_connect_info.Location = new System.Drawing.Point(42, 690);
            this.lb_laser_connect_info.Name = "lb_laser_connect_info";
            this.lb_laser_connect_info.Size = new System.Drawing.Size(47, 17);
            this.lb_laser_connect_info.TabIndex = 45;
            this.lb_laser_connect_info.Text = "레이저";
            // 
            // panelInsp1ConnectInfo
            // 
            this.panelInsp1ConnectInfo.BackColor = System.Drawing.Color.Red;
            this.panelInsp1ConnectInfo.ForeColor = System.Drawing.Color.White;
            this.panelInsp1ConnectInfo.Location = new System.Drawing.Point(11, 781);
            this.panelInsp1ConnectInfo.Name = "panelInsp1ConnectInfo";
            this.panelInsp1ConnectInfo.Size = new System.Drawing.Size(25, 25);
            this.panelInsp1ConnectInfo.TabIndex = 49;
            // 
            // lb_sequence_connect_info
            // 
            this.lb_sequence_connect_info.AutoSize = true;
            this.lb_sequence_connect_info.Font = new System.Drawing.Font("Malgun Gothic", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_sequence_connect_info.ForeColor = System.Drawing.Color.White;
            this.lb_sequence_connect_info.Location = new System.Drawing.Point(42, 723);
            this.lb_sequence_connect_info.Name = "lb_sequence_connect_info";
            this.lb_sequence_connect_info.Size = new System.Drawing.Size(47, 17);
            this.lb_sequence_connect_info.TabIndex = 46;
            this.lb_sequence_connect_info.Text = "시퀀스";
            // 
            // lb_insp1_connect_info
            // 
            this.lb_insp1_connect_info.AutoSize = true;
            this.lb_insp1_connect_info.Font = new System.Drawing.Font("Malgun Gothic", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_insp1_connect_info.ForeColor = System.Drawing.Color.White;
            this.lb_insp1_connect_info.Location = new System.Drawing.Point(42, 785);
            this.lb_insp1_connect_info.Name = "lb_insp1_connect_info";
            this.lb_insp1_connect_info.Size = new System.Drawing.Size(60, 17);
            this.lb_insp1_connect_info.TabIndex = 50;
            this.lb_insp1_connect_info.Text = "검사기 1";
            // 
            // panelSerialConnectInfo
            // 
            this.panelSerialConnectInfo.BackColor = System.Drawing.Color.Red;
            this.panelSerialConnectInfo.ForeColor = System.Drawing.Color.White;
            this.panelSerialConnectInfo.Location = new System.Drawing.Point(11, 750);
            this.panelSerialConnectInfo.Name = "panelSerialConnectInfo";
            this.panelSerialConnectInfo.Size = new System.Drawing.Size(25, 25);
            this.panelSerialConnectInfo.TabIndex = 47;
            // 
            // panelInsp2ConnectInfo
            // 
            this.panelInsp2ConnectInfo.BackColor = System.Drawing.Color.Red;
            this.panelInsp2ConnectInfo.ForeColor = System.Drawing.Color.White;
            this.panelInsp2ConnectInfo.Location = new System.Drawing.Point(11, 812);
            this.panelInsp2ConnectInfo.Name = "panelInsp2ConnectInfo";
            this.panelInsp2ConnectInfo.Size = new System.Drawing.Size(25, 25);
            this.panelInsp2ConnectInfo.TabIndex = 51;
            // 
            // lb_serial_connect_info
            // 
            this.lb_serial_connect_info.AutoSize = true;
            this.lb_serial_connect_info.Font = new System.Drawing.Font("Malgun Gothic", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_serial_connect_info.ForeColor = System.Drawing.Color.White;
            this.lb_serial_connect_info.Location = new System.Drawing.Point(42, 754);
            this.lb_serial_connect_info.Name = "lb_serial_connect_info";
            this.lb_serial_connect_info.Size = new System.Drawing.Size(73, 17);
            this.lb_serial_connect_info.TabIndex = 48;
            this.lb_serial_connect_info.Text = "Serial 통신";
            // 
            // lb_insp2_connect_info
            // 
            this.lb_insp2_connect_info.AutoSize = true;
            this.lb_insp2_connect_info.Font = new System.Drawing.Font("Malgun Gothic", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_insp2_connect_info.ForeColor = System.Drawing.Color.White;
            this.lb_insp2_connect_info.Location = new System.Drawing.Point(42, 816);
            this.lb_insp2_connect_info.Name = "lb_insp2_connect_info";
            this.lb_insp2_connect_info.Size = new System.Drawing.Size(60, 17);
            this.lb_insp2_connect_info.TabIndex = 52;
            this.lb_insp2_connect_info.Text = "검사기 2";
            // 
            // panel_runinfo
            // 
            this.panel_runinfo.BackColor = System.Drawing.Color.DimGray;
            this.panel_runinfo.Controls.Add(this.txttactTime);
            this.panel_runinfo.Controls.Add(this.txtrunCount);
            this.panel_runinfo.Controls.Add(this.txtrunTime);
            this.panel_runinfo.Controls.Add(this.txtstartTime);
            this.panel_runinfo.Controls.Add(this.txtcellSize);
            this.panel_runinfo.Controls.Add(this.txtprocess);
            this.panel_runinfo.Controls.Add(this.txtrunName);
            this.panel_runinfo.Controls.Add(this.txtrecipeName);
            this.panel_runinfo.Controls.Add(this.lb_tact_time);
            this.panel_runinfo.Controls.Add(this.lb_run_count);
            this.panel_runinfo.Controls.Add(this.lb_run_time);
            this.panel_runinfo.Controls.Add(this.lb_start_time);
            this.panel_runinfo.Controls.Add(this.lb_cell_size);
            this.panel_runinfo.Controls.Add(this.lb_process);
            this.panel_runinfo.Controls.Add(this.lb_recipe_name);
            this.panel_runinfo.Controls.Add(this.lb_run_name);
            this.panel_runinfo.Controls.Add(this.lb_run_info);
            this.panel_runinfo.Location = new System.Drawing.Point(4, 3);
            this.panel_runinfo.Name = "panel_runinfo";
            this.panel_runinfo.Size = new System.Drawing.Size(253, 246);
            this.panel_runinfo.TabIndex = 26;
            // 
            // txttactTime
            // 
            this.txttactTime.BackColor = System.Drawing.Color.DarkSlateGray;
            this.txttactTime.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txttactTime.Location = new System.Drawing.Point(83, 219);
            this.txttactTime.Name = "txttactTime";
            this.txttactTime.ReadOnly = true;
            this.txttactTime.Size = new System.Drawing.Size(164, 21);
            this.txttactTime.TabIndex = 19;
            // 
            // txtrunCount
            // 
            this.txtrunCount.BackColor = System.Drawing.Color.DarkSlateGray;
            this.txtrunCount.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtrunCount.Location = new System.Drawing.Point(83, 192);
            this.txtrunCount.Name = "txtrunCount";
            this.txtrunCount.ReadOnly = true;
            this.txtrunCount.Size = new System.Drawing.Size(164, 21);
            this.txtrunCount.TabIndex = 18;
            // 
            // txtrunTime
            // 
            this.txtrunTime.BackColor = System.Drawing.Color.DarkSlateGray;
            this.txtrunTime.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtrunTime.Location = new System.Drawing.Point(83, 165);
            this.txtrunTime.Name = "txtrunTime";
            this.txtrunTime.ReadOnly = true;
            this.txtrunTime.Size = new System.Drawing.Size(164, 21);
            this.txtrunTime.TabIndex = 17;
            // 
            // txtstartTime
            // 
            this.txtstartTime.BackColor = System.Drawing.Color.DarkSlateGray;
            this.txtstartTime.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtstartTime.Location = new System.Drawing.Point(83, 138);
            this.txtstartTime.Name = "txtstartTime";
            this.txtstartTime.ReadOnly = true;
            this.txtstartTime.Size = new System.Drawing.Size(164, 21);
            this.txtstartTime.TabIndex = 16;
            // 
            // txtcellSize
            // 
            this.txtcellSize.BackColor = System.Drawing.Color.DarkSlateGray;
            this.txtcellSize.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtcellSize.Location = new System.Drawing.Point(83, 111);
            this.txtcellSize.Name = "txtcellSize";
            this.txtcellSize.ReadOnly = true;
            this.txtcellSize.Size = new System.Drawing.Size(164, 21);
            this.txtcellSize.TabIndex = 15;
            // 
            // txtprocess
            // 
            this.txtprocess.BackColor = System.Drawing.Color.DarkSlateGray;
            this.txtprocess.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtprocess.Location = new System.Drawing.Point(83, 85);
            this.txtprocess.Name = "txtprocess";
            this.txtprocess.ReadOnly = true;
            this.txtprocess.Size = new System.Drawing.Size(164, 21);
            this.txtprocess.TabIndex = 14;
            // 
            // txtrunName
            // 
            this.txtrunName.BackColor = System.Drawing.Color.DarkSlateGray;
            this.txtrunName.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtrunName.Location = new System.Drawing.Point(83, 31);
            this.txtrunName.Name = "txtrunName";
            this.txtrunName.ReadOnly = true;
            this.txtrunName.Size = new System.Drawing.Size(164, 21);
            this.txtrunName.TabIndex = 12;
            // 
            // txtrecipeName
            // 
            this.txtrecipeName.BackColor = System.Drawing.Color.DarkSlateGray;
            this.txtrecipeName.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtrecipeName.Location = new System.Drawing.Point(83, 58);
            this.txtrecipeName.Name = "txtrecipeName";
            this.txtrecipeName.ReadOnly = true;
            this.txtrecipeName.Size = new System.Drawing.Size(164, 21);
            this.txtrecipeName.TabIndex = 13;
            // 
            // lb_tact_time
            // 
            this.lb_tact_time.AutoSize = true;
            this.lb_tact_time.BackColor = System.Drawing.Color.Transparent;
            this.lb_tact_time.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_tact_time.ForeColor = System.Drawing.Color.White;
            this.lb_tact_time.Location = new System.Drawing.Point(3, 222);
            this.lb_tact_time.Name = "lb_tact_time";
            this.lb_tact_time.Size = new System.Drawing.Size(80, 20);
            this.lb_tact_time.TabIndex = 8;
            this.lb_tact_time.Text = "Tact-Time";
            // 
            // lb_run_count
            // 
            this.lb_run_count.AutoSize = true;
            this.lb_run_count.BackColor = System.Drawing.Color.Transparent;
            this.lb_run_count.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_run_count.ForeColor = System.Drawing.Color.White;
            this.lb_run_count.Location = new System.Drawing.Point(3, 196);
            this.lb_run_count.Name = "lb_run_count";
            this.lb_run_count.Size = new System.Drawing.Size(74, 20);
            this.lb_run_count.TabIndex = 7;
            this.lb_run_count.Text = "작업 수량";
            // 
            // lb_run_time
            // 
            this.lb_run_time.AutoSize = true;
            this.lb_run_time.BackColor = System.Drawing.Color.Transparent;
            this.lb_run_time.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_run_time.ForeColor = System.Drawing.Color.White;
            this.lb_run_time.Location = new System.Drawing.Point(3, 170);
            this.lb_run_time.Name = "lb_run_time";
            this.lb_run_time.Size = new System.Drawing.Size(74, 20);
            this.lb_run_time.TabIndex = 6;
            this.lb_run_time.Text = "작업 시간";
            // 
            // lb_start_time
            // 
            this.lb_start_time.AutoSize = true;
            this.lb_start_time.BackColor = System.Drawing.Color.Transparent;
            this.lb_start_time.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_start_time.ForeColor = System.Drawing.Color.White;
            this.lb_start_time.Location = new System.Drawing.Point(3, 143);
            this.lb_start_time.Name = "lb_start_time";
            this.lb_start_time.Size = new System.Drawing.Size(74, 20);
            this.lb_start_time.TabIndex = 5;
            this.lb_start_time.Text = "시작 시간";
            // 
            // lb_cell_size
            // 
            this.lb_cell_size.AutoSize = true;
            this.lb_cell_size.BackColor = System.Drawing.Color.Transparent;
            this.lb_cell_size.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_cell_size.ForeColor = System.Drawing.Color.White;
            this.lb_cell_size.Location = new System.Drawing.Point(3, 113);
            this.lb_cell_size.Name = "lb_cell_size";
            this.lb_cell_size.Size = new System.Drawing.Size(74, 20);
            this.lb_cell_size.TabIndex = 4;
            this.lb_cell_size.Text = "셀 사이즈";
            // 
            // lb_process
            // 
            this.lb_process.AutoSize = true;
            this.lb_process.BackColor = System.Drawing.Color.Transparent;
            this.lb_process.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_process.ForeColor = System.Drawing.Color.White;
            this.lb_process.Location = new System.Drawing.Point(3, 86);
            this.lb_process.Name = "lb_process";
            this.lb_process.Size = new System.Drawing.Size(69, 20);
            this.lb_process.TabIndex = 3;
            this.lb_process.Text = "프로세스";
            // 
            // lb_recipe_name
            // 
            this.lb_recipe_name.AutoSize = true;
            this.lb_recipe_name.BackColor = System.Drawing.Color.Transparent;
            this.lb_recipe_name.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_recipe_name.ForeColor = System.Drawing.Color.White;
            this.lb_recipe_name.Location = new System.Drawing.Point(3, 61);
            this.lb_recipe_name.Name = "lb_recipe_name";
            this.lb_recipe_name.Size = new System.Drawing.Size(69, 20);
            this.lb_recipe_name.TabIndex = 2;
            this.lb_recipe_name.Text = "레시피명";
            // 
            // lb_run_name
            // 
            this.lb_run_name.AutoSize = true;
            this.lb_run_name.BackColor = System.Drawing.Color.Transparent;
            this.lb_run_name.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_run_name.ForeColor = System.Drawing.Color.White;
            this.lb_run_name.Location = new System.Drawing.Point(3, 33);
            this.lb_run_name.Name = "lb_run_name";
            this.lb_run_name.Size = new System.Drawing.Size(69, 20);
            this.lb_run_name.TabIndex = 1;
            this.lb_run_name.Text = "작업자명";
            // 
            // lb_run_info
            // 
            this.lb_run_info.AutoSize = true;
            this.lb_run_info.BackColor = System.Drawing.Color.Transparent;
            this.lb_run_info.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.lb_run_info.ForeColor = System.Drawing.Color.White;
            this.lb_run_info.Location = new System.Drawing.Point(3, 7);
            this.lb_run_info.Name = "lb_run_info";
            this.lb_run_info.Size = new System.Drawing.Size(74, 21);
            this.lb_run_info.TabIndex = 0;
            this.lb_run_info.Text = "작업정보";
            // 
            // gxtlaser_info
            // 
            this.gxtlaser_info.Controls.Add(this.lbShutter);
            this.gxtlaser_info.Controls.Add(this.lb_shutter_info);
            this.gxtlaser_info.Controls.Add(this.lbPower);
            this.gxtlaser_info.Controls.Add(this.lb_power_info);
            this.gxtlaser_info.Controls.Add(this.lbDivider);
            this.gxtlaser_info.Controls.Add(this.lb_divider_info);
            this.gxtlaser_info.Controls.Add(this.lbPd7);
            this.gxtlaser_info.Controls.Add(this.lb_pd7power_info);
            this.gxtlaser_info.Controls.Add(this.lbOutamplifier);
            this.gxtlaser_info.Controls.Add(this.lbAmplifier);
            this.gxtlaser_info.Controls.Add(this.lbBurst);
            this.gxtlaser_info.Controls.Add(this.lbPulseMode);
            this.gxtlaser_info.Controls.Add(this.lb_outamplifier_info);
            this.gxtlaser_info.Controls.Add(this.lb_amplifier_info);
            this.gxtlaser_info.Controls.Add(this.lb_burst_info);
            this.gxtlaser_info.Controls.Add(this.lb_pulsemode_info);
            this.gxtlaser_info.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.gxtlaser_info.ForeColor = System.Drawing.Color.White;
            this.gxtlaser_info.Location = new System.Drawing.Point(4, 255);
            this.gxtlaser_info.Name = "gxtlaser_info";
            this.gxtlaser_info.Size = new System.Drawing.Size(253, 130);
            this.gxtlaser_info.TabIndex = 27;
            this.gxtlaser_info.TabStop = false;
            this.gxtlaser_info.Text = "레이저 정보";
            // 
            // lbShutter
            // 
            this.lbShutter.AutoSize = true;
            this.lbShutter.BackColor = System.Drawing.Color.Transparent;
            this.lbShutter.Font = new System.Drawing.Font("Malgun Gothic", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbShutter.ForeColor = System.Drawing.Color.White;
            this.lbShutter.Location = new System.Drawing.Point(191, 107);
            this.lbShutter.Name = "lbShutter";
            this.lbShutter.Size = new System.Drawing.Size(13, 13);
            this.lbShutter.TabIndex = 23;
            this.lbShutter.Text = "0";
            // 
            // lb_shutter_info
            // 
            this.lb_shutter_info.AutoSize = true;
            this.lb_shutter_info.BackColor = System.Drawing.Color.Transparent;
            this.lb_shutter_info.Font = new System.Drawing.Font("Malgun Gothic", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_shutter_info.ForeColor = System.Drawing.Color.White;
            this.lb_shutter_info.Location = new System.Drawing.Point(142, 107);
            this.lb_shutter_info.Name = "lb_shutter_info";
            this.lb_shutter_info.Size = new System.Drawing.Size(52, 13);
            this.lb_shutter_info.TabIndex = 22;
            this.lb_shutter_info.Text = "Shutter :";
            // 
            // lbPower
            // 
            this.lbPower.AutoSize = true;
            this.lbPower.BackColor = System.Drawing.Color.Transparent;
            this.lbPower.Font = new System.Drawing.Font("Malgun Gothic", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbPower.ForeColor = System.Drawing.Color.White;
            this.lbPower.Location = new System.Drawing.Point(204, 79);
            this.lbPower.Name = "lbPower";
            this.lbPower.Size = new System.Drawing.Size(13, 13);
            this.lbPower.TabIndex = 21;
            this.lbPower.Text = "0";
            // 
            // lb_power_info
            // 
            this.lb_power_info.AutoSize = true;
            this.lb_power_info.BackColor = System.Drawing.Color.Transparent;
            this.lb_power_info.Font = new System.Drawing.Font("Malgun Gothic", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_power_info.ForeColor = System.Drawing.Color.White;
            this.lb_power_info.Location = new System.Drawing.Point(142, 79);
            this.lb_power_info.Name = "lb_power_info";
            this.lb_power_info.Size = new System.Drawing.Size(65, 13);
            this.lb_power_info.TabIndex = 20;
            this.lb_power_info.Text = "Power(%) :";
            // 
            // lbDivider
            // 
            this.lbDivider.AutoSize = true;
            this.lbDivider.BackColor = System.Drawing.Color.Transparent;
            this.lbDivider.Font = new System.Drawing.Font("Malgun Gothic", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbDivider.ForeColor = System.Drawing.Color.White;
            this.lbDivider.Location = new System.Drawing.Point(190, 51);
            this.lbDivider.Name = "lbDivider";
            this.lbDivider.Size = new System.Drawing.Size(13, 13);
            this.lbDivider.TabIndex = 19;
            this.lbDivider.Text = "0";
            // 
            // lb_divider_info
            // 
            this.lb_divider_info.AutoSize = true;
            this.lb_divider_info.BackColor = System.Drawing.Color.Transparent;
            this.lb_divider_info.Font = new System.Drawing.Font("Malgun Gothic", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_divider_info.ForeColor = System.Drawing.Color.White;
            this.lb_divider_info.Location = new System.Drawing.Point(142, 51);
            this.lb_divider_info.Name = "lb_divider_info";
            this.lb_divider_info.Size = new System.Drawing.Size(51, 13);
            this.lb_divider_info.TabIndex = 18;
            this.lb_divider_info.Text = "Divider :";
            // 
            // lbPd7
            // 
            this.lbPd7.AutoSize = true;
            this.lbPd7.BackColor = System.Drawing.Color.Transparent;
            this.lbPd7.Font = new System.Drawing.Font("Malgun Gothic", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbPd7.ForeColor = System.Drawing.Color.White;
            this.lbPd7.Location = new System.Drawing.Point(207, 25);
            this.lbPd7.Name = "lbPd7";
            this.lbPd7.Size = new System.Drawing.Size(13, 13);
            this.lbPd7.TabIndex = 17;
            this.lbPd7.Text = "0";
            // 
            // lb_pd7power_info
            // 
            this.lb_pd7power_info.AutoSize = true;
            this.lb_pd7power_info.BackColor = System.Drawing.Color.Transparent;
            this.lb_pd7power_info.Font = new System.Drawing.Font("Malgun Gothic", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_pd7power_info.ForeColor = System.Drawing.Color.White;
            this.lb_pd7power_info.Location = new System.Drawing.Point(142, 25);
            this.lb_pd7power_info.Name = "lb_pd7power_info";
            this.lb_pd7power_info.Size = new System.Drawing.Size(68, 13);
            this.lb_pd7power_info.TabIndex = 16;
            this.lb_pd7power_info.Text = "PD7Power :";
            // 
            // lbOutamplifier
            // 
            this.lbOutamplifier.AutoSize = true;
            this.lbOutamplifier.BackColor = System.Drawing.Color.Transparent;
            this.lbOutamplifier.Font = new System.Drawing.Font("Malgun Gothic", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbOutamplifier.ForeColor = System.Drawing.Color.White;
            this.lbOutamplifier.Location = new System.Drawing.Point(91, 107);
            this.lbOutamplifier.Name = "lbOutamplifier";
            this.lbOutamplifier.Size = new System.Drawing.Size(13, 13);
            this.lbOutamplifier.TabIndex = 15;
            this.lbOutamplifier.Text = "0";
            // 
            // lbAmplifier
            // 
            this.lbAmplifier.AutoSize = true;
            this.lbAmplifier.BackColor = System.Drawing.Color.Transparent;
            this.lbAmplifier.Font = new System.Drawing.Font("Malgun Gothic", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbAmplifier.ForeColor = System.Drawing.Color.White;
            this.lbAmplifier.Location = new System.Drawing.Point(65, 79);
            this.lbAmplifier.Name = "lbAmplifier";
            this.lbAmplifier.Size = new System.Drawing.Size(13, 13);
            this.lbAmplifier.TabIndex = 14;
            this.lbAmplifier.Text = "0";
            // 
            // lbBurst
            // 
            this.lbBurst.AutoSize = true;
            this.lbBurst.BackColor = System.Drawing.Color.Transparent;
            this.lbBurst.Font = new System.Drawing.Font("Malgun Gothic", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbBurst.ForeColor = System.Drawing.Color.White;
            this.lbBurst.Location = new System.Drawing.Point(42, 51);
            this.lbBurst.Name = "lbBurst";
            this.lbBurst.Size = new System.Drawing.Size(13, 13);
            this.lbBurst.TabIndex = 13;
            this.lbBurst.Text = "0";
            // 
            // lbPulseMode
            // 
            this.lbPulseMode.AutoSize = true;
            this.lbPulseMode.BackColor = System.Drawing.Color.Transparent;
            this.lbPulseMode.Font = new System.Drawing.Font("Malgun Gothic", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbPulseMode.ForeColor = System.Drawing.Color.White;
            this.lbPulseMode.Location = new System.Drawing.Point(78, 25);
            this.lbPulseMode.Name = "lbPulseMode";
            this.lbPulseMode.Size = new System.Drawing.Size(13, 13);
            this.lbPulseMode.TabIndex = 12;
            this.lbPulseMode.Text = "0";
            // 
            // lb_outamplifier_info
            // 
            this.lb_outamplifier_info.AutoSize = true;
            this.lb_outamplifier_info.BackColor = System.Drawing.Color.Transparent;
            this.lb_outamplifier_info.Font = new System.Drawing.Font("Malgun Gothic", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_outamplifier_info.ForeColor = System.Drawing.Color.White;
            this.lb_outamplifier_info.Location = new System.Drawing.Point(8, 107);
            this.lb_outamplifier_info.Name = "lb_outamplifier_info";
            this.lb_outamplifier_info.Size = new System.Drawing.Size(86, 13);
            this.lb_outamplifier_info.TabIndex = 11;
            this.lb_outamplifier_info.Text = "Out Amplifier :";
            // 
            // lb_amplifier_info
            // 
            this.lb_amplifier_info.AutoSize = true;
            this.lb_amplifier_info.BackColor = System.Drawing.Color.Transparent;
            this.lb_amplifier_info.Font = new System.Drawing.Font("Malgun Gothic", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_amplifier_info.ForeColor = System.Drawing.Color.White;
            this.lb_amplifier_info.Location = new System.Drawing.Point(6, 79);
            this.lb_amplifier_info.Name = "lb_amplifier_info";
            this.lb_amplifier_info.Size = new System.Drawing.Size(62, 13);
            this.lb_amplifier_info.TabIndex = 10;
            this.lb_amplifier_info.Text = "Amplifier :";
            // 
            // lb_burst_info
            // 
            this.lb_burst_info.AutoSize = true;
            this.lb_burst_info.BackColor = System.Drawing.Color.Transparent;
            this.lb_burst_info.Font = new System.Drawing.Font("Malgun Gothic", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_burst_info.ForeColor = System.Drawing.Color.White;
            this.lb_burst_info.Location = new System.Drawing.Point(4, 51);
            this.lb_burst_info.Name = "lb_burst_info";
            this.lb_burst_info.Size = new System.Drawing.Size(41, 13);
            this.lb_burst_info.TabIndex = 9;
            this.lb_burst_info.Text = "Burst :";
            // 
            // lb_pulsemode_info
            // 
            this.lb_pulsemode_info.AutoSize = true;
            this.lb_pulsemode_info.BackColor = System.Drawing.Color.Transparent;
            this.lb_pulsemode_info.Font = new System.Drawing.Font("Malgun Gothic", 8.25F, System.Drawing.FontStyle.Bold);
            this.lb_pulsemode_info.ForeColor = System.Drawing.Color.White;
            this.lb_pulsemode_info.Location = new System.Drawing.Point(4, 25);
            this.lb_pulsemode_info.Name = "lb_pulsemode_info";
            this.lb_pulsemode_info.Size = new System.Drawing.Size(77, 13);
            this.lb_pulsemode_info.TabIndex = 8;
            this.lb_pulsemode_info.Text = "Pulse Mode :";
            // 
            // gxtmcr_count
            // 
            this.gxtmcr_count.BackColor = System.Drawing.Color.Transparent;
            this.gxtmcr_count.Controls.Add(this.panel_cstinfo);
            this.gxtmcr_count.Controls.Add(this.panel_day_info);
            this.gxtmcr_count.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtmcr_count.ForeColor = System.Drawing.Color.White;
            this.gxtmcr_count.Location = new System.Drawing.Point(3, 627);
            this.gxtmcr_count.Name = "gxtmcr_count";
            this.gxtmcr_count.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gxtmcr_count.Size = new System.Drawing.Size(343, 106);
            this.gxtmcr_count.TabIndex = 31;
            this.gxtmcr_count.TabStop = false;
            this.gxtmcr_count.Text = "MCR 카운트";
            // 
            // panel_cstinfo
            // 
            this.panel_cstinfo.BackColor = System.Drawing.Color.DimGray;
            this.panel_cstinfo.Controls.Add(this.lbCstTotalCount);
            this.panel_cstinfo.Controls.Add(this.lb_cst_total_count_info);
            this.panel_cstinfo.Controls.Add(this.lbCstReadCount);
            this.panel_cstinfo.Controls.Add(this.lb_cst_read_count_info);
            this.panel_cstinfo.Controls.Add(this.lb_cst);
            this.panel_cstinfo.Location = new System.Drawing.Point(4, 63);
            this.panel_cstinfo.Name = "panel_cstinfo";
            this.panel_cstinfo.Size = new System.Drawing.Size(334, 28);
            this.panel_cstinfo.TabIndex = 5;
            // 
            // lbCstTotalCount
            // 
            this.lbCstTotalCount.AutoSize = true;
            this.lbCstTotalCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbCstTotalCount.Location = new System.Drawing.Point(289, 5);
            this.lbCstTotalCount.Name = "lbCstTotalCount";
            this.lbCstTotalCount.Size = new System.Drawing.Size(17, 18);
            this.lbCstTotalCount.TabIndex = 4;
            this.lbCstTotalCount.Text = "0";
            // 
            // lb_cst_total_count_info
            // 
            this.lb_cst_total_count_info.AutoSize = true;
            this.lb_cst_total_count_info.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_cst_total_count_info.Location = new System.Drawing.Point(192, 5);
            this.lb_cst_total_count_info.Name = "lb_cst_total_count_info";
            this.lb_cst_total_count_info.Size = new System.Drawing.Size(96, 20);
            this.lb_cst_total_count_info.TabIndex = 3;
            this.lb_cst_total_count_info.Text = "total count :";
            // 
            // lbCstReadCount
            // 
            this.lbCstReadCount.AutoSize = true;
            this.lbCstReadCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbCstReadCount.Location = new System.Drawing.Point(148, 5);
            this.lbCstReadCount.Name = "lbCstReadCount";
            this.lbCstReadCount.Size = new System.Drawing.Size(17, 18);
            this.lbCstReadCount.TabIndex = 2;
            this.lbCstReadCount.Text = "0";
            // 
            // lb_cst_read_count_info
            // 
            this.lb_cst_read_count_info.AutoSize = true;
            this.lb_cst_read_count_info.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_cst_read_count_info.Location = new System.Drawing.Point(52, 5);
            this.lb_cst_read_count_info.Name = "lb_cst_read_count_info";
            this.lb_cst_read_count_info.Size = new System.Drawing.Size(94, 20);
            this.lb_cst_read_count_info.TabIndex = 1;
            this.lb_cst_read_count_info.Text = "read count :";
            // 
            // lb_cst
            // 
            this.lb_cst.AutoSize = true;
            this.lb_cst.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_cst.Location = new System.Drawing.Point(6, 5);
            this.lb_cst.Name = "lb_cst";
            this.lb_cst.Size = new System.Drawing.Size(36, 20);
            this.lb_cst.TabIndex = 0;
            this.lb_cst.Text = "CST";
            // 
            // panel_day_info
            // 
            this.panel_day_info.BackColor = System.Drawing.Color.DimGray;
            this.panel_day_info.Controls.Add(this.lbDayTotalCount);
            this.panel_day_info.Controls.Add(this.lb_day_total_count_info);
            this.panel_day_info.Controls.Add(this.lbDayReadCount);
            this.panel_day_info.Controls.Add(this.lb_day_read_count_info);
            this.panel_day_info.Controls.Add(this.lb_day);
            this.panel_day_info.Location = new System.Drawing.Point(4, 28);
            this.panel_day_info.Name = "panel_day_info";
            this.panel_day_info.Size = new System.Drawing.Size(334, 28);
            this.panel_day_info.TabIndex = 4;
            // 
            // lbDayTotalCount
            // 
            this.lbDayTotalCount.AutoSize = true;
            this.lbDayTotalCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbDayTotalCount.Location = new System.Drawing.Point(289, 5);
            this.lbDayTotalCount.Name = "lbDayTotalCount";
            this.lbDayTotalCount.Size = new System.Drawing.Size(17, 18);
            this.lbDayTotalCount.TabIndex = 4;
            this.lbDayTotalCount.Text = "0";
            // 
            // lb_day_total_count_info
            // 
            this.lb_day_total_count_info.AutoSize = true;
            this.lb_day_total_count_info.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_day_total_count_info.Location = new System.Drawing.Point(192, 5);
            this.lb_day_total_count_info.Name = "lb_day_total_count_info";
            this.lb_day_total_count_info.Size = new System.Drawing.Size(96, 20);
            this.lb_day_total_count_info.TabIndex = 3;
            this.lb_day_total_count_info.Text = "total count :";
            // 
            // lbDayReadCount
            // 
            this.lbDayReadCount.AutoSize = true;
            this.lbDayReadCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbDayReadCount.Location = new System.Drawing.Point(148, 5);
            this.lbDayReadCount.Name = "lbDayReadCount";
            this.lbDayReadCount.Size = new System.Drawing.Size(17, 18);
            this.lbDayReadCount.TabIndex = 2;
            this.lbDayReadCount.Text = "0";
            // 
            // lb_day_read_count_info
            // 
            this.lb_day_read_count_info.AutoSize = true;
            this.lb_day_read_count_info.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_day_read_count_info.Location = new System.Drawing.Point(52, 5);
            this.lb_day_read_count_info.Name = "lb_day_read_count_info";
            this.lb_day_read_count_info.Size = new System.Drawing.Size(94, 20);
            this.lb_day_read_count_info.TabIndex = 1;
            this.lb_day_read_count_info.Text = "read count :";
            // 
            // lb_day
            // 
            this.lb_day.AutoSize = true;
            this.lb_day.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_day.Location = new System.Drawing.Point(6, 5);
            this.lb_day.Name = "lb_day";
            this.lb_day.Size = new System.Drawing.Size(40, 20);
            this.lb_day.TabIndex = 0;
            this.lb_day.Text = "DAY";
            // 
            // gxtuld
            // 
            this.gxtuld.BackColor = System.Drawing.Color.Transparent;
            this.gxtuld.Controls.Add(this.btnUldOutMuting);
            this.gxtuld.Controls.Add(this.btnUldInMuting);
            this.gxtuld.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.gxtuld.ForeColor = System.Drawing.Color.White;
            this.gxtuld.Location = new System.Drawing.Point(4, 739);
            this.gxtuld.Name = "gxtuld";
            this.gxtuld.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gxtuld.Size = new System.Drawing.Size(168, 117);
            this.gxtuld.TabIndex = 32;
            this.gxtuld.TabStop = false;
            this.gxtuld.Text = "언로딩";
            // 
            // btnUldOutMuting
            // 
            this.btnUldOutMuting.BackColor = System.Drawing.Color.DimGray;
            this.btnUldOutMuting.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.btnUldOutMuting.ForeColor = System.Drawing.Color.White;
            this.btnUldOutMuting.Location = new System.Drawing.Point(6, 28);
            this.btnUldOutMuting.Name = "btnUldOutMuting";
            this.btnUldOutMuting.Size = new System.Drawing.Size(156, 35);
            this.btnUldOutMuting.TabIndex = 92;
            this.btnUldOutMuting.Text = "배출 뮤팅";
            this.btnUldOutMuting.UseVisualStyleBackColor = false;
            // 
            // btnUldInMuting
            // 
            this.btnUldInMuting.BackColor = System.Drawing.Color.DimGray;
            this.btnUldInMuting.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.btnUldInMuting.ForeColor = System.Drawing.Color.White;
            this.btnUldInMuting.Location = new System.Drawing.Point(6, 69);
            this.btnUldInMuting.Name = "btnUldInMuting";
            this.btnUldInMuting.Size = new System.Drawing.Size(156, 35);
            this.btnUldInMuting.TabIndex = 92;
            this.btnUldInMuting.Text = "투입 뮤팅";
            this.btnUldInMuting.UseVisualStyleBackColor = false;
            // 
            // gxtld
            // 
            this.gxtld.BackColor = System.Drawing.Color.Transparent;
            this.gxtld.Controls.Add(this.btnLdOutMuting);
            this.gxtld.Controls.Add(this.btnLdInMuting);
            this.gxtld.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.gxtld.ForeColor = System.Drawing.Color.White;
            this.gxtld.Location = new System.Drawing.Point(178, 739);
            this.gxtld.Name = "gxtld";
            this.gxtld.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gxtld.Size = new System.Drawing.Size(168, 117);
            this.gxtld.TabIndex = 33;
            this.gxtld.TabStop = false;
            this.gxtld.Text = "로딩";
            // 
            // btnLdOutMuting
            // 
            this.btnLdOutMuting.BackColor = System.Drawing.Color.DimGray;
            this.btnLdOutMuting.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.btnLdOutMuting.ForeColor = System.Drawing.Color.White;
            this.btnLdOutMuting.Location = new System.Drawing.Point(6, 69);
            this.btnLdOutMuting.Name = "btnLdOutMuting";
            this.btnLdOutMuting.Size = new System.Drawing.Size(156, 35);
            this.btnLdOutMuting.TabIndex = 93;
            this.btnLdOutMuting.Text = "배출 뮤팅";
            this.btnLdOutMuting.UseVisualStyleBackColor = false;
            // 
            // btnLdInMuting
            // 
            this.btnLdInMuting.BackColor = System.Drawing.Color.DimGray;
            this.btnLdInMuting.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.btnLdInMuting.ForeColor = System.Drawing.Color.White;
            this.btnLdInMuting.Location = new System.Drawing.Point(6, 28);
            this.btnLdInMuting.Name = "btnLdInMuting";
            this.btnLdInMuting.Size = new System.Drawing.Size(156, 35);
            this.btnLdInMuting.TabIndex = 91;
            this.btnLdInMuting.Text = "투입 뮤팅";
            this.btnLdInMuting.UseVisualStyleBackColor = false;
            // 
            // btn_safety_reset
            // 
            this.btn_safety_reset.BackColor = System.Drawing.Color.DimGray;
            this.btn_safety_reset.Font = new System.Drawing.Font("Malgun Gothic", 17F, System.Drawing.FontStyle.Bold);
            this.btn_safety_reset.ForeColor = System.Drawing.Color.White;
            this.btn_safety_reset.Location = new System.Drawing.Point(352, 745);
            this.btn_safety_reset.Name = "btn_safety_reset";
            this.btn_safety_reset.Size = new System.Drawing.Size(168, 111);
            this.btn_safety_reset.TabIndex = 28;
            this.btn_safety_reset.Text = "세이프티 리셋";
            this.btn_safety_reset.UseVisualStyleBackColor = false;
            // 
            // panel_run
            // 
            this.panel_run.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_run.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_run.Controls.Add(this.btnACSTSkip);
            this.panel_run.Controls.Add(this.btnBCSTSkip);
            this.panel_run.Controls.Add(this.panel_lot_b_info);
            this.panel_run.Controls.Add(this.panel_lot_a_info);
            this.panel_run.Controls.Add(this.panel_ld_muting_out);
            this.panel_run.Controls.Add(this.panelUldMutingIn);
            this.panel_run.Controls.Add(this.panel_ld_muting_in);
            this.panel_run.Controls.Add(this.panelUldMutingOut);
            this.panel_run.Controls.Add(this.panelDoor2);
            this.panel_run.Controls.Add(this.panel38);
            this.panel_run.Controls.Add(this.panelDoor3);
            this.panel_run.Controls.Add(this.panelDoor5);
            this.panel_run.Controls.Add(this.panelDoor4);
            this.panel_run.Controls.Add(this.panelDoor6);
            this.panel_run.Controls.Add(this.panelDoor8);
            this.panel_run.Controls.Add(this.panelDoor7);
            this.panel_run.Controls.Add(this.panelDoor9);
            this.panel_run.Controls.Add(this.panelLdBox);
            this.panel_run.Controls.Add(this.panelEms3);
            this.panel_run.Controls.Add(this.panelProcessBox);
            this.panel_run.Controls.Add(this.panelEms5);
            this.panel_run.Controls.Add(this.panelUldBox);
            this.panel_run.Controls.Add(this.panel_run2);
            this.panel_run.Controls.Add(this.panel_door10);
            this.panel_run.Controls.Add(this.panel2);
            this.panel_run.Controls.Add(this.panel_camera_l_info);
            this.panel_run.Location = new System.Drawing.Point(471, 3);
            this.panel_run.Name = "panel_run";
            this.panel_run.Size = new System.Drawing.Size(1056, 382);
            this.panel_run.TabIndex = 60;
            // 
            // btnACSTSkip
            // 
            this.btnACSTSkip.BackColor = System.Drawing.Color.DimGray;
            this.btnACSTSkip.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold);
            this.btnACSTSkip.ForeColor = System.Drawing.Color.White;
            this.btnACSTSkip.Location = new System.Drawing.Point(695, 3);
            this.btnACSTSkip.Name = "btnACSTSkip";
            this.btnACSTSkip.Size = new System.Drawing.Size(64, 33);
            this.btnACSTSkip.TabIndex = 59;
            this.btnACSTSkip.Text = "A-CST\r\nSkip";
            this.btnACSTSkip.UseVisualStyleBackColor = false;
            // 
            // btnBCSTSkip
            // 
            this.btnBCSTSkip.BackColor = System.Drawing.Color.DimGray;
            this.btnBCSTSkip.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold);
            this.btnBCSTSkip.ForeColor = System.Drawing.Color.White;
            this.btnBCSTSkip.Location = new System.Drawing.Point(768, 3);
            this.btnBCSTSkip.Name = "btnBCSTSkip";
            this.btnBCSTSkip.Size = new System.Drawing.Size(64, 33);
            this.btnBCSTSkip.TabIndex = 58;
            this.btnBCSTSkip.Text = "B-CST\r\nSkip";
            this.btnBCSTSkip.UseVisualStyleBackColor = false;
            // 
            // panel_lot_b_info
            // 
            this.panel_lot_b_info.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_lot_b_info.Controls.Add(this.label5);
            this.panel_lot_b_info.Controls.Add(this.lb_lot_b_info);
            this.panel_lot_b_info.Location = new System.Drawing.Point(887, 156);
            this.panel_lot_b_info.Name = "panel_lot_b_info";
            this.panel_lot_b_info.Size = new System.Drawing.Size(168, 33);
            this.panel_lot_b_info.TabIndex = 24;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.ForeColor = System.Drawing.Color.YellowGreen;
            this.label5.Location = new System.Drawing.Point(10, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 12);
            this.label5.TabIndex = 1;
            this.label5.Text = "ABCDEFGHIJK";
            // 
            // lb_lot_b_info
            // 
            this.lb_lot_b_info.AutoSize = true;
            this.lb_lot_b_info.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_lot_b_info.ForeColor = System.Drawing.Color.YellowGreen;
            this.lb_lot_b_info.Location = new System.Drawing.Point(5, 2);
            this.lb_lot_b_info.Name = "lb_lot_b_info";
            this.lb_lot_b_info.Size = new System.Drawing.Size(64, 12);
            this.lb_lot_b_info.TabIndex = 0;
            this.lb_lot_b_info.Text = "LOT ID B";
            // 
            // panel_lot_a_info
            // 
            this.panel_lot_a_info.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_lot_a_info.Controls.Add(this.lb_lot_a);
            this.panel_lot_a_info.Controls.Add(this.lb_lot_a_info);
            this.panel_lot_a_info.Location = new System.Drawing.Point(887, 191);
            this.panel_lot_a_info.Name = "panel_lot_a_info";
            this.panel_lot_a_info.Size = new System.Drawing.Size(168, 33);
            this.panel_lot_a_info.TabIndex = 23;
            // 
            // lb_lot_a
            // 
            this.lb_lot_a.AutoSize = true;
            this.lb_lot_a.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_lot_a.ForeColor = System.Drawing.Color.YellowGreen;
            this.lb_lot_a.Location = new System.Drawing.Point(10, 16);
            this.lb_lot_a.Name = "lb_lot_a";
            this.lb_lot_a.Size = new System.Drawing.Size(98, 12);
            this.lb_lot_a.TabIndex = 1;
            this.lb_lot_a.Text = "ABCDEFGHIJK";
            // 
            // lb_lot_a_info
            // 
            this.lb_lot_a_info.AutoSize = true;
            this.lb_lot_a_info.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_lot_a_info.ForeColor = System.Drawing.Color.YellowGreen;
            this.lb_lot_a_info.Location = new System.Drawing.Point(5, 2);
            this.lb_lot_a_info.Name = "lb_lot_a_info";
            this.lb_lot_a_info.Size = new System.Drawing.Size(64, 12);
            this.lb_lot_a_info.TabIndex = 0;
            this.lb_lot_a_info.Text = "LOT ID A";
            // 
            // panel_ld_muting_out
            // 
            this.panel_ld_muting_out.BackColor = System.Drawing.Color.DimGray;
            this.panel_ld_muting_out.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_ld_muting_out.Controls.Add(this.lbLdMutingOut);
            this.panel_ld_muting_out.ForeColor = System.Drawing.Color.White;
            this.panel_ld_muting_out.Location = new System.Drawing.Point(844, 195);
            this.panel_ld_muting_out.Name = "panel_ld_muting_out";
            this.panel_ld_muting_out.Size = new System.Drawing.Size(37, 39);
            this.panel_ld_muting_out.TabIndex = 19;
            // 
            // lbLdMutingOut
            // 
            this.lbLdMutingOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbLdMutingOut.ForeColor = System.Drawing.Color.White;
            this.lbLdMutingOut.Location = new System.Drawing.Point(3, 3);
            this.lbLdMutingOut.Name = "lbLdMutingOut";
            this.lbLdMutingOut.Size = new System.Drawing.Size(32, 33);
            this.lbLdMutingOut.TabIndex = 1;
            this.lbLdMutingOut.Text = "뮤팅아웃";
            this.lbLdMutingOut.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelUldMutingIn
            // 
            this.panelUldMutingIn.BackColor = System.Drawing.Color.DimGray;
            this.panelUldMutingIn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelUldMutingIn.Controls.Add(this.lbUldMutingIn);
            this.panelUldMutingIn.ForeColor = System.Drawing.Color.White;
            this.panelUldMutingIn.Location = new System.Drawing.Point(4, 175);
            this.panelUldMutingIn.Name = "panelUldMutingIn";
            this.panelUldMutingIn.Size = new System.Drawing.Size(37, 39);
            this.panelUldMutingIn.TabIndex = 15;
            // 
            // lbUldMutingIn
            // 
            this.lbUldMutingIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbUldMutingIn.ForeColor = System.Drawing.Color.White;
            this.lbUldMutingIn.Location = new System.Drawing.Point(3, 3);
            this.lbUldMutingIn.Name = "lbUldMutingIn";
            this.lbUldMutingIn.Size = new System.Drawing.Size(32, 33);
            this.lbUldMutingIn.TabIndex = 1;
            this.lbUldMutingIn.Text = "뮤팅인";
            this.lbUldMutingIn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_ld_muting_in
            // 
            this.panel_ld_muting_in.BackColor = System.Drawing.Color.DimGray;
            this.panel_ld_muting_in.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_ld_muting_in.Controls.Add(this.lbLdMutingIn);
            this.panel_ld_muting_in.ForeColor = System.Drawing.Color.White;
            this.panel_ld_muting_in.Location = new System.Drawing.Point(844, 150);
            this.panel_ld_muting_in.Name = "panel_ld_muting_in";
            this.panel_ld_muting_in.Size = new System.Drawing.Size(37, 39);
            this.panel_ld_muting_in.TabIndex = 18;
            // 
            // lbLdMutingIn
            // 
            this.lbLdMutingIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbLdMutingIn.ForeColor = System.Drawing.Color.White;
            this.lbLdMutingIn.Location = new System.Drawing.Point(3, 3);
            this.lbLdMutingIn.Name = "lbLdMutingIn";
            this.lbLdMutingIn.Size = new System.Drawing.Size(32, 33);
            this.lbLdMutingIn.TabIndex = 1;
            this.lbLdMutingIn.Text = "뮤팅인";
            this.lbLdMutingIn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelUldMutingOut
            // 
            this.panelUldMutingOut.BackColor = System.Drawing.Color.DimGray;
            this.panelUldMutingOut.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelUldMutingOut.Controls.Add(this.lbUldMuting_out);
            this.panelUldMutingOut.ForeColor = System.Drawing.Color.White;
            this.panelUldMutingOut.Location = new System.Drawing.Point(4, 130);
            this.panelUldMutingOut.Name = "panelUldMutingOut";
            this.panelUldMutingOut.Size = new System.Drawing.Size(37, 39);
            this.panelUldMutingOut.TabIndex = 14;
            // 
            // lbUldMuting_out
            // 
            this.lbUldMuting_out.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbUldMuting_out.ForeColor = System.Drawing.Color.White;
            this.lbUldMuting_out.Location = new System.Drawing.Point(3, 3);
            this.lbUldMuting_out.Name = "lbUldMuting_out";
            this.lbUldMuting_out.Size = new System.Drawing.Size(32, 33);
            this.lbUldMuting_out.TabIndex = 1;
            this.lbUldMuting_out.Text = "뮤팅아웃";
            this.lbUldMuting_out.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelDoor2
            // 
            this.panelDoor2.BackColor = System.Drawing.Color.DimGray;
            this.panelDoor2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelDoor2.Controls.Add(this.lbDoor2);
            this.panelDoor2.ForeColor = System.Drawing.Color.White;
            this.panelDoor2.Location = new System.Drawing.Point(844, 85);
            this.panelDoor2.Name = "panelDoor2";
            this.panelDoor2.Size = new System.Drawing.Size(37, 59);
            this.panelDoor2.TabIndex = 16;
            // 
            // lbDoor2
            // 
            this.lbDoor2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbDoor2.ForeColor = System.Drawing.Color.White;
            this.lbDoor2.Location = new System.Drawing.Point(10, 3);
            this.lbDoor2.Name = "lbDoor2";
            this.lbDoor2.Size = new System.Drawing.Size(15, 51);
            this.lbDoor2.TabIndex = 1;
            this.lbDoor2.Text = "도어2";
            this.lbDoor2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel38
            // 
            this.panel38.BackColor = System.Drawing.Color.DimGray;
            this.panel38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel38.Controls.Add(this.label51);
            this.panel38.ForeColor = System.Drawing.Color.White;
            this.panel38.Location = new System.Drawing.Point(845, 86);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(37, 59);
            this.panel38.TabIndex = 17;
            // 
            // label51
            // 
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label51.ForeColor = System.Drawing.Color.White;
            this.label51.Location = new System.Drawing.Point(4, 3);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(27, 51);
            this.label51.TabIndex = 1;
            this.label51.Text = "도어10";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelDoor3
            // 
            this.panelDoor3.BackColor = System.Drawing.Color.DimGray;
            this.panelDoor3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelDoor3.Controls.Add(this.lb_door3);
            this.panelDoor3.ForeColor = System.Drawing.Color.White;
            this.panelDoor3.Location = new System.Drawing.Point(692, 344);
            this.panelDoor3.Name = "panelDoor3";
            this.panelDoor3.Size = new System.Drawing.Size(67, 33);
            this.panelDoor3.TabIndex = 19;
            // 
            // lb_door3
            // 
            this.lb_door3.AutoSize = true;
            this.lb_door3.BackColor = System.Drawing.Color.Transparent;
            this.lb_door3.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_door3.ForeColor = System.Drawing.Color.White;
            this.lb_door3.Location = new System.Drawing.Point(11, 9);
            this.lb_door3.Name = "lb_door3";
            this.lb_door3.Size = new System.Drawing.Size(43, 12);
            this.lb_door3.TabIndex = 4;
            this.lb_door3.Text = "도어 3";
            // 
            // panelDoor5
            // 
            this.panelDoor5.BackColor = System.Drawing.Color.DimGray;
            this.panelDoor5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelDoor5.Controls.Add(this.lbDoor5);
            this.panelDoor5.ForeColor = System.Drawing.Color.White;
            this.panelDoor5.Location = new System.Drawing.Point(495, 344);
            this.panelDoor5.Name = "panelDoor5";
            this.panelDoor5.Size = new System.Drawing.Size(67, 33);
            this.panelDoor5.TabIndex = 20;
            // 
            // lbDoor5
            // 
            this.lbDoor5.AutoSize = true;
            this.lbDoor5.BackColor = System.Drawing.Color.Transparent;
            this.lbDoor5.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbDoor5.ForeColor = System.Drawing.Color.White;
            this.lbDoor5.Location = new System.Drawing.Point(11, 9);
            this.lbDoor5.Name = "lbDoor5";
            this.lbDoor5.Size = new System.Drawing.Size(43, 12);
            this.lbDoor5.TabIndex = 4;
            this.lbDoor5.Text = "도어 5";
            // 
            // panelDoor4
            // 
            this.panelDoor4.BackColor = System.Drawing.Color.DimGray;
            this.panelDoor4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelDoor4.Controls.Add(this.lb_door4);
            this.panelDoor4.ForeColor = System.Drawing.Color.White;
            this.panelDoor4.Location = new System.Drawing.Point(619, 344);
            this.panelDoor4.Name = "panelDoor4";
            this.panelDoor4.Size = new System.Drawing.Size(67, 33);
            this.panelDoor4.TabIndex = 18;
            // 
            // lb_door4
            // 
            this.lb_door4.AutoSize = true;
            this.lb_door4.BackColor = System.Drawing.Color.Transparent;
            this.lb_door4.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_door4.ForeColor = System.Drawing.Color.White;
            this.lb_door4.Location = new System.Drawing.Point(11, 9);
            this.lb_door4.Name = "lb_door4";
            this.lb_door4.Size = new System.Drawing.Size(43, 12);
            this.lb_door4.TabIndex = 4;
            this.lb_door4.Text = "도어 4";
            // 
            // panelDoor6
            // 
            this.panelDoor6.BackColor = System.Drawing.Color.DimGray;
            this.panelDoor6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelDoor6.Controls.Add(this.lb_door6);
            this.panelDoor6.ForeColor = System.Drawing.Color.White;
            this.panelDoor6.Location = new System.Drawing.Point(422, 344);
            this.panelDoor6.Name = "panelDoor6";
            this.panelDoor6.Size = new System.Drawing.Size(67, 33);
            this.panelDoor6.TabIndex = 19;
            // 
            // lb_door6
            // 
            this.lb_door6.AutoSize = true;
            this.lb_door6.BackColor = System.Drawing.Color.Transparent;
            this.lb_door6.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_door6.ForeColor = System.Drawing.Color.White;
            this.lb_door6.Location = new System.Drawing.Point(11, 9);
            this.lb_door6.Name = "lb_door6";
            this.lb_door6.Size = new System.Drawing.Size(43, 12);
            this.lb_door6.TabIndex = 4;
            this.lb_door6.Text = "도어 6";
            // 
            // panelDoor8
            // 
            this.panelDoor8.BackColor = System.Drawing.Color.DimGray;
            this.panelDoor8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelDoor8.Controls.Add(this.lb_door8);
            this.panelDoor8.ForeColor = System.Drawing.Color.White;
            this.panelDoor8.Location = new System.Drawing.Point(223, 344);
            this.panelDoor8.Name = "panelDoor8";
            this.panelDoor8.Size = new System.Drawing.Size(67, 33);
            this.panelDoor8.TabIndex = 17;
            // 
            // lb_door8
            // 
            this.lb_door8.AutoSize = true;
            this.lb_door8.BackColor = System.Drawing.Color.Transparent;
            this.lb_door8.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_door8.ForeColor = System.Drawing.Color.White;
            this.lb_door8.Location = new System.Drawing.Point(11, 9);
            this.lb_door8.Name = "lb_door8";
            this.lb_door8.Size = new System.Drawing.Size(43, 12);
            this.lb_door8.TabIndex = 4;
            this.lb_door8.Text = "도어 8";
            // 
            // panelDoor7
            // 
            this.panelDoor7.BackColor = System.Drawing.Color.DimGray;
            this.panelDoor7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelDoor7.Controls.Add(this.lb_door7);
            this.panelDoor7.ForeColor = System.Drawing.Color.White;
            this.panelDoor7.Location = new System.Drawing.Point(349, 344);
            this.panelDoor7.Name = "panelDoor7";
            this.panelDoor7.Size = new System.Drawing.Size(67, 33);
            this.panelDoor7.TabIndex = 18;
            // 
            // lb_door7
            // 
            this.lb_door7.AutoSize = true;
            this.lb_door7.BackColor = System.Drawing.Color.Transparent;
            this.lb_door7.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_door7.ForeColor = System.Drawing.Color.White;
            this.lb_door7.Location = new System.Drawing.Point(11, 9);
            this.lb_door7.Name = "lb_door7";
            this.lb_door7.Size = new System.Drawing.Size(43, 12);
            this.lb_door7.TabIndex = 4;
            this.lb_door7.Text = "도어 7";
            // 
            // panelDoor9
            // 
            this.panelDoor9.BackColor = System.Drawing.Color.DimGray;
            this.panelDoor9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelDoor9.Controls.Add(this.lb_door9);
            this.panelDoor9.ForeColor = System.Drawing.Color.White;
            this.panelDoor9.Location = new System.Drawing.Point(150, 344);
            this.panelDoor9.Name = "panelDoor9";
            this.panelDoor9.Size = new System.Drawing.Size(67, 33);
            this.panelDoor9.TabIndex = 16;
            // 
            // lb_door9
            // 
            this.lb_door9.AutoSize = true;
            this.lb_door9.BackColor = System.Drawing.Color.Transparent;
            this.lb_door9.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_door9.ForeColor = System.Drawing.Color.White;
            this.lb_door9.Location = new System.Drawing.Point(11, 9);
            this.lb_door9.Name = "lb_door9";
            this.lb_door9.Size = new System.Drawing.Size(43, 12);
            this.lb_door9.TabIndex = 4;
            this.lb_door9.Text = "도어 9";
            // 
            // panelLdBox
            // 
            this.panelLdBox.BackColor = System.Drawing.Color.DimGray;
            this.panelLdBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelLdBox.Controls.Add(this.lb_ld_box);
            this.panelLdBox.ForeColor = System.Drawing.Color.White;
            this.panelLdBox.Location = new System.Drawing.Point(564, 3);
            this.panelLdBox.Name = "panelLdBox";
            this.panelLdBox.Size = new System.Drawing.Size(125, 33);
            this.panelLdBox.TabIndex = 16;
            // 
            // lb_ld_box
            // 
            this.lb_ld_box.AutoSize = true;
            this.lb_ld_box.BackColor = System.Drawing.Color.Transparent;
            this.lb_ld_box.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld_box.ForeColor = System.Drawing.Color.White;
            this.lb_ld_box.Location = new System.Drawing.Point(18, 9);
            this.lb_ld_box.Name = "lb_ld_box";
            this.lb_ld_box.Size = new System.Drawing.Size(88, 12);
            this.lb_ld_box.TabIndex = 4;
            this.lb_ld_box.Text = "로더 전장박스";
            // 
            // panelEms3
            // 
            this.panelEms3.BackColor = System.Drawing.Color.DimGray;
            this.panelEms3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelEms3.Controls.Add(this.lb_ems3);
            this.panelEms3.ForeColor = System.Drawing.Color.White;
            this.panelEms3.Location = new System.Drawing.Point(488, 3);
            this.panelEms3.Name = "panelEms3";
            this.panelEms3.Size = new System.Drawing.Size(55, 33);
            this.panelEms3.TabIndex = 16;
            // 
            // lb_ems3
            // 
            this.lb_ems3.AutoSize = true;
            this.lb_ems3.BackColor = System.Drawing.Color.Transparent;
            this.lb_ems3.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ems3.ForeColor = System.Drawing.Color.White;
            this.lb_ems3.Location = new System.Drawing.Point(4, 9);
            this.lb_ems3.Name = "lb_ems3";
            this.lb_ems3.Size = new System.Drawing.Size(47, 12);
            this.lb_ems3.TabIndex = 4;
            this.lb_ems3.Text = "EMS 3";
            // 
            // panelProcessBox
            // 
            this.panelProcessBox.BackColor = System.Drawing.Color.DimGray;
            this.panelProcessBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelProcessBox.Controls.Add(this.lb_process_box);
            this.panelProcessBox.ForeColor = System.Drawing.Color.White;
            this.panelProcessBox.Location = new System.Drawing.Point(341, 3);
            this.panelProcessBox.Name = "panelProcessBox";
            this.panelProcessBox.Size = new System.Drawing.Size(125, 33);
            this.panelProcessBox.TabIndex = 15;
            // 
            // lb_process_box
            // 
            this.lb_process_box.AutoSize = true;
            this.lb_process_box.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_box.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_box.ForeColor = System.Drawing.Color.White;
            this.lb_process_box.Location = new System.Drawing.Point(6, 9);
            this.lb_process_box.Name = "lb_process_box";
            this.lb_process_box.Size = new System.Drawing.Size(114, 12);
            this.lb_process_box.TabIndex = 4;
            this.lb_process_box.Text = "프로세스 전장박스";
            // 
            // panelEms5
            // 
            this.panelEms5.BackColor = System.Drawing.Color.DimGray;
            this.panelEms5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelEms5.Controls.Add(this.lb_ems5);
            this.panelEms5.ForeColor = System.Drawing.Color.White;
            this.panelEms5.Location = new System.Drawing.Point(263, 3);
            this.panelEms5.Name = "panelEms5";
            this.panelEms5.Size = new System.Drawing.Size(55, 33);
            this.panelEms5.TabIndex = 15;
            // 
            // lb_ems5
            // 
            this.lb_ems5.AutoSize = true;
            this.lb_ems5.BackColor = System.Drawing.Color.Transparent;
            this.lb_ems5.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ems5.ForeColor = System.Drawing.Color.White;
            this.lb_ems5.Location = new System.Drawing.Point(3, 9);
            this.lb_ems5.Name = "lb_ems5";
            this.lb_ems5.Size = new System.Drawing.Size(47, 12);
            this.lb_ems5.TabIndex = 4;
            this.lb_ems5.Text = "EMS 5";
            // 
            // panelUldBox
            // 
            this.panelUldBox.BackColor = System.Drawing.Color.DimGray;
            this.panelUldBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelUldBox.Controls.Add(this.lb_uld_box);
            this.panelUldBox.ForeColor = System.Drawing.Color.White;
            this.panelUldBox.Location = new System.Drawing.Point(111, 3);
            this.panelUldBox.Name = "panelUldBox";
            this.panelUldBox.Size = new System.Drawing.Size(125, 33);
            this.panelUldBox.TabIndex = 14;
            // 
            // lb_uld_box
            // 
            this.lb_uld_box.AutoSize = true;
            this.lb_uld_box.BackColor = System.Drawing.Color.Transparent;
            this.lb_uld_box.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld_box.ForeColor = System.Drawing.Color.White;
            this.lb_uld_box.Location = new System.Drawing.Point(12, 9);
            this.lb_uld_box.Name = "lb_uld_box";
            this.lb_uld_box.Size = new System.Drawing.Size(101, 12);
            this.lb_uld_box.TabIndex = 4;
            this.lb_uld_box.Text = "언로더 전장박스";
            // 
            // panel_run2
            // 
            this.panel_run2.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_run2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_run2.Controls.Add(this.btnUldLight);
            this.panel_run2.Controls.Add(this.btnProcessLight);
            this.panel_run2.Controls.Add(this.btnLdLight);
            this.panel_run2.Controls.Add(this.panelEms1);
            this.panel_run2.Controls.Add(this.panelTargetAvgpower);
            this.panel_run2.Controls.Add(this.panelZposGap);
            this.panel_run2.Controls.Add(this.panelEms4);
            this.panel_run2.Controls.Add(this.panelLdOutB);
            this.panel_run2.Controls.Add(this.panelLdInB);
            this.panel_run2.Controls.Add(this.panelLdOutA);
            this.panel_run2.Controls.Add(this.panelLdInA);
            this.panel_run2.Controls.Add(this.panelUldInB);
            this.panel_run2.Controls.Add(this.panelUldOutB);
            this.panel_run2.Controls.Add(this.panelUldInA);
            this.panel_run2.Controls.Add(this.panelUldOutA);
            this.panel_run2.Controls.Add(this.panelUld1Out2);
            this.panel_run2.Controls.Add(this.panelUld2Out2);
            this.panel_run2.Controls.Add(this.panelUld1Table);
            this.panel_run2.Controls.Add(this.panelUld2Table);
            this.panel_run2.Controls.Add(this.panelUld1Out1);
            this.panel_run2.Controls.Add(this.panelUld2Out1);
            this.panel_run2.Controls.Add(this.panelUld1);
            this.panel_run2.Controls.Add(this.panelUld2);
            this.panel_run2.Controls.Add(this.panelUldTransA1);
            this.panel_run2.Controls.Add(this.panelUldTransB1);
            this.panel_run2.Controls.Add(this.panelUldTransA2);
            this.panel_run2.Controls.Add(this.panel_uld_trans_b2);
            this.panel_run2.Controls.Add(this.panelBreakTableA1);
            this.panel_run2.Controls.Add(this.panelBreakTableB1);
            this.panel_run2.Controls.Add(this.panelBreakTrans1);
            this.panel_run2.Controls.Add(this.panelBreakTrans2);
            this.panel_run2.Controls.Add(this.panelBreakTableA2);
            this.panel_run2.Controls.Add(this.panel_break_table_b2);
            this.panel_run2.Controls.Add(this.panelProcessTableA1);
            this.panel_run2.Controls.Add(this.panelProcessTableB1);
            this.panel_run2.Controls.Add(this.panelProcessTableA2);
            this.panel_run2.Controls.Add(this.panelLd_Trans1);
            this.panel_run2.Controls.Add(this.panelProcessTableB2);
            this.panel_run2.Controls.Add(this.panelLd1);
            this.panel_run2.Controls.Add(this.panelLdTrans2);
            this.panel_run2.Controls.Add(this.panelLd1Table);
            this.panel_run2.Controls.Add(this.panelLd2);
            this.panel_run2.Controls.Add(this.panelL1In2);
            this.panel_run2.Controls.Add(this.panelLd2Table);
            this.panel_run2.Controls.Add(this.panelLd1In1);
            this.panel_run2.Controls.Add(this.panelLd2In2);
            this.panel_run2.Controls.Add(this.panelLd2In1);
            this.panel_run2.Controls.Add(this.lbLd1OTime);
            this.panel_run2.Controls.Add(this.lb_ld1_o);
            this.panel_run2.Controls.Add(this.lbLd1ITime);
            this.panel_run2.Controls.Add(this.lb_ld1_i);
            this.panel_run2.Controls.Add(this.lbLd2OTime);
            this.panel_run2.Controls.Add(this.lb_ld2_o);
            this.panel_run2.Controls.Add(this.lbLd2ITime);
            this.panel_run2.Controls.Add(this.lb_ld2_i);
            this.panel_run2.Controls.Add(this.lbUld1ITime);
            this.panel_run2.Controls.Add(this.lb_uld1_i);
            this.panel_run2.Controls.Add(this.lbUld1OTime);
            this.panel_run2.Controls.Add(this.lb_uld1_o);
            this.panel_run2.Controls.Add(this.lbUld2ITime);
            this.panel_run2.Controls.Add(this.lb_uld2_i);
            this.panel_run2.Controls.Add(this.lbUld2OTime);
            this.panel_run2.Controls.Add(this.lb_uld2_o);
            this.panel_run2.Controls.Add(this.panelShutterOpen);
            this.panel_run2.Controls.Add(this.panelLock);
            this.panel_run2.Controls.Add(this.panelLaserCover);
            this.panel_run2.Controls.Add(this.panelEms2);
            this.panel_run2.Controls.Add(this.panelEms6);
            this.panel_run2.Controls.Add(this.panelGrabEms1);
            this.panel_run2.Controls.Add(this.panelGrabEms2);
            this.panel_run2.Controls.Add(this.panelGrabSwitch1);
            this.panel_run2.Controls.Add(this.panelGrabSwitch2);
            this.panel_run2.Controls.Add(this.panelGrabEms3);
            this.panel_run2.Controls.Add(this.panelGrabSwitch3);
            this.panel_run2.Location = new System.Drawing.Point(46, 41);
            this.panel_run2.Name = "panel_run2";
            this.panel_run2.Size = new System.Drawing.Size(793, 300);
            this.panel_run2.TabIndex = 22;
            // 
            // btnUldLight
            // 
            this.btnUldLight.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnUldLight.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.btnUldLight.ForeColor = System.Drawing.Color.White;
            this.btnUldLight.Location = new System.Drawing.Point(134, 219);
            this.btnUldLight.Name = "btnUldLight";
            this.btnUldLight.Size = new System.Drawing.Size(125, 36);
            this.btnUldLight.TabIndex = 62;
            this.btnUldLight.Text = "언로더부 조명";
            this.btnUldLight.UseVisualStyleBackColor = false;
            // 
            // btnProcessLight
            // 
            this.btnProcessLight.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnProcessLight.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.btnProcessLight.ForeColor = System.Drawing.Color.White;
            this.btnProcessLight.Location = new System.Drawing.Point(327, 220);
            this.btnProcessLight.Name = "btnProcessLight";
            this.btnProcessLight.Size = new System.Drawing.Size(125, 36);
            this.btnProcessLight.TabIndex = 61;
            this.btnProcessLight.Text = "가공부 조명";
            this.btnProcessLight.UseVisualStyleBackColor = false;
            // 
            // btnLdLight
            // 
            this.btnLdLight.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnLdLight.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.btnLdLight.ForeColor = System.Drawing.Color.White;
            this.btnLdLight.Location = new System.Drawing.Point(529, 219);
            this.btnLdLight.Name = "btnLdLight";
            this.btnLdLight.Size = new System.Drawing.Size(125, 36);
            this.btnLdLight.TabIndex = 60;
            this.btnLdLight.Text = "로더부 조명";
            this.btnLdLight.UseVisualStyleBackColor = false;
            // 
            // panelEms1
            // 
            this.panelEms1.BackColor = System.Drawing.Color.DimGray;
            this.panelEms1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelEms1.Controls.Add(this.lb_ems1);
            this.panelEms1.ForeColor = System.Drawing.Color.White;
            this.panelEms1.Location = new System.Drawing.Point(528, 260);
            this.panelEms1.Name = "panelEms1";
            this.panelEms1.Size = new System.Drawing.Size(55, 35);
            this.panelEms1.TabIndex = 18;
            // 
            // lb_ems1
            // 
            this.lb_ems1.AutoSize = true;
            this.lb_ems1.BackColor = System.Drawing.Color.Transparent;
            this.lb_ems1.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ems1.ForeColor = System.Drawing.Color.White;
            this.lb_ems1.Location = new System.Drawing.Point(3, 10);
            this.lb_ems1.Name = "lb_ems1";
            this.lb_ems1.Size = new System.Drawing.Size(47, 12);
            this.lb_ems1.TabIndex = 4;
            this.lb_ems1.Text = "EMS 1";
            // 
            // panelTargetAvgpower
            // 
            this.panelTargetAvgpower.BackColor = System.Drawing.Color.DimGray;
            this.panelTargetAvgpower.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelTargetAvgpower.Controls.Add(this.lbAvgpower);
            this.panelTargetAvgpower.Controls.Add(this.lb_avgpower_info);
            this.panelTargetAvgpower.Controls.Add(this.lbTartget);
            this.panelTargetAvgpower.Controls.Add(this.lb_target_info);
            this.panelTargetAvgpower.ForeColor = System.Drawing.Color.White;
            this.panelTargetAvgpower.Location = new System.Drawing.Point(397, 260);
            this.panelTargetAvgpower.Name = "panelTargetAvgpower";
            this.panelTargetAvgpower.Size = new System.Drawing.Size(125, 36);
            this.panelTargetAvgpower.TabIndex = 16;
            // 
            // lbAvgpower
            // 
            this.lbAvgpower.AutoSize = true;
            this.lbAvgpower.BackColor = System.Drawing.Color.Transparent;
            this.lbAvgpower.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbAvgpower.ForeColor = System.Drawing.Color.White;
            this.lbAvgpower.Location = new System.Drawing.Point(99, 19);
            this.lbAvgpower.Name = "lbAvgpower";
            this.lbAvgpower.Size = new System.Drawing.Size(12, 12);
            this.lbAvgpower.TabIndex = 7;
            this.lbAvgpower.Text = "0";
            // 
            // lb_avgpower_info
            // 
            this.lb_avgpower_info.AutoSize = true;
            this.lb_avgpower_info.BackColor = System.Drawing.Color.Transparent;
            this.lb_avgpower_info.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_avgpower_info.ForeColor = System.Drawing.Color.White;
            this.lb_avgpower_info.Location = new System.Drawing.Point(0, 19);
            this.lb_avgpower_info.Name = "lb_avgpower_info";
            this.lb_avgpower_info.Size = new System.Drawing.Size(100, 12);
            this.lb_avgpower_info.TabIndex = 6;
            this.lb_avgpower_info.Text = "AvgPower[w]:";
            // 
            // lbTartget
            // 
            this.lbTartget.AutoSize = true;
            this.lbTartget.BackColor = System.Drawing.Color.Transparent;
            this.lbTartget.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbTartget.ForeColor = System.Drawing.Color.White;
            this.lbTartget.Location = new System.Drawing.Point(74, 4);
            this.lbTartget.Name = "lbTartget";
            this.lbTartget.Size = new System.Drawing.Size(12, 12);
            this.lbTartget.TabIndex = 5;
            this.lbTartget.Text = "0";
            // 
            // lb_target_info
            // 
            this.lb_target_info.AutoSize = true;
            this.lb_target_info.BackColor = System.Drawing.Color.Transparent;
            this.lb_target_info.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_target_info.ForeColor = System.Drawing.Color.White;
            this.lb_target_info.Location = new System.Drawing.Point(-1, 4);
            this.lb_target_info.Name = "lb_target_info";
            this.lb_target_info.Size = new System.Drawing.Size(77, 12);
            this.lb_target_info.TabIndex = 4;
            this.lb_target_info.Text = "Target[w]:";
            // 
            // panelZposGap
            // 
            this.panelZposGap.BackColor = System.Drawing.Color.DimGray;
            this.panelZposGap.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelZposGap.Controls.Add(this.lbZposGap);
            this.panelZposGap.Controls.Add(this.lb_zpos_gap_info);
            this.panelZposGap.ForeColor = System.Drawing.Color.White;
            this.panelZposGap.Location = new System.Drawing.Point(266, 260);
            this.panelZposGap.Name = "panelZposGap";
            this.panelZposGap.Size = new System.Drawing.Size(125, 36);
            this.panelZposGap.TabIndex = 15;
            // 
            // lbZposGap
            // 
            this.lbZposGap.AutoSize = true;
            this.lbZposGap.BackColor = System.Drawing.Color.Transparent;
            this.lbZposGap.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbZposGap.ForeColor = System.Drawing.Color.White;
            this.lbZposGap.Location = new System.Drawing.Point(80, 10);
            this.lbZposGap.Name = "lbZposGap";
            this.lbZposGap.Size = new System.Drawing.Size(12, 12);
            this.lbZposGap.TabIndex = 5;
            this.lbZposGap.Text = "0";
            // 
            // lb_zpos_gap_info
            // 
            this.lb_zpos_gap_info.AutoSize = true;
            this.lb_zpos_gap_info.BackColor = System.Drawing.Color.Transparent;
            this.lb_zpos_gap_info.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_zpos_gap_info.ForeColor = System.Drawing.Color.White;
            this.lb_zpos_gap_info.Location = new System.Drawing.Point(-1, 10);
            this.lb_zpos_gap_info.Name = "lb_zpos_gap_info";
            this.lb_zpos_gap_info.Size = new System.Drawing.Size(80, 12);
            this.lb_zpos_gap_info.TabIndex = 4;
            this.lb_zpos_gap_info.Text = "ZPos Gap :";
            // 
            // panelEms4
            // 
            this.panelEms4.BackColor = System.Drawing.Color.DimGray;
            this.panelEms4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelEms4.Controls.Add(this.lb_ems4);
            this.panelEms4.ForeColor = System.Drawing.Color.White;
            this.panelEms4.Location = new System.Drawing.Point(205, 260);
            this.panelEms4.Name = "panelEms4";
            this.panelEms4.Size = new System.Drawing.Size(55, 35);
            this.panelEms4.TabIndex = 17;
            // 
            // lb_ems4
            // 
            this.lb_ems4.AutoSize = true;
            this.lb_ems4.BackColor = System.Drawing.Color.Transparent;
            this.lb_ems4.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ems4.ForeColor = System.Drawing.Color.White;
            this.lb_ems4.Location = new System.Drawing.Point(3, 10);
            this.lb_ems4.Name = "lb_ems4";
            this.lb_ems4.Size = new System.Drawing.Size(47, 12);
            this.lb_ems4.TabIndex = 4;
            this.lb_ems4.Text = "EMS 4";
            // 
            // panelLdOutB
            // 
            this.panelLdOutB.BackColor = System.Drawing.Color.DimGray;
            this.panelLdOutB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelLdOutB.Controls.Add(this.lb_ld_out_b);
            this.panelLdOutB.ForeColor = System.Drawing.Color.White;
            this.panelLdOutB.Location = new System.Drawing.Point(753, 260);
            this.panelLdOutB.Name = "panelLdOutB";
            this.panelLdOutB.Size = new System.Drawing.Size(35, 35);
            this.panelLdOutB.TabIndex = 56;
            // 
            // lb_ld_out_b
            // 
            this.lb_ld_out_b.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld_out_b.ForeColor = System.Drawing.Color.White;
            this.lb_ld_out_b.Location = new System.Drawing.Point(2, 1);
            this.lb_ld_out_b.Name = "lb_ld_out_b";
            this.lb_ld_out_b.Size = new System.Drawing.Size(32, 33);
            this.lb_ld_out_b.TabIndex = 1;
            this.lb_ld_out_b.Text = "배출B";
            this.lb_ld_out_b.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelLdInB
            // 
            this.panelLdInB.BackColor = System.Drawing.Color.DimGray;
            this.panelLdInB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelLdInB.Controls.Add(this.lb_ld_in_b);
            this.panelLdInB.ForeColor = System.Drawing.Color.White;
            this.panelLdInB.Location = new System.Drawing.Point(753, 222);
            this.panelLdInB.Name = "panelLdInB";
            this.panelLdInB.Size = new System.Drawing.Size(35, 35);
            this.panelLdInB.TabIndex = 54;
            // 
            // lb_ld_in_b
            // 
            this.lb_ld_in_b.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld_in_b.ForeColor = System.Drawing.Color.White;
            this.lb_ld_in_b.Location = new System.Drawing.Point(2, 1);
            this.lb_ld_in_b.Name = "lb_ld_in_b";
            this.lb_ld_in_b.Size = new System.Drawing.Size(32, 33);
            this.lb_ld_in_b.TabIndex = 1;
            this.lb_ld_in_b.Text = "투입B";
            this.lb_ld_in_b.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelLdOutA
            // 
            this.panelLdOutA.BackColor = System.Drawing.Color.DimGray;
            this.panelLdOutA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelLdOutA.Controls.Add(this.lb_ld_out_a);
            this.panelLdOutA.ForeColor = System.Drawing.Color.White;
            this.panelLdOutA.Location = new System.Drawing.Point(715, 260);
            this.panelLdOutA.Name = "panelLdOutA";
            this.panelLdOutA.Size = new System.Drawing.Size(35, 35);
            this.panelLdOutA.TabIndex = 55;
            // 
            // lb_ld_out_a
            // 
            this.lb_ld_out_a.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld_out_a.ForeColor = System.Drawing.Color.White;
            this.lb_ld_out_a.Location = new System.Drawing.Point(2, 1);
            this.lb_ld_out_a.Name = "lb_ld_out_a";
            this.lb_ld_out_a.Size = new System.Drawing.Size(32, 33);
            this.lb_ld_out_a.TabIndex = 1;
            this.lb_ld_out_a.Text = "배출A";
            this.lb_ld_out_a.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelLdInA
            // 
            this.panelLdInA.BackColor = System.Drawing.Color.DimGray;
            this.panelLdInA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelLdInA.Controls.Add(this.lb_ld_in_a);
            this.panelLdInA.ForeColor = System.Drawing.Color.White;
            this.panelLdInA.Location = new System.Drawing.Point(715, 222);
            this.panelLdInA.Name = "panelLdInA";
            this.panelLdInA.Size = new System.Drawing.Size(35, 35);
            this.panelLdInA.TabIndex = 53;
            // 
            // lb_ld_in_a
            // 
            this.lb_ld_in_a.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld_in_a.ForeColor = System.Drawing.Color.White;
            this.lb_ld_in_a.Location = new System.Drawing.Point(2, 1);
            this.lb_ld_in_a.Name = "lb_ld_in_a";
            this.lb_ld_in_a.Size = new System.Drawing.Size(32, 33);
            this.lb_ld_in_a.TabIndex = 1;
            this.lb_ld_in_a.Text = "투입A";
            this.lb_ld_in_a.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelUldInB
            // 
            this.panelUldInB.BackColor = System.Drawing.Color.DimGray;
            this.panelUldInB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelUldInB.Controls.Add(this.lb_uld_in_b);
            this.panelUldInB.ForeColor = System.Drawing.Color.White;
            this.panelUldInB.Location = new System.Drawing.Point(41, 260);
            this.panelUldInB.Name = "panelUldInB";
            this.panelUldInB.Size = new System.Drawing.Size(35, 35);
            this.panelUldInB.TabIndex = 19;
            // 
            // lb_uld_in_b
            // 
            this.lb_uld_in_b.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld_in_b.ForeColor = System.Drawing.Color.White;
            this.lb_uld_in_b.Location = new System.Drawing.Point(2, 1);
            this.lb_uld_in_b.Name = "lb_uld_in_b";
            this.lb_uld_in_b.Size = new System.Drawing.Size(32, 33);
            this.lb_uld_in_b.TabIndex = 1;
            this.lb_uld_in_b.Text = "투입B";
            this.lb_uld_in_b.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelUldOutB
            // 
            this.panelUldOutB.BackColor = System.Drawing.Color.DimGray;
            this.panelUldOutB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelUldOutB.Controls.Add(this.lb_uld_out_b);
            this.panelUldOutB.ForeColor = System.Drawing.Color.White;
            this.panelUldOutB.Location = new System.Drawing.Point(41, 222);
            this.panelUldOutB.Name = "panelUldOutB";
            this.panelUldOutB.Size = new System.Drawing.Size(35, 35);
            this.panelUldOutB.TabIndex = 17;
            // 
            // lb_uld_out_b
            // 
            this.lb_uld_out_b.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld_out_b.ForeColor = System.Drawing.Color.White;
            this.lb_uld_out_b.Location = new System.Drawing.Point(2, 1);
            this.lb_uld_out_b.Name = "lb_uld_out_b";
            this.lb_uld_out_b.Size = new System.Drawing.Size(32, 33);
            this.lb_uld_out_b.TabIndex = 1;
            this.lb_uld_out_b.Text = "배출B";
            this.lb_uld_out_b.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelUldInA
            // 
            this.panelUldInA.BackColor = System.Drawing.Color.DimGray;
            this.panelUldInA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelUldInA.Controls.Add(this.lb_uld_in_a);
            this.panelUldInA.ForeColor = System.Drawing.Color.White;
            this.panelUldInA.Location = new System.Drawing.Point(3, 260);
            this.panelUldInA.Name = "panelUldInA";
            this.panelUldInA.Size = new System.Drawing.Size(35, 35);
            this.panelUldInA.TabIndex = 18;
            // 
            // lb_uld_in_a
            // 
            this.lb_uld_in_a.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld_in_a.ForeColor = System.Drawing.Color.White;
            this.lb_uld_in_a.Location = new System.Drawing.Point(2, 1);
            this.lb_uld_in_a.Name = "lb_uld_in_a";
            this.lb_uld_in_a.Size = new System.Drawing.Size(32, 33);
            this.lb_uld_in_a.TabIndex = 1;
            this.lb_uld_in_a.Text = "투입A";
            this.lb_uld_in_a.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelUldOutA
            // 
            this.panelUldOutA.BackColor = System.Drawing.Color.DimGray;
            this.panelUldOutA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelUldOutA.Controls.Add(this.lb_uld_out_a);
            this.panelUldOutA.ForeColor = System.Drawing.Color.White;
            this.panelUldOutA.Location = new System.Drawing.Point(3, 222);
            this.panelUldOutA.Name = "panelUldOutA";
            this.panelUldOutA.Size = new System.Drawing.Size(35, 35);
            this.panelUldOutA.TabIndex = 16;
            // 
            // lb_uld_out_a
            // 
            this.lb_uld_out_a.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld_out_a.ForeColor = System.Drawing.Color.White;
            this.lb_uld_out_a.Location = new System.Drawing.Point(2, 1);
            this.lb_uld_out_a.Name = "lb_uld_out_a";
            this.lb_uld_out_a.Size = new System.Drawing.Size(32, 33);
            this.lb_uld_out_a.TabIndex = 1;
            this.lb_uld_out_a.Text = "배출A";
            this.lb_uld_out_a.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelUld1Out2
            // 
            this.panelUld1Out2.BackColor = System.Drawing.Color.DimGray;
            this.panelUld1Out2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelUld1Out2.ForeColor = System.Drawing.Color.White;
            this.panelUld1Out2.Location = new System.Drawing.Point(96, 174);
            this.panelUld1Out2.Name = "panelUld1Out2";
            this.panelUld1Out2.Size = new System.Drawing.Size(20, 20);
            this.panelUld1Out2.TabIndex = 48;
            // 
            // panelUld2Out2
            // 
            this.panelUld2Out2.BackColor = System.Drawing.Color.DimGray;
            this.panelUld2Out2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelUld2Out2.ForeColor = System.Drawing.Color.White;
            this.panelUld2Out2.Location = new System.Drawing.Point(96, 98);
            this.panelUld2Out2.Name = "panelUld2Out2";
            this.panelUld2Out2.Size = new System.Drawing.Size(20, 20);
            this.panelUld2Out2.TabIndex = 23;
            // 
            // panelUld1Table
            // 
            this.panelUld1Table.BackColor = System.Drawing.Color.DimGray;
            this.panelUld1Table.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelUld1Table.ForeColor = System.Drawing.Color.White;
            this.panelUld1Table.Location = new System.Drawing.Point(121, 148);
            this.panelUld1Table.Name = "panelUld1Table";
            this.panelUld1Table.Size = new System.Drawing.Size(15, 47);
            this.panelUld1Table.TabIndex = 44;
            // 
            // panelUld2Table
            // 
            this.panelUld2Table.BackColor = System.Drawing.Color.DimGray;
            this.panelUld2Table.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelUld2Table.ForeColor = System.Drawing.Color.White;
            this.panelUld2Table.Location = new System.Drawing.Point(121, 72);
            this.panelUld2Table.Name = "panelUld2Table";
            this.panelUld2Table.Size = new System.Drawing.Size(15, 47);
            this.panelUld2Table.TabIndex = 22;
            // 
            // panelUld1Out1
            // 
            this.panelUld1Out1.BackColor = System.Drawing.Color.DimGray;
            this.panelUld1Out1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelUld1Out1.ForeColor = System.Drawing.Color.White;
            this.panelUld1Out1.Location = new System.Drawing.Point(96, 148);
            this.panelUld1Out1.Name = "panelUld1Out1";
            this.panelUld1Out1.Size = new System.Drawing.Size(20, 20);
            this.panelUld1Out1.TabIndex = 45;
            // 
            // panelUld2Out1
            // 
            this.panelUld2Out1.BackColor = System.Drawing.Color.DimGray;
            this.panelUld2Out1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelUld2Out1.ForeColor = System.Drawing.Color.White;
            this.panelUld2Out1.Location = new System.Drawing.Point(96, 72);
            this.panelUld2Out1.Name = "panelUld2Out1";
            this.panelUld2Out1.Size = new System.Drawing.Size(20, 20);
            this.panelUld2Out1.TabIndex = 22;
            // 
            // panelUld1
            // 
            this.panelUld1.BackColor = System.Drawing.Color.DimGray;
            this.panelUld1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelUld1.Controls.Add(this.lb_uld1);
            this.panelUld1.ForeColor = System.Drawing.Color.White;
            this.panelUld1.Location = new System.Drawing.Point(141, 154);
            this.panelUld1.Name = "panelUld1";
            this.panelUld1.Size = new System.Drawing.Size(67, 35);
            this.panelUld1.TabIndex = 46;
            // 
            // lb_uld1
            // 
            this.lb_uld1.AutoSize = true;
            this.lb_uld1.BackColor = System.Drawing.Color.Transparent;
            this.lb_uld1.Font = new System.Drawing.Font("Gulim", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld1.ForeColor = System.Drawing.Color.White;
            this.lb_uld1.Location = new System.Drawing.Point(6, 11);
            this.lb_uld1.Name = "lb_uld1";
            this.lb_uld1.Size = new System.Drawing.Size(53, 11);
            this.lb_uld1.TabIndex = 5;
            this.lb_uld1.Text = "언로더 1";
            // 
            // panelUld2
            // 
            this.panelUld2.BackColor = System.Drawing.Color.DimGray;
            this.panelUld2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelUld2.Controls.Add(this.lb_uld2);
            this.panelUld2.ForeColor = System.Drawing.Color.White;
            this.panelUld2.Location = new System.Drawing.Point(141, 78);
            this.panelUld2.Name = "panelUld2";
            this.panelUld2.Size = new System.Drawing.Size(67, 35);
            this.panelUld2.TabIndex = 22;
            // 
            // lb_uld2
            // 
            this.lb_uld2.AutoSize = true;
            this.lb_uld2.BackColor = System.Drawing.Color.Transparent;
            this.lb_uld2.Font = new System.Drawing.Font("Gulim", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld2.ForeColor = System.Drawing.Color.White;
            this.lb_uld2.Location = new System.Drawing.Point(6, 11);
            this.lb_uld2.Name = "lb_uld2";
            this.lb_uld2.Size = new System.Drawing.Size(53, 11);
            this.lb_uld2.TabIndex = 5;
            this.lb_uld2.Text = "언로더 2";
            // 
            // panelUldTransA1
            // 
            this.panelUldTransA1.BackColor = System.Drawing.Color.DimGray;
            this.panelUldTransA1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelUldTransA1.Controls.Add(this.lb_uld_trans_a1);
            this.panelUldTransA1.ForeColor = System.Drawing.Color.White;
            this.panelUldTransA1.Location = new System.Drawing.Point(214, 178);
            this.panelUldTransA1.Name = "panelUldTransA1";
            this.panelUldTransA1.Size = new System.Drawing.Size(67, 35);
            this.panelUldTransA1.TabIndex = 52;
            // 
            // lb_uld_trans_a1
            // 
            this.lb_uld_trans_a1.BackColor = System.Drawing.Color.Transparent;
            this.lb_uld_trans_a1.Font = new System.Drawing.Font("Gulim", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld_trans_a1.ForeColor = System.Drawing.Color.White;
            this.lb_uld_trans_a1.Location = new System.Drawing.Point(-5, -1);
            this.lb_uld_trans_a1.Name = "lb_uld_trans_a1";
            this.lb_uld_trans_a1.Size = new System.Drawing.Size(76, 36);
            this.lb_uld_trans_a1.TabIndex = 4;
            this.lb_uld_trans_a1.Text = "  언로더   트랜스퍼A1";
            this.lb_uld_trans_a1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelUldTransB1
            // 
            this.panelUldTransB1.BackColor = System.Drawing.Color.DimGray;
            this.panelUldTransB1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelUldTransB1.Controls.Add(this.lb_uld_trans_b1);
            this.panelUldTransB1.ForeColor = System.Drawing.Color.White;
            this.panelUldTransB1.Location = new System.Drawing.Point(214, 102);
            this.panelUldTransB1.Name = "panelUldTransB1";
            this.panelUldTransB1.Size = new System.Drawing.Size(67, 35);
            this.panelUldTransB1.TabIndex = 36;
            // 
            // lb_uld_trans_b1
            // 
            this.lb_uld_trans_b1.BackColor = System.Drawing.Color.Transparent;
            this.lb_uld_trans_b1.Font = new System.Drawing.Font("Gulim", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld_trans_b1.ForeColor = System.Drawing.Color.White;
            this.lb_uld_trans_b1.Location = new System.Drawing.Point(-5, -1);
            this.lb_uld_trans_b1.Name = "lb_uld_trans_b1";
            this.lb_uld_trans_b1.Size = new System.Drawing.Size(76, 36);
            this.lb_uld_trans_b1.TabIndex = 4;
            this.lb_uld_trans_b1.Text = "  언로더   트랜스퍼B1";
            this.lb_uld_trans_b1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelUldTransA2
            // 
            this.panelUldTransA2.BackColor = System.Drawing.Color.DimGray;
            this.panelUldTransA2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelUldTransA2.Controls.Add(this.lb_uld_trans_a2);
            this.panelUldTransA2.ForeColor = System.Drawing.Color.White;
            this.panelUldTransA2.Location = new System.Drawing.Point(214, 140);
            this.panelUldTransA2.Name = "panelUldTransA2";
            this.panelUldTransA2.Size = new System.Drawing.Size(67, 35);
            this.panelUldTransA2.TabIndex = 51;
            // 
            // lb_uld_trans_a2
            // 
            this.lb_uld_trans_a2.BackColor = System.Drawing.Color.Transparent;
            this.lb_uld_trans_a2.Font = new System.Drawing.Font("Gulim", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld_trans_a2.ForeColor = System.Drawing.Color.White;
            this.lb_uld_trans_a2.Location = new System.Drawing.Point(-5, -1);
            this.lb_uld_trans_a2.Name = "lb_uld_trans_a2";
            this.lb_uld_trans_a2.Size = new System.Drawing.Size(76, 36);
            this.lb_uld_trans_a2.TabIndex = 4;
            this.lb_uld_trans_a2.Text = "  언로더   트랜스퍼A2";
            this.lb_uld_trans_a2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_uld_trans_b2
            // 
            this.panel_uld_trans_b2.BackColor = System.Drawing.Color.DimGray;
            this.panel_uld_trans_b2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_uld_trans_b2.Controls.Add(this.lbUldTransB2);
            this.panel_uld_trans_b2.ForeColor = System.Drawing.Color.White;
            this.panel_uld_trans_b2.Location = new System.Drawing.Point(214, 64);
            this.panel_uld_trans_b2.Name = "panel_uld_trans_b2";
            this.panel_uld_trans_b2.Size = new System.Drawing.Size(67, 35);
            this.panel_uld_trans_b2.TabIndex = 35;
            // 
            // lbUldTransB2
            // 
            this.lbUldTransB2.BackColor = System.Drawing.Color.Transparent;
            this.lbUldTransB2.Font = new System.Drawing.Font("Gulim", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbUldTransB2.ForeColor = System.Drawing.Color.White;
            this.lbUldTransB2.Location = new System.Drawing.Point(-5, -1);
            this.lbUldTransB2.Name = "lbUldTransB2";
            this.lbUldTransB2.Size = new System.Drawing.Size(76, 36);
            this.lbUldTransB2.TabIndex = 4;
            this.lbUldTransB2.Text = "  언로더   트랜스퍼B2";
            this.lbUldTransB2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelBreakTableA1
            // 
            this.panelBreakTableA1.BackColor = System.Drawing.Color.DimGray;
            this.panelBreakTableA1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelBreakTableA1.Controls.Add(this.lb_break_table_a1);
            this.panelBreakTableA1.ForeColor = System.Drawing.Color.White;
            this.panelBreakTableA1.Location = new System.Drawing.Point(287, 178);
            this.panelBreakTableA1.Name = "panelBreakTableA1";
            this.panelBreakTableA1.Size = new System.Drawing.Size(67, 35);
            this.panelBreakTableA1.TabIndex = 50;
            // 
            // lb_break_table_a1
            // 
            this.lb_break_table_a1.BackColor = System.Drawing.Color.Transparent;
            this.lb_break_table_a1.Font = new System.Drawing.Font("Gulim", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_break_table_a1.ForeColor = System.Drawing.Color.White;
            this.lb_break_table_a1.Location = new System.Drawing.Point(-5, -1);
            this.lb_break_table_a1.Name = "lb_break_table_a1";
            this.lb_break_table_a1.Size = new System.Drawing.Size(76, 36);
            this.lb_break_table_a1.TabIndex = 4;
            this.lb_break_table_a1.Text = " 브레이크  테이블 A1";
            this.lb_break_table_a1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelBreakTableB1
            // 
            this.panelBreakTableB1.BackColor = System.Drawing.Color.DimGray;
            this.panelBreakTableB1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelBreakTableB1.Controls.Add(this.lb_break_table_b1);
            this.panelBreakTableB1.ForeColor = System.Drawing.Color.White;
            this.panelBreakTableB1.Location = new System.Drawing.Point(287, 102);
            this.panelBreakTableB1.Name = "panelBreakTableB1";
            this.panelBreakTableB1.Size = new System.Drawing.Size(67, 35);
            this.panelBreakTableB1.TabIndex = 24;
            // 
            // lb_break_table_b1
            // 
            this.lb_break_table_b1.BackColor = System.Drawing.Color.Transparent;
            this.lb_break_table_b1.Font = new System.Drawing.Font("Gulim", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_break_table_b1.ForeColor = System.Drawing.Color.White;
            this.lb_break_table_b1.Location = new System.Drawing.Point(-5, -1);
            this.lb_break_table_b1.Name = "lb_break_table_b1";
            this.lb_break_table_b1.Size = new System.Drawing.Size(76, 36);
            this.lb_break_table_b1.TabIndex = 4;
            this.lb_break_table_b1.Text = " 브레이크  테이블 B1";
            this.lb_break_table_b1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelBreakTrans1
            // 
            this.panelBreakTrans1.BackColor = System.Drawing.Color.DimGray;
            this.panelBreakTrans1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelBreakTrans1.Controls.Add(this.lb_break_trans1);
            this.panelBreakTrans1.ForeColor = System.Drawing.Color.White;
            this.panelBreakTrans1.Location = new System.Drawing.Point(359, 154);
            this.panelBreakTrans1.Name = "panelBreakTrans1";
            this.panelBreakTrans1.Size = new System.Drawing.Size(67, 35);
            this.panelBreakTrans1.TabIndex = 40;
            // 
            // lb_break_trans1
            // 
            this.lb_break_trans1.BackColor = System.Drawing.Color.Transparent;
            this.lb_break_trans1.Font = new System.Drawing.Font("Gulim", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_break_trans1.ForeColor = System.Drawing.Color.White;
            this.lb_break_trans1.Location = new System.Drawing.Point(-6, -1);
            this.lb_break_trans1.Name = "lb_break_trans1";
            this.lb_break_trans1.Size = new System.Drawing.Size(76, 36);
            this.lb_break_trans1.TabIndex = 4;
            this.lb_break_trans1.Text = " 브레이크  트랜스퍼 1";
            this.lb_break_trans1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelBreakTrans2
            // 
            this.panelBreakTrans2.BackColor = System.Drawing.Color.DimGray;
            this.panelBreakTrans2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelBreakTrans2.Controls.Add(this.lb_break_trans2);
            this.panelBreakTrans2.ForeColor = System.Drawing.Color.White;
            this.panelBreakTrans2.Location = new System.Drawing.Point(359, 78);
            this.panelBreakTrans2.Name = "panelBreakTrans2";
            this.panelBreakTrans2.Size = new System.Drawing.Size(67, 35);
            this.panelBreakTrans2.TabIndex = 21;
            // 
            // lb_break_trans2
            // 
            this.lb_break_trans2.BackColor = System.Drawing.Color.Transparent;
            this.lb_break_trans2.Font = new System.Drawing.Font("Gulim", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_break_trans2.ForeColor = System.Drawing.Color.White;
            this.lb_break_trans2.Location = new System.Drawing.Point(-6, -1);
            this.lb_break_trans2.Name = "lb_break_trans2";
            this.lb_break_trans2.Size = new System.Drawing.Size(76, 36);
            this.lb_break_trans2.TabIndex = 4;
            this.lb_break_trans2.Text = " 브레이크  트랜스퍼 2";
            this.lb_break_trans2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelBreakTableA2
            // 
            this.panelBreakTableA2.BackColor = System.Drawing.Color.DimGray;
            this.panelBreakTableA2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelBreakTableA2.Controls.Add(this.lb_break_table_a2);
            this.panelBreakTableA2.ForeColor = System.Drawing.Color.White;
            this.panelBreakTableA2.Location = new System.Drawing.Point(287, 140);
            this.panelBreakTableA2.Name = "panelBreakTableA2";
            this.panelBreakTableA2.Size = new System.Drawing.Size(67, 35);
            this.panelBreakTableA2.TabIndex = 49;
            // 
            // lb_break_table_a2
            // 
            this.lb_break_table_a2.BackColor = System.Drawing.Color.Transparent;
            this.lb_break_table_a2.Font = new System.Drawing.Font("Gulim", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_break_table_a2.ForeColor = System.Drawing.Color.White;
            this.lb_break_table_a2.Location = new System.Drawing.Point(-5, -1);
            this.lb_break_table_a2.Name = "lb_break_table_a2";
            this.lb_break_table_a2.Size = new System.Drawing.Size(76, 36);
            this.lb_break_table_a2.TabIndex = 4;
            this.lb_break_table_a2.Text = " 브레이크  테이블 A2";
            this.lb_break_table_a2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_break_table_b2
            // 
            this.panel_break_table_b2.BackColor = System.Drawing.Color.DimGray;
            this.panel_break_table_b2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_break_table_b2.Controls.Add(this.lbBreakTableB2);
            this.panel_break_table_b2.ForeColor = System.Drawing.Color.White;
            this.panel_break_table_b2.Location = new System.Drawing.Point(287, 64);
            this.panel_break_table_b2.Name = "panel_break_table_b2";
            this.panel_break_table_b2.Size = new System.Drawing.Size(67, 35);
            this.panel_break_table_b2.TabIndex = 23;
            // 
            // lbBreakTableB2
            // 
            this.lbBreakTableB2.BackColor = System.Drawing.Color.Transparent;
            this.lbBreakTableB2.Font = new System.Drawing.Font("Gulim", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbBreakTableB2.ForeColor = System.Drawing.Color.White;
            this.lbBreakTableB2.Location = new System.Drawing.Point(-5, -1);
            this.lbBreakTableB2.Name = "lbBreakTableB2";
            this.lbBreakTableB2.Size = new System.Drawing.Size(76, 36);
            this.lbBreakTableB2.TabIndex = 4;
            this.lbBreakTableB2.Text = " 브레이크  테이블 B2";
            this.lbBreakTableB2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelProcessTableA1
            // 
            this.panelProcessTableA1.BackColor = System.Drawing.Color.DimGray;
            this.panelProcessTableA1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelProcessTableA1.Controls.Add(this.lb_process_table_a1);
            this.panelProcessTableA1.ForeColor = System.Drawing.Color.White;
            this.panelProcessTableA1.Location = new System.Drawing.Point(431, 174);
            this.panelProcessTableA1.Name = "panelProcessTableA1";
            this.panelProcessTableA1.Size = new System.Drawing.Size(67, 35);
            this.panelProcessTableA1.TabIndex = 47;
            // 
            // lb_process_table_a1
            // 
            this.lb_process_table_a1.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_table_a1.Font = new System.Drawing.Font("Gulim", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_table_a1.ForeColor = System.Drawing.Color.White;
            this.lb_process_table_a1.Location = new System.Drawing.Point(-5, -1);
            this.lb_process_table_a1.Name = "lb_process_table_a1";
            this.lb_process_table_a1.Size = new System.Drawing.Size(76, 36);
            this.lb_process_table_a1.TabIndex = 4;
            this.lb_process_table_a1.Text = "   가공     테이블 A1";
            this.lb_process_table_a1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelProcessTableB1
            // 
            this.panelProcessTableB1.BackColor = System.Drawing.Color.DimGray;
            this.panelProcessTableB1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelProcessTableB1.Controls.Add(this.lb_process_table_b1);
            this.panelProcessTableB1.ForeColor = System.Drawing.Color.White;
            this.panelProcessTableB1.Location = new System.Drawing.Point(431, 98);
            this.panelProcessTableB1.Name = "panelProcessTableB1";
            this.panelProcessTableB1.Size = new System.Drawing.Size(67, 35);
            this.panelProcessTableB1.TabIndex = 22;
            // 
            // lb_process_table_b1
            // 
            this.lb_process_table_b1.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_table_b1.Font = new System.Drawing.Font("Gulim", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_table_b1.ForeColor = System.Drawing.Color.White;
            this.lb_process_table_b1.Location = new System.Drawing.Point(-5, -1);
            this.lb_process_table_b1.Name = "lb_process_table_b1";
            this.lb_process_table_b1.Size = new System.Drawing.Size(76, 36);
            this.lb_process_table_b1.TabIndex = 4;
            this.lb_process_table_b1.Text = "   가공     테이블 B1";
            this.lb_process_table_b1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelProcessTableA2
            // 
            this.panelProcessTableA2.BackColor = System.Drawing.Color.DimGray;
            this.panelProcessTableA2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelProcessTableA2.Controls.Add(this.lb_process_table_a2);
            this.panelProcessTableA2.ForeColor = System.Drawing.Color.White;
            this.panelProcessTableA2.Location = new System.Drawing.Point(431, 136);
            this.panelProcessTableA2.Name = "panelProcessTableA2";
            this.panelProcessTableA2.Size = new System.Drawing.Size(67, 35);
            this.panelProcessTableA2.TabIndex = 41;
            // 
            // lb_process_table_a2
            // 
            this.lb_process_table_a2.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_table_a2.Font = new System.Drawing.Font("Gulim", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_table_a2.ForeColor = System.Drawing.Color.White;
            this.lb_process_table_a2.Location = new System.Drawing.Point(-5, -1);
            this.lb_process_table_a2.Name = "lb_process_table_a2";
            this.lb_process_table_a2.Size = new System.Drawing.Size(76, 36);
            this.lb_process_table_a2.TabIndex = 4;
            this.lb_process_table_a2.Text = "   가공     테이블 A2";
            this.lb_process_table_a2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelLd_Trans1
            // 
            this.panelLd_Trans1.BackColor = System.Drawing.Color.DimGray;
            this.panelLd_Trans1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelLd_Trans1.Controls.Add(this.lb_ld_trans1);
            this.panelLd_Trans1.ForeColor = System.Drawing.Color.White;
            this.panelLd_Trans1.Location = new System.Drawing.Point(502, 154);
            this.panelLd_Trans1.Name = "panelLd_Trans1";
            this.panelLd_Trans1.Size = new System.Drawing.Size(67, 35);
            this.panelLd_Trans1.TabIndex = 38;
            // 
            // lb_ld_trans1
            // 
            this.lb_ld_trans1.BackColor = System.Drawing.Color.Transparent;
            this.lb_ld_trans1.Font = new System.Drawing.Font("Gulim", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld_trans1.ForeColor = System.Drawing.Color.White;
            this.lb_ld_trans1.Location = new System.Drawing.Point(-5, -1);
            this.lb_ld_trans1.Name = "lb_ld_trans1";
            this.lb_ld_trans1.Size = new System.Drawing.Size(76, 36);
            this.lb_ld_trans1.TabIndex = 4;
            this.lb_ld_trans1.Text = "   로더     트랜스퍼 1";
            this.lb_ld_trans1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelProcessTableB2
            // 
            this.panelProcessTableB2.BackColor = System.Drawing.Color.DimGray;
            this.panelProcessTableB2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelProcessTableB2.Controls.Add(this.lb_process_table_b2);
            this.panelProcessTableB2.ForeColor = System.Drawing.Color.White;
            this.panelProcessTableB2.Location = new System.Drawing.Point(431, 60);
            this.panelProcessTableB2.Name = "panelProcessTableB2";
            this.panelProcessTableB2.Size = new System.Drawing.Size(67, 35);
            this.panelProcessTableB2.TabIndex = 21;
            // 
            // lb_process_table_b2
            // 
            this.lb_process_table_b2.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_table_b2.Font = new System.Drawing.Font("Gulim", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_table_b2.ForeColor = System.Drawing.Color.White;
            this.lb_process_table_b2.Location = new System.Drawing.Point(-5, -1);
            this.lb_process_table_b2.Name = "lb_process_table_b2";
            this.lb_process_table_b2.Size = new System.Drawing.Size(76, 36);
            this.lb_process_table_b2.TabIndex = 4;
            this.lb_process_table_b2.Text = "   가공     테이블 B2";
            this.lb_process_table_b2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelLd1
            // 
            this.panelLd1.BackColor = System.Drawing.Color.DimGray;
            this.panelLd1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelLd1.Controls.Add(this.lb_ld1);
            this.panelLd1.ForeColor = System.Drawing.Color.White;
            this.panelLd1.Location = new System.Drawing.Point(573, 154);
            this.panelLd1.Name = "panelLd1";
            this.panelLd1.Size = new System.Drawing.Size(67, 35);
            this.panelLd1.TabIndex = 37;
            // 
            // lb_ld1
            // 
            this.lb_ld1.AutoSize = true;
            this.lb_ld1.BackColor = System.Drawing.Color.Transparent;
            this.lb_ld1.Font = new System.Drawing.Font("Gulim", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld1.ForeColor = System.Drawing.Color.White;
            this.lb_ld1.Location = new System.Drawing.Point(11, 11);
            this.lb_ld1.Name = "lb_ld1";
            this.lb_ld1.Size = new System.Drawing.Size(41, 11);
            this.lb_ld1.TabIndex = 4;
            this.lb_ld1.Text = "로더 1";
            // 
            // panelLdTrans2
            // 
            this.panelLdTrans2.BackColor = System.Drawing.Color.DimGray;
            this.panelLdTrans2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelLdTrans2.Controls.Add(this.lb_ld_trans2);
            this.panelLdTrans2.ForeColor = System.Drawing.Color.White;
            this.panelLdTrans2.Location = new System.Drawing.Point(502, 78);
            this.panelLdTrans2.Name = "panelLdTrans2";
            this.panelLdTrans2.Size = new System.Drawing.Size(67, 35);
            this.panelLdTrans2.TabIndex = 20;
            // 
            // lb_ld_trans2
            // 
            this.lb_ld_trans2.BackColor = System.Drawing.Color.Transparent;
            this.lb_ld_trans2.Font = new System.Drawing.Font("Gulim", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld_trans2.ForeColor = System.Drawing.Color.White;
            this.lb_ld_trans2.Location = new System.Drawing.Point(-5, -1);
            this.lb_ld_trans2.Name = "lb_ld_trans2";
            this.lb_ld_trans2.Size = new System.Drawing.Size(76, 36);
            this.lb_ld_trans2.TabIndex = 4;
            this.lb_ld_trans2.Text = "   로더     트랜스퍼 2";
            this.lb_ld_trans2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelLd1Table
            // 
            this.panelLd1Table.BackColor = System.Drawing.Color.DimGray;
            this.panelLd1Table.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelLd1Table.ForeColor = System.Drawing.Color.White;
            this.panelLd1Table.Location = new System.Drawing.Point(644, 148);
            this.panelLd1Table.Name = "panelLd1Table";
            this.panelLd1Table.Size = new System.Drawing.Size(15, 47);
            this.panelLd1Table.TabIndex = 42;
            // 
            // panelLd2
            // 
            this.panelLd2.BackColor = System.Drawing.Color.DimGray;
            this.panelLd2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelLd2.Controls.Add(this.lbLD2);
            this.panelLd2.ForeColor = System.Drawing.Color.White;
            this.panelLd2.Location = new System.Drawing.Point(573, 78);
            this.panelLd2.Name = "panelLd2";
            this.panelLd2.Size = new System.Drawing.Size(67, 35);
            this.panelLd2.TabIndex = 19;
            // 
            // lbLD2
            // 
            this.lbLD2.AutoSize = true;
            this.lbLD2.BackColor = System.Drawing.Color.Transparent;
            this.lbLD2.Font = new System.Drawing.Font("Gulim", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbLD2.ForeColor = System.Drawing.Color.White;
            this.lbLD2.Location = new System.Drawing.Point(11, 11);
            this.lbLD2.Name = "lbLD2";
            this.lbLD2.Size = new System.Drawing.Size(41, 11);
            this.lbLD2.TabIndex = 4;
            this.lbLD2.Text = "로더 2";
            // 
            // panelL1In2
            // 
            this.panelL1In2.BackColor = System.Drawing.Color.DimGray;
            this.panelL1In2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelL1In2.ForeColor = System.Drawing.Color.White;
            this.panelL1In2.Location = new System.Drawing.Point(663, 174);
            this.panelL1In2.Name = "panelL1In2";
            this.panelL1In2.Size = new System.Drawing.Size(20, 20);
            this.panelL1In2.TabIndex = 43;
            // 
            // panelLd2Table
            // 
            this.panelLd2Table.BackColor = System.Drawing.Color.DimGray;
            this.panelLd2Table.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelLd2Table.ForeColor = System.Drawing.Color.White;
            this.panelLd2Table.Location = new System.Drawing.Point(644, 72);
            this.panelLd2Table.Name = "panelLd2Table";
            this.panelLd2Table.Size = new System.Drawing.Size(15, 47);
            this.panelLd2Table.TabIndex = 21;
            // 
            // panelLd1In1
            // 
            this.panelLd1In1.BackColor = System.Drawing.Color.DimGray;
            this.panelLd1In1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelLd1In1.ForeColor = System.Drawing.Color.White;
            this.panelLd1In1.Location = new System.Drawing.Point(663, 148);
            this.panelLd1In1.Name = "panelLd1In1";
            this.panelLd1In1.Size = new System.Drawing.Size(20, 20);
            this.panelLd1In1.TabIndex = 39;
            // 
            // panelLd2In2
            // 
            this.panelLd2In2.BackColor = System.Drawing.Color.DimGray;
            this.panelLd2In2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelLd2In2.ForeColor = System.Drawing.Color.White;
            this.panelLd2In2.Location = new System.Drawing.Point(663, 98);
            this.panelLd2In2.Name = "panelLd2In2";
            this.panelLd2In2.Size = new System.Drawing.Size(20, 20);
            this.panelLd2In2.TabIndex = 21;
            // 
            // panelLd2In1
            // 
            this.panelLd2In1.BackColor = System.Drawing.Color.DimGray;
            this.panelLd2In1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelLd2In1.ForeColor = System.Drawing.Color.White;
            this.panelLd2In1.Location = new System.Drawing.Point(663, 72);
            this.panelLd2In1.Name = "panelLd2In1";
            this.panelLd2In1.Size = new System.Drawing.Size(20, 20);
            this.panelLd2In1.TabIndex = 20;
            // 
            // lbLd1OTime
            // 
            this.lbLd1OTime.AutoSize = true;
            this.lbLd1OTime.BackColor = System.Drawing.Color.Transparent;
            this.lbLd1OTime.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbLd1OTime.ForeColor = System.Drawing.Color.White;
            this.lbLd1OTime.Location = new System.Drawing.Point(716, 179);
            this.lbLd1OTime.Name = "lbLd1OTime";
            this.lbLd1OTime.Size = new System.Drawing.Size(57, 12);
            this.lbLd1OTime.TabIndex = 34;
            this.lbLd1OTime.Text = "00시 00분";
            // 
            // lb_ld1_o
            // 
            this.lb_ld1_o.AutoSize = true;
            this.lb_ld1_o.BackColor = System.Drawing.Color.Transparent;
            this.lb_ld1_o.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld1_o.ForeColor = System.Drawing.Color.White;
            this.lb_ld1_o.Location = new System.Drawing.Point(689, 179);
            this.lb_ld1_o.Name = "lb_ld1_o";
            this.lb_ld1_o.Size = new System.Drawing.Size(22, 12);
            this.lb_ld1_o.TabIndex = 33;
            this.lb_ld1_o.Text = "O :";
            // 
            // lbLd1ITime
            // 
            this.lbLd1ITime.AutoSize = true;
            this.lbLd1ITime.BackColor = System.Drawing.Color.Transparent;
            this.lbLd1ITime.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbLd1ITime.ForeColor = System.Drawing.Color.White;
            this.lbLd1ITime.Location = new System.Drawing.Point(716, 152);
            this.lbLd1ITime.Name = "lbLd1ITime";
            this.lbLd1ITime.Size = new System.Drawing.Size(57, 12);
            this.lbLd1ITime.TabIndex = 32;
            this.lbLd1ITime.Text = "00시 00분";
            // 
            // lb_ld1_i
            // 
            this.lb_ld1_i.AutoSize = true;
            this.lb_ld1_i.BackColor = System.Drawing.Color.Transparent;
            this.lb_ld1_i.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld1_i.ForeColor = System.Drawing.Color.White;
            this.lb_ld1_i.Location = new System.Drawing.Point(689, 152);
            this.lb_ld1_i.Name = "lb_ld1_i";
            this.lb_ld1_i.Size = new System.Drawing.Size(16, 12);
            this.lb_ld1_i.TabIndex = 31;
            this.lb_ld1_i.Text = "I :";
            // 
            // lbLd2OTime
            // 
            this.lbLd2OTime.AutoSize = true;
            this.lbLd2OTime.BackColor = System.Drawing.Color.Transparent;
            this.lbLd2OTime.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbLd2OTime.ForeColor = System.Drawing.Color.White;
            this.lbLd2OTime.Location = new System.Drawing.Point(716, 102);
            this.lbLd2OTime.Name = "lbLd2OTime";
            this.lbLd2OTime.Size = new System.Drawing.Size(57, 12);
            this.lbLd2OTime.TabIndex = 30;
            this.lbLd2OTime.Text = "00시 00분";
            // 
            // lb_ld2_o
            // 
            this.lb_ld2_o.AutoSize = true;
            this.lb_ld2_o.BackColor = System.Drawing.Color.Transparent;
            this.lb_ld2_o.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld2_o.ForeColor = System.Drawing.Color.White;
            this.lb_ld2_o.Location = new System.Drawing.Point(689, 102);
            this.lb_ld2_o.Name = "lb_ld2_o";
            this.lb_ld2_o.Size = new System.Drawing.Size(22, 12);
            this.lb_ld2_o.TabIndex = 29;
            this.lb_ld2_o.Text = "O :";
            // 
            // lbLd2ITime
            // 
            this.lbLd2ITime.AutoSize = true;
            this.lbLd2ITime.BackColor = System.Drawing.Color.Transparent;
            this.lbLd2ITime.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbLd2ITime.ForeColor = System.Drawing.Color.White;
            this.lbLd2ITime.Location = new System.Drawing.Point(716, 76);
            this.lbLd2ITime.Name = "lbLd2ITime";
            this.lbLd2ITime.Size = new System.Drawing.Size(57, 12);
            this.lbLd2ITime.TabIndex = 28;
            this.lbLd2ITime.Text = "00시 00분";
            // 
            // lb_ld2_i
            // 
            this.lb_ld2_i.AutoSize = true;
            this.lb_ld2_i.BackColor = System.Drawing.Color.Transparent;
            this.lb_ld2_i.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ld2_i.ForeColor = System.Drawing.Color.White;
            this.lb_ld2_i.Location = new System.Drawing.Point(689, 76);
            this.lb_ld2_i.Name = "lb_ld2_i";
            this.lb_ld2_i.Size = new System.Drawing.Size(16, 12);
            this.lb_ld2_i.TabIndex = 27;
            this.lb_ld2_i.Text = "I :";
            // 
            // lbUld1ITime
            // 
            this.lbUld1ITime.AutoSize = true;
            this.lbUld1ITime.BackColor = System.Drawing.Color.Transparent;
            this.lbUld1ITime.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbUld1ITime.ForeColor = System.Drawing.Color.White;
            this.lbUld1ITime.Location = new System.Drawing.Point(33, 179);
            this.lbUld1ITime.Name = "lbUld1ITime";
            this.lbUld1ITime.Size = new System.Drawing.Size(57, 12);
            this.lbUld1ITime.TabIndex = 26;
            this.lbUld1ITime.Text = "00시 00분";
            // 
            // lb_uld1_i
            // 
            this.lb_uld1_i.AutoSize = true;
            this.lb_uld1_i.BackColor = System.Drawing.Color.Transparent;
            this.lb_uld1_i.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld1_i.ForeColor = System.Drawing.Color.White;
            this.lb_uld1_i.Location = new System.Drawing.Point(6, 179);
            this.lb_uld1_i.Name = "lb_uld1_i";
            this.lb_uld1_i.Size = new System.Drawing.Size(16, 12);
            this.lb_uld1_i.TabIndex = 25;
            this.lb_uld1_i.Text = "I :";
            // 
            // lbUld1OTime
            // 
            this.lbUld1OTime.AutoSize = true;
            this.lbUld1OTime.BackColor = System.Drawing.Color.Transparent;
            this.lbUld1OTime.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbUld1OTime.ForeColor = System.Drawing.Color.White;
            this.lbUld1OTime.Location = new System.Drawing.Point(33, 152);
            this.lbUld1OTime.Name = "lbUld1OTime";
            this.lbUld1OTime.Size = new System.Drawing.Size(57, 12);
            this.lbUld1OTime.TabIndex = 24;
            this.lbUld1OTime.Text = "00시 00분";
            // 
            // lb_uld1_o
            // 
            this.lb_uld1_o.AutoSize = true;
            this.lb_uld1_o.BackColor = System.Drawing.Color.Transparent;
            this.lb_uld1_o.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld1_o.ForeColor = System.Drawing.Color.White;
            this.lb_uld1_o.Location = new System.Drawing.Point(6, 152);
            this.lb_uld1_o.Name = "lb_uld1_o";
            this.lb_uld1_o.Size = new System.Drawing.Size(22, 12);
            this.lb_uld1_o.TabIndex = 23;
            this.lb_uld1_o.Text = "O :";
            // 
            // lbUld2ITime
            // 
            this.lbUld2ITime.AutoSize = true;
            this.lbUld2ITime.BackColor = System.Drawing.Color.Transparent;
            this.lbUld2ITime.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbUld2ITime.ForeColor = System.Drawing.Color.White;
            this.lbUld2ITime.Location = new System.Drawing.Point(33, 103);
            this.lbUld2ITime.Name = "lbUld2ITime";
            this.lbUld2ITime.Size = new System.Drawing.Size(57, 12);
            this.lbUld2ITime.TabIndex = 22;
            this.lbUld2ITime.Text = "00시 00분";
            // 
            // lb_uld2_i
            // 
            this.lb_uld2_i.AutoSize = true;
            this.lb_uld2_i.BackColor = System.Drawing.Color.Transparent;
            this.lb_uld2_i.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld2_i.ForeColor = System.Drawing.Color.White;
            this.lb_uld2_i.Location = new System.Drawing.Point(6, 103);
            this.lb_uld2_i.Name = "lb_uld2_i";
            this.lb_uld2_i.Size = new System.Drawing.Size(16, 12);
            this.lb_uld2_i.TabIndex = 21;
            this.lb_uld2_i.Text = "I :";
            // 
            // lbUld2OTime
            // 
            this.lbUld2OTime.AutoSize = true;
            this.lbUld2OTime.BackColor = System.Drawing.Color.Transparent;
            this.lbUld2OTime.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbUld2OTime.ForeColor = System.Drawing.Color.White;
            this.lbUld2OTime.Location = new System.Drawing.Point(33, 76);
            this.lbUld2OTime.Name = "lbUld2OTime";
            this.lbUld2OTime.Size = new System.Drawing.Size(57, 12);
            this.lbUld2OTime.TabIndex = 6;
            this.lbUld2OTime.Text = "00시 00분";
            // 
            // lb_uld2_o
            // 
            this.lb_uld2_o.AutoSize = true;
            this.lb_uld2_o.BackColor = System.Drawing.Color.Transparent;
            this.lb_uld2_o.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_uld2_o.ForeColor = System.Drawing.Color.White;
            this.lb_uld2_o.Location = new System.Drawing.Point(6, 76);
            this.lb_uld2_o.Name = "lb_uld2_o";
            this.lb_uld2_o.Size = new System.Drawing.Size(22, 12);
            this.lb_uld2_o.TabIndex = 5;
            this.lb_uld2_o.Text = "O :";
            // 
            // panelShutterOpen
            // 
            this.panelShutterOpen.BackColor = System.Drawing.Color.DimGray;
            this.panelShutterOpen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelShutterOpen.Controls.Add(this.lb_shutter_open);
            this.panelShutterOpen.ForeColor = System.Drawing.Color.White;
            this.panelShutterOpen.Location = new System.Drawing.Point(606, 23);
            this.panelShutterOpen.Name = "panelShutterOpen";
            this.panelShutterOpen.Size = new System.Drawing.Size(65, 30);
            this.panelShutterOpen.TabIndex = 20;
            // 
            // lb_shutter_open
            // 
            this.lb_shutter_open.BackColor = System.Drawing.Color.Transparent;
            this.lb_shutter_open.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_shutter_open.ForeColor = System.Drawing.Color.White;
            this.lb_shutter_open.Location = new System.Drawing.Point(3, 3);
            this.lb_shutter_open.Name = "lb_shutter_open";
            this.lb_shutter_open.Size = new System.Drawing.Size(57, 25);
            this.lb_shutter_open.TabIndex = 4;
            this.lb_shutter_open.Text = "Shutter Open";
            this.lb_shutter_open.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelLock
            // 
            this.panelLock.BackColor = System.Drawing.Color.DimGray;
            this.panelLock.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelLock.Controls.Add(this.lb_lock);
            this.panelLock.ForeColor = System.Drawing.Color.White;
            this.panelLock.Location = new System.Drawing.Point(535, 23);
            this.panelLock.Name = "panelLock";
            this.panelLock.Size = new System.Drawing.Size(65, 30);
            this.panelLock.TabIndex = 19;
            // 
            // lb_lock
            // 
            this.lb_lock.AutoSize = true;
            this.lb_lock.BackColor = System.Drawing.Color.Transparent;
            this.lb_lock.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_lock.ForeColor = System.Drawing.Color.White;
            this.lb_lock.Location = new System.Drawing.Point(14, 9);
            this.lb_lock.Name = "lb_lock";
            this.lb_lock.Size = new System.Drawing.Size(36, 12);
            this.lb_lock.TabIndex = 4;
            this.lb_lock.Text = "Lock";
            // 
            // panelLaserCover
            // 
            this.panelLaserCover.BackColor = System.Drawing.Color.DimGray;
            this.panelLaserCover.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelLaserCover.Controls.Add(this.lb_laser_cover);
            this.panelLaserCover.ForeColor = System.Drawing.Color.White;
            this.panelLaserCover.Location = new System.Drawing.Point(447, 23);
            this.panelLaserCover.Name = "panelLaserCover";
            this.panelLaserCover.Size = new System.Drawing.Size(82, 30);
            this.panelLaserCover.TabIndex = 18;
            // 
            // lb_laser_cover
            // 
            this.lb_laser_cover.AutoSize = true;
            this.lb_laser_cover.BackColor = System.Drawing.Color.Transparent;
            this.lb_laser_cover.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_laser_cover.ForeColor = System.Drawing.Color.White;
            this.lb_laser_cover.Location = new System.Drawing.Point(3, 9);
            this.lb_laser_cover.Name = "lb_laser_cover";
            this.lb_laser_cover.Size = new System.Drawing.Size(75, 12);
            this.lb_laser_cover.TabIndex = 4;
            this.lb_laser_cover.Text = "레이저 커버";
            // 
            // panelEms2
            // 
            this.panelEms2.BackColor = System.Drawing.Color.DimGray;
            this.panelEms2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelEms2.Controls.Add(this.lb_ems2);
            this.panelEms2.ForeColor = System.Drawing.Color.White;
            this.panelEms2.Location = new System.Drawing.Point(727, 23);
            this.panelEms2.Name = "panelEms2";
            this.panelEms2.Size = new System.Drawing.Size(55, 30);
            this.panelEms2.TabIndex = 17;
            // 
            // lb_ems2
            // 
            this.lb_ems2.AutoSize = true;
            this.lb_ems2.BackColor = System.Drawing.Color.Transparent;
            this.lb_ems2.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ems2.ForeColor = System.Drawing.Color.White;
            this.lb_ems2.Location = new System.Drawing.Point(3, 9);
            this.lb_ems2.Name = "lb_ems2";
            this.lb_ems2.Size = new System.Drawing.Size(47, 12);
            this.lb_ems2.TabIndex = 4;
            this.lb_ems2.Text = "EMS 2";
            // 
            // panelEms6
            // 
            this.panelEms6.BackColor = System.Drawing.Color.DimGray;
            this.panelEms6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelEms6.Controls.Add(this.lb_ems6);
            this.panelEms6.ForeColor = System.Drawing.Color.White;
            this.panelEms6.Location = new System.Drawing.Point(8, 23);
            this.panelEms6.Name = "panelEms6";
            this.panelEms6.Size = new System.Drawing.Size(55, 30);
            this.panelEms6.TabIndex = 16;
            // 
            // lb_ems6
            // 
            this.lb_ems6.AutoSize = true;
            this.lb_ems6.BackColor = System.Drawing.Color.Transparent;
            this.lb_ems6.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_ems6.ForeColor = System.Drawing.Color.White;
            this.lb_ems6.Location = new System.Drawing.Point(3, 9);
            this.lb_ems6.Name = "lb_ems6";
            this.lb_ems6.Size = new System.Drawing.Size(47, 12);
            this.lb_ems6.TabIndex = 4;
            this.lb_ems6.Text = "EMS 6";
            // 
            // panelGrabEms1
            // 
            this.panelGrabEms1.BackColor = System.Drawing.Color.DimGray;
            this.panelGrabEms1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelGrabEms1.Controls.Add(this.lb_grab_ems1);
            this.panelGrabEms1.ForeColor = System.Drawing.Color.White;
            this.panelGrabEms1.Location = new System.Drawing.Point(619, 3);
            this.panelGrabEms1.Name = "panelGrabEms1";
            this.panelGrabEms1.Size = new System.Drawing.Size(94, 14);
            this.panelGrabEms1.TabIndex = 19;
            // 
            // lb_grab_ems1
            // 
            this.lb_grab_ems1.AutoSize = true;
            this.lb_grab_ems1.BackColor = System.Drawing.Color.Transparent;
            this.lb_grab_ems1.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_grab_ems1.ForeColor = System.Drawing.Color.White;
            this.lb_grab_ems1.Location = new System.Drawing.Point(5, 1);
            this.lb_grab_ems1.Name = "lb_grab_ems1";
            this.lb_grab_ems1.Size = new System.Drawing.Size(83, 12);
            this.lb_grab_ems1.TabIndex = 4;
            this.lb_grab_ems1.Text = "Grab EMS 1";
            // 
            // panelGrabEms2
            // 
            this.panelGrabEms2.BackColor = System.Drawing.Color.DimGray;
            this.panelGrabEms2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelGrabEms2.Controls.Add(this.lb_grab_ems2);
            this.panelGrabEms2.ForeColor = System.Drawing.Color.White;
            this.panelGrabEms2.Location = new System.Drawing.Point(405, 3);
            this.panelGrabEms2.Name = "panelGrabEms2";
            this.panelGrabEms2.Size = new System.Drawing.Size(94, 14);
            this.panelGrabEms2.TabIndex = 17;
            // 
            // lb_grab_ems2
            // 
            this.lb_grab_ems2.AutoSize = true;
            this.lb_grab_ems2.BackColor = System.Drawing.Color.Transparent;
            this.lb_grab_ems2.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_grab_ems2.ForeColor = System.Drawing.Color.White;
            this.lb_grab_ems2.Location = new System.Drawing.Point(5, 1);
            this.lb_grab_ems2.Name = "lb_grab_ems2";
            this.lb_grab_ems2.Size = new System.Drawing.Size(83, 12);
            this.lb_grab_ems2.TabIndex = 4;
            this.lb_grab_ems2.Text = "Grab EMS 2";
            // 
            // panelGrabSwitch1
            // 
            this.panelGrabSwitch1.BackColor = System.Drawing.Color.DimGray;
            this.panelGrabSwitch1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelGrabSwitch1.Controls.Add(this.lb_grab_switch1);
            this.panelGrabSwitch1.ForeColor = System.Drawing.Color.White;
            this.panelGrabSwitch1.Location = new System.Drawing.Point(505, 3);
            this.panelGrabSwitch1.Name = "panelGrabSwitch1";
            this.panelGrabSwitch1.Size = new System.Drawing.Size(108, 14);
            this.panelGrabSwitch1.TabIndex = 18;
            // 
            // lb_grab_switch1
            // 
            this.lb_grab_switch1.AutoSize = true;
            this.lb_grab_switch1.BackColor = System.Drawing.Color.Transparent;
            this.lb_grab_switch1.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_grab_switch1.ForeColor = System.Drawing.Color.White;
            this.lb_grab_switch1.Location = new System.Drawing.Point(5, 1);
            this.lb_grab_switch1.Name = "lb_grab_switch1";
            this.lb_grab_switch1.Size = new System.Drawing.Size(97, 12);
            this.lb_grab_switch1.TabIndex = 4;
            this.lb_grab_switch1.Text = "Grab Switch 1";
            // 
            // panelGrabSwitch2
            // 
            this.panelGrabSwitch2.BackColor = System.Drawing.Color.DimGray;
            this.panelGrabSwitch2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelGrabSwitch2.Controls.Add(this.lb_grab_switch2);
            this.panelGrabSwitch2.ForeColor = System.Drawing.Color.White;
            this.panelGrabSwitch2.Location = new System.Drawing.Point(291, 3);
            this.panelGrabSwitch2.Name = "panelGrabSwitch2";
            this.panelGrabSwitch2.Size = new System.Drawing.Size(108, 14);
            this.panelGrabSwitch2.TabIndex = 16;
            // 
            // lb_grab_switch2
            // 
            this.lb_grab_switch2.AutoSize = true;
            this.lb_grab_switch2.BackColor = System.Drawing.Color.Transparent;
            this.lb_grab_switch2.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_grab_switch2.ForeColor = System.Drawing.Color.White;
            this.lb_grab_switch2.Location = new System.Drawing.Point(5, 1);
            this.lb_grab_switch2.Name = "lb_grab_switch2";
            this.lb_grab_switch2.Size = new System.Drawing.Size(97, 12);
            this.lb_grab_switch2.TabIndex = 4;
            this.lb_grab_switch2.Text = "Grab Switch 2";
            // 
            // panelGrabEms3
            // 
            this.panelGrabEms3.BackColor = System.Drawing.Color.DimGray;
            this.panelGrabEms3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelGrabEms3.Controls.Add(this.lb_grab_ems3);
            this.panelGrabEms3.ForeColor = System.Drawing.Color.White;
            this.panelGrabEms3.Location = new System.Drawing.Point(191, 3);
            this.panelGrabEms3.Name = "panelGrabEms3";
            this.panelGrabEms3.Size = new System.Drawing.Size(94, 14);
            this.panelGrabEms3.TabIndex = 16;
            // 
            // lb_grab_ems3
            // 
            this.lb_grab_ems3.AutoSize = true;
            this.lb_grab_ems3.BackColor = System.Drawing.Color.Transparent;
            this.lb_grab_ems3.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_grab_ems3.ForeColor = System.Drawing.Color.White;
            this.lb_grab_ems3.Location = new System.Drawing.Point(5, 1);
            this.lb_grab_ems3.Name = "lb_grab_ems3";
            this.lb_grab_ems3.Size = new System.Drawing.Size(83, 12);
            this.lb_grab_ems3.TabIndex = 4;
            this.lb_grab_ems3.Text = "Grab EMS 3";
            // 
            // panelGrabSwitch3
            // 
            this.panelGrabSwitch3.BackColor = System.Drawing.Color.DimGray;
            this.panelGrabSwitch3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelGrabSwitch3.Controls.Add(this.lb_grab_switch3);
            this.panelGrabSwitch3.ForeColor = System.Drawing.Color.White;
            this.panelGrabSwitch3.Location = new System.Drawing.Point(77, 3);
            this.panelGrabSwitch3.Name = "panelGrabSwitch3";
            this.panelGrabSwitch3.Size = new System.Drawing.Size(108, 14);
            this.panelGrabSwitch3.TabIndex = 15;
            // 
            // lb_grab_switch3
            // 
            this.lb_grab_switch3.AutoSize = true;
            this.lb_grab_switch3.BackColor = System.Drawing.Color.Transparent;
            this.lb_grab_switch3.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_grab_switch3.ForeColor = System.Drawing.Color.White;
            this.lb_grab_switch3.Location = new System.Drawing.Point(5, 1);
            this.lb_grab_switch3.Name = "lb_grab_switch3";
            this.lb_grab_switch3.Size = new System.Drawing.Size(97, 12);
            this.lb_grab_switch3.TabIndex = 4;
            this.lb_grab_switch3.Text = "Grab Switch 3";
            // 
            // panel_door10
            // 
            this.panel_door10.BackColor = System.Drawing.Color.DimGray;
            this.panel_door10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_door10.Controls.Add(this.lbDoor10);
            this.panel_door10.ForeColor = System.Drawing.Color.White;
            this.panel_door10.Location = new System.Drawing.Point(4, 65);
            this.panel_door10.Name = "panel_door10";
            this.panel_door10.Size = new System.Drawing.Size(37, 59);
            this.panel_door10.TabIndex = 13;
            // 
            // lbDoor10
            // 
            this.lbDoor10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbDoor10.ForeColor = System.Drawing.Color.White;
            this.lbDoor10.Location = new System.Drawing.Point(4, 3);
            this.lbDoor10.Name = "lbDoor10";
            this.lbDoor10.Size = new System.Drawing.Size(27, 51);
            this.lbDoor10.TabIndex = 1;
            this.lbDoor10.Text = "도어10";
            this.lbDoor10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DimGray;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label30);
            this.panel2.ForeColor = System.Drawing.Color.White;
            this.panel2.Location = new System.Drawing.Point(5, 66);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(37, 59);
            this.panel2.TabIndex = 13;
            // 
            // label30
            // 
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label30.ForeColor = System.Drawing.Color.White;
            this.label30.Location = new System.Drawing.Point(4, 3);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(27, 51);
            this.label30.TabIndex = 1;
            this.label30.Text = "도어10";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_camera_l_info
            // 
            this.panel_camera_l_info.BackColor = System.Drawing.Color.DimGray;
            this.panel_camera_l_info.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_camera_l_info.Controls.Add(this.lb_camera_l);
            this.panel_camera_l_info.Controls.Add(this.panel_camera_l);
            this.panel_camera_l_info.ForeColor = System.Drawing.Color.White;
            this.panel_camera_l_info.Location = new System.Drawing.Point(887, 225);
            this.panel_camera_l_info.Name = "panel_camera_l_info";
            this.panel_camera_l_info.Size = new System.Drawing.Size(168, 156);
            this.panel_camera_l_info.TabIndex = 13;
            // 
            // lb_camera_l
            // 
            this.lb_camera_l.AutoSize = true;
            this.lb_camera_l.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_camera_l.Location = new System.Drawing.Point(3, 4);
            this.lb_camera_l.Name = "lb_camera_l";
            this.lb_camera_l.Size = new System.Drawing.Size(91, 24);
            this.lb_camera_l.TabIndex = 0;
            this.lb_camera_l.Text = "좌측 카메라";
            // 
            // panel_camera_l
            // 
            this.panel_camera_l.BackColor = System.Drawing.Color.White;
            this.panel_camera_l.Location = new System.Drawing.Point(3, 28);
            this.panel_camera_l.Name = "panel_camera_l";
            this.panel_camera_l.Size = new System.Drawing.Size(162, 123);
            this.panel_camera_l.TabIndex = 3;
            // 
            // panel_camera_r_info
            // 
            this.panel_camera_r_info.BackColor = System.Drawing.Color.DimGray;
            this.panel_camera_r_info.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_camera_r_info.Controls.Add(this.lb_camera_r);
            this.panel_camera_r_info.Controls.Add(this.panel_camera_r);
            this.panel_camera_r_info.ForeColor = System.Drawing.Color.White;
            this.panel_camera_r_info.Location = new System.Drawing.Point(1359, 3);
            this.panel_camera_r_info.Name = "panel_camera_r_info";
            this.panel_camera_r_info.Size = new System.Drawing.Size(168, 156);
            this.panel_camera_r_info.TabIndex = 39;
            // 
            // lb_camera_r
            // 
            this.lb_camera_r.AutoSize = true;
            this.lb_camera_r.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_camera_r.Location = new System.Drawing.Point(3, 4);
            this.lb_camera_r.Name = "lb_camera_r";
            this.lb_camera_r.Size = new System.Drawing.Size(91, 24);
            this.lb_camera_r.TabIndex = 0;
            this.lb_camera_r.Text = "우측 카메라";
            // 
            // panel_camera_r
            // 
            this.panel_camera_r.BackColor = System.Drawing.Color.White;
            this.panel_camera_r.Location = new System.Drawing.Point(3, 28);
            this.panel_camera_r.Name = "panel_camera_r";
            this.panel_camera_r.Size = new System.Drawing.Size(162, 123);
            this.panel_camera_r.TabIndex = 3;
            // 
            // panel_align
            // 
            this.panel_align.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_align.Controls.Add(this.txtaligBbreak4OffsetA);
            this.panel_align.Controls.Add(this.txtalignBreak3OffsetA);
            this.panel_align.Controls.Add(this.txtalignPre2OffsetA);
            this.panel_align.Controls.Add(this.lb_align_offset_a2);
            this.panel_align.Controls.Add(this.txtalignBreak4OffsetY);
            this.panel_align.Controls.Add(this.txtalignBreak3OffsetY);
            this.panel_align.Controls.Add(this.txtalignPre2OffsetY);
            this.panel_align.Controls.Add(this.lb_align_offset_y2);
            this.panel_align.Controls.Add(this.txtalignBreak4OffsetX);
            this.panel_align.Controls.Add(this.txtalignBreak3OffsetX);
            this.panel_align.Controls.Add(this.txtalignPre2OffsetX);
            this.panel_align.Controls.Add(this.lb_align_offset_x2);
            this.panel_align.Controls.Add(this.txtalignBreak4Result);
            this.panel_align.Controls.Add(this.txtalignBreak3Result);
            this.panel_align.Controls.Add(this.txtalignPre2Result);
            this.panel_align.Controls.Add(this.lb_align_result2);
            this.panel_align.Controls.Add(this.lb_align_break4);
            this.panel_align.Controls.Add(this.lb_align_break3);
            this.panel_align.Controls.Add(this.lb_align_pre2);
            this.panel_align.Controls.Add(this.lb_align_name2);
            this.panel_align.Controls.Add(this.txtalignBreak2OffsetA);
            this.panel_align.Controls.Add(this.txtalignBreak1Offset_A);
            this.panel_align.Controls.Add(this.txtalignPre1OffsetA);
            this.panel_align.Controls.Add(this.lb_align_offset_a1);
            this.panel_align.Controls.Add(this.txtalignBreak2OffsetY);
            this.panel_align.Controls.Add(this.txtalignBreak1OffsetY);
            this.panel_align.Controls.Add(this.txtalignPre1OffsetY);
            this.panel_align.Controls.Add(this.lb_align_offset_y1);
            this.panel_align.Controls.Add(this.txtalignBreak2OffsetX);
            this.panel_align.Controls.Add(this.txtalignBreak1OffsetX);
            this.panel_align.Controls.Add(this.txtalignPre1OffsetX);
            this.panel_align.Controls.Add(this.lb_align_offset_x1);
            this.panel_align.Controls.Add(this.txtalignBreak2Result);
            this.panel_align.Controls.Add(this.txtalignBreak1Result);
            this.panel_align.Controls.Add(this.txtalignPre1Result);
            this.panel_align.Controls.Add(this.lb_align_result1);
            this.panel_align.Controls.Add(this.lb_align_break2);
            this.panel_align.Controls.Add(this.lb_align_break1);
            this.panel_align.Controls.Add(this.lb_align_pre1);
            this.panel_align.Controls.Add(this.lb_align_name1);
            this.panel_align.ForeColor = System.Drawing.Color.White;
            this.panel_align.Location = new System.Drawing.Point(874, 745);
            this.panel_align.Name = "panel_align";
            this.panel_align.Size = new System.Drawing.Size(864, 112);
            this.panel_align.TabIndex = 54;
            // 
            // txtaligBbreak4OffsetA
            // 
            this.txtaligBbreak4OffsetA.BackColor = System.Drawing.Color.DimGray;
            this.txtaligBbreak4OffsetA.Location = new System.Drawing.Point(779, 82);
            this.txtaligBbreak4OffsetA.Name = "txtaligBbreak4OffsetA";
            this.txtaligBbreak4OffsetA.ReadOnly = true;
            this.txtaligBbreak4OffsetA.Size = new System.Drawing.Size(73, 21);
            this.txtaligBbreak4OffsetA.TabIndex = 49;
            // 
            // txtalignBreak3OffsetA
            // 
            this.txtalignBreak3OffsetA.BackColor = System.Drawing.Color.DimGray;
            this.txtalignBreak3OffsetA.Location = new System.Drawing.Point(779, 53);
            this.txtalignBreak3OffsetA.Name = "txtalignBreak3OffsetA";
            this.txtalignBreak3OffsetA.ReadOnly = true;
            this.txtalignBreak3OffsetA.Size = new System.Drawing.Size(73, 21);
            this.txtalignBreak3OffsetA.TabIndex = 48;
            // 
            // txtalignPre2OffsetA
            // 
            this.txtalignPre2OffsetA.BackColor = System.Drawing.Color.DimGray;
            this.txtalignPre2OffsetA.Location = new System.Drawing.Point(779, 26);
            this.txtalignPre2OffsetA.Name = "txtalignPre2OffsetA";
            this.txtalignPre2OffsetA.ReadOnly = true;
            this.txtalignPre2OffsetA.Size = new System.Drawing.Size(73, 21);
            this.txtalignPre2OffsetA.TabIndex = 46;
            // 
            // lb_align_offset_a2
            // 
            this.lb_align_offset_a2.AutoSize = true;
            this.lb_align_offset_a2.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_align_offset_a2.Location = new System.Drawing.Point(780, 3);
            this.lb_align_offset_a2.Name = "lb_align_offset_a2";
            this.lb_align_offset_a2.Size = new System.Drawing.Size(78, 20);
            this.lb_align_offset_a2.TabIndex = 47;
            this.lb_align_offset_a2.Text = "OFFSET A";
            // 
            // txtalignBreak4OffsetY
            // 
            this.txtalignBreak4OffsetY.BackColor = System.Drawing.Color.DimGray;
            this.txtalignBreak4OffsetY.Location = new System.Drawing.Point(702, 82);
            this.txtalignBreak4OffsetY.Name = "txtalignBreak4OffsetY";
            this.txtalignBreak4OffsetY.ReadOnly = true;
            this.txtalignBreak4OffsetY.Size = new System.Drawing.Size(73, 21);
            this.txtalignBreak4OffsetY.TabIndex = 45;
            // 
            // txtalignBreak3OffsetY
            // 
            this.txtalignBreak3OffsetY.BackColor = System.Drawing.Color.DimGray;
            this.txtalignBreak3OffsetY.Location = new System.Drawing.Point(702, 53);
            this.txtalignBreak3OffsetY.Name = "txtalignBreak3OffsetY";
            this.txtalignBreak3OffsetY.ReadOnly = true;
            this.txtalignBreak3OffsetY.Size = new System.Drawing.Size(73, 21);
            this.txtalignBreak3OffsetY.TabIndex = 44;
            // 
            // txtalignPre2OffsetY
            // 
            this.txtalignPre2OffsetY.BackColor = System.Drawing.Color.DimGray;
            this.txtalignPre2OffsetY.Location = new System.Drawing.Point(702, 26);
            this.txtalignPre2OffsetY.Name = "txtalignPre2OffsetY";
            this.txtalignPre2OffsetY.ReadOnly = true;
            this.txtalignPre2OffsetY.Size = new System.Drawing.Size(73, 21);
            this.txtalignPre2OffsetY.TabIndex = 42;
            // 
            // lb_align_offset_y2
            // 
            this.lb_align_offset_y2.AutoSize = true;
            this.lb_align_offset_y2.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_align_offset_y2.Location = new System.Drawing.Point(703, 3);
            this.lb_align_offset_y2.Name = "lb_align_offset_y2";
            this.lb_align_offset_y2.Size = new System.Drawing.Size(76, 20);
            this.lb_align_offset_y2.TabIndex = 43;
            this.lb_align_offset_y2.Text = "OFFSET Y";
            // 
            // txtalignBreak4OffsetX
            // 
            this.txtalignBreak4OffsetX.BackColor = System.Drawing.Color.DimGray;
            this.txtalignBreak4OffsetX.Location = new System.Drawing.Point(623, 82);
            this.txtalignBreak4OffsetX.Name = "txtalignBreak4OffsetX";
            this.txtalignBreak4OffsetX.ReadOnly = true;
            this.txtalignBreak4OffsetX.Size = new System.Drawing.Size(73, 21);
            this.txtalignBreak4OffsetX.TabIndex = 41;
            // 
            // txtalignBreak3OffsetX
            // 
            this.txtalignBreak3OffsetX.BackColor = System.Drawing.Color.DimGray;
            this.txtalignBreak3OffsetX.Location = new System.Drawing.Point(623, 53);
            this.txtalignBreak3OffsetX.Name = "txtalignBreak3OffsetX";
            this.txtalignBreak3OffsetX.ReadOnly = true;
            this.txtalignBreak3OffsetX.Size = new System.Drawing.Size(73, 21);
            this.txtalignBreak3OffsetX.TabIndex = 40;
            // 
            // txtalignPre2OffsetX
            // 
            this.txtalignPre2OffsetX.BackColor = System.Drawing.Color.DimGray;
            this.txtalignPre2OffsetX.Location = new System.Drawing.Point(623, 26);
            this.txtalignPre2OffsetX.Name = "txtalignPre2OffsetX";
            this.txtalignPre2OffsetX.ReadOnly = true;
            this.txtalignPre2OffsetX.Size = new System.Drawing.Size(73, 21);
            this.txtalignPre2OffsetX.TabIndex = 38;
            // 
            // lb_align_offset_x2
            // 
            this.lb_align_offset_x2.AutoSize = true;
            this.lb_align_offset_x2.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_align_offset_x2.Location = new System.Drawing.Point(624, 3);
            this.lb_align_offset_x2.Name = "lb_align_offset_x2";
            this.lb_align_offset_x2.Size = new System.Drawing.Size(77, 20);
            this.lb_align_offset_x2.TabIndex = 39;
            this.lb_align_offset_x2.Text = "OFFSET X";
            // 
            // txtalignBreak4Result
            // 
            this.txtalignBreak4Result.BackColor = System.Drawing.Color.DimGray;
            this.txtalignBreak4Result.Location = new System.Drawing.Point(544, 82);
            this.txtalignBreak4Result.Name = "txtalignBreak4Result";
            this.txtalignBreak4Result.ReadOnly = true;
            this.txtalignBreak4Result.Size = new System.Drawing.Size(73, 21);
            this.txtalignBreak4Result.TabIndex = 37;
            // 
            // txtalignBreak3Result
            // 
            this.txtalignBreak3Result.BackColor = System.Drawing.Color.DimGray;
            this.txtalignBreak3Result.Location = new System.Drawing.Point(544, 53);
            this.txtalignBreak3Result.Name = "txtalignBreak3Result";
            this.txtalignBreak3Result.ReadOnly = true;
            this.txtalignBreak3Result.Size = new System.Drawing.Size(73, 21);
            this.txtalignBreak3Result.TabIndex = 36;
            // 
            // txtalignPre2Result
            // 
            this.txtalignPre2Result.BackColor = System.Drawing.Color.DimGray;
            this.txtalignPre2Result.Location = new System.Drawing.Point(544, 26);
            this.txtalignPre2Result.Name = "txtalignPre2Result";
            this.txtalignPre2Result.ReadOnly = true;
            this.txtalignPre2Result.Size = new System.Drawing.Size(73, 21);
            this.txtalignPre2Result.TabIndex = 31;
            // 
            // lb_align_result2
            // 
            this.lb_align_result2.AutoSize = true;
            this.lb_align_result2.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_align_result2.Location = new System.Drawing.Point(551, 3);
            this.lb_align_result2.Name = "lb_align_result2";
            this.lb_align_result2.Size = new System.Drawing.Size(63, 20);
            this.lb_align_result2.TabIndex = 35;
            this.lb_align_result2.Text = "RESULT";
            // 
            // lb_align_break4
            // 
            this.lb_align_break4.AutoSize = true;
            this.lb_align_break4.Font = new System.Drawing.Font("Malgun Gothic", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_align_break4.Location = new System.Drawing.Point(453, 87);
            this.lb_align_break4.Name = "lb_align_break4";
            this.lb_align_break4.Size = new System.Drawing.Size(61, 17);
            this.lb_align_break4.TabIndex = 34;
            this.lb_align_break4.Text = "BREAK 4";
            // 
            // lb_align_break3
            // 
            this.lb_align_break3.AutoSize = true;
            this.lb_align_break3.Font = new System.Drawing.Font("Malgun Gothic", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_align_break3.Location = new System.Drawing.Point(453, 58);
            this.lb_align_break3.Name = "lb_align_break3";
            this.lb_align_break3.Size = new System.Drawing.Size(61, 17);
            this.lb_align_break3.TabIndex = 33;
            this.lb_align_break3.Text = "BREAK 3";
            // 
            // lb_align_pre2
            // 
            this.lb_align_pre2.AutoSize = true;
            this.lb_align_pre2.Font = new System.Drawing.Font("Malgun Gothic", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_align_pre2.Location = new System.Drawing.Point(460, 30);
            this.lb_align_pre2.Name = "lb_align_pre2";
            this.lb_align_pre2.Size = new System.Drawing.Size(44, 17);
            this.lb_align_pre2.TabIndex = 32;
            this.lb_align_pre2.Text = "PRE 2";
            // 
            // lb_align_name2
            // 
            this.lb_align_name2.AutoSize = true;
            this.lb_align_name2.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_align_name2.Location = new System.Drawing.Point(435, 3);
            this.lb_align_name2.Name = "lb_align_name2";
            this.lb_align_name2.Size = new System.Drawing.Size(106, 20);
            this.lb_align_name2.TabIndex = 30;
            this.lb_align_name2.Text = "ALIGN NAME";
            // 
            // txtalignBreak2OffsetA
            // 
            this.txtalignBreak2OffsetA.BackColor = System.Drawing.Color.DimGray;
            this.txtalignBreak2OffsetA.Location = new System.Drawing.Point(341, 82);
            this.txtalignBreak2OffsetA.Name = "txtalignBreak2OffsetA";
            this.txtalignBreak2OffsetA.ReadOnly = true;
            this.txtalignBreak2OffsetA.Size = new System.Drawing.Size(73, 21);
            this.txtalignBreak2OffsetA.TabIndex = 29;
            // 
            // txtalignBreak1Offset_A
            // 
            this.txtalignBreak1Offset_A.BackColor = System.Drawing.Color.DimGray;
            this.txtalignBreak1Offset_A.Location = new System.Drawing.Point(341, 53);
            this.txtalignBreak1Offset_A.Name = "txtalignBreak1Offset_A";
            this.txtalignBreak1Offset_A.ReadOnly = true;
            this.txtalignBreak1Offset_A.Size = new System.Drawing.Size(73, 21);
            this.txtalignBreak1Offset_A.TabIndex = 28;
            // 
            // txtalignPre1OffsetA
            // 
            this.txtalignPre1OffsetA.BackColor = System.Drawing.Color.DimGray;
            this.txtalignPre1OffsetA.Location = new System.Drawing.Point(341, 26);
            this.txtalignPre1OffsetA.Name = "txtalignPre1OffsetA";
            this.txtalignPre1OffsetA.ReadOnly = true;
            this.txtalignPre1OffsetA.Size = new System.Drawing.Size(73, 21);
            this.txtalignPre1OffsetA.TabIndex = 26;
            // 
            // lb_align_offset_a1
            // 
            this.lb_align_offset_a1.AutoSize = true;
            this.lb_align_offset_a1.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_align_offset_a1.Location = new System.Drawing.Point(342, 3);
            this.lb_align_offset_a1.Name = "lb_align_offset_a1";
            this.lb_align_offset_a1.Size = new System.Drawing.Size(78, 20);
            this.lb_align_offset_a1.TabIndex = 27;
            this.lb_align_offset_a1.Text = "OFFSET A";
            // 
            // txtalignBreak2OffsetY
            // 
            this.txtalignBreak2OffsetY.BackColor = System.Drawing.Color.DimGray;
            this.txtalignBreak2OffsetY.Location = new System.Drawing.Point(264, 82);
            this.txtalignBreak2OffsetY.Name = "txtalignBreak2OffsetY";
            this.txtalignBreak2OffsetY.ReadOnly = true;
            this.txtalignBreak2OffsetY.Size = new System.Drawing.Size(73, 21);
            this.txtalignBreak2OffsetY.TabIndex = 25;
            // 
            // txtalignBreak1OffsetY
            // 
            this.txtalignBreak1OffsetY.BackColor = System.Drawing.Color.DimGray;
            this.txtalignBreak1OffsetY.Location = new System.Drawing.Point(264, 53);
            this.txtalignBreak1OffsetY.Name = "txtalignBreak1OffsetY";
            this.txtalignBreak1OffsetY.ReadOnly = true;
            this.txtalignBreak1OffsetY.Size = new System.Drawing.Size(73, 21);
            this.txtalignBreak1OffsetY.TabIndex = 24;
            // 
            // txtalignPre1OffsetY
            // 
            this.txtalignPre1OffsetY.BackColor = System.Drawing.Color.DimGray;
            this.txtalignPre1OffsetY.Location = new System.Drawing.Point(264, 26);
            this.txtalignPre1OffsetY.Name = "txtalignPre1OffsetY";
            this.txtalignPre1OffsetY.ReadOnly = true;
            this.txtalignPre1OffsetY.Size = new System.Drawing.Size(73, 21);
            this.txtalignPre1OffsetY.TabIndex = 21;
            // 
            // lb_align_offset_y1
            // 
            this.lb_align_offset_y1.AutoSize = true;
            this.lb_align_offset_y1.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_align_offset_y1.Location = new System.Drawing.Point(265, 3);
            this.lb_align_offset_y1.Name = "lb_align_offset_y1";
            this.lb_align_offset_y1.Size = new System.Drawing.Size(76, 20);
            this.lb_align_offset_y1.TabIndex = 23;
            this.lb_align_offset_y1.Text = "OFFSET Y";
            // 
            // txtalignBreak2OffsetX
            // 
            this.txtalignBreak2OffsetX.BackColor = System.Drawing.Color.DimGray;
            this.txtalignBreak2OffsetX.Location = new System.Drawing.Point(185, 82);
            this.txtalignBreak2OffsetX.Name = "txtalignBreak2OffsetX";
            this.txtalignBreak2OffsetX.ReadOnly = true;
            this.txtalignBreak2OffsetX.Size = new System.Drawing.Size(73, 21);
            this.txtalignBreak2OffsetX.TabIndex = 20;
            // 
            // txtalignBreak1OffsetX
            // 
            this.txtalignBreak1OffsetX.BackColor = System.Drawing.Color.DimGray;
            this.txtalignBreak1OffsetX.Location = new System.Drawing.Point(185, 53);
            this.txtalignBreak1OffsetX.Name = "txtalignBreak1OffsetX";
            this.txtalignBreak1OffsetX.ReadOnly = true;
            this.txtalignBreak1OffsetX.Size = new System.Drawing.Size(73, 21);
            this.txtalignBreak1OffsetX.TabIndex = 19;
            // 
            // txtalignPre1OffsetX
            // 
            this.txtalignPre1OffsetX.BackColor = System.Drawing.Color.DimGray;
            this.txtalignPre1OffsetX.Location = new System.Drawing.Point(185, 26);
            this.txtalignPre1OffsetX.Name = "txtalignPre1OffsetX";
            this.txtalignPre1OffsetX.ReadOnly = true;
            this.txtalignPre1OffsetX.Size = new System.Drawing.Size(73, 21);
            this.txtalignPre1OffsetX.TabIndex = 16;
            // 
            // lb_align_offset_x1
            // 
            this.lb_align_offset_x1.AutoSize = true;
            this.lb_align_offset_x1.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_align_offset_x1.Location = new System.Drawing.Point(186, 3);
            this.lb_align_offset_x1.Name = "lb_align_offset_x1";
            this.lb_align_offset_x1.Size = new System.Drawing.Size(77, 20);
            this.lb_align_offset_x1.TabIndex = 18;
            this.lb_align_offset_x1.Text = "OFFSET X";
            // 
            // txtalignBreak2Result
            // 
            this.txtalignBreak2Result.BackColor = System.Drawing.Color.DimGray;
            this.txtalignBreak2Result.Location = new System.Drawing.Point(106, 82);
            this.txtalignBreak2Result.Name = "txtalignBreak2Result";
            this.txtalignBreak2Result.ReadOnly = true;
            this.txtalignBreak2Result.Size = new System.Drawing.Size(73, 21);
            this.txtalignBreak2Result.TabIndex = 15;
            // 
            // txtalignBreak1Result
            // 
            this.txtalignBreak1Result.BackColor = System.Drawing.Color.DimGray;
            this.txtalignBreak1Result.Location = new System.Drawing.Point(106, 53);
            this.txtalignBreak1Result.Name = "txtalignBreak1Result";
            this.txtalignBreak1Result.ReadOnly = true;
            this.txtalignBreak1Result.Size = new System.Drawing.Size(73, 21);
            this.txtalignBreak1Result.TabIndex = 14;
            // 
            // txtalignPre1Result
            // 
            this.txtalignPre1Result.BackColor = System.Drawing.Color.DimGray;
            this.txtalignPre1Result.Location = new System.Drawing.Point(106, 26);
            this.txtalignPre1Result.Name = "txtalignPre1Result";
            this.txtalignPre1Result.ReadOnly = true;
            this.txtalignPre1Result.Size = new System.Drawing.Size(73, 21);
            this.txtalignPre1Result.TabIndex = 9;
            // 
            // lb_align_result1
            // 
            this.lb_align_result1.AutoSize = true;
            this.lb_align_result1.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_align_result1.Location = new System.Drawing.Point(113, 3);
            this.lb_align_result1.Name = "lb_align_result1";
            this.lb_align_result1.Size = new System.Drawing.Size(63, 20);
            this.lb_align_result1.TabIndex = 13;
            this.lb_align_result1.Text = "RESULT";
            // 
            // lb_align_break2
            // 
            this.lb_align_break2.AutoSize = true;
            this.lb_align_break2.Font = new System.Drawing.Font("Malgun Gothic", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_align_break2.Location = new System.Drawing.Point(20, 87);
            this.lb_align_break2.Name = "lb_align_break2";
            this.lb_align_break2.Size = new System.Drawing.Size(61, 17);
            this.lb_align_break2.TabIndex = 12;
            this.lb_align_break2.Text = "BREAK 2";
            // 
            // lb_align_break1
            // 
            this.lb_align_break1.AutoSize = true;
            this.lb_align_break1.Font = new System.Drawing.Font("Malgun Gothic", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_align_break1.Location = new System.Drawing.Point(20, 58);
            this.lb_align_break1.Name = "lb_align_break1";
            this.lb_align_break1.Size = new System.Drawing.Size(61, 17);
            this.lb_align_break1.TabIndex = 11;
            this.lb_align_break1.Text = "BREAK 1";
            // 
            // lb_align_pre1
            // 
            this.lb_align_pre1.AutoSize = true;
            this.lb_align_pre1.Font = new System.Drawing.Font("Malgun Gothic", 9.75F, System.Drawing.FontStyle.Bold);
            this.lb_align_pre1.Location = new System.Drawing.Point(29, 30);
            this.lb_align_pre1.Name = "lb_align_pre1";
            this.lb_align_pre1.Size = new System.Drawing.Size(44, 17);
            this.lb_align_pre1.TabIndex = 10;
            this.lb_align_pre1.Text = "PRE 1";
            // 
            // lb_align_name1
            // 
            this.lb_align_name1.AutoSize = true;
            this.lb_align_name1.Font = new System.Drawing.Font("Malgun Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.lb_align_name1.Location = new System.Drawing.Point(4, 3);
            this.lb_align_name1.Name = "lb_align_name1";
            this.lb_align_name1.Size = new System.Drawing.Size(106, 20);
            this.lb_align_name1.TabIndex = 9;
            this.lb_align_name1.Text = "ALIGN NAME";
            // 
            // gxtbreaking_count
            // 
            this.gxtbreaking_count.BackColor = System.Drawing.Color.Transparent;
            this.gxtbreaking_count.Controls.Add(this.btnBreakingCountClear);
            this.gxtbreaking_count.Controls.Add(this.lb_breaking_count);
            this.gxtbreaking_count.Controls.Add(this.lb_breaking_count_info);
            this.gxtbreaking_count.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.gxtbreaking_count.ForeColor = System.Drawing.Color.White;
            this.gxtbreaking_count.Location = new System.Drawing.Point(1280, 655);
            this.gxtbreaking_count.Name = "gxtbreaking_count";
            this.gxtbreaking_count.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gxtbreaking_count.Size = new System.Drawing.Size(274, 76);
            this.gxtbreaking_count.TabIndex = 62;
            this.gxtbreaking_count.TabStop = false;
            this.gxtbreaking_count.Text = "브레이킹 COUNT";
            // 
            // btnBreakingCountClear
            // 
            this.btnBreakingCountClear.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnBreakingCountClear.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.btnBreakingCountClear.ForeColor = System.Drawing.Color.White;
            this.btnBreakingCountClear.Location = new System.Drawing.Point(181, 28);
            this.btnBreakingCountClear.Name = "btnBreakingCountClear";
            this.btnBreakingCountClear.Size = new System.Drawing.Size(87, 35);
            this.btnBreakingCountClear.TabIndex = 91;
            this.btnBreakingCountClear.Text = "Clear";
            this.btnBreakingCountClear.UseVisualStyleBackColor = false;
            // 
            // lb_breaking_count
            // 
            this.lb_breaking_count.AutoSize = true;
            this.lb_breaking_count.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_count.Location = new System.Drawing.Point(135, 37);
            this.lb_breaking_count.Name = "lb_breaking_count";
            this.lb_breaking_count.Size = new System.Drawing.Size(17, 18);
            this.lb_breaking_count.TabIndex = 2;
            this.lb_breaking_count.Text = "0";
            // 
            // lb_breaking_count_info
            // 
            this.lb_breaking_count_info.AutoSize = true;
            this.lb_breaking_count_info.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_count_info.Location = new System.Drawing.Point(6, 37);
            this.lb_breaking_count_info.Name = "lb_breaking_count_info";
            this.lb_breaking_count_info.Size = new System.Drawing.Size(134, 18);
            this.lb_breaking_count_info.TabIndex = 1;
            this.lb_breaking_count_info.Text = "Breaking Count :";
            // 
            // gxtdummy_tank
            // 
            this.gxtdummy_tank.BackColor = System.Drawing.Color.Transparent;
            this.gxtdummy_tank.Controls.Add(this.btnDummyTankSet);
            this.gxtdummy_tank.Controls.Add(this.btnDummyTankGet);
            this.gxtdummy_tank.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gxtdummy_tank.ForeColor = System.Drawing.Color.White;
            this.gxtdummy_tank.Location = new System.Drawing.Point(1570, 655);
            this.gxtdummy_tank.Name = "gxtdummy_tank";
            this.gxtdummy_tank.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gxtdummy_tank.Size = new System.Drawing.Size(166, 76);
            this.gxtdummy_tank.TabIndex = 63;
            this.gxtdummy_tank.TabStop = false;
            this.gxtdummy_tank.Text = "Dummy Tank";
            // 
            // btnDummyTankSet
            // 
            this.btnDummyTankSet.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnDummyTankSet.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.btnDummyTankSet.ForeColor = System.Drawing.Color.White;
            this.btnDummyTankSet.Location = new System.Drawing.Point(89, 22);
            this.btnDummyTankSet.Name = "btnDummyTankSet";
            this.btnDummyTankSet.Size = new System.Drawing.Size(63, 45);
            this.btnDummyTankSet.TabIndex = 93;
            this.btnDummyTankSet.Text = "배출";
            this.btnDummyTankSet.UseVisualStyleBackColor = false;
            // 
            // btnDummyTankGet
            // 
            this.btnDummyTankGet.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnDummyTankGet.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.btnDummyTankGet.ForeColor = System.Drawing.Color.White;
            this.btnDummyTankGet.Location = new System.Drawing.Point(20, 22);
            this.btnDummyTankGet.Name = "btnDummyTankGet";
            this.btnDummyTankGet.Size = new System.Drawing.Size(63, 45);
            this.btnDummyTankGet.TabIndex = 92;
            this.btnDummyTankGet.Text = "투입";
            this.btnDummyTankGet.UseVisualStyleBackColor = false;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView21);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView17);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView16);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView15);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView10);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView9);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView8);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView7);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView6);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView23);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView22);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView20);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView19);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView18);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView14);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView13);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView12);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView5);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView4);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView3);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView11);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView2);
            this.splitContainer1.Panel1.Controls.Add(this.detailTactView1);
            this.splitContainer1.Panel1.Controls.Add(this.cstCellCount3);
            this.splitContainer1.Panel1.Controls.Add(this.cstCellCount4);
            this.splitContainer1.Panel1.Controls.Add(this.cstCellCount2);
            this.splitContainer1.Panel1.Controls.Add(this.cstCellCount1);
            this.splitContainer1.Panel1.Controls.Add(this.gxtdummy_tank);
            this.splitContainer1.Panel1.Controls.Add(this.gxtbreaking_count);
            this.splitContainer1.Panel1.Controls.Add(this.panel_align);
            this.splitContainer1.Panel1.Controls.Add(this.panel_camera_r_info);
            this.splitContainer1.Panel1.Controls.Add(this.panel_run);
            this.splitContainer1.Panel1.Controls.Add(this.btn_safety_reset);
            this.splitContainer1.Panel1.Controls.Add(this.gxtld);
            this.splitContainer1.Panel1.Controls.Add(this.gxtuld);
            this.splitContainer1.Panel1.Controls.Add(this.gxtmcr_count);
            this.splitContainer1.Panel1.Controls.Add(this.gxtlaser_info);
            this.splitContainer1.Panel1.Controls.Add(this.panel_runinfo);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.DarkSlateGray;
            this.splitContainer1.Panel2.Controls.Add(this.lb_insp2_connect_info);
            this.splitContainer1.Panel2.Controls.Add(this.lb_serial_connect_info);
            this.splitContainer1.Panel2.Controls.Add(this.panelInsp2ConnectInfo);
            this.splitContainer1.Panel2.Controls.Add(this.panelSerialConnectInfo);
            this.splitContainer1.Panel2.Controls.Add(this.lb_insp1_connect_info);
            this.splitContainer1.Panel2.Controls.Add(this.lb_sequence_connect_info);
            this.splitContainer1.Panel2.Controls.Add(this.panelInsp1ConnectInfo);
            this.splitContainer1.Panel2.Controls.Add(this.lb_laser_connect_info);
            this.splitContainer1.Panel2.Controls.Add(this.panelSequenceConnectInfo);
            this.splitContainer1.Panel2.Controls.Add(this.panelLaserConnectInfo);
            this.splitContainer1.Panel2.Controls.Add(this.lb_umac_connect_info);
            this.splitContainer1.Panel2.Controls.Add(this.panelUmacConnectInfo);
            this.splitContainer1.Panel2.Controls.Add(this.lb_ajin_connect_info);
            this.splitContainer1.Panel2.Controls.Add(this.lb_cim_connect_info);
            this.splitContainer1.Panel2.Controls.Add(this.panelAjinConnectInfo);
            this.splitContainer1.Panel2.Controls.Add(this.panelCimConnectInfo);
            this.splitContainer1.Panel2.Controls.Add(this.lb_process_connect_info);
            this.splitContainer1.Panel2.Controls.Add(this.btnLight);
            this.splitContainer1.Panel2.Controls.Add(this.btnDooropen);
            this.splitContainer1.Panel2.Controls.Add(this.btnPm);
            this.splitContainer1.Panel2.Controls.Add(this.btnKeyswitch);
            this.splitContainer1.Panel2.Controls.Add(this.btn_stop);
            this.splitContainer1.Panel2.Controls.Add(this.btn_pause);
            this.splitContainer1.Panel2.Controls.Add(this.bt_start);
            this.splitContainer1.Size = new System.Drawing.Size(1880, 860);
            this.splitContainer1.SplitterDistance = 1740;
            this.splitContainer1.TabIndex = 0;
            // 
            // detailTactView21
            // 
            this.detailTactView21.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView21.Location = new System.Drawing.Point(1049, 626);
            this.detailTactView21.Name = "detailTactView21";
            this.detailTactView21.Size = new System.Drawing.Size(166, 110);
            this.detailTactView21.TabIndex = 90;
            // 
            // detailTactView17
            // 
            this.detailTactView17.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView17.Location = new System.Drawing.Point(1568, 507);
            this.detailTactView17.Name = "detailTactView17";
            this.detailTactView17.Size = new System.Drawing.Size(166, 110);
            this.detailTactView17.TabIndex = 89;
            // 
            // detailTactView16
            // 
            this.detailTactView16.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView16.Location = new System.Drawing.Point(1221, 508);
            this.detailTactView16.Name = "detailTactView16";
            this.detailTactView16.Size = new System.Drawing.Size(166, 110);
            this.detailTactView16.TabIndex = 88;
            // 
            // detailTactView15
            // 
            this.detailTactView15.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView15.Location = new System.Drawing.Point(1049, 508);
            this.detailTactView15.Name = "detailTactView15";
            this.detailTactView15.Size = new System.Drawing.Size(166, 110);
            this.detailTactView15.TabIndex = 87;
            // 
            // detailTactView10
            // 
            this.detailTactView10.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView10.Location = new System.Drawing.Point(1569, 391);
            this.detailTactView10.Name = "detailTactView10";
            this.detailTactView10.Size = new System.Drawing.Size(166, 110);
            this.detailTactView10.TabIndex = 86;
            // 
            // detailTactView9
            // 
            this.detailTactView9.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView9.Location = new System.Drawing.Point(1395, 391);
            this.detailTactView9.Name = "detailTactView9";
            this.detailTactView9.Size = new System.Drawing.Size(166, 110);
            this.detailTactView9.TabIndex = 85;
            // 
            // detailTactView8
            // 
            this.detailTactView8.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView8.Location = new System.Drawing.Point(1221, 391);
            this.detailTactView8.Name = "detailTactView8";
            this.detailTactView8.Size = new System.Drawing.Size(166, 110);
            this.detailTactView8.TabIndex = 84;
            // 
            // detailTactView7
            // 
            this.detailTactView7.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView7.Location = new System.Drawing.Point(1049, 391);
            this.detailTactView7.Name = "detailTactView7";
            this.detailTactView7.Size = new System.Drawing.Size(166, 110);
            this.detailTactView7.TabIndex = 83;
            // 
            // detailTactView6
            // 
            this.detailTactView6.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView6.Location = new System.Drawing.Point(875, 391);
            this.detailTactView6.Name = "detailTactView6";
            this.detailTactView6.Size = new System.Drawing.Size(166, 110);
            this.detailTactView6.TabIndex = 82;
            // 
            // detailTactView23
            // 
            this.detailTactView23.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView23.Location = new System.Drawing.Point(700, 742);
            this.detailTactView23.Name = "detailTactView23";
            this.detailTactView23.Size = new System.Drawing.Size(166, 110);
            this.detailTactView23.TabIndex = 81;
            // 
            // detailTactView22
            // 
            this.detailTactView22.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView22.Location = new System.Drawing.Point(526, 742);
            this.detailTactView22.Name = "detailTactView22";
            this.detailTactView22.Size = new System.Drawing.Size(166, 110);
            this.detailTactView22.TabIndex = 80;
            // 
            // detailTactView20
            // 
            this.detailTactView20.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView20.Location = new System.Drawing.Point(700, 626);
            this.detailTactView20.Name = "detailTactView20";
            this.detailTactView20.Size = new System.Drawing.Size(166, 110);
            this.detailTactView20.TabIndex = 79;
            // 
            // detailTactView19
            // 
            this.detailTactView19.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView19.Location = new System.Drawing.Point(526, 626);
            this.detailTactView19.Name = "detailTactView19";
            this.detailTactView19.Size = new System.Drawing.Size(166, 110);
            this.detailTactView19.TabIndex = 78;
            // 
            // detailTactView18
            // 
            this.detailTactView18.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView18.Location = new System.Drawing.Point(352, 626);
            this.detailTactView18.Name = "detailTactView18";
            this.detailTactView18.Size = new System.Drawing.Size(166, 110);
            this.detailTactView18.TabIndex = 77;
            // 
            // detailTactView14
            // 
            this.detailTactView14.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView14.Location = new System.Drawing.Point(700, 509);
            this.detailTactView14.Name = "detailTactView14";
            this.detailTactView14.Size = new System.Drawing.Size(166, 110);
            this.detailTactView14.TabIndex = 76;
            // 
            // detailTactView13
            // 
            this.detailTactView13.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView13.Location = new System.Drawing.Point(526, 509);
            this.detailTactView13.Name = "detailTactView13";
            this.detailTactView13.Size = new System.Drawing.Size(166, 110);
            this.detailTactView13.TabIndex = 75;
            // 
            // detailTactView12
            // 
            this.detailTactView12.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView12.Location = new System.Drawing.Point(352, 509);
            this.detailTactView12.Name = "detailTactView12";
            this.detailTactView12.Size = new System.Drawing.Size(166, 110);
            this.detailTactView12.TabIndex = 74;
            // 
            // detailTactView5
            // 
            this.detailTactView5.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView5.Location = new System.Drawing.Point(700, 391);
            this.detailTactView5.Name = "detailTactView5";
            this.detailTactView5.Size = new System.Drawing.Size(166, 110);
            this.detailTactView5.TabIndex = 73;
            // 
            // detailTactView4
            // 
            this.detailTactView4.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView4.Location = new System.Drawing.Point(526, 391);
            this.detailTactView4.Name = "detailTactView4";
            this.detailTactView4.Size = new System.Drawing.Size(166, 110);
            this.detailTactView4.TabIndex = 72;
            // 
            // detailTactView3
            // 
            this.detailTactView3.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView3.Location = new System.Drawing.Point(352, 391);
            this.detailTactView3.Name = "detailTactView3";
            this.detailTactView3.Size = new System.Drawing.Size(166, 110);
            this.detailTactView3.TabIndex = 71;
            // 
            // detailTactView11
            // 
            this.detailTactView11.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView11.Location = new System.Drawing.Point(7, 511);
            this.detailTactView11.Name = "detailTactView11";
            this.detailTactView11.Size = new System.Drawing.Size(166, 110);
            this.detailTactView11.TabIndex = 70;
            // 
            // detailTactView2
            // 
            this.detailTactView2.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView2.Location = new System.Drawing.Point(180, 391);
            this.detailTactView2.Name = "detailTactView2";
            this.detailTactView2.Size = new System.Drawing.Size(166, 110);
            this.detailTactView2.TabIndex = 69;
            // 
            // detailTactView1
            // 
            this.detailTactView1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.detailTactView1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailTactView1.Location = new System.Drawing.Point(6, 391);
            this.detailTactView1.Name = "detailTactView1";
            this.detailTactView1.Size = new System.Drawing.Size(166, 110);
            this.detailTactView1.TabIndex = 68;
            // 
            // cstCellCount3
            // 
            this.cstCellCount3.BackColor = System.Drawing.Color.DimGray;
            this.cstCellCount3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cstCellCount3.Location = new System.Drawing.Point(1638, 5);
            this.cstCellCount3.Name = "cstCellCount3";
            this.cstCellCount3.Size = new System.Drawing.Size(96, 380);
            this.cstCellCount3.TabIndex = 67;
            // 
            // cstCellCount4
            // 
            this.cstCellCount4.BackColor = System.Drawing.Color.DimGray;
            this.cstCellCount4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cstCellCount4.Location = new System.Drawing.Point(1533, 5);
            this.cstCellCount4.Name = "cstCellCount4";
            this.cstCellCount4.Size = new System.Drawing.Size(96, 380);
            this.cstCellCount4.TabIndex = 66;
            // 
            // cstCellCount2
            // 
            this.cstCellCount2.BackColor = System.Drawing.Color.DimGray;
            this.cstCellCount2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cstCellCount2.Location = new System.Drawing.Point(368, 5);
            this.cstCellCount2.Name = "cstCellCount2";
            this.cstCellCount2.Size = new System.Drawing.Size(96, 380);
            this.cstCellCount2.TabIndex = 65;
            // 
            // cstCellCount1
            // 
            this.cstCellCount1.BackColor = System.Drawing.Color.DimGray;
            this.cstCellCount1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cstCellCount1.Location = new System.Drawing.Point(263, 5);
            this.cstCellCount1.Name = "cstCellCount1";
            this.cstCellCount1.Size = new System.Drawing.Size(96, 380);
            this.cstCellCount1.TabIndex = 64;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.Controls.Add(this.splitContainer1);
            this.Name = "MainWindow";
            this.Size = new System.Drawing.Size(1880, 860);
            this.panel_runinfo.ResumeLayout(false);
            this.panel_runinfo.PerformLayout();
            this.gxtlaser_info.ResumeLayout(false);
            this.gxtlaser_info.PerformLayout();
            this.gxtmcr_count.ResumeLayout(false);
            this.panel_cstinfo.ResumeLayout(false);
            this.panel_cstinfo.PerformLayout();
            this.panel_day_info.ResumeLayout(false);
            this.panel_day_info.PerformLayout();
            this.gxtuld.ResumeLayout(false);
            this.gxtld.ResumeLayout(false);
            this.panel_run.ResumeLayout(false);
            this.panel_lot_b_info.ResumeLayout(false);
            this.panel_lot_b_info.PerformLayout();
            this.panel_lot_a_info.ResumeLayout(false);
            this.panel_lot_a_info.PerformLayout();
            this.panel_ld_muting_out.ResumeLayout(false);
            this.panelUldMutingIn.ResumeLayout(false);
            this.panel_ld_muting_in.ResumeLayout(false);
            this.panelUldMutingOut.ResumeLayout(false);
            this.panelDoor2.ResumeLayout(false);
            this.panel38.ResumeLayout(false);
            this.panelDoor3.ResumeLayout(false);
            this.panelDoor3.PerformLayout();
            this.panelDoor5.ResumeLayout(false);
            this.panelDoor5.PerformLayout();
            this.panelDoor4.ResumeLayout(false);
            this.panelDoor4.PerformLayout();
            this.panelDoor6.ResumeLayout(false);
            this.panelDoor6.PerformLayout();
            this.panelDoor8.ResumeLayout(false);
            this.panelDoor8.PerformLayout();
            this.panelDoor7.ResumeLayout(false);
            this.panelDoor7.PerformLayout();
            this.panelDoor9.ResumeLayout(false);
            this.panelDoor9.PerformLayout();
            this.panelLdBox.ResumeLayout(false);
            this.panelLdBox.PerformLayout();
            this.panelEms3.ResumeLayout(false);
            this.panelEms3.PerformLayout();
            this.panelProcessBox.ResumeLayout(false);
            this.panelProcessBox.PerformLayout();
            this.panelEms5.ResumeLayout(false);
            this.panelEms5.PerformLayout();
            this.panelUldBox.ResumeLayout(false);
            this.panelUldBox.PerformLayout();
            this.panel_run2.ResumeLayout(false);
            this.panel_run2.PerformLayout();
            this.panelEms1.ResumeLayout(false);
            this.panelEms1.PerformLayout();
            this.panelTargetAvgpower.ResumeLayout(false);
            this.panelTargetAvgpower.PerformLayout();
            this.panelZposGap.ResumeLayout(false);
            this.panelZposGap.PerformLayout();
            this.panelEms4.ResumeLayout(false);
            this.panelEms4.PerformLayout();
            this.panelLdOutB.ResumeLayout(false);
            this.panelLdInB.ResumeLayout(false);
            this.panelLdOutA.ResumeLayout(false);
            this.panelLdInA.ResumeLayout(false);
            this.panelUldInB.ResumeLayout(false);
            this.panelUldOutB.ResumeLayout(false);
            this.panelUldInA.ResumeLayout(false);
            this.panelUldOutA.ResumeLayout(false);
            this.panelUld1.ResumeLayout(false);
            this.panelUld1.PerformLayout();
            this.panelUld2.ResumeLayout(false);
            this.panelUld2.PerformLayout();
            this.panelUldTransA1.ResumeLayout(false);
            this.panelUldTransB1.ResumeLayout(false);
            this.panelUldTransA2.ResumeLayout(false);
            this.panel_uld_trans_b2.ResumeLayout(false);
            this.panelBreakTableA1.ResumeLayout(false);
            this.panelBreakTableB1.ResumeLayout(false);
            this.panelBreakTrans1.ResumeLayout(false);
            this.panelBreakTrans2.ResumeLayout(false);
            this.panelBreakTableA2.ResumeLayout(false);
            this.panel_break_table_b2.ResumeLayout(false);
            this.panelProcessTableA1.ResumeLayout(false);
            this.panelProcessTableB1.ResumeLayout(false);
            this.panelProcessTableA2.ResumeLayout(false);
            this.panelLd_Trans1.ResumeLayout(false);
            this.panelProcessTableB2.ResumeLayout(false);
            this.panelLd1.ResumeLayout(false);
            this.panelLd1.PerformLayout();
            this.panelLdTrans2.ResumeLayout(false);
            this.panelLd2.ResumeLayout(false);
            this.panelLd2.PerformLayout();
            this.panelShutterOpen.ResumeLayout(false);
            this.panelLock.ResumeLayout(false);
            this.panelLock.PerformLayout();
            this.panelLaserCover.ResumeLayout(false);
            this.panelLaserCover.PerformLayout();
            this.panelEms2.ResumeLayout(false);
            this.panelEms2.PerformLayout();
            this.panelEms6.ResumeLayout(false);
            this.panelEms6.PerformLayout();
            this.panelGrabEms1.ResumeLayout(false);
            this.panelGrabEms1.PerformLayout();
            this.panelGrabEms2.ResumeLayout(false);
            this.panelGrabEms2.PerformLayout();
            this.panelGrabSwitch1.ResumeLayout(false);
            this.panelGrabSwitch1.PerformLayout();
            this.panelGrabSwitch2.ResumeLayout(false);
            this.panelGrabSwitch2.PerformLayout();
            this.panelGrabEms3.ResumeLayout(false);
            this.panelGrabEms3.PerformLayout();
            this.panelGrabSwitch3.ResumeLayout(false);
            this.panelGrabSwitch3.PerformLayout();
            this.panel_door10.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel_camera_l_info.ResumeLayout(false);
            this.panel_camera_l_info.PerformLayout();
            this.panel_camera_r_info.ResumeLayout(false);
            this.panel_camera_r_info.PerformLayout();
            this.panel_align.ResumeLayout(false);
            this.panel_align.PerformLayout();
            this.gxtbreaking_count.ResumeLayout(false);
            this.gxtbreaking_count.PerformLayout();
            this.gxtdummy_tank.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bt_start;
        private System.Windows.Forms.Button btn_pause;
        private System.Windows.Forms.Button btn_stop;
        private System.Windows.Forms.Button btnKeyswitch;
        private System.Windows.Forms.Button btnPm;
        private System.Windows.Forms.Button btnDooropen;
        private System.Windows.Forms.Button btnLight;
        private System.Windows.Forms.Label lb_process_connect_info;
        private System.Windows.Forms.Panel panelCimConnectInfo;
        private System.Windows.Forms.Panel panelAjinConnectInfo;
        private System.Windows.Forms.Label lb_cim_connect_info;
        private System.Windows.Forms.Label lb_ajin_connect_info;
        private System.Windows.Forms.Panel panelUmacConnectInfo;
        private System.Windows.Forms.Label lb_umac_connect_info;
        private System.Windows.Forms.Panel panelLaserConnectInfo;
        private System.Windows.Forms.Panel panelSequenceConnectInfo;
        private System.Windows.Forms.Label lb_laser_connect_info;
        private System.Windows.Forms.Panel panelInsp1ConnectInfo;
        private System.Windows.Forms.Label lb_sequence_connect_info;
        private System.Windows.Forms.Label lb_insp1_connect_info;
        private System.Windows.Forms.Panel panelSerialConnectInfo;
        private System.Windows.Forms.Panel panelInsp2ConnectInfo;
        private System.Windows.Forms.Label lb_serial_connect_info;
        private System.Windows.Forms.Label lb_insp2_connect_info;
        private System.Windows.Forms.Panel panel_runinfo;
        private System.Windows.Forms.TextBox txttactTime;
        private System.Windows.Forms.TextBox txtrunCount;
        private System.Windows.Forms.TextBox txtrunTime;
        private System.Windows.Forms.TextBox txtstartTime;
        private System.Windows.Forms.TextBox txtcellSize;
        private System.Windows.Forms.TextBox txtprocess;
        private System.Windows.Forms.TextBox txtrunName;
        private System.Windows.Forms.TextBox txtrecipeName;
        private System.Windows.Forms.Label lb_tact_time;
        private System.Windows.Forms.Label lb_run_count;
        private System.Windows.Forms.Label lb_run_time;
        private System.Windows.Forms.Label lb_start_time;
        private System.Windows.Forms.Label lb_cell_size;
        private System.Windows.Forms.Label lb_process;
        private System.Windows.Forms.Label lb_recipe_name;
        private System.Windows.Forms.Label lb_run_name;
        private System.Windows.Forms.Label lb_run_info;
        private System.Windows.Forms.GroupBox gxtlaser_info;
        private System.Windows.Forms.Label lbShutter;
        private System.Windows.Forms.Label lb_shutter_info;
        private System.Windows.Forms.Label lbPower;
        private System.Windows.Forms.Label lb_power_info;
        private System.Windows.Forms.Label lbDivider;
        private System.Windows.Forms.Label lb_divider_info;
        private System.Windows.Forms.Label lbPd7;
        private System.Windows.Forms.Label lb_pd7power_info;
        private System.Windows.Forms.Label lbOutamplifier;
        private System.Windows.Forms.Label lbAmplifier;
        private System.Windows.Forms.Label lbBurst;
        private System.Windows.Forms.Label lbPulseMode;
        private System.Windows.Forms.Label lb_outamplifier_info;
        private System.Windows.Forms.Label lb_amplifier_info;
        private System.Windows.Forms.Label lb_burst_info;
        private System.Windows.Forms.Label lb_pulsemode_info;
        private System.Windows.Forms.GroupBox gxtmcr_count;
        private System.Windows.Forms.Panel panel_cstinfo;
        private System.Windows.Forms.Label lbCstTotalCount;
        private System.Windows.Forms.Label lb_cst_total_count_info;
        private System.Windows.Forms.Label lbCstReadCount;
        private System.Windows.Forms.Label lb_cst_read_count_info;
        private System.Windows.Forms.Label lb_cst;
        private System.Windows.Forms.Panel panel_day_info;
        private System.Windows.Forms.Label lbDayTotalCount;
        private System.Windows.Forms.Label lb_day_total_count_info;
        private System.Windows.Forms.Label lbDayReadCount;
        private System.Windows.Forms.Label lb_day_read_count_info;
        private System.Windows.Forms.Label lb_day;
        private System.Windows.Forms.GroupBox gxtuld;
        private System.Windows.Forms.GroupBox gxtld;
        private System.Windows.Forms.Button btn_safety_reset;
        private System.Windows.Forms.Panel panel_run;
        private System.Windows.Forms.Panel panel_ld_muting_out;
        private System.Windows.Forms.Label lbLdMutingOut;
        private System.Windows.Forms.Panel panelUldMutingIn;
        private System.Windows.Forms.Label lbUldMutingIn;
        private System.Windows.Forms.Panel panel_ld_muting_in;
        private System.Windows.Forms.Label lbLdMutingIn;
        private System.Windows.Forms.Panel panelUldMutingOut;
        private System.Windows.Forms.Label lbUldMuting_out;
        private System.Windows.Forms.Panel panelDoor2;
        private System.Windows.Forms.Label lbDoor2;
        private System.Windows.Forms.Panel panel38;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Panel panelDoor3;
        private System.Windows.Forms.Label lb_door3;
        private System.Windows.Forms.Panel panelDoor5;
        private System.Windows.Forms.Label lbDoor5;
        private System.Windows.Forms.Panel panelDoor4;
        private System.Windows.Forms.Label lb_door4;
        private System.Windows.Forms.Panel panelDoor6;
        private System.Windows.Forms.Label lb_door6;
        private System.Windows.Forms.Panel panelDoor8;
        private System.Windows.Forms.Label lb_door8;
        private System.Windows.Forms.Panel panelDoor7;
        private System.Windows.Forms.Label lb_door7;
        private System.Windows.Forms.Panel panelDoor9;
        private System.Windows.Forms.Label lb_door9;
        private System.Windows.Forms.Panel panelLdBox;
        private System.Windows.Forms.Label lb_ld_box;
        private System.Windows.Forms.Panel panelEms3;
        private System.Windows.Forms.Label lb_ems3;
        private System.Windows.Forms.Panel panelProcessBox;
        private System.Windows.Forms.Label lb_process_box;
        private System.Windows.Forms.Panel panelEms5;
        private System.Windows.Forms.Label lb_ems5;
        private System.Windows.Forms.Panel panelUldBox;
        private System.Windows.Forms.Label lb_uld_box;
        private System.Windows.Forms.Panel panel_run2;
        private System.Windows.Forms.Panel panelEms1;
        private System.Windows.Forms.Label lb_ems1;
        private System.Windows.Forms.Panel panelTargetAvgpower;
        private System.Windows.Forms.Label lbAvgpower;
        private System.Windows.Forms.Label lb_avgpower_info;
        private System.Windows.Forms.Label lbTartget;
        private System.Windows.Forms.Label lb_target_info;
        private System.Windows.Forms.Panel panelZposGap;
        private System.Windows.Forms.Label lbZposGap;
        private System.Windows.Forms.Label lb_zpos_gap_info;
        private System.Windows.Forms.Panel panelEms4;
        private System.Windows.Forms.Label lb_ems4;
        private System.Windows.Forms.Panel panelLdOutB;
        private System.Windows.Forms.Label lb_ld_out_b;
        private System.Windows.Forms.Panel panelLdInB;
        private System.Windows.Forms.Label lb_ld_in_b;
        private System.Windows.Forms.Panel panelLdOutA;
        private System.Windows.Forms.Label lb_ld_out_a;
        private System.Windows.Forms.Panel panelLdInA;
        private System.Windows.Forms.Label lb_ld_in_a;
        private System.Windows.Forms.Panel panelUldInB;
        private System.Windows.Forms.Label lb_uld_in_b;
        private System.Windows.Forms.Panel panelUldOutB;
        private System.Windows.Forms.Label lb_uld_out_b;
        private System.Windows.Forms.Panel panelUldInA;
        private System.Windows.Forms.Label lb_uld_in_a;
        private System.Windows.Forms.Panel panelUldOutA;
        private System.Windows.Forms.Label lb_uld_out_a;
        private System.Windows.Forms.Panel panelUld1Out2;
        private System.Windows.Forms.Panel panelUld2Out2;
        private System.Windows.Forms.Panel panelUld1Table;
        private System.Windows.Forms.Panel panelUld2Table;
        private System.Windows.Forms.Panel panelUld1Out1;
        private System.Windows.Forms.Panel panelUld2Out1;
        private System.Windows.Forms.Panel panelUld1;
        private System.Windows.Forms.Label lb_uld1;
        private System.Windows.Forms.Panel panelUld2;
        private System.Windows.Forms.Label lb_uld2;
        private System.Windows.Forms.Panel panelUldTransA1;
        private System.Windows.Forms.Label lb_uld_trans_a1;
        private System.Windows.Forms.Panel panelUldTransB1;
        private System.Windows.Forms.Label lb_uld_trans_b1;
        private System.Windows.Forms.Panel panelUldTransA2;
        private System.Windows.Forms.Label lb_uld_trans_a2;
        private System.Windows.Forms.Panel panel_uld_trans_b2;
        private System.Windows.Forms.Label lbUldTransB2;
        private System.Windows.Forms.Panel panelBreakTableA1;
        private System.Windows.Forms.Label lb_break_table_a1;
        private System.Windows.Forms.Panel panelBreakTableB1;
        private System.Windows.Forms.Label lb_break_table_b1;
        private System.Windows.Forms.Panel panelBreakTrans1;
        private System.Windows.Forms.Label lb_break_trans1;
        private System.Windows.Forms.Panel panelBreakTrans2;
        private System.Windows.Forms.Label lb_break_trans2;
        private System.Windows.Forms.Panel panelBreakTableA2;
        private System.Windows.Forms.Label lb_break_table_a2;
        private System.Windows.Forms.Panel panel_break_table_b2;
        private System.Windows.Forms.Label lbBreakTableB2;
        private System.Windows.Forms.Panel panelProcessTableA1;
        private System.Windows.Forms.Label lb_process_table_a1;
        private System.Windows.Forms.Panel panelProcessTableB1;
        private System.Windows.Forms.Label lb_process_table_b1;
        private System.Windows.Forms.Panel panelProcessTableA2;
        private System.Windows.Forms.Label lb_process_table_a2;
        private System.Windows.Forms.Panel panelLd_Trans1;
        private System.Windows.Forms.Label lb_ld_trans1;
        private System.Windows.Forms.Panel panelProcessTableB2;
        private System.Windows.Forms.Label lb_process_table_b2;
        private System.Windows.Forms.Panel panelLd1;
        private System.Windows.Forms.Label lb_ld1;
        private System.Windows.Forms.Panel panelLdTrans2;
        private System.Windows.Forms.Label lb_ld_trans2;
        private System.Windows.Forms.Panel panelLd1Table;
        private System.Windows.Forms.Panel panelLd2;
        private System.Windows.Forms.Label lbLD2;
        private System.Windows.Forms.Panel panelL1In2;
        private System.Windows.Forms.Panel panelLd2Table;
        private System.Windows.Forms.Panel panelLd1In1;
        private System.Windows.Forms.Panel panelLd2In2;
        private System.Windows.Forms.Panel panelLd2In1;
        private System.Windows.Forms.Label lbLd1OTime;
        private System.Windows.Forms.Label lb_ld1_o;
        private System.Windows.Forms.Label lbLd1ITime;
        private System.Windows.Forms.Label lb_ld1_i;
        private System.Windows.Forms.Label lbLd2OTime;
        private System.Windows.Forms.Label lb_ld2_o;
        private System.Windows.Forms.Label lbLd2ITime;
        private System.Windows.Forms.Label lb_ld2_i;
        private System.Windows.Forms.Label lbUld1ITime;
        private System.Windows.Forms.Label lb_uld1_i;
        private System.Windows.Forms.Label lbUld1OTime;
        private System.Windows.Forms.Label lb_uld1_o;
        private System.Windows.Forms.Label lbUld2ITime;
        private System.Windows.Forms.Label lb_uld2_i;
        private System.Windows.Forms.Label lbUld2OTime;
        private System.Windows.Forms.Label lb_uld2_o;
        private System.Windows.Forms.Panel panelShutterOpen;
        private System.Windows.Forms.Label lb_shutter_open;
        private System.Windows.Forms.Panel panelLock;
        private System.Windows.Forms.Label lb_lock;
        private System.Windows.Forms.Panel panelLaserCover;
        private System.Windows.Forms.Label lb_laser_cover;
        private System.Windows.Forms.Panel panelEms2;
        private System.Windows.Forms.Label lb_ems2;
        private System.Windows.Forms.Panel panelEms6;
        private System.Windows.Forms.Label lb_ems6;
        private System.Windows.Forms.Panel panelGrabEms1;
        private System.Windows.Forms.Label lb_grab_ems1;
        private System.Windows.Forms.Panel panelGrabEms2;
        private System.Windows.Forms.Label lb_grab_ems2;
        private System.Windows.Forms.Panel panelGrabSwitch1;
        private System.Windows.Forms.Label lb_grab_switch1;
        private System.Windows.Forms.Panel panelGrabSwitch2;
        private System.Windows.Forms.Label lb_grab_switch2;
        private System.Windows.Forms.Panel panelGrabEms3;
        private System.Windows.Forms.Label lb_grab_ems3;
        private System.Windows.Forms.Panel panelGrabSwitch3;
        private System.Windows.Forms.Label lb_grab_switch3;
        private System.Windows.Forms.Panel panel_door10;
        private System.Windows.Forms.Label lbDoor10;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Panel panel_camera_l_info;
        private System.Windows.Forms.Label lb_camera_l;
        private System.Windows.Forms.Panel panel_camera_l;
        private System.Windows.Forms.Panel panel_camera_r_info;
        private System.Windows.Forms.Label lb_camera_r;
        private System.Windows.Forms.Panel panel_camera_r;
        private System.Windows.Forms.Panel panel_align;
        private System.Windows.Forms.TextBox txtaligBbreak4OffsetA;
        private System.Windows.Forms.TextBox txtalignBreak3OffsetA;
        private System.Windows.Forms.TextBox txtalignPre2OffsetA;
        private System.Windows.Forms.Label lb_align_offset_a2;
        private System.Windows.Forms.TextBox txtalignBreak4OffsetY;
        private System.Windows.Forms.TextBox txtalignBreak3OffsetY;
        private System.Windows.Forms.TextBox txtalignPre2OffsetY;
        private System.Windows.Forms.Label lb_align_offset_y2;
        private System.Windows.Forms.TextBox txtalignBreak4OffsetX;
        private System.Windows.Forms.TextBox txtalignBreak3OffsetX;
        private System.Windows.Forms.TextBox txtalignPre2OffsetX;
        private System.Windows.Forms.Label lb_align_offset_x2;
        private System.Windows.Forms.TextBox txtalignBreak4Result;
        private System.Windows.Forms.TextBox txtalignBreak3Result;
        private System.Windows.Forms.TextBox txtalignPre2Result;
        private System.Windows.Forms.Label lb_align_result2;
        private System.Windows.Forms.Label lb_align_break4;
        private System.Windows.Forms.Label lb_align_break3;
        private System.Windows.Forms.Label lb_align_pre2;
        private System.Windows.Forms.Label lb_align_name2;
        private System.Windows.Forms.TextBox txtalignBreak2OffsetA;
        private System.Windows.Forms.TextBox txtalignBreak1Offset_A;
        private System.Windows.Forms.TextBox txtalignPre1OffsetA;
        private System.Windows.Forms.Label lb_align_offset_a1;
        private System.Windows.Forms.TextBox txtalignBreak2OffsetY;
        private System.Windows.Forms.TextBox txtalignBreak1OffsetY;
        private System.Windows.Forms.TextBox txtalignPre1OffsetY;
        private System.Windows.Forms.Label lb_align_offset_y1;
        private System.Windows.Forms.TextBox txtalignBreak2OffsetX;
        private System.Windows.Forms.TextBox txtalignBreak1OffsetX;
        private System.Windows.Forms.TextBox txtalignPre1OffsetX;
        private System.Windows.Forms.Label lb_align_offset_x1;
        private System.Windows.Forms.TextBox txtalignBreak2Result;
        private System.Windows.Forms.TextBox txtalignBreak1Result;
        private System.Windows.Forms.TextBox txtalignPre1Result;
        private System.Windows.Forms.Label lb_align_result1;
        private System.Windows.Forms.Label lb_align_break2;
        private System.Windows.Forms.Label lb_align_break1;
        private System.Windows.Forms.Label lb_align_pre1;
        private System.Windows.Forms.Label lb_align_name1;
        private System.Windows.Forms.GroupBox gxtbreaking_count;
        private System.Windows.Forms.Label lb_breaking_count;
        private System.Windows.Forms.Label lb_breaking_count_info;
        private System.Windows.Forms.GroupBox gxtdummy_tank;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel_lot_a_info;
        private System.Windows.Forms.Label lb_lot_a;
        private System.Windows.Forms.Label lb_lot_a_info;
        private System.Windows.Forms.Panel panel_lot_b_info;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lb_lot_b_info;
        private Main.DetailTactView detailTactView1;
        private Main.CstCellCount cstCellCount3;
        private Main.CstCellCount cstCellCount4;
        private Main.CstCellCount cstCellCount2;
        private Main.CstCellCount cstCellCount1;
        private Main.DetailTactView detailTactView21;
        private Main.DetailTactView detailTactView17;
        private Main.DetailTactView detailTactView16;
        private Main.DetailTactView detailTactView15;
        private Main.DetailTactView detailTactView10;
        private Main.DetailTactView detailTactView9;
        private Main.DetailTactView detailTactView8;
        private Main.DetailTactView detailTactView7;
        private Main.DetailTactView detailTactView6;
        private Main.DetailTactView detailTactView23;
        private Main.DetailTactView detailTactView22;
        private Main.DetailTactView detailTactView20;
        private Main.DetailTactView detailTactView19;
        private Main.DetailTactView detailTactView18;
        private Main.DetailTactView detailTactView14;
        private Main.DetailTactView detailTactView13;
        private Main.DetailTactView detailTactView12;
        private Main.DetailTactView detailTactView5;
        private Main.DetailTactView detailTactView4;
        private Main.DetailTactView detailTactView3;
        private Main.DetailTactView detailTactView11;
        private Main.DetailTactView detailTactView2;
        private System.Windows.Forms.Button btnUldOutMuting;
        private System.Windows.Forms.Button btnUldInMuting;
        private System.Windows.Forms.Button btnLdOutMuting;
        private System.Windows.Forms.Button btnLdInMuting;
        private System.Windows.Forms.Button btnACSTSkip;
        private System.Windows.Forms.Button btnBCSTSkip;
        private System.Windows.Forms.Button btnBreakingCountClear;
        private System.Windows.Forms.Button btnDummyTankSet;
        private System.Windows.Forms.Button btnDummyTankGet;
        private System.Windows.Forms.Button btnLdLight;
        private System.Windows.Forms.Button btnUldLight;
        private System.Windows.Forms.Button btnProcessLight;
    }
}
