﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DIT.TLC.CTRL;

namespace DIT.TLC.UI
{
    public partial class UcrlManagerSubMenu : UserControl
    {
        private UcrlManagerServoManualCtrl urcrMgrSevoManualCtrl = new UcrlManagerServoManualCtrl();    // 메뉴얼제어 UI

        private Manager_Reset manager_resetform = new Manager_Reset();                  // 레시피-장치초기화 UI
        private Manager_IOStatus manager_ioform = new Manager_IOStatus();               // 레시피-IO Status UI
        private Manager_TactTime manager_tacttime = new Manager_TactTime();             // tacttime UI

        private Manager_Control manager_control = new Manager_Control();
        private Manager_Laser manager_laser = new Manager_Laser();

        public UcrlManagerSubMenu()
        {
            InitializeComponent();

            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(manager_resetform);
            manager_resetform.Location = new Point(0, 0);
        }

        public void InitalizeUserControlUI(Equipment equip)
        {
            urcrMgrSevoManualCtrl.InitalizeUserControlUI(equip);
        }

        // 서브메뉴 버튼 클릭시 색 변경 함수
        Button SelectedButton = null;
        private void SubMenuButtonColorChange(object sender, EventArgs e)
        {
            if (SelectedButton != null)
                SelectedButton.BackColor = Color.DarkSlateGray;

            SelectedButton = sender as Button;
            SelectedButton.BackColor = Color.Blue;
        }

        // 장치 초기화 버튼 클릭
        private void btn_reset_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(manager_resetform);
            manager_resetform.Location = new Point(0, 0);

            SubMenuButtonColorChange(sender, e);
        }

        // IO Status 버튼 클릭
        private void btn_io_status_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(manager_ioform);
            manager_ioform.Location = new Point(0, 0);

            SubMenuButtonColorChange(sender, e);
        }

        // AIO Status 버튼 클릭
        private void btn_aio_status_Click(object sender, EventArgs e)
        {


            SubMenuButtonColorChange(sender, e);
        }

        // Motor Status 버튼 클릭
        private void btn_motor_status_Click(object sender, EventArgs e)
        {


            SubMenuButtonColorChange(sender, e);
        }

        // 메뉴얼 제어 버튼 클릭
        private void btn_manual_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(urcrMgrSevoManualCtrl);

            SubMenuButtonColorChange(sender, e);
        }

        // SEM 버튼 클릭
        private void btn_sem_Click(object sender, EventArgs e)
        {


            SubMenuButtonColorChange(sender, e);
        }

        // TactTime 버튼 클릭
        private void btn_tactime_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(manager_tacttime);

            SubMenuButtonColorChange(sender, e);
        }

        // 레이저 제어 버튼 클릭
        private void btn_laser_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(manager_laser);

            SubMenuButtonColorChange(sender, e);
        }

        // Control 버튼 클릭
        private void btn_control_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1.Controls.Clear();
            splitContainer1.Panel1.Controls.Add(manager_control);

            SubMenuButtonColorChange(sender, e);
        }

        // Process 버튼 클릭
        private void btn_process_Click(object sender, EventArgs e)
        {
            new FrmManager_Process().ShowDialog();

            SubMenuButtonColorChange(sender, e);
        }

        // CutLineTracker 버튼 클릭
        private void btn_cutlinetracker_Click(object sender, EventArgs e)
        {
            new FrmCutLineTracker().ShowDialog();

            SubMenuButtonColorChange(sender, e);
        }
    }
}
