﻿namespace DIT.TLC.UI
{
    partial class Parameter_System
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_Mode = new System.Windows.Forms.Label();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.gbox_Mode_DistributionMode = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_Mode_DistributionMode_DontUse = new System.Windows.Forms.Button();
            this.tbox_Mode_DistributionMode = new System.Windows.Forms.TextBox();
            this.btn_Mode_DistributionMode_Use = new System.Windows.Forms.Button();
            this.gbox_SimulationMode = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_SimulationMode_DontUse = new System.Windows.Forms.Button();
            this.tbox_SimulationMode = new System.Windows.Forms.TextBox();
            this.btn_SimulationMode_Use = new System.Windows.Forms.Button();
            this.gbox_SkipMode = new System.Windows.Forms.GroupBox();
            this.gbox_DoorSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel31 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_DoorSkip_DontUSe = new System.Windows.Forms.Button();
            this.tbox_DoorSkip = new System.Windows.Forms.TextBox();
            this.btn_DoorSkip_Use = new System.Windows.Forms.Button();
            this.gbox_GrabSwitchSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel30 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_GrabSwitchSkip_DontUse = new System.Windows.Forms.Button();
            this.tbox_GrabSwitchSkip = new System.Windows.Forms.TextBox();
            this.btn_GrabSwitchSkip_Use = new System.Windows.Forms.Button();
            this.gbox_McDownSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel29 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_McDownSkip_DontUse = new System.Windows.Forms.Button();
            this.tbox_McDownSkip = new System.Windows.Forms.TextBox();
            this.btn_McDownSkip_Use = new System.Windows.Forms.Button();
            this.gbox_TeachSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel28 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_TeachSkip_DontUse = new System.Windows.Forms.Button();
            this.tbox_TeachSkip = new System.Windows.Forms.TextBox();
            this.btn_TeachSkip_Use = new System.Windows.Forms.Button();
            this.gbox_MutingSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel27 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_MutingSkip_DontUse = new System.Windows.Forms.Button();
            this.tbox_MutingSkip = new System.Windows.Forms.TextBox();
            this.btn_MutingSkip_Use = new System.Windows.Forms.Button();
            this.gbox_LaserMeasureSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel26 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_LaserMeasureSkip_DontUse = new System.Windows.Forms.Button();
            this.tbox_LaserMeasureSkip = new System.Windows.Forms.TextBox();
            this.btn_LaserMeasureSkip_Use = new System.Windows.Forms.Button();
            this.gbox_UnloaderInputSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel21 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel24 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_UnloaderInputSkipB_DontUse = new System.Windows.Forms.Button();
            this.tbox_UnloaderInputSkipB = new System.Windows.Forms.TextBox();
            this.btn_UnloaderInputSkipB_Use = new System.Windows.Forms.Button();
            this.tableLayoutPanel25 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_UnloaderInputSkipA_DontUse = new System.Windows.Forms.Button();
            this.tbox_UnloaderInputSkipA = new System.Windows.Forms.TextBox();
            this.btn_UnloaderInputSkipA_Use = new System.Windows.Forms.Button();
            this.gbox_LoaderInputSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel20 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel23 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_LoaderInputSkipB_DontUse = new System.Windows.Forms.Button();
            this.tbox_LoaderInputSkipB = new System.Windows.Forms.TextBox();
            this.btn_LoaderInputSkipB_Use = new System.Windows.Forms.Button();
            this.tableLayoutPanel22 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_LoaderInputSkipA_DontUse = new System.Windows.Forms.Button();
            this.tbox_LoaderInputSkipA = new System.Windows.Forms.TextBox();
            this.btn_LoaderInputSkipA_Use = new System.Windows.Forms.Button();
            this.gbox_BufferPickerSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel19 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_BufferPickerSkip_DontUse = new System.Windows.Forms.Button();
            this.tbox_BufferPickerSkip = new System.Windows.Forms.TextBox();
            this.btn_BufferPickerSkip_Use = new System.Windows.Forms.Button();
            this.gbox_TiltMeasureSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel18 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_TiltMeasureSkip_DontUse = new System.Windows.Forms.Button();
            this.tbox_TiltMeasureSkip = new System.Windows.Forms.TextBox();
            this.btn_TiltMeasureSkip_Use = new System.Windows.Forms.Button();
            this.gbox_BreakingAlignSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel17 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_BreakingAlignSkip_DontUse = new System.Windows.Forms.Button();
            this.tbox_BreakingAlignSkip = new System.Windows.Forms.TextBox();
            this.btn_BreakingAlignSkip_Use = new System.Windows.Forms.Button();
            this.gbox_BcrSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_BcrSkip_DontUse = new System.Windows.Forms.Button();
            this.tbox_BcrSkip = new System.Windows.Forms.TextBox();
            this.btn_BcrSkip_Use = new System.Windows.Forms.Button();
            this.gbox_McrSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_McrSkip_DontUse = new System.Windows.Forms.Button();
            this.tbox_McrSkip = new System.Windows.Forms.TextBox();
            this.btn_McrSkip_Use = new System.Windows.Forms.Button();
            this.gbox_InspectionSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_InspectionSkip_DontUse = new System.Windows.Forms.Button();
            this.tbox_InspectionSkip = new System.Windows.Forms.TextBox();
            this.btn_InspectionSkip_Use = new System.Windows.Forms.Button();
            this.gbox_BreakingMotionSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_BreakingMotionSkip_DontUse = new System.Windows.Forms.Button();
            this.tbox_BreakingMotionSkip = new System.Windows.Forms.TextBox();
            this.btn_BreakingMotionSkip_Use = new System.Windows.Forms.Button();
            this.gbox_LaserSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_LaserSkip_DontUse = new System.Windows.Forms.Button();
            this.tbox_LaserSkip = new System.Windows.Forms.TextBox();
            this.btn_LaserSkip_Use = new System.Windows.Forms.Button();
            this.gbox_BreakingBSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_BreakingBSkip_DontUse = new System.Windows.Forms.Button();
            this.tbox_BreakingBSkip = new System.Windows.Forms.TextBox();
            this.btn_BreakingBSkip_Use = new System.Windows.Forms.Button();
            this.gbox_BreakingASkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_BreakingASkip_DontUse = new System.Windows.Forms.Button();
            this.tbox_BreakingASkip = new System.Windows.Forms.TextBox();
            this.btn_BreakingASkip_Use = new System.Windows.Forms.Button();
            this.gbox_ProcessBSkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_ProcessBSkip_DontUse = new System.Windows.Forms.Button();
            this.tbox_ProcessBSkip = new System.Windows.Forms.TextBox();
            this.btn_ProcessBSkip_Use = new System.Windows.Forms.Button();
            this.gbox_ProcessASkip = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_ProcessASkip_DontUse = new System.Windows.Forms.Button();
            this.tbox_ProcessASkip = new System.Windows.Forms.TextBox();
            this.btn_ProcessASkip_Use = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel32 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_Setting = new System.Windows.Forms.Label();
            this.tableLayoutPanel33 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel34 = new System.Windows.Forms.TableLayoutPanel();
            this.gbox_AutoDeleteCycle = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel40 = new System.Windows.Forms.TableLayoutPanel();
            this.tbox_AutoDeleteCycle = new System.Windows.Forms.TextBox();
            this.lbl_AutoDeleteCycle = new System.Windows.Forms.Label();
            this.gbox_BreakingCount = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel39 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_CriticalAlarm = new System.Windows.Forms.Label();
            this.tbox_LightAlarm = new System.Windows.Forms.TextBox();
            this.lbl_LightAlarm = new System.Windows.Forms.Label();
            this.tbox_CriticalAlarm = new System.Windows.Forms.TextBox();
            this.gbox_DoorOpenSpeed = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel38 = new System.Windows.Forms.TableLayoutPanel();
            this.tbox_DoorOpenSpeed = new System.Windows.Forms.TextBox();
            this.lbl_DoorOpenSpped = new System.Windows.Forms.Label();
            this.gbox_CountError = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel37 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_INSP = new System.Windows.Forms.Label();
            this.tbox_MCR = new System.Windows.Forms.TextBox();
            this.lbl_MCR = new System.Windows.Forms.Label();
            this.tbox_INSP = new System.Windows.Forms.TextBox();
            this.gbox_Timeout = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel36 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_MutingTimeout = new System.Windows.Forms.Label();
            this.tbox_VacuumTimeout = new System.Windows.Forms.TextBox();
            this.lbl_VacuumTimeout = new System.Windows.Forms.Label();
            this.tbox_CylinderTimeout = new System.Windows.Forms.TextBox();
            this.tbox_PreAlignTimeout = new System.Windows.Forms.TextBox();
            this.tbox_BreakingAlignTimeout = new System.Windows.Forms.TextBox();
            this.tbox_InspectiongTimeout = new System.Windows.Forms.TextBox();
            this.tbox_SequenceMoveTimeout = new System.Windows.Forms.TextBox();
            this.tbox_MutingTimeout = new System.Windows.Forms.TextBox();
            this.lbl_CylinderTimeout = new System.Windows.Forms.Label();
            this.lbl_PreAlignTimeout = new System.Windows.Forms.Label();
            this.lbl_BreakingAlignTimeout = new System.Windows.Forms.Label();
            this.lbl_InspectionTimeout = new System.Windows.Forms.Label();
            this.lbl_SequenceMoveTimeout = new System.Windows.Forms.Label();
            this.gbox_Setting_DistributionMode = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel35 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_BreakingAlignSpeed = new System.Windows.Forms.Label();
            this.lbl_FineAlignSpeed = new System.Windows.Forms.Label();
            this.tbox_PreAlignSpeed = new System.Windows.Forms.TextBox();
            this.lbl_PreAlignSpeed = new System.Windows.Forms.Label();
            this.tbox_FineAlignSpeed = new System.Windows.Forms.TextBox();
            this.tbox_BreakingAlignSpeed = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel41 = new System.Windows.Forms.TableLayoutPanel();
            this.gbox_LDS = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel48 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_ErrorRange = new System.Windows.Forms.Label();
            this.lbl_WaitingTime = new System.Windows.Forms.Label();
            this.tbox_LDS_MeasureCycle = new System.Windows.Forms.TextBox();
            this.lbl_LDS_MeasureCycle = new System.Windows.Forms.Label();
            this.tbox_WaitingTime = new System.Windows.Forms.TextBox();
            this.lbl_LDS_MeasureTime = new System.Windows.Forms.Label();
            this.tbox_LDS_MeasureTime = new System.Windows.Forms.TextBox();
            this.tbox_ErrorRange = new System.Windows.Forms.TextBox();
            this.gbox_TemperatureAlarm = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel42 = new System.Windows.Forms.TableLayoutPanel();
            this.tbox_TemperatureLightAlarm = new System.Windows.Forms.TextBox();
            this.tbox_TemperatureCriticalAlarm = new System.Windows.Forms.TextBox();
            this.lbl_TemperatureAlarm = new System.Windows.Forms.Label();
            this.gbox_MeasureCycle = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel43 = new System.Windows.Forms.TableLayoutPanel();
            this.tbox_CutLineMeasure = new System.Windows.Forms.TextBox();
            this.lbl_CutLineMeasure = new System.Windows.Forms.Label();
            this.gbox_BlowingCheck = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel44 = new System.Windows.Forms.TableLayoutPanel();
            this.tbox_BlowingCheck = new System.Windows.Forms.TextBox();
            this.lbl_BlowingCheck = new System.Windows.Forms.Label();
            this.gbox_RetryCount = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel45 = new System.Windows.Forms.TableLayoutPanel();
            this.tbox_RetryCount = new System.Windows.Forms.TextBox();
            this.lbl_RetryCount = new System.Windows.Forms.Label();
            this.gbox_LaserPD7Error = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel46 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_LaserPD7ErrorRange = new System.Windows.Forms.Label();
            this.tbox_LaserPD7Power = new System.Windows.Forms.TextBox();
            this.lbl_LaserPD7Power = new System.Windows.Forms.Label();
            this.tbox_LaserPD7ErrorRange = new System.Windows.Forms.TextBox();
            this.gbox_LaserPowerMeasure = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel47 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_TargetErrorRange = new System.Windows.Forms.Label();
            this.tbox_MeasureCycle = new System.Windows.Forms.TextBox();
            this.lbl_LaserPowerMeasure_MeasureCycle = new System.Windows.Forms.Label();
            this.tbox_TargetPEC = new System.Windows.Forms.TextBox();
            this.tbox_TargetPower = new System.Windows.Forms.TextBox();
            this.tbox_WarmUpTime = new System.Windows.Forms.TextBox();
            this.tbox_MeasureTime = new System.Windows.Forms.TextBox();
            this.tbox_CorrectionErrorRange = new System.Windows.Forms.TextBox();
            this.tbox_TargetErrorRange = new System.Windows.Forms.TextBox();
            this.lbl_TargetPEC = new System.Windows.Forms.Label();
            this.lbl_TargetPower = new System.Windows.Forms.Label();
            this.lbl_WarmUpTime = new System.Windows.Forms.Label();
            this.lbl_MeasureTime = new System.Windows.Forms.Label();
            this.lbl_CorrectionErrorRange = new System.Windows.Forms.Label();
            this.btn_Save = new System.Windows.Forms.Button();
            this.btn_AlarmClear = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.gbox_Mode_DistributionMode.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.gbox_SimulationMode.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.gbox_SkipMode.SuspendLayout();
            this.gbox_DoorSkip.SuspendLayout();
            this.tableLayoutPanel31.SuspendLayout();
            this.gbox_GrabSwitchSkip.SuspendLayout();
            this.tableLayoutPanel30.SuspendLayout();
            this.gbox_McDownSkip.SuspendLayout();
            this.tableLayoutPanel29.SuspendLayout();
            this.gbox_TeachSkip.SuspendLayout();
            this.tableLayoutPanel28.SuspendLayout();
            this.gbox_MutingSkip.SuspendLayout();
            this.tableLayoutPanel27.SuspendLayout();
            this.gbox_LaserMeasureSkip.SuspendLayout();
            this.tableLayoutPanel26.SuspendLayout();
            this.gbox_UnloaderInputSkip.SuspendLayout();
            this.tableLayoutPanel21.SuspendLayout();
            this.tableLayoutPanel24.SuspendLayout();
            this.tableLayoutPanel25.SuspendLayout();
            this.gbox_LoaderInputSkip.SuspendLayout();
            this.tableLayoutPanel20.SuspendLayout();
            this.tableLayoutPanel23.SuspendLayout();
            this.tableLayoutPanel22.SuspendLayout();
            this.gbox_BufferPickerSkip.SuspendLayout();
            this.tableLayoutPanel19.SuspendLayout();
            this.gbox_TiltMeasureSkip.SuspendLayout();
            this.tableLayoutPanel18.SuspendLayout();
            this.gbox_BreakingAlignSkip.SuspendLayout();
            this.tableLayoutPanel17.SuspendLayout();
            this.gbox_BcrSkip.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            this.gbox_McrSkip.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.gbox_InspectionSkip.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.gbox_BreakingMotionSkip.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.gbox_LaserSkip.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.gbox_BreakingBSkip.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.gbox_BreakingASkip.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.gbox_ProcessBSkip.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.gbox_ProcessASkip.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel32.SuspendLayout();
            this.tableLayoutPanel33.SuspendLayout();
            this.tableLayoutPanel34.SuspendLayout();
            this.gbox_AutoDeleteCycle.SuspendLayout();
            this.tableLayoutPanel40.SuspendLayout();
            this.gbox_BreakingCount.SuspendLayout();
            this.tableLayoutPanel39.SuspendLayout();
            this.gbox_DoorOpenSpeed.SuspendLayout();
            this.tableLayoutPanel38.SuspendLayout();
            this.gbox_CountError.SuspendLayout();
            this.tableLayoutPanel37.SuspendLayout();
            this.gbox_Timeout.SuspendLayout();
            this.tableLayoutPanel36.SuspendLayout();
            this.gbox_Setting_DistributionMode.SuspendLayout();
            this.tableLayoutPanel35.SuspendLayout();
            this.tableLayoutPanel41.SuspendLayout();
            this.gbox_LDS.SuspendLayout();
            this.tableLayoutPanel48.SuspendLayout();
            this.gbox_TemperatureAlarm.SuspendLayout();
            this.tableLayoutPanel42.SuspendLayout();
            this.gbox_MeasureCycle.SuspendLayout();
            this.tableLayoutPanel43.SuspendLayout();
            this.gbox_BlowingCheck.SuspendLayout();
            this.tableLayoutPanel44.SuspendLayout();
            this.gbox_RetryCount.SuspendLayout();
            this.tableLayoutPanel45.SuspendLayout();
            this.gbox_LaserPD7Error.SuspendLayout();
            this.tableLayoutPanel46.SuspendLayout();
            this.gbox_LaserPowerMeasure.SuspendLayout();
            this.tableLayoutPanel47.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 0, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.513872F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 93.48613F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(865, 821);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.SystemColors.Highlight;
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.5F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.01746F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.49127F));
            this.tableLayoutPanel3.Controls.Add(this.lbl_Mode, 1, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(859, 47);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // lbl_Mode
            // 
            this.lbl_Mode.AutoSize = true;
            this.lbl_Mode.Font = new System.Drawing.Font("맑은 고딕", 14F, System.Drawing.FontStyle.Bold);
            this.lbl_Mode.Location = new System.Drawing.Point(400, 10);
            this.lbl_Mode.Margin = new System.Windows.Forms.Padding(35, 10, 3, 0);
            this.lbl_Mode.Name = "lbl_Mode";
            this.lbl_Mode.Size = new System.Drawing.Size(64, 25);
            this.lbl_Mode.TabIndex = 1;
            this.lbl_Mode.Text = "Mode";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel5, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.gbox_SkipMode, 0, 1);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 56);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.259259F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90.74074F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(859, 762);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.gbox_Mode_DistributionMode, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.gbox_SimulationMode, 0, 0);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(853, 64);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // gbox_Mode_DistributionMode
            // 
            this.gbox_Mode_DistributionMode.Controls.Add(this.tableLayoutPanel7);
            this.gbox_Mode_DistributionMode.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_Mode_DistributionMode.ForeColor = System.Drawing.Color.White;
            this.gbox_Mode_DistributionMode.Location = new System.Drawing.Point(429, 3);
            this.gbox_Mode_DistributionMode.Name = "gbox_Mode_DistributionMode";
            this.gbox_Mode_DistributionMode.Size = new System.Drawing.Size(420, 58);
            this.gbox_Mode_DistributionMode.TabIndex = 53;
            this.gbox_Mode_DistributionMode.TabStop = false;
            this.gbox_Mode_DistributionMode.Text = "     물류 모드    ";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 3;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel7.Controls.Add(this.btn_Mode_DistributionMode_DontUse, 2, 0);
            this.tableLayoutPanel7.Controls.Add(this.tbox_Mode_DistributionMode, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.btn_Mode_DistributionMode_Use, 1, 0);
            this.tableLayoutPanel7.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(408, 38);
            this.tableLayoutPanel7.TabIndex = 1;
            // 
            // btn_Mode_DistributionMode_DontUse
            // 
            this.btn_Mode_DistributionMode_DontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Mode_DistributionMode_DontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Mode_DistributionMode_DontUse.ForeColor = System.Drawing.Color.White;
            this.btn_Mode_DistributionMode_DontUse.Location = new System.Drawing.Point(298, 3);
            this.btn_Mode_DistributionMode_DontUse.Name = "btn_Mode_DistributionMode_DontUse";
            this.btn_Mode_DistributionMode_DontUse.Size = new System.Drawing.Size(106, 32);
            this.btn_Mode_DistributionMode_DontUse.TabIndex = 47;
            this.btn_Mode_DistributionMode_DontUse.Text = "사용안함";
            this.btn_Mode_DistributionMode_DontUse.UseVisualStyleBackColor = false;
            // 
            // tbox_Mode_DistributionMode
            // 
            this.tbox_Mode_DistributionMode.Location = new System.Drawing.Point(3, 3);
            this.tbox_Mode_DistributionMode.Name = "tbox_Mode_DistributionMode";
            this.tbox_Mode_DistributionMode.Size = new System.Drawing.Size(177, 29);
            this.tbox_Mode_DistributionMode.TabIndex = 0;
            // 
            // btn_Mode_DistributionMode_Use
            // 
            this.btn_Mode_DistributionMode_Use.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Mode_DistributionMode_Use.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Mode_DistributionMode_Use.ForeColor = System.Drawing.Color.White;
            this.btn_Mode_DistributionMode_Use.Location = new System.Drawing.Point(186, 3);
            this.btn_Mode_DistributionMode_Use.Name = "btn_Mode_DistributionMode_Use";
            this.btn_Mode_DistributionMode_Use.Size = new System.Drawing.Size(106, 32);
            this.btn_Mode_DistributionMode_Use.TabIndex = 46;
            this.btn_Mode_DistributionMode_Use.Text = "사용함";
            this.btn_Mode_DistributionMode_Use.UseVisualStyleBackColor = false;
            // 
            // gbox_SimulationMode
            // 
            this.gbox_SimulationMode.Controls.Add(this.tableLayoutPanel6);
            this.gbox_SimulationMode.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_SimulationMode.ForeColor = System.Drawing.Color.White;
            this.gbox_SimulationMode.Location = new System.Drawing.Point(3, 3);
            this.gbox_SimulationMode.Name = "gbox_SimulationMode";
            this.gbox_SimulationMode.Size = new System.Drawing.Size(420, 58);
            this.gbox_SimulationMode.TabIndex = 52;
            this.gbox_SimulationMode.TabStop = false;
            this.gbox_SimulationMode.Text = "     시뮬레이션 모드    ";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 3;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel6.Controls.Add(this.btn_SimulationMode_DontUse, 2, 0);
            this.tableLayoutPanel6.Controls.Add(this.tbox_SimulationMode, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.btn_SimulationMode_Use, 1, 0);
            this.tableLayoutPanel6.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(408, 38);
            this.tableLayoutPanel6.TabIndex = 1;
            // 
            // btn_SimulationMode_DontUse
            // 
            this.btn_SimulationMode_DontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_SimulationMode_DontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_SimulationMode_DontUse.ForeColor = System.Drawing.Color.White;
            this.btn_SimulationMode_DontUse.Location = new System.Drawing.Point(298, 3);
            this.btn_SimulationMode_DontUse.Name = "btn_SimulationMode_DontUse";
            this.btn_SimulationMode_DontUse.Size = new System.Drawing.Size(106, 32);
            this.btn_SimulationMode_DontUse.TabIndex = 47;
            this.btn_SimulationMode_DontUse.Text = "사용안함";
            this.btn_SimulationMode_DontUse.UseVisualStyleBackColor = false;
            // 
            // tbox_SimulationMode
            // 
            this.tbox_SimulationMode.Location = new System.Drawing.Point(3, 3);
            this.tbox_SimulationMode.Name = "tbox_SimulationMode";
            this.tbox_SimulationMode.Size = new System.Drawing.Size(177, 29);
            this.tbox_SimulationMode.TabIndex = 0;
            // 
            // btn_SimulationMode_Use
            // 
            this.btn_SimulationMode_Use.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_SimulationMode_Use.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_SimulationMode_Use.ForeColor = System.Drawing.Color.White;
            this.btn_SimulationMode_Use.Location = new System.Drawing.Point(186, 3);
            this.btn_SimulationMode_Use.Name = "btn_SimulationMode_Use";
            this.btn_SimulationMode_Use.Size = new System.Drawing.Size(106, 32);
            this.btn_SimulationMode_Use.TabIndex = 46;
            this.btn_SimulationMode_Use.Text = "사용함";
            this.btn_SimulationMode_Use.UseVisualStyleBackColor = false;
            // 
            // gbox_SkipMode
            // 
            this.gbox_SkipMode.Controls.Add(this.gbox_DoorSkip);
            this.gbox_SkipMode.Controls.Add(this.gbox_GrabSwitchSkip);
            this.gbox_SkipMode.Controls.Add(this.gbox_McDownSkip);
            this.gbox_SkipMode.Controls.Add(this.gbox_TeachSkip);
            this.gbox_SkipMode.Controls.Add(this.gbox_MutingSkip);
            this.gbox_SkipMode.Controls.Add(this.gbox_LaserMeasureSkip);
            this.gbox_SkipMode.Controls.Add(this.gbox_UnloaderInputSkip);
            this.gbox_SkipMode.Controls.Add(this.gbox_LoaderInputSkip);
            this.gbox_SkipMode.Controls.Add(this.gbox_BufferPickerSkip);
            this.gbox_SkipMode.Controls.Add(this.gbox_TiltMeasureSkip);
            this.gbox_SkipMode.Controls.Add(this.gbox_BreakingAlignSkip);
            this.gbox_SkipMode.Controls.Add(this.gbox_BcrSkip);
            this.gbox_SkipMode.Controls.Add(this.gbox_McrSkip);
            this.gbox_SkipMode.Controls.Add(this.gbox_InspectionSkip);
            this.gbox_SkipMode.Controls.Add(this.gbox_BreakingMotionSkip);
            this.gbox_SkipMode.Controls.Add(this.gbox_LaserSkip);
            this.gbox_SkipMode.Controls.Add(this.gbox_BreakingBSkip);
            this.gbox_SkipMode.Controls.Add(this.gbox_BreakingASkip);
            this.gbox_SkipMode.Controls.Add(this.gbox_ProcessBSkip);
            this.gbox_SkipMode.Controls.Add(this.gbox_ProcessASkip);
            this.gbox_SkipMode.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_SkipMode.ForeColor = System.Drawing.Color.White;
            this.gbox_SkipMode.Location = new System.Drawing.Point(3, 73);
            this.gbox_SkipMode.Name = "gbox_SkipMode";
            this.gbox_SkipMode.Size = new System.Drawing.Size(849, 686);
            this.gbox_SkipMode.TabIndex = 53;
            this.gbox_SkipMode.TabStop = false;
            this.gbox_SkipMode.Text = "     스킵 모드    ";
            // 
            // gbox_DoorSkip
            // 
            this.gbox_DoorSkip.Controls.Add(this.tableLayoutPanel31);
            this.gbox_DoorSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_DoorSkip.ForeColor = System.Drawing.Color.White;
            this.gbox_DoorSkip.Location = new System.Drawing.Point(428, 552);
            this.gbox_DoorSkip.Name = "gbox_DoorSkip";
            this.gbox_DoorSkip.Size = new System.Drawing.Size(419, 57);
            this.gbox_DoorSkip.TabIndex = 56;
            this.gbox_DoorSkip.TabStop = false;
            this.gbox_DoorSkip.Text = "     도어 스킵     ";
            // 
            // tableLayoutPanel31
            // 
            this.tableLayoutPanel31.ColumnCount = 3;
            this.tableLayoutPanel31.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel31.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel31.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel31.Controls.Add(this.btn_DoorSkip_DontUSe, 2, 0);
            this.tableLayoutPanel31.Controls.Add(this.tbox_DoorSkip, 0, 0);
            this.tableLayoutPanel31.Controls.Add(this.btn_DoorSkip_Use, 1, 0);
            this.tableLayoutPanel31.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel31.Name = "tableLayoutPanel31";
            this.tableLayoutPanel31.RowCount = 1;
            this.tableLayoutPanel31.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel31.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel31.TabIndex = 1;
            // 
            // btn_DoorSkip_DontUSe
            // 
            this.btn_DoorSkip_DontUSe.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_DoorSkip_DontUSe.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_DoorSkip_DontUSe.ForeColor = System.Drawing.Color.White;
            this.btn_DoorSkip_DontUSe.Location = new System.Drawing.Point(298, 3);
            this.btn_DoorSkip_DontUSe.Name = "btn_DoorSkip_DontUSe";
            this.btn_DoorSkip_DontUSe.Size = new System.Drawing.Size(106, 26);
            this.btn_DoorSkip_DontUSe.TabIndex = 47;
            this.btn_DoorSkip_DontUSe.Text = "사용안함";
            this.btn_DoorSkip_DontUSe.UseVisualStyleBackColor = false;
            // 
            // tbox_DoorSkip
            // 
            this.tbox_DoorSkip.Location = new System.Drawing.Point(3, 3);
            this.tbox_DoorSkip.Name = "tbox_DoorSkip";
            this.tbox_DoorSkip.Size = new System.Drawing.Size(177, 29);
            this.tbox_DoorSkip.TabIndex = 0;
            // 
            // btn_DoorSkip_Use
            // 
            this.btn_DoorSkip_Use.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_DoorSkip_Use.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_DoorSkip_Use.ForeColor = System.Drawing.Color.White;
            this.btn_DoorSkip_Use.Location = new System.Drawing.Point(186, 3);
            this.btn_DoorSkip_Use.Name = "btn_DoorSkip_Use";
            this.btn_DoorSkip_Use.Size = new System.Drawing.Size(106, 26);
            this.btn_DoorSkip_Use.TabIndex = 46;
            this.btn_DoorSkip_Use.Text = "사용함";
            this.btn_DoorSkip_Use.UseVisualStyleBackColor = false;
            // 
            // gbox_GrabSwitchSkip
            // 
            this.gbox_GrabSwitchSkip.Controls.Add(this.tableLayoutPanel30);
            this.gbox_GrabSwitchSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_GrabSwitchSkip.ForeColor = System.Drawing.Color.White;
            this.gbox_GrabSwitchSkip.Location = new System.Drawing.Point(428, 496);
            this.gbox_GrabSwitchSkip.Name = "gbox_GrabSwitchSkip";
            this.gbox_GrabSwitchSkip.Size = new System.Drawing.Size(419, 57);
            this.gbox_GrabSwitchSkip.TabIndex = 64;
            this.gbox_GrabSwitchSkip.TabStop = false;
            this.gbox_GrabSwitchSkip.Text = "     그랩 스위치 스킵     ";
            // 
            // tableLayoutPanel30
            // 
            this.tableLayoutPanel30.ColumnCount = 3;
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel30.Controls.Add(this.btn_GrabSwitchSkip_DontUse, 2, 0);
            this.tableLayoutPanel30.Controls.Add(this.tbox_GrabSwitchSkip, 0, 0);
            this.tableLayoutPanel30.Controls.Add(this.btn_GrabSwitchSkip_Use, 1, 0);
            this.tableLayoutPanel30.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel30.Name = "tableLayoutPanel30";
            this.tableLayoutPanel30.RowCount = 1;
            this.tableLayoutPanel30.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel30.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel30.TabIndex = 1;
            // 
            // btn_GrabSwitchSkip_DontUse
            // 
            this.btn_GrabSwitchSkip_DontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_GrabSwitchSkip_DontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_GrabSwitchSkip_DontUse.ForeColor = System.Drawing.Color.White;
            this.btn_GrabSwitchSkip_DontUse.Location = new System.Drawing.Point(298, 3);
            this.btn_GrabSwitchSkip_DontUse.Name = "btn_GrabSwitchSkip_DontUse";
            this.btn_GrabSwitchSkip_DontUse.Size = new System.Drawing.Size(106, 26);
            this.btn_GrabSwitchSkip_DontUse.TabIndex = 47;
            this.btn_GrabSwitchSkip_DontUse.Text = "사용안함";
            this.btn_GrabSwitchSkip_DontUse.UseVisualStyleBackColor = false;
            // 
            // tbox_GrabSwitchSkip
            // 
            this.tbox_GrabSwitchSkip.Location = new System.Drawing.Point(3, 3);
            this.tbox_GrabSwitchSkip.Name = "tbox_GrabSwitchSkip";
            this.tbox_GrabSwitchSkip.Size = new System.Drawing.Size(177, 29);
            this.tbox_GrabSwitchSkip.TabIndex = 0;
            // 
            // btn_GrabSwitchSkip_Use
            // 
            this.btn_GrabSwitchSkip_Use.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_GrabSwitchSkip_Use.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_GrabSwitchSkip_Use.ForeColor = System.Drawing.Color.White;
            this.btn_GrabSwitchSkip_Use.Location = new System.Drawing.Point(186, 3);
            this.btn_GrabSwitchSkip_Use.Name = "btn_GrabSwitchSkip_Use";
            this.btn_GrabSwitchSkip_Use.Size = new System.Drawing.Size(106, 26);
            this.btn_GrabSwitchSkip_Use.TabIndex = 46;
            this.btn_GrabSwitchSkip_Use.Text = "사용함";
            this.btn_GrabSwitchSkip_Use.UseVisualStyleBackColor = false;
            // 
            // gbox_McDownSkip
            // 
            this.gbox_McDownSkip.Controls.Add(this.tableLayoutPanel29);
            this.gbox_McDownSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_McDownSkip.ForeColor = System.Drawing.Color.White;
            this.gbox_McDownSkip.Location = new System.Drawing.Point(4, 608);
            this.gbox_McDownSkip.Name = "gbox_McDownSkip";
            this.gbox_McDownSkip.Size = new System.Drawing.Size(419, 57);
            this.gbox_McDownSkip.TabIndex = 63;
            this.gbox_McDownSkip.TabStop = false;
            this.gbox_McDownSkip.Text = "     MC 다운 스킵     ";
            // 
            // tableLayoutPanel29
            // 
            this.tableLayoutPanel29.ColumnCount = 3;
            this.tableLayoutPanel29.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel29.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel29.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel29.Controls.Add(this.btn_McDownSkip_DontUse, 2, 0);
            this.tableLayoutPanel29.Controls.Add(this.tbox_McDownSkip, 0, 0);
            this.tableLayoutPanel29.Controls.Add(this.btn_McDownSkip_Use, 1, 0);
            this.tableLayoutPanel29.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel29.Name = "tableLayoutPanel29";
            this.tableLayoutPanel29.RowCount = 1;
            this.tableLayoutPanel29.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel29.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel29.TabIndex = 1;
            // 
            // btn_McDownSkip_DontUse
            // 
            this.btn_McDownSkip_DontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_McDownSkip_DontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_McDownSkip_DontUse.ForeColor = System.Drawing.Color.White;
            this.btn_McDownSkip_DontUse.Location = new System.Drawing.Point(298, 3);
            this.btn_McDownSkip_DontUse.Name = "btn_McDownSkip_DontUse";
            this.btn_McDownSkip_DontUse.Size = new System.Drawing.Size(106, 26);
            this.btn_McDownSkip_DontUse.TabIndex = 47;
            this.btn_McDownSkip_DontUse.Text = "사용안함";
            this.btn_McDownSkip_DontUse.UseVisualStyleBackColor = false;
            // 
            // tbox_McDownSkip
            // 
            this.tbox_McDownSkip.Location = new System.Drawing.Point(3, 3);
            this.tbox_McDownSkip.Name = "tbox_McDownSkip";
            this.tbox_McDownSkip.Size = new System.Drawing.Size(177, 29);
            this.tbox_McDownSkip.TabIndex = 0;
            // 
            // btn_McDownSkip_Use
            // 
            this.btn_McDownSkip_Use.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_McDownSkip_Use.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_McDownSkip_Use.ForeColor = System.Drawing.Color.White;
            this.btn_McDownSkip_Use.Location = new System.Drawing.Point(186, 3);
            this.btn_McDownSkip_Use.Name = "btn_McDownSkip_Use";
            this.btn_McDownSkip_Use.Size = new System.Drawing.Size(106, 26);
            this.btn_McDownSkip_Use.TabIndex = 46;
            this.btn_McDownSkip_Use.Text = "사용함";
            this.btn_McDownSkip_Use.UseVisualStyleBackColor = false;
            // 
            // gbox_TeachSkip
            // 
            this.gbox_TeachSkip.Controls.Add(this.tableLayoutPanel28);
            this.gbox_TeachSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_TeachSkip.ForeColor = System.Drawing.Color.White;
            this.gbox_TeachSkip.Location = new System.Drawing.Point(3, 552);
            this.gbox_TeachSkip.Name = "gbox_TeachSkip";
            this.gbox_TeachSkip.Size = new System.Drawing.Size(419, 57);
            this.gbox_TeachSkip.TabIndex = 55;
            this.gbox_TeachSkip.TabStop = false;
            this.gbox_TeachSkip.Text = "     티치 스킵     ";
            // 
            // tableLayoutPanel28
            // 
            this.tableLayoutPanel28.ColumnCount = 3;
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel28.Controls.Add(this.btn_TeachSkip_DontUse, 2, 0);
            this.tableLayoutPanel28.Controls.Add(this.tbox_TeachSkip, 0, 0);
            this.tableLayoutPanel28.Controls.Add(this.btn_TeachSkip_Use, 1, 0);
            this.tableLayoutPanel28.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel28.Name = "tableLayoutPanel28";
            this.tableLayoutPanel28.RowCount = 1;
            this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel28.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel28.TabIndex = 1;
            // 
            // btn_TeachSkip_DontUse
            // 
            this.btn_TeachSkip_DontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_TeachSkip_DontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_TeachSkip_DontUse.ForeColor = System.Drawing.Color.White;
            this.btn_TeachSkip_DontUse.Location = new System.Drawing.Point(298, 3);
            this.btn_TeachSkip_DontUse.Name = "btn_TeachSkip_DontUse";
            this.btn_TeachSkip_DontUse.Size = new System.Drawing.Size(106, 26);
            this.btn_TeachSkip_DontUse.TabIndex = 47;
            this.btn_TeachSkip_DontUse.Text = "사용안함";
            this.btn_TeachSkip_DontUse.UseVisualStyleBackColor = false;
            // 
            // tbox_TeachSkip
            // 
            this.tbox_TeachSkip.Location = new System.Drawing.Point(3, 3);
            this.tbox_TeachSkip.Name = "tbox_TeachSkip";
            this.tbox_TeachSkip.Size = new System.Drawing.Size(177, 29);
            this.tbox_TeachSkip.TabIndex = 0;
            // 
            // btn_TeachSkip_Use
            // 
            this.btn_TeachSkip_Use.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_TeachSkip_Use.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_TeachSkip_Use.ForeColor = System.Drawing.Color.White;
            this.btn_TeachSkip_Use.Location = new System.Drawing.Point(186, 3);
            this.btn_TeachSkip_Use.Name = "btn_TeachSkip_Use";
            this.btn_TeachSkip_Use.Size = new System.Drawing.Size(106, 26);
            this.btn_TeachSkip_Use.TabIndex = 46;
            this.btn_TeachSkip_Use.Text = "사용함";
            this.btn_TeachSkip_Use.UseVisualStyleBackColor = false;
            // 
            // gbox_MutingSkip
            // 
            this.gbox_MutingSkip.Controls.Add(this.tableLayoutPanel27);
            this.gbox_MutingSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_MutingSkip.ForeColor = System.Drawing.Color.White;
            this.gbox_MutingSkip.Location = new System.Drawing.Point(3, 496);
            this.gbox_MutingSkip.Name = "gbox_MutingSkip";
            this.gbox_MutingSkip.Size = new System.Drawing.Size(419, 57);
            this.gbox_MutingSkip.TabIndex = 62;
            this.gbox_MutingSkip.TabStop = false;
            this.gbox_MutingSkip.Text = "     뮤팅 스킵     ";
            // 
            // tableLayoutPanel27
            // 
            this.tableLayoutPanel27.ColumnCount = 3;
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel27.Controls.Add(this.btn_MutingSkip_DontUse, 2, 0);
            this.tableLayoutPanel27.Controls.Add(this.tbox_MutingSkip, 0, 0);
            this.tableLayoutPanel27.Controls.Add(this.btn_MutingSkip_Use, 1, 0);
            this.tableLayoutPanel27.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel27.Name = "tableLayoutPanel27";
            this.tableLayoutPanel27.RowCount = 1;
            this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel27.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel27.TabIndex = 1;
            // 
            // btn_MutingSkip_DontUse
            // 
            this.btn_MutingSkip_DontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_MutingSkip_DontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_MutingSkip_DontUse.ForeColor = System.Drawing.Color.White;
            this.btn_MutingSkip_DontUse.Location = new System.Drawing.Point(298, 3);
            this.btn_MutingSkip_DontUse.Name = "btn_MutingSkip_DontUse";
            this.btn_MutingSkip_DontUse.Size = new System.Drawing.Size(106, 26);
            this.btn_MutingSkip_DontUse.TabIndex = 47;
            this.btn_MutingSkip_DontUse.Text = "사용안함";
            this.btn_MutingSkip_DontUse.UseVisualStyleBackColor = false;
            // 
            // tbox_MutingSkip
            // 
            this.tbox_MutingSkip.Location = new System.Drawing.Point(3, 3);
            this.tbox_MutingSkip.Name = "tbox_MutingSkip";
            this.tbox_MutingSkip.Size = new System.Drawing.Size(177, 29);
            this.tbox_MutingSkip.TabIndex = 0;
            // 
            // btn_MutingSkip_Use
            // 
            this.btn_MutingSkip_Use.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_MutingSkip_Use.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_MutingSkip_Use.ForeColor = System.Drawing.Color.White;
            this.btn_MutingSkip_Use.Location = new System.Drawing.Point(186, 3);
            this.btn_MutingSkip_Use.Name = "btn_MutingSkip_Use";
            this.btn_MutingSkip_Use.Size = new System.Drawing.Size(106, 26);
            this.btn_MutingSkip_Use.TabIndex = 46;
            this.btn_MutingSkip_Use.Text = "사용함";
            this.btn_MutingSkip_Use.UseVisualStyleBackColor = false;
            // 
            // gbox_LaserMeasureSkip
            // 
            this.gbox_LaserMeasureSkip.Controls.Add(this.tableLayoutPanel26);
            this.gbox_LaserMeasureSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_LaserMeasureSkip.ForeColor = System.Drawing.Color.White;
            this.gbox_LaserMeasureSkip.Location = new System.Drawing.Point(3, 421);
            this.gbox_LaserMeasureSkip.Name = "gbox_LaserMeasureSkip";
            this.gbox_LaserMeasureSkip.Size = new System.Drawing.Size(419, 57);
            this.gbox_LaserMeasureSkip.TabIndex = 61;
            this.gbox_LaserMeasureSkip.TabStop = false;
            this.gbox_LaserMeasureSkip.Text = "     레이져 측정 스킵     ";
            // 
            // tableLayoutPanel26
            // 
            this.tableLayoutPanel26.ColumnCount = 3;
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel26.Controls.Add(this.btn_LaserMeasureSkip_DontUse, 2, 0);
            this.tableLayoutPanel26.Controls.Add(this.tbox_LaserMeasureSkip, 0, 0);
            this.tableLayoutPanel26.Controls.Add(this.btn_LaserMeasureSkip_Use, 1, 0);
            this.tableLayoutPanel26.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel26.Name = "tableLayoutPanel26";
            this.tableLayoutPanel26.RowCount = 1;
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel26.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel26.TabIndex = 1;
            // 
            // btn_LaserMeasureSkip_DontUse
            // 
            this.btn_LaserMeasureSkip_DontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_LaserMeasureSkip_DontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_LaserMeasureSkip_DontUse.ForeColor = System.Drawing.Color.White;
            this.btn_LaserMeasureSkip_DontUse.Location = new System.Drawing.Point(298, 3);
            this.btn_LaserMeasureSkip_DontUse.Name = "btn_LaserMeasureSkip_DontUse";
            this.btn_LaserMeasureSkip_DontUse.Size = new System.Drawing.Size(106, 26);
            this.btn_LaserMeasureSkip_DontUse.TabIndex = 47;
            this.btn_LaserMeasureSkip_DontUse.Text = "사용안함";
            this.btn_LaserMeasureSkip_DontUse.UseVisualStyleBackColor = false;
            // 
            // tbox_LaserMeasureSkip
            // 
            this.tbox_LaserMeasureSkip.Location = new System.Drawing.Point(3, 3);
            this.tbox_LaserMeasureSkip.Name = "tbox_LaserMeasureSkip";
            this.tbox_LaserMeasureSkip.Size = new System.Drawing.Size(177, 29);
            this.tbox_LaserMeasureSkip.TabIndex = 0;
            // 
            // btn_LaserMeasureSkip_Use
            // 
            this.btn_LaserMeasureSkip_Use.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_LaserMeasureSkip_Use.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_LaserMeasureSkip_Use.ForeColor = System.Drawing.Color.White;
            this.btn_LaserMeasureSkip_Use.Location = new System.Drawing.Point(186, 3);
            this.btn_LaserMeasureSkip_Use.Name = "btn_LaserMeasureSkip_Use";
            this.btn_LaserMeasureSkip_Use.Size = new System.Drawing.Size(106, 26);
            this.btn_LaserMeasureSkip_Use.TabIndex = 46;
            this.btn_LaserMeasureSkip_Use.Text = "사용함";
            this.btn_LaserMeasureSkip_Use.UseVisualStyleBackColor = false;
            // 
            // gbox_UnloaderInputSkip
            // 
            this.gbox_UnloaderInputSkip.Controls.Add(this.tableLayoutPanel21);
            this.gbox_UnloaderInputSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_UnloaderInputSkip.ForeColor = System.Drawing.Color.White;
            this.gbox_UnloaderInputSkip.Location = new System.Drawing.Point(428, 362);
            this.gbox_UnloaderInputSkip.Name = "gbox_UnloaderInputSkip";
            this.gbox_UnloaderInputSkip.Size = new System.Drawing.Size(419, 60);
            this.gbox_UnloaderInputSkip.TabIndex = 60;
            this.gbox_UnloaderInputSkip.TabStop = false;
            this.gbox_UnloaderInputSkip.Text = "     언로더부 카세트 A / B 투입 스킵     ";
            // 
            // tableLayoutPanel21
            // 
            this.tableLayoutPanel21.ColumnCount = 2;
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel21.Controls.Add(this.tableLayoutPanel24, 0, 0);
            this.tableLayoutPanel21.Controls.Add(this.tableLayoutPanel25, 0, 0);
            this.tableLayoutPanel21.Location = new System.Drawing.Point(3, 18);
            this.tableLayoutPanel21.Name = "tableLayoutPanel21";
            this.tableLayoutPanel21.RowCount = 1;
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel21.Size = new System.Drawing.Size(415, 39);
            this.tableLayoutPanel21.TabIndex = 0;
            // 
            // tableLayoutPanel24
            // 
            this.tableLayoutPanel24.ColumnCount = 3;
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.5F));
            this.tableLayoutPanel24.Controls.Add(this.btn_UnloaderInputSkipB_DontUse, 2, 0);
            this.tableLayoutPanel24.Controls.Add(this.tbox_UnloaderInputSkipB, 0, 0);
            this.tableLayoutPanel24.Controls.Add(this.btn_UnloaderInputSkipB_Use, 1, 0);
            this.tableLayoutPanel24.Location = new System.Drawing.Point(210, 3);
            this.tableLayoutPanel24.Name = "tableLayoutPanel24";
            this.tableLayoutPanel24.RowCount = 1;
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel24.Size = new System.Drawing.Size(201, 33);
            this.tableLayoutPanel24.TabIndex = 3;
            // 
            // btn_UnloaderInputSkipB_DontUse
            // 
            this.btn_UnloaderInputSkipB_DontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderInputSkipB_DontUse.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_UnloaderInputSkipB_DontUse.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderInputSkipB_DontUse.Location = new System.Drawing.Point(138, 3);
            this.btn_UnloaderInputSkipB_DontUse.Name = "btn_UnloaderInputSkipB_DontUse";
            this.btn_UnloaderInputSkipB_DontUse.Size = new System.Drawing.Size(60, 27);
            this.btn_UnloaderInputSkipB_DontUse.TabIndex = 47;
            this.btn_UnloaderInputSkipB_DontUse.Text = "사용안함";
            this.btn_UnloaderInputSkipB_DontUse.UseVisualStyleBackColor = false;
            // 
            // tbox_UnloaderInputSkipB
            // 
            this.tbox_UnloaderInputSkipB.Location = new System.Drawing.Point(3, 3);
            this.tbox_UnloaderInputSkipB.Name = "tbox_UnloaderInputSkipB";
            this.tbox_UnloaderInputSkipB.Size = new System.Drawing.Size(74, 29);
            this.tbox_UnloaderInputSkipB.TabIndex = 0;
            // 
            // btn_UnloaderInputSkipB_Use
            // 
            this.btn_UnloaderInputSkipB_Use.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderInputSkipB_Use.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_UnloaderInputSkipB_Use.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderInputSkipB_Use.Location = new System.Drawing.Point(83, 3);
            this.btn_UnloaderInputSkipB_Use.Name = "btn_UnloaderInputSkipB_Use";
            this.btn_UnloaderInputSkipB_Use.Size = new System.Drawing.Size(49, 27);
            this.btn_UnloaderInputSkipB_Use.TabIndex = 46;
            this.btn_UnloaderInputSkipB_Use.Text = "사용함";
            this.btn_UnloaderInputSkipB_Use.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel25
            // 
            this.tableLayoutPanel25.ColumnCount = 3;
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.5F));
            this.tableLayoutPanel25.Controls.Add(this.btn_UnloaderInputSkipA_DontUse, 2, 0);
            this.tableLayoutPanel25.Controls.Add(this.tbox_UnloaderInputSkipA, 0, 0);
            this.tableLayoutPanel25.Controls.Add(this.btn_UnloaderInputSkipA_Use, 1, 0);
            this.tableLayoutPanel25.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel25.Name = "tableLayoutPanel25";
            this.tableLayoutPanel25.RowCount = 1;
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel25.Size = new System.Drawing.Size(201, 33);
            this.tableLayoutPanel25.TabIndex = 2;
            // 
            // btn_UnloaderInputSkipA_DontUse
            // 
            this.btn_UnloaderInputSkipA_DontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderInputSkipA_DontUse.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_UnloaderInputSkipA_DontUse.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderInputSkipA_DontUse.Location = new System.Drawing.Point(138, 3);
            this.btn_UnloaderInputSkipA_DontUse.Name = "btn_UnloaderInputSkipA_DontUse";
            this.btn_UnloaderInputSkipA_DontUse.Size = new System.Drawing.Size(60, 27);
            this.btn_UnloaderInputSkipA_DontUse.TabIndex = 47;
            this.btn_UnloaderInputSkipA_DontUse.Text = "사용안함";
            this.btn_UnloaderInputSkipA_DontUse.UseVisualStyleBackColor = false;
            // 
            // tbox_UnloaderInputSkipA
            // 
            this.tbox_UnloaderInputSkipA.Location = new System.Drawing.Point(3, 3);
            this.tbox_UnloaderInputSkipA.Name = "tbox_UnloaderInputSkipA";
            this.tbox_UnloaderInputSkipA.Size = new System.Drawing.Size(74, 29);
            this.tbox_UnloaderInputSkipA.TabIndex = 0;
            // 
            // btn_UnloaderInputSkipA_Use
            // 
            this.btn_UnloaderInputSkipA_Use.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderInputSkipA_Use.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_UnloaderInputSkipA_Use.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderInputSkipA_Use.Location = new System.Drawing.Point(83, 3);
            this.btn_UnloaderInputSkipA_Use.Name = "btn_UnloaderInputSkipA_Use";
            this.btn_UnloaderInputSkipA_Use.Size = new System.Drawing.Size(49, 27);
            this.btn_UnloaderInputSkipA_Use.TabIndex = 46;
            this.btn_UnloaderInputSkipA_Use.Text = "사용함";
            this.btn_UnloaderInputSkipA_Use.UseVisualStyleBackColor = false;
            // 
            // gbox_LoaderInputSkip
            // 
            this.gbox_LoaderInputSkip.Controls.Add(this.tableLayoutPanel20);
            this.gbox_LoaderInputSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_LoaderInputSkip.ForeColor = System.Drawing.Color.White;
            this.gbox_LoaderInputSkip.Location = new System.Drawing.Point(3, 362);
            this.gbox_LoaderInputSkip.Name = "gbox_LoaderInputSkip";
            this.gbox_LoaderInputSkip.Size = new System.Drawing.Size(419, 60);
            this.gbox_LoaderInputSkip.TabIndex = 59;
            this.gbox_LoaderInputSkip.TabStop = false;
            this.gbox_LoaderInputSkip.Text = "     로더부 카세트 A / B 투입 스킵     ";
            // 
            // tableLayoutPanel20
            // 
            this.tableLayoutPanel20.ColumnCount = 2;
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel20.Controls.Add(this.tableLayoutPanel23, 0, 0);
            this.tableLayoutPanel20.Controls.Add(this.tableLayoutPanel22, 0, 0);
            this.tableLayoutPanel20.Location = new System.Drawing.Point(3, 18);
            this.tableLayoutPanel20.Name = "tableLayoutPanel20";
            this.tableLayoutPanel20.RowCount = 1;
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel20.Size = new System.Drawing.Size(415, 39);
            this.tableLayoutPanel20.TabIndex = 0;
            // 
            // tableLayoutPanel23
            // 
            this.tableLayoutPanel23.ColumnCount = 3;
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.5F));
            this.tableLayoutPanel23.Controls.Add(this.btn_LoaderInputSkipB_DontUse, 2, 0);
            this.tableLayoutPanel23.Controls.Add(this.tbox_LoaderInputSkipB, 0, 0);
            this.tableLayoutPanel23.Controls.Add(this.btn_LoaderInputSkipB_Use, 1, 0);
            this.tableLayoutPanel23.Location = new System.Drawing.Point(210, 3);
            this.tableLayoutPanel23.Name = "tableLayoutPanel23";
            this.tableLayoutPanel23.RowCount = 1;
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel23.Size = new System.Drawing.Size(201, 33);
            this.tableLayoutPanel23.TabIndex = 3;
            // 
            // btn_LoaderInputSkipB_DontUse
            // 
            this.btn_LoaderInputSkipB_DontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_LoaderInputSkipB_DontUse.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_LoaderInputSkipB_DontUse.ForeColor = System.Drawing.Color.White;
            this.btn_LoaderInputSkipB_DontUse.Location = new System.Drawing.Point(138, 3);
            this.btn_LoaderInputSkipB_DontUse.Name = "btn_LoaderInputSkipB_DontUse";
            this.btn_LoaderInputSkipB_DontUse.Size = new System.Drawing.Size(60, 27);
            this.btn_LoaderInputSkipB_DontUse.TabIndex = 47;
            this.btn_LoaderInputSkipB_DontUse.Text = "사용안함";
            this.btn_LoaderInputSkipB_DontUse.UseVisualStyleBackColor = false;
            // 
            // tbox_LoaderInputSkipB
            // 
            this.tbox_LoaderInputSkipB.Location = new System.Drawing.Point(3, 3);
            this.tbox_LoaderInputSkipB.Name = "tbox_LoaderInputSkipB";
            this.tbox_LoaderInputSkipB.Size = new System.Drawing.Size(74, 29);
            this.tbox_LoaderInputSkipB.TabIndex = 0;
            // 
            // btn_LoaderInputSkipB_Use
            // 
            this.btn_LoaderInputSkipB_Use.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_LoaderInputSkipB_Use.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_LoaderInputSkipB_Use.ForeColor = System.Drawing.Color.White;
            this.btn_LoaderInputSkipB_Use.Location = new System.Drawing.Point(83, 3);
            this.btn_LoaderInputSkipB_Use.Name = "btn_LoaderInputSkipB_Use";
            this.btn_LoaderInputSkipB_Use.Size = new System.Drawing.Size(49, 27);
            this.btn_LoaderInputSkipB_Use.TabIndex = 46;
            this.btn_LoaderInputSkipB_Use.Text = "사용함";
            this.btn_LoaderInputSkipB_Use.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel22
            // 
            this.tableLayoutPanel22.ColumnCount = 3;
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.5F));
            this.tableLayoutPanel22.Controls.Add(this.btn_LoaderInputSkipA_DontUse, 2, 0);
            this.tableLayoutPanel22.Controls.Add(this.tbox_LoaderInputSkipA, 0, 0);
            this.tableLayoutPanel22.Controls.Add(this.btn_LoaderInputSkipA_Use, 1, 0);
            this.tableLayoutPanel22.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel22.Name = "tableLayoutPanel22";
            this.tableLayoutPanel22.RowCount = 1;
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel22.Size = new System.Drawing.Size(201, 33);
            this.tableLayoutPanel22.TabIndex = 2;
            // 
            // btn_LoaderInputSkipA_DontUse
            // 
            this.btn_LoaderInputSkipA_DontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_LoaderInputSkipA_DontUse.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_LoaderInputSkipA_DontUse.ForeColor = System.Drawing.Color.White;
            this.btn_LoaderInputSkipA_DontUse.Location = new System.Drawing.Point(138, 3);
            this.btn_LoaderInputSkipA_DontUse.Name = "btn_LoaderInputSkipA_DontUse";
            this.btn_LoaderInputSkipA_DontUse.Size = new System.Drawing.Size(60, 27);
            this.btn_LoaderInputSkipA_DontUse.TabIndex = 47;
            this.btn_LoaderInputSkipA_DontUse.Text = "사용안함";
            this.btn_LoaderInputSkipA_DontUse.UseVisualStyleBackColor = false;
            // 
            // tbox_LoaderInputSkipA
            // 
            this.tbox_LoaderInputSkipA.Location = new System.Drawing.Point(3, 3);
            this.tbox_LoaderInputSkipA.Name = "tbox_LoaderInputSkipA";
            this.tbox_LoaderInputSkipA.Size = new System.Drawing.Size(74, 29);
            this.tbox_LoaderInputSkipA.TabIndex = 0;
            // 
            // btn_LoaderInputSkipA_Use
            // 
            this.btn_LoaderInputSkipA_Use.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_LoaderInputSkipA_Use.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_LoaderInputSkipA_Use.ForeColor = System.Drawing.Color.White;
            this.btn_LoaderInputSkipA_Use.Location = new System.Drawing.Point(83, 3);
            this.btn_LoaderInputSkipA_Use.Name = "btn_LoaderInputSkipA_Use";
            this.btn_LoaderInputSkipA_Use.Size = new System.Drawing.Size(49, 27);
            this.btn_LoaderInputSkipA_Use.TabIndex = 46;
            this.btn_LoaderInputSkipA_Use.Text = "사용함";
            this.btn_LoaderInputSkipA_Use.UseVisualStyleBackColor = false;
            // 
            // gbox_BufferPickerSkip
            // 
            this.gbox_BufferPickerSkip.Controls.Add(this.tableLayoutPanel19);
            this.gbox_BufferPickerSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BufferPickerSkip.ForeColor = System.Drawing.Color.White;
            this.gbox_BufferPickerSkip.Location = new System.Drawing.Point(428, 306);
            this.gbox_BufferPickerSkip.Name = "gbox_BufferPickerSkip";
            this.gbox_BufferPickerSkip.Size = new System.Drawing.Size(419, 57);
            this.gbox_BufferPickerSkip.TabIndex = 58;
            this.gbox_BufferPickerSkip.TabStop = false;
            this.gbox_BufferPickerSkip.Text = "     버퍼 피커 스킵     ";
            // 
            // tableLayoutPanel19
            // 
            this.tableLayoutPanel19.ColumnCount = 3;
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel19.Controls.Add(this.btn_BufferPickerSkip_DontUse, 2, 0);
            this.tableLayoutPanel19.Controls.Add(this.tbox_BufferPickerSkip, 0, 0);
            this.tableLayoutPanel19.Controls.Add(this.btn_BufferPickerSkip_Use, 1, 0);
            this.tableLayoutPanel19.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel19.Name = "tableLayoutPanel19";
            this.tableLayoutPanel19.RowCount = 1;
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel19.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel19.TabIndex = 1;
            // 
            // btn_BufferPickerSkip_DontUse
            // 
            this.btn_BufferPickerSkip_DontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BufferPickerSkip_DontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BufferPickerSkip_DontUse.ForeColor = System.Drawing.Color.White;
            this.btn_BufferPickerSkip_DontUse.Location = new System.Drawing.Point(298, 3);
            this.btn_BufferPickerSkip_DontUse.Name = "btn_BufferPickerSkip_DontUse";
            this.btn_BufferPickerSkip_DontUse.Size = new System.Drawing.Size(106, 26);
            this.btn_BufferPickerSkip_DontUse.TabIndex = 47;
            this.btn_BufferPickerSkip_DontUse.Text = "사용안함";
            this.btn_BufferPickerSkip_DontUse.UseVisualStyleBackColor = false;
            // 
            // tbox_BufferPickerSkip
            // 
            this.tbox_BufferPickerSkip.Location = new System.Drawing.Point(3, 3);
            this.tbox_BufferPickerSkip.Name = "tbox_BufferPickerSkip";
            this.tbox_BufferPickerSkip.Size = new System.Drawing.Size(177, 29);
            this.tbox_BufferPickerSkip.TabIndex = 0;
            // 
            // btn_BufferPickerSkip_Use
            // 
            this.btn_BufferPickerSkip_Use.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BufferPickerSkip_Use.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BufferPickerSkip_Use.ForeColor = System.Drawing.Color.White;
            this.btn_BufferPickerSkip_Use.Location = new System.Drawing.Point(186, 3);
            this.btn_BufferPickerSkip_Use.Name = "btn_BufferPickerSkip_Use";
            this.btn_BufferPickerSkip_Use.Size = new System.Drawing.Size(106, 26);
            this.btn_BufferPickerSkip_Use.TabIndex = 46;
            this.btn_BufferPickerSkip_Use.Text = "사용함";
            this.btn_BufferPickerSkip_Use.UseVisualStyleBackColor = false;
            // 
            // gbox_TiltMeasureSkip
            // 
            this.gbox_TiltMeasureSkip.Controls.Add(this.tableLayoutPanel18);
            this.gbox_TiltMeasureSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_TiltMeasureSkip.ForeColor = System.Drawing.Color.White;
            this.gbox_TiltMeasureSkip.Location = new System.Drawing.Point(428, 250);
            this.gbox_TiltMeasureSkip.Name = "gbox_TiltMeasureSkip";
            this.gbox_TiltMeasureSkip.Size = new System.Drawing.Size(419, 57);
            this.gbox_TiltMeasureSkip.TabIndex = 54;
            this.gbox_TiltMeasureSkip.TabStop = false;
            this.gbox_TiltMeasureSkip.Text = "     틸트 측정 스킵";
            // 
            // tableLayoutPanel18
            // 
            this.tableLayoutPanel18.ColumnCount = 3;
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel18.Controls.Add(this.btn_TiltMeasureSkip_DontUse, 2, 0);
            this.tableLayoutPanel18.Controls.Add(this.tbox_TiltMeasureSkip, 0, 0);
            this.tableLayoutPanel18.Controls.Add(this.btn_TiltMeasureSkip_Use, 1, 0);
            this.tableLayoutPanel18.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel18.Name = "tableLayoutPanel18";
            this.tableLayoutPanel18.RowCount = 1;
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel18.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel18.TabIndex = 1;
            // 
            // btn_TiltMeasureSkip_DontUse
            // 
            this.btn_TiltMeasureSkip_DontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_TiltMeasureSkip_DontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_TiltMeasureSkip_DontUse.ForeColor = System.Drawing.Color.White;
            this.btn_TiltMeasureSkip_DontUse.Location = new System.Drawing.Point(298, 3);
            this.btn_TiltMeasureSkip_DontUse.Name = "btn_TiltMeasureSkip_DontUse";
            this.btn_TiltMeasureSkip_DontUse.Size = new System.Drawing.Size(106, 26);
            this.btn_TiltMeasureSkip_DontUse.TabIndex = 47;
            this.btn_TiltMeasureSkip_DontUse.Text = "사용안함";
            this.btn_TiltMeasureSkip_DontUse.UseVisualStyleBackColor = false;
            // 
            // tbox_TiltMeasureSkip
            // 
            this.tbox_TiltMeasureSkip.Location = new System.Drawing.Point(3, 3);
            this.tbox_TiltMeasureSkip.Name = "tbox_TiltMeasureSkip";
            this.tbox_TiltMeasureSkip.Size = new System.Drawing.Size(177, 29);
            this.tbox_TiltMeasureSkip.TabIndex = 0;
            // 
            // btn_TiltMeasureSkip_Use
            // 
            this.btn_TiltMeasureSkip_Use.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_TiltMeasureSkip_Use.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_TiltMeasureSkip_Use.ForeColor = System.Drawing.Color.White;
            this.btn_TiltMeasureSkip_Use.Location = new System.Drawing.Point(186, 3);
            this.btn_TiltMeasureSkip_Use.Name = "btn_TiltMeasureSkip_Use";
            this.btn_TiltMeasureSkip_Use.Size = new System.Drawing.Size(106, 26);
            this.btn_TiltMeasureSkip_Use.TabIndex = 46;
            this.btn_TiltMeasureSkip_Use.Text = "사용함";
            this.btn_TiltMeasureSkip_Use.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakingAlignSkip
            // 
            this.gbox_BreakingAlignSkip.Controls.Add(this.tableLayoutPanel17);
            this.gbox_BreakingAlignSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakingAlignSkip.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakingAlignSkip.Location = new System.Drawing.Point(428, 194);
            this.gbox_BreakingAlignSkip.Name = "gbox_BreakingAlignSkip";
            this.gbox_BreakingAlignSkip.Size = new System.Drawing.Size(419, 57);
            this.gbox_BreakingAlignSkip.TabIndex = 57;
            this.gbox_BreakingAlignSkip.TabStop = false;
            this.gbox_BreakingAlignSkip.Text = "     브레이킹 얼라인 스킵     ";
            // 
            // tableLayoutPanel17
            // 
            this.tableLayoutPanel17.ColumnCount = 3;
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel17.Controls.Add(this.btn_BreakingAlignSkip_DontUse, 2, 0);
            this.tableLayoutPanel17.Controls.Add(this.tbox_BreakingAlignSkip, 0, 0);
            this.tableLayoutPanel17.Controls.Add(this.btn_BreakingAlignSkip_Use, 1, 0);
            this.tableLayoutPanel17.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel17.Name = "tableLayoutPanel17";
            this.tableLayoutPanel17.RowCount = 1;
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel17.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel17.TabIndex = 1;
            // 
            // btn_BreakingAlignSkip_DontUse
            // 
            this.btn_BreakingAlignSkip_DontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakingAlignSkip_DontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakingAlignSkip_DontUse.ForeColor = System.Drawing.Color.White;
            this.btn_BreakingAlignSkip_DontUse.Location = new System.Drawing.Point(298, 3);
            this.btn_BreakingAlignSkip_DontUse.Name = "btn_BreakingAlignSkip_DontUse";
            this.btn_BreakingAlignSkip_DontUse.Size = new System.Drawing.Size(106, 26);
            this.btn_BreakingAlignSkip_DontUse.TabIndex = 47;
            this.btn_BreakingAlignSkip_DontUse.Text = "사용안함";
            this.btn_BreakingAlignSkip_DontUse.UseVisualStyleBackColor = false;
            // 
            // tbox_BreakingAlignSkip
            // 
            this.tbox_BreakingAlignSkip.Location = new System.Drawing.Point(3, 3);
            this.tbox_BreakingAlignSkip.Name = "tbox_BreakingAlignSkip";
            this.tbox_BreakingAlignSkip.Size = new System.Drawing.Size(177, 29);
            this.tbox_BreakingAlignSkip.TabIndex = 0;
            // 
            // btn_BreakingAlignSkip_Use
            // 
            this.btn_BreakingAlignSkip_Use.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakingAlignSkip_Use.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakingAlignSkip_Use.ForeColor = System.Drawing.Color.White;
            this.btn_BreakingAlignSkip_Use.Location = new System.Drawing.Point(186, 3);
            this.btn_BreakingAlignSkip_Use.Name = "btn_BreakingAlignSkip_Use";
            this.btn_BreakingAlignSkip_Use.Size = new System.Drawing.Size(106, 26);
            this.btn_BreakingAlignSkip_Use.TabIndex = 46;
            this.btn_BreakingAlignSkip_Use.Text = "사용함";
            this.btn_BreakingAlignSkip_Use.UseVisualStyleBackColor = false;
            // 
            // gbox_BcrSkip
            // 
            this.gbox_BcrSkip.Controls.Add(this.tableLayoutPanel16);
            this.gbox_BcrSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BcrSkip.ForeColor = System.Drawing.Color.White;
            this.gbox_BcrSkip.Location = new System.Drawing.Point(3, 306);
            this.gbox_BcrSkip.Name = "gbox_BcrSkip";
            this.gbox_BcrSkip.Size = new System.Drawing.Size(419, 57);
            this.gbox_BcrSkip.TabIndex = 54;
            this.gbox_BcrSkip.TabStop = false;
            this.gbox_BcrSkip.Text = "     BCR 스킵     ";
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.ColumnCount = 3;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel16.Controls.Add(this.btn_BcrSkip_DontUse, 2, 0);
            this.tableLayoutPanel16.Controls.Add(this.tbox_BcrSkip, 0, 0);
            this.tableLayoutPanel16.Controls.Add(this.btn_BcrSkip_Use, 1, 0);
            this.tableLayoutPanel16.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 1;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel16.TabIndex = 1;
            // 
            // btn_BcrSkip_DontUse
            // 
            this.btn_BcrSkip_DontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BcrSkip_DontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BcrSkip_DontUse.ForeColor = System.Drawing.Color.White;
            this.btn_BcrSkip_DontUse.Location = new System.Drawing.Point(298, 3);
            this.btn_BcrSkip_DontUse.Name = "btn_BcrSkip_DontUse";
            this.btn_BcrSkip_DontUse.Size = new System.Drawing.Size(106, 26);
            this.btn_BcrSkip_DontUse.TabIndex = 47;
            this.btn_BcrSkip_DontUse.Text = "사용안함";
            this.btn_BcrSkip_DontUse.UseVisualStyleBackColor = false;
            // 
            // tbox_BcrSkip
            // 
            this.tbox_BcrSkip.Location = new System.Drawing.Point(3, 3);
            this.tbox_BcrSkip.Name = "tbox_BcrSkip";
            this.tbox_BcrSkip.Size = new System.Drawing.Size(177, 29);
            this.tbox_BcrSkip.TabIndex = 0;
            // 
            // btn_BcrSkip_Use
            // 
            this.btn_BcrSkip_Use.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BcrSkip_Use.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BcrSkip_Use.ForeColor = System.Drawing.Color.White;
            this.btn_BcrSkip_Use.Location = new System.Drawing.Point(186, 3);
            this.btn_BcrSkip_Use.Name = "btn_BcrSkip_Use";
            this.btn_BcrSkip_Use.Size = new System.Drawing.Size(106, 26);
            this.btn_BcrSkip_Use.TabIndex = 46;
            this.btn_BcrSkip_Use.Text = "사용함";
            this.btn_BcrSkip_Use.UseVisualStyleBackColor = false;
            // 
            // gbox_McrSkip
            // 
            this.gbox_McrSkip.Controls.Add(this.tableLayoutPanel15);
            this.gbox_McrSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_McrSkip.ForeColor = System.Drawing.Color.White;
            this.gbox_McrSkip.Location = new System.Drawing.Point(3, 250);
            this.gbox_McrSkip.Name = "gbox_McrSkip";
            this.gbox_McrSkip.Size = new System.Drawing.Size(419, 57);
            this.gbox_McrSkip.TabIndex = 54;
            this.gbox_McrSkip.TabStop = false;
            this.gbox_McrSkip.Text = "     MCR 스킵    ";
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 3;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel15.Controls.Add(this.btn_McrSkip_DontUse, 2, 0);
            this.tableLayoutPanel15.Controls.Add(this.tbox_McrSkip, 0, 0);
            this.tableLayoutPanel15.Controls.Add(this.btn_McrSkip_Use, 1, 0);
            this.tableLayoutPanel15.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 1;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel15.TabIndex = 1;
            // 
            // btn_McrSkip_DontUse
            // 
            this.btn_McrSkip_DontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_McrSkip_DontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_McrSkip_DontUse.ForeColor = System.Drawing.Color.White;
            this.btn_McrSkip_DontUse.Location = new System.Drawing.Point(298, 3);
            this.btn_McrSkip_DontUse.Name = "btn_McrSkip_DontUse";
            this.btn_McrSkip_DontUse.Size = new System.Drawing.Size(106, 26);
            this.btn_McrSkip_DontUse.TabIndex = 47;
            this.btn_McrSkip_DontUse.Text = "사용안함";
            this.btn_McrSkip_DontUse.UseVisualStyleBackColor = false;
            // 
            // tbox_McrSkip
            // 
            this.tbox_McrSkip.Location = new System.Drawing.Point(3, 3);
            this.tbox_McrSkip.Name = "tbox_McrSkip";
            this.tbox_McrSkip.Size = new System.Drawing.Size(177, 29);
            this.tbox_McrSkip.TabIndex = 0;
            // 
            // btn_McrSkip_Use
            // 
            this.btn_McrSkip_Use.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_McrSkip_Use.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_McrSkip_Use.ForeColor = System.Drawing.Color.White;
            this.btn_McrSkip_Use.Location = new System.Drawing.Point(186, 3);
            this.btn_McrSkip_Use.Name = "btn_McrSkip_Use";
            this.btn_McrSkip_Use.Size = new System.Drawing.Size(106, 26);
            this.btn_McrSkip_Use.TabIndex = 46;
            this.btn_McrSkip_Use.Text = "사용함";
            this.btn_McrSkip_Use.UseVisualStyleBackColor = false;
            // 
            // gbox_InspectionSkip
            // 
            this.gbox_InspectionSkip.Controls.Add(this.tableLayoutPanel14);
            this.gbox_InspectionSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_InspectionSkip.ForeColor = System.Drawing.Color.White;
            this.gbox_InspectionSkip.Location = new System.Drawing.Point(3, 194);
            this.gbox_InspectionSkip.Name = "gbox_InspectionSkip";
            this.gbox_InspectionSkip.Size = new System.Drawing.Size(419, 57);
            this.gbox_InspectionSkip.TabIndex = 56;
            this.gbox_InspectionSkip.TabStop = false;
            this.gbox_InspectionSkip.Text = "     인스펙션 스킵     ";
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 3;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel14.Controls.Add(this.btn_InspectionSkip_DontUse, 2, 0);
            this.tableLayoutPanel14.Controls.Add(this.tbox_InspectionSkip, 0, 0);
            this.tableLayoutPanel14.Controls.Add(this.btn_InspectionSkip_Use, 1, 0);
            this.tableLayoutPanel14.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 1;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel14.TabIndex = 1;
            // 
            // btn_InspectionSkip_DontUse
            // 
            this.btn_InspectionSkip_DontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_InspectionSkip_DontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_InspectionSkip_DontUse.ForeColor = System.Drawing.Color.White;
            this.btn_InspectionSkip_DontUse.Location = new System.Drawing.Point(298, 3);
            this.btn_InspectionSkip_DontUse.Name = "btn_InspectionSkip_DontUse";
            this.btn_InspectionSkip_DontUse.Size = new System.Drawing.Size(106, 26);
            this.btn_InspectionSkip_DontUse.TabIndex = 47;
            this.btn_InspectionSkip_DontUse.Text = "사용안함";
            this.btn_InspectionSkip_DontUse.UseVisualStyleBackColor = false;
            // 
            // tbox_InspectionSkip
            // 
            this.tbox_InspectionSkip.Location = new System.Drawing.Point(3, 3);
            this.tbox_InspectionSkip.Name = "tbox_InspectionSkip";
            this.tbox_InspectionSkip.Size = new System.Drawing.Size(177, 29);
            this.tbox_InspectionSkip.TabIndex = 0;
            // 
            // btn_InspectionSkip_Use
            // 
            this.btn_InspectionSkip_Use.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_InspectionSkip_Use.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_InspectionSkip_Use.ForeColor = System.Drawing.Color.White;
            this.btn_InspectionSkip_Use.Location = new System.Drawing.Point(186, 3);
            this.btn_InspectionSkip_Use.Name = "btn_InspectionSkip_Use";
            this.btn_InspectionSkip_Use.Size = new System.Drawing.Size(106, 26);
            this.btn_InspectionSkip_Use.TabIndex = 46;
            this.btn_InspectionSkip_Use.Text = "사용함";
            this.btn_InspectionSkip_Use.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakingMotionSkip
            // 
            this.gbox_BreakingMotionSkip.Controls.Add(this.tableLayoutPanel13);
            this.gbox_BreakingMotionSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakingMotionSkip.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakingMotionSkip.Location = new System.Drawing.Point(428, 138);
            this.gbox_BreakingMotionSkip.Name = "gbox_BreakingMotionSkip";
            this.gbox_BreakingMotionSkip.Size = new System.Drawing.Size(419, 57);
            this.gbox_BreakingMotionSkip.TabIndex = 54;
            this.gbox_BreakingMotionSkip.TabStop = false;
            this.gbox_BreakingMotionSkip.Text = "     브레이킹 모션 스킵     ";
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 3;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel13.Controls.Add(this.btn_BreakingMotionSkip_DontUse, 2, 0);
            this.tableLayoutPanel13.Controls.Add(this.tbox_BreakingMotionSkip, 0, 0);
            this.tableLayoutPanel13.Controls.Add(this.btn_BreakingMotionSkip_Use, 1, 0);
            this.tableLayoutPanel13.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 1;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel13.TabIndex = 1;
            // 
            // btn_BreakingMotionSkip_DontUse
            // 
            this.btn_BreakingMotionSkip_DontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakingMotionSkip_DontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakingMotionSkip_DontUse.ForeColor = System.Drawing.Color.White;
            this.btn_BreakingMotionSkip_DontUse.Location = new System.Drawing.Point(298, 3);
            this.btn_BreakingMotionSkip_DontUse.Name = "btn_BreakingMotionSkip_DontUse";
            this.btn_BreakingMotionSkip_DontUse.Size = new System.Drawing.Size(106, 26);
            this.btn_BreakingMotionSkip_DontUse.TabIndex = 47;
            this.btn_BreakingMotionSkip_DontUse.Text = "사용안함";
            this.btn_BreakingMotionSkip_DontUse.UseVisualStyleBackColor = false;
            // 
            // tbox_BreakingMotionSkip
            // 
            this.tbox_BreakingMotionSkip.Location = new System.Drawing.Point(3, 3);
            this.tbox_BreakingMotionSkip.Name = "tbox_BreakingMotionSkip";
            this.tbox_BreakingMotionSkip.Size = new System.Drawing.Size(177, 29);
            this.tbox_BreakingMotionSkip.TabIndex = 0;
            // 
            // btn_BreakingMotionSkip_Use
            // 
            this.btn_BreakingMotionSkip_Use.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakingMotionSkip_Use.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakingMotionSkip_Use.ForeColor = System.Drawing.Color.White;
            this.btn_BreakingMotionSkip_Use.Location = new System.Drawing.Point(186, 3);
            this.btn_BreakingMotionSkip_Use.Name = "btn_BreakingMotionSkip_Use";
            this.btn_BreakingMotionSkip_Use.Size = new System.Drawing.Size(106, 26);
            this.btn_BreakingMotionSkip_Use.TabIndex = 46;
            this.btn_BreakingMotionSkip_Use.Text = "사용함";
            this.btn_BreakingMotionSkip_Use.UseVisualStyleBackColor = false;
            // 
            // gbox_LaserSkip
            // 
            this.gbox_LaserSkip.Controls.Add(this.tableLayoutPanel12);
            this.gbox_LaserSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_LaserSkip.ForeColor = System.Drawing.Color.White;
            this.gbox_LaserSkip.Location = new System.Drawing.Point(3, 138);
            this.gbox_LaserSkip.Name = "gbox_LaserSkip";
            this.gbox_LaserSkip.Size = new System.Drawing.Size(419, 57);
            this.gbox_LaserSkip.TabIndex = 55;
            this.gbox_LaserSkip.TabStop = false;
            this.gbox_LaserSkip.Text = "     Laser 스킵     ";
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 3;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel12.Controls.Add(this.btn_LaserSkip_DontUse, 2, 0);
            this.tableLayoutPanel12.Controls.Add(this.tbox_LaserSkip, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.btn_LaserSkip_Use, 1, 0);
            this.tableLayoutPanel12.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 1;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel12.TabIndex = 1;
            // 
            // btn_LaserSkip_DontUse
            // 
            this.btn_LaserSkip_DontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_LaserSkip_DontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_LaserSkip_DontUse.ForeColor = System.Drawing.Color.White;
            this.btn_LaserSkip_DontUse.Location = new System.Drawing.Point(298, 3);
            this.btn_LaserSkip_DontUse.Name = "btn_LaserSkip_DontUse";
            this.btn_LaserSkip_DontUse.Size = new System.Drawing.Size(106, 26);
            this.btn_LaserSkip_DontUse.TabIndex = 47;
            this.btn_LaserSkip_DontUse.Text = "사용안함";
            this.btn_LaserSkip_DontUse.UseVisualStyleBackColor = false;
            // 
            // tbox_LaserSkip
            // 
            this.tbox_LaserSkip.Location = new System.Drawing.Point(3, 3);
            this.tbox_LaserSkip.Name = "tbox_LaserSkip";
            this.tbox_LaserSkip.Size = new System.Drawing.Size(177, 29);
            this.tbox_LaserSkip.TabIndex = 0;
            // 
            // btn_LaserSkip_Use
            // 
            this.btn_LaserSkip_Use.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_LaserSkip_Use.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_LaserSkip_Use.ForeColor = System.Drawing.Color.White;
            this.btn_LaserSkip_Use.Location = new System.Drawing.Point(186, 3);
            this.btn_LaserSkip_Use.Name = "btn_LaserSkip_Use";
            this.btn_LaserSkip_Use.Size = new System.Drawing.Size(106, 26);
            this.btn_LaserSkip_Use.TabIndex = 46;
            this.btn_LaserSkip_Use.Text = "사용함";
            this.btn_LaserSkip_Use.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakingBSkip
            // 
            this.gbox_BreakingBSkip.Controls.Add(this.tableLayoutPanel11);
            this.gbox_BreakingBSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakingBSkip.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakingBSkip.Location = new System.Drawing.Point(428, 82);
            this.gbox_BreakingBSkip.Name = "gbox_BreakingBSkip";
            this.gbox_BreakingBSkip.Size = new System.Drawing.Size(419, 57);
            this.gbox_BreakingBSkip.TabIndex = 54;
            this.gbox_BreakingBSkip.TabStop = false;
            this.gbox_BreakingBSkip.Text = "     브레이킹 B 물류 스킵     ";
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 3;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel11.Controls.Add(this.btn_BreakingBSkip_DontUse, 2, 0);
            this.tableLayoutPanel11.Controls.Add(this.tbox_BreakingBSkip, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this.btn_BreakingBSkip_Use, 1, 0);
            this.tableLayoutPanel11.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 1;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel11.TabIndex = 1;
            // 
            // btn_BreakingBSkip_DontUse
            // 
            this.btn_BreakingBSkip_DontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakingBSkip_DontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakingBSkip_DontUse.ForeColor = System.Drawing.Color.White;
            this.btn_BreakingBSkip_DontUse.Location = new System.Drawing.Point(298, 3);
            this.btn_BreakingBSkip_DontUse.Name = "btn_BreakingBSkip_DontUse";
            this.btn_BreakingBSkip_DontUse.Size = new System.Drawing.Size(106, 26);
            this.btn_BreakingBSkip_DontUse.TabIndex = 47;
            this.btn_BreakingBSkip_DontUse.Text = "사용안함";
            this.btn_BreakingBSkip_DontUse.UseVisualStyleBackColor = false;
            // 
            // tbox_BreakingBSkip
            // 
            this.tbox_BreakingBSkip.Location = new System.Drawing.Point(3, 3);
            this.tbox_BreakingBSkip.Name = "tbox_BreakingBSkip";
            this.tbox_BreakingBSkip.Size = new System.Drawing.Size(177, 29);
            this.tbox_BreakingBSkip.TabIndex = 0;
            // 
            // btn_BreakingBSkip_Use
            // 
            this.btn_BreakingBSkip_Use.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakingBSkip_Use.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakingBSkip_Use.ForeColor = System.Drawing.Color.White;
            this.btn_BreakingBSkip_Use.Location = new System.Drawing.Point(186, 3);
            this.btn_BreakingBSkip_Use.Name = "btn_BreakingBSkip_Use";
            this.btn_BreakingBSkip_Use.Size = new System.Drawing.Size(106, 26);
            this.btn_BreakingBSkip_Use.TabIndex = 46;
            this.btn_BreakingBSkip_Use.Text = "사용함";
            this.btn_BreakingBSkip_Use.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakingASkip
            // 
            this.gbox_BreakingASkip.Controls.Add(this.tableLayoutPanel10);
            this.gbox_BreakingASkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakingASkip.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakingASkip.Location = new System.Drawing.Point(3, 82);
            this.gbox_BreakingASkip.Name = "gbox_BreakingASkip";
            this.gbox_BreakingASkip.Size = new System.Drawing.Size(419, 57);
            this.gbox_BreakingASkip.TabIndex = 54;
            this.gbox_BreakingASkip.TabStop = false;
            this.gbox_BreakingASkip.Text = "     브레이킹 A 물류 스킵     ";
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 3;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel10.Controls.Add(this.btn_BreakingASkip_DontUse, 2, 0);
            this.tableLayoutPanel10.Controls.Add(this.tbox_BreakingASkip, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.btn_BreakingASkip_Use, 1, 0);
            this.tableLayoutPanel10.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 1;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel10.TabIndex = 1;
            // 
            // btn_BreakingASkip_DontUse
            // 
            this.btn_BreakingASkip_DontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakingASkip_DontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakingASkip_DontUse.ForeColor = System.Drawing.Color.White;
            this.btn_BreakingASkip_DontUse.Location = new System.Drawing.Point(298, 3);
            this.btn_BreakingASkip_DontUse.Name = "btn_BreakingASkip_DontUse";
            this.btn_BreakingASkip_DontUse.Size = new System.Drawing.Size(106, 26);
            this.btn_BreakingASkip_DontUse.TabIndex = 47;
            this.btn_BreakingASkip_DontUse.Text = "사용안함";
            this.btn_BreakingASkip_DontUse.UseVisualStyleBackColor = false;
            // 
            // tbox_BreakingASkip
            // 
            this.tbox_BreakingASkip.Location = new System.Drawing.Point(3, 3);
            this.tbox_BreakingASkip.Name = "tbox_BreakingASkip";
            this.tbox_BreakingASkip.Size = new System.Drawing.Size(177, 29);
            this.tbox_BreakingASkip.TabIndex = 0;
            // 
            // btn_BreakingASkip_Use
            // 
            this.btn_BreakingASkip_Use.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakingASkip_Use.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakingASkip_Use.ForeColor = System.Drawing.Color.White;
            this.btn_BreakingASkip_Use.Location = new System.Drawing.Point(186, 3);
            this.btn_BreakingASkip_Use.Name = "btn_BreakingASkip_Use";
            this.btn_BreakingASkip_Use.Size = new System.Drawing.Size(106, 26);
            this.btn_BreakingASkip_Use.TabIndex = 46;
            this.btn_BreakingASkip_Use.Text = "사용함";
            this.btn_BreakingASkip_Use.UseVisualStyleBackColor = false;
            // 
            // gbox_ProcessBSkip
            // 
            this.gbox_ProcessBSkip.Controls.Add(this.tableLayoutPanel9);
            this.gbox_ProcessBSkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_ProcessBSkip.ForeColor = System.Drawing.Color.White;
            this.gbox_ProcessBSkip.Location = new System.Drawing.Point(428, 26);
            this.gbox_ProcessBSkip.Name = "gbox_ProcessBSkip";
            this.gbox_ProcessBSkip.Size = new System.Drawing.Size(419, 57);
            this.gbox_ProcessBSkip.TabIndex = 53;
            this.gbox_ProcessBSkip.TabStop = false;
            this.gbox_ProcessBSkip.Text = "     프로세스 B 물류 스킵     ";
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 3;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel9.Controls.Add(this.btn_ProcessBSkip_DontUse, 2, 0);
            this.tableLayoutPanel9.Controls.Add(this.tbox_ProcessBSkip, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.btn_ProcessBSkip_Use, 1, 0);
            this.tableLayoutPanel9.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel9.TabIndex = 1;
            // 
            // btn_ProcessBSkip_DontUse
            // 
            this.btn_ProcessBSkip_DontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_ProcessBSkip_DontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_ProcessBSkip_DontUse.ForeColor = System.Drawing.Color.White;
            this.btn_ProcessBSkip_DontUse.Location = new System.Drawing.Point(298, 3);
            this.btn_ProcessBSkip_DontUse.Name = "btn_ProcessBSkip_DontUse";
            this.btn_ProcessBSkip_DontUse.Size = new System.Drawing.Size(106, 26);
            this.btn_ProcessBSkip_DontUse.TabIndex = 47;
            this.btn_ProcessBSkip_DontUse.Text = "사용안함";
            this.btn_ProcessBSkip_DontUse.UseVisualStyleBackColor = false;
            // 
            // tbox_ProcessBSkip
            // 
            this.tbox_ProcessBSkip.Location = new System.Drawing.Point(3, 3);
            this.tbox_ProcessBSkip.Name = "tbox_ProcessBSkip";
            this.tbox_ProcessBSkip.Size = new System.Drawing.Size(177, 29);
            this.tbox_ProcessBSkip.TabIndex = 0;
            // 
            // btn_ProcessBSkip_Use
            // 
            this.btn_ProcessBSkip_Use.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_ProcessBSkip_Use.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_ProcessBSkip_Use.ForeColor = System.Drawing.Color.White;
            this.btn_ProcessBSkip_Use.Location = new System.Drawing.Point(186, 3);
            this.btn_ProcessBSkip_Use.Name = "btn_ProcessBSkip_Use";
            this.btn_ProcessBSkip_Use.Size = new System.Drawing.Size(106, 26);
            this.btn_ProcessBSkip_Use.TabIndex = 46;
            this.btn_ProcessBSkip_Use.Text = "사용함";
            this.btn_ProcessBSkip_Use.UseVisualStyleBackColor = false;
            // 
            // gbox_ProcessASkip
            // 
            this.gbox_ProcessASkip.Controls.Add(this.tableLayoutPanel8);
            this.gbox_ProcessASkip.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_ProcessASkip.ForeColor = System.Drawing.Color.White;
            this.gbox_ProcessASkip.Location = new System.Drawing.Point(3, 26);
            this.gbox_ProcessASkip.Name = "gbox_ProcessASkip";
            this.gbox_ProcessASkip.Size = new System.Drawing.Size(419, 57);
            this.gbox_ProcessASkip.TabIndex = 53;
            this.gbox_ProcessASkip.TabStop = false;
            this.gbox_ProcessASkip.Text = "     프로세스 A 물류 스킵     ";
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 3;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.5F));
            this.tableLayoutPanel8.Controls.Add(this.btn_ProcessASkip_DontUse, 2, 0);
            this.tableLayoutPanel8.Controls.Add(this.tbox_ProcessASkip, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.btn_ProcessASkip_Use, 1, 0);
            this.tableLayoutPanel8.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(408, 32);
            this.tableLayoutPanel8.TabIndex = 1;
            // 
            // btn_ProcessASkip_DontUse
            // 
            this.btn_ProcessASkip_DontUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_ProcessASkip_DontUse.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_ProcessASkip_DontUse.ForeColor = System.Drawing.Color.White;
            this.btn_ProcessASkip_DontUse.Location = new System.Drawing.Point(298, 3);
            this.btn_ProcessASkip_DontUse.Name = "btn_ProcessASkip_DontUse";
            this.btn_ProcessASkip_DontUse.Size = new System.Drawing.Size(106, 26);
            this.btn_ProcessASkip_DontUse.TabIndex = 47;
            this.btn_ProcessASkip_DontUse.Text = "사용안함";
            this.btn_ProcessASkip_DontUse.UseVisualStyleBackColor = false;
            // 
            // tbox_ProcessASkip
            // 
            this.tbox_ProcessASkip.Location = new System.Drawing.Point(3, 3);
            this.tbox_ProcessASkip.Name = "tbox_ProcessASkip";
            this.tbox_ProcessASkip.Size = new System.Drawing.Size(177, 29);
            this.tbox_ProcessASkip.TabIndex = 0;
            // 
            // btn_ProcessASkip_Use
            // 
            this.btn_ProcessASkip_Use.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_ProcessASkip_Use.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_ProcessASkip_Use.ForeColor = System.Drawing.Color.White;
            this.btn_ProcessASkip_Use.Location = new System.Drawing.Point(186, 3);
            this.btn_ProcessASkip_Use.Name = "btn_ProcessASkip_Use";
            this.btn_ProcessASkip_Use.Size = new System.Drawing.Size(106, 26);
            this.btn_ProcessASkip_Use.TabIndex = 46;
            this.btn_ProcessASkip_Use.Text = "사용함";
            this.btn_ProcessASkip_Use.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel32, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel33, 0, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(875, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.513872F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 93.48613F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(862, 821);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // tableLayoutPanel32
            // 
            this.tableLayoutPanel32.BackColor = System.Drawing.SystemColors.Highlight;
            this.tableLayoutPanel32.ColumnCount = 3;
            this.tableLayoutPanel32.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.5F));
            this.tableLayoutPanel32.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.01746F));
            this.tableLayoutPanel32.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.49127F));
            this.tableLayoutPanel32.Controls.Add(this.lbl_Setting, 1, 0);
            this.tableLayoutPanel32.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel32.Name = "tableLayoutPanel32";
            this.tableLayoutPanel32.RowCount = 1;
            this.tableLayoutPanel32.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel32.Size = new System.Drawing.Size(856, 47);
            this.tableLayoutPanel32.TabIndex = 1;
            // 
            // lbl_Setting
            // 
            this.lbl_Setting.AutoSize = true;
            this.lbl_Setting.Font = new System.Drawing.Font("맑은 고딕", 14F, System.Drawing.FontStyle.Bold);
            this.lbl_Setting.Location = new System.Drawing.Point(398, 10);
            this.lbl_Setting.Margin = new System.Windows.Forms.Padding(35, 10, 3, 0);
            this.lbl_Setting.Name = "lbl_Setting";
            this.lbl_Setting.Size = new System.Drawing.Size(76, 25);
            this.lbl_Setting.TabIndex = 1;
            this.lbl_Setting.Text = "Setting";
            // 
            // tableLayoutPanel33
            // 
            this.tableLayoutPanel33.ColumnCount = 2;
            this.tableLayoutPanel33.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel33.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel33.Controls.Add(this.tableLayoutPanel34, 0, 0);
            this.tableLayoutPanel33.Controls.Add(this.tableLayoutPanel41, 1, 0);
            this.tableLayoutPanel33.Location = new System.Drawing.Point(3, 56);
            this.tableLayoutPanel33.Name = "tableLayoutPanel33";
            this.tableLayoutPanel33.RowCount = 1;
            this.tableLayoutPanel33.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel33.Size = new System.Drawing.Size(856, 762);
            this.tableLayoutPanel33.TabIndex = 2;
            // 
            // tableLayoutPanel34
            // 
            this.tableLayoutPanel34.ColumnCount = 1;
            this.tableLayoutPanel34.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel34.Controls.Add(this.gbox_AutoDeleteCycle, 0, 5);
            this.tableLayoutPanel34.Controls.Add(this.gbox_BreakingCount, 0, 4);
            this.tableLayoutPanel34.Controls.Add(this.gbox_DoorOpenSpeed, 0, 3);
            this.tableLayoutPanel34.Controls.Add(this.gbox_CountError, 0, 2);
            this.tableLayoutPanel34.Controls.Add(this.gbox_Timeout, 0, 1);
            this.tableLayoutPanel34.Controls.Add(this.gbox_Setting_DistributionMode, 0, 0);
            this.tableLayoutPanel34.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel34.Name = "tableLayoutPanel34";
            this.tableLayoutPanel34.RowCount = 6;
            this.tableLayoutPanel34.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 129F));
            this.tableLayoutPanel34.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 269F));
            this.tableLayoutPanel34.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 101F));
            this.tableLayoutPanel34.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 66F));
            this.tableLayoutPanel34.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 106F));
            this.tableLayoutPanel34.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 17F));
            this.tableLayoutPanel34.Size = new System.Drawing.Size(422, 735);
            this.tableLayoutPanel34.TabIndex = 3;
            // 
            // gbox_AutoDeleteCycle
            // 
            this.gbox_AutoDeleteCycle.Controls.Add(this.tableLayoutPanel40);
            this.gbox_AutoDeleteCycle.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_AutoDeleteCycle.ForeColor = System.Drawing.Color.White;
            this.gbox_AutoDeleteCycle.Location = new System.Drawing.Point(3, 674);
            this.gbox_AutoDeleteCycle.Name = "gbox_AutoDeleteCycle";
            this.gbox_AutoDeleteCycle.Size = new System.Drawing.Size(416, 58);
            this.gbox_AutoDeleteCycle.TabIndex = 59;
            this.gbox_AutoDeleteCycle.TabStop = false;
            this.gbox_AutoDeleteCycle.Text = "     Auto Delete 주기    ";
            // 
            // tableLayoutPanel40
            // 
            this.tableLayoutPanel40.ColumnCount = 2;
            this.tableLayoutPanel40.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel40.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel40.Controls.Add(this.tbox_AutoDeleteCycle, 1, 0);
            this.tableLayoutPanel40.Controls.Add(this.lbl_AutoDeleteCycle, 0, 0);
            this.tableLayoutPanel40.Location = new System.Drawing.Point(6, 22);
            this.tableLayoutPanel40.Name = "tableLayoutPanel40";
            this.tableLayoutPanel40.RowCount = 1;
            this.tableLayoutPanel40.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel40.Size = new System.Drawing.Size(404, 30);
            this.tableLayoutPanel40.TabIndex = 1;
            // 
            // tbox_AutoDeleteCycle
            // 
            this.tbox_AutoDeleteCycle.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_AutoDeleteCycle.Location = new System.Drawing.Point(205, 3);
            this.tbox_AutoDeleteCycle.Name = "tbox_AutoDeleteCycle";
            this.tbox_AutoDeleteCycle.Size = new System.Drawing.Size(196, 22);
            this.tbox_AutoDeleteCycle.TabIndex = 0;
            // 
            // lbl_AutoDeleteCycle
            // 
            this.lbl_AutoDeleteCycle.AutoSize = true;
            this.lbl_AutoDeleteCycle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_AutoDeleteCycle.Location = new System.Drawing.Point(3, 6);
            this.lbl_AutoDeleteCycle.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_AutoDeleteCycle.Name = "lbl_AutoDeleteCycle";
            this.lbl_AutoDeleteCycle.Size = new System.Drawing.Size(145, 15);
            this.lbl_AutoDeleteCycle.TabIndex = 2;
            this.lbl_AutoDeleteCycle.Text = "Auto Delete 주기[Day] : ";
            // 
            // gbox_BreakingCount
            // 
            this.gbox_BreakingCount.Controls.Add(this.tableLayoutPanel39);
            this.gbox_BreakingCount.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakingCount.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakingCount.Location = new System.Drawing.Point(3, 568);
            this.gbox_BreakingCount.Name = "gbox_BreakingCount";
            this.gbox_BreakingCount.Size = new System.Drawing.Size(416, 95);
            this.gbox_BreakingCount.TabIndex = 58;
            this.gbox_BreakingCount.TabStop = false;
            this.gbox_BreakingCount.Text = "     브레이킹 횟수    ";
            // 
            // tableLayoutPanel39
            // 
            this.tableLayoutPanel39.ColumnCount = 2;
            this.tableLayoutPanel39.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel39.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel39.Controls.Add(this.lbl_CriticalAlarm, 0, 1);
            this.tableLayoutPanel39.Controls.Add(this.tbox_LightAlarm, 1, 0);
            this.tableLayoutPanel39.Controls.Add(this.lbl_LightAlarm, 0, 0);
            this.tableLayoutPanel39.Controls.Add(this.tbox_CriticalAlarm, 1, 1);
            this.tableLayoutPanel39.Location = new System.Drawing.Point(6, 22);
            this.tableLayoutPanel39.Name = "tableLayoutPanel39";
            this.tableLayoutPanel39.RowCount = 2;
            this.tableLayoutPanel39.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel39.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel39.Size = new System.Drawing.Size(404, 67);
            this.tableLayoutPanel39.TabIndex = 1;
            // 
            // lbl_CriticalAlarm
            // 
            this.lbl_CriticalAlarm.AutoSize = true;
            this.lbl_CriticalAlarm.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_CriticalAlarm.Location = new System.Drawing.Point(3, 39);
            this.lbl_CriticalAlarm.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_CriticalAlarm.Name = "lbl_CriticalAlarm";
            this.lbl_CriticalAlarm.Size = new System.Drawing.Size(90, 15);
            this.lbl_CriticalAlarm.TabIndex = 3;
            this.lbl_CriticalAlarm.Text = "중알림[횟수] :  ";
            // 
            // tbox_LightAlarm
            // 
            this.tbox_LightAlarm.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_LightAlarm.Location = new System.Drawing.Point(205, 3);
            this.tbox_LightAlarm.Name = "tbox_LightAlarm";
            this.tbox_LightAlarm.Size = new System.Drawing.Size(196, 22);
            this.tbox_LightAlarm.TabIndex = 0;
            // 
            // lbl_LightAlarm
            // 
            this.lbl_LightAlarm.AutoSize = true;
            this.lbl_LightAlarm.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_LightAlarm.Location = new System.Drawing.Point(3, 6);
            this.lbl_LightAlarm.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_LightAlarm.Name = "lbl_LightAlarm";
            this.lbl_LightAlarm.Size = new System.Drawing.Size(86, 15);
            this.lbl_LightAlarm.TabIndex = 2;
            this.lbl_LightAlarm.Text = "경알림[횟수] : ";
            // 
            // tbox_CriticalAlarm
            // 
            this.tbox_CriticalAlarm.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_CriticalAlarm.Location = new System.Drawing.Point(205, 36);
            this.tbox_CriticalAlarm.Name = "tbox_CriticalAlarm";
            this.tbox_CriticalAlarm.Size = new System.Drawing.Size(196, 22);
            this.tbox_CriticalAlarm.TabIndex = 1;
            // 
            // gbox_DoorOpenSpeed
            // 
            this.gbox_DoorOpenSpeed.Controls.Add(this.tableLayoutPanel38);
            this.gbox_DoorOpenSpeed.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_DoorOpenSpeed.ForeColor = System.Drawing.Color.White;
            this.gbox_DoorOpenSpeed.Location = new System.Drawing.Point(3, 502);
            this.gbox_DoorOpenSpeed.Name = "gbox_DoorOpenSpeed";
            this.gbox_DoorOpenSpeed.Size = new System.Drawing.Size(416, 60);
            this.gbox_DoorOpenSpeed.TabIndex = 57;
            this.gbox_DoorOpenSpeed.TabStop = false;
            this.gbox_DoorOpenSpeed.Text = "     도어 오픈 스피드    ";
            // 
            // tableLayoutPanel38
            // 
            this.tableLayoutPanel38.ColumnCount = 2;
            this.tableLayoutPanel38.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel38.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel38.Controls.Add(this.tbox_DoorOpenSpeed, 1, 0);
            this.tableLayoutPanel38.Controls.Add(this.lbl_DoorOpenSpped, 0, 0);
            this.tableLayoutPanel38.Location = new System.Drawing.Point(6, 22);
            this.tableLayoutPanel38.Name = "tableLayoutPanel38";
            this.tableLayoutPanel38.RowCount = 1;
            this.tableLayoutPanel38.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel38.Size = new System.Drawing.Size(404, 30);
            this.tableLayoutPanel38.TabIndex = 1;
            // 
            // tbox_DoorOpenSpeed
            // 
            this.tbox_DoorOpenSpeed.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_DoorOpenSpeed.Location = new System.Drawing.Point(205, 3);
            this.tbox_DoorOpenSpeed.Name = "tbox_DoorOpenSpeed";
            this.tbox_DoorOpenSpeed.Size = new System.Drawing.Size(196, 22);
            this.tbox_DoorOpenSpeed.TabIndex = 0;
            // 
            // lbl_DoorOpenSpped
            // 
            this.lbl_DoorOpenSpped.AutoSize = true;
            this.lbl_DoorOpenSpped.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_DoorOpenSpped.Location = new System.Drawing.Point(3, 6);
            this.lbl_DoorOpenSpped.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_DoorOpenSpped.Name = "lbl_DoorOpenSpped";
            this.lbl_DoorOpenSpped.Size = new System.Drawing.Size(176, 15);
            this.lbl_DoorOpenSpped.TabIndex = 2;
            this.lbl_DoorOpenSpped.Text = "Door Open Speed[mm/sec] : ";
            // 
            // gbox_CountError
            // 
            this.gbox_CountError.Controls.Add(this.tableLayoutPanel37);
            this.gbox_CountError.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CountError.ForeColor = System.Drawing.Color.White;
            this.gbox_CountError.Location = new System.Drawing.Point(3, 401);
            this.gbox_CountError.Name = "gbox_CountError";
            this.gbox_CountError.Size = new System.Drawing.Size(416, 95);
            this.gbox_CountError.TabIndex = 56;
            this.gbox_CountError.TabStop = false;
            this.gbox_CountError.Text = "     Count Error    ";
            // 
            // tableLayoutPanel37
            // 
            this.tableLayoutPanel37.ColumnCount = 2;
            this.tableLayoutPanel37.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel37.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel37.Controls.Add(this.lbl_INSP, 0, 1);
            this.tableLayoutPanel37.Controls.Add(this.tbox_MCR, 1, 0);
            this.tableLayoutPanel37.Controls.Add(this.lbl_MCR, 0, 0);
            this.tableLayoutPanel37.Controls.Add(this.tbox_INSP, 1, 1);
            this.tableLayoutPanel37.Location = new System.Drawing.Point(6, 22);
            this.tableLayoutPanel37.Name = "tableLayoutPanel37";
            this.tableLayoutPanel37.RowCount = 2;
            this.tableLayoutPanel37.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel37.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel37.Size = new System.Drawing.Size(404, 67);
            this.tableLayoutPanel37.TabIndex = 1;
            // 
            // lbl_INSP
            // 
            this.lbl_INSP.AutoSize = true;
            this.lbl_INSP.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_INSP.Location = new System.Drawing.Point(3, 39);
            this.lbl_INSP.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_INSP.Name = "lbl_INSP";
            this.lbl_INSP.Size = new System.Drawing.Size(50, 15);
            this.lbl_INSP.TabIndex = 3;
            this.lbl_INSP.Text = "INSP :  ";
            // 
            // tbox_MCR
            // 
            this.tbox_MCR.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_MCR.Location = new System.Drawing.Point(205, 3);
            this.tbox_MCR.Name = "tbox_MCR";
            this.tbox_MCR.Size = new System.Drawing.Size(196, 22);
            this.tbox_MCR.TabIndex = 0;
            // 
            // lbl_MCR
            // 
            this.lbl_MCR.AutoSize = true;
            this.lbl_MCR.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_MCR.Location = new System.Drawing.Point(3, 6);
            this.lbl_MCR.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_MCR.Name = "lbl_MCR";
            this.lbl_MCR.Size = new System.Drawing.Size(46, 15);
            this.lbl_MCR.TabIndex = 2;
            this.lbl_MCR.Text = "MCR : ";
            // 
            // tbox_INSP
            // 
            this.tbox_INSP.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_INSP.Location = new System.Drawing.Point(205, 36);
            this.tbox_INSP.Name = "tbox_INSP";
            this.tbox_INSP.Size = new System.Drawing.Size(196, 22);
            this.tbox_INSP.TabIndex = 1;
            // 
            // gbox_Timeout
            // 
            this.gbox_Timeout.Controls.Add(this.tableLayoutPanel36);
            this.gbox_Timeout.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_Timeout.ForeColor = System.Drawing.Color.White;
            this.gbox_Timeout.Location = new System.Drawing.Point(3, 132);
            this.gbox_Timeout.Name = "gbox_Timeout";
            this.gbox_Timeout.Size = new System.Drawing.Size(416, 263);
            this.gbox_Timeout.TabIndex = 55;
            this.gbox_Timeout.TabStop = false;
            this.gbox_Timeout.Text = "     타임아웃    ";
            // 
            // tableLayoutPanel36
            // 
            this.tableLayoutPanel36.ColumnCount = 2;
            this.tableLayoutPanel36.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel36.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel36.Controls.Add(this.lbl_MutingTimeout, 0, 6);
            this.tableLayoutPanel36.Controls.Add(this.tbox_VacuumTimeout, 1, 0);
            this.tableLayoutPanel36.Controls.Add(this.lbl_VacuumTimeout, 0, 0);
            this.tableLayoutPanel36.Controls.Add(this.tbox_CylinderTimeout, 1, 1);
            this.tableLayoutPanel36.Controls.Add(this.tbox_PreAlignTimeout, 1, 2);
            this.tableLayoutPanel36.Controls.Add(this.tbox_BreakingAlignTimeout, 1, 3);
            this.tableLayoutPanel36.Controls.Add(this.tbox_InspectiongTimeout, 1, 4);
            this.tableLayoutPanel36.Controls.Add(this.tbox_SequenceMoveTimeout, 1, 5);
            this.tableLayoutPanel36.Controls.Add(this.tbox_MutingTimeout, 1, 6);
            this.tableLayoutPanel36.Controls.Add(this.lbl_CylinderTimeout, 0, 1);
            this.tableLayoutPanel36.Controls.Add(this.lbl_PreAlignTimeout, 0, 2);
            this.tableLayoutPanel36.Controls.Add(this.lbl_BreakingAlignTimeout, 0, 3);
            this.tableLayoutPanel36.Controls.Add(this.lbl_InspectionTimeout, 0, 4);
            this.tableLayoutPanel36.Controls.Add(this.lbl_SequenceMoveTimeout, 0, 5);
            this.tableLayoutPanel36.Location = new System.Drawing.Point(6, 22);
            this.tableLayoutPanel36.Name = "tableLayoutPanel36";
            this.tableLayoutPanel36.RowCount = 7;
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel36.Size = new System.Drawing.Size(404, 235);
            this.tableLayoutPanel36.TabIndex = 1;
            // 
            // lbl_MutingTimeout
            // 
            this.lbl_MutingTimeout.AutoSize = true;
            this.lbl_MutingTimeout.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_MutingTimeout.Location = new System.Drawing.Point(3, 204);
            this.lbl_MutingTimeout.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_MutingTimeout.Name = "lbl_MutingTimeout";
            this.lbl_MutingTimeout.Size = new System.Drawing.Size(125, 15);
            this.lbl_MutingTimeout.TabIndex = 22;
            this.lbl_MutingTimeout.Text = "Muting Timeout[s] : ";
            // 
            // tbox_VacuumTimeout
            // 
            this.tbox_VacuumTimeout.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_VacuumTimeout.Location = new System.Drawing.Point(205, 6);
            this.tbox_VacuumTimeout.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tbox_VacuumTimeout.Name = "tbox_VacuumTimeout";
            this.tbox_VacuumTimeout.Size = new System.Drawing.Size(196, 22);
            this.tbox_VacuumTimeout.TabIndex = 0;
            // 
            // lbl_VacuumTimeout
            // 
            this.lbl_VacuumTimeout.AutoSize = true;
            this.lbl_VacuumTimeout.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_VacuumTimeout.Location = new System.Drawing.Point(3, 6);
            this.lbl_VacuumTimeout.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_VacuumTimeout.Name = "lbl_VacuumTimeout";
            this.lbl_VacuumTimeout.Size = new System.Drawing.Size(129, 15);
            this.lbl_VacuumTimeout.TabIndex = 2;
            this.lbl_VacuumTimeout.Text = "Vacuum Timeout[s] : ";
            // 
            // tbox_CylinderTimeout
            // 
            this.tbox_CylinderTimeout.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_CylinderTimeout.Location = new System.Drawing.Point(205, 39);
            this.tbox_CylinderTimeout.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tbox_CylinderTimeout.Name = "tbox_CylinderTimeout";
            this.tbox_CylinderTimeout.Size = new System.Drawing.Size(196, 22);
            this.tbox_CylinderTimeout.TabIndex = 1;
            // 
            // tbox_PreAlignTimeout
            // 
            this.tbox_PreAlignTimeout.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_PreAlignTimeout.Location = new System.Drawing.Point(205, 72);
            this.tbox_PreAlignTimeout.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tbox_PreAlignTimeout.Name = "tbox_PreAlignTimeout";
            this.tbox_PreAlignTimeout.Size = new System.Drawing.Size(196, 22);
            this.tbox_PreAlignTimeout.TabIndex = 2;
            // 
            // tbox_BreakingAlignTimeout
            // 
            this.tbox_BreakingAlignTimeout.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_BreakingAlignTimeout.Location = new System.Drawing.Point(205, 105);
            this.tbox_BreakingAlignTimeout.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tbox_BreakingAlignTimeout.Name = "tbox_BreakingAlignTimeout";
            this.tbox_BreakingAlignTimeout.Size = new System.Drawing.Size(196, 22);
            this.tbox_BreakingAlignTimeout.TabIndex = 13;
            // 
            // tbox_InspectiongTimeout
            // 
            this.tbox_InspectiongTimeout.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_InspectiongTimeout.Location = new System.Drawing.Point(205, 138);
            this.tbox_InspectiongTimeout.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tbox_InspectiongTimeout.Name = "tbox_InspectiongTimeout";
            this.tbox_InspectiongTimeout.Size = new System.Drawing.Size(196, 22);
            this.tbox_InspectiongTimeout.TabIndex = 14;
            // 
            // tbox_SequenceMoveTimeout
            // 
            this.tbox_SequenceMoveTimeout.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_SequenceMoveTimeout.Location = new System.Drawing.Point(205, 171);
            this.tbox_SequenceMoveTimeout.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tbox_SequenceMoveTimeout.Name = "tbox_SequenceMoveTimeout";
            this.tbox_SequenceMoveTimeout.Size = new System.Drawing.Size(196, 22);
            this.tbox_SequenceMoveTimeout.TabIndex = 15;
            // 
            // tbox_MutingTimeout
            // 
            this.tbox_MutingTimeout.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_MutingTimeout.Location = new System.Drawing.Point(205, 204);
            this.tbox_MutingTimeout.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tbox_MutingTimeout.Name = "tbox_MutingTimeout";
            this.tbox_MutingTimeout.Size = new System.Drawing.Size(196, 22);
            this.tbox_MutingTimeout.TabIndex = 16;
            // 
            // lbl_CylinderTimeout
            // 
            this.lbl_CylinderTimeout.AutoSize = true;
            this.lbl_CylinderTimeout.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_CylinderTimeout.Location = new System.Drawing.Point(3, 39);
            this.lbl_CylinderTimeout.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_CylinderTimeout.Name = "lbl_CylinderTimeout";
            this.lbl_CylinderTimeout.Size = new System.Drawing.Size(130, 15);
            this.lbl_CylinderTimeout.TabIndex = 17;
            this.lbl_CylinderTimeout.Text = "Cylinder Timeout[s] : ";
            // 
            // lbl_PreAlignTimeout
            // 
            this.lbl_PreAlignTimeout.AutoSize = true;
            this.lbl_PreAlignTimeout.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_PreAlignTimeout.Location = new System.Drawing.Point(3, 72);
            this.lbl_PreAlignTimeout.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_PreAlignTimeout.Name = "lbl_PreAlignTimeout";
            this.lbl_PreAlignTimeout.Size = new System.Drawing.Size(131, 15);
            this.lbl_PreAlignTimeout.TabIndex = 18;
            this.lbl_PreAlignTimeout.Text = "PreAlign Timeout[s] : ";
            // 
            // lbl_BreakingAlignTimeout
            // 
            this.lbl_BreakingAlignTimeout.AutoSize = true;
            this.lbl_BreakingAlignTimeout.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_BreakingAlignTimeout.Location = new System.Drawing.Point(3, 105);
            this.lbl_BreakingAlignTimeout.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_BreakingAlignTimeout.Name = "lbl_BreakingAlignTimeout";
            this.lbl_BreakingAlignTimeout.Size = new System.Drawing.Size(164, 15);
            this.lbl_BreakingAlignTimeout.TabIndex = 19;
            this.lbl_BreakingAlignTimeout.Text = "BreakingAilgn Timeout[s] : ";
            // 
            // lbl_InspectionTimeout
            // 
            this.lbl_InspectionTimeout.AutoSize = true;
            this.lbl_InspectionTimeout.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_InspectionTimeout.Location = new System.Drawing.Point(3, 138);
            this.lbl_InspectionTimeout.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_InspectionTimeout.Name = "lbl_InspectionTimeout";
            this.lbl_InspectionTimeout.Size = new System.Drawing.Size(143, 15);
            this.lbl_InspectionTimeout.TabIndex = 20;
            this.lbl_InspectionTimeout.Text = "Inspection Timeout[s] : ";
            // 
            // lbl_SequenceMoveTimeout
            // 
            this.lbl_SequenceMoveTimeout.AutoSize = true;
            this.lbl_SequenceMoveTimeout.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_SequenceMoveTimeout.Location = new System.Drawing.Point(3, 171);
            this.lbl_SequenceMoveTimeout.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_SequenceMoveTimeout.Name = "lbl_SequenceMoveTimeout";
            this.lbl_SequenceMoveTimeout.Size = new System.Drawing.Size(175, 15);
            this.lbl_SequenceMoveTimeout.TabIndex = 21;
            this.lbl_SequenceMoveTimeout.Text = "Sequence Move Timeout[s] : ";
            // 
            // gbox_Setting_DistributionMode
            // 
            this.gbox_Setting_DistributionMode.Controls.Add(this.tableLayoutPanel35);
            this.gbox_Setting_DistributionMode.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_Setting_DistributionMode.ForeColor = System.Drawing.Color.White;
            this.gbox_Setting_DistributionMode.Location = new System.Drawing.Point(3, 3);
            this.gbox_Setting_DistributionMode.Name = "gbox_Setting_DistributionMode";
            this.gbox_Setting_DistributionMode.Size = new System.Drawing.Size(416, 123);
            this.gbox_Setting_DistributionMode.TabIndex = 54;
            this.gbox_Setting_DistributionMode.TabStop = false;
            this.gbox_Setting_DistributionMode.Text = "     물류 모드    ";
            // 
            // tableLayoutPanel35
            // 
            this.tableLayoutPanel35.ColumnCount = 2;
            this.tableLayoutPanel35.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel35.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel35.Controls.Add(this.lbl_BreakingAlignSpeed, 0, 2);
            this.tableLayoutPanel35.Controls.Add(this.lbl_FineAlignSpeed, 0, 1);
            this.tableLayoutPanel35.Controls.Add(this.tbox_PreAlignSpeed, 1, 0);
            this.tableLayoutPanel35.Controls.Add(this.lbl_PreAlignSpeed, 0, 0);
            this.tableLayoutPanel35.Controls.Add(this.tbox_FineAlignSpeed, 1, 1);
            this.tableLayoutPanel35.Controls.Add(this.tbox_BreakingAlignSpeed, 1, 2);
            this.tableLayoutPanel35.Location = new System.Drawing.Point(6, 22);
            this.tableLayoutPanel35.Name = "tableLayoutPanel35";
            this.tableLayoutPanel35.RowCount = 3;
            this.tableLayoutPanel35.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel35.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel35.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel35.Size = new System.Drawing.Size(404, 95);
            this.tableLayoutPanel35.TabIndex = 1;
            // 
            // lbl_BreakingAlignSpeed
            // 
            this.lbl_BreakingAlignSpeed.AutoSize = true;
            this.lbl_BreakingAlignSpeed.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_BreakingAlignSpeed.Location = new System.Drawing.Point(3, 68);
            this.lbl_BreakingAlignSpeed.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_BreakingAlignSpeed.Name = "lbl_BreakingAlignSpeed";
            this.lbl_BreakingAlignSpeed.Size = new System.Drawing.Size(194, 15);
            this.lbl_BreakingAlignSpeed.TabIndex = 4;
            this.lbl_BreakingAlignSpeed.Text = "BreakingAlign Speed[mm/sec] : ";
            // 
            // lbl_FineAlignSpeed
            // 
            this.lbl_FineAlignSpeed.AutoSize = true;
            this.lbl_FineAlignSpeed.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_FineAlignSpeed.Location = new System.Drawing.Point(3, 37);
            this.lbl_FineAlignSpeed.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_FineAlignSpeed.Name = "lbl_FineAlignSpeed";
            this.lbl_FineAlignSpeed.Size = new System.Drawing.Size(165, 15);
            this.lbl_FineAlignSpeed.TabIndex = 3;
            this.lbl_FineAlignSpeed.Text = "FineAlign Speed[mm/sec] : ";
            // 
            // tbox_PreAlignSpeed
            // 
            this.tbox_PreAlignSpeed.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_PreAlignSpeed.Location = new System.Drawing.Point(205, 3);
            this.tbox_PreAlignSpeed.Name = "tbox_PreAlignSpeed";
            this.tbox_PreAlignSpeed.Size = new System.Drawing.Size(196, 22);
            this.tbox_PreAlignSpeed.TabIndex = 0;
            // 
            // lbl_PreAlignSpeed
            // 
            this.lbl_PreAlignSpeed.AutoSize = true;
            this.lbl_PreAlignSpeed.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_PreAlignSpeed.Location = new System.Drawing.Point(3, 6);
            this.lbl_PreAlignSpeed.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_PreAlignSpeed.Name = "lbl_PreAlignSpeed";
            this.lbl_PreAlignSpeed.Size = new System.Drawing.Size(161, 15);
            this.lbl_PreAlignSpeed.TabIndex = 2;
            this.lbl_PreAlignSpeed.Text = "PreAlign Speed[mm/sec] : ";
            // 
            // tbox_FineAlignSpeed
            // 
            this.tbox_FineAlignSpeed.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_FineAlignSpeed.Location = new System.Drawing.Point(205, 34);
            this.tbox_FineAlignSpeed.Name = "tbox_FineAlignSpeed";
            this.tbox_FineAlignSpeed.Size = new System.Drawing.Size(196, 22);
            this.tbox_FineAlignSpeed.TabIndex = 1;
            // 
            // tbox_BreakingAlignSpeed
            // 
            this.tbox_BreakingAlignSpeed.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_BreakingAlignSpeed.Location = new System.Drawing.Point(205, 65);
            this.tbox_BreakingAlignSpeed.Name = "tbox_BreakingAlignSpeed";
            this.tbox_BreakingAlignSpeed.Size = new System.Drawing.Size(196, 22);
            this.tbox_BreakingAlignSpeed.TabIndex = 2;
            // 
            // tableLayoutPanel41
            // 
            this.tableLayoutPanel41.ColumnCount = 1;
            this.tableLayoutPanel41.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel41.Controls.Add(this.gbox_LDS, 0, 6);
            this.tableLayoutPanel41.Controls.Add(this.gbox_TemperatureAlarm, 0, 0);
            this.tableLayoutPanel41.Controls.Add(this.gbox_MeasureCycle, 0, 1);
            this.tableLayoutPanel41.Controls.Add(this.gbox_BlowingCheck, 0, 2);
            this.tableLayoutPanel41.Controls.Add(this.gbox_RetryCount, 0, 3);
            this.tableLayoutPanel41.Controls.Add(this.gbox_LaserPD7Error, 0, 4);
            this.tableLayoutPanel41.Controls.Add(this.gbox_LaserPowerMeasure, 0, 5);
            this.tableLayoutPanel41.Location = new System.Drawing.Point(431, 3);
            this.tableLayoutPanel41.Name = "tableLayoutPanel41";
            this.tableLayoutPanel41.RowCount = 7;
            this.tableLayoutPanel41.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel41.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel41.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel41.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 61F));
            this.tableLayoutPanel41.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 98F));
            this.tableLayoutPanel41.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 267F));
            this.tableLayoutPanel41.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 106F));
            this.tableLayoutPanel41.Size = new System.Drawing.Size(422, 756);
            this.tableLayoutPanel41.TabIndex = 4;
            // 
            // gbox_LDS
            // 
            this.gbox_LDS.Controls.Add(this.tableLayoutPanel48);
            this.gbox_LDS.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_LDS.ForeColor = System.Drawing.Color.White;
            this.gbox_LDS.Location = new System.Drawing.Point(3, 613);
            this.gbox_LDS.Name = "gbox_LDS";
            this.gbox_LDS.Size = new System.Drawing.Size(416, 140);
            this.gbox_LDS.TabIndex = 64;
            this.gbox_LDS.TabStop = false;
            this.gbox_LDS.Text = "     LDS    ";
            // 
            // tableLayoutPanel48
            // 
            this.tableLayoutPanel48.ColumnCount = 2;
            this.tableLayoutPanel48.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel48.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel48.Controls.Add(this.lbl_ErrorRange, 0, 3);
            this.tableLayoutPanel48.Controls.Add(this.lbl_WaitingTime, 0, 1);
            this.tableLayoutPanel48.Controls.Add(this.tbox_LDS_MeasureCycle, 1, 0);
            this.tableLayoutPanel48.Controls.Add(this.lbl_LDS_MeasureCycle, 0, 0);
            this.tableLayoutPanel48.Controls.Add(this.tbox_WaitingTime, 1, 1);
            this.tableLayoutPanel48.Controls.Add(this.lbl_LDS_MeasureTime, 0, 2);
            this.tableLayoutPanel48.Controls.Add(this.tbox_LDS_MeasureTime, 1, 2);
            this.tableLayoutPanel48.Controls.Add(this.tbox_ErrorRange, 1, 3);
            this.tableLayoutPanel48.Location = new System.Drawing.Point(6, 22);
            this.tableLayoutPanel48.Name = "tableLayoutPanel48";
            this.tableLayoutPanel48.RowCount = 4;
            this.tableLayoutPanel48.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel48.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel48.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel48.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel48.Size = new System.Drawing.Size(404, 112);
            this.tableLayoutPanel48.TabIndex = 1;
            // 
            // lbl_ErrorRange
            // 
            this.lbl_ErrorRange.AutoSize = true;
            this.lbl_ErrorRange.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_ErrorRange.Location = new System.Drawing.Point(3, 90);
            this.lbl_ErrorRange.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_ErrorRange.Name = "lbl_ErrorRange";
            this.lbl_ErrorRange.Size = new System.Drawing.Size(100, 15);
            this.lbl_ErrorRange.TabIndex = 5;
            this.lbl_ErrorRange.Text = "오차 범위[mm] : ";
            // 
            // lbl_WaitingTime
            // 
            this.lbl_WaitingTime.AutoSize = true;
            this.lbl_WaitingTime.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_WaitingTime.Location = new System.Drawing.Point(3, 34);
            this.lbl_WaitingTime.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_WaitingTime.Name = "lbl_WaitingTime";
            this.lbl_WaitingTime.Size = new System.Drawing.Size(113, 15);
            this.lbl_WaitingTime.TabIndex = 3;
            this.lbl_WaitingTime.Text = "Waiting Time[s] :  ";
            // 
            // tbox_LDS_MeasureCycle
            // 
            this.tbox_LDS_MeasureCycle.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_LDS_MeasureCycle.Location = new System.Drawing.Point(205, 3);
            this.tbox_LDS_MeasureCycle.Name = "tbox_LDS_MeasureCycle";
            this.tbox_LDS_MeasureCycle.Size = new System.Drawing.Size(196, 22);
            this.tbox_LDS_MeasureCycle.TabIndex = 0;
            // 
            // lbl_LDS_MeasureCycle
            // 
            this.lbl_LDS_MeasureCycle.AutoSize = true;
            this.lbl_LDS_MeasureCycle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_LDS_MeasureCycle.Location = new System.Drawing.Point(3, 6);
            this.lbl_LDS_MeasureCycle.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_LDS_MeasureCycle.Name = "lbl_LDS_MeasureCycle";
            this.lbl_LDS_MeasureCycle.Size = new System.Drawing.Size(85, 15);
            this.lbl_LDS_MeasureCycle.TabIndex = 2;
            this.lbl_LDS_MeasureCycle.Text = "측정 주기[h] : ";
            // 
            // tbox_WaitingTime
            // 
            this.tbox_WaitingTime.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_WaitingTime.Location = new System.Drawing.Point(205, 31);
            this.tbox_WaitingTime.Name = "tbox_WaitingTime";
            this.tbox_WaitingTime.Size = new System.Drawing.Size(196, 22);
            this.tbox_WaitingTime.TabIndex = 1;
            // 
            // lbl_LDS_MeasureTime
            // 
            this.lbl_LDS_MeasureTime.AutoSize = true;
            this.lbl_LDS_MeasureTime.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_LDS_MeasureTime.Location = new System.Drawing.Point(3, 62);
            this.lbl_LDS_MeasureTime.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_LDS_MeasureTime.Name = "lbl_LDS_MeasureTime";
            this.lbl_LDS_MeasureTime.Size = new System.Drawing.Size(88, 15);
            this.lbl_LDS_MeasureTime.TabIndex = 4;
            this.lbl_LDS_MeasureTime.Text = "측정 시간[s] :  ";
            // 
            // tbox_LDS_MeasureTime
            // 
            this.tbox_LDS_MeasureTime.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_LDS_MeasureTime.Location = new System.Drawing.Point(205, 59);
            this.tbox_LDS_MeasureTime.Name = "tbox_LDS_MeasureTime";
            this.tbox_LDS_MeasureTime.Size = new System.Drawing.Size(196, 22);
            this.tbox_LDS_MeasureTime.TabIndex = 6;
            // 
            // tbox_ErrorRange
            // 
            this.tbox_ErrorRange.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_ErrorRange.Location = new System.Drawing.Point(205, 87);
            this.tbox_ErrorRange.Name = "tbox_ErrorRange";
            this.tbox_ErrorRange.Size = new System.Drawing.Size(196, 22);
            this.tbox_ErrorRange.TabIndex = 7;
            // 
            // gbox_TemperatureAlarm
            // 
            this.gbox_TemperatureAlarm.Controls.Add(this.tableLayoutPanel42);
            this.gbox_TemperatureAlarm.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_TemperatureAlarm.ForeColor = System.Drawing.Color.White;
            this.gbox_TemperatureAlarm.Location = new System.Drawing.Point(3, 3);
            this.gbox_TemperatureAlarm.Name = "gbox_TemperatureAlarm";
            this.gbox_TemperatureAlarm.Size = new System.Drawing.Size(416, 54);
            this.gbox_TemperatureAlarm.TabIndex = 58;
            this.gbox_TemperatureAlarm.TabStop = false;
            this.gbox_TemperatureAlarm.Text = "     온도 알림    ";
            // 
            // tableLayoutPanel42
            // 
            this.tableLayoutPanel42.ColumnCount = 3;
            this.tableLayoutPanel42.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel42.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel42.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel42.Controls.Add(this.tbox_TemperatureLightAlarm, 0, 0);
            this.tableLayoutPanel42.Controls.Add(this.tbox_TemperatureCriticalAlarm, 1, 0);
            this.tableLayoutPanel42.Controls.Add(this.lbl_TemperatureAlarm, 0, 0);
            this.tableLayoutPanel42.Location = new System.Drawing.Point(6, 22);
            this.tableLayoutPanel42.Name = "tableLayoutPanel42";
            this.tableLayoutPanel42.RowCount = 1;
            this.tableLayoutPanel42.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel42.Size = new System.Drawing.Size(404, 30);
            this.tableLayoutPanel42.TabIndex = 1;
            // 
            // tbox_TemperatureLightAlarm
            // 
            this.tbox_TemperatureLightAlarm.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_TemperatureLightAlarm.Location = new System.Drawing.Point(205, 3);
            this.tbox_TemperatureLightAlarm.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.tbox_TemperatureLightAlarm.Name = "tbox_TemperatureLightAlarm";
            this.tbox_TemperatureLightAlarm.Size = new System.Drawing.Size(95, 22);
            this.tbox_TemperatureLightAlarm.TabIndex = 3;
            // 
            // tbox_TemperatureCriticalAlarm
            // 
            this.tbox_TemperatureCriticalAlarm.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_TemperatureCriticalAlarm.Location = new System.Drawing.Point(309, 3);
            this.tbox_TemperatureCriticalAlarm.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
            this.tbox_TemperatureCriticalAlarm.Name = "tbox_TemperatureCriticalAlarm";
            this.tbox_TemperatureCriticalAlarm.Size = new System.Drawing.Size(92, 22);
            this.tbox_TemperatureCriticalAlarm.TabIndex = 0;
            // 
            // lbl_TemperatureAlarm
            // 
            this.lbl_TemperatureAlarm.AutoSize = true;
            this.lbl_TemperatureAlarm.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_TemperatureAlarm.Location = new System.Drawing.Point(3, 6);
            this.lbl_TemperatureAlarm.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_TemperatureAlarm.Name = "lbl_TemperatureAlarm";
            this.lbl_TemperatureAlarm.Size = new System.Drawing.Size(114, 15);
            this.lbl_TemperatureAlarm.TabIndex = 2;
            this.lbl_TemperatureAlarm.Text = "경알람 / 중알람[ºC]";
            // 
            // gbox_MeasureCycle
            // 
            this.gbox_MeasureCycle.Controls.Add(this.tableLayoutPanel43);
            this.gbox_MeasureCycle.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_MeasureCycle.ForeColor = System.Drawing.Color.White;
            this.gbox_MeasureCycle.Location = new System.Drawing.Point(3, 65);
            this.gbox_MeasureCycle.Name = "gbox_MeasureCycle";
            this.gbox_MeasureCycle.Size = new System.Drawing.Size(416, 56);
            this.gbox_MeasureCycle.TabIndex = 59;
            this.gbox_MeasureCycle.TabStop = false;
            this.gbox_MeasureCycle.Text = "     측정주기    ";
            // 
            // tableLayoutPanel43
            // 
            this.tableLayoutPanel43.ColumnCount = 2;
            this.tableLayoutPanel43.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel43.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel43.Controls.Add(this.tbox_CutLineMeasure, 1, 0);
            this.tableLayoutPanel43.Controls.Add(this.lbl_CutLineMeasure, 0, 0);
            this.tableLayoutPanel43.Location = new System.Drawing.Point(6, 22);
            this.tableLayoutPanel43.Name = "tableLayoutPanel43";
            this.tableLayoutPanel43.RowCount = 1;
            this.tableLayoutPanel43.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel43.Size = new System.Drawing.Size(404, 30);
            this.tableLayoutPanel43.TabIndex = 1;
            // 
            // tbox_CutLineMeasure
            // 
            this.tbox_CutLineMeasure.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_CutLineMeasure.Location = new System.Drawing.Point(205, 3);
            this.tbox_CutLineMeasure.Name = "tbox_CutLineMeasure";
            this.tbox_CutLineMeasure.Size = new System.Drawing.Size(196, 22);
            this.tbox_CutLineMeasure.TabIndex = 0;
            // 
            // lbl_CutLineMeasure
            // 
            this.lbl_CutLineMeasure.AutoSize = true;
            this.lbl_CutLineMeasure.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_CutLineMeasure.Location = new System.Drawing.Point(3, 6);
            this.lbl_CutLineMeasure.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_CutLineMeasure.Name = "lbl_CutLineMeasure";
            this.lbl_CutLineMeasure.Size = new System.Drawing.Size(75, 15);
            this.lbl_CutLineMeasure.TabIndex = 2;
            this.lbl_CutLineMeasure.Text = "컷 라인 주기";
            // 
            // gbox_BlowingCheck
            // 
            this.gbox_BlowingCheck.Controls.Add(this.tableLayoutPanel44);
            this.gbox_BlowingCheck.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BlowingCheck.ForeColor = System.Drawing.Color.White;
            this.gbox_BlowingCheck.Location = new System.Drawing.Point(3, 127);
            this.gbox_BlowingCheck.Name = "gbox_BlowingCheck";
            this.gbox_BlowingCheck.Size = new System.Drawing.Size(416, 54);
            this.gbox_BlowingCheck.TabIndex = 60;
            this.gbox_BlowingCheck.TabStop = false;
            this.gbox_BlowingCheck.Text = "     Blowing 체크    ";
            // 
            // tableLayoutPanel44
            // 
            this.tableLayoutPanel44.ColumnCount = 2;
            this.tableLayoutPanel44.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel44.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel44.Controls.Add(this.tbox_BlowingCheck, 1, 0);
            this.tableLayoutPanel44.Controls.Add(this.lbl_BlowingCheck, 0, 0);
            this.tableLayoutPanel44.Location = new System.Drawing.Point(6, 22);
            this.tableLayoutPanel44.Name = "tableLayoutPanel44";
            this.tableLayoutPanel44.RowCount = 1;
            this.tableLayoutPanel44.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel44.Size = new System.Drawing.Size(404, 30);
            this.tableLayoutPanel44.TabIndex = 1;
            // 
            // tbox_BlowingCheck
            // 
            this.tbox_BlowingCheck.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_BlowingCheck.Location = new System.Drawing.Point(205, 3);
            this.tbox_BlowingCheck.Name = "tbox_BlowingCheck";
            this.tbox_BlowingCheck.Size = new System.Drawing.Size(196, 22);
            this.tbox_BlowingCheck.TabIndex = 0;
            // 
            // lbl_BlowingCheck
            // 
            this.lbl_BlowingCheck.AutoSize = true;
            this.lbl_BlowingCheck.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_BlowingCheck.Location = new System.Drawing.Point(3, 6);
            this.lbl_BlowingCheck.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_BlowingCheck.Name = "lbl_BlowingCheck";
            this.lbl_BlowingCheck.Size = new System.Drawing.Size(92, 15);
            this.lbl_BlowingCheck.TabIndex = 2;
            this.lbl_BlowingCheck.Text = "Blowing 체크 : ";
            // 
            // gbox_RetryCount
            // 
            this.gbox_RetryCount.Controls.Add(this.tableLayoutPanel45);
            this.gbox_RetryCount.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_RetryCount.ForeColor = System.Drawing.Color.White;
            this.gbox_RetryCount.Location = new System.Drawing.Point(3, 187);
            this.gbox_RetryCount.Name = "gbox_RetryCount";
            this.gbox_RetryCount.Size = new System.Drawing.Size(416, 55);
            this.gbox_RetryCount.TabIndex = 61;
            this.gbox_RetryCount.TabStop = false;
            this.gbox_RetryCount.Text = "     Retry Count    ";
            // 
            // tableLayoutPanel45
            // 
            this.tableLayoutPanel45.ColumnCount = 2;
            this.tableLayoutPanel45.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel45.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel45.Controls.Add(this.tbox_RetryCount, 1, 0);
            this.tableLayoutPanel45.Controls.Add(this.lbl_RetryCount, 0, 0);
            this.tableLayoutPanel45.Location = new System.Drawing.Point(6, 22);
            this.tableLayoutPanel45.Name = "tableLayoutPanel45";
            this.tableLayoutPanel45.RowCount = 1;
            this.tableLayoutPanel45.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel45.Size = new System.Drawing.Size(404, 30);
            this.tableLayoutPanel45.TabIndex = 1;
            // 
            // tbox_RetryCount
            // 
            this.tbox_RetryCount.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_RetryCount.Location = new System.Drawing.Point(205, 3);
            this.tbox_RetryCount.Name = "tbox_RetryCount";
            this.tbox_RetryCount.Size = new System.Drawing.Size(196, 22);
            this.tbox_RetryCount.TabIndex = 0;
            // 
            // lbl_RetryCount
            // 
            this.lbl_RetryCount.AutoSize = true;
            this.lbl_RetryCount.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_RetryCount.Location = new System.Drawing.Point(3, 6);
            this.lbl_RetryCount.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_RetryCount.Name = "lbl_RetryCount";
            this.lbl_RetryCount.Size = new System.Drawing.Size(87, 15);
            this.lbl_RetryCount.TabIndex = 2;
            this.lbl_RetryCount.Text = "Retry Count : ";
            // 
            // gbox_LaserPD7Error
            // 
            this.gbox_LaserPD7Error.Controls.Add(this.tableLayoutPanel46);
            this.gbox_LaserPD7Error.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_LaserPD7Error.ForeColor = System.Drawing.Color.White;
            this.gbox_LaserPD7Error.Location = new System.Drawing.Point(3, 248);
            this.gbox_LaserPD7Error.Name = "gbox_LaserPD7Error";
            this.gbox_LaserPD7Error.Size = new System.Drawing.Size(416, 92);
            this.gbox_LaserPD7Error.TabIndex = 62;
            this.gbox_LaserPD7Error.TabStop = false;
            this.gbox_LaserPD7Error.Text = "     레이져 PD7 에러    ";
            // 
            // tableLayoutPanel46
            // 
            this.tableLayoutPanel46.ColumnCount = 2;
            this.tableLayoutPanel46.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel46.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel46.Controls.Add(this.lbl_LaserPD7ErrorRange, 0, 1);
            this.tableLayoutPanel46.Controls.Add(this.tbox_LaserPD7Power, 1, 0);
            this.tableLayoutPanel46.Controls.Add(this.lbl_LaserPD7Power, 0, 0);
            this.tableLayoutPanel46.Controls.Add(this.tbox_LaserPD7ErrorRange, 1, 1);
            this.tableLayoutPanel46.Location = new System.Drawing.Point(6, 22);
            this.tableLayoutPanel46.Name = "tableLayoutPanel46";
            this.tableLayoutPanel46.RowCount = 2;
            this.tableLayoutPanel46.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel46.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel46.Size = new System.Drawing.Size(404, 67);
            this.tableLayoutPanel46.TabIndex = 1;
            // 
            // lbl_LaserPD7ErrorRange
            // 
            this.lbl_LaserPD7ErrorRange.AutoSize = true;
            this.lbl_LaserPD7ErrorRange.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_LaserPD7ErrorRange.Location = new System.Drawing.Point(3, 39);
            this.lbl_LaserPD7ErrorRange.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_LaserPD7ErrorRange.Name = "lbl_LaserPD7ErrorRange";
            this.lbl_LaserPD7ErrorRange.Size = new System.Drawing.Size(160, 15);
            this.lbl_LaserPD7ErrorRange.TabIndex = 3;
            this.lbl_LaserPD7ErrorRange.Text = "레이져 PD7 오차 범위[%] :  ";
            // 
            // tbox_LaserPD7Power
            // 
            this.tbox_LaserPD7Power.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_LaserPD7Power.Location = new System.Drawing.Point(205, 3);
            this.tbox_LaserPD7Power.Name = "tbox_LaserPD7Power";
            this.tbox_LaserPD7Power.Size = new System.Drawing.Size(196, 22);
            this.tbox_LaserPD7Power.TabIndex = 0;
            // 
            // lbl_LaserPD7Power
            // 
            this.lbl_LaserPD7Power.AutoSize = true;
            this.lbl_LaserPD7Power.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_LaserPD7Power.Location = new System.Drawing.Point(3, 6);
            this.lbl_LaserPD7Power.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_LaserPD7Power.Name = "lbl_LaserPD7Power";
            this.lbl_LaserPD7Power.Size = new System.Drawing.Size(127, 15);
            this.lbl_LaserPD7Power.TabIndex = 2;
            this.lbl_LaserPD7Power.Text = "레이져 PD7 파워[w] : ";
            // 
            // tbox_LaserPD7ErrorRange
            // 
            this.tbox_LaserPD7ErrorRange.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_LaserPD7ErrorRange.Location = new System.Drawing.Point(205, 36);
            this.tbox_LaserPD7ErrorRange.Name = "tbox_LaserPD7ErrorRange";
            this.tbox_LaserPD7ErrorRange.Size = new System.Drawing.Size(196, 22);
            this.tbox_LaserPD7ErrorRange.TabIndex = 1;
            // 
            // gbox_LaserPowerMeasure
            // 
            this.gbox_LaserPowerMeasure.Controls.Add(this.tableLayoutPanel47);
            this.gbox_LaserPowerMeasure.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_LaserPowerMeasure.ForeColor = System.Drawing.Color.White;
            this.gbox_LaserPowerMeasure.Location = new System.Drawing.Point(3, 346);
            this.gbox_LaserPowerMeasure.Name = "gbox_LaserPowerMeasure";
            this.gbox_LaserPowerMeasure.Size = new System.Drawing.Size(416, 261);
            this.gbox_LaserPowerMeasure.TabIndex = 63;
            this.gbox_LaserPowerMeasure.TabStop = false;
            this.gbox_LaserPowerMeasure.Text = "     레이져 파워 측정    ";
            // 
            // tableLayoutPanel47
            // 
            this.tableLayoutPanel47.ColumnCount = 2;
            this.tableLayoutPanel47.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel47.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel47.Controls.Add(this.lbl_TargetErrorRange, 0, 6);
            this.tableLayoutPanel47.Controls.Add(this.tbox_MeasureCycle, 1, 0);
            this.tableLayoutPanel47.Controls.Add(this.lbl_LaserPowerMeasure_MeasureCycle, 0, 0);
            this.tableLayoutPanel47.Controls.Add(this.tbox_TargetPEC, 1, 1);
            this.tableLayoutPanel47.Controls.Add(this.tbox_TargetPower, 1, 2);
            this.tableLayoutPanel47.Controls.Add(this.tbox_WarmUpTime, 1, 3);
            this.tableLayoutPanel47.Controls.Add(this.tbox_MeasureTime, 1, 4);
            this.tableLayoutPanel47.Controls.Add(this.tbox_CorrectionErrorRange, 1, 5);
            this.tableLayoutPanel47.Controls.Add(this.tbox_TargetErrorRange, 1, 6);
            this.tableLayoutPanel47.Controls.Add(this.lbl_TargetPEC, 0, 1);
            this.tableLayoutPanel47.Controls.Add(this.lbl_TargetPower, 0, 2);
            this.tableLayoutPanel47.Controls.Add(this.lbl_WarmUpTime, 0, 3);
            this.tableLayoutPanel47.Controls.Add(this.lbl_MeasureTime, 0, 4);
            this.tableLayoutPanel47.Controls.Add(this.lbl_CorrectionErrorRange, 0, 5);
            this.tableLayoutPanel47.Location = new System.Drawing.Point(6, 22);
            this.tableLayoutPanel47.Name = "tableLayoutPanel47";
            this.tableLayoutPanel47.RowCount = 7;
            this.tableLayoutPanel47.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel47.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel47.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel47.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel47.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel47.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel47.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel47.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel47.Size = new System.Drawing.Size(404, 235);
            this.tableLayoutPanel47.TabIndex = 1;
            // 
            // lbl_TargetErrorRange
            // 
            this.lbl_TargetErrorRange.AutoSize = true;
            this.lbl_TargetErrorRange.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_TargetErrorRange.Location = new System.Drawing.Point(3, 204);
            this.lbl_TargetErrorRange.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_TargetErrorRange.Name = "lbl_TargetErrorRange";
            this.lbl_TargetErrorRange.Size = new System.Drawing.Size(131, 15);
            this.lbl_TargetErrorRange.TabIndex = 22;
            this.lbl_TargetErrorRange.Text = "Target 오차 범위[w] : ";
            // 
            // tbox_MeasureCycle
            // 
            this.tbox_MeasureCycle.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_MeasureCycle.Location = new System.Drawing.Point(205, 6);
            this.tbox_MeasureCycle.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tbox_MeasureCycle.Name = "tbox_MeasureCycle";
            this.tbox_MeasureCycle.Size = new System.Drawing.Size(196, 22);
            this.tbox_MeasureCycle.TabIndex = 0;
            // 
            // lbl_LaserPowerMeasure_MeasureCycle
            // 
            this.lbl_LaserPowerMeasure_MeasureCycle.AutoSize = true;
            this.lbl_LaserPowerMeasure_MeasureCycle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_LaserPowerMeasure_MeasureCycle.Location = new System.Drawing.Point(3, 6);
            this.lbl_LaserPowerMeasure_MeasureCycle.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_LaserPowerMeasure_MeasureCycle.Name = "lbl_LaserPowerMeasure_MeasureCycle";
            this.lbl_LaserPowerMeasure_MeasureCycle.Size = new System.Drawing.Size(85, 15);
            this.lbl_LaserPowerMeasure_MeasureCycle.TabIndex = 2;
            this.lbl_LaserPowerMeasure_MeasureCycle.Text = "측정 주기[h] : ";
            // 
            // tbox_TargetPEC
            // 
            this.tbox_TargetPEC.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_TargetPEC.Location = new System.Drawing.Point(205, 39);
            this.tbox_TargetPEC.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tbox_TargetPEC.Name = "tbox_TargetPEC";
            this.tbox_TargetPEC.Size = new System.Drawing.Size(196, 22);
            this.tbox_TargetPEC.TabIndex = 1;
            // 
            // tbox_TargetPower
            // 
            this.tbox_TargetPower.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_TargetPower.Location = new System.Drawing.Point(205, 72);
            this.tbox_TargetPower.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tbox_TargetPower.Name = "tbox_TargetPower";
            this.tbox_TargetPower.Size = new System.Drawing.Size(196, 22);
            this.tbox_TargetPower.TabIndex = 2;
            // 
            // tbox_WarmUpTime
            // 
            this.tbox_WarmUpTime.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_WarmUpTime.Location = new System.Drawing.Point(205, 105);
            this.tbox_WarmUpTime.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tbox_WarmUpTime.Name = "tbox_WarmUpTime";
            this.tbox_WarmUpTime.Size = new System.Drawing.Size(196, 22);
            this.tbox_WarmUpTime.TabIndex = 13;
            // 
            // tbox_MeasureTime
            // 
            this.tbox_MeasureTime.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_MeasureTime.Location = new System.Drawing.Point(205, 138);
            this.tbox_MeasureTime.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tbox_MeasureTime.Name = "tbox_MeasureTime";
            this.tbox_MeasureTime.Size = new System.Drawing.Size(196, 22);
            this.tbox_MeasureTime.TabIndex = 14;
            // 
            // tbox_CorrectionErrorRange
            // 
            this.tbox_CorrectionErrorRange.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_CorrectionErrorRange.Location = new System.Drawing.Point(205, 171);
            this.tbox_CorrectionErrorRange.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tbox_CorrectionErrorRange.Name = "tbox_CorrectionErrorRange";
            this.tbox_CorrectionErrorRange.Size = new System.Drawing.Size(196, 22);
            this.tbox_CorrectionErrorRange.TabIndex = 15;
            // 
            // tbox_TargetErrorRange
            // 
            this.tbox_TargetErrorRange.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_TargetErrorRange.Location = new System.Drawing.Point(205, 204);
            this.tbox_TargetErrorRange.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tbox_TargetErrorRange.Name = "tbox_TargetErrorRange";
            this.tbox_TargetErrorRange.Size = new System.Drawing.Size(196, 22);
            this.tbox_TargetErrorRange.TabIndex = 16;
            // 
            // lbl_TargetPEC
            // 
            this.lbl_TargetPEC.AutoSize = true;
            this.lbl_TargetPEC.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_TargetPEC.Location = new System.Drawing.Point(3, 39);
            this.lbl_TargetPEC.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_TargetPEC.Name = "lbl_TargetPEC";
            this.lbl_TargetPEC.Size = new System.Drawing.Size(101, 15);
            this.lbl_TargetPEC.TabIndex = 17;
            this.lbl_TargetPEC.Text = "Target PEC[%] : ";
            // 
            // lbl_TargetPower
            // 
            this.lbl_TargetPower.AutoSize = true;
            this.lbl_TargetPower.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_TargetPower.Location = new System.Drawing.Point(3, 72);
            this.lbl_TargetPower.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_TargetPower.Name = "lbl_TargetPower";
            this.lbl_TargetPower.Size = new System.Drawing.Size(103, 15);
            this.lbl_TargetPower.TabIndex = 18;
            this.lbl_TargetPower.Text = "Target 파워[w] : ";
            // 
            // lbl_WarmUpTime
            // 
            this.lbl_WarmUpTime.AutoSize = true;
            this.lbl_WarmUpTime.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_WarmUpTime.Location = new System.Drawing.Point(3, 105);
            this.lbl_WarmUpTime.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_WarmUpTime.Name = "lbl_WarmUpTime";
            this.lbl_WarmUpTime.Size = new System.Drawing.Size(116, 15);
            this.lbl_WarmUpTime.TabIndex = 19;
            this.lbl_WarmUpTime.Text = "WarmUp Time[s] : ";
            // 
            // lbl_MeasureTime
            // 
            this.lbl_MeasureTime.AutoSize = true;
            this.lbl_MeasureTime.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_MeasureTime.Location = new System.Drawing.Point(3, 138);
            this.lbl_MeasureTime.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_MeasureTime.Name = "lbl_MeasureTime";
            this.lbl_MeasureTime.Size = new System.Drawing.Size(84, 15);
            this.lbl_MeasureTime.TabIndex = 20;
            this.lbl_MeasureTime.Text = "측정 시간[s] : ";
            // 
            // lbl_CorrectionErrorRange
            // 
            this.lbl_CorrectionErrorRange.AutoSize = true;
            this.lbl_CorrectionErrorRange.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_CorrectionErrorRange.Location = new System.Drawing.Point(3, 171);
            this.lbl_CorrectionErrorRange.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.lbl_CorrectionErrorRange.Name = "lbl_CorrectionErrorRange";
            this.lbl_CorrectionErrorRange.Size = new System.Drawing.Size(116, 15);
            this.lbl_CorrectionErrorRange.TabIndex = 21;
            this.lbl_CorrectionErrorRange.Text = "보정 오차 범위[w] : ";
            // 
            // btn_Save
            // 
            this.btn_Save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Save.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btn_Save.ForeColor = System.Drawing.Color.White;
            this.btn_Save.Location = new System.Drawing.Point(1575, 827);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(159, 45);
            this.btn_Save.TabIndex = 48;
            this.btn_Save.Text = "저장";
            this.btn_Save.UseVisualStyleBackColor = false;
            // 
            // btn_AlarmClear
            // 
            this.btn_AlarmClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_AlarmClear.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btn_AlarmClear.ForeColor = System.Drawing.Color.White;
            this.btn_AlarmClear.Location = new System.Drawing.Point(1410, 827);
            this.btn_AlarmClear.Name = "btn_AlarmClear";
            this.btn_AlarmClear.Size = new System.Drawing.Size(159, 45);
            this.btn_AlarmClear.TabIndex = 49;
            this.btn_AlarmClear.Text = "알람 클리어";
            this.btn_AlarmClear.UseVisualStyleBackColor = false;
            // 
            // Parameter_System
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Controls.Add(this.btn_AlarmClear);
            this.Controls.Add(this.btn_Save);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Parameter_System";
            this.Size = new System.Drawing.Size(1740, 875);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.gbox_Mode_DistributionMode.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.gbox_SimulationMode.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.gbox_SkipMode.ResumeLayout(false);
            this.gbox_DoorSkip.ResumeLayout(false);
            this.tableLayoutPanel31.ResumeLayout(false);
            this.tableLayoutPanel31.PerformLayout();
            this.gbox_GrabSwitchSkip.ResumeLayout(false);
            this.tableLayoutPanel30.ResumeLayout(false);
            this.tableLayoutPanel30.PerformLayout();
            this.gbox_McDownSkip.ResumeLayout(false);
            this.tableLayoutPanel29.ResumeLayout(false);
            this.tableLayoutPanel29.PerformLayout();
            this.gbox_TeachSkip.ResumeLayout(false);
            this.tableLayoutPanel28.ResumeLayout(false);
            this.tableLayoutPanel28.PerformLayout();
            this.gbox_MutingSkip.ResumeLayout(false);
            this.tableLayoutPanel27.ResumeLayout(false);
            this.tableLayoutPanel27.PerformLayout();
            this.gbox_LaserMeasureSkip.ResumeLayout(false);
            this.tableLayoutPanel26.ResumeLayout(false);
            this.tableLayoutPanel26.PerformLayout();
            this.gbox_UnloaderInputSkip.ResumeLayout(false);
            this.tableLayoutPanel21.ResumeLayout(false);
            this.tableLayoutPanel24.ResumeLayout(false);
            this.tableLayoutPanel24.PerformLayout();
            this.tableLayoutPanel25.ResumeLayout(false);
            this.tableLayoutPanel25.PerformLayout();
            this.gbox_LoaderInputSkip.ResumeLayout(false);
            this.tableLayoutPanel20.ResumeLayout(false);
            this.tableLayoutPanel23.ResumeLayout(false);
            this.tableLayoutPanel23.PerformLayout();
            this.tableLayoutPanel22.ResumeLayout(false);
            this.tableLayoutPanel22.PerformLayout();
            this.gbox_BufferPickerSkip.ResumeLayout(false);
            this.tableLayoutPanel19.ResumeLayout(false);
            this.tableLayoutPanel19.PerformLayout();
            this.gbox_TiltMeasureSkip.ResumeLayout(false);
            this.tableLayoutPanel18.ResumeLayout(false);
            this.tableLayoutPanel18.PerformLayout();
            this.gbox_BreakingAlignSkip.ResumeLayout(false);
            this.tableLayoutPanel17.ResumeLayout(false);
            this.tableLayoutPanel17.PerformLayout();
            this.gbox_BcrSkip.ResumeLayout(false);
            this.tableLayoutPanel16.ResumeLayout(false);
            this.tableLayoutPanel16.PerformLayout();
            this.gbox_McrSkip.ResumeLayout(false);
            this.tableLayoutPanel15.ResumeLayout(false);
            this.tableLayoutPanel15.PerformLayout();
            this.gbox_InspectionSkip.ResumeLayout(false);
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel14.PerformLayout();
            this.gbox_BreakingMotionSkip.ResumeLayout(false);
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            this.gbox_LaserSkip.ResumeLayout(false);
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel12.PerformLayout();
            this.gbox_BreakingBSkip.ResumeLayout(false);
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            this.gbox_BreakingASkip.ResumeLayout(false);
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.gbox_ProcessBSkip.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.gbox_ProcessASkip.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel32.ResumeLayout(false);
            this.tableLayoutPanel32.PerformLayout();
            this.tableLayoutPanel33.ResumeLayout(false);
            this.tableLayoutPanel34.ResumeLayout(false);
            this.gbox_AutoDeleteCycle.ResumeLayout(false);
            this.tableLayoutPanel40.ResumeLayout(false);
            this.tableLayoutPanel40.PerformLayout();
            this.gbox_BreakingCount.ResumeLayout(false);
            this.tableLayoutPanel39.ResumeLayout(false);
            this.tableLayoutPanel39.PerformLayout();
            this.gbox_DoorOpenSpeed.ResumeLayout(false);
            this.tableLayoutPanel38.ResumeLayout(false);
            this.tableLayoutPanel38.PerformLayout();
            this.gbox_CountError.ResumeLayout(false);
            this.tableLayoutPanel37.ResumeLayout(false);
            this.tableLayoutPanel37.PerformLayout();
            this.gbox_Timeout.ResumeLayout(false);
            this.tableLayoutPanel36.ResumeLayout(false);
            this.tableLayoutPanel36.PerformLayout();
            this.gbox_Setting_DistributionMode.ResumeLayout(false);
            this.tableLayoutPanel35.ResumeLayout(false);
            this.tableLayoutPanel35.PerformLayout();
            this.tableLayoutPanel41.ResumeLayout(false);
            this.gbox_LDS.ResumeLayout(false);
            this.tableLayoutPanel48.ResumeLayout(false);
            this.tableLayoutPanel48.PerformLayout();
            this.gbox_TemperatureAlarm.ResumeLayout(false);
            this.tableLayoutPanel42.ResumeLayout(false);
            this.tableLayoutPanel42.PerformLayout();
            this.gbox_MeasureCycle.ResumeLayout(false);
            this.tableLayoutPanel43.ResumeLayout(false);
            this.tableLayoutPanel43.PerformLayout();
            this.gbox_BlowingCheck.ResumeLayout(false);
            this.tableLayoutPanel44.ResumeLayout(false);
            this.tableLayoutPanel44.PerformLayout();
            this.gbox_RetryCount.ResumeLayout(false);
            this.tableLayoutPanel45.ResumeLayout(false);
            this.tableLayoutPanel45.PerformLayout();
            this.gbox_LaserPD7Error.ResumeLayout(false);
            this.tableLayoutPanel46.ResumeLayout(false);
            this.tableLayoutPanel46.PerformLayout();
            this.gbox_LaserPowerMeasure.ResumeLayout(false);
            this.tableLayoutPanel47.ResumeLayout(false);
            this.tableLayoutPanel47.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label lbl_Mode;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.GroupBox gbox_SimulationMode;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TextBox tbox_SimulationMode;
        private System.Windows.Forms.GroupBox gbox_SkipMode;
        private System.Windows.Forms.GroupBox gbox_LoaderInputSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel20;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel22;
        private System.Windows.Forms.Button btn_LoaderInputSkipA_DontUse;
        private System.Windows.Forms.TextBox tbox_LoaderInputSkipA;
        private System.Windows.Forms.Button btn_LoaderInputSkipA_Use;
        private System.Windows.Forms.GroupBox gbox_BufferPickerSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel19;
        private System.Windows.Forms.Button btn_BufferPickerSkip_DontUse;
        private System.Windows.Forms.TextBox tbox_BufferPickerSkip;
        private System.Windows.Forms.Button btn_BufferPickerSkip_Use;
        private System.Windows.Forms.GroupBox gbox_TiltMeasureSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel18;
        private System.Windows.Forms.Button btn_TiltMeasureSkip_DontUse;
        private System.Windows.Forms.TextBox tbox_TiltMeasureSkip;
        private System.Windows.Forms.Button btn_TiltMeasureSkip_Use;
        private System.Windows.Forms.GroupBox gbox_BreakingAlignSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel17;
        private System.Windows.Forms.Button btn_BreakingAlignSkip_DontUse;
        private System.Windows.Forms.TextBox tbox_BreakingAlignSkip;
        private System.Windows.Forms.Button btn_BreakingAlignSkip_Use;
        private System.Windows.Forms.GroupBox gbox_BcrSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        private System.Windows.Forms.Button btn_BcrSkip_DontUse;
        private System.Windows.Forms.TextBox tbox_BcrSkip;
        private System.Windows.Forms.Button btn_BcrSkip_Use;
        private System.Windows.Forms.GroupBox gbox_McrSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private System.Windows.Forms.Button btn_McrSkip_DontUse;
        private System.Windows.Forms.TextBox tbox_McrSkip;
        private System.Windows.Forms.Button btn_McrSkip_Use;
        private System.Windows.Forms.GroupBox gbox_InspectionSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.Button btn_InspectionSkip_DontUse;
        private System.Windows.Forms.TextBox tbox_InspectionSkip;
        private System.Windows.Forms.Button btn_InspectionSkip_Use;
        private System.Windows.Forms.GroupBox gbox_BreakingMotionSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.Button btn_BreakingMotionSkip_DontUse;
        private System.Windows.Forms.TextBox tbox_BreakingMotionSkip;
        private System.Windows.Forms.Button btn_BreakingMotionSkip_Use;
        private System.Windows.Forms.GroupBox gbox_LaserSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.Button btn_LaserSkip_DontUse;
        private System.Windows.Forms.TextBox tbox_LaserSkip;
        private System.Windows.Forms.Button btn_LaserSkip_Use;
        private System.Windows.Forms.GroupBox gbox_BreakingBSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.Button btn_BreakingBSkip_DontUse;
        private System.Windows.Forms.TextBox tbox_BreakingBSkip;
        private System.Windows.Forms.Button btn_BreakingBSkip_Use;
        private System.Windows.Forms.GroupBox gbox_BreakingASkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.Button btn_BreakingASkip_DontUse;
        private System.Windows.Forms.TextBox tbox_BreakingASkip;
        private System.Windows.Forms.Button btn_BreakingASkip_Use;
        private System.Windows.Forms.GroupBox gbox_ProcessBSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Button btn_ProcessBSkip_DontUse;
        private System.Windows.Forms.TextBox tbox_ProcessBSkip;
        private System.Windows.Forms.Button btn_ProcessBSkip_Use;
        private System.Windows.Forms.GroupBox gbox_ProcessASkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Button btn_ProcessASkip_DontUse;
        private System.Windows.Forms.TextBox tbox_ProcessASkip;
        private System.Windows.Forms.Button btn_ProcessASkip_Use;
        private System.Windows.Forms.GroupBox gbox_Mode_DistributionMode;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Button btn_Mode_DistributionMode_DontUse;
        private System.Windows.Forms.TextBox tbox_Mode_DistributionMode;
        private System.Windows.Forms.Button btn_Mode_DistributionMode_Use;
        private System.Windows.Forms.Button btn_SimulationMode_DontUse;
        private System.Windows.Forms.Button btn_SimulationMode_Use;
        private System.Windows.Forms.GroupBox gbox_DoorSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel31;
        private System.Windows.Forms.Button btn_DoorSkip_DontUSe;
        private System.Windows.Forms.TextBox tbox_DoorSkip;
        private System.Windows.Forms.Button btn_DoorSkip_Use;
        private System.Windows.Forms.GroupBox gbox_GrabSwitchSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel30;
        private System.Windows.Forms.Button btn_GrabSwitchSkip_DontUse;
        private System.Windows.Forms.TextBox tbox_GrabSwitchSkip;
        private System.Windows.Forms.Button btn_GrabSwitchSkip_Use;
        private System.Windows.Forms.GroupBox gbox_McDownSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel29;
        private System.Windows.Forms.Button btn_McDownSkip_DontUse;
        private System.Windows.Forms.TextBox tbox_McDownSkip;
        private System.Windows.Forms.Button btn_McDownSkip_Use;
        private System.Windows.Forms.GroupBox gbox_TeachSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel28;
        private System.Windows.Forms.Button btn_TeachSkip_DontUse;
        private System.Windows.Forms.TextBox tbox_TeachSkip;
        private System.Windows.Forms.Button btn_TeachSkip_Use;
        private System.Windows.Forms.GroupBox gbox_MutingSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel27;
        private System.Windows.Forms.Button btn_MutingSkip_DontUse;
        private System.Windows.Forms.TextBox tbox_MutingSkip;
        private System.Windows.Forms.Button btn_MutingSkip_Use;
        private System.Windows.Forms.GroupBox gbox_LaserMeasureSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel26;
        private System.Windows.Forms.Button btn_LaserMeasureSkip_DontUse;
        private System.Windows.Forms.TextBox tbox_LaserMeasureSkip;
        private System.Windows.Forms.Button btn_LaserMeasureSkip_Use;
        private System.Windows.Forms.GroupBox gbox_UnloaderInputSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel21;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel24;
        private System.Windows.Forms.Button btn_UnloaderInputSkipB_DontUse;
        private System.Windows.Forms.TextBox tbox_UnloaderInputSkipB;
        private System.Windows.Forms.Button btn_UnloaderInputSkipB_Use;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel25;
        private System.Windows.Forms.Button btn_UnloaderInputSkipA_DontUse;
        private System.Windows.Forms.TextBox tbox_UnloaderInputSkipA;
        private System.Windows.Forms.Button btn_UnloaderInputSkipA_Use;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel23;
        private System.Windows.Forms.Button btn_LoaderInputSkipB_DontUse;
        private System.Windows.Forms.TextBox tbox_LoaderInputSkipB;
        private System.Windows.Forms.Button btn_LoaderInputSkipB_Use;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel32;
        private System.Windows.Forms.Label lbl_Setting;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel33;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel34;
        private System.Windows.Forms.GroupBox gbox_Setting_DistributionMode;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel35;
        private System.Windows.Forms.Label lbl_BreakingAlignSpeed;
        private System.Windows.Forms.Label lbl_FineAlignSpeed;
        private System.Windows.Forms.Label lbl_PreAlignSpeed;
        private System.Windows.Forms.GroupBox gbox_AutoDeleteCycle;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel40;
        private System.Windows.Forms.TextBox tbox_AutoDeleteCycle;
        private System.Windows.Forms.Label lbl_AutoDeleteCycle;
        private System.Windows.Forms.GroupBox gbox_BreakingCount;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel39;
        private System.Windows.Forms.Label lbl_CriticalAlarm;
        private System.Windows.Forms.TextBox tbox_LightAlarm;
        private System.Windows.Forms.Label lbl_LightAlarm;
        private System.Windows.Forms.TextBox tbox_CriticalAlarm;
        private System.Windows.Forms.GroupBox gbox_DoorOpenSpeed;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel38;
        private System.Windows.Forms.TextBox tbox_DoorOpenSpeed;
        private System.Windows.Forms.Label lbl_DoorOpenSpped;
        private System.Windows.Forms.GroupBox gbox_CountError;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel37;
        private System.Windows.Forms.Label lbl_INSP;
        private System.Windows.Forms.TextBox tbox_MCR;
        private System.Windows.Forms.Label lbl_MCR;
        private System.Windows.Forms.TextBox tbox_INSP;
        private System.Windows.Forms.GroupBox gbox_Timeout;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel36;
        private System.Windows.Forms.Label lbl_MutingTimeout;
        private System.Windows.Forms.TextBox tbox_VacuumTimeout;
        private System.Windows.Forms.Label lbl_VacuumTimeout;
        private System.Windows.Forms.TextBox tbox_CylinderTimeout;
        private System.Windows.Forms.TextBox tbox_PreAlignTimeout;
        private System.Windows.Forms.TextBox tbox_BreakingAlignTimeout;
        private System.Windows.Forms.TextBox tbox_InspectiongTimeout;
        private System.Windows.Forms.TextBox tbox_SequenceMoveTimeout;
        private System.Windows.Forms.TextBox tbox_MutingTimeout;
        private System.Windows.Forms.Label lbl_CylinderTimeout;
        private System.Windows.Forms.Label lbl_PreAlignTimeout;
        private System.Windows.Forms.Label lbl_BreakingAlignTimeout;
        private System.Windows.Forms.Label lbl_InspectionTimeout;
        private System.Windows.Forms.Label lbl_SequenceMoveTimeout;
        private System.Windows.Forms.TextBox tbox_PreAlignSpeed;
        private System.Windows.Forms.TextBox tbox_FineAlignSpeed;
        private System.Windows.Forms.TextBox tbox_BreakingAlignSpeed;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel41;
        private System.Windows.Forms.GroupBox gbox_LDS;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel48;
        private System.Windows.Forms.Label lbl_ErrorRange;
        private System.Windows.Forms.Label lbl_WaitingTime;
        private System.Windows.Forms.TextBox tbox_LDS_MeasureCycle;
        private System.Windows.Forms.Label lbl_LDS_MeasureCycle;
        private System.Windows.Forms.TextBox tbox_WaitingTime;
        private System.Windows.Forms.Label lbl_LDS_MeasureTime;
        private System.Windows.Forms.TextBox tbox_LDS_MeasureTime;
        private System.Windows.Forms.TextBox tbox_ErrorRange;
        private System.Windows.Forms.GroupBox gbox_TemperatureAlarm;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel42;
        private System.Windows.Forms.TextBox tbox_TemperatureLightAlarm;
        private System.Windows.Forms.TextBox tbox_TemperatureCriticalAlarm;
        private System.Windows.Forms.Label lbl_TemperatureAlarm;
        private System.Windows.Forms.GroupBox gbox_MeasureCycle;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel43;
        private System.Windows.Forms.TextBox tbox_CutLineMeasure;
        private System.Windows.Forms.Label lbl_CutLineMeasure;
        private System.Windows.Forms.GroupBox gbox_BlowingCheck;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel44;
        private System.Windows.Forms.TextBox tbox_BlowingCheck;
        private System.Windows.Forms.Label lbl_BlowingCheck;
        private System.Windows.Forms.GroupBox gbox_RetryCount;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel45;
        private System.Windows.Forms.TextBox tbox_RetryCount;
        private System.Windows.Forms.Label lbl_RetryCount;
        private System.Windows.Forms.GroupBox gbox_LaserPD7Error;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel46;
        private System.Windows.Forms.Label lbl_LaserPD7ErrorRange;
        private System.Windows.Forms.TextBox tbox_LaserPD7Power;
        private System.Windows.Forms.Label lbl_LaserPD7Power;
        private System.Windows.Forms.TextBox tbox_LaserPD7ErrorRange;
        private System.Windows.Forms.GroupBox gbox_LaserPowerMeasure;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel47;
        private System.Windows.Forms.Label lbl_TargetErrorRange;
        private System.Windows.Forms.TextBox tbox_MeasureCycle;
        private System.Windows.Forms.Label lbl_LaserPowerMeasure_MeasureCycle;
        private System.Windows.Forms.TextBox tbox_TargetPEC;
        private System.Windows.Forms.TextBox tbox_TargetPower;
        private System.Windows.Forms.TextBox tbox_WarmUpTime;
        private System.Windows.Forms.TextBox tbox_MeasureTime;
        private System.Windows.Forms.TextBox tbox_CorrectionErrorRange;
        private System.Windows.Forms.TextBox tbox_TargetErrorRange;
        private System.Windows.Forms.Label lbl_TargetPEC;
        private System.Windows.Forms.Label lbl_TargetPower;
        private System.Windows.Forms.Label lbl_WarmUpTime;
        private System.Windows.Forms.Label lbl_MeasureTime;
        private System.Windows.Forms.Label lbl_CorrectionErrorRange;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.Button btn_AlarmClear;
    }
}
