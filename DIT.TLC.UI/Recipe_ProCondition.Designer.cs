﻿namespace DIT.TLC.UI
{
    partial class Recipe_ProCondition
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel_process = new System.Windows.Forms.Panel();
            this.btn_process_save = new System.Windows.Forms.Button();
            this.tbox_process_acc_dec = new System.Windows.Forms.TextBox();
            this.lb_process_acc_dec = new System.Windows.Forms.Label();
            this.tbox_process_scan = new System.Windows.Forms.TextBox();
            this.lb_process_scan = new System.Windows.Forms.Label();
            this.tbox_process_g = new System.Windows.Forms.TextBox();
            this.lb_process_g = new System.Windows.Forms.Label();
            this.tbox_process_segment_value = new System.Windows.Forms.TextBox();
            this.lb_process_segment_value = new System.Windows.Forms.Label();
            this.tbox_process_overlap = new System.Windows.Forms.TextBox();
            this.lb_process_overlap = new System.Windows.Forms.Label();
            this.tbox_process_zpos = new System.Windows.Forms.TextBox();
            this.lb_process_zpos = new System.Windows.Forms.Label();
            this.tbox_process_speed = new System.Windows.Forms.TextBox();
            this.lb_process_speed = new System.Windows.Forms.Label();
            this.tbox_process_frequence = new System.Windows.Forms.TextBox();
            this.lb_process_frequence = new System.Windows.Forms.Label();
            this.tbox_process_burst = new System.Windows.Forms.TextBox();
            this.lb_process_burst = new System.Windows.Forms.Label();
            this.tbox_process_error = new System.Windows.Forms.TextBox();
            this.lb_process_error = new System.Windows.Forms.Label();
            this.tbox_process_power_ratio = new System.Windows.Forms.TextBox();
            this.lb_process_power_ratio = new System.Windows.Forms.Label();
            this.tbox_process_power = new System.Windows.Forms.TextBox();
            this.lb_process_power = new System.Windows.Forms.Label();
            this.tbox_process_divider = new System.Windows.Forms.TextBox();
            this.btn_process_delete = new System.Windows.Forms.Button();
            this.lb_process_divider = new System.Windows.Forms.Label();
            this.btn_process_copy = new System.Windows.Forms.Button();
            this.btn_process_make = new System.Windows.Forms.Button();
            this.lv_process = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel_breaking = new System.Windows.Forms.Panel();
            this.gbox_breaking_load_trans_sequence_offset = new System.Windows.Forms.GroupBox();
            this.gbox_breaking_load_trans_sequence_offset_b = new System.Windows.Forms.GroupBox();
            this.lb_breaking_load_trans_sequence_offset_b_y2_mm = new System.Windows.Forms.Label();
            this.tbox_breaking_load_trans_sequence_offset_b_y1 = new System.Windows.Forms.TextBox();
            this.lb_breaking_load_trans_sequence_offset_b_y1_mm = new System.Windows.Forms.Label();
            this.lb_breaking_load_trans_sequence_offset_b_y1 = new System.Windows.Forms.Label();
            this.tbox_breaking_load_trans_sequence_offset_b_y2 = new System.Windows.Forms.TextBox();
            this.lb_breaking_load_trans_sequence_offset_b_y2 = new System.Windows.Forms.Label();
            this.gbox_breaking_load_trans_sequence_offset_a = new System.Windows.Forms.GroupBox();
            this.lb_breaking_load_trans_sequence_offset_a_y2_mm = new System.Windows.Forms.Label();
            this.lb_breaking_load_trans_sequence_offset_a_y1_mm = new System.Windows.Forms.Label();
            this.tbox_breaking_load_trans_sequence_offset_a_y2 = new System.Windows.Forms.TextBox();
            this.tbox_breaking_load_trans_sequence_offset_a_y1 = new System.Windows.Forms.TextBox();
            this.lb_breaking_load_trans_sequence_offset_a_y2 = new System.Windows.Forms.Label();
            this.lb_breaking_load_trans_sequence_offset_a_y1 = new System.Windows.Forms.Label();
            this.tbox_breaking_slowdown_speed = new System.Windows.Forms.TextBox();
            this.lb_breaking_slowdown_speed = new System.Windows.Forms.Label();
            this.tbox_breaking_fastdown_speed = new System.Windows.Forms.TextBox();
            this.lb_breaking_fastdown_speed = new System.Windows.Forms.Label();
            this.lb_breaking_slowdown_ptich_mm = new System.Windows.Forms.Label();
            this.lb_breaking_fastdown_ptich_mm = new System.Windows.Forms.Label();
            this.lb_breaking_slowdown_ptich = new System.Windows.Forms.Label();
            this.tbox_breaking_slowdown_ptich = new System.Windows.Forms.TextBox();
            this.tbox_breaking_fastdown_ptich = new System.Windows.Forms.TextBox();
            this.lb_breaking_fastdown_ptich = new System.Windows.Forms.Label();
            this.panel_breaking_2pass = new System.Windows.Forms.Panel();
            this.lb_breaking_2pass = new System.Windows.Forms.Label();
            this.tbox_breaking_b_mcr_offset_xy_2 = new System.Windows.Forms.TextBox();
            this.tbox_breaking_a_mcr_offset_xy_2 = new System.Windows.Forms.TextBox();
            this.tbox_breaking_b_mcr_offset_xy_1 = new System.Windows.Forms.TextBox();
            this.lb_breaking_b_mcr_offset_xy = new System.Windows.Forms.Label();
            this.tbox_breaking_a_mcr_offset_xy_1 = new System.Windows.Forms.TextBox();
            this.lb_breaking_a_mcr_offset_xy = new System.Windows.Forms.Label();
            this.tbox_breaking_b2_xy_offset_2 = new System.Windows.Forms.TextBox();
            this.tbox_breaking_b2_xy_offset_1 = new System.Windows.Forms.TextBox();
            this.lb_breaking_b2_xy_offset = new System.Windows.Forms.Label();
            this.tbox_breaking_b1_xy_offset_2 = new System.Windows.Forms.TextBox();
            this.tbox_breaking_b1_xy_offset_1 = new System.Windows.Forms.TextBox();
            this.lb_breaking_b1_xy_offset = new System.Windows.Forms.Label();
            this.tbox_breaking_a2_xy_offset_2 = new System.Windows.Forms.TextBox();
            this.tbox_breaking_a2_xy_offset_1 = new System.Windows.Forms.TextBox();
            this.lb_breaking_a2_xy_offset = new System.Windows.Forms.Label();
            this.tbox_breaking_a1_xy_offset_2 = new System.Windows.Forms.TextBox();
            this.tbox_breaking_a1_xy_offset_1 = new System.Windows.Forms.TextBox();
            this.lb_breaking_a1_xy_offset = new System.Windows.Forms.Label();
            this.panel_breaking_xyt_breaking4_calc = new System.Windows.Forms.Panel();
            this.lb_breaking_xyt_breaking4_calc = new System.Windows.Forms.Label();
            this.tbox_breaking_xyt_breaking4_3 = new System.Windows.Forms.TextBox();
            this.tbox_breaking_xyt_breaking4_2 = new System.Windows.Forms.TextBox();
            this.tbox_breaking_xyt_breaking4_1 = new System.Windows.Forms.TextBox();
            this.lb_breaking_xyt_breaking4 = new System.Windows.Forms.Label();
            this.panel_breaking_xyt_breaking3_calc = new System.Windows.Forms.Panel();
            this.lb_breaking_xyt_breaking3_calc = new System.Windows.Forms.Label();
            this.tbox_breaking_xyt_breaking3_3 = new System.Windows.Forms.TextBox();
            this.tbox_breaking_xyt_breaking3_2 = new System.Windows.Forms.TextBox();
            this.tbox_breaking_xyt_breaking3_1 = new System.Windows.Forms.TextBox();
            this.lb_breaking_xyt_breaking3 = new System.Windows.Forms.Label();
            this.panel_breaking_xyt_breaking2_calc = new System.Windows.Forms.Panel();
            this.lb_breaking_xyt_breaking2_calc = new System.Windows.Forms.Label();
            this.tbox_breaking_xyt_breaking2_3 = new System.Windows.Forms.TextBox();
            this.panel_breaking_xyt_breaking1_calc = new System.Windows.Forms.Panel();
            this.label49 = new System.Windows.Forms.Label();
            this.tbox_breaking_xyt_breaking2_2 = new System.Windows.Forms.TextBox();
            this.panel_breaking_half = new System.Windows.Forms.Panel();
            this.lb_breaking_half = new System.Windows.Forms.Label();
            this.tbox_breaking_xyt_breaking2_1 = new System.Windows.Forms.TextBox();
            this.panel_breaking_right = new System.Windows.Forms.Panel();
            this.lb_breaking_right = new System.Windows.Forms.Label();
            this.lb_breaking_xyt_breaking2 = new System.Windows.Forms.Label();
            this.tbox_breaking_xyt_breaking1_3 = new System.Windows.Forms.TextBox();
            this.tbox_breaking_xyt_breaking1_2 = new System.Windows.Forms.TextBox();
            this.tbox_breaking_xyt_breaking1_1 = new System.Windows.Forms.TextBox();
            this.panel_breaking_left = new System.Windows.Forms.Panel();
            this.lb_breaking_left = new System.Windows.Forms.Label();
            this.tbox_breaking_xy_mcr_2 = new System.Windows.Forms.TextBox();
            this.tbox_breaking_xy_align_mark2_2 = new System.Windows.Forms.TextBox();
            this.tbox_breaking_xy_align_mark1_2 = new System.Windows.Forms.TextBox();
            this.tbox_breaking_xy_pin_center_2 = new System.Windows.Forms.TextBox();
            this.lb_breaking_xyt_breaking1 = new System.Windows.Forms.Label();
            this.tbox_breaking_pin_distance_b = new System.Windows.Forms.TextBox();
            this.lb_breaking_pin_distance_b = new System.Windows.Forms.Label();
            this.tbox_breaking_pin_distance_a = new System.Windows.Forms.TextBox();
            this.lb_breaking_pin_distance_a = new System.Windows.Forms.Label();
            this.tbox_breaking_xy_mcr_1 = new System.Windows.Forms.TextBox();
            this.panel_breaking_xy_mcr = new System.Windows.Forms.Panel();
            this.lb_breaking_xy_mcr = new System.Windows.Forms.Label();
            this.tbox_breaking_xy_align_mark2_1 = new System.Windows.Forms.TextBox();
            this.panel_breaking_xy_align_mark2 = new System.Windows.Forms.Panel();
            this.lb_breaking_xy_align_mark2 = new System.Windows.Forms.Label();
            this.tbox_breaking_xy_align_mark1_1 = new System.Windows.Forms.TextBox();
            this.panel_breaking_xy_align_mark1 = new System.Windows.Forms.Panel();
            this.lb_breaking_xy_align_mark1 = new System.Windows.Forms.Label();
            this.tbox_breaking_xy_pin_center_1 = new System.Windows.Forms.TextBox();
            this.panel_breaking_xy_pin_center = new System.Windows.Forms.Panel();
            this.lb_breaking_xy_pin_center = new System.Windows.Forms.Label();
            this.btn_breaking_save = new System.Windows.Forms.Button();
            this.btn_breaking_delete = new System.Windows.Forms.Button();
            this.btn_breaking_copy = new System.Windows.Forms.Button();
            this.btn_breaking_make = new System.Windows.Forms.Button();
            this.lv_breaking = new System.Windows.Forms.ListView();
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.textBox56 = new System.Windows.Forms.TextBox();
            this.textBox57 = new System.Windows.Forms.TextBox();
            this.textBox58 = new System.Windows.Forms.TextBox();
            this.textBox59 = new System.Windows.Forms.TextBox();
            this.button9 = new System.Windows.Forms.Button();
            this.panel_recipeinfo_2 = new System.Windows.Forms.Panel();
            this.panel_recipeinfo_1 = new System.Windows.Forms.Panel();
            this.gbox_breaking_zig = new System.Windows.Forms.GroupBox();
            this.btn_breaking_zig_save = new System.Windows.Forms.Button();
            this.tbox_breaking_zig_cam_to_pin = new System.Windows.Forms.TextBox();
            this.tbox_breaking_zig_thickness = new System.Windows.Forms.TextBox();
            this.tbox_breaking_zig_y = new System.Windows.Forms.TextBox();
            this.tbox_breaking_zig_x = new System.Windows.Forms.TextBox();
            this.lb_breaking_zig_cam_to_pin = new System.Windows.Forms.Label();
            this.lb_breaking_zig_thickness = new System.Windows.Forms.Label();
            this.lb_breaking_zig_y = new System.Windows.Forms.Label();
            this.lb_breaking_zig_x = new System.Windows.Forms.Label();
            this.panel_default_process = new System.Windows.Forms.Panel();
            this.tbox_default_process_prealign_error_y = new System.Windows.Forms.TextBox();
            this.tbox_default_process_align_angle = new System.Windows.Forms.TextBox();
            this.lb_default_process_prealign_error_y = new System.Windows.Forms.Label();
            this.lb_default_process_align_angle = new System.Windows.Forms.Label();
            this.tbox_default_process_prealign_error_x = new System.Windows.Forms.TextBox();
            this.tbox_default_process_align_distance = new System.Windows.Forms.TextBox();
            this.lb_default_process_prealign_error_x = new System.Windows.Forms.Label();
            this.lb_default_process_align_distance = new System.Windows.Forms.Label();
            this.tbox_default_process_break_error_y = new System.Windows.Forms.TextBox();
            this.lb_default_process_break_error_y = new System.Windows.Forms.Label();
            this.tbox_default_process_prealign_error_angle = new System.Windows.Forms.TextBox();
            this.tbox_default_process_align_match = new System.Windows.Forms.TextBox();
            this.lb_default_process_prealign_error_angle = new System.Windows.Forms.Label();
            this.lb_default_process_align_match = new System.Windows.Forms.Label();
            this.btn_default_process_save = new System.Windows.Forms.Button();
            this.tbox_default_process_default_z = new System.Windows.Forms.TextBox();
            this.tbox_default_process_jump_speed = new System.Windows.Forms.TextBox();
            this.lb_default_process_default_z = new System.Windows.Forms.Label();
            this.lb_default_process_jump_speed = new System.Windows.Forms.Label();
            this.panel_default_laser = new System.Windows.Forms.Panel();
            this.btn_default_laser_save = new System.Windows.Forms.Button();
            this.tbox_default_laser_cst_pitch_reverse = new System.Windows.Forms.TextBox();
            this.tbox_default_laser_cst_pitch_normal = new System.Windows.Forms.TextBox();
            this.tbox_default_laser_ontime = new System.Windows.Forms.TextBox();
            this.lb_default_laser_cst_pitch_reverse = new System.Windows.Forms.Label();
            this.lb_default_laser_cst_pitch_normal = new System.Windows.Forms.Label();
            this.lb_default_laser_ontime = new System.Windows.Forms.Label();
            this.panel_process.SuspendLayout();
            this.panel_breaking.SuspendLayout();
            this.gbox_breaking_load_trans_sequence_offset.SuspendLayout();
            this.gbox_breaking_load_trans_sequence_offset_b.SuspendLayout();
            this.gbox_breaking_load_trans_sequence_offset_a.SuspendLayout();
            this.panel_breaking_xyt_breaking4_calc.SuspendLayout();
            this.panel_breaking_xyt_breaking3_calc.SuspendLayout();
            this.panel_breaking_xyt_breaking2_calc.SuspendLayout();
            this.panel_breaking_xyt_breaking1_calc.SuspendLayout();
            this.panel_breaking_half.SuspendLayout();
            this.panel_breaking_right.SuspendLayout();
            this.panel_breaking_left.SuspendLayout();
            this.panel_breaking_xy_mcr.SuspendLayout();
            this.panel_breaking_xy_align_mark2.SuspendLayout();
            this.panel_breaking_xy_align_mark1.SuspendLayout();
            this.panel_breaking_xy_pin_center.SuspendLayout();
            this.gbox_breaking_zig.SuspendLayout();
            this.panel_default_process.SuspendLayout();
            this.panel_default_laser.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_process
            // 
            this.panel_process.BackColor = System.Drawing.Color.DimGray;
            this.panel_process.Controls.Add(this.btn_process_save);
            this.panel_process.Controls.Add(this.tbox_process_acc_dec);
            this.panel_process.Controls.Add(this.lb_process_acc_dec);
            this.panel_process.Controls.Add(this.tbox_process_scan);
            this.panel_process.Controls.Add(this.lb_process_scan);
            this.panel_process.Controls.Add(this.tbox_process_g);
            this.panel_process.Controls.Add(this.lb_process_g);
            this.panel_process.Controls.Add(this.tbox_process_segment_value);
            this.panel_process.Controls.Add(this.lb_process_segment_value);
            this.panel_process.Controls.Add(this.tbox_process_overlap);
            this.panel_process.Controls.Add(this.lb_process_overlap);
            this.panel_process.Controls.Add(this.tbox_process_zpos);
            this.panel_process.Controls.Add(this.lb_process_zpos);
            this.panel_process.Controls.Add(this.tbox_process_speed);
            this.panel_process.Controls.Add(this.lb_process_speed);
            this.panel_process.Controls.Add(this.tbox_process_frequence);
            this.panel_process.Controls.Add(this.lb_process_frequence);
            this.panel_process.Controls.Add(this.tbox_process_burst);
            this.panel_process.Controls.Add(this.lb_process_burst);
            this.panel_process.Controls.Add(this.tbox_process_error);
            this.panel_process.Controls.Add(this.lb_process_error);
            this.panel_process.Controls.Add(this.tbox_process_power_ratio);
            this.panel_process.Controls.Add(this.lb_process_power_ratio);
            this.panel_process.Controls.Add(this.tbox_process_power);
            this.panel_process.Controls.Add(this.lb_process_power);
            this.panel_process.Controls.Add(this.tbox_process_divider);
            this.panel_process.Controls.Add(this.btn_process_delete);
            this.panel_process.Controls.Add(this.lb_process_divider);
            this.panel_process.Controls.Add(this.btn_process_copy);
            this.panel_process.Controls.Add(this.btn_process_make);
            this.panel_process.Controls.Add(this.lv_process);
            this.panel_process.Location = new System.Drawing.Point(3, 3);
            this.panel_process.Name = "panel_process";
            this.panel_process.Size = new System.Drawing.Size(1734, 173);
            this.panel_process.TabIndex = 0;
            // 
            // btn_process_save
            // 
            this.btn_process_save.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_process_save.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_process_save.ForeColor = System.Drawing.Color.White;
            this.btn_process_save.Location = new System.Drawing.Point(1601, 139);
            this.btn_process_save.Name = "btn_process_save";
            this.btn_process_save.Size = new System.Drawing.Size(130, 30);
            this.btn_process_save.TabIndex = 69;
            this.btn_process_save.Text = "Save";
            this.btn_process_save.UseVisualStyleBackColor = false;
            // 
            // tbox_process_acc_dec
            // 
            this.tbox_process_acc_dec.Location = new System.Drawing.Point(1325, 106);
            this.tbox_process_acc_dec.Name = "tbox_process_acc_dec";
            this.tbox_process_acc_dec.Size = new System.Drawing.Size(130, 21);
            this.tbox_process_acc_dec.TabIndex = 68;
            // 
            // lb_process_acc_dec
            // 
            this.lb_process_acc_dec.AutoSize = true;
            this.lb_process_acc_dec.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_acc_dec.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_acc_dec.ForeColor = System.Drawing.Color.White;
            this.lb_process_acc_dec.Location = new System.Drawing.Point(1250, 108);
            this.lb_process_acc_dec.Name = "lb_process_acc_dec";
            this.lb_process_acc_dec.Size = new System.Drawing.Size(66, 13);
            this.lb_process_acc_dec.TabIndex = 67;
            this.lb_process_acc_dec.Text = "Acc dec";
            // 
            // tbox_process_scan
            // 
            this.tbox_process_scan.Location = new System.Drawing.Point(1325, 75);
            this.tbox_process_scan.Name = "tbox_process_scan";
            this.tbox_process_scan.Size = new System.Drawing.Size(130, 21);
            this.tbox_process_scan.TabIndex = 66;
            // 
            // lb_process_scan
            // 
            this.lb_process_scan.AutoSize = true;
            this.lb_process_scan.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_scan.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_scan.ForeColor = System.Drawing.Color.White;
            this.lb_process_scan.Location = new System.Drawing.Point(1272, 77);
            this.lb_process_scan.Name = "lb_process_scan";
            this.lb_process_scan.Size = new System.Drawing.Size(44, 13);
            this.lb_process_scan.TabIndex = 65;
            this.lb_process_scan.Text = "Scan";
            // 
            // tbox_process_g
            // 
            this.tbox_process_g.Location = new System.Drawing.Point(1325, 9);
            this.tbox_process_g.Name = "tbox_process_g";
            this.tbox_process_g.Size = new System.Drawing.Size(130, 21);
            this.tbox_process_g.TabIndex = 62;
            // 
            // lb_process_g
            // 
            this.lb_process_g.AutoSize = true;
            this.lb_process_g.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_g.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_g.ForeColor = System.Drawing.Color.White;
            this.lb_process_g.Location = new System.Drawing.Point(1298, 11);
            this.lb_process_g.Name = "lb_process_g";
            this.lb_process_g.Size = new System.Drawing.Size(18, 13);
            this.lb_process_g.TabIndex = 61;
            this.lb_process_g.Text = "G";
            // 
            // tbox_process_segment_value
            // 
            this.tbox_process_segment_value.Location = new System.Drawing.Point(1042, 106);
            this.tbox_process_segment_value.Name = "tbox_process_segment_value";
            this.tbox_process_segment_value.Size = new System.Drawing.Size(130, 21);
            this.tbox_process_segment_value.TabIndex = 60;
            // 
            // lb_process_segment_value
            // 
            this.lb_process_segment_value.AutoSize = true;
            this.lb_process_segment_value.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_segment_value.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_segment_value.ForeColor = System.Drawing.Color.White;
            this.lb_process_segment_value.Location = new System.Drawing.Point(922, 108);
            this.lb_process_segment_value.Name = "lb_process_segment_value";
            this.lb_process_segment_value.Size = new System.Drawing.Size(114, 13);
            this.lb_process_segment_value.TabIndex = 59;
            this.lb_process_segment_value.Text = "Segment Value";
            // 
            // tbox_process_overlap
            // 
            this.tbox_process_overlap.Location = new System.Drawing.Point(1042, 73);
            this.tbox_process_overlap.Name = "tbox_process_overlap";
            this.tbox_process_overlap.Size = new System.Drawing.Size(130, 21);
            this.tbox_process_overlap.TabIndex = 58;
            // 
            // lb_process_overlap
            // 
            this.lb_process_overlap.AutoSize = true;
            this.lb_process_overlap.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_overlap.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_overlap.ForeColor = System.Drawing.Color.White;
            this.lb_process_overlap.Location = new System.Drawing.Point(971, 75);
            this.lb_process_overlap.Name = "lb_process_overlap";
            this.lb_process_overlap.Size = new System.Drawing.Size(62, 13);
            this.lb_process_overlap.TabIndex = 57;
            this.lb_process_overlap.Text = "Overlap";
            // 
            // tbox_process_zpos
            // 
            this.tbox_process_zpos.Location = new System.Drawing.Point(1042, 42);
            this.tbox_process_zpos.Name = "tbox_process_zpos";
            this.tbox_process_zpos.Size = new System.Drawing.Size(130, 21);
            this.tbox_process_zpos.TabIndex = 56;
            // 
            // lb_process_zpos
            // 
            this.lb_process_zpos.AutoSize = true;
            this.lb_process_zpos.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_zpos.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_zpos.ForeColor = System.Drawing.Color.White;
            this.lb_process_zpos.Location = new System.Drawing.Point(957, 44);
            this.lb_process_zpos.Name = "lb_process_zpos";
            this.lb_process_zpos.Size = new System.Drawing.Size(85, 13);
            this.lb_process_zpos.TabIndex = 55;
            this.lb_process_zpos.Text = "ZPos[mm]";
            // 
            // tbox_process_speed
            // 
            this.tbox_process_speed.Location = new System.Drawing.Point(1042, 9);
            this.tbox_process_speed.Name = "tbox_process_speed";
            this.tbox_process_speed.Size = new System.Drawing.Size(130, 21);
            this.tbox_process_speed.TabIndex = 54;
            // 
            // lb_process_speed
            // 
            this.lb_process_speed.AutoSize = true;
            this.lb_process_speed.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_speed.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_speed.ForeColor = System.Drawing.Color.White;
            this.lb_process_speed.Location = new System.Drawing.Point(980, 11);
            this.lb_process_speed.Name = "lb_process_speed";
            this.lb_process_speed.Size = new System.Drawing.Size(53, 13);
            this.lb_process_speed.TabIndex = 53;
            this.lb_process_speed.Text = "Speed";
            // 
            // tbox_process_frequence
            // 
            this.tbox_process_frequence.Location = new System.Drawing.Point(771, 71);
            this.tbox_process_frequence.Name = "tbox_process_frequence";
            this.tbox_process_frequence.Size = new System.Drawing.Size(130, 21);
            this.tbox_process_frequence.TabIndex = 52;
            // 
            // lb_process_frequence
            // 
            this.lb_process_frequence.AutoSize = true;
            this.lb_process_frequence.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_frequence.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_frequence.ForeColor = System.Drawing.Color.White;
            this.lb_process_frequence.Location = new System.Drawing.Point(681, 77);
            this.lb_process_frequence.Name = "lb_process_frequence";
            this.lb_process_frequence.Size = new System.Drawing.Size(84, 13);
            this.lb_process_frequence.TabIndex = 51;
            this.lb_process_frequence.Text = "Frequence";
            // 
            // tbox_process_burst
            // 
            this.tbox_process_burst.Location = new System.Drawing.Point(512, 71);
            this.tbox_process_burst.Name = "tbox_process_burst";
            this.tbox_process_burst.Size = new System.Drawing.Size(130, 21);
            this.tbox_process_burst.TabIndex = 50;
            // 
            // lb_process_burst
            // 
            this.lb_process_burst.AutoSize = true;
            this.lb_process_burst.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_burst.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_burst.ForeColor = System.Drawing.Color.White;
            this.lb_process_burst.Location = new System.Drawing.Point(462, 75);
            this.lb_process_burst.Name = "lb_process_burst";
            this.lb_process_burst.Size = new System.Drawing.Size(45, 13);
            this.lb_process_burst.TabIndex = 49;
            this.lb_process_burst.Text = "Burst";
            // 
            // tbox_process_error
            // 
            this.tbox_process_error.Location = new System.Drawing.Point(512, 40);
            this.tbox_process_error.Name = "tbox_process_error";
            this.tbox_process_error.Size = new System.Drawing.Size(130, 21);
            this.tbox_process_error.TabIndex = 48;
            // 
            // lb_process_error
            // 
            this.lb_process_error.AutoSize = true;
            this.lb_process_error.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_error.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_error.ForeColor = System.Drawing.Color.White;
            this.lb_process_error.Location = new System.Drawing.Point(450, 42);
            this.lb_process_error.Name = "lb_process_error";
            this.lb_process_error.Size = new System.Drawing.Size(57, 13);
            this.lb_process_error.TabIndex = 47;
            this.lb_process_error.Text = "ERROR";
            // 
            // tbox_process_power_ratio
            // 
            this.tbox_process_power_ratio.Location = new System.Drawing.Point(275, 71);
            this.tbox_process_power_ratio.Name = "tbox_process_power_ratio";
            this.tbox_process_power_ratio.Size = new System.Drawing.Size(130, 21);
            this.tbox_process_power_ratio.TabIndex = 46;
            // 
            // lb_process_power_ratio
            // 
            this.lb_process_power_ratio.AutoSize = true;
            this.lb_process_power_ratio.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_power_ratio.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_power_ratio.ForeColor = System.Drawing.Color.White;
            this.lb_process_power_ratio.Location = new System.Drawing.Point(180, 73);
            this.lb_process_power_ratio.Name = "lb_process_power_ratio";
            this.lb_process_power_ratio.Size = new System.Drawing.Size(89, 13);
            this.lb_process_power_ratio.TabIndex = 45;
            this.lb_process_power_ratio.Text = "Power ratio";
            // 
            // tbox_process_power
            // 
            this.tbox_process_power.Location = new System.Drawing.Point(275, 40);
            this.tbox_process_power.Name = "tbox_process_power";
            this.tbox_process_power.Size = new System.Drawing.Size(130, 21);
            this.tbox_process_power.TabIndex = 44;
            // 
            // lb_process_power
            // 
            this.lb_process_power.AutoSize = true;
            this.lb_process_power.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_power.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_power.ForeColor = System.Drawing.Color.White;
            this.lb_process_power.Location = new System.Drawing.Point(190, 42);
            this.lb_process_power.Name = "lb_process_power";
            this.lb_process_power.Size = new System.Drawing.Size(79, 13);
            this.lb_process_power.TabIndex = 43;
            this.lb_process_power.Text = "Power[w]";
            // 
            // tbox_process_divider
            // 
            this.tbox_process_divider.Location = new System.Drawing.Point(275, 9);
            this.tbox_process_divider.Name = "tbox_process_divider";
            this.tbox_process_divider.Size = new System.Drawing.Size(130, 21);
            this.tbox_process_divider.TabIndex = 42;
            // 
            // btn_process_delete
            // 
            this.btn_process_delete.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_process_delete.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_process_delete.ForeColor = System.Drawing.Color.White;
            this.btn_process_delete.Location = new System.Drawing.Point(275, 140);
            this.btn_process_delete.Name = "btn_process_delete";
            this.btn_process_delete.Size = new System.Drawing.Size(130, 30);
            this.btn_process_delete.TabIndex = 41;
            this.btn_process_delete.Text = "Delete";
            this.btn_process_delete.UseVisualStyleBackColor = false;
            // 
            // lb_process_divider
            // 
            this.lb_process_divider.AutoSize = true;
            this.lb_process_divider.BackColor = System.Drawing.Color.Transparent;
            this.lb_process_divider.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_process_divider.ForeColor = System.Drawing.Color.White;
            this.lb_process_divider.Location = new System.Drawing.Point(213, 11);
            this.lb_process_divider.Name = "lb_process_divider";
            this.lb_process_divider.Size = new System.Drawing.Size(56, 13);
            this.lb_process_divider.TabIndex = 40;
            this.lb_process_divider.Text = "Divider";
            // 
            // btn_process_copy
            // 
            this.btn_process_copy.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_process_copy.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_process_copy.ForeColor = System.Drawing.Color.White;
            this.btn_process_copy.Location = new System.Drawing.Point(139, 140);
            this.btn_process_copy.Name = "btn_process_copy";
            this.btn_process_copy.Size = new System.Drawing.Size(130, 30);
            this.btn_process_copy.TabIndex = 39;
            this.btn_process_copy.Text = "Copy";
            this.btn_process_copy.UseVisualStyleBackColor = false;
            // 
            // btn_process_make
            // 
            this.btn_process_make.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_process_make.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_process_make.ForeColor = System.Drawing.Color.White;
            this.btn_process_make.Location = new System.Drawing.Point(3, 139);
            this.btn_process_make.Name = "btn_process_make";
            this.btn_process_make.Size = new System.Drawing.Size(130, 30);
            this.btn_process_make.TabIndex = 38;
            this.btn_process_make.Text = "Make";
            this.btn_process_make.UseVisualStyleBackColor = false;
            // 
            // lv_process
            // 
            this.lv_process.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.lv_process.GridLines = true;
            this.lv_process.Location = new System.Drawing.Point(3, 3);
            this.lv_process.Name = "lv_process";
            this.lv_process.Size = new System.Drawing.Size(130, 130);
            this.lv_process.TabIndex = 0;
            this.lv_process.UseCompatibleStateImageBehavior = false;
            this.lv_process.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Name";
            this.columnHeader1.Width = 120;
            // 
            // panel_breaking
            // 
            this.panel_breaking.BackColor = System.Drawing.Color.DimGray;
            this.panel_breaking.Controls.Add(this.gbox_breaking_load_trans_sequence_offset);
            this.panel_breaking.Controls.Add(this.tbox_breaking_slowdown_speed);
            this.panel_breaking.Controls.Add(this.lb_breaking_slowdown_speed);
            this.panel_breaking.Controls.Add(this.tbox_breaking_fastdown_speed);
            this.panel_breaking.Controls.Add(this.lb_breaking_fastdown_speed);
            this.panel_breaking.Controls.Add(this.lb_breaking_slowdown_ptich_mm);
            this.panel_breaking.Controls.Add(this.lb_breaking_fastdown_ptich_mm);
            this.panel_breaking.Controls.Add(this.lb_breaking_slowdown_ptich);
            this.panel_breaking.Controls.Add(this.tbox_breaking_slowdown_ptich);
            this.panel_breaking.Controls.Add(this.tbox_breaking_fastdown_ptich);
            this.panel_breaking.Controls.Add(this.lb_breaking_fastdown_ptich);
            this.panel_breaking.Controls.Add(this.panel_breaking_2pass);
            this.panel_breaking.Controls.Add(this.lb_breaking_2pass);
            this.panel_breaking.Controls.Add(this.tbox_breaking_b_mcr_offset_xy_2);
            this.panel_breaking.Controls.Add(this.tbox_breaking_a_mcr_offset_xy_2);
            this.panel_breaking.Controls.Add(this.tbox_breaking_b_mcr_offset_xy_1);
            this.panel_breaking.Controls.Add(this.lb_breaking_b_mcr_offset_xy);
            this.panel_breaking.Controls.Add(this.tbox_breaking_a_mcr_offset_xy_1);
            this.panel_breaking.Controls.Add(this.lb_breaking_a_mcr_offset_xy);
            this.panel_breaking.Controls.Add(this.tbox_breaking_b2_xy_offset_2);
            this.panel_breaking.Controls.Add(this.tbox_breaking_b2_xy_offset_1);
            this.panel_breaking.Controls.Add(this.lb_breaking_b2_xy_offset);
            this.panel_breaking.Controls.Add(this.tbox_breaking_b1_xy_offset_2);
            this.panel_breaking.Controls.Add(this.tbox_breaking_b1_xy_offset_1);
            this.panel_breaking.Controls.Add(this.lb_breaking_b1_xy_offset);
            this.panel_breaking.Controls.Add(this.tbox_breaking_a2_xy_offset_2);
            this.panel_breaking.Controls.Add(this.tbox_breaking_a2_xy_offset_1);
            this.panel_breaking.Controls.Add(this.lb_breaking_a2_xy_offset);
            this.panel_breaking.Controls.Add(this.tbox_breaking_a1_xy_offset_2);
            this.panel_breaking.Controls.Add(this.tbox_breaking_a1_xy_offset_1);
            this.panel_breaking.Controls.Add(this.lb_breaking_a1_xy_offset);
            this.panel_breaking.Controls.Add(this.panel_breaking_xyt_breaking4_calc);
            this.panel_breaking.Controls.Add(this.tbox_breaking_xyt_breaking4_3);
            this.panel_breaking.Controls.Add(this.tbox_breaking_xyt_breaking4_2);
            this.panel_breaking.Controls.Add(this.tbox_breaking_xyt_breaking4_1);
            this.panel_breaking.Controls.Add(this.lb_breaking_xyt_breaking4);
            this.panel_breaking.Controls.Add(this.panel_breaking_xyt_breaking3_calc);
            this.panel_breaking.Controls.Add(this.tbox_breaking_xyt_breaking3_3);
            this.panel_breaking.Controls.Add(this.tbox_breaking_xyt_breaking3_2);
            this.panel_breaking.Controls.Add(this.tbox_breaking_xyt_breaking3_1);
            this.panel_breaking.Controls.Add(this.lb_breaking_xyt_breaking3);
            this.panel_breaking.Controls.Add(this.panel_breaking_xyt_breaking2_calc);
            this.panel_breaking.Controls.Add(this.tbox_breaking_xyt_breaking2_3);
            this.panel_breaking.Controls.Add(this.panel_breaking_xyt_breaking1_calc);
            this.panel_breaking.Controls.Add(this.tbox_breaking_xyt_breaking2_2);
            this.panel_breaking.Controls.Add(this.panel_breaking_half);
            this.panel_breaking.Controls.Add(this.tbox_breaking_xyt_breaking2_1);
            this.panel_breaking.Controls.Add(this.panel_breaking_right);
            this.panel_breaking.Controls.Add(this.lb_breaking_xyt_breaking2);
            this.panel_breaking.Controls.Add(this.tbox_breaking_xyt_breaking1_3);
            this.panel_breaking.Controls.Add(this.tbox_breaking_xyt_breaking1_2);
            this.panel_breaking.Controls.Add(this.tbox_breaking_xyt_breaking1_1);
            this.panel_breaking.Controls.Add(this.panel_breaking_left);
            this.panel_breaking.Controls.Add(this.tbox_breaking_xy_mcr_2);
            this.panel_breaking.Controls.Add(this.tbox_breaking_xy_align_mark2_2);
            this.panel_breaking.Controls.Add(this.tbox_breaking_xy_align_mark1_2);
            this.panel_breaking.Controls.Add(this.tbox_breaking_xy_pin_center_2);
            this.panel_breaking.Controls.Add(this.lb_breaking_xyt_breaking1);
            this.panel_breaking.Controls.Add(this.tbox_breaking_pin_distance_b);
            this.panel_breaking.Controls.Add(this.lb_breaking_pin_distance_b);
            this.panel_breaking.Controls.Add(this.tbox_breaking_pin_distance_a);
            this.panel_breaking.Controls.Add(this.lb_breaking_pin_distance_a);
            this.panel_breaking.Controls.Add(this.tbox_breaking_xy_mcr_1);
            this.panel_breaking.Controls.Add(this.panel_breaking_xy_mcr);
            this.panel_breaking.Controls.Add(this.tbox_breaking_xy_align_mark2_1);
            this.panel_breaking.Controls.Add(this.panel_breaking_xy_align_mark2);
            this.panel_breaking.Controls.Add(this.tbox_breaking_xy_align_mark1_1);
            this.panel_breaking.Controls.Add(this.panel_breaking_xy_align_mark1);
            this.panel_breaking.Controls.Add(this.tbox_breaking_xy_pin_center_1);
            this.panel_breaking.Controls.Add(this.panel_breaking_xy_pin_center);
            this.panel_breaking.Controls.Add(this.btn_breaking_save);
            this.panel_breaking.Controls.Add(this.btn_breaking_delete);
            this.panel_breaking.Controls.Add(this.btn_breaking_copy);
            this.panel_breaking.Controls.Add(this.btn_breaking_make);
            this.panel_breaking.Controls.Add(this.lv_breaking);
            this.panel_breaking.Location = new System.Drawing.Point(3, 178);
            this.panel_breaking.Name = "panel_breaking";
            this.panel_breaking.Size = new System.Drawing.Size(1734, 299);
            this.panel_breaking.TabIndex = 70;
            // 
            // gbox_breaking_load_trans_sequence_offset
            // 
            this.gbox_breaking_load_trans_sequence_offset.BackColor = System.Drawing.Color.Transparent;
            this.gbox_breaking_load_trans_sequence_offset.Controls.Add(this.gbox_breaking_load_trans_sequence_offset_b);
            this.gbox_breaking_load_trans_sequence_offset.Controls.Add(this.gbox_breaking_load_trans_sequence_offset_a);
            this.gbox_breaking_load_trans_sequence_offset.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.gbox_breaking_load_trans_sequence_offset.ForeColor = System.Drawing.Color.White;
            this.gbox_breaking_load_trans_sequence_offset.Location = new System.Drawing.Point(1388, 129);
            this.gbox_breaking_load_trans_sequence_offset.Name = "gbox_breaking_load_trans_sequence_offset";
            this.gbox_breaking_load_trans_sequence_offset.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gbox_breaking_load_trans_sequence_offset.Size = new System.Drawing.Size(343, 131);
            this.gbox_breaking_load_trans_sequence_offset.TabIndex = 137;
            this.gbox_breaking_load_trans_sequence_offset.TabStop = false;
            this.gbox_breaking_load_trans_sequence_offset.Text = "로드 이재기 Sequence Offset";
            // 
            // gbox_breaking_load_trans_sequence_offset_b
            // 
            this.gbox_breaking_load_trans_sequence_offset_b.BackColor = System.Drawing.Color.Transparent;
            this.gbox_breaking_load_trans_sequence_offset_b.Controls.Add(this.lb_breaking_load_trans_sequence_offset_b_y2_mm);
            this.gbox_breaking_load_trans_sequence_offset_b.Controls.Add(this.tbox_breaking_load_trans_sequence_offset_b_y1);
            this.gbox_breaking_load_trans_sequence_offset_b.Controls.Add(this.lb_breaking_load_trans_sequence_offset_b_y1_mm);
            this.gbox_breaking_load_trans_sequence_offset_b.Controls.Add(this.lb_breaking_load_trans_sequence_offset_b_y1);
            this.gbox_breaking_load_trans_sequence_offset_b.Controls.Add(this.tbox_breaking_load_trans_sequence_offset_b_y2);
            this.gbox_breaking_load_trans_sequence_offset_b.Controls.Add(this.lb_breaking_load_trans_sequence_offset_b_y2);
            this.gbox_breaking_load_trans_sequence_offset_b.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_breaking_load_trans_sequence_offset_b.ForeColor = System.Drawing.Color.White;
            this.gbox_breaking_load_trans_sequence_offset_b.Location = new System.Drawing.Point(176, 25);
            this.gbox_breaking_load_trans_sequence_offset_b.Name = "gbox_breaking_load_trans_sequence_offset_b";
            this.gbox_breaking_load_trans_sequence_offset_b.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gbox_breaking_load_trans_sequence_offset_b.Size = new System.Drawing.Size(160, 100);
            this.gbox_breaking_load_trans_sequence_offset_b.TabIndex = 139;
            this.gbox_breaking_load_trans_sequence_offset_b.TabStop = false;
            this.gbox_breaking_load_trans_sequence_offset_b.Text = "B";
            // 
            // lb_breaking_load_trans_sequence_offset_b_y2_mm
            // 
            this.lb_breaking_load_trans_sequence_offset_b_y2_mm.AutoSize = true;
            this.lb_breaking_load_trans_sequence_offset_b_y2_mm.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_load_trans_sequence_offset_b_y2_mm.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_load_trans_sequence_offset_b_y2_mm.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_load_trans_sequence_offset_b_y2_mm.Location = new System.Drawing.Point(108, 64);
            this.lb_breaking_load_trans_sequence_offset_b_y2_mm.Name = "lb_breaking_load_trans_sequence_offset_b_y2_mm";
            this.lb_breaking_load_trans_sequence_offset_b_y2_mm.Size = new System.Drawing.Size(43, 13);
            this.lb_breaking_load_trans_sequence_offset_b_y2_mm.TabIndex = 148;
            this.lb_breaking_load_trans_sequence_offset_b_y2_mm.Text = "(mm)";
            // 
            // tbox_breaking_load_trans_sequence_offset_b_y1
            // 
            this.tbox_breaking_load_trans_sequence_offset_b_y1.Location = new System.Drawing.Point(43, 29);
            this.tbox_breaking_load_trans_sequence_offset_b_y1.Name = "tbox_breaking_load_trans_sequence_offset_b_y1";
            this.tbox_breaking_load_trans_sequence_offset_b_y1.Size = new System.Drawing.Size(59, 22);
            this.tbox_breaking_load_trans_sequence_offset_b_y1.TabIndex = 143;
            // 
            // lb_breaking_load_trans_sequence_offset_b_y1_mm
            // 
            this.lb_breaking_load_trans_sequence_offset_b_y1_mm.AutoSize = true;
            this.lb_breaking_load_trans_sequence_offset_b_y1_mm.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_load_trans_sequence_offset_b_y1_mm.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_load_trans_sequence_offset_b_y1_mm.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_load_trans_sequence_offset_b_y1_mm.Location = new System.Drawing.Point(108, 33);
            this.lb_breaking_load_trans_sequence_offset_b_y1_mm.Name = "lb_breaking_load_trans_sequence_offset_b_y1_mm";
            this.lb_breaking_load_trans_sequence_offset_b_y1_mm.Size = new System.Drawing.Size(43, 13);
            this.lb_breaking_load_trans_sequence_offset_b_y1_mm.TabIndex = 147;
            this.lb_breaking_load_trans_sequence_offset_b_y1_mm.Text = "(mm)";
            // 
            // lb_breaking_load_trans_sequence_offset_b_y1
            // 
            this.lb_breaking_load_trans_sequence_offset_b_y1.AutoSize = true;
            this.lb_breaking_load_trans_sequence_offset_b_y1.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_load_trans_sequence_offset_b_y1.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_load_trans_sequence_offset_b_y1.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_load_trans_sequence_offset_b_y1.Location = new System.Drawing.Point(13, 31);
            this.lb_breaking_load_trans_sequence_offset_b_y1.Name = "lb_breaking_load_trans_sequence_offset_b_y1";
            this.lb_breaking_load_trans_sequence_offset_b_y1.Size = new System.Drawing.Size(24, 13);
            this.lb_breaking_load_trans_sequence_offset_b_y1.TabIndex = 144;
            this.lb_breaking_load_trans_sequence_offset_b_y1.Text = "Y1";
            // 
            // tbox_breaking_load_trans_sequence_offset_b_y2
            // 
            this.tbox_breaking_load_trans_sequence_offset_b_y2.Location = new System.Drawing.Point(43, 60);
            this.tbox_breaking_load_trans_sequence_offset_b_y2.Name = "tbox_breaking_load_trans_sequence_offset_b_y2";
            this.tbox_breaking_load_trans_sequence_offset_b_y2.Size = new System.Drawing.Size(59, 22);
            this.tbox_breaking_load_trans_sequence_offset_b_y2.TabIndex = 146;
            // 
            // lb_breaking_load_trans_sequence_offset_b_y2
            // 
            this.lb_breaking_load_trans_sequence_offset_b_y2.AutoSize = true;
            this.lb_breaking_load_trans_sequence_offset_b_y2.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_load_trans_sequence_offset_b_y2.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_load_trans_sequence_offset_b_y2.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_load_trans_sequence_offset_b_y2.Location = new System.Drawing.Point(13, 64);
            this.lb_breaking_load_trans_sequence_offset_b_y2.Name = "lb_breaking_load_trans_sequence_offset_b_y2";
            this.lb_breaking_load_trans_sequence_offset_b_y2.Size = new System.Drawing.Size(24, 13);
            this.lb_breaking_load_trans_sequence_offset_b_y2.TabIndex = 145;
            this.lb_breaking_load_trans_sequence_offset_b_y2.Text = "Y2";
            // 
            // gbox_breaking_load_trans_sequence_offset_a
            // 
            this.gbox_breaking_load_trans_sequence_offset_a.BackColor = System.Drawing.Color.Transparent;
            this.gbox_breaking_load_trans_sequence_offset_a.Controls.Add(this.lb_breaking_load_trans_sequence_offset_a_y2_mm);
            this.gbox_breaking_load_trans_sequence_offset_a.Controls.Add(this.lb_breaking_load_trans_sequence_offset_a_y1_mm);
            this.gbox_breaking_load_trans_sequence_offset_a.Controls.Add(this.tbox_breaking_load_trans_sequence_offset_a_y2);
            this.gbox_breaking_load_trans_sequence_offset_a.Controls.Add(this.tbox_breaking_load_trans_sequence_offset_a_y1);
            this.gbox_breaking_load_trans_sequence_offset_a.Controls.Add(this.lb_breaking_load_trans_sequence_offset_a_y2);
            this.gbox_breaking_load_trans_sequence_offset_a.Controls.Add(this.lb_breaking_load_trans_sequence_offset_a_y1);
            this.gbox_breaking_load_trans_sequence_offset_a.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_breaking_load_trans_sequence_offset_a.ForeColor = System.Drawing.Color.White;
            this.gbox_breaking_load_trans_sequence_offset_a.Location = new System.Drawing.Point(10, 25);
            this.gbox_breaking_load_trans_sequence_offset_a.Name = "gbox_breaking_load_trans_sequence_offset_a";
            this.gbox_breaking_load_trans_sequence_offset_a.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gbox_breaking_load_trans_sequence_offset_a.Size = new System.Drawing.Size(160, 100);
            this.gbox_breaking_load_trans_sequence_offset_a.TabIndex = 138;
            this.gbox_breaking_load_trans_sequence_offset_a.TabStop = false;
            this.gbox_breaking_load_trans_sequence_offset_a.Text = "A";
            // 
            // lb_breaking_load_trans_sequence_offset_a_y2_mm
            // 
            this.lb_breaking_load_trans_sequence_offset_a_y2_mm.AutoSize = true;
            this.lb_breaking_load_trans_sequence_offset_a_y2_mm.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_load_trans_sequence_offset_a_y2_mm.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_load_trans_sequence_offset_a_y2_mm.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_load_trans_sequence_offset_a_y2_mm.Location = new System.Drawing.Point(106, 66);
            this.lb_breaking_load_trans_sequence_offset_a_y2_mm.Name = "lb_breaking_load_trans_sequence_offset_a_y2_mm";
            this.lb_breaking_load_trans_sequence_offset_a_y2_mm.Size = new System.Drawing.Size(43, 13);
            this.lb_breaking_load_trans_sequence_offset_a_y2_mm.TabIndex = 142;
            this.lb_breaking_load_trans_sequence_offset_a_y2_mm.Text = "(mm)";
            // 
            // lb_breaking_load_trans_sequence_offset_a_y1_mm
            // 
            this.lb_breaking_load_trans_sequence_offset_a_y1_mm.AutoSize = true;
            this.lb_breaking_load_trans_sequence_offset_a_y1_mm.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_load_trans_sequence_offset_a_y1_mm.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_load_trans_sequence_offset_a_y1_mm.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_load_trans_sequence_offset_a_y1_mm.Location = new System.Drawing.Point(106, 35);
            this.lb_breaking_load_trans_sequence_offset_a_y1_mm.Name = "lb_breaking_load_trans_sequence_offset_a_y1_mm";
            this.lb_breaking_load_trans_sequence_offset_a_y1_mm.Size = new System.Drawing.Size(43, 13);
            this.lb_breaking_load_trans_sequence_offset_a_y1_mm.TabIndex = 141;
            this.lb_breaking_load_trans_sequence_offset_a_y1_mm.Text = "(mm)";
            // 
            // tbox_breaking_load_trans_sequence_offset_a_y2
            // 
            this.tbox_breaking_load_trans_sequence_offset_a_y2.Location = new System.Drawing.Point(41, 62);
            this.tbox_breaking_load_trans_sequence_offset_a_y2.Name = "tbox_breaking_load_trans_sequence_offset_a_y2";
            this.tbox_breaking_load_trans_sequence_offset_a_y2.Size = new System.Drawing.Size(59, 22);
            this.tbox_breaking_load_trans_sequence_offset_a_y2.TabIndex = 140;
            // 
            // tbox_breaking_load_trans_sequence_offset_a_y1
            // 
            this.tbox_breaking_load_trans_sequence_offset_a_y1.Location = new System.Drawing.Point(41, 31);
            this.tbox_breaking_load_trans_sequence_offset_a_y1.Name = "tbox_breaking_load_trans_sequence_offset_a_y1";
            this.tbox_breaking_load_trans_sequence_offset_a_y1.Size = new System.Drawing.Size(59, 22);
            this.tbox_breaking_load_trans_sequence_offset_a_y1.TabIndex = 138;
            // 
            // lb_breaking_load_trans_sequence_offset_a_y2
            // 
            this.lb_breaking_load_trans_sequence_offset_a_y2.AutoSize = true;
            this.lb_breaking_load_trans_sequence_offset_a_y2.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_load_trans_sequence_offset_a_y2.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_load_trans_sequence_offset_a_y2.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_load_trans_sequence_offset_a_y2.Location = new System.Drawing.Point(11, 66);
            this.lb_breaking_load_trans_sequence_offset_a_y2.Name = "lb_breaking_load_trans_sequence_offset_a_y2";
            this.lb_breaking_load_trans_sequence_offset_a_y2.Size = new System.Drawing.Size(24, 13);
            this.lb_breaking_load_trans_sequence_offset_a_y2.TabIndex = 139;
            this.lb_breaking_load_trans_sequence_offset_a_y2.Text = "Y2";
            // 
            // lb_breaking_load_trans_sequence_offset_a_y1
            // 
            this.lb_breaking_load_trans_sequence_offset_a_y1.AutoSize = true;
            this.lb_breaking_load_trans_sequence_offset_a_y1.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_load_trans_sequence_offset_a_y1.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_load_trans_sequence_offset_a_y1.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_load_trans_sequence_offset_a_y1.Location = new System.Drawing.Point(11, 33);
            this.lb_breaking_load_trans_sequence_offset_a_y1.Name = "lb_breaking_load_trans_sequence_offset_a_y1";
            this.lb_breaking_load_trans_sequence_offset_a_y1.Size = new System.Drawing.Size(24, 13);
            this.lb_breaking_load_trans_sequence_offset_a_y1.TabIndex = 138;
            this.lb_breaking_load_trans_sequence_offset_a_y1.Text = "Y1";
            // 
            // tbox_breaking_slowdown_speed
            // 
            this.tbox_breaking_slowdown_speed.Location = new System.Drawing.Point(658, 199);
            this.tbox_breaking_slowdown_speed.Name = "tbox_breaking_slowdown_speed";
            this.tbox_breaking_slowdown_speed.Size = new System.Drawing.Size(137, 21);
            this.tbox_breaking_slowdown_speed.TabIndex = 136;
            // 
            // lb_breaking_slowdown_speed
            // 
            this.lb_breaking_slowdown_speed.AutoSize = true;
            this.lb_breaking_slowdown_speed.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_slowdown_speed.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_slowdown_speed.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_slowdown_speed.Location = new System.Drawing.Point(516, 201);
            this.lb_breaking_slowdown_speed.Name = "lb_breaking_slowdown_speed";
            this.lb_breaking_slowdown_speed.Size = new System.Drawing.Size(130, 13);
            this.lb_breaking_slowdown_speed.TabIndex = 135;
            this.lb_breaking_slowdown_speed.Text = "Z축 저속 하강 속도";
            // 
            // tbox_breaking_fastdown_speed
            // 
            this.tbox_breaking_fastdown_speed.Location = new System.Drawing.Point(658, 169);
            this.tbox_breaking_fastdown_speed.Name = "tbox_breaking_fastdown_speed";
            this.tbox_breaking_fastdown_speed.Size = new System.Drawing.Size(137, 21);
            this.tbox_breaking_fastdown_speed.TabIndex = 134;
            // 
            // lb_breaking_fastdown_speed
            // 
            this.lb_breaking_fastdown_speed.AutoSize = true;
            this.lb_breaking_fastdown_speed.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_fastdown_speed.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_fastdown_speed.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_fastdown_speed.Location = new System.Drawing.Point(516, 171);
            this.lb_breaking_fastdown_speed.Name = "lb_breaking_fastdown_speed";
            this.lb_breaking_fastdown_speed.Size = new System.Drawing.Size(130, 13);
            this.lb_breaking_fastdown_speed.TabIndex = 133;
            this.lb_breaking_fastdown_speed.Text = "Z축 고속 하강 속도";
            // 
            // lb_breaking_slowdown_ptich_mm
            // 
            this.lb_breaking_slowdown_ptich_mm.AutoSize = true;
            this.lb_breaking_slowdown_ptich_mm.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_slowdown_ptich_mm.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_slowdown_ptich_mm.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_slowdown_ptich_mm.Location = new System.Drawing.Point(441, 201);
            this.lb_breaking_slowdown_ptich_mm.Name = "lb_breaking_slowdown_ptich_mm";
            this.lb_breaking_slowdown_ptich_mm.Size = new System.Drawing.Size(43, 13);
            this.lb_breaking_slowdown_ptich_mm.TabIndex = 132;
            this.lb_breaking_slowdown_ptich_mm.Text = "(mm)";
            // 
            // lb_breaking_fastdown_ptich_mm
            // 
            this.lb_breaking_fastdown_ptich_mm.AutoSize = true;
            this.lb_breaking_fastdown_ptich_mm.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_fastdown_ptich_mm.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_fastdown_ptich_mm.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_fastdown_ptich_mm.Location = new System.Drawing.Point(441, 171);
            this.lb_breaking_fastdown_ptich_mm.Name = "lb_breaking_fastdown_ptich_mm";
            this.lb_breaking_fastdown_ptich_mm.Size = new System.Drawing.Size(43, 13);
            this.lb_breaking_fastdown_ptich_mm.TabIndex = 131;
            this.lb_breaking_fastdown_ptich_mm.Text = "(mm)";
            // 
            // lb_breaking_slowdown_ptich
            // 
            this.lb_breaking_slowdown_ptich.AutoSize = true;
            this.lb_breaking_slowdown_ptich.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_slowdown_ptich.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_slowdown_ptich.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_slowdown_ptich.Location = new System.Drawing.Point(143, 201);
            this.lb_breaking_slowdown_ptich.Name = "lb_breaking_slowdown_ptich";
            this.lb_breaking_slowdown_ptich.Size = new System.Drawing.Size(132, 13);
            this.lb_breaking_slowdown_ptich.TabIndex = 130;
            this.lb_breaking_slowdown_ptich.Text = "Z축 저속하강 pitch";
            // 
            // tbox_breaking_slowdown_ptich
            // 
            this.tbox_breaking_slowdown_ptich.Location = new System.Drawing.Point(301, 199);
            this.tbox_breaking_slowdown_ptich.Name = "tbox_breaking_slowdown_ptich";
            this.tbox_breaking_slowdown_ptich.Size = new System.Drawing.Size(137, 21);
            this.tbox_breaking_slowdown_ptich.TabIndex = 129;
            // 
            // tbox_breaking_fastdown_ptich
            // 
            this.tbox_breaking_fastdown_ptich.Location = new System.Drawing.Point(301, 169);
            this.tbox_breaking_fastdown_ptich.Name = "tbox_breaking_fastdown_ptich";
            this.tbox_breaking_fastdown_ptich.Size = new System.Drawing.Size(137, 21);
            this.tbox_breaking_fastdown_ptich.TabIndex = 127;
            // 
            // lb_breaking_fastdown_ptich
            // 
            this.lb_breaking_fastdown_ptich.AutoSize = true;
            this.lb_breaking_fastdown_ptich.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_fastdown_ptich.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_fastdown_ptich.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_fastdown_ptich.Location = new System.Drawing.Point(143, 171);
            this.lb_breaking_fastdown_ptich.Name = "lb_breaking_fastdown_ptich";
            this.lb_breaking_fastdown_ptich.Size = new System.Drawing.Size(132, 13);
            this.lb_breaking_fastdown_ptich.TabIndex = 126;
            this.lb_breaking_fastdown_ptich.Text = "Z축 고속하강 pitch";
            // 
            // panel_breaking_2pass
            // 
            this.panel_breaking_2pass.BackColor = System.Drawing.Color.Aquamarine;
            this.panel_breaking_2pass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_breaking_2pass.Location = new System.Drawing.Point(1683, 99);
            this.panel_breaking_2pass.Name = "panel_breaking_2pass";
            this.panel_breaking_2pass.Size = new System.Drawing.Size(30, 30);
            this.panel_breaking_2pass.TabIndex = 74;
            // 
            // lb_breaking_2pass
            // 
            this.lb_breaking_2pass.AutoSize = true;
            this.lb_breaking_2pass.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_2pass.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_2pass.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_2pass.Location = new System.Drawing.Point(1595, 108);
            this.lb_breaking_2pass.Name = "lb_breaking_2pass";
            this.lb_breaking_2pass.Size = new System.Drawing.Size(82, 13);
            this.lb_breaking_2pass.TabIndex = 125;
            this.lb_breaking_2pass.Text = "상하부 가공";
            // 
            // tbox_breaking_b_mcr_offset_xy_2
            // 
            this.tbox_breaking_b_mcr_offset_xy_2.Location = new System.Drawing.Point(1623, 57);
            this.tbox_breaking_b_mcr_offset_xy_2.Name = "tbox_breaking_b_mcr_offset_xy_2";
            this.tbox_breaking_b_mcr_offset_xy_2.Size = new System.Drawing.Size(90, 21);
            this.tbox_breaking_b_mcr_offset_xy_2.TabIndex = 124;
            // 
            // tbox_breaking_a_mcr_offset_xy_2
            // 
            this.tbox_breaking_a_mcr_offset_xy_2.Location = new System.Drawing.Point(1623, 30);
            this.tbox_breaking_a_mcr_offset_xy_2.Name = "tbox_breaking_a_mcr_offset_xy_2";
            this.tbox_breaking_a_mcr_offset_xy_2.Size = new System.Drawing.Size(90, 21);
            this.tbox_breaking_a_mcr_offset_xy_2.TabIndex = 123;
            // 
            // tbox_breaking_b_mcr_offset_xy_1
            // 
            this.tbox_breaking_b_mcr_offset_xy_1.Location = new System.Drawing.Point(1527, 57);
            this.tbox_breaking_b_mcr_offset_xy_1.Name = "tbox_breaking_b_mcr_offset_xy_1";
            this.tbox_breaking_b_mcr_offset_xy_1.Size = new System.Drawing.Size(90, 21);
            this.tbox_breaking_b_mcr_offset_xy_1.TabIndex = 122;
            // 
            // lb_breaking_b_mcr_offset_xy
            // 
            this.lb_breaking_b_mcr_offset_xy.AutoSize = true;
            this.lb_breaking_b_mcr_offset_xy.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_b_mcr_offset_xy.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_b_mcr_offset_xy.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_b_mcr_offset_xy.Location = new System.Drawing.Point(1354, 61);
            this.lb_breaking_b_mcr_offset_xy.Name = "lb_breaking_b_mcr_offset_xy";
            this.lb_breaking_b_mcr_offset_xy.Size = new System.Drawing.Size(177, 13);
            this.lb_breaking_b_mcr_offset_xy.TabIndex = 121;
            this.lb_breaking_b_mcr_offset_xy.Text = "B열 MCR Offset XY[mm]";
            // 
            // tbox_breaking_a_mcr_offset_xy_1
            // 
            this.tbox_breaking_a_mcr_offset_xy_1.Location = new System.Drawing.Point(1527, 30);
            this.tbox_breaking_a_mcr_offset_xy_1.Name = "tbox_breaking_a_mcr_offset_xy_1";
            this.tbox_breaking_a_mcr_offset_xy_1.Size = new System.Drawing.Size(90, 21);
            this.tbox_breaking_a_mcr_offset_xy_1.TabIndex = 119;
            // 
            // lb_breaking_a_mcr_offset_xy
            // 
            this.lb_breaking_a_mcr_offset_xy.AutoSize = true;
            this.lb_breaking_a_mcr_offset_xy.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_a_mcr_offset_xy.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_a_mcr_offset_xy.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_a_mcr_offset_xy.Location = new System.Drawing.Point(1354, 34);
            this.lb_breaking_a_mcr_offset_xy.Name = "lb_breaking_a_mcr_offset_xy";
            this.lb_breaking_a_mcr_offset_xy.Size = new System.Drawing.Size(176, 13);
            this.lb_breaking_a_mcr_offset_xy.TabIndex = 118;
            this.lb_breaking_a_mcr_offset_xy.Text = "A열 MCR Offset XY[mm]";
            // 
            // tbox_breaking_b2_xy_offset_2
            // 
            this.tbox_breaking_b2_xy_offset_2.Location = new System.Drawing.Point(1289, 111);
            this.tbox_breaking_b2_xy_offset_2.Name = "tbox_breaking_b2_xy_offset_2";
            this.tbox_breaking_b2_xy_offset_2.Size = new System.Drawing.Size(59, 21);
            this.tbox_breaking_b2_xy_offset_2.TabIndex = 117;
            // 
            // tbox_breaking_b2_xy_offset_1
            // 
            this.tbox_breaking_b2_xy_offset_1.Location = new System.Drawing.Point(1224, 111);
            this.tbox_breaking_b2_xy_offset_1.Name = "tbox_breaking_b2_xy_offset_1";
            this.tbox_breaking_b2_xy_offset_1.Size = new System.Drawing.Size(59, 21);
            this.tbox_breaking_b2_xy_offset_1.TabIndex = 116;
            // 
            // lb_breaking_b2_xy_offset
            // 
            this.lb_breaking_b2_xy_offset.AutoSize = true;
            this.lb_breaking_b2_xy_offset.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_b2_xy_offset.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_b2_xy_offset.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_b2_xy_offset.Location = new System.Drawing.Point(1079, 115);
            this.lb_breaking_b2_xy_offset.Name = "lb_breaking_b2_xy_offset";
            this.lb_breaking_b2_xy_offset.Size = new System.Drawing.Size(146, 13);
            this.lb_breaking_b2_xy_offset.TabIndex = 115;
            this.lb_breaking_b2_xy_offset.Text = "B열2 XY offset[mm]";
            // 
            // tbox_breaking_b1_xy_offset_2
            // 
            this.tbox_breaking_b1_xy_offset_2.Location = new System.Drawing.Point(1289, 84);
            this.tbox_breaking_b1_xy_offset_2.Name = "tbox_breaking_b1_xy_offset_2";
            this.tbox_breaking_b1_xy_offset_2.Size = new System.Drawing.Size(59, 21);
            this.tbox_breaking_b1_xy_offset_2.TabIndex = 114;
            // 
            // tbox_breaking_b1_xy_offset_1
            // 
            this.tbox_breaking_b1_xy_offset_1.Location = new System.Drawing.Point(1224, 84);
            this.tbox_breaking_b1_xy_offset_1.Name = "tbox_breaking_b1_xy_offset_1";
            this.tbox_breaking_b1_xy_offset_1.Size = new System.Drawing.Size(59, 21);
            this.tbox_breaking_b1_xy_offset_1.TabIndex = 113;
            // 
            // lb_breaking_b1_xy_offset
            // 
            this.lb_breaking_b1_xy_offset.AutoSize = true;
            this.lb_breaking_b1_xy_offset.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_b1_xy_offset.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_b1_xy_offset.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_b1_xy_offset.Location = new System.Drawing.Point(1079, 88);
            this.lb_breaking_b1_xy_offset.Name = "lb_breaking_b1_xy_offset";
            this.lb_breaking_b1_xy_offset.Size = new System.Drawing.Size(146, 13);
            this.lb_breaking_b1_xy_offset.TabIndex = 112;
            this.lb_breaking_b1_xy_offset.Text = "B열1 XY offset[mm]";
            // 
            // tbox_breaking_a2_xy_offset_2
            // 
            this.tbox_breaking_a2_xy_offset_2.Location = new System.Drawing.Point(1289, 57);
            this.tbox_breaking_a2_xy_offset_2.Name = "tbox_breaking_a2_xy_offset_2";
            this.tbox_breaking_a2_xy_offset_2.Size = new System.Drawing.Size(59, 21);
            this.tbox_breaking_a2_xy_offset_2.TabIndex = 111;
            // 
            // tbox_breaking_a2_xy_offset_1
            // 
            this.tbox_breaking_a2_xy_offset_1.Location = new System.Drawing.Point(1224, 57);
            this.tbox_breaking_a2_xy_offset_1.Name = "tbox_breaking_a2_xy_offset_1";
            this.tbox_breaking_a2_xy_offset_1.Size = new System.Drawing.Size(59, 21);
            this.tbox_breaking_a2_xy_offset_1.TabIndex = 110;
            // 
            // lb_breaking_a2_xy_offset
            // 
            this.lb_breaking_a2_xy_offset.AutoSize = true;
            this.lb_breaking_a2_xy_offset.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_a2_xy_offset.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_a2_xy_offset.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_a2_xy_offset.Location = new System.Drawing.Point(1079, 61);
            this.lb_breaking_a2_xy_offset.Name = "lb_breaking_a2_xy_offset";
            this.lb_breaking_a2_xy_offset.Size = new System.Drawing.Size(145, 13);
            this.lb_breaking_a2_xy_offset.TabIndex = 109;
            this.lb_breaking_a2_xy_offset.Text = "A열2 XY offset[mm]";
            // 
            // tbox_breaking_a1_xy_offset_2
            // 
            this.tbox_breaking_a1_xy_offset_2.Location = new System.Drawing.Point(1289, 30);
            this.tbox_breaking_a1_xy_offset_2.Name = "tbox_breaking_a1_xy_offset_2";
            this.tbox_breaking_a1_xy_offset_2.Size = new System.Drawing.Size(59, 21);
            this.tbox_breaking_a1_xy_offset_2.TabIndex = 108;
            // 
            // tbox_breaking_a1_xy_offset_1
            // 
            this.tbox_breaking_a1_xy_offset_1.Location = new System.Drawing.Point(1224, 30);
            this.tbox_breaking_a1_xy_offset_1.Name = "tbox_breaking_a1_xy_offset_1";
            this.tbox_breaking_a1_xy_offset_1.Size = new System.Drawing.Size(59, 21);
            this.tbox_breaking_a1_xy_offset_1.TabIndex = 107;
            // 
            // lb_breaking_a1_xy_offset
            // 
            this.lb_breaking_a1_xy_offset.AutoSize = true;
            this.lb_breaking_a1_xy_offset.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_a1_xy_offset.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_a1_xy_offset.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_a1_xy_offset.Location = new System.Drawing.Point(1079, 34);
            this.lb_breaking_a1_xy_offset.Name = "lb_breaking_a1_xy_offset";
            this.lb_breaking_a1_xy_offset.Size = new System.Drawing.Size(145, 13);
            this.lb_breaking_a1_xy_offset.TabIndex = 106;
            this.lb_breaking_a1_xy_offset.Text = "A열1 XY offset[mm]";
            // 
            // panel_breaking_xyt_breaking4_calc
            // 
            this.panel_breaking_xyt_breaking4_calc.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_breaking_xyt_breaking4_calc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_breaking_xyt_breaking4_calc.Controls.Add(this.lb_breaking_xyt_breaking4_calc);
            this.panel_breaking_xyt_breaking4_calc.Location = new System.Drawing.Point(1019, 111);
            this.panel_breaking_xyt_breaking4_calc.Name = "panel_breaking_xyt_breaking4_calc";
            this.panel_breaking_xyt_breaking4_calc.Size = new System.Drawing.Size(54, 21);
            this.panel_breaking_xyt_breaking4_calc.TabIndex = 105;
            // 
            // lb_breaking_xyt_breaking4_calc
            // 
            this.lb_breaking_xyt_breaking4_calc.AutoSize = true;
            this.lb_breaking_xyt_breaking4_calc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_xyt_breaking4_calc.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_xyt_breaking4_calc.Location = new System.Drawing.Point(10, 2);
            this.lb_breaking_xyt_breaking4_calc.Name = "lb_breaking_xyt_breaking4_calc";
            this.lb_breaking_xyt_breaking4_calc.Size = new System.Drawing.Size(39, 16);
            this.lb_breaking_xyt_breaking4_calc.TabIndex = 0;
            this.lb_breaking_xyt_breaking4_calc.Text = "Calc";
            // 
            // tbox_breaking_xyt_breaking4_3
            // 
            this.tbox_breaking_xyt_breaking4_3.Location = new System.Drawing.Point(930, 111);
            this.tbox_breaking_xyt_breaking4_3.Name = "tbox_breaking_xyt_breaking4_3";
            this.tbox_breaking_xyt_breaking4_3.Size = new System.Drawing.Size(83, 21);
            this.tbox_breaking_xyt_breaking4_3.TabIndex = 104;
            // 
            // tbox_breaking_xyt_breaking4_2
            // 
            this.tbox_breaking_xyt_breaking4_2.Location = new System.Drawing.Point(841, 111);
            this.tbox_breaking_xyt_breaking4_2.Name = "tbox_breaking_xyt_breaking4_2";
            this.tbox_breaking_xyt_breaking4_2.Size = new System.Drawing.Size(83, 21);
            this.tbox_breaking_xyt_breaking4_2.TabIndex = 103;
            // 
            // tbox_breaking_xyt_breaking4_1
            // 
            this.tbox_breaking_xyt_breaking4_1.Location = new System.Drawing.Point(752, 111);
            this.tbox_breaking_xyt_breaking4_1.Name = "tbox_breaking_xyt_breaking4_1";
            this.tbox_breaking_xyt_breaking4_1.Size = new System.Drawing.Size(83, 21);
            this.tbox_breaking_xyt_breaking4_1.TabIndex = 102;
            // 
            // lb_breaking_xyt_breaking4
            // 
            this.lb_breaking_xyt_breaking4.AutoSize = true;
            this.lb_breaking_xyt_breaking4.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_xyt_breaking4.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_xyt_breaking4.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_xyt_breaking4.Location = new System.Drawing.Point(596, 116);
            this.lb_breaking_xyt_breaking4.Name = "lb_breaking_xyt_breaking4";
            this.lb_breaking_xyt_breaking4.Size = new System.Drawing.Size(150, 13);
            this.lb_breaking_xyt_breaking4.TabIndex = 101;
            this.lb_breaking_xyt_breaking4.Text = "XYT Breaking4[mm]";
            // 
            // panel_breaking_xyt_breaking3_calc
            // 
            this.panel_breaking_xyt_breaking3_calc.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_breaking_xyt_breaking3_calc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_breaking_xyt_breaking3_calc.Controls.Add(this.lb_breaking_xyt_breaking3_calc);
            this.panel_breaking_xyt_breaking3_calc.Location = new System.Drawing.Point(1019, 84);
            this.panel_breaking_xyt_breaking3_calc.Name = "panel_breaking_xyt_breaking3_calc";
            this.panel_breaking_xyt_breaking3_calc.Size = new System.Drawing.Size(54, 21);
            this.panel_breaking_xyt_breaking3_calc.TabIndex = 100;
            // 
            // lb_breaking_xyt_breaking3_calc
            // 
            this.lb_breaking_xyt_breaking3_calc.AutoSize = true;
            this.lb_breaking_xyt_breaking3_calc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_xyt_breaking3_calc.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_xyt_breaking3_calc.Location = new System.Drawing.Point(10, 2);
            this.lb_breaking_xyt_breaking3_calc.Name = "lb_breaking_xyt_breaking3_calc";
            this.lb_breaking_xyt_breaking3_calc.Size = new System.Drawing.Size(39, 16);
            this.lb_breaking_xyt_breaking3_calc.TabIndex = 0;
            this.lb_breaking_xyt_breaking3_calc.Text = "Calc";
            // 
            // tbox_breaking_xyt_breaking3_3
            // 
            this.tbox_breaking_xyt_breaking3_3.Location = new System.Drawing.Point(930, 84);
            this.tbox_breaking_xyt_breaking3_3.Name = "tbox_breaking_xyt_breaking3_3";
            this.tbox_breaking_xyt_breaking3_3.Size = new System.Drawing.Size(83, 21);
            this.tbox_breaking_xyt_breaking3_3.TabIndex = 99;
            // 
            // tbox_breaking_xyt_breaking3_2
            // 
            this.tbox_breaking_xyt_breaking3_2.Location = new System.Drawing.Point(841, 84);
            this.tbox_breaking_xyt_breaking3_2.Name = "tbox_breaking_xyt_breaking3_2";
            this.tbox_breaking_xyt_breaking3_2.Size = new System.Drawing.Size(83, 21);
            this.tbox_breaking_xyt_breaking3_2.TabIndex = 98;
            // 
            // tbox_breaking_xyt_breaking3_1
            // 
            this.tbox_breaking_xyt_breaking3_1.Location = new System.Drawing.Point(752, 84);
            this.tbox_breaking_xyt_breaking3_1.Name = "tbox_breaking_xyt_breaking3_1";
            this.tbox_breaking_xyt_breaking3_1.Size = new System.Drawing.Size(83, 21);
            this.tbox_breaking_xyt_breaking3_1.TabIndex = 97;
            // 
            // lb_breaking_xyt_breaking3
            // 
            this.lb_breaking_xyt_breaking3.AutoSize = true;
            this.lb_breaking_xyt_breaking3.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_xyt_breaking3.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_xyt_breaking3.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_xyt_breaking3.Location = new System.Drawing.Point(596, 89);
            this.lb_breaking_xyt_breaking3.Name = "lb_breaking_xyt_breaking3";
            this.lb_breaking_xyt_breaking3.Size = new System.Drawing.Size(150, 13);
            this.lb_breaking_xyt_breaking3.TabIndex = 96;
            this.lb_breaking_xyt_breaking3.Text = "XYT Breaking3[mm]";
            // 
            // panel_breaking_xyt_breaking2_calc
            // 
            this.panel_breaking_xyt_breaking2_calc.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_breaking_xyt_breaking2_calc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_breaking_xyt_breaking2_calc.Controls.Add(this.lb_breaking_xyt_breaking2_calc);
            this.panel_breaking_xyt_breaking2_calc.Location = new System.Drawing.Point(1019, 57);
            this.panel_breaking_xyt_breaking2_calc.Name = "panel_breaking_xyt_breaking2_calc";
            this.panel_breaking_xyt_breaking2_calc.Size = new System.Drawing.Size(54, 21);
            this.panel_breaking_xyt_breaking2_calc.TabIndex = 95;
            // 
            // lb_breaking_xyt_breaking2_calc
            // 
            this.lb_breaking_xyt_breaking2_calc.AutoSize = true;
            this.lb_breaking_xyt_breaking2_calc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_xyt_breaking2_calc.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_xyt_breaking2_calc.Location = new System.Drawing.Point(10, 2);
            this.lb_breaking_xyt_breaking2_calc.Name = "lb_breaking_xyt_breaking2_calc";
            this.lb_breaking_xyt_breaking2_calc.Size = new System.Drawing.Size(39, 16);
            this.lb_breaking_xyt_breaking2_calc.TabIndex = 0;
            this.lb_breaking_xyt_breaking2_calc.Text = "Calc";
            // 
            // tbox_breaking_xyt_breaking2_3
            // 
            this.tbox_breaking_xyt_breaking2_3.Location = new System.Drawing.Point(930, 57);
            this.tbox_breaking_xyt_breaking2_3.Name = "tbox_breaking_xyt_breaking2_3";
            this.tbox_breaking_xyt_breaking2_3.Size = new System.Drawing.Size(83, 21);
            this.tbox_breaking_xyt_breaking2_3.TabIndex = 94;
            // 
            // panel_breaking_xyt_breaking1_calc
            // 
            this.panel_breaking_xyt_breaking1_calc.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel_breaking_xyt_breaking1_calc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_breaking_xyt_breaking1_calc.Controls.Add(this.label49);
            this.panel_breaking_xyt_breaking1_calc.Location = new System.Drawing.Point(1019, 30);
            this.panel_breaking_xyt_breaking1_calc.Name = "panel_breaking_xyt_breaking1_calc";
            this.panel_breaking_xyt_breaking1_calc.Size = new System.Drawing.Size(54, 21);
            this.panel_breaking_xyt_breaking1_calc.TabIndex = 90;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label49.ForeColor = System.Drawing.Color.White;
            this.label49.Location = new System.Drawing.Point(10, 2);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(39, 16);
            this.label49.TabIndex = 0;
            this.label49.Text = "Calc";
            // 
            // tbox_breaking_xyt_breaking2_2
            // 
            this.tbox_breaking_xyt_breaking2_2.Location = new System.Drawing.Point(841, 57);
            this.tbox_breaking_xyt_breaking2_2.Name = "tbox_breaking_xyt_breaking2_2";
            this.tbox_breaking_xyt_breaking2_2.Size = new System.Drawing.Size(83, 21);
            this.tbox_breaking_xyt_breaking2_2.TabIndex = 93;
            // 
            // panel_breaking_half
            // 
            this.panel_breaking_half.BackColor = System.Drawing.Color.Aquamarine;
            this.panel_breaking_half.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_breaking_half.Controls.Add(this.lb_breaking_half);
            this.panel_breaking_half.Location = new System.Drawing.Point(970, 5);
            this.panel_breaking_half.Name = "panel_breaking_half";
            this.panel_breaking_half.Size = new System.Drawing.Size(103, 21);
            this.panel_breaking_half.TabIndex = 73;
            // 
            // lb_breaking_half
            // 
            this.lb_breaking_half.AutoSize = true;
            this.lb_breaking_half.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_half.Location = new System.Drawing.Point(9, 0);
            this.lb_breaking_half.Name = "lb_breaking_half";
            this.lb_breaking_half.Size = new System.Drawing.Size(38, 18);
            this.lb_breaking_half.TabIndex = 0;
            this.lb_breaking_half.Text = "Half";
            // 
            // tbox_breaking_xyt_breaking2_1
            // 
            this.tbox_breaking_xyt_breaking2_1.Location = new System.Drawing.Point(752, 57);
            this.tbox_breaking_xyt_breaking2_1.Name = "tbox_breaking_xyt_breaking2_1";
            this.tbox_breaking_xyt_breaking2_1.Size = new System.Drawing.Size(83, 21);
            this.tbox_breaking_xyt_breaking2_1.TabIndex = 92;
            // 
            // panel_breaking_right
            // 
            this.panel_breaking_right.BackColor = System.Drawing.Color.Aquamarine;
            this.panel_breaking_right.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_breaking_right.Controls.Add(this.lb_breaking_right);
            this.panel_breaking_right.Location = new System.Drawing.Point(861, 5);
            this.panel_breaking_right.Name = "panel_breaking_right";
            this.panel_breaking_right.Size = new System.Drawing.Size(103, 21);
            this.panel_breaking_right.TabIndex = 72;
            // 
            // lb_breaking_right
            // 
            this.lb_breaking_right.AutoSize = true;
            this.lb_breaking_right.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_right.Location = new System.Drawing.Point(9, 0);
            this.lb_breaking_right.Name = "lb_breaking_right";
            this.lb_breaking_right.Size = new System.Drawing.Size(47, 18);
            this.lb_breaking_right.TabIndex = 0;
            this.lb_breaking_right.Text = "Right";
            // 
            // lb_breaking_xyt_breaking2
            // 
            this.lb_breaking_xyt_breaking2.AutoSize = true;
            this.lb_breaking_xyt_breaking2.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_xyt_breaking2.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_xyt_breaking2.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_xyt_breaking2.Location = new System.Drawing.Point(596, 62);
            this.lb_breaking_xyt_breaking2.Name = "lb_breaking_xyt_breaking2";
            this.lb_breaking_xyt_breaking2.Size = new System.Drawing.Size(150, 13);
            this.lb_breaking_xyt_breaking2.TabIndex = 91;
            this.lb_breaking_xyt_breaking2.Text = "XYT Breaking2[mm]";
            // 
            // tbox_breaking_xyt_breaking1_3
            // 
            this.tbox_breaking_xyt_breaking1_3.Location = new System.Drawing.Point(930, 30);
            this.tbox_breaking_xyt_breaking1_3.Name = "tbox_breaking_xyt_breaking1_3";
            this.tbox_breaking_xyt_breaking1_3.Size = new System.Drawing.Size(83, 21);
            this.tbox_breaking_xyt_breaking1_3.TabIndex = 89;
            // 
            // tbox_breaking_xyt_breaking1_2
            // 
            this.tbox_breaking_xyt_breaking1_2.Location = new System.Drawing.Point(841, 30);
            this.tbox_breaking_xyt_breaking1_2.Name = "tbox_breaking_xyt_breaking1_2";
            this.tbox_breaking_xyt_breaking1_2.Size = new System.Drawing.Size(83, 21);
            this.tbox_breaking_xyt_breaking1_2.TabIndex = 88;
            // 
            // tbox_breaking_xyt_breaking1_1
            // 
            this.tbox_breaking_xyt_breaking1_1.Location = new System.Drawing.Point(752, 30);
            this.tbox_breaking_xyt_breaking1_1.Name = "tbox_breaking_xyt_breaking1_1";
            this.tbox_breaking_xyt_breaking1_1.Size = new System.Drawing.Size(83, 21);
            this.tbox_breaking_xyt_breaking1_1.TabIndex = 87;
            // 
            // panel_breaking_left
            // 
            this.panel_breaking_left.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.panel_breaking_left.BackColor = System.Drawing.Color.Aquamarine;
            this.panel_breaking_left.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_breaking_left.Controls.Add(this.lb_breaking_left);
            this.panel_breaking_left.Location = new System.Drawing.Point(752, 5);
            this.panel_breaking_left.Name = "panel_breaking_left";
            this.panel_breaking_left.Size = new System.Drawing.Size(103, 21);
            this.panel_breaking_left.TabIndex = 71;
            // 
            // lb_breaking_left
            // 
            this.lb_breaking_left.AutoSize = true;
            this.lb_breaking_left.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_left.Location = new System.Drawing.Point(9, 0);
            this.lb_breaking_left.Name = "lb_breaking_left";
            this.lb_breaking_left.Size = new System.Drawing.Size(36, 18);
            this.lb_breaking_left.TabIndex = 0;
            this.lb_breaking_left.Text = "Left";
            // 
            // tbox_breaking_xy_mcr_2
            // 
            this.tbox_breaking_xy_mcr_2.Location = new System.Drawing.Point(444, 84);
            this.tbox_breaking_xy_mcr_2.Name = "tbox_breaking_xy_mcr_2";
            this.tbox_breaking_xy_mcr_2.Size = new System.Drawing.Size(137, 21);
            this.tbox_breaking_xy_mcr_2.TabIndex = 86;
            // 
            // tbox_breaking_xy_align_mark2_2
            // 
            this.tbox_breaking_xy_align_mark2_2.Location = new System.Drawing.Point(444, 57);
            this.tbox_breaking_xy_align_mark2_2.Name = "tbox_breaking_xy_align_mark2_2";
            this.tbox_breaking_xy_align_mark2_2.Size = new System.Drawing.Size(137, 21);
            this.tbox_breaking_xy_align_mark2_2.TabIndex = 85;
            // 
            // tbox_breaking_xy_align_mark1_2
            // 
            this.tbox_breaking_xy_align_mark1_2.Location = new System.Drawing.Point(444, 30);
            this.tbox_breaking_xy_align_mark1_2.Name = "tbox_breaking_xy_align_mark1_2";
            this.tbox_breaking_xy_align_mark1_2.Size = new System.Drawing.Size(137, 21);
            this.tbox_breaking_xy_align_mark1_2.TabIndex = 84;
            // 
            // tbox_breaking_xy_pin_center_2
            // 
            this.tbox_breaking_xy_pin_center_2.Location = new System.Drawing.Point(444, 4);
            this.tbox_breaking_xy_pin_center_2.Name = "tbox_breaking_xy_pin_center_2";
            this.tbox_breaking_xy_pin_center_2.Size = new System.Drawing.Size(137, 21);
            this.tbox_breaking_xy_pin_center_2.TabIndex = 83;
            // 
            // lb_breaking_xyt_breaking1
            // 
            this.lb_breaking_xyt_breaking1.AutoSize = true;
            this.lb_breaking_xyt_breaking1.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_xyt_breaking1.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_xyt_breaking1.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_xyt_breaking1.Location = new System.Drawing.Point(596, 35);
            this.lb_breaking_xyt_breaking1.Name = "lb_breaking_xyt_breaking1";
            this.lb_breaking_xyt_breaking1.Size = new System.Drawing.Size(150, 13);
            this.lb_breaking_xyt_breaking1.TabIndex = 70;
            this.lb_breaking_xyt_breaking1.Text = "XYT Breaking1[mm]";
            // 
            // tbox_breaking_pin_distance_b
            // 
            this.tbox_breaking_pin_distance_b.Location = new System.Drawing.Point(520, 111);
            this.tbox_breaking_pin_distance_b.Name = "tbox_breaking_pin_distance_b";
            this.tbox_breaking_pin_distance_b.Size = new System.Drawing.Size(61, 21);
            this.tbox_breaking_pin_distance_b.TabIndex = 82;
            // 
            // lb_breaking_pin_distance_b
            // 
            this.lb_breaking_pin_distance_b.AutoSize = true;
            this.lb_breaking_pin_distance_b.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_pin_distance_b.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_pin_distance_b.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_pin_distance_b.Location = new System.Drawing.Point(368, 115);
            this.lb_breaking_pin_distance_b.Name = "lb_breaking_pin_distance_b";
            this.lb_breaking_pin_distance_b.Size = new System.Drawing.Size(153, 13);
            this.lb_breaking_pin_distance_b.TabIndex = 81;
            this.lb_breaking_pin_distance_b.Text = "Pin Distance B[mm]";
            // 
            // tbox_breaking_pin_distance_a
            // 
            this.tbox_breaking_pin_distance_a.Location = new System.Drawing.Point(301, 111);
            this.tbox_breaking_pin_distance_a.Name = "tbox_breaking_pin_distance_a";
            this.tbox_breaking_pin_distance_a.Size = new System.Drawing.Size(61, 21);
            this.tbox_breaking_pin_distance_a.TabIndex = 71;
            // 
            // lb_breaking_pin_distance_a
            // 
            this.lb_breaking_pin_distance_a.AutoSize = true;
            this.lb_breaking_pin_distance_a.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_pin_distance_a.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_pin_distance_a.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_pin_distance_a.Location = new System.Drawing.Point(143, 115);
            this.lb_breaking_pin_distance_a.Name = "lb_breaking_pin_distance_a";
            this.lb_breaking_pin_distance_a.Size = new System.Drawing.Size(152, 13);
            this.lb_breaking_pin_distance_a.TabIndex = 70;
            this.lb_breaking_pin_distance_a.Text = "Pin Distance A[mm]";
            // 
            // tbox_breaking_xy_mcr_1
            // 
            this.tbox_breaking_xy_mcr_1.Location = new System.Drawing.Point(301, 84);
            this.tbox_breaking_xy_mcr_1.Name = "tbox_breaking_xy_mcr_1";
            this.tbox_breaking_xy_mcr_1.Size = new System.Drawing.Size(137, 21);
            this.tbox_breaking_xy_mcr_1.TabIndex = 78;
            // 
            // panel_breaking_xy_mcr
            // 
            this.panel_breaking_xy_mcr.BackColor = System.Drawing.Color.Red;
            this.panel_breaking_xy_mcr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_breaking_xy_mcr.Controls.Add(this.lb_breaking_xy_mcr);
            this.panel_breaking_xy_mcr.Location = new System.Drawing.Point(139, 84);
            this.panel_breaking_xy_mcr.Name = "panel_breaking_xy_mcr";
            this.panel_breaking_xy_mcr.Size = new System.Drawing.Size(156, 21);
            this.panel_breaking_xy_mcr.TabIndex = 79;
            // 
            // lb_breaking_xy_mcr
            // 
            this.lb_breaking_xy_mcr.AutoSize = true;
            this.lb_breaking_xy_mcr.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_xy_mcr.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_xy_mcr.Location = new System.Drawing.Point(25, 0);
            this.lb_breaking_xy_mcr.Name = "lb_breaking_xy_mcr";
            this.lb_breaking_xy_mcr.Size = new System.Drawing.Size(110, 18);
            this.lb_breaking_xy_mcr.TabIndex = 0;
            this.lb_breaking_xy_mcr.Text = "XY MCR[mm]";
            // 
            // tbox_breaking_xy_align_mark2_1
            // 
            this.tbox_breaking_xy_align_mark2_1.Location = new System.Drawing.Point(301, 57);
            this.tbox_breaking_xy_align_mark2_1.Name = "tbox_breaking_xy_align_mark2_1";
            this.tbox_breaking_xy_align_mark2_1.Size = new System.Drawing.Size(137, 21);
            this.tbox_breaking_xy_align_mark2_1.TabIndex = 75;
            // 
            // panel_breaking_xy_align_mark2
            // 
            this.panel_breaking_xy_align_mark2.BackColor = System.Drawing.Color.Blue;
            this.panel_breaking_xy_align_mark2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_breaking_xy_align_mark2.Controls.Add(this.lb_breaking_xy_align_mark2);
            this.panel_breaking_xy_align_mark2.Location = new System.Drawing.Point(139, 57);
            this.panel_breaking_xy_align_mark2.Name = "panel_breaking_xy_align_mark2";
            this.panel_breaking_xy_align_mark2.Size = new System.Drawing.Size(156, 21);
            this.panel_breaking_xy_align_mark2.TabIndex = 76;
            // 
            // lb_breaking_xy_align_mark2
            // 
            this.lb_breaking_xy_align_mark2.AutoSize = true;
            this.lb_breaking_xy_align_mark2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_xy_align_mark2.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_xy_align_mark2.Location = new System.Drawing.Point(-3, 0);
            this.lb_breaking_xy_align_mark2.Name = "lb_breaking_xy_align_mark2";
            this.lb_breaking_xy_align_mark2.Size = new System.Drawing.Size(160, 18);
            this.lb_breaking_xy_align_mark2.TabIndex = 0;
            this.lb_breaking_xy_align_mark2.Text = "XY Align Mark2[mm]";
            // 
            // tbox_breaking_xy_align_mark1_1
            // 
            this.tbox_breaking_xy_align_mark1_1.Location = new System.Drawing.Point(301, 30);
            this.tbox_breaking_xy_align_mark1_1.Name = "tbox_breaking_xy_align_mark1_1";
            this.tbox_breaking_xy_align_mark1_1.Size = new System.Drawing.Size(137, 21);
            this.tbox_breaking_xy_align_mark1_1.TabIndex = 72;
            // 
            // panel_breaking_xy_align_mark1
            // 
            this.panel_breaking_xy_align_mark1.BackColor = System.Drawing.Color.LimeGreen;
            this.panel_breaking_xy_align_mark1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_breaking_xy_align_mark1.Controls.Add(this.lb_breaking_xy_align_mark1);
            this.panel_breaking_xy_align_mark1.Location = new System.Drawing.Point(139, 30);
            this.panel_breaking_xy_align_mark1.Name = "panel_breaking_xy_align_mark1";
            this.panel_breaking_xy_align_mark1.Size = new System.Drawing.Size(156, 21);
            this.panel_breaking_xy_align_mark1.TabIndex = 73;
            // 
            // lb_breaking_xy_align_mark1
            // 
            this.lb_breaking_xy_align_mark1.AutoSize = true;
            this.lb_breaking_xy_align_mark1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_xy_align_mark1.Location = new System.Drawing.Point(-2, 0);
            this.lb_breaking_xy_align_mark1.Name = "lb_breaking_xy_align_mark1";
            this.lb_breaking_xy_align_mark1.Size = new System.Drawing.Size(160, 18);
            this.lb_breaking_xy_align_mark1.TabIndex = 0;
            this.lb_breaking_xy_align_mark1.Text = "XY Align Mark1[mm]";
            // 
            // tbox_breaking_xy_pin_center_1
            // 
            this.tbox_breaking_xy_pin_center_1.Location = new System.Drawing.Point(301, 3);
            this.tbox_breaking_xy_pin_center_1.Name = "tbox_breaking_xy_pin_center_1";
            this.tbox_breaking_xy_pin_center_1.Size = new System.Drawing.Size(137, 21);
            this.tbox_breaking_xy_pin_center_1.TabIndex = 70;
            // 
            // panel_breaking_xy_pin_center
            // 
            this.panel_breaking_xy_pin_center.BackColor = System.Drawing.Color.Yellow;
            this.panel_breaking_xy_pin_center.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_breaking_xy_pin_center.Controls.Add(this.lb_breaking_xy_pin_center);
            this.panel_breaking_xy_pin_center.Location = new System.Drawing.Point(139, 3);
            this.panel_breaking_xy_pin_center.Name = "panel_breaking_xy_pin_center";
            this.panel_breaking_xy_pin_center.Size = new System.Drawing.Size(156, 21);
            this.panel_breaking_xy_pin_center.TabIndex = 70;
            // 
            // lb_breaking_xy_pin_center
            // 
            this.lb_breaking_xy_pin_center.AutoSize = true;
            this.lb_breaking_xy_pin_center.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_xy_pin_center.Location = new System.Drawing.Point(4, 0);
            this.lb_breaking_xy_pin_center.Name = "lb_breaking_xy_pin_center";
            this.lb_breaking_xy_pin_center.Size = new System.Drawing.Size(151, 18);
            this.lb_breaking_xy_pin_center.TabIndex = 0;
            this.lb_breaking_xy_pin_center.Text = "XY Pin Center[mm]";
            // 
            // btn_breaking_save
            // 
            this.btn_breaking_save.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_breaking_save.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_breaking_save.ForeColor = System.Drawing.Color.White;
            this.btn_breaking_save.Location = new System.Drawing.Point(1601, 266);
            this.btn_breaking_save.Name = "btn_breaking_save";
            this.btn_breaking_save.Size = new System.Drawing.Size(130, 30);
            this.btn_breaking_save.TabIndex = 69;
            this.btn_breaking_save.Text = "Save";
            this.btn_breaking_save.UseVisualStyleBackColor = false;
            // 
            // btn_breaking_delete
            // 
            this.btn_breaking_delete.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_breaking_delete.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_breaking_delete.ForeColor = System.Drawing.Color.White;
            this.btn_breaking_delete.Location = new System.Drawing.Point(275, 266);
            this.btn_breaking_delete.Name = "btn_breaking_delete";
            this.btn_breaking_delete.Size = new System.Drawing.Size(130, 30);
            this.btn_breaking_delete.TabIndex = 41;
            this.btn_breaking_delete.Text = "Delete";
            this.btn_breaking_delete.UseVisualStyleBackColor = false;
            // 
            // btn_breaking_copy
            // 
            this.btn_breaking_copy.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_breaking_copy.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_breaking_copy.ForeColor = System.Drawing.Color.White;
            this.btn_breaking_copy.Location = new System.Drawing.Point(139, 266);
            this.btn_breaking_copy.Name = "btn_breaking_copy";
            this.btn_breaking_copy.Size = new System.Drawing.Size(130, 30);
            this.btn_breaking_copy.TabIndex = 39;
            this.btn_breaking_copy.Text = "Copy";
            this.btn_breaking_copy.UseVisualStyleBackColor = false;
            // 
            // btn_breaking_make
            // 
            this.btn_breaking_make.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_breaking_make.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_breaking_make.ForeColor = System.Drawing.Color.White;
            this.btn_breaking_make.Location = new System.Drawing.Point(3, 266);
            this.btn_breaking_make.Name = "btn_breaking_make";
            this.btn_breaking_make.Size = new System.Drawing.Size(130, 30);
            this.btn_breaking_make.TabIndex = 38;
            this.btn_breaking_make.Text = "Make";
            this.btn_breaking_make.UseVisualStyleBackColor = false;
            // 
            // lv_breaking
            // 
            this.lv_breaking.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader2});
            this.lv_breaking.GridLines = true;
            this.lv_breaking.Location = new System.Drawing.Point(3, 3);
            this.lv_breaking.Name = "lv_breaking";
            this.lv_breaking.Size = new System.Drawing.Size(130, 257);
            this.lv_breaking.TabIndex = 0;
            this.lv_breaking.UseCompatibleStateImageBehavior = false;
            this.lv_breaking.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Name";
            this.columnHeader2.Width = 120;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.BackColor = System.Drawing.Color.Transparent;
            this.label51.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label51.ForeColor = System.Drawing.Color.White;
            this.label51.Location = new System.Drawing.Point(6, 35);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(16, 13);
            this.label51.TabIndex = 139;
            this.label51.Text = "X";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.BackColor = System.Drawing.Color.Transparent;
            this.label52.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label52.ForeColor = System.Drawing.Color.White;
            this.label52.Location = new System.Drawing.Point(6, 67);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(16, 13);
            this.label52.TabIndex = 140;
            this.label52.Text = "Y";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.BackColor = System.Drawing.Color.Transparent;
            this.label53.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label53.ForeColor = System.Drawing.Color.White;
            this.label53.Location = new System.Drawing.Point(6, 99);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(81, 13);
            this.label53.TabIndex = 141;
            this.label53.Text = "Thickness";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.BackColor = System.Drawing.Color.Transparent;
            this.label54.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label54.ForeColor = System.Drawing.Color.White;
            this.label54.Location = new System.Drawing.Point(6, 131);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(89, 13);
            this.label54.TabIndex = 142;
            this.label54.Text = "Cam To Pin";
            // 
            // textBox56
            // 
            this.textBox56.Location = new System.Drawing.Point(105, 28);
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new System.Drawing.Size(77, 21);
            this.textBox56.TabIndex = 138;
            // 
            // textBox57
            // 
            this.textBox57.Location = new System.Drawing.Point(105, 60);
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new System.Drawing.Size(77, 21);
            this.textBox57.TabIndex = 143;
            // 
            // textBox58
            // 
            this.textBox58.Location = new System.Drawing.Point(105, 92);
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new System.Drawing.Size(77, 21);
            this.textBox58.TabIndex = 144;
            // 
            // textBox59
            // 
            this.textBox59.Location = new System.Drawing.Point(105, 124);
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new System.Drawing.Size(77, 21);
            this.textBox59.TabIndex = 145;
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button9.ForeColor = System.Drawing.Color.White;
            this.button9.Location = new System.Drawing.Point(374, 699);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(77, 30);
            this.button9.TabIndex = 146;
            this.button9.Text = "Save";
            this.button9.UseVisualStyleBackColor = false;
            // 
            // panel_recipeinfo_2
            // 
            this.panel_recipeinfo_2.BackgroundImage = global::DIT.TLC.UI.Properties.Resources.RECIPE_INFO2;
            this.panel_recipeinfo_2.Location = new System.Drawing.Point(3, 480);
            this.panel_recipeinfo_2.Name = "panel_recipeinfo_2";
            this.panel_recipeinfo_2.Size = new System.Drawing.Size(269, 377);
            this.panel_recipeinfo_2.TabIndex = 71;
            // 
            // panel_recipeinfo_1
            // 
            this.panel_recipeinfo_1.BackgroundImage = global::DIT.TLC.UI.Properties.Resources.RECIPE_INFO;
            this.panel_recipeinfo_1.Location = new System.Drawing.Point(1468, 480);
            this.panel_recipeinfo_1.Name = "panel_recipeinfo_1";
            this.panel_recipeinfo_1.Size = new System.Drawing.Size(269, 377);
            this.panel_recipeinfo_1.TabIndex = 72;
            // 
            // gbox_breaking_zig
            // 
            this.gbox_breaking_zig.BackColor = System.Drawing.Color.Transparent;
            this.gbox_breaking_zig.Controls.Add(this.btn_breaking_zig_save);
            this.gbox_breaking_zig.Controls.Add(this.tbox_breaking_zig_cam_to_pin);
            this.gbox_breaking_zig.Controls.Add(this.tbox_breaking_zig_thickness);
            this.gbox_breaking_zig.Controls.Add(this.tbox_breaking_zig_y);
            this.gbox_breaking_zig.Controls.Add(this.tbox_breaking_zig_x);
            this.gbox_breaking_zig.Controls.Add(this.lb_breaking_zig_cam_to_pin);
            this.gbox_breaking_zig.Controls.Add(this.lb_breaking_zig_thickness);
            this.gbox_breaking_zig.Controls.Add(this.lb_breaking_zig_y);
            this.gbox_breaking_zig.Controls.Add(this.lb_breaking_zig_x);
            this.gbox_breaking_zig.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.gbox_breaking_zig.ForeColor = System.Drawing.Color.White;
            this.gbox_breaking_zig.Location = new System.Drawing.Point(278, 480);
            this.gbox_breaking_zig.Name = "gbox_breaking_zig";
            this.gbox_breaking_zig.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gbox_breaking_zig.Size = new System.Drawing.Size(188, 193);
            this.gbox_breaking_zig.TabIndex = 168;
            this.gbox_breaking_zig.TabStop = false;
            this.gbox_breaking_zig.Text = "브레이킹 Zig";
            // 
            // btn_breaking_zig_save
            // 
            this.btn_breaking_zig_save.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_breaking_zig_save.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_breaking_zig_save.ForeColor = System.Drawing.Color.White;
            this.btn_breaking_zig_save.Location = new System.Drawing.Point(105, 156);
            this.btn_breaking_zig_save.Name = "btn_breaking_zig_save";
            this.btn_breaking_zig_save.Size = new System.Drawing.Size(77, 30);
            this.btn_breaking_zig_save.TabIndex = 146;
            this.btn_breaking_zig_save.Text = "Save";
            this.btn_breaking_zig_save.UseVisualStyleBackColor = false;
            // 
            // tbox_breaking_zig_cam_to_pin
            // 
            this.tbox_breaking_zig_cam_to_pin.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbox_breaking_zig_cam_to_pin.Location = new System.Drawing.Point(105, 124);
            this.tbox_breaking_zig_cam_to_pin.Name = "tbox_breaking_zig_cam_to_pin";
            this.tbox_breaking_zig_cam_to_pin.Size = new System.Drawing.Size(77, 21);
            this.tbox_breaking_zig_cam_to_pin.TabIndex = 145;
            // 
            // tbox_breaking_zig_thickness
            // 
            this.tbox_breaking_zig_thickness.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbox_breaking_zig_thickness.Location = new System.Drawing.Point(105, 92);
            this.tbox_breaking_zig_thickness.Name = "tbox_breaking_zig_thickness";
            this.tbox_breaking_zig_thickness.Size = new System.Drawing.Size(77, 21);
            this.tbox_breaking_zig_thickness.TabIndex = 144;
            // 
            // tbox_breaking_zig_y
            // 
            this.tbox_breaking_zig_y.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbox_breaking_zig_y.Location = new System.Drawing.Point(105, 60);
            this.tbox_breaking_zig_y.Name = "tbox_breaking_zig_y";
            this.tbox_breaking_zig_y.Size = new System.Drawing.Size(77, 21);
            this.tbox_breaking_zig_y.TabIndex = 143;
            // 
            // tbox_breaking_zig_x
            // 
            this.tbox_breaking_zig_x.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbox_breaking_zig_x.Location = new System.Drawing.Point(105, 28);
            this.tbox_breaking_zig_x.Name = "tbox_breaking_zig_x";
            this.tbox_breaking_zig_x.Size = new System.Drawing.Size(77, 21);
            this.tbox_breaking_zig_x.TabIndex = 138;
            // 
            // lb_breaking_zig_cam_to_pin
            // 
            this.lb_breaking_zig_cam_to_pin.AutoSize = true;
            this.lb_breaking_zig_cam_to_pin.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_zig_cam_to_pin.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_zig_cam_to_pin.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_zig_cam_to_pin.Location = new System.Drawing.Point(6, 131);
            this.lb_breaking_zig_cam_to_pin.Name = "lb_breaking_zig_cam_to_pin";
            this.lb_breaking_zig_cam_to_pin.Size = new System.Drawing.Size(89, 13);
            this.lb_breaking_zig_cam_to_pin.TabIndex = 142;
            this.lb_breaking_zig_cam_to_pin.Text = "Cam To Pin";
            // 
            // lb_breaking_zig_thickness
            // 
            this.lb_breaking_zig_thickness.AutoSize = true;
            this.lb_breaking_zig_thickness.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_zig_thickness.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_zig_thickness.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_zig_thickness.Location = new System.Drawing.Point(6, 99);
            this.lb_breaking_zig_thickness.Name = "lb_breaking_zig_thickness";
            this.lb_breaking_zig_thickness.Size = new System.Drawing.Size(81, 13);
            this.lb_breaking_zig_thickness.TabIndex = 141;
            this.lb_breaking_zig_thickness.Text = "Thickness";
            // 
            // lb_breaking_zig_y
            // 
            this.lb_breaking_zig_y.AutoSize = true;
            this.lb_breaking_zig_y.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_zig_y.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_zig_y.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_zig_y.Location = new System.Drawing.Point(6, 67);
            this.lb_breaking_zig_y.Name = "lb_breaking_zig_y";
            this.lb_breaking_zig_y.Size = new System.Drawing.Size(16, 13);
            this.lb_breaking_zig_y.TabIndex = 140;
            this.lb_breaking_zig_y.Text = "Y";
            // 
            // lb_breaking_zig_x
            // 
            this.lb_breaking_zig_x.AutoSize = true;
            this.lb_breaking_zig_x.BackColor = System.Drawing.Color.Transparent;
            this.lb_breaking_zig_x.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_breaking_zig_x.ForeColor = System.Drawing.Color.White;
            this.lb_breaking_zig_x.Location = new System.Drawing.Point(6, 35);
            this.lb_breaking_zig_x.Name = "lb_breaking_zig_x";
            this.lb_breaking_zig_x.Size = new System.Drawing.Size(16, 13);
            this.lb_breaking_zig_x.TabIndex = 139;
            this.lb_breaking_zig_x.Text = "X";
            // 
            // panel_default_process
            // 
            this.panel_default_process.BackColor = System.Drawing.Color.DimGray;
            this.panel_default_process.Controls.Add(this.tbox_default_process_prealign_error_y);
            this.panel_default_process.Controls.Add(this.tbox_default_process_align_angle);
            this.panel_default_process.Controls.Add(this.lb_default_process_prealign_error_y);
            this.panel_default_process.Controls.Add(this.lb_default_process_align_angle);
            this.panel_default_process.Controls.Add(this.tbox_default_process_prealign_error_x);
            this.panel_default_process.Controls.Add(this.tbox_default_process_align_distance);
            this.panel_default_process.Controls.Add(this.lb_default_process_prealign_error_x);
            this.panel_default_process.Controls.Add(this.lb_default_process_align_distance);
            this.panel_default_process.Controls.Add(this.tbox_default_process_break_error_y);
            this.panel_default_process.Controls.Add(this.lb_default_process_break_error_y);
            this.panel_default_process.Controls.Add(this.tbox_default_process_prealign_error_angle);
            this.panel_default_process.Controls.Add(this.tbox_default_process_align_match);
            this.panel_default_process.Controls.Add(this.lb_default_process_prealign_error_angle);
            this.panel_default_process.Controls.Add(this.lb_default_process_align_match);
            this.panel_default_process.Controls.Add(this.btn_default_process_save);
            this.panel_default_process.Controls.Add(this.tbox_default_process_default_z);
            this.panel_default_process.Controls.Add(this.tbox_default_process_jump_speed);
            this.panel_default_process.Controls.Add(this.lb_default_process_default_z);
            this.panel_default_process.Controls.Add(this.lb_default_process_jump_speed);
            this.panel_default_process.Location = new System.Drawing.Point(506, 601);
            this.panel_default_process.Name = "panel_default_process";
            this.panel_default_process.Size = new System.Drawing.Size(916, 115);
            this.panel_default_process.TabIndex = 171;
            // 
            // tbox_default_process_prealign_error_y
            // 
            this.tbox_default_process_prealign_error_y.Location = new System.Drawing.Point(817, 46);
            this.tbox_default_process_prealign_error_y.Name = "tbox_default_process_prealign_error_y";
            this.tbox_default_process_prealign_error_y.Size = new System.Drawing.Size(65, 21);
            this.tbox_default_process_prealign_error_y.TabIndex = 162;
            // 
            // tbox_default_process_align_angle
            // 
            this.tbox_default_process_align_angle.Location = new System.Drawing.Point(817, 17);
            this.tbox_default_process_align_angle.Name = "tbox_default_process_align_angle";
            this.tbox_default_process_align_angle.Size = new System.Drawing.Size(65, 21);
            this.tbox_default_process_align_angle.TabIndex = 161;
            // 
            // lb_default_process_prealign_error_y
            // 
            this.lb_default_process_prealign_error_y.AutoSize = true;
            this.lb_default_process_prealign_error_y.BackColor = System.Drawing.Color.Transparent;
            this.lb_default_process_prealign_error_y.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_default_process_prealign_error_y.ForeColor = System.Drawing.Color.White;
            this.lb_default_process_prealign_error_y.Location = new System.Drawing.Point(662, 49);
            this.lb_default_process_prealign_error_y.Name = "lb_default_process_prealign_error_y";
            this.lb_default_process_prealign_error_y.Size = new System.Drawing.Size(122, 13);
            this.lb_default_process_prealign_error_y.TabIndex = 160;
            this.lb_default_process_prealign_error_y.Text = "PreAlign Error Y";
            // 
            // lb_default_process_align_angle
            // 
            this.lb_default_process_align_angle.AutoSize = true;
            this.lb_default_process_align_angle.BackColor = System.Drawing.Color.Transparent;
            this.lb_default_process_align_angle.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_default_process_align_angle.ForeColor = System.Drawing.Color.White;
            this.lb_default_process_align_angle.Location = new System.Drawing.Point(662, 20);
            this.lb_default_process_align_angle.Name = "lb_default_process_align_angle";
            this.lb_default_process_align_angle.Size = new System.Drawing.Size(87, 13);
            this.lb_default_process_align_angle.TabIndex = 159;
            this.lb_default_process_align_angle.Text = "Align angle";
            // 
            // tbox_default_process_prealign_error_x
            // 
            this.tbox_default_process_prealign_error_x.Location = new System.Drawing.Point(587, 46);
            this.tbox_default_process_prealign_error_x.Name = "tbox_default_process_prealign_error_x";
            this.tbox_default_process_prealign_error_x.Size = new System.Drawing.Size(65, 21);
            this.tbox_default_process_prealign_error_x.TabIndex = 158;
            // 
            // tbox_default_process_align_distance
            // 
            this.tbox_default_process_align_distance.Location = new System.Drawing.Point(587, 17);
            this.tbox_default_process_align_distance.Name = "tbox_default_process_align_distance";
            this.tbox_default_process_align_distance.Size = new System.Drawing.Size(65, 21);
            this.tbox_default_process_align_distance.TabIndex = 157;
            // 
            // lb_default_process_prealign_error_x
            // 
            this.lb_default_process_prealign_error_x.AutoSize = true;
            this.lb_default_process_prealign_error_x.BackColor = System.Drawing.Color.Transparent;
            this.lb_default_process_prealign_error_x.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_default_process_prealign_error_x.ForeColor = System.Drawing.Color.White;
            this.lb_default_process_prealign_error_x.Location = new System.Drawing.Point(434, 49);
            this.lb_default_process_prealign_error_x.Name = "lb_default_process_prealign_error_x";
            this.lb_default_process_prealign_error_x.Size = new System.Drawing.Size(122, 13);
            this.lb_default_process_prealign_error_x.TabIndex = 156;
            this.lb_default_process_prealign_error_x.Text = "PreAlign Error X";
            // 
            // lb_default_process_align_distance
            // 
            this.lb_default_process_align_distance.AutoSize = true;
            this.lb_default_process_align_distance.BackColor = System.Drawing.Color.Transparent;
            this.lb_default_process_align_distance.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_default_process_align_distance.ForeColor = System.Drawing.Color.White;
            this.lb_default_process_align_distance.Location = new System.Drawing.Point(434, 20);
            this.lb_default_process_align_distance.Name = "lb_default_process_align_distance";
            this.lb_default_process_align_distance.Size = new System.Drawing.Size(149, 13);
            this.lb_default_process_align_distance.TabIndex = 155;
            this.lb_default_process_align_distance.Text = "Align distance[mm]";
            // 
            // tbox_default_process_break_error_y
            // 
            this.tbox_default_process_break_error_y.Location = new System.Drawing.Point(360, 75);
            this.tbox_default_process_break_error_y.Name = "tbox_default_process_break_error_y";
            this.tbox_default_process_break_error_y.Size = new System.Drawing.Size(65, 21);
            this.tbox_default_process_break_error_y.TabIndex = 154;
            // 
            // lb_default_process_break_error_y
            // 
            this.lb_default_process_break_error_y.AutoSize = true;
            this.lb_default_process_break_error_y.BackColor = System.Drawing.Color.Transparent;
            this.lb_default_process_break_error_y.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_default_process_break_error_y.ForeColor = System.Drawing.Color.White;
            this.lb_default_process_break_error_y.Location = new System.Drawing.Point(203, 77);
            this.lb_default_process_break_error_y.Name = "lb_default_process_break_error_y";
            this.lb_default_process_break_error_y.Size = new System.Drawing.Size(103, 13);
            this.lb_default_process_break_error_y.TabIndex = 153;
            this.lb_default_process_break_error_y.Text = "Break Error Y";
            // 
            // tbox_default_process_prealign_error_angle
            // 
            this.tbox_default_process_prealign_error_angle.Location = new System.Drawing.Point(360, 46);
            this.tbox_default_process_prealign_error_angle.Name = "tbox_default_process_prealign_error_angle";
            this.tbox_default_process_prealign_error_angle.Size = new System.Drawing.Size(65, 21);
            this.tbox_default_process_prealign_error_angle.TabIndex = 152;
            // 
            // tbox_default_process_align_match
            // 
            this.tbox_default_process_align_match.Location = new System.Drawing.Point(360, 17);
            this.tbox_default_process_align_match.Name = "tbox_default_process_align_match";
            this.tbox_default_process_align_match.Size = new System.Drawing.Size(65, 21);
            this.tbox_default_process_align_match.TabIndex = 151;
            // 
            // lb_default_process_prealign_error_angle
            // 
            this.lb_default_process_prealign_error_angle.AutoSize = true;
            this.lb_default_process_prealign_error_angle.BackColor = System.Drawing.Color.Transparent;
            this.lb_default_process_prealign_error_angle.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_default_process_prealign_error_angle.ForeColor = System.Drawing.Color.White;
            this.lb_default_process_prealign_error_angle.Location = new System.Drawing.Point(203, 48);
            this.lb_default_process_prealign_error_angle.Name = "lb_default_process_prealign_error_angle";
            this.lb_default_process_prealign_error_angle.Size = new System.Drawing.Size(153, 13);
            this.lb_default_process_prealign_error_angle.TabIndex = 150;
            this.lb_default_process_prealign_error_angle.Text = "PreAlign Error Angle";
            // 
            // lb_default_process_align_match
            // 
            this.lb_default_process_align_match.AutoSize = true;
            this.lb_default_process_align_match.BackColor = System.Drawing.Color.Transparent;
            this.lb_default_process_align_match.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_default_process_align_match.ForeColor = System.Drawing.Color.White;
            this.lb_default_process_align_match.Location = new System.Drawing.Point(203, 20);
            this.lb_default_process_align_match.Name = "lb_default_process_align_match";
            this.lb_default_process_align_match.Size = new System.Drawing.Size(130, 13);
            this.lb_default_process_align_match.TabIndex = 149;
            this.lb_default_process_align_match.Text = "Align match[mm]";
            // 
            // btn_default_process_save
            // 
            this.btn_default_process_save.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_default_process_save.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_default_process_save.ForeColor = System.Drawing.Color.White;
            this.btn_default_process_save.Location = new System.Drawing.Point(778, 79);
            this.btn_default_process_save.Name = "btn_default_process_save";
            this.btn_default_process_save.Size = new System.Drawing.Size(130, 30);
            this.btn_default_process_save.TabIndex = 138;
            this.btn_default_process_save.Text = "Save";
            this.btn_default_process_save.UseVisualStyleBackColor = false;
            // 
            // tbox_default_process_default_z
            // 
            this.tbox_default_process_default_z.Location = new System.Drawing.Point(128, 65);
            this.tbox_default_process_default_z.Name = "tbox_default_process_default_z";
            this.tbox_default_process_default_z.Size = new System.Drawing.Size(65, 21);
            this.tbox_default_process_default_z.TabIndex = 148;
            // 
            // tbox_default_process_jump_speed
            // 
            this.tbox_default_process_jump_speed.Location = new System.Drawing.Point(128, 36);
            this.tbox_default_process_jump_speed.Name = "tbox_default_process_jump_speed";
            this.tbox_default_process_jump_speed.Size = new System.Drawing.Size(65, 21);
            this.tbox_default_process_jump_speed.TabIndex = 147;
            // 
            // lb_default_process_default_z
            // 
            this.lb_default_process_default_z.AutoSize = true;
            this.lb_default_process_default_z.BackColor = System.Drawing.Color.Transparent;
            this.lb_default_process_default_z.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_default_process_default_z.ForeColor = System.Drawing.Color.White;
            this.lb_default_process_default_z.Location = new System.Drawing.Point(15, 67);
            this.lb_default_process_default_z.Name = "lb_default_process_default_z";
            this.lb_default_process_default_z.Size = new System.Drawing.Size(111, 13);
            this.lb_default_process_default_z.TabIndex = 128;
            this.lb_default_process_default_z.Text = "Default Z[mm]";
            // 
            // lb_default_process_jump_speed
            // 
            this.lb_default_process_jump_speed.AutoSize = true;
            this.lb_default_process_jump_speed.BackColor = System.Drawing.Color.Transparent;
            this.lb_default_process_jump_speed.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_default_process_jump_speed.ForeColor = System.Drawing.Color.White;
            this.lb_default_process_jump_speed.Location = new System.Drawing.Point(15, 39);
            this.lb_default_process_jump_speed.Name = "lb_default_process_jump_speed";
            this.lb_default_process_jump_speed.Size = new System.Drawing.Size(94, 13);
            this.lb_default_process_jump_speed.TabIndex = 127;
            this.lb_default_process_jump_speed.Text = "Jump speed";
            // 
            // panel_default_laser
            // 
            this.panel_default_laser.BackColor = System.Drawing.Color.DimGray;
            this.panel_default_laser.Controls.Add(this.btn_default_laser_save);
            this.panel_default_laser.Controls.Add(this.tbox_default_laser_cst_pitch_reverse);
            this.panel_default_laser.Controls.Add(this.tbox_default_laser_cst_pitch_normal);
            this.panel_default_laser.Controls.Add(this.tbox_default_laser_ontime);
            this.panel_default_laser.Controls.Add(this.lb_default_laser_cst_pitch_reverse);
            this.panel_default_laser.Controls.Add(this.lb_default_laser_cst_pitch_normal);
            this.panel_default_laser.Controls.Add(this.lb_default_laser_ontime);
            this.panel_default_laser.Location = new System.Drawing.Point(506, 480);
            this.panel_default_laser.Name = "panel_default_laser";
            this.panel_default_laser.Size = new System.Drawing.Size(916, 115);
            this.panel_default_laser.TabIndex = 170;
            // 
            // btn_default_laser_save
            // 
            this.btn_default_laser_save.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_default_laser_save.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_default_laser_save.ForeColor = System.Drawing.Color.White;
            this.btn_default_laser_save.Location = new System.Drawing.Point(778, 80);
            this.btn_default_laser_save.Name = "btn_default_laser_save";
            this.btn_default_laser_save.Size = new System.Drawing.Size(130, 30);
            this.btn_default_laser_save.TabIndex = 138;
            this.btn_default_laser_save.Text = "Save";
            this.btn_default_laser_save.UseVisualStyleBackColor = false;
            // 
            // tbox_default_laser_cst_pitch_reverse
            // 
            this.tbox_default_laser_cst_pitch_reverse.Location = new System.Drawing.Point(560, 44);
            this.tbox_default_laser_cst_pitch_reverse.Name = "tbox_default_laser_cst_pitch_reverse";
            this.tbox_default_laser_cst_pitch_reverse.Size = new System.Drawing.Size(108, 21);
            this.tbox_default_laser_cst_pitch_reverse.TabIndex = 149;
            // 
            // tbox_default_laser_cst_pitch_normal
            // 
            this.tbox_default_laser_cst_pitch_normal.Location = new System.Drawing.Point(204, 44);
            this.tbox_default_laser_cst_pitch_normal.Name = "tbox_default_laser_cst_pitch_normal";
            this.tbox_default_laser_cst_pitch_normal.Size = new System.Drawing.Size(108, 21);
            this.tbox_default_laser_cst_pitch_normal.TabIndex = 148;
            // 
            // tbox_default_laser_ontime
            // 
            this.tbox_default_laser_ontime.Location = new System.Drawing.Point(204, 15);
            this.tbox_default_laser_ontime.Name = "tbox_default_laser_ontime";
            this.tbox_default_laser_ontime.Size = new System.Drawing.Size(109, 21);
            this.tbox_default_laser_ontime.TabIndex = 147;
            // 
            // lb_default_laser_cst_pitch_reverse
            // 
            this.lb_default_laser_cst_pitch_reverse.AutoSize = true;
            this.lb_default_laser_cst_pitch_reverse.BackColor = System.Drawing.Color.Transparent;
            this.lb_default_laser_cst_pitch_reverse.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_default_laser_cst_pitch_reverse.ForeColor = System.Drawing.Color.White;
            this.lb_default_laser_cst_pitch_reverse.Location = new System.Drawing.Point(367, 46);
            this.lb_default_laser_cst_pitch_reverse.Name = "lb_default_laser_cst_pitch_reverse";
            this.lb_default_laser_cst_pitch_reverse.Size = new System.Drawing.Size(181, 13);
            this.lb_default_laser_cst_pitch_reverse.TabIndex = 129;
            this.lb_default_laser_cst_pitch_reverse.Text = "Cassette pitch(Reverse)";
            // 
            // lb_default_laser_cst_pitch_normal
            // 
            this.lb_default_laser_cst_pitch_normal.AutoSize = true;
            this.lb_default_laser_cst_pitch_normal.BackColor = System.Drawing.Color.Transparent;
            this.lb_default_laser_cst_pitch_normal.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_default_laser_cst_pitch_normal.ForeColor = System.Drawing.Color.White;
            this.lb_default_laser_cst_pitch_normal.Location = new System.Drawing.Point(17, 46);
            this.lb_default_laser_cst_pitch_normal.Name = "lb_default_laser_cst_pitch_normal";
            this.lb_default_laser_cst_pitch_normal.Size = new System.Drawing.Size(172, 13);
            this.lb_default_laser_cst_pitch_normal.TabIndex = 128;
            this.lb_default_laser_cst_pitch_normal.Text = "Cassette pitch(Normal)";
            // 
            // lb_default_laser_ontime
            // 
            this.lb_default_laser_ontime.AutoSize = true;
            this.lb_default_laser_ontime.BackColor = System.Drawing.Color.Transparent;
            this.lb_default_laser_ontime.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_default_laser_ontime.ForeColor = System.Drawing.Color.White;
            this.lb_default_laser_ontime.Location = new System.Drawing.Point(17, 17);
            this.lb_default_laser_ontime.Name = "lb_default_laser_ontime";
            this.lb_default_laser_ontime.Size = new System.Drawing.Size(86, 13);
            this.lb_default_laser_ontime.TabIndex = 127;
            this.lb_default_laser_ontime.Text = "On time[s]";
            // 
            // Recipe_ProCondition
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.Controls.Add(this.panel_default_process);
            this.Controls.Add(this.panel_default_laser);
            this.Controls.Add(this.gbox_breaking_zig);
            this.Controls.Add(this.panel_recipeinfo_1);
            this.Controls.Add(this.panel_recipeinfo_2);
            this.Controls.Add(this.panel_breaking);
            this.Controls.Add(this.panel_process);
            this.Name = "Recipe_ProCondition";
            this.Size = new System.Drawing.Size(1740, 860);
            this.panel_process.ResumeLayout(false);
            this.panel_process.PerformLayout();
            this.panel_breaking.ResumeLayout(false);
            this.panel_breaking.PerformLayout();
            this.gbox_breaking_load_trans_sequence_offset.ResumeLayout(false);
            this.gbox_breaking_load_trans_sequence_offset_b.ResumeLayout(false);
            this.gbox_breaking_load_trans_sequence_offset_b.PerformLayout();
            this.gbox_breaking_load_trans_sequence_offset_a.ResumeLayout(false);
            this.gbox_breaking_load_trans_sequence_offset_a.PerformLayout();
            this.panel_breaking_xyt_breaking4_calc.ResumeLayout(false);
            this.panel_breaking_xyt_breaking4_calc.PerformLayout();
            this.panel_breaking_xyt_breaking3_calc.ResumeLayout(false);
            this.panel_breaking_xyt_breaking3_calc.PerformLayout();
            this.panel_breaking_xyt_breaking2_calc.ResumeLayout(false);
            this.panel_breaking_xyt_breaking2_calc.PerformLayout();
            this.panel_breaking_xyt_breaking1_calc.ResumeLayout(false);
            this.panel_breaking_xyt_breaking1_calc.PerformLayout();
            this.panel_breaking_half.ResumeLayout(false);
            this.panel_breaking_half.PerformLayout();
            this.panel_breaking_right.ResumeLayout(false);
            this.panel_breaking_right.PerformLayout();
            this.panel_breaking_left.ResumeLayout(false);
            this.panel_breaking_left.PerformLayout();
            this.panel_breaking_xy_mcr.ResumeLayout(false);
            this.panel_breaking_xy_mcr.PerformLayout();
            this.panel_breaking_xy_align_mark2.ResumeLayout(false);
            this.panel_breaking_xy_align_mark2.PerformLayout();
            this.panel_breaking_xy_align_mark1.ResumeLayout(false);
            this.panel_breaking_xy_align_mark1.PerformLayout();
            this.panel_breaking_xy_pin_center.ResumeLayout(false);
            this.panel_breaking_xy_pin_center.PerformLayout();
            this.gbox_breaking_zig.ResumeLayout(false);
            this.gbox_breaking_zig.PerformLayout();
            this.panel_default_process.ResumeLayout(false);
            this.panel_default_process.PerformLayout();
            this.panel_default_laser.ResumeLayout(false);
            this.panel_default_laser.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_process;
        private System.Windows.Forms.ListView lv_process;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.Button btn_process_copy;
        private System.Windows.Forms.Button btn_process_make;
        private System.Windows.Forms.TextBox tbox_process_power_ratio;
        private System.Windows.Forms.Label lb_process_power_ratio;
        private System.Windows.Forms.TextBox tbox_process_power;
        private System.Windows.Forms.Label lb_process_power;
        private System.Windows.Forms.TextBox tbox_process_divider;
        private System.Windows.Forms.Button btn_process_delete;
        private System.Windows.Forms.Label lb_process_divider;
        private System.Windows.Forms.Button btn_process_save;
        private System.Windows.Forms.TextBox tbox_process_acc_dec;
        private System.Windows.Forms.Label lb_process_acc_dec;
        private System.Windows.Forms.TextBox tbox_process_scan;
        private System.Windows.Forms.Label lb_process_scan;
        private System.Windows.Forms.TextBox tbox_process_g;
        private System.Windows.Forms.Label lb_process_g;
        private System.Windows.Forms.TextBox tbox_process_segment_value;
        private System.Windows.Forms.Label lb_process_segment_value;
        private System.Windows.Forms.TextBox tbox_process_overlap;
        private System.Windows.Forms.Label lb_process_overlap;
        private System.Windows.Forms.TextBox tbox_process_zpos;
        private System.Windows.Forms.Label lb_process_zpos;
        private System.Windows.Forms.TextBox tbox_process_speed;
        private System.Windows.Forms.Label lb_process_speed;
        private System.Windows.Forms.TextBox tbox_process_frequence;
        private System.Windows.Forms.Label lb_process_frequence;
        private System.Windows.Forms.TextBox tbox_process_burst;
        private System.Windows.Forms.Label lb_process_burst;
        private System.Windows.Forms.TextBox tbox_process_error;
        private System.Windows.Forms.Label lb_process_error;
        private System.Windows.Forms.Panel panel_breaking;
        private System.Windows.Forms.Button btn_breaking_save;
        private System.Windows.Forms.Button btn_breaking_delete;
        private System.Windows.Forms.Button btn_breaking_copy;
        private System.Windows.Forms.Button btn_breaking_make;
        private System.Windows.Forms.ListView lv_breaking;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Panel panel_breaking_half;
        private System.Windows.Forms.Label lb_breaking_half;
        private System.Windows.Forms.Panel panel_breaking_right;
        private System.Windows.Forms.Label lb_breaking_right;
        private System.Windows.Forms.TextBox tbox_breaking_xyt_breaking1_3;
        private System.Windows.Forms.TextBox tbox_breaking_xyt_breaking1_2;
        private System.Windows.Forms.TextBox tbox_breaking_xyt_breaking1_1;
        private System.Windows.Forms.Panel panel_breaking_left;
        private System.Windows.Forms.Label lb_breaking_left;
        private System.Windows.Forms.TextBox tbox_breaking_xy_mcr_2;
        private System.Windows.Forms.TextBox tbox_breaking_xy_align_mark2_2;
        private System.Windows.Forms.TextBox tbox_breaking_xy_align_mark1_2;
        private System.Windows.Forms.TextBox tbox_breaking_xy_pin_center_2;
        private System.Windows.Forms.Label lb_breaking_xyt_breaking1;
        private System.Windows.Forms.TextBox tbox_breaking_pin_distance_b;
        private System.Windows.Forms.Label lb_breaking_pin_distance_b;
        private System.Windows.Forms.TextBox tbox_breaking_pin_distance_a;
        private System.Windows.Forms.Label lb_breaking_pin_distance_a;
        private System.Windows.Forms.TextBox tbox_breaking_xy_mcr_1;
        private System.Windows.Forms.Panel panel_breaking_xy_mcr;
        private System.Windows.Forms.Label lb_breaking_xy_mcr;
        private System.Windows.Forms.TextBox tbox_breaking_xy_align_mark2_1;
        private System.Windows.Forms.Panel panel_breaking_xy_align_mark2;
        private System.Windows.Forms.Label lb_breaking_xy_align_mark2;
        private System.Windows.Forms.TextBox tbox_breaking_xy_align_mark1_1;
        private System.Windows.Forms.Panel panel_breaking_xy_align_mark1;
        private System.Windows.Forms.Label lb_breaking_xy_align_mark1;
        private System.Windows.Forms.TextBox tbox_breaking_xy_pin_center_1;
        private System.Windows.Forms.Panel panel_breaking_xy_pin_center;
        private System.Windows.Forms.Label lb_breaking_xy_pin_center;
        private System.Windows.Forms.TextBox tbox_breaking_b_mcr_offset_xy_2;
        private System.Windows.Forms.TextBox tbox_breaking_a_mcr_offset_xy_2;
        private System.Windows.Forms.TextBox tbox_breaking_b_mcr_offset_xy_1;
        private System.Windows.Forms.Label lb_breaking_b_mcr_offset_xy;
        private System.Windows.Forms.TextBox tbox_breaking_a_mcr_offset_xy_1;
        private System.Windows.Forms.Label lb_breaking_a_mcr_offset_xy;
        private System.Windows.Forms.TextBox tbox_breaking_b2_xy_offset_2;
        private System.Windows.Forms.TextBox tbox_breaking_b2_xy_offset_1;
        private System.Windows.Forms.Label lb_breaking_b2_xy_offset;
        private System.Windows.Forms.TextBox tbox_breaking_b1_xy_offset_2;
        private System.Windows.Forms.TextBox tbox_breaking_b1_xy_offset_1;
        private System.Windows.Forms.Label lb_breaking_b1_xy_offset;
        private System.Windows.Forms.TextBox tbox_breaking_a2_xy_offset_2;
        private System.Windows.Forms.TextBox tbox_breaking_a2_xy_offset_1;
        private System.Windows.Forms.Label lb_breaking_a2_xy_offset;
        private System.Windows.Forms.TextBox tbox_breaking_a1_xy_offset_2;
        private System.Windows.Forms.TextBox tbox_breaking_a1_xy_offset_1;
        private System.Windows.Forms.Label lb_breaking_a1_xy_offset;
        private System.Windows.Forms.Panel panel_breaking_xyt_breaking4_calc;
        private System.Windows.Forms.Label lb_breaking_xyt_breaking4_calc;
        private System.Windows.Forms.TextBox tbox_breaking_xyt_breaking4_3;
        private System.Windows.Forms.TextBox tbox_breaking_xyt_breaking4_2;
        private System.Windows.Forms.TextBox tbox_breaking_xyt_breaking4_1;
        private System.Windows.Forms.Label lb_breaking_xyt_breaking4;
        private System.Windows.Forms.Panel panel_breaking_xyt_breaking3_calc;
        private System.Windows.Forms.Label lb_breaking_xyt_breaking3_calc;
        private System.Windows.Forms.TextBox tbox_breaking_xyt_breaking3_3;
        private System.Windows.Forms.TextBox tbox_breaking_xyt_breaking3_2;
        private System.Windows.Forms.TextBox tbox_breaking_xyt_breaking3_1;
        private System.Windows.Forms.Label lb_breaking_xyt_breaking3;
        private System.Windows.Forms.Panel panel_breaking_xyt_breaking2_calc;
        private System.Windows.Forms.Label lb_breaking_xyt_breaking2_calc;
        private System.Windows.Forms.TextBox tbox_breaking_xyt_breaking2_3;
        private System.Windows.Forms.Panel panel_breaking_xyt_breaking1_calc;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox tbox_breaking_xyt_breaking2_2;
        private System.Windows.Forms.TextBox tbox_breaking_xyt_breaking2_1;
        private System.Windows.Forms.Label lb_breaking_xyt_breaking2;
        private System.Windows.Forms.Panel panel_breaking_2pass;
        private System.Windows.Forms.Label lb_breaking_2pass;
        private System.Windows.Forms.TextBox tbox_breaking_slowdown_ptich;
        private System.Windows.Forms.TextBox tbox_breaking_fastdown_ptich;
        private System.Windows.Forms.Label lb_breaking_fastdown_ptich;
        private System.Windows.Forms.TextBox tbox_breaking_slowdown_speed;
        private System.Windows.Forms.Label lb_breaking_slowdown_speed;
        private System.Windows.Forms.TextBox tbox_breaking_fastdown_speed;
        private System.Windows.Forms.Label lb_breaking_fastdown_speed;
        private System.Windows.Forms.Label lb_breaking_slowdown_ptich_mm;
        private System.Windows.Forms.Label lb_breaking_fastdown_ptich_mm;
        private System.Windows.Forms.Label lb_breaking_slowdown_ptich;
        private System.Windows.Forms.GroupBox gbox_breaking_load_trans_sequence_offset;
        private System.Windows.Forms.GroupBox gbox_breaking_load_trans_sequence_offset_b;
        private System.Windows.Forms.Label lb_breaking_load_trans_sequence_offset_b_y2_mm;
        private System.Windows.Forms.TextBox tbox_breaking_load_trans_sequence_offset_b_y1;
        private System.Windows.Forms.Label lb_breaking_load_trans_sequence_offset_b_y1_mm;
        private System.Windows.Forms.Label lb_breaking_load_trans_sequence_offset_b_y1;
        private System.Windows.Forms.TextBox tbox_breaking_load_trans_sequence_offset_b_y2;
        private System.Windows.Forms.Label lb_breaking_load_trans_sequence_offset_b_y2;
        private System.Windows.Forms.GroupBox gbox_breaking_load_trans_sequence_offset_a;
        private System.Windows.Forms.Label lb_breaking_load_trans_sequence_offset_a_y2_mm;
        private System.Windows.Forms.Label lb_breaking_load_trans_sequence_offset_a_y1_mm;
        private System.Windows.Forms.TextBox tbox_breaking_load_trans_sequence_offset_a_y2;
        private System.Windows.Forms.TextBox tbox_breaking_load_trans_sequence_offset_a_y1;
        private System.Windows.Forms.Label lb_breaking_load_trans_sequence_offset_a_y2;
        private System.Windows.Forms.Label lb_breaking_load_trans_sequence_offset_a_y1;
        private System.Windows.Forms.Panel panel_recipeinfo_2;
        private System.Windows.Forms.TextBox textBox59;
        private System.Windows.Forms.TextBox textBox58;
        private System.Windows.Forms.TextBox textBox57;
        private System.Windows.Forms.TextBox textBox56;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Panel panel_recipeinfo_1;
        private System.Windows.Forms.GroupBox gbox_breaking_zig;
        private System.Windows.Forms.Button btn_breaking_zig_save;
        private System.Windows.Forms.TextBox tbox_breaking_zig_cam_to_pin;
        private System.Windows.Forms.TextBox tbox_breaking_zig_thickness;
        private System.Windows.Forms.TextBox tbox_breaking_zig_y;
        private System.Windows.Forms.TextBox tbox_breaking_zig_x;
        private System.Windows.Forms.Label lb_breaking_zig_cam_to_pin;
        private System.Windows.Forms.Label lb_breaking_zig_thickness;
        private System.Windows.Forms.Label lb_breaking_zig_y;
        private System.Windows.Forms.Label lb_breaking_zig_x;
        private System.Windows.Forms.Panel panel_default_process;
        private System.Windows.Forms.TextBox tbox_default_process_prealign_error_y;
        private System.Windows.Forms.TextBox tbox_default_process_align_angle;
        private System.Windows.Forms.Label lb_default_process_prealign_error_y;
        private System.Windows.Forms.Label lb_default_process_align_angle;
        private System.Windows.Forms.TextBox tbox_default_process_prealign_error_x;
        private System.Windows.Forms.TextBox tbox_default_process_align_distance;
        private System.Windows.Forms.Label lb_default_process_prealign_error_x;
        private System.Windows.Forms.Label lb_default_process_align_distance;
        private System.Windows.Forms.TextBox tbox_default_process_break_error_y;
        private System.Windows.Forms.Label lb_default_process_break_error_y;
        private System.Windows.Forms.TextBox tbox_default_process_prealign_error_angle;
        private System.Windows.Forms.TextBox tbox_default_process_align_match;
        private System.Windows.Forms.Label lb_default_process_prealign_error_angle;
        private System.Windows.Forms.Label lb_default_process_align_match;
        private System.Windows.Forms.Button btn_default_process_save;
        private System.Windows.Forms.TextBox tbox_default_process_default_z;
        private System.Windows.Forms.TextBox tbox_default_process_jump_speed;
        private System.Windows.Forms.Label lb_default_process_default_z;
        private System.Windows.Forms.Label lb_default_process_jump_speed;
        private System.Windows.Forms.Panel panel_default_laser;
        private System.Windows.Forms.Button btn_default_laser_save;
        private System.Windows.Forms.TextBox tbox_default_laser_cst_pitch_reverse;
        private System.Windows.Forms.TextBox tbox_default_laser_cst_pitch_normal;
        private System.Windows.Forms.TextBox tbox_default_laser_ontime;
        private System.Windows.Forms.Label lb_default_laser_cst_pitch_reverse;
        private System.Windows.Forms.Label lb_default_laser_cst_pitch_normal;
        private System.Windows.Forms.Label lb_default_laser_ontime;
    }
}
