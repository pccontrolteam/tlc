﻿namespace DIT.TLC.UI
{
    partial class Parameter_location
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.tc_iostatus_info = new System.Windows.Forms.TabControl();
            this.tp_CasseteLoad = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CasseteLoad_A_BCSD = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_A_BCSU = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_A_BCUG = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_A_BCG = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_A_LTSD = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_A_LTSU = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_A_LUG = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_A_LG = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_A_TCSD = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_A_TCSU = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_A_TCUG = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_A_TCG = new System.Windows.Forms.Button();
            this.gbox_CasseteLoad_Move = new System.Windows.Forms.GroupBox();
            this.gbox_CasseteLoad_MoveB = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CasseteLoad_MoveB_Z2CellOut = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_MoveB_Z2Tilt = new System.Windows.Forms.Button();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CasseteLoad_MoveB_Z2Out = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_MoveB_Z2InWait = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_MoveB_Z2In = new System.Windows.Forms.Button();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CasseteLoad_MoveB_T4Out = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_MoveB_T3Move = new System.Windows.Forms.Button();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CasseteLoad_MoveB_T4Move = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_MoveB_T3In = new System.Windows.Forms.Button();
            this.gbox_CasseteLoad_MoveA = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CasseteLoad_MoveA_Z1CellOut = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_MoveA_Z1Tilt = new System.Windows.Forms.Button();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CasseteLoad_MoveA_Z1Out = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_MoveA_Z1InWait = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_MoveA_Z1In = new System.Windows.Forms.Button();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CasseteLoad_MoveA_T2Out = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_MoveA_T1Move = new System.Windows.Forms.Button();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CasseteLoad_MoveA_T2Move = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_MoveA_T1In = new System.Windows.Forms.Button();
            this.gbox_CasseteLoad_B = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CasseteLoad_B_BCSD = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_B_BCSU = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_B_BCUG = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_B_BCG = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_B_LTSD = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_B_LTSU = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_B_LUG = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_B_LG = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_B_TCSD = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_B_TCSU = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_B_TCUG = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_B_TCG = new System.Windows.Forms.Button();
            this.gbox_CasseteLoad_A = new System.Windows.Forms.GroupBox();
            this.btn_CasseteLoad_Save = new System.Windows.Forms.Button();
            this.gbox_CasseteLoad_Muting = new System.Windows.Forms.GroupBox();
            this.btn_CasseteLoad_Muting4 = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_Muting2 = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_Muting3 = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_Muting1 = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_MutingOff = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_MutingIn = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_SpeedSetting = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_AllSetting = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_LocationSetting = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_MoveLocation = new System.Windows.Forms.Button();
            this.tbox_CasseteLoad_Location = new System.Windows.Forms.TextBox();
            this.tbox_CasseteLoad_Speed = new System.Windows.Forms.TextBox();
            this.tbox_CasseteLoad_CurrentLocation = new System.Windows.Forms.TextBox();
            this.tbox_CasseteLoad_SelectedShaft = new System.Windows.Forms.TextBox();
            this.lbl_CasseteLoad_Location = new System.Windows.Forms.Label();
            this.lbl_CasseteLoad_Speed = new System.Windows.Forms.Label();
            this.lbl_CasseteLoad_SelectedShaft = new System.Windows.Forms.Label();
            this.tbox_CasseteLoad_LocationSetting = new System.Windows.Forms.TextBox();
            this.lbl_CasseteLoad_CurrentLocation = new System.Windows.Forms.Label();
            this.btn_CasseteLoad_GrabSwitch3 = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_GrabSwitch2 = new System.Windows.Forms.Button();
            this.btn_CasseteLoad_GrabSwitch1 = new System.Windows.Forms.Button();
            this.lv_CasseteLoad = new System.Windows.Forms.ListView();
            this.col_CasseteLoad_NamebyLocation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_CasseteLoad_Shaft = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_CasseteLoad_Location = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_CasseteLoad_Speed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tp_Cellpurge = new System.Windows.Forms.TabPage();
            this.gbox_Cellpurge_Move = new System.Windows.Forms.GroupBox();
            this.gbox_Cellpurge_MoveCenter = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel21 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_Cellpurge_MoveCenter_Y2C = new System.Windows.Forms.Button();
            this.btn_Cellpurge_MoveCenter_BBuffer = new System.Windows.Forms.Button();
            this.btn_Cellpurge_MoveCenter_Y2Uld = new System.Windows.Forms.Button();
            this.btn_Cellpurge_MoveCenter_Y1C = new System.Windows.Forms.Button();
            this.btn_Cellpurge_MoveCenter_ABuffer = new System.Windows.Forms.Button();
            this.btn_Cellpurge_MoveCenter_Y1Uld = new System.Windows.Forms.Button();
            this.gbox_Cellpurge_MoveB = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel20 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_Cellpurge_MoveB_X2BC = new System.Windows.Forms.Button();
            this.btn_Cellpurge_MoveB_X2BW = new System.Windows.Forms.Button();
            this.btn_Cellpurge_MoveB_X1BW = new System.Windows.Forms.Button();
            this.btn_Cellpurge_MoveB_X1BC = new System.Windows.Forms.Button();
            this.gbox_Cellpurge_MoveA = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel19 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_Cellpurge_MoveA_X2AC = new System.Windows.Forms.Button();
            this.btn_Cellpurge_MoveA_X2AW = new System.Windows.Forms.Button();
            this.btn_Cellpurge_MoveA_X1AW = new System.Windows.Forms.Button();
            this.btn_Cellpurge_MoveA_X1AC = new System.Windows.Forms.Button();
            this.gbox_Cellpurge_B = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel18 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_Cellpurge_B_DestructionOn = new System.Windows.Forms.Button();
            this.btn_Cellpurge_B_DestructionOff = new System.Windows.Forms.Button();
            this.btn_Cellpurge_B_PneumaticOff = new System.Windows.Forms.Button();
            this.btn_Cellpurge_B_PneumaticOn = new System.Windows.Forms.Button();
            this.gbox_Cellpurge_A = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel17 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_Cellpurge_A_DestructionOn = new System.Windows.Forms.Button();
            this.btn_Cellpurge_A_DestructionOff = new System.Windows.Forms.Button();
            this.btn_Cellpurge_A_PneumaticOff = new System.Windows.Forms.Button();
            this.btn_Cellpurge_A_PneumaticOn = new System.Windows.Forms.Button();
            this.btn_Cellpurge_Save = new System.Windows.Forms.Button();
            this.gbox_Cellpurge_Ionizer = new System.Windows.Forms.GroupBox();
            this.btn_Cellpurge_DestructionOff = new System.Windows.Forms.Button();
            this.btn_Cellpurge_IonizerOff = new System.Windows.Forms.Button();
            this.btn_Cellpurge_DestructionOn = new System.Windows.Forms.Button();
            this.btn_Cellpurge_IonizerOn = new System.Windows.Forms.Button();
            this.btn_Cellpurge_SpeedSetting = new System.Windows.Forms.Button();
            this.btn_Cellpurge_AllSetting = new System.Windows.Forms.Button();
            this.btn_Cellpurge_LocationSetting = new System.Windows.Forms.Button();
            this.btn_Cellpurge_MoveLocation = new System.Windows.Forms.Button();
            this.tbox_Cellpurge_Location = new System.Windows.Forms.TextBox();
            this.tbox_Cellpurge_Speed = new System.Windows.Forms.TextBox();
            this.tbox_Cellpurge_CurrentLocation = new System.Windows.Forms.TextBox();
            this.tbox_Cellpurge_SelectedShaft = new System.Windows.Forms.TextBox();
            this.lbl_Cellpurge_Location = new System.Windows.Forms.Label();
            this.lbl_Cellpurge_Speed = new System.Windows.Forms.Label();
            this.lbl_Cellpurge_SelectedShaft = new System.Windows.Forms.Label();
            this.tbox_Cellpurge_LocationSetting = new System.Windows.Forms.TextBox();
            this.lbl_Cellpurge_CurrentLocation = new System.Windows.Forms.Label();
            this.btn_Cellpurge_GrabSwitch3 = new System.Windows.Forms.Button();
            this.btn_Cellpurge_GrabSwitch2 = new System.Windows.Forms.Button();
            this.btn_Cellpurge_GrabSwitch1 = new System.Windows.Forms.Button();
            this.lv_Cellpurge = new System.Windows.Forms.ListView();
            this.col_Cellpurge_NamebyLocation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_Cellpurge_Shaft = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_Cellpurge_Location = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_Cellpurge_Speed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tp_CellLoad = new System.Windows.Forms.TabPage();
            this.gbox_CellLoad_Move = new System.Windows.Forms.GroupBox();
            this.gbox_CellLoad_MoveLorUn = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel28 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CellLoad_MoveLorUn_PreAlignMark1 = new System.Windows.Forms.Button();
            this.btn_CellLoad_MoveLorUn_PreAlignMark2 = new System.Windows.Forms.Button();
            this.btn_CellLoad_MoveLorUn_Y1UnL = new System.Windows.Forms.Button();
            this.btn_CellLoad_MoveLorUn_Y1L = new System.Windows.Forms.Button();
            this.btn_CellLoad_MoveLorUn_X2L = new System.Windows.Forms.Button();
            this.btn_CellLoad_MoveLorUn_X1L = new System.Windows.Forms.Button();
            this.gbox_CellLoad_MoveB = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel31 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel32 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CellLoad_MoveB_BPickerM90Angle = new System.Windows.Forms.Button();
            this.btn_CellLoad_MoveB_BPickerP90Angle = new System.Windows.Forms.Button();
            this.btn_CellLoad_MoveB_BPicker0Angle = new System.Windows.Forms.Button();
            this.tableLayoutPanel33 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CellLoad_MoveB_BPickerUnL = new System.Windows.Forms.Button();
            this.btn_CellLoad_MoveB_BPickerL = new System.Windows.Forms.Button();
            this.tableLayoutPanel34 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CellLoad_MoveB_X2BStageUnL = new System.Windows.Forms.Button();
            this.btn_CellLoad_MoveB_X1BStageUnL = new System.Windows.Forms.Button();
            this.gbox_CellLoad_MoveA = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel26 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel30 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CellLoad_MoveA_APickerM90Angle = new System.Windows.Forms.Button();
            this.btn_CellLoad_MoveA_APickerP90Angle = new System.Windows.Forms.Button();
            this.btn_CellLoad_MoveA_APicker0Angle = new System.Windows.Forms.Button();
            this.tableLayoutPanel29 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CellLoad_MoveA_APickerUnL = new System.Windows.Forms.Button();
            this.btn_CellLoad_MoveA_APickerL = new System.Windows.Forms.Button();
            this.tableLayoutPanel27 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CellLoad_MoveA_X2AStageUnL = new System.Windows.Forms.Button();
            this.btn_CellLoad_MoveA_X1AStageUnL = new System.Windows.Forms.Button();
            this.gbox_CellLoad_B = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel25 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CellLoad_B_PickerDestructionOn = new System.Windows.Forms.Button();
            this.btn_CellLoad_B_PickerDestructionOff = new System.Windows.Forms.Button();
            this.btn_CellLoad_B_PickerUp = new System.Windows.Forms.Button();
            this.btn_CellLoad_B_PickerDown = new System.Windows.Forms.Button();
            this.btn_CellLoad_B_PickerPneumaticOn = new System.Windows.Forms.Button();
            this.btn_CellLoad_B_PickerPneumaticOff = new System.Windows.Forms.Button();
            this.gbox_CellLoad_A = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel24 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CellLoad_A_PickerDestructionOn = new System.Windows.Forms.Button();
            this.btn_CellLoad_A_PickerDestructionOff = new System.Windows.Forms.Button();
            this.btn_CellLoad_A_PickerUp = new System.Windows.Forms.Button();
            this.btn_CellLoad_A_PickerDown = new System.Windows.Forms.Button();
            this.btn_CellLoad_A_PickerPneumaticOn = new System.Windows.Forms.Button();
            this.btn_CellLoad_A_PickerPneumaticOff = new System.Windows.Forms.Button();
            this.btn_CellLoad_Save = new System.Windows.Forms.Button();
            this.btn_CellLoad_SpeedSetting = new System.Windows.Forms.Button();
            this.btn_CellLoad_AllSetting = new System.Windows.Forms.Button();
            this.btn_CellLoad_LocationSetting = new System.Windows.Forms.Button();
            this.btn_CellLoad_MoveLocation = new System.Windows.Forms.Button();
            this.tbox_CellLoad_Location = new System.Windows.Forms.TextBox();
            this.tbox_CellLoad_Speed = new System.Windows.Forms.TextBox();
            this.tbox_CellLoad_CurrentLocation = new System.Windows.Forms.TextBox();
            this.tbox_CellLoad_SelectedShaft = new System.Windows.Forms.TextBox();
            this.lbl_CellLoad_Location = new System.Windows.Forms.Label();
            this.lbl_CellLoad_Speed = new System.Windows.Forms.Label();
            this.lbl_CellLoad_SelectedShaft = new System.Windows.Forms.Label();
            this.tbox_CellLoad_LocationSetting = new System.Windows.Forms.TextBox();
            this.lbl_CellLoad_CurrentLocation = new System.Windows.Forms.Label();
            this.btn_CellLoad_GrabSwitch3 = new System.Windows.Forms.Button();
            this.btn_CellLoad_GrabSwitch2 = new System.Windows.Forms.Button();
            this.btn_CellLoad_GrabSwitch1 = new System.Windows.Forms.Button();
            this.lv_CellLoad = new System.Windows.Forms.ListView();
            this.col_CellLoad_NamebyLocation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_CellLoad_Shaft = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_CellLoad_Location = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_CellLoad_Speed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tp_IRCutProcess = new System.Windows.Forms.TabPage();
            this.gbox_IRCutProcess_Ionizer = new System.Windows.Forms.GroupBox();
            this.btn_IRCutProcess_Destruction = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_IonizerOff = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_DestructionOn = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_IonizerOn = new System.Windows.Forms.Button();
            this.gbox_IRCutProcess_Move = new System.Windows.Forms.GroupBox();
            this.gbox_IRCutProcess_RightOffset = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel40 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_IRCutProcess_RightOffset_Right_Vision2Focus = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_RightOffset_Right_Focus2Vision = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_RightOffset_Left_Vision2Focus = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_RightOffset_Left_Focus2Vision = new System.Windows.Forms.Button();
            this.gbox_IRCutProcess_LeftOffset = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel39 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_IRCutProcess_LeftOffset_Right_Visoin2Focus = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_LeftOffset_Right_Focus2Visoin = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_LeftOffset_Left_Vision2Focus = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_LeftOffset_Left_Focus2Vision = new System.Windows.Forms.Button();
            this.gbox_IRCutProcess_MoveB = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel38 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_IRCutProcess_MoveB_R1Camera = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_MoveB_R2Camera = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_MoveB_RightCellLoad = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_MoveB_RightCellUnload = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_MoveB_R1Laser = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_MoveB_R2Laser = new System.Windows.Forms.Button();
            this.gbox_IRCutProcess_MoveA = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel37 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_IRCutProcess_MoveA_L1Camera = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_MoveA_L2Camera = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_MoveA_LeftCellLoad = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_MoveA_LeftCellUnload = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_MoveA_L1Laser = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_MoveA_L2Laser = new System.Windows.Forms.Button();
            this.gbox_IRCutProcess_B = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel36 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1Off = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1On = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1Off = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2Off = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2On = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2Off = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2On = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1On = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_R1Destruction_Ch1On = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_R1Destruction_Ch1Off = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_R2Destruction_Ch1On = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_R2Destruction_Ch1Off = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_R1Destruction_Ch2On = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_R1Destruction_Ch2Off = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_R2Destruction_Ch2Off = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_R2Destruction_Ch2On = new System.Windows.Forms.Button();
            this.gbox_IRCutProcess_A = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel35 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_IRCutProcess_A_L1Destruction_Ch2Off = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_L1Destruction_Ch2On = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_L2Destruction_Ch2Off = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_L2Destruction_Ch2On = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1Off = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1On = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1Off = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2Off = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2On = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2Off = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2On = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1On = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_L1Destruction_Ch1On = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_L1Destruction_Ch1Off = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_L2Destruction_Ch1On = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_A_L2Destruction_Ch1Off = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_Save = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_SpeedSetting = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_AllSetting = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_LocationSetting = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_MoveLocation = new System.Windows.Forms.Button();
            this.tbox_IRCutProcess_Location = new System.Windows.Forms.TextBox();
            this.tbox_IRCutProcess_Speed = new System.Windows.Forms.TextBox();
            this.tbox_IRCutProcess_CurrentLocation = new System.Windows.Forms.TextBox();
            this.tbox_IRCutProcess_SelectedShaft = new System.Windows.Forms.TextBox();
            this.lbl_IRCutProcess_Location = new System.Windows.Forms.Label();
            this.lbl_IRCutProcess_Speed = new System.Windows.Forms.Label();
            this.lbl_IRCutProcess_SelectedShaft = new System.Windows.Forms.Label();
            this.tbox_IRCutProcess_LocationSetting = new System.Windows.Forms.TextBox();
            this.lbl_IRCutProcess_CurrentLocation = new System.Windows.Forms.Label();
            this.btn_IRCutProcess_GrabSwitch3 = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_GrabSwitch2 = new System.Windows.Forms.Button();
            this.btn_IRCutProcess_GrabSwitch1 = new System.Windows.Forms.Button();
            this.lv_IRCutProcess = new System.Windows.Forms.ListView();
            this.col_IRCutProcess_NamebyLocation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_IRCutProcess_Shaft = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_IRCutProcess_Location = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_IRCutProcess_Speed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tp_BreakTransfer = new System.Windows.Forms.TabPage();
            this.gbox_BreakTransfer_Move = new System.Windows.Forms.GroupBox();
            this.gbox_BreakTransfer_MoveB = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel44 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_BreakTransfer_MoveB_Unload = new System.Windows.Forms.Button();
            this.btn_BreakTransfer_MoveB_Load = new System.Windows.Forms.Button();
            this.gbox_BreakTransfer_MoveA = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel43 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_BreakTransfer_MoveA_Unload = new System.Windows.Forms.Button();
            this.btn_BreakTransfer_MoveA_Load = new System.Windows.Forms.Button();
            this.gbox_BreakTransfer_B = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel42 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_BreakTransfer_B_PickerDestructionOn = new System.Windows.Forms.Button();
            this.btn_BreakTransfer_B_PickerDestructionOff = new System.Windows.Forms.Button();
            this.btn_BreakTransfer_B_PickerUp = new System.Windows.Forms.Button();
            this.btn_BreakTransfer_B_PickerDown = new System.Windows.Forms.Button();
            this.btn_BreakTransfer_B_PickerPneumaticOn = new System.Windows.Forms.Button();
            this.btn_BreakTransfer_B_PneumaticOff = new System.Windows.Forms.Button();
            this.gbox_BreakTransfer_A = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel41 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_BreakTransfer_A_PickerDestructionOn = new System.Windows.Forms.Button();
            this.btn_BreakTransfer_A_PickerDestructionOff = new System.Windows.Forms.Button();
            this.btn_BreakTransfer_A_PickerUp = new System.Windows.Forms.Button();
            this.btn_BreakTransfer_A_PickerDown = new System.Windows.Forms.Button();
            this.btn_BreakTransfer_A_PickerPneumaticOn = new System.Windows.Forms.Button();
            this.btn_BreakTransfer_A_PickerPneumaticOff = new System.Windows.Forms.Button();
            this.btn_BreakTransfer_Save = new System.Windows.Forms.Button();
            this.btn_BreakTransfer_SpeedSetting = new System.Windows.Forms.Button();
            this.btn_BreakTransfer_AllSetting = new System.Windows.Forms.Button();
            this.btn_BreakTransfer_LocationSetting = new System.Windows.Forms.Button();
            this.btn_BreakTransfer_MoveLocation = new System.Windows.Forms.Button();
            this.tbox_BreakTransfer_Location = new System.Windows.Forms.TextBox();
            this.tbox_BreakTransfer_Speed = new System.Windows.Forms.TextBox();
            this.tbox_BreakTransfer_CurrentLocation = new System.Windows.Forms.TextBox();
            this.tbox_BreakTransfer_SelectedShaft = new System.Windows.Forms.TextBox();
            this.lbl_BreakTransfer_Location = new System.Windows.Forms.Label();
            this.lbl_BreakTransfer_Speed = new System.Windows.Forms.Label();
            this.lbl_BreakTransfer_SelectedShaft = new System.Windows.Forms.Label();
            this.tbox_BreakTransfer_LocationSetting = new System.Windows.Forms.TextBox();
            this.lbl_BreakTransfer_CurrentLocation = new System.Windows.Forms.Label();
            this.btn_BreakTransfer_GrabSwitch3 = new System.Windows.Forms.Button();
            this.btn_BreakTransfer_GrabSwitch2 = new System.Windows.Forms.Button();
            this.btn_BreakTransfer_GrabSwitch1 = new System.Windows.Forms.Button();
            this.lv_BreakTransfer = new System.Windows.Forms.ListView();
            this.col_BreakTransfer_NamebyLocation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_BreakTransfer_Shaft = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_BreakTransfer_Location = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_BreakTransfer_Speed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tp_BreakUnitXZ = new System.Windows.Forms.TabPage();
            this.gbox_BreakUnitXZ_Move = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel45 = new System.Windows.Forms.TableLayoutPanel();
            this.gbox_BreakUnitXZ_MoveZ4 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel49 = new System.Windows.Forms.TableLayoutPanel();
            this.gbox_BreakUnitXZ_MoveZ4_SlowDescent = new System.Windows.Forms.Button();
            this.gbox_BreakUnitXZ_MoveZ4_VisionCheck = new System.Windows.Forms.Button();
            this.gbox_BreakUnitXZ_MoveZ4_FastDescent = new System.Windows.Forms.Button();
            this.gbox_BreakUnitXZ_MoveZ3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel48 = new System.Windows.Forms.TableLayoutPanel();
            this.gbox_BreakUnitXZ_MoveZ3_SlowDescent = new System.Windows.Forms.Button();
            this.gbox_BreakUnitXZ_MoveZ3_VisionCheck = new System.Windows.Forms.Button();
            this.gbox_BreakUnitXZ_MoveZ3_FastDescent = new System.Windows.Forms.Button();
            this.gbox_BreakUnitXZ_MoveZ2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel47 = new System.Windows.Forms.TableLayoutPanel();
            this.gbox_BreakUnitXZ_MoveZ2_SlowDescent = new System.Windows.Forms.Button();
            this.gbox_BreakUnitXZ_MoveZ2_VisionCheck = new System.Windows.Forms.Button();
            this.gbox_BreakUnitXZ_MoveZ2_FastDescent = new System.Windows.Forms.Button();
            this.gbox_BreakUnitXZ_MoveZ1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel46 = new System.Windows.Forms.TableLayoutPanel();
            this.gbox_BreakUnitXZ_MoveZ1_SlowDescent = new System.Windows.Forms.Button();
            this.gbox_BreakUnitXZ_MoveZ1_VisionCheck = new System.Windows.Forms.Button();
            this.gbox_BreakUnitXZ_MoveZ1_FastDescent = new System.Windows.Forms.Button();
            this.gbox_BreakUnitXZ_Save = new System.Windows.Forms.Button();
            this.btn_BreakUnitXZ_SpeedSetting = new System.Windows.Forms.Button();
            this.btn_BreakUnitXZ_AllSetting = new System.Windows.Forms.Button();
            this.btn_BreakUnitXZ_LocationSetting = new System.Windows.Forms.Button();
            this.btn_BreakUnitXZ_MoveLocation = new System.Windows.Forms.Button();
            this.tbox_BreakUnitXZ_Location = new System.Windows.Forms.TextBox();
            this.tbox_BreakUnitXZ_Speed = new System.Windows.Forms.TextBox();
            this.tbox_BreakUnitXZ_CurrentLocation = new System.Windows.Forms.TextBox();
            this.tbox_BreakUnitXZ_SelectedShaft = new System.Windows.Forms.TextBox();
            this.lbl_BreakUnitXZ_Location = new System.Windows.Forms.Label();
            this.lbl_BreakUnitXZ_Speed = new System.Windows.Forms.Label();
            this.lbl_BreakUnitXZ_SelectedShaft = new System.Windows.Forms.Label();
            this.tbox_BreakUnitXZ_LocationSetting = new System.Windows.Forms.TextBox();
            this.lbl_BreakUnitXZ_CurrentLocation = new System.Windows.Forms.Label();
            this.btn_BreakUnitXZ_GrabSwitch3 = new System.Windows.Forms.Button();
            this.btn_BreakUnitXZ_GrabSwitch2 = new System.Windows.Forms.Button();
            this.btn_BreakUnitXZ_GrabSwitch1 = new System.Windows.Forms.Button();
            this.lv_BreakUnitXZ = new System.Windows.Forms.ListView();
            this.col_BreakUnitXZ_NamebyLocation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_BreakUnitXZ_Shaft = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_BreakUnitXZ_Location = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_BreakUnitXZ_Speed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tp_BreakUnitTY = new System.Windows.Forms.TabPage();
            this.gbox_BreakUnitTY_Shutter = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel53 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_BreakUnitTY_Shutter_Close = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_Shutter_Open = new System.Windows.Forms.Button();
            this.gbox_BreakUnitTY_DummyBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel52 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_BreakUnitTY_DummyBox_Output = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_DummyBox_Input = new System.Windows.Forms.Button();
            this.gbox_BreakUnitTY_Ionizer = new System.Windows.Forms.GroupBox();
            this.btn_BreakUnitTY_DestructionOff = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_IonizerOff = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_DestructionOn = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_IonizerOn = new System.Windows.Forms.Button();
            this.gbox_BreakUnitTY_Move = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel54 = new System.Windows.Forms.TableLayoutPanel();
            this.gbox_BreakUnitTY_Theta = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel57 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_BreakUnitTY_Theta_T2Wait = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_Theta_T4Wait = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_Theta_T1Wait = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_Theta_T3Wait = new System.Windows.Forms.Button();
            this.gbox_BreakUnitTY_MoveB = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel56 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_BreakUnitTY_MoveB_Unload = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_MoveB_Load = new System.Windows.Forms.Button();
            this.gbox_BreakUnitTY_MoveA = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel55 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_BreakUnitTY_MoveA_Unload = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_MoveA_Load = new System.Windows.Forms.Button();
            this.gbox_BreakUnitTY_B = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel50 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_BreakUnitTY_B_1Destruction_On = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_B_1Destruction_Off = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_B_2Destruction_On = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_B_2Destruction_Off = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_B_1Pneumatic_Off = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_B_1Pneumatic_On = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_B_2Pneumatic_On = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_B_2Pneumatic_Off = new System.Windows.Forms.Button();
            this.gbox_BreakUnitTY_A = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel51 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_BreakUnitTY_A_1Destruction_On = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_A_1Destruction_Off = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_A_2Destruction_On = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_A_2Destruction_Off = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_A_1Pneumatic_Off = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_A_1Pneumatic_On = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_A_2Pneumatic_On = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_A_2Pneumatic_Off = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_Save = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_SpeedSetting = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_AllSetting = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_LocationSetting = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_MoveLocation = new System.Windows.Forms.Button();
            this.tbox_BreakUnitTY_Location = new System.Windows.Forms.TextBox();
            this.tbox_BreakUnitTY_Speed = new System.Windows.Forms.TextBox();
            this.tbox_BreakUnitTY_CurrentLocation = new System.Windows.Forms.TextBox();
            this.tbox_BreakUnitTY_SelectedShaft = new System.Windows.Forms.TextBox();
            this.lbl_BreakUnitTY_Location = new System.Windows.Forms.Label();
            this.lbl_BreakUnitTY_Speed = new System.Windows.Forms.Label();
            this.lbl_BreakUnitTY_SelectedShaft = new System.Windows.Forms.Label();
            this.tbox_BreakUnitTY_LocationSetting = new System.Windows.Forms.TextBox();
            this.lbl_BreakUnitTY_CurrentLocation = new System.Windows.Forms.Label();
            this.btn_BreakUnitTY_GrabSwitch3 = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_GrabSwitch2 = new System.Windows.Forms.Button();
            this.btn_BreakUnitTY_GrabSwitch1 = new System.Windows.Forms.Button();
            this.lv_BreakUnitTY = new System.Windows.Forms.ListView();
            this.col_BreakUnitTY_NamebyLocation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_BreakUnitTY_Shaft = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_BreakUnitTY_Location = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_BreakUnitTY_Speed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tp_UnloaderTransfer = new System.Windows.Forms.TabPage();
            this.gbox_UnloaderTransfer_Move = new System.Windows.Forms.GroupBox();
            this.gbox_UnloaderTransfer_MoveB = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel61 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel62 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_UnloaderTransfer_MoveB_T40Angle = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_MoveB_T4P90Angle = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_MoveB_T4M90Angle = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_MoveB_T30Angle = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_MoveB_T3P90Angle = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_MoveB_T3M90Angle = new System.Windows.Forms.Button();
            this.tableLayoutPanel63 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_UnloaderTransfer_MoveB_BStage = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_MoveB_BUnloading = new System.Windows.Forms.Button();
            this.gbox_UnloaderTransfer_MoveA = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel58 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel60 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_UnloaderTransfer_MoveA_T20Angle = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_MoveA_T2P90Angle = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_MoveA_T2M90Angle = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_MoveA_T10Angle = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_MoveA_T1P90Angle = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_MoveA_T1M90Angle = new System.Windows.Forms.Button();
            this.tableLayoutPanel59 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_UnloaderTransfer_MoveA_AStage = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_MoveA_AUnloading = new System.Windows.Forms.Button();
            this.gbox_UnloaderTransfer_B = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_UnloaderTransfer_B_PickerDestruction2 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_B_PickerDestructionOn2 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_B_PickerDestructionOff1 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_B_PickerDestructionOn1 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_B_PickerPneumaticOff2 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_B_PickerPneumaticOn2 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_B_PickerPneumaticOff1 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_B_PickerPneumaticOn1 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_B_PickerDown2 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_B_PickerUp2 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_B_PickerDown1 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_B_PickerUp1 = new System.Windows.Forms.Button();
            this.gbox_UnloaderTransfer_A = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_UnloaderTransfer_A_PickerDestructionOff2 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_A_PickerDestructionOn2 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_A_PickerDestructionOff1 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_A_PickerDestructionOn1 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_A_PickerPneumaticOff2 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_A_PickerPneumaticOn2 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_A_PickerPneumaticOff1 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_A_PickerPneumaticOn1 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_A_PickerDown2 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_A_PickerUp2 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_A_PickerDown1 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_A_PickerUp1 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_Save = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_SpeedSetting = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_AllSetting = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_LocationSetting = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_MoveLocation = new System.Windows.Forms.Button();
            this.tbox_UnloaderTransfer_Location = new System.Windows.Forms.TextBox();
            this.tbox_UnloaderTransfer_Speed = new System.Windows.Forms.TextBox();
            this.tbox_UnloaderTransfer_CurrentLocation = new System.Windows.Forms.TextBox();
            this.tbox_UnloaderTransfer_SelectedShaft = new System.Windows.Forms.TextBox();
            this.lbl_UnloaderTransfer_Location = new System.Windows.Forms.Label();
            this.lbl_UnloaderTransfer_Speed = new System.Windows.Forms.Label();
            this.lbl_UnloaderTransfer_SeletedShaft = new System.Windows.Forms.Label();
            this.tbox_UnloaderTransfer_LocationSetting = new System.Windows.Forms.TextBox();
            this.lbl_UnloaderTransfer_CurrentLocation = new System.Windows.Forms.Label();
            this.btn_UnloaderTransfer_GrabSwitch3 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_GrabSwitch2 = new System.Windows.Forms.Button();
            this.btn_UnloaderTransfer_GrabSwitch1 = new System.Windows.Forms.Button();
            this.lv_UnloaderTransfer = new System.Windows.Forms.ListView();
            this.col_UnloaderTransfer_NamebyLocation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_UnloaderTransfer_Shaft = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_UnloaderTransfer_Location = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_UnloaderTransfer_Speed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tp_CameraUnit = new System.Windows.Forms.TabPage();
            this.gbox_CameraUnit_Move = new System.Windows.Forms.GroupBox();
            this.gbox_CameraUnit_MoveB = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel65 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CameraUnit_MoveB_InspectionMove4 = new System.Windows.Forms.Button();
            this.btn_CameraUnit_MoveB_InspectionMove3 = new System.Windows.Forms.Button();
            this.btn_CameraUnit_MoveB_MCRMove = new System.Windows.Forms.Button();
            this.gbox_CameraUnit_MoveA = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel64 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CameraUnit_MoveA_InspectionMove2 = new System.Windows.Forms.Button();
            this.btn_CameraUnit_MoveA_InspectionMove1 = new System.Windows.Forms.Button();
            this.btn_CameraUnit_MoveA_MCRMove = new System.Windows.Forms.Button();
            this.btn_CameraUnit_Save = new System.Windows.Forms.Button();
            this.btn_CameraUnit_SpeedSetting = new System.Windows.Forms.Button();
            this.btn_CameraUnit_AllSetting = new System.Windows.Forms.Button();
            this.btn_CameraUnit_LocationSetting = new System.Windows.Forms.Button();
            this.btn_CameraUnit_MoveLocation = new System.Windows.Forms.Button();
            this.tbox_CameraUnit_Location = new System.Windows.Forms.TextBox();
            this.tbox_CameraUnit_Speed = new System.Windows.Forms.TextBox();
            this.tbox_CameraUnit_CurrentLocation = new System.Windows.Forms.TextBox();
            this.tbox_CameraUnit_SelectedShaft = new System.Windows.Forms.TextBox();
            this.lbl_CameraUnit_Location = new System.Windows.Forms.Label();
            this.lbl_CameraUnit_Speed = new System.Windows.Forms.Label();
            this.lbl_CameraUnit_SelectedShaft = new System.Windows.Forms.Label();
            this.tbox_CameraUnit_LocationSetting = new System.Windows.Forms.TextBox();
            this.lbl_CameraUnit_CurrentLocation = new System.Windows.Forms.Label();
            this.btn_CameraUnit_GrabSwitch3 = new System.Windows.Forms.Button();
            this.btn_CameraUnit_GrabSwitch2 = new System.Windows.Forms.Button();
            this.btn_CameraUnit_GrabSwitch1 = new System.Windows.Forms.Button();
            this.lv_CameraUnit = new System.Windows.Forms.ListView();
            this.col_CameraUnit_NamebyLocation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_CameraUnit_Shaft = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_CameraUnit_Location = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_CameraUnit_Speed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tp_CellInput = new System.Windows.Forms.TabPage();
            this.gbox_CellInput_Buffer = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel66 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CellInput_Buffer_DestructionOn = new System.Windows.Forms.Button();
            this.btn_CellInput_Buffer_DestructionOff = new System.Windows.Forms.Button();
            this.btn_CellInput_Buffer_PickerUp = new System.Windows.Forms.Button();
            this.btn_CellInput_Buffer_PickerDown = new System.Windows.Forms.Button();
            this.btn_CellInput_Buffer_PneumaticOn = new System.Windows.Forms.Button();
            this.btn_CellInput_Buffer_PneumaticOff = new System.Windows.Forms.Button();
            this.gbox_CellInput_Ionizer = new System.Windows.Forms.GroupBox();
            this.btn_CellInput_DestructionOff = new System.Windows.Forms.Button();
            this.btn_CellInput_IonizerOff = new System.Windows.Forms.Button();
            this.btn_CellInput_DestructionOn = new System.Windows.Forms.Button();
            this.btn_CellInput_IonizerOn = new System.Windows.Forms.Button();
            this.gbox_CellInput_Move = new System.Windows.Forms.GroupBox();
            this.gbox_CellInput_MoveCenter = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel69 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CellInput_MoveCenter_ABuffer = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveCenter_BBuffer = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveCenter_Y1CstMid = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveCenter_Y2CstMid = new System.Windows.Forms.Button();
            this.gbox_CellInput_MoveB = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel68 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CellInput_MoveB_X2BUld = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveB_Y2BUld = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveB_X2BCstMid = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveB_X2BCstWait = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveB_X1AUld = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveB_Y1BUld = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveB_X1BCstMId = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveB_X1BCstWait = new System.Windows.Forms.Button();
            this.gbox_CellInput_MoveA = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel67 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CellInput_MoveA_X2AUld = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveA_Y2AUld = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveA_X2ACstMid = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveA_X2ACstWait = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveA_X1AUld = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveA_Y1AUld = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveA_X1ACstMid = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveA_X1ACstWait = new System.Windows.Forms.Button();
            this.gbox_CellInput_B = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel23 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CellInput_B_DestructionOn = new System.Windows.Forms.Button();
            this.btn_CellInput_B_DestructionOff = new System.Windows.Forms.Button();
            this.btn_CellInput_B_PneumaticOff = new System.Windows.Forms.Button();
            this.btn_CellInput_B_PneumaticOn = new System.Windows.Forms.Button();
            this.gbox_CellInput_A = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel22 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CellInput_A_DestructionOn = new System.Windows.Forms.Button();
            this.btn_CellInput_A_DestructionOff = new System.Windows.Forms.Button();
            this.btn_CellInput_A_PneumaticOff = new System.Windows.Forms.Button();
            this.btn_CellInput_A_PneumaticOn = new System.Windows.Forms.Button();
            this.btn_CellInput_Save = new System.Windows.Forms.Button();
            this.btn_CellInput_SpeedSetting = new System.Windows.Forms.Button();
            this.btn_CellInput_AllSetting = new System.Windows.Forms.Button();
            this.btn_CellInput_LocationSetting = new System.Windows.Forms.Button();
            this.btn_CellInput_MoveLocation = new System.Windows.Forms.Button();
            this.tbox_CellInput_Location = new System.Windows.Forms.TextBox();
            this.tbox_CellInput_Speed = new System.Windows.Forms.TextBox();
            this.tbox_CellInput_CurrentLocation = new System.Windows.Forms.TextBox();
            this.tbox_CellInput_SelectedShaft = new System.Windows.Forms.TextBox();
            this.lbl_CellInput_Location = new System.Windows.Forms.Label();
            this.lbl_CellInput_Speed = new System.Windows.Forms.Label();
            this.lbl_CellInput_SelectedShaft = new System.Windows.Forms.Label();
            this.tbox_CellInput_LocationSetting = new System.Windows.Forms.TextBox();
            this.lbl_CellInput_CurrentLocation = new System.Windows.Forms.Label();
            this.btn_CellInput_GrabSwitch3 = new System.Windows.Forms.Button();
            this.btn_CellInput_GrabSwitch2 = new System.Windows.Forms.Button();
            this.btn_CellInput_GrabSwitch1 = new System.Windows.Forms.Button();
            this.lv_CellInput = new System.Windows.Forms.ListView();
            this.col_CellInput_NamebyLocation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_CellInput_Shaft = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_CellInput_Location = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_CellInput_Speed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tp_CasseteUnload = new System.Windows.Forms.TabPage();
            this.gbox_CasseteUnload_Move = new System.Windows.Forms.GroupBox();
            this.gbox_CasseteUnload_MoveB = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel75 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel76 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CasseteUnload_MoveB_LiftZ2CellOut = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_MoveB_LiftZ2CstTilt = new System.Windows.Forms.Button();
            this.tableLayoutPanel77 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CasseteUnload_MoveB_LiftZ2CstOut = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_MoveB_LiftZ2CstWait = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_MoveB_LiftZ2CstIn = new System.Windows.Forms.Button();
            this.tableLayoutPanel78 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CasseteUnload_MoveB_BottomT4LiftOut = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_MoveB_TopT3CstMove = new System.Windows.Forms.Button();
            this.tableLayoutPanel79 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CasseteUnload_MoveB_BottomT4CstIn = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_MoveB_TopT3Out = new System.Windows.Forms.Button();
            this.gbox_CasseteUnload_MoveA = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel70 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel71 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CasseteUnload_MoveA_LiftZ1CellOut = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_MoveA_LiftZ1CstTilt = new System.Windows.Forms.Button();
            this.tableLayoutPanel72 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CasseteUnload_MoveA_LiftZ1CstOut = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_MoveA_LiftZ1CstWait = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_MoveA_LiftZ1CstIn = new System.Windows.Forms.Button();
            this.tableLayoutPanel73 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CasseteUnload_MoveA_BottomT2Move = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_MoveA_TopT1Move = new System.Windows.Forms.Button();
            this.tableLayoutPanel74 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CasseteUnload_MoveA_BottomT2In = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_MoveA_TopT1Out = new System.Windows.Forms.Button();
            this.gbox_CasseteUnload_B = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CasseteUnload_B_BottomCstStopperDown = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_B_BottomCstStopperUp = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_B_BottomCstUngrib = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_B_BottomCstGrib = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_B_LiftTiltSlinderDown = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_B_LiftTiltSlinderUp = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_B_LiftUngrib = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_B_LiftGrib = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_B_TopCstStopperDown = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_B_TopCstStopperUp = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_B_TopCstUngrib = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_B_TopCstGrib = new System.Windows.Forms.Button();
            this.gbox_CasseteUnload_A = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_CasseteUnload_A_BottomCstStopperDown = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_A_BottomCstStopperUp = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_A_BottomCstUngrib = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_A_BottomCstGrib = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_A_LiftTiltSlinderDown = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_A_LiftTiltSlinderUp = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_A_LiftUngrib = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_A_LiftGrib = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_A_TopCstStopperDown = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_A_TopCstStopperUp = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_A_TopCstUngrib = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_A_TopCstGrib = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_Save = new System.Windows.Forms.Button();
            this.gbox_CasseteUnload = new System.Windows.Forms.GroupBox();
            this.btn_CasseteUnload_Muting4 = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_Muting2 = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_Muting3 = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_Muting1 = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_MutingOut = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_MutingIn = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_SpeedSetting = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_AllSetting = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_LocationSetting = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_MoveLocation = new System.Windows.Forms.Button();
            this.tbox_CasseteUnload_Location = new System.Windows.Forms.TextBox();
            this.tbox_CasseteUnload_Speed = new System.Windows.Forms.TextBox();
            this.tbox_CasseteUnload_CurrentLocation = new System.Windows.Forms.TextBox();
            this.tbox_CasseteUnload_SelectedShaft = new System.Windows.Forms.TextBox();
            this.lbl_CasseteUnload_Location = new System.Windows.Forms.Label();
            this.lbl_CasseteUnload_Speed = new System.Windows.Forms.Label();
            this.lbl_CasseteUnload_SelectedShaft = new System.Windows.Forms.Label();
            this.tbox_CasseteUnload_LocationSetting = new System.Windows.Forms.TextBox();
            this.lbl_CasseteUnload_CurrentLocation = new System.Windows.Forms.Label();
            this.btn_CasseteUnload_GrabSwitch3 = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_GrabSwitch2 = new System.Windows.Forms.Button();
            this.btn_CasseteUnload_GrabSwitch1 = new System.Windows.Forms.Button();
            this.lv_CasseteUnload = new System.Windows.Forms.ListView();
            this.col_CasseteUnload_NamebyLocation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_CasseteUnload_Shaft = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_CasseteUnload_Location = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_CasseteUnload_Speed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ajin_Setting_CasseteLoad = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.ajin_Setting_Cellpurge = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.ajin_Setting_CellLoad = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.ajin_Setting_IRCutProcess = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.ajin_Setting_BreakTransfer = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.ajin_Setting_BreakUnitXZ = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.ajin_Setting_BreakUnitTY = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.ajin_Setting_UnloaderTransfer = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.ajin_Setting_CameraUnit = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.ajin_Setting_CellInput = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.ajin_Setting_CasseteUnload = new DIT.TLC.UI.UcrlAjinServoCtrl();
            this.tc_iostatus_info.SuspendLayout();
            this.tp_CasseteLoad.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.gbox_CasseteLoad_Move.SuspendLayout();
            this.gbox_CasseteLoad_MoveB.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            this.gbox_CasseteLoad_MoveA.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.gbox_CasseteLoad_B.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.gbox_CasseteLoad_Muting.SuspendLayout();
            this.tp_Cellpurge.SuspendLayout();
            this.gbox_Cellpurge_Move.SuspendLayout();
            this.gbox_Cellpurge_MoveCenter.SuspendLayout();
            this.tableLayoutPanel21.SuspendLayout();
            this.gbox_Cellpurge_MoveB.SuspendLayout();
            this.tableLayoutPanel20.SuspendLayout();
            this.gbox_Cellpurge_MoveA.SuspendLayout();
            this.tableLayoutPanel19.SuspendLayout();
            this.gbox_Cellpurge_B.SuspendLayout();
            this.tableLayoutPanel18.SuspendLayout();
            this.gbox_Cellpurge_A.SuspendLayout();
            this.tableLayoutPanel17.SuspendLayout();
            this.gbox_Cellpurge_Ionizer.SuspendLayout();
            this.tp_CellLoad.SuspendLayout();
            this.gbox_CellLoad_Move.SuspendLayout();
            this.gbox_CellLoad_MoveLorUn.SuspendLayout();
            this.tableLayoutPanel28.SuspendLayout();
            this.gbox_CellLoad_MoveB.SuspendLayout();
            this.tableLayoutPanel31.SuspendLayout();
            this.tableLayoutPanel32.SuspendLayout();
            this.tableLayoutPanel33.SuspendLayout();
            this.tableLayoutPanel34.SuspendLayout();
            this.gbox_CellLoad_MoveA.SuspendLayout();
            this.tableLayoutPanel26.SuspendLayout();
            this.tableLayoutPanel30.SuspendLayout();
            this.tableLayoutPanel29.SuspendLayout();
            this.tableLayoutPanel27.SuspendLayout();
            this.gbox_CellLoad_B.SuspendLayout();
            this.tableLayoutPanel25.SuspendLayout();
            this.gbox_CellLoad_A.SuspendLayout();
            this.tableLayoutPanel24.SuspendLayout();
            this.tp_IRCutProcess.SuspendLayout();
            this.gbox_IRCutProcess_Ionizer.SuspendLayout();
            this.gbox_IRCutProcess_Move.SuspendLayout();
            this.gbox_IRCutProcess_RightOffset.SuspendLayout();
            this.tableLayoutPanel40.SuspendLayout();
            this.gbox_IRCutProcess_LeftOffset.SuspendLayout();
            this.tableLayoutPanel39.SuspendLayout();
            this.gbox_IRCutProcess_MoveB.SuspendLayout();
            this.tableLayoutPanel38.SuspendLayout();
            this.gbox_IRCutProcess_MoveA.SuspendLayout();
            this.tableLayoutPanel37.SuspendLayout();
            this.gbox_IRCutProcess_B.SuspendLayout();
            this.tableLayoutPanel36.SuspendLayout();
            this.gbox_IRCutProcess_A.SuspendLayout();
            this.tableLayoutPanel35.SuspendLayout();
            this.tp_BreakTransfer.SuspendLayout();
            this.gbox_BreakTransfer_Move.SuspendLayout();
            this.gbox_BreakTransfer_MoveB.SuspendLayout();
            this.tableLayoutPanel44.SuspendLayout();
            this.gbox_BreakTransfer_MoveA.SuspendLayout();
            this.tableLayoutPanel43.SuspendLayout();
            this.gbox_BreakTransfer_B.SuspendLayout();
            this.tableLayoutPanel42.SuspendLayout();
            this.gbox_BreakTransfer_A.SuspendLayout();
            this.tableLayoutPanel41.SuspendLayout();
            this.tp_BreakUnitXZ.SuspendLayout();
            this.gbox_BreakUnitXZ_Move.SuspendLayout();
            this.tableLayoutPanel45.SuspendLayout();
            this.gbox_BreakUnitXZ_MoveZ4.SuspendLayout();
            this.tableLayoutPanel49.SuspendLayout();
            this.gbox_BreakUnitXZ_MoveZ3.SuspendLayout();
            this.tableLayoutPanel48.SuspendLayout();
            this.gbox_BreakUnitXZ_MoveZ2.SuspendLayout();
            this.tableLayoutPanel47.SuspendLayout();
            this.gbox_BreakUnitXZ_MoveZ1.SuspendLayout();
            this.tableLayoutPanel46.SuspendLayout();
            this.tp_BreakUnitTY.SuspendLayout();
            this.gbox_BreakUnitTY_Shutter.SuspendLayout();
            this.tableLayoutPanel53.SuspendLayout();
            this.gbox_BreakUnitTY_DummyBox.SuspendLayout();
            this.tableLayoutPanel52.SuspendLayout();
            this.gbox_BreakUnitTY_Ionizer.SuspendLayout();
            this.gbox_BreakUnitTY_Move.SuspendLayout();
            this.tableLayoutPanel54.SuspendLayout();
            this.gbox_BreakUnitTY_Theta.SuspendLayout();
            this.tableLayoutPanel57.SuspendLayout();
            this.gbox_BreakUnitTY_MoveB.SuspendLayout();
            this.tableLayoutPanel56.SuspendLayout();
            this.gbox_BreakUnitTY_MoveA.SuspendLayout();
            this.tableLayoutPanel55.SuspendLayout();
            this.gbox_BreakUnitTY_B.SuspendLayout();
            this.tableLayoutPanel50.SuspendLayout();
            this.gbox_BreakUnitTY_A.SuspendLayout();
            this.tableLayoutPanel51.SuspendLayout();
            this.tp_UnloaderTransfer.SuspendLayout();
            this.gbox_UnloaderTransfer_Move.SuspendLayout();
            this.gbox_UnloaderTransfer_MoveB.SuspendLayout();
            this.tableLayoutPanel61.SuspendLayout();
            this.tableLayoutPanel62.SuspendLayout();
            this.tableLayoutPanel63.SuspendLayout();
            this.gbox_UnloaderTransfer_MoveA.SuspendLayout();
            this.tableLayoutPanel58.SuspendLayout();
            this.tableLayoutPanel60.SuspendLayout();
            this.tableLayoutPanel59.SuspendLayout();
            this.gbox_UnloaderTransfer_B.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.gbox_UnloaderTransfer_A.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tp_CameraUnit.SuspendLayout();
            this.gbox_CameraUnit_Move.SuspendLayout();
            this.gbox_CameraUnit_MoveB.SuspendLayout();
            this.tableLayoutPanel65.SuspendLayout();
            this.gbox_CameraUnit_MoveA.SuspendLayout();
            this.tableLayoutPanel64.SuspendLayout();
            this.tp_CellInput.SuspendLayout();
            this.gbox_CellInput_Buffer.SuspendLayout();
            this.tableLayoutPanel66.SuspendLayout();
            this.gbox_CellInput_Ionizer.SuspendLayout();
            this.gbox_CellInput_Move.SuspendLayout();
            this.gbox_CellInput_MoveCenter.SuspendLayout();
            this.tableLayoutPanel69.SuspendLayout();
            this.gbox_CellInput_MoveB.SuspendLayout();
            this.tableLayoutPanel68.SuspendLayout();
            this.gbox_CellInput_MoveA.SuspendLayout();
            this.tableLayoutPanel67.SuspendLayout();
            this.gbox_CellInput_B.SuspendLayout();
            this.tableLayoutPanel23.SuspendLayout();
            this.gbox_CellInput_A.SuspendLayout();
            this.tableLayoutPanel22.SuspendLayout();
            this.tp_CasseteUnload.SuspendLayout();
            this.gbox_CasseteUnload_Move.SuspendLayout();
            this.gbox_CasseteUnload_MoveB.SuspendLayout();
            this.tableLayoutPanel75.SuspendLayout();
            this.tableLayoutPanel76.SuspendLayout();
            this.tableLayoutPanel77.SuspendLayout();
            this.tableLayoutPanel78.SuspendLayout();
            this.tableLayoutPanel79.SuspendLayout();
            this.gbox_CasseteUnload_MoveA.SuspendLayout();
            this.tableLayoutPanel70.SuspendLayout();
            this.tableLayoutPanel71.SuspendLayout();
            this.tableLayoutPanel72.SuspendLayout();
            this.tableLayoutPanel73.SuspendLayout();
            this.tableLayoutPanel74.SuspendLayout();
            this.gbox_CasseteUnload_B.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.gbox_CasseteUnload_A.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.gbox_CasseteUnload.SuspendLayout();
            this.SuspendLayout();
            // 
            // tc_iostatus_info
            // 
            this.tc_iostatus_info.Controls.Add(this.tp_CasseteLoad);
            this.tc_iostatus_info.Controls.Add(this.tp_Cellpurge);
            this.tc_iostatus_info.Controls.Add(this.tp_CellLoad);
            this.tc_iostatus_info.Controls.Add(this.tp_IRCutProcess);
            this.tc_iostatus_info.Controls.Add(this.tp_BreakTransfer);
            this.tc_iostatus_info.Controls.Add(this.tp_BreakUnitXZ);
            this.tc_iostatus_info.Controls.Add(this.tp_BreakUnitTY);
            this.tc_iostatus_info.Controls.Add(this.tp_UnloaderTransfer);
            this.tc_iostatus_info.Controls.Add(this.tp_CameraUnit);
            this.tc_iostatus_info.Controls.Add(this.tp_CellInput);
            this.tc_iostatus_info.Controls.Add(this.tp_CasseteUnload);
            this.tc_iostatus_info.ItemSize = new System.Drawing.Size(156, 30);
            this.tc_iostatus_info.Location = new System.Drawing.Point(3, 10);
            this.tc_iostatus_info.Name = "tc_iostatus_info";
            this.tc_iostatus_info.SelectedIndex = 0;
            this.tc_iostatus_info.Size = new System.Drawing.Size(1734, 854);
            this.tc_iostatus_info.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tc_iostatus_info.TabIndex = 0;
            // 
            // tp_CasseteLoad
            // 
            this.tp_CasseteLoad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tp_CasseteLoad.Controls.Add(this.tableLayoutPanel1);
            this.tp_CasseteLoad.Controls.Add(this.gbox_CasseteLoad_Move);
            this.tp_CasseteLoad.Controls.Add(this.gbox_CasseteLoad_B);
            this.tp_CasseteLoad.Controls.Add(this.gbox_CasseteLoad_A);
            this.tp_CasseteLoad.Controls.Add(this.btn_CasseteLoad_Save);
            this.tp_CasseteLoad.Controls.Add(this.gbox_CasseteLoad_Muting);
            this.tp_CasseteLoad.Controls.Add(this.btn_CasseteLoad_SpeedSetting);
            this.tp_CasseteLoad.Controls.Add(this.btn_CasseteLoad_AllSetting);
            this.tp_CasseteLoad.Controls.Add(this.btn_CasseteLoad_LocationSetting);
            this.tp_CasseteLoad.Controls.Add(this.btn_CasseteLoad_MoveLocation);
            this.tp_CasseteLoad.Controls.Add(this.tbox_CasseteLoad_Location);
            this.tp_CasseteLoad.Controls.Add(this.tbox_CasseteLoad_Speed);
            this.tp_CasseteLoad.Controls.Add(this.tbox_CasseteLoad_CurrentLocation);
            this.tp_CasseteLoad.Controls.Add(this.tbox_CasseteLoad_SelectedShaft);
            this.tp_CasseteLoad.Controls.Add(this.lbl_CasseteLoad_Location);
            this.tp_CasseteLoad.Controls.Add(this.lbl_CasseteLoad_Speed);
            this.tp_CasseteLoad.Controls.Add(this.lbl_CasseteLoad_SelectedShaft);
            this.tp_CasseteLoad.Controls.Add(this.tbox_CasseteLoad_LocationSetting);
            this.tp_CasseteLoad.Controls.Add(this.lbl_CasseteLoad_CurrentLocation);
            this.tp_CasseteLoad.Controls.Add(this.btn_CasseteLoad_GrabSwitch3);
            this.tp_CasseteLoad.Controls.Add(this.btn_CasseteLoad_GrabSwitch2);
            this.tp_CasseteLoad.Controls.Add(this.btn_CasseteLoad_GrabSwitch1);
            this.tp_CasseteLoad.Controls.Add(this.lv_CasseteLoad);
            this.tp_CasseteLoad.Controls.Add(this.ajin_Setting_CasseteLoad);
            this.tp_CasseteLoad.Location = new System.Drawing.Point(4, 34);
            this.tp_CasseteLoad.Name = "tp_CasseteLoad";
            this.tp_CasseteLoad.Size = new System.Drawing.Size(1726, 816);
            this.tp_CasseteLoad.TabIndex = 0;
            this.tp_CasseteLoad.Text = "카세트 로드";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Controls.Add(this.btn_CasseteLoad_A_BCSD, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.btn_CasseteLoad_A_BCSU, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.btn_CasseteLoad_A_BCUG, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.btn_CasseteLoad_A_BCG, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.btn_CasseteLoad_A_LTSD, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.btn_CasseteLoad_A_LTSU, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.btn_CasseteLoad_A_LUG, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.btn_CasseteLoad_A_LG, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btn_CasseteLoad_A_TCSD, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.btn_CasseteLoad_A_TCSU, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.btn_CasseteLoad_A_TCUG, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.btn_CasseteLoad_A_TCG, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(798, 438);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(437, 145);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // btn_CasseteLoad_A_BCSD
            // 
            this.btn_CasseteLoad_A_BCSD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_A_BCSD.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_A_BCSD.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_A_BCSD.Location = new System.Drawing.Point(330, 99);
            this.btn_CasseteLoad_A_BCSD.Name = "btn_CasseteLoad_A_BCSD";
            this.btn_CasseteLoad_A_BCSD.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_A_BCSD.TabIndex = 31;
            this.btn_CasseteLoad_A_BCSD.Text = "하단 카세트\r\n스토퍼 다운";
            this.btn_CasseteLoad_A_BCSD.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_A_BCSU
            // 
            this.btn_CasseteLoad_A_BCSU.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_A_BCSU.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_A_BCSU.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_A_BCSU.Location = new System.Drawing.Point(221, 99);
            this.btn_CasseteLoad_A_BCSU.Name = "btn_CasseteLoad_A_BCSU";
            this.btn_CasseteLoad_A_BCSU.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_A_BCSU.TabIndex = 30;
            this.btn_CasseteLoad_A_BCSU.Text = "하단 카세트\r\n스토퍼 업";
            this.btn_CasseteLoad_A_BCSU.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_A_BCUG
            // 
            this.btn_CasseteLoad_A_BCUG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_A_BCUG.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_A_BCUG.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_A_BCUG.Location = new System.Drawing.Point(112, 99);
            this.btn_CasseteLoad_A_BCUG.Name = "btn_CasseteLoad_A_BCUG";
            this.btn_CasseteLoad_A_BCUG.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_A_BCUG.TabIndex = 29;
            this.btn_CasseteLoad_A_BCUG.Text = "하단 카세트\r\n언그립";
            this.btn_CasseteLoad_A_BCUG.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_A_BCG
            // 
            this.btn_CasseteLoad_A_BCG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_A_BCG.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_A_BCG.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_A_BCG.Location = new System.Drawing.Point(3, 99);
            this.btn_CasseteLoad_A_BCG.Name = "btn_CasseteLoad_A_BCG";
            this.btn_CasseteLoad_A_BCG.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_A_BCG.TabIndex = 28;
            this.btn_CasseteLoad_A_BCG.Text = "하단 카세트\r\n그립";
            this.btn_CasseteLoad_A_BCG.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_A_LTSD
            // 
            this.btn_CasseteLoad_A_LTSD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_A_LTSD.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_A_LTSD.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_A_LTSD.Location = new System.Drawing.Point(330, 51);
            this.btn_CasseteLoad_A_LTSD.Name = "btn_CasseteLoad_A_LTSD";
            this.btn_CasseteLoad_A_LTSD.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_A_LTSD.TabIndex = 27;
            this.btn_CasseteLoad_A_LTSD.Text = "리프트 틸트\r\n실린더 다운";
            this.btn_CasseteLoad_A_LTSD.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_A_LTSU
            // 
            this.btn_CasseteLoad_A_LTSU.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_A_LTSU.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_A_LTSU.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_A_LTSU.Location = new System.Drawing.Point(221, 51);
            this.btn_CasseteLoad_A_LTSU.Name = "btn_CasseteLoad_A_LTSU";
            this.btn_CasseteLoad_A_LTSU.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_A_LTSU.TabIndex = 26;
            this.btn_CasseteLoad_A_LTSU.Text = "리프트 틸트\r\n실린더 업";
            this.btn_CasseteLoad_A_LTSU.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_A_LUG
            // 
            this.btn_CasseteLoad_A_LUG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_A_LUG.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_A_LUG.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_A_LUG.Location = new System.Drawing.Point(112, 51);
            this.btn_CasseteLoad_A_LUG.Name = "btn_CasseteLoad_A_LUG";
            this.btn_CasseteLoad_A_LUG.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_A_LUG.TabIndex = 25;
            this.btn_CasseteLoad_A_LUG.Text = "리프트\r\n언그립";
            this.btn_CasseteLoad_A_LUG.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_A_LG
            // 
            this.btn_CasseteLoad_A_LG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_A_LG.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_A_LG.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_A_LG.Location = new System.Drawing.Point(3, 51);
            this.btn_CasseteLoad_A_LG.Name = "btn_CasseteLoad_A_LG";
            this.btn_CasseteLoad_A_LG.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_A_LG.TabIndex = 24;
            this.btn_CasseteLoad_A_LG.Text = "리프트\r\n그립";
            this.btn_CasseteLoad_A_LG.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_A_TCSD
            // 
            this.btn_CasseteLoad_A_TCSD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_A_TCSD.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_A_TCSD.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_A_TCSD.Location = new System.Drawing.Point(330, 3);
            this.btn_CasseteLoad_A_TCSD.Name = "btn_CasseteLoad_A_TCSD";
            this.btn_CasseteLoad_A_TCSD.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_A_TCSD.TabIndex = 23;
            this.btn_CasseteLoad_A_TCSD.Text = "상단 카세트\r\n스토퍼 다운";
            this.btn_CasseteLoad_A_TCSD.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_A_TCSU
            // 
            this.btn_CasseteLoad_A_TCSU.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_A_TCSU.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_A_TCSU.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_A_TCSU.Location = new System.Drawing.Point(221, 3);
            this.btn_CasseteLoad_A_TCSU.Name = "btn_CasseteLoad_A_TCSU";
            this.btn_CasseteLoad_A_TCSU.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_A_TCSU.TabIndex = 22;
            this.btn_CasseteLoad_A_TCSU.Text = "상단 카세트\r\n스토퍼 업";
            this.btn_CasseteLoad_A_TCSU.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_A_TCUG
            // 
            this.btn_CasseteLoad_A_TCUG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_A_TCUG.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_A_TCUG.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_A_TCUG.Location = new System.Drawing.Point(112, 3);
            this.btn_CasseteLoad_A_TCUG.Name = "btn_CasseteLoad_A_TCUG";
            this.btn_CasseteLoad_A_TCUG.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_A_TCUG.TabIndex = 21;
            this.btn_CasseteLoad_A_TCUG.Text = "상단 카세트\r\n언그립";
            this.btn_CasseteLoad_A_TCUG.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_A_TCG
            // 
            this.btn_CasseteLoad_A_TCG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_A_TCG.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_A_TCG.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_A_TCG.Location = new System.Drawing.Point(3, 3);
            this.btn_CasseteLoad_A_TCG.Name = "btn_CasseteLoad_A_TCG";
            this.btn_CasseteLoad_A_TCG.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_A_TCG.TabIndex = 20;
            this.btn_CasseteLoad_A_TCG.Text = "상단 카세트\r\n그립";
            this.btn_CasseteLoad_A_TCG.UseVisualStyleBackColor = false;
            // 
            // gbox_CasseteLoad_Move
            // 
            this.gbox_CasseteLoad_Move.Controls.Add(this.gbox_CasseteLoad_MoveB);
            this.gbox_CasseteLoad_Move.Controls.Add(this.gbox_CasseteLoad_MoveA);
            this.gbox_CasseteLoad_Move.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CasseteLoad_Move.ForeColor = System.Drawing.Color.White;
            this.gbox_CasseteLoad_Move.Location = new System.Drawing.Point(792, 595);
            this.gbox_CasseteLoad_Move.Name = "gbox_CasseteLoad_Move";
            this.gbox_CasseteLoad_Move.Size = new System.Drawing.Size(904, 179);
            this.gbox_CasseteLoad_Move.TabIndex = 44;
            this.gbox_CasseteLoad_Move.TabStop = false;
            this.gbox_CasseteLoad_Move.Text = "     이동     ";
            // 
            // gbox_CasseteLoad_MoveB
            // 
            this.gbox_CasseteLoad_MoveB.Controls.Add(this.tableLayoutPanel12);
            this.gbox_CasseteLoad_MoveB.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CasseteLoad_MoveB.ForeColor = System.Drawing.Color.White;
            this.gbox_CasseteLoad_MoveB.Location = new System.Drawing.Point(455, 21);
            this.gbox_CasseteLoad_MoveB.Name = "gbox_CasseteLoad_MoveB";
            this.gbox_CasseteLoad_MoveB.Size = new System.Drawing.Size(443, 152);
            this.gbox_CasseteLoad_MoveB.TabIndex = 53;
            this.gbox_CasseteLoad_MoveB.TabStop = false;
            this.gbox_CasseteLoad_MoveB.Text = "     B    ";
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 4;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel12.Controls.Add(this.tableLayoutPanel13, 3, 0);
            this.tableLayoutPanel12.Controls.Add(this.tableLayoutPanel14, 2, 0);
            this.tableLayoutPanel12.Controls.Add(this.tableLayoutPanel15, 1, 0);
            this.tableLayoutPanel12.Controls.Add(this.tableLayoutPanel16, 0, 0);
            this.tableLayoutPanel12.Location = new System.Drawing.Point(6, 21);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 1;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(436, 124);
            this.tableLayoutPanel12.TabIndex = 1;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 1;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.Controls.Add(this.btn_CasseteLoad_MoveB_Z2CellOut, 0, 1);
            this.tableLayoutPanel13.Controls.Add(this.btn_CasseteLoad_MoveB_Z2Tilt, 0, 0);
            this.tableLayoutPanel13.Location = new System.Drawing.Point(330, 3);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 2;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel13.TabIndex = 3;
            // 
            // btn_CasseteLoad_MoveB_Z2CellOut
            // 
            this.btn_CasseteLoad_MoveB_Z2CellOut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveB_Z2CellOut.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveB_Z2CellOut.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveB_Z2CellOut.Location = new System.Drawing.Point(3, 62);
            this.btn_CasseteLoad_MoveB_Z2CellOut.Name = "btn_CasseteLoad_MoveB_Z2CellOut";
            this.btn_CasseteLoad_MoveB_Z2CellOut.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteLoad_MoveB_Z2CellOut.TabIndex = 62;
            this.btn_CasseteLoad_MoveB_Z2CellOut.Text = "리프트 Z2\r\n셀 배출 시작";
            this.btn_CasseteLoad_MoveB_Z2CellOut.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_MoveB_Z2Tilt
            // 
            this.btn_CasseteLoad_MoveB_Z2Tilt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveB_Z2Tilt.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveB_Z2Tilt.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveB_Z2Tilt.Location = new System.Drawing.Point(3, 3);
            this.btn_CasseteLoad_MoveB_Z2Tilt.Name = "btn_CasseteLoad_MoveB_Z2Tilt";
            this.btn_CasseteLoad_MoveB_Z2Tilt.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteLoad_MoveB_Z2Tilt.TabIndex = 57;
            this.btn_CasseteLoad_MoveB_Z2Tilt.Text = "리프트 Z2\r\n틸트 측정";
            this.btn_CasseteLoad_MoveB_Z2Tilt.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 1;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel14.Controls.Add(this.btn_CasseteLoad_MoveB_Z2Out, 0, 2);
            this.tableLayoutPanel14.Controls.Add(this.btn_CasseteLoad_MoveB_Z2InWait, 0, 0);
            this.tableLayoutPanel14.Controls.Add(this.btn_CasseteLoad_MoveB_Z2In, 0, 1);
            this.tableLayoutPanel14.Location = new System.Drawing.Point(221, 3);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 3;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel14.TabIndex = 2;
            // 
            // btn_CasseteLoad_MoveB_Z2Out
            // 
            this.btn_CasseteLoad_MoveB_Z2Out.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveB_Z2Out.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveB_Z2Out.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveB_Z2Out.Location = new System.Drawing.Point(3, 81);
            this.btn_CasseteLoad_MoveB_Z2Out.Name = "btn_CasseteLoad_MoveB_Z2Out";
            this.btn_CasseteLoad_MoveB_Z2Out.Size = new System.Drawing.Size(97, 34);
            this.btn_CasseteLoad_MoveB_Z2Out.TabIndex = 61;
            this.btn_CasseteLoad_MoveB_Z2Out.Text = "리프트 Z2\r\n카세트 배출";
            this.btn_CasseteLoad_MoveB_Z2Out.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_MoveB_Z2InWait
            // 
            this.btn_CasseteLoad_MoveB_Z2InWait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveB_Z2InWait.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveB_Z2InWait.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveB_Z2InWait.Location = new System.Drawing.Point(3, 3);
            this.btn_CasseteLoad_MoveB_Z2InWait.Name = "btn_CasseteLoad_MoveB_Z2InWait";
            this.btn_CasseteLoad_MoveB_Z2InWait.Size = new System.Drawing.Size(97, 33);
            this.btn_CasseteLoad_MoveB_Z2InWait.TabIndex = 56;
            this.btn_CasseteLoad_MoveB_Z2InWait.Text = "카세트 Z2\r\n투입 대기";
            this.btn_CasseteLoad_MoveB_Z2InWait.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_MoveB_Z2In
            // 
            this.btn_CasseteLoad_MoveB_Z2In.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveB_Z2In.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveB_Z2In.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveB_Z2In.Location = new System.Drawing.Point(3, 42);
            this.btn_CasseteLoad_MoveB_Z2In.Name = "btn_CasseteLoad_MoveB_Z2In";
            this.btn_CasseteLoad_MoveB_Z2In.Size = new System.Drawing.Size(97, 33);
            this.btn_CasseteLoad_MoveB_Z2In.TabIndex = 60;
            this.btn_CasseteLoad_MoveB_Z2In.Text = "카세트 Z2\r\n카세트 투입";
            this.btn_CasseteLoad_MoveB_Z2In.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 1;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.Controls.Add(this.btn_CasseteLoad_MoveB_T4Out, 0, 1);
            this.tableLayoutPanel15.Controls.Add(this.btn_CasseteLoad_MoveB_T3Move, 0, 0);
            this.tableLayoutPanel15.Location = new System.Drawing.Point(112, 3);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 2;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel15.TabIndex = 1;
            // 
            // btn_CasseteLoad_MoveB_T4Out
            // 
            this.btn_CasseteLoad_MoveB_T4Out.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveB_T4Out.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveB_T4Out.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveB_T4Out.Location = new System.Drawing.Point(3, 62);
            this.btn_CasseteLoad_MoveB_T4Out.Name = "btn_CasseteLoad_MoveB_T4Out";
            this.btn_CasseteLoad_MoveB_T4Out.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteLoad_MoveB_T4Out.TabIndex = 59;
            this.btn_CasseteLoad_MoveB_T4Out.Text = "하부 카세트 T4\r\n카세트 배출";
            this.btn_CasseteLoad_MoveB_T4Out.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_MoveB_T3Move
            // 
            this.btn_CasseteLoad_MoveB_T3Move.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveB_T3Move.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveB_T3Move.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveB_T3Move.Location = new System.Drawing.Point(3, 3);
            this.btn_CasseteLoad_MoveB_T3Move.Name = "btn_CasseteLoad_MoveB_T3Move";
            this.btn_CasseteLoad_MoveB_T3Move.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteLoad_MoveB_T3Move.TabIndex = 55;
            this.btn_CasseteLoad_MoveB_T3Move.Text = "상부 카세트 T3\r\n리프트 이동";
            this.btn_CasseteLoad_MoveB_T3Move.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.ColumnCount = 1;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.Controls.Add(this.btn_CasseteLoad_MoveB_T4Move, 0, 1);
            this.tableLayoutPanel16.Controls.Add(this.btn_CasseteLoad_MoveB_T3In, 0, 0);
            this.tableLayoutPanel16.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 2;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel16.TabIndex = 0;
            // 
            // btn_CasseteLoad_MoveB_T4Move
            // 
            this.btn_CasseteLoad_MoveB_T4Move.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveB_T4Move.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveB_T4Move.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveB_T4Move.Location = new System.Drawing.Point(3, 62);
            this.btn_CasseteLoad_MoveB_T4Move.Name = "btn_CasseteLoad_MoveB_T4Move";
            this.btn_CasseteLoad_MoveB_T4Move.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteLoad_MoveB_T4Move.TabIndex = 58;
            this.btn_CasseteLoad_MoveB_T4Move.Text = "하부 카세트 T4\r\n카세트 이동";
            this.btn_CasseteLoad_MoveB_T4Move.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_MoveB_T3In
            // 
            this.btn_CasseteLoad_MoveB_T3In.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveB_T3In.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveB_T3In.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveB_T3In.Location = new System.Drawing.Point(3, 3);
            this.btn_CasseteLoad_MoveB_T3In.Name = "btn_CasseteLoad_MoveB_T3In";
            this.btn_CasseteLoad_MoveB_T3In.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteLoad_MoveB_T3In.TabIndex = 54;
            this.btn_CasseteLoad_MoveB_T3In.Text = "상부 카세트 T3\r\n카세트 투입";
            this.btn_CasseteLoad_MoveB_T3In.UseVisualStyleBackColor = false;
            // 
            // gbox_CasseteLoad_MoveA
            // 
            this.gbox_CasseteLoad_MoveA.Controls.Add(this.tableLayoutPanel7);
            this.gbox_CasseteLoad_MoveA.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CasseteLoad_MoveA.ForeColor = System.Drawing.Color.White;
            this.gbox_CasseteLoad_MoveA.Location = new System.Drawing.Point(6, 21);
            this.gbox_CasseteLoad_MoveA.Name = "gbox_CasseteLoad_MoveA";
            this.gbox_CasseteLoad_MoveA.Size = new System.Drawing.Size(443, 152);
            this.gbox_CasseteLoad_MoveA.TabIndex = 44;
            this.gbox_CasseteLoad_MoveA.TabStop = false;
            this.gbox_CasseteLoad_MoveA.Text = "     A    ";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 4;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel11, 3, 0);
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel10, 2, 0);
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel9, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel8, 0, 0);
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 21);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(437, 124);
            this.tableLayoutPanel7.TabIndex = 0;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 1;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.Controls.Add(this.btn_CasseteLoad_MoveA_Z1CellOut, 0, 1);
            this.tableLayoutPanel11.Controls.Add(this.btn_CasseteLoad_MoveA_Z1Tilt, 0, 0);
            this.tableLayoutPanel11.Location = new System.Drawing.Point(330, 3);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 2;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel11.TabIndex = 3;
            // 
            // btn_CasseteLoad_MoveA_Z1CellOut
            // 
            this.btn_CasseteLoad_MoveA_Z1CellOut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveA_Z1CellOut.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveA_Z1CellOut.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveA_Z1CellOut.Location = new System.Drawing.Point(3, 62);
            this.btn_CasseteLoad_MoveA_Z1CellOut.Name = "btn_CasseteLoad_MoveA_Z1CellOut";
            this.btn_CasseteLoad_MoveA_Z1CellOut.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteLoad_MoveA_Z1CellOut.TabIndex = 52;
            this.btn_CasseteLoad_MoveA_Z1CellOut.Text = "리프트 Z1\r\n셀 배출 시작";
            this.btn_CasseteLoad_MoveA_Z1CellOut.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_MoveA_Z1Tilt
            // 
            this.btn_CasseteLoad_MoveA_Z1Tilt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveA_Z1Tilt.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveA_Z1Tilt.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveA_Z1Tilt.Location = new System.Drawing.Point(3, 3);
            this.btn_CasseteLoad_MoveA_Z1Tilt.Name = "btn_CasseteLoad_MoveA_Z1Tilt";
            this.btn_CasseteLoad_MoveA_Z1Tilt.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteLoad_MoveA_Z1Tilt.TabIndex = 47;
            this.btn_CasseteLoad_MoveA_Z1Tilt.Text = "리프트 Z1\r\n틸트 측정";
            this.btn_CasseteLoad_MoveA_Z1Tilt.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 1;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Controls.Add(this.btn_CasseteLoad_MoveA_Z1Out, 0, 2);
            this.tableLayoutPanel10.Controls.Add(this.btn_CasseteLoad_MoveA_Z1InWait, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.btn_CasseteLoad_MoveA_Z1In, 0, 1);
            this.tableLayoutPanel10.Location = new System.Drawing.Point(221, 3);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 3;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel10.TabIndex = 2;
            // 
            // btn_CasseteLoad_MoveA_Z1Out
            // 
            this.btn_CasseteLoad_MoveA_Z1Out.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveA_Z1Out.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveA_Z1Out.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveA_Z1Out.Location = new System.Drawing.Point(3, 81);
            this.btn_CasseteLoad_MoveA_Z1Out.Name = "btn_CasseteLoad_MoveA_Z1Out";
            this.btn_CasseteLoad_MoveA_Z1Out.Size = new System.Drawing.Size(97, 34);
            this.btn_CasseteLoad_MoveA_Z1Out.TabIndex = 51;
            this.btn_CasseteLoad_MoveA_Z1Out.Text = "리프트 Z1\r\n카세트 배출";
            this.btn_CasseteLoad_MoveA_Z1Out.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_MoveA_Z1InWait
            // 
            this.btn_CasseteLoad_MoveA_Z1InWait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveA_Z1InWait.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveA_Z1InWait.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveA_Z1InWait.Location = new System.Drawing.Point(3, 3);
            this.btn_CasseteLoad_MoveA_Z1InWait.Name = "btn_CasseteLoad_MoveA_Z1InWait";
            this.btn_CasseteLoad_MoveA_Z1InWait.Size = new System.Drawing.Size(97, 33);
            this.btn_CasseteLoad_MoveA_Z1InWait.TabIndex = 46;
            this.btn_CasseteLoad_MoveA_Z1InWait.Text = "카세트 Z1\r\n투입 대기";
            this.btn_CasseteLoad_MoveA_Z1InWait.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_MoveA_Z1In
            // 
            this.btn_CasseteLoad_MoveA_Z1In.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveA_Z1In.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveA_Z1In.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveA_Z1In.Location = new System.Drawing.Point(3, 42);
            this.btn_CasseteLoad_MoveA_Z1In.Name = "btn_CasseteLoad_MoveA_Z1In";
            this.btn_CasseteLoad_MoveA_Z1In.Size = new System.Drawing.Size(97, 33);
            this.btn_CasseteLoad_MoveA_Z1In.TabIndex = 50;
            this.btn_CasseteLoad_MoveA_Z1In.Text = "카세트 Z1\r\n카세트 투입";
            this.btn_CasseteLoad_MoveA_Z1In.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 1;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Controls.Add(this.btn_CasseteLoad_MoveA_T2Out, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.btn_CasseteLoad_MoveA_T1Move, 0, 0);
            this.tableLayoutPanel9.Location = new System.Drawing.Point(112, 3);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 2;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel9.TabIndex = 1;
            // 
            // btn_CasseteLoad_MoveA_T2Out
            // 
            this.btn_CasseteLoad_MoveA_T2Out.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveA_T2Out.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveA_T2Out.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveA_T2Out.Location = new System.Drawing.Point(3, 62);
            this.btn_CasseteLoad_MoveA_T2Out.Name = "btn_CasseteLoad_MoveA_T2Out";
            this.btn_CasseteLoad_MoveA_T2Out.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteLoad_MoveA_T2Out.TabIndex = 49;
            this.btn_CasseteLoad_MoveA_T2Out.Text = "하부 카세트 T2\r\n카세트 배출";
            this.btn_CasseteLoad_MoveA_T2Out.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_MoveA_T1Move
            // 
            this.btn_CasseteLoad_MoveA_T1Move.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveA_T1Move.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveA_T1Move.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveA_T1Move.Location = new System.Drawing.Point(3, 3);
            this.btn_CasseteLoad_MoveA_T1Move.Name = "btn_CasseteLoad_MoveA_T1Move";
            this.btn_CasseteLoad_MoveA_T1Move.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteLoad_MoveA_T1Move.TabIndex = 45;
            this.btn_CasseteLoad_MoveA_T1Move.Text = "상부 카세트 T1\r\n리프트 이동";
            this.btn_CasseteLoad_MoveA_T1Move.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 1;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Controls.Add(this.btn_CasseteLoad_MoveA_T2Move, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.btn_CasseteLoad_MoveA_T1In, 0, 0);
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 2;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel8.TabIndex = 0;
            // 
            // btn_CasseteLoad_MoveA_T2Move
            // 
            this.btn_CasseteLoad_MoveA_T2Move.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveA_T2Move.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveA_T2Move.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveA_T2Move.Location = new System.Drawing.Point(3, 62);
            this.btn_CasseteLoad_MoveA_T2Move.Name = "btn_CasseteLoad_MoveA_T2Move";
            this.btn_CasseteLoad_MoveA_T2Move.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteLoad_MoveA_T2Move.TabIndex = 48;
            this.btn_CasseteLoad_MoveA_T2Move.Text = "하부 카세트 T2\r\n카세트 이동";
            this.btn_CasseteLoad_MoveA_T2Move.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_MoveA_T1In
            // 
            this.btn_CasseteLoad_MoveA_T1In.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveA_T1In.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_MoveA_T1In.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveA_T1In.Location = new System.Drawing.Point(3, 3);
            this.btn_CasseteLoad_MoveA_T1In.Name = "btn_CasseteLoad_MoveA_T1In";
            this.btn_CasseteLoad_MoveA_T1In.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteLoad_MoveA_T1In.TabIndex = 44;
            this.btn_CasseteLoad_MoveA_T1In.Text = "상부 카세트 T1\r\n카세트 투입";
            this.btn_CasseteLoad_MoveA_T1In.UseVisualStyleBackColor = false;
            // 
            // gbox_CasseteLoad_B
            // 
            this.gbox_CasseteLoad_B.Controls.Add(this.tableLayoutPanel2);
            this.gbox_CasseteLoad_B.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CasseteLoad_B.ForeColor = System.Drawing.Color.White;
            this.gbox_CasseteLoad_B.Location = new System.Drawing.Point(1247, 410);
            this.gbox_CasseteLoad_B.Name = "gbox_CasseteLoad_B";
            this.gbox_CasseteLoad_B.Size = new System.Drawing.Size(449, 179);
            this.gbox_CasseteLoad_B.TabIndex = 32;
            this.gbox_CasseteLoad_B.TabStop = false;
            this.gbox_CasseteLoad_B.Text = "     B     ";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Controls.Add(this.btn_CasseteLoad_B_BCSD, 3, 2);
            this.tableLayoutPanel2.Controls.Add(this.btn_CasseteLoad_B_BCSU, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.btn_CasseteLoad_B_BCUG, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.btn_CasseteLoad_B_BCG, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.btn_CasseteLoad_B_LTSD, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.btn_CasseteLoad_B_LTSU, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.btn_CasseteLoad_B_LUG, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.btn_CasseteLoad_B_LG, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.btn_CasseteLoad_B_TCSD, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.btn_CasseteLoad_B_TCSU, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.btn_CasseteLoad_B_TCUG, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.btn_CasseteLoad_B_TCG, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(437, 145);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // btn_CasseteLoad_B_BCSD
            // 
            this.btn_CasseteLoad_B_BCSD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_B_BCSD.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_B_BCSD.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_B_BCSD.Location = new System.Drawing.Point(330, 99);
            this.btn_CasseteLoad_B_BCSD.Name = "btn_CasseteLoad_B_BCSD";
            this.btn_CasseteLoad_B_BCSD.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_B_BCSD.TabIndex = 43;
            this.btn_CasseteLoad_B_BCSD.Text = "하단 카세트\r\n스토퍼 다운";
            this.btn_CasseteLoad_B_BCSD.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_B_BCSU
            // 
            this.btn_CasseteLoad_B_BCSU.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_B_BCSU.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_B_BCSU.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_B_BCSU.Location = new System.Drawing.Point(221, 99);
            this.btn_CasseteLoad_B_BCSU.Name = "btn_CasseteLoad_B_BCSU";
            this.btn_CasseteLoad_B_BCSU.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_B_BCSU.TabIndex = 42;
            this.btn_CasseteLoad_B_BCSU.Text = "하단 카세트\r\n스토퍼 업";
            this.btn_CasseteLoad_B_BCSU.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_B_BCUG
            // 
            this.btn_CasseteLoad_B_BCUG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_B_BCUG.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_B_BCUG.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_B_BCUG.Location = new System.Drawing.Point(112, 99);
            this.btn_CasseteLoad_B_BCUG.Name = "btn_CasseteLoad_B_BCUG";
            this.btn_CasseteLoad_B_BCUG.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_B_BCUG.TabIndex = 41;
            this.btn_CasseteLoad_B_BCUG.Text = "하단 카세트\r\n언그립";
            this.btn_CasseteLoad_B_BCUG.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_B_BCG
            // 
            this.btn_CasseteLoad_B_BCG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_B_BCG.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_B_BCG.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_B_BCG.Location = new System.Drawing.Point(3, 99);
            this.btn_CasseteLoad_B_BCG.Name = "btn_CasseteLoad_B_BCG";
            this.btn_CasseteLoad_B_BCG.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_B_BCG.TabIndex = 40;
            this.btn_CasseteLoad_B_BCG.Text = "하단 카세트\r\n그립";
            this.btn_CasseteLoad_B_BCG.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_B_LTSD
            // 
            this.btn_CasseteLoad_B_LTSD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_B_LTSD.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_B_LTSD.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_B_LTSD.Location = new System.Drawing.Point(330, 51);
            this.btn_CasseteLoad_B_LTSD.Name = "btn_CasseteLoad_B_LTSD";
            this.btn_CasseteLoad_B_LTSD.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_B_LTSD.TabIndex = 39;
            this.btn_CasseteLoad_B_LTSD.Text = "리프트 틸트\r\n실린더 다운";
            this.btn_CasseteLoad_B_LTSD.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_B_LTSU
            // 
            this.btn_CasseteLoad_B_LTSU.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_B_LTSU.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_B_LTSU.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_B_LTSU.Location = new System.Drawing.Point(221, 51);
            this.btn_CasseteLoad_B_LTSU.Name = "btn_CasseteLoad_B_LTSU";
            this.btn_CasseteLoad_B_LTSU.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_B_LTSU.TabIndex = 38;
            this.btn_CasseteLoad_B_LTSU.Text = "리프트 틸트\r\n실린더 업";
            this.btn_CasseteLoad_B_LTSU.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_B_LUG
            // 
            this.btn_CasseteLoad_B_LUG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_B_LUG.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_B_LUG.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_B_LUG.Location = new System.Drawing.Point(112, 51);
            this.btn_CasseteLoad_B_LUG.Name = "btn_CasseteLoad_B_LUG";
            this.btn_CasseteLoad_B_LUG.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_B_LUG.TabIndex = 37;
            this.btn_CasseteLoad_B_LUG.Text = "리프트\r\n언그립";
            this.btn_CasseteLoad_B_LUG.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_B_LG
            // 
            this.btn_CasseteLoad_B_LG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_B_LG.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_B_LG.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_B_LG.Location = new System.Drawing.Point(3, 51);
            this.btn_CasseteLoad_B_LG.Name = "btn_CasseteLoad_B_LG";
            this.btn_CasseteLoad_B_LG.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_B_LG.TabIndex = 36;
            this.btn_CasseteLoad_B_LG.Text = "리프트\r\n그립";
            this.btn_CasseteLoad_B_LG.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_B_TCSD
            // 
            this.btn_CasseteLoad_B_TCSD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_B_TCSD.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_B_TCSD.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_B_TCSD.Location = new System.Drawing.Point(330, 3);
            this.btn_CasseteLoad_B_TCSD.Name = "btn_CasseteLoad_B_TCSD";
            this.btn_CasseteLoad_B_TCSD.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_B_TCSD.TabIndex = 35;
            this.btn_CasseteLoad_B_TCSD.Text = "상단 카세트\r\n스토퍼 다운";
            this.btn_CasseteLoad_B_TCSD.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_B_TCSU
            // 
            this.btn_CasseteLoad_B_TCSU.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_B_TCSU.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_B_TCSU.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_B_TCSU.Location = new System.Drawing.Point(221, 3);
            this.btn_CasseteLoad_B_TCSU.Name = "btn_CasseteLoad_B_TCSU";
            this.btn_CasseteLoad_B_TCSU.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_B_TCSU.TabIndex = 34;
            this.btn_CasseteLoad_B_TCSU.Text = "상단 카세트\r\n스토퍼 업";
            this.btn_CasseteLoad_B_TCSU.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_B_TCUG
            // 
            this.btn_CasseteLoad_B_TCUG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_B_TCUG.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_B_TCUG.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_B_TCUG.Location = new System.Drawing.Point(112, 3);
            this.btn_CasseteLoad_B_TCUG.Name = "btn_CasseteLoad_B_TCUG";
            this.btn_CasseteLoad_B_TCUG.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_B_TCUG.TabIndex = 33;
            this.btn_CasseteLoad_B_TCUG.Text = "상단 카세트\r\n언그립";
            this.btn_CasseteLoad_B_TCUG.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_B_TCG
            // 
            this.btn_CasseteLoad_B_TCG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_B_TCG.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_B_TCG.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_B_TCG.Location = new System.Drawing.Point(3, 3);
            this.btn_CasseteLoad_B_TCG.Name = "btn_CasseteLoad_B_TCG";
            this.btn_CasseteLoad_B_TCG.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteLoad_B_TCG.TabIndex = 32;
            this.btn_CasseteLoad_B_TCG.Text = "상단 카세트\r\n그립";
            this.btn_CasseteLoad_B_TCG.UseVisualStyleBackColor = false;
            // 
            // gbox_CasseteLoad_A
            // 
            this.gbox_CasseteLoad_A.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CasseteLoad_A.ForeColor = System.Drawing.Color.White;
            this.gbox_CasseteLoad_A.Location = new System.Drawing.Point(792, 410);
            this.gbox_CasseteLoad_A.Name = "gbox_CasseteLoad_A";
            this.gbox_CasseteLoad_A.Size = new System.Drawing.Size(449, 179);
            this.gbox_CasseteLoad_A.TabIndex = 20;
            this.gbox_CasseteLoad_A.TabStop = false;
            this.gbox_CasseteLoad_A.Text = "     A    ";
            // 
            // btn_CasseteLoad_Save
            // 
            this.btn_CasseteLoad_Save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_Save.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_Save.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_Save.Location = new System.Drawing.Point(1482, 783);
            this.btn_CasseteLoad_Save.Name = "btn_CasseteLoad_Save";
            this.btn_CasseteLoad_Save.Size = new System.Drawing.Size(228, 28);
            this.btn_CasseteLoad_Save.TabIndex = 64;
            this.btn_CasseteLoad_Save.Text = "저장";
            this.btn_CasseteLoad_Save.UseVisualStyleBackColor = false;
            // 
            // gbox_CasseteLoad_Muting
            // 
            this.gbox_CasseteLoad_Muting.Controls.Add(this.btn_CasseteLoad_Muting4);
            this.gbox_CasseteLoad_Muting.Controls.Add(this.btn_CasseteLoad_Muting2);
            this.gbox_CasseteLoad_Muting.Controls.Add(this.btn_CasseteLoad_Muting3);
            this.gbox_CasseteLoad_Muting.Controls.Add(this.btn_CasseteLoad_Muting1);
            this.gbox_CasseteLoad_Muting.Controls.Add(this.btn_CasseteLoad_MutingOff);
            this.gbox_CasseteLoad_Muting.Controls.Add(this.btn_CasseteLoad_MutingIn);
            this.gbox_CasseteLoad_Muting.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CasseteLoad_Muting.ForeColor = System.Drawing.Color.White;
            this.gbox_CasseteLoad_Muting.Location = new System.Drawing.Point(1483, 223);
            this.gbox_CasseteLoad_Muting.Name = "gbox_CasseteLoad_Muting";
            this.gbox_CasseteLoad_Muting.Size = new System.Drawing.Size(212, 181);
            this.gbox_CasseteLoad_Muting.TabIndex = 13;
            this.gbox_CasseteLoad_Muting.TabStop = false;
            this.gbox_CasseteLoad_Muting.Text = "     뮤팅     ";
            // 
            // btn_CasseteLoad_Muting4
            // 
            this.btn_CasseteLoad_Muting4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_Muting4.Location = new System.Drawing.Point(107, 125);
            this.btn_CasseteLoad_Muting4.Name = "btn_CasseteLoad_Muting4";
            this.btn_CasseteLoad_Muting4.Size = new System.Drawing.Size(99, 44);
            this.btn_CasseteLoad_Muting4.TabIndex = 19;
            this.btn_CasseteLoad_Muting4.Text = "뮤팅 4";
            this.btn_CasseteLoad_Muting4.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_Muting2
            // 
            this.btn_CasseteLoad_Muting2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_Muting2.Location = new System.Drawing.Point(6, 125);
            this.btn_CasseteLoad_Muting2.Name = "btn_CasseteLoad_Muting2";
            this.btn_CasseteLoad_Muting2.Size = new System.Drawing.Size(99, 44);
            this.btn_CasseteLoad_Muting2.TabIndex = 18;
            this.btn_CasseteLoad_Muting2.Text = "뮤팅 2";
            this.btn_CasseteLoad_Muting2.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_Muting3
            // 
            this.btn_CasseteLoad_Muting3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_Muting3.Location = new System.Drawing.Point(107, 75);
            this.btn_CasseteLoad_Muting3.Name = "btn_CasseteLoad_Muting3";
            this.btn_CasseteLoad_Muting3.Size = new System.Drawing.Size(99, 44);
            this.btn_CasseteLoad_Muting3.TabIndex = 17;
            this.btn_CasseteLoad_Muting3.Text = "뮤팅 3";
            this.btn_CasseteLoad_Muting3.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_Muting1
            // 
            this.btn_CasseteLoad_Muting1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_Muting1.Location = new System.Drawing.Point(6, 75);
            this.btn_CasseteLoad_Muting1.Name = "btn_CasseteLoad_Muting1";
            this.btn_CasseteLoad_Muting1.Size = new System.Drawing.Size(99, 44);
            this.btn_CasseteLoad_Muting1.TabIndex = 16;
            this.btn_CasseteLoad_Muting1.Text = "뮤팅 1";
            this.btn_CasseteLoad_Muting1.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_MutingOff
            // 
            this.btn_CasseteLoad_MutingOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MutingOff.Location = new System.Drawing.Point(107, 29);
            this.btn_CasseteLoad_MutingOff.Name = "btn_CasseteLoad_MutingOff";
            this.btn_CasseteLoad_MutingOff.Size = new System.Drawing.Size(99, 44);
            this.btn_CasseteLoad_MutingOff.TabIndex = 15;
            this.btn_CasseteLoad_MutingOff.Text = "뮤팅 아웃";
            this.btn_CasseteLoad_MutingOff.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_MutingIn
            // 
            this.btn_CasseteLoad_MutingIn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MutingIn.Location = new System.Drawing.Point(6, 28);
            this.btn_CasseteLoad_MutingIn.Name = "btn_CasseteLoad_MutingIn";
            this.btn_CasseteLoad_MutingIn.Size = new System.Drawing.Size(99, 44);
            this.btn_CasseteLoad_MutingIn.TabIndex = 14;
            this.btn_CasseteLoad_MutingIn.Text = "뮤팅 인";
            this.btn_CasseteLoad_MutingIn.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_SpeedSetting
            // 
            this.btn_CasseteLoad_SpeedSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_SpeedSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CasseteLoad_SpeedSetting.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_SpeedSetting.Location = new System.Drawing.Point(1291, 315);
            this.btn_CasseteLoad_SpeedSetting.Name = "btn_CasseteLoad_SpeedSetting";
            this.btn_CasseteLoad_SpeedSetting.Size = new System.Drawing.Size(90, 27);
            this.btn_CasseteLoad_SpeedSetting.TabIndex = 12;
            this.btn_CasseteLoad_SpeedSetting.Text = "속도 설정";
            this.btn_CasseteLoad_SpeedSetting.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_AllSetting
            // 
            this.btn_CasseteLoad_AllSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_AllSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CasseteLoad_AllSetting.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_AllSetting.Location = new System.Drawing.Point(1387, 273);
            this.btn_CasseteLoad_AllSetting.Name = "btn_CasseteLoad_AllSetting";
            this.btn_CasseteLoad_AllSetting.Size = new System.Drawing.Size(90, 27);
            this.btn_CasseteLoad_AllSetting.TabIndex = 8;
            this.btn_CasseteLoad_AllSetting.Text = "전체 설정";
            this.btn_CasseteLoad_AllSetting.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_LocationSetting
            // 
            this.btn_CasseteLoad_LocationSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_LocationSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CasseteLoad_LocationSetting.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_LocationSetting.Location = new System.Drawing.Point(1071, 315);
            this.btn_CasseteLoad_LocationSetting.Name = "btn_CasseteLoad_LocationSetting";
            this.btn_CasseteLoad_LocationSetting.Size = new System.Drawing.Size(90, 27);
            this.btn_CasseteLoad_LocationSetting.TabIndex = 11;
            this.btn_CasseteLoad_LocationSetting.Text = "위치 설정";
            this.btn_CasseteLoad_LocationSetting.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_MoveLocation
            // 
            this.btn_CasseteLoad_MoveLocation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_MoveLocation.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CasseteLoad_MoveLocation.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_MoveLocation.Location = new System.Drawing.Point(975, 315);
            this.btn_CasseteLoad_MoveLocation.Name = "btn_CasseteLoad_MoveLocation";
            this.btn_CasseteLoad_MoveLocation.Size = new System.Drawing.Size(90, 27);
            this.btn_CasseteLoad_MoveLocation.TabIndex = 10;
            this.btn_CasseteLoad_MoveLocation.Text = "위치 이동";
            this.btn_CasseteLoad_MoveLocation.UseVisualStyleBackColor = false;
            // 
            // tbox_CasseteLoad_Location
            // 
            this.tbox_CasseteLoad_Location.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_CasseteLoad_Location.Location = new System.Drawing.Point(879, 316);
            this.tbox_CasseteLoad_Location.Name = "tbox_CasseteLoad_Location";
            this.tbox_CasseteLoad_Location.Size = new System.Drawing.Size(90, 27);
            this.tbox_CasseteLoad_Location.TabIndex = 9;
            // 
            // tbox_CasseteLoad_Speed
            // 
            this.tbox_CasseteLoad_Speed.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_CasseteLoad_Speed.Location = new System.Drawing.Point(1291, 273);
            this.tbox_CasseteLoad_Speed.Name = "tbox_CasseteLoad_Speed";
            this.tbox_CasseteLoad_Speed.Size = new System.Drawing.Size(90, 27);
            this.tbox_CasseteLoad_Speed.TabIndex = 7;
            // 
            // tbox_CasseteLoad_CurrentLocation
            // 
            this.tbox_CasseteLoad_CurrentLocation.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_CasseteLoad_CurrentLocation.Location = new System.Drawing.Point(1084, 274);
            this.tbox_CasseteLoad_CurrentLocation.Name = "tbox_CasseteLoad_CurrentLocation";
            this.tbox_CasseteLoad_CurrentLocation.Size = new System.Drawing.Size(90, 27);
            this.tbox_CasseteLoad_CurrentLocation.TabIndex = 6;
            // 
            // tbox_CasseteLoad_SelectedShaft
            // 
            this.tbox_CasseteLoad_SelectedShaft.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_CasseteLoad_SelectedShaft.Location = new System.Drawing.Point(879, 273);
            this.tbox_CasseteLoad_SelectedShaft.Name = "tbox_CasseteLoad_SelectedShaft";
            this.tbox_CasseteLoad_SelectedShaft.Size = new System.Drawing.Size(90, 27);
            this.tbox_CasseteLoad_SelectedShaft.TabIndex = 5;
            // 
            // lbl_CasseteLoad_Location
            // 
            this.lbl_CasseteLoad_Location.AutoSize = true;
            this.lbl_CasseteLoad_Location.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CasseteLoad_Location.ForeColor = System.Drawing.Color.White;
            this.lbl_CasseteLoad_Location.Location = new System.Drawing.Point(801, 316);
            this.lbl_CasseteLoad_Location.Name = "lbl_CasseteLoad_Location";
            this.lbl_CasseteLoad_Location.Size = new System.Drawing.Size(84, 21);
            this.lbl_CasseteLoad_Location.TabIndex = 0;
            this.lbl_CasseteLoad_Location.Text = "위치[mm]";
            // 
            // lbl_CasseteLoad_Speed
            // 
            this.lbl_CasseteLoad_Speed.AutoSize = true;
            this.lbl_CasseteLoad_Speed.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CasseteLoad_Speed.ForeColor = System.Drawing.Color.White;
            this.lbl_CasseteLoad_Speed.Location = new System.Drawing.Point(1179, 275);
            this.lbl_CasseteLoad_Speed.Name = "lbl_CasseteLoad_Speed";
            this.lbl_CasseteLoad_Speed.Size = new System.Drawing.Size(115, 21);
            this.lbl_CasseteLoad_Speed.TabIndex = 0;
            this.lbl_CasseteLoad_Speed.Text = "속도[mm/sec]";
            // 
            // lbl_CasseteLoad_SelectedShaft
            // 
            this.lbl_CasseteLoad_SelectedShaft.AutoSize = true;
            this.lbl_CasseteLoad_SelectedShaft.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CasseteLoad_SelectedShaft.ForeColor = System.Drawing.Color.White;
            this.lbl_CasseteLoad_SelectedShaft.Location = new System.Drawing.Point(805, 274);
            this.lbl_CasseteLoad_SelectedShaft.Name = "lbl_CasseteLoad_SelectedShaft";
            this.lbl_CasseteLoad_SelectedShaft.Size = new System.Drawing.Size(80, 21);
            this.lbl_CasseteLoad_SelectedShaft.TabIndex = 0;
            this.lbl_CasseteLoad_SelectedShaft.Text = "선택된 축";
            // 
            // tbox_CasseteLoad_LocationSetting
            // 
            this.tbox_CasseteLoad_LocationSetting.BackColor = System.Drawing.SystemColors.Highlight;
            this.tbox_CasseteLoad_LocationSetting.Cursor = System.Windows.Forms.Cursors.Default;
            this.tbox_CasseteLoad_LocationSetting.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.tbox_CasseteLoad_LocationSetting.ForeColor = System.Drawing.Color.White;
            this.tbox_CasseteLoad_LocationSetting.Location = new System.Drawing.Point(792, 223);
            this.tbox_CasseteLoad_LocationSetting.Name = "tbox_CasseteLoad_LocationSetting";
            this.tbox_CasseteLoad_LocationSetting.ReadOnly = true;
            this.tbox_CasseteLoad_LocationSetting.Size = new System.Drawing.Size(89, 27);
            this.tbox_CasseteLoad_LocationSetting.TabIndex = 37;
            this.tbox_CasseteLoad_LocationSetting.TabStop = false;
            this.tbox_CasseteLoad_LocationSetting.Text = "위치값 설정";
            this.tbox_CasseteLoad_LocationSetting.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbl_CasseteLoad_CurrentLocation
            // 
            this.lbl_CasseteLoad_CurrentLocation.AutoSize = true;
            this.lbl_CasseteLoad_CurrentLocation.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CasseteLoad_CurrentLocation.ForeColor = System.Drawing.Color.White;
            this.lbl_CasseteLoad_CurrentLocation.Location = new System.Drawing.Point(975, 274);
            this.lbl_CasseteLoad_CurrentLocation.Name = "lbl_CasseteLoad_CurrentLocation";
            this.lbl_CasseteLoad_CurrentLocation.Size = new System.Drawing.Size(116, 21);
            this.lbl_CasseteLoad_CurrentLocation.TabIndex = 0;
            this.lbl_CasseteLoad_CurrentLocation.Text = "현재위치[mm]";
            // 
            // btn_CasseteLoad_GrabSwitch3
            // 
            this.btn_CasseteLoad_GrabSwitch3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_GrabSwitch3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_GrabSwitch3.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_GrabSwitch3.Location = new System.Drawing.Point(1514, 6);
            this.btn_CasseteLoad_GrabSwitch3.Name = "btn_CasseteLoad_GrabSwitch3";
            this.btn_CasseteLoad_GrabSwitch3.Size = new System.Drawing.Size(172, 23);
            this.btn_CasseteLoad_GrabSwitch3.TabIndex = 3;
            this.btn_CasseteLoad_GrabSwitch3.Text = "Grab Switch 3";
            this.btn_CasseteLoad_GrabSwitch3.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_GrabSwitch2
            // 
            this.btn_CasseteLoad_GrabSwitch2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_GrabSwitch2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_GrabSwitch2.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_GrabSwitch2.Location = new System.Drawing.Point(1336, 6);
            this.btn_CasseteLoad_GrabSwitch2.Name = "btn_CasseteLoad_GrabSwitch2";
            this.btn_CasseteLoad_GrabSwitch2.Size = new System.Drawing.Size(172, 23);
            this.btn_CasseteLoad_GrabSwitch2.TabIndex = 2;
            this.btn_CasseteLoad_GrabSwitch2.Text = "Grab Switch 2";
            this.btn_CasseteLoad_GrabSwitch2.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteLoad_GrabSwitch1
            // 
            this.btn_CasseteLoad_GrabSwitch1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteLoad_GrabSwitch1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteLoad_GrabSwitch1.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteLoad_GrabSwitch1.Location = new System.Drawing.Point(1158, 6);
            this.btn_CasseteLoad_GrabSwitch1.Name = "btn_CasseteLoad_GrabSwitch1";
            this.btn_CasseteLoad_GrabSwitch1.Size = new System.Drawing.Size(172, 23);
            this.btn_CasseteLoad_GrabSwitch1.TabIndex = 1;
            this.btn_CasseteLoad_GrabSwitch1.Text = "Grab Switch 1";
            this.btn_CasseteLoad_GrabSwitch1.UseVisualStyleBackColor = false;
            // 
            // lv_CasseteLoad
            // 
            this.lv_CasseteLoad.BackColor = System.Drawing.SystemColors.Window;
            this.lv_CasseteLoad.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.col_CasseteLoad_NamebyLocation,
            this.col_CasseteLoad_Shaft,
            this.col_CasseteLoad_Location,
            this.col_CasseteLoad_Speed});
            this.lv_CasseteLoad.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lv_CasseteLoad.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lv_CasseteLoad.GridLines = true;
            this.lv_CasseteLoad.Location = new System.Drawing.Point(17, 16);
            this.lv_CasseteLoad.Name = "lv_CasseteLoad";
            this.lv_CasseteLoad.Size = new System.Drawing.Size(758, 734);
            this.lv_CasseteLoad.TabIndex = 0;
            this.lv_CasseteLoad.TabStop = false;
            this.lv_CasseteLoad.UseCompatibleStateImageBehavior = false;
            this.lv_CasseteLoad.View = System.Windows.Forms.View.Details;
            // 
            // col_CasseteLoad_NamebyLocation
            // 
            this.col_CasseteLoad_NamebyLocation.Text = "위치별 명칭";
            this.col_CasseteLoad_NamebyLocation.Width = 430;
            // 
            // col_CasseteLoad_Shaft
            // 
            this.col_CasseteLoad_Shaft.Text = "축";
            this.col_CasseteLoad_Shaft.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // col_CasseteLoad_Location
            // 
            this.col_CasseteLoad_Location.Text = "위치[mm]";
            this.col_CasseteLoad_Location.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_CasseteLoad_Location.Width = 125;
            // 
            // col_CasseteLoad_Speed
            // 
            this.col_CasseteLoad_Speed.Text = "속도[mm/s]";
            this.col_CasseteLoad_Speed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_CasseteLoad_Speed.Width = 125;
            // 
            // tp_Cellpurge
            // 
            this.tp_Cellpurge.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tp_Cellpurge.Controls.Add(this.gbox_Cellpurge_Move);
            this.tp_Cellpurge.Controls.Add(this.gbox_Cellpurge_B);
            this.tp_Cellpurge.Controls.Add(this.gbox_Cellpurge_A);
            this.tp_Cellpurge.Controls.Add(this.btn_Cellpurge_Save);
            this.tp_Cellpurge.Controls.Add(this.gbox_Cellpurge_Ionizer);
            this.tp_Cellpurge.Controls.Add(this.btn_Cellpurge_SpeedSetting);
            this.tp_Cellpurge.Controls.Add(this.btn_Cellpurge_AllSetting);
            this.tp_Cellpurge.Controls.Add(this.btn_Cellpurge_LocationSetting);
            this.tp_Cellpurge.Controls.Add(this.btn_Cellpurge_MoveLocation);
            this.tp_Cellpurge.Controls.Add(this.tbox_Cellpurge_Location);
            this.tp_Cellpurge.Controls.Add(this.tbox_Cellpurge_Speed);
            this.tp_Cellpurge.Controls.Add(this.tbox_Cellpurge_CurrentLocation);
            this.tp_Cellpurge.Controls.Add(this.tbox_Cellpurge_SelectedShaft);
            this.tp_Cellpurge.Controls.Add(this.lbl_Cellpurge_Location);
            this.tp_Cellpurge.Controls.Add(this.lbl_Cellpurge_Speed);
            this.tp_Cellpurge.Controls.Add(this.lbl_Cellpurge_SelectedShaft);
            this.tp_Cellpurge.Controls.Add(this.tbox_Cellpurge_LocationSetting);
            this.tp_Cellpurge.Controls.Add(this.lbl_Cellpurge_CurrentLocation);
            this.tp_Cellpurge.Controls.Add(this.btn_Cellpurge_GrabSwitch3);
            this.tp_Cellpurge.Controls.Add(this.btn_Cellpurge_GrabSwitch2);
            this.tp_Cellpurge.Controls.Add(this.btn_Cellpurge_GrabSwitch1);
            this.tp_Cellpurge.Controls.Add(this.lv_Cellpurge);
            this.tp_Cellpurge.Controls.Add(this.ajin_Setting_Cellpurge);
            this.tp_Cellpurge.Location = new System.Drawing.Point(4, 34);
            this.tp_Cellpurge.Name = "tp_Cellpurge";
            this.tp_Cellpurge.Size = new System.Drawing.Size(1726, 816);
            this.tp_Cellpurge.TabIndex = 1;
            this.tp_Cellpurge.Text = "셀 취출 이재기";
            // 
            // gbox_Cellpurge_Move
            // 
            this.gbox_Cellpurge_Move.Controls.Add(this.gbox_Cellpurge_MoveCenter);
            this.gbox_Cellpurge_Move.Controls.Add(this.gbox_Cellpurge_MoveB);
            this.gbox_Cellpurge_Move.Controls.Add(this.gbox_Cellpurge_MoveA);
            this.gbox_Cellpurge_Move.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_Cellpurge_Move.ForeColor = System.Drawing.Color.White;
            this.gbox_Cellpurge_Move.Location = new System.Drawing.Point(792, 595);
            this.gbox_Cellpurge_Move.Name = "gbox_Cellpurge_Move";
            this.gbox_Cellpurge_Move.Size = new System.Drawing.Size(904, 179);
            this.gbox_Cellpurge_Move.TabIndex = 25;
            this.gbox_Cellpurge_Move.TabStop = false;
            this.gbox_Cellpurge_Move.Text = "     이동     ";
            // 
            // gbox_Cellpurge_MoveCenter
            // 
            this.gbox_Cellpurge_MoveCenter.Controls.Add(this.tableLayoutPanel21);
            this.gbox_Cellpurge_MoveCenter.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_Cellpurge_MoveCenter.ForeColor = System.Drawing.Color.White;
            this.gbox_Cellpurge_MoveCenter.Location = new System.Drawing.Point(604, 28);
            this.gbox_Cellpurge_MoveCenter.Name = "gbox_Cellpurge_MoveCenter";
            this.gbox_Cellpurge_MoveCenter.Size = new System.Drawing.Size(293, 145);
            this.gbox_Cellpurge_MoveCenter.TabIndex = 33;
            this.gbox_Cellpurge_MoveCenter.TabStop = false;
            this.gbox_Cellpurge_MoveCenter.Text = "     중간, 언로딩    ";
            // 
            // tableLayoutPanel21
            // 
            this.tableLayoutPanel21.ColumnCount = 3;
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel21.Controls.Add(this.btn_Cellpurge_MoveCenter_Y2C, 0, 1);
            this.tableLayoutPanel21.Controls.Add(this.btn_Cellpurge_MoveCenter_BBuffer, 0, 1);
            this.tableLayoutPanel21.Controls.Add(this.btn_Cellpurge_MoveCenter_Y2Uld, 0, 1);
            this.tableLayoutPanel21.Controls.Add(this.btn_Cellpurge_MoveCenter_Y1C, 0, 0);
            this.tableLayoutPanel21.Controls.Add(this.btn_Cellpurge_MoveCenter_ABuffer, 1, 0);
            this.tableLayoutPanel21.Controls.Add(this.btn_Cellpurge_MoveCenter_Y1Uld, 2, 0);
            this.tableLayoutPanel21.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel21.Name = "tableLayoutPanel21";
            this.tableLayoutPanel21.RowCount = 2;
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel21.Size = new System.Drawing.Size(281, 111);
            this.tableLayoutPanel21.TabIndex = 32;
            // 
            // btn_Cellpurge_MoveCenter_Y2C
            // 
            this.btn_Cellpurge_MoveCenter_Y2C.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_MoveCenter_Y2C.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_Cellpurge_MoveCenter_Y2C.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_MoveCenter_Y2C.Location = new System.Drawing.Point(3, 58);
            this.btn_Cellpurge_MoveCenter_Y2C.Name = "btn_Cellpurge_MoveCenter_Y2C";
            this.btn_Cellpurge_MoveCenter_Y2C.Size = new System.Drawing.Size(87, 50);
            this.btn_Cellpurge_MoveCenter_Y2C.TabIndex = 36;
            this.btn_Cellpurge_MoveCenter_Y2C.Text = "Y2\r\n카세트\r\n중간";
            this.btn_Cellpurge_MoveCenter_Y2C.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_MoveCenter_BBuffer
            // 
            this.btn_Cellpurge_MoveCenter_BBuffer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_MoveCenter_BBuffer.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Cellpurge_MoveCenter_BBuffer.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_MoveCenter_BBuffer.Location = new System.Drawing.Point(96, 58);
            this.btn_Cellpurge_MoveCenter_BBuffer.Name = "btn_Cellpurge_MoveCenter_BBuffer";
            this.btn_Cellpurge_MoveCenter_BBuffer.Size = new System.Drawing.Size(87, 50);
            this.btn_Cellpurge_MoveCenter_BBuffer.TabIndex = 37;
            this.btn_Cellpurge_MoveCenter_BBuffer.Text = "B\r\nBUFFER";
            this.btn_Cellpurge_MoveCenter_BBuffer.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_MoveCenter_Y2Uld
            // 
            this.btn_Cellpurge_MoveCenter_Y2Uld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_MoveCenter_Y2Uld.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_Cellpurge_MoveCenter_Y2Uld.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_MoveCenter_Y2Uld.Location = new System.Drawing.Point(189, 58);
            this.btn_Cellpurge_MoveCenter_Y2Uld.Name = "btn_Cellpurge_MoveCenter_Y2Uld";
            this.btn_Cellpurge_MoveCenter_Y2Uld.Size = new System.Drawing.Size(87, 50);
            this.btn_Cellpurge_MoveCenter_Y2Uld.TabIndex = 38;
            this.btn_Cellpurge_MoveCenter_Y2Uld.Text = "Y2\r\n언로딩 이재기";
            this.btn_Cellpurge_MoveCenter_Y2Uld.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_MoveCenter_Y1C
            // 
            this.btn_Cellpurge_MoveCenter_Y1C.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_MoveCenter_Y1C.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_Cellpurge_MoveCenter_Y1C.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_MoveCenter_Y1C.Location = new System.Drawing.Point(3, 3);
            this.btn_Cellpurge_MoveCenter_Y1C.Name = "btn_Cellpurge_MoveCenter_Y1C";
            this.btn_Cellpurge_MoveCenter_Y1C.Size = new System.Drawing.Size(87, 49);
            this.btn_Cellpurge_MoveCenter_Y1C.TabIndex = 33;
            this.btn_Cellpurge_MoveCenter_Y1C.Text = "Y1\r\n카세트\r\n중간";
            this.btn_Cellpurge_MoveCenter_Y1C.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_MoveCenter_ABuffer
            // 
            this.btn_Cellpurge_MoveCenter_ABuffer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_MoveCenter_ABuffer.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Cellpurge_MoveCenter_ABuffer.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_MoveCenter_ABuffer.Location = new System.Drawing.Point(96, 3);
            this.btn_Cellpurge_MoveCenter_ABuffer.Name = "btn_Cellpurge_MoveCenter_ABuffer";
            this.btn_Cellpurge_MoveCenter_ABuffer.Size = new System.Drawing.Size(87, 49);
            this.btn_Cellpurge_MoveCenter_ABuffer.TabIndex = 34;
            this.btn_Cellpurge_MoveCenter_ABuffer.Text = "A\r\nBUFFER";
            this.btn_Cellpurge_MoveCenter_ABuffer.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_MoveCenter_Y1Uld
            // 
            this.btn_Cellpurge_MoveCenter_Y1Uld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_MoveCenter_Y1Uld.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_Cellpurge_MoveCenter_Y1Uld.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_MoveCenter_Y1Uld.Location = new System.Drawing.Point(189, 3);
            this.btn_Cellpurge_MoveCenter_Y1Uld.Name = "btn_Cellpurge_MoveCenter_Y1Uld";
            this.btn_Cellpurge_MoveCenter_Y1Uld.Size = new System.Drawing.Size(89, 49);
            this.btn_Cellpurge_MoveCenter_Y1Uld.TabIndex = 35;
            this.btn_Cellpurge_MoveCenter_Y1Uld.Text = "Y1\r\n언로딩 이재기";
            this.btn_Cellpurge_MoveCenter_Y1Uld.UseVisualStyleBackColor = false;
            // 
            // gbox_Cellpurge_MoveB
            // 
            this.gbox_Cellpurge_MoveB.Controls.Add(this.tableLayoutPanel20);
            this.gbox_Cellpurge_MoveB.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_Cellpurge_MoveB.ForeColor = System.Drawing.Color.White;
            this.gbox_Cellpurge_MoveB.Location = new System.Drawing.Point(305, 28);
            this.gbox_Cellpurge_MoveB.Name = "gbox_Cellpurge_MoveB";
            this.gbox_Cellpurge_MoveB.Size = new System.Drawing.Size(293, 145);
            this.gbox_Cellpurge_MoveB.TabIndex = 29;
            this.gbox_Cellpurge_MoveB.TabStop = false;
            this.gbox_Cellpurge_MoveB.Text = "     B    ";
            // 
            // tableLayoutPanel20
            // 
            this.tableLayoutPanel20.ColumnCount = 2;
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel20.Controls.Add(this.btn_Cellpurge_MoveB_X2BC, 0, 1);
            this.tableLayoutPanel20.Controls.Add(this.btn_Cellpurge_MoveB_X2BW, 0, 1);
            this.tableLayoutPanel20.Controls.Add(this.btn_Cellpurge_MoveB_X1BW, 1, 0);
            this.tableLayoutPanel20.Controls.Add(this.btn_Cellpurge_MoveB_X1BC, 0, 0);
            this.tableLayoutPanel20.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel20.Name = "tableLayoutPanel20";
            this.tableLayoutPanel20.RowCount = 2;
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel20.Size = new System.Drawing.Size(281, 111);
            this.tableLayoutPanel20.TabIndex = 56;
            // 
            // btn_Cellpurge_MoveB_X2BC
            // 
            this.btn_Cellpurge_MoveB_X2BC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_MoveB_X2BC.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_Cellpurge_MoveB_X2BC.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_MoveB_X2BC.Location = new System.Drawing.Point(3, 58);
            this.btn_Cellpurge_MoveB_X2BC.Name = "btn_Cellpurge_MoveB_X2BC";
            this.btn_Cellpurge_MoveB_X2BC.Size = new System.Drawing.Size(134, 50);
            this.btn_Cellpurge_MoveB_X2BC.TabIndex = 31;
            this.btn_Cellpurge_MoveB_X2BC.Text = "X2\r\nB 카세트\r\n중간";
            this.btn_Cellpurge_MoveB_X2BC.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_MoveB_X2BW
            // 
            this.btn_Cellpurge_MoveB_X2BW.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_MoveB_X2BW.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_Cellpurge_MoveB_X2BW.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_MoveB_X2BW.Location = new System.Drawing.Point(143, 58);
            this.btn_Cellpurge_MoveB_X2BW.Name = "btn_Cellpurge_MoveB_X2BW";
            this.btn_Cellpurge_MoveB_X2BW.Size = new System.Drawing.Size(135, 50);
            this.btn_Cellpurge_MoveB_X2BW.TabIndex = 32;
            this.btn_Cellpurge_MoveB_X2BW.Text = "X2\r\nB 카세트\r\n대기";
            this.btn_Cellpurge_MoveB_X2BW.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_MoveB_X1BW
            // 
            this.btn_Cellpurge_MoveB_X1BW.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_MoveB_X1BW.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_Cellpurge_MoveB_X1BW.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_MoveB_X1BW.Location = new System.Drawing.Point(143, 3);
            this.btn_Cellpurge_MoveB_X1BW.Name = "btn_Cellpurge_MoveB_X1BW";
            this.btn_Cellpurge_MoveB_X1BW.Size = new System.Drawing.Size(135, 49);
            this.btn_Cellpurge_MoveB_X1BW.TabIndex = 30;
            this.btn_Cellpurge_MoveB_X1BW.Text = "X1\r\nB 카세트\r\n대기";
            this.btn_Cellpurge_MoveB_X1BW.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_MoveB_X1BC
            // 
            this.btn_Cellpurge_MoveB_X1BC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_MoveB_X1BC.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_Cellpurge_MoveB_X1BC.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_MoveB_X1BC.Location = new System.Drawing.Point(3, 3);
            this.btn_Cellpurge_MoveB_X1BC.Name = "btn_Cellpurge_MoveB_X1BC";
            this.btn_Cellpurge_MoveB_X1BC.Size = new System.Drawing.Size(134, 49);
            this.btn_Cellpurge_MoveB_X1BC.TabIndex = 29;
            this.btn_Cellpurge_MoveB_X1BC.Text = "X1\r\nB 카세트\r\n중간";
            this.btn_Cellpurge_MoveB_X1BC.UseVisualStyleBackColor = false;
            // 
            // gbox_Cellpurge_MoveA
            // 
            this.gbox_Cellpurge_MoveA.Controls.Add(this.tableLayoutPanel19);
            this.gbox_Cellpurge_MoveA.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_Cellpurge_MoveA.ForeColor = System.Drawing.Color.White;
            this.gbox_Cellpurge_MoveA.Location = new System.Drawing.Point(6, 28);
            this.gbox_Cellpurge_MoveA.Name = "gbox_Cellpurge_MoveA";
            this.gbox_Cellpurge_MoveA.Size = new System.Drawing.Size(293, 145);
            this.gbox_Cellpurge_MoveA.TabIndex = 25;
            this.gbox_Cellpurge_MoveA.TabStop = false;
            this.gbox_Cellpurge_MoveA.Text = "     A    ";
            // 
            // tableLayoutPanel19
            // 
            this.tableLayoutPanel19.ColumnCount = 2;
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.Controls.Add(this.btn_Cellpurge_MoveA_X2AC, 0, 1);
            this.tableLayoutPanel19.Controls.Add(this.btn_Cellpurge_MoveA_X2AW, 0, 1);
            this.tableLayoutPanel19.Controls.Add(this.btn_Cellpurge_MoveA_X1AW, 1, 0);
            this.tableLayoutPanel19.Controls.Add(this.btn_Cellpurge_MoveA_X1AC, 0, 0);
            this.tableLayoutPanel19.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel19.Name = "tableLayoutPanel19";
            this.tableLayoutPanel19.RowCount = 2;
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel19.Size = new System.Drawing.Size(281, 111);
            this.tableLayoutPanel19.TabIndex = 55;
            // 
            // btn_Cellpurge_MoveA_X2AC
            // 
            this.btn_Cellpurge_MoveA_X2AC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_MoveA_X2AC.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_Cellpurge_MoveA_X2AC.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_MoveA_X2AC.Location = new System.Drawing.Point(3, 58);
            this.btn_Cellpurge_MoveA_X2AC.Name = "btn_Cellpurge_MoveA_X2AC";
            this.btn_Cellpurge_MoveA_X2AC.Size = new System.Drawing.Size(134, 50);
            this.btn_Cellpurge_MoveA_X2AC.TabIndex = 27;
            this.btn_Cellpurge_MoveA_X2AC.Text = "X2\r\nA 카세트\r\n중간";
            this.btn_Cellpurge_MoveA_X2AC.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_MoveA_X2AW
            // 
            this.btn_Cellpurge_MoveA_X2AW.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_MoveA_X2AW.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_Cellpurge_MoveA_X2AW.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_MoveA_X2AW.Location = new System.Drawing.Point(143, 58);
            this.btn_Cellpurge_MoveA_X2AW.Name = "btn_Cellpurge_MoveA_X2AW";
            this.btn_Cellpurge_MoveA_X2AW.Size = new System.Drawing.Size(135, 50);
            this.btn_Cellpurge_MoveA_X2AW.TabIndex = 28;
            this.btn_Cellpurge_MoveA_X2AW.Text = "X2\r\nA 카세트\r\n대기";
            this.btn_Cellpurge_MoveA_X2AW.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_MoveA_X1AW
            // 
            this.btn_Cellpurge_MoveA_X1AW.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_MoveA_X1AW.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_Cellpurge_MoveA_X1AW.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_MoveA_X1AW.Location = new System.Drawing.Point(143, 3);
            this.btn_Cellpurge_MoveA_X1AW.Name = "btn_Cellpurge_MoveA_X1AW";
            this.btn_Cellpurge_MoveA_X1AW.Size = new System.Drawing.Size(135, 49);
            this.btn_Cellpurge_MoveA_X1AW.TabIndex = 26;
            this.btn_Cellpurge_MoveA_X1AW.Text = "X1\r\nA 카세트\r\n대기";
            this.btn_Cellpurge_MoveA_X1AW.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_MoveA_X1AC
            // 
            this.btn_Cellpurge_MoveA_X1AC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_MoveA_X1AC.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_Cellpurge_MoveA_X1AC.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_MoveA_X1AC.Location = new System.Drawing.Point(3, 3);
            this.btn_Cellpurge_MoveA_X1AC.Name = "btn_Cellpurge_MoveA_X1AC";
            this.btn_Cellpurge_MoveA_X1AC.Size = new System.Drawing.Size(134, 49);
            this.btn_Cellpurge_MoveA_X1AC.TabIndex = 25;
            this.btn_Cellpurge_MoveA_X1AC.Text = "X1\r\nA 카세트\r\n중간";
            this.btn_Cellpurge_MoveA_X1AC.UseVisualStyleBackColor = false;
            // 
            // gbox_Cellpurge_B
            // 
            this.gbox_Cellpurge_B.Controls.Add(this.tableLayoutPanel18);
            this.gbox_Cellpurge_B.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_Cellpurge_B.ForeColor = System.Drawing.Color.White;
            this.gbox_Cellpurge_B.Location = new System.Drawing.Point(1247, 410);
            this.gbox_Cellpurge_B.Name = "gbox_Cellpurge_B";
            this.gbox_Cellpurge_B.Size = new System.Drawing.Size(449, 179);
            this.gbox_Cellpurge_B.TabIndex = 21;
            this.gbox_Cellpurge_B.TabStop = false;
            this.gbox_Cellpurge_B.Text = "     B     ";
            // 
            // tableLayoutPanel18
            // 
            this.tableLayoutPanel18.ColumnCount = 2;
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.Controls.Add(this.btn_Cellpurge_B_DestructionOn, 0, 1);
            this.tableLayoutPanel18.Controls.Add(this.btn_Cellpurge_B_DestructionOff, 0, 1);
            this.tableLayoutPanel18.Controls.Add(this.btn_Cellpurge_B_PneumaticOff, 1, 0);
            this.tableLayoutPanel18.Controls.Add(this.btn_Cellpurge_B_PneumaticOn, 0, 0);
            this.tableLayoutPanel18.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel18.Name = "tableLayoutPanel18";
            this.tableLayoutPanel18.RowCount = 2;
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel18.Size = new System.Drawing.Size(437, 145);
            this.tableLayoutPanel18.TabIndex = 55;
            // 
            // btn_Cellpurge_B_DestructionOn
            // 
            this.btn_Cellpurge_B_DestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_B_DestructionOn.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Cellpurge_B_DestructionOn.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_B_DestructionOn.Location = new System.Drawing.Point(3, 75);
            this.btn_Cellpurge_B_DestructionOn.Name = "btn_Cellpurge_B_DestructionOn";
            this.btn_Cellpurge_B_DestructionOn.Size = new System.Drawing.Size(212, 66);
            this.btn_Cellpurge_B_DestructionOn.TabIndex = 23;
            this.btn_Cellpurge_B_DestructionOn.Text = "파기 온";
            this.btn_Cellpurge_B_DestructionOn.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_B_DestructionOff
            // 
            this.btn_Cellpurge_B_DestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_B_DestructionOff.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Cellpurge_B_DestructionOff.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_B_DestructionOff.Location = new System.Drawing.Point(221, 75);
            this.btn_Cellpurge_B_DestructionOff.Name = "btn_Cellpurge_B_DestructionOff";
            this.btn_Cellpurge_B_DestructionOff.Size = new System.Drawing.Size(212, 66);
            this.btn_Cellpurge_B_DestructionOff.TabIndex = 24;
            this.btn_Cellpurge_B_DestructionOff.Text = "파기 오프";
            this.btn_Cellpurge_B_DestructionOff.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_B_PneumaticOff
            // 
            this.btn_Cellpurge_B_PneumaticOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_B_PneumaticOff.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Cellpurge_B_PneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_B_PneumaticOff.Location = new System.Drawing.Point(221, 3);
            this.btn_Cellpurge_B_PneumaticOff.Name = "btn_Cellpurge_B_PneumaticOff";
            this.btn_Cellpurge_B_PneumaticOff.Size = new System.Drawing.Size(212, 66);
            this.btn_Cellpurge_B_PneumaticOff.TabIndex = 22;
            this.btn_Cellpurge_B_PneumaticOff.Text = "공압 오프";
            this.btn_Cellpurge_B_PneumaticOff.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_B_PneumaticOn
            // 
            this.btn_Cellpurge_B_PneumaticOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_B_PneumaticOn.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Cellpurge_B_PneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_B_PneumaticOn.Location = new System.Drawing.Point(3, 3);
            this.btn_Cellpurge_B_PneumaticOn.Name = "btn_Cellpurge_B_PneumaticOn";
            this.btn_Cellpurge_B_PneumaticOn.Size = new System.Drawing.Size(212, 66);
            this.btn_Cellpurge_B_PneumaticOn.TabIndex = 21;
            this.btn_Cellpurge_B_PneumaticOn.Text = "공압 온";
            this.btn_Cellpurge_B_PneumaticOn.UseVisualStyleBackColor = false;
            // 
            // gbox_Cellpurge_A
            // 
            this.gbox_Cellpurge_A.Controls.Add(this.tableLayoutPanel17);
            this.gbox_Cellpurge_A.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_Cellpurge_A.ForeColor = System.Drawing.Color.White;
            this.gbox_Cellpurge_A.Location = new System.Drawing.Point(792, 410);
            this.gbox_Cellpurge_A.Name = "gbox_Cellpurge_A";
            this.gbox_Cellpurge_A.Size = new System.Drawing.Size(449, 179);
            this.gbox_Cellpurge_A.TabIndex = 17;
            this.gbox_Cellpurge_A.TabStop = false;
            this.gbox_Cellpurge_A.Text = "     A    ";
            // 
            // tableLayoutPanel17
            // 
            this.tableLayoutPanel17.ColumnCount = 2;
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel17.Controls.Add(this.btn_Cellpurge_A_DestructionOn, 0, 1);
            this.tableLayoutPanel17.Controls.Add(this.btn_Cellpurge_A_DestructionOff, 0, 1);
            this.tableLayoutPanel17.Controls.Add(this.btn_Cellpurge_A_PneumaticOff, 1, 0);
            this.tableLayoutPanel17.Controls.Add(this.btn_Cellpurge_A_PneumaticOn, 0, 0);
            this.tableLayoutPanel17.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel17.Name = "tableLayoutPanel17";
            this.tableLayoutPanel17.RowCount = 2;
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel17.Size = new System.Drawing.Size(437, 145);
            this.tableLayoutPanel17.TabIndex = 54;
            // 
            // btn_Cellpurge_A_DestructionOn
            // 
            this.btn_Cellpurge_A_DestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_A_DestructionOn.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Cellpurge_A_DestructionOn.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_A_DestructionOn.Location = new System.Drawing.Point(3, 75);
            this.btn_Cellpurge_A_DestructionOn.Name = "btn_Cellpurge_A_DestructionOn";
            this.btn_Cellpurge_A_DestructionOn.Size = new System.Drawing.Size(212, 66);
            this.btn_Cellpurge_A_DestructionOn.TabIndex = 19;
            this.btn_Cellpurge_A_DestructionOn.Text = "파기 온";
            this.btn_Cellpurge_A_DestructionOn.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_A_DestructionOff
            // 
            this.btn_Cellpurge_A_DestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_A_DestructionOff.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Cellpurge_A_DestructionOff.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_A_DestructionOff.Location = new System.Drawing.Point(221, 75);
            this.btn_Cellpurge_A_DestructionOff.Name = "btn_Cellpurge_A_DestructionOff";
            this.btn_Cellpurge_A_DestructionOff.Size = new System.Drawing.Size(212, 66);
            this.btn_Cellpurge_A_DestructionOff.TabIndex = 20;
            this.btn_Cellpurge_A_DestructionOff.Text = "파기 오프";
            this.btn_Cellpurge_A_DestructionOff.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_A_PneumaticOff
            // 
            this.btn_Cellpurge_A_PneumaticOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_A_PneumaticOff.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Cellpurge_A_PneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_A_PneumaticOff.Location = new System.Drawing.Point(221, 3);
            this.btn_Cellpurge_A_PneumaticOff.Name = "btn_Cellpurge_A_PneumaticOff";
            this.btn_Cellpurge_A_PneumaticOff.Size = new System.Drawing.Size(212, 66);
            this.btn_Cellpurge_A_PneumaticOff.TabIndex = 18;
            this.btn_Cellpurge_A_PneumaticOff.Text = "공압 오프";
            this.btn_Cellpurge_A_PneumaticOff.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_A_PneumaticOn
            // 
            this.btn_Cellpurge_A_PneumaticOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_A_PneumaticOn.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Cellpurge_A_PneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_A_PneumaticOn.Location = new System.Drawing.Point(3, 3);
            this.btn_Cellpurge_A_PneumaticOn.Name = "btn_Cellpurge_A_PneumaticOn";
            this.btn_Cellpurge_A_PneumaticOn.Size = new System.Drawing.Size(212, 66);
            this.btn_Cellpurge_A_PneumaticOn.TabIndex = 17;
            this.btn_Cellpurge_A_PneumaticOn.Text = "공압 온";
            this.btn_Cellpurge_A_PneumaticOn.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_Save
            // 
            this.btn_Cellpurge_Save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_Save.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Cellpurge_Save.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_Save.Location = new System.Drawing.Point(1482, 783);
            this.btn_Cellpurge_Save.Name = "btn_Cellpurge_Save";
            this.btn_Cellpurge_Save.Size = new System.Drawing.Size(228, 28);
            this.btn_Cellpurge_Save.TabIndex = 50;
            this.btn_Cellpurge_Save.Text = "저장";
            this.btn_Cellpurge_Save.UseVisualStyleBackColor = false;
            // 
            // gbox_Cellpurge_Ionizer
            // 
            this.gbox_Cellpurge_Ionizer.Controls.Add(this.btn_Cellpurge_DestructionOff);
            this.gbox_Cellpurge_Ionizer.Controls.Add(this.btn_Cellpurge_IonizerOff);
            this.gbox_Cellpurge_Ionizer.Controls.Add(this.btn_Cellpurge_DestructionOn);
            this.gbox_Cellpurge_Ionizer.Controls.Add(this.btn_Cellpurge_IonizerOn);
            this.gbox_Cellpurge_Ionizer.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_Cellpurge_Ionizer.ForeColor = System.Drawing.Color.White;
            this.gbox_Cellpurge_Ionizer.Location = new System.Drawing.Point(1483, 223);
            this.gbox_Cellpurge_Ionizer.Name = "gbox_Cellpurge_Ionizer";
            this.gbox_Cellpurge_Ionizer.Size = new System.Drawing.Size(212, 181);
            this.gbox_Cellpurge_Ionizer.TabIndex = 13;
            this.gbox_Cellpurge_Ionizer.TabStop = false;
            this.gbox_Cellpurge_Ionizer.Text = "     이오나이저     ";
            // 
            // btn_Cellpurge_DestructionOff
            // 
            this.btn_Cellpurge_DestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_DestructionOff.Location = new System.Drawing.Point(107, 103);
            this.btn_Cellpurge_DestructionOff.Name = "btn_Cellpurge_DestructionOff";
            this.btn_Cellpurge_DestructionOff.Size = new System.Drawing.Size(99, 72);
            this.btn_Cellpurge_DestructionOff.TabIndex = 16;
            this.btn_Cellpurge_DestructionOff.Text = "파기 오프";
            this.btn_Cellpurge_DestructionOff.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_IonizerOff
            // 
            this.btn_Cellpurge_IonizerOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_IonizerOff.Location = new System.Drawing.Point(107, 28);
            this.btn_Cellpurge_IonizerOff.Name = "btn_Cellpurge_IonizerOff";
            this.btn_Cellpurge_IonizerOff.Size = new System.Drawing.Size(99, 72);
            this.btn_Cellpurge_IonizerOff.TabIndex = 14;
            this.btn_Cellpurge_IonizerOff.Text = "이오나이저\r\n오프";
            this.btn_Cellpurge_IonizerOff.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_DestructionOn
            // 
            this.btn_Cellpurge_DestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_DestructionOn.Location = new System.Drawing.Point(6, 103);
            this.btn_Cellpurge_DestructionOn.Name = "btn_Cellpurge_DestructionOn";
            this.btn_Cellpurge_DestructionOn.Size = new System.Drawing.Size(99, 72);
            this.btn_Cellpurge_DestructionOn.TabIndex = 15;
            this.btn_Cellpurge_DestructionOn.Text = "파기 온";
            this.btn_Cellpurge_DestructionOn.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_IonizerOn
            // 
            this.btn_Cellpurge_IonizerOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_IonizerOn.Location = new System.Drawing.Point(6, 28);
            this.btn_Cellpurge_IonizerOn.Name = "btn_Cellpurge_IonizerOn";
            this.btn_Cellpurge_IonizerOn.Size = new System.Drawing.Size(99, 72);
            this.btn_Cellpurge_IonizerOn.TabIndex = 13;
            this.btn_Cellpurge_IonizerOn.Text = "이오나이저\r\n온";
            this.btn_Cellpurge_IonizerOn.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_SpeedSetting
            // 
            this.btn_Cellpurge_SpeedSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_SpeedSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_Cellpurge_SpeedSetting.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_SpeedSetting.Location = new System.Drawing.Point(1291, 315);
            this.btn_Cellpurge_SpeedSetting.Name = "btn_Cellpurge_SpeedSetting";
            this.btn_Cellpurge_SpeedSetting.Size = new System.Drawing.Size(90, 27);
            this.btn_Cellpurge_SpeedSetting.TabIndex = 12;
            this.btn_Cellpurge_SpeedSetting.Text = "속도 설정";
            this.btn_Cellpurge_SpeedSetting.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_AllSetting
            // 
            this.btn_Cellpurge_AllSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_AllSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_Cellpurge_AllSetting.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_AllSetting.Location = new System.Drawing.Point(1387, 273);
            this.btn_Cellpurge_AllSetting.Name = "btn_Cellpurge_AllSetting";
            this.btn_Cellpurge_AllSetting.Size = new System.Drawing.Size(90, 27);
            this.btn_Cellpurge_AllSetting.TabIndex = 8;
            this.btn_Cellpurge_AllSetting.Text = "전체 설정";
            this.btn_Cellpurge_AllSetting.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_LocationSetting
            // 
            this.btn_Cellpurge_LocationSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_LocationSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_Cellpurge_LocationSetting.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_LocationSetting.Location = new System.Drawing.Point(1071, 315);
            this.btn_Cellpurge_LocationSetting.Name = "btn_Cellpurge_LocationSetting";
            this.btn_Cellpurge_LocationSetting.Size = new System.Drawing.Size(90, 27);
            this.btn_Cellpurge_LocationSetting.TabIndex = 11;
            this.btn_Cellpurge_LocationSetting.Text = "위치 설정";
            this.btn_Cellpurge_LocationSetting.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_MoveLocation
            // 
            this.btn_Cellpurge_MoveLocation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_MoveLocation.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_Cellpurge_MoveLocation.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_MoveLocation.Location = new System.Drawing.Point(975, 315);
            this.btn_Cellpurge_MoveLocation.Name = "btn_Cellpurge_MoveLocation";
            this.btn_Cellpurge_MoveLocation.Size = new System.Drawing.Size(90, 27);
            this.btn_Cellpurge_MoveLocation.TabIndex = 10;
            this.btn_Cellpurge_MoveLocation.Text = "위치 이동";
            this.btn_Cellpurge_MoveLocation.UseVisualStyleBackColor = false;
            // 
            // tbox_Cellpurge_Location
            // 
            this.tbox_Cellpurge_Location.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_Cellpurge_Location.Location = new System.Drawing.Point(879, 316);
            this.tbox_Cellpurge_Location.Name = "tbox_Cellpurge_Location";
            this.tbox_Cellpurge_Location.Size = new System.Drawing.Size(90, 27);
            this.tbox_Cellpurge_Location.TabIndex = 9;
            // 
            // tbox_Cellpurge_Speed
            // 
            this.tbox_Cellpurge_Speed.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_Cellpurge_Speed.Location = new System.Drawing.Point(1291, 273);
            this.tbox_Cellpurge_Speed.Name = "tbox_Cellpurge_Speed";
            this.tbox_Cellpurge_Speed.Size = new System.Drawing.Size(90, 27);
            this.tbox_Cellpurge_Speed.TabIndex = 7;
            // 
            // tbox_Cellpurge_CurrentLocation
            // 
            this.tbox_Cellpurge_CurrentLocation.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_Cellpurge_CurrentLocation.Location = new System.Drawing.Point(1084, 274);
            this.tbox_Cellpurge_CurrentLocation.Name = "tbox_Cellpurge_CurrentLocation";
            this.tbox_Cellpurge_CurrentLocation.Size = new System.Drawing.Size(90, 27);
            this.tbox_Cellpurge_CurrentLocation.TabIndex = 6;
            // 
            // tbox_Cellpurge_SelectedShaft
            // 
            this.tbox_Cellpurge_SelectedShaft.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_Cellpurge_SelectedShaft.Location = new System.Drawing.Point(879, 273);
            this.tbox_Cellpurge_SelectedShaft.Name = "tbox_Cellpurge_SelectedShaft";
            this.tbox_Cellpurge_SelectedShaft.Size = new System.Drawing.Size(90, 27);
            this.tbox_Cellpurge_SelectedShaft.TabIndex = 5;
            // 
            // lbl_Cellpurge_Location
            // 
            this.lbl_Cellpurge_Location.AutoSize = true;
            this.lbl_Cellpurge_Location.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Cellpurge_Location.ForeColor = System.Drawing.Color.White;
            this.lbl_Cellpurge_Location.Location = new System.Drawing.Point(801, 316);
            this.lbl_Cellpurge_Location.Name = "lbl_Cellpurge_Location";
            this.lbl_Cellpurge_Location.Size = new System.Drawing.Size(84, 21);
            this.lbl_Cellpurge_Location.TabIndex = 0;
            this.lbl_Cellpurge_Location.Text = "위치[mm]";
            // 
            // lbl_Cellpurge_Speed
            // 
            this.lbl_Cellpurge_Speed.AutoSize = true;
            this.lbl_Cellpurge_Speed.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Cellpurge_Speed.ForeColor = System.Drawing.Color.White;
            this.lbl_Cellpurge_Speed.Location = new System.Drawing.Point(1179, 275);
            this.lbl_Cellpurge_Speed.Name = "lbl_Cellpurge_Speed";
            this.lbl_Cellpurge_Speed.Size = new System.Drawing.Size(115, 21);
            this.lbl_Cellpurge_Speed.TabIndex = 0;
            this.lbl_Cellpurge_Speed.Text = "속도[mm/sec]";
            // 
            // lbl_Cellpurge_SelectedShaft
            // 
            this.lbl_Cellpurge_SelectedShaft.AutoSize = true;
            this.lbl_Cellpurge_SelectedShaft.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Cellpurge_SelectedShaft.ForeColor = System.Drawing.Color.White;
            this.lbl_Cellpurge_SelectedShaft.Location = new System.Drawing.Point(805, 274);
            this.lbl_Cellpurge_SelectedShaft.Name = "lbl_Cellpurge_SelectedShaft";
            this.lbl_Cellpurge_SelectedShaft.Size = new System.Drawing.Size(80, 21);
            this.lbl_Cellpurge_SelectedShaft.TabIndex = 0;
            this.lbl_Cellpurge_SelectedShaft.Text = "선택된 축";
            // 
            // tbox_Cellpurge_LocationSetting
            // 
            this.tbox_Cellpurge_LocationSetting.BackColor = System.Drawing.SystemColors.Highlight;
            this.tbox_Cellpurge_LocationSetting.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.tbox_Cellpurge_LocationSetting.ForeColor = System.Drawing.Color.White;
            this.tbox_Cellpurge_LocationSetting.Location = new System.Drawing.Point(792, 223);
            this.tbox_Cellpurge_LocationSetting.Name = "tbox_Cellpurge_LocationSetting";
            this.tbox_Cellpurge_LocationSetting.ReadOnly = true;
            this.tbox_Cellpurge_LocationSetting.Size = new System.Drawing.Size(89, 27);
            this.tbox_Cellpurge_LocationSetting.TabIndex = 0;
            this.tbox_Cellpurge_LocationSetting.TabStop = false;
            this.tbox_Cellpurge_LocationSetting.Text = "위치값 설정";
            this.tbox_Cellpurge_LocationSetting.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbl_Cellpurge_CurrentLocation
            // 
            this.lbl_Cellpurge_CurrentLocation.AutoSize = true;
            this.lbl_Cellpurge_CurrentLocation.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_Cellpurge_CurrentLocation.ForeColor = System.Drawing.Color.White;
            this.lbl_Cellpurge_CurrentLocation.Location = new System.Drawing.Point(975, 274);
            this.lbl_Cellpurge_CurrentLocation.Name = "lbl_Cellpurge_CurrentLocation";
            this.lbl_Cellpurge_CurrentLocation.Size = new System.Drawing.Size(116, 21);
            this.lbl_Cellpurge_CurrentLocation.TabIndex = 0;
            this.lbl_Cellpurge_CurrentLocation.Text = "현재위치[mm]";
            // 
            // btn_Cellpurge_GrabSwitch3
            // 
            this.btn_Cellpurge_GrabSwitch3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_GrabSwitch3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Cellpurge_GrabSwitch3.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_GrabSwitch3.Location = new System.Drawing.Point(1514, 6);
            this.btn_Cellpurge_GrabSwitch3.Name = "btn_Cellpurge_GrabSwitch3";
            this.btn_Cellpurge_GrabSwitch3.Size = new System.Drawing.Size(172, 23);
            this.btn_Cellpurge_GrabSwitch3.TabIndex = 3;
            this.btn_Cellpurge_GrabSwitch3.Text = "Grab Switch 3";
            this.btn_Cellpurge_GrabSwitch3.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_GrabSwitch2
            // 
            this.btn_Cellpurge_GrabSwitch2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_GrabSwitch2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Cellpurge_GrabSwitch2.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_GrabSwitch2.Location = new System.Drawing.Point(1336, 6);
            this.btn_Cellpurge_GrabSwitch2.Name = "btn_Cellpurge_GrabSwitch2";
            this.btn_Cellpurge_GrabSwitch2.Size = new System.Drawing.Size(172, 23);
            this.btn_Cellpurge_GrabSwitch2.TabIndex = 2;
            this.btn_Cellpurge_GrabSwitch2.Text = "Grab Switch 2";
            this.btn_Cellpurge_GrabSwitch2.UseVisualStyleBackColor = false;
            // 
            // btn_Cellpurge_GrabSwitch1
            // 
            this.btn_Cellpurge_GrabSwitch1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_Cellpurge_GrabSwitch1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Cellpurge_GrabSwitch1.ForeColor = System.Drawing.Color.White;
            this.btn_Cellpurge_GrabSwitch1.Location = new System.Drawing.Point(1158, 6);
            this.btn_Cellpurge_GrabSwitch1.Name = "btn_Cellpurge_GrabSwitch1";
            this.btn_Cellpurge_GrabSwitch1.Size = new System.Drawing.Size(172, 23);
            this.btn_Cellpurge_GrabSwitch1.TabIndex = 1;
            this.btn_Cellpurge_GrabSwitch1.Text = "Grab Switch 1";
            this.btn_Cellpurge_GrabSwitch1.UseVisualStyleBackColor = false;
            // 
            // lv_Cellpurge
            // 
            this.lv_Cellpurge.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.col_Cellpurge_NamebyLocation,
            this.col_Cellpurge_Shaft,
            this.col_Cellpurge_Location,
            this.col_Cellpurge_Speed});
            this.lv_Cellpurge.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lv_Cellpurge.GridLines = true;
            this.lv_Cellpurge.Location = new System.Drawing.Point(17, 16);
            this.lv_Cellpurge.Name = "lv_Cellpurge";
            this.lv_Cellpurge.Size = new System.Drawing.Size(758, 734);
            this.lv_Cellpurge.TabIndex = 0;
            this.lv_Cellpurge.TabStop = false;
            this.lv_Cellpurge.UseCompatibleStateImageBehavior = false;
            this.lv_Cellpurge.View = System.Windows.Forms.View.Details;
            // 
            // col_Cellpurge_NamebyLocation
            // 
            this.col_Cellpurge_NamebyLocation.Text = "위치별 명칭";
            this.col_Cellpurge_NamebyLocation.Width = 430;
            // 
            // col_Cellpurge_Shaft
            // 
            this.col_Cellpurge_Shaft.Text = "축";
            this.col_Cellpurge_Shaft.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // col_Cellpurge_Location
            // 
            this.col_Cellpurge_Location.Text = "위치[mm]";
            this.col_Cellpurge_Location.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_Cellpurge_Location.Width = 125;
            // 
            // col_Cellpurge_Speed
            // 
            this.col_Cellpurge_Speed.Text = "속도[mm/s]";
            this.col_Cellpurge_Speed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_Cellpurge_Speed.Width = 125;
            // 
            // tp_CellLoad
            // 
            this.tp_CellLoad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tp_CellLoad.Controls.Add(this.gbox_CellLoad_Move);
            this.tp_CellLoad.Controls.Add(this.gbox_CellLoad_B);
            this.tp_CellLoad.Controls.Add(this.gbox_CellLoad_A);
            this.tp_CellLoad.Controls.Add(this.btn_CellLoad_Save);
            this.tp_CellLoad.Controls.Add(this.btn_CellLoad_SpeedSetting);
            this.tp_CellLoad.Controls.Add(this.btn_CellLoad_AllSetting);
            this.tp_CellLoad.Controls.Add(this.btn_CellLoad_LocationSetting);
            this.tp_CellLoad.Controls.Add(this.btn_CellLoad_MoveLocation);
            this.tp_CellLoad.Controls.Add(this.tbox_CellLoad_Location);
            this.tp_CellLoad.Controls.Add(this.tbox_CellLoad_Speed);
            this.tp_CellLoad.Controls.Add(this.tbox_CellLoad_CurrentLocation);
            this.tp_CellLoad.Controls.Add(this.tbox_CellLoad_SelectedShaft);
            this.tp_CellLoad.Controls.Add(this.lbl_CellLoad_Location);
            this.tp_CellLoad.Controls.Add(this.lbl_CellLoad_Speed);
            this.tp_CellLoad.Controls.Add(this.lbl_CellLoad_SelectedShaft);
            this.tp_CellLoad.Controls.Add(this.tbox_CellLoad_LocationSetting);
            this.tp_CellLoad.Controls.Add(this.lbl_CellLoad_CurrentLocation);
            this.tp_CellLoad.Controls.Add(this.btn_CellLoad_GrabSwitch3);
            this.tp_CellLoad.Controls.Add(this.btn_CellLoad_GrabSwitch2);
            this.tp_CellLoad.Controls.Add(this.btn_CellLoad_GrabSwitch1);
            this.tp_CellLoad.Controls.Add(this.lv_CellLoad);
            this.tp_CellLoad.Controls.Add(this.ajin_Setting_CellLoad);
            this.tp_CellLoad.Location = new System.Drawing.Point(4, 34);
            this.tp_CellLoad.Name = "tp_CellLoad";
            this.tp_CellLoad.Size = new System.Drawing.Size(1726, 816);
            this.tp_CellLoad.TabIndex = 2;
            this.tp_CellLoad.Text = "셀 로드 이재기";
            // 
            // gbox_CellLoad_Move
            // 
            this.gbox_CellLoad_Move.Controls.Add(this.gbox_CellLoad_MoveLorUn);
            this.gbox_CellLoad_Move.Controls.Add(this.gbox_CellLoad_MoveB);
            this.gbox_CellLoad_Move.Controls.Add(this.gbox_CellLoad_MoveA);
            this.gbox_CellLoad_Move.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CellLoad_Move.ForeColor = System.Drawing.Color.White;
            this.gbox_CellLoad_Move.Location = new System.Drawing.Point(792, 595);
            this.gbox_CellLoad_Move.Name = "gbox_CellLoad_Move";
            this.gbox_CellLoad_Move.Size = new System.Drawing.Size(904, 179);
            this.gbox_CellLoad_Move.TabIndex = 53;
            this.gbox_CellLoad_Move.TabStop = false;
            this.gbox_CellLoad_Move.Text = "     이동     ";
            // 
            // gbox_CellLoad_MoveLorUn
            // 
            this.gbox_CellLoad_MoveLorUn.Controls.Add(this.tableLayoutPanel28);
            this.gbox_CellLoad_MoveLorUn.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CellLoad_MoveLorUn.ForeColor = System.Drawing.Color.White;
            this.gbox_CellLoad_MoveLorUn.Location = new System.Drawing.Point(7, 21);
            this.gbox_CellLoad_MoveLorUn.Name = "gbox_CellLoad_MoveLorUn";
            this.gbox_CellLoad_MoveLorUn.Size = new System.Drawing.Size(293, 152);
            this.gbox_CellLoad_MoveLorUn.TabIndex = 57;
            this.gbox_CellLoad_MoveLorUn.TabStop = false;
            this.gbox_CellLoad_MoveLorUn.Text = "     로딩/언로딩    ";
            // 
            // tableLayoutPanel28
            // 
            this.tableLayoutPanel28.ColumnCount = 2;
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel28.Controls.Add(this.btn_CellLoad_MoveLorUn_PreAlignMark1, 0, 2);
            this.tableLayoutPanel28.Controls.Add(this.btn_CellLoad_MoveLorUn_PreAlignMark2, 0, 2);
            this.tableLayoutPanel28.Controls.Add(this.btn_CellLoad_MoveLorUn_Y1UnL, 0, 1);
            this.tableLayoutPanel28.Controls.Add(this.btn_CellLoad_MoveLorUn_Y1L, 0, 1);
            this.tableLayoutPanel28.Controls.Add(this.btn_CellLoad_MoveLorUn_X2L, 1, 0);
            this.tableLayoutPanel28.Controls.Add(this.btn_CellLoad_MoveLorUn_X1L, 0, 0);
            this.tableLayoutPanel28.Location = new System.Drawing.Point(6, 21);
            this.tableLayoutPanel28.Name = "tableLayoutPanel28";
            this.tableLayoutPanel28.RowCount = 3;
            this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel28.Size = new System.Drawing.Size(281, 125);
            this.tableLayoutPanel28.TabIndex = 55;
            // 
            // btn_CellLoad_MoveLorUn_PreAlignMark1
            // 
            this.btn_CellLoad_MoveLorUn_PreAlignMark1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveLorUn_PreAlignMark1.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveLorUn_PreAlignMark1.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveLorUn_PreAlignMark1.Location = new System.Drawing.Point(3, 85);
            this.btn_CellLoad_MoveLorUn_PreAlignMark1.Name = "btn_CellLoad_MoveLorUn_PreAlignMark1";
            this.btn_CellLoad_MoveLorUn_PreAlignMark1.Size = new System.Drawing.Size(134, 37);
            this.btn_CellLoad_MoveLorUn_PreAlignMark1.TabIndex = 60;
            this.btn_CellLoad_MoveLorUn_PreAlignMark1.Text = "PreAlign\r\nMark1";
            this.btn_CellLoad_MoveLorUn_PreAlignMark1.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_MoveLorUn_PreAlignMark2
            // 
            this.btn_CellLoad_MoveLorUn_PreAlignMark2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveLorUn_PreAlignMark2.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveLorUn_PreAlignMark2.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveLorUn_PreAlignMark2.Location = new System.Drawing.Point(143, 85);
            this.btn_CellLoad_MoveLorUn_PreAlignMark2.Name = "btn_CellLoad_MoveLorUn_PreAlignMark2";
            this.btn_CellLoad_MoveLorUn_PreAlignMark2.Size = new System.Drawing.Size(134, 37);
            this.btn_CellLoad_MoveLorUn_PreAlignMark2.TabIndex = 59;
            this.btn_CellLoad_MoveLorUn_PreAlignMark2.Text = "PreAlign\r\nMark2";
            this.btn_CellLoad_MoveLorUn_PreAlignMark2.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_MoveLorUn_Y1UnL
            // 
            this.btn_CellLoad_MoveLorUn_Y1UnL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveLorUn_Y1UnL.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveLorUn_Y1UnL.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveLorUn_Y1UnL.Location = new System.Drawing.Point(143, 44);
            this.btn_CellLoad_MoveLorUn_Y1UnL.Name = "btn_CellLoad_MoveLorUn_Y1UnL";
            this.btn_CellLoad_MoveLorUn_Y1UnL.Size = new System.Drawing.Size(134, 35);
            this.btn_CellLoad_MoveLorUn_Y1UnL.TabIndex = 58;
            this.btn_CellLoad_MoveLorUn_Y1UnL.Text = "Y1\r\n언로딩";
            this.btn_CellLoad_MoveLorUn_Y1UnL.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_MoveLorUn_Y1L
            // 
            this.btn_CellLoad_MoveLorUn_Y1L.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveLorUn_Y1L.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveLorUn_Y1L.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveLorUn_Y1L.Location = new System.Drawing.Point(3, 44);
            this.btn_CellLoad_MoveLorUn_Y1L.Name = "btn_CellLoad_MoveLorUn_Y1L";
            this.btn_CellLoad_MoveLorUn_Y1L.Size = new System.Drawing.Size(134, 35);
            this.btn_CellLoad_MoveLorUn_Y1L.TabIndex = 56;
            this.btn_CellLoad_MoveLorUn_Y1L.Text = "Y1\r\n로딩";
            this.btn_CellLoad_MoveLorUn_Y1L.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_MoveLorUn_X2L
            // 
            this.btn_CellLoad_MoveLorUn_X2L.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveLorUn_X2L.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveLorUn_X2L.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveLorUn_X2L.Location = new System.Drawing.Point(143, 3);
            this.btn_CellLoad_MoveLorUn_X2L.Name = "btn_CellLoad_MoveLorUn_X2L";
            this.btn_CellLoad_MoveLorUn_X2L.Size = new System.Drawing.Size(135, 35);
            this.btn_CellLoad_MoveLorUn_X2L.TabIndex = 55;
            this.btn_CellLoad_MoveLorUn_X2L.Text = "X2\r\n로딩";
            this.btn_CellLoad_MoveLorUn_X2L.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_MoveLorUn_X1L
            // 
            this.btn_CellLoad_MoveLorUn_X1L.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveLorUn_X1L.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveLorUn_X1L.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveLorUn_X1L.Location = new System.Drawing.Point(3, 3);
            this.btn_CellLoad_MoveLorUn_X1L.Name = "btn_CellLoad_MoveLorUn_X1L";
            this.btn_CellLoad_MoveLorUn_X1L.Size = new System.Drawing.Size(134, 35);
            this.btn_CellLoad_MoveLorUn_X1L.TabIndex = 57;
            this.btn_CellLoad_MoveLorUn_X1L.Text = "X1\r\n로딩";
            this.btn_CellLoad_MoveLorUn_X1L.UseVisualStyleBackColor = false;
            // 
            // gbox_CellLoad_MoveB
            // 
            this.gbox_CellLoad_MoveB.Controls.Add(this.tableLayoutPanel31);
            this.gbox_CellLoad_MoveB.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CellLoad_MoveB.ForeColor = System.Drawing.Color.White;
            this.gbox_CellLoad_MoveB.Location = new System.Drawing.Point(605, 21);
            this.gbox_CellLoad_MoveB.Name = "gbox_CellLoad_MoveB";
            this.gbox_CellLoad_MoveB.Size = new System.Drawing.Size(293, 152);
            this.gbox_CellLoad_MoveB.TabIndex = 56;
            this.gbox_CellLoad_MoveB.TabStop = false;
            this.gbox_CellLoad_MoveB.Text = "     B    ";
            // 
            // tableLayoutPanel31
            // 
            this.tableLayoutPanel31.ColumnCount = 1;
            this.tableLayoutPanel31.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel31.Controls.Add(this.tableLayoutPanel32, 0, 2);
            this.tableLayoutPanel31.Controls.Add(this.tableLayoutPanel33, 0, 1);
            this.tableLayoutPanel31.Controls.Add(this.tableLayoutPanel34, 0, 0);
            this.tableLayoutPanel31.Location = new System.Drawing.Point(6, 21);
            this.tableLayoutPanel31.Name = "tableLayoutPanel31";
            this.tableLayoutPanel31.RowCount = 3;
            this.tableLayoutPanel31.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel31.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel31.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel31.Size = new System.Drawing.Size(281, 125);
            this.tableLayoutPanel31.TabIndex = 1;
            // 
            // tableLayoutPanel32
            // 
            this.tableLayoutPanel32.ColumnCount = 3;
            this.tableLayoutPanel32.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel32.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel32.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel32.Controls.Add(this.btn_CellLoad_MoveB_BPickerM90Angle, 0, 0);
            this.tableLayoutPanel32.Controls.Add(this.btn_CellLoad_MoveB_BPickerP90Angle, 0, 0);
            this.tableLayoutPanel32.Controls.Add(this.btn_CellLoad_MoveB_BPicker0Angle, 0, 0);
            this.tableLayoutPanel32.Location = new System.Drawing.Point(3, 85);
            this.tableLayoutPanel32.Name = "tableLayoutPanel32";
            this.tableLayoutPanel32.RowCount = 1;
            this.tableLayoutPanel32.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel32.Size = new System.Drawing.Size(275, 35);
            this.tableLayoutPanel32.TabIndex = 2;
            // 
            // btn_CellLoad_MoveB_BPickerM90Angle
            // 
            this.btn_CellLoad_MoveB_BPickerM90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveB_BPickerM90Angle.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveB_BPickerM90Angle.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveB_BPickerM90Angle.Location = new System.Drawing.Point(185, 3);
            this.btn_CellLoad_MoveB_BPickerM90Angle.Name = "btn_CellLoad_MoveB_BPickerM90Angle";
            this.btn_CellLoad_MoveB_BPickerM90Angle.Size = new System.Drawing.Size(87, 29);
            this.btn_CellLoad_MoveB_BPickerM90Angle.TabIndex = 60;
            this.btn_CellLoad_MoveB_BPickerM90Angle.Text = "B 피커 -90도";
            this.btn_CellLoad_MoveB_BPickerM90Angle.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_MoveB_BPickerP90Angle
            // 
            this.btn_CellLoad_MoveB_BPickerP90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveB_BPickerP90Angle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveB_BPickerP90Angle.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveB_BPickerP90Angle.Location = new System.Drawing.Point(94, 3);
            this.btn_CellLoad_MoveB_BPickerP90Angle.Name = "btn_CellLoad_MoveB_BPickerP90Angle";
            this.btn_CellLoad_MoveB_BPickerP90Angle.Size = new System.Drawing.Size(85, 29);
            this.btn_CellLoad_MoveB_BPickerP90Angle.TabIndex = 59;
            this.btn_CellLoad_MoveB_BPickerP90Angle.Text = "B 피커 +90도";
            this.btn_CellLoad_MoveB_BPickerP90Angle.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_MoveB_BPicker0Angle
            // 
            this.btn_CellLoad_MoveB_BPicker0Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveB_BPicker0Angle.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveB_BPicker0Angle.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveB_BPicker0Angle.Location = new System.Drawing.Point(3, 3);
            this.btn_CellLoad_MoveB_BPicker0Angle.Name = "btn_CellLoad_MoveB_BPicker0Angle";
            this.btn_CellLoad_MoveB_BPicker0Angle.Size = new System.Drawing.Size(85, 29);
            this.btn_CellLoad_MoveB_BPicker0Angle.TabIndex = 58;
            this.btn_CellLoad_MoveB_BPicker0Angle.Text = "B 피커 0도";
            this.btn_CellLoad_MoveB_BPicker0Angle.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel33
            // 
            this.tableLayoutPanel33.ColumnCount = 2;
            this.tableLayoutPanel33.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel33.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel33.Controls.Add(this.btn_CellLoad_MoveB_BPickerUnL, 0, 0);
            this.tableLayoutPanel33.Controls.Add(this.btn_CellLoad_MoveB_BPickerL, 0, 0);
            this.tableLayoutPanel33.Location = new System.Drawing.Point(3, 44);
            this.tableLayoutPanel33.Name = "tableLayoutPanel33";
            this.tableLayoutPanel33.RowCount = 1;
            this.tableLayoutPanel33.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel33.Size = new System.Drawing.Size(275, 35);
            this.tableLayoutPanel33.TabIndex = 1;
            // 
            // btn_CellLoad_MoveB_BPickerUnL
            // 
            this.btn_CellLoad_MoveB_BPickerUnL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveB_BPickerUnL.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveB_BPickerUnL.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveB_BPickerUnL.Location = new System.Drawing.Point(140, 3);
            this.btn_CellLoad_MoveB_BPickerUnL.Name = "btn_CellLoad_MoveB_BPickerUnL";
            this.btn_CellLoad_MoveB_BPickerUnL.Size = new System.Drawing.Size(132, 29);
            this.btn_CellLoad_MoveB_BPickerUnL.TabIndex = 59;
            this.btn_CellLoad_MoveB_BPickerUnL.Text = "B 피커 언로딩";
            this.btn_CellLoad_MoveB_BPickerUnL.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_MoveB_BPickerL
            // 
            this.btn_CellLoad_MoveB_BPickerL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveB_BPickerL.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveB_BPickerL.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveB_BPickerL.Location = new System.Drawing.Point(3, 3);
            this.btn_CellLoad_MoveB_BPickerL.Name = "btn_CellLoad_MoveB_BPickerL";
            this.btn_CellLoad_MoveB_BPickerL.Size = new System.Drawing.Size(131, 29);
            this.btn_CellLoad_MoveB_BPickerL.TabIndex = 58;
            this.btn_CellLoad_MoveB_BPickerL.Text = "B 피커 로딩";
            this.btn_CellLoad_MoveB_BPickerL.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel34
            // 
            this.tableLayoutPanel34.ColumnCount = 2;
            this.tableLayoutPanel34.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel34.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel34.Controls.Add(this.btn_CellLoad_MoveB_X2BStageUnL, 0, 0);
            this.tableLayoutPanel34.Controls.Add(this.btn_CellLoad_MoveB_X1BStageUnL, 0, 0);
            this.tableLayoutPanel34.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel34.Name = "tableLayoutPanel34";
            this.tableLayoutPanel34.RowCount = 1;
            this.tableLayoutPanel34.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel34.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel34.Size = new System.Drawing.Size(275, 35);
            this.tableLayoutPanel34.TabIndex = 0;
            // 
            // btn_CellLoad_MoveB_X2BStageUnL
            // 
            this.btn_CellLoad_MoveB_X2BStageUnL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveB_X2BStageUnL.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveB_X2BStageUnL.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveB_X2BStageUnL.Location = new System.Drawing.Point(140, 3);
            this.btn_CellLoad_MoveB_X2BStageUnL.Name = "btn_CellLoad_MoveB_X2BStageUnL";
            this.btn_CellLoad_MoveB_X2BStageUnL.Size = new System.Drawing.Size(132, 29);
            this.btn_CellLoad_MoveB_X2BStageUnL.TabIndex = 59;
            this.btn_CellLoad_MoveB_X2BStageUnL.Text = "X2 B 스테이지 언로딩";
            this.btn_CellLoad_MoveB_X2BStageUnL.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_MoveB_X1BStageUnL
            // 
            this.btn_CellLoad_MoveB_X1BStageUnL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveB_X1BStageUnL.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveB_X1BStageUnL.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveB_X1BStageUnL.Location = new System.Drawing.Point(3, 3);
            this.btn_CellLoad_MoveB_X1BStageUnL.Name = "btn_CellLoad_MoveB_X1BStageUnL";
            this.btn_CellLoad_MoveB_X1BStageUnL.Size = new System.Drawing.Size(131, 29);
            this.btn_CellLoad_MoveB_X1BStageUnL.TabIndex = 58;
            this.btn_CellLoad_MoveB_X1BStageUnL.Text = "X1 B 스테이지 언로딩";
            this.btn_CellLoad_MoveB_X1BStageUnL.UseVisualStyleBackColor = false;
            // 
            // gbox_CellLoad_MoveA
            // 
            this.gbox_CellLoad_MoveA.Controls.Add(this.tableLayoutPanel26);
            this.gbox_CellLoad_MoveA.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CellLoad_MoveA.ForeColor = System.Drawing.Color.White;
            this.gbox_CellLoad_MoveA.Location = new System.Drawing.Point(306, 21);
            this.gbox_CellLoad_MoveA.Name = "gbox_CellLoad_MoveA";
            this.gbox_CellLoad_MoveA.Size = new System.Drawing.Size(293, 152);
            this.gbox_CellLoad_MoveA.TabIndex = 31;
            this.gbox_CellLoad_MoveA.TabStop = false;
            this.gbox_CellLoad_MoveA.Text = "     A    ";
            // 
            // tableLayoutPanel26
            // 
            this.tableLayoutPanel26.ColumnCount = 1;
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel26.Controls.Add(this.tableLayoutPanel30, 0, 2);
            this.tableLayoutPanel26.Controls.Add(this.tableLayoutPanel29, 0, 1);
            this.tableLayoutPanel26.Controls.Add(this.tableLayoutPanel27, 0, 0);
            this.tableLayoutPanel26.Location = new System.Drawing.Point(6, 21);
            this.tableLayoutPanel26.Name = "tableLayoutPanel26";
            this.tableLayoutPanel26.RowCount = 3;
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel26.Size = new System.Drawing.Size(281, 125);
            this.tableLayoutPanel26.TabIndex = 0;
            // 
            // tableLayoutPanel30
            // 
            this.tableLayoutPanel30.ColumnCount = 3;
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel30.Controls.Add(this.btn_CellLoad_MoveA_APickerM90Angle, 0, 0);
            this.tableLayoutPanel30.Controls.Add(this.btn_CellLoad_MoveA_APickerP90Angle, 0, 0);
            this.tableLayoutPanel30.Controls.Add(this.btn_CellLoad_MoveA_APicker0Angle, 0, 0);
            this.tableLayoutPanel30.Location = new System.Drawing.Point(3, 85);
            this.tableLayoutPanel30.Name = "tableLayoutPanel30";
            this.tableLayoutPanel30.RowCount = 1;
            this.tableLayoutPanel30.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel30.Size = new System.Drawing.Size(275, 35);
            this.tableLayoutPanel30.TabIndex = 2;
            // 
            // btn_CellLoad_MoveA_APickerM90Angle
            // 
            this.btn_CellLoad_MoveA_APickerM90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveA_APickerM90Angle.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveA_APickerM90Angle.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveA_APickerM90Angle.Location = new System.Drawing.Point(185, 3);
            this.btn_CellLoad_MoveA_APickerM90Angle.Name = "btn_CellLoad_MoveA_APickerM90Angle";
            this.btn_CellLoad_MoveA_APickerM90Angle.Size = new System.Drawing.Size(87, 29);
            this.btn_CellLoad_MoveA_APickerM90Angle.TabIndex = 60;
            this.btn_CellLoad_MoveA_APickerM90Angle.Text = "A 피커 -90도";
            this.btn_CellLoad_MoveA_APickerM90Angle.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_MoveA_APickerP90Angle
            // 
            this.btn_CellLoad_MoveA_APickerP90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveA_APickerP90Angle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveA_APickerP90Angle.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveA_APickerP90Angle.Location = new System.Drawing.Point(94, 3);
            this.btn_CellLoad_MoveA_APickerP90Angle.Name = "btn_CellLoad_MoveA_APickerP90Angle";
            this.btn_CellLoad_MoveA_APickerP90Angle.Size = new System.Drawing.Size(85, 29);
            this.btn_CellLoad_MoveA_APickerP90Angle.TabIndex = 59;
            this.btn_CellLoad_MoveA_APickerP90Angle.Text = "A 피커 +90도";
            this.btn_CellLoad_MoveA_APickerP90Angle.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_MoveA_APicker0Angle
            // 
            this.btn_CellLoad_MoveA_APicker0Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveA_APicker0Angle.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveA_APicker0Angle.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveA_APicker0Angle.Location = new System.Drawing.Point(3, 3);
            this.btn_CellLoad_MoveA_APicker0Angle.Name = "btn_CellLoad_MoveA_APicker0Angle";
            this.btn_CellLoad_MoveA_APicker0Angle.Size = new System.Drawing.Size(85, 29);
            this.btn_CellLoad_MoveA_APicker0Angle.TabIndex = 58;
            this.btn_CellLoad_MoveA_APicker0Angle.Text = "A 피커 0도";
            this.btn_CellLoad_MoveA_APicker0Angle.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel29
            // 
            this.tableLayoutPanel29.ColumnCount = 2;
            this.tableLayoutPanel29.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel29.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel29.Controls.Add(this.btn_CellLoad_MoveA_APickerUnL, 0, 0);
            this.tableLayoutPanel29.Controls.Add(this.btn_CellLoad_MoveA_APickerL, 0, 0);
            this.tableLayoutPanel29.Location = new System.Drawing.Point(3, 44);
            this.tableLayoutPanel29.Name = "tableLayoutPanel29";
            this.tableLayoutPanel29.RowCount = 1;
            this.tableLayoutPanel29.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel29.Size = new System.Drawing.Size(275, 35);
            this.tableLayoutPanel29.TabIndex = 1;
            // 
            // btn_CellLoad_MoveA_APickerUnL
            // 
            this.btn_CellLoad_MoveA_APickerUnL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveA_APickerUnL.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveA_APickerUnL.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveA_APickerUnL.Location = new System.Drawing.Point(140, 3);
            this.btn_CellLoad_MoveA_APickerUnL.Name = "btn_CellLoad_MoveA_APickerUnL";
            this.btn_CellLoad_MoveA_APickerUnL.Size = new System.Drawing.Size(132, 29);
            this.btn_CellLoad_MoveA_APickerUnL.TabIndex = 59;
            this.btn_CellLoad_MoveA_APickerUnL.Text = "A 피커 언로딩";
            this.btn_CellLoad_MoveA_APickerUnL.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_MoveA_APickerL
            // 
            this.btn_CellLoad_MoveA_APickerL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveA_APickerL.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveA_APickerL.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveA_APickerL.Location = new System.Drawing.Point(3, 3);
            this.btn_CellLoad_MoveA_APickerL.Name = "btn_CellLoad_MoveA_APickerL";
            this.btn_CellLoad_MoveA_APickerL.Size = new System.Drawing.Size(131, 29);
            this.btn_CellLoad_MoveA_APickerL.TabIndex = 58;
            this.btn_CellLoad_MoveA_APickerL.Text = "A 피커 로딩";
            this.btn_CellLoad_MoveA_APickerL.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel27
            // 
            this.tableLayoutPanel27.ColumnCount = 2;
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel27.Controls.Add(this.btn_CellLoad_MoveA_X2AStageUnL, 0, 0);
            this.tableLayoutPanel27.Controls.Add(this.btn_CellLoad_MoveA_X1AStageUnL, 0, 0);
            this.tableLayoutPanel27.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel27.Name = "tableLayoutPanel27";
            this.tableLayoutPanel27.RowCount = 1;
            this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel27.Size = new System.Drawing.Size(275, 35);
            this.tableLayoutPanel27.TabIndex = 0;
            // 
            // btn_CellLoad_MoveA_X2AStageUnL
            // 
            this.btn_CellLoad_MoveA_X2AStageUnL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveA_X2AStageUnL.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveA_X2AStageUnL.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveA_X2AStageUnL.Location = new System.Drawing.Point(140, 3);
            this.btn_CellLoad_MoveA_X2AStageUnL.Name = "btn_CellLoad_MoveA_X2AStageUnL";
            this.btn_CellLoad_MoveA_X2AStageUnL.Size = new System.Drawing.Size(132, 29);
            this.btn_CellLoad_MoveA_X2AStageUnL.TabIndex = 59;
            this.btn_CellLoad_MoveA_X2AStageUnL.Text = "X2 A 스테이지 언로딩";
            this.btn_CellLoad_MoveA_X2AStageUnL.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_MoveA_X1AStageUnL
            // 
            this.btn_CellLoad_MoveA_X1AStageUnL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveA_X1AStageUnL.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_MoveA_X1AStageUnL.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveA_X1AStageUnL.Location = new System.Drawing.Point(3, 3);
            this.btn_CellLoad_MoveA_X1AStageUnL.Name = "btn_CellLoad_MoveA_X1AStageUnL";
            this.btn_CellLoad_MoveA_X1AStageUnL.Size = new System.Drawing.Size(131, 29);
            this.btn_CellLoad_MoveA_X1AStageUnL.TabIndex = 58;
            this.btn_CellLoad_MoveA_X1AStageUnL.Text = "X1 A 스테이지 언로딩";
            this.btn_CellLoad_MoveA_X1AStageUnL.UseVisualStyleBackColor = false;
            // 
            // gbox_CellLoad_B
            // 
            this.gbox_CellLoad_B.Controls.Add(this.tableLayoutPanel25);
            this.gbox_CellLoad_B.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CellLoad_B.ForeColor = System.Drawing.Color.White;
            this.gbox_CellLoad_B.Location = new System.Drawing.Point(1247, 410);
            this.gbox_CellLoad_B.Name = "gbox_CellLoad_B";
            this.gbox_CellLoad_B.Size = new System.Drawing.Size(449, 179);
            this.gbox_CellLoad_B.TabIndex = 52;
            this.gbox_CellLoad_B.TabStop = false;
            this.gbox_CellLoad_B.Text = "     B     ";
            // 
            // tableLayoutPanel25
            // 
            this.tableLayoutPanel25.ColumnCount = 2;
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel25.Controls.Add(this.btn_CellLoad_B_PickerDestructionOn, 0, 2);
            this.tableLayoutPanel25.Controls.Add(this.btn_CellLoad_B_PickerDestructionOff, 0, 2);
            this.tableLayoutPanel25.Controls.Add(this.btn_CellLoad_B_PickerUp, 0, 0);
            this.tableLayoutPanel25.Controls.Add(this.btn_CellLoad_B_PickerDown, 1, 0);
            this.tableLayoutPanel25.Controls.Add(this.btn_CellLoad_B_PickerPneumaticOn, 0, 1);
            this.tableLayoutPanel25.Controls.Add(this.btn_CellLoad_B_PickerPneumaticOff, 1, 1);
            this.tableLayoutPanel25.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel25.Name = "tableLayoutPanel25";
            this.tableLayoutPanel25.RowCount = 3;
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel25.Size = new System.Drawing.Size(437, 145);
            this.tableLayoutPanel25.TabIndex = 55;
            // 
            // btn_CellLoad_B_PickerDestructionOn
            // 
            this.btn_CellLoad_B_PickerDestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_B_PickerDestructionOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_B_PickerDestructionOn.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_B_PickerDestructionOn.Location = new System.Drawing.Point(3, 99);
            this.btn_CellLoad_B_PickerDestructionOn.Name = "btn_CellLoad_B_PickerDestructionOn";
            this.btn_CellLoad_B_PickerDestructionOn.Size = new System.Drawing.Size(212, 43);
            this.btn_CellLoad_B_PickerDestructionOn.TabIndex = 63;
            this.btn_CellLoad_B_PickerDestructionOn.Text = "피커\r\n파기 온";
            this.btn_CellLoad_B_PickerDestructionOn.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_B_PickerDestructionOff
            // 
            this.btn_CellLoad_B_PickerDestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_B_PickerDestructionOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_B_PickerDestructionOff.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_B_PickerDestructionOff.Location = new System.Drawing.Point(221, 99);
            this.btn_CellLoad_B_PickerDestructionOff.Name = "btn_CellLoad_B_PickerDestructionOff";
            this.btn_CellLoad_B_PickerDestructionOff.Size = new System.Drawing.Size(212, 43);
            this.btn_CellLoad_B_PickerDestructionOff.TabIndex = 62;
            this.btn_CellLoad_B_PickerDestructionOff.Text = "피커\r\n파기 오프";
            this.btn_CellLoad_B_PickerDestructionOff.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_B_PickerUp
            // 
            this.btn_CellLoad_B_PickerUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_B_PickerUp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_B_PickerUp.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_B_PickerUp.Location = new System.Drawing.Point(3, 3);
            this.btn_CellLoad_B_PickerUp.Name = "btn_CellLoad_B_PickerUp";
            this.btn_CellLoad_B_PickerUp.Size = new System.Drawing.Size(212, 42);
            this.btn_CellLoad_B_PickerUp.TabIndex = 58;
            this.btn_CellLoad_B_PickerUp.Text = "피커\r\n업";
            this.btn_CellLoad_B_PickerUp.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_B_PickerDown
            // 
            this.btn_CellLoad_B_PickerDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_B_PickerDown.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_B_PickerDown.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_B_PickerDown.Location = new System.Drawing.Point(221, 3);
            this.btn_CellLoad_B_PickerDown.Name = "btn_CellLoad_B_PickerDown";
            this.btn_CellLoad_B_PickerDown.Size = new System.Drawing.Size(212, 42);
            this.btn_CellLoad_B_PickerDown.TabIndex = 59;
            this.btn_CellLoad_B_PickerDown.Text = "피커\r\n다운";
            this.btn_CellLoad_B_PickerDown.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_B_PickerPneumaticOn
            // 
            this.btn_CellLoad_B_PickerPneumaticOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_B_PickerPneumaticOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_B_PickerPneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_B_PickerPneumaticOn.Location = new System.Drawing.Point(3, 51);
            this.btn_CellLoad_B_PickerPneumaticOn.Name = "btn_CellLoad_B_PickerPneumaticOn";
            this.btn_CellLoad_B_PickerPneumaticOn.Size = new System.Drawing.Size(212, 42);
            this.btn_CellLoad_B_PickerPneumaticOn.TabIndex = 60;
            this.btn_CellLoad_B_PickerPneumaticOn.Text = "피커\r\n공압 온";
            this.btn_CellLoad_B_PickerPneumaticOn.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_B_PickerPneumaticOff
            // 
            this.btn_CellLoad_B_PickerPneumaticOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_B_PickerPneumaticOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_B_PickerPneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_B_PickerPneumaticOff.Location = new System.Drawing.Point(221, 51);
            this.btn_CellLoad_B_PickerPneumaticOff.Name = "btn_CellLoad_B_PickerPneumaticOff";
            this.btn_CellLoad_B_PickerPneumaticOff.Size = new System.Drawing.Size(212, 42);
            this.btn_CellLoad_B_PickerPneumaticOff.TabIndex = 61;
            this.btn_CellLoad_B_PickerPneumaticOff.Text = "피커\r\n공압 오프";
            this.btn_CellLoad_B_PickerPneumaticOff.UseVisualStyleBackColor = false;
            // 
            // gbox_CellLoad_A
            // 
            this.gbox_CellLoad_A.Controls.Add(this.tableLayoutPanel24);
            this.gbox_CellLoad_A.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CellLoad_A.ForeColor = System.Drawing.Color.White;
            this.gbox_CellLoad_A.Location = new System.Drawing.Point(792, 410);
            this.gbox_CellLoad_A.Name = "gbox_CellLoad_A";
            this.gbox_CellLoad_A.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gbox_CellLoad_A.Size = new System.Drawing.Size(449, 179);
            this.gbox_CellLoad_A.TabIndex = 51;
            this.gbox_CellLoad_A.TabStop = false;
            this.gbox_CellLoad_A.Text = "     A    ";
            // 
            // tableLayoutPanel24
            // 
            this.tableLayoutPanel24.ColumnCount = 2;
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel24.Controls.Add(this.btn_CellLoad_A_PickerDestructionOn, 0, 2);
            this.tableLayoutPanel24.Controls.Add(this.btn_CellLoad_A_PickerDestructionOff, 0, 2);
            this.tableLayoutPanel24.Controls.Add(this.btn_CellLoad_A_PickerUp, 0, 0);
            this.tableLayoutPanel24.Controls.Add(this.btn_CellLoad_A_PickerDown, 1, 0);
            this.tableLayoutPanel24.Controls.Add(this.btn_CellLoad_A_PickerPneumaticOn, 0, 1);
            this.tableLayoutPanel24.Controls.Add(this.btn_CellLoad_A_PickerPneumaticOff, 1, 1);
            this.tableLayoutPanel24.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel24.Name = "tableLayoutPanel24";
            this.tableLayoutPanel24.RowCount = 3;
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel24.Size = new System.Drawing.Size(437, 145);
            this.tableLayoutPanel24.TabIndex = 54;
            // 
            // btn_CellLoad_A_PickerDestructionOn
            // 
            this.btn_CellLoad_A_PickerDestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_A_PickerDestructionOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_A_PickerDestructionOn.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_A_PickerDestructionOn.Location = new System.Drawing.Point(3, 99);
            this.btn_CellLoad_A_PickerDestructionOn.Name = "btn_CellLoad_A_PickerDestructionOn";
            this.btn_CellLoad_A_PickerDestructionOn.Size = new System.Drawing.Size(212, 43);
            this.btn_CellLoad_A_PickerDestructionOn.TabIndex = 63;
            this.btn_CellLoad_A_PickerDestructionOn.Text = "피커\r\n파기 온";
            this.btn_CellLoad_A_PickerDestructionOn.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_A_PickerDestructionOff
            // 
            this.btn_CellLoad_A_PickerDestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_A_PickerDestructionOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_A_PickerDestructionOff.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_A_PickerDestructionOff.Location = new System.Drawing.Point(221, 99);
            this.btn_CellLoad_A_PickerDestructionOff.Name = "btn_CellLoad_A_PickerDestructionOff";
            this.btn_CellLoad_A_PickerDestructionOff.Size = new System.Drawing.Size(212, 43);
            this.btn_CellLoad_A_PickerDestructionOff.TabIndex = 62;
            this.btn_CellLoad_A_PickerDestructionOff.Text = "피커\r\n파기 오프";
            this.btn_CellLoad_A_PickerDestructionOff.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_A_PickerUp
            // 
            this.btn_CellLoad_A_PickerUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_A_PickerUp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_A_PickerUp.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_A_PickerUp.Location = new System.Drawing.Point(3, 3);
            this.btn_CellLoad_A_PickerUp.Name = "btn_CellLoad_A_PickerUp";
            this.btn_CellLoad_A_PickerUp.Size = new System.Drawing.Size(212, 42);
            this.btn_CellLoad_A_PickerUp.TabIndex = 58;
            this.btn_CellLoad_A_PickerUp.Text = "피커\r\n업";
            this.btn_CellLoad_A_PickerUp.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_A_PickerDown
            // 
            this.btn_CellLoad_A_PickerDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_A_PickerDown.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_A_PickerDown.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_A_PickerDown.Location = new System.Drawing.Point(221, 3);
            this.btn_CellLoad_A_PickerDown.Name = "btn_CellLoad_A_PickerDown";
            this.btn_CellLoad_A_PickerDown.Size = new System.Drawing.Size(212, 42);
            this.btn_CellLoad_A_PickerDown.TabIndex = 59;
            this.btn_CellLoad_A_PickerDown.Text = "피커\r\n다운";
            this.btn_CellLoad_A_PickerDown.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_A_PickerPneumaticOn
            // 
            this.btn_CellLoad_A_PickerPneumaticOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_A_PickerPneumaticOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_A_PickerPneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_A_PickerPneumaticOn.Location = new System.Drawing.Point(3, 51);
            this.btn_CellLoad_A_PickerPneumaticOn.Name = "btn_CellLoad_A_PickerPneumaticOn";
            this.btn_CellLoad_A_PickerPneumaticOn.Size = new System.Drawing.Size(212, 42);
            this.btn_CellLoad_A_PickerPneumaticOn.TabIndex = 60;
            this.btn_CellLoad_A_PickerPneumaticOn.Text = "피커\r\n공압 온";
            this.btn_CellLoad_A_PickerPneumaticOn.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_A_PickerPneumaticOff
            // 
            this.btn_CellLoad_A_PickerPneumaticOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_A_PickerPneumaticOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_CellLoad_A_PickerPneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_A_PickerPneumaticOff.Location = new System.Drawing.Point(221, 51);
            this.btn_CellLoad_A_PickerPneumaticOff.Name = "btn_CellLoad_A_PickerPneumaticOff";
            this.btn_CellLoad_A_PickerPneumaticOff.Size = new System.Drawing.Size(212, 42);
            this.btn_CellLoad_A_PickerPneumaticOff.TabIndex = 61;
            this.btn_CellLoad_A_PickerPneumaticOff.Text = "피커\r\n공압 오프";
            this.btn_CellLoad_A_PickerPneumaticOff.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_Save
            // 
            this.btn_CellLoad_Save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_Save.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CellLoad_Save.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_Save.Location = new System.Drawing.Point(1482, 783);
            this.btn_CellLoad_Save.Name = "btn_CellLoad_Save";
            this.btn_CellLoad_Save.Size = new System.Drawing.Size(228, 28);
            this.btn_CellLoad_Save.TabIndex = 50;
            this.btn_CellLoad_Save.Text = "저장";
            this.btn_CellLoad_Save.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_SpeedSetting
            // 
            this.btn_CellLoad_SpeedSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_SpeedSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CellLoad_SpeedSetting.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_SpeedSetting.Location = new System.Drawing.Point(1291, 315);
            this.btn_CellLoad_SpeedSetting.Name = "btn_CellLoad_SpeedSetting";
            this.btn_CellLoad_SpeedSetting.Size = new System.Drawing.Size(90, 27);
            this.btn_CellLoad_SpeedSetting.TabIndex = 48;
            this.btn_CellLoad_SpeedSetting.Text = "속도 설정";
            this.btn_CellLoad_SpeedSetting.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_AllSetting
            // 
            this.btn_CellLoad_AllSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_AllSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CellLoad_AllSetting.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_AllSetting.Location = new System.Drawing.Point(1387, 273);
            this.btn_CellLoad_AllSetting.Name = "btn_CellLoad_AllSetting";
            this.btn_CellLoad_AllSetting.Size = new System.Drawing.Size(90, 27);
            this.btn_CellLoad_AllSetting.TabIndex = 8;
            this.btn_CellLoad_AllSetting.Text = "전체 설정";
            this.btn_CellLoad_AllSetting.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_LocationSetting
            // 
            this.btn_CellLoad_LocationSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_LocationSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CellLoad_LocationSetting.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_LocationSetting.Location = new System.Drawing.Point(1071, 315);
            this.btn_CellLoad_LocationSetting.Name = "btn_CellLoad_LocationSetting";
            this.btn_CellLoad_LocationSetting.Size = new System.Drawing.Size(90, 27);
            this.btn_CellLoad_LocationSetting.TabIndex = 46;
            this.btn_CellLoad_LocationSetting.Text = "위치 설정";
            this.btn_CellLoad_LocationSetting.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_MoveLocation
            // 
            this.btn_CellLoad_MoveLocation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_MoveLocation.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CellLoad_MoveLocation.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_MoveLocation.Location = new System.Drawing.Point(975, 315);
            this.btn_CellLoad_MoveLocation.Name = "btn_CellLoad_MoveLocation";
            this.btn_CellLoad_MoveLocation.Size = new System.Drawing.Size(90, 27);
            this.btn_CellLoad_MoveLocation.TabIndex = 45;
            this.btn_CellLoad_MoveLocation.Text = "위치 이동";
            this.btn_CellLoad_MoveLocation.UseVisualStyleBackColor = false;
            // 
            // tbox_CellLoad_Location
            // 
            this.tbox_CellLoad_Location.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_CellLoad_Location.Location = new System.Drawing.Point(879, 316);
            this.tbox_CellLoad_Location.Name = "tbox_CellLoad_Location";
            this.tbox_CellLoad_Location.Size = new System.Drawing.Size(90, 27);
            this.tbox_CellLoad_Location.TabIndex = 44;
            // 
            // tbox_CellLoad_Speed
            // 
            this.tbox_CellLoad_Speed.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_CellLoad_Speed.Location = new System.Drawing.Point(1291, 273);
            this.tbox_CellLoad_Speed.Name = "tbox_CellLoad_Speed";
            this.tbox_CellLoad_Speed.Size = new System.Drawing.Size(90, 27);
            this.tbox_CellLoad_Speed.TabIndex = 7;
            // 
            // tbox_CellLoad_CurrentLocation
            // 
            this.tbox_CellLoad_CurrentLocation.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_CellLoad_CurrentLocation.Location = new System.Drawing.Point(1084, 274);
            this.tbox_CellLoad_CurrentLocation.Name = "tbox_CellLoad_CurrentLocation";
            this.tbox_CellLoad_CurrentLocation.Size = new System.Drawing.Size(90, 27);
            this.tbox_CellLoad_CurrentLocation.TabIndex = 6;
            // 
            // tbox_CellLoad_SelectedShaft
            // 
            this.tbox_CellLoad_SelectedShaft.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_CellLoad_SelectedShaft.Location = new System.Drawing.Point(879, 273);
            this.tbox_CellLoad_SelectedShaft.Name = "tbox_CellLoad_SelectedShaft";
            this.tbox_CellLoad_SelectedShaft.Size = new System.Drawing.Size(90, 27);
            this.tbox_CellLoad_SelectedShaft.TabIndex = 5;
            // 
            // lbl_CellLoad_Location
            // 
            this.lbl_CellLoad_Location.AutoSize = true;
            this.lbl_CellLoad_Location.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CellLoad_Location.ForeColor = System.Drawing.Color.White;
            this.lbl_CellLoad_Location.Location = new System.Drawing.Point(801, 316);
            this.lbl_CellLoad_Location.Name = "lbl_CellLoad_Location";
            this.lbl_CellLoad_Location.Size = new System.Drawing.Size(84, 21);
            this.lbl_CellLoad_Location.TabIndex = 40;
            this.lbl_CellLoad_Location.Text = "위치[mm]";
            // 
            // lbl_CellLoad_Speed
            // 
            this.lbl_CellLoad_Speed.AutoSize = true;
            this.lbl_CellLoad_Speed.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CellLoad_Speed.ForeColor = System.Drawing.Color.White;
            this.lbl_CellLoad_Speed.Location = new System.Drawing.Point(1179, 275);
            this.lbl_CellLoad_Speed.Name = "lbl_CellLoad_Speed";
            this.lbl_CellLoad_Speed.Size = new System.Drawing.Size(115, 21);
            this.lbl_CellLoad_Speed.TabIndex = 7;
            this.lbl_CellLoad_Speed.Text = "속도[mm/sec]";
            // 
            // lbl_CellLoad_SelectedShaft
            // 
            this.lbl_CellLoad_SelectedShaft.AutoSize = true;
            this.lbl_CellLoad_SelectedShaft.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CellLoad_SelectedShaft.ForeColor = System.Drawing.Color.White;
            this.lbl_CellLoad_SelectedShaft.Location = new System.Drawing.Point(805, 274);
            this.lbl_CellLoad_SelectedShaft.Name = "lbl_CellLoad_SelectedShaft";
            this.lbl_CellLoad_SelectedShaft.Size = new System.Drawing.Size(80, 21);
            this.lbl_CellLoad_SelectedShaft.TabIndex = 5;
            this.lbl_CellLoad_SelectedShaft.Text = "선택된 축";
            // 
            // tbox_CellLoad_LocationSetting
            // 
            this.tbox_CellLoad_LocationSetting.BackColor = System.Drawing.SystemColors.Highlight;
            this.tbox_CellLoad_LocationSetting.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.tbox_CellLoad_LocationSetting.ForeColor = System.Drawing.Color.White;
            this.tbox_CellLoad_LocationSetting.Location = new System.Drawing.Point(792, 223);
            this.tbox_CellLoad_LocationSetting.Name = "tbox_CellLoad_LocationSetting";
            this.tbox_CellLoad_LocationSetting.ReadOnly = true;
            this.tbox_CellLoad_LocationSetting.Size = new System.Drawing.Size(89, 27);
            this.tbox_CellLoad_LocationSetting.TabIndex = 5;
            this.tbox_CellLoad_LocationSetting.TabStop = false;
            this.tbox_CellLoad_LocationSetting.Text = "위치값 설정";
            this.tbox_CellLoad_LocationSetting.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbl_CellLoad_CurrentLocation
            // 
            this.lbl_CellLoad_CurrentLocation.AutoSize = true;
            this.lbl_CellLoad_CurrentLocation.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CellLoad_CurrentLocation.ForeColor = System.Drawing.Color.White;
            this.lbl_CellLoad_CurrentLocation.Location = new System.Drawing.Point(975, 274);
            this.lbl_CellLoad_CurrentLocation.Name = "lbl_CellLoad_CurrentLocation";
            this.lbl_CellLoad_CurrentLocation.Size = new System.Drawing.Size(116, 21);
            this.lbl_CellLoad_CurrentLocation.TabIndex = 6;
            this.lbl_CellLoad_CurrentLocation.Text = "현재위치[mm]";
            // 
            // btn_CellLoad_GrabSwitch3
            // 
            this.btn_CellLoad_GrabSwitch3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_GrabSwitch3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CellLoad_GrabSwitch3.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_GrabSwitch3.Location = new System.Drawing.Point(1514, 6);
            this.btn_CellLoad_GrabSwitch3.Name = "btn_CellLoad_GrabSwitch3";
            this.btn_CellLoad_GrabSwitch3.Size = new System.Drawing.Size(172, 23);
            this.btn_CellLoad_GrabSwitch3.TabIndex = 3;
            this.btn_CellLoad_GrabSwitch3.Text = "Grab Switch 3";
            this.btn_CellLoad_GrabSwitch3.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_GrabSwitch2
            // 
            this.btn_CellLoad_GrabSwitch2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_GrabSwitch2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CellLoad_GrabSwitch2.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_GrabSwitch2.Location = new System.Drawing.Point(1336, 6);
            this.btn_CellLoad_GrabSwitch2.Name = "btn_CellLoad_GrabSwitch2";
            this.btn_CellLoad_GrabSwitch2.Size = new System.Drawing.Size(172, 23);
            this.btn_CellLoad_GrabSwitch2.TabIndex = 2;
            this.btn_CellLoad_GrabSwitch2.Text = "Grab Switch 2";
            this.btn_CellLoad_GrabSwitch2.UseVisualStyleBackColor = false;
            // 
            // btn_CellLoad_GrabSwitch1
            // 
            this.btn_CellLoad_GrabSwitch1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellLoad_GrabSwitch1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CellLoad_GrabSwitch1.ForeColor = System.Drawing.Color.White;
            this.btn_CellLoad_GrabSwitch1.Location = new System.Drawing.Point(1158, 6);
            this.btn_CellLoad_GrabSwitch1.Name = "btn_CellLoad_GrabSwitch1";
            this.btn_CellLoad_GrabSwitch1.Size = new System.Drawing.Size(172, 23);
            this.btn_CellLoad_GrabSwitch1.TabIndex = 1;
            this.btn_CellLoad_GrabSwitch1.Text = "Grab Switch 1";
            this.btn_CellLoad_GrabSwitch1.UseVisualStyleBackColor = false;
            // 
            // lv_CellLoad
            // 
            this.lv_CellLoad.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.col_CellLoad_NamebyLocation,
            this.col_CellLoad_Shaft,
            this.col_CellLoad_Location,
            this.col_CellLoad_Speed});
            this.lv_CellLoad.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lv_CellLoad.GridLines = true;
            this.lv_CellLoad.Location = new System.Drawing.Point(17, 16);
            this.lv_CellLoad.Name = "lv_CellLoad";
            this.lv_CellLoad.Size = new System.Drawing.Size(758, 734);
            this.lv_CellLoad.TabIndex = 0;
            this.lv_CellLoad.TabStop = false;
            this.lv_CellLoad.UseCompatibleStateImageBehavior = false;
            this.lv_CellLoad.View = System.Windows.Forms.View.Details;
            // 
            // col_CellLoad_NamebyLocation
            // 
            this.col_CellLoad_NamebyLocation.Text = "위치별 명칭";
            this.col_CellLoad_NamebyLocation.Width = 430;
            // 
            // col_CellLoad_Shaft
            // 
            this.col_CellLoad_Shaft.Text = "축";
            this.col_CellLoad_Shaft.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // col_CellLoad_Location
            // 
            this.col_CellLoad_Location.Text = "위치[mm]";
            this.col_CellLoad_Location.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_CellLoad_Location.Width = 125;
            // 
            // col_CellLoad_Speed
            // 
            this.col_CellLoad_Speed.Text = "속도[mm/s]";
            this.col_CellLoad_Speed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_CellLoad_Speed.Width = 125;
            // 
            // tp_IRCutProcess
            // 
            this.tp_IRCutProcess.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tp_IRCutProcess.Controls.Add(this.gbox_IRCutProcess_Ionizer);
            this.tp_IRCutProcess.Controls.Add(this.gbox_IRCutProcess_Move);
            this.tp_IRCutProcess.Controls.Add(this.gbox_IRCutProcess_B);
            this.tp_IRCutProcess.Controls.Add(this.gbox_IRCutProcess_A);
            this.tp_IRCutProcess.Controls.Add(this.btn_IRCutProcess_Save);
            this.tp_IRCutProcess.Controls.Add(this.btn_IRCutProcess_SpeedSetting);
            this.tp_IRCutProcess.Controls.Add(this.btn_IRCutProcess_AllSetting);
            this.tp_IRCutProcess.Controls.Add(this.btn_IRCutProcess_LocationSetting);
            this.tp_IRCutProcess.Controls.Add(this.btn_IRCutProcess_MoveLocation);
            this.tp_IRCutProcess.Controls.Add(this.tbox_IRCutProcess_Location);
            this.tp_IRCutProcess.Controls.Add(this.tbox_IRCutProcess_Speed);
            this.tp_IRCutProcess.Controls.Add(this.tbox_IRCutProcess_CurrentLocation);
            this.tp_IRCutProcess.Controls.Add(this.tbox_IRCutProcess_SelectedShaft);
            this.tp_IRCutProcess.Controls.Add(this.lbl_IRCutProcess_Location);
            this.tp_IRCutProcess.Controls.Add(this.lbl_IRCutProcess_Speed);
            this.tp_IRCutProcess.Controls.Add(this.lbl_IRCutProcess_SelectedShaft);
            this.tp_IRCutProcess.Controls.Add(this.tbox_IRCutProcess_LocationSetting);
            this.tp_IRCutProcess.Controls.Add(this.lbl_IRCutProcess_CurrentLocation);
            this.tp_IRCutProcess.Controls.Add(this.btn_IRCutProcess_GrabSwitch3);
            this.tp_IRCutProcess.Controls.Add(this.btn_IRCutProcess_GrabSwitch2);
            this.tp_IRCutProcess.Controls.Add(this.btn_IRCutProcess_GrabSwitch1);
            this.tp_IRCutProcess.Controls.Add(this.lv_IRCutProcess);
            this.tp_IRCutProcess.Controls.Add(this.ajin_Setting_IRCutProcess);
            this.tp_IRCutProcess.Location = new System.Drawing.Point(4, 34);
            this.tp_IRCutProcess.Name = "tp_IRCutProcess";
            this.tp_IRCutProcess.Size = new System.Drawing.Size(1726, 816);
            this.tp_IRCutProcess.TabIndex = 3;
            this.tp_IRCutProcess.Text = "IR Cut 프로세스";
            // 
            // gbox_IRCutProcess_Ionizer
            // 
            this.gbox_IRCutProcess_Ionizer.Controls.Add(this.btn_IRCutProcess_Destruction);
            this.gbox_IRCutProcess_Ionizer.Controls.Add(this.btn_IRCutProcess_IonizerOff);
            this.gbox_IRCutProcess_Ionizer.Controls.Add(this.btn_IRCutProcess_DestructionOn);
            this.gbox_IRCutProcess_Ionizer.Controls.Add(this.btn_IRCutProcess_IonizerOn);
            this.gbox_IRCutProcess_Ionizer.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_IRCutProcess_Ionizer.ForeColor = System.Drawing.Color.White;
            this.gbox_IRCutProcess_Ionizer.Location = new System.Drawing.Point(1484, 223);
            this.gbox_IRCutProcess_Ionizer.Name = "gbox_IRCutProcess_Ionizer";
            this.gbox_IRCutProcess_Ionizer.Size = new System.Drawing.Size(212, 181);
            this.gbox_IRCutProcess_Ionizer.TabIndex = 50;
            this.gbox_IRCutProcess_Ionizer.TabStop = false;
            this.gbox_IRCutProcess_Ionizer.Text = "     이오나이저     ";
            // 
            // btn_IRCutProcess_Destruction
            // 
            this.btn_IRCutProcess_Destruction.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_Destruction.Location = new System.Drawing.Point(107, 103);
            this.btn_IRCutProcess_Destruction.Name = "btn_IRCutProcess_Destruction";
            this.btn_IRCutProcess_Destruction.Size = new System.Drawing.Size(99, 72);
            this.btn_IRCutProcess_Destruction.TabIndex = 6;
            this.btn_IRCutProcess_Destruction.Text = "파기 오프";
            this.btn_IRCutProcess_Destruction.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_IonizerOff
            // 
            this.btn_IRCutProcess_IonizerOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_IonizerOff.Location = new System.Drawing.Point(107, 28);
            this.btn_IRCutProcess_IonizerOff.Name = "btn_IRCutProcess_IonizerOff";
            this.btn_IRCutProcess_IonizerOff.Size = new System.Drawing.Size(99, 72);
            this.btn_IRCutProcess_IonizerOff.TabIndex = 5;
            this.btn_IRCutProcess_IonizerOff.Text = "이오나이저\r\n오프";
            this.btn_IRCutProcess_IonizerOff.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_DestructionOn
            // 
            this.btn_IRCutProcess_DestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_DestructionOn.Location = new System.Drawing.Point(6, 103);
            this.btn_IRCutProcess_DestructionOn.Name = "btn_IRCutProcess_DestructionOn";
            this.btn_IRCutProcess_DestructionOn.Size = new System.Drawing.Size(99, 72);
            this.btn_IRCutProcess_DestructionOn.TabIndex = 4;
            this.btn_IRCutProcess_DestructionOn.Text = "파기 온";
            this.btn_IRCutProcess_DestructionOn.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_IonizerOn
            // 
            this.btn_IRCutProcess_IonizerOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_IonizerOn.Location = new System.Drawing.Point(6, 28);
            this.btn_IRCutProcess_IonizerOn.Name = "btn_IRCutProcess_IonizerOn";
            this.btn_IRCutProcess_IonizerOn.Size = new System.Drawing.Size(99, 72);
            this.btn_IRCutProcess_IonizerOn.TabIndex = 3;
            this.btn_IRCutProcess_IonizerOn.Text = "이오나이저\r\n온";
            this.btn_IRCutProcess_IonizerOn.UseVisualStyleBackColor = false;
            // 
            // gbox_IRCutProcess_Move
            // 
            this.gbox_IRCutProcess_Move.Controls.Add(this.gbox_IRCutProcess_RightOffset);
            this.gbox_IRCutProcess_Move.Controls.Add(this.gbox_IRCutProcess_LeftOffset);
            this.gbox_IRCutProcess_Move.Controls.Add(this.gbox_IRCutProcess_MoveB);
            this.gbox_IRCutProcess_Move.Controls.Add(this.gbox_IRCutProcess_MoveA);
            this.gbox_IRCutProcess_Move.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_IRCutProcess_Move.ForeColor = System.Drawing.Color.White;
            this.gbox_IRCutProcess_Move.Location = new System.Drawing.Point(792, 595);
            this.gbox_IRCutProcess_Move.Name = "gbox_IRCutProcess_Move";
            this.gbox_IRCutProcess_Move.Size = new System.Drawing.Size(904, 179);
            this.gbox_IRCutProcess_Move.TabIndex = 53;
            this.gbox_IRCutProcess_Move.TabStop = false;
            this.gbox_IRCutProcess_Move.Text = "     이동     ";
            // 
            // gbox_IRCutProcess_RightOffset
            // 
            this.gbox_IRCutProcess_RightOffset.Controls.Add(this.tableLayoutPanel40);
            this.gbox_IRCutProcess_RightOffset.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_IRCutProcess_RightOffset.ForeColor = System.Drawing.Color.White;
            this.gbox_IRCutProcess_RightOffset.Location = new System.Drawing.Point(678, 19);
            this.gbox_IRCutProcess_RightOffset.Name = "gbox_IRCutProcess_RightOffset";
            this.gbox_IRCutProcess_RightOffset.Size = new System.Drawing.Size(212, 154);
            this.gbox_IRCutProcess_RightOffset.TabIndex = 32;
            this.gbox_IRCutProcess_RightOffset.TabStop = false;
            this.gbox_IRCutProcess_RightOffset.Text = "     우측 오프셋    ";
            // 
            // tableLayoutPanel40
            // 
            this.tableLayoutPanel40.ColumnCount = 2;
            this.tableLayoutPanel40.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel40.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel40.Controls.Add(this.btn_IRCutProcess_RightOffset_Right_Vision2Focus, 0, 1);
            this.tableLayoutPanel40.Controls.Add(this.btn_IRCutProcess_RightOffset_Right_Focus2Vision, 0, 1);
            this.tableLayoutPanel40.Controls.Add(this.btn_IRCutProcess_RightOffset_Left_Vision2Focus, 0, 0);
            this.tableLayoutPanel40.Controls.Add(this.btn_IRCutProcess_RightOffset_Left_Focus2Vision, 1, 0);
            this.tableLayoutPanel40.Location = new System.Drawing.Point(6, 25);
            this.tableLayoutPanel40.Name = "tableLayoutPanel40";
            this.tableLayoutPanel40.RowCount = 2;
            this.tableLayoutPanel40.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel40.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel40.Size = new System.Drawing.Size(202, 118);
            this.tableLayoutPanel40.TabIndex = 34;
            // 
            // btn_IRCutProcess_RightOffset_Right_Vision2Focus
            // 
            this.btn_IRCutProcess_RightOffset_Right_Vision2Focus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_RightOffset_Right_Vision2Focus.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_RightOffset_Right_Vision2Focus.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_RightOffset_Right_Vision2Focus.Location = new System.Drawing.Point(3, 62);
            this.btn_IRCutProcess_RightOffset_Right_Vision2Focus.Name = "btn_IRCutProcess_RightOffset_Right_Vision2Focus";
            this.btn_IRCutProcess_RightOffset_Right_Vision2Focus.Size = new System.Drawing.Size(95, 53);
            this.btn_IRCutProcess_RightOffset_Right_Vision2Focus.TabIndex = 66;
            this.btn_IRCutProcess_RightOffset_Right_Vision2Focus.Text = "우측\r\n비젼 ▶ 포커스";
            this.btn_IRCutProcess_RightOffset_Right_Vision2Focus.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_RightOffset_Right_Focus2Vision
            // 
            this.btn_IRCutProcess_RightOffset_Right_Focus2Vision.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_RightOffset_Right_Focus2Vision.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_RightOffset_Right_Focus2Vision.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_RightOffset_Right_Focus2Vision.Location = new System.Drawing.Point(104, 62);
            this.btn_IRCutProcess_RightOffset_Right_Focus2Vision.Name = "btn_IRCutProcess_RightOffset_Right_Focus2Vision";
            this.btn_IRCutProcess_RightOffset_Right_Focus2Vision.Size = new System.Drawing.Size(95, 53);
            this.btn_IRCutProcess_RightOffset_Right_Focus2Vision.TabIndex = 65;
            this.btn_IRCutProcess_RightOffset_Right_Focus2Vision.Text = "우측\r\n포커스 ▶ 비젼";
            this.btn_IRCutProcess_RightOffset_Right_Focus2Vision.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_RightOffset_Left_Vision2Focus
            // 
            this.btn_IRCutProcess_RightOffset_Left_Vision2Focus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_RightOffset_Left_Vision2Focus.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_RightOffset_Left_Vision2Focus.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_RightOffset_Left_Vision2Focus.Location = new System.Drawing.Point(3, 3);
            this.btn_IRCutProcess_RightOffset_Left_Vision2Focus.Name = "btn_IRCutProcess_RightOffset_Left_Vision2Focus";
            this.btn_IRCutProcess_RightOffset_Left_Vision2Focus.Size = new System.Drawing.Size(95, 53);
            this.btn_IRCutProcess_RightOffset_Left_Vision2Focus.TabIndex = 63;
            this.btn_IRCutProcess_RightOffset_Left_Vision2Focus.Text = "좌측\r\n비젼 ▶ 포커스";
            this.btn_IRCutProcess_RightOffset_Left_Vision2Focus.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_RightOffset_Left_Focus2Vision
            // 
            this.btn_IRCutProcess_RightOffset_Left_Focus2Vision.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_RightOffset_Left_Focus2Vision.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_RightOffset_Left_Focus2Vision.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_RightOffset_Left_Focus2Vision.Location = new System.Drawing.Point(104, 3);
            this.btn_IRCutProcess_RightOffset_Left_Focus2Vision.Name = "btn_IRCutProcess_RightOffset_Left_Focus2Vision";
            this.btn_IRCutProcess_RightOffset_Left_Focus2Vision.Size = new System.Drawing.Size(95, 53);
            this.btn_IRCutProcess_RightOffset_Left_Focus2Vision.TabIndex = 64;
            this.btn_IRCutProcess_RightOffset_Left_Focus2Vision.Text = "좌측\r\n포커스 ▶ 비젼";
            this.btn_IRCutProcess_RightOffset_Left_Focus2Vision.UseVisualStyleBackColor = false;
            // 
            // gbox_IRCutProcess_LeftOffset
            // 
            this.gbox_IRCutProcess_LeftOffset.Controls.Add(this.tableLayoutPanel39);
            this.gbox_IRCutProcess_LeftOffset.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_IRCutProcess_LeftOffset.ForeColor = System.Drawing.Color.White;
            this.gbox_IRCutProcess_LeftOffset.Location = new System.Drawing.Point(457, 19);
            this.gbox_IRCutProcess_LeftOffset.Name = "gbox_IRCutProcess_LeftOffset";
            this.gbox_IRCutProcess_LeftOffset.Size = new System.Drawing.Size(212, 154);
            this.gbox_IRCutProcess_LeftOffset.TabIndex = 32;
            this.gbox_IRCutProcess_LeftOffset.TabStop = false;
            this.gbox_IRCutProcess_LeftOffset.Text = "     좌측 오프셋    ";
            // 
            // tableLayoutPanel39
            // 
            this.tableLayoutPanel39.ColumnCount = 2;
            this.tableLayoutPanel39.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel39.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel39.Controls.Add(this.btn_IRCutProcess_LeftOffset_Right_Visoin2Focus, 0, 1);
            this.tableLayoutPanel39.Controls.Add(this.btn_IRCutProcess_LeftOffset_Right_Focus2Visoin, 0, 1);
            this.tableLayoutPanel39.Controls.Add(this.btn_IRCutProcess_LeftOffset_Left_Vision2Focus, 0, 0);
            this.tableLayoutPanel39.Controls.Add(this.btn_IRCutProcess_LeftOffset_Left_Focus2Vision, 1, 0);
            this.tableLayoutPanel39.Location = new System.Drawing.Point(4, 25);
            this.tableLayoutPanel39.Name = "tableLayoutPanel39";
            this.tableLayoutPanel39.RowCount = 2;
            this.tableLayoutPanel39.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel39.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel39.Size = new System.Drawing.Size(202, 118);
            this.tableLayoutPanel39.TabIndex = 33;
            // 
            // btn_IRCutProcess_LeftOffset_Right_Visoin2Focus
            // 
            this.btn_IRCutProcess_LeftOffset_Right_Visoin2Focus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_LeftOffset_Right_Visoin2Focus.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_LeftOffset_Right_Visoin2Focus.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_LeftOffset_Right_Visoin2Focus.Location = new System.Drawing.Point(3, 62);
            this.btn_IRCutProcess_LeftOffset_Right_Visoin2Focus.Name = "btn_IRCutProcess_LeftOffset_Right_Visoin2Focus";
            this.btn_IRCutProcess_LeftOffset_Right_Visoin2Focus.Size = new System.Drawing.Size(95, 53);
            this.btn_IRCutProcess_LeftOffset_Right_Visoin2Focus.TabIndex = 66;
            this.btn_IRCutProcess_LeftOffset_Right_Visoin2Focus.Text = "우측\r\n비젼 ▶ 포커스";
            this.btn_IRCutProcess_LeftOffset_Right_Visoin2Focus.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_LeftOffset_Right_Focus2Visoin
            // 
            this.btn_IRCutProcess_LeftOffset_Right_Focus2Visoin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_LeftOffset_Right_Focus2Visoin.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_LeftOffset_Right_Focus2Visoin.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_LeftOffset_Right_Focus2Visoin.Location = new System.Drawing.Point(104, 62);
            this.btn_IRCutProcess_LeftOffset_Right_Focus2Visoin.Name = "btn_IRCutProcess_LeftOffset_Right_Focus2Visoin";
            this.btn_IRCutProcess_LeftOffset_Right_Focus2Visoin.Size = new System.Drawing.Size(95, 53);
            this.btn_IRCutProcess_LeftOffset_Right_Focus2Visoin.TabIndex = 65;
            this.btn_IRCutProcess_LeftOffset_Right_Focus2Visoin.Text = "우측\r\n포커스 ▶ 비젼";
            this.btn_IRCutProcess_LeftOffset_Right_Focus2Visoin.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_LeftOffset_Left_Vision2Focus
            // 
            this.btn_IRCutProcess_LeftOffset_Left_Vision2Focus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_LeftOffset_Left_Vision2Focus.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_LeftOffset_Left_Vision2Focus.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_LeftOffset_Left_Vision2Focus.Location = new System.Drawing.Point(3, 3);
            this.btn_IRCutProcess_LeftOffset_Left_Vision2Focus.Name = "btn_IRCutProcess_LeftOffset_Left_Vision2Focus";
            this.btn_IRCutProcess_LeftOffset_Left_Vision2Focus.Size = new System.Drawing.Size(95, 53);
            this.btn_IRCutProcess_LeftOffset_Left_Vision2Focus.TabIndex = 63;
            this.btn_IRCutProcess_LeftOffset_Left_Vision2Focus.Text = "좌측\r\n비젼 ▶ 포커스";
            this.btn_IRCutProcess_LeftOffset_Left_Vision2Focus.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_LeftOffset_Left_Focus2Vision
            // 
            this.btn_IRCutProcess_LeftOffset_Left_Focus2Vision.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_LeftOffset_Left_Focus2Vision.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_LeftOffset_Left_Focus2Vision.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_LeftOffset_Left_Focus2Vision.Location = new System.Drawing.Point(104, 3);
            this.btn_IRCutProcess_LeftOffset_Left_Focus2Vision.Name = "btn_IRCutProcess_LeftOffset_Left_Focus2Vision";
            this.btn_IRCutProcess_LeftOffset_Left_Focus2Vision.Size = new System.Drawing.Size(95, 53);
            this.btn_IRCutProcess_LeftOffset_Left_Focus2Vision.TabIndex = 64;
            this.btn_IRCutProcess_LeftOffset_Left_Focus2Vision.Text = "좌측\r\n포커스 ▶ 비젼";
            this.btn_IRCutProcess_LeftOffset_Left_Focus2Vision.UseVisualStyleBackColor = false;
            // 
            // gbox_IRCutProcess_MoveB
            // 
            this.gbox_IRCutProcess_MoveB.Controls.Add(this.tableLayoutPanel38);
            this.gbox_IRCutProcess_MoveB.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_IRCutProcess_MoveB.ForeColor = System.Drawing.Color.White;
            this.gbox_IRCutProcess_MoveB.Location = new System.Drawing.Point(237, 19);
            this.gbox_IRCutProcess_MoveB.Name = "gbox_IRCutProcess_MoveB";
            this.gbox_IRCutProcess_MoveB.Size = new System.Drawing.Size(212, 154);
            this.gbox_IRCutProcess_MoveB.TabIndex = 31;
            this.gbox_IRCutProcess_MoveB.TabStop = false;
            this.gbox_IRCutProcess_MoveB.Text = "     B    ";
            // 
            // tableLayoutPanel38
            // 
            this.tableLayoutPanel38.ColumnCount = 2;
            this.tableLayoutPanel38.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel38.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel38.Controls.Add(this.btn_IRCutProcess_MoveB_R1Camera, 0, 2);
            this.tableLayoutPanel38.Controls.Add(this.btn_IRCutProcess_MoveB_R2Camera, 0, 2);
            this.tableLayoutPanel38.Controls.Add(this.btn_IRCutProcess_MoveB_RightCellLoad, 0, 0);
            this.tableLayoutPanel38.Controls.Add(this.btn_IRCutProcess_MoveB_RightCellUnload, 1, 0);
            this.tableLayoutPanel38.Controls.Add(this.btn_IRCutProcess_MoveB_R1Laser, 0, 1);
            this.tableLayoutPanel38.Controls.Add(this.btn_IRCutProcess_MoveB_R2Laser, 1, 1);
            this.tableLayoutPanel38.Location = new System.Drawing.Point(4, 25);
            this.tableLayoutPanel38.Name = "tableLayoutPanel38";
            this.tableLayoutPanel38.RowCount = 3;
            this.tableLayoutPanel38.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel38.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel38.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel38.Size = new System.Drawing.Size(206, 123);
            this.tableLayoutPanel38.TabIndex = 55;
            // 
            // btn_IRCutProcess_MoveB_R1Camera
            // 
            this.btn_IRCutProcess_MoveB_R1Camera.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_MoveB_R1Camera.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_MoveB_R1Camera.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_MoveB_R1Camera.Location = new System.Drawing.Point(3, 85);
            this.btn_IRCutProcess_MoveB_R1Camera.Name = "btn_IRCutProcess_MoveB_R1Camera";
            this.btn_IRCutProcess_MoveB_R1Camera.Size = new System.Drawing.Size(97, 35);
            this.btn_IRCutProcess_MoveB_R1Camera.TabIndex = 65;
            this.btn_IRCutProcess_MoveB_R1Camera.Text = "우측1\r\n카메라 확인";
            this.btn_IRCutProcess_MoveB_R1Camera.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_MoveB_R2Camera
            // 
            this.btn_IRCutProcess_MoveB_R2Camera.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_MoveB_R2Camera.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_MoveB_R2Camera.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_MoveB_R2Camera.Location = new System.Drawing.Point(106, 85);
            this.btn_IRCutProcess_MoveB_R2Camera.Name = "btn_IRCutProcess_MoveB_R2Camera";
            this.btn_IRCutProcess_MoveB_R2Camera.Size = new System.Drawing.Size(97, 35);
            this.btn_IRCutProcess_MoveB_R2Camera.TabIndex = 64;
            this.btn_IRCutProcess_MoveB_R2Camera.Text = "우측2\r\n카메라 확인";
            this.btn_IRCutProcess_MoveB_R2Camera.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_MoveB_RightCellLoad
            // 
            this.btn_IRCutProcess_MoveB_RightCellLoad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_MoveB_RightCellLoad.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_MoveB_RightCellLoad.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_MoveB_RightCellLoad.Location = new System.Drawing.Point(3, 3);
            this.btn_IRCutProcess_MoveB_RightCellLoad.Name = "btn_IRCutProcess_MoveB_RightCellLoad";
            this.btn_IRCutProcess_MoveB_RightCellLoad.Size = new System.Drawing.Size(97, 34);
            this.btn_IRCutProcess_MoveB_RightCellLoad.TabIndex = 60;
            this.btn_IRCutProcess_MoveB_RightCellLoad.Text = "우측 셀 로드";
            this.btn_IRCutProcess_MoveB_RightCellLoad.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_MoveB_RightCellUnload
            // 
            this.btn_IRCutProcess_MoveB_RightCellUnload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_MoveB_RightCellUnload.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_MoveB_RightCellUnload.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_MoveB_RightCellUnload.Location = new System.Drawing.Point(106, 3);
            this.btn_IRCutProcess_MoveB_RightCellUnload.Name = "btn_IRCutProcess_MoveB_RightCellUnload";
            this.btn_IRCutProcess_MoveB_RightCellUnload.Size = new System.Drawing.Size(97, 34);
            this.btn_IRCutProcess_MoveB_RightCellUnload.TabIndex = 62;
            this.btn_IRCutProcess_MoveB_RightCellUnload.Text = "우측 셀 언로드";
            this.btn_IRCutProcess_MoveB_RightCellUnload.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_MoveB_R1Laser
            // 
            this.btn_IRCutProcess_MoveB_R1Laser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_MoveB_R1Laser.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_MoveB_R1Laser.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_MoveB_R1Laser.Location = new System.Drawing.Point(3, 44);
            this.btn_IRCutProcess_MoveB_R1Laser.Name = "btn_IRCutProcess_MoveB_R1Laser";
            this.btn_IRCutProcess_MoveB_R1Laser.Size = new System.Drawing.Size(97, 34);
            this.btn_IRCutProcess_MoveB_R1Laser.TabIndex = 61;
            this.btn_IRCutProcess_MoveB_R1Laser.Text = "우측1 레이저샷";
            this.btn_IRCutProcess_MoveB_R1Laser.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_MoveB_R2Laser
            // 
            this.btn_IRCutProcess_MoveB_R2Laser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_MoveB_R2Laser.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_MoveB_R2Laser.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_MoveB_R2Laser.Location = new System.Drawing.Point(106, 44);
            this.btn_IRCutProcess_MoveB_R2Laser.Name = "btn_IRCutProcess_MoveB_R2Laser";
            this.btn_IRCutProcess_MoveB_R2Laser.Size = new System.Drawing.Size(97, 34);
            this.btn_IRCutProcess_MoveB_R2Laser.TabIndex = 63;
            this.btn_IRCutProcess_MoveB_R2Laser.Text = "우측2 레이저샷";
            this.btn_IRCutProcess_MoveB_R2Laser.UseVisualStyleBackColor = false;
            // 
            // gbox_IRCutProcess_MoveA
            // 
            this.gbox_IRCutProcess_MoveA.Controls.Add(this.tableLayoutPanel37);
            this.gbox_IRCutProcess_MoveA.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_IRCutProcess_MoveA.ForeColor = System.Drawing.Color.White;
            this.gbox_IRCutProcess_MoveA.Location = new System.Drawing.Point(14, 19);
            this.gbox_IRCutProcess_MoveA.Name = "gbox_IRCutProcess_MoveA";
            this.gbox_IRCutProcess_MoveA.Size = new System.Drawing.Size(215, 154);
            this.gbox_IRCutProcess_MoveA.TabIndex = 30;
            this.gbox_IRCutProcess_MoveA.TabStop = false;
            this.gbox_IRCutProcess_MoveA.Text = "     A    ";
            // 
            // tableLayoutPanel37
            // 
            this.tableLayoutPanel37.ColumnCount = 2;
            this.tableLayoutPanel37.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel37.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel37.Controls.Add(this.btn_IRCutProcess_MoveA_L1Camera, 0, 2);
            this.tableLayoutPanel37.Controls.Add(this.btn_IRCutProcess_MoveA_L2Camera, 0, 2);
            this.tableLayoutPanel37.Controls.Add(this.btn_IRCutProcess_MoveA_LeftCellLoad, 0, 0);
            this.tableLayoutPanel37.Controls.Add(this.btn_IRCutProcess_MoveA_LeftCellUnload, 1, 0);
            this.tableLayoutPanel37.Controls.Add(this.btn_IRCutProcess_MoveA_L1Laser, 0, 1);
            this.tableLayoutPanel37.Controls.Add(this.btn_IRCutProcess_MoveA_L2Laser, 1, 1);
            this.tableLayoutPanel37.Location = new System.Drawing.Point(3, 25);
            this.tableLayoutPanel37.Name = "tableLayoutPanel37";
            this.tableLayoutPanel37.RowCount = 3;
            this.tableLayoutPanel37.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel37.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel37.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel37.Size = new System.Drawing.Size(206, 123);
            this.tableLayoutPanel37.TabIndex = 54;
            // 
            // btn_IRCutProcess_MoveA_L1Camera
            // 
            this.btn_IRCutProcess_MoveA_L1Camera.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_MoveA_L1Camera.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_MoveA_L1Camera.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_MoveA_L1Camera.Location = new System.Drawing.Point(3, 85);
            this.btn_IRCutProcess_MoveA_L1Camera.Name = "btn_IRCutProcess_MoveA_L1Camera";
            this.btn_IRCutProcess_MoveA_L1Camera.Size = new System.Drawing.Size(97, 35);
            this.btn_IRCutProcess_MoveA_L1Camera.TabIndex = 65;
            this.btn_IRCutProcess_MoveA_L1Camera.Text = "좌측1\r\n카메라 확인";
            this.btn_IRCutProcess_MoveA_L1Camera.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_MoveA_L2Camera
            // 
            this.btn_IRCutProcess_MoveA_L2Camera.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_MoveA_L2Camera.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_MoveA_L2Camera.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_MoveA_L2Camera.Location = new System.Drawing.Point(106, 85);
            this.btn_IRCutProcess_MoveA_L2Camera.Name = "btn_IRCutProcess_MoveA_L2Camera";
            this.btn_IRCutProcess_MoveA_L2Camera.Size = new System.Drawing.Size(97, 35);
            this.btn_IRCutProcess_MoveA_L2Camera.TabIndex = 64;
            this.btn_IRCutProcess_MoveA_L2Camera.Text = "좌측2\r\n카메라 확인";
            this.btn_IRCutProcess_MoveA_L2Camera.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_MoveA_LeftCellLoad
            // 
            this.btn_IRCutProcess_MoveA_LeftCellLoad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_MoveA_LeftCellLoad.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_MoveA_LeftCellLoad.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_MoveA_LeftCellLoad.Location = new System.Drawing.Point(3, 3);
            this.btn_IRCutProcess_MoveA_LeftCellLoad.Name = "btn_IRCutProcess_MoveA_LeftCellLoad";
            this.btn_IRCutProcess_MoveA_LeftCellLoad.Size = new System.Drawing.Size(97, 34);
            this.btn_IRCutProcess_MoveA_LeftCellLoad.TabIndex = 60;
            this.btn_IRCutProcess_MoveA_LeftCellLoad.Text = "좌측 셀 로드";
            this.btn_IRCutProcess_MoveA_LeftCellLoad.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_MoveA_LeftCellUnload
            // 
            this.btn_IRCutProcess_MoveA_LeftCellUnload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_MoveA_LeftCellUnload.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_MoveA_LeftCellUnload.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_MoveA_LeftCellUnload.Location = new System.Drawing.Point(106, 3);
            this.btn_IRCutProcess_MoveA_LeftCellUnload.Name = "btn_IRCutProcess_MoveA_LeftCellUnload";
            this.btn_IRCutProcess_MoveA_LeftCellUnload.Size = new System.Drawing.Size(97, 34);
            this.btn_IRCutProcess_MoveA_LeftCellUnload.TabIndex = 62;
            this.btn_IRCutProcess_MoveA_LeftCellUnload.Text = "좌측 셀 언로드";
            this.btn_IRCutProcess_MoveA_LeftCellUnload.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_MoveA_L1Laser
            // 
            this.btn_IRCutProcess_MoveA_L1Laser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_MoveA_L1Laser.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_MoveA_L1Laser.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_MoveA_L1Laser.Location = new System.Drawing.Point(3, 44);
            this.btn_IRCutProcess_MoveA_L1Laser.Name = "btn_IRCutProcess_MoveA_L1Laser";
            this.btn_IRCutProcess_MoveA_L1Laser.Size = new System.Drawing.Size(97, 34);
            this.btn_IRCutProcess_MoveA_L1Laser.TabIndex = 61;
            this.btn_IRCutProcess_MoveA_L1Laser.Text = "좌측1 레이저샷";
            this.btn_IRCutProcess_MoveA_L1Laser.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_MoveA_L2Laser
            // 
            this.btn_IRCutProcess_MoveA_L2Laser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_MoveA_L2Laser.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_MoveA_L2Laser.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_MoveA_L2Laser.Location = new System.Drawing.Point(106, 44);
            this.btn_IRCutProcess_MoveA_L2Laser.Name = "btn_IRCutProcess_MoveA_L2Laser";
            this.btn_IRCutProcess_MoveA_L2Laser.Size = new System.Drawing.Size(97, 34);
            this.btn_IRCutProcess_MoveA_L2Laser.TabIndex = 63;
            this.btn_IRCutProcess_MoveA_L2Laser.Text = "좌측2 레이저샷";
            this.btn_IRCutProcess_MoveA_L2Laser.UseVisualStyleBackColor = false;
            // 
            // gbox_IRCutProcess_B
            // 
            this.gbox_IRCutProcess_B.Controls.Add(this.tableLayoutPanel36);
            this.gbox_IRCutProcess_B.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_IRCutProcess_B.ForeColor = System.Drawing.Color.White;
            this.gbox_IRCutProcess_B.Location = new System.Drawing.Point(1247, 397);
            this.gbox_IRCutProcess_B.Name = "gbox_IRCutProcess_B";
            this.gbox_IRCutProcess_B.Size = new System.Drawing.Size(449, 192);
            this.gbox_IRCutProcess_B.TabIndex = 52;
            this.gbox_IRCutProcess_B.TabStop = false;
            this.gbox_IRCutProcess_B.Text = "     B     ";
            // 
            // tableLayoutPanel36
            // 
            this.tableLayoutPanel36.ColumnCount = 4;
            this.tableLayoutPanel36.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel36.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel36.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel36.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel36.Controls.Add(this.btn_IRCutProcess_B_R1Pneumatic_Ch1Off, 1, 0);
            this.tableLayoutPanel36.Controls.Add(this.btn_IRCutProcess_B_R2Pneumatic_Ch1On, 2, 0);
            this.tableLayoutPanel36.Controls.Add(this.btn_IRCutProcess_B_R2Pneumatic_Ch1Off, 3, 0);
            this.tableLayoutPanel36.Controls.Add(this.btn_IRCutProcess_B_R1Pneumatic_Ch2Off, 1, 1);
            this.tableLayoutPanel36.Controls.Add(this.btn_IRCutProcess_B_R2Pneumatic_Ch2On, 2, 1);
            this.tableLayoutPanel36.Controls.Add(this.btn_IRCutProcess_B_R2Pneumatic_Ch2Off, 3, 1);
            this.tableLayoutPanel36.Controls.Add(this.btn_IRCutProcess_B_R1Pneumatic_Ch2On, 0, 1);
            this.tableLayoutPanel36.Controls.Add(this.btn_IRCutProcess_B_R1Pneumatic_Ch1On, 0, 0);
            this.tableLayoutPanel36.Controls.Add(this.btn_IRCutProcess_A_R1Destruction_Ch1On, 0, 2);
            this.tableLayoutPanel36.Controls.Add(this.btn_IRCutProcess_A_R1Destruction_Ch1Off, 1, 2);
            this.tableLayoutPanel36.Controls.Add(this.btn_IRCutProcess_A_R2Destruction_Ch1On, 2, 2);
            this.tableLayoutPanel36.Controls.Add(this.btn_IRCutProcess_A_R2Destruction_Ch1Off, 3, 2);
            this.tableLayoutPanel36.Controls.Add(this.btn_IRCutProcess_A_R1Destruction_Ch2On, 0, 3);
            this.tableLayoutPanel36.Controls.Add(this.btn_IRCutProcess_A_R1Destruction_Ch2Off, 1, 3);
            this.tableLayoutPanel36.Controls.Add(this.btn_IRCutProcess_A_R2Destruction_Ch2Off, 3, 3);
            this.tableLayoutPanel36.Controls.Add(this.btn_IRCutProcess_A_R2Destruction_Ch2On, 2, 3);
            this.tableLayoutPanel36.Location = new System.Drawing.Point(6, 21);
            this.tableLayoutPanel36.Name = "tableLayoutPanel36";
            this.tableLayoutPanel36.RowCount = 4;
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel36.Size = new System.Drawing.Size(437, 165);
            this.tableLayoutPanel36.TabIndex = 55;
            // 
            // btn_IRCutProcess_B_R1Pneumatic_Ch1Off
            // 
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1Off.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1Off.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1Off.Location = new System.Drawing.Point(112, 3);
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1Off.Name = "btn_IRCutProcess_B_R1Pneumatic_Ch1Off";
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1Off.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1Off.TabIndex = 60;
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1Off.Text = "우측1 공압\r\n채널1 오프";
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1Off.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_B_R2Pneumatic_Ch1On
            // 
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1On.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1On.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1On.Location = new System.Drawing.Point(221, 3);
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1On.Name = "btn_IRCutProcess_B_R2Pneumatic_Ch1On";
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1On.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1On.TabIndex = 63;
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1On.Text = "우측2 공압\r\n채널1 온";
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1On.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_B_R2Pneumatic_Ch1Off
            // 
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1Off.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1Off.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1Off.Location = new System.Drawing.Point(330, 3);
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1Off.Name = "btn_IRCutProcess_B_R2Pneumatic_Ch1Off";
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1Off.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1Off.TabIndex = 62;
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1Off.Text = "우측2 공압\r\n채널1 오프";
            this.btn_IRCutProcess_B_R2Pneumatic_Ch1Off.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_B_R1Pneumatic_Ch2Off
            // 
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2Off.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2Off.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2Off.Location = new System.Drawing.Point(112, 44);
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2Off.Name = "btn_IRCutProcess_B_R1Pneumatic_Ch2Off";
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2Off.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2Off.TabIndex = 65;
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2Off.Text = "우측1 공압\r\n채널2 오프";
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2Off.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_B_R2Pneumatic_Ch2On
            // 
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2On.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2On.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2On.Location = new System.Drawing.Point(221, 44);
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2On.Name = "btn_IRCutProcess_B_R2Pneumatic_Ch2On";
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2On.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2On.TabIndex = 64;
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2On.Text = "우측2 공압\r\n채널2 온";
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2On.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_B_R2Pneumatic_Ch2Off
            // 
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2Off.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2Off.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2Off.Location = new System.Drawing.Point(330, 44);
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2Off.Name = "btn_IRCutProcess_B_R2Pneumatic_Ch2Off";
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2Off.Size = new System.Drawing.Size(104, 35);
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2Off.TabIndex = 68;
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2Off.Text = "우측2 공압\r\n채널2 오프";
            this.btn_IRCutProcess_B_R2Pneumatic_Ch2Off.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_B_R1Pneumatic_Ch2On
            // 
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2On.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2On.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2On.Location = new System.Drawing.Point(3, 44);
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2On.Name = "btn_IRCutProcess_B_R1Pneumatic_Ch2On";
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2On.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2On.TabIndex = 61;
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2On.Text = "우측1 공압\r\n채널2 온";
            this.btn_IRCutProcess_B_R1Pneumatic_Ch2On.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_B_R1Pneumatic_Ch1On
            // 
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1On.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1On.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1On.Location = new System.Drawing.Point(3, 3);
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1On.Name = "btn_IRCutProcess_B_R1Pneumatic_Ch1On";
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1On.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1On.TabIndex = 59;
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1On.Text = "우측1 공압\r\n채널1 온";
            this.btn_IRCutProcess_B_R1Pneumatic_Ch1On.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_R1Destruction_Ch1On
            // 
            this.btn_IRCutProcess_A_R1Destruction_Ch1On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_R1Destruction_Ch1On.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_R1Destruction_Ch1On.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_R1Destruction_Ch1On.Location = new System.Drawing.Point(3, 85);
            this.btn_IRCutProcess_A_R1Destruction_Ch1On.Name = "btn_IRCutProcess_A_R1Destruction_Ch1On";
            this.btn_IRCutProcess_A_R1Destruction_Ch1On.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_R1Destruction_Ch1On.TabIndex = 69;
            this.btn_IRCutProcess_A_R1Destruction_Ch1On.Text = "우측1 파기\r\n채널1 온";
            this.btn_IRCutProcess_A_R1Destruction_Ch1On.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_R1Destruction_Ch1Off
            // 
            this.btn_IRCutProcess_A_R1Destruction_Ch1Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_R1Destruction_Ch1Off.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_R1Destruction_Ch1Off.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_R1Destruction_Ch1Off.Location = new System.Drawing.Point(112, 85);
            this.btn_IRCutProcess_A_R1Destruction_Ch1Off.Name = "btn_IRCutProcess_A_R1Destruction_Ch1Off";
            this.btn_IRCutProcess_A_R1Destruction_Ch1Off.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_R1Destruction_Ch1Off.TabIndex = 70;
            this.btn_IRCutProcess_A_R1Destruction_Ch1Off.Text = "우측1 파기\r\n채널1 오프";
            this.btn_IRCutProcess_A_R1Destruction_Ch1Off.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_R2Destruction_Ch1On
            // 
            this.btn_IRCutProcess_A_R2Destruction_Ch1On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_R2Destruction_Ch1On.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_R2Destruction_Ch1On.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_R2Destruction_Ch1On.Location = new System.Drawing.Point(221, 85);
            this.btn_IRCutProcess_A_R2Destruction_Ch1On.Name = "btn_IRCutProcess_A_R2Destruction_Ch1On";
            this.btn_IRCutProcess_A_R2Destruction_Ch1On.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_R2Destruction_Ch1On.TabIndex = 71;
            this.btn_IRCutProcess_A_R2Destruction_Ch1On.Text = "우측2 파기\r\n채널1 온";
            this.btn_IRCutProcess_A_R2Destruction_Ch1On.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_R2Destruction_Ch1Off
            // 
            this.btn_IRCutProcess_A_R2Destruction_Ch1Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_R2Destruction_Ch1Off.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_R2Destruction_Ch1Off.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_R2Destruction_Ch1Off.Location = new System.Drawing.Point(330, 85);
            this.btn_IRCutProcess_A_R2Destruction_Ch1Off.Name = "btn_IRCutProcess_A_R2Destruction_Ch1Off";
            this.btn_IRCutProcess_A_R2Destruction_Ch1Off.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_R2Destruction_Ch1Off.TabIndex = 72;
            this.btn_IRCutProcess_A_R2Destruction_Ch1Off.Text = "우측2 파기\r\n채널1 오프";
            this.btn_IRCutProcess_A_R2Destruction_Ch1Off.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_R1Destruction_Ch2On
            // 
            this.btn_IRCutProcess_A_R1Destruction_Ch2On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_R1Destruction_Ch2On.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_R1Destruction_Ch2On.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_R1Destruction_Ch2On.Location = new System.Drawing.Point(3, 126);
            this.btn_IRCutProcess_A_R1Destruction_Ch2On.Name = "btn_IRCutProcess_A_R1Destruction_Ch2On";
            this.btn_IRCutProcess_A_R1Destruction_Ch2On.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_R1Destruction_Ch2On.TabIndex = 73;
            this.btn_IRCutProcess_A_R1Destruction_Ch2On.Text = "우측1 파기\r\n채널2 온";
            this.btn_IRCutProcess_A_R1Destruction_Ch2On.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_R1Destruction_Ch2Off
            // 
            this.btn_IRCutProcess_A_R1Destruction_Ch2Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_R1Destruction_Ch2Off.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_R1Destruction_Ch2Off.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_R1Destruction_Ch2Off.Location = new System.Drawing.Point(112, 126);
            this.btn_IRCutProcess_A_R1Destruction_Ch2Off.Name = "btn_IRCutProcess_A_R1Destruction_Ch2Off";
            this.btn_IRCutProcess_A_R1Destruction_Ch2Off.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_R1Destruction_Ch2Off.TabIndex = 74;
            this.btn_IRCutProcess_A_R1Destruction_Ch2Off.Text = "우측1 파기\r\n채널2 오프";
            this.btn_IRCutProcess_A_R1Destruction_Ch2Off.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_R2Destruction_Ch2Off
            // 
            this.btn_IRCutProcess_A_R2Destruction_Ch2Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_R2Destruction_Ch2Off.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_R2Destruction_Ch2Off.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_R2Destruction_Ch2Off.Location = new System.Drawing.Point(330, 126);
            this.btn_IRCutProcess_A_R2Destruction_Ch2Off.Name = "btn_IRCutProcess_A_R2Destruction_Ch2Off";
            this.btn_IRCutProcess_A_R2Destruction_Ch2Off.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_R2Destruction_Ch2Off.TabIndex = 76;
            this.btn_IRCutProcess_A_R2Destruction_Ch2Off.Text = "우측2 파기\r\n채널2 오프";
            this.btn_IRCutProcess_A_R2Destruction_Ch2Off.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_R2Destruction_Ch2On
            // 
            this.btn_IRCutProcess_A_R2Destruction_Ch2On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_R2Destruction_Ch2On.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_R2Destruction_Ch2On.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_R2Destruction_Ch2On.Location = new System.Drawing.Point(221, 126);
            this.btn_IRCutProcess_A_R2Destruction_Ch2On.Name = "btn_IRCutProcess_A_R2Destruction_Ch2On";
            this.btn_IRCutProcess_A_R2Destruction_Ch2On.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_R2Destruction_Ch2On.TabIndex = 75;
            this.btn_IRCutProcess_A_R2Destruction_Ch2On.Text = "우측2 파기\r\n채널2 온";
            this.btn_IRCutProcess_A_R2Destruction_Ch2On.UseVisualStyleBackColor = false;
            // 
            // gbox_IRCutProcess_A
            // 
            this.gbox_IRCutProcess_A.Controls.Add(this.tableLayoutPanel35);
            this.gbox_IRCutProcess_A.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_IRCutProcess_A.ForeColor = System.Drawing.Color.White;
            this.gbox_IRCutProcess_A.Location = new System.Drawing.Point(792, 397);
            this.gbox_IRCutProcess_A.Name = "gbox_IRCutProcess_A";
            this.gbox_IRCutProcess_A.Size = new System.Drawing.Size(449, 192);
            this.gbox_IRCutProcess_A.TabIndex = 51;
            this.gbox_IRCutProcess_A.TabStop = false;
            this.gbox_IRCutProcess_A.Text = "     A    ";
            // 
            // tableLayoutPanel35
            // 
            this.tableLayoutPanel35.ColumnCount = 4;
            this.tableLayoutPanel35.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel35.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel35.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel35.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel35.Controls.Add(this.btn_IRCutProcess_A_L1Destruction_Ch2Off, 0, 3);
            this.tableLayoutPanel35.Controls.Add(this.btn_IRCutProcess_A_L1Destruction_Ch2On, 0, 3);
            this.tableLayoutPanel35.Controls.Add(this.btn_IRCutProcess_A_L2Destruction_Ch2Off, 0, 3);
            this.tableLayoutPanel35.Controls.Add(this.btn_IRCutProcess_A_L2Destruction_Ch2On, 0, 3);
            this.tableLayoutPanel35.Controls.Add(this.btn_IRCutProcess_A_L1Pneumatic_Ch1Off, 1, 0);
            this.tableLayoutPanel35.Controls.Add(this.btn_IRCutProcess_A_L2Pneumatic_Ch1On, 2, 0);
            this.tableLayoutPanel35.Controls.Add(this.btn_IRCutProcess_A_L2Pneumatic_Ch1Off, 3, 0);
            this.tableLayoutPanel35.Controls.Add(this.btn_IRCutProcess_A_L1Pneumatic_Ch2Off, 1, 1);
            this.tableLayoutPanel35.Controls.Add(this.btn_IRCutProcess_A_L2Pneumatic_Ch2On, 2, 1);
            this.tableLayoutPanel35.Controls.Add(this.btn_IRCutProcess_A_L2Pneumatic_Ch2Off, 3, 1);
            this.tableLayoutPanel35.Controls.Add(this.btn_IRCutProcess_A_L1Pneumatic_Ch2On, 0, 1);
            this.tableLayoutPanel35.Controls.Add(this.btn_IRCutProcess_A_L1Pneumatic_Ch1On, 0, 0);
            this.tableLayoutPanel35.Controls.Add(this.btn_IRCutProcess_A_L1Destruction_Ch1On, 0, 2);
            this.tableLayoutPanel35.Controls.Add(this.btn_IRCutProcess_A_L1Destruction_Ch1Off, 1, 2);
            this.tableLayoutPanel35.Controls.Add(this.btn_IRCutProcess_A_L2Destruction_Ch1On, 2, 2);
            this.tableLayoutPanel35.Controls.Add(this.btn_IRCutProcess_A_L2Destruction_Ch1Off, 3, 2);
            this.tableLayoutPanel35.Location = new System.Drawing.Point(6, 21);
            this.tableLayoutPanel35.Name = "tableLayoutPanel35";
            this.tableLayoutPanel35.RowCount = 4;
            this.tableLayoutPanel35.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel35.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel35.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel35.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel35.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel35.Size = new System.Drawing.Size(437, 165);
            this.tableLayoutPanel35.TabIndex = 54;
            // 
            // btn_IRCutProcess_A_L1Destruction_Ch2Off
            // 
            this.btn_IRCutProcess_A_L1Destruction_Ch2Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_L1Destruction_Ch2Off.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_L1Destruction_Ch2Off.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_L1Destruction_Ch2Off.Location = new System.Drawing.Point(112, 126);
            this.btn_IRCutProcess_A_L1Destruction_Ch2Off.Name = "btn_IRCutProcess_A_L1Destruction_Ch2Off";
            this.btn_IRCutProcess_A_L1Destruction_Ch2Off.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_L1Destruction_Ch2Off.TabIndex = 76;
            this.btn_IRCutProcess_A_L1Destruction_Ch2Off.Text = "좌측1 파기\r\n채널2 오프";
            this.btn_IRCutProcess_A_L1Destruction_Ch2Off.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_L1Destruction_Ch2On
            // 
            this.btn_IRCutProcess_A_L1Destruction_Ch2On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_L1Destruction_Ch2On.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_L1Destruction_Ch2On.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_L1Destruction_Ch2On.Location = new System.Drawing.Point(3, 126);
            this.btn_IRCutProcess_A_L1Destruction_Ch2On.Name = "btn_IRCutProcess_A_L1Destruction_Ch2On";
            this.btn_IRCutProcess_A_L1Destruction_Ch2On.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_L1Destruction_Ch2On.TabIndex = 75;
            this.btn_IRCutProcess_A_L1Destruction_Ch2On.Text = "좌측1 파기\r\n채널2 온";
            this.btn_IRCutProcess_A_L1Destruction_Ch2On.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_L2Destruction_Ch2Off
            // 
            this.btn_IRCutProcess_A_L2Destruction_Ch2Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_L2Destruction_Ch2Off.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_L2Destruction_Ch2Off.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_L2Destruction_Ch2Off.Location = new System.Drawing.Point(330, 126);
            this.btn_IRCutProcess_A_L2Destruction_Ch2Off.Name = "btn_IRCutProcess_A_L2Destruction_Ch2Off";
            this.btn_IRCutProcess_A_L2Destruction_Ch2Off.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_L2Destruction_Ch2Off.TabIndex = 74;
            this.btn_IRCutProcess_A_L2Destruction_Ch2Off.Text = "좌측2 파기\r\n채널2 오프";
            this.btn_IRCutProcess_A_L2Destruction_Ch2Off.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_L2Destruction_Ch2On
            // 
            this.btn_IRCutProcess_A_L2Destruction_Ch2On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_L2Destruction_Ch2On.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_L2Destruction_Ch2On.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_L2Destruction_Ch2On.Location = new System.Drawing.Point(221, 126);
            this.btn_IRCutProcess_A_L2Destruction_Ch2On.Name = "btn_IRCutProcess_A_L2Destruction_Ch2On";
            this.btn_IRCutProcess_A_L2Destruction_Ch2On.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_L2Destruction_Ch2On.TabIndex = 73;
            this.btn_IRCutProcess_A_L2Destruction_Ch2On.Text = "좌측2 파기\r\n채널2 온";
            this.btn_IRCutProcess_A_L2Destruction_Ch2On.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_L1Pneumatic_Ch1Off
            // 
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1Off.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1Off.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1Off.Location = new System.Drawing.Point(112, 3);
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1Off.Name = "btn_IRCutProcess_A_L1Pneumatic_Ch1Off";
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1Off.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1Off.TabIndex = 60;
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1Off.Text = "좌측1 공압\r\n채널1 오프";
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1Off.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_L2Pneumatic_Ch1On
            // 
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1On.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1On.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1On.Location = new System.Drawing.Point(221, 3);
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1On.Name = "btn_IRCutProcess_A_L2Pneumatic_Ch1On";
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1On.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1On.TabIndex = 63;
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1On.Text = "좌측2 공압\r\n채널1 온";
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1On.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_L2Pneumatic_Ch1Off
            // 
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1Off.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1Off.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1Off.Location = new System.Drawing.Point(330, 3);
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1Off.Name = "btn_IRCutProcess_A_L2Pneumatic_Ch1Off";
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1Off.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1Off.TabIndex = 62;
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1Off.Text = "좌측2 공압\r\n채널1 오프";
            this.btn_IRCutProcess_A_L2Pneumatic_Ch1Off.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_L1Pneumatic_Ch2Off
            // 
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2Off.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2Off.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2Off.Location = new System.Drawing.Point(112, 44);
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2Off.Name = "btn_IRCutProcess_A_L1Pneumatic_Ch2Off";
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2Off.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2Off.TabIndex = 65;
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2Off.Text = "좌측1 공압\r\n채널2 오프";
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2Off.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_L2Pneumatic_Ch2On
            // 
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2On.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2On.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2On.Location = new System.Drawing.Point(221, 44);
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2On.Name = "btn_IRCutProcess_A_L2Pneumatic_Ch2On";
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2On.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2On.TabIndex = 64;
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2On.Text = "좌측2 공압\r\n채널2 온";
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2On.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_L2Pneumatic_Ch2Off
            // 
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2Off.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2Off.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2Off.Location = new System.Drawing.Point(330, 44);
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2Off.Name = "btn_IRCutProcess_A_L2Pneumatic_Ch2Off";
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2Off.Size = new System.Drawing.Size(104, 35);
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2Off.TabIndex = 68;
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2Off.Text = "좌측2 공압\r\n채널2 오프";
            this.btn_IRCutProcess_A_L2Pneumatic_Ch2Off.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_L1Pneumatic_Ch2On
            // 
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2On.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2On.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2On.Location = new System.Drawing.Point(3, 44);
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2On.Name = "btn_IRCutProcess_A_L1Pneumatic_Ch2On";
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2On.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2On.TabIndex = 61;
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2On.Text = "좌측1 공압\r\n채널2 온";
            this.btn_IRCutProcess_A_L1Pneumatic_Ch2On.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_L1Pneumatic_Ch1On
            // 
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1On.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1On.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1On.Location = new System.Drawing.Point(3, 3);
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1On.Name = "btn_IRCutProcess_A_L1Pneumatic_Ch1On";
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1On.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1On.TabIndex = 59;
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1On.Text = "좌측1 공압\r\n채널1 온";
            this.btn_IRCutProcess_A_L1Pneumatic_Ch1On.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_L1Destruction_Ch1On
            // 
            this.btn_IRCutProcess_A_L1Destruction_Ch1On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_L1Destruction_Ch1On.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_L1Destruction_Ch1On.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_L1Destruction_Ch1On.Location = new System.Drawing.Point(3, 85);
            this.btn_IRCutProcess_A_L1Destruction_Ch1On.Name = "btn_IRCutProcess_A_L1Destruction_Ch1On";
            this.btn_IRCutProcess_A_L1Destruction_Ch1On.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_L1Destruction_Ch1On.TabIndex = 69;
            this.btn_IRCutProcess_A_L1Destruction_Ch1On.Text = "좌측1 파기\r\n채널1 온";
            this.btn_IRCutProcess_A_L1Destruction_Ch1On.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_L1Destruction_Ch1Off
            // 
            this.btn_IRCutProcess_A_L1Destruction_Ch1Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_L1Destruction_Ch1Off.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_L1Destruction_Ch1Off.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_L1Destruction_Ch1Off.Location = new System.Drawing.Point(112, 85);
            this.btn_IRCutProcess_A_L1Destruction_Ch1Off.Name = "btn_IRCutProcess_A_L1Destruction_Ch1Off";
            this.btn_IRCutProcess_A_L1Destruction_Ch1Off.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_L1Destruction_Ch1Off.TabIndex = 70;
            this.btn_IRCutProcess_A_L1Destruction_Ch1Off.Text = "좌측1 파기\r\n채널1 오프";
            this.btn_IRCutProcess_A_L1Destruction_Ch1Off.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_L2Destruction_Ch1On
            // 
            this.btn_IRCutProcess_A_L2Destruction_Ch1On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_L2Destruction_Ch1On.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_L2Destruction_Ch1On.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_L2Destruction_Ch1On.Location = new System.Drawing.Point(221, 85);
            this.btn_IRCutProcess_A_L2Destruction_Ch1On.Name = "btn_IRCutProcess_A_L2Destruction_Ch1On";
            this.btn_IRCutProcess_A_L2Destruction_Ch1On.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_L2Destruction_Ch1On.TabIndex = 71;
            this.btn_IRCutProcess_A_L2Destruction_Ch1On.Text = "좌측2 파기\r\n채널1 온";
            this.btn_IRCutProcess_A_L2Destruction_Ch1On.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_A_L2Destruction_Ch1Off
            // 
            this.btn_IRCutProcess_A_L2Destruction_Ch1Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_A_L2Destruction_Ch1Off.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_IRCutProcess_A_L2Destruction_Ch1Off.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_A_L2Destruction_Ch1Off.Location = new System.Drawing.Point(330, 85);
            this.btn_IRCutProcess_A_L2Destruction_Ch1Off.Name = "btn_IRCutProcess_A_L2Destruction_Ch1Off";
            this.btn_IRCutProcess_A_L2Destruction_Ch1Off.Size = new System.Drawing.Size(103, 35);
            this.btn_IRCutProcess_A_L2Destruction_Ch1Off.TabIndex = 72;
            this.btn_IRCutProcess_A_L2Destruction_Ch1Off.Text = "좌측2 파기\r\n채널1 오프";
            this.btn_IRCutProcess_A_L2Destruction_Ch1Off.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_Save
            // 
            this.btn_IRCutProcess_Save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_Save.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_IRCutProcess_Save.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_Save.Location = new System.Drawing.Point(1482, 783);
            this.btn_IRCutProcess_Save.Name = "btn_IRCutProcess_Save";
            this.btn_IRCutProcess_Save.Size = new System.Drawing.Size(228, 28);
            this.btn_IRCutProcess_Save.TabIndex = 50;
            this.btn_IRCutProcess_Save.Text = "저장";
            this.btn_IRCutProcess_Save.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_SpeedSetting
            // 
            this.btn_IRCutProcess_SpeedSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_SpeedSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_IRCutProcess_SpeedSetting.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_SpeedSetting.Location = new System.Drawing.Point(1291, 315);
            this.btn_IRCutProcess_SpeedSetting.Name = "btn_IRCutProcess_SpeedSetting";
            this.btn_IRCutProcess_SpeedSetting.Size = new System.Drawing.Size(90, 27);
            this.btn_IRCutProcess_SpeedSetting.TabIndex = 48;
            this.btn_IRCutProcess_SpeedSetting.Text = "속도 설정";
            this.btn_IRCutProcess_SpeedSetting.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_AllSetting
            // 
            this.btn_IRCutProcess_AllSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_AllSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_IRCutProcess_AllSetting.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_AllSetting.Location = new System.Drawing.Point(1387, 273);
            this.btn_IRCutProcess_AllSetting.Name = "btn_IRCutProcess_AllSetting";
            this.btn_IRCutProcess_AllSetting.Size = new System.Drawing.Size(90, 27);
            this.btn_IRCutProcess_AllSetting.TabIndex = 47;
            this.btn_IRCutProcess_AllSetting.Text = "전체 설정";
            this.btn_IRCutProcess_AllSetting.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_LocationSetting
            // 
            this.btn_IRCutProcess_LocationSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_LocationSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_IRCutProcess_LocationSetting.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_LocationSetting.Location = new System.Drawing.Point(1071, 315);
            this.btn_IRCutProcess_LocationSetting.Name = "btn_IRCutProcess_LocationSetting";
            this.btn_IRCutProcess_LocationSetting.Size = new System.Drawing.Size(90, 27);
            this.btn_IRCutProcess_LocationSetting.TabIndex = 46;
            this.btn_IRCutProcess_LocationSetting.Text = "위치 설정";
            this.btn_IRCutProcess_LocationSetting.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_MoveLocation
            // 
            this.btn_IRCutProcess_MoveLocation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_MoveLocation.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_IRCutProcess_MoveLocation.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_MoveLocation.Location = new System.Drawing.Point(975, 315);
            this.btn_IRCutProcess_MoveLocation.Name = "btn_IRCutProcess_MoveLocation";
            this.btn_IRCutProcess_MoveLocation.Size = new System.Drawing.Size(90, 27);
            this.btn_IRCutProcess_MoveLocation.TabIndex = 45;
            this.btn_IRCutProcess_MoveLocation.Text = "위치 이동";
            this.btn_IRCutProcess_MoveLocation.UseVisualStyleBackColor = false;
            // 
            // tbox_IRCutProcess_Location
            // 
            this.tbox_IRCutProcess_Location.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_IRCutProcess_Location.Location = new System.Drawing.Point(879, 316);
            this.tbox_IRCutProcess_Location.Name = "tbox_IRCutProcess_Location";
            this.tbox_IRCutProcess_Location.Size = new System.Drawing.Size(90, 27);
            this.tbox_IRCutProcess_Location.TabIndex = 44;
            // 
            // tbox_IRCutProcess_Speed
            // 
            this.tbox_IRCutProcess_Speed.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_IRCutProcess_Speed.Location = new System.Drawing.Point(1291, 273);
            this.tbox_IRCutProcess_Speed.Name = "tbox_IRCutProcess_Speed";
            this.tbox_IRCutProcess_Speed.Size = new System.Drawing.Size(90, 27);
            this.tbox_IRCutProcess_Speed.TabIndex = 43;
            // 
            // tbox_IRCutProcess_CurrentLocation
            // 
            this.tbox_IRCutProcess_CurrentLocation.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_IRCutProcess_CurrentLocation.Location = new System.Drawing.Point(1084, 274);
            this.tbox_IRCutProcess_CurrentLocation.Name = "tbox_IRCutProcess_CurrentLocation";
            this.tbox_IRCutProcess_CurrentLocation.Size = new System.Drawing.Size(90, 27);
            this.tbox_IRCutProcess_CurrentLocation.TabIndex = 42;
            // 
            // tbox_IRCutProcess_SelectedShaft
            // 
            this.tbox_IRCutProcess_SelectedShaft.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_IRCutProcess_SelectedShaft.Location = new System.Drawing.Point(879, 273);
            this.tbox_IRCutProcess_SelectedShaft.Name = "tbox_IRCutProcess_SelectedShaft";
            this.tbox_IRCutProcess_SelectedShaft.Size = new System.Drawing.Size(90, 27);
            this.tbox_IRCutProcess_SelectedShaft.TabIndex = 41;
            // 
            // lbl_IRCutProcess_Location
            // 
            this.lbl_IRCutProcess_Location.AutoSize = true;
            this.lbl_IRCutProcess_Location.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_IRCutProcess_Location.ForeColor = System.Drawing.Color.White;
            this.lbl_IRCutProcess_Location.Location = new System.Drawing.Point(801, 316);
            this.lbl_IRCutProcess_Location.Name = "lbl_IRCutProcess_Location";
            this.lbl_IRCutProcess_Location.Size = new System.Drawing.Size(84, 21);
            this.lbl_IRCutProcess_Location.TabIndex = 40;
            this.lbl_IRCutProcess_Location.Text = "위치[mm]";
            // 
            // lbl_IRCutProcess_Speed
            // 
            this.lbl_IRCutProcess_Speed.AutoSize = true;
            this.lbl_IRCutProcess_Speed.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_IRCutProcess_Speed.ForeColor = System.Drawing.Color.White;
            this.lbl_IRCutProcess_Speed.Location = new System.Drawing.Point(1179, 275);
            this.lbl_IRCutProcess_Speed.Name = "lbl_IRCutProcess_Speed";
            this.lbl_IRCutProcess_Speed.Size = new System.Drawing.Size(115, 21);
            this.lbl_IRCutProcess_Speed.TabIndex = 39;
            this.lbl_IRCutProcess_Speed.Text = "속도[mm/sec]";
            // 
            // lbl_IRCutProcess_SelectedShaft
            // 
            this.lbl_IRCutProcess_SelectedShaft.AutoSize = true;
            this.lbl_IRCutProcess_SelectedShaft.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_IRCutProcess_SelectedShaft.ForeColor = System.Drawing.Color.White;
            this.lbl_IRCutProcess_SelectedShaft.Location = new System.Drawing.Point(805, 274);
            this.lbl_IRCutProcess_SelectedShaft.Name = "lbl_IRCutProcess_SelectedShaft";
            this.lbl_IRCutProcess_SelectedShaft.Size = new System.Drawing.Size(80, 21);
            this.lbl_IRCutProcess_SelectedShaft.TabIndex = 38;
            this.lbl_IRCutProcess_SelectedShaft.Text = "선택된 축";
            // 
            // tbox_IRCutProcess_LocationSetting
            // 
            this.tbox_IRCutProcess_LocationSetting.BackColor = System.Drawing.SystemColors.Highlight;
            this.tbox_IRCutProcess_LocationSetting.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.tbox_IRCutProcess_LocationSetting.ForeColor = System.Drawing.Color.White;
            this.tbox_IRCutProcess_LocationSetting.Location = new System.Drawing.Point(792, 223);
            this.tbox_IRCutProcess_LocationSetting.Name = "tbox_IRCutProcess_LocationSetting";
            this.tbox_IRCutProcess_LocationSetting.ReadOnly = true;
            this.tbox_IRCutProcess_LocationSetting.Size = new System.Drawing.Size(89, 27);
            this.tbox_IRCutProcess_LocationSetting.TabIndex = 37;
            this.tbox_IRCutProcess_LocationSetting.Text = "위치값 설정";
            this.tbox_IRCutProcess_LocationSetting.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbl_IRCutProcess_CurrentLocation
            // 
            this.lbl_IRCutProcess_CurrentLocation.AutoSize = true;
            this.lbl_IRCutProcess_CurrentLocation.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_IRCutProcess_CurrentLocation.ForeColor = System.Drawing.Color.White;
            this.lbl_IRCutProcess_CurrentLocation.Location = new System.Drawing.Point(975, 274);
            this.lbl_IRCutProcess_CurrentLocation.Name = "lbl_IRCutProcess_CurrentLocation";
            this.lbl_IRCutProcess_CurrentLocation.Size = new System.Drawing.Size(116, 21);
            this.lbl_IRCutProcess_CurrentLocation.TabIndex = 36;
            this.lbl_IRCutProcess_CurrentLocation.Text = "현재위치[mm]";
            // 
            // btn_IRCutProcess_GrabSwitch3
            // 
            this.btn_IRCutProcess_GrabSwitch3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_GrabSwitch3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_IRCutProcess_GrabSwitch3.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_GrabSwitch3.Location = new System.Drawing.Point(1514, 6);
            this.btn_IRCutProcess_GrabSwitch3.Name = "btn_IRCutProcess_GrabSwitch3";
            this.btn_IRCutProcess_GrabSwitch3.Size = new System.Drawing.Size(172, 23);
            this.btn_IRCutProcess_GrabSwitch3.TabIndex = 35;
            this.btn_IRCutProcess_GrabSwitch3.Text = "Grab Switch 3";
            this.btn_IRCutProcess_GrabSwitch3.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_GrabSwitch2
            // 
            this.btn_IRCutProcess_GrabSwitch2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_GrabSwitch2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_IRCutProcess_GrabSwitch2.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_GrabSwitch2.Location = new System.Drawing.Point(1336, 6);
            this.btn_IRCutProcess_GrabSwitch2.Name = "btn_IRCutProcess_GrabSwitch2";
            this.btn_IRCutProcess_GrabSwitch2.Size = new System.Drawing.Size(172, 23);
            this.btn_IRCutProcess_GrabSwitch2.TabIndex = 34;
            this.btn_IRCutProcess_GrabSwitch2.Text = "Grab Switch 2";
            this.btn_IRCutProcess_GrabSwitch2.UseVisualStyleBackColor = false;
            // 
            // btn_IRCutProcess_GrabSwitch1
            // 
            this.btn_IRCutProcess_GrabSwitch1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_IRCutProcess_GrabSwitch1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_IRCutProcess_GrabSwitch1.ForeColor = System.Drawing.Color.White;
            this.btn_IRCutProcess_GrabSwitch1.Location = new System.Drawing.Point(1158, 6);
            this.btn_IRCutProcess_GrabSwitch1.Name = "btn_IRCutProcess_GrabSwitch1";
            this.btn_IRCutProcess_GrabSwitch1.Size = new System.Drawing.Size(172, 23);
            this.btn_IRCutProcess_GrabSwitch1.TabIndex = 33;
            this.btn_IRCutProcess_GrabSwitch1.Text = "Grab Switch 1";
            this.btn_IRCutProcess_GrabSwitch1.UseVisualStyleBackColor = false;
            // 
            // lv_IRCutProcess
            // 
            this.lv_IRCutProcess.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.col_IRCutProcess_NamebyLocation,
            this.col_IRCutProcess_Shaft,
            this.col_IRCutProcess_Location,
            this.col_IRCutProcess_Speed});
            this.lv_IRCutProcess.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lv_IRCutProcess.GridLines = true;
            this.lv_IRCutProcess.Location = new System.Drawing.Point(17, 16);
            this.lv_IRCutProcess.Name = "lv_IRCutProcess";
            this.lv_IRCutProcess.Size = new System.Drawing.Size(758, 734);
            this.lv_IRCutProcess.TabIndex = 32;
            this.lv_IRCutProcess.UseCompatibleStateImageBehavior = false;
            this.lv_IRCutProcess.View = System.Windows.Forms.View.Details;
            // 
            // col_IRCutProcess_NamebyLocation
            // 
            this.col_IRCutProcess_NamebyLocation.Text = "위치별 명칭";
            this.col_IRCutProcess_NamebyLocation.Width = 430;
            // 
            // col_IRCutProcess_Shaft
            // 
            this.col_IRCutProcess_Shaft.Text = "축";
            this.col_IRCutProcess_Shaft.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // col_IRCutProcess_Location
            // 
            this.col_IRCutProcess_Location.Text = "위치[mm]";
            this.col_IRCutProcess_Location.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_IRCutProcess_Location.Width = 125;
            // 
            // col_IRCutProcess_Speed
            // 
            this.col_IRCutProcess_Speed.Text = "속도[mm/s]";
            this.col_IRCutProcess_Speed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_IRCutProcess_Speed.Width = 125;
            // 
            // tp_BreakTransfer
            // 
            this.tp_BreakTransfer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tp_BreakTransfer.Controls.Add(this.gbox_BreakTransfer_Move);
            this.tp_BreakTransfer.Controls.Add(this.gbox_BreakTransfer_B);
            this.tp_BreakTransfer.Controls.Add(this.gbox_BreakTransfer_A);
            this.tp_BreakTransfer.Controls.Add(this.btn_BreakTransfer_Save);
            this.tp_BreakTransfer.Controls.Add(this.btn_BreakTransfer_SpeedSetting);
            this.tp_BreakTransfer.Controls.Add(this.btn_BreakTransfer_AllSetting);
            this.tp_BreakTransfer.Controls.Add(this.btn_BreakTransfer_LocationSetting);
            this.tp_BreakTransfer.Controls.Add(this.btn_BreakTransfer_MoveLocation);
            this.tp_BreakTransfer.Controls.Add(this.tbox_BreakTransfer_Location);
            this.tp_BreakTransfer.Controls.Add(this.tbox_BreakTransfer_Speed);
            this.tp_BreakTransfer.Controls.Add(this.tbox_BreakTransfer_CurrentLocation);
            this.tp_BreakTransfer.Controls.Add(this.tbox_BreakTransfer_SelectedShaft);
            this.tp_BreakTransfer.Controls.Add(this.lbl_BreakTransfer_Location);
            this.tp_BreakTransfer.Controls.Add(this.lbl_BreakTransfer_Speed);
            this.tp_BreakTransfer.Controls.Add(this.lbl_BreakTransfer_SelectedShaft);
            this.tp_BreakTransfer.Controls.Add(this.tbox_BreakTransfer_LocationSetting);
            this.tp_BreakTransfer.Controls.Add(this.lbl_BreakTransfer_CurrentLocation);
            this.tp_BreakTransfer.Controls.Add(this.btn_BreakTransfer_GrabSwitch3);
            this.tp_BreakTransfer.Controls.Add(this.btn_BreakTransfer_GrabSwitch2);
            this.tp_BreakTransfer.Controls.Add(this.btn_BreakTransfer_GrabSwitch1);
            this.tp_BreakTransfer.Controls.Add(this.lv_BreakTransfer);
            this.tp_BreakTransfer.Controls.Add(this.ajin_Setting_BreakTransfer);
            this.tp_BreakTransfer.Location = new System.Drawing.Point(4, 34);
            this.tp_BreakTransfer.Name = "tp_BreakTransfer";
            this.tp_BreakTransfer.Size = new System.Drawing.Size(1726, 816);
            this.tp_BreakTransfer.TabIndex = 4;
            this.tp_BreakTransfer.Text = "Break 트랜스퍼";
            // 
            // gbox_BreakTransfer_Move
            // 
            this.gbox_BreakTransfer_Move.Controls.Add(this.gbox_BreakTransfer_MoveB);
            this.gbox_BreakTransfer_Move.Controls.Add(this.gbox_BreakTransfer_MoveA);
            this.gbox_BreakTransfer_Move.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakTransfer_Move.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakTransfer_Move.Location = new System.Drawing.Point(792, 595);
            this.gbox_BreakTransfer_Move.Name = "gbox_BreakTransfer_Move";
            this.gbox_BreakTransfer_Move.Size = new System.Drawing.Size(904, 179);
            this.gbox_BreakTransfer_Move.TabIndex = 53;
            this.gbox_BreakTransfer_Move.TabStop = false;
            this.gbox_BreakTransfer_Move.Text = "     이동     ";
            // 
            // gbox_BreakTransfer_MoveB
            // 
            this.gbox_BreakTransfer_MoveB.Controls.Add(this.tableLayoutPanel44);
            this.gbox_BreakTransfer_MoveB.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakTransfer_MoveB.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakTransfer_MoveB.Location = new System.Drawing.Point(455, 28);
            this.gbox_BreakTransfer_MoveB.Name = "gbox_BreakTransfer_MoveB";
            this.gbox_BreakTransfer_MoveB.Size = new System.Drawing.Size(443, 145);
            this.gbox_BreakTransfer_MoveB.TabIndex = 31;
            this.gbox_BreakTransfer_MoveB.TabStop = false;
            this.gbox_BreakTransfer_MoveB.Text = "     B    ";
            // 
            // tableLayoutPanel44
            // 
            this.tableLayoutPanel44.ColumnCount = 2;
            this.tableLayoutPanel44.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel44.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel44.Controls.Add(this.btn_BreakTransfer_MoveB_Unload, 0, 0);
            this.tableLayoutPanel44.Controls.Add(this.btn_BreakTransfer_MoveB_Load, 0, 0);
            this.tableLayoutPanel44.Location = new System.Drawing.Point(6, 27);
            this.tableLayoutPanel44.Name = "tableLayoutPanel44";
            this.tableLayoutPanel44.RowCount = 1;
            this.tableLayoutPanel44.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel44.Size = new System.Drawing.Size(430, 112);
            this.tableLayoutPanel44.TabIndex = 55;
            // 
            // btn_BreakTransfer_MoveB_Unload
            // 
            this.btn_BreakTransfer_MoveB_Unload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_MoveB_Unload.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btn_BreakTransfer_MoveB_Unload.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_MoveB_Unload.Location = new System.Drawing.Point(218, 3);
            this.btn_BreakTransfer_MoveB_Unload.Name = "btn_BreakTransfer_MoveB_Unload";
            this.btn_BreakTransfer_MoveB_Unload.Size = new System.Drawing.Size(209, 106);
            this.btn_BreakTransfer_MoveB_Unload.TabIndex = 67;
            this.btn_BreakTransfer_MoveB_Unload.Text = "언로드";
            this.btn_BreakTransfer_MoveB_Unload.UseVisualStyleBackColor = false;
            // 
            // btn_BreakTransfer_MoveB_Load
            // 
            this.btn_BreakTransfer_MoveB_Load.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_MoveB_Load.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btn_BreakTransfer_MoveB_Load.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_MoveB_Load.Location = new System.Drawing.Point(3, 3);
            this.btn_BreakTransfer_MoveB_Load.Name = "btn_BreakTransfer_MoveB_Load";
            this.btn_BreakTransfer_MoveB_Load.Size = new System.Drawing.Size(209, 106);
            this.btn_BreakTransfer_MoveB_Load.TabIndex = 66;
            this.btn_BreakTransfer_MoveB_Load.Text = "로드";
            this.btn_BreakTransfer_MoveB_Load.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakTransfer_MoveA
            // 
            this.gbox_BreakTransfer_MoveA.Controls.Add(this.tableLayoutPanel43);
            this.gbox_BreakTransfer_MoveA.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakTransfer_MoveA.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakTransfer_MoveA.Location = new System.Drawing.Point(6, 28);
            this.gbox_BreakTransfer_MoveA.Name = "gbox_BreakTransfer_MoveA";
            this.gbox_BreakTransfer_MoveA.Size = new System.Drawing.Size(443, 145);
            this.gbox_BreakTransfer_MoveA.TabIndex = 30;
            this.gbox_BreakTransfer_MoveA.TabStop = false;
            this.gbox_BreakTransfer_MoveA.Text = "     A    ";
            // 
            // tableLayoutPanel43
            // 
            this.tableLayoutPanel43.ColumnCount = 2;
            this.tableLayoutPanel43.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel43.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel43.Controls.Add(this.btn_BreakTransfer_MoveA_Unload, 0, 0);
            this.tableLayoutPanel43.Controls.Add(this.btn_BreakTransfer_MoveA_Load, 0, 0);
            this.tableLayoutPanel43.Location = new System.Drawing.Point(7, 27);
            this.tableLayoutPanel43.Name = "tableLayoutPanel43";
            this.tableLayoutPanel43.RowCount = 1;
            this.tableLayoutPanel43.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel43.Size = new System.Drawing.Size(430, 112);
            this.tableLayoutPanel43.TabIndex = 54;
            // 
            // btn_BreakTransfer_MoveA_Unload
            // 
            this.btn_BreakTransfer_MoveA_Unload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_MoveA_Unload.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btn_BreakTransfer_MoveA_Unload.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_MoveA_Unload.Location = new System.Drawing.Point(218, 3);
            this.btn_BreakTransfer_MoveA_Unload.Name = "btn_BreakTransfer_MoveA_Unload";
            this.btn_BreakTransfer_MoveA_Unload.Size = new System.Drawing.Size(209, 106);
            this.btn_BreakTransfer_MoveA_Unload.TabIndex = 67;
            this.btn_BreakTransfer_MoveA_Unload.Text = "언로드";
            this.btn_BreakTransfer_MoveA_Unload.UseVisualStyleBackColor = false;
            // 
            // btn_BreakTransfer_MoveA_Load
            // 
            this.btn_BreakTransfer_MoveA_Load.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_MoveA_Load.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btn_BreakTransfer_MoveA_Load.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_MoveA_Load.Location = new System.Drawing.Point(3, 3);
            this.btn_BreakTransfer_MoveA_Load.Name = "btn_BreakTransfer_MoveA_Load";
            this.btn_BreakTransfer_MoveA_Load.Size = new System.Drawing.Size(209, 106);
            this.btn_BreakTransfer_MoveA_Load.TabIndex = 66;
            this.btn_BreakTransfer_MoveA_Load.Text = "로드";
            this.btn_BreakTransfer_MoveA_Load.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakTransfer_B
            // 
            this.gbox_BreakTransfer_B.Controls.Add(this.tableLayoutPanel42);
            this.gbox_BreakTransfer_B.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakTransfer_B.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakTransfer_B.Location = new System.Drawing.Point(1247, 410);
            this.gbox_BreakTransfer_B.Name = "gbox_BreakTransfer_B";
            this.gbox_BreakTransfer_B.Size = new System.Drawing.Size(449, 179);
            this.gbox_BreakTransfer_B.TabIndex = 52;
            this.gbox_BreakTransfer_B.TabStop = false;
            this.gbox_BreakTransfer_B.Text = "     B     ";
            // 
            // tableLayoutPanel42
            // 
            this.tableLayoutPanel42.ColumnCount = 2;
            this.tableLayoutPanel42.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel42.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel42.Controls.Add(this.btn_BreakTransfer_B_PickerDestructionOn, 0, 2);
            this.tableLayoutPanel42.Controls.Add(this.btn_BreakTransfer_B_PickerDestructionOff, 0, 2);
            this.tableLayoutPanel42.Controls.Add(this.btn_BreakTransfer_B_PickerUp, 0, 0);
            this.tableLayoutPanel42.Controls.Add(this.btn_BreakTransfer_B_PickerDown, 1, 0);
            this.tableLayoutPanel42.Controls.Add(this.btn_BreakTransfer_B_PickerPneumaticOn, 0, 1);
            this.tableLayoutPanel42.Controls.Add(this.btn_BreakTransfer_B_PneumaticOff, 1, 1);
            this.tableLayoutPanel42.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel42.Name = "tableLayoutPanel42";
            this.tableLayoutPanel42.RowCount = 3;
            this.tableLayoutPanel42.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel42.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel42.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel42.Size = new System.Drawing.Size(437, 145);
            this.tableLayoutPanel42.TabIndex = 55;
            // 
            // btn_BreakTransfer_B_PickerDestructionOn
            // 
            this.btn_BreakTransfer_B_PickerDestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_B_PickerDestructionOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_BreakTransfer_B_PickerDestructionOn.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_B_PickerDestructionOn.Location = new System.Drawing.Point(3, 99);
            this.btn_BreakTransfer_B_PickerDestructionOn.Name = "btn_BreakTransfer_B_PickerDestructionOn";
            this.btn_BreakTransfer_B_PickerDestructionOn.Size = new System.Drawing.Size(212, 42);
            this.btn_BreakTransfer_B_PickerDestructionOn.TabIndex = 65;
            this.btn_BreakTransfer_B_PickerDestructionOn.Text = "피커\r\n파기 온";
            this.btn_BreakTransfer_B_PickerDestructionOn.UseVisualStyleBackColor = false;
            // 
            // btn_BreakTransfer_B_PickerDestructionOff
            // 
            this.btn_BreakTransfer_B_PickerDestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_B_PickerDestructionOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_BreakTransfer_B_PickerDestructionOff.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_B_PickerDestructionOff.Location = new System.Drawing.Point(221, 99);
            this.btn_BreakTransfer_B_PickerDestructionOff.Name = "btn_BreakTransfer_B_PickerDestructionOff";
            this.btn_BreakTransfer_B_PickerDestructionOff.Size = new System.Drawing.Size(212, 42);
            this.btn_BreakTransfer_B_PickerDestructionOff.TabIndex = 64;
            this.btn_BreakTransfer_B_PickerDestructionOff.Text = "피커\r\n파기 오프";
            this.btn_BreakTransfer_B_PickerDestructionOff.UseVisualStyleBackColor = false;
            // 
            // btn_BreakTransfer_B_PickerUp
            // 
            this.btn_BreakTransfer_B_PickerUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_B_PickerUp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_BreakTransfer_B_PickerUp.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_B_PickerUp.Location = new System.Drawing.Point(3, 3);
            this.btn_BreakTransfer_B_PickerUp.Name = "btn_BreakTransfer_B_PickerUp";
            this.btn_BreakTransfer_B_PickerUp.Size = new System.Drawing.Size(212, 42);
            this.btn_BreakTransfer_B_PickerUp.TabIndex = 60;
            this.btn_BreakTransfer_B_PickerUp.Text = "피커\r\n업";
            this.btn_BreakTransfer_B_PickerUp.UseVisualStyleBackColor = false;
            // 
            // btn_BreakTransfer_B_PickerDown
            // 
            this.btn_BreakTransfer_B_PickerDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_B_PickerDown.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_BreakTransfer_B_PickerDown.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_B_PickerDown.Location = new System.Drawing.Point(221, 3);
            this.btn_BreakTransfer_B_PickerDown.Name = "btn_BreakTransfer_B_PickerDown";
            this.btn_BreakTransfer_B_PickerDown.Size = new System.Drawing.Size(212, 42);
            this.btn_BreakTransfer_B_PickerDown.TabIndex = 61;
            this.btn_BreakTransfer_B_PickerDown.Text = "피커\r\n다운";
            this.btn_BreakTransfer_B_PickerDown.UseVisualStyleBackColor = false;
            // 
            // btn_BreakTransfer_B_PickerPneumaticOn
            // 
            this.btn_BreakTransfer_B_PickerPneumaticOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_B_PickerPneumaticOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_BreakTransfer_B_PickerPneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_B_PickerPneumaticOn.Location = new System.Drawing.Point(3, 51);
            this.btn_BreakTransfer_B_PickerPneumaticOn.Name = "btn_BreakTransfer_B_PickerPneumaticOn";
            this.btn_BreakTransfer_B_PickerPneumaticOn.Size = new System.Drawing.Size(212, 42);
            this.btn_BreakTransfer_B_PickerPneumaticOn.TabIndex = 62;
            this.btn_BreakTransfer_B_PickerPneumaticOn.Text = "피커\r\n공압 온";
            this.btn_BreakTransfer_B_PickerPneumaticOn.UseVisualStyleBackColor = false;
            // 
            // btn_BreakTransfer_B_PneumaticOff
            // 
            this.btn_BreakTransfer_B_PneumaticOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_B_PneumaticOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_BreakTransfer_B_PneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_B_PneumaticOff.Location = new System.Drawing.Point(221, 51);
            this.btn_BreakTransfer_B_PneumaticOff.Name = "btn_BreakTransfer_B_PneumaticOff";
            this.btn_BreakTransfer_B_PneumaticOff.Size = new System.Drawing.Size(212, 42);
            this.btn_BreakTransfer_B_PneumaticOff.TabIndex = 63;
            this.btn_BreakTransfer_B_PneumaticOff.Text = "피커\r\n공압 오프";
            this.btn_BreakTransfer_B_PneumaticOff.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakTransfer_A
            // 
            this.gbox_BreakTransfer_A.Controls.Add(this.tableLayoutPanel41);
            this.gbox_BreakTransfer_A.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakTransfer_A.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakTransfer_A.Location = new System.Drawing.Point(792, 410);
            this.gbox_BreakTransfer_A.Name = "gbox_BreakTransfer_A";
            this.gbox_BreakTransfer_A.Size = new System.Drawing.Size(449, 179);
            this.gbox_BreakTransfer_A.TabIndex = 51;
            this.gbox_BreakTransfer_A.TabStop = false;
            this.gbox_BreakTransfer_A.Text = "     A    ";
            // 
            // tableLayoutPanel41
            // 
            this.tableLayoutPanel41.ColumnCount = 2;
            this.tableLayoutPanel41.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel41.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel41.Controls.Add(this.btn_BreakTransfer_A_PickerDestructionOn, 0, 2);
            this.tableLayoutPanel41.Controls.Add(this.btn_BreakTransfer_A_PickerDestructionOff, 0, 2);
            this.tableLayoutPanel41.Controls.Add(this.btn_BreakTransfer_A_PickerUp, 0, 0);
            this.tableLayoutPanel41.Controls.Add(this.btn_BreakTransfer_A_PickerDown, 1, 0);
            this.tableLayoutPanel41.Controls.Add(this.btn_BreakTransfer_A_PickerPneumaticOn, 0, 1);
            this.tableLayoutPanel41.Controls.Add(this.btn_BreakTransfer_A_PickerPneumaticOff, 1, 1);
            this.tableLayoutPanel41.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel41.Name = "tableLayoutPanel41";
            this.tableLayoutPanel41.RowCount = 3;
            this.tableLayoutPanel41.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel41.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel41.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel41.Size = new System.Drawing.Size(437, 145);
            this.tableLayoutPanel41.TabIndex = 54;
            // 
            // btn_BreakTransfer_A_PickerDestructionOn
            // 
            this.btn_BreakTransfer_A_PickerDestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_A_PickerDestructionOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_BreakTransfer_A_PickerDestructionOn.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_A_PickerDestructionOn.Location = new System.Drawing.Point(3, 99);
            this.btn_BreakTransfer_A_PickerDestructionOn.Name = "btn_BreakTransfer_A_PickerDestructionOn";
            this.btn_BreakTransfer_A_PickerDestructionOn.Size = new System.Drawing.Size(212, 42);
            this.btn_BreakTransfer_A_PickerDestructionOn.TabIndex = 65;
            this.btn_BreakTransfer_A_PickerDestructionOn.Text = "피커\r\n파기 온";
            this.btn_BreakTransfer_A_PickerDestructionOn.UseVisualStyleBackColor = false;
            // 
            // btn_BreakTransfer_A_PickerDestructionOff
            // 
            this.btn_BreakTransfer_A_PickerDestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_A_PickerDestructionOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_BreakTransfer_A_PickerDestructionOff.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_A_PickerDestructionOff.Location = new System.Drawing.Point(221, 99);
            this.btn_BreakTransfer_A_PickerDestructionOff.Name = "btn_BreakTransfer_A_PickerDestructionOff";
            this.btn_BreakTransfer_A_PickerDestructionOff.Size = new System.Drawing.Size(212, 42);
            this.btn_BreakTransfer_A_PickerDestructionOff.TabIndex = 64;
            this.btn_BreakTransfer_A_PickerDestructionOff.Text = "피커\r\n파기 오프";
            this.btn_BreakTransfer_A_PickerDestructionOff.UseVisualStyleBackColor = false;
            // 
            // btn_BreakTransfer_A_PickerUp
            // 
            this.btn_BreakTransfer_A_PickerUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_A_PickerUp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_BreakTransfer_A_PickerUp.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_A_PickerUp.Location = new System.Drawing.Point(3, 3);
            this.btn_BreakTransfer_A_PickerUp.Name = "btn_BreakTransfer_A_PickerUp";
            this.btn_BreakTransfer_A_PickerUp.Size = new System.Drawing.Size(212, 42);
            this.btn_BreakTransfer_A_PickerUp.TabIndex = 60;
            this.btn_BreakTransfer_A_PickerUp.Text = "피커\r\n업";
            this.btn_BreakTransfer_A_PickerUp.UseVisualStyleBackColor = false;
            // 
            // btn_BreakTransfer_A_PickerDown
            // 
            this.btn_BreakTransfer_A_PickerDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_A_PickerDown.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_BreakTransfer_A_PickerDown.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_A_PickerDown.Location = new System.Drawing.Point(221, 3);
            this.btn_BreakTransfer_A_PickerDown.Name = "btn_BreakTransfer_A_PickerDown";
            this.btn_BreakTransfer_A_PickerDown.Size = new System.Drawing.Size(212, 42);
            this.btn_BreakTransfer_A_PickerDown.TabIndex = 61;
            this.btn_BreakTransfer_A_PickerDown.Text = "피커\r\n다운";
            this.btn_BreakTransfer_A_PickerDown.UseVisualStyleBackColor = false;
            // 
            // btn_BreakTransfer_A_PickerPneumaticOn
            // 
            this.btn_BreakTransfer_A_PickerPneumaticOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_A_PickerPneumaticOn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_BreakTransfer_A_PickerPneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_A_PickerPneumaticOn.Location = new System.Drawing.Point(3, 51);
            this.btn_BreakTransfer_A_PickerPneumaticOn.Name = "btn_BreakTransfer_A_PickerPneumaticOn";
            this.btn_BreakTransfer_A_PickerPneumaticOn.Size = new System.Drawing.Size(212, 42);
            this.btn_BreakTransfer_A_PickerPneumaticOn.TabIndex = 62;
            this.btn_BreakTransfer_A_PickerPneumaticOn.Text = "피커\r\n공압 온";
            this.btn_BreakTransfer_A_PickerPneumaticOn.UseVisualStyleBackColor = false;
            // 
            // btn_BreakTransfer_A_PickerPneumaticOff
            // 
            this.btn_BreakTransfer_A_PickerPneumaticOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_A_PickerPneumaticOff.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_BreakTransfer_A_PickerPneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_A_PickerPneumaticOff.Location = new System.Drawing.Point(221, 51);
            this.btn_BreakTransfer_A_PickerPneumaticOff.Name = "btn_BreakTransfer_A_PickerPneumaticOff";
            this.btn_BreakTransfer_A_PickerPneumaticOff.Size = new System.Drawing.Size(212, 42);
            this.btn_BreakTransfer_A_PickerPneumaticOff.TabIndex = 63;
            this.btn_BreakTransfer_A_PickerPneumaticOff.Text = "피커\r\n공압 오프";
            this.btn_BreakTransfer_A_PickerPneumaticOff.UseVisualStyleBackColor = false;
            // 
            // btn_BreakTransfer_Save
            // 
            this.btn_BreakTransfer_Save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_Save.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakTransfer_Save.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_Save.Location = new System.Drawing.Point(1482, 783);
            this.btn_BreakTransfer_Save.Name = "btn_BreakTransfer_Save";
            this.btn_BreakTransfer_Save.Size = new System.Drawing.Size(228, 28);
            this.btn_BreakTransfer_Save.TabIndex = 50;
            this.btn_BreakTransfer_Save.Text = "저장";
            this.btn_BreakTransfer_Save.UseVisualStyleBackColor = false;
            // 
            // btn_BreakTransfer_SpeedSetting
            // 
            this.btn_BreakTransfer_SpeedSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_SpeedSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_BreakTransfer_SpeedSetting.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_SpeedSetting.Location = new System.Drawing.Point(1291, 315);
            this.btn_BreakTransfer_SpeedSetting.Name = "btn_BreakTransfer_SpeedSetting";
            this.btn_BreakTransfer_SpeedSetting.Size = new System.Drawing.Size(90, 27);
            this.btn_BreakTransfer_SpeedSetting.TabIndex = 48;
            this.btn_BreakTransfer_SpeedSetting.Text = "속도 설정";
            this.btn_BreakTransfer_SpeedSetting.UseVisualStyleBackColor = false;
            // 
            // btn_BreakTransfer_AllSetting
            // 
            this.btn_BreakTransfer_AllSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_AllSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_BreakTransfer_AllSetting.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_AllSetting.Location = new System.Drawing.Point(1387, 273);
            this.btn_BreakTransfer_AllSetting.Name = "btn_BreakTransfer_AllSetting";
            this.btn_BreakTransfer_AllSetting.Size = new System.Drawing.Size(90, 27);
            this.btn_BreakTransfer_AllSetting.TabIndex = 47;
            this.btn_BreakTransfer_AllSetting.Text = "전체 설정";
            this.btn_BreakTransfer_AllSetting.UseVisualStyleBackColor = false;
            // 
            // btn_BreakTransfer_LocationSetting
            // 
            this.btn_BreakTransfer_LocationSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_LocationSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_BreakTransfer_LocationSetting.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_LocationSetting.Location = new System.Drawing.Point(1071, 315);
            this.btn_BreakTransfer_LocationSetting.Name = "btn_BreakTransfer_LocationSetting";
            this.btn_BreakTransfer_LocationSetting.Size = new System.Drawing.Size(90, 27);
            this.btn_BreakTransfer_LocationSetting.TabIndex = 46;
            this.btn_BreakTransfer_LocationSetting.Text = "위치 설정";
            this.btn_BreakTransfer_LocationSetting.UseVisualStyleBackColor = false;
            // 
            // btn_BreakTransfer_MoveLocation
            // 
            this.btn_BreakTransfer_MoveLocation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_MoveLocation.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_BreakTransfer_MoveLocation.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_MoveLocation.Location = new System.Drawing.Point(975, 315);
            this.btn_BreakTransfer_MoveLocation.Name = "btn_BreakTransfer_MoveLocation";
            this.btn_BreakTransfer_MoveLocation.Size = new System.Drawing.Size(90, 27);
            this.btn_BreakTransfer_MoveLocation.TabIndex = 45;
            this.btn_BreakTransfer_MoveLocation.Text = "위치 이동";
            this.btn_BreakTransfer_MoveLocation.UseVisualStyleBackColor = false;
            // 
            // tbox_BreakTransfer_Location
            // 
            this.tbox_BreakTransfer_Location.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_BreakTransfer_Location.Location = new System.Drawing.Point(879, 316);
            this.tbox_BreakTransfer_Location.Name = "tbox_BreakTransfer_Location";
            this.tbox_BreakTransfer_Location.Size = new System.Drawing.Size(90, 27);
            this.tbox_BreakTransfer_Location.TabIndex = 44;
            // 
            // tbox_BreakTransfer_Speed
            // 
            this.tbox_BreakTransfer_Speed.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_BreakTransfer_Speed.Location = new System.Drawing.Point(1291, 273);
            this.tbox_BreakTransfer_Speed.Name = "tbox_BreakTransfer_Speed";
            this.tbox_BreakTransfer_Speed.Size = new System.Drawing.Size(90, 27);
            this.tbox_BreakTransfer_Speed.TabIndex = 43;
            // 
            // tbox_BreakTransfer_CurrentLocation
            // 
            this.tbox_BreakTransfer_CurrentLocation.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_BreakTransfer_CurrentLocation.Location = new System.Drawing.Point(1084, 274);
            this.tbox_BreakTransfer_CurrentLocation.Name = "tbox_BreakTransfer_CurrentLocation";
            this.tbox_BreakTransfer_CurrentLocation.Size = new System.Drawing.Size(90, 27);
            this.tbox_BreakTransfer_CurrentLocation.TabIndex = 42;
            // 
            // tbox_BreakTransfer_SelectedShaft
            // 
            this.tbox_BreakTransfer_SelectedShaft.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_BreakTransfer_SelectedShaft.Location = new System.Drawing.Point(879, 273);
            this.tbox_BreakTransfer_SelectedShaft.Name = "tbox_BreakTransfer_SelectedShaft";
            this.tbox_BreakTransfer_SelectedShaft.Size = new System.Drawing.Size(90, 27);
            this.tbox_BreakTransfer_SelectedShaft.TabIndex = 41;
            // 
            // lbl_BreakTransfer_Location
            // 
            this.lbl_BreakTransfer_Location.AutoSize = true;
            this.lbl_BreakTransfer_Location.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_BreakTransfer_Location.ForeColor = System.Drawing.Color.White;
            this.lbl_BreakTransfer_Location.Location = new System.Drawing.Point(801, 316);
            this.lbl_BreakTransfer_Location.Name = "lbl_BreakTransfer_Location";
            this.lbl_BreakTransfer_Location.Size = new System.Drawing.Size(84, 21);
            this.lbl_BreakTransfer_Location.TabIndex = 40;
            this.lbl_BreakTransfer_Location.Text = "위치[mm]";
            // 
            // lbl_BreakTransfer_Speed
            // 
            this.lbl_BreakTransfer_Speed.AutoSize = true;
            this.lbl_BreakTransfer_Speed.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_BreakTransfer_Speed.ForeColor = System.Drawing.Color.White;
            this.lbl_BreakTransfer_Speed.Location = new System.Drawing.Point(1179, 275);
            this.lbl_BreakTransfer_Speed.Name = "lbl_BreakTransfer_Speed";
            this.lbl_BreakTransfer_Speed.Size = new System.Drawing.Size(115, 21);
            this.lbl_BreakTransfer_Speed.TabIndex = 39;
            this.lbl_BreakTransfer_Speed.Text = "속도[mm/sec]";
            // 
            // lbl_BreakTransfer_SelectedShaft
            // 
            this.lbl_BreakTransfer_SelectedShaft.AutoSize = true;
            this.lbl_BreakTransfer_SelectedShaft.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_BreakTransfer_SelectedShaft.ForeColor = System.Drawing.Color.White;
            this.lbl_BreakTransfer_SelectedShaft.Location = new System.Drawing.Point(805, 274);
            this.lbl_BreakTransfer_SelectedShaft.Name = "lbl_BreakTransfer_SelectedShaft";
            this.lbl_BreakTransfer_SelectedShaft.Size = new System.Drawing.Size(80, 21);
            this.lbl_BreakTransfer_SelectedShaft.TabIndex = 38;
            this.lbl_BreakTransfer_SelectedShaft.Text = "선택된 축";
            // 
            // tbox_BreakTransfer_LocationSetting
            // 
            this.tbox_BreakTransfer_LocationSetting.BackColor = System.Drawing.SystemColors.Highlight;
            this.tbox_BreakTransfer_LocationSetting.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.tbox_BreakTransfer_LocationSetting.ForeColor = System.Drawing.Color.White;
            this.tbox_BreakTransfer_LocationSetting.Location = new System.Drawing.Point(792, 223);
            this.tbox_BreakTransfer_LocationSetting.Name = "tbox_BreakTransfer_LocationSetting";
            this.tbox_BreakTransfer_LocationSetting.ReadOnly = true;
            this.tbox_BreakTransfer_LocationSetting.Size = new System.Drawing.Size(89, 27);
            this.tbox_BreakTransfer_LocationSetting.TabIndex = 37;
            this.tbox_BreakTransfer_LocationSetting.Text = "위치값 설정";
            this.tbox_BreakTransfer_LocationSetting.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbl_BreakTransfer_CurrentLocation
            // 
            this.lbl_BreakTransfer_CurrentLocation.AutoSize = true;
            this.lbl_BreakTransfer_CurrentLocation.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_BreakTransfer_CurrentLocation.ForeColor = System.Drawing.Color.White;
            this.lbl_BreakTransfer_CurrentLocation.Location = new System.Drawing.Point(975, 274);
            this.lbl_BreakTransfer_CurrentLocation.Name = "lbl_BreakTransfer_CurrentLocation";
            this.lbl_BreakTransfer_CurrentLocation.Size = new System.Drawing.Size(116, 21);
            this.lbl_BreakTransfer_CurrentLocation.TabIndex = 36;
            this.lbl_BreakTransfer_CurrentLocation.Text = "현재위치[mm]";
            // 
            // btn_BreakTransfer_GrabSwitch3
            // 
            this.btn_BreakTransfer_GrabSwitch3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_GrabSwitch3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakTransfer_GrabSwitch3.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_GrabSwitch3.Location = new System.Drawing.Point(1514, 6);
            this.btn_BreakTransfer_GrabSwitch3.Name = "btn_BreakTransfer_GrabSwitch3";
            this.btn_BreakTransfer_GrabSwitch3.Size = new System.Drawing.Size(172, 23);
            this.btn_BreakTransfer_GrabSwitch3.TabIndex = 35;
            this.btn_BreakTransfer_GrabSwitch3.Text = "Grab Switch 3";
            this.btn_BreakTransfer_GrabSwitch3.UseVisualStyleBackColor = false;
            // 
            // btn_BreakTransfer_GrabSwitch2
            // 
            this.btn_BreakTransfer_GrabSwitch2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_GrabSwitch2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakTransfer_GrabSwitch2.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_GrabSwitch2.Location = new System.Drawing.Point(1336, 6);
            this.btn_BreakTransfer_GrabSwitch2.Name = "btn_BreakTransfer_GrabSwitch2";
            this.btn_BreakTransfer_GrabSwitch2.Size = new System.Drawing.Size(172, 23);
            this.btn_BreakTransfer_GrabSwitch2.TabIndex = 34;
            this.btn_BreakTransfer_GrabSwitch2.Text = "Grab Switch 2";
            this.btn_BreakTransfer_GrabSwitch2.UseVisualStyleBackColor = false;
            // 
            // btn_BreakTransfer_GrabSwitch1
            // 
            this.btn_BreakTransfer_GrabSwitch1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakTransfer_GrabSwitch1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakTransfer_GrabSwitch1.ForeColor = System.Drawing.Color.White;
            this.btn_BreakTransfer_GrabSwitch1.Location = new System.Drawing.Point(1158, 6);
            this.btn_BreakTransfer_GrabSwitch1.Name = "btn_BreakTransfer_GrabSwitch1";
            this.btn_BreakTransfer_GrabSwitch1.Size = new System.Drawing.Size(172, 23);
            this.btn_BreakTransfer_GrabSwitch1.TabIndex = 33;
            this.btn_BreakTransfer_GrabSwitch1.Text = "Grab Switch 1";
            this.btn_BreakTransfer_GrabSwitch1.UseVisualStyleBackColor = false;
            // 
            // lv_BreakTransfer
            // 
            this.lv_BreakTransfer.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.col_BreakTransfer_NamebyLocation,
            this.col_BreakTransfer_Shaft,
            this.col_BreakTransfer_Location,
            this.col_BreakTransfer_Speed});
            this.lv_BreakTransfer.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lv_BreakTransfer.GridLines = true;
            this.lv_BreakTransfer.Location = new System.Drawing.Point(17, 16);
            this.lv_BreakTransfer.Name = "lv_BreakTransfer";
            this.lv_BreakTransfer.Size = new System.Drawing.Size(758, 734);
            this.lv_BreakTransfer.TabIndex = 32;
            this.lv_BreakTransfer.UseCompatibleStateImageBehavior = false;
            this.lv_BreakTransfer.View = System.Windows.Forms.View.Details;
            // 
            // col_BreakTransfer_NamebyLocation
            // 
            this.col_BreakTransfer_NamebyLocation.Text = "위치별 명칭";
            this.col_BreakTransfer_NamebyLocation.Width = 430;
            // 
            // col_BreakTransfer_Shaft
            // 
            this.col_BreakTransfer_Shaft.Text = "축";
            this.col_BreakTransfer_Shaft.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // col_BreakTransfer_Location
            // 
            this.col_BreakTransfer_Location.Text = "위치[mm]";
            this.col_BreakTransfer_Location.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_BreakTransfer_Location.Width = 125;
            // 
            // col_BreakTransfer_Speed
            // 
            this.col_BreakTransfer_Speed.Text = "속도[mm/s]";
            this.col_BreakTransfer_Speed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_BreakTransfer_Speed.Width = 125;
            // 
            // tp_BreakUnitXZ
            // 
            this.tp_BreakUnitXZ.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tp_BreakUnitXZ.Controls.Add(this.gbox_BreakUnitXZ_Move);
            this.tp_BreakUnitXZ.Controls.Add(this.gbox_BreakUnitXZ_Save);
            this.tp_BreakUnitXZ.Controls.Add(this.btn_BreakUnitXZ_SpeedSetting);
            this.tp_BreakUnitXZ.Controls.Add(this.btn_BreakUnitXZ_AllSetting);
            this.tp_BreakUnitXZ.Controls.Add(this.btn_BreakUnitXZ_LocationSetting);
            this.tp_BreakUnitXZ.Controls.Add(this.btn_BreakUnitXZ_MoveLocation);
            this.tp_BreakUnitXZ.Controls.Add(this.tbox_BreakUnitXZ_Location);
            this.tp_BreakUnitXZ.Controls.Add(this.tbox_BreakUnitXZ_Speed);
            this.tp_BreakUnitXZ.Controls.Add(this.tbox_BreakUnitXZ_CurrentLocation);
            this.tp_BreakUnitXZ.Controls.Add(this.tbox_BreakUnitXZ_SelectedShaft);
            this.tp_BreakUnitXZ.Controls.Add(this.lbl_BreakUnitXZ_Location);
            this.tp_BreakUnitXZ.Controls.Add(this.lbl_BreakUnitXZ_Speed);
            this.tp_BreakUnitXZ.Controls.Add(this.lbl_BreakUnitXZ_SelectedShaft);
            this.tp_BreakUnitXZ.Controls.Add(this.tbox_BreakUnitXZ_LocationSetting);
            this.tp_BreakUnitXZ.Controls.Add(this.lbl_BreakUnitXZ_CurrentLocation);
            this.tp_BreakUnitXZ.Controls.Add(this.btn_BreakUnitXZ_GrabSwitch3);
            this.tp_BreakUnitXZ.Controls.Add(this.btn_BreakUnitXZ_GrabSwitch2);
            this.tp_BreakUnitXZ.Controls.Add(this.btn_BreakUnitXZ_GrabSwitch1);
            this.tp_BreakUnitXZ.Controls.Add(this.lv_BreakUnitXZ);
            this.tp_BreakUnitXZ.Controls.Add(this.ajin_Setting_BreakUnitXZ);
            this.tp_BreakUnitXZ.Location = new System.Drawing.Point(4, 34);
            this.tp_BreakUnitXZ.Name = "tp_BreakUnitXZ";
            this.tp_BreakUnitXZ.Size = new System.Drawing.Size(1726, 816);
            this.tp_BreakUnitXZ.TabIndex = 5;
            this.tp_BreakUnitXZ.Text = "Break 유닛(X축 Z축)";
            // 
            // gbox_BreakUnitXZ_Move
            // 
            this.gbox_BreakUnitXZ_Move.Controls.Add(this.tableLayoutPanel45);
            this.gbox_BreakUnitXZ_Move.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakUnitXZ_Move.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_Move.Location = new System.Drawing.Point(792, 595);
            this.gbox_BreakUnitXZ_Move.Name = "gbox_BreakUnitXZ_Move";
            this.gbox_BreakUnitXZ_Move.Size = new System.Drawing.Size(904, 179);
            this.gbox_BreakUnitXZ_Move.TabIndex = 53;
            this.gbox_BreakUnitXZ_Move.TabStop = false;
            this.gbox_BreakUnitXZ_Move.Text = "     이동     ";
            // 
            // tableLayoutPanel45
            // 
            this.tableLayoutPanel45.ColumnCount = 4;
            this.tableLayoutPanel45.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel45.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel45.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel45.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel45.Controls.Add(this.gbox_BreakUnitXZ_MoveZ4, 3, 0);
            this.tableLayoutPanel45.Controls.Add(this.gbox_BreakUnitXZ_MoveZ3, 2, 0);
            this.tableLayoutPanel45.Controls.Add(this.gbox_BreakUnitXZ_MoveZ2, 1, 0);
            this.tableLayoutPanel45.Controls.Add(this.gbox_BreakUnitXZ_MoveZ1, 0, 0);
            this.tableLayoutPanel45.Location = new System.Drawing.Point(6, 19);
            this.tableLayoutPanel45.Name = "tableLayoutPanel45";
            this.tableLayoutPanel45.RowCount = 1;
            this.tableLayoutPanel45.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel45.Size = new System.Drawing.Size(661, 154);
            this.tableLayoutPanel45.TabIndex = 54;
            // 
            // gbox_BreakUnitXZ_MoveZ4
            // 
            this.gbox_BreakUnitXZ_MoveZ4.Controls.Add(this.tableLayoutPanel49);
            this.gbox_BreakUnitXZ_MoveZ4.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_MoveZ4.Location = new System.Drawing.Point(498, 3);
            this.gbox_BreakUnitXZ_MoveZ4.Name = "gbox_BreakUnitXZ_MoveZ4";
            this.gbox_BreakUnitXZ_MoveZ4.Size = new System.Drawing.Size(159, 148);
            this.gbox_BreakUnitXZ_MoveZ4.TabIndex = 57;
            this.gbox_BreakUnitXZ_MoveZ4.TabStop = false;
            this.gbox_BreakUnitXZ_MoveZ4.Text = "     Z4     ";
            // 
            // tableLayoutPanel49
            // 
            this.tableLayoutPanel49.ColumnCount = 1;
            this.tableLayoutPanel49.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel49.Controls.Add(this.gbox_BreakUnitXZ_MoveZ4_SlowDescent, 0, 2);
            this.tableLayoutPanel49.Controls.Add(this.gbox_BreakUnitXZ_MoveZ4_VisionCheck, 0, 0);
            this.tableLayoutPanel49.Controls.Add(this.gbox_BreakUnitXZ_MoveZ4_FastDescent, 0, 1);
            this.tableLayoutPanel49.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel49.Name = "tableLayoutPanel49";
            this.tableLayoutPanel49.RowCount = 3;
            this.tableLayoutPanel49.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel49.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel49.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel49.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel49.Size = new System.Drawing.Size(149, 114);
            this.tableLayoutPanel49.TabIndex = 55;
            // 
            // gbox_BreakUnitXZ_MoveZ4_SlowDescent
            // 
            this.gbox_BreakUnitXZ_MoveZ4_SlowDescent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gbox_BreakUnitXZ_MoveZ4_SlowDescent.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.gbox_BreakUnitXZ_MoveZ4_SlowDescent.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_MoveZ4_SlowDescent.Location = new System.Drawing.Point(3, 79);
            this.gbox_BreakUnitXZ_MoveZ4_SlowDescent.Name = "gbox_BreakUnitXZ_MoveZ4_SlowDescent";
            this.gbox_BreakUnitXZ_MoveZ4_SlowDescent.Size = new System.Drawing.Size(143, 32);
            this.gbox_BreakUnitXZ_MoveZ4_SlowDescent.TabIndex = 63;
            this.gbox_BreakUnitXZ_MoveZ4_SlowDescent.Text = "저속 하강";
            this.gbox_BreakUnitXZ_MoveZ4_SlowDescent.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitXZ_MoveZ4_VisionCheck
            // 
            this.gbox_BreakUnitXZ_MoveZ4_VisionCheck.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gbox_BreakUnitXZ_MoveZ4_VisionCheck.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.gbox_BreakUnitXZ_MoveZ4_VisionCheck.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_MoveZ4_VisionCheck.Location = new System.Drawing.Point(3, 3);
            this.gbox_BreakUnitXZ_MoveZ4_VisionCheck.Name = "gbox_BreakUnitXZ_MoveZ4_VisionCheck";
            this.gbox_BreakUnitXZ_MoveZ4_VisionCheck.Size = new System.Drawing.Size(143, 31);
            this.gbox_BreakUnitXZ_MoveZ4_VisionCheck.TabIndex = 61;
            this.gbox_BreakUnitXZ_MoveZ4_VisionCheck.Text = "비젼 확인";
            this.gbox_BreakUnitXZ_MoveZ4_VisionCheck.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitXZ_MoveZ4_FastDescent
            // 
            this.gbox_BreakUnitXZ_MoveZ4_FastDescent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gbox_BreakUnitXZ_MoveZ4_FastDescent.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.gbox_BreakUnitXZ_MoveZ4_FastDescent.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_MoveZ4_FastDescent.Location = new System.Drawing.Point(3, 41);
            this.gbox_BreakUnitXZ_MoveZ4_FastDescent.Name = "gbox_BreakUnitXZ_MoveZ4_FastDescent";
            this.gbox_BreakUnitXZ_MoveZ4_FastDescent.Size = new System.Drawing.Size(143, 31);
            this.gbox_BreakUnitXZ_MoveZ4_FastDescent.TabIndex = 62;
            this.gbox_BreakUnitXZ_MoveZ4_FastDescent.Text = "고속 하강";
            this.gbox_BreakUnitXZ_MoveZ4_FastDescent.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitXZ_MoveZ3
            // 
            this.gbox_BreakUnitXZ_MoveZ3.Controls.Add(this.tableLayoutPanel48);
            this.gbox_BreakUnitXZ_MoveZ3.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_MoveZ3.Location = new System.Drawing.Point(333, 3);
            this.gbox_BreakUnitXZ_MoveZ3.Name = "gbox_BreakUnitXZ_MoveZ3";
            this.gbox_BreakUnitXZ_MoveZ3.Size = new System.Drawing.Size(159, 148);
            this.gbox_BreakUnitXZ_MoveZ3.TabIndex = 56;
            this.gbox_BreakUnitXZ_MoveZ3.TabStop = false;
            this.gbox_BreakUnitXZ_MoveZ3.Text = "     Z3     ";
            // 
            // tableLayoutPanel48
            // 
            this.tableLayoutPanel48.ColumnCount = 1;
            this.tableLayoutPanel48.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel48.Controls.Add(this.gbox_BreakUnitXZ_MoveZ3_SlowDescent, 0, 2);
            this.tableLayoutPanel48.Controls.Add(this.gbox_BreakUnitXZ_MoveZ3_VisionCheck, 0, 0);
            this.tableLayoutPanel48.Controls.Add(this.gbox_BreakUnitXZ_MoveZ3_FastDescent, 0, 1);
            this.tableLayoutPanel48.Location = new System.Drawing.Point(4, 28);
            this.tableLayoutPanel48.Name = "tableLayoutPanel48";
            this.tableLayoutPanel48.RowCount = 3;
            this.tableLayoutPanel48.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel48.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel48.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel48.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel48.Size = new System.Drawing.Size(149, 114);
            this.tableLayoutPanel48.TabIndex = 55;
            // 
            // gbox_BreakUnitXZ_MoveZ3_SlowDescent
            // 
            this.gbox_BreakUnitXZ_MoveZ3_SlowDescent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gbox_BreakUnitXZ_MoveZ3_SlowDescent.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.gbox_BreakUnitXZ_MoveZ3_SlowDescent.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_MoveZ3_SlowDescent.Location = new System.Drawing.Point(3, 79);
            this.gbox_BreakUnitXZ_MoveZ3_SlowDescent.Name = "gbox_BreakUnitXZ_MoveZ3_SlowDescent";
            this.gbox_BreakUnitXZ_MoveZ3_SlowDescent.Size = new System.Drawing.Size(143, 32);
            this.gbox_BreakUnitXZ_MoveZ3_SlowDescent.TabIndex = 63;
            this.gbox_BreakUnitXZ_MoveZ3_SlowDescent.Text = "저속 하강";
            this.gbox_BreakUnitXZ_MoveZ3_SlowDescent.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitXZ_MoveZ3_VisionCheck
            // 
            this.gbox_BreakUnitXZ_MoveZ3_VisionCheck.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gbox_BreakUnitXZ_MoveZ3_VisionCheck.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.gbox_BreakUnitXZ_MoveZ3_VisionCheck.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_MoveZ3_VisionCheck.Location = new System.Drawing.Point(3, 3);
            this.gbox_BreakUnitXZ_MoveZ3_VisionCheck.Name = "gbox_BreakUnitXZ_MoveZ3_VisionCheck";
            this.gbox_BreakUnitXZ_MoveZ3_VisionCheck.Size = new System.Drawing.Size(143, 31);
            this.gbox_BreakUnitXZ_MoveZ3_VisionCheck.TabIndex = 61;
            this.gbox_BreakUnitXZ_MoveZ3_VisionCheck.Text = "비젼 확인";
            this.gbox_BreakUnitXZ_MoveZ3_VisionCheck.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitXZ_MoveZ3_FastDescent
            // 
            this.gbox_BreakUnitXZ_MoveZ3_FastDescent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gbox_BreakUnitXZ_MoveZ3_FastDescent.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.gbox_BreakUnitXZ_MoveZ3_FastDescent.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_MoveZ3_FastDescent.Location = new System.Drawing.Point(3, 41);
            this.gbox_BreakUnitXZ_MoveZ3_FastDescent.Name = "gbox_BreakUnitXZ_MoveZ3_FastDescent";
            this.gbox_BreakUnitXZ_MoveZ3_FastDescent.Size = new System.Drawing.Size(143, 31);
            this.gbox_BreakUnitXZ_MoveZ3_FastDescent.TabIndex = 62;
            this.gbox_BreakUnitXZ_MoveZ3_FastDescent.Text = "고속 하강";
            this.gbox_BreakUnitXZ_MoveZ3_FastDescent.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitXZ_MoveZ2
            // 
            this.gbox_BreakUnitXZ_MoveZ2.Controls.Add(this.tableLayoutPanel47);
            this.gbox_BreakUnitXZ_MoveZ2.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_MoveZ2.Location = new System.Drawing.Point(168, 3);
            this.gbox_BreakUnitXZ_MoveZ2.Name = "gbox_BreakUnitXZ_MoveZ2";
            this.gbox_BreakUnitXZ_MoveZ2.Size = new System.Drawing.Size(159, 148);
            this.gbox_BreakUnitXZ_MoveZ2.TabIndex = 55;
            this.gbox_BreakUnitXZ_MoveZ2.TabStop = false;
            this.gbox_BreakUnitXZ_MoveZ2.Text = "     Z2     ";
            // 
            // tableLayoutPanel47
            // 
            this.tableLayoutPanel47.ColumnCount = 1;
            this.tableLayoutPanel47.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel47.Controls.Add(this.gbox_BreakUnitXZ_MoveZ2_SlowDescent, 0, 2);
            this.tableLayoutPanel47.Controls.Add(this.gbox_BreakUnitXZ_MoveZ2_VisionCheck, 0, 0);
            this.tableLayoutPanel47.Controls.Add(this.gbox_BreakUnitXZ_MoveZ2_FastDescent, 0, 1);
            this.tableLayoutPanel47.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel47.Name = "tableLayoutPanel47";
            this.tableLayoutPanel47.RowCount = 3;
            this.tableLayoutPanel47.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel47.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel47.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel47.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel47.Size = new System.Drawing.Size(149, 114);
            this.tableLayoutPanel47.TabIndex = 55;
            // 
            // gbox_BreakUnitXZ_MoveZ2_SlowDescent
            // 
            this.gbox_BreakUnitXZ_MoveZ2_SlowDescent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gbox_BreakUnitXZ_MoveZ2_SlowDescent.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.gbox_BreakUnitXZ_MoveZ2_SlowDescent.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_MoveZ2_SlowDescent.Location = new System.Drawing.Point(3, 79);
            this.gbox_BreakUnitXZ_MoveZ2_SlowDescent.Name = "gbox_BreakUnitXZ_MoveZ2_SlowDescent";
            this.gbox_BreakUnitXZ_MoveZ2_SlowDescent.Size = new System.Drawing.Size(143, 32);
            this.gbox_BreakUnitXZ_MoveZ2_SlowDescent.TabIndex = 63;
            this.gbox_BreakUnitXZ_MoveZ2_SlowDescent.Text = "저속 하강";
            this.gbox_BreakUnitXZ_MoveZ2_SlowDescent.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitXZ_MoveZ2_VisionCheck
            // 
            this.gbox_BreakUnitXZ_MoveZ2_VisionCheck.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gbox_BreakUnitXZ_MoveZ2_VisionCheck.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.gbox_BreakUnitXZ_MoveZ2_VisionCheck.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_MoveZ2_VisionCheck.Location = new System.Drawing.Point(3, 3);
            this.gbox_BreakUnitXZ_MoveZ2_VisionCheck.Name = "gbox_BreakUnitXZ_MoveZ2_VisionCheck";
            this.gbox_BreakUnitXZ_MoveZ2_VisionCheck.Size = new System.Drawing.Size(143, 31);
            this.gbox_BreakUnitXZ_MoveZ2_VisionCheck.TabIndex = 61;
            this.gbox_BreakUnitXZ_MoveZ2_VisionCheck.Text = "비젼 확인";
            this.gbox_BreakUnitXZ_MoveZ2_VisionCheck.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitXZ_MoveZ2_FastDescent
            // 
            this.gbox_BreakUnitXZ_MoveZ2_FastDescent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gbox_BreakUnitXZ_MoveZ2_FastDescent.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.gbox_BreakUnitXZ_MoveZ2_FastDescent.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_MoveZ2_FastDescent.Location = new System.Drawing.Point(3, 41);
            this.gbox_BreakUnitXZ_MoveZ2_FastDescent.Name = "gbox_BreakUnitXZ_MoveZ2_FastDescent";
            this.gbox_BreakUnitXZ_MoveZ2_FastDescent.Size = new System.Drawing.Size(143, 31);
            this.gbox_BreakUnitXZ_MoveZ2_FastDescent.TabIndex = 62;
            this.gbox_BreakUnitXZ_MoveZ2_FastDescent.Text = "고속 하강";
            this.gbox_BreakUnitXZ_MoveZ2_FastDescent.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitXZ_MoveZ1
            // 
            this.gbox_BreakUnitXZ_MoveZ1.Controls.Add(this.tableLayoutPanel46);
            this.gbox_BreakUnitXZ_MoveZ1.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_MoveZ1.Location = new System.Drawing.Point(3, 3);
            this.gbox_BreakUnitXZ_MoveZ1.Name = "gbox_BreakUnitXZ_MoveZ1";
            this.gbox_BreakUnitXZ_MoveZ1.Size = new System.Drawing.Size(159, 148);
            this.gbox_BreakUnitXZ_MoveZ1.TabIndex = 54;
            this.gbox_BreakUnitXZ_MoveZ1.TabStop = false;
            this.gbox_BreakUnitXZ_MoveZ1.Text = "     Z1     ";
            // 
            // tableLayoutPanel46
            // 
            this.tableLayoutPanel46.ColumnCount = 1;
            this.tableLayoutPanel46.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel46.Controls.Add(this.gbox_BreakUnitXZ_MoveZ1_SlowDescent, 0, 2);
            this.tableLayoutPanel46.Controls.Add(this.gbox_BreakUnitXZ_MoveZ1_VisionCheck, 0, 0);
            this.tableLayoutPanel46.Controls.Add(this.gbox_BreakUnitXZ_MoveZ1_FastDescent, 0, 1);
            this.tableLayoutPanel46.Location = new System.Drawing.Point(4, 28);
            this.tableLayoutPanel46.Name = "tableLayoutPanel46";
            this.tableLayoutPanel46.RowCount = 3;
            this.tableLayoutPanel46.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel46.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel46.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel46.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel46.Size = new System.Drawing.Size(149, 114);
            this.tableLayoutPanel46.TabIndex = 54;
            // 
            // gbox_BreakUnitXZ_MoveZ1_SlowDescent
            // 
            this.gbox_BreakUnitXZ_MoveZ1_SlowDescent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gbox_BreakUnitXZ_MoveZ1_SlowDescent.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.gbox_BreakUnitXZ_MoveZ1_SlowDescent.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_MoveZ1_SlowDescent.Location = new System.Drawing.Point(3, 79);
            this.gbox_BreakUnitXZ_MoveZ1_SlowDescent.Name = "gbox_BreakUnitXZ_MoveZ1_SlowDescent";
            this.gbox_BreakUnitXZ_MoveZ1_SlowDescent.Size = new System.Drawing.Size(143, 32);
            this.gbox_BreakUnitXZ_MoveZ1_SlowDescent.TabIndex = 63;
            this.gbox_BreakUnitXZ_MoveZ1_SlowDescent.Text = "저속 하강";
            this.gbox_BreakUnitXZ_MoveZ1_SlowDescent.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitXZ_MoveZ1_VisionCheck
            // 
            this.gbox_BreakUnitXZ_MoveZ1_VisionCheck.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gbox_BreakUnitXZ_MoveZ1_VisionCheck.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.gbox_BreakUnitXZ_MoveZ1_VisionCheck.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_MoveZ1_VisionCheck.Location = new System.Drawing.Point(3, 3);
            this.gbox_BreakUnitXZ_MoveZ1_VisionCheck.Name = "gbox_BreakUnitXZ_MoveZ1_VisionCheck";
            this.gbox_BreakUnitXZ_MoveZ1_VisionCheck.Size = new System.Drawing.Size(143, 31);
            this.gbox_BreakUnitXZ_MoveZ1_VisionCheck.TabIndex = 61;
            this.gbox_BreakUnitXZ_MoveZ1_VisionCheck.Text = "비젼 확인";
            this.gbox_BreakUnitXZ_MoveZ1_VisionCheck.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitXZ_MoveZ1_FastDescent
            // 
            this.gbox_BreakUnitXZ_MoveZ1_FastDescent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gbox_BreakUnitXZ_MoveZ1_FastDescent.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.gbox_BreakUnitXZ_MoveZ1_FastDescent.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_MoveZ1_FastDescent.Location = new System.Drawing.Point(3, 41);
            this.gbox_BreakUnitXZ_MoveZ1_FastDescent.Name = "gbox_BreakUnitXZ_MoveZ1_FastDescent";
            this.gbox_BreakUnitXZ_MoveZ1_FastDescent.Size = new System.Drawing.Size(143, 31);
            this.gbox_BreakUnitXZ_MoveZ1_FastDescent.TabIndex = 62;
            this.gbox_BreakUnitXZ_MoveZ1_FastDescent.Text = "고속 하강";
            this.gbox_BreakUnitXZ_MoveZ1_FastDescent.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitXZ_Save
            // 
            this.gbox_BreakUnitXZ_Save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gbox_BreakUnitXZ_Save.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakUnitXZ_Save.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitXZ_Save.Location = new System.Drawing.Point(1482, 783);
            this.gbox_BreakUnitXZ_Save.Name = "gbox_BreakUnitXZ_Save";
            this.gbox_BreakUnitXZ_Save.Size = new System.Drawing.Size(228, 28);
            this.gbox_BreakUnitXZ_Save.TabIndex = 50;
            this.gbox_BreakUnitXZ_Save.Text = "저장";
            this.gbox_BreakUnitXZ_Save.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitXZ_SpeedSetting
            // 
            this.btn_BreakUnitXZ_SpeedSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitXZ_SpeedSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_BreakUnitXZ_SpeedSetting.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitXZ_SpeedSetting.Location = new System.Drawing.Point(1291, 315);
            this.btn_BreakUnitXZ_SpeedSetting.Name = "btn_BreakUnitXZ_SpeedSetting";
            this.btn_BreakUnitXZ_SpeedSetting.Size = new System.Drawing.Size(90, 27);
            this.btn_BreakUnitXZ_SpeedSetting.TabIndex = 48;
            this.btn_BreakUnitXZ_SpeedSetting.Text = "속도 설정";
            this.btn_BreakUnitXZ_SpeedSetting.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitXZ_AllSetting
            // 
            this.btn_BreakUnitXZ_AllSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitXZ_AllSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_BreakUnitXZ_AllSetting.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitXZ_AllSetting.Location = new System.Drawing.Point(1387, 273);
            this.btn_BreakUnitXZ_AllSetting.Name = "btn_BreakUnitXZ_AllSetting";
            this.btn_BreakUnitXZ_AllSetting.Size = new System.Drawing.Size(90, 27);
            this.btn_BreakUnitXZ_AllSetting.TabIndex = 47;
            this.btn_BreakUnitXZ_AllSetting.Text = "전체 설정";
            this.btn_BreakUnitXZ_AllSetting.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitXZ_LocationSetting
            // 
            this.btn_BreakUnitXZ_LocationSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitXZ_LocationSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_BreakUnitXZ_LocationSetting.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitXZ_LocationSetting.Location = new System.Drawing.Point(1071, 315);
            this.btn_BreakUnitXZ_LocationSetting.Name = "btn_BreakUnitXZ_LocationSetting";
            this.btn_BreakUnitXZ_LocationSetting.Size = new System.Drawing.Size(90, 27);
            this.btn_BreakUnitXZ_LocationSetting.TabIndex = 46;
            this.btn_BreakUnitXZ_LocationSetting.Text = "위치 설정";
            this.btn_BreakUnitXZ_LocationSetting.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitXZ_MoveLocation
            // 
            this.btn_BreakUnitXZ_MoveLocation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitXZ_MoveLocation.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_BreakUnitXZ_MoveLocation.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitXZ_MoveLocation.Location = new System.Drawing.Point(975, 315);
            this.btn_BreakUnitXZ_MoveLocation.Name = "btn_BreakUnitXZ_MoveLocation";
            this.btn_BreakUnitXZ_MoveLocation.Size = new System.Drawing.Size(90, 27);
            this.btn_BreakUnitXZ_MoveLocation.TabIndex = 45;
            this.btn_BreakUnitXZ_MoveLocation.Text = "위치 이동";
            this.btn_BreakUnitXZ_MoveLocation.UseVisualStyleBackColor = false;
            // 
            // tbox_BreakUnitXZ_Location
            // 
            this.tbox_BreakUnitXZ_Location.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_BreakUnitXZ_Location.Location = new System.Drawing.Point(879, 316);
            this.tbox_BreakUnitXZ_Location.Name = "tbox_BreakUnitXZ_Location";
            this.tbox_BreakUnitXZ_Location.Size = new System.Drawing.Size(90, 27);
            this.tbox_BreakUnitXZ_Location.TabIndex = 44;
            // 
            // tbox_BreakUnitXZ_Speed
            // 
            this.tbox_BreakUnitXZ_Speed.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_BreakUnitXZ_Speed.Location = new System.Drawing.Point(1291, 273);
            this.tbox_BreakUnitXZ_Speed.Name = "tbox_BreakUnitXZ_Speed";
            this.tbox_BreakUnitXZ_Speed.Size = new System.Drawing.Size(90, 27);
            this.tbox_BreakUnitXZ_Speed.TabIndex = 43;
            // 
            // tbox_BreakUnitXZ_CurrentLocation
            // 
            this.tbox_BreakUnitXZ_CurrentLocation.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_BreakUnitXZ_CurrentLocation.Location = new System.Drawing.Point(1084, 274);
            this.tbox_BreakUnitXZ_CurrentLocation.Name = "tbox_BreakUnitXZ_CurrentLocation";
            this.tbox_BreakUnitXZ_CurrentLocation.Size = new System.Drawing.Size(90, 27);
            this.tbox_BreakUnitXZ_CurrentLocation.TabIndex = 42;
            // 
            // tbox_BreakUnitXZ_SelectedShaft
            // 
            this.tbox_BreakUnitXZ_SelectedShaft.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_BreakUnitXZ_SelectedShaft.Location = new System.Drawing.Point(879, 273);
            this.tbox_BreakUnitXZ_SelectedShaft.Name = "tbox_BreakUnitXZ_SelectedShaft";
            this.tbox_BreakUnitXZ_SelectedShaft.Size = new System.Drawing.Size(90, 27);
            this.tbox_BreakUnitXZ_SelectedShaft.TabIndex = 41;
            // 
            // lbl_BreakUnitXZ_Location
            // 
            this.lbl_BreakUnitXZ_Location.AutoSize = true;
            this.lbl_BreakUnitXZ_Location.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_BreakUnitXZ_Location.ForeColor = System.Drawing.Color.White;
            this.lbl_BreakUnitXZ_Location.Location = new System.Drawing.Point(801, 316);
            this.lbl_BreakUnitXZ_Location.Name = "lbl_BreakUnitXZ_Location";
            this.lbl_BreakUnitXZ_Location.Size = new System.Drawing.Size(84, 21);
            this.lbl_BreakUnitXZ_Location.TabIndex = 40;
            this.lbl_BreakUnitXZ_Location.Text = "위치[mm]";
            // 
            // lbl_BreakUnitXZ_Speed
            // 
            this.lbl_BreakUnitXZ_Speed.AutoSize = true;
            this.lbl_BreakUnitXZ_Speed.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_BreakUnitXZ_Speed.ForeColor = System.Drawing.Color.White;
            this.lbl_BreakUnitXZ_Speed.Location = new System.Drawing.Point(1179, 275);
            this.lbl_BreakUnitXZ_Speed.Name = "lbl_BreakUnitXZ_Speed";
            this.lbl_BreakUnitXZ_Speed.Size = new System.Drawing.Size(115, 21);
            this.lbl_BreakUnitXZ_Speed.TabIndex = 39;
            this.lbl_BreakUnitXZ_Speed.Text = "속도[mm/sec]";
            // 
            // lbl_BreakUnitXZ_SelectedShaft
            // 
            this.lbl_BreakUnitXZ_SelectedShaft.AutoSize = true;
            this.lbl_BreakUnitXZ_SelectedShaft.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_BreakUnitXZ_SelectedShaft.ForeColor = System.Drawing.Color.White;
            this.lbl_BreakUnitXZ_SelectedShaft.Location = new System.Drawing.Point(805, 274);
            this.lbl_BreakUnitXZ_SelectedShaft.Name = "lbl_BreakUnitXZ_SelectedShaft";
            this.lbl_BreakUnitXZ_SelectedShaft.Size = new System.Drawing.Size(80, 21);
            this.lbl_BreakUnitXZ_SelectedShaft.TabIndex = 38;
            this.lbl_BreakUnitXZ_SelectedShaft.Text = "선택된 축";
            // 
            // tbox_BreakUnitXZ_LocationSetting
            // 
            this.tbox_BreakUnitXZ_LocationSetting.BackColor = System.Drawing.SystemColors.Highlight;
            this.tbox_BreakUnitXZ_LocationSetting.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.tbox_BreakUnitXZ_LocationSetting.ForeColor = System.Drawing.Color.White;
            this.tbox_BreakUnitXZ_LocationSetting.Location = new System.Drawing.Point(792, 223);
            this.tbox_BreakUnitXZ_LocationSetting.Name = "tbox_BreakUnitXZ_LocationSetting";
            this.tbox_BreakUnitXZ_LocationSetting.ReadOnly = true;
            this.tbox_BreakUnitXZ_LocationSetting.Size = new System.Drawing.Size(89, 27);
            this.tbox_BreakUnitXZ_LocationSetting.TabIndex = 37;
            this.tbox_BreakUnitXZ_LocationSetting.Text = "위치값 설정";
            this.tbox_BreakUnitXZ_LocationSetting.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbl_BreakUnitXZ_CurrentLocation
            // 
            this.lbl_BreakUnitXZ_CurrentLocation.AutoSize = true;
            this.lbl_BreakUnitXZ_CurrentLocation.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_BreakUnitXZ_CurrentLocation.ForeColor = System.Drawing.Color.White;
            this.lbl_BreakUnitXZ_CurrentLocation.Location = new System.Drawing.Point(975, 274);
            this.lbl_BreakUnitXZ_CurrentLocation.Name = "lbl_BreakUnitXZ_CurrentLocation";
            this.lbl_BreakUnitXZ_CurrentLocation.Size = new System.Drawing.Size(116, 21);
            this.lbl_BreakUnitXZ_CurrentLocation.TabIndex = 36;
            this.lbl_BreakUnitXZ_CurrentLocation.Text = "현재위치[mm]";
            // 
            // btn_BreakUnitXZ_GrabSwitch3
            // 
            this.btn_BreakUnitXZ_GrabSwitch3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitXZ_GrabSwitch3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitXZ_GrabSwitch3.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitXZ_GrabSwitch3.Location = new System.Drawing.Point(1514, 6);
            this.btn_BreakUnitXZ_GrabSwitch3.Name = "btn_BreakUnitXZ_GrabSwitch3";
            this.btn_BreakUnitXZ_GrabSwitch3.Size = new System.Drawing.Size(172, 23);
            this.btn_BreakUnitXZ_GrabSwitch3.TabIndex = 35;
            this.btn_BreakUnitXZ_GrabSwitch3.Text = "Grab Switch 3";
            this.btn_BreakUnitXZ_GrabSwitch3.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitXZ_GrabSwitch2
            // 
            this.btn_BreakUnitXZ_GrabSwitch2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitXZ_GrabSwitch2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitXZ_GrabSwitch2.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitXZ_GrabSwitch2.Location = new System.Drawing.Point(1336, 6);
            this.btn_BreakUnitXZ_GrabSwitch2.Name = "btn_BreakUnitXZ_GrabSwitch2";
            this.btn_BreakUnitXZ_GrabSwitch2.Size = new System.Drawing.Size(172, 23);
            this.btn_BreakUnitXZ_GrabSwitch2.TabIndex = 34;
            this.btn_BreakUnitXZ_GrabSwitch2.Text = "Grab Switch 2";
            this.btn_BreakUnitXZ_GrabSwitch2.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitXZ_GrabSwitch1
            // 
            this.btn_BreakUnitXZ_GrabSwitch1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitXZ_GrabSwitch1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitXZ_GrabSwitch1.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitXZ_GrabSwitch1.Location = new System.Drawing.Point(1158, 6);
            this.btn_BreakUnitXZ_GrabSwitch1.Name = "btn_BreakUnitXZ_GrabSwitch1";
            this.btn_BreakUnitXZ_GrabSwitch1.Size = new System.Drawing.Size(172, 23);
            this.btn_BreakUnitXZ_GrabSwitch1.TabIndex = 33;
            this.btn_BreakUnitXZ_GrabSwitch1.Text = "Grab Switch 1";
            this.btn_BreakUnitXZ_GrabSwitch1.UseVisualStyleBackColor = false;
            // 
            // lv_BreakUnitXZ
            // 
            this.lv_BreakUnitXZ.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.col_BreakUnitXZ_NamebyLocation,
            this.col_BreakUnitXZ_Shaft,
            this.col_BreakUnitXZ_Location,
            this.col_BreakUnitXZ_Speed});
            this.lv_BreakUnitXZ.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lv_BreakUnitXZ.GridLines = true;
            this.lv_BreakUnitXZ.Location = new System.Drawing.Point(17, 16);
            this.lv_BreakUnitXZ.Name = "lv_BreakUnitXZ";
            this.lv_BreakUnitXZ.Size = new System.Drawing.Size(758, 734);
            this.lv_BreakUnitXZ.TabIndex = 32;
            this.lv_BreakUnitXZ.UseCompatibleStateImageBehavior = false;
            this.lv_BreakUnitXZ.View = System.Windows.Forms.View.Details;
            // 
            // col_BreakUnitXZ_NamebyLocation
            // 
            this.col_BreakUnitXZ_NamebyLocation.Text = "위치별 명칭";
            this.col_BreakUnitXZ_NamebyLocation.Width = 430;
            // 
            // col_BreakUnitXZ_Shaft
            // 
            this.col_BreakUnitXZ_Shaft.Text = "축";
            this.col_BreakUnitXZ_Shaft.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // col_BreakUnitXZ_Location
            // 
            this.col_BreakUnitXZ_Location.Text = "위치[mm]";
            this.col_BreakUnitXZ_Location.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_BreakUnitXZ_Location.Width = 125;
            // 
            // col_BreakUnitXZ_Speed
            // 
            this.col_BreakUnitXZ_Speed.Text = "속도[mm/s]";
            this.col_BreakUnitXZ_Speed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_BreakUnitXZ_Speed.Width = 125;
            // 
            // tp_BreakUnitTY
            // 
            this.tp_BreakUnitTY.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tp_BreakUnitTY.Controls.Add(this.gbox_BreakUnitTY_Shutter);
            this.tp_BreakUnitTY.Controls.Add(this.gbox_BreakUnitTY_DummyBox);
            this.tp_BreakUnitTY.Controls.Add(this.gbox_BreakUnitTY_Ionizer);
            this.tp_BreakUnitTY.Controls.Add(this.gbox_BreakUnitTY_Move);
            this.tp_BreakUnitTY.Controls.Add(this.gbox_BreakUnitTY_B);
            this.tp_BreakUnitTY.Controls.Add(this.gbox_BreakUnitTY_A);
            this.tp_BreakUnitTY.Controls.Add(this.btn_BreakUnitTY_Save);
            this.tp_BreakUnitTY.Controls.Add(this.btn_BreakUnitTY_SpeedSetting);
            this.tp_BreakUnitTY.Controls.Add(this.btn_BreakUnitTY_AllSetting);
            this.tp_BreakUnitTY.Controls.Add(this.btn_BreakUnitTY_LocationSetting);
            this.tp_BreakUnitTY.Controls.Add(this.btn_BreakUnitTY_MoveLocation);
            this.tp_BreakUnitTY.Controls.Add(this.tbox_BreakUnitTY_Location);
            this.tp_BreakUnitTY.Controls.Add(this.tbox_BreakUnitTY_Speed);
            this.tp_BreakUnitTY.Controls.Add(this.tbox_BreakUnitTY_CurrentLocation);
            this.tp_BreakUnitTY.Controls.Add(this.tbox_BreakUnitTY_SelectedShaft);
            this.tp_BreakUnitTY.Controls.Add(this.lbl_BreakUnitTY_Location);
            this.tp_BreakUnitTY.Controls.Add(this.lbl_BreakUnitTY_Speed);
            this.tp_BreakUnitTY.Controls.Add(this.lbl_BreakUnitTY_SelectedShaft);
            this.tp_BreakUnitTY.Controls.Add(this.tbox_BreakUnitTY_LocationSetting);
            this.tp_BreakUnitTY.Controls.Add(this.lbl_BreakUnitTY_CurrentLocation);
            this.tp_BreakUnitTY.Controls.Add(this.btn_BreakUnitTY_GrabSwitch3);
            this.tp_BreakUnitTY.Controls.Add(this.btn_BreakUnitTY_GrabSwitch2);
            this.tp_BreakUnitTY.Controls.Add(this.btn_BreakUnitTY_GrabSwitch1);
            this.tp_BreakUnitTY.Controls.Add(this.lv_BreakUnitTY);
            this.tp_BreakUnitTY.Controls.Add(this.ajin_Setting_BreakUnitTY);
            this.tp_BreakUnitTY.Location = new System.Drawing.Point(4, 34);
            this.tp_BreakUnitTY.Name = "tp_BreakUnitTY";
            this.tp_BreakUnitTY.Size = new System.Drawing.Size(1726, 816);
            this.tp_BreakUnitTY.TabIndex = 6;
            this.tp_BreakUnitTY.Text = "Break 유닛(T축 Y축)";
            // 
            // gbox_BreakUnitTY_Shutter
            // 
            this.gbox_BreakUnitTY_Shutter.Controls.Add(this.tableLayoutPanel53);
            this.gbox_BreakUnitTY_Shutter.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakUnitTY_Shutter.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitTY_Shutter.Location = new System.Drawing.Point(1484, 480);
            this.gbox_BreakUnitTY_Shutter.Name = "gbox_BreakUnitTY_Shutter";
            this.gbox_BreakUnitTY_Shutter.Size = new System.Drawing.Size(212, 109);
            this.gbox_BreakUnitTY_Shutter.TabIndex = 55;
            this.gbox_BreakUnitTY_Shutter.TabStop = false;
            this.gbox_BreakUnitTY_Shutter.Text = "     셔터    ";
            // 
            // tableLayoutPanel53
            // 
            this.tableLayoutPanel53.ColumnCount = 2;
            this.tableLayoutPanel53.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel53.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel53.Controls.Add(this.btn_BreakUnitTY_Shutter_Close, 0, 0);
            this.tableLayoutPanel53.Controls.Add(this.btn_BreakUnitTY_Shutter_Open, 0, 0);
            this.tableLayoutPanel53.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel53.Name = "tableLayoutPanel53";
            this.tableLayoutPanel53.RowCount = 1;
            this.tableLayoutPanel53.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel53.Size = new System.Drawing.Size(200, 75);
            this.tableLayoutPanel53.TabIndex = 57;
            // 
            // btn_BreakUnitTY_Shutter_Close
            // 
            this.btn_BreakUnitTY_Shutter_Close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_Shutter_Close.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_Shutter_Close.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_Shutter_Close.Location = new System.Drawing.Point(103, 3);
            this.btn_BreakUnitTY_Shutter_Close.Name = "btn_BreakUnitTY_Shutter_Close";
            this.btn_BreakUnitTY_Shutter_Close.Size = new System.Drawing.Size(94, 69);
            this.btn_BreakUnitTY_Shutter_Close.TabIndex = 58;
            this.btn_BreakUnitTY_Shutter_Close.Text = "더미 셔터\r\nClose";
            this.btn_BreakUnitTY_Shutter_Close.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_Shutter_Open
            // 
            this.btn_BreakUnitTY_Shutter_Open.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_Shutter_Open.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_Shutter_Open.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_Shutter_Open.Location = new System.Drawing.Point(3, 3);
            this.btn_BreakUnitTY_Shutter_Open.Name = "btn_BreakUnitTY_Shutter_Open";
            this.btn_BreakUnitTY_Shutter_Open.Size = new System.Drawing.Size(94, 69);
            this.btn_BreakUnitTY_Shutter_Open.TabIndex = 57;
            this.btn_BreakUnitTY_Shutter_Open.Text = "더미 셔터\r\nOpen";
            this.btn_BreakUnitTY_Shutter_Open.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitTY_DummyBox
            // 
            this.gbox_BreakUnitTY_DummyBox.Controls.Add(this.tableLayoutPanel52);
            this.gbox_BreakUnitTY_DummyBox.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakUnitTY_DummyBox.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitTY_DummyBox.Location = new System.Drawing.Point(1484, 410);
            this.gbox_BreakUnitTY_DummyBox.Name = "gbox_BreakUnitTY_DummyBox";
            this.gbox_BreakUnitTY_DummyBox.Size = new System.Drawing.Size(212, 64);
            this.gbox_BreakUnitTY_DummyBox.TabIndex = 31;
            this.gbox_BreakUnitTY_DummyBox.TabStop = false;
            this.gbox_BreakUnitTY_DummyBox.Text = "     더미 박스    ";
            // 
            // tableLayoutPanel52
            // 
            this.tableLayoutPanel52.ColumnCount = 2;
            this.tableLayoutPanel52.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel52.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel52.Controls.Add(this.btn_BreakUnitTY_DummyBox_Output, 0, 0);
            this.tableLayoutPanel52.Controls.Add(this.btn_BreakUnitTY_DummyBox_Input, 0, 0);
            this.tableLayoutPanel52.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel52.Name = "tableLayoutPanel52";
            this.tableLayoutPanel52.RowCount = 1;
            this.tableLayoutPanel52.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel52.Size = new System.Drawing.Size(200, 30);
            this.tableLayoutPanel52.TabIndex = 56;
            // 
            // btn_BreakUnitTY_DummyBox_Output
            // 
            this.btn_BreakUnitTY_DummyBox_Output.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_DummyBox_Output.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_BreakUnitTY_DummyBox_Output.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_DummyBox_Output.Location = new System.Drawing.Point(103, 3);
            this.btn_BreakUnitTY_DummyBox_Output.Name = "btn_BreakUnitTY_DummyBox_Output";
            this.btn_BreakUnitTY_DummyBox_Output.Size = new System.Drawing.Size(94, 24);
            this.btn_BreakUnitTY_DummyBox_Output.TabIndex = 58;
            this.btn_BreakUnitTY_DummyBox_Output.Text = "더미 배출";
            this.btn_BreakUnitTY_DummyBox_Output.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_DummyBox_Input
            // 
            this.btn_BreakUnitTY_DummyBox_Input.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_DummyBox_Input.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_BreakUnitTY_DummyBox_Input.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_DummyBox_Input.Location = new System.Drawing.Point(3, 3);
            this.btn_BreakUnitTY_DummyBox_Input.Name = "btn_BreakUnitTY_DummyBox_Input";
            this.btn_BreakUnitTY_DummyBox_Input.Size = new System.Drawing.Size(94, 24);
            this.btn_BreakUnitTY_DummyBox_Input.TabIndex = 57;
            this.btn_BreakUnitTY_DummyBox_Input.Text = "더미 투입";
            this.btn_BreakUnitTY_DummyBox_Input.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitTY_Ionizer
            // 
            this.gbox_BreakUnitTY_Ionizer.Controls.Add(this.btn_BreakUnitTY_DestructionOff);
            this.gbox_BreakUnitTY_Ionizer.Controls.Add(this.btn_BreakUnitTY_IonizerOff);
            this.gbox_BreakUnitTY_Ionizer.Controls.Add(this.btn_BreakUnitTY_DestructionOn);
            this.gbox_BreakUnitTY_Ionizer.Controls.Add(this.btn_BreakUnitTY_IonizerOn);
            this.gbox_BreakUnitTY_Ionizer.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakUnitTY_Ionizer.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitTY_Ionizer.Location = new System.Drawing.Point(1484, 223);
            this.gbox_BreakUnitTY_Ionizer.Name = "gbox_BreakUnitTY_Ionizer";
            this.gbox_BreakUnitTY_Ionizer.Size = new System.Drawing.Size(212, 181);
            this.gbox_BreakUnitTY_Ionizer.TabIndex = 54;
            this.gbox_BreakUnitTY_Ionizer.TabStop = false;
            this.gbox_BreakUnitTY_Ionizer.Text = "     이오나이저     ";
            // 
            // btn_BreakUnitTY_DestructionOff
            // 
            this.btn_BreakUnitTY_DestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_DestructionOff.Location = new System.Drawing.Point(107, 103);
            this.btn_BreakUnitTY_DestructionOff.Name = "btn_BreakUnitTY_DestructionOff";
            this.btn_BreakUnitTY_DestructionOff.Size = new System.Drawing.Size(99, 72);
            this.btn_BreakUnitTY_DestructionOff.TabIndex = 6;
            this.btn_BreakUnitTY_DestructionOff.Text = "파기 오프";
            this.btn_BreakUnitTY_DestructionOff.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_IonizerOff
            // 
            this.btn_BreakUnitTY_IonizerOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_IonizerOff.Location = new System.Drawing.Point(107, 28);
            this.btn_BreakUnitTY_IonizerOff.Name = "btn_BreakUnitTY_IonizerOff";
            this.btn_BreakUnitTY_IonizerOff.Size = new System.Drawing.Size(99, 72);
            this.btn_BreakUnitTY_IonizerOff.TabIndex = 5;
            this.btn_BreakUnitTY_IonizerOff.Text = "이오나이저\r\n오프";
            this.btn_BreakUnitTY_IonizerOff.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_DestructionOn
            // 
            this.btn_BreakUnitTY_DestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_DestructionOn.Location = new System.Drawing.Point(6, 103);
            this.btn_BreakUnitTY_DestructionOn.Name = "btn_BreakUnitTY_DestructionOn";
            this.btn_BreakUnitTY_DestructionOn.Size = new System.Drawing.Size(99, 72);
            this.btn_BreakUnitTY_DestructionOn.TabIndex = 4;
            this.btn_BreakUnitTY_DestructionOn.Text = "파기 온";
            this.btn_BreakUnitTY_DestructionOn.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_IonizerOn
            // 
            this.btn_BreakUnitTY_IonizerOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_IonizerOn.Location = new System.Drawing.Point(6, 28);
            this.btn_BreakUnitTY_IonizerOn.Name = "btn_BreakUnitTY_IonizerOn";
            this.btn_BreakUnitTY_IonizerOn.Size = new System.Drawing.Size(99, 72);
            this.btn_BreakUnitTY_IonizerOn.TabIndex = 3;
            this.btn_BreakUnitTY_IonizerOn.Text = "이오나이저\r\n온";
            this.btn_BreakUnitTY_IonizerOn.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitTY_Move
            // 
            this.gbox_BreakUnitTY_Move.Controls.Add(this.tableLayoutPanel54);
            this.gbox_BreakUnitTY_Move.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakUnitTY_Move.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitTY_Move.Location = new System.Drawing.Point(792, 595);
            this.gbox_BreakUnitTY_Move.Name = "gbox_BreakUnitTY_Move";
            this.gbox_BreakUnitTY_Move.Size = new System.Drawing.Size(904, 179);
            this.gbox_BreakUnitTY_Move.TabIndex = 53;
            this.gbox_BreakUnitTY_Move.TabStop = false;
            this.gbox_BreakUnitTY_Move.Text = "     이동     ";
            // 
            // tableLayoutPanel54
            // 
            this.tableLayoutPanel54.ColumnCount = 3;
            this.tableLayoutPanel54.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel54.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel54.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel54.Controls.Add(this.gbox_BreakUnitTY_Theta, 0, 0);
            this.tableLayoutPanel54.Controls.Add(this.gbox_BreakUnitTY_MoveB, 0, 0);
            this.tableLayoutPanel54.Controls.Add(this.gbox_BreakUnitTY_MoveA, 0, 0);
            this.tableLayoutPanel54.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel54.Name = "tableLayoutPanel54";
            this.tableLayoutPanel54.RowCount = 1;
            this.tableLayoutPanel54.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel54.Size = new System.Drawing.Size(892, 145);
            this.tableLayoutPanel54.TabIndex = 0;
            // 
            // gbox_BreakUnitTY_Theta
            // 
            this.gbox_BreakUnitTY_Theta.Controls.Add(this.tableLayoutPanel57);
            this.gbox_BreakUnitTY_Theta.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakUnitTY_Theta.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitTY_Theta.Location = new System.Drawing.Point(597, 3);
            this.gbox_BreakUnitTY_Theta.Name = "gbox_BreakUnitTY_Theta";
            this.gbox_BreakUnitTY_Theta.Size = new System.Drawing.Size(290, 139);
            this.gbox_BreakUnitTY_Theta.TabIndex = 54;
            this.gbox_BreakUnitTY_Theta.TabStop = false;
            this.gbox_BreakUnitTY_Theta.Text = "     세타    ";
            // 
            // tableLayoutPanel57
            // 
            this.tableLayoutPanel57.ColumnCount = 2;
            this.tableLayoutPanel57.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel57.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel57.Controls.Add(this.btn_BreakUnitTY_Theta_T2Wait, 0, 1);
            this.tableLayoutPanel57.Controls.Add(this.btn_BreakUnitTY_Theta_T4Wait, 0, 1);
            this.tableLayoutPanel57.Controls.Add(this.btn_BreakUnitTY_Theta_T1Wait, 0, 0);
            this.tableLayoutPanel57.Controls.Add(this.btn_BreakUnitTY_Theta_T3Wait, 1, 0);
            this.tableLayoutPanel57.Location = new System.Drawing.Point(3, 25);
            this.tableLayoutPanel57.Name = "tableLayoutPanel57";
            this.tableLayoutPanel57.RowCount = 2;
            this.tableLayoutPanel57.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel57.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel57.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel57.Size = new System.Drawing.Size(281, 108);
            this.tableLayoutPanel57.TabIndex = 0;
            // 
            // btn_BreakUnitTY_Theta_T2Wait
            // 
            this.btn_BreakUnitTY_Theta_T2Wait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_Theta_T2Wait.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_Theta_T2Wait.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_Theta_T2Wait.Location = new System.Drawing.Point(3, 57);
            this.btn_BreakUnitTY_Theta_T2Wait.Name = "btn_BreakUnitTY_Theta_T2Wait";
            this.btn_BreakUnitTY_Theta_T2Wait.Size = new System.Drawing.Size(134, 48);
            this.btn_BreakUnitTY_Theta_T2Wait.TabIndex = 65;
            this.btn_BreakUnitTY_Theta_T2Wait.Text = "T2 대기";
            this.btn_BreakUnitTY_Theta_T2Wait.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_Theta_T4Wait
            // 
            this.btn_BreakUnitTY_Theta_T4Wait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_Theta_T4Wait.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_Theta_T4Wait.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_Theta_T4Wait.Location = new System.Drawing.Point(143, 57);
            this.btn_BreakUnitTY_Theta_T4Wait.Name = "btn_BreakUnitTY_Theta_T4Wait";
            this.btn_BreakUnitTY_Theta_T4Wait.Size = new System.Drawing.Size(134, 48);
            this.btn_BreakUnitTY_Theta_T4Wait.TabIndex = 64;
            this.btn_BreakUnitTY_Theta_T4Wait.Text = "T4 대기";
            this.btn_BreakUnitTY_Theta_T4Wait.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_Theta_T1Wait
            // 
            this.btn_BreakUnitTY_Theta_T1Wait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_Theta_T1Wait.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_Theta_T1Wait.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_Theta_T1Wait.Location = new System.Drawing.Point(3, 3);
            this.btn_BreakUnitTY_Theta_T1Wait.Name = "btn_BreakUnitTY_Theta_T1Wait";
            this.btn_BreakUnitTY_Theta_T1Wait.Size = new System.Drawing.Size(134, 48);
            this.btn_BreakUnitTY_Theta_T1Wait.TabIndex = 62;
            this.btn_BreakUnitTY_Theta_T1Wait.Text = "T1 대기";
            this.btn_BreakUnitTY_Theta_T1Wait.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_Theta_T3Wait
            // 
            this.btn_BreakUnitTY_Theta_T3Wait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_Theta_T3Wait.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_Theta_T3Wait.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_Theta_T3Wait.Location = new System.Drawing.Point(143, 3);
            this.btn_BreakUnitTY_Theta_T3Wait.Name = "btn_BreakUnitTY_Theta_T3Wait";
            this.btn_BreakUnitTY_Theta_T3Wait.Size = new System.Drawing.Size(134, 48);
            this.btn_BreakUnitTY_Theta_T3Wait.TabIndex = 63;
            this.btn_BreakUnitTY_Theta_T3Wait.Text = "T3 대기";
            this.btn_BreakUnitTY_Theta_T3Wait.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitTY_MoveB
            // 
            this.gbox_BreakUnitTY_MoveB.Controls.Add(this.tableLayoutPanel56);
            this.gbox_BreakUnitTY_MoveB.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakUnitTY_MoveB.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitTY_MoveB.Location = new System.Drawing.Point(300, 3);
            this.gbox_BreakUnitTY_MoveB.Name = "gbox_BreakUnitTY_MoveB";
            this.gbox_BreakUnitTY_MoveB.Size = new System.Drawing.Size(290, 139);
            this.gbox_BreakUnitTY_MoveB.TabIndex = 53;
            this.gbox_BreakUnitTY_MoveB.TabStop = false;
            this.gbox_BreakUnitTY_MoveB.Text = "     B    ";
            // 
            // tableLayoutPanel56
            // 
            this.tableLayoutPanel56.ColumnCount = 2;
            this.tableLayoutPanel56.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel56.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel56.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel56.Controls.Add(this.btn_BreakUnitTY_MoveB_Unload, 0, 0);
            this.tableLayoutPanel56.Controls.Add(this.btn_BreakUnitTY_MoveB_Load, 0, 0);
            this.tableLayoutPanel56.Location = new System.Drawing.Point(3, 25);
            this.tableLayoutPanel56.Name = "tableLayoutPanel56";
            this.tableLayoutPanel56.RowCount = 1;
            this.tableLayoutPanel56.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel56.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 108F));
            this.tableLayoutPanel56.Size = new System.Drawing.Size(281, 108);
            this.tableLayoutPanel56.TabIndex = 0;
            // 
            // btn_BreakUnitTY_MoveB_Unload
            // 
            this.btn_BreakUnitTY_MoveB_Unload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_MoveB_Unload.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_MoveB_Unload.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_MoveB_Unload.Location = new System.Drawing.Point(143, 3);
            this.btn_BreakUnitTY_MoveB_Unload.Name = "btn_BreakUnitTY_MoveB_Unload";
            this.btn_BreakUnitTY_MoveB_Unload.Size = new System.Drawing.Size(134, 102);
            this.btn_BreakUnitTY_MoveB_Unload.TabIndex = 63;
            this.btn_BreakUnitTY_MoveB_Unload.Text = "언로드";
            this.btn_BreakUnitTY_MoveB_Unload.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_MoveB_Load
            // 
            this.btn_BreakUnitTY_MoveB_Load.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_MoveB_Load.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_MoveB_Load.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_MoveB_Load.Location = new System.Drawing.Point(3, 3);
            this.btn_BreakUnitTY_MoveB_Load.Name = "btn_BreakUnitTY_MoveB_Load";
            this.btn_BreakUnitTY_MoveB_Load.Size = new System.Drawing.Size(134, 102);
            this.btn_BreakUnitTY_MoveB_Load.TabIndex = 62;
            this.btn_BreakUnitTY_MoveB_Load.Text = "로드";
            this.btn_BreakUnitTY_MoveB_Load.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitTY_MoveA
            // 
            this.gbox_BreakUnitTY_MoveA.Controls.Add(this.tableLayoutPanel55);
            this.gbox_BreakUnitTY_MoveA.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakUnitTY_MoveA.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitTY_MoveA.Location = new System.Drawing.Point(3, 3);
            this.gbox_BreakUnitTY_MoveA.Name = "gbox_BreakUnitTY_MoveA";
            this.gbox_BreakUnitTY_MoveA.Size = new System.Drawing.Size(290, 139);
            this.gbox_BreakUnitTY_MoveA.TabIndex = 52;
            this.gbox_BreakUnitTY_MoveA.TabStop = false;
            this.gbox_BreakUnitTY_MoveA.Text = "     A    ";
            // 
            // tableLayoutPanel55
            // 
            this.tableLayoutPanel55.ColumnCount = 2;
            this.tableLayoutPanel55.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel55.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel55.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel55.Controls.Add(this.btn_BreakUnitTY_MoveA_Unload, 0, 0);
            this.tableLayoutPanel55.Controls.Add(this.btn_BreakUnitTY_MoveA_Load, 0, 0);
            this.tableLayoutPanel55.Location = new System.Drawing.Point(3, 25);
            this.tableLayoutPanel55.Name = "tableLayoutPanel55";
            this.tableLayoutPanel55.RowCount = 1;
            this.tableLayoutPanel55.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel55.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 108F));
            this.tableLayoutPanel55.Size = new System.Drawing.Size(281, 108);
            this.tableLayoutPanel55.TabIndex = 0;
            // 
            // btn_BreakUnitTY_MoveA_Unload
            // 
            this.btn_BreakUnitTY_MoveA_Unload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_MoveA_Unload.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_MoveA_Unload.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_MoveA_Unload.Location = new System.Drawing.Point(143, 3);
            this.btn_BreakUnitTY_MoveA_Unload.Name = "btn_BreakUnitTY_MoveA_Unload";
            this.btn_BreakUnitTY_MoveA_Unload.Size = new System.Drawing.Size(134, 102);
            this.btn_BreakUnitTY_MoveA_Unload.TabIndex = 63;
            this.btn_BreakUnitTY_MoveA_Unload.Text = "언로드";
            this.btn_BreakUnitTY_MoveA_Unload.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_MoveA_Load
            // 
            this.btn_BreakUnitTY_MoveA_Load.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_MoveA_Load.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_MoveA_Load.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_MoveA_Load.Location = new System.Drawing.Point(3, 3);
            this.btn_BreakUnitTY_MoveA_Load.Name = "btn_BreakUnitTY_MoveA_Load";
            this.btn_BreakUnitTY_MoveA_Load.Size = new System.Drawing.Size(134, 102);
            this.btn_BreakUnitTY_MoveA_Load.TabIndex = 62;
            this.btn_BreakUnitTY_MoveA_Load.Text = "로드";
            this.btn_BreakUnitTY_MoveA_Load.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitTY_B
            // 
            this.gbox_BreakUnitTY_B.Controls.Add(this.tableLayoutPanel50);
            this.gbox_BreakUnitTY_B.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakUnitTY_B.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitTY_B.Location = new System.Drawing.Point(1138, 410);
            this.gbox_BreakUnitTY_B.Name = "gbox_BreakUnitTY_B";
            this.gbox_BreakUnitTY_B.Size = new System.Drawing.Size(340, 179);
            this.gbox_BreakUnitTY_B.TabIndex = 52;
            this.gbox_BreakUnitTY_B.TabStop = false;
            this.gbox_BreakUnitTY_B.Text = "     B     ";
            // 
            // tableLayoutPanel50
            // 
            this.tableLayoutPanel50.ColumnCount = 4;
            this.tableLayoutPanel50.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel50.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel50.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel50.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel50.Controls.Add(this.btn_BreakUnitTY_B_1Destruction_On, 0, 1);
            this.tableLayoutPanel50.Controls.Add(this.btn_BreakUnitTY_B_1Destruction_Off, 0, 1);
            this.tableLayoutPanel50.Controls.Add(this.btn_BreakUnitTY_B_2Destruction_On, 0, 1);
            this.tableLayoutPanel50.Controls.Add(this.btn_BreakUnitTY_B_2Destruction_Off, 0, 1);
            this.tableLayoutPanel50.Controls.Add(this.btn_BreakUnitTY_B_1Pneumatic_Off, 1, 0);
            this.tableLayoutPanel50.Controls.Add(this.btn_BreakUnitTY_B_1Pneumatic_On, 0, 0);
            this.tableLayoutPanel50.Controls.Add(this.btn_BreakUnitTY_B_2Pneumatic_On, 2, 0);
            this.tableLayoutPanel50.Controls.Add(this.btn_BreakUnitTY_B_2Pneumatic_Off, 3, 0);
            this.tableLayoutPanel50.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel50.Name = "tableLayoutPanel50";
            this.tableLayoutPanel50.RowCount = 2;
            this.tableLayoutPanel50.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel50.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel50.Size = new System.Drawing.Size(328, 145);
            this.tableLayoutPanel50.TabIndex = 56;
            // 
            // btn_BreakUnitTY_B_1Destruction_On
            // 
            this.btn_BreakUnitTY_B_1Destruction_On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_B_1Destruction_On.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_B_1Destruction_On.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_B_1Destruction_On.Location = new System.Drawing.Point(3, 75);
            this.btn_BreakUnitTY_B_1Destruction_On.Name = "btn_BreakUnitTY_B_1Destruction_On";
            this.btn_BreakUnitTY_B_1Destruction_On.Size = new System.Drawing.Size(76, 66);
            this.btn_BreakUnitTY_B_1Destruction_On.TabIndex = 62;
            this.btn_BreakUnitTY_B_1Destruction_On.Text = "1 파기\r\n온";
            this.btn_BreakUnitTY_B_1Destruction_On.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_B_1Destruction_Off
            // 
            this.btn_BreakUnitTY_B_1Destruction_Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_B_1Destruction_Off.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_B_1Destruction_Off.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_B_1Destruction_Off.Location = new System.Drawing.Point(85, 75);
            this.btn_BreakUnitTY_B_1Destruction_Off.Name = "btn_BreakUnitTY_B_1Destruction_Off";
            this.btn_BreakUnitTY_B_1Destruction_Off.Size = new System.Drawing.Size(76, 66);
            this.btn_BreakUnitTY_B_1Destruction_Off.TabIndex = 61;
            this.btn_BreakUnitTY_B_1Destruction_Off.Text = "1 파기\r\n오프";
            this.btn_BreakUnitTY_B_1Destruction_Off.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_B_2Destruction_On
            // 
            this.btn_BreakUnitTY_B_2Destruction_On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_B_2Destruction_On.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_B_2Destruction_On.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_B_2Destruction_On.Location = new System.Drawing.Point(167, 75);
            this.btn_BreakUnitTY_B_2Destruction_On.Name = "btn_BreakUnitTY_B_2Destruction_On";
            this.btn_BreakUnitTY_B_2Destruction_On.Size = new System.Drawing.Size(76, 66);
            this.btn_BreakUnitTY_B_2Destruction_On.TabIndex = 60;
            this.btn_BreakUnitTY_B_2Destruction_On.Text = "2 파기\r\n온";
            this.btn_BreakUnitTY_B_2Destruction_On.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_B_2Destruction_Off
            // 
            this.btn_BreakUnitTY_B_2Destruction_Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_B_2Destruction_Off.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_B_2Destruction_Off.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_B_2Destruction_Off.Location = new System.Drawing.Point(249, 75);
            this.btn_BreakUnitTY_B_2Destruction_Off.Name = "btn_BreakUnitTY_B_2Destruction_Off";
            this.btn_BreakUnitTY_B_2Destruction_Off.Size = new System.Drawing.Size(76, 66);
            this.btn_BreakUnitTY_B_2Destruction_Off.TabIndex = 58;
            this.btn_BreakUnitTY_B_2Destruction_Off.Text = "2 파기\r\n오프";
            this.btn_BreakUnitTY_B_2Destruction_Off.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_B_1Pneumatic_Off
            // 
            this.btn_BreakUnitTY_B_1Pneumatic_Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_B_1Pneumatic_Off.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_B_1Pneumatic_Off.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_B_1Pneumatic_Off.Location = new System.Drawing.Point(85, 3);
            this.btn_BreakUnitTY_B_1Pneumatic_Off.Name = "btn_BreakUnitTY_B_1Pneumatic_Off";
            this.btn_BreakUnitTY_B_1Pneumatic_Off.Size = new System.Drawing.Size(76, 66);
            this.btn_BreakUnitTY_B_1Pneumatic_Off.TabIndex = 55;
            this.btn_BreakUnitTY_B_1Pneumatic_Off.Text = "1 공압\r\n오프";
            this.btn_BreakUnitTY_B_1Pneumatic_Off.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_B_1Pneumatic_On
            // 
            this.btn_BreakUnitTY_B_1Pneumatic_On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_B_1Pneumatic_On.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_B_1Pneumatic_On.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_B_1Pneumatic_On.Location = new System.Drawing.Point(3, 3);
            this.btn_BreakUnitTY_B_1Pneumatic_On.Name = "btn_BreakUnitTY_B_1Pneumatic_On";
            this.btn_BreakUnitTY_B_1Pneumatic_On.Size = new System.Drawing.Size(76, 66);
            this.btn_BreakUnitTY_B_1Pneumatic_On.TabIndex = 57;
            this.btn_BreakUnitTY_B_1Pneumatic_On.Text = "1 공압\r\n온";
            this.btn_BreakUnitTY_B_1Pneumatic_On.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_B_2Pneumatic_On
            // 
            this.btn_BreakUnitTY_B_2Pneumatic_On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_B_2Pneumatic_On.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_B_2Pneumatic_On.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_B_2Pneumatic_On.Location = new System.Drawing.Point(167, 3);
            this.btn_BreakUnitTY_B_2Pneumatic_On.Name = "btn_BreakUnitTY_B_2Pneumatic_On";
            this.btn_BreakUnitTY_B_2Pneumatic_On.Size = new System.Drawing.Size(76, 66);
            this.btn_BreakUnitTY_B_2Pneumatic_On.TabIndex = 59;
            this.btn_BreakUnitTY_B_2Pneumatic_On.Text = "2 공압\r\n온";
            this.btn_BreakUnitTY_B_2Pneumatic_On.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_B_2Pneumatic_Off
            // 
            this.btn_BreakUnitTY_B_2Pneumatic_Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_B_2Pneumatic_Off.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_B_2Pneumatic_Off.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_B_2Pneumatic_Off.Location = new System.Drawing.Point(249, 3);
            this.btn_BreakUnitTY_B_2Pneumatic_Off.Name = "btn_BreakUnitTY_B_2Pneumatic_Off";
            this.btn_BreakUnitTY_B_2Pneumatic_Off.Size = new System.Drawing.Size(76, 66);
            this.btn_BreakUnitTY_B_2Pneumatic_Off.TabIndex = 56;
            this.btn_BreakUnitTY_B_2Pneumatic_Off.Text = "2 공압\r\n오프";
            this.btn_BreakUnitTY_B_2Pneumatic_Off.UseVisualStyleBackColor = false;
            // 
            // gbox_BreakUnitTY_A
            // 
            this.gbox_BreakUnitTY_A.Controls.Add(this.tableLayoutPanel51);
            this.gbox_BreakUnitTY_A.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_BreakUnitTY_A.ForeColor = System.Drawing.Color.White;
            this.gbox_BreakUnitTY_A.Location = new System.Drawing.Point(792, 410);
            this.gbox_BreakUnitTY_A.Name = "gbox_BreakUnitTY_A";
            this.gbox_BreakUnitTY_A.Size = new System.Drawing.Size(340, 179);
            this.gbox_BreakUnitTY_A.TabIndex = 51;
            this.gbox_BreakUnitTY_A.TabStop = false;
            this.gbox_BreakUnitTY_A.Text = "     A    ";
            // 
            // tableLayoutPanel51
            // 
            this.tableLayoutPanel51.ColumnCount = 4;
            this.tableLayoutPanel51.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel51.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel51.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel51.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel51.Controls.Add(this.btn_BreakUnitTY_A_1Destruction_On, 0, 1);
            this.tableLayoutPanel51.Controls.Add(this.btn_BreakUnitTY_A_1Destruction_Off, 0, 1);
            this.tableLayoutPanel51.Controls.Add(this.btn_BreakUnitTY_A_2Destruction_On, 0, 1);
            this.tableLayoutPanel51.Controls.Add(this.btn_BreakUnitTY_A_2Destruction_Off, 0, 1);
            this.tableLayoutPanel51.Controls.Add(this.btn_BreakUnitTY_A_1Pneumatic_Off, 1, 0);
            this.tableLayoutPanel51.Controls.Add(this.btn_BreakUnitTY_A_1Pneumatic_On, 0, 0);
            this.tableLayoutPanel51.Controls.Add(this.btn_BreakUnitTY_A_2Pneumatic_On, 2, 0);
            this.tableLayoutPanel51.Controls.Add(this.btn_BreakUnitTY_A_2Pneumatic_Off, 3, 0);
            this.tableLayoutPanel51.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel51.Name = "tableLayoutPanel51";
            this.tableLayoutPanel51.RowCount = 2;
            this.tableLayoutPanel51.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel51.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel51.Size = new System.Drawing.Size(328, 145);
            this.tableLayoutPanel51.TabIndex = 55;
            // 
            // btn_BreakUnitTY_A_1Destruction_On
            // 
            this.btn_BreakUnitTY_A_1Destruction_On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_A_1Destruction_On.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_A_1Destruction_On.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_A_1Destruction_On.Location = new System.Drawing.Point(3, 75);
            this.btn_BreakUnitTY_A_1Destruction_On.Name = "btn_BreakUnitTY_A_1Destruction_On";
            this.btn_BreakUnitTY_A_1Destruction_On.Size = new System.Drawing.Size(76, 66);
            this.btn_BreakUnitTY_A_1Destruction_On.TabIndex = 62;
            this.btn_BreakUnitTY_A_1Destruction_On.Text = "1 파기\r\n온";
            this.btn_BreakUnitTY_A_1Destruction_On.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_A_1Destruction_Off
            // 
            this.btn_BreakUnitTY_A_1Destruction_Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_A_1Destruction_Off.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_A_1Destruction_Off.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_A_1Destruction_Off.Location = new System.Drawing.Point(85, 75);
            this.btn_BreakUnitTY_A_1Destruction_Off.Name = "btn_BreakUnitTY_A_1Destruction_Off";
            this.btn_BreakUnitTY_A_1Destruction_Off.Size = new System.Drawing.Size(76, 66);
            this.btn_BreakUnitTY_A_1Destruction_Off.TabIndex = 61;
            this.btn_BreakUnitTY_A_1Destruction_Off.Text = "1 파기\r\n오프";
            this.btn_BreakUnitTY_A_1Destruction_Off.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_A_2Destruction_On
            // 
            this.btn_BreakUnitTY_A_2Destruction_On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_A_2Destruction_On.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_A_2Destruction_On.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_A_2Destruction_On.Location = new System.Drawing.Point(167, 75);
            this.btn_BreakUnitTY_A_2Destruction_On.Name = "btn_BreakUnitTY_A_2Destruction_On";
            this.btn_BreakUnitTY_A_2Destruction_On.Size = new System.Drawing.Size(76, 66);
            this.btn_BreakUnitTY_A_2Destruction_On.TabIndex = 60;
            this.btn_BreakUnitTY_A_2Destruction_On.Text = "2 파기\r\n온";
            this.btn_BreakUnitTY_A_2Destruction_On.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_A_2Destruction_Off
            // 
            this.btn_BreakUnitTY_A_2Destruction_Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_A_2Destruction_Off.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_A_2Destruction_Off.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_A_2Destruction_Off.Location = new System.Drawing.Point(249, 75);
            this.btn_BreakUnitTY_A_2Destruction_Off.Name = "btn_BreakUnitTY_A_2Destruction_Off";
            this.btn_BreakUnitTY_A_2Destruction_Off.Size = new System.Drawing.Size(76, 66);
            this.btn_BreakUnitTY_A_2Destruction_Off.TabIndex = 58;
            this.btn_BreakUnitTY_A_2Destruction_Off.Text = "2 파기\r\n오프";
            this.btn_BreakUnitTY_A_2Destruction_Off.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_A_1Pneumatic_Off
            // 
            this.btn_BreakUnitTY_A_1Pneumatic_Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_A_1Pneumatic_Off.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_A_1Pneumatic_Off.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_A_1Pneumatic_Off.Location = new System.Drawing.Point(85, 3);
            this.btn_BreakUnitTY_A_1Pneumatic_Off.Name = "btn_BreakUnitTY_A_1Pneumatic_Off";
            this.btn_BreakUnitTY_A_1Pneumatic_Off.Size = new System.Drawing.Size(76, 66);
            this.btn_BreakUnitTY_A_1Pneumatic_Off.TabIndex = 55;
            this.btn_BreakUnitTY_A_1Pneumatic_Off.Text = "1 공압\r\n오프";
            this.btn_BreakUnitTY_A_1Pneumatic_Off.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_A_1Pneumatic_On
            // 
            this.btn_BreakUnitTY_A_1Pneumatic_On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_A_1Pneumatic_On.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_A_1Pneumatic_On.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_A_1Pneumatic_On.Location = new System.Drawing.Point(3, 3);
            this.btn_BreakUnitTY_A_1Pneumatic_On.Name = "btn_BreakUnitTY_A_1Pneumatic_On";
            this.btn_BreakUnitTY_A_1Pneumatic_On.Size = new System.Drawing.Size(76, 66);
            this.btn_BreakUnitTY_A_1Pneumatic_On.TabIndex = 57;
            this.btn_BreakUnitTY_A_1Pneumatic_On.Text = "1 공압\r\n온";
            this.btn_BreakUnitTY_A_1Pneumatic_On.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_A_2Pneumatic_On
            // 
            this.btn_BreakUnitTY_A_2Pneumatic_On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_A_2Pneumatic_On.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_A_2Pneumatic_On.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_A_2Pneumatic_On.Location = new System.Drawing.Point(167, 3);
            this.btn_BreakUnitTY_A_2Pneumatic_On.Name = "btn_BreakUnitTY_A_2Pneumatic_On";
            this.btn_BreakUnitTY_A_2Pneumatic_On.Size = new System.Drawing.Size(76, 66);
            this.btn_BreakUnitTY_A_2Pneumatic_On.TabIndex = 59;
            this.btn_BreakUnitTY_A_2Pneumatic_On.Text = "2 공압\r\n온";
            this.btn_BreakUnitTY_A_2Pneumatic_On.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_A_2Pneumatic_Off
            // 
            this.btn_BreakUnitTY_A_2Pneumatic_Off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_A_2Pneumatic_Off.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_A_2Pneumatic_Off.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_A_2Pneumatic_Off.Location = new System.Drawing.Point(249, 3);
            this.btn_BreakUnitTY_A_2Pneumatic_Off.Name = "btn_BreakUnitTY_A_2Pneumatic_Off";
            this.btn_BreakUnitTY_A_2Pneumatic_Off.Size = new System.Drawing.Size(76, 66);
            this.btn_BreakUnitTY_A_2Pneumatic_Off.TabIndex = 56;
            this.btn_BreakUnitTY_A_2Pneumatic_Off.Text = "2 공압\r\n오프";
            this.btn_BreakUnitTY_A_2Pneumatic_Off.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_Save
            // 
            this.btn_BreakUnitTY_Save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_Save.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_Save.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_Save.Location = new System.Drawing.Point(1482, 783);
            this.btn_BreakUnitTY_Save.Name = "btn_BreakUnitTY_Save";
            this.btn_BreakUnitTY_Save.Size = new System.Drawing.Size(228, 28);
            this.btn_BreakUnitTY_Save.TabIndex = 50;
            this.btn_BreakUnitTY_Save.Text = "저장";
            this.btn_BreakUnitTY_Save.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_SpeedSetting
            // 
            this.btn_BreakUnitTY_SpeedSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_SpeedSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_BreakUnitTY_SpeedSetting.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_SpeedSetting.Location = new System.Drawing.Point(1291, 315);
            this.btn_BreakUnitTY_SpeedSetting.Name = "btn_BreakUnitTY_SpeedSetting";
            this.btn_BreakUnitTY_SpeedSetting.Size = new System.Drawing.Size(90, 27);
            this.btn_BreakUnitTY_SpeedSetting.TabIndex = 48;
            this.btn_BreakUnitTY_SpeedSetting.Text = "속도 설정";
            this.btn_BreakUnitTY_SpeedSetting.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_AllSetting
            // 
            this.btn_BreakUnitTY_AllSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_AllSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_BreakUnitTY_AllSetting.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_AllSetting.Location = new System.Drawing.Point(1387, 273);
            this.btn_BreakUnitTY_AllSetting.Name = "btn_BreakUnitTY_AllSetting";
            this.btn_BreakUnitTY_AllSetting.Size = new System.Drawing.Size(90, 27);
            this.btn_BreakUnitTY_AllSetting.TabIndex = 47;
            this.btn_BreakUnitTY_AllSetting.Text = "전체 설정";
            this.btn_BreakUnitTY_AllSetting.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_LocationSetting
            // 
            this.btn_BreakUnitTY_LocationSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_LocationSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_BreakUnitTY_LocationSetting.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_LocationSetting.Location = new System.Drawing.Point(1071, 315);
            this.btn_BreakUnitTY_LocationSetting.Name = "btn_BreakUnitTY_LocationSetting";
            this.btn_BreakUnitTY_LocationSetting.Size = new System.Drawing.Size(90, 27);
            this.btn_BreakUnitTY_LocationSetting.TabIndex = 46;
            this.btn_BreakUnitTY_LocationSetting.Text = "위치 설정";
            this.btn_BreakUnitTY_LocationSetting.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_MoveLocation
            // 
            this.btn_BreakUnitTY_MoveLocation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_MoveLocation.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_BreakUnitTY_MoveLocation.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_MoveLocation.Location = new System.Drawing.Point(975, 315);
            this.btn_BreakUnitTY_MoveLocation.Name = "btn_BreakUnitTY_MoveLocation";
            this.btn_BreakUnitTY_MoveLocation.Size = new System.Drawing.Size(90, 27);
            this.btn_BreakUnitTY_MoveLocation.TabIndex = 45;
            this.btn_BreakUnitTY_MoveLocation.Text = "위치 이동";
            this.btn_BreakUnitTY_MoveLocation.UseVisualStyleBackColor = false;
            // 
            // tbox_BreakUnitTY_Location
            // 
            this.tbox_BreakUnitTY_Location.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_BreakUnitTY_Location.Location = new System.Drawing.Point(879, 316);
            this.tbox_BreakUnitTY_Location.Name = "tbox_BreakUnitTY_Location";
            this.tbox_BreakUnitTY_Location.Size = new System.Drawing.Size(90, 27);
            this.tbox_BreakUnitTY_Location.TabIndex = 44;
            // 
            // tbox_BreakUnitTY_Speed
            // 
            this.tbox_BreakUnitTY_Speed.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_BreakUnitTY_Speed.Location = new System.Drawing.Point(1291, 273);
            this.tbox_BreakUnitTY_Speed.Name = "tbox_BreakUnitTY_Speed";
            this.tbox_BreakUnitTY_Speed.Size = new System.Drawing.Size(90, 27);
            this.tbox_BreakUnitTY_Speed.TabIndex = 43;
            // 
            // tbox_BreakUnitTY_CurrentLocation
            // 
            this.tbox_BreakUnitTY_CurrentLocation.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_BreakUnitTY_CurrentLocation.Location = new System.Drawing.Point(1084, 274);
            this.tbox_BreakUnitTY_CurrentLocation.Name = "tbox_BreakUnitTY_CurrentLocation";
            this.tbox_BreakUnitTY_CurrentLocation.Size = new System.Drawing.Size(90, 27);
            this.tbox_BreakUnitTY_CurrentLocation.TabIndex = 42;
            // 
            // tbox_BreakUnitTY_SelectedShaft
            // 
            this.tbox_BreakUnitTY_SelectedShaft.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_BreakUnitTY_SelectedShaft.Location = new System.Drawing.Point(879, 273);
            this.tbox_BreakUnitTY_SelectedShaft.Name = "tbox_BreakUnitTY_SelectedShaft";
            this.tbox_BreakUnitTY_SelectedShaft.Size = new System.Drawing.Size(90, 27);
            this.tbox_BreakUnitTY_SelectedShaft.TabIndex = 41;
            // 
            // lbl_BreakUnitTY_Location
            // 
            this.lbl_BreakUnitTY_Location.AutoSize = true;
            this.lbl_BreakUnitTY_Location.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_BreakUnitTY_Location.ForeColor = System.Drawing.Color.White;
            this.lbl_BreakUnitTY_Location.Location = new System.Drawing.Point(801, 316);
            this.lbl_BreakUnitTY_Location.Name = "lbl_BreakUnitTY_Location";
            this.lbl_BreakUnitTY_Location.Size = new System.Drawing.Size(84, 21);
            this.lbl_BreakUnitTY_Location.TabIndex = 40;
            this.lbl_BreakUnitTY_Location.Text = "위치[mm]";
            // 
            // lbl_BreakUnitTY_Speed
            // 
            this.lbl_BreakUnitTY_Speed.AutoSize = true;
            this.lbl_BreakUnitTY_Speed.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_BreakUnitTY_Speed.ForeColor = System.Drawing.Color.White;
            this.lbl_BreakUnitTY_Speed.Location = new System.Drawing.Point(1179, 275);
            this.lbl_BreakUnitTY_Speed.Name = "lbl_BreakUnitTY_Speed";
            this.lbl_BreakUnitTY_Speed.Size = new System.Drawing.Size(115, 21);
            this.lbl_BreakUnitTY_Speed.TabIndex = 39;
            this.lbl_BreakUnitTY_Speed.Text = "속도[mm/sec]";
            // 
            // lbl_BreakUnitTY_SelectedShaft
            // 
            this.lbl_BreakUnitTY_SelectedShaft.AutoSize = true;
            this.lbl_BreakUnitTY_SelectedShaft.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_BreakUnitTY_SelectedShaft.ForeColor = System.Drawing.Color.White;
            this.lbl_BreakUnitTY_SelectedShaft.Location = new System.Drawing.Point(805, 274);
            this.lbl_BreakUnitTY_SelectedShaft.Name = "lbl_BreakUnitTY_SelectedShaft";
            this.lbl_BreakUnitTY_SelectedShaft.Size = new System.Drawing.Size(80, 21);
            this.lbl_BreakUnitTY_SelectedShaft.TabIndex = 38;
            this.lbl_BreakUnitTY_SelectedShaft.Text = "선택된 축";
            // 
            // tbox_BreakUnitTY_LocationSetting
            // 
            this.tbox_BreakUnitTY_LocationSetting.BackColor = System.Drawing.SystemColors.Highlight;
            this.tbox_BreakUnitTY_LocationSetting.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.tbox_BreakUnitTY_LocationSetting.ForeColor = System.Drawing.Color.White;
            this.tbox_BreakUnitTY_LocationSetting.Location = new System.Drawing.Point(792, 223);
            this.tbox_BreakUnitTY_LocationSetting.Name = "tbox_BreakUnitTY_LocationSetting";
            this.tbox_BreakUnitTY_LocationSetting.ReadOnly = true;
            this.tbox_BreakUnitTY_LocationSetting.Size = new System.Drawing.Size(89, 27);
            this.tbox_BreakUnitTY_LocationSetting.TabIndex = 37;
            this.tbox_BreakUnitTY_LocationSetting.Text = "위치값 설정";
            this.tbox_BreakUnitTY_LocationSetting.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbl_BreakUnitTY_CurrentLocation
            // 
            this.lbl_BreakUnitTY_CurrentLocation.AutoSize = true;
            this.lbl_BreakUnitTY_CurrentLocation.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_BreakUnitTY_CurrentLocation.ForeColor = System.Drawing.Color.White;
            this.lbl_BreakUnitTY_CurrentLocation.Location = new System.Drawing.Point(975, 274);
            this.lbl_BreakUnitTY_CurrentLocation.Name = "lbl_BreakUnitTY_CurrentLocation";
            this.lbl_BreakUnitTY_CurrentLocation.Size = new System.Drawing.Size(116, 21);
            this.lbl_BreakUnitTY_CurrentLocation.TabIndex = 36;
            this.lbl_BreakUnitTY_CurrentLocation.Text = "현재위치[mm]";
            // 
            // btn_BreakUnitTY_GrabSwitch3
            // 
            this.btn_BreakUnitTY_GrabSwitch3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_GrabSwitch3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_GrabSwitch3.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_GrabSwitch3.Location = new System.Drawing.Point(1514, 6);
            this.btn_BreakUnitTY_GrabSwitch3.Name = "btn_BreakUnitTY_GrabSwitch3";
            this.btn_BreakUnitTY_GrabSwitch3.Size = new System.Drawing.Size(172, 23);
            this.btn_BreakUnitTY_GrabSwitch3.TabIndex = 35;
            this.btn_BreakUnitTY_GrabSwitch3.Text = "Grab Switch 3";
            this.btn_BreakUnitTY_GrabSwitch3.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_GrabSwitch2
            // 
            this.btn_BreakUnitTY_GrabSwitch2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_GrabSwitch2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_GrabSwitch2.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_GrabSwitch2.Location = new System.Drawing.Point(1336, 6);
            this.btn_BreakUnitTY_GrabSwitch2.Name = "btn_BreakUnitTY_GrabSwitch2";
            this.btn_BreakUnitTY_GrabSwitch2.Size = new System.Drawing.Size(172, 23);
            this.btn_BreakUnitTY_GrabSwitch2.TabIndex = 34;
            this.btn_BreakUnitTY_GrabSwitch2.Text = "Grab Switch 2";
            this.btn_BreakUnitTY_GrabSwitch2.UseVisualStyleBackColor = false;
            // 
            // btn_BreakUnitTY_GrabSwitch1
            // 
            this.btn_BreakUnitTY_GrabSwitch1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_BreakUnitTY_GrabSwitch1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_BreakUnitTY_GrabSwitch1.ForeColor = System.Drawing.Color.White;
            this.btn_BreakUnitTY_GrabSwitch1.Location = new System.Drawing.Point(1158, 6);
            this.btn_BreakUnitTY_GrabSwitch1.Name = "btn_BreakUnitTY_GrabSwitch1";
            this.btn_BreakUnitTY_GrabSwitch1.Size = new System.Drawing.Size(172, 23);
            this.btn_BreakUnitTY_GrabSwitch1.TabIndex = 33;
            this.btn_BreakUnitTY_GrabSwitch1.Text = "Grab Switch 1";
            this.btn_BreakUnitTY_GrabSwitch1.UseVisualStyleBackColor = false;
            // 
            // lv_BreakUnitTY
            // 
            this.lv_BreakUnitTY.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.col_BreakUnitTY_NamebyLocation,
            this.col_BreakUnitTY_Shaft,
            this.col_BreakUnitTY_Location,
            this.col_BreakUnitTY_Speed});
            this.lv_BreakUnitTY.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lv_BreakUnitTY.GridLines = true;
            this.lv_BreakUnitTY.Location = new System.Drawing.Point(17, 16);
            this.lv_BreakUnitTY.Name = "lv_BreakUnitTY";
            this.lv_BreakUnitTY.Size = new System.Drawing.Size(758, 734);
            this.lv_BreakUnitTY.TabIndex = 32;
            this.lv_BreakUnitTY.UseCompatibleStateImageBehavior = false;
            this.lv_BreakUnitTY.View = System.Windows.Forms.View.Details;
            // 
            // col_BreakUnitTY_NamebyLocation
            // 
            this.col_BreakUnitTY_NamebyLocation.Text = "위치별 명칭";
            this.col_BreakUnitTY_NamebyLocation.Width = 430;
            // 
            // col_BreakUnitTY_Shaft
            // 
            this.col_BreakUnitTY_Shaft.Text = "축";
            this.col_BreakUnitTY_Shaft.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // col_BreakUnitTY_Location
            // 
            this.col_BreakUnitTY_Location.Text = "위치[mm]";
            this.col_BreakUnitTY_Location.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_BreakUnitTY_Location.Width = 125;
            // 
            // col_BreakUnitTY_Speed
            // 
            this.col_BreakUnitTY_Speed.Text = "속도[mm/s]";
            this.col_BreakUnitTY_Speed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_BreakUnitTY_Speed.Width = 125;
            // 
            // tp_UnloaderTransfer
            // 
            this.tp_UnloaderTransfer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tp_UnloaderTransfer.Controls.Add(this.gbox_UnloaderTransfer_Move);
            this.tp_UnloaderTransfer.Controls.Add(this.gbox_UnloaderTransfer_B);
            this.tp_UnloaderTransfer.Controls.Add(this.gbox_UnloaderTransfer_A);
            this.tp_UnloaderTransfer.Controls.Add(this.btn_UnloaderTransfer_Save);
            this.tp_UnloaderTransfer.Controls.Add(this.btn_UnloaderTransfer_SpeedSetting);
            this.tp_UnloaderTransfer.Controls.Add(this.btn_UnloaderTransfer_AllSetting);
            this.tp_UnloaderTransfer.Controls.Add(this.btn_UnloaderTransfer_LocationSetting);
            this.tp_UnloaderTransfer.Controls.Add(this.btn_UnloaderTransfer_MoveLocation);
            this.tp_UnloaderTransfer.Controls.Add(this.tbox_UnloaderTransfer_Location);
            this.tp_UnloaderTransfer.Controls.Add(this.tbox_UnloaderTransfer_Speed);
            this.tp_UnloaderTransfer.Controls.Add(this.tbox_UnloaderTransfer_CurrentLocation);
            this.tp_UnloaderTransfer.Controls.Add(this.tbox_UnloaderTransfer_SelectedShaft);
            this.tp_UnloaderTransfer.Controls.Add(this.lbl_UnloaderTransfer_Location);
            this.tp_UnloaderTransfer.Controls.Add(this.lbl_UnloaderTransfer_Speed);
            this.tp_UnloaderTransfer.Controls.Add(this.lbl_UnloaderTransfer_SeletedShaft);
            this.tp_UnloaderTransfer.Controls.Add(this.tbox_UnloaderTransfer_LocationSetting);
            this.tp_UnloaderTransfer.Controls.Add(this.lbl_UnloaderTransfer_CurrentLocation);
            this.tp_UnloaderTransfer.Controls.Add(this.btn_UnloaderTransfer_GrabSwitch3);
            this.tp_UnloaderTransfer.Controls.Add(this.btn_UnloaderTransfer_GrabSwitch2);
            this.tp_UnloaderTransfer.Controls.Add(this.btn_UnloaderTransfer_GrabSwitch1);
            this.tp_UnloaderTransfer.Controls.Add(this.lv_UnloaderTransfer);
            this.tp_UnloaderTransfer.Controls.Add(this.ajin_Setting_UnloaderTransfer);
            this.tp_UnloaderTransfer.Location = new System.Drawing.Point(4, 34);
            this.tp_UnloaderTransfer.Name = "tp_UnloaderTransfer";
            this.tp_UnloaderTransfer.Size = new System.Drawing.Size(1726, 816);
            this.tp_UnloaderTransfer.TabIndex = 7;
            this.tp_UnloaderTransfer.Text = "언로더 트랜스퍼";
            // 
            // gbox_UnloaderTransfer_Move
            // 
            this.gbox_UnloaderTransfer_Move.Controls.Add(this.gbox_UnloaderTransfer_MoveB);
            this.gbox_UnloaderTransfer_Move.Controls.Add(this.gbox_UnloaderTransfer_MoveA);
            this.gbox_UnloaderTransfer_Move.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_UnloaderTransfer_Move.ForeColor = System.Drawing.Color.White;
            this.gbox_UnloaderTransfer_Move.Location = new System.Drawing.Point(792, 595);
            this.gbox_UnloaderTransfer_Move.Name = "gbox_UnloaderTransfer_Move";
            this.gbox_UnloaderTransfer_Move.Size = new System.Drawing.Size(904, 179);
            this.gbox_UnloaderTransfer_Move.TabIndex = 53;
            this.gbox_UnloaderTransfer_Move.TabStop = false;
            this.gbox_UnloaderTransfer_Move.Text = "     이동     ";
            // 
            // gbox_UnloaderTransfer_MoveB
            // 
            this.gbox_UnloaderTransfer_MoveB.Controls.Add(this.tableLayoutPanel61);
            this.gbox_UnloaderTransfer_MoveB.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_UnloaderTransfer_MoveB.ForeColor = System.Drawing.Color.White;
            this.gbox_UnloaderTransfer_MoveB.Location = new System.Drawing.Point(455, 28);
            this.gbox_UnloaderTransfer_MoveB.Name = "gbox_UnloaderTransfer_MoveB";
            this.gbox_UnloaderTransfer_MoveB.Size = new System.Drawing.Size(443, 145);
            this.gbox_UnloaderTransfer_MoveB.TabIndex = 31;
            this.gbox_UnloaderTransfer_MoveB.TabStop = false;
            this.gbox_UnloaderTransfer_MoveB.Text = "     B    ";
            // 
            // tableLayoutPanel61
            // 
            this.tableLayoutPanel61.ColumnCount = 1;
            this.tableLayoutPanel61.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel61.Controls.Add(this.tableLayoutPanel62, 0, 1);
            this.tableLayoutPanel61.Controls.Add(this.tableLayoutPanel63, 0, 0);
            this.tableLayoutPanel61.Location = new System.Drawing.Point(3, 27);
            this.tableLayoutPanel61.Name = "tableLayoutPanel61";
            this.tableLayoutPanel61.RowCount = 2;
            this.tableLayoutPanel61.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel61.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel61.Size = new System.Drawing.Size(434, 112);
            this.tableLayoutPanel61.TabIndex = 55;
            // 
            // tableLayoutPanel62
            // 
            this.tableLayoutPanel62.ColumnCount = 6;
            this.tableLayoutPanel62.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel62.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel62.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel62.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel62.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel62.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel62.Controls.Add(this.btn_UnloaderTransfer_MoveB_T40Angle, 0, 0);
            this.tableLayoutPanel62.Controls.Add(this.btn_UnloaderTransfer_MoveB_T4P90Angle, 0, 0);
            this.tableLayoutPanel62.Controls.Add(this.btn_UnloaderTransfer_MoveB_T4M90Angle, 0, 0);
            this.tableLayoutPanel62.Controls.Add(this.btn_UnloaderTransfer_MoveB_T30Angle, 0, 0);
            this.tableLayoutPanel62.Controls.Add(this.btn_UnloaderTransfer_MoveB_T3P90Angle, 0, 0);
            this.tableLayoutPanel62.Controls.Add(this.btn_UnloaderTransfer_MoveB_T3M90Angle, 0, 0);
            this.tableLayoutPanel62.Location = new System.Drawing.Point(3, 59);
            this.tableLayoutPanel62.Name = "tableLayoutPanel62";
            this.tableLayoutPanel62.RowCount = 1;
            this.tableLayoutPanel62.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel62.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel62.Size = new System.Drawing.Size(428, 50);
            this.tableLayoutPanel62.TabIndex = 56;
            // 
            // btn_UnloaderTransfer_MoveB_T40Angle
            // 
            this.btn_UnloaderTransfer_MoveB_T40Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_MoveB_T40Angle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_MoveB_T40Angle.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_MoveB_T40Angle.Location = new System.Drawing.Point(216, 3);
            this.btn_UnloaderTransfer_MoveB_T40Angle.Name = "btn_UnloaderTransfer_MoveB_T40Angle";
            this.btn_UnloaderTransfer_MoveB_T40Angle.Size = new System.Drawing.Size(65, 44);
            this.btn_UnloaderTransfer_MoveB_T40Angle.TabIndex = 61;
            this.btn_UnloaderTransfer_MoveB_T40Angle.Text = "T4\n0도";
            this.btn_UnloaderTransfer_MoveB_T40Angle.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_MoveB_T4P90Angle
            // 
            this.btn_UnloaderTransfer_MoveB_T4P90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_MoveB_T4P90Angle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_MoveB_T4P90Angle.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_MoveB_T4P90Angle.Location = new System.Drawing.Point(287, 3);
            this.btn_UnloaderTransfer_MoveB_T4P90Angle.Name = "btn_UnloaderTransfer_MoveB_T4P90Angle";
            this.btn_UnloaderTransfer_MoveB_T4P90Angle.Size = new System.Drawing.Size(65, 44);
            this.btn_UnloaderTransfer_MoveB_T4P90Angle.TabIndex = 60;
            this.btn_UnloaderTransfer_MoveB_T4P90Angle.Text = "T4\r\n+90도";
            this.btn_UnloaderTransfer_MoveB_T4P90Angle.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_MoveB_T4M90Angle
            // 
            this.btn_UnloaderTransfer_MoveB_T4M90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_MoveB_T4M90Angle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_MoveB_T4M90Angle.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_MoveB_T4M90Angle.Location = new System.Drawing.Point(358, 3);
            this.btn_UnloaderTransfer_MoveB_T4M90Angle.Name = "btn_UnloaderTransfer_MoveB_T4M90Angle";
            this.btn_UnloaderTransfer_MoveB_T4M90Angle.Size = new System.Drawing.Size(67, 44);
            this.btn_UnloaderTransfer_MoveB_T4M90Angle.TabIndex = 59;
            this.btn_UnloaderTransfer_MoveB_T4M90Angle.Text = "T4\r\n-90도";
            this.btn_UnloaderTransfer_MoveB_T4M90Angle.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_MoveB_T30Angle
            // 
            this.btn_UnloaderTransfer_MoveB_T30Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_MoveB_T30Angle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_MoveB_T30Angle.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_MoveB_T30Angle.Location = new System.Drawing.Point(3, 3);
            this.btn_UnloaderTransfer_MoveB_T30Angle.Name = "btn_UnloaderTransfer_MoveB_T30Angle";
            this.btn_UnloaderTransfer_MoveB_T30Angle.Size = new System.Drawing.Size(65, 44);
            this.btn_UnloaderTransfer_MoveB_T30Angle.TabIndex = 58;
            this.btn_UnloaderTransfer_MoveB_T30Angle.Text = "T3\r\n0도";
            this.btn_UnloaderTransfer_MoveB_T30Angle.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_MoveB_T3P90Angle
            // 
            this.btn_UnloaderTransfer_MoveB_T3P90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_MoveB_T3P90Angle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_MoveB_T3P90Angle.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_MoveB_T3P90Angle.Location = new System.Drawing.Point(74, 3);
            this.btn_UnloaderTransfer_MoveB_T3P90Angle.Name = "btn_UnloaderTransfer_MoveB_T3P90Angle";
            this.btn_UnloaderTransfer_MoveB_T3P90Angle.Size = new System.Drawing.Size(65, 44);
            this.btn_UnloaderTransfer_MoveB_T3P90Angle.TabIndex = 57;
            this.btn_UnloaderTransfer_MoveB_T3P90Angle.Text = "T3\r\n+90도";
            this.btn_UnloaderTransfer_MoveB_T3P90Angle.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_MoveB_T3M90Angle
            // 
            this.btn_UnloaderTransfer_MoveB_T3M90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_MoveB_T3M90Angle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_MoveB_T3M90Angle.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_MoveB_T3M90Angle.Location = new System.Drawing.Point(145, 3);
            this.btn_UnloaderTransfer_MoveB_T3M90Angle.Name = "btn_UnloaderTransfer_MoveB_T3M90Angle";
            this.btn_UnloaderTransfer_MoveB_T3M90Angle.Size = new System.Drawing.Size(65, 44);
            this.btn_UnloaderTransfer_MoveB_T3M90Angle.TabIndex = 56;
            this.btn_UnloaderTransfer_MoveB_T3M90Angle.Text = "T3\r\n-90도";
            this.btn_UnloaderTransfer_MoveB_T3M90Angle.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel63
            // 
            this.tableLayoutPanel63.ColumnCount = 5;
            this.tableLayoutPanel63.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel63.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel63.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel63.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel63.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel63.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel63.Controls.Add(this.btn_UnloaderTransfer_MoveB_BStage, 0, 0);
            this.tableLayoutPanel63.Controls.Add(this.btn_UnloaderTransfer_MoveB_BUnloading, 4, 0);
            this.tableLayoutPanel63.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel63.Name = "tableLayoutPanel63";
            this.tableLayoutPanel63.RowCount = 1;
            this.tableLayoutPanel63.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel63.Size = new System.Drawing.Size(428, 50);
            this.tableLayoutPanel63.TabIndex = 55;
            // 
            // btn_UnloaderTransfer_MoveB_BStage
            // 
            this.btn_UnloaderTransfer_MoveB_BStage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_MoveB_BStage.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_MoveB_BStage.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_MoveB_BStage.Location = new System.Drawing.Point(3, 3);
            this.btn_UnloaderTransfer_MoveB_BStage.Name = "btn_UnloaderTransfer_MoveB_BStage";
            this.btn_UnloaderTransfer_MoveB_BStage.Size = new System.Drawing.Size(79, 44);
            this.btn_UnloaderTransfer_MoveB_BStage.TabIndex = 55;
            this.btn_UnloaderTransfer_MoveB_BStage.Text = "B 스테이지";
            this.btn_UnloaderTransfer_MoveB_BStage.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_MoveB_BUnloading
            // 
            this.btn_UnloaderTransfer_MoveB_BUnloading.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_MoveB_BUnloading.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_MoveB_BUnloading.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_MoveB_BUnloading.Location = new System.Drawing.Point(343, 3);
            this.btn_UnloaderTransfer_MoveB_BUnloading.Name = "btn_UnloaderTransfer_MoveB_BUnloading";
            this.btn_UnloaderTransfer_MoveB_BUnloading.Size = new System.Drawing.Size(82, 44);
            this.btn_UnloaderTransfer_MoveB_BUnloading.TabIndex = 56;
            this.btn_UnloaderTransfer_MoveB_BUnloading.Text = "B 언로딩";
            this.btn_UnloaderTransfer_MoveB_BUnloading.UseVisualStyleBackColor = false;
            // 
            // gbox_UnloaderTransfer_MoveA
            // 
            this.gbox_UnloaderTransfer_MoveA.Controls.Add(this.tableLayoutPanel58);
            this.gbox_UnloaderTransfer_MoveA.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_UnloaderTransfer_MoveA.ForeColor = System.Drawing.Color.White;
            this.gbox_UnloaderTransfer_MoveA.Location = new System.Drawing.Point(6, 28);
            this.gbox_UnloaderTransfer_MoveA.Name = "gbox_UnloaderTransfer_MoveA";
            this.gbox_UnloaderTransfer_MoveA.Size = new System.Drawing.Size(443, 145);
            this.gbox_UnloaderTransfer_MoveA.TabIndex = 30;
            this.gbox_UnloaderTransfer_MoveA.TabStop = false;
            this.gbox_UnloaderTransfer_MoveA.Text = "     A    ";
            // 
            // tableLayoutPanel58
            // 
            this.tableLayoutPanel58.ColumnCount = 1;
            this.tableLayoutPanel58.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel58.Controls.Add(this.tableLayoutPanel60, 0, 1);
            this.tableLayoutPanel58.Controls.Add(this.tableLayoutPanel59, 0, 0);
            this.tableLayoutPanel58.Location = new System.Drawing.Point(3, 27);
            this.tableLayoutPanel58.Name = "tableLayoutPanel58";
            this.tableLayoutPanel58.RowCount = 2;
            this.tableLayoutPanel58.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel58.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel58.Size = new System.Drawing.Size(434, 112);
            this.tableLayoutPanel58.TabIndex = 54;
            // 
            // tableLayoutPanel60
            // 
            this.tableLayoutPanel60.ColumnCount = 6;
            this.tableLayoutPanel60.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel60.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel60.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel60.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel60.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel60.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel60.Controls.Add(this.btn_UnloaderTransfer_MoveA_T20Angle, 0, 0);
            this.tableLayoutPanel60.Controls.Add(this.btn_UnloaderTransfer_MoveA_T2P90Angle, 0, 0);
            this.tableLayoutPanel60.Controls.Add(this.btn_UnloaderTransfer_MoveA_T2M90Angle, 0, 0);
            this.tableLayoutPanel60.Controls.Add(this.btn_UnloaderTransfer_MoveA_T10Angle, 0, 0);
            this.tableLayoutPanel60.Controls.Add(this.btn_UnloaderTransfer_MoveA_T1P90Angle, 0, 0);
            this.tableLayoutPanel60.Controls.Add(this.btn_UnloaderTransfer_MoveA_T1M90Angle, 0, 0);
            this.tableLayoutPanel60.Location = new System.Drawing.Point(3, 59);
            this.tableLayoutPanel60.Name = "tableLayoutPanel60";
            this.tableLayoutPanel60.RowCount = 1;
            this.tableLayoutPanel60.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel60.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel60.Size = new System.Drawing.Size(428, 50);
            this.tableLayoutPanel60.TabIndex = 56;
            // 
            // btn_UnloaderTransfer_MoveA_T20Angle
            // 
            this.btn_UnloaderTransfer_MoveA_T20Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_MoveA_T20Angle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_MoveA_T20Angle.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_MoveA_T20Angle.Location = new System.Drawing.Point(216, 3);
            this.btn_UnloaderTransfer_MoveA_T20Angle.Name = "btn_UnloaderTransfer_MoveA_T20Angle";
            this.btn_UnloaderTransfer_MoveA_T20Angle.Size = new System.Drawing.Size(65, 44);
            this.btn_UnloaderTransfer_MoveA_T20Angle.TabIndex = 61;
            this.btn_UnloaderTransfer_MoveA_T20Angle.Text = "T2\r\n0도";
            this.btn_UnloaderTransfer_MoveA_T20Angle.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_MoveA_T2P90Angle
            // 
            this.btn_UnloaderTransfer_MoveA_T2P90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_MoveA_T2P90Angle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_MoveA_T2P90Angle.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_MoveA_T2P90Angle.Location = new System.Drawing.Point(287, 3);
            this.btn_UnloaderTransfer_MoveA_T2P90Angle.Name = "btn_UnloaderTransfer_MoveA_T2P90Angle";
            this.btn_UnloaderTransfer_MoveA_T2P90Angle.Size = new System.Drawing.Size(65, 44);
            this.btn_UnloaderTransfer_MoveA_T2P90Angle.TabIndex = 60;
            this.btn_UnloaderTransfer_MoveA_T2P90Angle.Text = "T2\r\n+90도";
            this.btn_UnloaderTransfer_MoveA_T2P90Angle.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_MoveA_T2M90Angle
            // 
            this.btn_UnloaderTransfer_MoveA_T2M90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_MoveA_T2M90Angle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_MoveA_T2M90Angle.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_MoveA_T2M90Angle.Location = new System.Drawing.Point(358, 3);
            this.btn_UnloaderTransfer_MoveA_T2M90Angle.Name = "btn_UnloaderTransfer_MoveA_T2M90Angle";
            this.btn_UnloaderTransfer_MoveA_T2M90Angle.Size = new System.Drawing.Size(67, 44);
            this.btn_UnloaderTransfer_MoveA_T2M90Angle.TabIndex = 59;
            this.btn_UnloaderTransfer_MoveA_T2M90Angle.Text = "T2\r\n-90도";
            this.btn_UnloaderTransfer_MoveA_T2M90Angle.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_MoveA_T10Angle
            // 
            this.btn_UnloaderTransfer_MoveA_T10Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_MoveA_T10Angle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_MoveA_T10Angle.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_MoveA_T10Angle.Location = new System.Drawing.Point(3, 3);
            this.btn_UnloaderTransfer_MoveA_T10Angle.Name = "btn_UnloaderTransfer_MoveA_T10Angle";
            this.btn_UnloaderTransfer_MoveA_T10Angle.Size = new System.Drawing.Size(65, 44);
            this.btn_UnloaderTransfer_MoveA_T10Angle.TabIndex = 58;
            this.btn_UnloaderTransfer_MoveA_T10Angle.Text = "T1\r\n0도";
            this.btn_UnloaderTransfer_MoveA_T10Angle.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_MoveA_T1P90Angle
            // 
            this.btn_UnloaderTransfer_MoveA_T1P90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_MoveA_T1P90Angle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_MoveA_T1P90Angle.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_MoveA_T1P90Angle.Location = new System.Drawing.Point(74, 3);
            this.btn_UnloaderTransfer_MoveA_T1P90Angle.Name = "btn_UnloaderTransfer_MoveA_T1P90Angle";
            this.btn_UnloaderTransfer_MoveA_T1P90Angle.Size = new System.Drawing.Size(65, 44);
            this.btn_UnloaderTransfer_MoveA_T1P90Angle.TabIndex = 57;
            this.btn_UnloaderTransfer_MoveA_T1P90Angle.Text = "T1\r\n+90도";
            this.btn_UnloaderTransfer_MoveA_T1P90Angle.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_MoveA_T1M90Angle
            // 
            this.btn_UnloaderTransfer_MoveA_T1M90Angle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_MoveA_T1M90Angle.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_MoveA_T1M90Angle.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_MoveA_T1M90Angle.Location = new System.Drawing.Point(145, 3);
            this.btn_UnloaderTransfer_MoveA_T1M90Angle.Name = "btn_UnloaderTransfer_MoveA_T1M90Angle";
            this.btn_UnloaderTransfer_MoveA_T1M90Angle.Size = new System.Drawing.Size(65, 44);
            this.btn_UnloaderTransfer_MoveA_T1M90Angle.TabIndex = 56;
            this.btn_UnloaderTransfer_MoveA_T1M90Angle.Text = "T1\r\n-90도";
            this.btn_UnloaderTransfer_MoveA_T1M90Angle.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel59
            // 
            this.tableLayoutPanel59.ColumnCount = 5;
            this.tableLayoutPanel59.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel59.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel59.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel59.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel59.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel59.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel59.Controls.Add(this.btn_UnloaderTransfer_MoveA_AStage, 0, 0);
            this.tableLayoutPanel59.Controls.Add(this.btn_UnloaderTransfer_MoveA_AUnloading, 4, 0);
            this.tableLayoutPanel59.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel59.Name = "tableLayoutPanel59";
            this.tableLayoutPanel59.RowCount = 1;
            this.tableLayoutPanel59.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel59.Size = new System.Drawing.Size(428, 50);
            this.tableLayoutPanel59.TabIndex = 55;
            // 
            // btn_UnloaderTransfer_MoveA_AStage
            // 
            this.btn_UnloaderTransfer_MoveA_AStage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_MoveA_AStage.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_MoveA_AStage.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_MoveA_AStage.Location = new System.Drawing.Point(3, 3);
            this.btn_UnloaderTransfer_MoveA_AStage.Name = "btn_UnloaderTransfer_MoveA_AStage";
            this.btn_UnloaderTransfer_MoveA_AStage.Size = new System.Drawing.Size(79, 44);
            this.btn_UnloaderTransfer_MoveA_AStage.TabIndex = 55;
            this.btn_UnloaderTransfer_MoveA_AStage.Text = "A 스테이지";
            this.btn_UnloaderTransfer_MoveA_AStage.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_MoveA_AUnloading
            // 
            this.btn_UnloaderTransfer_MoveA_AUnloading.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_MoveA_AUnloading.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_MoveA_AUnloading.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_MoveA_AUnloading.Location = new System.Drawing.Point(343, 3);
            this.btn_UnloaderTransfer_MoveA_AUnloading.Name = "btn_UnloaderTransfer_MoveA_AUnloading";
            this.btn_UnloaderTransfer_MoveA_AUnloading.Size = new System.Drawing.Size(82, 44);
            this.btn_UnloaderTransfer_MoveA_AUnloading.TabIndex = 56;
            this.btn_UnloaderTransfer_MoveA_AUnloading.Text = "A 언로딩";
            this.btn_UnloaderTransfer_MoveA_AUnloading.UseVisualStyleBackColor = false;
            // 
            // gbox_UnloaderTransfer_B
            // 
            this.gbox_UnloaderTransfer_B.Controls.Add(this.tableLayoutPanel4);
            this.gbox_UnloaderTransfer_B.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_UnloaderTransfer_B.ForeColor = System.Drawing.Color.White;
            this.gbox_UnloaderTransfer_B.Location = new System.Drawing.Point(1247, 410);
            this.gbox_UnloaderTransfer_B.Name = "gbox_UnloaderTransfer_B";
            this.gbox_UnloaderTransfer_B.Size = new System.Drawing.Size(449, 179);
            this.gbox_UnloaderTransfer_B.TabIndex = 52;
            this.gbox_UnloaderTransfer_B.TabStop = false;
            this.gbox_UnloaderTransfer_B.Text = "     B     ";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 4;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.Controls.Add(this.btn_UnloaderTransfer_B_PickerDestruction2, 3, 2);
            this.tableLayoutPanel4.Controls.Add(this.btn_UnloaderTransfer_B_PickerDestructionOn2, 2, 2);
            this.tableLayoutPanel4.Controls.Add(this.btn_UnloaderTransfer_B_PickerDestructionOff1, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.btn_UnloaderTransfer_B_PickerDestructionOn1, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.btn_UnloaderTransfer_B_PickerPneumaticOff2, 3, 1);
            this.tableLayoutPanel4.Controls.Add(this.btn_UnloaderTransfer_B_PickerPneumaticOn2, 2, 1);
            this.tableLayoutPanel4.Controls.Add(this.btn_UnloaderTransfer_B_PickerPneumaticOff1, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.btn_UnloaderTransfer_B_PickerPneumaticOn1, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.btn_UnloaderTransfer_B_PickerDown2, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.btn_UnloaderTransfer_B_PickerUp2, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.btn_UnloaderTransfer_B_PickerDown1, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.btn_UnloaderTransfer_B_PickerUp1, 0, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 3;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(437, 145);
            this.tableLayoutPanel4.TabIndex = 2;
            // 
            // btn_UnloaderTransfer_B_PickerDestruction2
            // 
            this.btn_UnloaderTransfer_B_PickerDestruction2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_B_PickerDestruction2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_B_PickerDestruction2.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_B_PickerDestruction2.Location = new System.Drawing.Point(330, 99);
            this.btn_UnloaderTransfer_B_PickerDestruction2.Name = "btn_UnloaderTransfer_B_PickerDestruction2";
            this.btn_UnloaderTransfer_B_PickerDestruction2.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_B_PickerDestruction2.TabIndex = 65;
            this.btn_UnloaderTransfer_B_PickerDestruction2.Text = "피커\r\n파기 오프";
            this.btn_UnloaderTransfer_B_PickerDestruction2.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_B_PickerDestructionOn2
            // 
            this.btn_UnloaderTransfer_B_PickerDestructionOn2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_B_PickerDestructionOn2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_B_PickerDestructionOn2.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_B_PickerDestructionOn2.Location = new System.Drawing.Point(221, 99);
            this.btn_UnloaderTransfer_B_PickerDestructionOn2.Name = "btn_UnloaderTransfer_B_PickerDestructionOn2";
            this.btn_UnloaderTransfer_B_PickerDestructionOn2.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_B_PickerDestructionOn2.TabIndex = 64;
            this.btn_UnloaderTransfer_B_PickerDestructionOn2.Text = "피커\r\n파기 온";
            this.btn_UnloaderTransfer_B_PickerDestructionOn2.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_B_PickerDestructionOff1
            // 
            this.btn_UnloaderTransfer_B_PickerDestructionOff1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_B_PickerDestructionOff1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_B_PickerDestructionOff1.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_B_PickerDestructionOff1.Location = new System.Drawing.Point(112, 99);
            this.btn_UnloaderTransfer_B_PickerDestructionOff1.Name = "btn_UnloaderTransfer_B_PickerDestructionOff1";
            this.btn_UnloaderTransfer_B_PickerDestructionOff1.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_B_PickerDestructionOff1.TabIndex = 63;
            this.btn_UnloaderTransfer_B_PickerDestructionOff1.Text = "피커\r\n파기 오프";
            this.btn_UnloaderTransfer_B_PickerDestructionOff1.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_B_PickerDestructionOn1
            // 
            this.btn_UnloaderTransfer_B_PickerDestructionOn1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_B_PickerDestructionOn1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_B_PickerDestructionOn1.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_B_PickerDestructionOn1.Location = new System.Drawing.Point(3, 99);
            this.btn_UnloaderTransfer_B_PickerDestructionOn1.Name = "btn_UnloaderTransfer_B_PickerDestructionOn1";
            this.btn_UnloaderTransfer_B_PickerDestructionOn1.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_B_PickerDestructionOn1.TabIndex = 62;
            this.btn_UnloaderTransfer_B_PickerDestructionOn1.Text = "피커\r\n파기 온";
            this.btn_UnloaderTransfer_B_PickerDestructionOn1.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_B_PickerPneumaticOff2
            // 
            this.btn_UnloaderTransfer_B_PickerPneumaticOff2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_B_PickerPneumaticOff2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_B_PickerPneumaticOff2.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_B_PickerPneumaticOff2.Location = new System.Drawing.Point(330, 51);
            this.btn_UnloaderTransfer_B_PickerPneumaticOff2.Name = "btn_UnloaderTransfer_B_PickerPneumaticOff2";
            this.btn_UnloaderTransfer_B_PickerPneumaticOff2.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_B_PickerPneumaticOff2.TabIndex = 61;
            this.btn_UnloaderTransfer_B_PickerPneumaticOff2.Text = "피커\r\n공압 오프";
            this.btn_UnloaderTransfer_B_PickerPneumaticOff2.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_B_PickerPneumaticOn2
            // 
            this.btn_UnloaderTransfer_B_PickerPneumaticOn2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_B_PickerPneumaticOn2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_B_PickerPneumaticOn2.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_B_PickerPneumaticOn2.Location = new System.Drawing.Point(221, 51);
            this.btn_UnloaderTransfer_B_PickerPneumaticOn2.Name = "btn_UnloaderTransfer_B_PickerPneumaticOn2";
            this.btn_UnloaderTransfer_B_PickerPneumaticOn2.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_B_PickerPneumaticOn2.TabIndex = 60;
            this.btn_UnloaderTransfer_B_PickerPneumaticOn2.Text = "피커\r\n공압 온";
            this.btn_UnloaderTransfer_B_PickerPneumaticOn2.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_B_PickerPneumaticOff1
            // 
            this.btn_UnloaderTransfer_B_PickerPneumaticOff1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_B_PickerPneumaticOff1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_B_PickerPneumaticOff1.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_B_PickerPneumaticOff1.Location = new System.Drawing.Point(112, 51);
            this.btn_UnloaderTransfer_B_PickerPneumaticOff1.Name = "btn_UnloaderTransfer_B_PickerPneumaticOff1";
            this.btn_UnloaderTransfer_B_PickerPneumaticOff1.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_B_PickerPneumaticOff1.TabIndex = 59;
            this.btn_UnloaderTransfer_B_PickerPneumaticOff1.Text = "피커\r\n공압 오프";
            this.btn_UnloaderTransfer_B_PickerPneumaticOff1.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_B_PickerPneumaticOn1
            // 
            this.btn_UnloaderTransfer_B_PickerPneumaticOn1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_B_PickerPneumaticOn1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_B_PickerPneumaticOn1.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_B_PickerPneumaticOn1.Location = new System.Drawing.Point(3, 51);
            this.btn_UnloaderTransfer_B_PickerPneumaticOn1.Name = "btn_UnloaderTransfer_B_PickerPneumaticOn1";
            this.btn_UnloaderTransfer_B_PickerPneumaticOn1.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_B_PickerPneumaticOn1.TabIndex = 58;
            this.btn_UnloaderTransfer_B_PickerPneumaticOn1.Text = "피커\r\n공압 온";
            this.btn_UnloaderTransfer_B_PickerPneumaticOn1.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_B_PickerDown2
            // 
            this.btn_UnloaderTransfer_B_PickerDown2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_B_PickerDown2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_B_PickerDown2.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_B_PickerDown2.Location = new System.Drawing.Point(330, 3);
            this.btn_UnloaderTransfer_B_PickerDown2.Name = "btn_UnloaderTransfer_B_PickerDown2";
            this.btn_UnloaderTransfer_B_PickerDown2.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_B_PickerDown2.TabIndex = 57;
            this.btn_UnloaderTransfer_B_PickerDown2.Text = "피커\r\n다운";
            this.btn_UnloaderTransfer_B_PickerDown2.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_B_PickerUp2
            // 
            this.btn_UnloaderTransfer_B_PickerUp2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_B_PickerUp2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_B_PickerUp2.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_B_PickerUp2.Location = new System.Drawing.Point(221, 3);
            this.btn_UnloaderTransfer_B_PickerUp2.Name = "btn_UnloaderTransfer_B_PickerUp2";
            this.btn_UnloaderTransfer_B_PickerUp2.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_B_PickerUp2.TabIndex = 56;
            this.btn_UnloaderTransfer_B_PickerUp2.Text = "피커\r\n업";
            this.btn_UnloaderTransfer_B_PickerUp2.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_B_PickerDown1
            // 
            this.btn_UnloaderTransfer_B_PickerDown1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_B_PickerDown1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_B_PickerDown1.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_B_PickerDown1.Location = new System.Drawing.Point(112, 3);
            this.btn_UnloaderTransfer_B_PickerDown1.Name = "btn_UnloaderTransfer_B_PickerDown1";
            this.btn_UnloaderTransfer_B_PickerDown1.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_B_PickerDown1.TabIndex = 55;
            this.btn_UnloaderTransfer_B_PickerDown1.Text = "피커\r\n다운";
            this.btn_UnloaderTransfer_B_PickerDown1.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_B_PickerUp1
            // 
            this.btn_UnloaderTransfer_B_PickerUp1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_B_PickerUp1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_B_PickerUp1.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_B_PickerUp1.Location = new System.Drawing.Point(3, 3);
            this.btn_UnloaderTransfer_B_PickerUp1.Name = "btn_UnloaderTransfer_B_PickerUp1";
            this.btn_UnloaderTransfer_B_PickerUp1.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_B_PickerUp1.TabIndex = 54;
            this.btn_UnloaderTransfer_B_PickerUp1.Text = "피커\r\n업";
            this.btn_UnloaderTransfer_B_PickerUp1.UseVisualStyleBackColor = false;
            // 
            // gbox_UnloaderTransfer_A
            // 
            this.gbox_UnloaderTransfer_A.Controls.Add(this.tableLayoutPanel3);
            this.gbox_UnloaderTransfer_A.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_UnloaderTransfer_A.ForeColor = System.Drawing.Color.White;
            this.gbox_UnloaderTransfer_A.Location = new System.Drawing.Point(792, 410);
            this.gbox_UnloaderTransfer_A.Name = "gbox_UnloaderTransfer_A";
            this.gbox_UnloaderTransfer_A.Size = new System.Drawing.Size(449, 179);
            this.gbox_UnloaderTransfer_A.TabIndex = 51;
            this.gbox_UnloaderTransfer_A.TabStop = false;
            this.gbox_UnloaderTransfer_A.Text = "     A    ";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.Controls.Add(this.btn_UnloaderTransfer_A_PickerDestructionOff2, 3, 2);
            this.tableLayoutPanel3.Controls.Add(this.btn_UnloaderTransfer_A_PickerDestructionOn2, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.btn_UnloaderTransfer_A_PickerDestructionOff1, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.btn_UnloaderTransfer_A_PickerDestructionOn1, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.btn_UnloaderTransfer_A_PickerPneumaticOff2, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.btn_UnloaderTransfer_A_PickerPneumaticOn2, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.btn_UnloaderTransfer_A_PickerPneumaticOff1, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.btn_UnloaderTransfer_A_PickerPneumaticOn1, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.btn_UnloaderTransfer_A_PickerDown2, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.btn_UnloaderTransfer_A_PickerUp2, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.btn_UnloaderTransfer_A_PickerDown1, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.btn_UnloaderTransfer_A_PickerUp1, 0, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(437, 145);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // btn_UnloaderTransfer_A_PickerDestructionOff2
            // 
            this.btn_UnloaderTransfer_A_PickerDestructionOff2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_A_PickerDestructionOff2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_A_PickerDestructionOff2.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_A_PickerDestructionOff2.Location = new System.Drawing.Point(330, 99);
            this.btn_UnloaderTransfer_A_PickerDestructionOff2.Name = "btn_UnloaderTransfer_A_PickerDestructionOff2";
            this.btn_UnloaderTransfer_A_PickerDestructionOff2.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_A_PickerDestructionOff2.TabIndex = 65;
            this.btn_UnloaderTransfer_A_PickerDestructionOff2.Text = "피커\r\n파기 오프";
            this.btn_UnloaderTransfer_A_PickerDestructionOff2.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_A_PickerDestructionOn2
            // 
            this.btn_UnloaderTransfer_A_PickerDestructionOn2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_A_PickerDestructionOn2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_A_PickerDestructionOn2.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_A_PickerDestructionOn2.Location = new System.Drawing.Point(221, 99);
            this.btn_UnloaderTransfer_A_PickerDestructionOn2.Name = "btn_UnloaderTransfer_A_PickerDestructionOn2";
            this.btn_UnloaderTransfer_A_PickerDestructionOn2.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_A_PickerDestructionOn2.TabIndex = 64;
            this.btn_UnloaderTransfer_A_PickerDestructionOn2.Text = "피커\r\n파기 온";
            this.btn_UnloaderTransfer_A_PickerDestructionOn2.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_A_PickerDestructionOff1
            // 
            this.btn_UnloaderTransfer_A_PickerDestructionOff1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_A_PickerDestructionOff1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_A_PickerDestructionOff1.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_A_PickerDestructionOff1.Location = new System.Drawing.Point(112, 99);
            this.btn_UnloaderTransfer_A_PickerDestructionOff1.Name = "btn_UnloaderTransfer_A_PickerDestructionOff1";
            this.btn_UnloaderTransfer_A_PickerDestructionOff1.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_A_PickerDestructionOff1.TabIndex = 63;
            this.btn_UnloaderTransfer_A_PickerDestructionOff1.Text = "피커\r\n파기 오프";
            this.btn_UnloaderTransfer_A_PickerDestructionOff1.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_A_PickerDestructionOn1
            // 
            this.btn_UnloaderTransfer_A_PickerDestructionOn1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_A_PickerDestructionOn1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_A_PickerDestructionOn1.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_A_PickerDestructionOn1.Location = new System.Drawing.Point(3, 99);
            this.btn_UnloaderTransfer_A_PickerDestructionOn1.Name = "btn_UnloaderTransfer_A_PickerDestructionOn1";
            this.btn_UnloaderTransfer_A_PickerDestructionOn1.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_A_PickerDestructionOn1.TabIndex = 62;
            this.btn_UnloaderTransfer_A_PickerDestructionOn1.Text = "피커\r\n파기 온";
            this.btn_UnloaderTransfer_A_PickerDestructionOn1.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_A_PickerPneumaticOff2
            // 
            this.btn_UnloaderTransfer_A_PickerPneumaticOff2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_A_PickerPneumaticOff2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_A_PickerPneumaticOff2.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_A_PickerPneumaticOff2.Location = new System.Drawing.Point(330, 51);
            this.btn_UnloaderTransfer_A_PickerPneumaticOff2.Name = "btn_UnloaderTransfer_A_PickerPneumaticOff2";
            this.btn_UnloaderTransfer_A_PickerPneumaticOff2.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_A_PickerPneumaticOff2.TabIndex = 61;
            this.btn_UnloaderTransfer_A_PickerPneumaticOff2.Text = "피커\r\n공압 오프";
            this.btn_UnloaderTransfer_A_PickerPneumaticOff2.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_A_PickerPneumaticOn2
            // 
            this.btn_UnloaderTransfer_A_PickerPneumaticOn2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_A_PickerPneumaticOn2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_A_PickerPneumaticOn2.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_A_PickerPneumaticOn2.Location = new System.Drawing.Point(221, 51);
            this.btn_UnloaderTransfer_A_PickerPneumaticOn2.Name = "btn_UnloaderTransfer_A_PickerPneumaticOn2";
            this.btn_UnloaderTransfer_A_PickerPneumaticOn2.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_A_PickerPneumaticOn2.TabIndex = 60;
            this.btn_UnloaderTransfer_A_PickerPneumaticOn2.Text = "피커\r\n공압 온";
            this.btn_UnloaderTransfer_A_PickerPneumaticOn2.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_A_PickerPneumaticOff1
            // 
            this.btn_UnloaderTransfer_A_PickerPneumaticOff1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_A_PickerPneumaticOff1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_A_PickerPneumaticOff1.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_A_PickerPneumaticOff1.Location = new System.Drawing.Point(112, 51);
            this.btn_UnloaderTransfer_A_PickerPneumaticOff1.Name = "btn_UnloaderTransfer_A_PickerPneumaticOff1";
            this.btn_UnloaderTransfer_A_PickerPneumaticOff1.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_A_PickerPneumaticOff1.TabIndex = 59;
            this.btn_UnloaderTransfer_A_PickerPneumaticOff1.Text = "피커\r\n공압 오프";
            this.btn_UnloaderTransfer_A_PickerPneumaticOff1.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_A_PickerPneumaticOn1
            // 
            this.btn_UnloaderTransfer_A_PickerPneumaticOn1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_A_PickerPneumaticOn1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_A_PickerPneumaticOn1.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_A_PickerPneumaticOn1.Location = new System.Drawing.Point(3, 51);
            this.btn_UnloaderTransfer_A_PickerPneumaticOn1.Name = "btn_UnloaderTransfer_A_PickerPneumaticOn1";
            this.btn_UnloaderTransfer_A_PickerPneumaticOn1.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_A_PickerPneumaticOn1.TabIndex = 58;
            this.btn_UnloaderTransfer_A_PickerPneumaticOn1.Text = "피커\r\n공압 온";
            this.btn_UnloaderTransfer_A_PickerPneumaticOn1.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_A_PickerDown2
            // 
            this.btn_UnloaderTransfer_A_PickerDown2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_A_PickerDown2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_A_PickerDown2.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_A_PickerDown2.Location = new System.Drawing.Point(330, 3);
            this.btn_UnloaderTransfer_A_PickerDown2.Name = "btn_UnloaderTransfer_A_PickerDown2";
            this.btn_UnloaderTransfer_A_PickerDown2.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_A_PickerDown2.TabIndex = 57;
            this.btn_UnloaderTransfer_A_PickerDown2.Text = "피커\r\n다운";
            this.btn_UnloaderTransfer_A_PickerDown2.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_A_PickerUp2
            // 
            this.btn_UnloaderTransfer_A_PickerUp2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_A_PickerUp2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_A_PickerUp2.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_A_PickerUp2.Location = new System.Drawing.Point(221, 3);
            this.btn_UnloaderTransfer_A_PickerUp2.Name = "btn_UnloaderTransfer_A_PickerUp2";
            this.btn_UnloaderTransfer_A_PickerUp2.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_A_PickerUp2.TabIndex = 56;
            this.btn_UnloaderTransfer_A_PickerUp2.Text = "피커\r\n업";
            this.btn_UnloaderTransfer_A_PickerUp2.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_A_PickerDown1
            // 
            this.btn_UnloaderTransfer_A_PickerDown1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_A_PickerDown1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_A_PickerDown1.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_A_PickerDown1.Location = new System.Drawing.Point(112, 3);
            this.btn_UnloaderTransfer_A_PickerDown1.Name = "btn_UnloaderTransfer_A_PickerDown1";
            this.btn_UnloaderTransfer_A_PickerDown1.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_A_PickerDown1.TabIndex = 55;
            this.btn_UnloaderTransfer_A_PickerDown1.Text = "피커\r\n다운";
            this.btn_UnloaderTransfer_A_PickerDown1.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_A_PickerUp1
            // 
            this.btn_UnloaderTransfer_A_PickerUp1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_A_PickerUp1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_A_PickerUp1.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_A_PickerUp1.Location = new System.Drawing.Point(3, 3);
            this.btn_UnloaderTransfer_A_PickerUp1.Name = "btn_UnloaderTransfer_A_PickerUp1";
            this.btn_UnloaderTransfer_A_PickerUp1.Size = new System.Drawing.Size(103, 42);
            this.btn_UnloaderTransfer_A_PickerUp1.TabIndex = 54;
            this.btn_UnloaderTransfer_A_PickerUp1.Text = "피커\r\n업";
            this.btn_UnloaderTransfer_A_PickerUp1.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_Save
            // 
            this.btn_UnloaderTransfer_Save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_Save.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_Save.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_Save.Location = new System.Drawing.Point(1482, 783);
            this.btn_UnloaderTransfer_Save.Name = "btn_UnloaderTransfer_Save";
            this.btn_UnloaderTransfer_Save.Size = new System.Drawing.Size(228, 28);
            this.btn_UnloaderTransfer_Save.TabIndex = 50;
            this.btn_UnloaderTransfer_Save.Text = "저장";
            this.btn_UnloaderTransfer_Save.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_SpeedSetting
            // 
            this.btn_UnloaderTransfer_SpeedSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_SpeedSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_UnloaderTransfer_SpeedSetting.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_SpeedSetting.Location = new System.Drawing.Point(1291, 315);
            this.btn_UnloaderTransfer_SpeedSetting.Name = "btn_UnloaderTransfer_SpeedSetting";
            this.btn_UnloaderTransfer_SpeedSetting.Size = new System.Drawing.Size(90, 27);
            this.btn_UnloaderTransfer_SpeedSetting.TabIndex = 48;
            this.btn_UnloaderTransfer_SpeedSetting.Text = "속도 설정";
            this.btn_UnloaderTransfer_SpeedSetting.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_AllSetting
            // 
            this.btn_UnloaderTransfer_AllSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_AllSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_UnloaderTransfer_AllSetting.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_AllSetting.Location = new System.Drawing.Point(1387, 273);
            this.btn_UnloaderTransfer_AllSetting.Name = "btn_UnloaderTransfer_AllSetting";
            this.btn_UnloaderTransfer_AllSetting.Size = new System.Drawing.Size(90, 27);
            this.btn_UnloaderTransfer_AllSetting.TabIndex = 47;
            this.btn_UnloaderTransfer_AllSetting.Text = "전체 설정";
            this.btn_UnloaderTransfer_AllSetting.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_LocationSetting
            // 
            this.btn_UnloaderTransfer_LocationSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_LocationSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_UnloaderTransfer_LocationSetting.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_LocationSetting.Location = new System.Drawing.Point(1071, 315);
            this.btn_UnloaderTransfer_LocationSetting.Name = "btn_UnloaderTransfer_LocationSetting";
            this.btn_UnloaderTransfer_LocationSetting.Size = new System.Drawing.Size(90, 27);
            this.btn_UnloaderTransfer_LocationSetting.TabIndex = 46;
            this.btn_UnloaderTransfer_LocationSetting.Text = "위치 설정";
            this.btn_UnloaderTransfer_LocationSetting.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_MoveLocation
            // 
            this.btn_UnloaderTransfer_MoveLocation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_MoveLocation.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_UnloaderTransfer_MoveLocation.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_MoveLocation.Location = new System.Drawing.Point(975, 315);
            this.btn_UnloaderTransfer_MoveLocation.Name = "btn_UnloaderTransfer_MoveLocation";
            this.btn_UnloaderTransfer_MoveLocation.Size = new System.Drawing.Size(90, 27);
            this.btn_UnloaderTransfer_MoveLocation.TabIndex = 45;
            this.btn_UnloaderTransfer_MoveLocation.Text = "위치 이동";
            this.btn_UnloaderTransfer_MoveLocation.UseVisualStyleBackColor = false;
            // 
            // tbox_UnloaderTransfer_Location
            // 
            this.tbox_UnloaderTransfer_Location.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_UnloaderTransfer_Location.Location = new System.Drawing.Point(879, 316);
            this.tbox_UnloaderTransfer_Location.Name = "tbox_UnloaderTransfer_Location";
            this.tbox_UnloaderTransfer_Location.Size = new System.Drawing.Size(90, 27);
            this.tbox_UnloaderTransfer_Location.TabIndex = 44;
            // 
            // tbox_UnloaderTransfer_Speed
            // 
            this.tbox_UnloaderTransfer_Speed.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_UnloaderTransfer_Speed.Location = new System.Drawing.Point(1291, 273);
            this.tbox_UnloaderTransfer_Speed.Name = "tbox_UnloaderTransfer_Speed";
            this.tbox_UnloaderTransfer_Speed.Size = new System.Drawing.Size(90, 27);
            this.tbox_UnloaderTransfer_Speed.TabIndex = 43;
            // 
            // tbox_UnloaderTransfer_CurrentLocation
            // 
            this.tbox_UnloaderTransfer_CurrentLocation.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_UnloaderTransfer_CurrentLocation.Location = new System.Drawing.Point(1084, 274);
            this.tbox_UnloaderTransfer_CurrentLocation.Name = "tbox_UnloaderTransfer_CurrentLocation";
            this.tbox_UnloaderTransfer_CurrentLocation.Size = new System.Drawing.Size(90, 27);
            this.tbox_UnloaderTransfer_CurrentLocation.TabIndex = 42;
            // 
            // tbox_UnloaderTransfer_SelectedShaft
            // 
            this.tbox_UnloaderTransfer_SelectedShaft.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_UnloaderTransfer_SelectedShaft.Location = new System.Drawing.Point(879, 273);
            this.tbox_UnloaderTransfer_SelectedShaft.Name = "tbox_UnloaderTransfer_SelectedShaft";
            this.tbox_UnloaderTransfer_SelectedShaft.Size = new System.Drawing.Size(90, 27);
            this.tbox_UnloaderTransfer_SelectedShaft.TabIndex = 41;
            // 
            // lbl_UnloaderTransfer_Location
            // 
            this.lbl_UnloaderTransfer_Location.AutoSize = true;
            this.lbl_UnloaderTransfer_Location.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_UnloaderTransfer_Location.ForeColor = System.Drawing.Color.White;
            this.lbl_UnloaderTransfer_Location.Location = new System.Drawing.Point(801, 316);
            this.lbl_UnloaderTransfer_Location.Name = "lbl_UnloaderTransfer_Location";
            this.lbl_UnloaderTransfer_Location.Size = new System.Drawing.Size(84, 21);
            this.lbl_UnloaderTransfer_Location.TabIndex = 40;
            this.lbl_UnloaderTransfer_Location.Text = "위치[mm]";
            // 
            // lbl_UnloaderTransfer_Speed
            // 
            this.lbl_UnloaderTransfer_Speed.AutoSize = true;
            this.lbl_UnloaderTransfer_Speed.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_UnloaderTransfer_Speed.ForeColor = System.Drawing.Color.White;
            this.lbl_UnloaderTransfer_Speed.Location = new System.Drawing.Point(1179, 275);
            this.lbl_UnloaderTransfer_Speed.Name = "lbl_UnloaderTransfer_Speed";
            this.lbl_UnloaderTransfer_Speed.Size = new System.Drawing.Size(115, 21);
            this.lbl_UnloaderTransfer_Speed.TabIndex = 39;
            this.lbl_UnloaderTransfer_Speed.Text = "속도[mm/sec]";
            // 
            // lbl_UnloaderTransfer_SeletedShaft
            // 
            this.lbl_UnloaderTransfer_SeletedShaft.AutoSize = true;
            this.lbl_UnloaderTransfer_SeletedShaft.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_UnloaderTransfer_SeletedShaft.ForeColor = System.Drawing.Color.White;
            this.lbl_UnloaderTransfer_SeletedShaft.Location = new System.Drawing.Point(805, 274);
            this.lbl_UnloaderTransfer_SeletedShaft.Name = "lbl_UnloaderTransfer_SeletedShaft";
            this.lbl_UnloaderTransfer_SeletedShaft.Size = new System.Drawing.Size(80, 21);
            this.lbl_UnloaderTransfer_SeletedShaft.TabIndex = 38;
            this.lbl_UnloaderTransfer_SeletedShaft.Text = "선택된 축";
            // 
            // tbox_UnloaderTransfer_LocationSetting
            // 
            this.tbox_UnloaderTransfer_LocationSetting.BackColor = System.Drawing.SystemColors.Highlight;
            this.tbox_UnloaderTransfer_LocationSetting.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.tbox_UnloaderTransfer_LocationSetting.ForeColor = System.Drawing.Color.White;
            this.tbox_UnloaderTransfer_LocationSetting.Location = new System.Drawing.Point(792, 223);
            this.tbox_UnloaderTransfer_LocationSetting.Name = "tbox_UnloaderTransfer_LocationSetting";
            this.tbox_UnloaderTransfer_LocationSetting.ReadOnly = true;
            this.tbox_UnloaderTransfer_LocationSetting.Size = new System.Drawing.Size(89, 27);
            this.tbox_UnloaderTransfer_LocationSetting.TabIndex = 37;
            this.tbox_UnloaderTransfer_LocationSetting.Text = "위치값 설정";
            this.tbox_UnloaderTransfer_LocationSetting.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbl_UnloaderTransfer_CurrentLocation
            // 
            this.lbl_UnloaderTransfer_CurrentLocation.AutoSize = true;
            this.lbl_UnloaderTransfer_CurrentLocation.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_UnloaderTransfer_CurrentLocation.ForeColor = System.Drawing.Color.White;
            this.lbl_UnloaderTransfer_CurrentLocation.Location = new System.Drawing.Point(975, 274);
            this.lbl_UnloaderTransfer_CurrentLocation.Name = "lbl_UnloaderTransfer_CurrentLocation";
            this.lbl_UnloaderTransfer_CurrentLocation.Size = new System.Drawing.Size(116, 21);
            this.lbl_UnloaderTransfer_CurrentLocation.TabIndex = 36;
            this.lbl_UnloaderTransfer_CurrentLocation.Text = "현재위치[mm]";
            // 
            // btn_UnloaderTransfer_GrabSwitch3
            // 
            this.btn_UnloaderTransfer_GrabSwitch3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_GrabSwitch3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_GrabSwitch3.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_GrabSwitch3.Location = new System.Drawing.Point(1514, 6);
            this.btn_UnloaderTransfer_GrabSwitch3.Name = "btn_UnloaderTransfer_GrabSwitch3";
            this.btn_UnloaderTransfer_GrabSwitch3.Size = new System.Drawing.Size(172, 23);
            this.btn_UnloaderTransfer_GrabSwitch3.TabIndex = 35;
            this.btn_UnloaderTransfer_GrabSwitch3.Text = "Grab Switch 3";
            this.btn_UnloaderTransfer_GrabSwitch3.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_GrabSwitch2
            // 
            this.btn_UnloaderTransfer_GrabSwitch2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_GrabSwitch2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_GrabSwitch2.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_GrabSwitch2.Location = new System.Drawing.Point(1336, 6);
            this.btn_UnloaderTransfer_GrabSwitch2.Name = "btn_UnloaderTransfer_GrabSwitch2";
            this.btn_UnloaderTransfer_GrabSwitch2.Size = new System.Drawing.Size(172, 23);
            this.btn_UnloaderTransfer_GrabSwitch2.TabIndex = 34;
            this.btn_UnloaderTransfer_GrabSwitch2.Text = "Grab Switch 2";
            this.btn_UnloaderTransfer_GrabSwitch2.UseVisualStyleBackColor = false;
            // 
            // btn_UnloaderTransfer_GrabSwitch1
            // 
            this.btn_UnloaderTransfer_GrabSwitch1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_UnloaderTransfer_GrabSwitch1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_UnloaderTransfer_GrabSwitch1.ForeColor = System.Drawing.Color.White;
            this.btn_UnloaderTransfer_GrabSwitch1.Location = new System.Drawing.Point(1158, 6);
            this.btn_UnloaderTransfer_GrabSwitch1.Name = "btn_UnloaderTransfer_GrabSwitch1";
            this.btn_UnloaderTransfer_GrabSwitch1.Size = new System.Drawing.Size(172, 23);
            this.btn_UnloaderTransfer_GrabSwitch1.TabIndex = 33;
            this.btn_UnloaderTransfer_GrabSwitch1.Text = "Grab Switch 1";
            this.btn_UnloaderTransfer_GrabSwitch1.UseVisualStyleBackColor = false;
            // 
            // lv_UnloaderTransfer
            // 
            this.lv_UnloaderTransfer.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.col_UnloaderTransfer_NamebyLocation,
            this.col_UnloaderTransfer_Shaft,
            this.col_UnloaderTransfer_Location,
            this.col_UnloaderTransfer_Speed});
            this.lv_UnloaderTransfer.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lv_UnloaderTransfer.GridLines = true;
            this.lv_UnloaderTransfer.Location = new System.Drawing.Point(17, 16);
            this.lv_UnloaderTransfer.Name = "lv_UnloaderTransfer";
            this.lv_UnloaderTransfer.Size = new System.Drawing.Size(758, 734);
            this.lv_UnloaderTransfer.TabIndex = 32;
            this.lv_UnloaderTransfer.UseCompatibleStateImageBehavior = false;
            this.lv_UnloaderTransfer.View = System.Windows.Forms.View.Details;
            // 
            // col_UnloaderTransfer_NamebyLocation
            // 
            this.col_UnloaderTransfer_NamebyLocation.Text = "위치별 명칭";
            this.col_UnloaderTransfer_NamebyLocation.Width = 430;
            // 
            // col_UnloaderTransfer_Shaft
            // 
            this.col_UnloaderTransfer_Shaft.Text = "축";
            this.col_UnloaderTransfer_Shaft.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // col_UnloaderTransfer_Location
            // 
            this.col_UnloaderTransfer_Location.Text = "위치[mm]";
            this.col_UnloaderTransfer_Location.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_UnloaderTransfer_Location.Width = 125;
            // 
            // col_UnloaderTransfer_Speed
            // 
            this.col_UnloaderTransfer_Speed.Text = "속도[mm/s]";
            this.col_UnloaderTransfer_Speed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_UnloaderTransfer_Speed.Width = 125;
            // 
            // tp_CameraUnit
            // 
            this.tp_CameraUnit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tp_CameraUnit.Controls.Add(this.gbox_CameraUnit_Move);
            this.tp_CameraUnit.Controls.Add(this.btn_CameraUnit_Save);
            this.tp_CameraUnit.Controls.Add(this.btn_CameraUnit_SpeedSetting);
            this.tp_CameraUnit.Controls.Add(this.btn_CameraUnit_AllSetting);
            this.tp_CameraUnit.Controls.Add(this.btn_CameraUnit_LocationSetting);
            this.tp_CameraUnit.Controls.Add(this.btn_CameraUnit_MoveLocation);
            this.tp_CameraUnit.Controls.Add(this.tbox_CameraUnit_Location);
            this.tp_CameraUnit.Controls.Add(this.tbox_CameraUnit_Speed);
            this.tp_CameraUnit.Controls.Add(this.tbox_CameraUnit_CurrentLocation);
            this.tp_CameraUnit.Controls.Add(this.tbox_CameraUnit_SelectedShaft);
            this.tp_CameraUnit.Controls.Add(this.lbl_CameraUnit_Location);
            this.tp_CameraUnit.Controls.Add(this.lbl_CameraUnit_Speed);
            this.tp_CameraUnit.Controls.Add(this.lbl_CameraUnit_SelectedShaft);
            this.tp_CameraUnit.Controls.Add(this.tbox_CameraUnit_LocationSetting);
            this.tp_CameraUnit.Controls.Add(this.lbl_CameraUnit_CurrentLocation);
            this.tp_CameraUnit.Controls.Add(this.btn_CameraUnit_GrabSwitch3);
            this.tp_CameraUnit.Controls.Add(this.btn_CameraUnit_GrabSwitch2);
            this.tp_CameraUnit.Controls.Add(this.btn_CameraUnit_GrabSwitch1);
            this.tp_CameraUnit.Controls.Add(this.lv_CameraUnit);
            this.tp_CameraUnit.Controls.Add(this.ajin_Setting_CameraUnit);
            this.tp_CameraUnit.Location = new System.Drawing.Point(4, 34);
            this.tp_CameraUnit.Name = "tp_CameraUnit";
            this.tp_CameraUnit.Size = new System.Drawing.Size(1726, 816);
            this.tp_CameraUnit.TabIndex = 8;
            this.tp_CameraUnit.Text = "카메라 유닛";
            // 
            // gbox_CameraUnit_Move
            // 
            this.gbox_CameraUnit_Move.Controls.Add(this.gbox_CameraUnit_MoveB);
            this.gbox_CameraUnit_Move.Controls.Add(this.gbox_CameraUnit_MoveA);
            this.gbox_CameraUnit_Move.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CameraUnit_Move.ForeColor = System.Drawing.Color.White;
            this.gbox_CameraUnit_Move.Location = new System.Drawing.Point(792, 595);
            this.gbox_CameraUnit_Move.Name = "gbox_CameraUnit_Move";
            this.gbox_CameraUnit_Move.Size = new System.Drawing.Size(904, 179);
            this.gbox_CameraUnit_Move.TabIndex = 53;
            this.gbox_CameraUnit_Move.TabStop = false;
            this.gbox_CameraUnit_Move.Text = "     이동     ";
            // 
            // gbox_CameraUnit_MoveB
            // 
            this.gbox_CameraUnit_MoveB.Controls.Add(this.tableLayoutPanel65);
            this.gbox_CameraUnit_MoveB.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CameraUnit_MoveB.ForeColor = System.Drawing.Color.White;
            this.gbox_CameraUnit_MoveB.Location = new System.Drawing.Point(455, 28);
            this.gbox_CameraUnit_MoveB.Name = "gbox_CameraUnit_MoveB";
            this.gbox_CameraUnit_MoveB.Size = new System.Drawing.Size(443, 145);
            this.gbox_CameraUnit_MoveB.TabIndex = 31;
            this.gbox_CameraUnit_MoveB.TabStop = false;
            this.gbox_CameraUnit_MoveB.Text = "     B    ";
            // 
            // tableLayoutPanel65
            // 
            this.tableLayoutPanel65.ColumnCount = 3;
            this.tableLayoutPanel65.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel65.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel65.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel65.Controls.Add(this.btn_CameraUnit_MoveB_InspectionMove4, 0, 0);
            this.tableLayoutPanel65.Controls.Add(this.btn_CameraUnit_MoveB_InspectionMove3, 0, 0);
            this.tableLayoutPanel65.Controls.Add(this.btn_CameraUnit_MoveB_MCRMove, 0, 0);
            this.tableLayoutPanel65.Location = new System.Drawing.Point(6, 27);
            this.tableLayoutPanel65.Name = "tableLayoutPanel65";
            this.tableLayoutPanel65.RowCount = 1;
            this.tableLayoutPanel65.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel65.Size = new System.Drawing.Size(431, 112);
            this.tableLayoutPanel65.TabIndex = 55;
            // 
            // btn_CameraUnit_MoveB_InspectionMove4
            // 
            this.btn_CameraUnit_MoveB_InspectionMove4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CameraUnit_MoveB_InspectionMove4.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btn_CameraUnit_MoveB_InspectionMove4.ForeColor = System.Drawing.Color.White;
            this.btn_CameraUnit_MoveB_InspectionMove4.Location = new System.Drawing.Point(289, 3);
            this.btn_CameraUnit_MoveB_InspectionMove4.Name = "btn_CameraUnit_MoveB_InspectionMove4";
            this.btn_CameraUnit_MoveB_InspectionMove4.Size = new System.Drawing.Size(137, 106);
            this.btn_CameraUnit_MoveB_InspectionMove4.TabIndex = 65;
            this.btn_CameraUnit_MoveB_InspectionMove4.Text = "Inspection 위치\r\n이동 4";
            this.btn_CameraUnit_MoveB_InspectionMove4.UseVisualStyleBackColor = false;
            // 
            // btn_CameraUnit_MoveB_InspectionMove3
            // 
            this.btn_CameraUnit_MoveB_InspectionMove3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CameraUnit_MoveB_InspectionMove3.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btn_CameraUnit_MoveB_InspectionMove3.ForeColor = System.Drawing.Color.White;
            this.btn_CameraUnit_MoveB_InspectionMove3.Location = new System.Drawing.Point(146, 3);
            this.btn_CameraUnit_MoveB_InspectionMove3.Name = "btn_CameraUnit_MoveB_InspectionMove3";
            this.btn_CameraUnit_MoveB_InspectionMove3.Size = new System.Drawing.Size(137, 106);
            this.btn_CameraUnit_MoveB_InspectionMove3.TabIndex = 64;
            this.btn_CameraUnit_MoveB_InspectionMove3.Text = "Inspection 위치\r\n이동 3";
            this.btn_CameraUnit_MoveB_InspectionMove3.UseVisualStyleBackColor = false;
            // 
            // btn_CameraUnit_MoveB_MCRMove
            // 
            this.btn_CameraUnit_MoveB_MCRMove.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CameraUnit_MoveB_MCRMove.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btn_CameraUnit_MoveB_MCRMove.ForeColor = System.Drawing.Color.White;
            this.btn_CameraUnit_MoveB_MCRMove.Location = new System.Drawing.Point(3, 3);
            this.btn_CameraUnit_MoveB_MCRMove.Name = "btn_CameraUnit_MoveB_MCRMove";
            this.btn_CameraUnit_MoveB_MCRMove.Size = new System.Drawing.Size(137, 106);
            this.btn_CameraUnit_MoveB_MCRMove.TabIndex = 63;
            this.btn_CameraUnit_MoveB_MCRMove.Text = "MCR 위치\r\n이동";
            this.btn_CameraUnit_MoveB_MCRMove.UseVisualStyleBackColor = false;
            // 
            // gbox_CameraUnit_MoveA
            // 
            this.gbox_CameraUnit_MoveA.Controls.Add(this.tableLayoutPanel64);
            this.gbox_CameraUnit_MoveA.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CameraUnit_MoveA.ForeColor = System.Drawing.Color.White;
            this.gbox_CameraUnit_MoveA.Location = new System.Drawing.Point(6, 28);
            this.gbox_CameraUnit_MoveA.Name = "gbox_CameraUnit_MoveA";
            this.gbox_CameraUnit_MoveA.Size = new System.Drawing.Size(443, 145);
            this.gbox_CameraUnit_MoveA.TabIndex = 30;
            this.gbox_CameraUnit_MoveA.TabStop = false;
            this.gbox_CameraUnit_MoveA.Text = "     A    ";
            // 
            // tableLayoutPanel64
            // 
            this.tableLayoutPanel64.ColumnCount = 3;
            this.tableLayoutPanel64.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel64.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel64.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel64.Controls.Add(this.btn_CameraUnit_MoveA_InspectionMove2, 0, 0);
            this.tableLayoutPanel64.Controls.Add(this.btn_CameraUnit_MoveA_InspectionMove1, 0, 0);
            this.tableLayoutPanel64.Controls.Add(this.btn_CameraUnit_MoveA_MCRMove, 0, 0);
            this.tableLayoutPanel64.Location = new System.Drawing.Point(6, 27);
            this.tableLayoutPanel64.Name = "tableLayoutPanel64";
            this.tableLayoutPanel64.RowCount = 1;
            this.tableLayoutPanel64.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel64.Size = new System.Drawing.Size(431, 112);
            this.tableLayoutPanel64.TabIndex = 54;
            // 
            // btn_CameraUnit_MoveA_InspectionMove2
            // 
            this.btn_CameraUnit_MoveA_InspectionMove2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CameraUnit_MoveA_InspectionMove2.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btn_CameraUnit_MoveA_InspectionMove2.ForeColor = System.Drawing.Color.White;
            this.btn_CameraUnit_MoveA_InspectionMove2.Location = new System.Drawing.Point(289, 3);
            this.btn_CameraUnit_MoveA_InspectionMove2.Name = "btn_CameraUnit_MoveA_InspectionMove2";
            this.btn_CameraUnit_MoveA_InspectionMove2.Size = new System.Drawing.Size(137, 106);
            this.btn_CameraUnit_MoveA_InspectionMove2.TabIndex = 65;
            this.btn_CameraUnit_MoveA_InspectionMove2.Text = "Inspection 위치\r\n이동 2";
            this.btn_CameraUnit_MoveA_InspectionMove2.UseVisualStyleBackColor = false;
            // 
            // btn_CameraUnit_MoveA_InspectionMove1
            // 
            this.btn_CameraUnit_MoveA_InspectionMove1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CameraUnit_MoveA_InspectionMove1.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btn_CameraUnit_MoveA_InspectionMove1.ForeColor = System.Drawing.Color.White;
            this.btn_CameraUnit_MoveA_InspectionMove1.Location = new System.Drawing.Point(146, 3);
            this.btn_CameraUnit_MoveA_InspectionMove1.Name = "btn_CameraUnit_MoveA_InspectionMove1";
            this.btn_CameraUnit_MoveA_InspectionMove1.Size = new System.Drawing.Size(137, 106);
            this.btn_CameraUnit_MoveA_InspectionMove1.TabIndex = 64;
            this.btn_CameraUnit_MoveA_InspectionMove1.Text = "Inspection 위치\r\n이동 1";
            this.btn_CameraUnit_MoveA_InspectionMove1.UseVisualStyleBackColor = false;
            // 
            // btn_CameraUnit_MoveA_MCRMove
            // 
            this.btn_CameraUnit_MoveA_MCRMove.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CameraUnit_MoveA_MCRMove.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btn_CameraUnit_MoveA_MCRMove.ForeColor = System.Drawing.Color.White;
            this.btn_CameraUnit_MoveA_MCRMove.Location = new System.Drawing.Point(3, 3);
            this.btn_CameraUnit_MoveA_MCRMove.Name = "btn_CameraUnit_MoveA_MCRMove";
            this.btn_CameraUnit_MoveA_MCRMove.Size = new System.Drawing.Size(137, 106);
            this.btn_CameraUnit_MoveA_MCRMove.TabIndex = 63;
            this.btn_CameraUnit_MoveA_MCRMove.Text = "MCR 위치\r\n이동";
            this.btn_CameraUnit_MoveA_MCRMove.UseVisualStyleBackColor = false;
            // 
            // btn_CameraUnit_Save
            // 
            this.btn_CameraUnit_Save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CameraUnit_Save.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CameraUnit_Save.ForeColor = System.Drawing.Color.White;
            this.btn_CameraUnit_Save.Location = new System.Drawing.Point(1482, 783);
            this.btn_CameraUnit_Save.Name = "btn_CameraUnit_Save";
            this.btn_CameraUnit_Save.Size = new System.Drawing.Size(228, 28);
            this.btn_CameraUnit_Save.TabIndex = 50;
            this.btn_CameraUnit_Save.Text = "저장";
            this.btn_CameraUnit_Save.UseVisualStyleBackColor = false;
            // 
            // btn_CameraUnit_SpeedSetting
            // 
            this.btn_CameraUnit_SpeedSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CameraUnit_SpeedSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CameraUnit_SpeedSetting.ForeColor = System.Drawing.Color.White;
            this.btn_CameraUnit_SpeedSetting.Location = new System.Drawing.Point(1291, 315);
            this.btn_CameraUnit_SpeedSetting.Name = "btn_CameraUnit_SpeedSetting";
            this.btn_CameraUnit_SpeedSetting.Size = new System.Drawing.Size(90, 27);
            this.btn_CameraUnit_SpeedSetting.TabIndex = 48;
            this.btn_CameraUnit_SpeedSetting.Text = "속도 설정";
            this.btn_CameraUnit_SpeedSetting.UseVisualStyleBackColor = false;
            // 
            // btn_CameraUnit_AllSetting
            // 
            this.btn_CameraUnit_AllSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CameraUnit_AllSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CameraUnit_AllSetting.ForeColor = System.Drawing.Color.White;
            this.btn_CameraUnit_AllSetting.Location = new System.Drawing.Point(1387, 273);
            this.btn_CameraUnit_AllSetting.Name = "btn_CameraUnit_AllSetting";
            this.btn_CameraUnit_AllSetting.Size = new System.Drawing.Size(90, 27);
            this.btn_CameraUnit_AllSetting.TabIndex = 47;
            this.btn_CameraUnit_AllSetting.Text = "전체 설정";
            this.btn_CameraUnit_AllSetting.UseVisualStyleBackColor = false;
            // 
            // btn_CameraUnit_LocationSetting
            // 
            this.btn_CameraUnit_LocationSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CameraUnit_LocationSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CameraUnit_LocationSetting.ForeColor = System.Drawing.Color.White;
            this.btn_CameraUnit_LocationSetting.Location = new System.Drawing.Point(1071, 315);
            this.btn_CameraUnit_LocationSetting.Name = "btn_CameraUnit_LocationSetting";
            this.btn_CameraUnit_LocationSetting.Size = new System.Drawing.Size(90, 27);
            this.btn_CameraUnit_LocationSetting.TabIndex = 46;
            this.btn_CameraUnit_LocationSetting.Text = "위치 설정";
            this.btn_CameraUnit_LocationSetting.UseVisualStyleBackColor = false;
            // 
            // btn_CameraUnit_MoveLocation
            // 
            this.btn_CameraUnit_MoveLocation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CameraUnit_MoveLocation.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CameraUnit_MoveLocation.ForeColor = System.Drawing.Color.White;
            this.btn_CameraUnit_MoveLocation.Location = new System.Drawing.Point(975, 315);
            this.btn_CameraUnit_MoveLocation.Name = "btn_CameraUnit_MoveLocation";
            this.btn_CameraUnit_MoveLocation.Size = new System.Drawing.Size(90, 27);
            this.btn_CameraUnit_MoveLocation.TabIndex = 45;
            this.btn_CameraUnit_MoveLocation.Text = "위치 이동";
            this.btn_CameraUnit_MoveLocation.UseVisualStyleBackColor = false;
            // 
            // tbox_CameraUnit_Location
            // 
            this.tbox_CameraUnit_Location.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_CameraUnit_Location.Location = new System.Drawing.Point(879, 316);
            this.tbox_CameraUnit_Location.Name = "tbox_CameraUnit_Location";
            this.tbox_CameraUnit_Location.Size = new System.Drawing.Size(90, 27);
            this.tbox_CameraUnit_Location.TabIndex = 44;
            // 
            // tbox_CameraUnit_Speed
            // 
            this.tbox_CameraUnit_Speed.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_CameraUnit_Speed.Location = new System.Drawing.Point(1291, 273);
            this.tbox_CameraUnit_Speed.Name = "tbox_CameraUnit_Speed";
            this.tbox_CameraUnit_Speed.Size = new System.Drawing.Size(90, 27);
            this.tbox_CameraUnit_Speed.TabIndex = 43;
            // 
            // tbox_CameraUnit_CurrentLocation
            // 
            this.tbox_CameraUnit_CurrentLocation.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_CameraUnit_CurrentLocation.Location = new System.Drawing.Point(1084, 274);
            this.tbox_CameraUnit_CurrentLocation.Name = "tbox_CameraUnit_CurrentLocation";
            this.tbox_CameraUnit_CurrentLocation.Size = new System.Drawing.Size(90, 27);
            this.tbox_CameraUnit_CurrentLocation.TabIndex = 42;
            // 
            // tbox_CameraUnit_SelectedShaft
            // 
            this.tbox_CameraUnit_SelectedShaft.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_CameraUnit_SelectedShaft.Location = new System.Drawing.Point(879, 273);
            this.tbox_CameraUnit_SelectedShaft.Name = "tbox_CameraUnit_SelectedShaft";
            this.tbox_CameraUnit_SelectedShaft.Size = new System.Drawing.Size(90, 27);
            this.tbox_CameraUnit_SelectedShaft.TabIndex = 41;
            // 
            // lbl_CameraUnit_Location
            // 
            this.lbl_CameraUnit_Location.AutoSize = true;
            this.lbl_CameraUnit_Location.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CameraUnit_Location.ForeColor = System.Drawing.Color.White;
            this.lbl_CameraUnit_Location.Location = new System.Drawing.Point(801, 316);
            this.lbl_CameraUnit_Location.Name = "lbl_CameraUnit_Location";
            this.lbl_CameraUnit_Location.Size = new System.Drawing.Size(84, 21);
            this.lbl_CameraUnit_Location.TabIndex = 40;
            this.lbl_CameraUnit_Location.Text = "위치[mm]";
            // 
            // lbl_CameraUnit_Speed
            // 
            this.lbl_CameraUnit_Speed.AutoSize = true;
            this.lbl_CameraUnit_Speed.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CameraUnit_Speed.ForeColor = System.Drawing.Color.White;
            this.lbl_CameraUnit_Speed.Location = new System.Drawing.Point(1179, 275);
            this.lbl_CameraUnit_Speed.Name = "lbl_CameraUnit_Speed";
            this.lbl_CameraUnit_Speed.Size = new System.Drawing.Size(115, 21);
            this.lbl_CameraUnit_Speed.TabIndex = 39;
            this.lbl_CameraUnit_Speed.Text = "속도[mm/sec]";
            // 
            // lbl_CameraUnit_SelectedShaft
            // 
            this.lbl_CameraUnit_SelectedShaft.AutoSize = true;
            this.lbl_CameraUnit_SelectedShaft.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CameraUnit_SelectedShaft.ForeColor = System.Drawing.Color.White;
            this.lbl_CameraUnit_SelectedShaft.Location = new System.Drawing.Point(805, 274);
            this.lbl_CameraUnit_SelectedShaft.Name = "lbl_CameraUnit_SelectedShaft";
            this.lbl_CameraUnit_SelectedShaft.Size = new System.Drawing.Size(80, 21);
            this.lbl_CameraUnit_SelectedShaft.TabIndex = 38;
            this.lbl_CameraUnit_SelectedShaft.Text = "선택된 축";
            // 
            // tbox_CameraUnit_LocationSetting
            // 
            this.tbox_CameraUnit_LocationSetting.BackColor = System.Drawing.SystemColors.Highlight;
            this.tbox_CameraUnit_LocationSetting.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.tbox_CameraUnit_LocationSetting.ForeColor = System.Drawing.Color.White;
            this.tbox_CameraUnit_LocationSetting.Location = new System.Drawing.Point(792, 223);
            this.tbox_CameraUnit_LocationSetting.Name = "tbox_CameraUnit_LocationSetting";
            this.tbox_CameraUnit_LocationSetting.ReadOnly = true;
            this.tbox_CameraUnit_LocationSetting.Size = new System.Drawing.Size(89, 27);
            this.tbox_CameraUnit_LocationSetting.TabIndex = 37;
            this.tbox_CameraUnit_LocationSetting.Text = "위치값 설정";
            this.tbox_CameraUnit_LocationSetting.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbl_CameraUnit_CurrentLocation
            // 
            this.lbl_CameraUnit_CurrentLocation.AutoSize = true;
            this.lbl_CameraUnit_CurrentLocation.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CameraUnit_CurrentLocation.ForeColor = System.Drawing.Color.White;
            this.lbl_CameraUnit_CurrentLocation.Location = new System.Drawing.Point(975, 274);
            this.lbl_CameraUnit_CurrentLocation.Name = "lbl_CameraUnit_CurrentLocation";
            this.lbl_CameraUnit_CurrentLocation.Size = new System.Drawing.Size(116, 21);
            this.lbl_CameraUnit_CurrentLocation.TabIndex = 36;
            this.lbl_CameraUnit_CurrentLocation.Text = "현재위치[mm]";
            // 
            // btn_CameraUnit_GrabSwitch3
            // 
            this.btn_CameraUnit_GrabSwitch3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CameraUnit_GrabSwitch3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CameraUnit_GrabSwitch3.ForeColor = System.Drawing.Color.White;
            this.btn_CameraUnit_GrabSwitch3.Location = new System.Drawing.Point(1514, 6);
            this.btn_CameraUnit_GrabSwitch3.Name = "btn_CameraUnit_GrabSwitch3";
            this.btn_CameraUnit_GrabSwitch3.Size = new System.Drawing.Size(172, 23);
            this.btn_CameraUnit_GrabSwitch3.TabIndex = 35;
            this.btn_CameraUnit_GrabSwitch3.Text = "Grab Switch 3";
            this.btn_CameraUnit_GrabSwitch3.UseVisualStyleBackColor = false;
            // 
            // btn_CameraUnit_GrabSwitch2
            // 
            this.btn_CameraUnit_GrabSwitch2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CameraUnit_GrabSwitch2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CameraUnit_GrabSwitch2.ForeColor = System.Drawing.Color.White;
            this.btn_CameraUnit_GrabSwitch2.Location = new System.Drawing.Point(1336, 6);
            this.btn_CameraUnit_GrabSwitch2.Name = "btn_CameraUnit_GrabSwitch2";
            this.btn_CameraUnit_GrabSwitch2.Size = new System.Drawing.Size(172, 23);
            this.btn_CameraUnit_GrabSwitch2.TabIndex = 34;
            this.btn_CameraUnit_GrabSwitch2.Text = "Grab Switch 2";
            this.btn_CameraUnit_GrabSwitch2.UseVisualStyleBackColor = false;
            // 
            // btn_CameraUnit_GrabSwitch1
            // 
            this.btn_CameraUnit_GrabSwitch1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CameraUnit_GrabSwitch1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CameraUnit_GrabSwitch1.ForeColor = System.Drawing.Color.White;
            this.btn_CameraUnit_GrabSwitch1.Location = new System.Drawing.Point(1158, 6);
            this.btn_CameraUnit_GrabSwitch1.Name = "btn_CameraUnit_GrabSwitch1";
            this.btn_CameraUnit_GrabSwitch1.Size = new System.Drawing.Size(172, 23);
            this.btn_CameraUnit_GrabSwitch1.TabIndex = 33;
            this.btn_CameraUnit_GrabSwitch1.Text = "Grab Switch 1";
            this.btn_CameraUnit_GrabSwitch1.UseVisualStyleBackColor = false;
            // 
            // lv_CameraUnit
            // 
            this.lv_CameraUnit.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.col_CameraUnit_NamebyLocation,
            this.col_CameraUnit_Shaft,
            this.col_CameraUnit_Location,
            this.col_CameraUnit_Speed});
            this.lv_CameraUnit.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lv_CameraUnit.GridLines = true;
            this.lv_CameraUnit.Location = new System.Drawing.Point(17, 16);
            this.lv_CameraUnit.Name = "lv_CameraUnit";
            this.lv_CameraUnit.Size = new System.Drawing.Size(758, 734);
            this.lv_CameraUnit.TabIndex = 32;
            this.lv_CameraUnit.UseCompatibleStateImageBehavior = false;
            this.lv_CameraUnit.View = System.Windows.Forms.View.Details;
            // 
            // col_CameraUnit_NamebyLocation
            // 
            this.col_CameraUnit_NamebyLocation.Text = "위치별 명칭";
            this.col_CameraUnit_NamebyLocation.Width = 430;
            // 
            // col_CameraUnit_Shaft
            // 
            this.col_CameraUnit_Shaft.Text = "축";
            this.col_CameraUnit_Shaft.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // col_CameraUnit_Location
            // 
            this.col_CameraUnit_Location.Text = "위치[mm]";
            this.col_CameraUnit_Location.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_CameraUnit_Location.Width = 125;
            // 
            // col_CameraUnit_Speed
            // 
            this.col_CameraUnit_Speed.Text = "속도[mm/s]";
            this.col_CameraUnit_Speed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_CameraUnit_Speed.Width = 125;
            // 
            // tp_CellInput
            // 
            this.tp_CellInput.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tp_CellInput.Controls.Add(this.gbox_CellInput_Buffer);
            this.tp_CellInput.Controls.Add(this.gbox_CellInput_Ionizer);
            this.tp_CellInput.Controls.Add(this.gbox_CellInput_Move);
            this.tp_CellInput.Controls.Add(this.gbox_CellInput_B);
            this.tp_CellInput.Controls.Add(this.gbox_CellInput_A);
            this.tp_CellInput.Controls.Add(this.btn_CellInput_Save);
            this.tp_CellInput.Controls.Add(this.btn_CellInput_SpeedSetting);
            this.tp_CellInput.Controls.Add(this.btn_CellInput_AllSetting);
            this.tp_CellInput.Controls.Add(this.btn_CellInput_LocationSetting);
            this.tp_CellInput.Controls.Add(this.btn_CellInput_MoveLocation);
            this.tp_CellInput.Controls.Add(this.tbox_CellInput_Location);
            this.tp_CellInput.Controls.Add(this.tbox_CellInput_Speed);
            this.tp_CellInput.Controls.Add(this.tbox_CellInput_CurrentLocation);
            this.tp_CellInput.Controls.Add(this.tbox_CellInput_SelectedShaft);
            this.tp_CellInput.Controls.Add(this.lbl_CellInput_Location);
            this.tp_CellInput.Controls.Add(this.lbl_CellInput_Speed);
            this.tp_CellInput.Controls.Add(this.lbl_CellInput_SelectedShaft);
            this.tp_CellInput.Controls.Add(this.tbox_CellInput_LocationSetting);
            this.tp_CellInput.Controls.Add(this.lbl_CellInput_CurrentLocation);
            this.tp_CellInput.Controls.Add(this.btn_CellInput_GrabSwitch3);
            this.tp_CellInput.Controls.Add(this.btn_CellInput_GrabSwitch2);
            this.tp_CellInput.Controls.Add(this.btn_CellInput_GrabSwitch1);
            this.tp_CellInput.Controls.Add(this.lv_CellInput);
            this.tp_CellInput.Controls.Add(this.ajin_Setting_CellInput);
            this.tp_CellInput.Location = new System.Drawing.Point(4, 34);
            this.tp_CellInput.Name = "tp_CellInput";
            this.tp_CellInput.Size = new System.Drawing.Size(1726, 816);
            this.tp_CellInput.TabIndex = 9;
            this.tp_CellInput.Text = "셀 투입 이재기";
            // 
            // gbox_CellInput_Buffer
            // 
            this.gbox_CellInput_Buffer.Controls.Add(this.tableLayoutPanel66);
            this.gbox_CellInput_Buffer.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CellInput_Buffer.ForeColor = System.Drawing.Color.White;
            this.gbox_CellInput_Buffer.Location = new System.Drawing.Point(1484, 410);
            this.gbox_CellInput_Buffer.Name = "gbox_CellInput_Buffer";
            this.gbox_CellInput_Buffer.Size = new System.Drawing.Size(212, 179);
            this.gbox_CellInput_Buffer.TabIndex = 54;
            this.gbox_CellInput_Buffer.TabStop = false;
            this.gbox_CellInput_Buffer.Text = "     BUFFER    ";
            // 
            // tableLayoutPanel66
            // 
            this.tableLayoutPanel66.ColumnCount = 2;
            this.tableLayoutPanel66.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel66.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel66.Controls.Add(this.btn_CellInput_Buffer_DestructionOn, 0, 2);
            this.tableLayoutPanel66.Controls.Add(this.btn_CellInput_Buffer_DestructionOff, 0, 2);
            this.tableLayoutPanel66.Controls.Add(this.btn_CellInput_Buffer_PickerUp, 0, 0);
            this.tableLayoutPanel66.Controls.Add(this.btn_CellInput_Buffer_PickerDown, 1, 0);
            this.tableLayoutPanel66.Controls.Add(this.btn_CellInput_Buffer_PneumaticOn, 0, 1);
            this.tableLayoutPanel66.Controls.Add(this.btn_CellInput_Buffer_PneumaticOff, 1, 1);
            this.tableLayoutPanel66.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel66.Name = "tableLayoutPanel66";
            this.tableLayoutPanel66.RowCount = 3;
            this.tableLayoutPanel66.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel66.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel66.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel66.Size = new System.Drawing.Size(200, 141);
            this.tableLayoutPanel66.TabIndex = 55;
            // 
            // btn_CellInput_Buffer_DestructionOn
            // 
            this.btn_CellInput_Buffer_DestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_Buffer_DestructionOn.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CellInput_Buffer_DestructionOn.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_Buffer_DestructionOn.Location = new System.Drawing.Point(3, 97);
            this.btn_CellInput_Buffer_DestructionOn.Name = "btn_CellInput_Buffer_DestructionOn";
            this.btn_CellInput_Buffer_DestructionOn.Size = new System.Drawing.Size(94, 41);
            this.btn_CellInput_Buffer_DestructionOn.TabIndex = 63;
            this.btn_CellInput_Buffer_DestructionOn.Text = "파기 온";
            this.btn_CellInput_Buffer_DestructionOn.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_Buffer_DestructionOff
            // 
            this.btn_CellInput_Buffer_DestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_Buffer_DestructionOff.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CellInput_Buffer_DestructionOff.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_Buffer_DestructionOff.Location = new System.Drawing.Point(103, 97);
            this.btn_CellInput_Buffer_DestructionOff.Name = "btn_CellInput_Buffer_DestructionOff";
            this.btn_CellInput_Buffer_DestructionOff.Size = new System.Drawing.Size(94, 41);
            this.btn_CellInput_Buffer_DestructionOff.TabIndex = 62;
            this.btn_CellInput_Buffer_DestructionOff.Text = "파기 오프";
            this.btn_CellInput_Buffer_DestructionOff.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_Buffer_PickerUp
            // 
            this.btn_CellInput_Buffer_PickerUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_Buffer_PickerUp.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_Buffer_PickerUp.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_Buffer_PickerUp.Location = new System.Drawing.Point(3, 3);
            this.btn_CellInput_Buffer_PickerUp.Name = "btn_CellInput_Buffer_PickerUp";
            this.btn_CellInput_Buffer_PickerUp.Size = new System.Drawing.Size(94, 40);
            this.btn_CellInput_Buffer_PickerUp.TabIndex = 58;
            this.btn_CellInput_Buffer_PickerUp.Text = "버퍼 피커\r\n업";
            this.btn_CellInput_Buffer_PickerUp.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_Buffer_PickerDown
            // 
            this.btn_CellInput_Buffer_PickerDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_Buffer_PickerDown.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_Buffer_PickerDown.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_Buffer_PickerDown.Location = new System.Drawing.Point(103, 3);
            this.btn_CellInput_Buffer_PickerDown.Name = "btn_CellInput_Buffer_PickerDown";
            this.btn_CellInput_Buffer_PickerDown.Size = new System.Drawing.Size(94, 40);
            this.btn_CellInput_Buffer_PickerDown.TabIndex = 60;
            this.btn_CellInput_Buffer_PickerDown.Text = "버퍼 피커\r\n다운";
            this.btn_CellInput_Buffer_PickerDown.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_Buffer_PneumaticOn
            // 
            this.btn_CellInput_Buffer_PneumaticOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_Buffer_PneumaticOn.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CellInput_Buffer_PneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_Buffer_PneumaticOn.Location = new System.Drawing.Point(3, 50);
            this.btn_CellInput_Buffer_PneumaticOn.Name = "btn_CellInput_Buffer_PneumaticOn";
            this.btn_CellInput_Buffer_PneumaticOn.Size = new System.Drawing.Size(94, 40);
            this.btn_CellInput_Buffer_PneumaticOn.TabIndex = 59;
            this.btn_CellInput_Buffer_PneumaticOn.Text = "공압 온";
            this.btn_CellInput_Buffer_PneumaticOn.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_Buffer_PneumaticOff
            // 
            this.btn_CellInput_Buffer_PneumaticOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_Buffer_PneumaticOff.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CellInput_Buffer_PneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_Buffer_PneumaticOff.Location = new System.Drawing.Point(103, 50);
            this.btn_CellInput_Buffer_PneumaticOff.Name = "btn_CellInput_Buffer_PneumaticOff";
            this.btn_CellInput_Buffer_PneumaticOff.Size = new System.Drawing.Size(94, 40);
            this.btn_CellInput_Buffer_PneumaticOff.TabIndex = 61;
            this.btn_CellInput_Buffer_PneumaticOff.Text = "공압 오프";
            this.btn_CellInput_Buffer_PneumaticOff.UseVisualStyleBackColor = false;
            // 
            // gbox_CellInput_Ionizer
            // 
            this.gbox_CellInput_Ionizer.Controls.Add(this.btn_CellInput_DestructionOff);
            this.gbox_CellInput_Ionizer.Controls.Add(this.btn_CellInput_IonizerOff);
            this.gbox_CellInput_Ionizer.Controls.Add(this.btn_CellInput_DestructionOn);
            this.gbox_CellInput_Ionizer.Controls.Add(this.btn_CellInput_IonizerOn);
            this.gbox_CellInput_Ionizer.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CellInput_Ionizer.ForeColor = System.Drawing.Color.White;
            this.gbox_CellInput_Ionizer.Location = new System.Drawing.Point(1484, 223);
            this.gbox_CellInput_Ionizer.Name = "gbox_CellInput_Ionizer";
            this.gbox_CellInput_Ionizer.Size = new System.Drawing.Size(212, 181);
            this.gbox_CellInput_Ionizer.TabIndex = 50;
            this.gbox_CellInput_Ionizer.TabStop = false;
            this.gbox_CellInput_Ionizer.Text = "     이오나이저     ";
            // 
            // btn_CellInput_DestructionOff
            // 
            this.btn_CellInput_DestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_DestructionOff.Location = new System.Drawing.Point(107, 103);
            this.btn_CellInput_DestructionOff.Name = "btn_CellInput_DestructionOff";
            this.btn_CellInput_DestructionOff.Size = new System.Drawing.Size(99, 72);
            this.btn_CellInput_DestructionOff.TabIndex = 6;
            this.btn_CellInput_DestructionOff.Text = "파기 오프";
            this.btn_CellInput_DestructionOff.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_IonizerOff
            // 
            this.btn_CellInput_IonizerOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_IonizerOff.Location = new System.Drawing.Point(107, 28);
            this.btn_CellInput_IonizerOff.Name = "btn_CellInput_IonizerOff";
            this.btn_CellInput_IonizerOff.Size = new System.Drawing.Size(99, 72);
            this.btn_CellInput_IonizerOff.TabIndex = 5;
            this.btn_CellInput_IonizerOff.Text = "이오나이저\r\n오프";
            this.btn_CellInput_IonizerOff.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_DestructionOn
            // 
            this.btn_CellInput_DestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_DestructionOn.Location = new System.Drawing.Point(6, 103);
            this.btn_CellInput_DestructionOn.Name = "btn_CellInput_DestructionOn";
            this.btn_CellInput_DestructionOn.Size = new System.Drawing.Size(99, 72);
            this.btn_CellInput_DestructionOn.TabIndex = 4;
            this.btn_CellInput_DestructionOn.Text = "파기 온";
            this.btn_CellInput_DestructionOn.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_IonizerOn
            // 
            this.btn_CellInput_IonizerOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_IonizerOn.Location = new System.Drawing.Point(6, 28);
            this.btn_CellInput_IonizerOn.Name = "btn_CellInput_IonizerOn";
            this.btn_CellInput_IonizerOn.Size = new System.Drawing.Size(99, 72);
            this.btn_CellInput_IonizerOn.TabIndex = 3;
            this.btn_CellInput_IonizerOn.Text = "이오나이저\r\n온";
            this.btn_CellInput_IonizerOn.UseVisualStyleBackColor = false;
            // 
            // gbox_CellInput_Move
            // 
            this.gbox_CellInput_Move.Controls.Add(this.gbox_CellInput_MoveCenter);
            this.gbox_CellInput_Move.Controls.Add(this.gbox_CellInput_MoveB);
            this.gbox_CellInput_Move.Controls.Add(this.gbox_CellInput_MoveA);
            this.gbox_CellInput_Move.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CellInput_Move.ForeColor = System.Drawing.Color.White;
            this.gbox_CellInput_Move.Location = new System.Drawing.Point(792, 595);
            this.gbox_CellInput_Move.Name = "gbox_CellInput_Move";
            this.gbox_CellInput_Move.Size = new System.Drawing.Size(904, 179);
            this.gbox_CellInput_Move.TabIndex = 53;
            this.gbox_CellInput_Move.TabStop = false;
            this.gbox_CellInput_Move.Text = "     이동     ";
            // 
            // gbox_CellInput_MoveCenter
            // 
            this.gbox_CellInput_MoveCenter.Controls.Add(this.tableLayoutPanel69);
            this.gbox_CellInput_MoveCenter.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CellInput_MoveCenter.ForeColor = System.Drawing.Color.White;
            this.gbox_CellInput_MoveCenter.Location = new System.Drawing.Point(692, 21);
            this.gbox_CellInput_MoveCenter.Name = "gbox_CellInput_MoveCenter";
            this.gbox_CellInput_MoveCenter.Size = new System.Drawing.Size(206, 152);
            this.gbox_CellInput_MoveCenter.TabIndex = 32;
            this.gbox_CellInput_MoveCenter.TabStop = false;
            this.gbox_CellInput_MoveCenter.Text = "     중간    ";
            // 
            // tableLayoutPanel69
            // 
            this.tableLayoutPanel69.ColumnCount = 2;
            this.tableLayoutPanel69.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel69.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel69.Controls.Add(this.btn_CellInput_MoveCenter_ABuffer, 0, 1);
            this.tableLayoutPanel69.Controls.Add(this.btn_CellInput_MoveCenter_BBuffer, 0, 1);
            this.tableLayoutPanel69.Controls.Add(this.btn_CellInput_MoveCenter_Y1CstMid, 0, 0);
            this.tableLayoutPanel69.Controls.Add(this.btn_CellInput_MoveCenter_Y2CstMid, 1, 0);
            this.tableLayoutPanel69.Location = new System.Drawing.Point(3, 28);
            this.tableLayoutPanel69.Name = "tableLayoutPanel69";
            this.tableLayoutPanel69.RowCount = 2;
            this.tableLayoutPanel69.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel69.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel69.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel69.Size = new System.Drawing.Size(200, 115);
            this.tableLayoutPanel69.TabIndex = 0;
            // 
            // btn_CellInput_MoveCenter_ABuffer
            // 
            this.btn_CellInput_MoveCenter_ABuffer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveCenter_ABuffer.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveCenter_ABuffer.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveCenter_ABuffer.Location = new System.Drawing.Point(3, 60);
            this.btn_CellInput_MoveCenter_ABuffer.Name = "btn_CellInput_MoveCenter_ABuffer";
            this.btn_CellInput_MoveCenter_ABuffer.Size = new System.Drawing.Size(94, 52);
            this.btn_CellInput_MoveCenter_ABuffer.TabIndex = 60;
            this.btn_CellInput_MoveCenter_ABuffer.Text = "A\r\nBUFFER";
            this.btn_CellInput_MoveCenter_ABuffer.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveCenter_BBuffer
            // 
            this.btn_CellInput_MoveCenter_BBuffer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveCenter_BBuffer.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveCenter_BBuffer.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveCenter_BBuffer.Location = new System.Drawing.Point(103, 60);
            this.btn_CellInput_MoveCenter_BBuffer.Name = "btn_CellInput_MoveCenter_BBuffer";
            this.btn_CellInput_MoveCenter_BBuffer.Size = new System.Drawing.Size(94, 52);
            this.btn_CellInput_MoveCenter_BBuffer.TabIndex = 59;
            this.btn_CellInput_MoveCenter_BBuffer.Text = "B\r\nBUFFER";
            this.btn_CellInput_MoveCenter_BBuffer.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveCenter_Y1CstMid
            // 
            this.btn_CellInput_MoveCenter_Y1CstMid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveCenter_Y1CstMid.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveCenter_Y1CstMid.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveCenter_Y1CstMid.Location = new System.Drawing.Point(3, 3);
            this.btn_CellInput_MoveCenter_Y1CstMid.Name = "btn_CellInput_MoveCenter_Y1CstMid";
            this.btn_CellInput_MoveCenter_Y1CstMid.Size = new System.Drawing.Size(94, 51);
            this.btn_CellInput_MoveCenter_Y1CstMid.TabIndex = 57;
            this.btn_CellInput_MoveCenter_Y1CstMid.Text = "Y1\r\n카세트\r\n중간";
            this.btn_CellInput_MoveCenter_Y1CstMid.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveCenter_Y2CstMid
            // 
            this.btn_CellInput_MoveCenter_Y2CstMid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveCenter_Y2CstMid.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveCenter_Y2CstMid.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveCenter_Y2CstMid.Location = new System.Drawing.Point(103, 3);
            this.btn_CellInput_MoveCenter_Y2CstMid.Name = "btn_CellInput_MoveCenter_Y2CstMid";
            this.btn_CellInput_MoveCenter_Y2CstMid.Size = new System.Drawing.Size(94, 51);
            this.btn_CellInput_MoveCenter_Y2CstMid.TabIndex = 58;
            this.btn_CellInput_MoveCenter_Y2CstMid.Text = "Y2\r\n카세트\r\n중간";
            this.btn_CellInput_MoveCenter_Y2CstMid.UseVisualStyleBackColor = false;
            // 
            // gbox_CellInput_MoveB
            // 
            this.gbox_CellInput_MoveB.Controls.Add(this.tableLayoutPanel68);
            this.gbox_CellInput_MoveB.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CellInput_MoveB.ForeColor = System.Drawing.Color.White;
            this.gbox_CellInput_MoveB.Location = new System.Drawing.Point(346, 21);
            this.gbox_CellInput_MoveB.Name = "gbox_CellInput_MoveB";
            this.gbox_CellInput_MoveB.Size = new System.Drawing.Size(340, 152);
            this.gbox_CellInput_MoveB.TabIndex = 31;
            this.gbox_CellInput_MoveB.TabStop = false;
            this.gbox_CellInput_MoveB.Text = "     B    ";
            // 
            // tableLayoutPanel68
            // 
            this.tableLayoutPanel68.ColumnCount = 4;
            this.tableLayoutPanel68.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel68.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel68.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel68.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel68.Controls.Add(this.btn_CellInput_MoveB_X2BUld, 0, 1);
            this.tableLayoutPanel68.Controls.Add(this.btn_CellInput_MoveB_Y2BUld, 0, 1);
            this.tableLayoutPanel68.Controls.Add(this.btn_CellInput_MoveB_X2BCstMid, 0, 1);
            this.tableLayoutPanel68.Controls.Add(this.btn_CellInput_MoveB_X2BCstWait, 0, 1);
            this.tableLayoutPanel68.Controls.Add(this.btn_CellInput_MoveB_X1AUld, 0, 0);
            this.tableLayoutPanel68.Controls.Add(this.btn_CellInput_MoveB_Y1BUld, 1, 0);
            this.tableLayoutPanel68.Controls.Add(this.btn_CellInput_MoveB_X1BCstMId, 2, 0);
            this.tableLayoutPanel68.Controls.Add(this.btn_CellInput_MoveB_X1BCstWait, 3, 0);
            this.tableLayoutPanel68.Location = new System.Drawing.Point(9, 28);
            this.tableLayoutPanel68.Name = "tableLayoutPanel68";
            this.tableLayoutPanel68.RowCount = 2;
            this.tableLayoutPanel68.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel68.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel68.Size = new System.Drawing.Size(318, 118);
            this.tableLayoutPanel68.TabIndex = 56;
            // 
            // btn_CellInput_MoveB_X2BUld
            // 
            this.btn_CellInput_MoveB_X2BUld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveB_X2BUld.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveB_X2BUld.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveB_X2BUld.Location = new System.Drawing.Point(3, 62);
            this.btn_CellInput_MoveB_X2BUld.Name = "btn_CellInput_MoveB_X2BUld";
            this.btn_CellInput_MoveB_X2BUld.Size = new System.Drawing.Size(73, 53);
            this.btn_CellInput_MoveB_X2BUld.TabIndex = 65;
            this.btn_CellInput_MoveB_X2BUld.Text = "X2\r\nB 언로딩\r\n이재기";
            this.btn_CellInput_MoveB_X2BUld.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveB_Y2BUld
            // 
            this.btn_CellInput_MoveB_Y2BUld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveB_Y2BUld.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveB_Y2BUld.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveB_Y2BUld.Location = new System.Drawing.Point(82, 62);
            this.btn_CellInput_MoveB_Y2BUld.Name = "btn_CellInput_MoveB_Y2BUld";
            this.btn_CellInput_MoveB_Y2BUld.Size = new System.Drawing.Size(73, 53);
            this.btn_CellInput_MoveB_Y2BUld.TabIndex = 64;
            this.btn_CellInput_MoveB_Y2BUld.Text = "Y2\r\nB 언로딩\r\n이재기";
            this.btn_CellInput_MoveB_Y2BUld.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveB_X2BCstMid
            // 
            this.btn_CellInput_MoveB_X2BCstMid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveB_X2BCstMid.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveB_X2BCstMid.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveB_X2BCstMid.Location = new System.Drawing.Point(161, 62);
            this.btn_CellInput_MoveB_X2BCstMid.Name = "btn_CellInput_MoveB_X2BCstMid";
            this.btn_CellInput_MoveB_X2BCstMid.Size = new System.Drawing.Size(73, 53);
            this.btn_CellInput_MoveB_X2BCstMid.TabIndex = 63;
            this.btn_CellInput_MoveB_X2BCstMid.Text = "X2\r\nB 카세트\r\n중간";
            this.btn_CellInput_MoveB_X2BCstMid.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveB_X2BCstWait
            // 
            this.btn_CellInput_MoveB_X2BCstWait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveB_X2BCstWait.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveB_X2BCstWait.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveB_X2BCstWait.Location = new System.Drawing.Point(240, 62);
            this.btn_CellInput_MoveB_X2BCstWait.Name = "btn_CellInput_MoveB_X2BCstWait";
            this.btn_CellInput_MoveB_X2BCstWait.Size = new System.Drawing.Size(73, 53);
            this.btn_CellInput_MoveB_X2BCstWait.TabIndex = 62;
            this.btn_CellInput_MoveB_X2BCstWait.Text = "X2\r\nB 카세트\r\n대기";
            this.btn_CellInput_MoveB_X2BCstWait.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveB_X1AUld
            // 
            this.btn_CellInput_MoveB_X1AUld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveB_X1AUld.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveB_X1AUld.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveB_X1AUld.Location = new System.Drawing.Point(3, 3);
            this.btn_CellInput_MoveB_X1AUld.Name = "btn_CellInput_MoveB_X1AUld";
            this.btn_CellInput_MoveB_X1AUld.Size = new System.Drawing.Size(73, 53);
            this.btn_CellInput_MoveB_X1AUld.TabIndex = 58;
            this.btn_CellInput_MoveB_X1AUld.Text = "X1\r\nB 언로딩\r\n이재기";
            this.btn_CellInput_MoveB_X1AUld.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveB_Y1BUld
            // 
            this.btn_CellInput_MoveB_Y1BUld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveB_Y1BUld.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveB_Y1BUld.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveB_Y1BUld.Location = new System.Drawing.Point(82, 3);
            this.btn_CellInput_MoveB_Y1BUld.Name = "btn_CellInput_MoveB_Y1BUld";
            this.btn_CellInput_MoveB_Y1BUld.Size = new System.Drawing.Size(73, 53);
            this.btn_CellInput_MoveB_Y1BUld.TabIndex = 59;
            this.btn_CellInput_MoveB_Y1BUld.Text = "Y1\r\nB 언로딩\r\n이재기";
            this.btn_CellInput_MoveB_Y1BUld.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveB_X1BCstMId
            // 
            this.btn_CellInput_MoveB_X1BCstMId.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveB_X1BCstMId.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveB_X1BCstMId.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveB_X1BCstMId.Location = new System.Drawing.Point(161, 3);
            this.btn_CellInput_MoveB_X1BCstMId.Name = "btn_CellInput_MoveB_X1BCstMId";
            this.btn_CellInput_MoveB_X1BCstMId.Size = new System.Drawing.Size(73, 53);
            this.btn_CellInput_MoveB_X1BCstMId.TabIndex = 60;
            this.btn_CellInput_MoveB_X1BCstMId.Text = "X1\r\nB 카세트\r\n중간";
            this.btn_CellInput_MoveB_X1BCstMId.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveB_X1BCstWait
            // 
            this.btn_CellInput_MoveB_X1BCstWait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveB_X1BCstWait.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveB_X1BCstWait.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveB_X1BCstWait.Location = new System.Drawing.Point(240, 3);
            this.btn_CellInput_MoveB_X1BCstWait.Name = "btn_CellInput_MoveB_X1BCstWait";
            this.btn_CellInput_MoveB_X1BCstWait.Size = new System.Drawing.Size(73, 53);
            this.btn_CellInput_MoveB_X1BCstWait.TabIndex = 61;
            this.btn_CellInput_MoveB_X1BCstWait.Text = "X1\r\nB 카세트\r\n대기";
            this.btn_CellInput_MoveB_X1BCstWait.UseVisualStyleBackColor = false;
            // 
            // gbox_CellInput_MoveA
            // 
            this.gbox_CellInput_MoveA.Controls.Add(this.tableLayoutPanel67);
            this.gbox_CellInput_MoveA.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CellInput_MoveA.ForeColor = System.Drawing.Color.White;
            this.gbox_CellInput_MoveA.Location = new System.Drawing.Point(6, 21);
            this.gbox_CellInput_MoveA.Name = "gbox_CellInput_MoveA";
            this.gbox_CellInput_MoveA.Size = new System.Drawing.Size(334, 152);
            this.gbox_CellInput_MoveA.TabIndex = 30;
            this.gbox_CellInput_MoveA.TabStop = false;
            this.gbox_CellInput_MoveA.Text = "     A    ";
            // 
            // tableLayoutPanel67
            // 
            this.tableLayoutPanel67.ColumnCount = 4;
            this.tableLayoutPanel67.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel67.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel67.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel67.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel67.Controls.Add(this.btn_CellInput_MoveA_X2AUld, 0, 1);
            this.tableLayoutPanel67.Controls.Add(this.btn_CellInput_MoveA_Y2AUld, 0, 1);
            this.tableLayoutPanel67.Controls.Add(this.btn_CellInput_MoveA_X2ACstMid, 0, 1);
            this.tableLayoutPanel67.Controls.Add(this.btn_CellInput_MoveA_X2ACstWait, 0, 1);
            this.tableLayoutPanel67.Controls.Add(this.btn_CellInput_MoveA_X1AUld, 0, 0);
            this.tableLayoutPanel67.Controls.Add(this.btn_CellInput_MoveA_Y1AUld, 1, 0);
            this.tableLayoutPanel67.Controls.Add(this.btn_CellInput_MoveA_X1ACstMid, 2, 0);
            this.tableLayoutPanel67.Controls.Add(this.btn_CellInput_MoveA_X1ACstWait, 3, 0);
            this.tableLayoutPanel67.Location = new System.Drawing.Point(7, 28);
            this.tableLayoutPanel67.Name = "tableLayoutPanel67";
            this.tableLayoutPanel67.RowCount = 2;
            this.tableLayoutPanel67.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel67.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel67.Size = new System.Drawing.Size(318, 118);
            this.tableLayoutPanel67.TabIndex = 55;
            // 
            // btn_CellInput_MoveA_X2AUld
            // 
            this.btn_CellInput_MoveA_X2AUld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveA_X2AUld.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveA_X2AUld.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveA_X2AUld.Location = new System.Drawing.Point(3, 62);
            this.btn_CellInput_MoveA_X2AUld.Name = "btn_CellInput_MoveA_X2AUld";
            this.btn_CellInput_MoveA_X2AUld.Size = new System.Drawing.Size(73, 53);
            this.btn_CellInput_MoveA_X2AUld.TabIndex = 65;
            this.btn_CellInput_MoveA_X2AUld.Text = "X2\r\nA 언로딩\r\n이재기";
            this.btn_CellInput_MoveA_X2AUld.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveA_Y2AUld
            // 
            this.btn_CellInput_MoveA_Y2AUld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveA_Y2AUld.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveA_Y2AUld.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveA_Y2AUld.Location = new System.Drawing.Point(82, 62);
            this.btn_CellInput_MoveA_Y2AUld.Name = "btn_CellInput_MoveA_Y2AUld";
            this.btn_CellInput_MoveA_Y2AUld.Size = new System.Drawing.Size(73, 53);
            this.btn_CellInput_MoveA_Y2AUld.TabIndex = 64;
            this.btn_CellInput_MoveA_Y2AUld.Text = "Y2\r\nA 언로딩\r\n이재기";
            this.btn_CellInput_MoveA_Y2AUld.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveA_X2ACstMid
            // 
            this.btn_CellInput_MoveA_X2ACstMid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveA_X2ACstMid.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveA_X2ACstMid.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveA_X2ACstMid.Location = new System.Drawing.Point(161, 62);
            this.btn_CellInput_MoveA_X2ACstMid.Name = "btn_CellInput_MoveA_X2ACstMid";
            this.btn_CellInput_MoveA_X2ACstMid.Size = new System.Drawing.Size(73, 53);
            this.btn_CellInput_MoveA_X2ACstMid.TabIndex = 63;
            this.btn_CellInput_MoveA_X2ACstMid.Text = "X2\r\nA 카세트\r\n중간";
            this.btn_CellInput_MoveA_X2ACstMid.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveA_X2ACstWait
            // 
            this.btn_CellInput_MoveA_X2ACstWait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveA_X2ACstWait.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveA_X2ACstWait.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveA_X2ACstWait.Location = new System.Drawing.Point(240, 62);
            this.btn_CellInput_MoveA_X2ACstWait.Name = "btn_CellInput_MoveA_X2ACstWait";
            this.btn_CellInput_MoveA_X2ACstWait.Size = new System.Drawing.Size(73, 53);
            this.btn_CellInput_MoveA_X2ACstWait.TabIndex = 62;
            this.btn_CellInput_MoveA_X2ACstWait.Text = "X2\r\nA 카세트\r\n대기";
            this.btn_CellInput_MoveA_X2ACstWait.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveA_X1AUld
            // 
            this.btn_CellInput_MoveA_X1AUld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveA_X1AUld.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveA_X1AUld.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveA_X1AUld.Location = new System.Drawing.Point(3, 3);
            this.btn_CellInput_MoveA_X1AUld.Name = "btn_CellInput_MoveA_X1AUld";
            this.btn_CellInput_MoveA_X1AUld.Size = new System.Drawing.Size(73, 53);
            this.btn_CellInput_MoveA_X1AUld.TabIndex = 58;
            this.btn_CellInput_MoveA_X1AUld.Text = "X1\r\nA 언로딩\r\n이재기";
            this.btn_CellInput_MoveA_X1AUld.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveA_Y1AUld
            // 
            this.btn_CellInput_MoveA_Y1AUld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveA_Y1AUld.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveA_Y1AUld.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveA_Y1AUld.Location = new System.Drawing.Point(82, 3);
            this.btn_CellInput_MoveA_Y1AUld.Name = "btn_CellInput_MoveA_Y1AUld";
            this.btn_CellInput_MoveA_Y1AUld.Size = new System.Drawing.Size(73, 53);
            this.btn_CellInput_MoveA_Y1AUld.TabIndex = 59;
            this.btn_CellInput_MoveA_Y1AUld.Text = "Y1\r\nA 언로딩\r\n이재기";
            this.btn_CellInput_MoveA_Y1AUld.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveA_X1ACstMid
            // 
            this.btn_CellInput_MoveA_X1ACstMid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveA_X1ACstMid.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveA_X1ACstMid.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveA_X1ACstMid.Location = new System.Drawing.Point(161, 3);
            this.btn_CellInput_MoveA_X1ACstMid.Name = "btn_CellInput_MoveA_X1ACstMid";
            this.btn_CellInput_MoveA_X1ACstMid.Size = new System.Drawing.Size(73, 53);
            this.btn_CellInput_MoveA_X1ACstMid.TabIndex = 60;
            this.btn_CellInput_MoveA_X1ACstMid.Text = "X1\r\nA 카세트\r\n중간";
            this.btn_CellInput_MoveA_X1ACstMid.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveA_X1ACstWait
            // 
            this.btn_CellInput_MoveA_X1ACstWait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveA_X1ACstWait.Font = new System.Drawing.Font("맑은 고딕", 8F, System.Drawing.FontStyle.Bold);
            this.btn_CellInput_MoveA_X1ACstWait.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveA_X1ACstWait.Location = new System.Drawing.Point(240, 3);
            this.btn_CellInput_MoveA_X1ACstWait.Name = "btn_CellInput_MoveA_X1ACstWait";
            this.btn_CellInput_MoveA_X1ACstWait.Size = new System.Drawing.Size(73, 53);
            this.btn_CellInput_MoveA_X1ACstWait.TabIndex = 61;
            this.btn_CellInput_MoveA_X1ACstWait.Text = "X1\r\nA 카세트\r\n대기";
            this.btn_CellInput_MoveA_X1ACstWait.UseVisualStyleBackColor = false;
            // 
            // gbox_CellInput_B
            // 
            this.gbox_CellInput_B.Controls.Add(this.tableLayoutPanel23);
            this.gbox_CellInput_B.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CellInput_B.ForeColor = System.Drawing.Color.White;
            this.gbox_CellInput_B.Location = new System.Drawing.Point(1138, 410);
            this.gbox_CellInput_B.Name = "gbox_CellInput_B";
            this.gbox_CellInput_B.Size = new System.Drawing.Size(340, 179);
            this.gbox_CellInput_B.TabIndex = 52;
            this.gbox_CellInput_B.TabStop = false;
            this.gbox_CellInput_B.Text = "     B     ";
            // 
            // tableLayoutPanel23
            // 
            this.tableLayoutPanel23.ColumnCount = 2;
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel23.Controls.Add(this.btn_CellInput_B_DestructionOn, 0, 1);
            this.tableLayoutPanel23.Controls.Add(this.btn_CellInput_B_DestructionOff, 0, 1);
            this.tableLayoutPanel23.Controls.Add(this.btn_CellInput_B_PneumaticOff, 1, 0);
            this.tableLayoutPanel23.Controls.Add(this.btn_CellInput_B_PneumaticOn, 0, 0);
            this.tableLayoutPanel23.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel23.Name = "tableLayoutPanel23";
            this.tableLayoutPanel23.RowCount = 2;
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel23.Size = new System.Drawing.Size(328, 145);
            this.tableLayoutPanel23.TabIndex = 55;
            // 
            // btn_CellInput_B_DestructionOn
            // 
            this.btn_CellInput_B_DestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_B_DestructionOn.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CellInput_B_DestructionOn.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_B_DestructionOn.Location = new System.Drawing.Point(3, 75);
            this.btn_CellInput_B_DestructionOn.Name = "btn_CellInput_B_DestructionOn";
            this.btn_CellInput_B_DestructionOn.Size = new System.Drawing.Size(158, 66);
            this.btn_CellInput_B_DestructionOn.TabIndex = 58;
            this.btn_CellInput_B_DestructionOn.Text = "파기 온";
            this.btn_CellInput_B_DestructionOn.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_B_DestructionOff
            // 
            this.btn_CellInput_B_DestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_B_DestructionOff.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CellInput_B_DestructionOff.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_B_DestructionOff.Location = new System.Drawing.Point(167, 75);
            this.btn_CellInput_B_DestructionOff.Name = "btn_CellInput_B_DestructionOff";
            this.btn_CellInput_B_DestructionOff.Size = new System.Drawing.Size(158, 66);
            this.btn_CellInput_B_DestructionOff.TabIndex = 56;
            this.btn_CellInput_B_DestructionOff.Text = "파기 오프";
            this.btn_CellInput_B_DestructionOff.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_B_PneumaticOff
            // 
            this.btn_CellInput_B_PneumaticOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_B_PneumaticOff.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CellInput_B_PneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_B_PneumaticOff.Location = new System.Drawing.Point(167, 3);
            this.btn_CellInput_B_PneumaticOff.Name = "btn_CellInput_B_PneumaticOff";
            this.btn_CellInput_B_PneumaticOff.Size = new System.Drawing.Size(158, 66);
            this.btn_CellInput_B_PneumaticOff.TabIndex = 55;
            this.btn_CellInput_B_PneumaticOff.Text = "공압 오프";
            this.btn_CellInput_B_PneumaticOff.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_B_PneumaticOn
            // 
            this.btn_CellInput_B_PneumaticOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_B_PneumaticOn.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CellInput_B_PneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_B_PneumaticOn.Location = new System.Drawing.Point(3, 3);
            this.btn_CellInput_B_PneumaticOn.Name = "btn_CellInput_B_PneumaticOn";
            this.btn_CellInput_B_PneumaticOn.Size = new System.Drawing.Size(158, 66);
            this.btn_CellInput_B_PneumaticOn.TabIndex = 57;
            this.btn_CellInput_B_PneumaticOn.Text = "공압 온";
            this.btn_CellInput_B_PneumaticOn.UseVisualStyleBackColor = false;
            // 
            // gbox_CellInput_A
            // 
            this.gbox_CellInput_A.Controls.Add(this.tableLayoutPanel22);
            this.gbox_CellInput_A.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CellInput_A.ForeColor = System.Drawing.Color.White;
            this.gbox_CellInput_A.Location = new System.Drawing.Point(792, 410);
            this.gbox_CellInput_A.Name = "gbox_CellInput_A";
            this.gbox_CellInput_A.Size = new System.Drawing.Size(340, 179);
            this.gbox_CellInput_A.TabIndex = 51;
            this.gbox_CellInput_A.TabStop = false;
            this.gbox_CellInput_A.Text = "     A    ";
            // 
            // tableLayoutPanel22
            // 
            this.tableLayoutPanel22.ColumnCount = 2;
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel22.Controls.Add(this.btn_CellInput_A_DestructionOn, 0, 1);
            this.tableLayoutPanel22.Controls.Add(this.btn_CellInput_A_DestructionOff, 0, 1);
            this.tableLayoutPanel22.Controls.Add(this.btn_CellInput_A_PneumaticOff, 1, 0);
            this.tableLayoutPanel22.Controls.Add(this.btn_CellInput_A_PneumaticOn, 0, 0);
            this.tableLayoutPanel22.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel22.Name = "tableLayoutPanel22";
            this.tableLayoutPanel22.RowCount = 2;
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel22.Size = new System.Drawing.Size(328, 145);
            this.tableLayoutPanel22.TabIndex = 55;
            // 
            // btn_CellInput_A_DestructionOn
            // 
            this.btn_CellInput_A_DestructionOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_A_DestructionOn.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CellInput_A_DestructionOn.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_A_DestructionOn.Location = new System.Drawing.Point(3, 75);
            this.btn_CellInput_A_DestructionOn.Name = "btn_CellInput_A_DestructionOn";
            this.btn_CellInput_A_DestructionOn.Size = new System.Drawing.Size(158, 66);
            this.btn_CellInput_A_DestructionOn.TabIndex = 58;
            this.btn_CellInput_A_DestructionOn.Text = "파기 온";
            this.btn_CellInput_A_DestructionOn.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_A_DestructionOff
            // 
            this.btn_CellInput_A_DestructionOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_A_DestructionOff.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CellInput_A_DestructionOff.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_A_DestructionOff.Location = new System.Drawing.Point(167, 75);
            this.btn_CellInput_A_DestructionOff.Name = "btn_CellInput_A_DestructionOff";
            this.btn_CellInput_A_DestructionOff.Size = new System.Drawing.Size(158, 66);
            this.btn_CellInput_A_DestructionOff.TabIndex = 56;
            this.btn_CellInput_A_DestructionOff.Text = "파기 오프";
            this.btn_CellInput_A_DestructionOff.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_A_PneumaticOff
            // 
            this.btn_CellInput_A_PneumaticOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_A_PneumaticOff.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CellInput_A_PneumaticOff.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_A_PneumaticOff.Location = new System.Drawing.Point(167, 3);
            this.btn_CellInput_A_PneumaticOff.Name = "btn_CellInput_A_PneumaticOff";
            this.btn_CellInput_A_PneumaticOff.Size = new System.Drawing.Size(158, 66);
            this.btn_CellInput_A_PneumaticOff.TabIndex = 55;
            this.btn_CellInput_A_PneumaticOff.Text = "공압 오프";
            this.btn_CellInput_A_PneumaticOff.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_A_PneumaticOn
            // 
            this.btn_CellInput_A_PneumaticOn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_A_PneumaticOn.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CellInput_A_PneumaticOn.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_A_PneumaticOn.Location = new System.Drawing.Point(3, 3);
            this.btn_CellInput_A_PneumaticOn.Name = "btn_CellInput_A_PneumaticOn";
            this.btn_CellInput_A_PneumaticOn.Size = new System.Drawing.Size(158, 66);
            this.btn_CellInput_A_PneumaticOn.TabIndex = 57;
            this.btn_CellInput_A_PneumaticOn.Text = "공압 온";
            this.btn_CellInput_A_PneumaticOn.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_Save
            // 
            this.btn_CellInput_Save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_Save.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CellInput_Save.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_Save.Location = new System.Drawing.Point(1482, 783);
            this.btn_CellInput_Save.Name = "btn_CellInput_Save";
            this.btn_CellInput_Save.Size = new System.Drawing.Size(228, 28);
            this.btn_CellInput_Save.TabIndex = 50;
            this.btn_CellInput_Save.Text = "저장";
            this.btn_CellInput_Save.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_SpeedSetting
            // 
            this.btn_CellInput_SpeedSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_SpeedSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CellInput_SpeedSetting.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_SpeedSetting.Location = new System.Drawing.Point(1291, 315);
            this.btn_CellInput_SpeedSetting.Name = "btn_CellInput_SpeedSetting";
            this.btn_CellInput_SpeedSetting.Size = new System.Drawing.Size(90, 27);
            this.btn_CellInput_SpeedSetting.TabIndex = 48;
            this.btn_CellInput_SpeedSetting.Text = "속도 설정";
            this.btn_CellInput_SpeedSetting.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_AllSetting
            // 
            this.btn_CellInput_AllSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_AllSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CellInput_AllSetting.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_AllSetting.Location = new System.Drawing.Point(1387, 273);
            this.btn_CellInput_AllSetting.Name = "btn_CellInput_AllSetting";
            this.btn_CellInput_AllSetting.Size = new System.Drawing.Size(90, 27);
            this.btn_CellInput_AllSetting.TabIndex = 47;
            this.btn_CellInput_AllSetting.Text = "전체 설정";
            this.btn_CellInput_AllSetting.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_LocationSetting
            // 
            this.btn_CellInput_LocationSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_LocationSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CellInput_LocationSetting.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_LocationSetting.Location = new System.Drawing.Point(1071, 315);
            this.btn_CellInput_LocationSetting.Name = "btn_CellInput_LocationSetting";
            this.btn_CellInput_LocationSetting.Size = new System.Drawing.Size(90, 27);
            this.btn_CellInput_LocationSetting.TabIndex = 46;
            this.btn_CellInput_LocationSetting.Text = "위치 설정";
            this.btn_CellInput_LocationSetting.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_MoveLocation
            // 
            this.btn_CellInput_MoveLocation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_MoveLocation.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CellInput_MoveLocation.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_MoveLocation.Location = new System.Drawing.Point(975, 315);
            this.btn_CellInput_MoveLocation.Name = "btn_CellInput_MoveLocation";
            this.btn_CellInput_MoveLocation.Size = new System.Drawing.Size(90, 27);
            this.btn_CellInput_MoveLocation.TabIndex = 45;
            this.btn_CellInput_MoveLocation.Text = "위치 이동";
            this.btn_CellInput_MoveLocation.UseVisualStyleBackColor = false;
            // 
            // tbox_CellInput_Location
            // 
            this.tbox_CellInput_Location.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_CellInput_Location.Location = new System.Drawing.Point(879, 316);
            this.tbox_CellInput_Location.Name = "tbox_CellInput_Location";
            this.tbox_CellInput_Location.Size = new System.Drawing.Size(90, 27);
            this.tbox_CellInput_Location.TabIndex = 44;
            // 
            // tbox_CellInput_Speed
            // 
            this.tbox_CellInput_Speed.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_CellInput_Speed.Location = new System.Drawing.Point(1291, 273);
            this.tbox_CellInput_Speed.Name = "tbox_CellInput_Speed";
            this.tbox_CellInput_Speed.Size = new System.Drawing.Size(90, 27);
            this.tbox_CellInput_Speed.TabIndex = 43;
            // 
            // tbox_CellInput_CurrentLocation
            // 
            this.tbox_CellInput_CurrentLocation.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_CellInput_CurrentLocation.Location = new System.Drawing.Point(1084, 274);
            this.tbox_CellInput_CurrentLocation.Name = "tbox_CellInput_CurrentLocation";
            this.tbox_CellInput_CurrentLocation.Size = new System.Drawing.Size(90, 27);
            this.tbox_CellInput_CurrentLocation.TabIndex = 42;
            // 
            // tbox_CellInput_SelectedShaft
            // 
            this.tbox_CellInput_SelectedShaft.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_CellInput_SelectedShaft.Location = new System.Drawing.Point(879, 273);
            this.tbox_CellInput_SelectedShaft.Name = "tbox_CellInput_SelectedShaft";
            this.tbox_CellInput_SelectedShaft.Size = new System.Drawing.Size(90, 27);
            this.tbox_CellInput_SelectedShaft.TabIndex = 41;
            // 
            // lbl_CellInput_Location
            // 
            this.lbl_CellInput_Location.AutoSize = true;
            this.lbl_CellInput_Location.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CellInput_Location.ForeColor = System.Drawing.Color.White;
            this.lbl_CellInput_Location.Location = new System.Drawing.Point(801, 316);
            this.lbl_CellInput_Location.Name = "lbl_CellInput_Location";
            this.lbl_CellInput_Location.Size = new System.Drawing.Size(84, 21);
            this.lbl_CellInput_Location.TabIndex = 40;
            this.lbl_CellInput_Location.Text = "위치[mm]";
            // 
            // lbl_CellInput_Speed
            // 
            this.lbl_CellInput_Speed.AutoSize = true;
            this.lbl_CellInput_Speed.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CellInput_Speed.ForeColor = System.Drawing.Color.White;
            this.lbl_CellInput_Speed.Location = new System.Drawing.Point(1179, 275);
            this.lbl_CellInput_Speed.Name = "lbl_CellInput_Speed";
            this.lbl_CellInput_Speed.Size = new System.Drawing.Size(115, 21);
            this.lbl_CellInput_Speed.TabIndex = 39;
            this.lbl_CellInput_Speed.Text = "속도[mm/sec]";
            // 
            // lbl_CellInput_SelectedShaft
            // 
            this.lbl_CellInput_SelectedShaft.AutoSize = true;
            this.lbl_CellInput_SelectedShaft.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CellInput_SelectedShaft.ForeColor = System.Drawing.Color.White;
            this.lbl_CellInput_SelectedShaft.Location = new System.Drawing.Point(805, 274);
            this.lbl_CellInput_SelectedShaft.Name = "lbl_CellInput_SelectedShaft";
            this.lbl_CellInput_SelectedShaft.Size = new System.Drawing.Size(80, 21);
            this.lbl_CellInput_SelectedShaft.TabIndex = 38;
            this.lbl_CellInput_SelectedShaft.Text = "선택된 축";
            // 
            // tbox_CellInput_LocationSetting
            // 
            this.tbox_CellInput_LocationSetting.BackColor = System.Drawing.SystemColors.Highlight;
            this.tbox_CellInput_LocationSetting.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.tbox_CellInput_LocationSetting.ForeColor = System.Drawing.Color.White;
            this.tbox_CellInput_LocationSetting.Location = new System.Drawing.Point(792, 223);
            this.tbox_CellInput_LocationSetting.Name = "tbox_CellInput_LocationSetting";
            this.tbox_CellInput_LocationSetting.ReadOnly = true;
            this.tbox_CellInput_LocationSetting.Size = new System.Drawing.Size(89, 27);
            this.tbox_CellInput_LocationSetting.TabIndex = 37;
            this.tbox_CellInput_LocationSetting.Text = "위치값 설정";
            this.tbox_CellInput_LocationSetting.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbl_CellInput_CurrentLocation
            // 
            this.lbl_CellInput_CurrentLocation.AutoSize = true;
            this.lbl_CellInput_CurrentLocation.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CellInput_CurrentLocation.ForeColor = System.Drawing.Color.White;
            this.lbl_CellInput_CurrentLocation.Location = new System.Drawing.Point(975, 274);
            this.lbl_CellInput_CurrentLocation.Name = "lbl_CellInput_CurrentLocation";
            this.lbl_CellInput_CurrentLocation.Size = new System.Drawing.Size(116, 21);
            this.lbl_CellInput_CurrentLocation.TabIndex = 36;
            this.lbl_CellInput_CurrentLocation.Text = "현재위치[mm]";
            // 
            // btn_CellInput_GrabSwitch3
            // 
            this.btn_CellInput_GrabSwitch3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_GrabSwitch3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CellInput_GrabSwitch3.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_GrabSwitch3.Location = new System.Drawing.Point(1514, 6);
            this.btn_CellInput_GrabSwitch3.Name = "btn_CellInput_GrabSwitch3";
            this.btn_CellInput_GrabSwitch3.Size = new System.Drawing.Size(172, 23);
            this.btn_CellInput_GrabSwitch3.TabIndex = 35;
            this.btn_CellInput_GrabSwitch3.Text = "Grab Switch 3";
            this.btn_CellInput_GrabSwitch3.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_GrabSwitch2
            // 
            this.btn_CellInput_GrabSwitch2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_GrabSwitch2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CellInput_GrabSwitch2.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_GrabSwitch2.Location = new System.Drawing.Point(1336, 6);
            this.btn_CellInput_GrabSwitch2.Name = "btn_CellInput_GrabSwitch2";
            this.btn_CellInput_GrabSwitch2.Size = new System.Drawing.Size(172, 23);
            this.btn_CellInput_GrabSwitch2.TabIndex = 34;
            this.btn_CellInput_GrabSwitch2.Text = "Grab Switch 2";
            this.btn_CellInput_GrabSwitch2.UseVisualStyleBackColor = false;
            // 
            // btn_CellInput_GrabSwitch1
            // 
            this.btn_CellInput_GrabSwitch1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CellInput_GrabSwitch1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CellInput_GrabSwitch1.ForeColor = System.Drawing.Color.White;
            this.btn_CellInput_GrabSwitch1.Location = new System.Drawing.Point(1158, 6);
            this.btn_CellInput_GrabSwitch1.Name = "btn_CellInput_GrabSwitch1";
            this.btn_CellInput_GrabSwitch1.Size = new System.Drawing.Size(172, 23);
            this.btn_CellInput_GrabSwitch1.TabIndex = 33;
            this.btn_CellInput_GrabSwitch1.Text = "Grab Switch 1";
            this.btn_CellInput_GrabSwitch1.UseVisualStyleBackColor = false;
            // 
            // lv_CellInput
            // 
            this.lv_CellInput.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.col_CellInput_NamebyLocation,
            this.col_CellInput_Shaft,
            this.col_CellInput_Location,
            this.col_CellInput_Speed});
            this.lv_CellInput.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lv_CellInput.GridLines = true;
            this.lv_CellInput.Location = new System.Drawing.Point(17, 16);
            this.lv_CellInput.Name = "lv_CellInput";
            this.lv_CellInput.Size = new System.Drawing.Size(758, 734);
            this.lv_CellInput.TabIndex = 32;
            this.lv_CellInput.UseCompatibleStateImageBehavior = false;
            this.lv_CellInput.View = System.Windows.Forms.View.Details;
            // 
            // col_CellInput_NamebyLocation
            // 
            this.col_CellInput_NamebyLocation.Text = "위치별 명칭";
            this.col_CellInput_NamebyLocation.Width = 430;
            // 
            // col_CellInput_Shaft
            // 
            this.col_CellInput_Shaft.Text = "축";
            this.col_CellInput_Shaft.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // col_CellInput_Location
            // 
            this.col_CellInput_Location.Text = "위치[mm]";
            this.col_CellInput_Location.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_CellInput_Location.Width = 125;
            // 
            // col_CellInput_Speed
            // 
            this.col_CellInput_Speed.Text = "속도[mm/s]";
            this.col_CellInput_Speed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_CellInput_Speed.Width = 125;
            // 
            // tp_CasseteUnload
            // 
            this.tp_CasseteUnload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tp_CasseteUnload.Controls.Add(this.gbox_CasseteUnload_Move);
            this.tp_CasseteUnload.Controls.Add(this.gbox_CasseteUnload_B);
            this.tp_CasseteUnload.Controls.Add(this.gbox_CasseteUnload_A);
            this.tp_CasseteUnload.Controls.Add(this.btn_CasseteUnload_Save);
            this.tp_CasseteUnload.Controls.Add(this.gbox_CasseteUnload);
            this.tp_CasseteUnload.Controls.Add(this.btn_CasseteUnload_SpeedSetting);
            this.tp_CasseteUnload.Controls.Add(this.btn_CasseteUnload_AllSetting);
            this.tp_CasseteUnload.Controls.Add(this.btn_CasseteUnload_LocationSetting);
            this.tp_CasseteUnload.Controls.Add(this.btn_CasseteUnload_MoveLocation);
            this.tp_CasseteUnload.Controls.Add(this.tbox_CasseteUnload_Location);
            this.tp_CasseteUnload.Controls.Add(this.tbox_CasseteUnload_Speed);
            this.tp_CasseteUnload.Controls.Add(this.tbox_CasseteUnload_CurrentLocation);
            this.tp_CasseteUnload.Controls.Add(this.tbox_CasseteUnload_SelectedShaft);
            this.tp_CasseteUnload.Controls.Add(this.lbl_CasseteUnload_Location);
            this.tp_CasseteUnload.Controls.Add(this.lbl_CasseteUnload_Speed);
            this.tp_CasseteUnload.Controls.Add(this.lbl_CasseteUnload_SelectedShaft);
            this.tp_CasseteUnload.Controls.Add(this.tbox_CasseteUnload_LocationSetting);
            this.tp_CasseteUnload.Controls.Add(this.lbl_CasseteUnload_CurrentLocation);
            this.tp_CasseteUnload.Controls.Add(this.btn_CasseteUnload_GrabSwitch3);
            this.tp_CasseteUnload.Controls.Add(this.btn_CasseteUnload_GrabSwitch2);
            this.tp_CasseteUnload.Controls.Add(this.btn_CasseteUnload_GrabSwitch1);
            this.tp_CasseteUnload.Controls.Add(this.lv_CasseteUnload);
            this.tp_CasseteUnload.Controls.Add(this.ajin_Setting_CasseteUnload);
            this.tp_CasseteUnload.Location = new System.Drawing.Point(4, 34);
            this.tp_CasseteUnload.Name = "tp_CasseteUnload";
            this.tp_CasseteUnload.Size = new System.Drawing.Size(1726, 816);
            this.tp_CasseteUnload.TabIndex = 10;
            this.tp_CasseteUnload.Text = "카세트 언로드";
            // 
            // gbox_CasseteUnload_Move
            // 
            this.gbox_CasseteUnload_Move.Controls.Add(this.gbox_CasseteUnload_MoveB);
            this.gbox_CasseteUnload_Move.Controls.Add(this.gbox_CasseteUnload_MoveA);
            this.gbox_CasseteUnload_Move.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CasseteUnload_Move.ForeColor = System.Drawing.Color.White;
            this.gbox_CasseteUnload_Move.Location = new System.Drawing.Point(792, 595);
            this.gbox_CasseteUnload_Move.Name = "gbox_CasseteUnload_Move";
            this.gbox_CasseteUnload_Move.Size = new System.Drawing.Size(904, 179);
            this.gbox_CasseteUnload_Move.TabIndex = 53;
            this.gbox_CasseteUnload_Move.TabStop = false;
            this.gbox_CasseteUnload_Move.Text = "     이동     ";
            // 
            // gbox_CasseteUnload_MoveB
            // 
            this.gbox_CasseteUnload_MoveB.Controls.Add(this.tableLayoutPanel75);
            this.gbox_CasseteUnload_MoveB.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CasseteUnload_MoveB.ForeColor = System.Drawing.Color.White;
            this.gbox_CasseteUnload_MoveB.Location = new System.Drawing.Point(455, 28);
            this.gbox_CasseteUnload_MoveB.Name = "gbox_CasseteUnload_MoveB";
            this.gbox_CasseteUnload_MoveB.Size = new System.Drawing.Size(443, 145);
            this.gbox_CasseteUnload_MoveB.TabIndex = 31;
            this.gbox_CasseteUnload_MoveB.TabStop = false;
            this.gbox_CasseteUnload_MoveB.Text = "     B    ";
            // 
            // tableLayoutPanel75
            // 
            this.tableLayoutPanel75.ColumnCount = 4;
            this.tableLayoutPanel75.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel75.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel75.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel75.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel75.Controls.Add(this.tableLayoutPanel76, 3, 0);
            this.tableLayoutPanel75.Controls.Add(this.tableLayoutPanel77, 2, 0);
            this.tableLayoutPanel75.Controls.Add(this.tableLayoutPanel78, 1, 0);
            this.tableLayoutPanel75.Controls.Add(this.tableLayoutPanel79, 0, 0);
            this.tableLayoutPanel75.Location = new System.Drawing.Point(5, 17);
            this.tableLayoutPanel75.Name = "tableLayoutPanel75";
            this.tableLayoutPanel75.RowCount = 1;
            this.tableLayoutPanel75.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel75.Size = new System.Drawing.Size(437, 124);
            this.tableLayoutPanel75.TabIndex = 2;
            // 
            // tableLayoutPanel76
            // 
            this.tableLayoutPanel76.ColumnCount = 1;
            this.tableLayoutPanel76.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel76.Controls.Add(this.btn_CasseteUnload_MoveB_LiftZ2CellOut, 0, 1);
            this.tableLayoutPanel76.Controls.Add(this.btn_CasseteUnload_MoveB_LiftZ2CstTilt, 0, 0);
            this.tableLayoutPanel76.Location = new System.Drawing.Point(330, 3);
            this.tableLayoutPanel76.Name = "tableLayoutPanel76";
            this.tableLayoutPanel76.RowCount = 2;
            this.tableLayoutPanel76.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel76.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel76.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel76.TabIndex = 3;
            // 
            // btn_CasseteUnload_MoveB_LiftZ2CellOut
            // 
            this.btn_CasseteUnload_MoveB_LiftZ2CellOut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_MoveB_LiftZ2CellOut.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteUnload_MoveB_LiftZ2CellOut.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_MoveB_LiftZ2CellOut.Location = new System.Drawing.Point(3, 62);
            this.btn_CasseteUnload_MoveB_LiftZ2CellOut.Name = "btn_CasseteUnload_MoveB_LiftZ2CellOut";
            this.btn_CasseteUnload_MoveB_LiftZ2CellOut.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteUnload_MoveB_LiftZ2CellOut.TabIndex = 56;
            this.btn_CasseteUnload_MoveB_LiftZ2CellOut.Text = "리프트 Z2\r\n셀 배출\r\n시작";
            this.btn_CasseteUnload_MoveB_LiftZ2CellOut.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_MoveB_LiftZ2CstTilt
            // 
            this.btn_CasseteUnload_MoveB_LiftZ2CstTilt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_MoveB_LiftZ2CstTilt.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteUnload_MoveB_LiftZ2CstTilt.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_MoveB_LiftZ2CstTilt.Location = new System.Drawing.Point(3, 3);
            this.btn_CasseteUnload_MoveB_LiftZ2CstTilt.Name = "btn_CasseteUnload_MoveB_LiftZ2CstTilt";
            this.btn_CasseteUnload_MoveB_LiftZ2CstTilt.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteUnload_MoveB_LiftZ2CstTilt.TabIndex = 55;
            this.btn_CasseteUnload_MoveB_LiftZ2CstTilt.Text = "리프트 Z2\r\n카세트 틸트\r\n측정";
            this.btn_CasseteUnload_MoveB_LiftZ2CstTilt.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel77
            // 
            this.tableLayoutPanel77.ColumnCount = 1;
            this.tableLayoutPanel77.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel77.Controls.Add(this.btn_CasseteUnload_MoveB_LiftZ2CstOut, 0, 2);
            this.tableLayoutPanel77.Controls.Add(this.btn_CasseteUnload_MoveB_LiftZ2CstWait, 0, 0);
            this.tableLayoutPanel77.Controls.Add(this.btn_CasseteUnload_MoveB_LiftZ2CstIn, 0, 1);
            this.tableLayoutPanel77.Location = new System.Drawing.Point(221, 3);
            this.tableLayoutPanel77.Name = "tableLayoutPanel77";
            this.tableLayoutPanel77.RowCount = 3;
            this.tableLayoutPanel77.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel77.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel77.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel77.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel77.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel77.TabIndex = 2;
            // 
            // btn_CasseteUnload_MoveB_LiftZ2CstOut
            // 
            this.btn_CasseteUnload_MoveB_LiftZ2CstOut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_MoveB_LiftZ2CstOut.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteUnload_MoveB_LiftZ2CstOut.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_MoveB_LiftZ2CstOut.Location = new System.Drawing.Point(3, 81);
            this.btn_CasseteUnload_MoveB_LiftZ2CstOut.Name = "btn_CasseteUnload_MoveB_LiftZ2CstOut";
            this.btn_CasseteUnload_MoveB_LiftZ2CstOut.Size = new System.Drawing.Size(97, 34);
            this.btn_CasseteUnload_MoveB_LiftZ2CstOut.TabIndex = 57;
            this.btn_CasseteUnload_MoveB_LiftZ2CstOut.Text = "리프트 Z2\r\n카세트 배출";
            this.btn_CasseteUnload_MoveB_LiftZ2CstOut.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_MoveB_LiftZ2CstWait
            // 
            this.btn_CasseteUnload_MoveB_LiftZ2CstWait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_MoveB_LiftZ2CstWait.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteUnload_MoveB_LiftZ2CstWait.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_MoveB_LiftZ2CstWait.Location = new System.Drawing.Point(3, 3);
            this.btn_CasseteUnload_MoveB_LiftZ2CstWait.Name = "btn_CasseteUnload_MoveB_LiftZ2CstWait";
            this.btn_CasseteUnload_MoveB_LiftZ2CstWait.Size = new System.Drawing.Size(97, 33);
            this.btn_CasseteUnload_MoveB_LiftZ2CstWait.TabIndex = 55;
            this.btn_CasseteUnload_MoveB_LiftZ2CstWait.Text = "리프트 Z2\r\n카세트 투입 대기";
            this.btn_CasseteUnload_MoveB_LiftZ2CstWait.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_MoveB_LiftZ2CstIn
            // 
            this.btn_CasseteUnload_MoveB_LiftZ2CstIn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_MoveB_LiftZ2CstIn.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteUnload_MoveB_LiftZ2CstIn.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_MoveB_LiftZ2CstIn.Location = new System.Drawing.Point(3, 42);
            this.btn_CasseteUnload_MoveB_LiftZ2CstIn.Name = "btn_CasseteUnload_MoveB_LiftZ2CstIn";
            this.btn_CasseteUnload_MoveB_LiftZ2CstIn.Size = new System.Drawing.Size(97, 33);
            this.btn_CasseteUnload_MoveB_LiftZ2CstIn.TabIndex = 56;
            this.btn_CasseteUnload_MoveB_LiftZ2CstIn.Text = "리프트 Z2\r\n카세트 투입";
            this.btn_CasseteUnload_MoveB_LiftZ2CstIn.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel78
            // 
            this.tableLayoutPanel78.ColumnCount = 1;
            this.tableLayoutPanel78.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel78.Controls.Add(this.btn_CasseteUnload_MoveB_BottomT4LiftOut, 0, 1);
            this.tableLayoutPanel78.Controls.Add(this.btn_CasseteUnload_MoveB_TopT3CstMove, 0, 0);
            this.tableLayoutPanel78.Location = new System.Drawing.Point(112, 3);
            this.tableLayoutPanel78.Name = "tableLayoutPanel78";
            this.tableLayoutPanel78.RowCount = 2;
            this.tableLayoutPanel78.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel78.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel78.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel78.TabIndex = 1;
            // 
            // btn_CasseteUnload_MoveB_BottomT4LiftOut
            // 
            this.btn_CasseteUnload_MoveB_BottomT4LiftOut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_MoveB_BottomT4LiftOut.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteUnload_MoveB_BottomT4LiftOut.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_MoveB_BottomT4LiftOut.Location = new System.Drawing.Point(3, 62);
            this.btn_CasseteUnload_MoveB_BottomT4LiftOut.Name = "btn_CasseteUnload_MoveB_BottomT4LiftOut";
            this.btn_CasseteUnload_MoveB_BottomT4LiftOut.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteUnload_MoveB_BottomT4LiftOut.TabIndex = 56;
            this.btn_CasseteUnload_MoveB_BottomT4LiftOut.Text = "하부 카세트 T4\r\n리프트 배출";
            this.btn_CasseteUnload_MoveB_BottomT4LiftOut.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_MoveB_TopT3CstMove
            // 
            this.btn_CasseteUnload_MoveB_TopT3CstMove.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_MoveB_TopT3CstMove.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteUnload_MoveB_TopT3CstMove.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_MoveB_TopT3CstMove.Location = new System.Drawing.Point(3, 3);
            this.btn_CasseteUnload_MoveB_TopT3CstMove.Name = "btn_CasseteUnload_MoveB_TopT3CstMove";
            this.btn_CasseteUnload_MoveB_TopT3CstMove.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteUnload_MoveB_TopT3CstMove.TabIndex = 55;
            this.btn_CasseteUnload_MoveB_TopT3CstMove.Text = "상부 카세트 T3\r\n카세트 이동";
            this.btn_CasseteUnload_MoveB_TopT3CstMove.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel79
            // 
            this.tableLayoutPanel79.ColumnCount = 1;
            this.tableLayoutPanel79.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel79.Controls.Add(this.btn_CasseteUnload_MoveB_BottomT4CstIn, 0, 1);
            this.tableLayoutPanel79.Controls.Add(this.btn_CasseteUnload_MoveB_TopT3Out, 0, 0);
            this.tableLayoutPanel79.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel79.Name = "tableLayoutPanel79";
            this.tableLayoutPanel79.RowCount = 2;
            this.tableLayoutPanel79.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel79.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel79.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel79.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel79.TabIndex = 0;
            // 
            // btn_CasseteUnload_MoveB_BottomT4CstIn
            // 
            this.btn_CasseteUnload_MoveB_BottomT4CstIn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_MoveB_BottomT4CstIn.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteUnload_MoveB_BottomT4CstIn.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_MoveB_BottomT4CstIn.Location = new System.Drawing.Point(3, 62);
            this.btn_CasseteUnload_MoveB_BottomT4CstIn.Name = "btn_CasseteUnload_MoveB_BottomT4CstIn";
            this.btn_CasseteUnload_MoveB_BottomT4CstIn.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteUnload_MoveB_BottomT4CstIn.TabIndex = 56;
            this.btn_CasseteUnload_MoveB_BottomT4CstIn.Text = "하부 카세트 T4\r\n카세트 투입";
            this.btn_CasseteUnload_MoveB_BottomT4CstIn.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_MoveB_TopT3Out
            // 
            this.btn_CasseteUnload_MoveB_TopT3Out.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_MoveB_TopT3Out.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteUnload_MoveB_TopT3Out.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_MoveB_TopT3Out.Location = new System.Drawing.Point(3, 3);
            this.btn_CasseteUnload_MoveB_TopT3Out.Name = "btn_CasseteUnload_MoveB_TopT3Out";
            this.btn_CasseteUnload_MoveB_TopT3Out.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteUnload_MoveB_TopT3Out.TabIndex = 55;
            this.btn_CasseteUnload_MoveB_TopT3Out.Text = "상부 카세트 T3\r\n카세트 배출";
            this.btn_CasseteUnload_MoveB_TopT3Out.UseVisualStyleBackColor = false;
            // 
            // gbox_CasseteUnload_MoveA
            // 
            this.gbox_CasseteUnload_MoveA.Controls.Add(this.tableLayoutPanel70);
            this.gbox_CasseteUnload_MoveA.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CasseteUnload_MoveA.ForeColor = System.Drawing.Color.White;
            this.gbox_CasseteUnload_MoveA.Location = new System.Drawing.Point(6, 28);
            this.gbox_CasseteUnload_MoveA.Name = "gbox_CasseteUnload_MoveA";
            this.gbox_CasseteUnload_MoveA.Size = new System.Drawing.Size(443, 145);
            this.gbox_CasseteUnload_MoveA.TabIndex = 30;
            this.gbox_CasseteUnload_MoveA.TabStop = false;
            this.gbox_CasseteUnload_MoveA.Text = "     A    ";
            // 
            // tableLayoutPanel70
            // 
            this.tableLayoutPanel70.ColumnCount = 4;
            this.tableLayoutPanel70.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel70.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel70.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel70.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel70.Controls.Add(this.tableLayoutPanel71, 3, 0);
            this.tableLayoutPanel70.Controls.Add(this.tableLayoutPanel72, 2, 0);
            this.tableLayoutPanel70.Controls.Add(this.tableLayoutPanel73, 1, 0);
            this.tableLayoutPanel70.Controls.Add(this.tableLayoutPanel74, 0, 0);
            this.tableLayoutPanel70.Location = new System.Drawing.Point(3, 17);
            this.tableLayoutPanel70.Name = "tableLayoutPanel70";
            this.tableLayoutPanel70.RowCount = 1;
            this.tableLayoutPanel70.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel70.Size = new System.Drawing.Size(437, 124);
            this.tableLayoutPanel70.TabIndex = 1;
            // 
            // tableLayoutPanel71
            // 
            this.tableLayoutPanel71.ColumnCount = 1;
            this.tableLayoutPanel71.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel71.Controls.Add(this.btn_CasseteUnload_MoveA_LiftZ1CellOut, 0, 1);
            this.tableLayoutPanel71.Controls.Add(this.btn_CasseteUnload_MoveA_LiftZ1CstTilt, 0, 0);
            this.tableLayoutPanel71.Location = new System.Drawing.Point(330, 3);
            this.tableLayoutPanel71.Name = "tableLayoutPanel71";
            this.tableLayoutPanel71.RowCount = 2;
            this.tableLayoutPanel71.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel71.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel71.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel71.TabIndex = 3;
            // 
            // btn_CasseteUnload_MoveA_LiftZ1CellOut
            // 
            this.btn_CasseteUnload_MoveA_LiftZ1CellOut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_MoveA_LiftZ1CellOut.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteUnload_MoveA_LiftZ1CellOut.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_MoveA_LiftZ1CellOut.Location = new System.Drawing.Point(3, 62);
            this.btn_CasseteUnload_MoveA_LiftZ1CellOut.Name = "btn_CasseteUnload_MoveA_LiftZ1CellOut";
            this.btn_CasseteUnload_MoveA_LiftZ1CellOut.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteUnload_MoveA_LiftZ1CellOut.TabIndex = 56;
            this.btn_CasseteUnload_MoveA_LiftZ1CellOut.Text = "리프트 Z1\r\n셀 배출 \r\n시작";
            this.btn_CasseteUnload_MoveA_LiftZ1CellOut.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_MoveA_LiftZ1CstTilt
            // 
            this.btn_CasseteUnload_MoveA_LiftZ1CstTilt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_MoveA_LiftZ1CstTilt.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteUnload_MoveA_LiftZ1CstTilt.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_MoveA_LiftZ1CstTilt.Location = new System.Drawing.Point(3, 3);
            this.btn_CasseteUnload_MoveA_LiftZ1CstTilt.Name = "btn_CasseteUnload_MoveA_LiftZ1CstTilt";
            this.btn_CasseteUnload_MoveA_LiftZ1CstTilt.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteUnload_MoveA_LiftZ1CstTilt.TabIndex = 55;
            this.btn_CasseteUnload_MoveA_LiftZ1CstTilt.Text = "리프트 Z1\r\n카세트 틸트\r\n측정";
            this.btn_CasseteUnload_MoveA_LiftZ1CstTilt.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel72
            // 
            this.tableLayoutPanel72.ColumnCount = 1;
            this.tableLayoutPanel72.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel72.Controls.Add(this.btn_CasseteUnload_MoveA_LiftZ1CstOut, 0, 2);
            this.tableLayoutPanel72.Controls.Add(this.btn_CasseteUnload_MoveA_LiftZ1CstWait, 0, 0);
            this.tableLayoutPanel72.Controls.Add(this.btn_CasseteUnload_MoveA_LiftZ1CstIn, 0, 1);
            this.tableLayoutPanel72.Location = new System.Drawing.Point(221, 3);
            this.tableLayoutPanel72.Name = "tableLayoutPanel72";
            this.tableLayoutPanel72.RowCount = 3;
            this.tableLayoutPanel72.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel72.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel72.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel72.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel72.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel72.TabIndex = 2;
            // 
            // btn_CasseteUnload_MoveA_LiftZ1CstOut
            // 
            this.btn_CasseteUnload_MoveA_LiftZ1CstOut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_MoveA_LiftZ1CstOut.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteUnload_MoveA_LiftZ1CstOut.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_MoveA_LiftZ1CstOut.Location = new System.Drawing.Point(3, 81);
            this.btn_CasseteUnload_MoveA_LiftZ1CstOut.Name = "btn_CasseteUnload_MoveA_LiftZ1CstOut";
            this.btn_CasseteUnload_MoveA_LiftZ1CstOut.Size = new System.Drawing.Size(97, 34);
            this.btn_CasseteUnload_MoveA_LiftZ1CstOut.TabIndex = 57;
            this.btn_CasseteUnload_MoveA_LiftZ1CstOut.Text = "리프트 Z1\r\n카세트 배출";
            this.btn_CasseteUnload_MoveA_LiftZ1CstOut.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_MoveA_LiftZ1CstWait
            // 
            this.btn_CasseteUnload_MoveA_LiftZ1CstWait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_MoveA_LiftZ1CstWait.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteUnload_MoveA_LiftZ1CstWait.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_MoveA_LiftZ1CstWait.Location = new System.Drawing.Point(3, 3);
            this.btn_CasseteUnload_MoveA_LiftZ1CstWait.Name = "btn_CasseteUnload_MoveA_LiftZ1CstWait";
            this.btn_CasseteUnload_MoveA_LiftZ1CstWait.Size = new System.Drawing.Size(97, 33);
            this.btn_CasseteUnload_MoveA_LiftZ1CstWait.TabIndex = 55;
            this.btn_CasseteUnload_MoveA_LiftZ1CstWait.Text = "리프트 Z1\r\n카세트 투입 대기";
            this.btn_CasseteUnload_MoveA_LiftZ1CstWait.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_MoveA_LiftZ1CstIn
            // 
            this.btn_CasseteUnload_MoveA_LiftZ1CstIn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_MoveA_LiftZ1CstIn.Font = new System.Drawing.Font("맑은 고딕", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteUnload_MoveA_LiftZ1CstIn.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_MoveA_LiftZ1CstIn.Location = new System.Drawing.Point(3, 42);
            this.btn_CasseteUnload_MoveA_LiftZ1CstIn.Name = "btn_CasseteUnload_MoveA_LiftZ1CstIn";
            this.btn_CasseteUnload_MoveA_LiftZ1CstIn.Size = new System.Drawing.Size(97, 33);
            this.btn_CasseteUnload_MoveA_LiftZ1CstIn.TabIndex = 56;
            this.btn_CasseteUnload_MoveA_LiftZ1CstIn.Text = "리프트 Z1\r\n카세트 투입";
            this.btn_CasseteUnload_MoveA_LiftZ1CstIn.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel73
            // 
            this.tableLayoutPanel73.ColumnCount = 1;
            this.tableLayoutPanel73.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel73.Controls.Add(this.btn_CasseteUnload_MoveA_BottomT2Move, 0, 1);
            this.tableLayoutPanel73.Controls.Add(this.btn_CasseteUnload_MoveA_TopT1Move, 0, 0);
            this.tableLayoutPanel73.Location = new System.Drawing.Point(112, 3);
            this.tableLayoutPanel73.Name = "tableLayoutPanel73";
            this.tableLayoutPanel73.RowCount = 2;
            this.tableLayoutPanel73.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel73.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel73.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel73.TabIndex = 1;
            // 
            // btn_CasseteUnload_MoveA_BottomT2Move
            // 
            this.btn_CasseteUnload_MoveA_BottomT2Move.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_MoveA_BottomT2Move.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteUnload_MoveA_BottomT2Move.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_MoveA_BottomT2Move.Location = new System.Drawing.Point(3, 62);
            this.btn_CasseteUnload_MoveA_BottomT2Move.Name = "btn_CasseteUnload_MoveA_BottomT2Move";
            this.btn_CasseteUnload_MoveA_BottomT2Move.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteUnload_MoveA_BottomT2Move.TabIndex = 56;
            this.btn_CasseteUnload_MoveA_BottomT2Move.Text = "하부 카세트 T2\r\n리프트 이동";
            this.btn_CasseteUnload_MoveA_BottomT2Move.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_MoveA_TopT1Move
            // 
            this.btn_CasseteUnload_MoveA_TopT1Move.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_MoveA_TopT1Move.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteUnload_MoveA_TopT1Move.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_MoveA_TopT1Move.Location = new System.Drawing.Point(3, 3);
            this.btn_CasseteUnload_MoveA_TopT1Move.Name = "btn_CasseteUnload_MoveA_TopT1Move";
            this.btn_CasseteUnload_MoveA_TopT1Move.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteUnload_MoveA_TopT1Move.TabIndex = 55;
            this.btn_CasseteUnload_MoveA_TopT1Move.Text = "상부 카세트 T1\r\n카세트 이동";
            this.btn_CasseteUnload_MoveA_TopT1Move.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel74
            // 
            this.tableLayoutPanel74.ColumnCount = 1;
            this.tableLayoutPanel74.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel74.Controls.Add(this.btn_CasseteUnload_MoveA_BottomT2In, 0, 1);
            this.tableLayoutPanel74.Controls.Add(this.btn_CasseteUnload_MoveA_TopT1Out, 0, 0);
            this.tableLayoutPanel74.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel74.Name = "tableLayoutPanel74";
            this.tableLayoutPanel74.RowCount = 2;
            this.tableLayoutPanel74.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel74.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel74.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel74.Size = new System.Drawing.Size(103, 118);
            this.tableLayoutPanel74.TabIndex = 0;
            // 
            // btn_CasseteUnload_MoveA_BottomT2In
            // 
            this.btn_CasseteUnload_MoveA_BottomT2In.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_MoveA_BottomT2In.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteUnload_MoveA_BottomT2In.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_MoveA_BottomT2In.Location = new System.Drawing.Point(3, 62);
            this.btn_CasseteUnload_MoveA_BottomT2In.Name = "btn_CasseteUnload_MoveA_BottomT2In";
            this.btn_CasseteUnload_MoveA_BottomT2In.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteUnload_MoveA_BottomT2In.TabIndex = 56;
            this.btn_CasseteUnload_MoveA_BottomT2In.Text = "하부 카세트 T2\r\n카세트 투입";
            this.btn_CasseteUnload_MoveA_BottomT2In.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_MoveA_TopT1Out
            // 
            this.btn_CasseteUnload_MoveA_TopT1Out.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_MoveA_TopT1Out.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteUnload_MoveA_TopT1Out.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_MoveA_TopT1Out.Location = new System.Drawing.Point(3, 3);
            this.btn_CasseteUnload_MoveA_TopT1Out.Name = "btn_CasseteUnload_MoveA_TopT1Out";
            this.btn_CasseteUnload_MoveA_TopT1Out.Size = new System.Drawing.Size(97, 53);
            this.btn_CasseteUnload_MoveA_TopT1Out.TabIndex = 55;
            this.btn_CasseteUnload_MoveA_TopT1Out.Text = "상부 카세트 T1\r\n카세트 배출";
            this.btn_CasseteUnload_MoveA_TopT1Out.UseVisualStyleBackColor = false;
            // 
            // gbox_CasseteUnload_B
            // 
            this.gbox_CasseteUnload_B.Controls.Add(this.tableLayoutPanel6);
            this.gbox_CasseteUnload_B.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CasseteUnload_B.ForeColor = System.Drawing.Color.White;
            this.gbox_CasseteUnload_B.Location = new System.Drawing.Point(1247, 410);
            this.gbox_CasseteUnload_B.Name = "gbox_CasseteUnload_B";
            this.gbox_CasseteUnload_B.Size = new System.Drawing.Size(449, 179);
            this.gbox_CasseteUnload_B.TabIndex = 52;
            this.gbox_CasseteUnload_B.TabStop = false;
            this.gbox_CasseteUnload_B.Text = "     B     ";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 4;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.Controls.Add(this.btn_CasseteUnload_B_BottomCstStopperDown, 3, 2);
            this.tableLayoutPanel6.Controls.Add(this.btn_CasseteUnload_B_BottomCstStopperUp, 2, 2);
            this.tableLayoutPanel6.Controls.Add(this.btn_CasseteUnload_B_BottomCstUngrib, 1, 2);
            this.tableLayoutPanel6.Controls.Add(this.btn_CasseteUnload_B_BottomCstGrib, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.btn_CasseteUnload_B_LiftTiltSlinderDown, 3, 1);
            this.tableLayoutPanel6.Controls.Add(this.btn_CasseteUnload_B_LiftTiltSlinderUp, 2, 1);
            this.tableLayoutPanel6.Controls.Add(this.btn_CasseteUnload_B_LiftUngrib, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.btn_CasseteUnload_B_LiftGrib, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.btn_CasseteUnload_B_TopCstStopperDown, 3, 0);
            this.tableLayoutPanel6.Controls.Add(this.btn_CasseteUnload_B_TopCstStopperUp, 2, 0);
            this.tableLayoutPanel6.Controls.Add(this.btn_CasseteUnload_B_TopCstUngrib, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.btn_CasseteUnload_B_TopCstGrib, 0, 0);
            this.tableLayoutPanel6.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 3;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(437, 145);
            this.tableLayoutPanel6.TabIndex = 1;
            // 
            // btn_CasseteUnload_B_BottomCstStopperDown
            // 
            this.btn_CasseteUnload_B_BottomCstStopperDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_B_BottomCstStopperDown.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CasseteUnload_B_BottomCstStopperDown.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_B_BottomCstStopperDown.Location = new System.Drawing.Point(330, 99);
            this.btn_CasseteUnload_B_BottomCstStopperDown.Name = "btn_CasseteUnload_B_BottomCstStopperDown";
            this.btn_CasseteUnload_B_BottomCstStopperDown.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteUnload_B_BottomCstStopperDown.TabIndex = 65;
            this.btn_CasseteUnload_B_BottomCstStopperDown.Text = "하단 카세트\r\n스토퍼 다운";
            this.btn_CasseteUnload_B_BottomCstStopperDown.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_B_BottomCstStopperUp
            // 
            this.btn_CasseteUnload_B_BottomCstStopperUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_B_BottomCstStopperUp.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CasseteUnload_B_BottomCstStopperUp.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_B_BottomCstStopperUp.Location = new System.Drawing.Point(221, 99);
            this.btn_CasseteUnload_B_BottomCstStopperUp.Name = "btn_CasseteUnload_B_BottomCstStopperUp";
            this.btn_CasseteUnload_B_BottomCstStopperUp.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteUnload_B_BottomCstStopperUp.TabIndex = 64;
            this.btn_CasseteUnload_B_BottomCstStopperUp.Text = "하단 카세트\r\n스토퍼 업";
            this.btn_CasseteUnload_B_BottomCstStopperUp.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_B_BottomCstUngrib
            // 
            this.btn_CasseteUnload_B_BottomCstUngrib.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_B_BottomCstUngrib.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CasseteUnload_B_BottomCstUngrib.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_B_BottomCstUngrib.Location = new System.Drawing.Point(112, 99);
            this.btn_CasseteUnload_B_BottomCstUngrib.Name = "btn_CasseteUnload_B_BottomCstUngrib";
            this.btn_CasseteUnload_B_BottomCstUngrib.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteUnload_B_BottomCstUngrib.TabIndex = 63;
            this.btn_CasseteUnload_B_BottomCstUngrib.Text = "하단 카세트\r\n언그립";
            this.btn_CasseteUnload_B_BottomCstUngrib.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_B_BottomCstGrib
            // 
            this.btn_CasseteUnload_B_BottomCstGrib.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_B_BottomCstGrib.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CasseteUnload_B_BottomCstGrib.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_B_BottomCstGrib.Location = new System.Drawing.Point(3, 99);
            this.btn_CasseteUnload_B_BottomCstGrib.Name = "btn_CasseteUnload_B_BottomCstGrib";
            this.btn_CasseteUnload_B_BottomCstGrib.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteUnload_B_BottomCstGrib.TabIndex = 62;
            this.btn_CasseteUnload_B_BottomCstGrib.Text = "하단 카세트\r\n그립";
            this.btn_CasseteUnload_B_BottomCstGrib.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_B_LiftTiltSlinderDown
            // 
            this.btn_CasseteUnload_B_LiftTiltSlinderDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_B_LiftTiltSlinderDown.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CasseteUnload_B_LiftTiltSlinderDown.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_B_LiftTiltSlinderDown.Location = new System.Drawing.Point(330, 51);
            this.btn_CasseteUnload_B_LiftTiltSlinderDown.Name = "btn_CasseteUnload_B_LiftTiltSlinderDown";
            this.btn_CasseteUnload_B_LiftTiltSlinderDown.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteUnload_B_LiftTiltSlinderDown.TabIndex = 61;
            this.btn_CasseteUnload_B_LiftTiltSlinderDown.Text = "리프트 틸트\r\n실린더 다운";
            this.btn_CasseteUnload_B_LiftTiltSlinderDown.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_B_LiftTiltSlinderUp
            // 
            this.btn_CasseteUnload_B_LiftTiltSlinderUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_B_LiftTiltSlinderUp.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CasseteUnload_B_LiftTiltSlinderUp.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_B_LiftTiltSlinderUp.Location = new System.Drawing.Point(221, 51);
            this.btn_CasseteUnload_B_LiftTiltSlinderUp.Name = "btn_CasseteUnload_B_LiftTiltSlinderUp";
            this.btn_CasseteUnload_B_LiftTiltSlinderUp.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteUnload_B_LiftTiltSlinderUp.TabIndex = 60;
            this.btn_CasseteUnload_B_LiftTiltSlinderUp.Text = "리프트 틸트\r\n실린더 업";
            this.btn_CasseteUnload_B_LiftTiltSlinderUp.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_B_LiftUngrib
            // 
            this.btn_CasseteUnload_B_LiftUngrib.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_B_LiftUngrib.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CasseteUnload_B_LiftUngrib.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_B_LiftUngrib.Location = new System.Drawing.Point(112, 51);
            this.btn_CasseteUnload_B_LiftUngrib.Name = "btn_CasseteUnload_B_LiftUngrib";
            this.btn_CasseteUnload_B_LiftUngrib.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteUnload_B_LiftUngrib.TabIndex = 59;
            this.btn_CasseteUnload_B_LiftUngrib.Text = "리프트\r\n언그립";
            this.btn_CasseteUnload_B_LiftUngrib.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_B_LiftGrib
            // 
            this.btn_CasseteUnload_B_LiftGrib.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_B_LiftGrib.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CasseteUnload_B_LiftGrib.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_B_LiftGrib.Location = new System.Drawing.Point(3, 51);
            this.btn_CasseteUnload_B_LiftGrib.Name = "btn_CasseteUnload_B_LiftGrib";
            this.btn_CasseteUnload_B_LiftGrib.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteUnload_B_LiftGrib.TabIndex = 58;
            this.btn_CasseteUnload_B_LiftGrib.Text = "리프트\r\n그립";
            this.btn_CasseteUnload_B_LiftGrib.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_B_TopCstStopperDown
            // 
            this.btn_CasseteUnload_B_TopCstStopperDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_B_TopCstStopperDown.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CasseteUnload_B_TopCstStopperDown.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_B_TopCstStopperDown.Location = new System.Drawing.Point(330, 3);
            this.btn_CasseteUnload_B_TopCstStopperDown.Name = "btn_CasseteUnload_B_TopCstStopperDown";
            this.btn_CasseteUnload_B_TopCstStopperDown.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteUnload_B_TopCstStopperDown.TabIndex = 57;
            this.btn_CasseteUnload_B_TopCstStopperDown.Text = "상단 카세트\r\n스토퍼 다운";
            this.btn_CasseteUnload_B_TopCstStopperDown.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_B_TopCstStopperUp
            // 
            this.btn_CasseteUnload_B_TopCstStopperUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_B_TopCstStopperUp.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CasseteUnload_B_TopCstStopperUp.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_B_TopCstStopperUp.Location = new System.Drawing.Point(221, 3);
            this.btn_CasseteUnload_B_TopCstStopperUp.Name = "btn_CasseteUnload_B_TopCstStopperUp";
            this.btn_CasseteUnload_B_TopCstStopperUp.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteUnload_B_TopCstStopperUp.TabIndex = 56;
            this.btn_CasseteUnload_B_TopCstStopperUp.Text = "상단 카세트\r\n스토퍼 업";
            this.btn_CasseteUnload_B_TopCstStopperUp.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_B_TopCstUngrib
            // 
            this.btn_CasseteUnload_B_TopCstUngrib.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_B_TopCstUngrib.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CasseteUnload_B_TopCstUngrib.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_B_TopCstUngrib.Location = new System.Drawing.Point(112, 3);
            this.btn_CasseteUnload_B_TopCstUngrib.Name = "btn_CasseteUnload_B_TopCstUngrib";
            this.btn_CasseteUnload_B_TopCstUngrib.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteUnload_B_TopCstUngrib.TabIndex = 55;
            this.btn_CasseteUnload_B_TopCstUngrib.Text = "상단 카세트\r\n언그립";
            this.btn_CasseteUnload_B_TopCstUngrib.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_B_TopCstGrib
            // 
            this.btn_CasseteUnload_B_TopCstGrib.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_B_TopCstGrib.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CasseteUnload_B_TopCstGrib.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_B_TopCstGrib.Location = new System.Drawing.Point(3, 3);
            this.btn_CasseteUnload_B_TopCstGrib.Name = "btn_CasseteUnload_B_TopCstGrib";
            this.btn_CasseteUnload_B_TopCstGrib.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteUnload_B_TopCstGrib.TabIndex = 54;
            this.btn_CasseteUnload_B_TopCstGrib.Text = "상단 카세트\r\n그립";
            this.btn_CasseteUnload_B_TopCstGrib.UseVisualStyleBackColor = false;
            // 
            // gbox_CasseteUnload_A
            // 
            this.gbox_CasseteUnload_A.Controls.Add(this.tableLayoutPanel5);
            this.gbox_CasseteUnload_A.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CasseteUnload_A.ForeColor = System.Drawing.Color.White;
            this.gbox_CasseteUnload_A.Location = new System.Drawing.Point(792, 410);
            this.gbox_CasseteUnload_A.Name = "gbox_CasseteUnload_A";
            this.gbox_CasseteUnload_A.Size = new System.Drawing.Size(449, 179);
            this.gbox_CasseteUnload_A.TabIndex = 51;
            this.gbox_CasseteUnload_A.TabStop = false;
            this.gbox_CasseteUnload_A.Text = "     A    ";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 4;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.Controls.Add(this.btn_CasseteUnload_A_BottomCstStopperDown, 3, 2);
            this.tableLayoutPanel5.Controls.Add(this.btn_CasseteUnload_A_BottomCstStopperUp, 2, 2);
            this.tableLayoutPanel5.Controls.Add(this.btn_CasseteUnload_A_BottomCstUngrib, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.btn_CasseteUnload_A_BottomCstGrib, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.btn_CasseteUnload_A_LiftTiltSlinderDown, 3, 1);
            this.tableLayoutPanel5.Controls.Add(this.btn_CasseteUnload_A_LiftTiltSlinderUp, 2, 1);
            this.tableLayoutPanel5.Controls.Add(this.btn_CasseteUnload_A_LiftUngrib, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.btn_CasseteUnload_A_LiftGrib, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.btn_CasseteUnload_A_TopCstStopperDown, 3, 0);
            this.tableLayoutPanel5.Controls.Add(this.btn_CasseteUnload_A_TopCstStopperUp, 2, 0);
            this.tableLayoutPanel5.Controls.Add(this.btn_CasseteUnload_A_TopCstUngrib, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.btn_CasseteUnload_A_TopCstGrib, 0, 0);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(6, 28);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 3;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(437, 145);
            this.tableLayoutPanel5.TabIndex = 1;
            // 
            // btn_CasseteUnload_A_BottomCstStopperDown
            // 
            this.btn_CasseteUnload_A_BottomCstStopperDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_A_BottomCstStopperDown.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CasseteUnload_A_BottomCstStopperDown.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_A_BottomCstStopperDown.Location = new System.Drawing.Point(330, 99);
            this.btn_CasseteUnload_A_BottomCstStopperDown.Name = "btn_CasseteUnload_A_BottomCstStopperDown";
            this.btn_CasseteUnload_A_BottomCstStopperDown.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteUnload_A_BottomCstStopperDown.TabIndex = 65;
            this.btn_CasseteUnload_A_BottomCstStopperDown.Text = "하단 카세트\r\n스토퍼 다운";
            this.btn_CasseteUnload_A_BottomCstStopperDown.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_A_BottomCstStopperUp
            // 
            this.btn_CasseteUnload_A_BottomCstStopperUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_A_BottomCstStopperUp.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CasseteUnload_A_BottomCstStopperUp.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_A_BottomCstStopperUp.Location = new System.Drawing.Point(221, 99);
            this.btn_CasseteUnload_A_BottomCstStopperUp.Name = "btn_CasseteUnload_A_BottomCstStopperUp";
            this.btn_CasseteUnload_A_BottomCstStopperUp.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteUnload_A_BottomCstStopperUp.TabIndex = 64;
            this.btn_CasseteUnload_A_BottomCstStopperUp.Text = "하단 카세트\r\n스토퍼 업";
            this.btn_CasseteUnload_A_BottomCstStopperUp.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_A_BottomCstUngrib
            // 
            this.btn_CasseteUnload_A_BottomCstUngrib.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_A_BottomCstUngrib.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CasseteUnload_A_BottomCstUngrib.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_A_BottomCstUngrib.Location = new System.Drawing.Point(112, 99);
            this.btn_CasseteUnload_A_BottomCstUngrib.Name = "btn_CasseteUnload_A_BottomCstUngrib";
            this.btn_CasseteUnload_A_BottomCstUngrib.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteUnload_A_BottomCstUngrib.TabIndex = 63;
            this.btn_CasseteUnload_A_BottomCstUngrib.Text = "하단 카세트\r\n언그립";
            this.btn_CasseteUnload_A_BottomCstUngrib.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_A_BottomCstGrib
            // 
            this.btn_CasseteUnload_A_BottomCstGrib.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_A_BottomCstGrib.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CasseteUnload_A_BottomCstGrib.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_A_BottomCstGrib.Location = new System.Drawing.Point(3, 99);
            this.btn_CasseteUnload_A_BottomCstGrib.Name = "btn_CasseteUnload_A_BottomCstGrib";
            this.btn_CasseteUnload_A_BottomCstGrib.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteUnload_A_BottomCstGrib.TabIndex = 62;
            this.btn_CasseteUnload_A_BottomCstGrib.Text = "하단 카세트\r\n그립";
            this.btn_CasseteUnload_A_BottomCstGrib.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_A_LiftTiltSlinderDown
            // 
            this.btn_CasseteUnload_A_LiftTiltSlinderDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_A_LiftTiltSlinderDown.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CasseteUnload_A_LiftTiltSlinderDown.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_A_LiftTiltSlinderDown.Location = new System.Drawing.Point(330, 51);
            this.btn_CasseteUnload_A_LiftTiltSlinderDown.Name = "btn_CasseteUnload_A_LiftTiltSlinderDown";
            this.btn_CasseteUnload_A_LiftTiltSlinderDown.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteUnload_A_LiftTiltSlinderDown.TabIndex = 61;
            this.btn_CasseteUnload_A_LiftTiltSlinderDown.Text = "리프트 틸트\r\n실린더 다운";
            this.btn_CasseteUnload_A_LiftTiltSlinderDown.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_A_LiftTiltSlinderUp
            // 
            this.btn_CasseteUnload_A_LiftTiltSlinderUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_A_LiftTiltSlinderUp.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CasseteUnload_A_LiftTiltSlinderUp.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_A_LiftTiltSlinderUp.Location = new System.Drawing.Point(221, 51);
            this.btn_CasseteUnload_A_LiftTiltSlinderUp.Name = "btn_CasseteUnload_A_LiftTiltSlinderUp";
            this.btn_CasseteUnload_A_LiftTiltSlinderUp.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteUnload_A_LiftTiltSlinderUp.TabIndex = 60;
            this.btn_CasseteUnload_A_LiftTiltSlinderUp.Text = "리프트 틸트\r\n실린더 업";
            this.btn_CasseteUnload_A_LiftTiltSlinderUp.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_A_LiftUngrib
            // 
            this.btn_CasseteUnload_A_LiftUngrib.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_A_LiftUngrib.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CasseteUnload_A_LiftUngrib.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_A_LiftUngrib.Location = new System.Drawing.Point(112, 51);
            this.btn_CasseteUnload_A_LiftUngrib.Name = "btn_CasseteUnload_A_LiftUngrib";
            this.btn_CasseteUnload_A_LiftUngrib.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteUnload_A_LiftUngrib.TabIndex = 59;
            this.btn_CasseteUnload_A_LiftUngrib.Text = "리프트\r\n언그립";
            this.btn_CasseteUnload_A_LiftUngrib.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_A_LiftGrib
            // 
            this.btn_CasseteUnload_A_LiftGrib.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_A_LiftGrib.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CasseteUnload_A_LiftGrib.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_A_LiftGrib.Location = new System.Drawing.Point(3, 51);
            this.btn_CasseteUnload_A_LiftGrib.Name = "btn_CasseteUnload_A_LiftGrib";
            this.btn_CasseteUnload_A_LiftGrib.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteUnload_A_LiftGrib.TabIndex = 58;
            this.btn_CasseteUnload_A_LiftGrib.Text = "리프트\r\n그립";
            this.btn_CasseteUnload_A_LiftGrib.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_A_TopCstStopperDown
            // 
            this.btn_CasseteUnload_A_TopCstStopperDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_A_TopCstStopperDown.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CasseteUnload_A_TopCstStopperDown.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_A_TopCstStopperDown.Location = new System.Drawing.Point(330, 3);
            this.btn_CasseteUnload_A_TopCstStopperDown.Name = "btn_CasseteUnload_A_TopCstStopperDown";
            this.btn_CasseteUnload_A_TopCstStopperDown.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteUnload_A_TopCstStopperDown.TabIndex = 57;
            this.btn_CasseteUnload_A_TopCstStopperDown.Text = "상단 카세트\r\n스토퍼 다운";
            this.btn_CasseteUnload_A_TopCstStopperDown.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_A_TopCstStopperUp
            // 
            this.btn_CasseteUnload_A_TopCstStopperUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_A_TopCstStopperUp.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CasseteUnload_A_TopCstStopperUp.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_A_TopCstStopperUp.Location = new System.Drawing.Point(221, 3);
            this.btn_CasseteUnload_A_TopCstStopperUp.Name = "btn_CasseteUnload_A_TopCstStopperUp";
            this.btn_CasseteUnload_A_TopCstStopperUp.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteUnload_A_TopCstStopperUp.TabIndex = 56;
            this.btn_CasseteUnload_A_TopCstStopperUp.Text = "상단 카세트\r\n스토퍼 업";
            this.btn_CasseteUnload_A_TopCstStopperUp.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_A_TopCstUngrib
            // 
            this.btn_CasseteUnload_A_TopCstUngrib.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_A_TopCstUngrib.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CasseteUnload_A_TopCstUngrib.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_A_TopCstUngrib.Location = new System.Drawing.Point(112, 3);
            this.btn_CasseteUnload_A_TopCstUngrib.Name = "btn_CasseteUnload_A_TopCstUngrib";
            this.btn_CasseteUnload_A_TopCstUngrib.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteUnload_A_TopCstUngrib.TabIndex = 55;
            this.btn_CasseteUnload_A_TopCstUngrib.Text = "상단 카세트\r\n언그립";
            this.btn_CasseteUnload_A_TopCstUngrib.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_A_TopCstGrib
            // 
            this.btn_CasseteUnload_A_TopCstGrib.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_A_TopCstGrib.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CasseteUnload_A_TopCstGrib.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_A_TopCstGrib.Location = new System.Drawing.Point(3, 3);
            this.btn_CasseteUnload_A_TopCstGrib.Name = "btn_CasseteUnload_A_TopCstGrib";
            this.btn_CasseteUnload_A_TopCstGrib.Size = new System.Drawing.Size(103, 42);
            this.btn_CasseteUnload_A_TopCstGrib.TabIndex = 54;
            this.btn_CasseteUnload_A_TopCstGrib.Text = "상단 카세트\r\n그립";
            this.btn_CasseteUnload_A_TopCstGrib.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_Save
            // 
            this.btn_CasseteUnload_Save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_Save.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteUnload_Save.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_Save.Location = new System.Drawing.Point(1482, 783);
            this.btn_CasseteUnload_Save.Name = "btn_CasseteUnload_Save";
            this.btn_CasseteUnload_Save.Size = new System.Drawing.Size(228, 28);
            this.btn_CasseteUnload_Save.TabIndex = 50;
            this.btn_CasseteUnload_Save.Text = "저장";
            this.btn_CasseteUnload_Save.UseVisualStyleBackColor = false;
            // 
            // gbox_CasseteUnload
            // 
            this.gbox_CasseteUnload.Controls.Add(this.btn_CasseteUnload_Muting4);
            this.gbox_CasseteUnload.Controls.Add(this.btn_CasseteUnload_Muting2);
            this.gbox_CasseteUnload.Controls.Add(this.btn_CasseteUnload_Muting3);
            this.gbox_CasseteUnload.Controls.Add(this.btn_CasseteUnload_Muting1);
            this.gbox_CasseteUnload.Controls.Add(this.btn_CasseteUnload_MutingOut);
            this.gbox_CasseteUnload.Controls.Add(this.btn_CasseteUnload_MutingIn);
            this.gbox_CasseteUnload.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CasseteUnload.ForeColor = System.Drawing.Color.White;
            this.gbox_CasseteUnload.Location = new System.Drawing.Point(1483, 223);
            this.gbox_CasseteUnload.Name = "gbox_CasseteUnload";
            this.gbox_CasseteUnload.Size = new System.Drawing.Size(212, 181);
            this.gbox_CasseteUnload.TabIndex = 49;
            this.gbox_CasseteUnload.TabStop = false;
            this.gbox_CasseteUnload.Text = "     뮤팅     ";
            // 
            // btn_CasseteUnload_Muting4
            // 
            this.btn_CasseteUnload_Muting4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_Muting4.Location = new System.Drawing.Point(107, 125);
            this.btn_CasseteUnload_Muting4.Name = "btn_CasseteUnload_Muting4";
            this.btn_CasseteUnload_Muting4.Size = new System.Drawing.Size(99, 44);
            this.btn_CasseteUnload_Muting4.TabIndex = 5;
            this.btn_CasseteUnload_Muting4.Text = "뮤팅 4";
            this.btn_CasseteUnload_Muting4.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_Muting2
            // 
            this.btn_CasseteUnload_Muting2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_Muting2.Location = new System.Drawing.Point(6, 125);
            this.btn_CasseteUnload_Muting2.Name = "btn_CasseteUnload_Muting2";
            this.btn_CasseteUnload_Muting2.Size = new System.Drawing.Size(99, 44);
            this.btn_CasseteUnload_Muting2.TabIndex = 4;
            this.btn_CasseteUnload_Muting2.Text = "뮤팅 2";
            this.btn_CasseteUnload_Muting2.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_Muting3
            // 
            this.btn_CasseteUnload_Muting3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_Muting3.Location = new System.Drawing.Point(107, 75);
            this.btn_CasseteUnload_Muting3.Name = "btn_CasseteUnload_Muting3";
            this.btn_CasseteUnload_Muting3.Size = new System.Drawing.Size(99, 44);
            this.btn_CasseteUnload_Muting3.TabIndex = 3;
            this.btn_CasseteUnload_Muting3.Text = "뮤팅 3";
            this.btn_CasseteUnload_Muting3.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_Muting1
            // 
            this.btn_CasseteUnload_Muting1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_Muting1.Location = new System.Drawing.Point(6, 75);
            this.btn_CasseteUnload_Muting1.Name = "btn_CasseteUnload_Muting1";
            this.btn_CasseteUnload_Muting1.Size = new System.Drawing.Size(99, 44);
            this.btn_CasseteUnload_Muting1.TabIndex = 2;
            this.btn_CasseteUnload_Muting1.Text = "뮤팅 1";
            this.btn_CasseteUnload_Muting1.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_MutingOut
            // 
            this.btn_CasseteUnload_MutingOut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_MutingOut.Location = new System.Drawing.Point(107, 29);
            this.btn_CasseteUnload_MutingOut.Name = "btn_CasseteUnload_MutingOut";
            this.btn_CasseteUnload_MutingOut.Size = new System.Drawing.Size(99, 44);
            this.btn_CasseteUnload_MutingOut.TabIndex = 1;
            this.btn_CasseteUnload_MutingOut.Text = "뮤팅 아웃";
            this.btn_CasseteUnload_MutingOut.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_MutingIn
            // 
            this.btn_CasseteUnload_MutingIn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_MutingIn.Location = new System.Drawing.Point(6, 28);
            this.btn_CasseteUnload_MutingIn.Name = "btn_CasseteUnload_MutingIn";
            this.btn_CasseteUnload_MutingIn.Size = new System.Drawing.Size(99, 44);
            this.btn_CasseteUnload_MutingIn.TabIndex = 0;
            this.btn_CasseteUnload_MutingIn.Text = "뮤팅 인";
            this.btn_CasseteUnload_MutingIn.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_SpeedSetting
            // 
            this.btn_CasseteUnload_SpeedSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_SpeedSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CasseteUnload_SpeedSetting.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_SpeedSetting.Location = new System.Drawing.Point(1291, 315);
            this.btn_CasseteUnload_SpeedSetting.Name = "btn_CasseteUnload_SpeedSetting";
            this.btn_CasseteUnload_SpeedSetting.Size = new System.Drawing.Size(90, 27);
            this.btn_CasseteUnload_SpeedSetting.TabIndex = 48;
            this.btn_CasseteUnload_SpeedSetting.Text = "속도 설정";
            this.btn_CasseteUnload_SpeedSetting.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_AllSetting
            // 
            this.btn_CasseteUnload_AllSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_AllSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CasseteUnload_AllSetting.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_AllSetting.Location = new System.Drawing.Point(1387, 273);
            this.btn_CasseteUnload_AllSetting.Name = "btn_CasseteUnload_AllSetting";
            this.btn_CasseteUnload_AllSetting.Size = new System.Drawing.Size(90, 27);
            this.btn_CasseteUnload_AllSetting.TabIndex = 47;
            this.btn_CasseteUnload_AllSetting.Text = "전체 설정";
            this.btn_CasseteUnload_AllSetting.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_LocationSetting
            // 
            this.btn_CasseteUnload_LocationSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_LocationSetting.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CasseteUnload_LocationSetting.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_LocationSetting.Location = new System.Drawing.Point(1071, 315);
            this.btn_CasseteUnload_LocationSetting.Name = "btn_CasseteUnload_LocationSetting";
            this.btn_CasseteUnload_LocationSetting.Size = new System.Drawing.Size(90, 27);
            this.btn_CasseteUnload_LocationSetting.TabIndex = 46;
            this.btn_CasseteUnload_LocationSetting.Text = "위치 설정";
            this.btn_CasseteUnload_LocationSetting.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_MoveLocation
            // 
            this.btn_CasseteUnload_MoveLocation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_MoveLocation.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.btn_CasseteUnload_MoveLocation.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_MoveLocation.Location = new System.Drawing.Point(975, 315);
            this.btn_CasseteUnload_MoveLocation.Name = "btn_CasseteUnload_MoveLocation";
            this.btn_CasseteUnload_MoveLocation.Size = new System.Drawing.Size(90, 27);
            this.btn_CasseteUnload_MoveLocation.TabIndex = 45;
            this.btn_CasseteUnload_MoveLocation.Text = "위치 이동";
            this.btn_CasseteUnload_MoveLocation.UseVisualStyleBackColor = false;
            // 
            // tbox_CasseteUnload_Location
            // 
            this.tbox_CasseteUnload_Location.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_CasseteUnload_Location.Location = new System.Drawing.Point(879, 316);
            this.tbox_CasseteUnload_Location.Name = "tbox_CasseteUnload_Location";
            this.tbox_CasseteUnload_Location.Size = new System.Drawing.Size(90, 27);
            this.tbox_CasseteUnload_Location.TabIndex = 44;
            // 
            // tbox_CasseteUnload_Speed
            // 
            this.tbox_CasseteUnload_Speed.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_CasseteUnload_Speed.Location = new System.Drawing.Point(1291, 273);
            this.tbox_CasseteUnload_Speed.Name = "tbox_CasseteUnload_Speed";
            this.tbox_CasseteUnload_Speed.Size = new System.Drawing.Size(90, 27);
            this.tbox_CasseteUnload_Speed.TabIndex = 43;
            // 
            // tbox_CasseteUnload_CurrentLocation
            // 
            this.tbox_CasseteUnload_CurrentLocation.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_CasseteUnload_CurrentLocation.Location = new System.Drawing.Point(1084, 274);
            this.tbox_CasseteUnload_CurrentLocation.Name = "tbox_CasseteUnload_CurrentLocation";
            this.tbox_CasseteUnload_CurrentLocation.Size = new System.Drawing.Size(90, 27);
            this.tbox_CasseteUnload_CurrentLocation.TabIndex = 42;
            // 
            // tbox_CasseteUnload_SelectedShaft
            // 
            this.tbox_CasseteUnload_SelectedShaft.Font = new System.Drawing.Font("굴림", 13F);
            this.tbox_CasseteUnload_SelectedShaft.Location = new System.Drawing.Point(879, 273);
            this.tbox_CasseteUnload_SelectedShaft.Name = "tbox_CasseteUnload_SelectedShaft";
            this.tbox_CasseteUnload_SelectedShaft.Size = new System.Drawing.Size(90, 27);
            this.tbox_CasseteUnload_SelectedShaft.TabIndex = 41;
            // 
            // lbl_CasseteUnload_Location
            // 
            this.lbl_CasseteUnload_Location.AutoSize = true;
            this.lbl_CasseteUnload_Location.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CasseteUnload_Location.ForeColor = System.Drawing.Color.White;
            this.lbl_CasseteUnload_Location.Location = new System.Drawing.Point(801, 316);
            this.lbl_CasseteUnload_Location.Name = "lbl_CasseteUnload_Location";
            this.lbl_CasseteUnload_Location.Size = new System.Drawing.Size(84, 21);
            this.lbl_CasseteUnload_Location.TabIndex = 40;
            this.lbl_CasseteUnload_Location.Text = "위치[mm]";
            // 
            // lbl_CasseteUnload_Speed
            // 
            this.lbl_CasseteUnload_Speed.AutoSize = true;
            this.lbl_CasseteUnload_Speed.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CasseteUnload_Speed.ForeColor = System.Drawing.Color.White;
            this.lbl_CasseteUnload_Speed.Location = new System.Drawing.Point(1179, 275);
            this.lbl_CasseteUnload_Speed.Name = "lbl_CasseteUnload_Speed";
            this.lbl_CasseteUnload_Speed.Size = new System.Drawing.Size(115, 21);
            this.lbl_CasseteUnload_Speed.TabIndex = 39;
            this.lbl_CasseteUnload_Speed.Text = "속도[mm/sec]";
            // 
            // lbl_CasseteUnload_SelectedShaft
            // 
            this.lbl_CasseteUnload_SelectedShaft.AutoSize = true;
            this.lbl_CasseteUnload_SelectedShaft.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CasseteUnload_SelectedShaft.ForeColor = System.Drawing.Color.White;
            this.lbl_CasseteUnload_SelectedShaft.Location = new System.Drawing.Point(805, 274);
            this.lbl_CasseteUnload_SelectedShaft.Name = "lbl_CasseteUnload_SelectedShaft";
            this.lbl_CasseteUnload_SelectedShaft.Size = new System.Drawing.Size(80, 21);
            this.lbl_CasseteUnload_SelectedShaft.TabIndex = 38;
            this.lbl_CasseteUnload_SelectedShaft.Text = "선택된 축";
            // 
            // tbox_CasseteUnload_LocationSetting
            // 
            this.tbox_CasseteUnload_LocationSetting.BackColor = System.Drawing.SystemColors.Highlight;
            this.tbox_CasseteUnload_LocationSetting.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
            this.tbox_CasseteUnload_LocationSetting.ForeColor = System.Drawing.Color.White;
            this.tbox_CasseteUnload_LocationSetting.Location = new System.Drawing.Point(792, 223);
            this.tbox_CasseteUnload_LocationSetting.Name = "tbox_CasseteUnload_LocationSetting";
            this.tbox_CasseteUnload_LocationSetting.ReadOnly = true;
            this.tbox_CasseteUnload_LocationSetting.Size = new System.Drawing.Size(89, 27);
            this.tbox_CasseteUnload_LocationSetting.TabIndex = 37;
            this.tbox_CasseteUnload_LocationSetting.Text = "위치값 설정";
            this.tbox_CasseteUnload_LocationSetting.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbl_CasseteUnload_CurrentLocation
            // 
            this.lbl_CasseteUnload_CurrentLocation.AutoSize = true;
            this.lbl_CasseteUnload_CurrentLocation.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_CasseteUnload_CurrentLocation.ForeColor = System.Drawing.Color.White;
            this.lbl_CasseteUnload_CurrentLocation.Location = new System.Drawing.Point(975, 274);
            this.lbl_CasseteUnload_CurrentLocation.Name = "lbl_CasseteUnload_CurrentLocation";
            this.lbl_CasseteUnload_CurrentLocation.Size = new System.Drawing.Size(116, 21);
            this.lbl_CasseteUnload_CurrentLocation.TabIndex = 36;
            this.lbl_CasseteUnload_CurrentLocation.Text = "현재위치[mm]";
            // 
            // btn_CasseteUnload_GrabSwitch3
            // 
            this.btn_CasseteUnload_GrabSwitch3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_GrabSwitch3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteUnload_GrabSwitch3.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_GrabSwitch3.Location = new System.Drawing.Point(1514, 6);
            this.btn_CasseteUnload_GrabSwitch3.Name = "btn_CasseteUnload_GrabSwitch3";
            this.btn_CasseteUnload_GrabSwitch3.Size = new System.Drawing.Size(172, 23);
            this.btn_CasseteUnload_GrabSwitch3.TabIndex = 35;
            this.btn_CasseteUnload_GrabSwitch3.Text = "Grab Switch 3";
            this.btn_CasseteUnload_GrabSwitch3.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_GrabSwitch2
            // 
            this.btn_CasseteUnload_GrabSwitch2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_GrabSwitch2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteUnload_GrabSwitch2.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_GrabSwitch2.Location = new System.Drawing.Point(1336, 6);
            this.btn_CasseteUnload_GrabSwitch2.Name = "btn_CasseteUnload_GrabSwitch2";
            this.btn_CasseteUnload_GrabSwitch2.Size = new System.Drawing.Size(172, 23);
            this.btn_CasseteUnload_GrabSwitch2.TabIndex = 34;
            this.btn_CasseteUnload_GrabSwitch2.Text = "Grab Switch 2";
            this.btn_CasseteUnload_GrabSwitch2.UseVisualStyleBackColor = false;
            // 
            // btn_CasseteUnload_GrabSwitch1
            // 
            this.btn_CasseteUnload_GrabSwitch1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_CasseteUnload_GrabSwitch1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_CasseteUnload_GrabSwitch1.ForeColor = System.Drawing.Color.White;
            this.btn_CasseteUnload_GrabSwitch1.Location = new System.Drawing.Point(1158, 6);
            this.btn_CasseteUnload_GrabSwitch1.Name = "btn_CasseteUnload_GrabSwitch1";
            this.btn_CasseteUnload_GrabSwitch1.Size = new System.Drawing.Size(172, 23);
            this.btn_CasseteUnload_GrabSwitch1.TabIndex = 33;
            this.btn_CasseteUnload_GrabSwitch1.Text = "Grab Switch 1";
            this.btn_CasseteUnload_GrabSwitch1.UseVisualStyleBackColor = false;
            // 
            // lv_CasseteUnload
            // 
            this.lv_CasseteUnload.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.col_CasseteUnload_NamebyLocation,
            this.col_CasseteUnload_Shaft,
            this.col_CasseteUnload_Location,
            this.col_CasseteUnload_Speed});
            this.lv_CasseteUnload.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.lv_CasseteUnload.GridLines = true;
            this.lv_CasseteUnload.Location = new System.Drawing.Point(17, 16);
            this.lv_CasseteUnload.Name = "lv_CasseteUnload";
            this.lv_CasseteUnload.Size = new System.Drawing.Size(758, 734);
            this.lv_CasseteUnload.TabIndex = 32;
            this.lv_CasseteUnload.UseCompatibleStateImageBehavior = false;
            this.lv_CasseteUnload.View = System.Windows.Forms.View.Details;
            // 
            // col_CasseteUnload_NamebyLocation
            // 
            this.col_CasseteUnload_NamebyLocation.Text = "위치별 명칭";
            this.col_CasseteUnload_NamebyLocation.Width = 430;
            // 
            // col_CasseteUnload_Shaft
            // 
            this.col_CasseteUnload_Shaft.Text = "축";
            this.col_CasseteUnload_Shaft.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // col_CasseteUnload_Location
            // 
            this.col_CasseteUnload_Location.Text = "위치[mm]";
            this.col_CasseteUnload_Location.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_CasseteUnload_Location.Width = 125;
            // 
            // col_CasseteUnload_Speed
            // 
            this.col_CasseteUnload_Speed.Text = "속도[mm/s]";
            this.col_CasseteUnload_Speed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.col_CasseteUnload_Speed.Width = 125;
            // 
            // ajin_Setting_CasseteLoad
            // 
            this.ajin_Setting_CasseteLoad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CasseteLoad.Location = new System.Drawing.Point(792, 34);
            this.ajin_Setting_CasseteLoad.Name = "ajin_Setting_CasseteLoad";
            this.ajin_Setting_CasseteLoad.Size = new System.Drawing.Size(903, 183);
            this.ajin_Setting_CasseteLoad.TabIndex = 4;
            // 
            // ajin_Setting_Cellpurge
            // 
            this.ajin_Setting_Cellpurge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_Cellpurge.Location = new System.Drawing.Point(792, 34);
            this.ajin_Setting_Cellpurge.Name = "ajin_Setting_Cellpurge";
            this.ajin_Setting_Cellpurge.Size = new System.Drawing.Size(903, 183);
            this.ajin_Setting_Cellpurge.TabIndex = 4;
            // 
            // ajin_Setting_CellLoad
            // 
            this.ajin_Setting_CellLoad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CellLoad.Location = new System.Drawing.Point(792, 34);
            this.ajin_Setting_CellLoad.Name = "ajin_Setting_CellLoad";
            this.ajin_Setting_CellLoad.Size = new System.Drawing.Size(903, 183);
            this.ajin_Setting_CellLoad.TabIndex = 4;
            // 
            // ajin_Setting_IRCutProcess
            // 
            this.ajin_Setting_IRCutProcess.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_IRCutProcess.Location = new System.Drawing.Point(792, 34);
            this.ajin_Setting_IRCutProcess.Name = "ajin_Setting_IRCutProcess";
            this.ajin_Setting_IRCutProcess.Size = new System.Drawing.Size(903, 183);
            this.ajin_Setting_IRCutProcess.TabIndex = 4;
            // 
            // ajin_Setting_BreakTransfer
            // 
            this.ajin_Setting_BreakTransfer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_BreakTransfer.Location = new System.Drawing.Point(792, 34);
            this.ajin_Setting_BreakTransfer.Name = "ajin_Setting_BreakTransfer";
            this.ajin_Setting_BreakTransfer.Size = new System.Drawing.Size(903, 183);
            this.ajin_Setting_BreakTransfer.TabIndex = 31;
            // 
            // ajin_Setting_BreakUnitXZ
            // 
            this.ajin_Setting_BreakUnitXZ.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_BreakUnitXZ.Location = new System.Drawing.Point(792, 34);
            this.ajin_Setting_BreakUnitXZ.Name = "ajin_Setting_BreakUnitXZ";
            this.ajin_Setting_BreakUnitXZ.Size = new System.Drawing.Size(903, 183);
            this.ajin_Setting_BreakUnitXZ.TabIndex = 31;
            // 
            // ajin_Setting_BreakUnitTY
            // 
            this.ajin_Setting_BreakUnitTY.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_BreakUnitTY.Location = new System.Drawing.Point(792, 34);
            this.ajin_Setting_BreakUnitTY.Name = "ajin_Setting_BreakUnitTY";
            this.ajin_Setting_BreakUnitTY.Size = new System.Drawing.Size(903, 183);
            this.ajin_Setting_BreakUnitTY.TabIndex = 31;
            // 
            // ajin_Setting_UnloaderTransfer
            // 
            this.ajin_Setting_UnloaderTransfer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_UnloaderTransfer.Location = new System.Drawing.Point(792, 34);
            this.ajin_Setting_UnloaderTransfer.Name = "ajin_Setting_UnloaderTransfer";
            this.ajin_Setting_UnloaderTransfer.Size = new System.Drawing.Size(903, 183);
            this.ajin_Setting_UnloaderTransfer.TabIndex = 31;
            // 
            // ajin_Setting_CameraUnit
            // 
            this.ajin_Setting_CameraUnit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CameraUnit.Location = new System.Drawing.Point(792, 34);
            this.ajin_Setting_CameraUnit.Name = "ajin_Setting_CameraUnit";
            this.ajin_Setting_CameraUnit.Size = new System.Drawing.Size(903, 183);
            this.ajin_Setting_CameraUnit.TabIndex = 31;
            // 
            // ajin_Setting_CellInput
            // 
            this.ajin_Setting_CellInput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CellInput.Location = new System.Drawing.Point(792, 34);
            this.ajin_Setting_CellInput.Name = "ajin_Setting_CellInput";
            this.ajin_Setting_CellInput.Size = new System.Drawing.Size(903, 183);
            this.ajin_Setting_CellInput.TabIndex = 31;
            // 
            // ajin_Setting_CasseteUnload
            // 
            this.ajin_Setting_CasseteUnload.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ajin_Setting_CasseteUnload.Location = new System.Drawing.Point(792, 34);
            this.ajin_Setting_CasseteUnload.Name = "ajin_Setting_CasseteUnload";
            this.ajin_Setting_CasseteUnload.Size = new System.Drawing.Size(903, 183);
            this.ajin_Setting_CasseteUnload.TabIndex = 31;
            // 
            // Parameter_location
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Controls.Add(this.tc_iostatus_info);
            this.Name = "Parameter_location";
            this.Size = new System.Drawing.Size(1740, 875);
            this.tc_iostatus_info.ResumeLayout(false);
            this.tp_CasseteLoad.ResumeLayout(false);
            this.tp_CasseteLoad.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.gbox_CasseteLoad_Move.ResumeLayout(false);
            this.gbox_CasseteLoad_MoveB.ResumeLayout(false);
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel15.ResumeLayout(false);
            this.tableLayoutPanel16.ResumeLayout(false);
            this.gbox_CasseteLoad_MoveA.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.gbox_CasseteLoad_B.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.gbox_CasseteLoad_Muting.ResumeLayout(false);
            this.tp_Cellpurge.ResumeLayout(false);
            this.tp_Cellpurge.PerformLayout();
            this.gbox_Cellpurge_Move.ResumeLayout(false);
            this.gbox_Cellpurge_MoveCenter.ResumeLayout(false);
            this.tableLayoutPanel21.ResumeLayout(false);
            this.gbox_Cellpurge_MoveB.ResumeLayout(false);
            this.tableLayoutPanel20.ResumeLayout(false);
            this.gbox_Cellpurge_MoveA.ResumeLayout(false);
            this.tableLayoutPanel19.ResumeLayout(false);
            this.gbox_Cellpurge_B.ResumeLayout(false);
            this.tableLayoutPanel18.ResumeLayout(false);
            this.gbox_Cellpurge_A.ResumeLayout(false);
            this.tableLayoutPanel17.ResumeLayout(false);
            this.gbox_Cellpurge_Ionizer.ResumeLayout(false);
            this.tp_CellLoad.ResumeLayout(false);
            this.tp_CellLoad.PerformLayout();
            this.gbox_CellLoad_Move.ResumeLayout(false);
            this.gbox_CellLoad_MoveLorUn.ResumeLayout(false);
            this.tableLayoutPanel28.ResumeLayout(false);
            this.gbox_CellLoad_MoveB.ResumeLayout(false);
            this.tableLayoutPanel31.ResumeLayout(false);
            this.tableLayoutPanel32.ResumeLayout(false);
            this.tableLayoutPanel33.ResumeLayout(false);
            this.tableLayoutPanel34.ResumeLayout(false);
            this.gbox_CellLoad_MoveA.ResumeLayout(false);
            this.tableLayoutPanel26.ResumeLayout(false);
            this.tableLayoutPanel30.ResumeLayout(false);
            this.tableLayoutPanel29.ResumeLayout(false);
            this.tableLayoutPanel27.ResumeLayout(false);
            this.gbox_CellLoad_B.ResumeLayout(false);
            this.tableLayoutPanel25.ResumeLayout(false);
            this.gbox_CellLoad_A.ResumeLayout(false);
            this.tableLayoutPanel24.ResumeLayout(false);
            this.tp_IRCutProcess.ResumeLayout(false);
            this.tp_IRCutProcess.PerformLayout();
            this.gbox_IRCutProcess_Ionizer.ResumeLayout(false);
            this.gbox_IRCutProcess_Move.ResumeLayout(false);
            this.gbox_IRCutProcess_RightOffset.ResumeLayout(false);
            this.tableLayoutPanel40.ResumeLayout(false);
            this.gbox_IRCutProcess_LeftOffset.ResumeLayout(false);
            this.tableLayoutPanel39.ResumeLayout(false);
            this.gbox_IRCutProcess_MoveB.ResumeLayout(false);
            this.tableLayoutPanel38.ResumeLayout(false);
            this.gbox_IRCutProcess_MoveA.ResumeLayout(false);
            this.tableLayoutPanel37.ResumeLayout(false);
            this.gbox_IRCutProcess_B.ResumeLayout(false);
            this.tableLayoutPanel36.ResumeLayout(false);
            this.gbox_IRCutProcess_A.ResumeLayout(false);
            this.tableLayoutPanel35.ResumeLayout(false);
            this.tp_BreakTransfer.ResumeLayout(false);
            this.tp_BreakTransfer.PerformLayout();
            this.gbox_BreakTransfer_Move.ResumeLayout(false);
            this.gbox_BreakTransfer_MoveB.ResumeLayout(false);
            this.tableLayoutPanel44.ResumeLayout(false);
            this.gbox_BreakTransfer_MoveA.ResumeLayout(false);
            this.tableLayoutPanel43.ResumeLayout(false);
            this.gbox_BreakTransfer_B.ResumeLayout(false);
            this.tableLayoutPanel42.ResumeLayout(false);
            this.gbox_BreakTransfer_A.ResumeLayout(false);
            this.tableLayoutPanel41.ResumeLayout(false);
            this.tp_BreakUnitXZ.ResumeLayout(false);
            this.tp_BreakUnitXZ.PerformLayout();
            this.gbox_BreakUnitXZ_Move.ResumeLayout(false);
            this.tableLayoutPanel45.ResumeLayout(false);
            this.gbox_BreakUnitXZ_MoveZ4.ResumeLayout(false);
            this.tableLayoutPanel49.ResumeLayout(false);
            this.gbox_BreakUnitXZ_MoveZ3.ResumeLayout(false);
            this.tableLayoutPanel48.ResumeLayout(false);
            this.gbox_BreakUnitXZ_MoveZ2.ResumeLayout(false);
            this.tableLayoutPanel47.ResumeLayout(false);
            this.gbox_BreakUnitXZ_MoveZ1.ResumeLayout(false);
            this.tableLayoutPanel46.ResumeLayout(false);
            this.tp_BreakUnitTY.ResumeLayout(false);
            this.tp_BreakUnitTY.PerformLayout();
            this.gbox_BreakUnitTY_Shutter.ResumeLayout(false);
            this.tableLayoutPanel53.ResumeLayout(false);
            this.gbox_BreakUnitTY_DummyBox.ResumeLayout(false);
            this.tableLayoutPanel52.ResumeLayout(false);
            this.gbox_BreakUnitTY_Ionizer.ResumeLayout(false);
            this.gbox_BreakUnitTY_Move.ResumeLayout(false);
            this.tableLayoutPanel54.ResumeLayout(false);
            this.gbox_BreakUnitTY_Theta.ResumeLayout(false);
            this.tableLayoutPanel57.ResumeLayout(false);
            this.gbox_BreakUnitTY_MoveB.ResumeLayout(false);
            this.tableLayoutPanel56.ResumeLayout(false);
            this.gbox_BreakUnitTY_MoveA.ResumeLayout(false);
            this.tableLayoutPanel55.ResumeLayout(false);
            this.gbox_BreakUnitTY_B.ResumeLayout(false);
            this.tableLayoutPanel50.ResumeLayout(false);
            this.gbox_BreakUnitTY_A.ResumeLayout(false);
            this.tableLayoutPanel51.ResumeLayout(false);
            this.tp_UnloaderTransfer.ResumeLayout(false);
            this.tp_UnloaderTransfer.PerformLayout();
            this.gbox_UnloaderTransfer_Move.ResumeLayout(false);
            this.gbox_UnloaderTransfer_MoveB.ResumeLayout(false);
            this.tableLayoutPanel61.ResumeLayout(false);
            this.tableLayoutPanel62.ResumeLayout(false);
            this.tableLayoutPanel63.ResumeLayout(false);
            this.gbox_UnloaderTransfer_MoveA.ResumeLayout(false);
            this.tableLayoutPanel58.ResumeLayout(false);
            this.tableLayoutPanel60.ResumeLayout(false);
            this.tableLayoutPanel59.ResumeLayout(false);
            this.gbox_UnloaderTransfer_B.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.gbox_UnloaderTransfer_A.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tp_CameraUnit.ResumeLayout(false);
            this.tp_CameraUnit.PerformLayout();
            this.gbox_CameraUnit_Move.ResumeLayout(false);
            this.gbox_CameraUnit_MoveB.ResumeLayout(false);
            this.tableLayoutPanel65.ResumeLayout(false);
            this.gbox_CameraUnit_MoveA.ResumeLayout(false);
            this.tableLayoutPanel64.ResumeLayout(false);
            this.tp_CellInput.ResumeLayout(false);
            this.tp_CellInput.PerformLayout();
            this.gbox_CellInput_Buffer.ResumeLayout(false);
            this.tableLayoutPanel66.ResumeLayout(false);
            this.gbox_CellInput_Ionizer.ResumeLayout(false);
            this.gbox_CellInput_Move.ResumeLayout(false);
            this.gbox_CellInput_MoveCenter.ResumeLayout(false);
            this.tableLayoutPanel69.ResumeLayout(false);
            this.gbox_CellInput_MoveB.ResumeLayout(false);
            this.tableLayoutPanel68.ResumeLayout(false);
            this.gbox_CellInput_MoveA.ResumeLayout(false);
            this.tableLayoutPanel67.ResumeLayout(false);
            this.gbox_CellInput_B.ResumeLayout(false);
            this.tableLayoutPanel23.ResumeLayout(false);
            this.gbox_CellInput_A.ResumeLayout(false);
            this.tableLayoutPanel22.ResumeLayout(false);
            this.tp_CasseteUnload.ResumeLayout(false);
            this.tp_CasseteUnload.PerformLayout();
            this.gbox_CasseteUnload_Move.ResumeLayout(false);
            this.gbox_CasseteUnload_MoveB.ResumeLayout(false);
            this.tableLayoutPanel75.ResumeLayout(false);
            this.tableLayoutPanel76.ResumeLayout(false);
            this.tableLayoutPanel77.ResumeLayout(false);
            this.tableLayoutPanel78.ResumeLayout(false);
            this.tableLayoutPanel79.ResumeLayout(false);
            this.gbox_CasseteUnload_MoveA.ResumeLayout(false);
            this.tableLayoutPanel70.ResumeLayout(false);
            this.tableLayoutPanel71.ResumeLayout(false);
            this.tableLayoutPanel72.ResumeLayout(false);
            this.tableLayoutPanel73.ResumeLayout(false);
            this.tableLayoutPanel74.ResumeLayout(false);
            this.gbox_CasseteUnload_B.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.gbox_CasseteUnload_A.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.gbox_CasseteUnload.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tc_iostatus_info;
        private System.Windows.Forms.TabPage tp_CasseteLoad;
        private System.Windows.Forms.TabPage tp_Cellpurge;
        private System.Windows.Forms.TabPage tp_CellLoad;
        private System.Windows.Forms.TabPage tp_IRCutProcess;
        private System.Windows.Forms.TabPage tp_BreakTransfer;
        private System.Windows.Forms.TabPage tp_BreakUnitXZ;
        private System.Windows.Forms.TabPage tp_BreakUnitTY;
        private System.Windows.Forms.TabPage tp_UnloaderTransfer;
        private System.Windows.Forms.TabPage tp_CameraUnit;
        private System.Windows.Forms.TabPage tp_CellInput;
        private System.Windows.Forms.TabPage tp_CasseteUnload;
        private System.Windows.Forms.GroupBox gbox_CasseteLoad_Move;
        private System.Windows.Forms.GroupBox gbox_CasseteLoad_MoveB;
        private System.Windows.Forms.GroupBox gbox_CasseteLoad_MoveA;
        private System.Windows.Forms.GroupBox gbox_CasseteLoad_B;
        private System.Windows.Forms.GroupBox gbox_CasseteLoad_A;
        private System.Windows.Forms.Button btn_CasseteLoad_Save;
        private System.Windows.Forms.GroupBox gbox_CasseteLoad_Muting;
        private System.Windows.Forms.Button btn_CasseteLoad_Muting4;
        private System.Windows.Forms.Button btn_CasseteLoad_Muting2;
        private System.Windows.Forms.Button btn_CasseteLoad_Muting3;
        private System.Windows.Forms.Button btn_CasseteLoad_Muting1;
        private System.Windows.Forms.Button btn_CasseteLoad_MutingOff;
        private System.Windows.Forms.Button btn_CasseteLoad_MutingIn;
        private System.Windows.Forms.Button btn_CasseteLoad_SpeedSetting;
        private System.Windows.Forms.Button btn_CasseteLoad_AllSetting;
        private System.Windows.Forms.Button btn_CasseteLoad_LocationSetting;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveLocation;
        private System.Windows.Forms.TextBox tbox_CasseteLoad_Location;
        private System.Windows.Forms.TextBox tbox_CasseteLoad_Speed;
        private System.Windows.Forms.TextBox tbox_CasseteLoad_CurrentLocation;
        private System.Windows.Forms.TextBox tbox_CasseteLoad_SelectedShaft;
        private System.Windows.Forms.Label lbl_CasseteLoad_Location;
        private System.Windows.Forms.Label lbl_CasseteLoad_Speed;
        private System.Windows.Forms.Label lbl_CasseteLoad_SelectedShaft;
        private System.Windows.Forms.TextBox tbox_CasseteLoad_LocationSetting;
        private System.Windows.Forms.Label lbl_CasseteLoad_CurrentLocation;
        private System.Windows.Forms.Button btn_CasseteLoad_GrabSwitch3;
        private System.Windows.Forms.Button btn_CasseteLoad_GrabSwitch2;
        private System.Windows.Forms.Button btn_CasseteLoad_GrabSwitch1;
        private System.Windows.Forms.ListView lv_CasseteLoad;
        private System.Windows.Forms.ColumnHeader col_CasseteLoad_NamebyLocation;
        private System.Windows.Forms.ColumnHeader col_CasseteLoad_Shaft;
        private System.Windows.Forms.ColumnHeader col_CasseteLoad_Location;
        private System.Windows.Forms.ColumnHeader col_CasseteLoad_Speed;
        private UcrlAjinServoCtrl ajin_Setting_CasseteLoad;
        private System.Windows.Forms.GroupBox gbox_Cellpurge_Move;
        private System.Windows.Forms.GroupBox gbox_Cellpurge_MoveA;
        private System.Windows.Forms.GroupBox gbox_Cellpurge_B;
        private System.Windows.Forms.GroupBox gbox_Cellpurge_A;
        private System.Windows.Forms.Button btn_Cellpurge_Save;
        private System.Windows.Forms.GroupBox gbox_Cellpurge_Ionizer;
        private System.Windows.Forms.Button btn_Cellpurge_DestructionOff;
        private System.Windows.Forms.Button btn_Cellpurge_IonizerOff;
        private System.Windows.Forms.Button btn_Cellpurge_DestructionOn;
        private System.Windows.Forms.Button btn_Cellpurge_IonizerOn;
        private System.Windows.Forms.Button btn_Cellpurge_SpeedSetting;
        private System.Windows.Forms.Button btn_Cellpurge_AllSetting;
        private System.Windows.Forms.Button btn_Cellpurge_LocationSetting;
        private System.Windows.Forms.Button btn_Cellpurge_MoveLocation;
        private System.Windows.Forms.TextBox tbox_Cellpurge_Location;
        private System.Windows.Forms.TextBox tbox_Cellpurge_Speed;
        private System.Windows.Forms.TextBox tbox_Cellpurge_CurrentLocation;
        private System.Windows.Forms.TextBox tbox_Cellpurge_SelectedShaft;
        private System.Windows.Forms.Label lbl_Cellpurge_Location;
        private System.Windows.Forms.Label lbl_Cellpurge_Speed;
        private System.Windows.Forms.Label lbl_Cellpurge_SelectedShaft;
        private System.Windows.Forms.TextBox tbox_Cellpurge_LocationSetting;
        private System.Windows.Forms.Label lbl_Cellpurge_CurrentLocation;
        private System.Windows.Forms.Button btn_Cellpurge_GrabSwitch3;
        private System.Windows.Forms.Button btn_Cellpurge_GrabSwitch2;
        private System.Windows.Forms.Button btn_Cellpurge_GrabSwitch1;
        private System.Windows.Forms.ListView lv_Cellpurge;
        private System.Windows.Forms.ColumnHeader col_Cellpurge_NamebyLocation;
        private System.Windows.Forms.ColumnHeader col_Cellpurge_Shaft;
        private System.Windows.Forms.ColumnHeader col_Cellpurge_Location;
        private System.Windows.Forms.ColumnHeader col_Cellpurge_Speed;
        private UcrlAjinServoCtrl ajin_Setting_Cellpurge;
        private System.Windows.Forms.GroupBox gbox_CellLoad_Move;
        private System.Windows.Forms.GroupBox gbox_CellLoad_B;
        private System.Windows.Forms.GroupBox gbox_CellLoad_A;
        private System.Windows.Forms.Button btn_CellLoad_Save;
        private System.Windows.Forms.Button btn_CellLoad_SpeedSetting;
        private System.Windows.Forms.Button btn_CellLoad_AllSetting;
        private System.Windows.Forms.Button btn_CellLoad_LocationSetting;
        private System.Windows.Forms.Button btn_CellLoad_MoveLocation;
        private System.Windows.Forms.TextBox tbox_CellLoad_Location;
        private System.Windows.Forms.TextBox tbox_CellLoad_Speed;
        private System.Windows.Forms.TextBox tbox_CellLoad_CurrentLocation;
        private System.Windows.Forms.TextBox tbox_CellLoad_SelectedShaft;
        private System.Windows.Forms.Label lbl_CellLoad_Location;
        private System.Windows.Forms.Label lbl_CellLoad_Speed;
        private System.Windows.Forms.Label lbl_CellLoad_SelectedShaft;
        private System.Windows.Forms.TextBox tbox_CellLoad_LocationSetting;
        private System.Windows.Forms.Label lbl_CellLoad_CurrentLocation;
        private System.Windows.Forms.Button btn_CellLoad_GrabSwitch3;
        private System.Windows.Forms.Button btn_CellLoad_GrabSwitch2;
        private System.Windows.Forms.Button btn_CellLoad_GrabSwitch1;
        private System.Windows.Forms.ListView lv_CellLoad;
        private System.Windows.Forms.ColumnHeader col_CellLoad_NamebyLocation;
        private System.Windows.Forms.ColumnHeader col_CellLoad_Shaft;
        private System.Windows.Forms.ColumnHeader col_CellLoad_Location;
        private System.Windows.Forms.ColumnHeader col_CellLoad_Speed;
        private UcrlAjinServoCtrl ajin_Setting_CellLoad;
        private System.Windows.Forms.GroupBox gbox_IRCutProcess_Ionizer;
        private System.Windows.Forms.Button btn_IRCutProcess_Destruction;
        private System.Windows.Forms.Button btn_IRCutProcess_IonizerOff;
        private System.Windows.Forms.Button btn_IRCutProcess_DestructionOn;
        private System.Windows.Forms.Button btn_IRCutProcess_IonizerOn;
        private System.Windows.Forms.GroupBox gbox_IRCutProcess_Move;
        private System.Windows.Forms.GroupBox gbox_IRCutProcess_MoveB;
        private System.Windows.Forms.GroupBox gbox_IRCutProcess_MoveA;
        private System.Windows.Forms.GroupBox gbox_IRCutProcess_B;
        private System.Windows.Forms.GroupBox gbox_IRCutProcess_A;
        private System.Windows.Forms.Button btn_IRCutProcess_Save;
        private System.Windows.Forms.Button btn_IRCutProcess_SpeedSetting;
        private System.Windows.Forms.Button btn_IRCutProcess_AllSetting;
        private System.Windows.Forms.Button btn_IRCutProcess_LocationSetting;
        private System.Windows.Forms.Button btn_IRCutProcess_MoveLocation;
        private System.Windows.Forms.TextBox tbox_IRCutProcess_Location;
        private System.Windows.Forms.TextBox tbox_IRCutProcess_Speed;
        private System.Windows.Forms.TextBox tbox_IRCutProcess_CurrentLocation;
        private System.Windows.Forms.TextBox tbox_IRCutProcess_SelectedShaft;
        private System.Windows.Forms.Label lbl_IRCutProcess_Location;
        private System.Windows.Forms.Label lbl_IRCutProcess_Speed;
        private System.Windows.Forms.Label lbl_IRCutProcess_SelectedShaft;
        private System.Windows.Forms.TextBox tbox_IRCutProcess_LocationSetting;
        private System.Windows.Forms.Label lbl_IRCutProcess_CurrentLocation;
        private System.Windows.Forms.Button btn_IRCutProcess_GrabSwitch3;
        private System.Windows.Forms.Button btn_IRCutProcess_GrabSwitch2;
        private System.Windows.Forms.Button btn_IRCutProcess_GrabSwitch1;
        private System.Windows.Forms.ListView lv_IRCutProcess;
        private System.Windows.Forms.ColumnHeader col_IRCutProcess_NamebyLocation;
        private System.Windows.Forms.ColumnHeader col_IRCutProcess_Shaft;
        private System.Windows.Forms.ColumnHeader col_IRCutProcess_Location;
        private System.Windows.Forms.ColumnHeader col_IRCutProcess_Speed;
        private UcrlAjinServoCtrl ajin_Setting_IRCutProcess;
        private System.Windows.Forms.GroupBox gbox_BreakTransfer_Move;
        private System.Windows.Forms.GroupBox gbox_BreakTransfer_MoveB;
        private System.Windows.Forms.GroupBox gbox_BreakTransfer_MoveA;
        private System.Windows.Forms.GroupBox gbox_BreakTransfer_B;
        private System.Windows.Forms.GroupBox gbox_BreakTransfer_A;
        private System.Windows.Forms.Button btn_BreakTransfer_Save;
        private System.Windows.Forms.Button btn_BreakTransfer_SpeedSetting;
        private System.Windows.Forms.Button btn_BreakTransfer_AllSetting;
        private System.Windows.Forms.Button btn_BreakTransfer_LocationSetting;
        private System.Windows.Forms.Button btn_BreakTransfer_MoveLocation;
        private System.Windows.Forms.TextBox tbox_BreakTransfer_Location;
        private System.Windows.Forms.TextBox tbox_BreakTransfer_Speed;
        private System.Windows.Forms.TextBox tbox_BreakTransfer_CurrentLocation;
        private System.Windows.Forms.TextBox tbox_BreakTransfer_SelectedShaft;
        private System.Windows.Forms.Label lbl_BreakTransfer_Location;
        private System.Windows.Forms.Label lbl_BreakTransfer_Speed;
        private System.Windows.Forms.Label lbl_BreakTransfer_SelectedShaft;
        private System.Windows.Forms.TextBox tbox_BreakTransfer_LocationSetting;
        private System.Windows.Forms.Label lbl_BreakTransfer_CurrentLocation;
        private System.Windows.Forms.Button btn_BreakTransfer_GrabSwitch3;
        private System.Windows.Forms.Button btn_BreakTransfer_GrabSwitch2;
        private System.Windows.Forms.Button btn_BreakTransfer_GrabSwitch1;
        private System.Windows.Forms.ListView lv_BreakTransfer;
        private System.Windows.Forms.ColumnHeader col_BreakTransfer_NamebyLocation;
        private System.Windows.Forms.ColumnHeader col_BreakTransfer_Shaft;
        private System.Windows.Forms.ColumnHeader col_BreakTransfer_Location;
        private System.Windows.Forms.ColumnHeader col_BreakTransfer_Speed;
        private UcrlAjinServoCtrl ajin_Setting_BreakTransfer;
        private System.Windows.Forms.GroupBox gbox_BreakUnitXZ_Move;
        private System.Windows.Forms.Button gbox_BreakUnitXZ_Save;
        private System.Windows.Forms.Button btn_BreakUnitXZ_SpeedSetting;
        private System.Windows.Forms.Button btn_BreakUnitXZ_AllSetting;
        private System.Windows.Forms.Button btn_BreakUnitXZ_LocationSetting;
        private System.Windows.Forms.Button btn_BreakUnitXZ_MoveLocation;
        private System.Windows.Forms.TextBox tbox_BreakUnitXZ_Location;
        private System.Windows.Forms.TextBox tbox_BreakUnitXZ_Speed;
        private System.Windows.Forms.TextBox tbox_BreakUnitXZ_CurrentLocation;
        private System.Windows.Forms.TextBox tbox_BreakUnitXZ_SelectedShaft;
        private System.Windows.Forms.Label lbl_BreakUnitXZ_Location;
        private System.Windows.Forms.Label lbl_BreakUnitXZ_Speed;
        private System.Windows.Forms.Label lbl_BreakUnitXZ_SelectedShaft;
        private System.Windows.Forms.TextBox tbox_BreakUnitXZ_LocationSetting;
        private System.Windows.Forms.Label lbl_BreakUnitXZ_CurrentLocation;
        private System.Windows.Forms.Button btn_BreakUnitXZ_GrabSwitch3;
        private System.Windows.Forms.Button btn_BreakUnitXZ_GrabSwitch2;
        private System.Windows.Forms.Button btn_BreakUnitXZ_GrabSwitch1;
        private System.Windows.Forms.ListView lv_BreakUnitXZ;
        private System.Windows.Forms.ColumnHeader col_BreakUnitXZ_NamebyLocation;
        private System.Windows.Forms.ColumnHeader col_BreakUnitXZ_Shaft;
        private System.Windows.Forms.ColumnHeader col_BreakUnitXZ_Location;
        private System.Windows.Forms.ColumnHeader col_BreakUnitXZ_Speed;
        private UcrlAjinServoCtrl ajin_Setting_BreakUnitXZ;
        private System.Windows.Forms.GroupBox gbox_BreakUnitTY_Ionizer;
        private System.Windows.Forms.Button btn_BreakUnitTY_DestructionOff;
        private System.Windows.Forms.Button btn_BreakUnitTY_IonizerOff;
        private System.Windows.Forms.Button btn_BreakUnitTY_DestructionOn;
        private System.Windows.Forms.Button btn_BreakUnitTY_IonizerOn;
        private System.Windows.Forms.GroupBox gbox_BreakUnitTY_Move;
        private System.Windows.Forms.Button btn_BreakUnitTY_Save;
        private System.Windows.Forms.Button btn_BreakUnitTY_SpeedSetting;
        private System.Windows.Forms.Button btn_BreakUnitTY_AllSetting;
        private System.Windows.Forms.Button btn_BreakUnitTY_LocationSetting;
        private System.Windows.Forms.Button btn_BreakUnitTY_MoveLocation;
        private System.Windows.Forms.TextBox tbox_BreakUnitTY_Location;
        private System.Windows.Forms.TextBox tbox_BreakUnitTY_Speed;
        private System.Windows.Forms.TextBox tbox_BreakUnitTY_CurrentLocation;
        private System.Windows.Forms.TextBox tbox_BreakUnitTY_SelectedShaft;
        private System.Windows.Forms.Label lbl_BreakUnitTY_Location;
        private System.Windows.Forms.Label lbl_BreakUnitTY_Speed;
        private System.Windows.Forms.Label lbl_BreakUnitTY_SelectedShaft;
        private System.Windows.Forms.TextBox tbox_BreakUnitTY_LocationSetting;
        private System.Windows.Forms.Label lbl_BreakUnitTY_CurrentLocation;
        private System.Windows.Forms.Button btn_BreakUnitTY_GrabSwitch3;
        private System.Windows.Forms.Button btn_BreakUnitTY_GrabSwitch2;
        private System.Windows.Forms.Button btn_BreakUnitTY_GrabSwitch1;
        private System.Windows.Forms.ListView lv_BreakUnitTY;
        private System.Windows.Forms.ColumnHeader col_BreakUnitTY_NamebyLocation;
        private System.Windows.Forms.ColumnHeader col_BreakUnitTY_Shaft;
        private System.Windows.Forms.ColumnHeader col_BreakUnitTY_Location;
        private System.Windows.Forms.ColumnHeader col_BreakUnitTY_Speed;
        private UcrlAjinServoCtrl ajin_Setting_BreakUnitTY;
        private System.Windows.Forms.GroupBox gbox_UnloaderTransfer_Move;
        private System.Windows.Forms.GroupBox gbox_UnloaderTransfer_MoveB;
        private System.Windows.Forms.GroupBox gbox_UnloaderTransfer_MoveA;
        private System.Windows.Forms.GroupBox gbox_UnloaderTransfer_B;
        private System.Windows.Forms.GroupBox gbox_UnloaderTransfer_A;
        private System.Windows.Forms.Button btn_UnloaderTransfer_Save;
        private System.Windows.Forms.Button btn_UnloaderTransfer_SpeedSetting;
        private System.Windows.Forms.Button btn_UnloaderTransfer_AllSetting;
        private System.Windows.Forms.Button btn_UnloaderTransfer_LocationSetting;
        private System.Windows.Forms.Button btn_UnloaderTransfer_MoveLocation;
        private System.Windows.Forms.TextBox tbox_UnloaderTransfer_Location;
        private System.Windows.Forms.TextBox tbox_UnloaderTransfer_Speed;
        private System.Windows.Forms.TextBox tbox_UnloaderTransfer_CurrentLocation;
        private System.Windows.Forms.TextBox tbox_UnloaderTransfer_SelectedShaft;
        private System.Windows.Forms.Label lbl_UnloaderTransfer_Location;
        private System.Windows.Forms.Label lbl_UnloaderTransfer_Speed;
        private System.Windows.Forms.Label lbl_UnloaderTransfer_SeletedShaft;
        private System.Windows.Forms.TextBox tbox_UnloaderTransfer_LocationSetting;
        private System.Windows.Forms.Label lbl_UnloaderTransfer_CurrentLocation;
        private System.Windows.Forms.Button btn_UnloaderTransfer_GrabSwitch3;
        private System.Windows.Forms.Button btn_UnloaderTransfer_GrabSwitch2;
        private System.Windows.Forms.Button btn_UnloaderTransfer_GrabSwitch1;
        private System.Windows.Forms.ListView lv_UnloaderTransfer;
        private System.Windows.Forms.ColumnHeader col_UnloaderTransfer_NamebyLocation;
        private System.Windows.Forms.ColumnHeader col_UnloaderTransfer_Shaft;
        private System.Windows.Forms.ColumnHeader col_UnloaderTransfer_Location;
        private System.Windows.Forms.ColumnHeader col_UnloaderTransfer_Speed;
        private UcrlAjinServoCtrl ajin_Setting_UnloaderTransfer;
        private System.Windows.Forms.GroupBox gbox_CameraUnit_Move;
        private System.Windows.Forms.GroupBox gbox_CameraUnit_MoveB;
        private System.Windows.Forms.GroupBox gbox_CameraUnit_MoveA;
        private System.Windows.Forms.Button btn_CameraUnit_Save;
        private System.Windows.Forms.Button btn_CameraUnit_SpeedSetting;
        private System.Windows.Forms.Button btn_CameraUnit_AllSetting;
        private System.Windows.Forms.Button btn_CameraUnit_LocationSetting;
        private System.Windows.Forms.Button btn_CameraUnit_MoveLocation;
        private System.Windows.Forms.TextBox tbox_CameraUnit_Location;
        private System.Windows.Forms.TextBox tbox_CameraUnit_Speed;
        private System.Windows.Forms.TextBox tbox_CameraUnit_CurrentLocation;
        private System.Windows.Forms.TextBox tbox_CameraUnit_SelectedShaft;
        private System.Windows.Forms.Label lbl_CameraUnit_Location;
        private System.Windows.Forms.Label lbl_CameraUnit_Speed;
        private System.Windows.Forms.Label lbl_CameraUnit_SelectedShaft;
        private System.Windows.Forms.TextBox tbox_CameraUnit_LocationSetting;
        private System.Windows.Forms.Label lbl_CameraUnit_CurrentLocation;
        private System.Windows.Forms.Button btn_CameraUnit_GrabSwitch3;
        private System.Windows.Forms.Button btn_CameraUnit_GrabSwitch2;
        private System.Windows.Forms.Button btn_CameraUnit_GrabSwitch1;
        private System.Windows.Forms.ListView lv_CameraUnit;
        private System.Windows.Forms.ColumnHeader col_CameraUnit_NamebyLocation;
        private System.Windows.Forms.ColumnHeader col_CameraUnit_Shaft;
        private System.Windows.Forms.ColumnHeader col_CameraUnit_Location;
        private System.Windows.Forms.ColumnHeader col_CameraUnit_Speed;
        private UcrlAjinServoCtrl ajin_Setting_CameraUnit;
        private System.Windows.Forms.GroupBox gbox_CellInput_Ionizer;
        private System.Windows.Forms.Button btn_CellInput_DestructionOff;
        private System.Windows.Forms.Button btn_CellInput_IonizerOff;
        private System.Windows.Forms.Button btn_CellInput_DestructionOn;
        private System.Windows.Forms.Button btn_CellInput_IonizerOn;
        private System.Windows.Forms.GroupBox gbox_CellInput_Move;
        private System.Windows.Forms.GroupBox gbox_CellInput_MoveB;
        private System.Windows.Forms.GroupBox gbox_CellInput_MoveA;
        private System.Windows.Forms.Button btn_CellInput_Save;
        private System.Windows.Forms.Button btn_CellInput_SpeedSetting;
        private System.Windows.Forms.Button btn_CellInput_AllSetting;
        private System.Windows.Forms.Button btn_CellInput_LocationSetting;
        private System.Windows.Forms.Button btn_CellInput_MoveLocation;
        private System.Windows.Forms.TextBox tbox_CellInput_Location;
        private System.Windows.Forms.TextBox tbox_CellInput_Speed;
        private System.Windows.Forms.TextBox tbox_CellInput_CurrentLocation;
        private System.Windows.Forms.TextBox tbox_CellInput_SelectedShaft;
        private System.Windows.Forms.Label lbl_CellInput_Location;
        private System.Windows.Forms.Label lbl_CellInput_Speed;
        private System.Windows.Forms.Label lbl_CellInput_SelectedShaft;
        private System.Windows.Forms.TextBox tbox_CellInput_LocationSetting;
        private System.Windows.Forms.Label lbl_CellInput_CurrentLocation;
        private System.Windows.Forms.Button btn_CellInput_GrabSwitch3;
        private System.Windows.Forms.Button btn_CellInput_GrabSwitch2;
        private System.Windows.Forms.Button btn_CellInput_GrabSwitch1;
        private System.Windows.Forms.ListView lv_CellInput;
        private System.Windows.Forms.ColumnHeader col_CellInput_NamebyLocation;
        private System.Windows.Forms.ColumnHeader col_CellInput_Shaft;
        private System.Windows.Forms.ColumnHeader col_CellInput_Location;
        private System.Windows.Forms.ColumnHeader col_CellInput_Speed;
        private UcrlAjinServoCtrl ajin_Setting_CellInput;
        private System.Windows.Forms.GroupBox gbox_CasseteUnload_Move;
        private System.Windows.Forms.GroupBox gbox_CasseteUnload_MoveB;
        private System.Windows.Forms.GroupBox gbox_CasseteUnload_MoveA;
        private System.Windows.Forms.GroupBox gbox_CasseteUnload_B;
        private System.Windows.Forms.GroupBox gbox_CasseteUnload_A;
        private System.Windows.Forms.Button btn_CasseteUnload_Save;
        private System.Windows.Forms.GroupBox gbox_CasseteUnload;
        private System.Windows.Forms.Button btn_CasseteUnload_Muting4;
        private System.Windows.Forms.Button btn_CasseteUnload_Muting2;
        private System.Windows.Forms.Button btn_CasseteUnload_Muting3;
        private System.Windows.Forms.Button btn_CasseteUnload_Muting1;
        private System.Windows.Forms.Button btn_CasseteUnload_MutingOut;
        private System.Windows.Forms.Button btn_CasseteUnload_MutingIn;
        private System.Windows.Forms.Button btn_CasseteUnload_SpeedSetting;
        private System.Windows.Forms.Button btn_CasseteUnload_AllSetting;
        private System.Windows.Forms.Button btn_CasseteUnload_LocationSetting;
        private System.Windows.Forms.Button btn_CasseteUnload_MoveLocation;
        private System.Windows.Forms.TextBox tbox_CasseteUnload_Location;
        private System.Windows.Forms.TextBox tbox_CasseteUnload_Speed;
        private System.Windows.Forms.TextBox tbox_CasseteUnload_CurrentLocation;
        private System.Windows.Forms.TextBox tbox_CasseteUnload_SelectedShaft;
        private System.Windows.Forms.Label lbl_CasseteUnload_Location;
        private System.Windows.Forms.Label lbl_CasseteUnload_Speed;
        private System.Windows.Forms.Label lbl_CasseteUnload_SelectedShaft;
        private System.Windows.Forms.TextBox tbox_CasseteUnload_LocationSetting;
        private System.Windows.Forms.Label lbl_CasseteUnload_CurrentLocation;
        private System.Windows.Forms.Button btn_CasseteUnload_GrabSwitch3;
        private System.Windows.Forms.Button btn_CasseteUnload_GrabSwitch2;
        private System.Windows.Forms.Button btn_CasseteUnload_GrabSwitch1;
        private System.Windows.Forms.ListView lv_CasseteUnload;
        private System.Windows.Forms.ColumnHeader col_CasseteUnload_NamebyLocation;
        private System.Windows.Forms.ColumnHeader col_CasseteUnload_Shaft;
        private System.Windows.Forms.ColumnHeader col_CasseteUnload_Location;
        private System.Windows.Forms.ColumnHeader col_CasseteUnload_Speed;
        private UcrlAjinServoCtrl ajin_Setting_CasseteUnload;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btn_CasseteLoad_A_BCSD;
        private System.Windows.Forms.Button btn_CasseteLoad_A_BCSU;
        private System.Windows.Forms.Button btn_CasseteLoad_A_BCUG;
        private System.Windows.Forms.Button btn_CasseteLoad_A_BCG;
        private System.Windows.Forms.Button btn_CasseteLoad_A_LTSD;
        private System.Windows.Forms.Button btn_CasseteLoad_A_LTSU;
        private System.Windows.Forms.Button btn_CasseteLoad_A_LUG;
        private System.Windows.Forms.Button btn_CasseteLoad_A_LG;
        private System.Windows.Forms.Button btn_CasseteLoad_A_TCSD;
        private System.Windows.Forms.Button btn_CasseteLoad_A_TCSU;
        private System.Windows.Forms.Button btn_CasseteLoad_A_TCUG;
        private System.Windows.Forms.Button btn_CasseteLoad_A_TCG;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button btn_CasseteLoad_B_BCSD;
        private System.Windows.Forms.Button btn_CasseteLoad_B_BCSU;
        private System.Windows.Forms.Button btn_CasseteLoad_B_BCUG;
        private System.Windows.Forms.Button btn_CasseteLoad_B_BCG;
        private System.Windows.Forms.Button btn_CasseteLoad_B_LTSD;
        private System.Windows.Forms.Button btn_CasseteLoad_B_LTSU;
        private System.Windows.Forms.Button btn_CasseteLoad_B_LUG;
        private System.Windows.Forms.Button btn_CasseteLoad_B_LG;
        private System.Windows.Forms.Button btn_CasseteLoad_B_TCSD;
        private System.Windows.Forms.Button btn_CasseteLoad_B_TCSU;
        private System.Windows.Forms.Button btn_CasseteLoad_B_TCUG;
        private System.Windows.Forms.Button btn_CasseteLoad_B_TCG;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Button btn_UnloaderTransfer_B_PickerDestruction2;
        private System.Windows.Forms.Button btn_UnloaderTransfer_B_PickerDestructionOn2;
        private System.Windows.Forms.Button btn_UnloaderTransfer_B_PickerDestructionOff1;
        private System.Windows.Forms.Button btn_UnloaderTransfer_B_PickerDestructionOn1;
        private System.Windows.Forms.Button btn_UnloaderTransfer_B_PickerPneumaticOff2;
        private System.Windows.Forms.Button btn_UnloaderTransfer_B_PickerPneumaticOn2;
        private System.Windows.Forms.Button btn_UnloaderTransfer_B_PickerPneumaticOff1;
        private System.Windows.Forms.Button btn_UnloaderTransfer_B_PickerPneumaticOn1;
        private System.Windows.Forms.Button btn_UnloaderTransfer_B_PickerDown2;
        private System.Windows.Forms.Button btn_UnloaderTransfer_B_PickerUp2;
        private System.Windows.Forms.Button btn_UnloaderTransfer_B_PickerDown1;
        private System.Windows.Forms.Button btn_UnloaderTransfer_B_PickerUp1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button btn_UnloaderTransfer_A_PickerDestructionOff2;
        private System.Windows.Forms.Button btn_UnloaderTransfer_A_PickerDestructionOn2;
        private System.Windows.Forms.Button btn_UnloaderTransfer_A_PickerDestructionOff1;
        private System.Windows.Forms.Button btn_UnloaderTransfer_A_PickerDestructionOn1;
        private System.Windows.Forms.Button btn_UnloaderTransfer_A_PickerPneumaticOff2;
        private System.Windows.Forms.Button btn_UnloaderTransfer_A_PickerPneumaticOn2;
        private System.Windows.Forms.Button btn_UnloaderTransfer_A_PickerPneumaticOff1;
        private System.Windows.Forms.Button btn_UnloaderTransfer_A_PickerPneumaticOn1;
        private System.Windows.Forms.Button btn_UnloaderTransfer_A_PickerDown2;
        private System.Windows.Forms.Button btn_UnloaderTransfer_A_PickerUp2;
        private System.Windows.Forms.Button btn_UnloaderTransfer_A_PickerDown1;
        private System.Windows.Forms.Button btn_UnloaderTransfer_A_PickerUp1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Button btn_CasseteUnload_B_BottomCstStopperDown;
        private System.Windows.Forms.Button btn_CasseteUnload_B_BottomCstStopperUp;
        private System.Windows.Forms.Button btn_CasseteUnload_B_BottomCstUngrib;
        private System.Windows.Forms.Button btn_CasseteUnload_B_BottomCstGrib;
        private System.Windows.Forms.Button btn_CasseteUnload_B_LiftTiltSlinderDown;
        private System.Windows.Forms.Button btn_CasseteUnload_B_LiftTiltSlinderUp;
        private System.Windows.Forms.Button btn_CasseteUnload_B_LiftUngrib;
        private System.Windows.Forms.Button btn_CasseteUnload_B_LiftGrib;
        private System.Windows.Forms.Button btn_CasseteUnload_B_TopCstStopperDown;
        private System.Windows.Forms.Button btn_CasseteUnload_B_TopCstStopperUp;
        private System.Windows.Forms.Button btn_CasseteUnload_B_TopCstUngrib;
        private System.Windows.Forms.Button btn_CasseteUnload_B_TopCstGrib;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Button btn_CasseteUnload_A_BottomCstStopperDown;
        private System.Windows.Forms.Button btn_CasseteUnload_A_BottomCstStopperUp;
        private System.Windows.Forms.Button btn_CasseteUnload_A_BottomCstUngrib;
        private System.Windows.Forms.Button btn_CasseteUnload_A_BottomCstGrib;
        private System.Windows.Forms.Button btn_CasseteUnload_A_LiftTiltSlinderDown;
        private System.Windows.Forms.Button btn_CasseteUnload_A_LiftTiltSlinderUp;
        private System.Windows.Forms.Button btn_CasseteUnload_A_LiftUngrib;
        private System.Windows.Forms.Button btn_CasseteUnload_A_LiftGrib;
        private System.Windows.Forms.Button btn_CasseteUnload_A_TopCstStopperDown;
        private System.Windows.Forms.Button btn_CasseteUnload_A_TopCstStopperUp;
        private System.Windows.Forms.Button btn_CasseteUnload_A_TopCstUngrib;
        private System.Windows.Forms.Button btn_CasseteUnload_A_TopCstGrib;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveB_Z2CellOut;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveB_Z2Tilt;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveB_Z2Out;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveB_Z2InWait;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveB_Z2In;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveB_T4Out;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveB_T3Move;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveB_T4Move;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveB_T3In;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveA_Z1CellOut;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveA_Z1Tilt;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveA_Z1Out;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveA_Z1InWait;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveA_Z1In;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveA_T2Out;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveA_T1Move;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveA_T2Move;
        private System.Windows.Forms.Button btn_CasseteLoad_MoveA_T1In;
        private System.Windows.Forms.GroupBox gbox_Cellpurge_MoveCenter;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel21;
        private System.Windows.Forms.Button btn_Cellpurge_MoveCenter_Y2C;
        private System.Windows.Forms.Button btn_Cellpurge_MoveCenter_BBuffer;
        private System.Windows.Forms.Button btn_Cellpurge_MoveCenter_Y2Uld;
        private System.Windows.Forms.Button btn_Cellpurge_MoveCenter_Y1C;
        private System.Windows.Forms.Button btn_Cellpurge_MoveCenter_ABuffer;
        private System.Windows.Forms.Button btn_Cellpurge_MoveCenter_Y1Uld;
        private System.Windows.Forms.GroupBox gbox_Cellpurge_MoveB;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel20;
        private System.Windows.Forms.Button btn_Cellpurge_MoveB_X2BC;
        private System.Windows.Forms.Button btn_Cellpurge_MoveB_X2BW;
        private System.Windows.Forms.Button btn_Cellpurge_MoveB_X1BW;
        private System.Windows.Forms.Button btn_Cellpurge_MoveB_X1BC;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel19;
        private System.Windows.Forms.Button btn_Cellpurge_MoveA_X2AC;
        private System.Windows.Forms.Button btn_Cellpurge_MoveA_X2AW;
        private System.Windows.Forms.Button btn_Cellpurge_MoveA_X1AW;
        private System.Windows.Forms.Button btn_Cellpurge_MoveA_X1AC;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel18;
        private System.Windows.Forms.Button btn_Cellpurge_B_DestructionOn;
        private System.Windows.Forms.Button btn_Cellpurge_B_DestructionOff;
        private System.Windows.Forms.Button btn_Cellpurge_B_PneumaticOff;
        private System.Windows.Forms.Button btn_Cellpurge_B_PneumaticOn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel17;
        private System.Windows.Forms.Button btn_Cellpurge_A_DestructionOn;
        private System.Windows.Forms.Button btn_Cellpurge_A_DestructionOff;
        private System.Windows.Forms.Button btn_Cellpurge_A_PneumaticOff;
        private System.Windows.Forms.Button btn_Cellpurge_A_PneumaticOn;
        private System.Windows.Forms.GroupBox gbox_CellLoad_MoveLorUn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel28;
        private System.Windows.Forms.Button btn_CellLoad_MoveLorUn_PreAlignMark1;
        private System.Windows.Forms.Button btn_CellLoad_MoveLorUn_PreAlignMark2;
        private System.Windows.Forms.Button btn_CellLoad_MoveLorUn_Y1UnL;
        private System.Windows.Forms.Button btn_CellLoad_MoveLorUn_Y1L;
        private System.Windows.Forms.Button btn_CellLoad_MoveLorUn_X2L;
        private System.Windows.Forms.Button btn_CellLoad_MoveLorUn_X1L;
        private System.Windows.Forms.GroupBox gbox_CellLoad_MoveB;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel31;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel32;
        private System.Windows.Forms.Button btn_CellLoad_MoveB_BPickerM90Angle;
        private System.Windows.Forms.Button btn_CellLoad_MoveB_BPickerP90Angle;
        private System.Windows.Forms.Button btn_CellLoad_MoveB_BPicker0Angle;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel33;
        private System.Windows.Forms.Button btn_CellLoad_MoveB_BPickerUnL;
        private System.Windows.Forms.Button btn_CellLoad_MoveB_BPickerL;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel34;
        private System.Windows.Forms.Button btn_CellLoad_MoveB_X2BStageUnL;
        private System.Windows.Forms.Button btn_CellLoad_MoveB_X1BStageUnL;
        private System.Windows.Forms.GroupBox gbox_CellLoad_MoveA;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel26;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel30;
        private System.Windows.Forms.Button btn_CellLoad_MoveA_APickerM90Angle;
        private System.Windows.Forms.Button btn_CellLoad_MoveA_APickerP90Angle;
        private System.Windows.Forms.Button btn_CellLoad_MoveA_APicker0Angle;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel29;
        private System.Windows.Forms.Button btn_CellLoad_MoveA_APickerUnL;
        private System.Windows.Forms.Button btn_CellLoad_MoveA_APickerL;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel27;
        private System.Windows.Forms.Button btn_CellLoad_MoveA_X2AStageUnL;
        private System.Windows.Forms.Button btn_CellLoad_MoveA_X1AStageUnL;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel25;
        private System.Windows.Forms.Button btn_CellLoad_B_PickerDestructionOn;
        private System.Windows.Forms.Button btn_CellLoad_B_PickerDestructionOff;
        private System.Windows.Forms.Button btn_CellLoad_B_PickerUp;
        private System.Windows.Forms.Button btn_CellLoad_B_PickerDown;
        private System.Windows.Forms.Button btn_CellLoad_B_PickerPneumaticOn;
        private System.Windows.Forms.Button btn_CellLoad_B_PickerPneumaticOff;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel24;
        private System.Windows.Forms.Button btn_CellLoad_A_PickerDestructionOn;
        private System.Windows.Forms.Button btn_CellLoad_A_PickerDestructionOff;
        private System.Windows.Forms.Button btn_CellLoad_A_PickerUp;
        private System.Windows.Forms.Button btn_CellLoad_A_PickerDown;
        private System.Windows.Forms.Button btn_CellLoad_A_PickerPneumaticOn;
        private System.Windows.Forms.Button btn_CellLoad_A_PickerPneumaticOff;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel35;
        private System.Windows.Forms.Button btn_IRCutProcess_A_L1Pneumatic_Ch1On;
        private System.Windows.Forms.Button btn_IRCutProcess_A_L1Pneumatic_Ch1Off;
        private System.Windows.Forms.Button btn_IRCutProcess_A_L2Pneumatic_Ch1On;
        private System.Windows.Forms.Button btn_IRCutProcess_A_L2Pneumatic_Ch1Off;
        private System.Windows.Forms.Button btn_IRCutProcess_A_L1Pneumatic_Ch2On;
        private System.Windows.Forms.Button btn_IRCutProcess_A_L1Pneumatic_Ch2Off;
        private System.Windows.Forms.Button btn_IRCutProcess_A_L2Pneumatic_Ch2On;
        private System.Windows.Forms.Button btn_IRCutProcess_A_L2Pneumatic_Ch2Off;
        private System.Windows.Forms.GroupBox gbox_CellInput_B;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel23;
        private System.Windows.Forms.Button btn_CellInput_B_DestructionOn;
        private System.Windows.Forms.Button btn_CellInput_B_DestructionOff;
        private System.Windows.Forms.Button btn_CellInput_B_PneumaticOff;
        private System.Windows.Forms.Button btn_CellInput_B_PneumaticOn;
        private System.Windows.Forms.GroupBox gbox_CellInput_A;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel22;
        private System.Windows.Forms.Button btn_CellInput_A_DestructionOn;
        private System.Windows.Forms.Button btn_CellInput_A_DestructionOff;
        private System.Windows.Forms.Button btn_CellInput_A_PneumaticOff;
        private System.Windows.Forms.Button btn_CellInput_A_PneumaticOn;
        private System.Windows.Forms.GroupBox gbox_IRCutProcess_RightOffset;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel40;
        private System.Windows.Forms.Button btn_IRCutProcess_RightOffset_Right_Vision2Focus;
        private System.Windows.Forms.Button btn_IRCutProcess_RightOffset_Right_Focus2Vision;
        private System.Windows.Forms.Button btn_IRCutProcess_RightOffset_Left_Vision2Focus;
        private System.Windows.Forms.Button btn_IRCutProcess_RightOffset_Left_Focus2Vision;
        private System.Windows.Forms.GroupBox gbox_IRCutProcess_LeftOffset;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel39;
        private System.Windows.Forms.Button btn_IRCutProcess_LeftOffset_Right_Visoin2Focus;
        private System.Windows.Forms.Button btn_IRCutProcess_LeftOffset_Right_Focus2Visoin;
        private System.Windows.Forms.Button btn_IRCutProcess_LeftOffset_Left_Vision2Focus;
        private System.Windows.Forms.Button btn_IRCutProcess_LeftOffset_Left_Focus2Vision;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel38;
        private System.Windows.Forms.Button btn_IRCutProcess_MoveB_R1Camera;
        private System.Windows.Forms.Button btn_IRCutProcess_MoveB_R2Camera;
        private System.Windows.Forms.Button btn_IRCutProcess_MoveB_RightCellLoad;
        private System.Windows.Forms.Button btn_IRCutProcess_MoveB_RightCellUnload;
        private System.Windows.Forms.Button btn_IRCutProcess_MoveB_R1Laser;
        private System.Windows.Forms.Button btn_IRCutProcess_MoveB_R2Laser;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel37;
        private System.Windows.Forms.Button btn_IRCutProcess_MoveA_L1Camera;
        private System.Windows.Forms.Button btn_IRCutProcess_MoveA_L2Camera;
        private System.Windows.Forms.Button btn_IRCutProcess_MoveA_LeftCellLoad;
        private System.Windows.Forms.Button btn_IRCutProcess_MoveA_LeftCellUnload;
        private System.Windows.Forms.Button btn_IRCutProcess_MoveA_L1Laser;
        private System.Windows.Forms.Button btn_IRCutProcess_MoveA_L2Laser;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel36;
        private System.Windows.Forms.Button btn_IRCutProcess_A_R2Destruction_Ch2Off;
        private System.Windows.Forms.Button btn_IRCutProcess_A_R2Destruction_Ch2On;
        private System.Windows.Forms.Button btn_IRCutProcess_A_R1Destruction_Ch2Off;
        private System.Windows.Forms.Button btn_IRCutProcess_A_R1Destruction_Ch2On;
        private System.Windows.Forms.Button btn_IRCutProcess_B_R1Pneumatic_Ch1Off;
        private System.Windows.Forms.Button btn_IRCutProcess_B_R2Pneumatic_Ch1On;
        private System.Windows.Forms.Button btn_IRCutProcess_B_R2Pneumatic_Ch1Off;
        private System.Windows.Forms.Button btn_IRCutProcess_B_R1Pneumatic_Ch2Off;
        private System.Windows.Forms.Button btn_IRCutProcess_B_R2Pneumatic_Ch2On;
        private System.Windows.Forms.Button btn_IRCutProcess_B_R2Pneumatic_Ch2Off;
        private System.Windows.Forms.Button btn_IRCutProcess_B_R1Pneumatic_Ch2On;
        private System.Windows.Forms.Button btn_IRCutProcess_B_R1Pneumatic_Ch1On;
        private System.Windows.Forms.Button btn_IRCutProcess_A_R1Destruction_Ch1On;
        private System.Windows.Forms.Button btn_IRCutProcess_A_R1Destruction_Ch1Off;
        private System.Windows.Forms.Button btn_IRCutProcess_A_R2Destruction_Ch1On;
        private System.Windows.Forms.Button btn_IRCutProcess_A_R2Destruction_Ch1Off;
        private System.Windows.Forms.Button btn_IRCutProcess_A_L1Destruction_Ch2Off;
        private System.Windows.Forms.Button btn_IRCutProcess_A_L1Destruction_Ch2On;
        private System.Windows.Forms.Button btn_IRCutProcess_A_L2Destruction_Ch2Off;
        private System.Windows.Forms.Button btn_IRCutProcess_A_L2Destruction_Ch2On;
        private System.Windows.Forms.Button btn_IRCutProcess_A_L1Destruction_Ch1On;
        private System.Windows.Forms.Button btn_IRCutProcess_A_L1Destruction_Ch1Off;
        private System.Windows.Forms.Button btn_IRCutProcess_A_L2Destruction_Ch1On;
        private System.Windows.Forms.Button btn_IRCutProcess_A_L2Destruction_Ch1Off;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel43;
        private System.Windows.Forms.Button btn_BreakTransfer_MoveA_Unload;
        private System.Windows.Forms.Button btn_BreakTransfer_MoveA_Load;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel42;
        private System.Windows.Forms.Button btn_BreakTransfer_B_PickerDestructionOn;
        private System.Windows.Forms.Button btn_BreakTransfer_B_PickerDestructionOff;
        private System.Windows.Forms.Button btn_BreakTransfer_B_PickerUp;
        private System.Windows.Forms.Button btn_BreakTransfer_B_PickerDown;
        private System.Windows.Forms.Button btn_BreakTransfer_B_PickerPneumaticOn;
        private System.Windows.Forms.Button btn_BreakTransfer_B_PneumaticOff;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel41;
        private System.Windows.Forms.Button btn_BreakTransfer_A_PickerDestructionOn;
        private System.Windows.Forms.Button btn_BreakTransfer_A_PickerDestructionOff;
        private System.Windows.Forms.Button btn_BreakTransfer_A_PickerUp;
        private System.Windows.Forms.Button btn_BreakTransfer_A_PickerDown;
        private System.Windows.Forms.Button btn_BreakTransfer_A_PickerPneumaticOn;
        private System.Windows.Forms.Button btn_BreakTransfer_A_PickerPneumaticOff;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel44;
        private System.Windows.Forms.Button btn_BreakTransfer_MoveB_Unload;
        private System.Windows.Forms.Button btn_BreakTransfer_MoveB_Load;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel45;
        private System.Windows.Forms.GroupBox gbox_BreakUnitXZ_MoveZ4;
        private System.Windows.Forms.GroupBox gbox_BreakUnitXZ_MoveZ3;
        private System.Windows.Forms.GroupBox gbox_BreakUnitXZ_MoveZ2;
        private System.Windows.Forms.GroupBox gbox_BreakUnitXZ_MoveZ1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel46;
        private System.Windows.Forms.Button gbox_BreakUnitXZ_MoveZ1_SlowDescent;
        private System.Windows.Forms.Button gbox_BreakUnitXZ_MoveZ1_VisionCheck;
        private System.Windows.Forms.Button gbox_BreakUnitXZ_MoveZ1_FastDescent;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel49;
        private System.Windows.Forms.Button gbox_BreakUnitXZ_MoveZ4_SlowDescent;
        private System.Windows.Forms.Button gbox_BreakUnitXZ_MoveZ4_VisionCheck;
        private System.Windows.Forms.Button gbox_BreakUnitXZ_MoveZ4_FastDescent;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel48;
        private System.Windows.Forms.Button gbox_BreakUnitXZ_MoveZ3_SlowDescent;
        private System.Windows.Forms.Button gbox_BreakUnitXZ_MoveZ3_VisionCheck;
        private System.Windows.Forms.Button gbox_BreakUnitXZ_MoveZ3_FastDescent;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel47;
        private System.Windows.Forms.Button gbox_BreakUnitXZ_MoveZ2_SlowDescent;
        private System.Windows.Forms.Button gbox_BreakUnitXZ_MoveZ2_VisionCheck;
        private System.Windows.Forms.Button gbox_BreakUnitXZ_MoveZ2_FastDescent;
        private System.Windows.Forms.GroupBox gbox_BreakUnitTY_Shutter;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel53;
        private System.Windows.Forms.Button btn_BreakUnitTY_Shutter_Close;
        private System.Windows.Forms.Button btn_BreakUnitTY_Shutter_Open;
        private System.Windows.Forms.GroupBox gbox_BreakUnitTY_DummyBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel52;
        private System.Windows.Forms.Button btn_BreakUnitTY_DummyBox_Output;
        private System.Windows.Forms.Button btn_BreakUnitTY_DummyBox_Input;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel54;
        private System.Windows.Forms.GroupBox gbox_BreakUnitTY_Theta;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel57;
        private System.Windows.Forms.Button btn_BreakUnitTY_Theta_T2Wait;
        private System.Windows.Forms.Button btn_BreakUnitTY_Theta_T4Wait;
        private System.Windows.Forms.Button btn_BreakUnitTY_Theta_T1Wait;
        private System.Windows.Forms.Button btn_BreakUnitTY_Theta_T3Wait;
        private System.Windows.Forms.GroupBox gbox_BreakUnitTY_MoveB;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel56;
        private System.Windows.Forms.Button btn_BreakUnitTY_MoveB_Unload;
        private System.Windows.Forms.Button btn_BreakUnitTY_MoveB_Load;
        private System.Windows.Forms.GroupBox gbox_BreakUnitTY_MoveA;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel55;
        private System.Windows.Forms.Button btn_BreakUnitTY_MoveA_Unload;
        private System.Windows.Forms.Button btn_BreakUnitTY_MoveA_Load;
        private System.Windows.Forms.GroupBox gbox_BreakUnitTY_B;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel50;
        private System.Windows.Forms.Button btn_BreakUnitTY_B_1Destruction_On;
        private System.Windows.Forms.Button btn_BreakUnitTY_B_1Destruction_Off;
        private System.Windows.Forms.Button btn_BreakUnitTY_B_2Destruction_On;
        private System.Windows.Forms.Button btn_BreakUnitTY_B_2Destruction_Off;
        private System.Windows.Forms.Button btn_BreakUnitTY_B_1Pneumatic_Off;
        private System.Windows.Forms.Button btn_BreakUnitTY_B_1Pneumatic_On;
        private System.Windows.Forms.Button btn_BreakUnitTY_B_2Pneumatic_On;
        private System.Windows.Forms.Button btn_BreakUnitTY_B_2Pneumatic_Off;
        private System.Windows.Forms.GroupBox gbox_BreakUnitTY_A;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel51;
        private System.Windows.Forms.Button btn_BreakUnitTY_A_1Destruction_On;
        private System.Windows.Forms.Button btn_BreakUnitTY_A_1Destruction_Off;
        private System.Windows.Forms.Button btn_BreakUnitTY_A_2Destruction_On;
        private System.Windows.Forms.Button btn_BreakUnitTY_A_2Destruction_Off;
        private System.Windows.Forms.Button btn_BreakUnitTY_A_1Pneumatic_Off;
        private System.Windows.Forms.Button btn_BreakUnitTY_A_1Pneumatic_On;
        private System.Windows.Forms.Button btn_BreakUnitTY_A_2Pneumatic_On;
        private System.Windows.Forms.Button btn_BreakUnitTY_A_2Pneumatic_Off;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel61;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel62;
        private System.Windows.Forms.Button btn_UnloaderTransfer_MoveB_T40Angle;
        private System.Windows.Forms.Button btn_UnloaderTransfer_MoveB_T4P90Angle;
        private System.Windows.Forms.Button btn_UnloaderTransfer_MoveB_T4M90Angle;
        private System.Windows.Forms.Button btn_UnloaderTransfer_MoveB_T30Angle;
        private System.Windows.Forms.Button btn_UnloaderTransfer_MoveB_T3P90Angle;
        private System.Windows.Forms.Button btn_UnloaderTransfer_MoveB_T3M90Angle;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel63;
        private System.Windows.Forms.Button btn_UnloaderTransfer_MoveB_BStage;
        private System.Windows.Forms.Button btn_UnloaderTransfer_MoveB_BUnloading;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel58;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel60;
        private System.Windows.Forms.Button btn_UnloaderTransfer_MoveA_T20Angle;
        private System.Windows.Forms.Button btn_UnloaderTransfer_MoveA_T2P90Angle;
        private System.Windows.Forms.Button btn_UnloaderTransfer_MoveA_T2M90Angle;
        private System.Windows.Forms.Button btn_UnloaderTransfer_MoveA_T10Angle;
        private System.Windows.Forms.Button btn_UnloaderTransfer_MoveA_T1P90Angle;
        private System.Windows.Forms.Button btn_UnloaderTransfer_MoveA_T1M90Angle;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel59;
        private System.Windows.Forms.Button btn_UnloaderTransfer_MoveA_AStage;
        private System.Windows.Forms.Button btn_UnloaderTransfer_MoveA_AUnloading;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel65;
        private System.Windows.Forms.Button btn_CameraUnit_MoveB_InspectionMove4;
        private System.Windows.Forms.Button btn_CameraUnit_MoveB_InspectionMove3;
        private System.Windows.Forms.Button btn_CameraUnit_MoveB_MCRMove;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel64;
        private System.Windows.Forms.Button btn_CameraUnit_MoveA_InspectionMove2;
        private System.Windows.Forms.Button btn_CameraUnit_MoveA_InspectionMove1;
        private System.Windows.Forms.Button btn_CameraUnit_MoveA_MCRMove;
        private System.Windows.Forms.GroupBox gbox_CellInput_Buffer;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel66;
        private System.Windows.Forms.Button btn_CellInput_Buffer_DestructionOn;
        private System.Windows.Forms.Button btn_CellInput_Buffer_DestructionOff;
        private System.Windows.Forms.Button btn_CellInput_Buffer_PickerUp;
        private System.Windows.Forms.Button btn_CellInput_Buffer_PickerDown;
        private System.Windows.Forms.Button btn_CellInput_Buffer_PneumaticOn;
        private System.Windows.Forms.Button btn_CellInput_Buffer_PneumaticOff;
        private System.Windows.Forms.GroupBox gbox_CellInput_MoveCenter;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel69;
        private System.Windows.Forms.Button btn_CellInput_MoveCenter_ABuffer;
        private System.Windows.Forms.Button btn_CellInput_MoveCenter_BBuffer;
        private System.Windows.Forms.Button btn_CellInput_MoveCenter_Y1CstMid;
        private System.Windows.Forms.Button btn_CellInput_MoveCenter_Y2CstMid;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel68;
        private System.Windows.Forms.Button btn_CellInput_MoveB_X2BUld;
        private System.Windows.Forms.Button btn_CellInput_MoveB_Y2BUld;
        private System.Windows.Forms.Button btn_CellInput_MoveB_X2BCstMid;
        private System.Windows.Forms.Button btn_CellInput_MoveB_X2BCstWait;
        private System.Windows.Forms.Button btn_CellInput_MoveB_X1AUld;
        private System.Windows.Forms.Button btn_CellInput_MoveB_Y1BUld;
        private System.Windows.Forms.Button btn_CellInput_MoveB_X1BCstMId;
        private System.Windows.Forms.Button btn_CellInput_MoveB_X1BCstWait;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel67;
        private System.Windows.Forms.Button btn_CellInput_MoveA_X2AUld;
        private System.Windows.Forms.Button btn_CellInput_MoveA_Y2AUld;
        private System.Windows.Forms.Button btn_CellInput_MoveA_X2ACstMid;
        private System.Windows.Forms.Button btn_CellInput_MoveA_X2ACstWait;
        private System.Windows.Forms.Button btn_CellInput_MoveA_X1AUld;
        private System.Windows.Forms.Button btn_CellInput_MoveA_Y1AUld;
        private System.Windows.Forms.Button btn_CellInput_MoveA_X1ACstMid;
        private System.Windows.Forms.Button btn_CellInput_MoveA_X1ACstWait;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel75;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel76;
        private System.Windows.Forms.Button btn_CasseteUnload_MoveB_LiftZ2CellOut;
        private System.Windows.Forms.Button btn_CasseteUnload_MoveB_LiftZ2CstTilt;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel77;
        private System.Windows.Forms.Button btn_CasseteUnload_MoveB_LiftZ2CstOut;
        private System.Windows.Forms.Button btn_CasseteUnload_MoveB_LiftZ2CstWait;
        private System.Windows.Forms.Button btn_CasseteUnload_MoveB_LiftZ2CstIn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel78;
        private System.Windows.Forms.Button btn_CasseteUnload_MoveB_BottomT4LiftOut;
        private System.Windows.Forms.Button btn_CasseteUnload_MoveB_TopT3CstMove;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel79;
        private System.Windows.Forms.Button btn_CasseteUnload_MoveB_BottomT4CstIn;
        private System.Windows.Forms.Button btn_CasseteUnload_MoveB_TopT3Out;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel70;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel71;
        private System.Windows.Forms.Button btn_CasseteUnload_MoveA_LiftZ1CellOut;
        private System.Windows.Forms.Button btn_CasseteUnload_MoveA_LiftZ1CstTilt;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel72;
        private System.Windows.Forms.Button btn_CasseteUnload_MoveA_LiftZ1CstOut;
        private System.Windows.Forms.Button btn_CasseteUnload_MoveA_LiftZ1CstWait;
        private System.Windows.Forms.Button btn_CasseteUnload_MoveA_LiftZ1CstIn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel73;
        private System.Windows.Forms.Button btn_CasseteUnload_MoveA_BottomT2Move;
        private System.Windows.Forms.Button btn_CasseteUnload_MoveA_TopT1Move;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel74;
        private System.Windows.Forms.Button btn_CasseteUnload_MoveA_BottomT2In;
        private System.Windows.Forms.Button btn_CasseteUnload_MoveA_TopT1Out;
    }
}
